﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExcelCalcService.Models
{
    public class ResponseModel
    {
        public bool key { get; set; }
        public string formula { get; set; }
        public string value { get; set; }
    }
}