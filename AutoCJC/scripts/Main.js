﻿$(document).ready(function () {
    $('input[maxlength],textarea[maxlength]').maxlength({ alwaysShow: true, });
});

function showLoading(flag) {    
    if (flag) {       
        $(".quick-nav-overlay").show();
        $("#divLoader").show();
    }
    else {
        $("#divLoader").hide();
        $(".quick-nav-overlay").hide();
    }
}

function clearFirstRow(objFirstLine) {
    $('input[type="text"],select', objFirstLine).val('');
    $('input[type="checkbox"]', objFirstLine).prop('checked', false);
}

function DisplayNotiMessage(Type, Message, Title) {
    if (Type.toLowerCase() == "success") {
        toastr.success(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    else if (Type.toLowerCase() == "error") {
        toastr.error(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    else if (Type.toLowerCase() == "info") {
        toastr.info(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
    else if (Type.toLowerCase() == "warning") {
        toastr.warning(Message, Title, {
            "closeButton": true,
            "debug": false,
            "positionClass": "toast-top-right",
            "onclick": null,
            "showDuration": "1",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        });
    }
}

function GetGlobalData(ID, Key, strBU, strLocation) {
    $.ajax({
        url: "/Utility/General/GetGlobalData",
        type: 'POST',
        async: false,
        data: { Key: Key, strBU: strBU, strLocation: strLocation },
        success: function (result) {

        }
    });
}

jQuery.validator.addMethod(
    "multiemail",
     function (value, element) {
         if (this.optional(element)) // return true on optional element 
             return true;
         var emails = value.split(/[;,]+/); // split element by , and ;
         valid = true;
         for (var i in emails) {
             value = emails[i];
             valid = valid &&
                     jQuery.validator.methods.email.call(this, $.trim(value), element);
         }
         return valid;
     },

    jQuery.validator.messages.email
);

function DisableBtn(objectctrl) {
    $(objectctrl).prop("disabled", true);
}

function EnableBtn(objectctrl) {
    $(objectctrl).removeAttr("disabled", true);
}

function SetReadonly(objectctrl) {
    $(objectctrl).prop("readonly", "readonly");
}

function HideControl(objectctrl) {
    $(objectctrl).addClass('hideControl');
}

function RemoveReadonly(objectctrl) {
    $(objectctrl).removeAttr("readonly");
}

$('.set-numeric').keydown(function (e) {
    // Allow: backspace, delete, tab, escape, enter
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        // Allow: Ctrl+A, Command+A
        (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
        // Allow: home, end, left, right, down, up
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        // let it happen, don't do anything
        return;
    }
    // Ensure that it is a number and stop the keypress
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
});
// Added by M. Ozair Khatri (90385894) on 24-08-2017
/**
 * Opens a new Tab with the report defined by ReportPath and ParameterData
 * @param {Array[string,string]} ParameterData - data in the format : [{Param:"Param1", Value:"Param1Value"}, {Param:"Param2", Value:"Param2Value1"}, {Param:"Param2", Value:"Param2Value2"} ]
 * @param {String} ReportPath - Server Relative report path : '/Reports/SubFolder/SubFolder2/SampleReport'
 */
function ShowReport(ParameterData, ReportPath, ShowAttachment) {
    if (typeof (ShowAttachment) == "undefined")
        ShowAttachment = false;
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/Home/OpenSSRSReport",
        dataType: "html",
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            serverRelativeUrlPath: ReportPath,
            ReportParameters: ParameterData,
            ShowAttachment: ShowAttachment
        },
        success: function (result) {
            if (result != 'Error') {
                var x = window.open();
                if (x == null) {
                    DisplayNotiMessage('Warning', 'Pop-up blocker has interrupted your request, please allow pop-ups to continue', 'Pop-ups blocked!');
                    return;
                }
                x.target = '_blank';
                x.document.write(result);
            }
            else {
                DisplayNotiMessage("error", "An Error occured while fetching the report.", "Error !");
            }
        }
    });
}

function GetSeamFilteredData(seamNo, Project, QualityProject, tooltip1) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/IPI/ApproveSeamList/GetSeamDetails",
        datatype: 'json',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: { Project: Project, QualityProject: QualityProject, seamNo: seamNo },
        success: function (data) {
            if (data.SeamNo != null) {
                GetFilteredData(data.QualityProject, data.Project, data.BU, data.Location, data.SeamNo, "", "", "", "", "", true, "Seam Details");
            }
            else {
                //DisplayNotiMessage("error", "Seam Details not available", "Error");
                bootbox.alert("Seam Details not available");
            }
        },
        error: function () {
        }
    });
}

function GetFilteredData(qualityProject, project, bu, location, partNo, projDesc, buDesc, locDesc, parentPart, childPart, isFromSeam, Title) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/IPI/PartlistOfferInspection/LoadPartlistFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            QualityProject: qualityProject,
            Project: project,
            BU: bu,
            Location: location,
            Number: partNo,
            ProjectDesc: projDesc,
            BUDesc: buDesc,
            LocationDesc: locDesc,
            ParentPartNo: parentPart,
            ChildPartNo: childPart,
            isFromSeam: isFromSeam
        },
        success: function (result) {
            bootbox.dialog({
                title: Title,
                message: result,
                size: 'large',
                buttons: {
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            }).find("div.modal-dialog").addClass("clsBootBoxWidth");
        },
        error: function () {
        }
    });
}
function GetReqFilteredData(Title, qualityProject, project, bu, location, partNo, projDesc, buDesc, locDesc, parentPart, childPart, stageCode, stageDesc, stageSeq, tableID, isFromPopup) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/IPI/PartlistOfferInspection/LoadFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            QualityProject: qualityProject,
            Project: project,
            BU: bu,
            Location: location,
            Number: partNo,
            ProjectDesc: projDesc,
            BUDesc: buDesc,
            LocationDesc: locDesc,
            ParentPartNo: parentPart,
            ChildPartNo: childPart,
            StageCode: stageCode,
            StageDesc: stageDesc,
            StageSeq: stageSeq,
            tableID: tableID
        },
        success: function (result) {
            if (isFromPopup) {
                $("#divReqDetails").html(result);
                $("#frmReqFilteredData").hide();
            }
            else {
                bootbox.dialog({
                    title: Title,
                    message: result,
                    size: 'large',
                    buttons: {
                        'cancel': {
                            label: 'Close',
                            className: 'btn btnclose'
                        }
                    }
                }).find("div.modal-dialog").addClass("clsBootBoxWidth");
            }
        },
        error: function () {
        }
    });
}

function GetLTFPSFilteredData(LTFPSNo, qualityProject, project, bu, location, partNo, buDesc, locDesc, isFromSeam, Title) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/LTFPS/OfferInspection/LoadLTFPSFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            LTFPSNo: LTFPSNo,
            QualityProject: qualityProject,
            Project: project,
            BU: bu,
            Location: location,
            Number: partNo,
            ProjectDesc: "",
            BUDesc: buDesc,
            LocationDesc: locDesc,
            ParentPartNo: "",
            ChildPartNo: "",
            isFromSeam: isFromSeam
        },
        success: function (result) {
            bootbox.dialog({
                title: Title,
                message: result,
                size: 'large',
                buttons: {
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            }).find("div.modal-dialog").addClass("clsBootBoxWidth");
        },
        error: function () {
        }
    });
}

function GetLTFPSReqFilteredData(Title, qualityProject, project, bu, location, partNo, projDesc, buDesc, locDesc, stageCode, stageDesc, stageSeq, isNDEStage, isFromSeam, isFromPopup) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/LTFPS/OfferInspection/LoadLTFPSReqFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            QualityProject: qualityProject,
            Project: project,
            BU: bu,
            Location: location,
            Number: partNo,
            ProjectDesc: projDesc,
            BUDesc: buDesc,
            LocationDesc: locDesc,
            ParentPartNo: "",
            ChildPartNo: "",
            StageCode: stageCode,
            StageDesc: stageDesc,
            StageSeq: stageSeq,
            isNDEStage: isNDEStage,
            isFromSeam: isFromSeam
        },
        success: function (result) {
            if (isFromPopup) {
                $("#divLTFPSReqDetails").html(result);
                $("#frmLTFPSReqFilteredData").hide();
            }
            else {
                bootbox.dialog({
                    title: Title,
                    message: result,
                    size: 'large',
                    buttons: {
                        'cancel': {
                            label: 'Close',
                            className: 'btn btnclose'
                        }
                    }
                }).find("div.modal-dialog").addClass("clsBootBoxWidth");
            }
        },
        error: function () {
        }
    });
}
//created by Nikita for traveller
function PrintTVLReport(projectno, travelerno, travelerrevno, identificationNo, subassemblyno) {
    var url = "/TVL/Traveler";
    var dat = [];
    dat.push({ Param: "Project", Value: projectno });
    dat.push({ Param: "TravelerNo", Value: travelerno });
    dat.push({ Param: "TravelerRevNo", Value: parseInt(travelerrevno) });
    dat.push({ Param: "AssemblyNo", Value: identificationNo });
    dat.push({ Param: "SubAssemblyNo", Value: subassemblyno });
    ShowReport(dat, url);
}
function GetTravelerFilteredData(ProjectNo, Customer, travellerno, revno, identificationno, bu, location, subassemblyno, Title) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/TVL/OfferInspection/LoadOfferFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            ProjectNo: ProjectNo,
            Customer: Customer,
            TravelerNo: travellerno,
            TravelRev: revno,
            IdentificationNo: identificationno,
            BU: bu,
            Location: location,
            SubAssemblyNo: subassemblyno
        },
        success: function (result) {
            bootbox.dialog({
                title: Title,
                message: result,
                size: 'large',
                buttons: {
                    'cancel': {
                        label: 'Close',
                        className: 'btn btnclose'
                    }
                }
            }).find("div.modal-dialog").addClass("clsBootBoxWidth");
        },
        error: function () {
        }
    });
}

//created By Nikita For traveller request pop up
function GetTravelerReqFilteredData(ProjectNo, Customer, travellerno, revno, identificationno, bu, location, operationno, activity, subassemblyno, hdrId, Title, tableID, isFromPopup) {
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/TVL/OfferInspection/LoadRequestFilteredDataPartial",
        datatype: 'html',
        async: false,
        beforeSend: function () {
            showLoading(true);
        },
        complete: function () {
            showLoading(false);
        },
        data: {
            ProjectNo: ProjectNo,
            Customer: Customer,
            TravelerNo: travellerno,
            TravelRev: revno,
            IdentificationNo: identificationno,
            BU: bu,
            Location: location,
            OperationNo: operationno,
            Activity: activity,
            SubAssemblyNo: subassemblyno,
            HeaderId: hdrId,
            tableID: tableID
        },
        success: function (result) {

            if (isFromPopup) {
                $("#divReqDetails").html(result);
                $("#frmReqFilteredData").hide();
            }
            else {
                bootbox.dialog({
                    title: Title,
                    message: result,
                    size: 'large',
                    buttons: {
                        'cancel': {
                            label: 'Close',
                            className: 'btn btnclose'
                        }
                    }
                }).find("div.modal-dialog").addClass("clsBootBoxWidth");
            }
        },
        error: function () {
        }
    });
}

function ViewTVLRefDocAttachment(lineid, revno) {
    var objFileUploadLine = new FileUploadEnt('dvFileUploadLineID' + lineid, 'FileUploadLineID' + lineid);
    objFileUploadLine.ShowDeleteAction = false;
    objFileUploadLine.ShowAttachmentBtn = false;
    objFileUploadLine.FolderPath = "TVL011/" + lineid + "/R" + revno;
    objFileUploadLine.LoadInPopup = true;
    LoadFileUploadPartialView(objFileUploadLine);
}