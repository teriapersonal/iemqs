﻿var ServerSideError = "Error in Server Side";
var ClientSideError = "Error in Script";
var fltid = 0;
var Equal = "Equal";
var DntEqual = "Does Not Equal";
var Swith = "Starts With";
var Gequal = "Greater Then or Equals";
var Lequal = "Less Then or Equals";
var Gthen = "Greater Then";
var Lthen = "Less Then";
var Contains = "Contains";
var DntContains = "Does Not Contains";
var Endswith = "Ends With";
var Dntendswith = "Does Not Ends With";
var isempty = "Is Empty";
var Isnotempty = "Is Not Empty";
var Filterstatus;
var fltid = 0;
var fltothers = 0;
var type;
var typeothers;
var string = "";
var classname = "";
var FltType = "";
var Imagepath
var Fltcross = 0;
var FilterTitle;
var filterItemArray = [];
var Table_status;
$(document).ready(function () {
    $('body').click(function (evt) {
        if (evt.target.className === "iconspace fa fa-filter fltbtn") {
            return false;
        }
        else {
            $(".dvfilters").hide();
        }
    });
});

// Developed By Deepak Tiwari
/* ---- How To use Search Filter ----- //
//-------------------------------------//

    //* below css is only used for UI issue.
    <link href="~/Content/DataTable/searchFilter.custom.dataTables.css" rel="stylesheet" />

 *  below js must be included after the datatable.min.js
    <script src="~/Scripts/common.js"></script>
    
 *  below function must be execute after datatable initialize
    
    InitSearchFilter("TableId")   //Table id (must be pre init with dataTable() ) | Required
    or
    $('#tblTableID').dtSearchFilter();  // this function must be execute after datatable initialize
    
    [Note]:
    =======
    your datatable must define as below
    $('#tblTableID').dataTable({ .....,
        "columns": [
                // to set on column
                {
                    "name": "Title", // must same as Database Table Field name | Required
                    "attr":"date", // column fiter type ["string","numeric","date","multiple","dropdown"]  | default: "string" 
                    "ColData": "JsonData/URL" // to set autocomplete add jsondata content or url
                },
                // to not generate searchbox by column below setting must be implement
                {
                    "sClass": "hide", // set only one class "hide" to igoner searchbox
                    //"name": "Title", // don't add column name
                    "attr":"", // Use "" or false to not generate searchbox
                },
        ],
    });
    
*/

jQuery.fn.dtSearchFilter = function () {
    try {
        InitSearchFilter(this[0].id)
        return this;
    } catch (e) {
        console.log(e);
        return false;
    }
}
function Filtervalue(Colvalue, table_id) {

    fltid = fltid + 1;
    var str = "<div class='btngroup' id='btn' style='margin-top:6px'>"
  // +  "<a  onclick='OpenFilterContol(this," + fltid + ")' id='aflt" + fltid + "' class='iconspace fa fa-filter fltbtn' style='color: darkgrey;width:16px' title='Filter by Multiple Options' data-toggle='dropdown' data-hover='dropdown' data-close-others='true' >"
//+ "<a  onclick='OpenFilterContol(this," + fltid + ")' id='aflt" + fltid + "' class='dropdown-toggle'   style='color:#47494c' title='Filter by Multiple Options' data-toggle='dropdown' data-hover='dropdown' data-close-others='true' ><img src='" + WebsiteURL + "/Images/contains.png' style='padding-right: 5px'; />"
  + "<img id='img" + fltid + "' src='" + WebsiteURL + "/Images/contains.png' data-attType='" + Colvalue + "' onclick=\"OpenFilterContol(this," + fltid + ",'" + table_id + "')\" id='aflt" + fltid + "' class='iconspace fa fa-filter fltbtn'   style='color:#47494c' title='Filter by Multiple Options' data-toggle='dropdown' data-hover='dropdown' data-close-others='true' />"
   // +"</a>"
    + "</div>"
    return str;
}
function GetFilterControl(fltid, table_id) {

    var str = "<div id='dvfilters" + fltid + "' data-aid='' class='dvfilters dropdown-menu hold-on-click dropdown-checkboxes pull-right border border clsdropdown' style='z-index:99999'>"
        + "<label id='lblequal' class='checkbox-inline'   onclick=\"LoadDatatable(Equal,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rdequal'  style='color:#47494c'><img src='" + WebsiteURL + "/Images/equal.png' style='padding-right: 5px'; /> Equal</a>"
        + "</label><span></span>"
        + "<label id='lbldntequal' class='checkbox-inline' onclick=\"LoadDatatable(DntEqual,this," + fltid + ",'" + table_id + "')\"  style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a id='rddntequal'  style='color:#47494c;'><img src='" + WebsiteURL + "/Images/notequal.png' style='padding-right: 5px'; />Does Not Equal</a>"
        + "</label>"
        + "<span></span>"
        + "<label id='lblswith' class='checkbox-inline' onclick=\"LoadDatatable(Swith,this," + fltid + ",'" + table_id + "')\"  style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rdswith'  style='color:#47494c'><img src='" + WebsiteURL + "/Images/startwith.png' style='padding-right: 5px'; /> Starts With</a>"
        + "</label><span></span>"
        + "<label id='lblcontains'  class='checkbox-inline' onclick=\"LoadDatatable(Contains,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rdcontains'  style='color:#47494c'><img src='" + WebsiteURL + "/Images/contains.png' style='padding-right: 5px'; />Contains</a>"
        + "</label><span></span>"
        + "<label id='lbldoesnotcontains' class='checkbox-inline' onclick=\"LoadDatatable(DntContains,this," + fltid + ",'" + table_id + "')\"  style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rddoesnotcontains'  style='color:#47494c'><img src='" + WebsiteURL + "/Images/notcontains.png' style='padding-right: 5px'; />Does Not Contains</a>"
        + "</label><span></span>"
        + "<label id='lblendswith' class='checkbox-inline' onclick=\"LoadDatatable(Endswith,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rdendswith'  style='color: #47494c'><img src='" + WebsiteURL + "/Images/endwith.png' style='padding-right: 5px'; />Ends With </a>"
        + "</label><span></span>"
        + "<label id='lbldntendswith' class='checkbox-inline' onclick=\"LoadDatatable(Dntendswith,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rddntendswith'  style='color:#47494c;'><img src='" + WebsiteURL + "/Images/notendwith.png' style='padding-right: 5px'; /> Does Not Ends With</a>"
        + "</label><span></span>"
        + "<label id='lblisempty' class='checkbox-inline' onclick=\"LoadDatatable(isempty,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rdisempty'  style='color:#47494c'><img src='" + WebsiteURL + "/Images/isempty.png' style='padding-right: 5px'; /> Is Empty</a>"
        + "</label><span></span>"
        + "<label id='lblisnotempty' class='checkbox-inline' onclick=\"LoadDatatable(Isnotempty,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rdisnotempty'  style='color:#47494c;'><img src='" + WebsiteURL + "/Images/isnotempty.png' style='padding-right: 5px'; />Is Not Empty</a>"
        + "</label><span></span>"
        + "</div>";
    return str;
}

function OpenFilterContol(flt, fltid, table_id) {

    //if($("#dvfilters").length==0)
    // {
    var filterOption = GetFilterControl(fltid, table_id);
    $("body").append(filterOption);
    // }
    //  .dropdown-menu
    var pos = $(flt).offset();

    $(".dvfilters").css({ "top": (pos.top + 8) + "px", "left": (pos.left - 190) + "px", "position": "absolute", "width": "200px" })

    var previd = $(".dvfilters").attr("data-aid");
    var newid = $(flt).attr('id');

    if (previd == undefined || previd == null || previd == "" || (previd == $(flt).attr('id'))) {
        $("#dvfilters" + fltid).toggle();
    }
    else {
        $("#dvfilters" + fltid).css({ "display": "inline-block" });
    }
    $("#dvfilters" + fltid).attr("data-aid", newid)
}
function Filtervalueother(Colvalue, table_id) {

    // fltothers = fltothers + 1;
    fltid = fltid + 1;
    var str = "<div class='btngroup' id='btn'  style='margin-top:6px;'>"
 //   + "<a onclick='OpenFilterContolother(this," + fltothers + ")'  id='afltother" + fltothers + "' class='iconspace fa fa-filter fltbtn' style='color: darkgrey;' title='Filter by Multiple Options' data-toggle='dropdown' data-hover='dropdown' data-close-others='true'>"
   // + "</a>"
   + "<img id='imgother" + fltid + "' src='" + WebsiteURL + "/Images/equal.png' data-attType='" + Colvalue + "'  onclick=\"OpenFilterContolother(this," + fltid + ",'" + table_id + "')\" id='aflt" + fltid + "' class='iconspace fa fa-filter fltbtn'   style='color:#47494c' title='Filter by Multiple Options' data-toggle='dropdown' data-hover='dropdown' data-close-others='true' />"
    + "</div>"

    return str;
}

function GetFilterControlother(fltid, table_id) {

    var str = "<div id='dvfiltersothers" + fltid + "' data-aid='' class='dvfilters dropdown-menu hold-on-click dropdown-checkboxes pull-right border border'>"
        + "<label id='lblequalother' class='checkbox-inline' onclick=\"LoadDatatable(Equal,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rdequal'   style='color:#47494c'><img sty src='" + WebsiteURL + "/Images/equal.png' style='padding-right: 5px' />Equal </a>"
        + "</label><span></span>"
        + "<label id='lbldntequalother' class='checkbox-inline' onclick=\"LoadDatatable(DntEqual,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a id='rddntequal'  style='color:#47494c'><img src='" + WebsiteURL + "/Images/notequal.png' style='padding-right: 5px' /> Does Not Equal </a>"
        + "</label>"
        + "<span></span>"
        + "<label id='lblgequalother' class='checkbox-inline' onclick=\"LoadDatatable(Gequal,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rdgequal'  style='color:#47494c'><img src='" + WebsiteURL + "/Images/greaterthenequal.png' style='padding-right: 5px' /> Greater Then or Equals</a>"
        + "</label><span></span>"
        + "<label id='lbllessequalother' class='checkbox-inline' onclick=\"LoadDatatable(Lequal,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rdlessequal'  style='color:#47494c'><img src='" + WebsiteURL + "/Images/lessthenequal.png' style='padding-right: 5px' />Less Then or Equals</a>"
        + "</label><span></span>"
        + "<label id='lblgreaterthenother' class='checkbox-inline' onclick=\"LoadDatatable(Gthen,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rdgreaterthen'  style='color:#47494c'><img src='" + WebsiteURL + "/Images/greaterthen.png' style='padding-right: 5px' /> Greater Then </a>"
        + "</label><span></span>"
        + "<label id='lbllessthenother' class='checkbox-inline'  onclick=\"LoadDatatable(Lthen,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rdlessthen'  style='color:#47494c'><img src='" + WebsiteURL + "/Images/lessthen.png' style='padding-right: 5px' /> Less Then</a>"
        + "</label><span></span>"
        + "<label id='lblisemptyother' class='checkbox-inline' onclick=\"LoadDatatable(isempty,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rdisempty'  style='color:#47494c'><img src='" + WebsiteURL + "/Images/isempty.png' style='padding-right: 5px' /> Is Empty </a>"
        + "</label><span></span>"
        + "<label id='lblisnotemptyother' class='checkbox-inline' onclick=\"LoadDatatable(Isnotempty,this," + fltid + ",'" + table_id + "')\" style='padding-left: 5px;padding-bottom: 5px;border-bottom:1px solid #eee'>"
        + "<a  id='rdisnotempty'  style='color:#47494c'><img src='" + WebsiteURL + "/Images/isnotempty.png' style='padding-right: 5px' /> IsNotEmpty </a>"
        + "</label><span></span>"
        + "</div>";
    return str;
}

function OpenFilterContolother(flt, fltid, table_id) {
    // if ($("#dvfiltersothers").length == 0) {
    var filterOptionother = GetFilterControlother(fltid, table_id);

    $("body").append(filterOptionother);
    //}

    var pos = $(flt).offset();
    $("#dvfiltersothers" + fltid).css({ "top": (pos.top + 8) + "px", "left": (pos.left - 190) + "px", "position": "absolute", "width": "200px" })

    var previd = $("#dvfiltersothers" + fltid).attr("data-aid");
    var newid = $(flt).attr('id');

    if (previd == undefined || previd == null || previd == "" || (previd == $(flt).attr('id'))) {
        $("#dvfiltersothers" + fltid).toggle();
    }
    else {
        $("#dvfiltersothers" + fltid).css({ "display": "inline-block" });
    }
    $("#dvfiltersothers" + fltid).attr("data-aid", newid)
}
//<th class='all fontwid'
function GenerateGridControl(table) {
    var FltType = "flt";
    var tablegrid = $('#' + table).DataTable();
    var columns = tablegrid.settings().init().columns;
    tableTRContent = "";
    
    tablegrid.columns().every(function (index) {
        var ColName = (columns[index].name);
        var ColType = (columns[index].attr);
        var ColClass = (columns[index].sClass) == undefined ? "" : (columns[index].sClass);
       
        if (ColType == undefined) { // Default
            ColType = "string";
            //columns[index].attr = "string";
        }
        if (ColType != "" && ColType != false && ColClass.indexOf("hide") == -1 && ColName != undefined) {
            Fltcross = Fltcross + 1;
            if (ColType == "string") {
                classname = "all";
                tableTRContent += "<th  style = 'min-width:100px; padding: 8px 2px !important;' ><div class='all fontwid' id='fltall" + ColName + "' style='display: inline-flex;'><div class='input-icon right'><i id='fltcloseIcon_img" + Fltcross + "' data-attr='" + ColName + "'  class='fa fa-close hidden clscloseIcon' style='cursor:pointer'></i><input type='text' data-attType='" + ColType + "' id='" + FltType + "" + ColName + "' name=flt" + ColName + "' style = 'padding-left: 2px !important;' class='form-control col-md-3'  data-flttype='" + Contains + "'/></div></div></th>"
            }
            else if (ColType == "numeric") {
                classname = "all";
                tableTRContent += "<th style='min-width:100px; padding: 8px 2px !important;' ><div class='all fontwid' id='fltallnumeric" + ColName + "' style='display: inline-flex;'><div class='input-icon right'><i id='fltcloseIcon_imgother" + Fltcross + "' class='fa fa-close hidden clscloseIcon' style='cursor:pointer'></i><input type='text' data-attType='" + ColType + "' id='" + FltType + "" + ColName + "' name=flt" + ColName + "'style = 'padding-left: 2px !important;' class='numeric form-control col-md-3'  data-flttype='" + Equal + "'/></div></div></th>"
            }
            else if (ColType == "date") {
                classname = "center";
                tableTRContent += "<th style = 'min-width:100px; padding: 8px 2px !important;'  ><div class='all fontwid' id='fltall" + ColName + "' style='display: inline-flex;'><div class='input-icon right'><i id='fltcloseIcon_imgother" + Fltcross + "' class='fa fa-close hidden clscloseIcon' style='cursor:pointer'></i><input type='date' data-attType='" + ColType + "' id='" + FltType + "" + ColName + "' name=flt" + ColName + "' style = 'padding-left: 2px !important;' class='form-control col-md-3'  data-flttype='" + Equal + "'/></div></div></th>"
            }
            else if (ColType == "dropdown") {
                classname = "center";
                tableTRContent += "<th  style = 'min-width:100px; padding: 8px 2px !important;' ><div class='all fontwid' id='fltall" + ColName + "' style='display: inline-flex;'><div class='input-icon right'><i id='fltcloseIcon_imgother" + Fltcross + "' class='fa fa-close hidden clscloseIcon' style='cursor:pointer'></i><select id='" + FltType + "" + ColName + "'  name=flt" + ColName + "' data-attType='" + ColType + "' style = 'padding-left: 2px !important;' class='form-control col-md-3'  data-flttype='" + Equal + "'><option value=''>Select</option> <option value='1'>Yes</option><option value='0'>No</option></select></div></div></th>"
            }
            else if (ColType == "multiple") {
                tableTRContent += "<th style = 'min-width:100px; padding: 8px 2px !important;' ><div class='all fontwid' id='fltall" + ColName + "' style='display: inline-flex;'><div class='input-icon right'><i id='fltcloseIcon_img" + Fltcross + "' class='fa fa-close hidden clscloseIcon' style='cursor:pointer'></i><select name='flt" + ColName + "' data-name='ddlmultiple' data-attType='" + ColType + "' id='" + FltType + "" + ColName + "' style = 'padding-left: 2px !important;'  class='form-control multiselect-drodown'  data-area='Roles' multiple='multiple' data-flttype='" + Equal + "' ></select></div></div></th>"
            }
            else
               tableTRContent += "<th class='min-phone-l'></th>"
        }
        else {
            if (ColClass.indexOf("hide") != -1) {
                tableTRContent += "<th class='min-phone-l hide'></th>"
            }
            else if ((Table_status == "All" && ColClass == "left18") || ColType == "s") {
                tableTRContent += "<th class='min-phone-l' style='display:none;'></th>"
            }
            else {               
                classname = "min-phone-l";
                tableTRContent += "<th class='min-phone-l' ></th>"
            }
            //else {
            //    classname = "min-phone-l";
            //    tableTRContent += "<th class='min-phone-l' ></th>"
            //}
        }
    });

    if ($('#' + table + ' > thead:first tr.mycustomfilter').length == 0) {
       $('#' + table + ' > thead:first ').prepend("<tr class='mycustomfilter'>" + tableTRContent + "</tr>");
    }

    $.each($("#" + table).DataTable().settings().init().columns, function (idx, col) {
        if (typeof (col.ColData) === "string") {
            LoadPreValueSelect2(col.ColData, $('#' + FltType + col.name, '#' + table), "Search..", '')
        } else if (col.ColData != undefined) {
            FillInlineAutoComplete($('#' + FltType + col.name, '#' + table), col.ColData)
        }
    });

}
function ImagePathurl(type) {
    var Equal = "Equal";
    var DntEqual = "Does Not Equal";
    var Swith = "Starts With";
    var Gequal = "Greater Then or Equals";
    var Lequal = "Less Then or Equals";
    var Gthen = "Greater Then";
    var Lthen = "Less Then";
    var Contains = "Contains";
    var DntContains = "Does Not Contains";
    var Endswith = "Ends With";
    var Dntendswith = "Does Not Ends With";
    var isempty = "Is Empty";
    var Isnotempty = "Is Not Empty";

    if (type == Equal) {
        Imagepath = WebsiteURL + "/Images/equal.png";
    }
    else if (type == DntEqual) {
        Imagepath = WebsiteURL + "/Images/notequal.png";
    }
    else if (type == Swith) {
        Imagepath = WebsiteURL + "/Images/startwith.png";
    }
    else if (type == Gequal) {
        Imagepath = WebsiteURL + "/Images/greaterthenequal.png";
    }
    else if (type == Lequal) {
        Imagepath = WebsiteURL + "/Images/lessthenequal.png";
    }
    else if (type == Gthen) {
        Imagepath = WebsiteURL + "/Images/greaterthen.png";
    }
    else if (type == Lthen) {
        Imagepath = WebsiteURL + "/Images/lessthen.png";
    }
    else if (type == Contains) {
        Imagepath = WebsiteURL + "/Images/contains.png";
    }
    else if (type == DntContains) {
        Imagepath = WebsiteURL + "/Images/notcontains.png";
    }
    else if (type == Endswith) {
        Imagepath = WebsiteURL + "/Images/endwith.png";
    }
    else if (type == Dntendswith) {
        Imagepath = WebsiteURL + "/Images/notendwith.png";
    }
    else if (type == isempty) {
        Imagepath = WebsiteURL + "/Images/isempty.png";
    }
    else if (type == Isnotempty) {
        Imagepath = WebsiteURL + "/Images/isnotempty.png";
    }
    return Imagepath;
}

function SetDtServerParameter(tableid, aoData) {

    try {
        SearchFilter = [];
        $.each($("#" + tableid).DataTable().settings().init().columns, function (idx, col) {
            var ColTitle = (col.name);
            var Colattr = (col.attr);
            var Colname = (col.ColName != undefined && col.ColName != "") ? col.ColName : ColTitle;
            var ColValue = $("#flt" + ColTitle, "#" + tableid).val();
            var FilterType = $("#flt" + ColTitle, "#" + tableid).attr('data-flttype');
            if (Colname != undefined && Colname != "" && ColValue != undefined && ColValue != "" && Colattr != "" && Colattr != false) {
                if (typeof (ColValue) === "object")
                    aoData.push({ "name": Colname, "value": ColValue });
                else
                    SearchFilter.push({ "ColumnName": Colname, "Value": ColValue, "FilterType": FilterType, "DataType": Colattr == undefined ? "string" : Colattr });
            } else if (Colname != undefined && Colname != "" && (FilterType == "Is Empty" || FilterType == "Is Not Empty") && Colattr != "" && Colattr != false) { // Apply filter without seachbox
                SearchFilter.push({ "ColumnName": Colname, "Value": "-", "FilterType": FilterType, "DataType": Colattr == undefined ? "string" : Colattr });
                $("#flt" + ColTitle, "#" + tableid).siblings('i').removeClass("hidden");
            }
        });
        aoData.push({ "name": "SearchFilter", "value": JSON.stringify(SearchFilter) });
        return JSON.stringify(SearchFilter);
    } catch (e) {
        console.log(e);
    }
}
function InitEventSearch(table_id) {
    var isTyping = false;
    $("#" + table_id + '_wrapper thead input').keyup(function () {
        $("#" + table_id).DataTable().search(''); //clear main searchbox
        $(this).siblings("i.clscloseIcon").removeClass('hidden');
        Filterstatus = "";
        FilterTitle = "";
        if (this.value.length > 0) {
            isTyping = true;
            setTimeout(function () {
                if (isTyping) {
                    isTyping = false; // stop typing for moment
                    setTimeout(function () {
                        if (!isTyping) { // check after moment
                            isTyping = true; // ignor mutiple threads
                            $("#" + table_id).DataTable().ajax.reload();
                        }
                    }, 1000);
                }
            }, 500);
        }
        else {
            $(this).siblings("i#fltcloseIcon").addClass('hidden');
            $("#" + table_id).DataTable().ajax.reload();
        }
    });

    $("#" + table_id + '_wrapper thead input[type="date"]').change(function () {
        $("#" + table_id).DataTable().search(''); //clear main searchbox
        var Atttypeid = ($(this).siblings("i").attr("id").split("_").pop());
        Imagepath = WebsiteURL + "/Images/equal.png";

        $('#' + Atttypeid + '').attr("src", "");
        $('#' + Atttypeid + '').attr("src", Imagepath);
        $(this).attr("data-flttype", '')
        Filterstatus = "";
        FilterTitle = "";
        $("#" + table_id).DataTable().ajax.reload();
    });

    $("#" + table_id + '_wrapper thead select').change(function () {
        $("#" + table_id).DataTable().search(''); //clear main searchbox
        Filterstatus = "";
        FilterTitle = "";
        $("#" + table_id).DataTable().ajax.reload();
    });
}

function InitClearSearchText(table_id) {
    
    $("#" + table_id + " .clscloseIcon").on("click", function () {
        var Atttype = ($(this).siblings("input").attr("data-atttype"));
        var Imageicon = $(this).attr('id').split("_").pop();
        $('#' + Imageicon + '').attr("src", ImagePathurl( (Atttype != "numeric" && Atttype != "date")? "Contains": "Equal"));

        $(this).siblings("input").attr("data-flttype", '')
        Filterstatus = "";
        FilterTitle = "";
        $(this).addClass('hidden');
        $(this).siblings('input').val('');
        $("#" + table_id).DataTable().ajax.reload();
    });
}

function LoadDatatable(filteredValue, value, fltid, table_id) {
    var id = $(value).attr('id');
    var img = $("#img" + fltid);
    if (id.indexOf('other') != -1) { 
        img= $("#imgother" + fltid);
    }
    img.attr("src", ImagePathurl(filteredValue));
    FilterTitle = img.attr('data-attType');

    $("#flt" + FilterTitle + "", '#' + table_id).attr('data-flttype', filteredValue);

    Filterstatus = filteredValue;
    table_id = table_id.replace('_wrapper', ''); // for Fixed Column patch
    $("#" + table_id).DataTable().ajax.reload();
    $('a').removeClass("active");
    $(this).addClass("active");
    $(".dvfilters").hide();
}
function BindFilterType(table_id) {

    var tablegrid = $('#' + table_id).DataTable();
    var columns = tablegrid.settings().init().columns;
    string = "";
   
    tablegrid.columns().every(function (index) {
        
        var Colvalue = (columns[index].name);
        var Colattr = (columns[index].attr) == undefined ? "string" : (columns[index].attr);
        var Colatts = (columns[index].sClass);

        if (Colvalue != undefined && Colattr != "" && Colattr != false && Colatts!="hide") {
            type = ($(this).find("input").attr("data-atttype"));
            typeothers = ($(this).find("select").attr("data-atttype"));

            if (Colattr == "string") {
                var htmlstring = Filtervalue(Colvalue, table_id);               
                if ($('#' + table_id).find('#fltall' + Colvalue + ' > div.btngroup').length == 0) {
                    $("#fltall" + Colvalue, '#' + table_id).prepend(htmlstring);
                }                
            }
            else if (Colattr == "date") {
                var htmlstringother = Filtervalueother(Colvalue, table_id);
                if ($('#' + table_id).find('#fltall' + Colvalue + ' > div.btngroup').length == 0) {
                    $("#fltall" + Colvalue, '#' + table_id).prepend(htmlstringother);
                }
            }
            else if (Colattr == "dropdown") {
            }
            else if (Colattr == "numeric") {
                var htmlstringother = Filtervalueother(Colvalue, table_id);
                if ($('#' + table_id).find('#fltallnumeric' + Colvalue + ' > div.btngroup').length == 0) {
                    $("#fltallnumeric" + Colvalue, '#' + table_id).prepend(htmlstringother);
                }
            }
            else if (typeothers == "string" || typeothers == undefined) {
                var htmlstring = Filtervalue(Colvalue, table_id);
                if ($('#' + table_id).find('#fltall' + Colvalue + ' > div.btngroup').length == 0) {
                    $("#fltall" + Colvalue, '#' + table_id).prepend(htmlstring);
                }
            }
        }
    });


}
function InitSearchFilter(table_id) {
    
    $('#' + table_id).on('preXhr.dt', function (e, settings, data) {
        data.SearchFilter = SetDtServerParameter(table_id, []);
    })

    setTimeout(function () {
        $('#' + table_id + " thead tr:eq(1) th").each(function (idx, th) {
            if (!$(th).is(":visible"))
                $('#' + table_id + "_wrapper thead tr:first th:eq(" + idx + ")").hide();
        });
        $('#' + table_id).DataTable().columns.adjust()
    }, 100);

    $('#' + table_id).on('responsive-resize.dt', function (e, datatable, columns) {
        $.each(columns, function (idx, visiblity) {
            if (visiblity)
                $('#' + table_id + "_wrapper thead tr:first th:eq(" + idx + ")").show();
            else
                $('#' + table_id + "_wrapper thead tr:first th:eq(" + idx + ")").hide();
        })
    });
    // this now works as well with the '.dt' appended

    $('#' + table_id).trigger('resize');
    GenerateGridControl(table_id);
    BindFilterType(table_id);
    InitEventSearch(table_id);
    InitClearSearchText(table_id);
}