﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace AutoCJC.Models
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
    public class SessionExpireFilterCustom : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string SessionExpiryURL = ConfigurationManager.AppSettings["SessionExpireURL"].ToString();
            HttpContext ctx = System.Web.HttpContext.Current;
            if (ctx.Session[clsCommon.LoginInfo] == null)
            {
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Home", action = "External", ReturnURL = SessionExpiryURL }));
                return;
            }

            base.OnActionExecuting(filterContext);
        }
    }
}