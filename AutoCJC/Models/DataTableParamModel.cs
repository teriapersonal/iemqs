﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutoCJC.Models
{
    public class DataTableParamModel
    {
        /// <summary>
        /// Request sequence number sent by DataTable, same value must be returned in response
        /// </summary>       
        public object[] aoData { get; set; }

        /// <summary>
        /// Request sequence number sent by DataTable, same value must be returned in response
        /// </summary>       
        public string sEcho { get; set; }

        /// <summary>
        /// Text used for filtering
        /// </summary>
        private string _sSearch { get; set; }
        public string sSearch { get { return _sSearch; } set { _sSearch = value == null ? null : value.Replace("'", "''").Replace("%", "[%]").Replace("_", "[_]"); } }

        /// <summary>
        /// Number of records that should be shown in table
        /// </summary>
        public int iDisplayLength { get; set; }

        /// <summary>
        /// First record that should be shown(used for paging)
        /// </summary>
        public int iDisplayStart { get; set; }

        /// <summary>
        /// Number of columns in table
        /// </summary>
        public int iColumns { get; set; }

        /// <summary>
        /// Number of columns that are used in sorting
        /// </summary>
        public int iSortingCols { get; set; }

        /// <summary>
        /// Comma separated list of column names
        /// </summary>
        public string sColumns { get; set; }
        public string CTQHeaderId { get; set; }
        public string Headerid { get; set; }
        public string Project { get; set; }
        public bool IsVisible { get; set; }

        //added for Temptt(Getgriddatatpartial)
        public string Location { get; set; }
        public string Createdby { get; set; }
        public string CreatedOn { get; set; }
        public string SearchFilter { get; set; }
        public bool Flag { get; set; }
        public string Status { get; set; }
        public string Shop { get; set; }
        public string CJCNo { get; set; }
    }
}