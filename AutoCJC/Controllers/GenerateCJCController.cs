﻿using AutoCJC.CJCService;
using AutoCJC.Models;
using IEMQSImplementation;
using IEMQSImplementation.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Net;
using System.Text;
using System.Web.Mvc;

namespace IEMQS.Areas.CJC.Controllers
{
    public class GenerateCJCController : clsBase
    {
        const string ActionAddNew = "ADD_NEW";
        const string ActionEdit = "EDIT";

        #region Index

        [SessionExpireFilterCustom]
        public ActionResult Index()
        {
            ViewBag.Title = "Auto Billing";
            return View();
        }

        public ActionResult LoadDataGridPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_LoadIndexGridDataPartial");
        }

        public ActionResult LoadDataGridData(DataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1 and CreatedBy = '" + objClsLoginInfo.UserName + "'";
                string[] columnName = { "Project", "ShopDesc", "DiaDesc", "LocationDesc", "PurchaseOdrerNo", "PurchaseOdrerLineNo", "BillNo", "CONVERT(nvarchar(20),BillDate,103)", "Status", "CONVERT(nvarchar(20),CreatedOn,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string[] arrPendingStatus = { clsImplementationEnum.AutoCJCStatus.Pending_with_Authoriser.GetStringValue(), clsImplementationEnum.AutoCJCStatus.Pending_with_A_cs.GetStringValue(), clsImplementationEnum.AutoCJCStatus.Pending_with_Certifier.GetStringValue(), clsImplementationEnum.AutoCJCStatus.Pending_with_Initiator.GetStringValue() };
                var lstLines = db.SP_CJC_GET_HEADER_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.HeaderId),
                                        Convert.ToString(c.Project),
                                        Convert.ToString(c.DiaDesc),
                                        Convert.ToString(c.LocationDesc),
                                        Convert.ToString(c.ShopDesc),
                                        Convert.ToString(c.PurchaseOdrerNo),
                                        Convert.ToString(c.PurchaseOdrerLineNo),
                                        Convert.ToString(c.BillNo),
                                        c.BillDate.HasValue ? Convert.ToDateTime(c.BillDate).ToString("dd/MM/yyyy"): "",
                                        Convert.ToString(c.CJCNo),
                                        Convert.ToString(c.Status != clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue() ? (string.IsNullOrWhiteSpace(c.LNCJCStatus) ?clsImplementationEnum.AutoCJCStatus.Deleted.GetStringValue(): c.LNCJCStatus) : c.Status),
                                        Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                        Helper.GenerateActionIcon(c.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/GenerateCJC/CJCDetails/"+c.HeaderId ,false)+
                                        Helper.GenerateActionIcon(c.HeaderId, "Print", "Print Report", "fa fa-print", "PrintReport('"+c.HeaderId+"')","" ,false)
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region CJC Detail Page

        [SessionExpireFilterCustom]
        public ActionResult CJCDetails(int Id = 0)
        {
            CJC020 objCJC020 = new CJC020();
            var JobDiaList = (from u in db.CJC003
                              where u.IsActive == true
                              select new { Key = u.Id, Value = u.Description }).ToList();
            var Shops = db.CJC015
                            .Where(x => x.VendorCode == objClsLoginInfo.UserName)
                            .Select(x => new { Key = x.ShopCode, Value = x.ShopCode }).Distinct().ToList();

            int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
            var LocationList = (from com2 in db.COM002
                                where com2.t_dtyp == LOC
                                select new { Key = com2.t_dimx, Value = com2.t_desc, CatID = com2.t_dimx, CatDesc = com2.t_desc, }).Distinct().ToList();

            ViewBag.BusinessPartnerName = objClsLoginInfo.UserName + "-" + objClsLoginInfo.Name;
            if (Id > 0)
            {
                objCJC020 = db.CJC020.Where(x => x.HeaderId == Id).FirstOrDefault();
                if (objCJC020 != null)
                {
                    string status = (objCJC020.Status != clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue()) ? Manager.GetAutoCJCStatus(objCJC020.CJCNo) : objCJC020.Status;
                    objCJC020.Status = string.IsNullOrWhiteSpace(status) ? clsImplementationEnum.AutoCJCStatus.Deleted.GetStringValue() : status;
                    ViewBag.Location = LocationList.Where(x => x.Key == objCJC020.Location).Select(x => x.Value).FirstOrDefault();
                    ViewBag.Project = Manager.GetProjectAndDescription(objCJC020.Project);
                    ViewBag.Shop = Shops.Where(x => x.Key == objCJC020.Shop).Select(x => x.Value).FirstOrDefault();
                    ViewBag.JobDia = JobDiaList.Where(x => x.Key == objCJC020.Dia).Select(x => x.Value).FirstOrDefault();
                    ViewBag.Initiator = Manager.GetPsidandDescription(objCJC020.Initiator);
                    ViewBag.BusinessPartner = db.COM006.Where(i => i.t_bpid == objCJC020.BusinessPartner).Select(i => i.t_bpid + " - " + i.t_nama).FirstOrDefault();
                    ViewBag.POLine = db.SP_COMMON_GET_LN_PO_LINES(1, int.MaxValue, "", " ln.t_orno = '" + objCJC020.PurchaseOdrerNo + "' and ln.pono = " + objCJC020.PurchaseOdrerLineNo + "")?.Select(x => x.item).FirstOrDefault();
                    ViewBag.Action = "edit";
                }
            }
            else
            {
                var objHZWLoc = LocationList.Where(x => x.Key == "HZW").FirstOrDefault();
                if (objHZWLoc != null)
                {
                    ViewBag.Location = objHZWLoc.Value;
                    objCJC020.Location = "HZW";
                }
                objCJC020.Revision = 0;
                objCJC020.BusinessPartner = objClsLoginInfo.UserName;
                objCJC020.Shop = objClsLoginInfo.UserName;
                ViewBag.Shop = Shops.Where(x => x.Key == objCJC020.Shop).Select(x => x.Value).FirstOrDefault();
                objCJC020.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                ViewBag.Action = "new";
            }


            ViewBag.Locations = LocationList;
            ViewBag.ContractorShops = Shops;
            ViewBag.DiaList = JobDiaList;
            return View(objCJC020);
        }

        [HttpPost]
        public void removeTempValue()
        {
            TempData["FetchSeamList"] = null;
            TempData["ProjectLines"] = null;
        }

        [HttpPost]
        public ActionResult IsProjectDetailsExist(CJC020 cjc20)
        {
            bool isDia = true;
            bool isDuplicate = false;
            ResponceMsgWithCJC objResponseMsg = new ResponceMsgWithCJC();
            var obj = db.CJC020.Where(x => x.Project == cjc20.Project).FirstOrDefault();
            if (obj != null)
            {
                if (obj.Dia != cjc20.Dia)
                {
                    isDia = false;
                }
                //else if (obj.Dia == cjc20.Dia && obj.PurchaseOdrerNo == cjc20.PurchaseOdrerNo && obj.PurchaseOdrerLineNo == cjc20.PurchaseOdrerLineNo)
                //{
                //    isDuplicate = true;
                //}
            }
            objResponseMsg.Key = isDia;
            objResponseMsg.dataKey = isDuplicate;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public void RetenTempDataValue()
        {
            TempData.Keep("FetchSeamList");
            TempData.Keep("ProjectLines");
        }

        #endregion

        #region Declared global variable/array which are used multiple times
        double pi = Math.PI;
        string[] arrSeamTypeNW = { clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue() };
        string[] arrSeamTypeNC2 = { clsImplementationEnum.AutoCJCSeamType.AW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.NP.GetStringValue() };
        string[] arrSeamTypeWp = { clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.CW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.LW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.OW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.CR.GetStringValue() ,clsImplementationEnum.AutoCJCSeamType.HX.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PTCLW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PTCCW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PTCOW.GetStringValue(),
                                       clsImplementationEnum.AutoCJCSeamType.PTCBW.GetStringValue()};
        string[] arrSetUpMinThk = { clsImplementationEnum.AutoCJCSeamType.PTCLW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PTCCW.GetStringValue() };

        string[] arrSetUpNonOverlayRate = { clsImplementationEnum.AutoCJCSeamCategory.SC002.GetStringValue(), clsImplementationEnum.AutoCJCSeamCategory.SC003.GetStringValue() };
        string[] arrSetUpNozzleSizeRate = { clsImplementationEnum.AutoCJCSeamCategory.SC010.GetStringValue() };

        #endregion

        #region Seam Line Details

        public ActionResult LoadSeamLineGridDataPartial()
        {
            ViewBag.SeamCategory = Manager.GetSubCatagorywithoutBULocation("Seam Category").Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList();
            ViewBag.NozzleCategory = Manager.GetSubCatagorywithoutBULocation("Nozzle Category").Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList();

            List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();

            ViewBag.WeldingProcessList = lstWeldingProcess.AsEnumerable().Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList();

            List<CategoryData> lstCriticalSeam = Manager.GetSubCatagorywithoutBULocation("Critical Seam").ToList();
            ViewBag.CriticalSeam = lstCriticalSeam.AsEnumerable().Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList();

            return PartialView("_LoadSeamLineGridDataPartial");
        }

        public ActionResult GetWPSWeldingProcessList(string QualityProject, string SeamNo)
        {
            List<string> list = new List<string>();
            list = db.WPS025.Where(x => x.QualityProject == QualityProject && x.SeamNo == SeamNo && x.RefTokenId > 0).Select(x => x.WeldingProcess).Distinct().ToList();
            if (list.Count > 0)
            {
                List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();
                lstWeldingProcess = (from a in lstWeldingProcess where list.Contains(a.Value) select a).Distinct().ToList();
                return Json(lstWeldingProcess.AsEnumerable().Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FetchSeamCategorywiseNozzleCategory(string seamcategory, int dia)
        {
            List<CategoryData> list = new List<CategoryData>();
            list = db.CJC011.Where(x => x.SeamCategory == seamcategory && x.Dia == dia).Where(x => x.NozzleCategory != "" && x.NozzleCategory != null).Select(x => new CategoryData { Value = x.NozzleCategory, CategoryDescription = x.NozzleCategory }).Distinct().ToList();
            if (list.Count > 0)
            {
                return Json(list.AsEnumerable().Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LoadFetchSeamGridDataTable(DataTableParamModel param, int HeaderId)
        {
            try
            {
                string HeaderCJCNo = !string.IsNullOrWhiteSpace(param.CJCNo) ? param.CJCNo : "";
                string HeaderCJCStatus = !string.IsNullOrWhiteSpace(HeaderCJCNo) ? Manager.GetAutoCJCStatus(HeaderCJCNo) : "";
                string ActionType = "";
                if (HeaderId > 0)
                {
                    ActionType = ActionEdit;
                }
                else
                {
                    ActionType = ActionAddNew;
                }

                int Jobdia = Convert.ToInt32(param.CTQHeaderId);
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int? totalRecords = 0;
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                string whereCondition = " 1=1 AND  Shop='" + param.Shop + "'";
                //whereCondition += "  AND ((IsSetUpCleared=1 and(SetupCJCStatus like '%Returned%' or ISNULL(SetupCJCStatus,'') = '')) or (IsPWHTCleared=1 AND ( WeldCJCStatus like '%Returned%' or ISNULL(WeldCJCStatus,'') = '')))";
                if (param.Status.Contains("Returned"))
                {
                    whereCondition += "  AND ((IsSetUpCleared=1 and  SetupCJCStatus <> 'Draft' and ISNULL(SetupCJCNo,'') = '') or (IsPWHTCleared=1 AND WeldCJCStatus <> 'Draft' and ISNULL(WeldCJCNo,'') = ''))";
                }
                else {
                    whereCondition += "  AND ((IsSetUpCleared=1 and ISNULL(SetupCJCNo,'') = '') or (IsPWHTCleared=1 AND ISNULL(WeldCJCNo,'') = ''))";

                }
                DataTable dtSeamLines = new DataTable();
                if (TempData["FetchSeamList"] == null)
                {
                    var lstLines = db.SP_CJC_GET_APPLICABLE_SEAM_DATA(0, 0, strSortOrder, whereCondition, param.Project, param.Location).ToList();
                    dtSeamLines = Helper.ToDataTable(lstLines);
                    dtSeamLines.Columns.Add("EditLineId", typeof(Int32));
                    dtSeamLines.AsEnumerable().ToList<DataRow>().ForEach(r =>
                    {
                        r["EditLineId"] = 0;
                    });
                    dtSeamLines = CalculateAndUpdateDataTable(dtSeamLines, HeaderCJCNo, ActionType, Jobdia);
                    dtSeamLines = MergeEditLineDataWithMainDT(dtSeamLines, HeaderId);
                    TempData["FetchSeamList"] = dtSeamLines;
                }
                else
                {
                    dtSeamLines = (DataTable)TempData["FetchSeamList"];
                }

                string searchExpression = "1=1 ";
                string sortExpression = "";

                string[] columnName = { "QualityProject","SeamNo"
                                ,"SetupCJCNo"
                                ,"WeldCJCNo"
                                ,"SeamCategory"
                                ,"SeamCategoryDesc"
                               // ,"NozzleSize"
                                ,"NozzleCategory"
                                ,"NozzleCategoryDesc"
                                ,"SeamJointType"
                                ,"Convert(SeamLength, System.String)"
                                ,"Convert(Part1Position, System.String)"
                                ,"Convert(Part2Position, System.String)"
                                ,"Convert(Part3Position, System.String)"
                                ,"Convert(Part1Thickness, System.String)"
                                ,"Convert(Part2Thickness, System.String)"
                                ,"Convert(Part3Thickness, System.String)"
                                ,"Convert(MinThickness, System.String)"
                                ,"Convert(Unit, System.String)"
                                ,"WeldingProcess"
                                ,"WeldingProcessDesc"
                                //,"SetupRate"
                                //,"WeldingRate"
                                ,"CriticalSeamDesc"
                                //,"Factor"
                                //,"Amount"
                };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    searchExpression += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    searchExpression += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    sortExpression = "" + sortColumnName + " " + sortDirection + " ";
                }

                DataView dataView = dtSeamLines.AsDataView();
                if (!string.IsNullOrWhiteSpace(searchExpression))
                    dataView.RowFilter = searchExpression;
                if (!string.IsNullOrWhiteSpace(sortExpression))
                    dataView.Sort = sortExpression;

                totalRecords = dataView.ToTable().Rows.Count;

                List<DataRow> filteredList = dataView.ToTable().Select().Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();

                var res = (from c in filteredList
                           select new[] {
                                    Convert.ToString(c["ROW_NO"]),
                                    Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "LineId", Convert.ToString(c["LineId"])),
                                    Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "HeaderId", Convert.ToString(c["HeaderId"])),
                                    Convert.ToString(c["QualityProject"]),
                                    Convert.ToString(c["SeamNo"]),
                                    "",
                                    WebUtility.HtmlEncode(Convert.ToString(c["SeamCategoryDesc"]))+""+Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "SeamCategory",Convert.ToString(c["SeamCategory"])),
                                    Convert.ToString(c["NozzleSize"]),
                                    Helper.HTMLAutoComplete(Convert.ToInt32(c["ROW_NO"]), "txtNozzleCategory",(arrSeamTypeNW.Contains(Convert.ToString(c["SeamType"])) || arrSeamTypeNC2.Contains(Convert.ToString(c["SeamType"]))) ? Convert.ToString(c["NozzleCategoryDesc"]) : "","", false,"","NozzleCategory",EnableDisableNozzleCategorySelection(HeaderCJCNo,HeaderCJCStatus,Convert.ToString(c["SetupCJCNo"]),Convert.ToString(c["WeldCJCNo"]),Convert.ToString(c["SeamType"])),"","","Select","clsMandatory")+""+Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "NozzleCategory",(arrSeamTypeNW.Contains(Convert.ToString(c["SeamType"])) || arrSeamTypeNC2.Contains(Convert.ToString(c["SeamType"]))) ? Convert.ToString(c["NozzleCategoryDesc"]) : ""),
                                    Convert.ToString(c["SeamJointType"]),
                                    Convert.ToString(c["SeamLength"]),
                                    Convert.ToString(c["Part1Position"]),
                                    Convert.ToString(c["Part2Position"]),
                                    Convert.ToString(c["Part3Position"]),
                                    Convert.ToString(c["Part1Thickness"]),
                                    Convert.ToString(c["Part2Thickness"]),
                                    Convert.ToString(c["Part3Thickness"]),
                                    !string.IsNullOrWhiteSpace(Convert.ToString(c["MinThickness"])) ? Convert.ToString(c["MinThickness"]) : "",
                                    Convert.ToString(c["Unit"]),
                                    Helper.HTMLAutoComplete(Convert.ToInt32(c["ROW_NO"]),"txtWeldingProcess",arrSeamTypeWp.Contains(Convert.ToString(c["SeamType"])) ? Convert.ToString(c["WeldingProcessDesc"]) : "","", false,"","WeldingProcess",EnableDisableWeldingProcessSelection(HeaderCJCNo,HeaderCJCStatus,Convert.ToString(c["SetupCJCNo"]),Convert.ToString(c["WeldCJCNo"]),Convert.ToString(c["SeamType"])),"","","Select","clsMandatory")+""+Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "WeldingProcess",arrSeamTypeWp.Contains(Convert.ToString(c["SeamType"])) ? Convert.ToString(c["WeldingProcessDesc"]) : ""),  // Helper.GenerateLabelFor(Convert.ToInt32(c["ROW_NO"]),"lblWeldingProcess",(arrSeamTypeWp.Contains(c["SeamType"]) ? Convert.ToString(c["WeldingProcess"]) : string.Empty ),"","clsWeldingProcess")+Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "WeldingProcess",arrSeamTypeWp.Contains(c["SeamType"]) ? Convert.ToString(c["WeldingProcess"]) : ""),//
                                    Convert.ToString(c["SetupRate"]),
                                    Convert.ToString(c["WeldingRate"]),
                                    Helper.HTMLAutoComplete(Convert.ToInt32(c["ROW_NO"]), "txtCriticalSeam", Convert.ToString(c["CriticalSeamDesc"]),"", false,"","CriticalSeam",EnableDisableCriticalSeamSelection(HeaderCJCNo,HeaderCJCStatus,Convert.ToString(c["SetupCJCNo"]),Convert.ToString(c["WeldCJCNo"])),"","","Select","clsMandatory")+""+Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "CriticalSeam",Convert.ToString(c["CriticalSeam"])),
                                    Convert.ToString(c["Factor"]),
                                    Helper.GenerateLabelFor(Convert.ToInt32(c["ROW_NO"]),"Amount",Convert.ToString(c["Amount"]),"","clsAmount"),
                                    !string.IsNullOrWhiteSpace(Convert.ToString(c["SetupCJCNo"])) ? Convert.ToString(c["SetupCJCNo"]) : "",
                                    !string.IsNullOrWhiteSpace(Convert.ToString(c["WeldCJCNo"])) ? Convert.ToString(c["WeldCJCNo"]) : "",
                                    "",
                                    Convert.ToString(c["IsSetUpCleared"]),
                                    Convert.ToString(c["IsPWHTCleared"]),
                                    Convert.ToString(c["Shop"]),
                                    Convert.ToString(c["SetupCJCStatus"]),
                                    Convert.ToString(c["WeldCJCStatus"]),
                                    Convert.ToString(c["SeamType"]),
                                    Convert.ToString(c["SeamQuantity"]),
                                    Helper.HTMLActionString(Convert.ToInt32(c["ROW_NO"]),"Delete","Delete Record","fa fa-trash-o","DeleteRowDatatable(this,"+Convert.ToInt32(c["ROW_NO"])+","+Convert.ToInt32(c["EditLineId"])+");"),
                        }).ToList();

                RetenTempDataValue();

                return Json(
                            new
                            {
                                sEcho = param.sEcho,
                                iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                                iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                                aaData = res,
                                strSortOrder = strSortOrder,
                                whereCondition = whereCondition
                            },
                            "application/json",
                            Encoding.UTF8,
                            JsonRequestBehavior.AllowGet
                        );
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }

                }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool EnableDisableWeldingProcessSelection(string HeaderCJCNo, string HeaderCJCStatus, string SetUpCJCNo, string WeldCJCNo, string SeamType)
        {
            bool IsDisabled = true;
            if (string.IsNullOrWhiteSpace(HeaderCJCNo))
            {
                if (arrSeamTypeWp.Contains(SeamType))
                    IsDisabled = false;
            }
            else
            {
                if (arrSeamTypeWp.Contains(SeamType))
                {
                    if (string.IsNullOrWhiteSpace(HeaderCJCStatus) || HeaderCJCStatus.Trim().ToLower().Contains("returned") || HeaderCJCStatus.Trim().ToLower().Contains("rejected"))
                    {
                        if (string.IsNullOrWhiteSpace(SetUpCJCNo) && string.IsNullOrWhiteSpace(WeldCJCNo))
                        {
                            IsDisabled = false;
                        }
                        else
                        {
                            if (HeaderCJCNo == SetUpCJCNo || HeaderCJCNo == WeldCJCNo)
                            {
                                IsDisabled = false;
                            }
                        }
                    }
                }
            }
            return IsDisabled;
        }

        public bool EnableDisableNozzleCategorySelection(string HeaderCJCNo, string HeaderCJCStatus, string SetUpCJCNo, string WeldCJCNo, string SeamType)
        {
            bool IsDisabled = true;
            if (string.IsNullOrWhiteSpace(HeaderCJCNo))
            {
                if (arrSeamTypeNW.Contains(SeamType) || arrSeamTypeNC2.Contains(SeamType))
                    IsDisabled = false;
            }
            else
            {
                if (arrSeamTypeNW.Contains(SeamType) || arrSeamTypeNC2.Contains(SeamType))
                {
                    if (string.IsNullOrWhiteSpace(HeaderCJCStatus) || HeaderCJCStatus.Trim().ToLower().Contains("returned") || HeaderCJCStatus.Trim().ToLower().Contains("rejected"))
                    {
                        if (string.IsNullOrWhiteSpace(SetUpCJCNo) && string.IsNullOrWhiteSpace(WeldCJCNo))
                        {
                            IsDisabled = false;
                        }
                        else
                        {
                            if (HeaderCJCNo == SetUpCJCNo || HeaderCJCNo == WeldCJCNo)
                            {
                                IsDisabled = false;
                            }
                        }
                    }
                }
            }
            return IsDisabled;
        }

        public bool EnableDisableCriticalSeamSelection(string HeaderCJCNo, string HeaderCJCStatus, string SetUpCJCNo, string WeldCJCNo)
        {
            bool IsDisabled = true;
            if (string.IsNullOrWhiteSpace(HeaderCJCNo))
            {
                IsDisabled = false;
            }
            else
            {
                if (string.IsNullOrWhiteSpace(HeaderCJCStatus) || HeaderCJCStatus.Trim().ToLower().Contains("returned") || HeaderCJCStatus.Trim().ToLower().Contains("rejected"))
                {
                    if (string.IsNullOrWhiteSpace(SetUpCJCNo) && string.IsNullOrWhiteSpace(WeldCJCNo))
                    {
                        IsDisabled = false;
                    }
                    else
                    {
                        if (HeaderCJCNo == SetUpCJCNo || HeaderCJCNo == WeldCJCNo)
                        {
                            IsDisabled = false;
                        }
                    }
                }
            }
            return IsDisabled;
        }

        public DataTable MergeEditLineDataWithMainDT(DataTable dtSeamLines, int HeaderId)
        {
            int CurrentRowNo = 1;
            if (dtSeamLines.Rows.Count > 0)
                CurrentRowNo = dtSeamLines.Rows.Count + 1;

            if (HeaderId > 0)
            {
                CJC020 objCJC020 = db.CJC020.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objCJC020 != null)
                {
                    string whereCondition = " 1=1 and HeaderId=" + HeaderId;
                    var lstLines = db.SP_CJC_GET_SEAM_LINE_DETAILS(0, 0, "", whereCondition, objCJC020.Project, objCJC020.Location).ToList();
                    int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();
                    foreach (var item in lstLines)
                    {
                        DataRow drExisting = (from u in dtSeamLines.AsEnumerable()
                                              where Convert.ToString(u["QualityProject"]) == item.QualityProject && Convert.ToString(u["SeamNo"]) == item.SeamNo
                                              select u).FirstOrDefault();
                        if (drExisting != null)
                        {
                            drExisting["EditLineId"] = item.LineId;
                            dtSeamLines.AcceptChanges();
                        }
                        else
                        {
                            DataRow drNewRow = dtSeamLines.NewRow();
                            drNewRow["EditLineId"] = item.LineId;
                            drNewRow["ROW_NO"] = CurrentRowNo;

                            drNewRow["Project"] = objCJC020.Project;
                            drNewRow["HeaderId"] = HeaderId;
                            drNewRow["QualityProject"] = item.QualityProject;
                            drNewRow["SeamNo"] = item.SeamNo;
                            drNewRow["SetupCJCNo"] = item.SetupCJCNo;
                            drNewRow["WeldCJCNo"] = item.WeldCJCNo;
                            drNewRow["SeamCategory"] = item.SeamCategory;
                            drNewRow["SeamCategoryDesc"] = item.SeamCategoryDesc;

                            drNewRow["NozzleSize"] = item.NozzleSize;
                            drNewRow["NozzleCategory"] = item.NozzleCategory;
                            drNewRow["NozzleCategoryDesc"] = item.NozzleCategoryDesc;

                            drNewRow["SeamJointType"] = item.SeamJointType;
                            drNewRow["SeamLength"] = item.SeamLength;

                            drNewRow["Part1Position"] = item.Part1Position;
                            drNewRow["Part2Position"] = item.Part2Position;
                            drNewRow["Part3Position"] = item.Part3Position;

                            drNewRow["Part1Thickness"] = item.Part1Thickness;
                            drNewRow["Part2Thickness"] = item.Part2Thickness;
                            drNewRow["Part3Thickness"] = item.Part3Thickness;

                            drNewRow["MinThickness"] = item.MinThickness;
                            drNewRow["Unit"] = item.Unit;
                            drNewRow["WeldingProcess"] = item.WeldingProcess;
                            drNewRow["WeldingProcessDesc"] = item.WeldingProcessDesc;

                            drNewRow["SetupRate"] = item.SetupRate;
                            drNewRow["WeldingRate"] = item.WeldingRate;
                            drNewRow["CriticalSeam"] = item.CriticalSeam;
                            drNewRow["CriticalSeamDesc"] = item.CriticalSeamDesc;

                            drNewRow["Factor"] = item.Factor;
                            drNewRow["Amount"] = item.Amount;

                            drNewRow["Revision"] = item.Revision;
                            drNewRow["Status"] = item.Status;
                            drNewRow["IsSetUpCleared"] = item.IsSetUpCleared;
                            drNewRow["IsPWHTCleared"] = item.IsPWHTCleared;

                            drNewRow["Shop"] = item.Shop;
                            drNewRow["SetupCJCStatus"] = item.SetupCJCStatus;
                            drNewRow["WeldCJCStatus"] = item.WeldCJCStatus;
                            drNewRow["IsSeamDisabled"] = true;

                            drNewRow["SeamType"] = item.SeamType;
                            drNewRow["IsSeamFetchedBySeamType"] = item.IsSeamFetchedBySeamType;

                            dtSeamLines.Rows.Add(drNewRow);

                            dtSeamLines.AsEnumerable().ToList<DataRow>().ForEach(r =>
                            {
                                r["TotalCount"] = CurrentRowNo;
                            });

                            CurrentRowNo++;
                        }
                    }
                }
            }
            return dtSeamLines;
        }

        [HttpPost]
        public ActionResult EditSeamLineData(CJC021 cjc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                double ClaimAmount = 0;
                double? Factor = 0;
                DataTable dt = (DataTable)TempData["FetchSeamList"];
                int? rowid = cjc.LineId;
                if (dt != null)
                {
                    DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                  where Convert.ToInt32(u["ROW_NO"]) == rowid
                                  select u).FirstOrDefault();

                    if (dr != null)
                    {
                        //changes the Column Value
                        dr["SeamCategory"] = cjc.SeamCategory;
                        dr["NozzleSize"] = cjc.NozzleSize;
                        dr["NozzleCategory"] = cjc.NozzleCategory;
                        dr["WeldingProcess"] = cjc.WeldingProcess;

                        dr["SeamCategoryDesc"] = !string.IsNullOrWhiteSpace(cjc.SeamCategory) ? GetCategory("Seam Category", cjc.SeamCategory).CategoryDescription : "";
                        dr["NozzleCategoryDesc"] = !string.IsNullOrWhiteSpace(cjc.NozzleCategory) ? GetCategory("Nozzle Category", cjc.NozzleCategory).CategoryDescription : "";
                        dr["WeldingProcessDesc"] = !string.IsNullOrWhiteSpace(cjc.WeldingProcess) ? GetCategory("Welding Process", cjc.WeldingProcess).CategoryDescription : "";

                        dr["SetupRate"] = cjc.SetupRate;
                        dr["WeldingRate"] = cjc.WeldingRate;

                        dr["CriticalSeamDesc"] = !string.IsNullOrWhiteSpace(cjc.CriticalSeam) ? GetCategory("Critical Seam", cjc.CriticalSeam).CategoryDescription : "";
                        dr["CriticalSeam"] = cjc.CriticalSeam;

                        Factor = !string.IsNullOrWhiteSpace(cjc.CriticalSeam) ? FetchFactorData(cjc.CriticalSeam) : 0;
                        if (Factor > 0)
                        {
                            dr["Factor"] = Factor;
                        }
                        else
                        {
                            dr["Factor"] = 1;
                            Factor = 1;
                        }
                        dr["Unit"] = cjc.Unit;
                        ClaimAmount = claimSeamAmount(0, cjc.SeamCategory, Convert.ToInt32(Convert.ToString(dr["Seamlength"])), Convert.ToDouble(cjc.Unit), Convert.ToDouble(cjc.WeldingRate), Convert.ToDouble(Factor), Convert.ToDouble(cjc.SetupRate), Convert.ToDouble(cjc.NozzleSize));
                        if (ClaimAmount > 0)
                        {
                            dr["Amount"] = ClaimAmount;
                        }
                        else { dr["Amount"] = null; }
                    }
                    dt.AcceptChanges();
                }
                RetenTempDataValue();
                objResponseMsg.Key = true;
                objResponseMsg.ItemNumber1 = Factor.ToString();
                objResponseMsg.ItemNumber2 = ClaimAmount.ToString();
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult EditSeamLineUtilityData(CJC021 cjc)
        {
            ResponceMsgWithCJC objResponseMsg = new ResponceMsgWithCJC();
            try
            {
                double ClaimAmount = 0;
                double? Factor = 0;
                int? LineId = cjc.LineId;
                CJC021 objCJC021 = db.CJC021.Where(x => x.LineId == cjc.LineId).FirstOrDefault();
                if (objCJC021 != null)
                {
                    //changes the Column Value
                    objCJC021.SeamCategory = cjc.SeamCategory;
                    objCJC021.NozzleSize = cjc.NozzleSize;
                    objCJC021.NozzleCategory = cjc.NozzleCategory;
                    objCJC021.WeldingProcess = cjc.WeldingProcess;
                    objCJC021.SetupRate = cjc.SetupRate;
                    objCJC021.WeldingRate = cjc.WeldingRate;
                    objCJC021.CriticalSeam = cjc.CriticalSeam;

                    Factor = !string.IsNullOrWhiteSpace(cjc.CriticalSeam) ? FetchFactorData(cjc.CriticalSeam) : 0;
                    if (Factor > 0)
                    {
                        objCJC021.Factor = Factor;
                    }
                    else
                    {
                        objCJC021.Factor = null;
                        Factor = null;
                    }
                    objCJC021.Unit = cjc.Unit;
                    if (cjc.WeldingRate > 0 && Factor > 0)
                    {
                        ClaimAmount = claimSeamAmount(0, cjc.SeamCategory, Convert.ToInt32(objCJC021.SeamLength.ToString()), Convert.ToDouble(cjc.Unit), Convert.ToDouble(cjc.WeldingRate), Convert.ToDouble(Factor), Convert.ToDouble(cjc.SetupRate), Convert.ToDouble(cjc.NozzleSize));
                    }
                    if (ClaimAmount > 0)
                    {
                        objCJC021.Amount = ClaimAmount;
                    }
                    else { objCJC021.Amount = null; }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.ItemNumber1 = Factor.ToString();
                objResponseMsg.ItemNumber2 = ClaimAmount.ToString();
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult DeleteDTSeamLineData(int rowid, int editlineid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                DataTable dt = (DataTable)TempData["FetchSeamList"];
                if (dt != null)
                {
                    DataRow dr = (from u in dt.AsEnumerable()
                                  where Convert.ToInt32(u["ROW_NO"]) == rowid
                                  select u).FirstOrDefault();
                    if (dr != null)
                    {
                        dr.Delete();

                        if (editlineid > 0)
                        {
                            CJC021 objCJC021 = db.CJC021.Where(x => x.LineId == editlineid).FirstOrDefault();
                            if (objCJC021 != null)
                            {
                                db.CJC021.Remove(objCJC021);
                                db.SaveChanges();

                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete;
                        }
                    }
                    dt.AcceptChanges();
                }
                RetenTempDataValue();
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        public ActionResult LoadSeamLineGridDataTable(DataTableParamModel param, int HeaderId)
        {
            try
            {
                string HeaderCJCStatus = !string.IsNullOrWhiteSpace(param.CJCNo) ? Manager.GetAutoCJCStatus(param.CJCNo) : "";

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                bool IsEditable = param.Flag;
                string whereCondition = " 1=1 and HeaderId=" + HeaderId;

                string[] columnName = { "QualityProject","SeamNo"
                                ,"SetupCJCNo"
                                ,"WeldCJCNo"
                                ,"SeamCategory"
                                ,"SeamCategoryDesc"
                                ,"NozzleSize"
                                ,"NozzleCategory"
                                ,"NozzleCategoryDesc"
                                ,"SeamJointType"
                                ,"SeamLength"
                                ,"Part1Position"
                                ,"Part2Position"
                                ,"Part3Position"
                                ,"Part1Thickness"
                                ,"Part2Thickness"
                                ,"Part3Thickness"
                                ,"MinThickness"
                                ,"Unit"
                                ,"WeldingProcess"
                                ,"WeldingProcessDesc"
                                ,"SetupRate"
                                ,"WeldingRate"
                                ,"CriticalSeamDesc"
                                ,"Factor"
                                ,"Amount"
                };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string CJCNo = param.CJCNo;
                var lstLines = db.SP_CJC_GET_SEAM_LINE_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition, param.Project, param.Location).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                    Convert.ToString(c.ROW_NO),
                                    Helper.GenerateHidden(c.LineId, "LineId", Convert.ToString(c.LineId)),
                                    Helper.GenerateHidden(c.LineId, "HeaderId",Convert.ToString(c.HeaderId)),
                                    Convert.ToString(c.QualityProject),
                                    Convert.ToString(c.SeamNo),
                                    "",
                                    WebUtility.HtmlEncode(c.SeamCategoryDesc)+""+Helper.GenerateHidden(c.LineId, "SeamCategory",c.SeamCategory),
                                    c.NozzleSize.HasValue ? Convert.ToString(c.NozzleSize) : "",
                                    IsEditable ?  Helper.HTMLAutoComplete(c.LineId, "txtNozzleCategory",(arrSeamTypeNW.Contains(c.SeamType) || arrSeamTypeNC2.Contains(c.SeamType)) ? c.NozzleCategoryDesc : "","", false,"","NozzleCategory",EnableDisableNozzleCategorySelection(CJCNo,HeaderCJCStatus,c.SetupCJCNo,c.WeldCJCNo,c.SeamType),"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "NozzleCategory",(arrSeamTypeNW.Contains(c.SeamType) || arrSeamTypeNC2.Contains(c.SeamType)) ? Convert.ToString(c.NozzleCategory) : ""):Convert.ToString(c.NozzleCategoryDesc),
                                    c.SeamJointType,
                                    Convert.ToString(c.SeamLength),
                                    c.Part1Position,
                                    c.Part2Position,
                                    c.Part3Position,
                                    c.Part1Thickness,
                                    c.Part2Thickness,
                                    c.Part3Thickness,
                                    !string.IsNullOrWhiteSpace(Convert.ToString(c.MinThickness)) ? Convert.ToString(c.MinThickness) : "",
                                    Convert.ToString(c.Unit),
                                    IsEditable ? Helper.HTMLAutoComplete(c.LineId, "txtWeldingProcess",arrSeamTypeWp.Contains(c.SeamType) ? c.WeldingProcessDesc : "","", false,"","WeldingProcess",EnableDisableWeldingProcessSelection(CJCNo,HeaderCJCStatus,c.SetupCJCNo,c.WeldCJCNo,c.SeamType),"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "WeldingProcess",arrSeamTypeWp.Contains(c.SeamType) ? c.WeldingProcess : "") : c.WeldingProcessDesc,//(arrSeamTypeWp.Contains(c.SeamType) ? Convert.ToString(c.WeldingProcess): string.Empty) + Helper.GenerateHidden(c.LineId, "WeldingProcess",arrSeamTypeWp.Contains(c.SeamType) ? c.WeldingProcess : ""),//
                                    Convert.ToString(c.SetupRate),
                                    Convert.ToString(c.WeldingRate),
                                    IsEditable ? Helper.HTMLAutoComplete(c.LineId, "txtCriticalSeam",c.CriticalSeamDesc,"", false,"","CriticalSeam",EnableDisableCriticalSeamSelection(CJCNo,HeaderCJCStatus,c.SetupCJCNo,c.WeldCJCNo),"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "CriticalSeam",c.CriticalSeam) : c.CriticalSeamDesc,
                                    Convert.ToString(c.Factor),
                                    IsEditable ? Helper.GenerateLabelFor(c.LineId,"Amount", Convert.ToString(c.Amount),"","clsAmount") : Convert.ToString(c.Amount),
                                    !string.IsNullOrWhiteSpace(c.SetupCJCNo) ? c.SetupCJCNo : "",
                                    !string.IsNullOrWhiteSpace(c.WeldCJCNo) ? c.WeldCJCNo : "",
                                    "",
                                    Convert.ToString(c.IsSetUpCleared),
                                    Convert.ToString(c.IsPWHTCleared),
                                    "",
                                    c.SetupCJCStatus,
                                    c.WeldCJCStatus,
                                    c.SeamType,
                                    Convert.ToString(c.SeamQuantity),
                                    Helper.HTMLActionString(c.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteRowDatatable(this,"+c.LineId+","+c.LineId+");","", !IsEditable ),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Project Details

        public ActionResult LoadProjectLineGridDataPartial()
        {
            ViewBag.ProjectActivityList = (from cjc012 in db.CJC012
                                           join glb002 in db.GLB002 on cjc012.ProjectActivity equals glb002.Code
                                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                           where glb001.Category.Equals("Project Activity", StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true
                                           select glb002).Select(i => new { Key = i.Code, Value = i.Description }).Distinct().ToList();

            return PartialView("_LoadProjectLineGridDataPartial");
        }

        public ActionResult LoadFetchProjectLineGridDataTable(DataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1 AND HeaderId=" + (!string.IsNullOrWhiteSpace(param.Headerid) ? Convert.ToInt32(param.Headerid) : 0);
                DataTable dtProjectLines = new DataTable();
                if (TempData["ProjectLines"] == null)
                {
                    var lstLines = db.SP_CJC_GET_PROJECT_LINE_DETAILS(0, 0, strSortOrder, whereCondition).ToList();
                    dtProjectLines = Helper.ToDataTable(lstLines);
                    TempData["ProjectLines"] = dtProjectLines;
                }
                else
                {
                    dtProjectLines = (DataTable)TempData["ProjectLines"];
                }

                string searchExpression = "1=1 ";
                string sortExpression = "";

                string[] columnName = { "ActivityDesc", "Convert(Quantity,System.String)", "UOM", "Convert(Rate, System.String)", "Convert(Amount, System.String)", "CJCNo" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    searchExpression += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    searchExpression += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    sortExpression = sortColumnName + " " + sortDirection + " ";
                }
                string Contingency_amount = clsImplementationEnum.AutoCJCProjectActivity.Contingency_amount.GetStringValue();
                DataView dataView = dtProjectLines.AsDataView();
                if (!string.IsNullOrWhiteSpace(searchExpression))
                    dataView.RowFilter = searchExpression;
                if (!string.IsNullOrWhiteSpace(sortExpression))
                    dataView.Sort = sortExpression;

                int? totalRecords = dataView.ToTable().Rows.Count;

                List<DataRow> filteredList = dataView.ToTable().Select().Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();

                var res = (from c in filteredList
                           select new[] {
                                    Convert.ToString(c["ROW_NO"]),
                                    Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "Id", ""),
                                    Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "HeaderId", Convert.ToString(HeaderId)),
                                    Helper.HTMLAutoComplete(Convert.ToInt32(c["ROW_NO"]), "txtActivity",Convert.ToString(c["ActivityDesc"]),"", false,"width:420px;","Activity",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "Activity",Convert.ToString(c["Activity"].ToString())),
                                    Helper.GenerateNumericTextbox(Convert.ToInt32(c["ROW_NO"]), "txtQuantity",Convert.ToString(c["Quantity"]), "",false, "", ""),
                                    Helper.GenerateLabelFor(Convert.ToInt32(c["ROW_NO"]),"UOM",Convert.ToString(c["UOM"]),""," clsProjUOM"),
                                    Helper.GenerateLabelFor(Convert.ToInt32(c["ROW_NO"]),"Rate",Convert.ToString(c["Rate"]),""," clsProjRate"),
                                    Helper.GenerateLabelFor(Convert.ToInt32(c["ROW_NO"]),"ProjAmount",Convert.ToString(c["Amount"]),""," clsProjAmount"),
                                    Helper.GenerateTextArea(Convert.ToInt32(c["ROW_NO"]),"Remarks",Convert.ToString(c["Remarks"]),"EditRemarkRow("+Convert.ToInt32(c["ROW_NO"])+")",false,"height: 33px !important;","200"," clsProjRemarks"),
                                    Helper.GenerateLabelFor(Convert.ToInt32(c["ROW_NO"]),"CJCNo",Convert.ToString(c["CJCNo"]),""," clsProjCJCNo"),
                                    Helper.HTMLActionString(Convert.ToInt32(c["ROW_NO"]),"Delete","Delete Record","fa fa-trash-o","DeleteActivityRowDatatable(this,"+Convert.ToInt32(c["ROW_NO"]) +");"),
                          }).ToList();

                TempData["ProjectLines"] = dtProjectLines;
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LoadProjectLineGridDataTable(DataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1 and HeaderId=" + (!string.IsNullOrWhiteSpace(param.Headerid) ? Convert.ToInt32(param.Headerid) : 0);
                string[] columnName = { "ActivityDesc", "Quantity", "UOM", "Rate", "Amount", "CJCNo" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }
                bool IsEditable = param.Flag;
                var lstLines = db.SP_CJC_GET_PROJECT_LINE_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();
                string Contingency_amount = clsImplementationEnum.AutoCJCProjectActivity.Contingency_amount.GetStringValue();
                var res = (from c in lstLines
                           select new[] {
                                    Convert.ToString(c.ROW_NO),
                                    Helper.GenerateHidden(c.LineId, "Id", Convert.ToString(c.LineId)),
                                    Helper.GenerateHidden(c.LineId, "HeaderId", Convert.ToString(c.HeaderId)),
                                    IsEditable ? Helper.HTMLAutoComplete(c.LineId, "txtActivity",c.ActivityDesc,"", false,"width:420px;","Activity",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "Activity", c.Activity) : c.ActivityDesc,
                                    IsEditable ? Helper.GenerateNumericTextbox(c.LineId, "txtQuantity",Convert.ToString(c.Quantity), "",false, "", ""):Convert.ToString(c.Quantity),
                                    Helper.GenerateLabelFor(c.LineId,"UOM",Convert.ToString(c.UOM),""," clsProjUOM"),
                                    Helper.GenerateLabelFor(c.LineId,"Rate",Convert.ToString(c.Rate),""," clsProjRate"),
                                    Helper.GenerateLabelFor(c.LineId,"ProjAmount",Convert.ToString(c.Amount),""," clsProjAmount"),
                                    IsEditable ? Helper.GenerateTextArea(c.LineId,"Remarks",Convert.ToString(c.Remarks),"EditRemarkRow("+Convert.ToInt32(c.LineId)+")",false,"height: 33px !important;","200"," clsProjRemarks") : Convert.ToString(c.Remarks),
                                    Helper.GenerateLabelFor(c.LineId,"CJCNo",Convert.ToString(c.CJCNo),""," clsProjCJCNo"),
                                    Helper.HTMLActionString(c.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteActivityRowDatatable(this,"+c.LineId +");","",!IsEditable),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetProjectDetailInlineEditableRow(int id, int HeaderId = 0, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "Id", Convert.ToString(id)),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                                    Helper.HTMLAutoComplete(newRecordId, "txtActivity","","", false,"","Activity",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(newRecordId, "Activity",Convert.ToString("")),
                                    Helper.GenerateNumericTextbox(newRecordId, "txtQuantity","1", "", false, "", ""),
                                    Helper.GenerateLabelFor(newRecordId,"UOM","",""," clsProjUOM"),
                                    Helper.GenerateLabelFor(newRecordId,"Rate","",""," clsProjRate"),
                                    Helper.GenerateLabelFor(newRecordId,"ProjAmount","",""," clsProjAmount"),
                                    Helper.GenerateTextArea(newRecordId,"Remarks","","",false,"height: 33px !important;","200",""," clsProjRemarks"),
                                    Helper.GenerateLabelFor(newRecordId,"CJCNo","",""," clsProjCJCNo"),
                                    Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveActivityRecord(0);"),
                    };
                    data.Add(newRecord);
                }

                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetProjectActivityData(string ProjectActivity, int Jobdia, int LineId, string Project,int headerid)
        {
            double? Rate = 0;
            string UOM = string.Empty;
            CJC012 obj = new CJC012();
            bool isActivityexists = (from a in db.CJC020
                                     join b in db.CJC022 on a.HeaderId equals b.HeaderId
                                     where a.Project == Project && b.LineId != LineId && b.Activity == ProjectActivity
                                     select b.LineId).Any();
            bool isActivityAlreadyexists = (from a in db.CJC020
                                     join b in db.CJC022 on a.HeaderId equals b.HeaderId
                                     where a.Project == Project && b.LineId != LineId && b.HeaderId == headerid && b.Activity == ProjectActivity
                                     select b.LineId).Any();
            if (!isActivityexists)
            {
                obj = db.CJC012.Where(x => x.ProjectActivity == ProjectActivity && x.Dia == Jobdia).FirstOrDefault();
                if (obj != null)
                {
                    UOM = obj.UOM;
                    Rate = obj.Rate;
                }
            }

            var objData = new
            {
                isActivityAlreadyexists = isActivityAlreadyexists,
                isActivityexists = isActivityexists,
                UOM = UOM,
                Rate = Rate
            };
            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveProjectLineData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (fc != null)
                {
                    int HeaderId = Convert.ToInt32(fc["HeaderId"]);
                    int LineId = Convert.ToInt32(fc["LineId"]);

                    string Activity = fc["Activity" + LineId];
                    string UOM = fc["txtUOM" + LineId];
                    string CJCNo = fc["txtCJCNo" + LineId];
                    string Remarks = fc["Remarks" + LineId];

                    double? Quantity = null;
                    if (fc["txtQuantity" + LineId] != null && fc["txtQuantity" + LineId] != "")
                        Quantity = Convert.ToDouble(fc["txtQuantity" + LineId]);

                    double? Rate = null;
                    if (fc["txtRate" + LineId] != null && fc["txtRate" + LineId] != "")
                        Rate = Convert.ToDouble(fc["txtRate" + LineId]);

                    double? Amount = null;
                    if (fc["txtAmount" + LineId] != null && fc["txtAmount" + LineId] != "")
                        Amount = Convert.ToDouble(fc["txtAmount" + LineId]);


                    CJC022 objCJC022 = null;
                    if (LineId > 0)
                    {
                        objCJC022 = db.CJC022.Where(x => x.LineId == LineId).FirstOrDefault();
                        if (objCJC022 != null)
                        {
                            if (!db.CJC022.Any(x => x.HeaderId == HeaderId && x.Activity == Activity && x.LineId != LineId))
                            {
                                objCJC022.Activity = Activity;
                                objCJC022.Quantity = Quantity;
                                objCJC022.UOM = UOM;
                                objCJC022.Rate = Rate;
                                objCJC022.Amount = Amount;
                                objCJC022.Remarks = Remarks;
                                objCJC022.EditedBy = objClsLoginInfo.UserName;
                                objCJC022.EditedOn = DateTime.Now;

                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
                        }
                    }
                    else
                    {
                        if (!db.CJC022.Any(x => x.HeaderId == HeaderId && x.Activity == Activity))
                        {
                            objCJC022 = new CJC022();

                            objCJC022.HeaderId = HeaderId;
                            objCJC022.Activity = Activity;
                            objCJC022.Quantity = Quantity;
                            objCJC022.UOM = UOM;
                            objCJC022.Rate = Rate;
                            objCJC022.Amount = Amount;
                            objCJC022.Remarks = Remarks;

                            objCJC022.CreatedBy = objClsLoginInfo.UserName;
                            objCJC022.CreatedOn = DateTime.Now;

                            db.CJC022.Add(objCJC022);

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteProjectLineData(int rowid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                CJC022 objCJC022 = db.CJC022.Where(x => x.LineId == rowid).FirstOrDefault();
                if (objCJC022 != null)
                {
                    db.CJC022.Remove(objCJC022);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveActivityLineData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                DataTable dt = TempData["ProjectLines"] as DataTable;
                int? rowid = Convert.ToInt32(fc["MainId"]);
                var strdtSort = " ROW_NO DESC";
                var strWhere = " Activity ='" + fc["Activity" + rowid] + "'";
                DataRow drsr = dt.Select("", strdtSort).FirstOrDefault();
                DataRow dr = dt.NewRow();
                if (!dt.Select(strWhere).Any())
                {
                    // insert in the desired place
                    dr["ROW_NO"] = dt.Rows.Count + 1;
                    dr["TotalCount"] = dt.Rows.Count + 1;
                    dr["Activity"] = fc["Activity" + rowid];
                    dr["ActivityDesc"] = !string.IsNullOrWhiteSpace(fc["Activity" + rowid]) ? GetCategory("Project Activity", fc["Activity" + rowid]).CategoryDescription : "";
                    dr["Quantity"] = fc["txtQuantity" + rowid];
                    dr["UOM"] = fc["UOM" + rowid];
                    dr["Remarks"] = fc["Remarks" + rowid];
                    dr["Rate"] = !string.IsNullOrWhiteSpace(fc["Rate" + rowid]) ? Convert.ToDouble(fc["Rate" + rowid]) : 0;
                    dr["Amount"] = !string.IsNullOrWhiteSpace(fc["Amount" + rowid]) ? Convert.ToDouble(fc["Amount" + rowid]) : 0;
                    dt.Rows.InsertAt(dr, dt.Rows.Count + 1);
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                }
                objResponseMsg.Key = true;
                TempData["ProjectLines"] = dt;
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult SaveActivityUtilityData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int rowid = 0;
                int HeaderId = Convert.ToInt32(fc["HeaderId"]);
                string Activity = fc["Activity" + rowid];
                double? Quantity = null;
                if (fc["txtQuantity" + rowid] != null)
                    Quantity = Convert.ToDouble(fc["txtQuantity" + rowid]);

                string UOM = fc["UOM" + rowid];

                double? Rate = null;
                if (fc["Rate" + rowid] != null)
                    Rate = Convert.ToDouble(fc["Rate" + rowid]);

                double? Amount = null;
                if (fc["Amount" + rowid] != null)
                    Amount = Convert.ToDouble(fc["Amount" + rowid]);
                string Remarks = fc["Remarks" + rowid];
                if (!db.CJC022.Any(x => x.HeaderId == HeaderId && x.Activity == Activity))
                {
                    CJC022 objCJC022 = new CJC022();
                    objCJC022.HeaderId = HeaderId;
                    objCJC022.Activity = Activity;
                    objCJC022.Quantity = Quantity;
                    objCJC022.UOM = UOM;
                    objCJC022.Rate = Rate;
                    objCJC022.Amount = Amount;
                    objCJC022.Remarks = Remarks;
                    objCJC022.CreatedBy = objClsLoginInfo.UserName;
                    objCJC022.CreatedOn = DateTime.Now;
                    db.CJC022.Add(objCJC022);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult EditActivityLineData(CJC022 cjc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                DataTable dt = (DataTable)TempData["ProjectLines"];
                int? rowid = cjc.LineId;
                if (dt != null)
                {
                    DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                  where Convert.ToInt32(u["ROW_NO"]) == rowid
                                  select u).FirstOrDefault();

                    if (dr != null)
                    {
                        var recExistCount = (from u in dt.AsEnumerable()
                                             where Convert.ToInt32(u["ROW_NO"]) != rowid && Convert.ToString(u["Activity"]) == cjc.Activity
                                             select u).Count();

                        if (recExistCount <= 0)
                        {
                            dr["Activity"] = cjc.Activity;
                            dr["ActivityDesc"] = !string.IsNullOrWhiteSpace(cjc.Activity) ? GetCategory("Project Activity", cjc.Activity).CategoryDescription : "";
                            dr["Quantity"] = cjc.Quantity;
                            dr["UOM"] = cjc.UOM;
                            dr["Rate"] = cjc.Rate;
                            dr["Amount"] = cjc.Amount;
                            dr["Remarks"] = cjc.Remarks;
                        }
                        //else
                        //{
                        //    objResponseMsg.Key = false;
                        //    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                        //}
                    }
                    objResponseMsg.Key = true;
                    dt.AcceptChanges();
                    TempData["ProjectLines"] = dt;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult EditActivityUtilityData(CJC022 cjc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                CJC022 objCJC022 = db.CJC022.Where(x => x.LineId == cjc.LineId).FirstOrDefault();
                if (objCJC022 != null)
                {
                    if (!db.CJC022.Any(x => x.HeaderId == objCJC022.HeaderId && x.LineId != cjc.LineId && x.Activity == cjc.Activity))
                    {
                        objCJC022.Activity = cjc.Activity;
                        objCJC022.Quantity = cjc.Quantity;
                        objCJC022.UOM = cjc.UOM;
                        objCJC022.Rate = cjc.Rate;
                        objCJC022.Amount = cjc.Amount;
                        objCJC022.Remarks = cjc.Remarks;
                        objCJC022.EditedBy = objClsLoginInfo.UserName;
                        objCJC022.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult EditActivityRemarksLineData(int lineid, string Remarks)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                DataTable dt = (DataTable)TempData["ProjectLines"];
                int? rowid = lineid;
                if (dt != null)
                {
                    DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                  where Convert.ToInt32(u["ROW_NO"]) == rowid
                                  select u).FirstOrDefault();

                    if (dr != null)
                    {
                        dr["Remarks"] = Remarks;
                    }
                    objResponseMsg.Key = true;
                    dt.AcceptChanges();
                    TempData["ProjectLines"] = dt;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult EditActivityRemarksUtilityData(int lineid, string Remarks)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                CJC022 objCJC022 = db.CJC022.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objCJC022 != null)
                {
                    objCJC022.Remarks = Remarks;
                    objCJC022.EditedBy = objClsLoginInfo.UserName;
                    objCJC022.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public ActionResult DeleteDTActivityLineData(int rowid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                DataTable dt = (DataTable)TempData["ProjectLines"];
                if (dt != null)
                {
                    DataRow dr = (from u in dt.AsEnumerable()
                                  where Convert.ToInt32(u["ROW_NO"]) == rowid
                                  select u).FirstOrDefault();
                    if (dr != null)
                    {
                        dr.Delete();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete;
                    }
                    dt.AcceptChanges();
                    TempData["ProjectLines"] = dt;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Utility

        public CategoryData GetCategory(string categoryCode, string Code)
        {
            CategoryData objGLB002 = new CategoryData();

            objGLB002 = (from glb002 in db.GLB002
                         join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                         where glb001.Category.Equals(categoryCode, StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(Code) && glb002.IsActive == true
                         select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Description }).FirstOrDefault();
            if (objGLB002 == null)
            {
                objGLB002 = new CategoryData();
                objGLB002.Code = Code;
                objGLB002.CategoryDescription = Code;
            }
            return objGLB002;
        }

        [HttpPost]
        public JsonResult GetInitiatorData(string term, string location)
        {
            string whereCondition = " Location='" + location + "' ";

            if (!string.IsNullOrWhiteSpace(term))
            {
                whereCondition += " and (PsNo like '%" + term + "%' or UPPER(InitiaorName) like '%" + term.ToUpper() + "%')";
            }

            var lstLNInitiator = db.SP_CJC_GET_LN_INITIATOR(1, 10, "", whereCondition).ToList();
            var lstlstInitiator = (from a in lstLNInitiator
                                   select new { Value = a.PsNo, Text = a.InitiaorName }).Distinct().ToList();
            return Json(lstlstInitiator, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetProjectResult(string term)
        {
            List<Projects> lstProjects = new List<Projects>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProjects = (from q1 in db.QMS001
                               join c1 in db.COM001 on q1.Project equals c1.t_cprj
                               where (q1.Project.Contains(term) || c1.t_cprj.Contains(term)) && q1.IsLTFPS != true
                               select new Projects
                               {
                                   Value = q1.Project,
                                   projectCode = q1.Project,
                                   projectDescription = q1.Project + " - " + c1.t_dsca,
                                   Text = q1.Project + " - " + c1.t_dsca
                               }
                              ).Distinct().ToList();
            }
            else
            {
                lstProjects = (from q1 in db.QMS001
                               join c1 in db.COM001 on q1.Project equals c1.t_cprj
                               where q1.Project.Contains(c1.t_cprj) && q1.IsLTFPS != true
                               select new Projects
                               {
                                   Value = q1.Project,
                                   projectCode = q1.Project,
                                   projectDescription = q1.Project + " - " + c1.t_dsca,
                                   Text = q1.Project + " - " + c1.t_dsca
                               }
                              ).Distinct().Take(10).ToList();
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDiaByProjectResult(string project, int HeaderId)
        {
            string dia = string.Empty;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string draft = clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue();
            if (!string.IsNullOrWhiteSpace(project))
            {
                var diaDescription = (from c16 in db.CJC016
                                      join c3 in db.CJC003 on c16.ProductType equals c3.Id
                                      where c16.Project == project
                                      orderby c16.EditedOn descending
                                      select new { id = c3.Id, desc = c3.Description }).FirstOrDefault();
                var objCJC020 = db.CJC020.Where(x => x.Project == project && x.HeaderId != HeaderId && x.Status == draft).FirstOrDefault();
                if (objCJC020 != null)
                {
                    objResponseMsg.HeaderId = objCJC020.HeaderId;
                }
                objResponseMsg.Value = diaDescription.id + "#" + diaDescription.desc;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPurchaseOdrerData(string term, string location)
        {
            string whereCondition = " AutoCJCFlag=1";

            if (!string.IsNullOrWhiteSpace(term))
            {
                whereCondition += " and (UPPER(PurchaseOdrerNo) like '%" + term.ToUpper() + "%')";
            }

            var lstPOResult = db.SP_CJC_GET_PO_FOR_AUTOCJC(1, int.MaxValue, "", whereCondition).ToList();
            var lstOrder = db.SP_COMMON_GET_LN_APPROVED_PO(1, int.MaxValue, "", " BP ='" + objClsLoginInfo.UserName + "' and t_loca='" + location + "'").Select(x => x.PONo).ToList();

            var lstPONo = (from a in lstPOResult
                           where lstOrder.Contains(a.PurchaseOdrerNo)
                           select new { Value = a.PurchaseOdrerNo, Text = a.PurchaseOdrerNo }).Take(10).Distinct().ToList();

            return Json(lstPONo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPurchaseOdrerLineData(string term, string pono, string location)
        {
            string whereCondition = " ln.t_orno='" + pono + "' and ln.t_loca=" + location;

            if (!string.IsNullOrWhiteSpace(term))
            {
                whereCondition += " and (ln.pono like '%" + term + "%' or UPPER(ln.t_item) like '%" + term.ToUpper() + "%')";
            }

            var lstPOResult = db.SP_COMMON_GET_LN_PO_LINES(1, 10, "", whereCondition).ToList();
            var lstPOLines = (from a in lstPOResult
                              select new { Value = a.POLineNo, Text = a.item }).Distinct().ToList();
            return Json(lstPOLines, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityProject(string term, string Project)
        {
            List<ddlValue> lstQProject = null;
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstQProject = db.QMS012_Log
                                     .Where(x => x.Project == Project && x.QualityProject.Contains(term))
                                     .Select(x => new ddlValue { Value = x.QualityProject, Text = x.QualityProject }).Distinct().Take(10).ToList();
            }
            else
            {
                lstQProject = db.QMS012_Log
                             .Where(x => x.Project == Project)
                             .Select(x => new ddlValue { Value = x.QualityProject, Text = x.QualityProject }).Distinct().Take(10).ToList();
            }
            return Json(lstQProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSeamFromQualityProject(string term, string qProject)
        {
            string approved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
            List<ddlValue> lstSeam = null;
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstSeam = db.QMS012_Log
                                .Where(x => x.QualityProject == qProject && x.SeamNo.Contains(term) && x.Status == approved)
                                .OrderByDescending(x => x.RefId)
                                .Select(x => new ddlValue { Value = x.SeamNo, Text = x.SeamNo + "-" + x.SeamDescription }).Distinct().Take(10).ToList();
            }
            else
            {
                lstSeam = db.QMS012_Log
                         .Where(x => x.QualityProject == qProject && x.Status == approved)
                         .OrderByDescending(x => x.RefId)
                         .Select(x => new ddlValue { Value = x.SeamNo, Text = x.SeamNo + "-" + x.SeamDescription }).Distinct().Take(10).ToList();
            }
            return Json(lstSeam, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FetchSeamListData(string seamno, string qProject)
        {
            CJC021 objModelCJC021 = SeamData(qProject, seamno);

            return Json(objModelCJC021, JsonRequestBehavior.AllowGet);
        }

        public CJC021 SeamData(string qproject, string seamno)
        {
            CJC021 objFetchCJC021 = new CJC021();
            string approved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
            var objSeamData = (from q12 in db.QMS012_Log
                               join q11 in db.QMS011_Log on q12.HeaderId equals q11.HeaderId
                               where q11.QualityProject == qproject.Trim() && q12.SeamNo == seamno && q12.Status == approved
                               orderby q11.Id descending
                               select q12).FirstOrDefault();
            if (objSeamData != null)
            {
                objFetchCJC021.MinThickness = 0;
                //objFetchCJC021.NozzleSize = (objSeamData.SeamLengthArea / Math.PI) / 25.4;
                objFetchCJC021.SeamJointType = objSeamData.WeldType;
                objFetchCJC021.SeamLength = objSeamData.SeamLengthArea;

                if (!string.IsNullOrWhiteSpace(objSeamData.Position))
                {
                    var lstPosition = objSeamData.Position.Split(',');
                    objFetchCJC021.Part1Position = (lstPosition.Count() > 0) ? objSeamData.Position.Split(',')[0] : "";
                    objFetchCJC021.Part2Position = (lstPosition.Count() > 1) ? objSeamData.Position.Split(',')[1] : "";
                    objFetchCJC021.Part3Position = (lstPosition.Count() > 2) ? objSeamData.Position.Split(',')[2] : "";
                }
                if (!string.IsNullOrWhiteSpace(objSeamData.Thickness))
                {
                    var lstThickness = objSeamData.Thickness.Split(',');
                    List<double> minthickness = new List<double>();
                    objFetchCJC021.Part1Thickness = (lstThickness.Count() > 0) ? objSeamData.Thickness.Split(',')[0] : "";
                    objFetchCJC021.Part2Thickness = (lstThickness.Count() > 1) ? objSeamData.Thickness.Split(',')[1] : "";
                    objFetchCJC021.Part3Thickness = (lstThickness.Count() > 2) ? objSeamData.Thickness.Split(',')[2] : "";
                    if (lstThickness.Count() > 0)
                    {
                        if (lstThickness.Count() > 0)
                            minthickness.Add(Convert.ToDouble(objFetchCJC021.Part1Thickness));
                        if (lstThickness.Count() > 1)
                            minthickness.Add(Convert.ToDouble(objFetchCJC021.Part2Thickness));
                        if (lstThickness.Count() > 2)
                            minthickness.Add(Convert.ToDouble(objFetchCJC021.Part3Thickness));
                    }

                    objFetchCJC021.MinThickness = minthickness.Min(x => x);
                }
            }

            return objFetchCJC021;
        }

        public double? FetchFactorData(string CriticalSeam)
        {
            double? Factor = 1;
            if (!string.IsNullOrWhiteSpace(CriticalSeam))
            {
                var obj = db.CJC014.Where(i => i.CriticalSeam == CriticalSeam).FirstOrDefault();
                Factor = obj?.Factor;
            }
            return Factor;
        }
        #endregion

        #region Seam Utility Grid
        [HttpPost]
        public JsonResult GetSeamDetailInlineEditableRow(int id, int HeaderId = 0, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                          clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                          Helper.GenerateHidden(newRecordId, "LineId", Convert.ToString(id)),
                          Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                          Helper.HTMLAutoComplete(newRecordId, "txtQualityProject","","", false,"","QualityProject",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(newRecordId, "QualityProject",Convert.ToString("")),
                          Helper.HTMLAutoComplete(newRecordId, "txtSeamNo","","", false,"","SeamNo",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(newRecordId, "SeamNo",Convert.ToString("")),
                          "",
                          Helper.HTMLAutoComplete(newRecordId, "txtSeamCategory","","", false,"","SeamCategory",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(newRecordId, "SeamCategory",Convert.ToString("")),
                          Helper.GenerateHTMLTextbox(newRecordId, "NozzleSize",  "", "",true, "", false,"","",""),
                          Helper.HTMLAutoComplete(newRecordId, "txtNozzleCategory","","", false,"","NozzleCategory",false)+""+Helper.GenerateHidden(newRecordId, "NozzleCategory",Convert.ToString("")),
                          Helper.GenerateHTMLTextbox(newRecordId, "SeamJointType",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "SeamLength",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part1Position",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part2Position",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part3Position",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part1Thickness",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part2Thickness",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part3Thickness",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "MinThickness",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Unit",  "", "",true, "", false,"","",""),
                          Helper.HTMLAutoComplete(newRecordId, "txtWeldingProcess","","", false,"","WeldingProcess",false)+""+Helper.GenerateHidden(newRecordId, "WeldingProcess",Convert.ToString("")),
                          Helper.GenerateHTMLTextbox(newRecordId, "SetupRate",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "WeldingRate",  "", "",true, "", false,"","",""),
                          Helper.HTMLAutoComplete(newRecordId, "txtCriticalSeam","","", false,"","CriticalSeam",false)+""+Helper.GenerateHidden(newRecordId, "CriticalSeam",Convert.ToString("")),
                         Helper.GenerateHTMLTextbox(newRecordId, "Factor",  "1", "",true, "", false,"","",""),
                          Helper.GenerateTextbox(newRecordId, "Amount",  "", "",false, "","","clsMandatory", false,"",""),
                          "",
                          "",
                          "",
                          "",
                          "",
                          "",
                          Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveRecord(0);"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstLines = db.SP_CJC_GET_SEAM_LINE_DETAILS(1, 0, "", "LineId = " + id, "", "").Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(c.LineId, "LineId", Convert.ToString(c.LineId)),
                                    Helper.GenerateHidden(c.LineId, "HeaderId", Convert.ToString(c.HeaderId)),
                                    isReadOnly ? c.QualityProject.ToString():Helper.HTMLAutoComplete(c.LineId, "txtQualityProject",c.QualityProject,"", true,"","QualityProject",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "QualityProject",Convert.ToString(c.QualityProject)),
                                    isReadOnly ? c.SeamNo.ToString():Helper.HTMLAutoComplete(c.LineId, "txtSeamNo",c.SeamNo,"", true,"","SeamNo",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "SeamNo",Convert.ToString(c.SeamNo)),
                                    isReadOnly ? c.Status.ToString():"",
                                    isReadOnly ? c.SeamCategory.ToString():Helper.HTMLAutoComplete(c.LineId, "txtSeamCategory",WebUtility.HtmlEncode(c.SeamCategoryDesc),"", false,"","SeamCategory",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "SeamCategory",Convert.ToString(c.SeamCategory)),
                                    isReadOnly ? c.NozzleSize.ToString():Helper.GenerateTextbox(c.LineId, "NozzleSize", c.NozzleSize.ToString(), "",true, "","","clsMandatory", false,""),
                                    isReadOnly ? c.NozzleCategory.ToString():Helper.HTMLAutoComplete(c.LineId, "txtNozzleCategory",c.NozzleCategoryDesc,"", false,"","NozzleCategory",false)+""+Helper.GenerateHidden(c.LineId, "NozzleCategory",Convert.ToString(c.NozzleCategory)),
                                    isReadOnly ? c.SeamJointType.ToString():Helper.GenerateTextbox(c.LineId, "SeamJointType",  c.SeamJointType, "",true, "","","", false,"",""),
                                    isReadOnly ? c.SeamLength.ToString():Helper.GenerateTextbox(c.LineId, "SeamLength",  c.SeamLength.ToString(), "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part1Position.ToString():Helper.GenerateTextbox(c.LineId, "Part1Position",  c.Part1Position, "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part2Position.ToString():Helper.GenerateTextbox(c.LineId, "Part2Position",  c.Part2Position , "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part3Position.ToString():Helper.GenerateTextbox(c.LineId, "Part3Position",  c.Part3Position , "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part1Thickness.ToString():Helper.GenerateTextbox(c.LineId, "Part1Thickness",  c.Part1Thickness, "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part2Thickness.ToString():Helper.GenerateTextbox(c.LineId, "Part2Thickness",  c.Part2Thickness, "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part3Thickness.ToString():Helper.GenerateTextbox(c.LineId, "Part3Thickness",  c.Part3Thickness, "",true, "","","", false,"",""),
                                    isReadOnly ? c.MinThickness.ToString():Helper.GenerateTextbox(c.LineId, "MinThickness", Convert.ToString(c.MinThickness), "",true,"","", "", false,"",""),
                                    isReadOnly ? c.Unit.ToString():Helper.GenerateTextbox(c.LineId, "Unit",  c.Unit.ToString(), "",true, "","","", false,"",""),
                                     isReadOnly ? c.WeldingProcess.ToString():Helper.HTMLAutoComplete(c.LineId, "txtWeldingProcess",c.WeldingProcessDesc.ToString(),"", false,"","WeldingProcess",false)+""+Helper.GenerateHidden(c.LineId, "WeldingProcess",Convert.ToString(c.WeldingProcess)),//Convert.ToString(c.WeldingProcess) ,//
                                    isReadOnly ? (c.SetupRate.HasValue ? c.SetupRate.ToString() :""):Helper.GenerateTextbox(c.LineId, "SetupRate", c.SetupRate.ToString(), "",true,"", "","", false,"",""),
                                    isReadOnly ? (c.WeldingRate.HasValue ?  c.WeldingRate.ToString():""):Helper.GenerateTextbox(c.LineId, "WeldingRate", c.WeldingRate.ToString(), "",true,"","", "", false,"",""),
                                    isReadOnly ? c.CriticalSeam.ToString():Helper.HTMLAutoComplete(c.LineId, "txtCriticalSeam",c.CriticalSeam,"", false,"","CriticalSeam",false)+""+Helper.GenerateHidden(c.LineId, "CriticalSeam",Convert.ToString(c.CriticalSeam)),
                                    isReadOnly ? (c.Factor.HasValue ?c.Factor.ToString():""):Helper.GenerateTextbox(c.LineId, "Factor",  c.Factor.ToString(), "",true, "","","", false,"",""),
                                    isReadOnly ? (c.Amount.HasValue? c.Amount.ToString():""):Helper.GenerateTextbox(c.LineId, "Amount",  c.Amount.ToString(), "",false, "","","clsMandatory", false,"",""),
                                    !string.IsNullOrWhiteSpace(c.SetupCJCNo)? c.SetupCJCNo.ToString() :"",
                                    !string.IsNullOrWhiteSpace(c.WeldCJCNo)?   c.WeldCJCNo.ToString():"",
                                    "R"+ c.Revision.ToString(),
                          c.CreatedBy,
                          Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                          Helper.HTMLActionString(c.LineId,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.LineId + ");") + Helper.HTMLActionString(c.LineId,"Update","Update Record","fa fa-floppy-o","SaveRecord(" + c.LineId + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.LineId,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.LineId + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.LineId +");"),
                        }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveSeamLineData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (fc != null)
                {
                    int HeaderId = Convert.ToInt32(fc["MainHeaderId"]);
                    int LineId = Convert.ToInt32(fc["MainLineId"]);

                    CJC021 objCJC021 = null;
                    CJC020 objCJC020 = db.CJC020.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    string QualityProject = fc["QualityProject" + LineId]; ;
                    string SeamNo = fc["SeamNo" + LineId];
                    if (LineId > 0)
                    {

                        objCJC021 = db.CJC021.Where(x => x.LineId == LineId).FirstOrDefault();
                        if (objCJC021 != null)
                        {
                            if (!db.CJC021.Any(x => x.HeaderId == HeaderId && x.QualityProject == QualityProject && x.SeamNo == SeamNo && x.LineId != LineId))
                            {
                                objCJC021.HeaderId = HeaderId;
                                objCJC021.QualityProject = fc["QualityProject" + LineId];
                                objCJC021.SeamNo = fc["SeamNo" + LineId];
                                objCJC021.SetupCJCNo = fc["SetupCJCNo" + LineId];
                                objCJC021.WeldCJCNo = fc["WeldCJCNo" + LineId];
                                objCJC021.SeamCategory = fc["SeamCategory" + LineId];
                                objCJC021.NozzleSize = Convert.ToDouble(fc["NozzleSize" + LineId]);
                                objCJC021.NozzleCategory = fc["NozzleCategory" + LineId];
                                objCJC021.SeamJointType = fc["SeamJointType" + LineId];
                                objCJC021.SeamLength = Convert.ToInt32(fc["SeamLength" + LineId]);
                                objCJC021.Part1Position = fc["Part1Position" + LineId];
                                objCJC021.Part2Position = fc["Part2Position" + LineId];
                                objCJC021.Part3Position = fc["Part3Position" + LineId];
                                objCJC021.Part1Thickness = fc["Part1Thickness" + LineId];
                                objCJC021.Part2Thickness = fc["Part2Thickness" + LineId];
                                objCJC021.Part3Thickness = fc["Part3Thickness" + LineId];
                                objCJC021.MinThickness = Convert.ToDouble(fc["MinThickness" + LineId]);
                                objCJC021.Unit = Convert.ToDouble(fc["Unit" + LineId]);
                                objCJC021.WeldingProcess = fc["WeldingProcess" + LineId];
                                objCJC021.SetupRate = Convert.ToDouble(fc["SetupRate" + LineId]);
                                objCJC021.WeldingRate = Convert.ToDouble(fc["WeldingRate" + LineId]);
                                objCJC021.CriticalSeam = fc["CriticalSeam" + LineId];
                                objCJC021.Factor = Convert.ToDouble(fc["Factor" + LineId]);
                                objCJC021.Amount = Convert.ToDouble(fc["Amount" + LineId]);
                                objCJC021.Revision = objCJC020.Revision;
                                objCJC021.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                objCJC021.EditedBy = objClsLoginInfo.UserName;
                                objCJC021.EditedOn = DateTime.Now;
                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
                        }
                    }
                    else
                    {
                        if (!db.CJC021.Any(x => x.HeaderId == HeaderId && x.QualityProject == QualityProject && x.SeamNo == SeamNo))
                        {
                            objCJC021 = new CJC021();
                            objCJC021.HeaderId = HeaderId;
                            objCJC021.QualityProject = fc["QualityProject" + LineId];
                            objCJC021.SeamNo = fc["SeamNo" + LineId];
                            objCJC021.SetupCJCNo = fc["SetupCJCNo" + LineId];
                            objCJC021.WeldCJCNo = fc["WeldCJCNo" + LineId];
                            objCJC021.SeamCategory = fc["SeamCategory" + LineId];
                            objCJC021.NozzleSize = Convert.ToDouble(fc["NozzleSize" + LineId]);
                            objCJC021.NozzleCategory = fc["NozzleCategory" + LineId];
                            objCJC021.SeamJointType = fc["SeamJointType" + LineId];
                            objCJC021.SeamLength = Convert.ToInt32(fc["SeamLength" + LineId]);
                            objCJC021.Part1Position = fc["Part1Position" + LineId];
                            objCJC021.Part2Position = fc["Part2Position" + LineId];
                            objCJC021.Part3Position = fc["Part3Position" + LineId];
                            objCJC021.Part1Thickness = fc["Part1Thickness" + LineId];
                            objCJC021.Part2Thickness = fc["Part2Thickness" + LineId];
                            objCJC021.Part3Thickness = fc["Part3Thickness" + LineId];
                            objCJC021.MinThickness = Convert.ToDouble(fc["MinThickness" + LineId]);
                            if (!(string.IsNullOrWhiteSpace(fc["Unit" + LineId])))
                            {
                                objCJC021.Unit = Convert.ToDouble(fc["Unit" + LineId]);
                            }
                            objCJC021.WeldingProcess = fc["WeldingProcess" + LineId];
                            objCJC021.SetupRate = Convert.ToDouble(fc["SetupRate" + LineId]);
                            objCJC021.WeldingRate = Convert.ToDouble(fc["WeldingRate" + LineId]);
                            objCJC021.CriticalSeam = fc["CriticalSeam" + LineId];
                            objCJC021.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(fc["Factor" + LineId]) ? fc["Factor" + LineId] : "0");
                            objCJC021.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(fc["Amount" + LineId]) ? fc["Amount" + LineId] : "0");
                            objCJC021.Revision = objCJC020.Revision;
                            objCJC021.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                            objCJC021.CreatedBy = objClsLoginInfo.UserName;
                            objCJC021.CreatedOn = DateTime.Now;
                            db.CJC021.Add(objCJC021);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteSeamLineData(int rowid, int editlineid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CJC021 objCJC021 = db.CJC021.Where(x => x.LineId == editlineid).FirstOrDefault();
                if (objCJC021 != null)
                {
                    db.CJC021.Remove(objCJC021);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Generate CJC

        //save/update header data and create CJC no by pushing data into LN
        [HttpPost]
        public ActionResult GenerateCJCHeader(CJC020 cjc020)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            clsHelper.ResponseMsg obj = new clsHelper.ResponseMsg();
            clsHelper.ResponseMsg objServiceRes = new clsHelper.ResponseMsg();
            try
            {
                CJC020 objCJC020 = new CJC020();
                if (cjc020.HeaderId == 0)
                {
                    #region Insert
                    IEMQSEntitiesContext context = new IEMQSEntitiesContext();
                    using (DbContextTransaction dbTran = context.Database.BeginTransaction())
                    {
                        try
                        {
                            objCJC020.CJCNo = objServiceRes.Value;
                            objCJC020.Project = cjc020.Project;
                            objCJC020.Shop = cjc020.Shop;
                            objCJC020.Dia = cjc020.Dia;
                            objCJC020.Location = cjc020.Location;
                            objCJC020.BusinessPartner = cjc020.BusinessPartner;
                            objCJC020.PurchaseOdrerNo = cjc020.PurchaseOdrerNo;
                            objCJC020.PurchaseOdrerLineNo = cjc020.PurchaseOdrerLineNo;
                            objCJC020.Initiator = cjc020.Initiator;
                            objCJC020.BillNo = cjc020.BillNo;
                            objCJC020.BillDate = cjc020.BillDate;
                            objCJC020.Revision = 0;
                            objCJC020.Status = clsImplementationEnum.AutoCJCStatus.Submitted.GetStringValue();
                            objCJC020.TotalCJCAmount = cjc020.TotalCJCAmount;
                            objCJC020.CreatedBy = objClsLoginInfo.UserName;
                            objCJC020.CreatedOn = DateTime.Now;

                            context.CJC020.Add(objCJC020);
                            context.SaveChanges();

                            #region Insert Seam

                            DataTable dtSeam = (DataTable)TempData["FetchSeamList"];
                            if (dtSeam != null)
                            {
                                List<DataRow> dtFinal = (from u in dtSeam.Rows.Cast<DataRow>()
                                                         where !string.IsNullOrWhiteSpace(u["SeamCategory"].ToString())
                                                                && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                         select u).ToList();

                                if (dtFinal != null && dtFinal.Count > 0)
                                {
                                    List<CJC021> lstAddSeam = new List<CJC021>();
                                    foreach (DataRow dtRow in dtFinal)
                                    {
                                        #region Assign Values 

                                        CJC021 objCJC021 = new CJC021();
                                        objCJC021.HeaderId = objCJC020.HeaderId;
                                        objCJC021.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                        objCJC021.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                        objCJC021.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                        objCJC021.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["NozzleSize"])) ? Convert.ToString(dtRow["NozzleSize"]) : "0");
                                        objCJC021.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                        objCJC021.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                        objCJC021.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamLength"])) ? Convert.ToString(dtRow["SeamLength"]) : "0");
                                        objCJC021.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                        objCJC021.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                        objCJC021.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                        objCJC021.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                        objCJC021.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                        objCJC021.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                        objCJC021.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                        if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                        {
                                            objCJC021.Unit = Convert.ToDouble(dtRow["Unit"]);
                                        }
                                        objCJC021.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                        objCJC021.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? Convert.ToString(dtRow["SetupRate"]) : "0");
                                        objCJC021.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? Convert.ToString(dtRow["WeldingRate"]) : "0");
                                        objCJC021.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                        objCJC021.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                        objCJC021.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                        objCJC021.Revision = objCJC020.Revision;
                                        objCJC021.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                        objCJC021.CreatedBy = objClsLoginInfo.UserName;
                                        objCJC021.CreatedOn = DateTime.Now;
                                        lstAddSeam.Add(objCJC021);

                                        #endregion
                                    }

                                    if (lstAddSeam.Count > 0)
                                    {
                                        context.CJC021.AddRange(lstAddSeam);
                                        context.SaveChanges();
                                    }
                                }
                            }

                            #endregion

                            #region Insert Activity

                            DataTable dtActivity = (DataTable)TempData["ProjectLines"];
                            if (dtActivity != null)
                            {
                                List<DataRow> dtFinal = (from u in dtActivity.Rows.Cast<DataRow>()
                                                         where !string.IsNullOrWhiteSpace(u["Activity"].ToString())
                                                               && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                         select u).ToList();

                                if (dtFinal != null && dtFinal.Count > 0)
                                {
                                    List<CJC022> lstAddActivity = new List<CJC022>();
                                    foreach (DataRow dtRow in dtFinal)
                                    {
                                        CJC022 objCJC022 = new CJC022();
                                        objCJC022.HeaderId = objCJC020.HeaderId;
                                        objCJC022.Activity = Convert.ToString(dtRow["Activity"]);
                                        objCJC022.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                        objCJC022.UOM = Convert.ToString(dtRow["UOM"]);
                                        objCJC022.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                        objCJC022.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                        objCJC022.Revision = 0;
                                        objCJC022.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                        objCJC022.CreatedBy = objClsLoginInfo.UserName;
                                        objCJC022.CreatedOn = DateTime.Now;
                                        lstAddActivity.Add(objCJC022);
                                    }
                                    if (lstAddActivity.Count > 0)
                                    {
                                        context.CJC022.AddRange(lstAddActivity);
                                        context.SaveChanges();
                                    }
                                }
                            }
                            #endregion

                            objServiceRes = GenerateCJCIntoLN(cjc020);
                            if (objServiceRes.Key)
                            {
                                string CJCNo = objServiceRes.Value;

                                objCJC020.CJCNo = CJCNo;

                                context.CJC021.Where(x => x.HeaderId == objCJC020.HeaderId && string.IsNullOrEmpty(x.SetupCJCNo) && x.SetupRate > 0).ToList().ForEach(x =>
                                {
                                    x.SetupCJCNo = CJCNo;
                                    x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                });

                                context.CJC021.Where(x => x.HeaderId == objCJC020.HeaderId && string.IsNullOrEmpty(x.WeldCJCNo) && x.WeldingRate > 0).ToList().ForEach(x =>
                                {
                                    x.WeldCJCNo = CJCNo;
                                    x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                });

                                context.CJC022.Where(x => x.HeaderId == objCJC020.HeaderId && x.Amount > 0).ToList().ForEach(x =>
                                {
                                    x.CJCNo = CJCNo;
                                    x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                });

                                context.SaveChanges();
                                dbTran.Commit();

                                TempData["FetchSeamList"] = null;
                                TempData["ProjectLines"] = null;

                                objResponseMsg.Key = true;
                                objResponseMsg.Value = "CJC : '" + CJCNo + "' generated successfully";
                                objResponseMsg.Status = objCJC020.Status;
                                objResponseMsg.HeaderId = objCJC020.HeaderId;
                            }
                            else
                            {
                                dbTran.Rollback();
                                RetenTempDataValue();
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = objServiceRes.Value;
                            }
                        }
                        catch (DbEntityValidationException dex)
                        {
                            dbTran.Rollback();
                            RetenTempDataValue();
                            throw dex;
                        }
                        catch (Exception ex)
                        {
                            dbTran.Rollback();
                            RetenTempDataValue();
                            throw ex;
                        }
                        finally
                        {
                            context = null;
                        }
                    }

                    #endregion
                }
                string existingCJCNo = string.Empty;
                if (cjc020.HeaderId > 0)
                {
                    IEMQSEntitiesContext context = new IEMQSEntitiesContext();
                    using (DbContextTransaction dbTran = context.Database.BeginTransaction())
                    {
                        try
                        {
                            objCJC020 = context.CJC020.Where(o => o.HeaderId == cjc020.HeaderId).FirstOrDefault();
                            if (objCJC020 != null)
                            {
                                objCJC020.Status = clsImplementationEnum.AutoCJCStatus.Submitted.GetStringValue();
                                objCJC020.EditedBy = objClsLoginInfo.UserName;
                                objCJC020.EditedOn = DateTime.Now;
                                existingCJCNo = objCJC020.CJCNo;
                                context.SaveChanges();

                                #region Insert Seam

                                DataTable dtSeam = (DataTable)TempData["FetchSeamList"];
                                if (dtSeam != null)
                                {
                                    List<DataRow> dtFinal = (from u in dtSeam.Rows.Cast<DataRow>()
                                                             where !string.IsNullOrWhiteSpace(u["SeamCategory"].ToString())
                                                                    && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                             select u).ToList();
                                    //List<DataRow> dtFinalLines = (from u in dtSeam.Rows.Cast<DataRow>()
                                    //                         where !string.IsNullOrWhiteSpace(u["SeamCategory"].ToString())
                                    //                                && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                    //                         select u).ToList();
                                    if (dtFinal != null && dtFinal.Count > 0)
                                    {
                                        List<CJC021> lstAddSeam = new List<CJC021>();
                                        List<CJC021> lstDeleteCJC021 = new List<CJC021>();
                                        var LineIds = dtFinal.Where(x => Convert.ToInt32(x["LineId"]) != 0).Select(x => Convert.ToInt32(x["LineId"]));

                                        lstDeleteCJC021 = db.CJC021.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == objCJC020.HeaderId).ToList();
                                        if (lstDeleteCJC021.Count > 0)
                                        {
                                            db.CJC021.RemoveRange(lstDeleteCJC021);
                                        }
                                        db.SaveChanges();

                                        foreach (DataRow dtRow in dtFinal)
                                        {
                                            CJC021 objCJC021 = new CJC021();
                                            var QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                            var SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                            objCJC021 = db.CJC021.Where(o => o.HeaderId == objCJC020.HeaderId && o.SeamNo == SeamNo && o.QualityProject == QualityProject).FirstOrDefault();
                                            int EditLineId = objCJC021 != null ? objCJC021.LineId : 0;
                                            if (EditLineId == 0)
                                            {
                                                #region Assign Values 

                                                objCJC021 = new CJC021();
                                                objCJC021.HeaderId = objCJC020.HeaderId;
                                                objCJC021.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                                objCJC021.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                                objCJC021.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                                objCJC021.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["NozzleSize"])) ? Convert.ToString(dtRow["NozzleSize"]) : "0");
                                                objCJC021.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                                objCJC021.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                                objCJC021.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamLength"])) ? Convert.ToString(dtRow["SeamLength"]) : "0");
                                                objCJC021.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                                objCJC021.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                                objCJC021.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                                objCJC021.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                                objCJC021.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                                objCJC021.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                                objCJC021.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                                if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                                {
                                                    objCJC021.Unit = Convert.ToDouble(dtRow["Unit"]);
                                                }
                                                objCJC021.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                                objCJC021.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? Convert.ToString(dtRow["SetupRate"]) : "0");
                                                objCJC021.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? Convert.ToString(dtRow["WeldingRate"]) : "0");
                                                objCJC021.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                                objCJC021.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                                objCJC021.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                                objCJC021.Revision = objCJC020.Revision;
                                                objCJC021.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                                objCJC021.CreatedBy = objClsLoginInfo.UserName;
                                                objCJC021.CreatedOn = DateTime.Now;
                                                lstAddSeam.Add(objCJC021);

                                                #endregion
                                            }
                                            else
                                            {
                                                #region Assign Values 

                                                objCJC021 = context.CJC021.Where(o => o.LineId == EditLineId).FirstOrDefault();
                                                if (objCJC021 != null)
                                                {
                                                    objCJC021.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                                    objCJC021.SeamNo = dtRow["SeamNo"].ToString();
                                                    objCJC021.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                                    objCJC021.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(dtRow["NozzleSize"].ToString()) ? dtRow["NozzleSize"].ToString() : "0");
                                                    objCJC021.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                                    objCJC021.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                                    objCJC021.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(dtRow["SeamLength"].ToString()) ? dtRow["SeamLength"].ToString() : "0");
                                                    objCJC021.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                                    objCJC021.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                                    objCJC021.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                                    objCJC021.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                                    objCJC021.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                                    objCJC021.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                                    objCJC021.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                                    if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                                    {
                                                        objCJC021.Unit = Convert.ToDouble(dtRow["Unit"]);
                                                    }
                                                    objCJC021.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                                    objCJC021.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? dtRow["SetupRate"].ToString() : "0");
                                                    objCJC021.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? dtRow["WeldingRate"].ToString() : "0");
                                                    objCJC021.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                                    objCJC021.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                                    objCJC021.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                                    objCJC021.Revision = objCJC020.Revision;
                                                    objCJC021.EditedBy = objClsLoginInfo.UserName;
                                                    objCJC021.EditedOn = DateTime.Now;
                                                }

                                                #endregion
                                            }
                                        }

                                        if (lstAddSeam.Count > 0)
                                        {
                                            context.CJC021.AddRange(lstAddSeam);
                                            context.SaveChanges();
                                        }
                                    }
                                }

                                #endregion

                                #region Insert Activity

                                DataTable dtActivity = (DataTable)TempData["ProjectLines"];
                                if (dtActivity != null)
                                {
                                    List<DataRow> dtFinal = (from u in dtActivity.Rows.Cast<DataRow>()
                                                             where !string.IsNullOrWhiteSpace(u["Activity"].ToString())
                                                                   && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                             select u).ToList();

                                    if (dtFinal != null && dtFinal.Count > 0)
                                    {
                                        List<CJC022> lstAddActivity = new List<CJC022>();
                                        List<CJC022> lstDeleteCJC022 = new List<CJC022>();
                                        
                                        var LineIds = dtFinal.Where(x => Convert.ToInt32(x["LineId"]) != 0).Select(x => Convert.ToInt32(x["LineId"]));

                                        lstDeleteCJC022 = db.CJC022.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == objCJC020.HeaderId).ToList();
                                        if (lstDeleteCJC022.Count > 0)
                                        {
                                            db.CJC022.RemoveRange(lstDeleteCJC022);
                                        }
                                        db.SaveChanges();

                                        foreach (DataRow dtRow in dtFinal)
                                        {
                                            string Activity = Convert.ToString(dtRow["Activity"]);
                                            if (!context.CJC022.Any(x => x.HeaderId == objCJC020.HeaderId && x.Activity == Activity))
                                            {
                                                CJC022 objCJC022 = new CJC022();
                                                objCJC022.HeaderId = objCJC020.HeaderId;
                                                objCJC022.Activity = Activity;
                                                objCJC022.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                                objCJC022.UOM = Convert.ToString(dtRow["UOM"]);
                                                objCJC022.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                                objCJC022.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                                objCJC022.Revision = 0;
                                                objCJC022.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                                objCJC022.CreatedBy = objClsLoginInfo.UserName;
                                                objCJC022.CreatedOn = DateTime.Now;
                                                lstAddActivity.Add(objCJC022);
                                            }
                                            else
                                            {
                                                CJC022 objCJC022 = context.CJC022.Where(x => x.HeaderId == objCJC020.HeaderId && x.Activity == Activity).FirstOrDefault();
                                                if (objCJC022 != null)
                                                {
                                                    objCJC022.Activity = Activity;
                                                    objCJC022.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                                    objCJC022.UOM = Convert.ToString(dtRow["UOM"]);
                                                    objCJC022.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                                    objCJC022.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                                    objCJC022.Revision = 0;
                                                    objCJC022.EditedBy = objClsLoginInfo.UserName;
                                                    objCJC022.EditedOn = DateTime.Now;
                                                }
                                            }
                                        }
                                        if (lstAddActivity.Count > 0)
                                        {
                                            context.CJC022.AddRange(lstAddActivity);
                                            context.SaveChanges();
                                        }
                                    }
                                }

                                #endregion

                                objServiceRes = GenerateCJCIntoLN(cjc020);
                                if (objServiceRes.Key)
                                {
                                    string CJCNo = objServiceRes.Value;

                                    objCJC020.CJCNo = CJCNo;

                                    context.CJC021.Where(x => x.HeaderId == objCJC020.HeaderId && string.IsNullOrEmpty(x.SetupCJCNo) && x.SetupRate > 0).ToList().ForEach(x =>
                                    {
                                        x.SetupCJCNo = CJCNo;
                                        x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                    });

                                    context.CJC021.Where(x => x.HeaderId == objCJC020.HeaderId && string.IsNullOrEmpty(x.WeldCJCNo) && x.WeldingRate > 0).ToList().ForEach(x =>
                                    {
                                        x.WeldCJCNo = CJCNo;
                                        x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                    });

                                    context.CJC022.Where(x => x.HeaderId == objCJC020.HeaderId && x.Amount > 0).ToList().ForEach(x =>
                                    {
                                        x.CJCNo = CJCNo;
                                        x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                    });

                                    context.SaveChanges();
                                    dbTran.Commit();

                                    TempData["FetchSeamList"] = null;
                                    TempData["ProjectLines"] = null;

                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "CJC : '" + CJCNo + "' generated successfully";
                                    objResponseMsg.Status = objCJC020.Status;
                                    objResponseMsg.HeaderId = objCJC020.HeaderId;
                                }
                                else
                                {
                                    dbTran.Rollback();
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = objServiceRes.Value;
                                }
                            }
                            else
                            {
                                dbTran.Rollback();
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Record not found";
                            }
                        }
                        catch (DbEntityValidationException dex)
                        {
                            dbTran.Rollback();
                            throw dex;
                        }
                        catch (Exception ex)
                        {
                            dbTran.Rollback();
                            throw ex;
                        }
                        finally
                        {
                            context = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.Message;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //header data and reclaim amount with new CJC no by pushing data into LN
        [HttpPost]
        public ActionResult ReclaimCJC(CJC020 cjc020)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            clsHelper.ResponseMsg obj = new clsHelper.ResponseMsg();
            clsHelper.ResponseMsg objServiceRes = new clsHelper.ResponseMsg();
            try
            {
                CJC020 objCJC020 = new CJC020();
                string existingCJCNo = string.Empty;
                if (cjc020.HeaderId > 0)
                {
                    IEMQSEntitiesContext context = new IEMQSEntitiesContext();
                    using (DbContextTransaction dbTran = context.Database.BeginTransaction())
                    {
                        try
                        {
                            objCJC020 = context.CJC020.Where(o => o.HeaderId == cjc020.HeaderId).FirstOrDefault();
                            if (objCJC020 != null)
                            {
                                objCJC020.Status = clsImplementationEnum.AutoCJCStatus.Submitted.GetStringValue();
                                objCJC020.EditedBy = objClsLoginInfo.UserName;
                                objCJC020.EditedOn = DateTime.Now;
                                existingCJCNo = objCJC020.CJCNo;
                                context.SaveChanges();

                                #region Insert Seam

                                DataTable dtSeam = (DataTable)TempData["FetchSeamList"];
                                if (dtSeam != null)
                                {
                                    List<DataRow> dtFinal = (from u in dtSeam.Rows.Cast<DataRow>()
                                                             where !string.IsNullOrWhiteSpace(u["SeamCategory"].ToString())
                                                                    && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                             select u).ToList();

                                    if (dtFinal != null && dtFinal.Count > 0)
                                    {
                                        List<CJC021> lstAddSeam = new List<CJC021>();
                                        foreach (DataRow dtRow in dtFinal)
                                        {
                                            CJC021 objCJC021 = new CJC021();
                                            var QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                            var SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                            objCJC021 = db.CJC021.Where(o => o.HeaderId == objCJC020.HeaderId && o.SeamNo == SeamNo && o.QualityProject == QualityProject).FirstOrDefault();
                                            int EditLineId = objCJC021 != null ? objCJC021.LineId : 0;//Convert.ToInt32(dtRow["EditLineId"]);
                                            if (EditLineId == 0)
                                            {
                                                #region Assign Values 

                                                objCJC021 = new CJC021();
                                                objCJC021.HeaderId = objCJC020.HeaderId;
                                                objCJC021.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                                objCJC021.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                                objCJC021.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                                objCJC021.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["NozzleSize"])) ? Convert.ToString(dtRow["NozzleSize"]) : "0");
                                                objCJC021.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                                objCJC021.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                                objCJC021.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamLength"])) ? Convert.ToString(dtRow["SeamLength"]) : "0");
                                                objCJC021.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                                objCJC021.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                                objCJC021.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                                objCJC021.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                                objCJC021.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                                objCJC021.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                                objCJC021.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                                if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                                {
                                                    objCJC021.Unit = Convert.ToDouble(dtRow["Unit"]);
                                                }
                                                objCJC021.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                                objCJC021.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? Convert.ToString(dtRow["SetupRate"]) : "0");
                                                objCJC021.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? Convert.ToString(dtRow["WeldingRate"]) : "0");
                                                objCJC021.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                                objCJC021.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                                objCJC021.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                                objCJC021.Revision = objCJC020.Revision;
                                                objCJC021.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                                objCJC021.CreatedBy = objClsLoginInfo.UserName;
                                                objCJC021.CreatedOn = DateTime.Now;
                                                lstAddSeam.Add(objCJC021);

                                                #endregion
                                            }
                                            else
                                            {
                                                #region Assign Values 

                                                objCJC021 = context.CJC021.Where(o => o.LineId == EditLineId).FirstOrDefault();
                                                if (objCJC021 != null)
                                                {
                                                    objCJC021.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                                    objCJC021.SeamNo = dtRow["SeamNo"].ToString();
                                                    objCJC021.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                                    objCJC021.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(dtRow["NozzleSize"].ToString()) ? dtRow["NozzleSize"].ToString() : "0");
                                                    objCJC021.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                                    objCJC021.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                                    objCJC021.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(dtRow["SeamLength"].ToString()) ? dtRow["SeamLength"].ToString() : "0");
                                                    objCJC021.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                                    objCJC021.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                                    objCJC021.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                                    objCJC021.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                                    objCJC021.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                                    objCJC021.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                                    objCJC021.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                                    if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                                    {
                                                        objCJC021.Unit = Convert.ToDouble(dtRow["Unit"]);
                                                    }
                                                    objCJC021.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                                    objCJC021.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? dtRow["SetupRate"].ToString() : "0");
                                                    objCJC021.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? dtRow["WeldingRate"].ToString() : "0");
                                                    objCJC021.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                                    objCJC021.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                                    objCJC021.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                                    objCJC021.Revision = objCJC020.Revision;
                                                    objCJC021.EditedBy = objClsLoginInfo.UserName;
                                                    objCJC021.EditedOn = DateTime.Now;
                                                }

                                                #endregion
                                            }
                                        }

                                        if (lstAddSeam.Count > 0)
                                        {
                                            context.CJC021.AddRange(lstAddSeam);
                                            context.SaveChanges();
                                        }
                                    }
                                }

                                #endregion

                                #region Insert Activity

                                DataTable dtActivity = (DataTable)TempData["ProjectLines"];
                                if (dtActivity != null)
                                {
                                    List<DataRow> dtFinal = (from u in dtActivity.Rows.Cast<DataRow>()
                                                             where !string.IsNullOrWhiteSpace(u["Activity"].ToString())
                                                                   && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                             select u).ToList();

                                    if (dtFinal != null && dtFinal.Count > 0)
                                    {
                                        List<CJC022> lstAddActivity = new List<CJC022>();
                                        foreach (DataRow dtRow in dtFinal)
                                        {
                                            string Activity = Convert.ToString(dtRow["Activity"]);
                                            if (!context.CJC022.Any(x => x.HeaderId == objCJC020.HeaderId && x.Activity == Activity))
                                            {
                                                CJC022 objCJC022 = new CJC022();
                                                objCJC022.HeaderId = objCJC020.HeaderId;
                                                objCJC022.Activity = Activity;
                                                objCJC022.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                                objCJC022.UOM = Convert.ToString(dtRow["UOM"]);
                                                objCJC022.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                                objCJC022.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                                objCJC022.Revision = 0;
                                                objCJC022.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                                objCJC022.CreatedBy = objClsLoginInfo.UserName;
                                                objCJC022.CreatedOn = DateTime.Now;
                                                lstAddActivity.Add(objCJC022);
                                            }
                                            else
                                            {
                                                CJC022 objCJC022 = context.CJC022.Where(x => x.HeaderId == objCJC020.HeaderId && x.Activity == Activity).FirstOrDefault();
                                                if (objCJC022 != null)
                                                {
                                                    objCJC022.Activity = Activity;
                                                    objCJC022.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                                    objCJC022.UOM = Convert.ToString(dtRow["UOM"]);
                                                    objCJC022.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                                    objCJC022.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                                    objCJC022.Revision = 0;
                                                    objCJC022.EditedBy = objClsLoginInfo.UserName;
                                                    objCJC022.EditedOn = DateTime.Now;
                                                }
                                            }
                                        }
                                        if (lstAddActivity.Count > 0)
                                        {
                                            context.CJC022.AddRange(lstAddActivity);
                                            context.SaveChanges();
                                        }
                                    }
                                }

                                #endregion

                                objServiceRes = GenerateCJCIntoLN(cjc020, true);
                                if (objServiceRes.Key)
                                {
                                    string CJCNo = objServiceRes.Value;

                                    objCJC020.CJCNo = CJCNo;

                                    context.CJC021.Where(x => x.HeaderId == objCJC020.HeaderId && x.SetupCJCNo == existingCJCNo && x.SetupRate > 0).ToList().ForEach(x =>
                                    {
                                        x.SetupCJCNo = CJCNo;
                                        x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                    });

                                    context.CJC021.Where(x => x.HeaderId == objCJC020.HeaderId && x.WeldCJCNo == existingCJCNo && x.WeldingRate > 0).ToList().ForEach(x =>
                                    {
                                        x.WeldCJCNo = CJCNo;
                                        x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                    });

                                    context.CJC022.Where(x => x.HeaderId == objCJC020.HeaderId && x.CJCNo == existingCJCNo && x.Amount > 0).ToList().ForEach(x =>
                                    {
                                        x.CJCNo = CJCNo;
                                        x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                    });

                                    dbTran.Commit();

                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "CJC : '" + CJCNo + "' reclaimed successfully";
                                    objResponseMsg.Status = objCJC020.Status;
                                    objResponseMsg.HeaderId = objCJC020.HeaderId;
                                }
                                else
                                {
                                    dbTran.Rollback();
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = objServiceRes.Value;
                                }
                            }
                            else
                            {
                                dbTran.Rollback();
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Record not found";
                            }
                        }
                        catch (DbEntityValidationException dex)
                        {
                            dbTran.Rollback();
                            throw dex;
                        }
                        catch (Exception ex)
                        {
                            dbTran.Rollback();
                            throw ex;
                        }
                        finally
                        {
                            context = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.Message;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //insert/update Headerlines with "Draft" status 
        [HttpPost]
        public ActionResult SaveAsDraft(CJC020 cjc020)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            clsHelper.ResponseMsg obj = new clsHelper.ResponseMsg();
            try
            {
                CJC020 objCJC020 = new CJC020();
                string existingCJCNo = string.Empty;
                if (cjc020.HeaderId > 0)
                {
                    #region Update as Draft

                    objCJC020 = db.CJC020.Where(o => o.HeaderId == cjc020.HeaderId).FirstOrDefault();
                    if (objCJC020 != null)
                    {
                        #region update Header
                        objCJC020.BusinessPartner = cjc020.BusinessPartner;
                        objCJC020.PurchaseOdrerNo = cjc020.PurchaseOdrerNo;
                        objCJC020.PurchaseOdrerLineNo = cjc020.PurchaseOdrerLineNo;
                        objCJC020.Initiator = cjc020.Initiator;
                        objCJC020.BillNo = cjc020.BillNo;
                        objCJC020.BillDate = cjc020.BillDate;
                        objCJC020.Revision = 0;
                        objCJC020.Status = clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue();
                        objCJC020.TotalCJCAmount = cjc020.TotalCJCAmount;
                        objCJC020.EditedBy = objClsLoginInfo.UserName;
                        objCJC020.EditedOn = DateTime.Now;
                        existingCJCNo = objCJC020.CJCNo;
                        db.SaveChanges();
                        #endregion

                        #region Insert Seam

                        DataTable dtSeam = (DataTable)TempData["FetchSeamList"];
                        if (dtSeam != null)
                        {
                            List<DataRow> dtFinal = (from u in dtSeam.Rows.Cast<DataRow>()
                                                     where !string.IsNullOrWhiteSpace(u["SeamCategory"].ToString())
                                                            && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                     select u).ToList();

                            if (dtFinal != null && dtFinal.Count > 0)
                            {
                                List<CJC021> lstAddSeam = new List<CJC021>();
                                List<CJC021> lstDeleteCJC021 = new List<CJC021>();

                                var LineIds = dtFinal.Where(x => Convert.ToInt32(x["LineId"]) != 0).Select(x => Convert.ToInt32(x["LineId"]));

                                lstDeleteCJC021 = db.CJC021.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == objCJC020.HeaderId).ToList();
                                if (lstDeleteCJC021.Count > 0)
                                {
                                    db.CJC021.RemoveRange(lstDeleteCJC021);
                                }
                                db.SaveChanges();
                                foreach (DataRow dtRow in dtFinal)
                                {
                                    CJC021 objCJC021 = new CJC021();
                                    var QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                    var SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                    objCJC021 = db.CJC021.Where(o => o.HeaderId == objCJC020.HeaderId && o.SeamNo == SeamNo && o.QualityProject == QualityProject).FirstOrDefault();
                                    int EditLineId = objCJC021 != null ? objCJC021.LineId : 0;// Convert.ToInt32(dtRow["EditLineId"]);
                                    if (EditLineId == 0)
                                    {
                                        #region Assign Values 

                                        objCJC021 = new CJC021();
                                        objCJC021.HeaderId = objCJC020.HeaderId;
                                        objCJC021.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                        objCJC021.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                        objCJC021.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                        objCJC021.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["NozzleSize"])) ? Convert.ToString(dtRow["NozzleSize"]) : "0");
                                        objCJC021.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                        objCJC021.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                        objCJC021.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamLength"])) ? Convert.ToString(dtRow["SeamLength"]) : "0");
                                        objCJC021.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                        objCJC021.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                        objCJC021.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                        objCJC021.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                        objCJC021.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                        objCJC021.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                        objCJC021.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                        if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                        {
                                            objCJC021.Unit = Convert.ToDouble(dtRow["Unit"]);
                                        }
                                        objCJC021.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                        objCJC021.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? Convert.ToString(dtRow["SetupRate"]) : "0");
                                        objCJC021.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? Convert.ToString(dtRow["WeldingRate"]) : "0");
                                        objCJC021.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                        objCJC021.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                        objCJC021.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                        objCJC021.Revision = objCJC020.Revision;
                                        objCJC021.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                        objCJC021.CreatedBy = objClsLoginInfo.UserName;
                                        objCJC021.CreatedOn = DateTime.Now;
                                        lstAddSeam.Add(objCJC021);

                                        #endregion
                                    }
                                    else
                                    {
                                        #region Assign Values 
                                        
                                        if (objCJC021 != null)
                                        {
                                            objCJC021.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                            objCJC021.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                            objCJC021.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                            objCJC021.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(dtRow["NozzleSize"].ToString()) ? dtRow["NozzleSize"].ToString() : "0");
                                            objCJC021.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                            objCJC021.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                            objCJC021.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(dtRow["SeamLength"].ToString()) ? dtRow["SeamLength"].ToString() : "0");
                                            objCJC021.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                            objCJC021.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                            objCJC021.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                            objCJC021.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                            objCJC021.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                            objCJC021.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                            objCJC021.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                            if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                            {
                                                objCJC021.Unit = Convert.ToDouble(dtRow["Unit"]);
                                            }
                                            objCJC021.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                            objCJC021.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? dtRow["SetupRate"].ToString() : "0");
                                            objCJC021.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? dtRow["WeldingRate"].ToString() : "0");
                                            objCJC021.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                            objCJC021.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                            objCJC021.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                            objCJC021.Revision = objCJC020.Revision;
                                            objCJC021.EditedBy = objClsLoginInfo.UserName;
                                            objCJC021.EditedOn = DateTime.Now;
                                        }

                                        #endregion
                                    }
                                }

                                if (lstAddSeam.Count > 0)
                                {
                                    db.CJC021.AddRange(lstAddSeam);
                                    db.SaveChanges();
                                }
                            }
                        }

                        #endregion

                        #region Insert Activity

                        DataTable dtActivity = (DataTable)TempData["ProjectLines"];
                        if (dtActivity != null)
                        {
                            List<DataRow> dtFinal = (from u in dtActivity.Rows.Cast<DataRow>()
                                                     where !string.IsNullOrWhiteSpace(u["Activity"].ToString())
                                                           && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                     select u).ToList();

                            if (dtFinal != null && dtFinal.Count > 0)
                            {
                                List<CJC022> lstAddActivity = new List<CJC022>();
                                List<CJC022> lstDeleteCJC022 = new List<CJC022>();

                                var LineIds = dtFinal.Where(x => Convert.ToInt32(x["LineId"]) != 0).Select(x => Convert.ToInt32(x["LineId"]));

                                lstDeleteCJC022 = db.CJC022.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == objCJC020.HeaderId).ToList();
                                if (lstDeleteCJC022.Count > 0)
                                {
                                    db.CJC022.RemoveRange(lstDeleteCJC022);
                                }
                                db.SaveChanges();
                                foreach (DataRow dtRow in dtFinal)
                                {
                                    string Activity = Convert.ToString(dtRow["Activity"]);
                                    if (!db.CJC022.Any(x => x.HeaderId == objCJC020.HeaderId && x.Activity == Activity))
                                    {
                                        CJC022 objCJC022 = new CJC022();
                                        objCJC022.HeaderId = objCJC020.HeaderId;
                                        objCJC022.Activity = Activity;
                                        objCJC022.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                        objCJC022.UOM = Convert.ToString(dtRow["UOM"]);
                                        objCJC022.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                        objCJC022.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                        objCJC022.Revision = 0;
                                        objCJC022.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                        objCJC022.CreatedBy = objClsLoginInfo.UserName;
                                        objCJC022.CreatedOn = DateTime.Now;
                                        lstAddActivity.Add(objCJC022);
                                    }
                                    else
                                    {
                                        CJC022 objCJC022 = db.CJC022.Where(x => x.HeaderId == objCJC020.HeaderId && x.Activity == Activity).FirstOrDefault();
                                        if (objCJC022 != null)
                                        {
                                            objCJC022.Activity = Activity;
                                            objCJC022.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                            objCJC022.UOM = Convert.ToString(dtRow["UOM"]);
                                            objCJC022.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                            objCJC022.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                            objCJC022.Revision = 0;
                                            objCJC022.EditedBy = objClsLoginInfo.UserName;
                                            objCJC022.EditedOn = DateTime.Now;
                                        }
                                    }
                                }
                                if (lstAddActivity.Count > 0)
                                {
                                    db.CJC022.AddRange(lstAddActivity);
                                    db.SaveChanges();
                                }
                            }
                        }

                        #endregion

                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record not found";
                    }
                    #endregion
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                }
                else
                {
                    #region Insert Record as Draft
                    objCJC020.CJCNo = existingCJCNo;
                    objCJC020.Project = cjc020.Project;
                    objCJC020.Shop = cjc020.Shop;
                    objCJC020.Dia = cjc020.Dia;
                    objCJC020.Location = cjc020.Location;
                    objCJC020.BusinessPartner = cjc020.BusinessPartner;
                    objCJC020.PurchaseOdrerNo = cjc020.PurchaseOdrerNo;
                    objCJC020.PurchaseOdrerLineNo = cjc020.PurchaseOdrerLineNo;
                    objCJC020.Initiator = cjc020.Initiator;
                    objCJC020.BillNo = cjc020.BillNo;
                    objCJC020.BillDate = cjc020.BillDate;
                    objCJC020.Revision = 0;
                    objCJC020.Status = clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue();
                    objCJC020.TotalCJCAmount = cjc020.TotalCJCAmount;
                    objCJC020.CreatedBy = objClsLoginInfo.UserName;
                    objCJC020.CreatedOn = DateTime.Now;

                    db.CJC020.Add(objCJC020);
                    db.SaveChanges();

                    #region Insert Seam

                    DataTable dtSeam = (DataTable)TempData["FetchSeamList"];
                    if (dtSeam != null)
                    {
                        List<DataRow> dtFinal = (from u in dtSeam.Rows.Cast<DataRow>()
                                                 where !string.IsNullOrWhiteSpace(u["SeamCategory"].ToString())
                                                        && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                 select u).ToList();

                        if (dtFinal != null && dtFinal.Count > 0)
                        {
                            List<CJC021> lstAddSeam = new List<CJC021>();
                           
                            foreach (DataRow dtRow in dtFinal)
                            {
                                #region Assign Values 

                                CJC021 objCJC021 = new CJC021();
                                objCJC021.HeaderId = objCJC020.HeaderId;
                                objCJC021.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                objCJC021.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                objCJC021.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                objCJC021.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["NozzleSize"])) ? Convert.ToString(dtRow["NozzleSize"]) : "0");
                                objCJC021.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                objCJC021.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                objCJC021.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamLength"])) ? Convert.ToString(dtRow["SeamLength"]) : "0");
                                objCJC021.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                objCJC021.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                objCJC021.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                objCJC021.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                objCJC021.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                objCJC021.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                objCJC021.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                {
                                    objCJC021.Unit = Convert.ToDouble(dtRow["Unit"]);
                                }
                                objCJC021.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                objCJC021.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? Convert.ToString(dtRow["SetupRate"]) : "0");
                                objCJC021.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? Convert.ToString(dtRow["WeldingRate"]) : "0");
                                objCJC021.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                objCJC021.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                objCJC021.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                objCJC021.Revision = objCJC020.Revision;
                                objCJC021.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                objCJC021.CreatedBy = objClsLoginInfo.UserName;
                                objCJC021.CreatedOn = DateTime.Now;
                                lstAddSeam.Add(objCJC021);

                                #endregion
                            }

                            if (lstAddSeam.Count > 0)
                            {
                                db.CJC021.AddRange(lstAddSeam);
                                db.SaveChanges();
                            }
                        }
                    }

                    #endregion

                    #region Insert Activity

                    DataTable dtActivity = (DataTable)TempData["ProjectLines"];
                    if (dtActivity != null)
                    {
                        List<DataRow> dtFinal = (from u in dtActivity.Rows.Cast<DataRow>()
                                                 where !string.IsNullOrWhiteSpace(u["Activity"].ToString())
                                                       && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                 select u).ToList();

                        if (dtFinal != null && dtFinal.Count > 0)
                        {
                            List<CJC022> lstAddActivity = new List<CJC022>();
                            List<CJC022> lstDeleteCJC022 = new List<CJC022>();

                            var LineIds = dtFinal.Where(x => Convert.ToInt32(x["LineId"]) != 0).Select(x => Convert.ToInt32(x["LineId"]));

                            lstDeleteCJC022 = db.CJC022.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == objCJC020.HeaderId).ToList();
                            if (lstDeleteCJC022.Count > 0)
                            {
                                db.CJC022.RemoveRange(lstDeleteCJC022);
                            }
                            db.SaveChanges();

                            foreach (DataRow dtRow in dtFinal)
                            {
                                CJC022 objCJC022 = new CJC022();
                                objCJC022.HeaderId = objCJC020.HeaderId;
                                objCJC022.Activity = Convert.ToString(dtRow["Activity"]);
                                objCJC022.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                objCJC022.UOM = Convert.ToString(dtRow["UOM"]);
                                objCJC022.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                objCJC022.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                objCJC022.Revision = 0;
                                objCJC022.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                objCJC022.CreatedBy = objClsLoginInfo.UserName;
                                objCJC022.CreatedOn = DateTime.Now;
                                lstAddActivity.Add(objCJC022);
                            }
                            if (lstAddActivity.Count > 0)
                            {
                                db.CJC022.AddRange(lstAddActivity);
                                db.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "CJC saved successfully";
                    objResponseMsg.Status = objCJC020.Status;
                    objResponseMsg.HeaderId = objCJC020.HeaderId;

                    #endregion
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                }
                objResponseMsg.Key = true;
                objResponseMsg.Status = objCJC020.Status;
                objResponseMsg.HeaderId = objCJC020.HeaderId;
                RetenTempDataValue();
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.Message;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public clsHelper.ResponseMsg SaveSeamLineData(CJC020 cjc20)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (cjc20 != null)
                {
                    int HeaderId = cjc20.HeaderId;

                    CJC020 objCJC020 = db.CJC020.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    DataTable dt = (DataTable)TempData["FetchSeamList"];
                    List<CJC021> lstAddSeam = new List<CJC021>();
                    foreach (DataRow dtRow in dt.Rows)
                    {
                        CJC021 objCJC021 = new CJC021();
                        double amnt = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                        string QualityProject = Convert.ToString(dtRow["QualityProject"]);
                        string SeamNo = Convert.ToString(dtRow["SeamNo"]);
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamCategory"])) && amnt > 0)
                        {
                            if (!db.CJC021.Any(x => x.HeaderId == HeaderId && x.QualityProject == QualityProject && x.SeamNo == SeamNo))
                            {
                                objCJC021 = new CJC021();
                                objCJC021.HeaderId = HeaderId;
                                objCJC021.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                objCJC021.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                objCJC021.SetupCJCNo = Convert.ToString(dtRow["SetupCJCNo"]);
                                objCJC021.WeldCJCNo = Convert.ToString(dtRow["WeldCJCNo"]);
                                objCJC021.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                objCJC021.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["NozzleSize"])) ? Convert.ToString(dtRow["NozzleSize"]) : "0");
                                objCJC021.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                objCJC021.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                objCJC021.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamLength"])) ? Convert.ToString(dtRow["SeamLength"]) : "0");
                                objCJC021.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                objCJC021.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                objCJC021.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                objCJC021.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                objCJC021.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                objCJC021.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                objCJC021.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                {
                                    objCJC021.Unit = Convert.ToDouble(dtRow["Unit"]);
                                }
                                objCJC021.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                objCJC021.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? Convert.ToString(dtRow["SetupRate"]) : "0");
                                objCJC021.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? Convert.ToString(dtRow["WeldingRate"]) : "0");
                                objCJC021.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                objCJC021.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                objCJC021.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                objCJC021.Revision = objCJC020.Revision;
                                objCJC021.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                objCJC021.CreatedBy = objClsLoginInfo.UserName;
                                objCJC021.CreatedOn = DateTime.Now;

                                if (objCJC021.SetupRate > 0)
                                    objCJC021.SetupCJCNo = objCJC020.CJCNo;

                                if (objCJC021.WeldingRate > 0)
                                    objCJC021.WeldCJCNo = objCJC020.CJCNo;

                                lstAddSeam.Add(objCJC021);

                                //objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                            }
                        }
                    }
                    if (lstAddSeam.Count > 0)
                    {
                        db.CJC021.AddRange(lstAddSeam);
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return objResponseMsg;

        }

        [HttpPost]
        public clsHelper.ResponseMsg SaveActivityData(CJC020 cjc20)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (cjc20 != null)
                {
                    int HeaderId = cjc20.HeaderId;

                    CJC020 objCJC020 = db.CJC020.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    DataTable dt = TempData["ProjectLines"] as DataTable;
                    List<CJC022> lstAddSeam = new List<CJC022>();
                    foreach (DataRow dtRow in dt.Rows)
                    {
                        CJC022 objCJC022 = new CJC022();
                        double amnt = Convert.ToDouble(!string.IsNullOrWhiteSpace(dtRow["Amount"].ToString()) ? dtRow["Amount"] : "0");
                        string Activity = dtRow["Activity"].ToString();
                        if (!string.IsNullOrWhiteSpace(dtRow["Activity"].ToString()) && amnt > 0)
                        {
                            if (!db.CJC022.Any(x => x.HeaderId == HeaderId && x.Activity == Activity))
                            {
                                objCJC022 = new CJC022();
                                objCJC022.HeaderId = HeaderId;
                                objCJC022.Activity = Convert.ToString(dtRow["Activity"]);
                                objCJC022.Quantity = Convert.ToDouble(dtRow["Quantity"]);
                                objCJC022.UOM = Convert.ToString(dtRow["UOM"]);
                                objCJC022.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                objCJC022.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                objCJC022.CJCNo = Convert.ToString(dtRow["CJCNo"]);
                                objCJC022.Revision = 0;
                                objCJC022.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                objCJC022.CreatedBy = objClsLoginInfo.UserName;
                                objCJC022.CreatedOn = DateTime.Now;
                                objCJC022.CJCNo = objCJC020.CJCNo;
                                lstAddSeam.Add(objCJC022);
                                //objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                            }
                        }
                    }
                    if (lstAddSeam.Count > 0)
                    {
                        db.CJC022.AddRange(lstAddSeam);
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return objResponseMsg;
        }

        public clsHelper.ResponseMsg GenerateCJCIntoLN(CJC020 objCJC020, bool isReclaimCJC = false)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            objResponseMsg.Key = false;

            string InitiatorDept = string.Empty;
            var obj = db.SP_CJC_GET_LN_INITIATOR(1, 1, "", " PsNo = '" + objCJC020.Initiator + "'").FirstOrDefault();
            if (obj != null)
                InitiatorDept = obj.InitiatorDept;

            CjcDataService serviceObj = new CjcDataService();
            try
            {
                generateCJCResponseType P1createResponse = new generateCJCResponseType();
                generateCJCRequestType P1createRequest = new generateCJCRequestType();
                generateCJCRequestTypeControlArea P1controlArea = new generateCJCRequestTypeControlArea();
                generateCJCRequestTypeCjcData P1dataArea = new generateCJCRequestTypeCjcData();

                string PoWhere = " BP LIKE '%" + objClsLoginInfo.UserName + "%' AND t_orno LIKE '%" + objCJC020.PurchaseOdrerNo + "%' AND pono='" + objCJC020.PurchaseOdrerLineNo + "'";
                var PurOrder = db.SP_COMMON_GET_LN_APPROVED_PO(1, int.MaxValue, "", PoWhere).FirstOrDefault();

                if (isReclaimCJC)
                {
                    P1dataArea.cjcNo = objCJC020.CJCNo;
                    P1dataArea.purchaseOrder = objCJC020.PurchaseOdrerNo;
                    P1dataArea.BillDetail = new generateCJCRequestTypeCjcDataBillDetail[1];
                    P1dataArea.BillDetail[0].positionNo = Convert.ToString(objCJC020.PurchaseOdrerLineNo);
                    P1dataArea.BillDetail[0].billNo = objCJC020.BillNo;
                    if (objCJC020.BillDate.HasValue)
                    {
                        P1dataArea.BillDetail[0].billDate = Convert.ToDateTime(objCJC020.BillDate);
                        P1dataArea.BillDetail[0].billDateSpecified = true;
                    }
                    else
                    {
                        P1dataArea.BillDetail[0].billDateSpecified = false;
                    }
                    P1dataArea.BillDetail[0].claimedAmount = Convert.ToDecimal(objCJC020.TotalCJCAmount.Value);
                }
                else
                {
                    P1controlArea.processingScope = processingScope.request;
                    P1dataArea.cjcNo = objCJC020.Location;
                    P1dataArea.initiatorDept = InitiatorDept;
                    P1dataArea.briefDescription = "Auto Billing - " + objCJC020.Project;
                    P1dataArea.grossValue = objCJC020.TotalCJCAmount != null ? Convert.ToDecimal(objCJC020.TotalCJCAmount) : 0;
                    P1dataArea.grossValueSpecified = true;
                    P1dataArea.location = objCJC020.Location;
                    P1dataArea.purchaseOrder = objCJC020.PurchaseOdrerNo;
                    P1dataArea.initiatorsPsno = objCJC020.Initiator;
                    P1dataArea.supplier = objClsLoginInfo.UserName;

                    P1dataArea.BillDetail = new generateCJCRequestTypeCjcDataBillDetail[1];
                    P1dataArea.BillDetail[0] = new generateCJCRequestTypeCjcDataBillDetail();
                    P1dataArea.BillDetail[0].positionNo = Convert.ToString(objCJC020.PurchaseOdrerLineNo);
                    P1dataArea.BillDetail[0].billNo = objCJC020.BillNo;
                    if (objCJC020.BillDate.HasValue)
                    {
                        P1dataArea.BillDetail[0].billDate = Convert.ToDateTime(objCJC020.BillDate);
                        P1dataArea.BillDetail[0].billDateSpecified = true;
                    }
                    else
                    {
                        P1dataArea.BillDetail[0].billDateSpecified = false;
                    }
                    P1dataArea.BillDetail[0].claimedQty = Convert.ToDecimal(Math.Round(objCJC020.TotalCJCAmount.Value / PurOrder.Price, 4));
                    P1dataArea.BillDetail[0].claimedQtySpecified = true;

                    P1dataArea.BillDetail[0].claimedAmount = Convert.ToDecimal(objCJC020.TotalCJCAmount.Value);
                    P1dataArea.BillDetail[0].claimedAmountSpecified = true;

                    P1createRequest.ControlArea = P1controlArea;
                    P1createRequest.DataArea = new generateCJCRequestTypeCjcData[1];
                    P1createRequest.DataArea[0] = P1dataArea;
                }
                P1createResponse = serviceObj.generateCJC(P1createRequest);

                if (P1createResponse.DataArea[0].Remark.ToLower().StartsWith("0"))
                {
                    //success
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = P1createResponse.DataArea[0].cjcNo;
                }
                else
                {
                    //error
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = P1createResponse.DataArea[0].Remark.ToString();
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message.ToString();
            }
            finally
            {
                serviceObj = null;
            }
            return objResponseMsg;
        }

        #endregion

        #region Calculations

        [HttpPost]
        public ActionResult CalculateTotalClaimAmount(string PurchaseOdrerNo, int PurchaseOdrerLineNo, string Project = "", int dia = 0, bool isSaveAsDraft = false,string status="")
        {
            ResponceMsgWithCJC objData = new ResponceMsgWithCJC();
            double SeamAmount = 0;
            double totalAmount = 0, ActivityAmount = 0;
            try
            {
                DataTable dtSeam = new DataTable();
                DataTable dtActivity = new DataTable();
                if (TempData["FetchSeamList"] != null)
                {
                    string returned = clsImplementationEnum.AutoCJCStatus.Returned.GetStringValue();
                    string rejected = clsImplementationEnum.AutoCJCStatus.Rejected.GetStringValue();
                    string draft = clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue();
                    dtSeam = (DataTable)TempData["FetchSeamList"];
                    if (dtSeam != null && dtSeam.Rows.Count > 0)
                    {
                        if (status.Contains(returned))
                        {
                            //((IsSetUpCleared = 1 and SetupCJCStatus <> 'Draft' and ISNULL(SetupCJCNo,'') = '') or(IsPWHTCleared = 1 AND WeldCJCStatus <> 'Draft' and ISNULL(WeldCJCNo, '') = ''))";
                            SeamAmount = (from u in dtSeam.AsEnumerable()
                                          where Convert.ToString(u["Amount"]) != "" && Convert.ToString(u["Amount"]) != "0" && (Convert.ToString(u["SetupCJCStatus"]) == "" || u["SetupCJCStatus"].ToString().Contains(returned) || u["SetupCJCStatus"].ToString().Contains(rejected) || Convert.ToString(u["WeldCJCStatus"]) == "" || u["WeldCJCStatus"].ToString().Contains(returned) || u["WeldCJCStatus"].ToString().Contains(rejected))
                                          select Convert.ToDouble(u["Amount"])).Sum();
                        }
                        else {
                            SeamAmount = (from u in dtSeam.AsEnumerable()
                                          where Convert.ToString(u["Amount"]) != "" && Convert.ToString(u["Amount"]) != "0" && (Convert.ToString(u["SetupCJCStatus"]) == "" || u["SetupCJCStatus"].ToString().Contains(draft) || Convert.ToString(u["WeldCJCStatus"]) == "" || u["WeldCJCStatus"].ToString().Contains(draft))
                                          select Convert.ToDouble(u["Amount"])).Sum();
                        }
                    }
                }
                if (TempData["ProjectLines"] != null)
                {
                    dtActivity = (DataTable)TempData["ProjectLines"];
                    string Contingency_amount = clsImplementationEnum.AutoCJCProjectActivity.Contingency_amount.GetStringValue();

                    var isNotMaintained = (from u in dtActivity.AsEnumerable()
                                           where (Convert.ToString(u["Amount"]) != "" && Convert.ToString(u["Amount"]) != "0")
                                           && Convert.ToString(u["Activity"]) == Contingency_amount && string.IsNullOrWhiteSpace(Convert.ToString(u["Remarks"]))
                                           select u).ToList();
                    if (isNotMaintained.Count() > 0 && !isSaveAsDraft)
                    {
                        objData.ActionKey = false;
                        objData.ActionValue = "Please maintain Remarks for Project Activity : Contingency amount";
                        return Json(objData, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (dtActivity != null && dtActivity.Rows.Count > 0)
                        {
                            ActivityAmount = (from u in dtActivity.AsEnumerable()
                                              where Convert.ToString(u["Amount"]) != "" && Convert.ToString(u["Amount"]) != "0"
                                              select Convert.ToDouble(u["Amount"])).Sum();
                        }
                    }
                }
                totalAmount = SeamAmount + ActivityAmount;
                objData.TotalAmount = Convert.ToDouble(string.Format("{0:F2}", totalAmount));

                if (isSaveAsDraft)
                {
                    objData.ActionKey = true; objData.Key = true;
                }
                else
                {
                    objData.ActionKey = (Project != "" && totalAmount > 0) ? IsCJCinBudget(Project, totalAmount, dia) : false;
                    objData.ActionValue = !objData.ActionKey ? "Please contact to Buyer/Production head" : string.Empty;
                    if (objData.ActionKey)
                    {
                        string PoWhere = " BP LIKE '%" + objClsLoginInfo.UserName + "%' AND t_orno LIKE '%" + PurchaseOdrerNo + "%' AND pono='" + PurchaseOdrerLineNo + "'";
                        var PurOrder = db.SP_COMMON_GET_LN_APPROVED_PO(1, int.MaxValue, "", PoWhere).FirstOrDefault();

                        totalAmount = SeamAmount + ActivityAmount;
                        objData.Key = PurOrder != null ? (totalAmount <= PurOrder.BOAmount) : false;
                        objData.Value = PurOrder != null ? (!objData.Key ? "Total Claim amount should be less than '" + PurOrder.BOAmount + "'" : "") : "No records found";
                    }
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CalculateTotalReClaimAmount(int HeaderId, string CJCNo, string PurchaseOdrerNo, int PurchaseOdrerLineNo, bool IsFetchSeamClick, string Project = "", int dia = 0, bool isSaveAsDraft = false,string status = "")
        {
            ResponceMsgWithCJC objData = new ResponceMsgWithCJC();
            double? SeamAmount = 0;
            double? totalAmount = 0, ActivityAmount = 0;
            try
            {
                if (HeaderId > 0)
                {
                    if (IsFetchSeamClick)
                    {
                        DataTable dtSeam = new DataTable();
                        if (TempData["FetchSeamList"] != null)
                        {
                            string returned = clsImplementationEnum.AutoCJCStatus.Returned.GetStringValue();
                            string draft = clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue();
                            dtSeam = (DataTable)TempData["FetchSeamList"];
                            if (dtSeam != null && dtSeam.Rows.Count > 0)
                            {
                                if (status.Contains(returned))
                                {
                                    //((IsSetUpCleared = 1 and SetupCJCStatus <> 'Draft' and ISNULL(SetupCJCNo,'') = '') or(IsPWHTCleared = 1 AND WeldCJCStatus <> 'Draft' and ISNULL(WeldCJCNo, '') = ''))";
                                    SeamAmount = (from u in dtSeam.AsEnumerable()
                                                  where Convert.ToString(u["Amount"]) != "" && Convert.ToString(u["Amount"]) != "0" && (Convert.ToString(u["SetupCJCStatus"]) == "" || u["SetupCJCStatus"].ToString().Contains(returned) || Convert.ToString(u["WeldCJCStatus"]) == "" || u["WeldCJCStatus"].ToString().Contains(returned))
                                                  select Convert.ToDouble(u["Amount"])).Sum();
                                }
                                else
                                {
                                    SeamAmount = (from u in dtSeam.AsEnumerable()
                                                  where Convert.ToString(u["Amount"]) != "" && Convert.ToString(u["Amount"]) != "0" && (Convert.ToString(u["SetupCJCStatus"]) == "" || u["SetupCJCStatus"].ToString().Contains(draft) || Convert.ToString(u["WeldCJCStatus"]) == "" || u["WeldCJCStatus"].ToString().Contains(draft))
                                                  select Convert.ToDouble(u["Amount"])).Sum();
                                }

                            }
                        }
                    }
                    else
                    {
                        var lstSeam = (from u in db.CJC021
                                       where u.HeaderId == HeaderId && u.Amount > 0
                                       select u.Amount).ToList();
                        if (lstSeam != null && lstSeam.Count > 0)
                            SeamAmount = lstSeam.Sum();
                    }
                    string Contingency_amount = clsImplementationEnum.AutoCJCProjectActivity.Contingency_amount.GetStringValue();
                    var isNotMaintained = (from u in db.CJC022
                                           where u.HeaderId == HeaderId && u.Amount > 0
                                           && u.Activity == Contingency_amount && (u.Remarks == "" || u.Remarks == null)
                                           select u).ToList();
                    if (isNotMaintained.Count() > 0 && !isSaveAsDraft)
                    {
                        objData.ActionKey = false;
                        objData.ActionValue = "Please maintain Remarks for Project Activity : Contingency amount";
                        return Json(objData, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var lstActivity = (from u in db.CJC022
                                           where u.HeaderId == HeaderId && u.Amount > 0
                                           select u.Amount).ToList();
                        if (lstActivity != null && lstActivity.Count > 0)
                            ActivityAmount = lstActivity.Sum();
                    }
                }
                totalAmount = SeamAmount + ActivityAmount;
                objData.TotalAmount = Convert.ToDouble(string.Format("{0:F2}", totalAmount));
                if (isSaveAsDraft)
                { objData.ActionKey = true; objData.Key = true; }
                else
                {
                    objData.ActionKey = (Project != "" && totalAmount > 0) ? IsCJCinBudget(Project, Convert.ToDouble(totalAmount), dia) : false;
                    objData.ActionValue = !objData.ActionKey ? "Please contact to Buyer/Production head" : string.Empty;
                    if (objData.ActionKey)
                    {
                        string PoWhere = " BP LIKE '%" + objClsLoginInfo.UserName + "%' AND t_orno LIKE '%" + PurchaseOdrerNo + "%' AND pono='" + PurchaseOdrerLineNo + "'";
                        var PurOrder = db.SP_COMMON_GET_LN_APPROVED_PO(1, int.MaxValue, "", PoWhere).FirstOrDefault();

                        objData.Key = PurOrder != null ? (totalAmount <= PurOrder.BOAmount) : false;
                        objData.Value = objData.Value = PurOrder != null ? (!objData.Key ? "Total Claim amount should be less than '" + PurOrder.BOAmount + "'" : "") : "No records found";
                    }
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public double? CalculateSetUpNonOverlayRate(List<CJC011> objCJC011List, string SeamCategory, double Minthk, int Jobdia, string typethickness, string seamtype)
        {
            double? SetUpRate = 0;
            CJC011 obj = new CJC011();
            string id = db.Database.SqlQuery<string>("SELECT dbo.FN_CJC_FETCH_ID_FROM_MASTER(" + Minthk + ",'NonOverlay')").FirstOrDefault();
            string[] arr = !string.IsNullOrWhiteSpace(id) ? id.Split(',') : null;
            if (arr != null && arr.Count() > 0)
            {
                if (objCJC011List != null)
                    obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && arr.Contains(x.NonOverlayThickness.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                else
                    obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && arr.Contains(x.NonOverlayThickness.ToString()) && x.SeamType == seamtype).FirstOrDefault();

                if (obj != null)
                {
                    SetUpRate = obj?.Rate;
                }
            }

            return SetUpRate;// Json(SetUpRate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public double? CalculateSetUpNozzleSizeRate(List<CJC011> objCJC011List, string SeamCategory, string nozzlecategory, double nozzlesize, string typethickness, string seamtype, int Jobdia)
        {
            double? SetUpRate = 0;
            CJC011 obj = new CJC011();
            if (nozzlesize >= 0)
            {
                string id = db.Database.SqlQuery<string>("SELECT dbo.FN_CJC_FETCH_ID_FROM_MASTER(" + nozzlesize + ",'NozzleSize')").FirstOrDefault();
                string[] arr = !string.IsNullOrWhiteSpace(id) ? id.Split(',') : null;
                if (arr != null && arr.Count() > 0)
                {
                    if (objCJC011List != null)
                        obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && x.NozzleCategory == nozzlecategory && arr.Contains(x.NozzleSize.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                    else
                        obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && x.NozzleCategory == nozzlecategory && arr.Contains(x.NozzleSize.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                    if (obj != null)
                    {
                        SetUpRate = obj?.Rate;
                    }
                }
            }
            return SetUpRate;// Json(SetUpRate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public double? CalculateWeldNonOverlayRate(List<CJC011> objCJC011List, string SeamCategory, double Minthk, int Jobdia, string weldprocess, string jointtype, double nozzlesize, string nozzlecategory, bool IsPWHT, string seamtype)
        {
            //NonOverlay
            double? WeldRate = 0;
            CJC011 obj = new CJC011();
            if (IsPWHT)
            {
                //calculation depend on Seam  category- weld - min thickness

                string[] arr_Seam_WeldProc_MinThick = { clsImplementationEnum.AutoCJCSeamType.LW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.CW.GetStringValue() };
                string[] arr_Seam_WeldProc_MinThick2 = { clsImplementationEnum.AutoCJCSeamType.BU.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.HF.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PTCLW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PTCCW.GetStringValue() };
                if (arr_Seam_WeldProc_MinThick.Contains(seamtype) || arr_Seam_WeldProc_MinThick2.Contains(seamtype))
                {
                    if (!string.IsNullOrWhiteSpace(weldprocess))
                    {
                        string id = db.Database.SqlQuery<string>("SELECT dbo.FN_CJC_FETCH_ID_FROM_MASTER(" + Minthk + ",'NonOverlay')").FirstOrDefault();
                        string[] arr = !string.IsNullOrWhiteSpace(id) ? id.Split(',') : null;
                        if (arr != null && arr.Count() > 0)
                        {
                            if (arr_Seam_WeldProc_MinThick.Contains(seamtype))
                            {
                                string no = clsImplementationEnum.yesno.No.GetStringValue();
                                if (objCJC011List != null)
                                {
                                    if (objCJC011List.Any(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && arr.Contains(x.NonOverlayThickness.ToString())))//.Setup == no ? true : false)
                                    {
                                        obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.WeldingProcess == weldprocess && x.Dia == Jobdia && arr.Contains(x.NonOverlayThickness.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                                    }
                                }
                                else
                                {
                                    if (db.CJC011.Any(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && arr.Contains(x.NonOverlayThickness.ToString())))//.Setup == no ? true : false)
                                    {
                                        obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.WeldingProcess == weldprocess && x.Dia == Jobdia && arr.Contains(x.NonOverlayThickness.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                                    }
                                }
                            }

                            if (arr_Seam_WeldProc_MinThick2.Contains(seamtype))
                            {
                                if (objCJC011List != null)
                                {
                                    obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && x.WeldingProcess == weldprocess && arr.Contains(x.NonOverlayThickness.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                                }
                                else
                                {
                                    obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && x.WeldingProcess == weldprocess && arr.Contains(x.NonOverlayThickness.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                                }
                            }

                        }
                    }
                }
                if (seamtype == clsImplementationEnum.AutoCJCSeamType.HX.GetStringValue())
                {
                    //calculation depend on Seam category - weld proc
                    if (objCJC011List != null)
                        obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.WeldingProcess == weldprocess && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                    else
                        obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.WeldingProcess == weldprocess && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                }
                if (!string.IsNullOrWhiteSpace(jointtype))
                {
                    string[] arr_AW_PW_NP = { clsImplementationEnum.AutoCJCSeamType.AW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.NP.GetStringValue() };
                    string[] arr_jointtype_wp = { clsImplementationEnum.AutoCJCSeamType.CR.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.OW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PTCOW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PTCBW.GetStringValue() };
                    //calculation depend on Seam category -Joint type
                    if (arr_AW_PW_NP.Contains(seamtype))
                    {
                        if (objCJC011List != null)
                            obj = objCJC011List.Where(x => x.NozzleCategory == nozzlecategory && x.WeldType == jointtype && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                        else
                            obj = db.CJC011.Where(x => x.NozzleCategory == nozzlecategory && x.WeldType == jointtype && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                    }
                    else if (arr_jointtype_wp.Contains(seamtype))
                    {
                        //calculation depend on Seam category -Joint type - weld proc
                        if (objCJC011List != null)
                            obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.WeldType == jointtype && x.WeldingProcess == weldprocess && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                        else
                            obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.WeldType == jointtype && x.WeldingProcess == weldprocess && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                    }
                }

                //calculation depend on Seam category -nozzle category - weld proc
                if (seamtype == clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue())
                {
                    if (!string.IsNullOrWhiteSpace(nozzlecategory))
                    {
                        string id = db.Database.SqlQuery<string>("SELECT dbo.FN_CJC_FETCH_ID_FROM_MASTER(" + nozzlesize + ",'NozzleSize')").FirstOrDefault();
                        string idMinthk = db.Database.SqlQuery<string>("SELECT dbo.FN_CJC_FETCH_ID_FROM_MASTER(" + Minthk + ",'NonOverlay')").FirstOrDefault();
                        if (!string.IsNullOrWhiteSpace(id) && Convert.ToInt32(id) > 0)
                        {
                            int ID = Convert.ToInt32(id);
                            if (seamtype == clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue())
                            {
                                if (objCJC011List != null)
                                    obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.NozzleSize == ID && x.NozzleCategory == nozzlecategory && x.WeldingProcess == weldprocess && x.SeamType == seamtype && x.Dia == Jobdia).FirstOrDefault();
                                else
                                    obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.NozzleSize == ID && x.NozzleCategory == nozzlecategory && x.WeldingProcess == weldprocess && x.SeamType == seamtype && x.Dia == Jobdia).FirstOrDefault();
                            }
                        }
                    }
                }

                if (seamtype == clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue())
                {
                    if (objCJC011List != null)
                        obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                    else
                        obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                }

                WeldRate = obj?.Rate;
            }
            return WeldRate;// Json(WeldRate, JsonRequestBehavior.AllowGet);
        }

        //Check if calculations are applicable or not
        public bool IsCalculationApplicable(int headerid, int lineid, string cjcno, string qproject, string seamno, string SeamCategory, bool SetupRate, string SetUpCJCStatus, string WeldCJCStatus)
        {
            string yes = clsImplementationEnum.yesno.Yes.GetStringValue();
            bool isSetupYes = false;
            var objCJC011 = db.CJC011.Where(x => x.SeamCategory == SeamCategory).FirstOrDefault();
            if (objCJC011 != null)
                isSetupYes = objCJC011.Setup == yes ? true : false;

            bool flag = false;
            if (string.IsNullOrWhiteSpace(cjcno))
            {
                if (SetupRate)
                {
                    flag = isSetupYes;
                }
                else
                {
                    flag = true;
                }
            }
            else
            {
                if (headerid > 0)
                {
                    string HeaderCJCNo = "";
                    CJC020 objCJC020 = db.CJC020.Where(x => x.HeaderId == headerid).FirstOrDefault();
                    if (objCJC020 != null)
                        HeaderCJCNo = objCJC020.CJCNo;

                    if (cjcno.Trim().ToLower() == HeaderCJCNo.Trim().ToLower())
                    {
                        if (SetupRate)
                        {
                            flag = isSetupYes;
                        }
                        else
                        {
                            flag = true;
                        }
                    }
                }
            }

            return flag;
        }

        //Calculate Seam Amount
        public double claimSeamAmount(int rowid, string SeamCategory = "", int Seam_length = 0, double mm_mtr = 0, double Welding_rate = 0, double Factor = 0, double set_up_rate = 0, double Nozzle_size = 0)
        {
            double TotalAmount = 0;
            switch (SeamCategory)
            {
                #region R4 sheet
                case "SC002": //Long Seam
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC003": //Circ. Seam
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC006": //Overlay Long seam
                    TotalAmount = Seam_length * Welding_rate * Factor;
                    break;
                case "SC010": //Nozzle # Shell/D'end/Cone
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC032": //External / Internal
                    TotalAmount = mm_mtr * Welding_rate * Factor;
                    break;
                case "SC033": //Build up
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC034": //Hard Facing
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC035": //Refractory Anchor
                    TotalAmount = (mm_mtr * Welding_rate) * Factor;//(Note : mm_mtr == seam quantity)
                    break;
                case "SC036": //Hex Mesh
                    TotalAmount = (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC037": //Long Seam PTC
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC038": //Circ. Seam PTC
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC039": //PTC For Overlay
                    TotalAmount = Seam_length * Welding_rate * Factor;
                    break;
                case "SC040": //PTC Buttering
                    TotalAmount = Seam_length * Welding_rate * Factor;
                    break;
                    #endregion

                    #region R3 Sheet (Not in use)
                    //case "SC004": //External 
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC005": //Internal
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC007": //Overlay Circ. seam
                    //    TotalAmount = Seam_length * Welding_rate * Factor;
                    //    break;
                    //case "SC008": //Nozzle Long seam
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC009": //Nozzle Circ. Seam
                    //    TotalAmount = Nozzle_size * Welding_rate * Factor;
                    //    break;
                    //case "SC011": // Wrapper Plate # Shell assly
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC012": //Saddle # wrapper plate assly
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC013": //Groove grinding + WEEP hole/pad+ Leak path grinding
                    //    TotalAmount = Seam_length * Welding_rate * Factor;
                    //    break;
                    //case "SC014": //Liner Setup & Welding
                    //    TotalAmount = Seam_length * Welding_rate * Factor;
                    //    break;
                    //case "SC015": //Weep tup set up & Welding
                    //    TotalAmount = Seam_length * Welding_rate * Factor;
                    //    break;
                    //case "SC018": //Other Externals with fillet weld
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC017": // Other Externals with groove weld
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC029": // Externals platform / pipe supprt / insulation cleats with groove + concave
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC030":// Externals platform / pipe supprt / insulation cleats with Fillet + concave
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC019": // Externals platform / pipe supprt / insulation cleats with groove weld
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC020": // Externals platform / pipe supprt / insulation cleats with Fillet weld
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC021": // Internals with groove weld
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC031": // Internals with fillet weld 
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC022": // Internals with fillet weld + Concave 
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC023": // Internals with groove weld + concave 
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    #endregion
            }
            if (TotalAmount > 0)
            {
                TotalAmount = Convert.ToDouble(string.Format("{0:F2}", TotalAmount));
            }
            if (rowid > 0)
            {
                //update in DataTable
                DataTable dt = (DataTable)TempData["FetchSeamList"];
                if (dt != null)
                {
                    DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                  where Convert.ToInt32(u["ROW_NO"]) == rowid
                                  select u).FirstOrDefault();

                    if (dr != null)
                    {
                        dr["Amount"] = TotalAmount;
                        dt.AcceptChanges();
                    }
                }
                RetenTempDataValue();
            }
            return TotalAmount;
        }

        #endregion

        #region RuntimeCalculations

        //Nozzle Size
        public double? CalcultaionOfNozzleSize(int rowid, string SeamCategory, string SeamType, int SeamLength)
        {
            if (arrSeamTypeNW.Contains(SeamType))
            {
                if (SeamLength > 0)
                {

                    var NozzleSizeamt = (Convert.ToDouble(SeamLength) / pi) / 25.4;
                    var NozzleSize = Double.Parse(NozzleSizeamt.ToString("0.0000"));
                    if (rowid > 0)
                    {
                        DataTable dt = (DataTable)TempData["FetchSeamList"];
                        if (dt != null)
                        {
                            DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                          where Convert.ToInt32(u["ROW_NO"]) == rowid
                                          select u).FirstOrDefault();

                            if (dr != null)
                            {
                                //changes the Column Value
                                dr["NozzleSize"] = NozzleSize;
                                dt.AcceptChanges();
                            }

                            RetenTempDataValue();
                        }
                    }
                    return NozzleSize;
                }
                else { return null; }
            }
            else { return null; }
        }

        //Unit /Measure Value
        public double? CalculateUnit(int rowid, string SeamCategory, int SeamLength, double minthickness, string SeamType)
        {
            string[] arrSeamTypeMinThk ={ clsImplementationEnum.AutoCJCSeamType.LW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.CW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.AW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.NP.GetStringValue()
                                         ,clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.BU.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.HF.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PTCLW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PTCCW.GetStringValue()};

            if (SeamLength > 0 && !string.IsNullOrWhiteSpace(SeamCategory))
            {

                if (arrSeamTypeMinThk.Contains(SeamType))
                {
                    double unit = (Convert.ToDouble(SeamLength) / 1000) * minthickness;
                    if (rowid > 0)
                    {
                        DataTable dt = (DataTable)TempData["FetchSeamList"];
                        if (dt != null)
                        {
                            DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                          where Convert.ToInt32(u["ROW_NO"]) == rowid
                                          select u).FirstOrDefault();

                            if (dr != null)
                            {
                                //changes the Column Value
                                dr["Unit"] = unit;
                                dt.AcceptChanges();
                            }
                        }
                        RetenTempDataValue();
                    }
                    return unit;
                }
                else if (SeamType == clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue())
                {
                    double unit = 0;
                    if (rowid > 0)
                    {
                        DataTable dt = (DataTable)TempData["FetchSeamList"];
                        if (dt != null)
                        {
                            DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                          where Convert.ToInt32(u["ROW_NO"]) == rowid
                                          select u).FirstOrDefault();

                            if (dr != null)
                            {
                                unit = Convert.ToDouble(dr["SeamQuantity"]);
                                //changes the Column Value
                                dr["Unit"] = dr["SeamQuantity"];
                                dt.AcceptChanges();
                            }
                        }
                        RetenTempDataValue();
                    }
                    return unit;
                }
                else if (SeamType == clsImplementationEnum.AutoCJCSeamType.HX.GetStringValue() || SeamType == clsImplementationEnum.AutoCJCSeamType.CR.GetStringValue() || SeamType == clsImplementationEnum.AutoCJCSeamType.OW.GetStringValue() ||
                    SeamType == clsImplementationEnum.AutoCJCSeamType.PTCOW.GetStringValue() || SeamType == clsImplementationEnum.AutoCJCSeamType.PTCBW.GetStringValue())
                {
                    double unit = (Convert.ToDouble(SeamLength) / 1000);
                    if (rowid > 0)
                    {
                        DataTable dt = (DataTable)TempData["FetchSeamList"];
                        if (dt != null)
                        {
                            DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                          where Convert.ToInt32(u["ROW_NO"]) == rowid
                                          select u).FirstOrDefault();

                            if (dr != null)
                            {
                                //changes the Column Value
                                dr["Unit"] = unit;
                                dt.AcceptChanges();
                            }
                        }
                        RetenTempDataValue();
                    }
                    return unit;
                }
                else { return null; }
            }
            else { return null; }
        }

        //runtime check calcuation is applicable or not
        public string SetUpRateCalculation(List<CJC011> objCJC011List, int rowid, bool isSetUpCleared, string SetupCJCNo, string HeaderCJCNo, string ActionType, string SetUpCJCStatus, string SeamCategory, double Minthk, int Jobdia, string typethickness, string nozzlecategory, double nozzlesize, string seamtype)
        {
            string result = "";
            string yes = clsImplementationEnum.yesno.Yes.GetStringValue();
            bool isSetupYes = false;


            CJC011 objCJC011 = null;
            if (objCJC011List != null)
                objCJC011 = objCJC011List.Where(x => x.SeamCategory == SeamCategory).FirstOrDefault();
            else
                objCJC011 = db.CJC011.Where(x => x.SeamCategory == SeamCategory).FirstOrDefault();

            if (objCJC011 != null)
                isSetupYes = objCJC011.Setup == yes ? true : false;

            bool IsCalculateSetUpRate = false;
            string returned = clsImplementationEnum.AutoCJCStatus.Returned.GetStringValue();
            string rejected = clsImplementationEnum.AutoCJCStatus.Rejected.GetStringValue();
            if (isSetUpCleared && isSetupYes)
            {
                if (string.IsNullOrWhiteSpace(SetupCJCNo))
                {
                    IsCalculateSetUpRate = true;
                }
                else
                {
                    if (ActionType == ActionEdit)
                    {
                        if (SetupCJCNo.Trim().ToLower() == HeaderCJCNo.Trim().ToLower())
                        {
                            IsCalculateSetUpRate = true;
                        }
                    }
                }
            }

            if (IsCalculateSetUpRate)
            {
                if (arrSetUpNonOverlayRate.Contains(SeamCategory) || arrSetUpMinThk.Contains(seamtype))
                {
                    var setuprate = CalculateSetUpNonOverlayRate(objCJC011List, SeamCategory, Minthk, Jobdia, typethickness, seamtype);
                    if (rowid > 0)
                    {
                        DataTable dt = (DataTable)TempData["FetchSeamList"];
                        if (dt != null)
                        {
                            DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                          where Convert.ToInt32(u["ROW_NO"]) == rowid
                                          select u).FirstOrDefault();

                            if (dr != null)
                            {
                                dr["SetupRate"] = setuprate;
                                dt.AcceptChanges();
                            }
                        }
                        RetenTempDataValue();
                    }
                    result = setuprate + "";
                }
                else if (arrSetUpNozzleSizeRate.Contains(SeamCategory))
                {
                    var setuprate = CalculateSetUpNozzleSizeRate(objCJC011List, SeamCategory, nozzlecategory, nozzlesize, typethickness, seamtype, Jobdia);
                    if (rowid > 0)
                    {
                        DataTable dt = (DataTable)TempData["FetchSeamList"];
                        if (dt != null)
                        {
                            DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                          where Convert.ToInt32(u["ROW_NO"]) == rowid
                                          select u).FirstOrDefault();

                            if (dr != null)
                            {
                                dr["SetupRate"] = setuprate;
                                dt.AcceptChanges();
                            }
                        }
                        RetenTempDataValue();
                    }
                    result = setuprate + "";
                }
            }

            return result;
        }

        public double? WeldRateCalculation(List<CJC011> objCJC011List, int rowid, bool isWeldCleared, string WeldCJCNo, string HeaderCJCNo, string ActionType, string WeldCJCStatus, string SeamCategory, double Minthk, int Jobdia, string weldprocess, string jointtype, string nozzlecategory, double nozzlesize, string seamtype)
        {
            double? result = null;
            bool IsCalculateWeldRate = false;
            if (isWeldCleared)
            {
                if (string.IsNullOrWhiteSpace(WeldCJCNo))
                {
                    IsCalculateWeldRate = true;
                }
                else
                {
                    if (ActionType == ActionEdit)
                    {
                        if (WeldCJCNo.Trim().ToLower() == HeaderCJCNo.Trim().ToLower())
                        {
                            IsCalculateWeldRate = true;
                        }
                    }
                }
            }

            if (IsCalculateWeldRate)
            {
                var weldingrate = CalculateWeldNonOverlayRate(objCJC011List, SeamCategory, Minthk, Jobdia, weldprocess, jointtype, nozzlesize, nozzlecategory, isWeldCleared, seamtype);
                if (rowid > 0)
                {
                    DataTable dt = (DataTable)TempData["FetchSeamList"];
                    if (dt != null)
                    {
                        DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                      where Convert.ToInt32(u["ROW_NO"]) == rowid
                                      select u).FirstOrDefault();

                        if (dr != null)
                        {
                            dr["WeldingRate"] = weldingrate;
                            dt.AcceptChanges();
                        }
                    }
                    RetenTempDataValue();
                }
                result = weldingrate;
            }
            return result;
        }

        public DataTable CalculateAndUpdateDataTable(DataTable dtSeams, string HeaderCJCNo, string ActionType, int Jobdia)
        {
            if (dtSeams != null && dtSeams.Rows.Count > 0)
            {
                var objCJC011List = db.CJC011.ToList();
                foreach (DataRow row in dtSeams.Rows)
                {
                    int RowId = Convert.ToInt32(row["ROW_NO"]);
                    string SeamCategory = Convert.ToString(row["SeamCategory"]);
                    string SeamType = Convert.ToString(row["SeamType"]);
                    int SeamLength = Convert.ToInt32(row["SeamLength"]);
                    string SeamJointType = Convert.ToString(row["SeamJointType"]);
                    double MinThickness = Convert.ToDouble(row["MinThickness"]);
                    string NozzleCategory = Convert.ToString(row["NozzleCategory"]);
                    string NozzleSize = Convert.ToString(row["NozzleSize"]);

                    bool IsSeamFetchedBySeamType = Convert.ToBoolean(row["IsSeamFetchedBySeamType"]);
                    bool IsSetUpCleared = Convert.ToBoolean(row["IsSetUpCleared"]);
                    bool IsPWHTCleared = Convert.ToBoolean(row["IsPWHTCleared"]);

                    string SetupCJCNo = Convert.ToString(row["SetupCJCNo"]);
                    string WeldCJCNo = Convert.ToString(row["WeldCJCNo"]);

                    string SetUpCJCStatus = Convert.ToString(row["SetUpCJCStatus"]);
                    string WeldCJCStatus = Convert.ToString(row["WeldCJCStatus"]);

                    string SetupRate = Convert.ToString(row["SetupRate"]);
                    string WeldingRate = Convert.ToString(row["WeldingRate"]);

                    string Amount = Convert.ToString(row["Amount"]);
                    string Factor = Convert.ToString(row["Factor"]);

                    string weldingProcess = Convert.ToString(row["WeldingProcess"]);
                    #region Nozzle Size 

                    if (IsSeamFetchedBySeamType)
                    {
                        row["NozzleSize"] = CalcultaionOfNozzleSize(0, SeamCategory, SeamType, SeamLength);
                    }

                    #endregion

                    #region Measured Value/Unit

                    if (IsSeamFetchedBySeamType)
                    {
                        if (SeamType == clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue())
                        {
                            double Part1Thickness = Convert.ToDouble(row["Part1Thickness"]);
                            double Part2Thickness = string.IsNullOrWhiteSpace(Convert.ToString(row["Part2Thickness"])) ? 0 : Convert.ToDouble(row["Part2Thickness"]);
                            double Part3Thickness = string.IsNullOrWhiteSpace(Convert.ToString(row["Part3Thickness"])) ? 0 : Convert.ToDouble(row["Part3Thickness"]);
                            double[] arrThickness = { Part1Thickness, Part2Thickness, Part3Thickness };
                            double maxVal = arrThickness.Max();
                            row["Unit"] = CalculateUnit(0, SeamCategory, SeamLength, maxVal, SeamType);
                        }
                        else
                        {
                            row["Unit"] = CalculateUnit(0, SeamCategory, SeamLength, MinThickness, SeamType);
                        }
                    }

                    #endregion

                    #region Setup Rate

                    if (IsSeamFetchedBySeamType && (string.IsNullOrWhiteSpace(SetupRate)))
                    {
                        row["SetupRate"] = SetUpRateCalculation(objCJC011List, 0, IsSetUpCleared, SetupCJCNo, HeaderCJCNo, ActionType, SetUpCJCStatus, SeamCategory, MinThickness, Jobdia, "", NozzleCategory, (string.IsNullOrWhiteSpace(NozzleSize) ? 0 : Convert.ToDouble(NozzleSize)), SeamType);
                    }

                    #endregion

                    #region Welding Rate

                    if (IsSeamFetchedBySeamType && (string.IsNullOrWhiteSpace(WeldingRate)))
                    {
                        row["WeldingRate"] = WeldRateCalculation(objCJC011List, 0, IsPWHTCleared, WeldCJCNo, HeaderCJCNo, ActionType, WeldCJCStatus, SeamCategory, MinThickness, Jobdia, weldingProcess, SeamJointType, NozzleCategory, (string.IsNullOrWhiteSpace(NozzleSize) ? 0 : Convert.ToDouble(NozzleSize)), SeamType);
                    }

                    #endregion

                    #region Amount 

                    if (IsSeamFetchedBySeamType && (string.IsNullOrWhiteSpace(Amount) || Amount == "0"))
                    {
                        row["Amount"] = claimSeamAmount(0, SeamCategory, SeamLength, (!string.IsNullOrWhiteSpace(Convert.ToString(row["Unit"])) ? Convert.ToDouble(row["Unit"]) : 0),
                                                                                     (!string.IsNullOrWhiteSpace(Convert.ToString(row["WeldingRate"])) ? Convert.ToDouble(row["WeldingRate"]) : 0),
                                                                                     (!string.IsNullOrWhiteSpace(Factor) ? Convert.ToDouble(Factor) : 0),
                                                                                     (!string.IsNullOrWhiteSpace(Convert.ToString(row["SetupRate"])) ? Convert.ToDouble(row["SetupRate"]) : 0),
                                                                                     (!string.IsNullOrWhiteSpace(Convert.ToString(row["NozzleSize"])) ? Convert.ToDouble(row["NozzleSize"]) : 0));
                    }

                    #endregion

                }
                dtSeams.AcceptChanges();
            }
            return dtSeams;
        }

        public bool IsCJCinBudget(string project, double claimamount, int dia)
        {
            bool flg = true;
            var objCJC016 = db.CJC016.Where(a => a.Project == project && a.ProductType == dia).OrderByDescending(x => x.EditedOn).FirstOrDefault();
            double? EstimatedValue = objCJC016?.EstimatedValue;
            double? claimamount1 = db.CJC020.Where(a => a.Project == project && (a.CJCNo != "" || a.CJCNo != null)).Sum(x => x.TotalCJCAmount);
            double? totalClaimAmt = claimamount1 + claimamount;
            if (EstimatedValue > 0 && totalClaimAmt > 0)
            {
                if (objCJC016.LockAt80Perc == true)
                {
                    int percentage = (int)Math.Round(Convert.ToDouble((100 * totalClaimAmt)) / Convert.ToDouble(EstimatedValue));
                    if (percentage > 80)
                    {
                        flg = false;
                    }
                }

                if (totalClaimAmt > EstimatedValue)
                { flg = false; }
            }
            return flg;
        }
        #endregion

        public class ResponceMsgWithCJC : clsHelper.ResponseMsgWithStatus
        {
            public string SeamCategoryDesc;
            public string NozzleCategoryDesc;
            public string WeldingProcessDesc;
            public string CriticalSeamDesc;
            public double? TotalAmount;
        }
    }
}