﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace AutoCJC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();

            #region Date Formata Set
            var culturename = System.Configuration.ConfigurationManager.AppSettings["Culture"] != null ? System.Configuration.ConfigurationManager.AppSettings["Culture"].ToString() : "en-GB";
            var dateformat = System.Configuration.ConfigurationManager.AppSettings["DateFormat"] != null ? System.Configuration.ConfigurationManager.AppSettings["DateFormat"].ToString() : "dd/MM/yyyy";
            CultureInfo cInf = new CultureInfo(culturename, false);
            cInf.DateTimeFormat.DateSeparator = "/";
            cInf.DateTimeFormat.ShortDatePattern = dateformat;
            cInf.DateTimeFormat.LongDatePattern = dateformat + " hh:mm:ss tt";

            System.Threading.Thread.CurrentThread.CurrentCulture = cInf;
            System.Threading.Thread.CurrentThread.CurrentUICulture = cInf;
            #endregion
        }
    }
}
