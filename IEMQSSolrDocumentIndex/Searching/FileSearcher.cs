﻿using IEMQSSolrDocumentIndex.DAL;
using IEMQSSolrDocumentIndex.Indexing;
using IEMQSSolrDocumentIndex.Internals;
using SolrNet.Commands.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSSolrDocumentIndex.Searching
{
    public class FileSearcher
    {
        #region Members

        private string solrUrl;

        #endregion

        #region Constructors

        public FileSearcher(string solrUrl)
        {
            this.solrUrl = solrUrl;
        }

        #endregion

        #region Methods

        public SearchResults Search(string query, int pageNumber, int resultsPerPage)
        {
            var solrWorker = SolrOperationsCache<DocumentModel>.GetSolrOperations(this.solrUrl);
            var p = new Dictionary<string, string>();
            p.Add("wt", "xml");
            QueryOptions options = new QueryOptions
            {
                Rows = resultsPerPage,
                Start = (pageNumber - 1) * resultsPerPage,
                ExtraParams = p
            };

            var results = solrWorker.Query(query, options);

            //DocumentIndexer repository = new DocumentIndexer(solrUrl);

            IEnumerable<string> documentMappingIds = results.Select(tf => tf.ObjectId);

            var searchResults = new SearchResults();
            //searchResults.Results = repository.Getfiles(fileIDs);
            searchResults.TotalResults = results.NumFound;
            searchResults.QueryTime = results.Header.QTime;

            return searchResults;
        }

        #endregion
    }
}
