﻿using IEMQSSolrDocumentIndex.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMQS.DESCore.Data;

namespace IEMQSSolrDocumentIndex.Searching
{
    public class SearchResults
    {
        internal SearchResults() { }

        public IEnumerable<DES060> Results { get; internal set; }
        public int QueryTime { get; internal set; }
        public int TotalResults { get; internal set; }
    }
}
