﻿//using Microsoft.Practices.ServiceLocation;
using CommonServiceLocator;
using SolrNet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IEMQSSolrDocumentIndex.Internals
{
    internal static class SolrOperationsCache<T>
        where T : new()
    {
        private static ISolrOperations<T> solrOperations;

        public static ISolrOperations<T> GetSolrOperations(string solrUrl)
        {
            if (solrOperations == null)
            {
                Startup.Init<T>(solrUrl);
                solrOperations = ServiceLocator.Current.GetInstance<ISolrOperations<T>>();                
            }

            return solrOperations;
        }

    }
}
