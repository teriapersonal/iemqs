﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMQS.DESCore.Data;
using IEMQSSolrDocumentIndex.DAL;
using IEMQSSolrDocumentIndex.Internals;

namespace IEMQSSolrDocumentIndex.Indexing
{
    public class DocumentIndexer
    {
        #region Members

        private string solrUrl;


        #endregion
        public DocumentIndexer(string solrUrl)
        {
            this.solrUrl = solrUrl;
        }
        public void IndexFile(List<DocumentModel> documentModels)
        {
            var solrWorker = SolrOperationsCache<DocumentModel>.GetSolrOperations(solrUrl);
            solrWorker.AddRange(documentModels);
            solrWorker.Commit();
        }

        public void RemoveFiles(List<string> listIds)
        {
            //var listId = documentModels.Select(i => i.ObjectId).ToList();            
            var solrWorker = SolrOperationsCache<DocumentModel>.GetSolrOperations(solrUrl);
            solrWorker.Delete(listIds);
            solrWorker.Commit();
        }
    }
}
