﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mail;
using System.Text;

namespace DocBulkUpload
{
    public class EmailService
    {
        
        public static bool SendEmail_old(EmailParamModel model)
        {
            try
            {
                string SMTP_SERVER = "Select ConfigName,ConfigValues,UserId,UserPassword from DES104 WHERE ConfigName='SMTP_SERVER'";
                SqlDataAdapter daSMTP_SERVER = new SqlDataAdapter(SQLfunctions.GetCommandFromSQL(SMTP_SERVER, SQLfunctions.GetConnection()));
                DataTable dtSMTP_SERVER = new DataTable();
                daSMTP_SERVER.Fill(dtSMTP_SERVER);

                string SMTP_PORT = "Select ConfigName,ConfigValues,UserId,UserPassword from DES104 WHERE ConfigName='SMTP_PORT'";
                SqlDataAdapter daSMTP_PORT = new SqlDataAdapter(SQLfunctions.GetCommandFromSQL(SMTP_PORT, SQLfunctions.GetConnection()));
                DataTable dtSMTP_PORT = new DataTable();
                daSMTP_PORT.Fill(dtSMTP_PORT);

                string CC_EMAIL = "Select ConfigName,ConfigValues,UserId,UserPassword from DES104 WHERE ConfigName='CC_EMAIL'";
                SqlDataAdapter daCC_EMAIL = new SqlDataAdapter(SQLfunctions.GetCommandFromSQL(CC_EMAIL, SQLfunctions.GetConnection()));
                DataTable dtCC_EMAIL = new DataTable();
                daCC_EMAIL.Fill(dtCC_EMAIL);

                string BCC_EMAIL = "Select ConfigName,ConfigValues,UserId,UserPassword from DES104 WHERE ConfigName='BCC_EMAIL'";
                SqlDataAdapter daBCC_EMAIL = new SqlDataAdapter(SQLfunctions.GetCommandFromSQL(BCC_EMAIL, SQLfunctions.GetConnection()));
                DataTable dtBCC_EMAIL = new DataTable();
                daBCC_EMAIL.Fill(dtBCC_EMAIL);

                if (dtSMTP_SERVER != null && dtSMTP_SERVER.Rows.Count > 0 && dtSMTP_PORT != null && dtSMTP_PORT.Rows.Count > 0)
                {
                    string SMTPServer = Convert.ToString(dtSMTP_SERVER.Rows[0]["ConfigValues"]);
                    string SMTPUsername = Convert.ToString(dtSMTP_SERVER.Rows[0]["UserId"]);
                    string SMTPPassword = Convert.ToString(dtSMTP_SERVER.Rows[0]["UserPassword"]);
                    string SMTPPort = Convert.ToString(dtSMTP_PORT.Rows[0]["ConfigValues"]);


                    MailMessage mail = new MailMessage();
                    SmtpClient client = new SmtpClient(SMTPServer);
                    if (string.IsNullOrEmpty(model.FromEmailAddress))
                        model.FromEmailAddress = "IEMQS@larsentoubro.com";
                    mail.From = new MailAddress(model.FromEmailAddress);

                    if (model.ToEmailAddress != null)
                    {
                        string[] ToEmailId = model.ToEmailAddress.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (string ToEmail in ToEmailId)
                        {
                            mail.To.Add(new MailAddress(ToEmail));
                        }
                    }
                    if (model.Cc != null)
                    {
                        string[] CCEmailId = model.Cc.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (string CCEmail in CCEmailId)
                        {
                            mail.CC.Add(new MailAddress(CCEmail));
                        }
                    }
                    if (dtCC_EMAIL != null && dtCC_EMAIL.Rows.Count > 0)
                    {
                        string CCemail = Convert.ToString(dtCC_EMAIL.Rows[0]["ConfigValues"]);
                        if (!string.IsNullOrEmpty(CCemail))
                        {
                            string[] CCEmailId = CCemail.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string CCEmail in CCEmailId)
                            {
                                mail.CC.Add(new MailAddress(CCEmail));
                            }
                        }
                    }
                    if (model.Bcc != null)
                    {
                        string[] BccEmailId = model.Bcc.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (string BccEmail in BccEmailId)
                        {
                            mail.Bcc.Add(new MailAddress(BccEmail));
                        }
                    }
                    if (dtBCC_EMAIL != null && dtBCC_EMAIL.Rows.Count > 0)
                    {
                        string BCCemail = Convert.ToString(dtBCC_EMAIL.Rows[0]["ConfigValues"]);
                        if (!string.IsNullOrEmpty(BCCemail))
                        {
                            string[] BCCEmailId = BCCemail.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string BCCEmail in BCCEmailId)
                            {
                                mail.Bcc.Add(new MailAddress(BCCEmail));
                            }
                        }
                    }
                    mail.Subject = model.Subject;
                    mail.IsBodyHtml = true;
                    mail.Body = model.Body;

                    client.Port = Convert.ToInt32(SMTPPort);
                    client.Credentials = new System.Net.NetworkCredential(SMTPUsername, SMTPPassword);
                    client.EnableSsl = false;
                    client.Send(mail);

                    mail.Dispose();
                    client.Dispose();
                }
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        #region Methods        
        public static bool SendEmail(EmailParamModel model)
        {
            MailMessage mail = new MailMessage();
            try
            {
                string SMTP_SERVER = "Select ConfigName,ConfigValues,UserId,UserPassword from DES104 WHERE ConfigName='SMTP_SERVER'";
                SqlDataAdapter daSMTP_SERVER = new SqlDataAdapter(SQLfunctions.GetCommandFromSQL(SMTP_SERVER, SQLfunctions.GetConnection()));
                DataTable dtSMTP_SERVER = new DataTable();
                daSMTP_SERVER.Fill(dtSMTP_SERVER);


                string CC_EMAIL = "Select ConfigName,ConfigValues,UserId,UserPassword from DES104 WHERE ConfigName='CC_EMAIL'";
                SqlDataAdapter daCC_EMAIL = new SqlDataAdapter(SQLfunctions.GetCommandFromSQL(CC_EMAIL, SQLfunctions.GetConnection()));
                DataTable dtCC_EMAIL = new DataTable();
                daCC_EMAIL.Fill(dtCC_EMAIL);

                string BCC_EMAIL = "Select ConfigName,ConfigValues,UserId,UserPassword from DES104 WHERE ConfigName='BCC_EMAIL'";
                SqlDataAdapter daBCC_EMAIL = new SqlDataAdapter(SQLfunctions.GetCommandFromSQL(BCC_EMAIL, SQLfunctions.GetConnection()));
                DataTable dtBCC_EMAIL = new DataTable();
                daBCC_EMAIL.Fill(dtBCC_EMAIL);



                if (dtSMTP_SERVER != null && dtSMTP_SERVER.Rows.Count > 0)
                {
                    string SMTPServer = Convert.ToString(dtSMTP_SERVER.Rows[0]["ConfigValues"]);

                    SmtpClient sc = new SmtpClient(SMTPServer);

                    
                    //FROM
                    if (string.IsNullOrEmpty(model.FromEmailAddress))
                        model.FromEmailAddress = "IEMQS@larsentoubro.com";
                    mail.From = new MailAddress(model.FromEmailAddress);
                    //mm.ReplyTo = new MailAddress(tomail);


                    if (model.ToEmailAddress != null)
                    {
                        string[] ToEmailId = model.ToEmailAddress.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (string ToEmail in ToEmailId)
                        {
                            mail.To.Add(new MailAddress(ToEmail));
                        }
                    }
                    if (model.Cc != null)
                    {
                        string[] CCEmailId = model.Cc.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (string CCEmail in CCEmailId)
                        {
                            mail.CC.Add(new MailAddress(CCEmail));
                        }
                    }
                    if (dtCC_EMAIL != null && dtCC_EMAIL.Rows.Count > 0)
                    {
                        string CCemail = Convert.ToString(dtCC_EMAIL.Rows[0]["ConfigValues"]);
                        if (!string.IsNullOrEmpty(CCemail))
                        {
                            string[] CCEmailId = CCemail.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string CCEmail in CCEmailId)
                            {
                                mail.CC.Add(new MailAddress(CCEmail));
                            }
                        }
                    }
                    if (model.Bcc != null)
                    {
                        string[] BccEmailId = model.Bcc.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);

                        foreach (string BccEmail in BccEmailId)
                        {
                            mail.Bcc.Add(new MailAddress(BccEmail));
                        }
                    }
                    if (dtBCC_EMAIL != null && dtBCC_EMAIL.Rows.Count > 0)
                    {
                        string BCCemail = Convert.ToString(dtBCC_EMAIL.Rows[0]["ConfigValues"]);
                        if (!string.IsNullOrEmpty(BCCemail))
                        {
                            string[] BCCEmailId = BCCemail.Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);

                            foreach (string BCCEmail in BCCEmailId)
                            {
                                mail.Bcc.Add(new MailAddress(BCCEmail));
                            }
                        }
                    }


                    mail.Body = model.Body;
                    mail.Subject = model.Subject;

                    mail.IsBodyHtml = true;
                    mail.Priority = MailPriority.Normal;
                    sc.Send(mail);

                    mail.To.Clear();
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
            finally
            {
                mail.Dispose();
            }
        }

        #endregion
    }
    public class EmailParamModel
    {
        public string FromEmailAddress { get; set; }
        public string ToEmailAddress { get; set; }
        public string Cc { get; set; }
        public string Bcc { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public string ResetCode { get; set; }
    }
}
