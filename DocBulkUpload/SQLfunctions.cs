﻿using System;
using System.Data;
using System.Configuration;
using System.Web;

using System.IO;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Data.SqlTypes;

/// <summary>
/// Summary description for sqlHelper
/// </summary>
public class SQLfunctions
{

    public static string GetConnectionString
    {
        get
        {
            return ConfigurationSettings.AppSettings["ConnStr"].ToString();
        }
    }


    /// <summary>
    /// Returns the standard application connection.
    /// </summary>
    /// <returns>Connection</returns>
    public static SqlConnection GetConnection()
    {
#if NOTWEB
                return new System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings["DefaultDatabase"]);;    
#else

        return new System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings["ConnStr"].ToString());
#endif
    }
    /// <summary>
    /// Returns the standard application connection.
    /// </summary>
    /// <returns>Connection</returns>
    public static SqlConnection GetConnectionInsert()
    {
#if NOTWEB
                return new System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings["DefaultDatabase"]);;    
#else

        return new System.Data.SqlClient.SqlConnection(ConfigurationSettings.AppSettings["ConnStr"].ToString());
#endif
    }
    /// <summary>
    /// Executes the SQL string against the given connection.
    /// </summary>
    /// <param name="sql">SQL Command</param>
    /// <param name="connection">Connection</param>
    public static void ExecuteSqlCommand(string sql, SqlConnection connection)
    {
        SqlCommand cmd = new SqlCommand(sql, connection);
        cmd.CommandType = CommandType.Text;
        ExecuteSqlCommand(cmd);
    }

    /// <summary>
    /// Opens the database connection if it is not open already.
    /// Waits for any processing on the connection to complete
    /// before doing this.
    /// </summary>
    /// <param name="connection">The connection to ensure is open</param>
    /// <returns>True if the connection was previously closed.</returns>
    public static bool OpenConnection(SqlConnection connection)
    {
        bool connectionopened = false;

        // Wait for the current connection to stop processing.
        while (connection.State == ConnectionState.Connecting ||
            connection.State == ConnectionState.Executing ||
            connection.State == ConnectionState.Fetching)
        {
            Thread.Sleep(0);
        }

        // Open the connection if it is not currently.
        if (connection.State == ConnectionState.Closed)
        {
            connection.Open();
            connectionopened = true;
        }

        return connectionopened;
    }

    /// <summary>
    /// Executes the SQL command checking the connection status beforehand.
    /// If the connection needs to be opened the method will close it before
    /// returning.
    /// </summary>
    /// <param name="cmd">A fully setup SQLCommand</param>
    public static void ExecuteSqlCommand(SqlCommand cmd)
    {
        bool connectionopened = OpenConnection(cmd.Connection);

        // Execute the command allowing other threads
        // to execute during the time it takes a response
        // to be returned.
        try
        {
            IAsyncResult result = cmd.BeginExecuteNonQuery();
            while (!result.IsCompleted)
            {
                Thread.Sleep(0);
            }
            cmd.EndExecuteNonQuery(result);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            // Close the connection if it was not already open.
            if (connectionopened == true)
            {
                cmd.Connection.Close();
            }
        }
    }

    public static System.Data.SqlClient.SqlCommand GetCommandFromSQL(string sql)
    {
        System.Data.SqlClient.SqlCommand cmd =
            new System.Data.SqlClient.SqlCommand(sql, GetConnection());
        cmd.CommandType = System.Data.CommandType.Text;
        return cmd;
    }

    public static System.Data.SqlClient.SqlCommand GetCommandFromSQL(string sql, SqlConnection conn)
    {
        System.Data.SqlClient.SqlCommand cmd =
            new System.Data.SqlClient.SqlCommand(sql, conn);
        cmd.CommandType = System.Data.CommandType.Text;
        return cmd;
    }

    /// <summary>
    /// Passed an SQL command with all it's parameters set. Adds
    /// a parameter to capture the return value as an Integer. Executes
    /// the command and returns the Integer return value.
    /// </summary>
    /// <param name="cmd">SQLCommand already setup on the database.</param>
    /// <returns>Integer return value.</returns>
    public static Int64? ExecuteSqlCommandReturnInt(string SQL)
    {
        object obj = SQLfunctions.ExecuteScalar(SQL);
        return obj is DBNull || obj == null ? 0 : (Int64?)obj;
    }

    /// <summary>
    /// Passed an SQL command with all it's parameters set. Adds
    /// a parameter to capture the return value as a String. Executes
    /// the command and returns the String return value.
    /// </summary>
    /// <param name="cmd">SQLCommand already setup on the database.</param>
    /// <returns>String return value.</returns>
    public static string ExecuteSqlCommandReturnString(string SQL)
    {
        object obj = SQLfunctions.ExecuteScalar(SQL);
        return obj is DBNull || obj == null ? null : obj.ToString();
    }

    /// <summary>
    /// Execute Scalar the SQL command checking the connection status beforehand.
    /// If the connection needs to be opened the method will close it before
    /// returning.
    /// </summary>
    /// <param name="cmd">A fully setup SQLCommand</param>
    public static object ExecuteScalar(string SQL)
    {
        SqlCommand cmd = new SqlCommand(SQL, SQLfunctions.GetConnection());
        object res = null;
        bool connectionopened = OpenConnection(cmd.Connection);

        // Execute the command Scalar allowing other threads
        // to execute during the time it takes a response
        // to be returned.
        try
        {
            res = cmd.ExecuteScalar();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            // Close the connection if it was not already open.
            if (connectionopened == true)
            {
                cmd.Connection.Close();
            }
        }
        return res;
    }

    public static object ExecuteScalarInsert(string SQL)
    {
        SqlCommand cmd = new SqlCommand(SQL, SQLfunctions.GetConnectionInsert());
        object res = null;
        bool connectionopened = OpenConnection(cmd.Connection);

        // Execute the command Scalar allowing other threads
        // to execute during the time it takes a response
        // to be returned.
        try
        {
            res = cmd.ExecuteScalar();
        }
        catch (Exception ex)
        {
            throw ex;
        }
        finally
        {
            // Close the connection if it was not already open.
            if (connectionopened == true)
            {
                cmd.Connection.Close();
            }
        }
        return res;
    }

    /// <summary>
    /// Creates a standard StoredProcedure command.
    /// </summary>
    /// <param name="name">The name of the command in the default database.</param>
    /// <returns>An SQLCommand Stored Procedure object with it's connection set to the default
    /// database and it's name set to the value passed into the method.</returns>
    public static SqlCommand MakeStoredProcedureCommand(string name)
    {
        return MakeStoredProcedureCommand(name, SQLfunctions.GetConnection());
    }

    public static SqlCommand MakeStoredProcedureCommand(string name, SqlConnection connection)
    {
        SqlCommand cmd = new SqlCommand(name, SQLfunctions.GetConnection());
        cmd.CommandType = CommandType.StoredProcedure;
        return cmd;
    }

    public static SqlParameter MakeOutputParameter(string name, SqlDbType type)
    {
        SqlParameter ret = new SqlParameter(name, type);
        ret.Direction = ParameterDirection.Output;
        ret.IsNullable = true;
        return ret;
    }

    public static long DataTableReaderBytes(DataTable dt, int field, Stream str)
    {
        const int BUFFER_SIZE = 4096;

        int pos = 0;
        byte[] buf = new byte[BUFFER_SIZE];
        long bytes = 0;
        long read = 0;

        DataTableReader dr = dt.CreateDataReader();
        if (dr != null)
        {
            if (dr.Read())
            {
                read = dr.GetBytes(field, pos, buf, 0, BUFFER_SIZE);
                while (read > 0)
                {
                    bytes += read;
                    str.Write(buf, 0, Convert.ToInt32(read));
                    pos += BUFFER_SIZE;
                    read = dr.GetBytes(field, pos, buf, 0, BUFFER_SIZE);
                }
            }
            dr.Close();
        }

        return bytes;
    }

    public static string FormatDateTime(DateTime dt)
    {
        return String.Format("{0:s}", dt);
    }

    public static string FormatDateTime(SqlDateTime sdt)
    {
        return String.Format("{0:s}", sdt);
    }

    /// <summary>
    /// Formats a date for inclusion in an SQL string. The date is
    /// converted to it's UTC value and then returned as a SqlDateTime.
    /// </summary>
    /// <param name="dt">DateTime value</param>
    /// <returns>SqlDateTime</returns>
    public static SqlDateTime GetSQLUniversalTime(DateTime dt)
    {
        return GetSQLDateTime(dt.ToUniversalTime());
    }

    /// <summary>
    /// Takes a string date and time and turns it into a DateTime
    /// variable for inclusion in an SQL parameter query that
    /// takes datetime objects.
    /// If the date is invalid we return the SQL Server's minimum date.
    /// By trapping the exception in this method we avoid excessive
    /// error handling in other objects.
    /// </summary>
    /// <param name="dateTime">Date time in any format</param>
    /// <returns>DateTime version.</returns>
    public static SqlDateTime GetSQLDateTime(DateTime dt)
    {
        SqlDateTime sdt;
        try
        {
            sdt = new SqlDateTime(dt);
        }
        catch
        {
            // The date is out of range so return the lowest value.
            sdt = new SqlDateTime(0, 0);
        }
        return sdt;
    }

    public static string FormatSQLString(string src)
    {
        if (src != null)
        {
            return src.Replace("'", "''");
        }
        return null;
    }
}
