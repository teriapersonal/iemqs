﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IGlobalJEPTemplateService
    {
        #region Global JEP Template

        Int64 AddEdit(GlobalJEPTemplateModel model);
        GlobalJEPTemplateModel GetGlobalJEPTemplatebyId(Int64 Id);       
        List<GlobalJEPTemplateModel> GetAllGlobalJEPByProduct(Int64 Id);
        List<GlobalJEPTemplateModel> GetAllGlobalJEPTemplateByProductId(Int64 ProductId,Int64? BUId);
        List<SP_DES_GET_GLOBAL_JEP_TEMPLATE_Result> GetGlobalJEPTemplateList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        Int64 DeleteGlobalJEPTemplate(Int64 Id, out string errorMsg);
        bool CheckTemplateNameAlreadyExist(string name, Int64? jeptemplateId);

        #endregion

        #region JEP Template

        Int64 AddEditProjectTemplate(GlobalJEPTemplateModel model);
        GlobalJEPTemplateModel GetJEPTemplatebyId(Int64 Id);      
        List<SP_DES_GET_JEP_TEMPLATE_Result> GetJEPTemplateList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal,string Project);
        Int64 DeleteJEPTemplate(Int64 Id, out string errorMsg);

        #endregion

        bool UpdateIsCheckInJEPTemplate(Int64? JEPTemplateId, bool IsJEPTemplateCheckIn);

        bool IsCheckInJEPTemplate(string PsNo, Int64? JEPTemplateId);
    }
}
