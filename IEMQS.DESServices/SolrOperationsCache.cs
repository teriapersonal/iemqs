﻿using CommonServiceLocator;
using SolrNet;

namespace IEMQS.DESServices
{
    internal static class SolrOperationsCache<T>
        where T : new()
    {
        private static ISolrOperations<T> solrOperations;

        public static ISolrOperations<T> GetSolrOperations(string solrUrl)
        {
            if (solrOperations == null)
            {
                Startup.Init<T>(solrUrl);
                solrOperations = ServiceLocator.Current.GetInstance<ISolrOperations<T>>();
            }

            return solrOperations;
        }

    }
}
