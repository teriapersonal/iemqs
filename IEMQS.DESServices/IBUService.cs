﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IBUService
    {
        Int64 AddEdit(BUModel model,out string errorMsg);
        BUModel GetBUbyId(Int64 Id);
        List<BUModel> GetAllBU();
        List<SP_DES_GET_BU_Result> GetBUPBUList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        Int64 DeleteBU(Int64 Id, out string errorMsg);
    }
}
