﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Mvc;

namespace IEMQS.DESServices
{
    public class DCRService : IDCRService
    {
        #region Yash DCR   

        public string AddDCR(DCRModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES089.Find(model.DCRId);

                    DES089 _DES089 = temp == null ? new DES089() : temp;

                    _DES089.DCRDescription = model.DCRDescription;
                    _DES089.CategoryOfChange = model.CategoryOfChange;
                    _DES089.ResponsibleDesignEngineer = model.ResponsibleDesignEngineer;
                    _DES089.ResponsibleSeniorDesignEngineer = model.ResponsibleSeniorDesignEngineer;
                    _DES089.ReasonForChange = model.ReasonForChange;
                    _DES089.ChangeFrom = model.ChangeFrom;
                    _DES089.ChangeTo = model.ChangeTo;
                    _DES089.NatureOfChange = model.NatureOfChange;
                    _DES089.StatusOfTheItems = model.StatusOfTheItems;
                    _DES089.Originator = model.Originator;
                    _DES089.Owner = model.Owner;
                    _DES089.OriginatedDate = model.OriginatedDate;
                    _DES089.ModifiedDate = model.ModifiedDate;
                    _DES089.Status = ObjectStatus.Created.ToString();
                    _DES089.Project = model.Project;
                    _DES089.Department = model.Department;
                    if ((model.RoleGroup == null || model.FolderId == null) && (temp != null))
                    {
                        _DES089.RoleGroup = temp.RoleGroup;
                        _DES089.FolderId = temp.FolderId;
                    }
                    else
                    {
                        _DES089.RoleGroup = model.RoleGroup;
                        _DES089.FolderId = model.FolderId;
                    }
                    if (temp == null)
                    {
                        _DES089.DCRNo = GenerateDCRNumber(model.Project);
                        _DES089.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES089.CreatedOn = DateTime.Now;

                        #region getData From LifeCycle
                        var policy = GetPolicySteps(ObjectName.DCR.ToString(), model.BUId, null); // Get Polocy Steps
                        List<DES102> _DES102List = new List<DES102>();

                        if (policy != null)
                        {
                            foreach (var item in policy)
                            {
                                _DES089.PolicyId = item.PolicyId;

                                _DES102List.Add(new DES102
                                {
                                    PolicyID = item.PolicyId,
                                    PolicyStep = item.PolicyStep,
                                    PolicyOrderID = item.PolicyOrderID,
                                    IsCompleted = false,
                                    DesignationLevels = item.DesignationLevels,
                                    ColorCode = item.ColorCode
                                });
                            }
                        }
                        #endregion
                        _DES089.DES102 = _DES102List;

                        db.DES089.Add(_DES089);
                        AddDCRHistory(_DES089.DCRId, null, null, null, null, null, null, null, null, CommonService.objClsLoginInfo.Name + " has created DCR and promoted to Submit state", CommonService.objClsLoginInfo.UserName);
                    }
                    else
                    {
                        _DES089.DCRNo = model.DCRNo;
                        _DES089.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES089.EditedOn = DateTime.Now;

                        AddDCRHistory(_DES089.DCRId, null, null, null, null, null, null, null, null, CommonService.objClsLoginInfo.Name + " has edited DCR", CommonService.objClsLoginInfo.UserName);
                    }
                    db.SaveChanges();



                    return _DES089.DCRId + "|" + _DES089.DCRNo;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public string AddDetailDCR(DCRModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES089.Where(x => x.DCRId == model.DCRId).FirstOrDefault();
                    data.DCRDescription = model.DCRDescription;
                    data.CategoryOfChange = model.CategoryOfChange;
                    data.ReasonForChange = model.ReasonForChange;
                    data.ChangeFrom = model.ChangeFrom;
                    data.ChangeTo = model.ChangeTo;
                    data.NatureOfChange = model.NatureOfChange;
                    data.StatusOfTheItems = model.StatusOfTheItems;
                    data.EditedBy = CommonService.objClsLoginInfo.UserName;
                    data.EditedOn = DateTime.Now;
                    data.Status = ObjectStatus.InProcess.ToString();
                    db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return "";
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
        public string GenerateDCRNumber(string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var GetProjectDetail = db.DES051.Where(x => x.Project == Project).FirstOrDefault();
                    var GetProjectRelatedFromContract = db.DES051.Where(y => y.IdenticalGroup == GetProjectDetail.IdenticalGroup).Select(y => y.Project).ToList();
                    int iCount = 0;
                    foreach (var item in GetProjectRelatedFromContract)
                    {
                        int CiCount = db.DES089.Where(w => w.Project == item).Count();
                        iCount += CiCount;
                    }

                    iCount = iCount + 1;
                    string str = GetProjectDetail.IdenticalGroup + "-DCR-" + string.Format("{0:000}", iCount);
                    while (true)
                    {
                        if (db.DES089.Where(w => w.DCRNo == str).Count() > 0)
                        {
                            iCount = iCount + 1;
                            str = GetProjectDetail.IdenticalGroup + "-DCR-" + string.Format("{0:000}", iCount);
                        }
                        else
                        {
                            break;
                        }
                    }
                    return str;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw
;
            }
        }

        //        public string GenerateDCRNumber(string Project, int Id)
        //        {
        //            try
        //            {
        //                using (IEMQSDESEntities db = new IEMQSDESEntities())
        //                {
        //                    string str = string.Empty;
        //                    var GetProjectDetail = db.DES051.Where(x => x.Project == Project).FirstOrDefault();
        //                    var GetProjectRelatedFromContract = db.DES051.Where(y => y.IdenticalGroup == GetProjectDetail.IdenticalGroup).Select(y => y.Project).ToList();
        //                    int iCount = 0;
        //                    foreach (var item in GetProjectRelatedFromContract)
        //                    {
        //                        int CiCount = db.DES089.Where(w => w.Project == item).Count();
        //                        iCount += CiCount;
        //                    }

        //                    iCount = iCount + 1;
        //                    if (Id == 1)
        //                    {
        //                        str = GetProjectDetail.IdenticalGroup + "-DCR-" + string.Format("{0:000}", iCount);
        //                        while (true)
        //                        {
        //                            if (db.DES089.Where(w => w.DCRNo == str).Count() > 0)
        //                            {
        //                                iCount = iCount + 1;
        //                                str = GetProjectDetail.IdenticalGroup + "-DCR-" + string.Format("{0:000}", iCount);
        //                            }
        //                            else
        //                            {
        //                                break;
        //                            }
        //                        }
        //                    }
        //                    else
        //                    {
        //                        str = GetProjectDetail.Project + "-DCR-" + string.Format("{0:000}", iCount);
        //                        while (true)
        //                        {
        //                            if (db.DES089.Where(w => w.DCRNo == str).Count() > 0)
        //                            {
        //                                iCount = iCount + 1;
        //                                str = GetProjectDetail.Project + "-DCR-" + string.Format("{0:000}", iCount);
        //                            }
        //                            else
        //                            {
        //                                break;
        //                            }
        //                        }
        //                    }
        //                    return str;
        //                }
        //            }
        //            catch (Exception ex)
        //            {
        //                CommonService.SendErrorToText(ex);
        //                throw
        //;
        //            }
        //        }
        public List<SP_DES_GET_DCR_JEP_DOC_Result> GetJEPDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, out int recordsTotal, Int64? DCRId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DCR_JEP_DOC(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, DCRId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 AddEdit(DCRDocumentModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES090.Find(model.DCRAffectedDocumentId);
                    DES090 _DES090 = temp == null ? new DES090() : temp;

                    if (temp == null)
                    {
                        foreach (var d90 in model.dCRDocumentList)
                        {
                            _DES090.DCRId = model.DCRId;
                            _DES090.JEPDocumentDetailsId = d90.JEPDocumentDetailsId;
                            _DES090.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            _DES090.CreatedOn = DateTime.Now;
                            db.DES090.Add(_DES090);
                            db.SaveChanges();
                        }
                    }
                    else
                    {
                        _DES090.DCRId = temp.DCRId;
                        _DES090.JEPDocumentDetailsId = temp.JEPDocumentDetailsId;
                        _DES090.GeneralRemarks = model.GeneralRemarks;
                        _DES090.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES090.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }

                    return _DES090.DCRAffectedDocumentId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DCR_GLOBAL_JEP_DOC_Result> GetJEPGlobalDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string DocumentNo, Int64? DCRId, string CurrentProject)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DCR_GLOBAL_JEP_DOC(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, DocumentNo, CurrentProject, DCRId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DCR_AFFECTED_DOC_Result> GetAffectedDCRDoc(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DCRId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DCR_AFFECTED_DOC(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, DCRId).ToList();
                    // recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    recordsTotal = 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.DocumentId);
                    });
                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
        public List<SelectListItem> GetAllDOCByProject(string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES059.Where(x => x.Project == Project && x.IsLatestRevision == true).Select(x => new SelectListItem
                    {
                        Text = x.DocumentNo,
                        Value = x.DocumentNo
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
        //public List<SelectListItem> GetDCRName()
        //{
        //    try
        //    {
        //        using (IEMQSDESEntities db = new IEMQSDESEntities())
        //        {
        //            List<SelectListItem> DNS = new List<SelectListItem>();
        //            DNS.Add(new SelectListItem() { Text = "Identical Projects", Value = "1" });
        //            DNS.Add(new SelectListItem() { Text = "Project Specific", Value = "2" });

        //            return DNS;

        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        CommonService.SendErrorToText(ex);
        //        throw;
        //    }
        //}


        public List<SelectListItem> GetContractProjectList(string projectNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES051 = db.DES051.Where(w => w.Project == projectNo).FirstOrDefault();
                    var ContractNo = _DES051.Contract;
                    return db.DES051.Where(x => x.Contract == ContractNo && x.Status != ObjectStatus.Created.ToString()).Select(x => new SelectListItem
                    {
                        Text = x.Project,
                        Value = x.Project
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public bool DeleteAffectedDocument(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES090.Find(Id);
                    if (check != null)
                    {
                        db.DES090.Remove(check);
                        db.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                errorMsg = ex.Message;
                return false;
            }
        }

        public long UploadDCRSupportDocs(DCRDocumentMappingModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    DES091 _DES091 = new DES091();

                    _DES091.DCRId = model.DCRId;
                    _DES091.DocumentTitle = model.DocumentTitle;
                    _DES091.DocumentFormat = model.DocumentFormat;
                    _DES091.DocumentPath = model.DocumentPath;
                    _DES091.FileLocation = CommonService.GetUseIPConfig.Location;
                    _DES091.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _DES091.CreatedOn = DateTime.Now;
                    db.DES091.Add(_DES091);
                    db.SaveChanges();
                    return _DES091.DCRSupportedDocId;
                }

            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DCR_SUPPORTED_DOCUMENT_Result> GetDCRSupportedDocumentList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DCRId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DCR_SUPPORTED_DOCUMENT(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, DCRId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DCR_PART_Result> GetPartList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DCRId, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DCR_PART(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, DCRId, Project).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 AddPart(DCRPartModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DES094 _DES094 = new DES094();
                    foreach (var d94 in model.dCRPartList)
                    {
                        _DES094.DCRId = model.DCRId;
                        _DES094.ItemId = d94.ItemId;
                        _DES094.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES094.CreatedOn = DateTime.Now;
                        db.DES094.Add(_DES094);
                        db.SaveChanges();
                    }
                    return _DES094.DCRAffectedPartsId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DCR_AFFECTED_PART_Result> GetDCRAffectedPart(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DCRId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DCR_AFFECTED_PART(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, DCRId).ToList();
                    //recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    recordsTotal = 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        string qstring = CommonService.Encrypt("ItemId=" + x.ItemId);
                        x.QString = qstring;
                    });
                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public DCRModel GetDCRById(DCRModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES089 = db.DES089.Where(x => x.DCRId == model.DCRId).Select(x => new DCRModel
                    {
                        DCRId = x.DCRId,
                        DCRNo = x.DCRNo,
                        DCRDescription = x.DCRDescription,
                        CategoryOfChange = x.CategoryOfChange,
                        ResponsibleDesignEngineer = x.ResponsibleDesignEngineer,
                        ResponsibleSeniorDesignEngineer = x.ResponsibleSeniorDesignEngineer,
                        ReasonForChange = x.ReasonForChange,
                        ChangeFrom = x.ChangeFrom,
                        ChangeTo = x.ChangeTo,
                        NatureOfChange = x.NatureOfChange,
                        StatusOfTheItems = x.StatusOfTheItems,
                        Originator = x.Originator,
                        OriginatedDate = x.OriginatedDate,
                        ModifiedDate = x.ModifiedDate,
                        //OriginatedDatestr = x.OriginatedDate != null ? x.OriginatedDate.Value.ToString("dd/MM/yyyy") : "",
                        Owner = x.Owner,
                        // ModifiedDatestr = x.ModifiedDate != null ? x.ModifiedDate.Value.ToString("dd/mm/yyyy"):"",
                        Status = x.Status,
                        Project = x.Project,
                        FolderId = x.FolderId,
                        Department = x.Department,
                        RoleGroup = x.RoleGroup,
                        ReleasedStepId = x.ReleasedStepId,
                        ReleasedRemark = x.ReleasedRemark,
                    }).FirstOrDefault();

                    var _DES097 = db.DES097.Where(x => x.DCRId == model.DCRId && (x.Status == ReviewOrEvaluate.Accepted.ToString() || x.Status == ReviewOrEvaluate.Rejected.ToString())).FirstOrDefault();
                    if (_DES097 != null)
                    {
                        _DES089.DCRReviewModel = new DCRReviewModel();
                        _DES089.IsReviewed = true;
                        _DES089.DCRReviewModel.Remark = _DES097.Remark;
                        _DES089.DCRReviewModel.Status = _DES097.Status;
                        _DES089.DCRReviewModel.DCRReviewId = _DES097.DCRReviewId;
                    }

                    var _DES098 = db.DES098.Where(x => x.DCRId == model.DCRId && (x.Status == ReviewOrEvaluate.Accepted.ToString() || x.Status == ReviewOrEvaluate.Rejected.ToString())).FirstOrDefault();
                    if (_DES098 != null)
                    {
                        _DES089.DCREvaluateModel = new DCREvaluateModel();
                        _DES089.IsEvaluated = true;
                        _DES089.DCREvaluateModel.Remark = _DES098.Remark;
                        _DES089.DCREvaluateModel.Status = _DES098.Status;
                        _DES089.DCREvaluateModel.DCREvaluateId = _DES098.DCREvaluateId;

                        var _DES100List = db.DES100.Where(x => x.DCRId == model.DCRId && x.DCREvaluateId == _DES098.DCREvaluateId).ToList();
                        if (_DES100List != null && _DES100List.Count > 0)
                        {
                            _DES089.DepartmentGroupList = _DES100List.Select(z => new DCREvaluateCommentModel
                            {
                                DCREvaluateId = z.DCREvaluateId,
                                DCREvaluateCommentId = z.DCREvaluateCommentId,
                                DCRId = z.DCRId,
                                DeptId = z.DeptId,
                                IsApproved = z.IsApproved,
                                IsCompleted = z.IsCompleted,
                                Remark = z.Remark
                            }).ToList();

                            _DES100List.ForEach(b =>
                            {
                                if (b.DES101.Select(c => c.PSNumber).ToList().Contains(CommonService.objClsLoginInfo.UserName))
                                {
                                    _DES089.DCREvaluateCommentModel = _DES100List.Select(a => new DCREvaluateCommentModel
                                    {
                                        DCREvaluateCommentId = b.DCREvaluateCommentId,
                                        DCREvaluateId = b.DCREvaluateId,
                                        DCRId = b.DCRId,
                                        DeptId = b.DeptId,
                                        IsCompleted = b.IsCompleted,
                                        Remark = b.Remark,
                                        IsApproved = b.IsApproved,
                                    }).FirstOrDefault();
                                }
                            });
                        }
                    }
                    else
                    {
                        var _DES098Return = db.DES098.Where(x => x.DCRId == model.DCRId && (x.Status == ReviewOrEvaluate.Return.ToString())).OrderByDescending(x => x.DCREvaluateId).FirstOrDefault();
                        if (_DES098Return != null)
                        {
                            var _DES100List = db.DES100.Where(x => x.DCRId == model.DCRId && x.DCREvaluateId == _DES098Return.DCREvaluateId).ToList();
                            if (_DES100List != null && _DES100List.Count > 0)
                            {
                                _DES089.DepartmentGroupList = _DES100List.Select(z => new DCREvaluateCommentModel
                                {
                                    DCREvaluateId = z.DCREvaluateId,
                                    DCREvaluateCommentId = z.DCREvaluateCommentId,
                                    DCRId = z.DCRId,
                                    DeptId = z.DeptId,
                                    IsApproved = z.IsApproved,
                                    IsCompleted = z.IsCompleted,
                                    Remark = z.Remark,
                                    FunctionList = new DepartmentGroupService().GetTypeList().Select(zz => new SelectListItem
                                    {
                                        Text = zz.t_desc,
                                        Value = zz.t_dimx,
                                        Selected = zz.t_dimx == z.DeptId,
                                    }).ToList(),
                                    PSNumberList = z.DES101.Select(w => new SelectListItem
                                    {
                                        Text = new clsManager().GetPsidName(w.PSNumber),
                                        Value = w.PSNumber,
                                        Selected = true
                                    }).ToList()
                                }).ToList();

                                _DES100List.ForEach(b =>
                                {
                                    if (b.DES101.Select(c => c.PSNumber).ToList().Contains(CommonService.objClsLoginInfo.UserName))
                                    {
                                        _DES089.DCREvaluateCommentModel = _DES100List.Select(a => new DCREvaluateCommentModel
                                        {
                                            DCREvaluateCommentId = b.DCREvaluateCommentId,
                                            DCREvaluateId = b.DCREvaluateId,
                                            DCRId = b.DCRId,
                                            DeptId = b.DeptId,
                                            IsCompleted = b.IsCompleted,
                                            Remark = b.Remark,
                                            IsApproved = b.IsApproved,
                                        }).FirstOrDefault();
                                    }
                                });
                            }
                        }
                    }


                    var _DES108 = db.DES108.Where(x => x.DCRId == model.DCRId && (x.Status == ReviewOrEvaluate.Accepted.ToString() || x.Status == ReviewOrEvaluate.Rejected.ToString())).FirstOrDefault();
                    if (_DES108 != null)
                    {
                        _DES089.DCRReleaseModel = new DCRReleaseModel();
                        _DES089.IsReleased = true;
                        _DES089.DCRReleaseModel.Remark = _DES108.Remark;
                        _DES089.DCRReleaseModel.Status = _DES108.Status;
                        _DES089.DCRReleaseModel.DCRReleaseId = _DES108.DCRReleaseId;
                    }

                    if (_DES089.ReleasedStepId > 0)
                    {
                        if (_DES089.ReleasedStepId == (int)DCRReleasedSteps.Step3)
                        {
                            int? _DES090List1 = db.DES090.Count(x => x.DCRId == model.DCRId && (x.IsPartialSelect ?? false) == false);
                            int? _DES094List1 = db.DES094.Count(x => x.DCRId == model.DCRId && (x.IsPartialSelect ?? false) == false);

                            if (!(_DES090List1 > 0) && !(_DES094List1 > 0))
                            {
                                _DES089.IsAllPartialSelect = true;
                            }
                            else
                            {
                                _DES089.IsAllPartialSelect = false;
                            }
                        }
                        else
                        {
                            _DES089.IsAllPartialSelect = true;
                        }
                    }
                    return _DES089;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateDCRStatus(Int64? DCRId, string Status)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES089.Find(DCRId);
                    DES089 _DES089 = temp == null ? new DES089() : temp;

                    if (_DES089.Status != ObjectStatus.InProcess.ToString())
                    {
                        if (_DES089.Status == ObjectStatus.Created.ToString())
                        {
                            AddDCRHistory(_DES089.DCRId, null, null, null, null, null, null, null, null, CommonService.objClsLoginInfo.Name + " has created DCR and promoted to Submit state", CommonService.objClsLoginInfo.UserName);
                        }
                        pushDCRNotification(DCRId, DCRLifeCyclePolicyStep.Create.ToString());
                    }

                    _DES089.Status = Status;
                    UpdateLifeCycleStatus(DCRId, (int)DCRLifeCyclePolicyStep.Create);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool Delete(Int64? Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var x = db.DES089.Find(Id);
                    if (x != null)
                    {
                        db.DES090.RemoveRange(x.DES090);
                        db.DES091.RemoveRange(x.DES091);
                        db.DES094.RemoveRange(x.DES094);
                        db.DES097.RemoveRange(x.DES097);
                        db.DES098.RemoveRange(x.DES098);
                        x.DES100.ToList().ForEach(y =>
                        {
                            db.DES101.RemoveRange(y.DES101);
                            db.DES100.Remove(y);
                        });
                        db.DES102.RemoveRange(x.DES102);
                        db.DES089.Remove(x);
                        db.SaveChanges();
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool DeleteAffectedPart(Int64? Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var check = db.DES094.Find(Id);
                    if (check != null)
                    {
                        db.DES094.Remove(check);
                        db.SaveChanges();
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);

                throw;
            }
        }

        public List<SP_DES_GET_DCR_LIST_Result> GetDCRList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string Roles)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string IdenticalGroup = db.DES051.Where(x => x.Project == Project).Select(x => x.IdenticalGroup).FirstOrDefault();
                    var data = db.SP_DES_GET_DCR_LIST(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, IdenticalGroup, Roles).ToList();

                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.DCRId);
                    });

                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public string AddReviewOrEvaluate(DCRModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var result = "";
                    bool IsReturned = false;
                    bool IsRejected = false;

                    if (model.Status == ReviewOrEvaluate.Return.ToString())
                    {
                        UpdateLifeCycleReturn(model.DCRId);
                        IsReturned = true;
                    }
                    else if (model.Status == ReviewOrEvaluate.Rejected.ToString())
                    {
                        UpdateDCRStatus(model.DCRId, ObjectStatus.Rejected.ToString());
                        pushDCRNotification(model.DCRId, ObjectStatus.Rejected.ToString());
                        IsRejected = true;
                    }

                    if (model.Page.ToLower() == ReviewOrEvaluate.Review.ToString().ToLower())
                    {
                        var temp = db.DES097.Where(x => x.DCRId == model.DCRId && x.Status != ReviewOrEvaluate.Return.ToString()).FirstOrDefault();

                        DES097 _DES097 = temp == null ? new DES097() : temp;
                        _DES097.DCRId = model.DCRId;
                        _DES097.Remark = model.DCRReviewModel.Remark;
                        _DES097.Department = model.Department;
                        _DES097.RoleGroup = model.RoleGroup;
                        _DES097.Status = model.Status;

                        if (temp == null)
                        {
                            _DES097.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            _DES097.CreatedOn = DateTime.Now;
                            db.DES097.Add(_DES097);
                        }
                        else
                        {
                            _DES097.EditedBy = CommonService.objClsLoginInfo.UserName;
                            _DES097.EditedOn = DateTime.Now;
                        }
                        db.SaveChanges();
                        result = ReviewOrEvaluate.Review.ToString();

                        string strMsg = string.Empty;
                        UpdateDCROwner(_DES097.DCRId);
                        if (IsReturned)
                        {
                            strMsg = "DCR Returned";
                            AddDCRHistory(_DES097.DCRId, null, null, null, _DES097.DCRReviewId, null, null, null, null, CommonService.objClsLoginInfo.Name + " return DCR from Submit state, Return Remark: " + model.DCRReviewModel.Remark, CommonService.objClsLoginInfo.UserName);
                        }
                        else if (IsRejected)
                        {
                            strMsg = "DCR Rejected";
                            AddDCRHistory(_DES097.DCRId, null, null, null, _DES097.DCRReviewId, null, null, null, null, CommonService.objClsLoginInfo.Name + " reject DCR from Submit state, Reject Remark: " + model.DCRReviewModel.Remark, CommonService.objClsLoginInfo.UserName);
                        }
                        else
                        {
                            strMsg = "DCR Reviewed";
                            UpdateLifeCycleStatus(_DES097.DCRId, (int)DCRLifeCyclePolicyStep.Submit);
                            pushDCRNotification(_DES097.DCRId, DCRLifeCyclePolicyStep.Submit.ToString());
                            AddDCRHistory(_DES097.DCRId, null, null, null, _DES097.DCRReviewId, null, null, null, null, CommonService.objClsLoginInfo.Name + " promoted from Submit to Evaluate State ", CommonService.objClsLoginInfo.UserName);
                        }

                    }
                    else if (model.Page.ToLower() == ReviewOrEvaluate.Evaluate.ToString().ToLower())
                    {
                        var temp = db.DES098.Where(x => x.DCRId == model.DCRId && x.Status != ReviewOrEvaluate.Return.ToString()).FirstOrDefault();

                        DES098 _DES098 = temp == null ? new DES098() : temp;
                        _DES098.DCRId = model.DCRId;
                        _DES098.Remark = model.DCREvaluateModel.Remark;
                        _DES098.Department = model.Department;
                        _DES098.RoleGroup = model.RoleGroup;
                        _DES098.Status = model.Status;

                        if (temp == null)
                        {
                            _DES098.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            _DES098.CreatedOn = DateTime.Now;
                            db.DES098.Add(_DES098);
                        }
                        else
                        {
                            _DES098.EditedBy = CommonService.objClsLoginInfo.UserName;
                            _DES098.EditedOn = DateTime.Now;
                        }
                        result = ReviewOrEvaluate.Evaluate.ToString();
                        List<DES100> _DES100List = new List<DES100>();

                        if (model.DepartmentGroupList != null)
                        {
                            foreach (var d100 in model.DepartmentGroupList)
                            {
                                List<DES101> _DES101List = new List<DES101>();
                                if (!d100.IsDelete)
                                {
                                    if ((!string.IsNullOrEmpty(d100.DeptId)) && d100.PSNoList != null && d100.PSNoList.Count > 0)
                                    {
                                        DES100 _DES100 = new DES100();
                                        _DES100.DeptId = d100.DeptId;
                                        _DES100.DCRId = model.DCRId;
                                        foreach (var ps in d100.PSNoList)
                                        {
                                            _DES101List.Add(new DES101 { PSNumber = ps });
                                        }
                                        _DES100.DES101 = _DES101List;
                                        _DES100List.Add(_DES100);
                                    }
                                }
                            }
                        }
                        else
                        {
                            var _DES098Return = db.DES098.Where(x => x.DCRId == model.DCRId && (x.Status == ReviewOrEvaluate.Return.ToString())).OrderByDescending(x => x.DCREvaluateId).FirstOrDefault();
                            if (_DES098Return != null)
                            {
                                _DES100List = db.DES100.Where(x => x.DCRId == model.DCRId && x.DCREvaluateId == _DES098Return.DCREvaluateId).ToList();
                                if (_DES100List != null && _DES100List.Count > 0)
                                {
                                    model.DepartmentGroupList = _DES100List.Select(z => new DCREvaluateCommentModel
                                    {
                                        DCREvaluateId = z.DCREvaluateId,
                                        DCREvaluateCommentId = z.DCREvaluateCommentId,
                                        DCRId = z.DCRId,
                                        DeptId = z.DeptId,
                                        IsApproved = z.IsApproved,
                                        IsCompleted = z.IsCompleted,
                                        Remark = z.Remark
                                    }).ToList();

                                    _DES100List.ForEach(b =>
                                    {
                                        if (b.DES101.Select(c => c.PSNumber).ToList().Contains(CommonService.objClsLoginInfo.UserName))
                                        {
                                            model.DCREvaluateCommentModel = _DES100List.Select(a => new DCREvaluateCommentModel
                                            {
                                                DCREvaluateCommentId = b.DCREvaluateCommentId,
                                                DCREvaluateId = b.DCREvaluateId,
                                                DCRId = b.DCRId,
                                                DeptId = b.DeptId,
                                                IsCompleted = b.IsCompleted,
                                                Remark = b.Remark,
                                                IsApproved = b.IsApproved,
                                            }).FirstOrDefault();
                                        }
                                    });
                                }
                            }
                        }
                        _DES098.DES100 = _DES100List;

                        string strMsg = string.Empty;
                        UpdateDCROwner(_DES098.DCRId);

                        if (IsReturned)
                        {
                            strMsg = "DCR Returned";
                            AddDCRHistory(_DES098.DCRId, null, null, null, null, _DES098.DCREvaluateId, null, null, null, CommonService.objClsLoginInfo.Name + " return DCR from Evaluate state, Return Remark: " + model.DCREvaluateModel.Remark, CommonService.objClsLoginInfo.UserName);
                        }
                        else if (IsRejected)
                        {
                            strMsg = "DCR Rejected";
                            AddDCRHistory(_DES098.DCRId, null, null, null, null, _DES098.DCREvaluateId, null, null, null, CommonService.objClsLoginInfo.Name + " reject DCR from Evaluate state, Reject Remark: " + model.DCREvaluateModel.Remark, CommonService.objClsLoginInfo.UserName);
                        }
                        else
                        {
                            if (_DES100List.Count == 0)
                            {
                                var _DES102 = db.DES102.Where(x => x.DCRId == _DES098.DCRId && x.PolicyOrderID == (int)DCRLifeCyclePolicyStep.Review).FirstOrDefault();
                                _DES102.IsCompleted = true;
                            }
                            UpdateLifeCycleStatus(_DES098.DCRId, (int)DCRLifeCyclePolicyStep.Evaluate);


                            AddDCRHistory(_DES098.DCRId, null, null, null, _DES098.DCREvaluateId, null, null, null, null, CommonService.objClsLoginInfo.Name + " promoted from Evaluate to Review State ", CommonService.objClsLoginInfo.UserName);
                        }
                        db.SaveChanges();
                        if (_DES100List.Count > 0)
                        {
                            pushDCRNotification(_DES098.DCRId, DCRLifeCyclePolicyStep.Evaluate.ToString());
                        }
                        else
                        {
                            pushDCRNotification(_DES098.DCRId, DCRLifeCyclePolicyStep.Review.ToString());
                        }

                    }
                    else if (model.Page.ToLower() == ObjectStatus.Release.ToString().ToLower())
                    {
                        var temp = db.DES108.Where(x => x.DCRId == model.DCRId && x.Status != ReviewOrEvaluate.Return.ToString()).FirstOrDefault();

                        DES108 _DES108 = temp == null ? new DES108() : temp;
                        _DES108.DCRId = model.DCRId;
                        _DES108.Remark = model.DCRReleaseModel.Remark;
                        _DES108.Department = model.Department;
                        _DES108.RoleGroup = model.RoleGroup;
                        _DES108.Status = model.Status;

                        if (temp == null)
                        {
                            _DES108.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            _DES108.CreatedOn = DateTime.Now;
                            db.DES108.Add(_DES108);
                        }
                        else
                        {
                            _DES108.EditedBy = CommonService.objClsLoginInfo.UserName;
                            _DES108.EditedOn = DateTime.Now;
                        }

                        result = ObjectStatus.Release.ToString();

                        string strMsg = string.Empty;
                        UpdateDCROwner(_DES108.DCRId);
                        if (IsReturned)
                        {
                            strMsg = "DCR Returned";
                            AddDCRHistory(_DES108.DCRId, null, null, null, null, _DES108.DCRReleaseId, null, null, null, CommonService.objClsLoginInfo.Name + " return DCR from Release state, Return Remark: " + model.DCRReleaseModel.Remark, CommonService.objClsLoginInfo.UserName);
                        }
                        else if (IsRejected)
                        {
                            strMsg = "DCR Rejected";
                            AddDCRHistory(_DES108.DCRId, null, null, null, null, _DES108.DCRReleaseId, null, null, null, CommonService.objClsLoginInfo.Name + " reject DCR from Release state, Reject Remark: " + model.DCRReleaseModel.Remark, CommonService.objClsLoginInfo.UserName);
                        }
                        else
                        {
                            strMsg = "DCR Released";
                            UpdateLifeCycleStatus(_DES108.DCRId, (int)DCRLifeCyclePolicyStep.Release);
                            pushDCRNotification(_DES108.DCRId, DCRLifeCyclePolicyStep.Release.ToString());

                            AddDCRHistory(_DES108.DCRId, null, null, null, _DES108.DCRReleaseId, null, null, null, null, CommonService.objClsLoginInfo.Name + " promoted from Release to InCorporate State ", CommonService.objClsLoginInfo.UserName);
                        }
                        db.SaveChanges();
                    }
                    return result;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DCRPolicyStepsModel> GetPolicySteps(string Type, Int64? BUId, Int64? DCRId)
        {
            try
            {

                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (DCRId > 0)
                    {
                        var PoliicyData = db.DES102.Where(x => x.DCRId == DCRId).OrderBy(x => x.PolicyOrderID).ToList();

                        List<DCRPolicyStepsModel> listDCRPolicyStepsModel = new List<DCRPolicyStepsModel>();

                        foreach (var x in PoliicyData)
                        {
                            DCRPolicyStepsModel objectPolicyStepsModel = new DCRPolicyStepsModel();
                            objectPolicyStepsModel.DCRLifeCycleId = x.DCRLifeCycleId;
                            objectPolicyStepsModel.PolicyId = x.PolicyID;
                            objectPolicyStepsModel.PolicyStep = x.PolicyStep;
                            objectPolicyStepsModel.PolicyOrderID = x.PolicyOrderID;
                            objectPolicyStepsModel.IsCompleted = x.IsCompleted ?? false;
                            objectPolicyStepsModel.CreatedBy = new clsManager().GetUserNameFromPsNo((x.CreatedBy));
                            objectPolicyStepsModel.CreatedOn = x.CreatedOn;
                            objectPolicyStepsModel.ColorCode = x.ColorCode;
                            objectPolicyStepsModel.DesignationLevels = x.DesignationLevels;
                            listDCRPolicyStepsModel.Add(objectPolicyStepsModel);
                        }
                        return listDCRPolicyStepsModel.OrderBy(x => x.PolicyOrderID).ToList();
                    }
                    else
                    {
                        return db.SP_DES_GET_OBJECT_POLICY_BY_OBJECTNAME(Type, BUId).Select(x => new DCRPolicyStepsModel
                        {
                            PolicyId = x.PolicyId,
                            PolicyStep = x.StepName,
                            PolicyOrderID = x.OrderNo,
                            ColorCode = x.ColorCode,
                            DesignationLevels = x.DesignationLevels
                        }).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public void AddDCRHistory(Int64? DCRId, Int64? DCRAffectedDocumentId, Int64? DCRSupportedDocId, Int64? DCRAffectedPartsId, Int64? DCRReviewId, Int64? DCREvaluateId, Int64? DCREvaluateCommentId, Int64? DCRCommentPersonId, Int64? DCRLifeCycleId, string Discription, string CreatedBy)
        {
            using (IEMQSDESEntities db = new IEMQSDESEntities())
            {
                DES103 _DES103 = new DES103();
                _DES103.DCRId = DCRId;
                _DES103.DCRAffectedDocumentId = DCRAffectedDocumentId;
                _DES103.DCRAffectedPartsId = DCRAffectedPartsId;
                _DES103.DCRCommentPersonId = DCRCommentPersonId;
                _DES103.DCREvaluateCommentId = DCREvaluateCommentId;
                _DES103.DCREvaluateId = DCREvaluateId;
                _DES103.DCRLifeCycleId = DCRLifeCycleId;
                _DES103.DCRReviewId = DCRReviewId;
                _DES103.DCRSupportedDocId = DCRSupportedDocId;
                _DES103.Description = Discription;
                _DES103.CreatedBy = CommonService.objClsLoginInfo.UserName;
                _DES103.CreatedOn = DateTime.Now;
                db.DES103.Add(_DES103);
                db.SaveChanges();
            }
        }

        public List<SP_DES_GET_DCR_DEPT_PERSON_BYDCRID_Result> GetDeptPersonGridByDCRId(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DCRId, Int64? DCREvaluatId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DCR_DEPT_PERSON_BYDCRID(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, DCRId, DCREvaluatId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateApproval(DCREvaluateCommentModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES100.Find(model.DCREvaluateCommentId);
                    DES100 _DES100 = temp == null ? new DES100() : temp;

                    _DES100.IsApproved = model.IsApproved;
                    _DES100.IsCompleted = model.IsCompleted;
                    _DES100.Remark = model.Remark;
                    _DES100.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _DES100.CreatedOn = DateTime.Now;
                    db.SaveChanges();

                    int? _DES100ListCount = db.DES100.Where(c => c.DCRId == model.DCRId && c.IsCompleted != true && c.DCREvaluateId == _DES100.DCREvaluateId).Count();
                    if (!(_DES100ListCount > 0))
                    {
                        UpdateDCROwner(model.DCRId);
                        UpdateLifeCycleStatus(_DES100.DCRId, (int)DCRLifeCyclePolicyStep.Review);
                        pushDCRNotification(_DES100.DCRId, DCRLifeCyclePolicyStep.Review.ToString());
                    }
                    AddDCRHistory(_DES100.DCRId, null, null, null, null, _DES100.DCREvaluateId, _DES100.DCREvaluateCommentId, null, null, "Department Name " + CommonService.objClsLoginInfo.Name + " promoted from Review to Release State ", CommonService.objClsLoginInfo.UserName);
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateLifeCycleStatus(Int64? DCRId, int PolicyOrderID)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES102 = db.DES102.Where(x => x.DCRId == DCRId && x.PolicyOrderID == PolicyOrderID).FirstOrDefault();
                    _DES102.IsCompleted = true;
                    _DES102.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _DES102.CreatedOn = DateTime.Now;
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DOCModel> ReleasedProcess(DCRDocumentModel DCRDocumentModel, DCRPartModel DCRPartModel, DCRModel DCRModel, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<DOCModel> _DOCModelList = new List<DOCModel>();
                    List<Int64?> oldDOCListId = new List<Int64?>();
                    List<Int64?> oldPartListId = new List<Int64?>();
                    IpLocation ipLocation = CommonService.GetUseIPConfig;

                    var _DES089 = db.DES089.Find(DCRModel.DCRId);
                    _DES089.ReleasedStepId = DCRModel.ReleasedStepId;
                    _DES089.ReleasedRemark = DCRModel.ReleasedRemark;
                    _DES089.InCorporateBy = CommonService.objClsLoginInfo.UserName;
                    _DES089.InCorporateOn = DateTime.Now;
                    var _DES090List = db.DES090.Where(x => x.DCRId == DCRModel.DCRId).ToList();
                    var _DES094List = db.DES094.Where(x => x.DCRId == DCRModel.DCRId).ToList();

                    if (DCRModel.ReleasedStepId == (int)DCRReleasedSteps.Step1 || DCRModel.ReleasedStepId == (int)DCRReleasedSteps.Step2)
                    {
                        foreach (var item in _DES090List)
                        {
                            var _DES075 = db.DES075.Find(item.JEPDocumentDetailsId);
                            oldDOCListId.Add(_DES075.DocumentId);
                        }

                        foreach (var item in _DES094List)
                        {
                            oldPartListId.Add(item.ItemId);
                        }
                    }
                    else if ( DCRModel.ReleasedStepId == (int)DCRReleasedSteps.Step3 || DCRModel.ReleasedStepId == (int)DCRReleasedSteps.Step4)
                    {
                        if (DCRDocumentModel.dCRDocumentList != null && DCRDocumentModel.dCRDocumentList.Count > 0)
                        {
                            foreach (var itemDCRDocumentModel in DCRDocumentModel.dCRDocumentList)
                            {
                                var _DES090 = _DES090List.Where(c => c.JEPDocumentDetailsId == itemDCRDocumentModel.JEPDocumentDetailsId).FirstOrDefault();
                                _DES090.IsPartialSelect = true;

                                var _DES075 = db.DES075.Find(itemDCRDocumentModel.JEPDocumentDetailsId);
                                oldDOCListId.Add(_DES075.DocumentId);
                            }
                        }

                        if (DCRPartModel.dCRPartList != null && DCRPartModel.dCRPartList.Count > 0)
                        {
                            foreach (var itemDCRPartModel in DCRPartModel.dCRPartList)
                            {
                                var _DES094 = _DES094List.Where(c => c.ItemId == itemDCRPartModel.ItemId).FirstOrDefault();
                                _DES094.IsPartialSelect = true;

                                oldPartListId.Add(itemDCRPartModel.ItemId);
                            }
                        }
                    }


                    if (oldDOCListId.Count > 0)
                    {
                        foreach (var item in oldDOCListId)
                        {
                            ObjectParameter returnId = new ObjectParameter("newDocumentId", typeof(long?));
                            db.SP_DES_COPY_DOCDATA(item, Project, CommonService.objClsLoginInfo.UserName, CommonService.GetUseIPConfig.Location, returnId);
                            Int64 newDocumentId;
                            Int64.TryParse(Convert.ToString(returnId.Value), out newDocumentId);
                            if (newDocumentId > 0)
                            {
                                DOCModel _DOCModel = new DOCModel();
                                var _DES059 = db.DES059.Find(returnId.Value);
                                _DOCModel.DocumentId = _DES059.DocumentId;
                                _DOCModel.Project = _DES059.Project;
                                _DOCModel.DocumentTypeId = _DES059.DocumentTypeId;
                                _DOCModel.DocumentNo = _DES059.DocumentNo;
                                _DOCModel.DocumentTitle = _DES059.DocumentTitle;
                                _DOCModel.DocumentRevision = _DES059.DocumentRevision;
                                _DOCModel.ClientDocumentNumber = _DES059.ClientDocumentNumber;
                                _DOCModel.MilestoneDocument = Convert.ToBoolean(_DES059.MilestoneDocument);
                                _DOCModel.PolicyId = _DES059.PolicyId;
                                _DOCModel.IsJEP = Convert.ToBoolean(_DES059.IsJEP);
                                _DOCModel.Location = Convert.ToInt32(_DES059.Location);
                                _DOCModel.Status = _DES059.Status;
                                _DOCModel.FolderId = _DES059.FolderId;
                                _DOCModel.ClientVersion = _DES059.ClientVersion;
                                _DOCModel.CreatedBy = _DES059.CreatedBy;
                                _DOCModel.CreatedOn = _DES059.CreatedOn;
                                _DOCModel.EditedBy = _DES059.EditBy;
                                _DOCModel.EditedOn = _DES059.EditedOn;
                                _DOCModel.Department = _DES059.Department;
                                _DOCModel.RoleGroup = _DES059.RoleGroup;
                                _DOCModel.IsLatestRevision = true;
                                _DOCModel.ReturnNotes = _DES059.ReturnNotes;
                                _DOCModel.CurrentLocationIp = ipLocation.Ip;
                                _DOCModel.FilePath = ipLocation.File_Path;
                                _DOCModel.IsRevision = true;

                                var getDocMappingData = new DOCFileService().GetDocumentListByDocId(item);
                                _DOCModel.NewDocumentPathForRevison = new List<string>();
                                foreach (var docitem in getDocMappingData)
                                {
                                    string getDocuemtnUniqNamewith = docitem.MainDocumentPath;
                                    _DOCModel.NewDocumentPathForRevison.Add(getDocuemtnUniqNamewith);
                                }
                                _DOCModelList.Add(_DOCModel);
                                var _DES059item = db.DES059.Find(item);
                                try
                                {
                                    db.SP_DES_DCR_PUSH_DOC_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, DCRModel.DCRId, _DES059item.DocumentNo, _DES059item.CreatedBy);
                                }
                                catch (Exception ex)
                                {
                                    CommonService.SendErrorToText(ex);
                                }
                                if (DCRModel.ReleasedStepId == (int)DCRReleasedSteps.Step2)
                                {
                                    UpdatePartDocRevised((int)FolderObjectName.DOC, item, Convert.ToInt64(returnId.Value), DCRModel.DCRId);
                                }

                            }
                        }
                    }

                    if (oldPartListId.Count > 0)
                    {
                        foreach (var item in oldPartListId)
                        {
                            ObjectParameter returnId = new ObjectParameter("Out_R_ItemId", typeof(int));
                            db.SP_DES_COPY_ITEMDATA(item, Project, CommonService.objClsLoginInfo.UserName, returnId);
                            Int64 newPartId;
                            Int64.TryParse(Convert.ToString(returnId.Value), out newPartId);
                            if (newPartId > 0)
                            {
                                var partitem = new PartService().GetPartByID(Convert.ToInt64(item), 0);
                                try
                                {
                                    db.SP_DES_DCR_PUSH_PART_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, DCRModel.DCRId, partitem.ItemKey, partitem.CreatedBy);
                                }
                                catch (Exception ex)
                                {
                                    CommonService.SendErrorToText(ex);
                                }
                                if (DCRModel.ReleasedStepId == (int)DCRReleasedSteps.Step2)
                                {
                                    UpdatePartDocRevised((int)FolderObjectName.Part, item, newPartId, DCRModel.DCRId);
                                }
                            }
                        }
                    }

                    if (DCRModel.ReleasedStepId == (int)DCRReleasedSteps.Step1)
                    {
                        AddDCRHistory(DCRModel.DCRId, null, null, null, null, null, null, null, null, CommonService.objClsLoginInfo.Name + " promoted from incorporate ", CommonService.objClsLoginInfo.UserName);
                        UpdateLifeCycleStatus(DCRModel.DCRId, (int)DCRLifeCyclePolicyStep.InCorporate);
                        //pushDCRNotification(DCRModel.DCRId, DCRLifeCyclePolicyStep.InCorporate.ToString());
                    }
                    //pushDCRNotification(DCRModel.DCRId, DCRLifeCyclePolicyStep.ReviseDCR.ToString());
                    //pushDCRNotification(DCRModel.DCRId, DCRLifeCyclePolicyStep.ReviseDoc.ToString());
                    //pushDCRNotification(DCRModel.DCRId, DCRLifeCyclePolicyStep.RevisePart.ToString());
                    db.SaveChanges();

                    if (DCRModel.ReleasedStepId == (int)DCRReleasedSteps.Step3)
                    {
                        var _DES090List1 = db.DES090.Where(x => x.DCRId == DCRModel.DCRId && x.IsPartialSelect == null).FirstOrDefault();
                        var _DES094List1 = db.DES094.Where(x => x.DCRId == DCRModel.DCRId && x.IsPartialSelect == null).FirstOrDefault();
                        if (!(_DES090List1 != null) && !(_DES094List1 != null))
                        {
                            AddDCRHistory(DCRModel.DCRId, null, null, null, null, null, null, null, null, CommonService.objClsLoginInfo.Name + " promoted from incorporate ", CommonService.objClsLoginInfo.UserName);
                            UpdateLifeCycleStatus(DCRModel.DCRId, (int)DCRLifeCyclePolicyStep.InCorporate);
                            //pushDCRNotification(DCRModel.DCRId, DCRLifeCyclePolicyStep.InCorporate.ToString());
                        }
                    }
                    UpdateDCROwner(DCRModel.DCRId);
                    return _DOCModelList;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DCR_HISTORY_Result> GetDCRHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DCRId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DCR_HISTORY(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, DCRId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteDocFile(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    var checkFromChild = db.DES091.Where(yy => yy.DCRSupportedDocId == Id).FirstOrDefault();
                    if (checkFromChild != null)
                    {
                        db.DES091.Remove(checkFromChild);
                        db.SaveChanges();
                        return checkFromChild.DCRSupportedDocId;
                    }

                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public void UpdateDCROwner(Int64? DCRId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES089.Find(DCRId);
                    DES089 _DES089 = temp == null ? new DES089() : temp;
                    _DES089.Owner = CommonService.objClsLoginInfo.Name;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DCR_PENDINGTASK_LIST_Result> GetPendingDCRList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Roles, string Project, string psno)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DCR_PENDINGTASK_LIST(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Roles, Project, psno).ToList();

                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.DCRId);
                    });

                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateLifeCycleReturn(Int64? DCRId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    var _DES102List = db.DES102.Where(x => x.DCRId == DCRId && x.IsCompleted == true).OrderByDescending(x => x.DCRLifeCycleId).FirstOrDefault();
                    bool IsEvaluate = false;

                    _DES102List.IsCompleted = false;
                    _DES102List.CreatedBy = string.Empty;
                    _DES102List.CreatedOn = null;
                    //_DES102List.ForEach(b =>
                    //{
                    //    b.IsCompleted = false;
                    //    b.CreatedBy = string.Empty;
                    //    b.CreatedOn = null;
                    //});

                    if (_DES102List.PolicyOrderID == (int)DCRLifeCyclePolicyStep.Submit)
                    {
                        var _DES097 = db.DES097.Where(x => x.DCRId == DCRId && x.Status != ReviewOrEvaluate.Return.ToString()).FirstOrDefault();
                        if (_DES097 != null && _DES097.DCRReviewId > 0)
                        {
                            _DES097.Status = ReviewOrEvaluate.Return.ToString();
                            _DES097.EditedBy = CommonService.objClsLoginInfo.UserName;
                            _DES097.EditedOn = DateTime.Now;
                        }
                    }

                    if (_DES102List.PolicyOrderID == (int)DCRLifeCyclePolicyStep.Review)
                    {
                        var _DES108 = db.DES100.Where(x => x.DCRId == DCRId).ToList();
                        if (_DES108 != null && _DES108.Count > 0)
                        {
                            _DES108.ForEach(y =>
                            {
                                y.IsApproved = null;
                                y.IsCompleted = null;
                                y.Remark = null;
                                y.CreatedBy = null;
                                y.CreatedOn = null;
                            });
                        }
                        else
                        {
                            IsEvaluate = true;
                            var _DES102List1 = db.DES102.Where(x => x.DCRId == DCRId && x.PolicyOrderID == (int)DCRLifeCyclePolicyStep.Evaluate).FirstOrDefault();

                            _DES102List1.IsCompleted = false;
                            _DES102List1.CreatedBy = string.Empty;
                            _DES102List1.CreatedOn = null;
                        }
                        //if (_DES108 != null && _DES108.DCRReleaseId > 0)
                        //{
                        //    _DES108.Status = ReviewOrEvaluate.Return.ToString();
                        //    _DES108.EditedBy = CommonService.objClsLoginInfo.UserName;
                        //    _DES108.EditedOn = DateTime.Now;
                        //}
                    }


                    if (_DES102List.PolicyOrderID == (int)DCRLifeCyclePolicyStep.Evaluate || IsEvaluate)
                    {
                        var _DES098 = db.DES098.Where(x => x.DCRId == DCRId && x.Status != ReviewOrEvaluate.Return.ToString()).FirstOrDefault();
                        if (_DES098 != null && _DES098.DCREvaluateId > 0)
                        {
                            _DES098.Status = ReviewOrEvaluate.Return.ToString();
                            _DES098.EditedBy = CommonService.objClsLoginInfo.UserName;
                            _DES098.EditedOn = DateTime.Now;

                            //_DES098.DES100.ToList().ForEach(y =>
                            //{
                            //    db.DES101.RemoveRange(y.DES101);
                            //});
                        }
                    }



                    if (_DES102List.PolicyOrderID == (int)DCRLifeCyclePolicyStep.Create)
                    {
                        UpdateDCRStatus(DCRId, ObjectStatus.Created.ToString());
                    }
                    //UpdateDCRStatus(DCRId, ObjectStatus.Created.ToString());
                    pushDCRNotification(DCRId, ReviewOrEvaluate.Return.ToString());
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public void pushDCRNotification(Int64? DCRId, string ObjectType)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    #region Notification 
                    var _DES089 = db.DES089.Find(DCRId);
                    try
                    {
                        db.SP_DES_DCR_PUSH_NOTIFICATION(_DES089.Project, CommonService.objClsLoginInfo.UserName, ObjectType, DCRId);
                    }
                    catch
                    {
                    }

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

            #endregion
        }

        public DCRModel GetDCRCommentPersonModel(DCRModel model)
        {
            {
                try
                {
                    using (IEMQSDESEntities db = new IEMQSDESEntities())
                    {
                        DCRModel _DES089 = new DCRModel();
                        var _DES100List = db.DES100.Where(x => x.DCRId == model.DCRId && x.DCREvaluateId == model.DCREvaluateModel.DCREvaluateId).ToList();
                        if (_DES100List != null && _DES100List.Count > 0)
                        {
                            _DES089.DepartmentGroupList = _DES100List.Select(z => new DCREvaluateCommentModel
                            {
                                DCREvaluateId = z.DCREvaluateId,
                                DCREvaluateCommentId = z.DCREvaluateCommentId,
                                DCRId = z.DCRId,
                                DeptId = z.DeptId,
                                IsApproved = z.IsApproved,
                                IsCompleted = z.IsCompleted,
                                Remark = z.Remark,
                                PSNoList = z.DES101.Select(a => a.PSNumber).ToList(),
                            }).ToList();
                        }
                        return _DES089;
                    }
                }
                catch (Exception ex)
                {
                    CommonService.SendErrorToText(ex);
                    throw;
                }
            }
        }

        public void UpdatePartDocRevised(int ObjectId, Int64? oldId, Int64? newId, Int64? DCRId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (ObjectId == (int)FolderObjectName.Part)
                    {
                        var _DES094 = db.DES094.Where(m => m.DCRId == DCRId && m.ItemId == oldId).FirstOrDefault();
                        _DES094.ItemId = newId;
                    }
                    else if (ObjectId == (int)FolderObjectName.DOC)
                    {
                        var _DES075old = db.DES075.Where(x => x.DocumentId == oldId).FirstOrDefault();
                        var _DES075new = db.DES075.Where(x => x.DocumentId == newId).FirstOrDefault();

                        var _DES090 = db.DES090.Where(m => m.DCRId == DCRId && m.JEPDocumentDetailsId == _DES075old.JEPDocumentDetailsId).FirstOrDefault();
                        _DES090.JEPDocumentDetailsId = _DES075new.JEPDocumentDetailsId;
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public DES089 GetDCRIdByDCRSupportedDocId(Int64 DCRSupportedDocId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var objDCR091 = db.DES091.FirstOrDefault(i => i.DCRSupportedDocId == DCRSupportedDocId);
                    if (objDCR091 != null)
                    {
                        var objDES089 = db.DES089.FirstOrDefault(i => i.DCRId == objDCR091.DCRId);
                        return objDES089;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public string PolicyStepStatus(Int64? DCRId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string PolicyStep = string.Empty;
                    var getReview = db.DES102.Where(x => x.DCRId == DCRId && x.PolicyStep == "Review" && x.IsCompleted != true).FirstOrDefault();
                    PolicyStep = db.DES102.Where(x => x.DCRId == DCRId && x.IsCompleted != true).OrderBy(x => x.PolicyOrderID).Select(x => x.PolicyStep).FirstOrDefault();
                    return PolicyStep;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public string Identical(string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES051.Where(x => x.Project == Project).Select(x => x.IdenticalGroup).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        #endregion
    }
}