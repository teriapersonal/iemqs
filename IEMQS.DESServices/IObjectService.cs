﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IObjectService
    {
        Int64 AddEdit(ObjectModel model, out string errorMsg);
        ObjectModel GetObjectbyId(Int64 Id);
        List<ObjectModel> GetAllObject();
        List<SP_DES_GET_OBJECT_Result> GetObjectList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        Int64 DeleteObject(Int64 Id, out string errorMsg);
        bool UpdateApplyBU(Int64 Id,bool ApplyBU);
    }
}
