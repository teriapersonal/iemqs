﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Web.Mvc;

namespace IEMQS.DESServices
{
    public interface ICommonService
    {
        List<SelectListItem> GetMenuType();
        List<LookupModel> GetLookupData(string Type);

        List<SelectListItem> GetOrderPolicy(string selected = null);

        List<SelectListItem> GetItemKeyType(string selected = null);

        List<SelectListItem> GetPartAction(string selected = null);

        List<SelectListItem> GetLocation();

        List<SelectListItem> GetItemType(Int64? ProductFormId,string selected = null);
        List<SelectListItem> GetDefaultItemType(string selected = null);
        List<SelectListItem> GetActionforPCRelation(string selected = null);
        List<SelectListItem> GetYesOrNo();

        bool SendMail(string MailToAdd, string MailCc, string MailBcc, string MailSub, string MailBody, string MailAttachment, AttachmentCollection MailAttachMentCollection, bool IsAttachmentDelete);

        Nullable<int> GetUserLevel(string RoleGroup, string roles);
        List<SelectListItem> GetObjectName();

        List<SelectListItem> GetCodeStamp();

        List<SelectListItem> GetObjectStatus();

        List<SP_DES_GET_USER_BY_ROLES_Result> GetUserByRole(string Roles);
    }
}
