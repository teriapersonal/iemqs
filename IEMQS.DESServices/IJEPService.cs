﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Mvc;

namespace IEMQS.DESServices
{
    public interface IJEPService
    {
        JEPModel GetManitainJEPbyId(Int64 Id);

        Int64 AddEdit(JEPModel model);

        Int64 DeleteMaintainJEP(Int64 Id, out string errorMsg);

        JEPModel SetDefaultAddData(JEPModel model);
        string GenerateJEPNumber(string Project);

        List<ProductModel> GetAllProductByBUId(Int64 BUId);

        List<SP_DES_GET_JEPLISTPROJECTWISE_Result> GetJEPListByProject(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, Int64? FolderId, string Roles);

        List<SP_DES_GET_DOCUMENTLIST_Result> GetDocumentList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64 JEPId);

        Int64 AddEditDocumentList(JEPDocumentModel model, Int64 BUId, string Project, out string Msg);

        List<JEPProjectwiseDocument> GetJEPTemplatebyProject(string Project);


        bool AddDocumentListByProductANDTemplate(string Project, Int64 Product, Int64 JEPTemplateId, Int64 JEPId, string project, Int64? BUId, out string Msg);

        Int64? DeleteDocument(Int64? Id, out string errorMsg);

        bool AddDocumentListByProjectwise(Int64 JEPDocumentDetailsId, Int64 JEPId, string project, Int64? BUId, out string Msg);

        List<SP_DES_GET_ROLE_Result> GetAllRole();

        bool AddEditJEPStatus(Int64 JEPId, string Project, out string errmsg);

        List<SP_DES_GET_GLOBAL_JEP_DOC_BY_PROJECT_Result> GetAllGlobalDocumentListByProject(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string DocumentNo, string CurrentProject);

        bool AddLinkDocument(Int64 DocumentId, Int64 JEPId, out string Msg);

        List<JEPPolicyStepsModel> GetPolicySteps(string Type, Int64? BUId, Int64? JEPId);

        ProjectModel GetProjectDetails(string Project);
        JEPModel GetJEPdetail(Int64? JEPId);

        List<SP_DES_GET_JEP_POLICY_BYLEVEL_Result> GetJEPUserLevel(string RoleGroup, string Roles, Int64? JEPId);

        Int64? ReviseJEPPolicy(string ReviseDOCPolicy, Int64 JEPId, string Project, out string errorMsg);
        Int64? UpdateJEPPolicy(Int64 JEPLifeCycleId, string Project, out string errorMsg, out string infoMsg);

        #region Customer FeedBack
        List<Agencies> GetAllAgency();
        List<CustomerFeedback> GetDocumentDetailsWithRevision(Int64? AgencyId, Int64? JEPId, string project);

        Int64 AddEditCustomerFeedback(CustomerFeedback model, out string errorMsg);
        CustomerFeedback JEPGetCustomerDetail(Int64? Id, Int64? CFId);
        List<SP_DES_GET_CUSTOMER_FEEDBACK_DOCUMENT_Result> GetCustomerFeedbackDocumentList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string GroupId, Int64? JEPcustomerFeedbackId);
        long UploadCustomerFeedbackFiles(DocumentMapping model, out string errorMsg);
        CustomerFeedback GetCustomerFeedbackByDOCId_AGSId(Int64? JEPDocumentDetailsId, Int64? AgencyId, string Project);
        bool DeleteCustomerFeedbackDocs(Int64? Id, out string errorMsg, Int64? JEPId);
        List<SelectListItem> GetContractProjectList(string contract);
        List<SP_DES_GET_ALL_CUSTOMER_FEEDBACK_LIST_Result> GetAllCustomerFeedbackList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? JepId, out int recordsTotal);

        #endregion

        bool AddJEPLinkDocuments(JEPGlobalDoc Model, string Project, out string Msg, out string SuccessMsg);

        List<SP_DES_GET_PREVIEW_JEP_DOC_BY_PRODUCT_Result> BindpreviewProductJEPDoc(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64 JEPTemplateId);

        bool EditJEPDocument(JEPModel model);

        List<Tuple<Int64, double>> GetWeightage(Int64? JEPId);

        bool calculateWeightage(Int64 JEPId, out string errmsg);

        List<SP_DES_GET_CUSTOMER_FEEDBACK_Result> GetCustomerFeedbackGrid(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, Int64? AgencyId, out int recordsTotal);

        List<TransmittalModel> GetTransmittalAgencyList(string Project);
        int AddCustFeedbackByAgency(Int64? Agency);

        bool EditCustFeedback(CustomerFeedback model, out string errmsg);

        //bool IsValidateExcelRecords(string Project, Int64 JepID, string DocNo, string Revision, string srcFolder, string FileName, string DestFolder, out string strMsg);

        bool TempInsert(string SrcRevision, int? DestRevision, string Project, string DestFolder, string DocNo, out string strMsg);

        Int64? CheckDestFolder(string Project, string[] FolderName);

        //Int64 AddBulkDocument(string Project, Int64 JepID, string JEPNumber, string DocNo, string DocType, string Description, int Revision,
        //    string srcFolder, string FileName, string DestFolder, string CustDocNo, string CustRevNo, string Dept, DateTime? PlannedDate,
        //    string Weightage, int Milestone, Int64? FolderId, int Location, string RoleGroup, string ReturnNotes, int FileLocation,
        //    string PSNo, Int64? BUId, string IPAddress, out string strMsg);

        string AddImportedData(DataTable dt, string ProjectNo, IpLocation ipLocation, string RoleGroup, string PSNo, Int64? BUId, int Location, Int64 JEPId,  string FileName, string CurrentDepartment, out List<string> errmsg,out string AddImportedData);

        bool IsCheckInJEP(string PsNo, Int64? JEPId,out string GetCheckInPsName);

        bool UpdateIsCheckInJEP(Int64? JEPId, bool IsJEPCheckIn);

        bool IsCheckInAgency(string PsNo, Int64? AgencyId);
        string AgencyCheckInByPersonName(Int64? AgencyId);

        bool ExportToExcel(Int64? JEPId);

        bool IsJEPHasDocument(Int64? JEPId,string Department, out string errorMSG);
    }
}