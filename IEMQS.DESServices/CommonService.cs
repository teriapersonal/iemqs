﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQSImplementation;
using IEMQSImplementation.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.DESServices
{
    public class CommonService : ICommonService
    {
        #region Encrypt Decrypt Code(Don't change anything, it affact all application)
        private const string secretkey = "IEMQS_2_0";
        public static string Encrypt(string plainText)
        {

            byte[] EncryptKey = { };
            byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
            EncryptKey = System.Text.Encoding.UTF8.GetBytes(secretkey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            byte[] inputByte = Encoding.UTF8.GetBytes(plainText);
            MemoryStream mStream = new MemoryStream();
            CryptoStream cStream = new CryptoStream(mStream, des.CreateEncryptor(EncryptKey, IV), CryptoStreamMode.Write);
            cStream.Write(inputByte, 0, inputByte.Length);
            cStream.FlushFinalBlock();
            return Convert.ToBase64String(mStream.ToArray());
        }

        public static string Decrypt(string encryptedText)
        {

            byte[] DecryptKey = { };
            byte[] IV = { 55, 34, 87, 64, 87, 195, 54, 21 };
            byte[] inputByte = new byte[encryptedText.Length];

            DecryptKey = System.Text.Encoding.UTF8.GetBytes(secretkey.Substring(0, 8));
            DESCryptoServiceProvider des = new DESCryptoServiceProvider();
            inputByte = Convert.FromBase64String(encryptedText);
            MemoryStream ms = new MemoryStream();
            CryptoStream cs = new CryptoStream(ms, des.CreateDecryptor(DecryptKey, IV), CryptoStreamMode.Write);
            cs.Write(inputByte, 0, inputByte.Length);
            cs.FlushFinalBlock();
            System.Text.Encoding encoding = System.Text.Encoding.UTF8;
            return encoding.GetString(ms.ToArray());
        }
        #endregion

        #region Error Management
        private static String ErrorlineNo, Errormsg, extype, exurl, hostIp, ErrorLocation, HostAdd, ErrormsgLong, FileLocation = "~/Uploads/ErrorLog/";

        public static void SendErrorToText(Exception ex)
        {
            var line = Environment.NewLine + Environment.NewLine;

            ErrorlineNo = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
            Errormsg = ex.GetType().Name.ToString();
            ErrormsgLong = ex.StackTrace;
            extype = ex.GetType().ToString();
            exurl = HttpContext.Current.Request.Url.ToString();
            ErrorLocation = ex.Message.ToString();

            try
            {
                string filepath = HttpContext.Current.Server.MapPath(FileLocation);  //Text File Path

                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);

                }
                filepath = filepath + DateTime.Now.ToString("dd_MM_yyyy") + "_errorLog.txt";   //Text File Name
                if (!File.Exists(filepath))
                {


                    File.Create(filepath).Dispose();

                }
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    string error = "Log Written Date:" + " " + DateTime.Now.ToString() + line + "Error Line No :" + " " + ErrorlineNo + line + "Error Message:" + " " + Errormsg + line + "Error Message Full:" + " " + ErrormsgLong + line + "Exception Type:" + " " + extype + line + "Error Location :" + " " + ErrorLocation + line + " Error Page Url:" + " " + exurl + line + "User Host IP:" + " " + hostIp + line;
                    sw.WriteLine("-----------Exception Details on " + " " + DateTime.Now.ToString() + "-----------------");
                    sw.WriteLine("-------------------------------------------------------------------------------------");
                    sw.WriteLine(line);
                    sw.WriteLine(error);
                    sw.WriteLine("--------------------------------*End*------------------------------------------");
                    sw.WriteLine(line);
                    sw.Flush();
                    sw.Close();

                }

            }
            catch (Exception e)
            {
                e.ToString();

            }
        }

        public static void Error(string ex)
        {
            var line = Environment.NewLine + Environment.NewLine;


            string Errormsg = ex;
            //extype = ex;
            string exurl = HttpContext.Current.Request.Url.ToString();

            try
            {
                string filepath = HttpContext.Current.Server.MapPath(FileLocation);  //Text File Path

                if (!Directory.Exists(filepath))
                {
                    Directory.CreateDirectory(filepath);

                }
                filepath = filepath + DateTime.Now.ToString("dd_MM_yyyy") + "_errorLog.txt";   //Text File Name
                if (!File.Exists(filepath))
                {


                    File.Create(filepath).Dispose();

                }
                using (StreamWriter sw = File.AppendText(filepath))
                {
                    string error = "Log Written Date:" + " " + DateTime.Now.ToString() + line + "Message:" + " " + Errormsg + line + "Message Page Url:" + " " + exurl + line;
                    sw.WriteLine("-----------Message Details on " + " " + DateTime.Now.ToString() + "-----------------");
                    sw.WriteLine("-------------------------------------------------------------------------------------");
                    sw.WriteLine(line);
                    sw.WriteLine(error);
                    sw.WriteLine("--------------------------------*End*------------------------------------------");
                    sw.WriteLine(line);
                    sw.Flush();
                    sw.Close();

                }

            }
            catch (Exception e)
            {
                e.ToString();

            }
        }
        #endregion

        public static clsLoginInfo objClsLoginInfo
        {
            get
            {
                var s = System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
                if (s != null)
                {
                    return (clsLoginInfo)s;
                }
                return new clsLoginInfo();

            }
        }

        public static IpLocation GetUseIPConfig
        {
            get
            {
                try
                {
                    IpLocation ipLocation = new IpLocation();
                    string ipAdd = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                    if (string.IsNullOrEmpty(ipAdd))
                    {
                        ipAdd = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                    }
                    ipAdd = "10.1.1.1";
                    //Error(ipAdd);
                    ipLocation = GetUserIPConfigModel(ipAdd);

                    return ipLocation;
                }
                catch (Exception ex)
                {
                    CommonService.SendErrorToText(ex);
                    return null;
                }

            }
        }

        public static uint IPAddressToLongBackwards(string IPAddr)
        {
            System.Net.IPAddress oIP = System.Net.IPAddress.Parse(IPAddr);
            byte[] byteIP = oIP.GetAddressBytes();


            uint ip = (uint)byteIP[0] << 24;
            ip += (uint)byteIP[1] << 16;
            ip += (uint)byteIP[2] << 8;
            ip += (uint)byteIP[3];

            return ip;
        }

        public static string GetDeptIdByPSNo(string PsNo)
        {
            try
            {
                using (IEMQSEntitiesContext dbContext = new IEMQSEntitiesContext())
                {
                    if (!string.IsNullOrEmpty(PsNo))
                    {
                        return dbContext.COM003.Where(t => t.t_psno == PsNo).FirstOrDefault().t_depc;
                    }
                    else
                    {
                        return "0";
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public static IpLocation GetUserIPConfigModel(string ipAdd)
        {
            List<UserIPConfigModel> userIPConfigModel = new List<UserIPConfigModel>();
            string GetLocation = string.Empty;
            IpLocation iplocation = new IpLocation();
            CommonService cs = new CommonService();
            string Location = string.Empty;
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    userIPConfigModel = db.DES011.Select(x => new UserIPConfigModel
                    {
                        IPRangeId = x.IPRangeId,
                        IPStart = x.IPStart,
                        IPEnd = x.IPEnd,
                        Location = x.Location,
                        IP_SubnetMask = x.IP_SubnetMask,
                        VlenName = x.VlenName

                    }).ToList();
                }

                foreach (var item in userIPConfigModel)
                {
                    if (IPAddressToLongBackwards(ipAdd) >= IPAddressToLongBackwards(item.IPStart) && IPAddressToLongBackwards(item.IPEnd) >= IPAddressToLongBackwards(ipAdd))
                    {
                        Location = cs.GetLocation().Where(x => x.Value == item.Location.ToString()).Select(x => x.Text).FirstOrDefault();
                    }
                }
                GetCommonIPConfig getCommonIPConfig = new GetCommonIPConfig();
                GetCommonPathConfig getCommonPathConfig = new GetCommonPathConfig();
                if (Location == LocationType.Hazira.ToString())
                {
                    iplocation.Ip = getCommonIPConfig.Hazira;
                    iplocation.Location = (int)LocationType.Hazira;
                    iplocation.File_Path = getCommonPathConfig.Hazira;
                }
                else if (Location == LocationType.Powai.ToString())
                {
                    iplocation.Ip = getCommonIPConfig.Powai;
                    iplocation.Location = (int)LocationType.Powai;
                    iplocation.File_Path = getCommonPathConfig.Powai;
                }
                else if (Location == LocationType.Ranoli.ToString())
                {
                    iplocation.Ip = getCommonIPConfig.Ranoli;
                    iplocation.Location = (int)LocationType.Ranoli;
                    iplocation.File_Path = getCommonPathConfig.Ranoli;
                }

                return iplocation;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectListItem> GetMenuType()
        {
            try
            {
                return Enum.GetValues(typeof(MenuType)).Cast<MenuType>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = ((int)v).ToString()
                }).ToList();

            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<LookupModel> GetLookupData(string Type)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES026.Where(x => x.Type == Type).OrderBy(x => x.Id).Select(x => new LookupModel
                    {
                        Id = x.Id,
                        Lookup = x.Lookup,
                        Description = x.Description
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectListItem> GetOrderPolicy(string selected = null)
        {
            try
            {
                return Enum.GetValues(typeof(OrderPolicy)).Cast<OrderPolicy>().Select(v => new SelectListItem
                {
                    Text = v.ToString().Replace("_", " "),
                    Value = v.ToString().Replace("_", " "),
                    Selected = v.ToString().Replace("_", " ") == selected
                }).ToList();

            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectListItem> GetItemKeyType(string selected = null)
        {
            try
            {
                return Enum.GetValues(typeof(ItemKeyType)).Cast<ItemKeyType>().Where(x => x.ToString() != ItemKeyType.AutoGenerate_FindNo.ToString()).Select(v => new SelectListItem
                {
                    Text = v.ToString().Replace("_", " "),
                    Value = v.ToString().Replace("_", " "),
                    Selected = v.ToString().Replace("_", " ") == selected
                }).ToList();

            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectListItem> GetPartAction(string selected = null)
        {
            try
            {
                return Enum.GetValues(typeof(PartAction)).Cast<PartAction>().Select(v => new SelectListItem
                {
                    Text = v.ToString().Replace("_", " "),
                    Value = v.ToString().Replace("_", " "),
                    Selected = v.ToString().Replace("_", " ") == selected
                }).ToList();

            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectListItem> GetLocation()
        {
            try
            {
                return Enum.GetValues(typeof(LocationType)).Cast<LocationType>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = ((int)v).ToString()
                }).ToList();

            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectListItem> GetItemType(Int64? ProductFormId, string selected = null)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<SelectListItem> list = Enum.GetValues(typeof(ItemType)).Cast<ItemType>().Select(v => new SelectListItem
                    {
                        Text = v.ToString().Replace("_", " "),
                        Value = v.ToString().Replace("_", " "),
                        Selected = v.ToString().Replace("_", " ") == selected
                    }).ToList();
                    List<string> PMGroles = new List<string>{ "PLNG1"
                                                                ,"PLNG2"
                                                                ,"PLNG3"
                                                                ,"PMG1"
                                                                ,"PMG2"
                                                                ,"PMG3"};
                    if (CommonService.objClsLoginInfo.ListRoles.Any(y => PMGroles.Contains(y)))
                    {
                        list = list.Where(x => x.Text == ItemType.Jigfix.ToString()).ToList();
                    }
                    if (ProductFormId > 0)
                    {
                        var data = db.DES028.Where(x => x.ProductFormCodeId == ProductFormId).Select(x => x.ProductForm).FirstOrDefault();
                        if (!string.IsNullOrEmpty(data))
                        {
                            var types = db.DES040.Where(x => x.ProductForm == data).FirstOrDefault();
                            if (types != null && (!string.IsNullOrEmpty(types.ItemType)))
                            {
                                List<string> tlist = types.ItemType.Split(',').Select(x => x.Trim()).ToList();
                                list = list.Where(x => tlist.Contains(x.Value)).ToList();
                            }
                        }
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectListItem> GetDefaultItemType(string selected = null)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<SelectListItem> list = Enum.GetValues(typeof(ItemType)).Cast<ItemType>().Select(v => new SelectListItem
                    {
                        Text = v.ToString().Replace("_", " "),
                        Value = v.ToString().Replace("_", " "),
                        Selected = v.ToString().Replace("_", " ") == selected
                    }).ToList();
                    return list;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
        public List<SelectListItem> GetActionforPCRelation(string selected = null)
        {
            try
            {
                return Enum.GetValues(typeof(ActionforPCRelation)).Cast<ActionforPCRelation>().Select(v => new SelectListItem
                {
                    Text = v.ToString().Replace("_", " "),
                    Value = v.ToString().Replace("_", " "),
                    Selected = v.ToString().Replace("_", " ") == selected
                }).ToList();

            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectListItem> GetYesOrNo()
        {
            try
            {
                return Enum.GetValues(typeof(YesOrNo)).Cast<YesOrNo>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = v.ToString(),
                }).ToList();

            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool SendMail(string MailToAdd, string MailCc, string MailBcc, string MailSub, string MailBody, string MailAttachment, AttachmentCollection MailAttachMentCollection, bool IsAttachmentDelete)
        {
            MailMessage mm = new MailMessage();
            try
            {
                SmtpClient sc = new SmtpClient(ConfigurationManager.AppSettings["SMTPClient"]);

                /* HTML Modification start */

                StringBuilder _strBuilder = new StringBuilder();
                StringBuilder _strSubject = new StringBuilder();
                string strBody = string.Empty;
                string strsubject = string.Empty;

                if (MailBody != null)
                {
                    //FROM
                    mm.From = new MailAddress(ConfigurationManager.AppSettings["FromMail"].ToString());
                    //mm.ReplyTo = new MailAddress(tomail);


                    //To
                    string[] _strArr;
                    if (!string.IsNullOrEmpty(MailToAdd))
                    {
                        _strArr = MailToAdd.ToString().Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string _str in _strArr)
                            mm.To.Add(new MailAddress(_str));
                    }

                    //CC
                    string[] _strCC;
                    if (!string.IsNullOrEmpty(MailCc))
                    {
                        _strCC = MailCc.ToString().Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string _str in _strCC)
                            mm.CC.Add(new MailAddress(_str));

                    }



                    //BCC
                    string[] _strBcc;
                    if (!string.IsNullOrEmpty(MailBcc))
                    {
                        _strBcc = MailBcc.ToString().Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string _str in _strBcc)
                            mm.Bcc.Add(new MailAddress(_str));
                    }



                    //Sub
                    if (!string.IsNullOrEmpty(MailSub))
                        strsubject = MailSub;


                    //Body
                    if (!string.IsNullOrEmpty(MailBody))
                        strBody = MailBody;


                    //finding dynamic value from the body and replace it with Hash key value
                    if (strBody != string.Empty)
                    {
                        _strBuilder = new StringBuilder(strBody);
                        _strSubject = new StringBuilder(strsubject);
                        //if (ht != null && ht.Count > 0)
                        //{
                        //    foreach (string key in ht.Keys)
                        //    {
                        //        if (strBody.Contains(key))
                        //        {
                        //            if (key == "[Subject]" && string.IsNullOrEmpty(ht[key].ToString()))
                        //            {
                        //                _strBuilder.Replace(key.ToString(), mm.Subject);
                        //            }
                        //            else if (ht[key] != null && ht[key].ToString() != "")
                        //            {
                        //                _strBuilder.Replace(key.ToString(), ht[key].ToString());
                        //            }
                        //            else
                        //            {
                        //                _strBuilder.Replace(key.ToString(), "---");
                        //            }
                        //        }

                        //        if (strsubject.Contains(key))
                        //        {
                        //            _strSubject.Replace(key.ToString(), ht[key].ToString());
                        //        }
                        //    }
                        //}
                    }
                    mm.Body = _strBuilder.ToString();
                    mm.Subject = _strSubject.ToString();
                    if (!string.IsNullOrEmpty(MailAttachment))
                    {
                        Attachment a = new Attachment(MailAttachment);
                        mm.Attachments.Add(a);
                    }
                    //foreach (Attachment at in MailAttachMentCollection)
                    //{
                    //    mm.Attachments.Add(at);
                    //}
                    mm.IsBodyHtml = true;
                    mm.Priority = MailPriority.Normal;
                    sc.Send(mm);

                    mm.To.Clear();
                }
                return true;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                //return false;
                throw;
            }
            finally
            {
                if (IsAttachmentDelete && !string.IsNullOrEmpty(MailAttachment) && (MailAttachment.ToUpper().Contains("EMAILFILES") || MailAttachment.ToUpper().Contains("EXPORTFILES")))
                {
                    System.IO.FileInfo f = new System.IO.FileInfo(MailAttachment);
                    mm.Dispose();
                    f.Delete();
                }
                mm.Dispose();
            }
        }

        /// <summary>
        /// Generate Folder Breadcrumbs of selected FolderId
        /// </summary>
        /// <param name="ProjectFolderList"></param>
        /// <param name="FolderId"></param>
        /// <returns></returns>
        public static string fnFolderBreadcrumbs(ProjectFolderModel ProjectFolderList, Int64 FolderId)
        {
            string strHtml = "";
            try
            {
                List<Int64?> _lst = fnFolderIdList(ProjectFolderList, FolderId);

                #region Make string of Folder breadcurmbs
                strHtml += "<ul class=\"m-subheader__breadcrumbs m-nav m-nav--inline\"> " +
                                 "   <li class=\"m-nav__item m-nav__item--home\"> " +
                                 "      <a href = \"#\" class=\"m-nav__link m-nav__link--icon\"> " +
                                 "          <span class=\"m-nav__link-text\"><span class=\"m--font-bolder\">" + ProjectFolderList.ProjectNo + "</span></span> " +
                                 "      </a> " +
                                 "  </li>";

                for (int i = (_lst.Count - 1); i >= 0; i--)
                {
                    var _obj = ProjectFolderList.FolderList.Where(w => w.ProjectFolderId == _lst[i].Value).FirstOrDefault();
                    if (_obj != null)
                    {
                        strHtml += "<li class=\"m-nav__separator\">-</li> " +
                                   "  <li class=\"m-nav__item\"> " +
                                   "    <a href = \"/DES/Project/Folder?q=" + CommonService.Encrypt("Id=" + _obj.ProjectFolderId) + "\" class=\"m-nav__link\"> " +
                                   "        <span class=\"m-nav__link-text\">" + _obj.FolderName + "</span> " +
                                   "    </a> " +
                                   "</li>";
                    }

                }

                strHtml += "</ul>";

                #endregion
            }
            catch (Exception ex)
            {
                strHtml = "";
                CommonService.SendErrorToText(ex);
                throw;
            }
            return strHtml;
        }

        public static List<Int64?> fnFolderIdList(ProjectFolderModel ProjectFolderList, Int64 FolderId)
        {
            try
            {
                #region FILD ALL PARENT OF FOLDERID
                List<Int64?> _lst = new List<Int64?>();
                _lst.Add(FolderId);
                if (ProjectFolderList.FolderList.Count > 0)
                {
                    Int64? iParent = FolderId;
                    while (iParent > 0 && iParent != null)
                    {
                        var obj = ProjectFolderList.FolderList.Where(w => w.ProjectFolderId == iParent && w.ParentProjectFolderId != null).FirstOrDefault();
                        if (obj != null)
                        {
                            iParent = obj.ParentProjectFolderId;
                            _lst.Add(iParent);
                        }
                        else
                            break;
                    }
                }
                #endregion

                return _lst;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return new List<long?>();
            }
        }

        public static SP_DES_GET_PROJECT_FOLDERLIST_Result fnFolderRoleGroup(ProjectFolderModel ProjectFolderList, Int64 FolderId)
        {
            SP_DES_GET_PROJECT_FOLDERLIST_Result model = new SP_DES_GET_PROJECT_FOLDERLIST_Result();
            try
            {
                if (ProjectFolderList.FolderList.Count > 0)
                {
                    Int64? iParent = FolderId;
                    var f = ProjectFolderList.FolderList.Where(w => w.ProjectFolderId == FolderId).FirstOrDefault();
                    if (f != null)
                        model = f;
                    while (iParent > 0 && iParent != null)
                    {
                        f = ProjectFolderList.FolderList.Where(w => w.ProjectFolderId == iParent).FirstOrDefault();
                        if (f != null)
                            model = f;
                        var obj = ProjectFolderList.FolderList.Where(w => w.ProjectFolderId == iParent && w.ParentProjectFolderId != null).FirstOrDefault();
                        if (obj != null)
                        {
                            iParent = obj.ParentProjectFolderId;

                        }
                        else
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
            return model;
        }

        public static List<Int64?> fnFolderIdList(Int64 FolderId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    #region FILD ALL PARENT OF FOLDERID
                    List<Int64?> _lst = new List<Int64?>();
                    _lst.Add(FolderId);

                    Int64? iParent = FolderId;
                    while (iParent > 0 && iParent != null)
                    {
                        var obj = db.DES008.Where(w => w.FolderId == iParent && w.ParentFolderId != null).FirstOrDefault();
                        if (obj != null)
                        {
                            iParent = obj.ParentFolderId;
                            _lst.Add(iParent);
                        }
                        else
                            break;
                    }

                    #endregion

                    return _lst;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return new List<long?>();
            }

        }

        public Nullable<int> GetUserLevel(string RoleGroup, string roles)
        {
            using (IEMQSDESEntities db = new IEMQSDESEntities())
            {
                return db.SP_DES_GET_USER_LEVEL(RoleGroup, roles).FirstOrDefault();
            }

        }

        public List<SelectListItem> GetObjectName()
        {
            try
            {
                var _GetObjectName = Enum.GetValues(typeof(FolderObjectName)).Cast<FolderObjectName>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = ((int)v).ToString()
                }).ToList();
                return _GetObjectName;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectListItem> GetCodeStamp()
        {
            try
            {
                var _GetObjectName = Enum.GetValues(typeof(CodeStamp)).Cast<CodeStamp>().Select(v => new SelectListItem
                {
                    Text = v.ToString().Contains("_") ? v.ToString().Replace('_', ' ') : v.ToString(),
                    Value = v.ToString().Contains("_") ? v.ToString().Replace('_', ' ') : v.ToString()
                }).ToList();
                return _GetObjectName;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }




        public List<SelectListItem> GetObjectStatus()
        {
            try
            {
                var _GetObjectStatus = Enum.GetValues(typeof(ObjectStatus)).Cast<ObjectStatus>().Select(v => new SelectListItem
                {
                    Text = v.ToString(),
                    Value = ((int)v).ToString()
                }).ToList();
                return _GetObjectStatus;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public static List<MasterMenu> GetMasterMenu(string UserRole, string Location)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.SP_DES_GET_MASTER_MENU(UserRole, Location).Select(x => new MasterMenu
                    {
                        Id = x.Id,
                        Process = x.Process,
                        Description = x.Description,
                        Area = x.Area,
                        Controller = x.Controller,
                        Action = x.Action,
                        ParentId = x.ParentId,
                        MainMenuOrderNo = x.MainMenuOrderNo,
                        OrderNo = x.OrderNo
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public static bool CompleteDCR(string ItemKey, string DocNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<string> ListItemKeys = new List<string>();
                    List<string> ListDocNo = new List<string>();


                    if (!string.IsNullOrEmpty(ItemKey))
                    {
                        var _DES094List = db.DES094.Where(x => x.DES066.ItemKey == ItemKey && x.DES089.Status != ObjectStatus.Completed.ToString()).ToList();
                        foreach (var item in _DES094List)
                        {
                            int? temp = 0;
                            int? temp1 = 0;

                            if (item.DES089 != null && (item.DES089.ReleasedStepId == (int)DCRReleasedSteps.Step1 || item.DES089.ReleasedStepId == (int)DCRReleasedSteps.Step3))
                            {
                                var _D94 = db.DES094.Where(x => x.DCRId == item.DCRId).ToList();
                                if (_D94 != null && _D94.Count > 0)
                                {
                                    ListItemKeys = _D94.Select(y => y.DES066.ItemKey).ToList();
                                    temp = db.DES066.Where(x => ListItemKeys.Contains(x.ItemKey) && x.Status != ObjectStatus.Completed.ToString()).Count();
                                }
                                var _D90 = db.DES090.Where(x => x.DCRId == item.DCRId).ToList();
                                if (_D90 != null && _D90.Count > 0)
                                {
                                    ListDocNo = _D90.Select(y => y.DES075.DocNo).ToList();
                                    temp1 = db.DES059.Where(x => ListDocNo.Contains(x.DocumentNo) && x.Status != ObjectStatus.Completed.ToString()).Count();
                                }

                                if ((!(temp > 0)) && (!(temp1 > 0)))
                                {
                                    Int64? icount = 0;
                                    if (item.DES089.ReleasedStepId == (int)DCRReleasedSteps.Step3)
                                    {
                                        icount = _D94.Where(x => (x.IsPartialSelect ?? false) != true).Count();
                                    }
                                    if ((icount ?? 0) == 0)
                                    {
                                        new DCRService().UpdateDCRStatus(item.DES089.DCRId, ObjectStatus.Completed.ToString());
                                        new DCRService().UpdateLifeCycleStatus(item.DES089.DCRId, (int)DCRLifeCyclePolicyStep.Complete);
                                        new DCRService().AddDCRHistory(item.DES089.DCRId, null, null, null, null, null, null, null, null, "DCR Completed", CommonService.objClsLoginInfo.UserName);
                                        new DCRService().pushDCRNotification(item.DES089.DCRId, DCRLifeCyclePolicyStep.InCorporate.ToString());
                                    }
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(DocNo))
                    {
                        var _DES090List = db.DES090.Where(x => x.DES075.DocNo == DocNo && x.DES089.Status != ObjectStatus.Completed.ToString()).ToList();

                        foreach (var item in _DES090List)
                        {
                            int? temp = 0;
                            int? temp1 = 0;

                            if (item.DES089 != null && (item.DES089.ReleasedStepId == (int)DCRReleasedSteps.Step1 || item.DES089.ReleasedStepId == (int)DCRReleasedSteps.Step3))
                            {
                                var _D94 = db.DES094.Where(x => x.DCRId == item.DCRId).ToList();
                                if (_D94 != null && _D94.Count > 0)
                                {
                                    ListItemKeys = _D94.Select(y => y.DES066.ItemKey).ToList();
                                    temp = db.DES066.Where(x => ListItemKeys.Contains(x.ItemKey) && x.Status != ObjectStatus.Completed.ToString()).Count();
                                }

                                var _D90 = db.DES090.Where(x => x.DCRId == item.DCRId).ToList();
                                if (_D90 != null && _D90.Count > 0)
                                {
                                    ListDocNo = _D90.Select(y => y.DES075.DocNo).ToList();
                                    temp1 = db.DES059.Where(x => ListDocNo.Contains(x.DocumentNo) && x.Status != ObjectStatus.Completed.ToString()).Count();
                                }

                                if ((!(temp > 0)) && (!(temp1 > 0)))
                                {

                                    Int64? icount = 0;

                                    if (item.DES089.ReleasedStepId == (int)DCRReleasedSteps.Step3)
                                    {
                                        icount = _D90.Where(x => (x.IsPartialSelect ?? false) != true).Count();
                                    }
                                    if ((icount ?? 0) == 0)
                                    {
                                        new DCRService().UpdateDCRStatus(item.DES089.DCRId, ObjectStatus.Completed.ToString());
                                        new DCRService().UpdateLifeCycleStatus(item.DES089.DCRId, (int)DCRLifeCyclePolicyStep.Complete);
                                        new DCRService().AddDCRHistory(item.DES089.DCRId, null, null, null, null, null, null, null, null, "DCR Completed", CommonService.objClsLoginInfo.UserName);
                                        new DCRService().pushDCRNotification(item.DES089.DCRId, DCRLifeCyclePolicyStep.InCorporate.ToString());
                                    }
                                }
                            }
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_USER_BY_ROLES_Result> GetUserByRole(string Roles)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.SP_DES_GET_USER_BY_ROLES(Roles).Select(x => new SP_DES_GET_USER_BY_ROLES_Result
                    {
                        t_name = x.t_psno + " - " + x.t_name,
                        t_psno = x.t_psno,
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public static bool PushNotification(string Project, string BU, string Location, string Roles, string NotificationMsg, string NotificationType, string RedirectionPath, string PSNos, string CreatedBy)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    db.SP_DES_ADD_NOTIFICATION(Project, BU, Location, Roles, NotificationMsg, NotificationType, RedirectionPath, null);
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return true;
            }
        }

        public string GetConfigvalues(string ConfigName)
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    var HaziraNodePath = dbContext.DES104.Where(c => c.ConfigName == ConfigName).FirstOrDefault();
                    return HaziraNodePath.ConfigValues;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}

