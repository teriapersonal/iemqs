﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class HBOMService : IHBOMService
    {
        public List<PowerViewModel> GetHBOMPowerViewList(string Project, long? ItemId = null)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PART_POWERVIEW(Project, ItemId).Select(x => new PowerViewModel
                    {
                        BOMId = x.BOMId,
                        ItemId = x.ItemId,
                        ParentItemId = x.ParentItemId,
                        ItemKey = x.ItemKey,
                        Type = x.Type,
                        Rev = x.RevNo,
                        policy = x.policy,
                        FindNumber = x.FindNumber,
                        ExtFindNumber = x.ExtFindNumber,
                        ItemName = x.ItemName,
                        Quantity = x.Quantity,
                        Length = x.Length,
                        Width = x.Width,
                        NumberOfPieces = x.NumberOfPieces,
                        UOM = x.UOM,
                        Material = x.Material,
                        State = x.State,
                        ItemGroup = x.ItemGroup,
                        //TableCount = x.TableCount,

                    }).ToList();

                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public TreeViewModel GetHBOMTreeViewList(string Project, long? ItemId = null)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var list = db.SP_DES_GET_PART_POWERVIEW(Project, ItemId).ToList();
                    var data = list.Where(x => x.ItemId == ItemId).Select(x => new TreeViewModel
                    {
                        //id = x.ItemId,
                        name = x.ItemKey,
                        title = ""
                    }).FirstOrDefault();
                    TreeViewModel TreeView = new TreeViewModel();
                    TreeView = (data);
                    GetHBOMChildItems(list, TreeView, ItemId);
                    return TreeView;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public void GetHBOMChildItems(List<SP_DES_GET_PART_POWERVIEW_Result> treeViewList, TreeViewModel TreeView, Int64? ParentId)
        {
            var listChilds = treeViewList.Where(i => i.ParentItemId == ParentId).ToList();
            TreeView.children = new List<TreeViewModel>();
            if (listChilds != null)
                foreach (var item in listChilds)
                {
                    var data = new TreeViewModel
                    {
                        // id = item.ItemId,
                        name = item.ItemKey,
                        title = "Quantity: " + item.Quantity + ", Find No:" + item.FindNumber
                    };
                    GetHBOMChildItems(treeViewList, data, item.ItemId);
                    TreeView.children.Add(data);
                }

        }

        public Int64 GetItemIdByProject(string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    Int64 ItemId = db.DES066.OrderByDescending(x => x.RevNo).Where(x => x.ItemKey == Project + "-M1Top").Select(x => x.ItemId).FirstOrDefault();

                    return ItemId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

    }
}
