﻿using IEMQS.DESCore.Data;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Web;
using System.Xml;
using System.Net;
using System.Linq;
using System.IO;

namespace IEMQS.DESServices
{
    public class CreateXML
    {
        public string CreateXMLFile(string Project,string CurrentUser, out string errorMsg)
        {

            errorMsg = "";
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string localpath = HttpContext.Current.Server.MapPath("~/Uploads/XMLFiles");
                    var LN_SERVER_FOR_BOM_XML = db.DES104.Where(x => x.ConfigName == "LN_SERVER_FOR_BOM_XML").FirstOrDefault();
                    if (LN_SERVER_FOR_BOM_XML != null
                        && (!string.IsNullOrEmpty(LN_SERVER_FOR_BOM_XML.ConfigValues))
                        && (!string.IsNullOrEmpty(LN_SERVER_FOR_BOM_XML.UserId))
                        && (!string.IsNullOrEmpty(LN_SERVER_FOR_BOM_XML.UserPassword)))
                    {
                        NetworkCredential credentials = new NetworkCredential(LN_SERVER_FOR_BOM_XML.UserId, LN_SERVER_FOR_BOM_XML.UserPassword);
                        string strFullPath = LN_SERVER_FOR_BOM_XML.ConfigValues;
                        //using (new ConnectToSharedFolder(strFullPath, credentials))



                        {
                            string CurrentPsNoName = new clsManager().GetUserNameFromPsNo((CurrentUser).ToString());
                            var _ProjectModel = new ProjectService().GetProjectByProjectNo(Project);
                            //string _email = new clsManager().GetMailIdFromPsNo(_ProjectModel.CreatedByPsno);
                            string _email = new clsManager().GetMailIdFromPsNo(CurrentUser);
                            List<SP_DES_GENERATE_XML_Result> temp2 = new PartService().GetPartBOMDataByProject(Project);

                            if (temp2 != null && temp2.Count > 0)
                            {
                                string date = string.Format("{0:yyyyMMdd_HHmmssfff}", DateTime.Now);
                                string filename = Project + "_" + date + ".xml";

                                XmlWriterSettings xmlWriterSettings = new XmlWriterSettings();
                                xmlWriterSettings.NewLineOnAttributes = true;
                                xmlWriterSettings.Indent = true;
                                localpath = localpath + "/" + filename;
                                XmlWriter xmlWriter = XmlWriter.Create(localpath, xmlWriterSettings);

                                xmlWriter.WriteStartDocument();
                                xmlWriter.WriteStartElement("gtwydata");

                                #region E-Person
                                xmlWriter.WriteStartElement("E-Person");
                                xmlWriter.WriteElementString("PLM_USER", CurrentPsNoName);
                                xmlWriter.WriteElementString("USER_EMAIL", _email);
                                xmlWriter.WriteEndElement();
                                #endregion

                                #region E-NET-CHANGE
                                xmlWriter.WriteStartElement("E-NET-CHANGE");

                                xmlWriter.WriteElementString("NET_CHANGE", "2");
                                xmlWriter.WriteEndElement();
                                #endregion

                                for (int i = 0; i < temp2.Count; i++)
                                {
                                    #region E-Item-Rev
                                    xmlWriter.WriteStartElement("E-Item-Rev");
                                    xmlWriter.WriteElementString("ERP_PROJECT", Project);
                                    xmlWriter.WriteElementString("PLM_USER", "h" + temp2[i].CreatedBy);
                                    xmlWriter.WriteElementString("ITEM_KEY", temp2[i].ItemKey);
                                    xmlWriter.WriteElementString("REVISION", Convert.ToString("R" + temp2[i].RevNo));
                                    xmlWriter.WriteElementString("ITEM_NAME", temp2[i].ItemName);
                                    xmlWriter.WriteElementString("DESCRIPTION_1", temp2[i].String2);
                                    xmlWriter.WriteElementString("DESCRIPTION_2", temp2[i].String3);
                                    xmlWriter.WriteElementString("DESCRIPTION_3", temp2[i].String4);
                                    xmlWriter.WriteElementString("ITEM_GROUP", temp2[i].ItemGroup);
                                    xmlWriter.WriteElementString("UNIT_OF_MEASURE", temp2[i].UOM);
                                    xmlWriter.WriteElementString("WEIGHT", Convert.ToString(temp2[i].ItemWeight));
                                    xmlWriter.WriteElementString("ERP_SIZE", Convert.ToString(temp2[i].SizeCode));
                                    xmlWriter.WriteElementString("MATERIAL", temp2[i].Material);
                                    xmlWriter.WriteElementString("ARM_CODE", temp2[i].ARMSet);
                                    xmlWriter.WriteElementString("SIGNAL_CODE", temp2[i].SignalCode);
                                    xmlWriter.WriteElementString("SELECTION_CODE", temp2[i].SelectionCode);
                                    xmlWriter.WriteElementString("THICKNESS", Convert.ToString(temp2[i].Thickness));
                                    xmlWriter.WriteElementString("DENSITY", Convert.ToString(temp2[i].Density));
                                    xmlWriter.WriteElementString("CLAD_THICKNESS_1", Convert.ToString(temp2[i].CladPlatePartThickness1));
                                    xmlWriter.WriteElementString("CLAD_THICKNESS_2", Convert.ToString(temp2[i].CladPlatePartThickness2));
                                    xmlWriter.WriteElementString("CLAD_DENSITY_1", Convert.ToString(temp2[i].CladSpecificGravity1));
                                    xmlWriter.WriteElementString("CLAD_DENSITY_2", Convert.ToString(temp2[i].CladSpecificGravity2));
                                    xmlWriter.WriteElementString("PRODUCT_CLASS", temp2[i].ProductClass);
                                    xmlWriter.WriteElementString("PRODUCT_LINE", temp2[i].ProductLine);
                                    xmlWriter.WriteEndElement();
                                    #endregion

                                    #region CreateDoc
                                    if (temp2[i].ListBOMModel.Count > 0)
                                    {
                                        for (int j = 0; j < temp2[i].ListBOMModel.Count; j++)
                                        {
                                            if (temp2[i].ListBOMModel[j].DOCModel != null)
                                            {
                                                for (int k = 0; k < temp2[i].ListBOMModel[j].DOCModel.ListDOCFileModel.Count; k++)
                                                {
                                                    xmlWriter.WriteStartElement("CreateDoc");
                                                    xmlWriter.WriteElementString("ERP_PROJECT", Project);
                                                    xmlWriter.WriteElementString("ITEM_KEY", temp2[i].ItemKey);
                                                    xmlWriter.WriteElementString("REVISION", Convert.ToString("R" + temp2[i].RevNo));
                                                    xmlWriter.WriteElementString("DOC_SPEC_REF", "Specification");
                                                    xmlWriter.WriteElementString("DOC_TYPE", temp2[i].ListBOMModel[j].DOCModel.DocumentType);
                                                    xmlWriter.WriteElementString("DOC_NAME", temp2[i].ListBOMModel[j].DOCModel.DocumentNo);
                                                    xmlWriter.WriteElementString("DOC_REVISION", Convert.ToString("R" + temp2[i].ListBOMModel[j].DOCModel.DocumentRevision));
                                                    xmlWriter.WriteElementString("DOC_DESCRIPTION", temp2[i].ListBOMModel[j].DOCModel.DocumentTitle);
                                                    xmlWriter.WriteElementString("DOC_URL", temp2[i].ListBOMModel[j].DOCModel.ListDOCFileModel[k].MainDocumentPath);
                                                    xmlWriter.WriteEndElement();
                                                }
                                            }
                                        }
                                    }
                                    #endregion

                                    if (temp2[i].ListBOMModel.Count > 0)
                                    {
                                        foreach (var bomitem in temp2[i].ListBOMModel)
                                        {
                                            var Pdata = db.DES066.Find(bomitem.ParentItemId);
                                            if (Pdata != null)
                                            {
                                                #region E-BOM
                                                xmlWriter.WriteStartElement("E-BOM");
                                                xmlWriter.WriteElementString("ERP_PROJECT", Project);
                                                xmlWriter.WriteElementString("PLM_USER", "h" + bomitem.CreatedBy);
                                                xmlWriter.WriteElementString("PARENT_ITEM", Convert.ToString(Pdata.ItemKey));
                                                xmlWriter.WriteElementString("PARENT_REVISION", Convert.ToString("R" + Pdata.RevNo));
                                                xmlWriter.WriteElementString("CHILD_ITEM", Convert.ToString(temp2[i].ItemKey));
                                                xmlWriter.WriteElementString("CHILD_ITEM_REVISION", Convert.ToString("R" + temp2[i].RevNo));
                                                xmlWriter.WriteElementString("FIND_NO", Convert.ToString(bomitem.FindNumber));
                                                xmlWriter.WriteElementString("FIND_NO_DESCRIPTION", bomitem.FindNumberDescription);
                                                xmlWriter.WriteElementString("QUANTITY", Convert.ToString(bomitem.Quantity));
                                                xmlWriter.WriteElementString("LENGTH", Convert.ToString(bomitem.Length));
                                                xmlWriter.WriteElementString("WIDTH", Convert.ToString(bomitem.Width));
                                                xmlWriter.WriteElementString("UNITS", temp2[i].UOM);
                                                xmlWriter.WriteElementString("EXTRA_INFO", Convert.ToString(bomitem.Remarks));
                                                xmlWriter.WriteElementString("CONSUMPTION_DESCRIPTION", "");
                                                xmlWriter.WriteElementString("REFERENCE_DESIGNATOR_APPLICABLE", "");
                                                xmlWriter.WriteElementString("REFERENCE_DESIGNATOR", "");
                                                xmlWriter.WriteElementString("REMARKS", Convert.ToString(bomitem.Remarks));
                                                xmlWriter.WriteElementString("BOM-LEVEL", Convert.ToString(bomitem.BOMLevel));
                                                xmlWriter.WriteElementString("SHAPE_CODE", "");
                                                xmlWriter.WriteElementString("DIM_1", Convert.ToString(bomitem.Length));
                                                xmlWriter.WriteElementString("DIM_2", Convert.ToString(bomitem.Width));
                                                xmlWriter.WriteElementString("DIM_3", "");
                                                xmlWriter.WriteElementString("DIM_4", "");
                                                xmlWriter.WriteElementString("DIM_5", "");
                                                xmlWriter.WriteElementString("DIM_6", "");
                                                xmlWriter.WriteElementString("DIM_7", "");
                                                xmlWriter.WriteElementString("DIM_8", "");
                                                xmlWriter.WriteElementString("DIM_9", "");
                                                xmlWriter.WriteElementString("DIM_10", "");
                                                xmlWriter.WriteElementString("NO_OF_PIECES", Convert.ToString(bomitem.NumberOfPieces));
                                                xmlWriter.WriteEndElement();
                                                #endregion
                                            }
                                        }
                                    }

                                    if (!string.IsNullOrEmpty(temp2[i].ARMSet))
                                    {
                                        #region ARMCollection
                                        xmlWriter.WriteStartElement("ARMCollection");
                                        xmlWriter.WriteElementString("ERP_PROJECT", Project);
                                        xmlWriter.WriteElementString("PLM_USER", "h" + temp2[i].CreatedBy);
                                        xmlWriter.WriteElementString("ARM_SET_ID", temp2[i].ARMSet);
                                        xmlWriter.WriteElementString("REVISION", temp2[i].ARMRev);
                                        xmlWriter.WriteElementString("ARM_CODE", "");
                                        xmlWriter.WriteElementString("PARAMETERS", "");
                                        xmlWriter.WriteElementString("DESCRIPTION", "");
                                        xmlWriter.WriteElementString("DESCRIPTION_WITH_REP", "");
                                        xmlWriter.WriteEndElement();
                                        #endregion

                                        #region ARMSet
                                        xmlWriter.WriteStartElement("ARMSet");
                                        xmlWriter.WriteElementString("ERP_PROJECT", Project);
                                        xmlWriter.WriteElementString("PLM_USER", "h" + temp2[i].CreatedBy);
                                        xmlWriter.WriteElementString("ARM_SET_ID", temp2[i].ARMSet);
                                        xmlWriter.WriteElementString("REVISION", temp2[i].ARMRev);
                                        xmlWriter.WriteElementString("DESCRIPTION", temp2[i].ARMDescription);
                                        xmlWriter.WriteElementString("TEXT", Convert.ToString(temp2[i].ARMText));
                                        xmlWriter.WriteEndElement();
                                        #endregion
                                    }
                                }

                                xmlWriter.WriteEndDocument();
                                xmlWriter.Close();

                                string ftpPath = Path.Combine(strFullPath, filename);
                                FtpWebRequest ftp = (FtpWebRequest)FtpWebRequest.Create(ftpPath);
                                ftp.Credentials = credentials;

                                ftp.KeepAlive = true;
                                ftp.UseBinary = true;
                                ftp.Method = WebRequestMethods.Ftp.UploadFile;


                                FileStream fs = File.OpenRead(localpath);
                                byte[] buffer = new byte[fs.Length];
                                fs.Read(buffer, 0, buffer.Length);
                                fs.Close();

                                Stream ftpstream = ftp.GetRequestStream();
                                ftpstream.Write(buffer, 0, buffer.Length);
                                ftpstream.Close();

                                return filename;
                            }
                            else
                            {
                                errorMsg = "Top Item not found";
                                return "";
                            }

                        }
                    }
                    else
                    {
                        errorMsg = "LN Server Confrigration not found.";
                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                errorMsg = ex.Message;
                return "";
            }
        }

    }
}
