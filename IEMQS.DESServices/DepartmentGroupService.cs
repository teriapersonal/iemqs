﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IEMQS.DESServices
{
    public class DepartmentGroupService : IDepartmentGroupService
    {
        public Int64 AddEdit(DINGroupModel model, out string errorMsg, out string infoMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    infoMsg = "";
                    //if (model.DINGroupId > 0)
                    //{
                    //    var checkchild = db.DES033.Find(model.DINGroupId);
                    //    if (checkchild != null)
                    //    {
                    //        if (checkchild.DES081 != null && checkchild.DES081.Count > 0)
                    //        {
                    //            errorMsg = "Department Group already in used. you cannot edit";
                    //            return 0;
                    //        }
                    //        if (checkchild.DES099 != null && checkchild.DES099.Count > 0)
                    //        {
                    //            errorMsg = "Department Group already in used. you cannot edit";
                    //            return 0;
                    //        }
                    //    }

                    //}
                    List<string> PsNo = new List<string>();
                    if (model.DepartmentGroupList.Count > 0)
                    {
                        foreach (var item in model.DepartmentGroupList)
                        {
                            if (item.PSNoList != null)
                            {
                                foreach (var items in item.PSNoList)
                                {
                                    if (!PsNo.Contains(items))
                                    {
                                        PsNo.Add(items);
                                    }
                                    else
                                    {
                                        infoMsg = "Can't add same Person in Same Gorup";
                                        return 0;
                                    }
                                }
                            }
                        }
                    }
                    var temp = db.DES033.Find(model.DINGroupId);

                    DES033 _DES033 = temp == null ? new DES033() : temp;
                    if (temp != null)
                    {
                        _DES033.DES034.ToList().ForEach(x =>
                        {
                            db.DES035.RemoveRange(x.DES035);
                            db.DES034.Remove(x);
                        });
                    }

                    _DES033.GroupName = model.GroupName;
                    List<DES034> _DES034List = new List<DES034>();
                    foreach (var d34 in model.DepartmentGroupList)
                    {
                        if (!d34.IsDelete) //d34.IsDelete == false
                        {
                            List<DES035> _DES035List = new List<DES035>();
                            if ((!string.IsNullOrEmpty(d34.DeptId)) && d34.PSNoList != null && d34.PSNoList.Count > 0)
                            {
                                foreach (var ps in d34.PSNoList)
                                {
                                    _DES035List.Add(new DES035 { PSNumber = ps });
                                }

                                _DES034List.Add(new DES034
                                {
                                    DeptId = d34.DeptId,
                                    DES035 = _DES035List
                                });
                            }
                        }
                    }
                    _DES033.DES034 = _DES034List;
                    if (temp == null)
                    {
                        _DES033.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES033.CreatedOn = DateTime.Now;
                        db.DES033.Add(_DES033);
                    }
                    else
                    {
                        _DES033.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES033.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES033.DINGroupId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<DINGroupModel> GetAllDepartmentGroup()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES033.OrderBy(x => x.DINGroupId).Select(x => new DINGroupModel
                    {
                        DINGroupId = x.DINGroupId,
                        GroupName = x.GroupName
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public DINGroupModel GetDepartmentGroupbyId(Int64 Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var clsmgr = new IEMQSImplementation.clsManager();
                    var x = db.DES033.Where(y => y.DINGroupId == Id).FirstOrDefault();
                    if (x != null)
                        return new DINGroupModel
                        {
                            DINGroupId = x.DINGroupId,
                            GroupName = x.GroupName,
                            DepartmentGroupList = x.DES034.Select(y => new DINGroupDepartmentModel
                            {
                                DINGroupDeptId = y.DINGroupDeptId,
                                DINGroupId = y.DINGroupId,
                                DeptId = y.DeptId,
                                PSNoList = y.DES035.Select(z => z.PSNumber).ToList(),
                                FunctionList = GetTypeList().Select(z => new SelectListItem
                                {
                                    Text = z.t_desc,
                                    Value = z.t_dimx,
                                    Selected = z.t_dimx == y.DeptId,
                                }).ToList(),
                                PSNumberList = y.DES035.Select(z => new SelectListItem
                                {
                                    Text = clsmgr.GetPsidName(z.PSNumber),
                                    Value = z.PSNumber,
                                    Selected = true
                                }).ToList()
                                //PSNumberList = GetPSNumberList(y.DeptId).Select(z => new SelectListItem
                                //{
                                //    Text = z.t_name,
                                //    Value = z.t_psno,
                                //    Selected = y.DES035.Select(a => a.PSNumber).ToList().Contains(z.t_psno)
                                //}).ToList()
                            }).ToList(),

                        };
                    else
                        return null;



                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DIN_GROUP_Result> GetDepartmentGroupList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DIN_GROUP(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_TYPE_Result> GetTypeList()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_TYPE().ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PS_NUMBER_BY_DEPARTMENT_Result> GetPSNumberList(string dept)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PS_NUMBER_BY_DEPARTMENT(dept).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public List<SP_DES_GET_PS_NUMBER_BY_DEPARTMENT_Result> GetAllFunctionByPSNo(string dept)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PS_NUMBER_BY_DEPARTMENT(dept).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PSNO_ON_TERMS_AND_DEPT_Result> GetALLPSNOByNameandDepartment(string terms, string dept)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PSNO_ON_TERMS_AND_DEPT(terms, dept).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteDepartmentGroup(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES033.Find(Id);
                    if (check != null)
                    {
                        if (check.DES081 != null && check.DES081.Count > 0)
                        {
                            errorMsg = "Department Group is already in used. you cannot delete";
                            return 0;
                        }
                        if (check.DES099 != null && check.DES099.Count > 0)
                        {
                            errorMsg = "Department Group is already in used. you cannot delete";
                            return 0;
                        }
                        db.DES033.Where(x => x.DINGroupId == Id).ToList().ForEach(x =>
                        {
                            x.DES034.ToList().ForEach(y =>
                            {
                                db.DES035.RemoveRange(y.DES035);
                                db.DES034.Remove(y);
                            });
                            db.DES033.Remove(x);
                        });
                    }
                    db.SaveChanges();
                    return 1;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool CheckGroupNameAlreadyExist(string name, Int64? DINgroupId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    name = name.Trim();
                    var data = db.DES033.Where(x => x.GroupName == name && (x.DINGroupId != DINgroupId || DINgroupId == null)).ToList();
                    if (data != null && data.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
            }
        }
    }
}
