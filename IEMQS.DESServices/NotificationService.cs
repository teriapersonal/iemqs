﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using IEMQS.DESCore.Data;
using IEMQSImplementation;

namespace IEMQS.DESServices
{
    public class NotificationService : INotificationService
    {
        public List<SP_DES_GET_NOTIFICATION_Result> GetAllNotificationByPSNo(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection,string PSNo, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_NOTIFICATION(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, PSNo).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<GLB010> GetNotificationContent(DateTime currentdate)
        {
            try
            {
                using (IEMQSEntitiesContext db = new IEMQSEntitiesContext())
                {
                    DateTime dateTime = currentdate.AddDays(-1);
                    var notificationList= db.GLB010.Where(a => a.CreatedOn > dateTime && a.PSNo == CommonService.objClsLoginInfo.UserName && a.IsForDES==true).OrderByDescending(a => a.CreatedOn).ToList();
                    return notificationList;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return new List<GLB010>();
            }

        }

        public string GettotalUnreadNotificationasperPSno(string PSNo)
        {
            try
            {
                using (IEMQSEntitiesContext db = new IEMQSEntitiesContext())
                {
                    string totNotification = string.Empty;
                    totNotification = Convert.ToString(db.GLB010.Where(a => a.IsRead == false && a.PSNo == PSNo && a.IsForDES == true).Count());
                    return totNotification;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return "0";
            }
        }

        public string MarkNotificationasRead(string PSNo)
        {
            try
            {
                using (IEMQSEntitiesContext db = new IEMQSEntitiesContext())
                {
                    var notificationList = db.GLB010.Where(a => a.IsRead == false && a.PSNo == CommonService.objClsLoginInfo.UserName && a.IsForDES == true).OrderByDescending(a => a.CreatedOn).ToList();

                    foreach (GLB010 gLB010data in notificationList)
                    {
                        gLB010data.IsRead = true;
                    }
                    db.SaveChanges();
                    return "Success";
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return "Success";
            }
        }

    }
}
