﻿using IEMQS.DESCore.Data;
using System;
using System.ComponentModel.DataAnnotations;

namespace IEMQS.DESServices
{
    public class DESProjectApiService
    {
        public static bool Add(ApiProjectModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var temp = db.DES051.Find(model.Project);
                    DES051 _DES051 = temp == null ? new DES051() : temp;

                    if (temp == null)
                    {
                        _DES051.Project = model.Project;
                        _DES051.MainProject = model.MainProject;
                        _DES051.Contract = model.Contract;
                        _DES051.ProjectDescription = model.ProjectDescription;
                        _DES051.BUCode = model.BU;
                        _DES051.PBU = model.PBU;
                        _DES051.Customer = model.Customer;
                        _DES051.CustomerLOINumber = model.CustomerLOINumber;
                        _DES051.CustomerPONumber = model.CustomerPONumber;
                        _DES051.Owner = model.Owner;
                        _DES051.ProductType = model.ProductType;
                        _DES051.Consultant = model.Consultant;
                        //_DES051.Licensor = model.Licensor;
                        _DES051.DesignCode = model.DesignCode;
                        _DES051.CodeStamp = model.CodeStamp;
                        _DES051.RegulatoryRequirement = model.RegulatoryRequirement;
                        _DES051.MajorMaterial = model.MajorMaterial;
                        _DES051.EquipmentDiameter = model.EquipmentDiameter;
                        _DES051.TLtoTL = model.TLtoTL;
                        _DES051.FabricatedWeight = model.FabricatedWeight;
                        DateTime loldt;
                        if (DateTime.TryParse(model.LOIDate, out loldt))
                            _DES051.LOIDate = loldt;
                        DateTime plmdt;
                        if (DateTime.TryParse(model.PIMDate, out plmdt))
                            _DES051.PIMDate = plmdt;
                        DateTime podt;
                        if (DateTime.TryParse(model.PODate, out podt))
                            _DES051.PODate = podt;
                        DateTime ContractualDeliverydt;
                        if (DateTime.TryParse(model.ContractualDeliveryDate, out ContractualDeliverydt))
                            _DES051.ContractualDeliveryDate = ContractualDeliverydt;
                        _DES051.SiteWorkRequire = model.SiteWorkRequire;
                        _DES051.DeliveryCondition = model.DeliveryCondition;
                        _DES051.DeliveryTerms = model.DeliveryTerms;
                        _DES051.ProductEndUser = model.ProductEndUser;
                        DateTime zerodt;
                        if (DateTime.TryParse(model.ZeroDate, out zerodt))
                            _DES051.ZeroDate = zerodt;
                        _DES051.EnquiryName = model.EnquiryName;
                        _DES051.OrderCategory = model.OrderCategory;
                        _DES051.Status = DESCore.ObjectStatus.Created.ToString();
                        _DES051.CreatedBy = model.CreatedBy;
                        _DES051.CreatedOn = DateTime.Now;
                        DateTime WarrantyExpirydt;
                        if (DateTime.TryParse(model.WarrantyExpiryDate, out WarrantyExpirydt))
                            _DES051.WarrantyExpiryDate = WarrantyExpirydt;
                        _DES051.WarrantyTerms = model.WarrantyTerms;
                        _DES051.location = model.Location;

                        string getIdenticalProject = string.Empty; 
                        var getLastDigOfProjectNo = model.Project.Substring(model.Project.Length - 1);
                        bool isNum = isNumeric(getLastDigOfProjectNo);
                        string b = string.Empty;
                        if (isNum == false)
                        {
                            getIdenticalProject = model.Project.Remove(model.Project.Length - 1);
                        }
                        else
                        {
                            getIdenticalProject = model.Project;
                        }

                        _DES051.IdenticalGroup = getIdenticalProject;
                        db.DES051.Add(_DES051);
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        errorMsg = "Project- " + model.Project + " already pushed to IEMQS ";
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                errorMsg = "";
                return false;
            }
        }

        public static bool isNumeric(string s)
        {
            if (s == null || s == "") return false;

            for (int i = 0; i < s.Length; i++)
                if ((s[i] ^ '0') > 9)
                    return false;

            return true;
        }

        public static bool Update(ApiProjectModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var temp = db.DES051.Find(model.Project);
                    DES051 _DES051 = temp == null ? new DES051() : temp;

                    if (temp != null)
                    {
                        //_DES051.Project = model.Project;

                        if (string.IsNullOrEmpty(_DES051.MainProject))
                        {
                            _DES051.MainProject = model.MainProject;
                        }
                        //_DES051.Contract = model.Contract;
                        if (string.IsNullOrEmpty(_DES051.ProjectDescription))
                        {
                            _DES051.ProjectDescription = model.ProjectDescription;
                        }
                        if (string.IsNullOrEmpty(_DES051.BUCode))
                        {
                            _DES051.BUCode = model.BU;
                        }
                        if (string.IsNullOrEmpty(_DES051.PBU))
                        {
                            _DES051.PBU = model.PBU;
                        }
                        if (string.IsNullOrEmpty(_DES051.Customer))
                        {
                            _DES051.Customer = model.Customer;
                        }
                        if (string.IsNullOrEmpty(_DES051.CustomerLOINumber))
                        {
                            _DES051.CustomerLOINumber = model.CustomerLOINumber;
                        }
                        if (string.IsNullOrEmpty(_DES051.CustomerPONumber))
                        {
                            _DES051.CustomerPONumber = model.CustomerPONumber;
                        }
                        if (string.IsNullOrEmpty(_DES051.Owner))
                        {
                            _DES051.Owner = model.Owner;
                        }
                        if (string.IsNullOrEmpty(_DES051.ProductType))
                        {
                            _DES051.ProductType = model.ProductType;
                        }
                        if (string.IsNullOrEmpty(_DES051.Consultant))
                        {
                            _DES051.Consultant = model.Consultant;
                        }
                        if (string.IsNullOrEmpty(_DES051.DesignCode))
                        {
                            _DES051.DesignCode = model.DesignCode;
                        }
                        if (string.IsNullOrEmpty(_DES051.CodeStamp))
                        {
                            _DES051.CodeStamp = model.CodeStamp;
                        }
                        if (string.IsNullOrEmpty(_DES051.RegulatoryRequirement))
                        {
                            _DES051.RegulatoryRequirement = model.RegulatoryRequirement;
                        }

                        if (string.IsNullOrEmpty(_DES051.MajorMaterial))
                        {
                            _DES051.MajorMaterial = model.MajorMaterial;
                        }
                        if (string.IsNullOrEmpty(_DES051.EquipmentDiameter))
                        {
                            _DES051.EquipmentDiameter = model.EquipmentDiameter;
                        }
                        if (string.IsNullOrEmpty(_DES051.TLtoTL))
                        {
                            _DES051.TLtoTL = model.TLtoTL;
                        }
                        if (string.IsNullOrEmpty(_DES051.FabricatedWeight))
                        {
                            _DES051.FabricatedWeight = model.FabricatedWeight;
                        }

                        if (_DES051.LOIDate == null)
                        {
                            DateTime loldt;
                            if (DateTime.TryParse(model.LOIDate, out loldt))
                                _DES051.LOIDate = loldt;
                        }

                        if (_DES051.PIMDate == null)
                        {
                            DateTime plmdt;
                            if (DateTime.TryParse(model.PIMDate, out plmdt))
                                _DES051.PIMDate = plmdt;
                        }
                        if (_DES051.PODate == null)
                        {
                            DateTime podt;
                            if (DateTime.TryParse(model.PODate, out podt))
                                _DES051.PODate = podt;
                        }
                        if (_DES051.ContractualDeliveryDate == null)
                        {
                            DateTime ContractualDeliverydt;
                            if (DateTime.TryParse(model.ContractualDeliveryDate, out ContractualDeliverydt))
                                _DES051.ContractualDeliveryDate = ContractualDeliverydt;
                        }
                        if (string.IsNullOrEmpty(_DES051.SiteWorkRequire))
                        {
                            _DES051.SiteWorkRequire = model.SiteWorkRequire;
                        }
                        if (string.IsNullOrEmpty(_DES051.DeliveryCondition))
                        {
                            _DES051.DeliveryCondition = model.DeliveryCondition;
                        }
                        if (string.IsNullOrEmpty(_DES051.DeliveryTerms))
                        {
                            _DES051.DeliveryTerms = model.DeliveryTerms;
                        }
                        if (string.IsNullOrEmpty(_DES051.ProductEndUser))
                        {
                            _DES051.ProductEndUser = model.ProductEndUser;
                        }
                        if (_DES051.ZeroDate == null)
                        {
                            DateTime zerodt;
                            if (DateTime.TryParse(model.ZeroDate, out zerodt))
                                _DES051.ZeroDate = zerodt;
                        }
                        if (string.IsNullOrEmpty(_DES051.EnquiryName))
                        {
                            _DES051.EnquiryName = model.EnquiryName;
                        }
                        if (string.IsNullOrEmpty(_DES051.OrderCategory))
                        {
                            _DES051.OrderCategory = model.OrderCategory;
                        }
                      
 
                        _DES051.EditedBy = model.EditedBy;
                        _DES051.EditedOn = DateTime.Now;

                        if (_DES051.WarrantyExpiryDate == null)
                        {
                            DateTime WarrantyExpirydt;
                            if (DateTime.TryParse(model.WarrantyExpiryDate, out WarrantyExpirydt))
                                _DES051.WarrantyExpiryDate = WarrantyExpirydt;
                        }
                        if (string.IsNullOrEmpty(_DES051.WarrantyTerms))
                        {
                            _DES051.WarrantyTerms = model.WarrantyTerms;
                        }
                        if (string.IsNullOrEmpty(_DES051.location))
                        {
                            _DES051.location = model.Location;
                        }

                        db.Entry(_DES051).State = System.Data.Entity.EntityState.Modified;                       
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        errorMsg = "Project- " + model.Project + " not found in IEMQS ";
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                errorMsg = "";
                return false;
            }
        }
    }

    public class ApiProjectModel
    {
        [Required(ErrorMessage = "Project is required")]
        [StringLength(9, ErrorMessage = "Project length can't be more than 9")]
        public string Project { get; set; }

        [StringLength(9, ErrorMessage = "Main Project length can't be more than 9")]
        public string MainProject { get; set; }

        [Required(ErrorMessage = "Contract is required")]
        [StringLength(9, ErrorMessage = "Contract length can't be more than 9")]
        public string Contract { get; set; }

        [Required(ErrorMessage = "Project Description is required")]
        [StringLength(30, ErrorMessage = "Project Description length can't be more than 30")]
        public string ProjectDescription { get; set; }

        [StringLength(6, ErrorMessage = "BU length can't be more than 6")]
        [Required(ErrorMessage = "BU is required")]
        public string BU { get; set; }

        [StringLength(10, ErrorMessage = "PBU length can't be more than 10")]
        [Required(ErrorMessage = "PBU is required")]
        public string PBU { get; set; }

        [StringLength(9, ErrorMessage = "Customer length can't be more than 9")]
        public string Customer { get; set; }

        [StringLength(30, ErrorMessage = "Customer LOI Number length can't be more than 30")]
        public string CustomerLOINumber { get; set; }

        [StringLength(20, ErrorMessage = "Customer PO Number length can't be more than 20")]
        public string CustomerPONumber { get; set; }

        [StringLength(9, ErrorMessage = "Owner length can't be more than 9")]
        public string Owner { get; set; }

        [StringLength(50, ErrorMessage = "Product Type length can't be more than 50")]
        public string ProductType { get; set; }

        [StringLength(50, ErrorMessage = "Consultant length can't be more than 50")]
        public string Consultant { get; set; }

        //[StringLength(50, ErrorMessage = "Licensor length can't be more than 50")]
        public string Licensor { get; set; }

        [StringLength(50, ErrorMessage = "Design Code length can't be more than 50")]
        public string DesignCode { get; set; }

        [StringLength(50, ErrorMessage = "Code Stamp length can't be more than 50")]
        public string CodeStamp { get; set; }

        [StringLength(50, ErrorMessage = "Regulatory Requirement length can't be more than 50")]
        public string RegulatoryRequirement { get; set; }

        [StringLength(50, ErrorMessage = "Major Material length can't be more than 50")]
        public string MajorMaterial { get; set; }

        [StringLength(50, ErrorMessage = "Equipment Diameter length can't be more than 50")]
        public string EquipmentDiameter { get; set; }

        [StringLength(50, ErrorMessage = "TL to TL length can't be more than 50")]
        public string TLtoTL { get; set; }

        [StringLength(50, ErrorMessage = "Fabricated Weight length can't be more than 50")]
        public string FabricatedWeight { get; set; }

        public string LOIDate { get; set; }

        public string PIMDate { get; set; }

        public string PODate { get; set; }

        public string ContractualDeliveryDate { get; set; }

        [StringLength(50, ErrorMessage = "Site Work Require length can't be more than 50")]
        public string SiteWorkRequire { get; set; }

        [StringLength(150, ErrorMessage = "Delivery Condition length can't be more than 150")]
        public string DeliveryCondition { get; set; }

        [StringLength(150, ErrorMessage = "Delivery Terms length can't be more than 150")]
        public string DeliveryTerms { get; set; }

        [StringLength(200, ErrorMessage = "Product End User length can't be more than 200")]
        public string ProductEndUser { get; set; }

        public string ZeroDate { get; set; }

        [StringLength(12, ErrorMessage = "Enquiry Name length can't be more than 12")]
        public string EnquiryName { get; set; }

        [StringLength(50, ErrorMessage = "Order Category length can't be more than 50")]
        public string OrderCategory { get; set; }

        [StringLength(25, ErrorMessage = "Status length can't be more than 25")]
        public string Status { get; set; }

        [StringLength(9, ErrorMessage = "Created By length can't be more than 9")]
        public string CreatedBy { get; set; }

        public string CreatedOn { get; set; }

        [StringLength(9, ErrorMessage = "EditedBy length can't be more than 9")]
        public string EditedBy { get; set; }

        public string EditedOn { get; set; }

        public string WarrantyTerms { get; set; }
        public string WarrantyExpiryDate { get; set; }

        [Required(ErrorMessage = "Location is required")]
        [StringLength(100, ErrorMessage = "Location length can't be more than 100")]
        public string Location { get; set; }
    }
}
