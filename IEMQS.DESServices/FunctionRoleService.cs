﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class FunctionRoleService : IFunctionRoleService
    {
        public bool AddEdit(FunctionRoleModel model, string RoleList, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    string[] strArr = RoleList.Split(',');

                    #region Check Validation

                    if (model.FunctionRoleMappingId == 0)
                    {                       
                        for (int i = 0; i < strArr.Length; i++)
                        {
                            int temp = Convert.ToInt32(strArr[i]);
                            var check = db.DES032.Where(x => x.FunctionId == model.FunctionId && x.RoleId == temp).FirstOrDefault();
                            if (check != null)
                            {
                                errorMsg = "Function with role is already exist";
                                return false;
                            }
                        }
                    }
                    #endregion

                    for (int i = 0; i < strArr.Length; i++)
                    {
                        if (i == 0 && model.FunctionRoleMappingId != 0)
                        {
                            List<DES032> temp = db.DES032.Where(w => w.FunctionId == model.FunctionId).ToList();
                            db.DES032.RemoveRange(temp);
                            db.SaveChanges();
                        }

                        DES032 _DES032 = new DES032();
                        _DES032.FunctionId = model.FunctionId.Trim();
                        _DES032.RoleId = Convert.ToInt32(strArr[i]);

                        db.DES032.Add(_DES032);
                    }
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                errorMsg = ex.Message;
                return false;
                throw;
            }
        }

        public List<FunctionRoleModel> GetAllFunctionRole()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES032.OrderBy(x => x.FunctionRoleMappingId).Select(x => new FunctionRoleModel
                    {
                        FunctionRoleMappingId = x.FunctionRoleMappingId,
                        FunctionId = x.FunctionId,
                        RoleId = x.RoleId
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<FunctionRoleModel> GetAllRolebyFunctionId(string Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES032.Where(x => x.FunctionId == Id).Select(x => new FunctionRoleModel
                    {
                        FunctionRoleMappingId = x.FunctionRoleMappingId,
                        FunctionId = x.FunctionId,
                        RoleId = x.RoleId
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_FUNCTION_ROLE_Result> GetFunctionRoleList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_FUNCTION_ROLE(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_TYPE_Result> GetTypeList()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_TYPE().ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ROLE_Result> GetRoleList()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ROLE().ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteFunctionRole(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES032.Find(Id);
                    if (check != null)
                    {
                        db.DES032.Remove(check);
                        db.SaveChanges();
                        return check.FunctionRoleMappingId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

    }
}
