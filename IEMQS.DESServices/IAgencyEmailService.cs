﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IAgencyEmailService
    {
        Int64 AddEdit(AgencyEmailModel model);
        AgencyEmailModel GetAgencyEmailbyId(Int64 Id);
        List<AgencyEmailModel> GetAllAgencyEmail();
        List<SP_DES_GET_TRANSMITTAL_EMAIL_CONFIGURATION_Result> GetAgencyEmailList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection,out int recordsTotal,string Project);
        Int64 DeleteAgencyEmail(Int64 Id, out string errorMsg);
        bool CheckAgencyEmailAlreadyExist(Int64? agencyId, string Project, Int64? transmittalemailId);
    }
}
