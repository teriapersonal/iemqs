﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IProductService
    {
        Int64 AddEdit(ProductModel model, out string errorMsg);
        ProductModel GetProductbyId(Int64 Id);
        List<ProductModel> GetAllProduct();       
        List<SP_DES_GET_PRODUCT_Result> GetProductList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        Int64 DeleteProduct(Int64 Id, out string errorMsg);
    }
}
