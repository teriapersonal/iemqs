﻿using IEMQS.DESCore.Data;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IFCSFileTransferManagementService
    {
        List<DES060> GetNotTransferredFilelist(int location);

        List<SP_DES_GET_DOC_OBJECT_FILE_FOR_TRANSFER_Result> GetDOCObjectFileForTransfer(int Location);

        void UpdateCentralFlag(long DocumentMappingId, int location);

        bool AddTransferFileLog(DES078 _des078, int location);

        void AddExceptionLogs(DES088 _dES088);

        List<SP_DES_GET_CUSTOMER_FEEDBACK_FILE_FOR_TRANSFER_Result> GetCustomerFeedbackFileForTransfer(int Location);

        void AddLogForTransfrredFiles(long OjbectId, int location, string Type);

        List<SP_DES_GET_DCR_OBJECT_FILE_FOR_TRANSFER_Result> GetDCRFileForTransfer(int Location);

        List<SP_DES_GET_ROC_COMMENTED_FILE_FOR_TRANSFER_Result> GetROCFileForTransfer(int Location);

        List<SP_DES_GET_ROC_COMMENTED_ATTACHED_FILE_FOR_TRANSFER_Result> GetROC_ATTACHED_FileForTransfer(int Location);

        List<SP_DES_GET_PLM_DOC_OBJECT_FILE_FOR_TRANSFER_TO_IEMQS_FCS_Result> GetPLMDOCObjectFilesforTransfer(int Location);

        List<DES086> GetCustomerFeedbackRecordofHazira(int location);

        List<DES091> GetDCRRecordofHazira(int location);

        List<DES070> GetROCFileRecordofHazira(int location);

        List<DES072> GetROCCommentedFileRecordofHazira(int location);

        void UpdateCentralANDMaindocumentPathFORPLMFiles(string MaindocumentPath, long DocumentMappingId);
    }
}
