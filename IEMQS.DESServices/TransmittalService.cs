﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;


namespace IEMQS.DESServices
{
    public class TransmittalService : ITransmittalService
    {
        public List<AgencyModel> AgnecywithEmailTemplate(string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var AgencyIds = new List<long?>();
                    AgencyIds = db.DES068.Where(x => x.Project == Project).Select(x => x.AgencyId).ToList();

                    return db.DES027.Where(x => AgencyIds.Contains(x.AgencyId)).OrderBy(x => x.AgencyId).Select(x => new AgencyModel
                    {
                        AgencyId = x.AgencyId,
                        AgencyName = x.AgencyName,
                        AgencyCheckInBy = x.AgencyCheckInBy,
                        AgencyCheckInOn = x.AgencyCheckInOn,
                        IsAgencyCheckIn = x.IsAgencyCheckIn ?? false
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_TRANSMITTAL_AGENCY_DOC_Result> GetAgencyDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? AgencyID, out int recordsTotal, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_TRANSMITTAL_AGENCY_DOC(startIndex, displayLength, sSearch, AgencyID, sortDirection, sortColumnIndex, Project).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_TRANSMITTAL_JEP_DOC_Result> GetJEPDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, Int64? AgencyID, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_TRANSMITTAL_JEP_DOC(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, AgencyID).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {

                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_TRANSMITTAL_GLOBAL_JEP_DOC_Result> GetGlobalJEPDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string DocumentNo, string CurrentProject, Int64? AgencyID)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_TRANSMITTAL_GLOBAL_JEP_DOC(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, DocumentNo, CurrentProject, AgencyID).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 AddEdit(AgencyDocumentModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var temp = db.DES076.Where(x => x.AgencyId == model.AgencyId && x.Project == model.Project).FirstOrDefault();
                    DES076 _DES076 = temp == null ? new DES076() : temp;

                    _DES076.Project = model.Project;
                    _DES076.AgencyId = model.AgencyId;
                    if (model.FolderId != null)
                    {
                        _DES076.FolderId = model.FolderId;
                    }
                    else
                    {
                        _DES076.FolderId = temp.FolderId;

                    }
                    _DES076.IsEmailSentToAgency = model.IsEmailSentToAgency;

                    List<DES077> _DES077List1 = new List<DES077>();
                    if (model.AgencyDocumentList != null)
                        foreach (var d77 in model.AgencyDocumentList)
                        {

                            AgencyDocumentDetailModel Agencymodels = new AgencyDocumentDetailModel();
                            Agencymodels = test(d77.JEPDocumentDetailsId);
                            _DES077List1.Add(new DES077
                            {
                                JEPDocumentDetailsId = d77.JEPDocumentDetailsId,
                                PlannedApprovalDate = Agencymodels.PlannedApprovalDate != null ? Agencymodels.PlannedApprovalDate : null,
                                Weightage = 0,
                                CreatedBy = CommonService.objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now
                            });
                        }
                    _DES076.DES077 = _DES077List1;
                    if (temp == null)
                    {
                        _DES076.RoleGroup = model.RoleGroup;
                        _DES076.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES076.CreatedOn = DateTime.Now;
                        db.DES076.Add(_DES076);
                    }
                    else
                    {
                        _DES076.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES076.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES076.AgencyDocId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public AgencyDocumentDetailModel test(Int64 JEPDocumentDetailsId)
        {
            try
            {
                AgencyDocumentDetailModel AgencyDocumentDetailModels = new AgencyDocumentDetailModel();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    #region Plaaned Date Logic

                    var DES075Data = db.DES075.Where(x => x.JEPDocumentDetailsId == JEPDocumentDetailsId).FirstOrDefault();
                    var DES075DataRev = db.DES075.Where(x => x.DocNo == DES075Data.DocNo && x.RevisionNo == DES075Data.RevisionNo - 1).FirstOrDefault();
                    if (DES075DataRev != null)
                    {
                        var DES085Data = db.DES085.Where(x => x.JEPDocumentDetailsId == DES075DataRev.JEPDocumentDetailsId).FirstOrDefault();
                        if (DES085Data != null)
                        {
                            //if (DES085Data.ReceiptDate != null)
                            {
                                var getDES077Data = db.DES077.Where(x => x.JEPDocumentDetailsId == DES075DataRev.JEPDocumentDetailsId).FirstOrDefault();
                                if (getDES077Data.PlannedApprovalDate != null)
                                {
                                    AgencyDocumentDetailModels.PlannedApprovalDate = getDES077Data.PlannedApprovalDate;
                                }
                            }
                        }
                    }
                }

                return AgencyDocumentDetailModels;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
            #endregion
        }

        public Int64 DeleteAgencyDoc(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES077.Find(Id);
                    if (check != null)
                    {
                        db.DES077.Remove(check);
                        db.SaveChanges();
                        return check.AgencyDocListId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateAgencyDoc(AgencyDocumentModel model, out string errorMG)
        {
            try
            {
                errorMG = "";
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (model.AgencyDocumentList != null)
                    {
                        foreach (var Objagencydoc in model.AgencyDocumentList)
                        {
                            var temp = db.DES077.Find(Objagencydoc.AgencyDocListId);
                            if (temp != null)
                            {
                                temp.ClientDocNo = Objagencydoc.ClientDocNo;
                                temp.ClientDocRev = Objagencydoc.ClientDocRev;
                                temp.PlannedSubmitDate = Objagencydoc.PlannedSubmitDate;
                                temp.PlannedApprovalDate = Objagencydoc.PlannedApprovalDate;
                                temp.Weightage = Objagencydoc.Weightage;
                                if (model.IsCommentChanges)
                                {
                                    temp.EditedBy = CommonService.objClsLoginInfo.UserName;
                                    temp.EditedOn = DateTime.Now;
                                }
                                else
                                {
                                    temp.CreatedBy = CommonService.objClsLoginInfo.UserName;
                                    temp.CreatedOn = DateTime.Now;
                                }
                                var tempAgency = db.DES076.Find(temp.AgencyDocId);
                                if (tempAgency != null)
                                {
                                    if (model.IsCommentChanges)
                                    {
                                        tempAgency.EditedBy = CommonService.objClsLoginInfo.UserName;
                                        tempAgency.EditedOn = DateTime.Now;
                                    }
                                    else
                                    {
                                        if (model.RoleGroup != null)
                                        {
                                            tempAgency.RoleGroup = model.RoleGroup.ToString().Trim();
                                        }

                                        tempAgency.CreatedBy = CommonService.objClsLoginInfo.UserName;
                                        tempAgency.CreatedOn = DateTime.Now;
                                    }
                                }
                            }
                            db.SaveChanges();
                        }

                        var getDataByAenecy = db.DES076.Where(x => x.AgencyId == model.AgencyId && x.Project == model.Project).FirstOrDefault();
                        double totalWeight = 0;
                        if (getDataByAenecy.AgencyId != null)
                        {
                            var _DES077List = db.DES077.Where(x => x.AgencyDocId == getDataByAenecy.AgencyDocId).ToList();


                            foreach (var item in _DES077List)
                            {
                                double i = item.Weightage ?? 0;
                                totalWeight += i;
                            }
                        }
                        if (totalWeight != 100)
                        {
                            errorMG = "Total Weightage should be 100%";
                            return false;
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public bool CheckWeight(Int64? AgencyId, string project, out string errorMG)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMG = "";
                    var getDataByAenecy = db.DES076.Where(x => x.AgencyId == AgencyId && x.Project == project).FirstOrDefault();
                    double totalWeight = 0;
                    if (getDataByAenecy.AgencyId != null)
                    {
                        var _DES077List = db.DES077.Where(x => x.AgencyDocId == getDataByAenecy.AgencyDocId).ToList();


                        foreach (var item in _DES077List)
                        {
                            double i = item.Weightage ?? 0;
                            totalWeight += i;
                        }
                    }
                    if (totalWeight != 100)
                    {
                        errorMG = "Please maintain Agency List and Total Weightage should be 100%";
                        return false;
                    }
                    else
                    {
                        return true;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateEmailFlag(AgencyDocumentModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES076.Where(x => x.AgencyId == model.AgencyId).FirstOrDefault();
                    if (temp != null)
                    {
                        temp.IsEmailSentToAgency = model.IsEmailSentToAgency;
                    }
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public AgencyDocModel AgencyDetail(Int64 AgencyID)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _AgencyModel = db.DES027.Where(x => x.AgencyId == AgencyID).Select(x => new AgencyDocModel
                    {
                        AgencyId = AgencyID,
                        AgencyName = x.AgencyName,
                        AgencyDesc = x.AgencyDescription
                    }).FirstOrDefault();

                    return _AgencyModel;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public AgencyDocModel TransmittalEditDetail(Int64 TransmittalId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    Int64 AgencyID = db.DES079.Where(x => x.TransmittalId == TransmittalId).Select(x => x.AgencyId).FirstOrDefault().Value;
                    var _AgencyModel = db.DES027.Where(x => x.AgencyId == AgencyID).Select(x => new AgencyDocModel
                    {
                        AgencyId = AgencyID,
                        AgencyName = x.AgencyName,
                        AgencyDesc = x.AgencyDescription,
                        TransmittalId = TransmittalId,
                    }).FirstOrDefault();

                    return _AgencyModel;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_TRANSMITTAL_AGENCY__WISE_DOC_Result> BindAgencywiseDocs(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? AgencyID, Int64? TransmittalId, out int recordsTotal, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_TRANSMITTAL_AGENCY__WISE_DOC(startIndex, displayLength, sSearch, AgencyID, sortDirection, sortColumnIndex, Project, TransmittalId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_TRANSMITTAL_EMAIL_HISTORY_Result> BindEmailHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? TransmittalId, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_TRANSMITTAL_EMAIL_HISTORY(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, TransmittalId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_TRANSMITTAL_Detail_Result> BindTransmittlDetail(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? TransmittalId, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_TRANSMITTAL_Detail(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, TransmittalId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 AddTransmittal(TransmittalModel model, out List<string> DocPathList, out string newGeneratedTransmittalNo, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = ""; newGeneratedTransmittalNo = ""; 
                    DES076 dES076 = null;
                    var check = db.DES080.Where(x => x.TransmittalId == model.TransmittalId).ToList();
                    foreach (var Doc in check)
                    {
                        db.DES080.Remove(Doc);
                        db.SaveChanges();
                    }
                    var temp = db.DES079.Find(model.TransmittalId);
                    DES079 _DES079 = temp == null ? new DES079() : temp;
                    string AgencyName = db.DES027.Where(x => x.AgencyId == model.AgencyId).Select(x => x.AgencyName).FirstOrDefault();
                    if (model.Status == "Created")
                    {
                        string TransmittalNo = GenerateTransmittalNo(model.Project, AgencyName, model.AgencyId);
                        _DES079.TransmittalNo = TransmittalNo;
                        newGeneratedTransmittalNo = TransmittalNo;
                    }
                    if (model.TransmittalDocumentList.Count > 0)
                    {
                        var getAgencyDocListId = model.TransmittalDocumentList.Select(x => x.AgencyDocListId).FirstOrDefault();
                        var getAgencyDocId = db.DES077.Where(x => x.AgencyDocListId == getAgencyDocListId).Select(x => x.AgencyDocId).FirstOrDefault();
                        var AgencyDocData = db.DES076.Where(x => x.AgencyDocId == getAgencyDocId).ToList();
                        dES076 = AgencyDocData[0];
                    }

                    _DES079.Project = model.Project;
                    _DES079.AgencyId = model.AgencyId;
                    if (model.FolderId != null)
                    {
                        _DES079.FolderId = model.FolderId;
                    }
                    else
                    {
                        _DES079.FolderId = dES076.FolderId;
                    }
                    _DES079.Status = model.Status;

                    List<DES080> _DES080List1 = new List<DES080>();
                    foreach (var d80 in model.TransmittalDocumentList)
                    {
                        _DES080List1.Add(new DES080
                        {
                            AgencyDocListId = d80.AgencyDocListId,
                            Status = model.Status,
                            CreatedBy = CommonService.objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        });
                    }
                    _DES079.DES080 = _DES080List1;
                    if (temp == null)
                    {
                        if (model.RoleGroup != null)
                        {
                            _DES079.RoleGroup = model.RoleGroup;
                        }
                        else
                        {
                            _DES079.RoleGroup = dES076.RoleGroup;
                        }
                        
                        _DES079.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES079.CreatedOn = DateTime.Now;
                        db.DES079.Add(_DES079);
                    }
                    else
                    {
                        _DES079.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES079.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();

                    IEnumerable<string> PathList = from D80 in db.DES080
                                                   join D77 in db.DES077 on D80.AgencyDocListId equals D77.AgencyDocListId
                                                   join D75 in db.DES075 on D77.JEPDocumentDetailsId equals D75.JEPDocumentDetailsId
                                                   join D60 in db.DES060 on D75.DocumentId equals D60.DocumentId
                                                   where D80.TransmittalId == _DES079.TransmittalId && D60.IsLatest == true && D60.IsMainDocument == true
                                                   select D60.MainDocumentPath;

                    DocPathList = PathList.ToList();
                    return _DES079.TransmittalId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        #region Transmittal Agency
        public List<SP_DES_GET_TRANSMITTAL_AGENCY_Result> GetTransmittalAgencyList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, long? FolderId, string Roles)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_TRANSMITTAL_AGENCY(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, FolderId, Roles).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.AgencyDocId);
                        x.GString = CommonService.Encrypt("AgencyID=" + x.AgencyId);
                        x.AgencyCheckInByName = new clsManager().GetUserNameFromPsNo((x.AgencyCheckInBy));
                    });

                    recordsTotal = rt ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_GENERATE_TRANSMITTAL_Result> GetGenerateTransmittalList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, long? FolderId, string Roles)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_GENERATE_TRANSMITTAL(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, FolderId, Roles).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.AgencyId + "&TransmittalId=" + x.TransmittalId);
                    });

                    recordsTotal = rt ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteAgencyWiseAllDoc(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var _DES076 = db.DES076.Find(Id);
                    if (_DES076 != null)
                    {
                        var _DES077 = db.DES077.Where(x => x.AgencyDocId == _DES076.AgencyDocId).ToList();
                        foreach (var item in _DES077)
                        {
                            if (item != null)
                            {
                                var _DES080 = db.DES080.Where(x => x.AgencyDocListId == item.AgencyDocListId).ToList();
                                foreach (var objDES080 in _DES080)
                                {
                                    if (objDES080 != null)
                                    {
                                        db.DES080.Remove(objDES080);
                                        db.SaveChanges();
                                    }
                                }
                                db.DES077.Remove(item);
                                db.SaveChanges();
                            }
                        }
                        db.DES076.Remove(_DES076);
                        db.SaveChanges();
                    }
                    return _DES076.AgencyDocId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteTransmittalWiseDoc(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES079.Find(Id);
                    if (check != null)
                    {
                        db.DES080.RemoveRange(check.DES080);
                        db.DES079.Remove(check);
                        db.SaveChanges();
                        return check.TransmittalId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool CheckAgencyAlreadyExist(long? agencyId, string project, Int64? AgencyDocId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES076.Where(x => x.Project == project && x.AgencyId == agencyId && (x.AgencyDocId != AgencyDocId || AgencyDocId == null)).ToList();
                    if (data != null && data.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
            }
        }

        public AgencyDocumentModel GetAgencyDocumentById(long id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES076.Where(x => x.AgencyDocId == id).Select(x => new AgencyDocumentModel
                    {
                        AgencyDocId = x.AgencyDocId,
                        Project = x.Project,
                        FolderId = x.FolderId,
                        AgencyId = x.AgencyId,
                        IsEmailSentToAgency = x.IsEmailSentToAgency ?? false,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        EditedBy = x.EditedBy,
                        EditedOn = x.EditedOn,

                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<AgencyDocModel> GetTransDocumentById(long id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES080.Where(x => x.TransmittalId == id).Select(x => new AgencyDocModel
                    {
                        AgencyId = db.DES079.Where(y => y.TransmittalId == id).Select(y => y.AgencyId).FirstOrDefault(),
                        AgencyName = db.DES027.Where(z => z.AgencyId == z.AgencyId).Select(z => z.AgencyName).FirstOrDefault(),
                        AgencyDesc = db.DES027.Where(p => p.AgencyId == p.AgencyId).Select(p => p.AgencyDescription).FirstOrDefault(),
                        TransmittalId = x.AgencyDocListId,
                        CurrentLocationIp = CommonService.GetUseIPConfig.Ip
                    }).ToList();

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public static string GenerateTransmittalNo(string Project, string AgencyName, Int64 AgnecyID)
        {
            try
            {
                int nextDOCNo = 0;
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var listOfTransmittalNo = db.DES079.Where(yy => yy.Project == Project && yy.Status != ObjectStatus.Draft.ToString() && yy.AgencyId == AgnecyID).Select(x => x.TransmittalNo).ToList();
                    List<int> listOfIssueNo = new List<int>();
                    int val = 0;

                    foreach (var item in listOfTransmittalNo)
                    {

                        var getTransmittal = item.Split('-').LastOrDefault();
                        if (getTransmittal != "")
                        {
                            Int32.TryParse(getTransmittal, out val);
                            listOfIssueNo.Add(Convert.ToInt32(val));
                        }
                    }

                    if (listOfIssueNo.Count() > 0)
                    {

                        int findMax = Convert.ToInt32(listOfIssueNo.Max());
                        nextDOCNo = findMax + 1;
                    }
                    else
                    {
                        nextDOCNo = 1;
                    }
                }
                return Project + "-" + Regex.Replace(AgencyName, @"\s+", "") + "-" + String.Format("{0:000}", nextDOCNo);
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<Int64> GetTransmittalDocList(Int64 TransmittalId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<Int64> list = db.DES080.Where(x => x.TransmittalId == TransmittalId).Select(x => x.AgencyDocListId ?? 0).ToList();
                    if (list != null && list.Count > 0)
                    {
                        return list;
                    }
                    else
                    {
                        return new List<long>();
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 AddEmailHistory(TransmittalEmailModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    DES105 _DES105 = new DES105();
                    _DES105.TransmittalId = model.TransmittalId;
                    _DES105.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _DES105.CreatedOn = DateTime.Now;
                    db.DES105.Add(_DES105);
                    db.SaveChanges();
                    return _DES105.TransmittalEmailHistoryID;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_AGENCY_EMAIL_TEMPLATE_Result> GetEmailTemplateByAgency(Int64? TransmittalId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.SP_DES_GET_AGENCY_EMAIL_TEMPLATE(TransmittalId).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<string> GetDownloadFiles(Int64? TransmittalId)
        {

            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    IEnumerable<string> PathList = from D80 in db.DES080
                                                   join D77 in db.DES077 on D80.AgencyDocListId equals D77.AgencyDocListId
                                                   join D75 in db.DES075 on D77.JEPDocumentDetailsId equals D75.JEPDocumentDetailsId
                                                   join D60 in db.DES060 on D75.DocumentId equals D60.DocumentId
                                                   where D80.TransmittalId == TransmittalId && D60.IsLatest == true && D60.IsMainDocument == true
                                                   select D60.MainDocumentPath;
                    return PathList.ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<Tuple<Int64, double>> GetWeightage(Int64? AgencyId, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<Tuple<Int64, double>> list = new List<Tuple<long, double>>();
                    if (AgencyId != null)
                    {
                        var getDataByAenecy = db.DES076.Where(x => x.AgencyId == AgencyId && x.Project == Project).FirstOrDefault();

                        if (getDataByAenecy != null && getDataByAenecy.AgencyId > 0)
                        {
                            var _DES077List = db.DES077.Where(x => x.AgencyDocId == getDataByAenecy.AgencyDocId).ToList();
                            foreach (var item in _DES077List)
                            {
                                double i = item.Weightage ?? 0;
                                list.Add(new Tuple<long, double>(item.AgencyDocListId, i));

                            }
                        }
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        #endregion
    }
}
