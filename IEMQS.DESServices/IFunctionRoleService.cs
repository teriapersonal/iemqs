﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IFunctionRoleService
    {
        bool AddEdit(FunctionRoleModel model,string RoleList, out string errorMsg);        
        List<FunctionRoleModel> GetAllRolebyFunctionId(string Id);
        List<FunctionRoleModel> GetAllFunctionRole();
        List<SP_DES_GET_FUNCTION_ROLE_Result> GetFunctionRoleList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        List<SP_DES_GET_TYPE_Result> GetTypeList();
        List<SP_DES_GET_ROLE_Result> GetRoleList();
        Int64 DeleteFunctionRole(Int64 Id, out string errorMsg);
    }
}
