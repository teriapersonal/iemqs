﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IPartSheetService
    {
        Int64 AddEdit(PartSheetModel model, out string errorMsg);
        List<SP_DES_GET_PART_SHEET_Result> GetPartSheetList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, long FolderId, out int recordsTotal);
        Int64 DeletePartSheet(Int64 Id);
        bool CheckSheetNameAlreadyExist(string sheetName, string project, long? itemSheetId);

        PartSheetModel GetPartSheetById(Int64? Id);

        bool UpdateIsCheckInSheet(Int64? ItemSheetId,bool IsCheckOut);
    }
}
