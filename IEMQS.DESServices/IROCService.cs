﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IROCService
   {

        List<DOCRevModel> BindDocRevision(string DocNo);

        #region ROC
        Int64 Add(long documentId , long AgencyId,string Project,out string errorMsg);

        ROCModel Edit(long rocID,string Rev,bool IsEdit);

        List<SP_DES_GET_ROC_Result> GetROCList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DocumentId, Int64? FolderId,string Project,string Roles);
        #endregion

        #region ROC Comment
        Int64 AddEdit(ROCModel model);

        List<SP_DES_GET_ROCCommentList_Result> GetROCCommentsList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64 ROCId);


        Int64 AddROCComment(ROCCommentModel model);
        bool EditROCComment(ROCModel model);

        bool UpdateAgencySaveAsDraft(Int64? ROCId, Int64 AgencyId);

        bool UpdateROCStatusSubmit(Int64? ROCId, out string errorMsg);

        bool UpdateROCStatusCheckin(Int64? ROCId, out string errorMsg);
        
        Int64 DeleteROCComment(Int64 Id, out string errorMsg);
        #endregion

        #region ROC Comment Attach
        Int64 AddEditCreateROCCommentsAttach(ROCCommentsAttachModel model, out string errorMsg);

        List<SP_DES_GET_ROCCommentsAttach_Result> GetROCCommentsAttachList(string sSearch, int startIndex,
            int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64 ROCCommentsId,
            bool? IsAgencyComment = null, bool? IsLandTComment = null, bool? IsAgencyResponse = null);


        Int64 DeleteROCCommentsAttach(Int64 Id, out string errorMsg);
        #endregion

        #region ROC Commented Document Attach
        Int64 AddEditCommentedDocument(ROCCommentedDocModel model, out string errorMsg);

        List<SP_DES_GET_ROCCommentedDOC_Result> GetROCCommentedDocumentList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64 ROCId,string Project);

        Int64 DeleteROCCommentedDocument(Int64 Id, out string errorMsg);
        #endregion

        void AddEditCreateROCCommentsHistory(long? ROCId, long? ROCCommentedDocId, long? ROCCommentsAttachId, string Description);

        double? GetSrNo(long? ROCId, int? DocumentRevision);

        List<ROCFileAttachModel> GetAllAttachmentPath(long ROCCommentsId);

        Int64? AddRevisedData(ROCModel model);

        List<string> GetAllAttachmentPathForRev(Int64? RocID);


        List<SP_DES_GET_ROC_BY_ROCID_Result> GetROCListByROCID(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection,out int recordsTotal, string ROCId);

        List<SP_DES_GET_ROC_HISTORY_Result> GetROCHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? ROCId);

        void ExportToExcel(Int64? ItemSheetId);

        bool UpdateIsCheckInROC(Int64? RocId);


        List<string> GetJEPData(Int64? RocID);
    }
}
