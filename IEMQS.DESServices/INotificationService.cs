﻿using IEMQS.DESCore.Data;
using IEMQSImplementation;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface INotificationService
    {
        List<GLB010> GetNotificationContent(DateTime currentdate);

        string GettotalUnreadNotificationasperPSno(string PSNo);

        string MarkNotificationasRead(string PSNo);

        List<SP_DES_GET_NOTIFICATION_Result> GetAllNotificationByPSNo(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string PSNo, out int recordsTotal);
    }
}
