﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IHBOMService
    {
        List<PowerViewModel> GetHBOMPowerViewList(string Project, long? ItemId = null);

        TreeViewModel GetHBOMTreeViewList(string Project, long? ItemId = null);

        Int64 GetItemIdByProject(string Project);
    }
}
