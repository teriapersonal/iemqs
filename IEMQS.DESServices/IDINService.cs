﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace IEMQS.DESServices
{
    public interface IDINService
    {
        List<DINGroupModel> GetDistributionGroup();
        Int64? AddEdit(DINModel model, out string errorMsg);
        List<SP_DES_GET_DIN_JEP_DOC_Result> GetJEPDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, Int64 DINId, out int recordsTotal);
        List<SP_DES_GET_DIN_GLOBAL_JEP_DOC_Result> GetGlobalJEPDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string DocumentNo, string CurrentProject, Int64 DINId);
        Int64 AddDinRefDocument(DINRefDocumentModel model, out string errorMsg);

        ProjectModel GetProjectDetails(string Project);

        List<SP_DES_GET_DIN_JepDocumentDetails_Result> GetJepDocumentDetails(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection,Int64? DINId,string Roles, out int recordsTotal);

        Int64 UpdateGenralRemark(DINRefDocumentModel model);

        Int64 DeleteDINRefDocument(Int64 Id, out string errorMsg);
        List<SP_DES_GET_DIN_Result> GetDINList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? folderId, string Roles,string Project,out int recordsTotal);

        Int64 DeleteDIN(Int64 DINId, out string errorMsg);

        DINModel GetDocumentByID(Int64? ID);

        List<DINPolicyStepsModel> GetPolicySteps(string type, Int64? BUId, Int64? DINId);
       
        Int64 UpdateDINLifeCycle(Int64? DINId, out string errorMsg);

        bool CheckDINNo(string DINNo, long? DINId, string project,int IssueNo);

        int GenerateIssueNo(string ProjectNo);

        int GenerateDINNo(string ProjectNo);
        Int64 AddAcknowledgeNote(Int64? DINId, string AcknowledgeNote);

        bool UpdateGird(DINRefDocumentModel DocDetail, out string errorMsg,out string infoMsg);

        List<SP_DES_GET_DIN_POLICY_BYLEVEL_Result> GetDINUserLevel(string RoleGroup, string Roles, Int64? DINId,string Department);

        Int64? ReviseDINPolicy(Int64 DINId, out string errorMsg);

        Int64? UpdateDINPolicy(Int64 DINLifeCycleId,Int64 DinID ,out string errorMsg, out string infoMsg);

        List<SP_DES_GET_DIN_HISTORY_Result> GetDINHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string DINNo, out int recordsTotal);
       
       Int64? AddStatus(DINModel prms,string Project,out string errorMsg);

        Int64? IsAllowACkTOUser(Int64? DINId, string UserDepartment, string PSNumber);
        Int64? UpdateAckStatus(Int64? DINACKId,Int64? DINID, string Project, out string errorMsg);

        List<SP_DES_GET_DIN_ACKNOWLEDGED_Result> ViewACKData(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? DocId, out int recordsTotal);

        List<DepartmentName> getDataByGroupID(Int64? gropId);

        string GetCustomerName(string CustomerCode);

        Int64? DinRevision(Int64? DinId,Int64? BUId,string NewGenrateDINNo,string Department);

        List<SP_DES_GET_PENDING_DIN_Result> GetPendingDINList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string psno, string Roles, out int recordsTotal, string Project);

        List<SelectListItem> GetContractProjectList(string projectNo);

        bool DINSaveAsDraft(Int64? DinID);

        bool UpdateIsCheckInDIN(Int64? DinId,bool HasPSNo);

         //List<DINDistributeDocument> GetDistributeDocList(Int64? DinId);

        List<DINDistributeDocument> GetDistributeDOCData(Int64? DinId);

        bool AddDocDistribute(DINDistributeDocument DINDistDocument);

        bool AddDocElectronicDistribute(DINDistributeDocument DINDistDocument);

        bool IsContainDocument(Int64? DinID);
    }
}
