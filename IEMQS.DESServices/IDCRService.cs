﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace IEMQS.DESServices
{
    public interface IDCRService
    {
        #region Yash DCR
        string AddDCR(DCRModel model);
        string AddDetailDCR(DCRModel model);

        List<SP_DES_GET_DCR_JEP_DOC_Result> GetJEPDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, out int recordsTotal, Int64? DCRId);
        Int64 AddEdit(DCRDocumentModel model);
        List<SP_DES_GET_DCR_GLOBAL_JEP_DOC_Result> GetJEPGlobalDocList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string DocumentNo, Int64? DCRId, string CurrentProject);
        List<SP_DES_GET_DCR_AFFECTED_DOC_Result> GetAffectedDCRDoc(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DCRId);
        bool DeleteAffectedDocument(Int64 Id, out string errorMsg);
        long UploadDCRSupportDocs(DCRDocumentMappingModel model, out string errorMsg);
        List<SP_DES_GET_DCR_SUPPORTED_DOCUMENT_Result> GetDCRSupportedDocumentList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DCRId);
        List<SP_DES_GET_DCR_PART_Result> GetPartList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DCRId, string Project);
        Int64 AddPart(DCRPartModel model);
        List<SP_DES_GET_DCR_AFFECTED_PART_Result> GetDCRAffectedPart(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DCRId);
        DCRModel GetDCRById(DCRModel model);
        bool UpdateDCRStatus(Int64? DCRId, string Status);
        bool Delete(Int64? Id);
        List<SP_DES_GET_DCR_LIST_Result> GetDCRList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, String Roles);
        bool DeleteAffectedPart(Int64? Id);
        string AddReviewOrEvaluate(DCRModel model);

        List<SelectListItem> GetAllDOCByProject(string Project);

       // List<SelectListItem> GetDCRName();
       
         List<SP_DES_GET_DCR_DEPT_PERSON_BYDCRID_Result> GetDeptPersonGridByDCRId(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DCRId, Int64? DCREvaluatId);
        bool UpdateApproval(DCREvaluateCommentModel model);

        List<DCRPolicyStepsModel> GetPolicySteps(string Type, Int64? BUId, Int64? DCRId);

        string GenerateDCRNumber(string Project);
        List<SelectListItem> GetContractProjectList(string projectNo);
        List<DOCModel> ReleasedProcess(DCRDocumentModel DCRDocumentModel, DCRPartModel DCRPartModel, DCRModel DCRModel, string Project);

        List<SP_DES_GET_DCR_HISTORY_Result> GetDCRHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DCRId);
        Int64 DeleteDocFile(Int64 Id, out string errorMsg);

        List<SP_DES_GET_DCR_PENDINGTASK_LIST_Result> GetPendingDCRList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Roles, string Project,string psno);

        DCRModel GetDCRCommentPersonModel(DCRModel model);

        DES089 GetDCRIdByDCRSupportedDocId(Int64 DCRSupportedDocId);

        string PolicyStepStatus(Int64? DCRId);

        string Identical(string Project);
        #endregion
    }
}
