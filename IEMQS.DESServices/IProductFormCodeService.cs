﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IProductFormCodeService
    {
        Int64 AddEdit(ProductFormCodeModel model, out string errorMsg);
        ProductFormCodeModel GetProductFormbyId(Int64 Id);
        List<ProductFormCodeModel> GetAllProductForm();
        List<SP_DES_GET_PRODUCT_FORM_CODE_Result> GetProductFormList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        Int64 DeleteProductForm(Int64 Id, out string errorMsg);
        List<string> GetProdctFormList();
        bool UpdateSubProduct(ProductFormCodeModel model);

        bool GetRoleGroup(string PsNo);
    }
}
