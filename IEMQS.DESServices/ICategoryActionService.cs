﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface ICategoryActionService
    {
        Int64 AddEdit(CategoryActionModel model, out string errorMsg);
        CategoryActionModel GetCategorybyId(Int64 Id);
        List<CategoryActionModel> GetAllCategory();
        List<CategoryActionModel> GetAllNameByType(int TypeId);
        List<SP_DES_GET_CATEGORY_ACTION_Result> GetCategoryActionList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);        
        Int64 DeleteCategory(Int64 Id, out int TypeId, out string errorMsg); 
    }
}
