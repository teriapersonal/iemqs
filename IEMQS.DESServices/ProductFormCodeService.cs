﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class ProductFormCodeService : IProductFormCodeService
    {
        public Int64 AddEdit(ProductFormCodeModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    #region check Validation
                    //if (model.ProductFormCodeId > 0)
                    //{
                    //    var checkchild = db.DES028.Find(model.ProductFormCodeId);
                    //    //if (checkchild != null)
                    //    //{
                    //    //    var checkDES066 = db.DES066.Select(x => x.ProductFormCodeId == model.ProductFormCodeId).ToList();

                    //    //    if (checkDES066 != null && checkDES066.Count() > 0)
                    //    //    {
                    //    //        errorMsg = "Product form is already in used. you cannot edit";
                    //    //        return 0;
                    //    //    }
                    //    //}
                    //}

                   
                    #endregion

                    var temp = db.DES028.Find(model.ProductFormCodeId);
                    DES028 _DES028 = temp == null ? new DES028() : temp;

                    _DES028.BUId = model.BUId;       
                    _DES028.Description1 = model.Description1;
                    _DES028.Description2 = model.Description2;
                    _DES028.Description3 = model.Description3;
                    _DES028.BOMReportSize = model.BOMReportSize;
                    _DES028.DrawingBOMSize = model.DrawingBOMSize;

                    if (temp == null)
                    {
                        var checkdublicateCode = db.DES028.Where(x => x.SubProductForm == model.SubProductForm && x.ProductForm == model.ProductForm && x.ProductFormCodeId != model.ProductFormCodeId).FirstOrDefault();
                        if (checkdublicateCode != null)
                        {
                            errorMsg = "SubProduct form is already exist";
                            return 0;
                        }

                        var check = db.DES028.Where(x => x.ProductFormCode == model.ProductFormCode && x.ProductFormCodeId != model.ProductFormCodeId).FirstOrDefault();
                        if (check != null)
                        {
                            errorMsg = "Product form code is already exist";
                            return 0;
                        }
                        _DES028.ProductFormCode = model.ProductFormCode;
                        _DES028.ProductForm = model.ProductForm;
                        _DES028.SubProductForm = model.SubProductForm;
                        _DES028.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES028.CreatedOn = DateTime.Now;
                        db.DES028.Add(_DES028);
                    }
                    else
                    {
                        _DES028.ProductFormCode = model.ProductFormCode;
                        _DES028.ProductForm = model.ProductForm;
                        _DES028.SubProductForm = model.SubProductForm;
                        _DES028.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES028.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES028.ProductFormCodeId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<ProductFormCodeModel> GetAllProductForm()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES028.OrderBy(x => x.ProductFormCodeId).Select(x => new ProductFormCodeModel
                    {
                        ProductFormCodeId = x.ProductFormCodeId,
                        BUId = x.BUId,
                        ProductForm = x.ProductForm,
                        SubProductForm = x.SubProductForm,
                        ProductFormCode = x.ProductFormCode,
                        Description1 = x.Description1,
                        Description2 = x.Description2,
                        Description3 = x.Description3,
                        BOMReportSize = x.BOMReportSize,
                        DrawingBOMSize = x.DrawingBOMSize
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public ProductFormCodeModel GetProductFormbyId(long Id)
        {
            throw new NotImplementedException();
        }

        public List<SP_DES_GET_PRODUCT_FORM_CODE_Result> GetProductFormList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PRODUCT_FORM_CODE(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteProductForm(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES028.Find(Id);
                    if (check != null)
                    {
                        if (check.DES066 != null && check.DES066.Count > 0)
                        {
                            errorMsg = "Product form is already in used. you cannot delete";
                            return 0;
                        }
                        db.DES028.Remove(check);
                        db.SaveChanges();
                        return check.ProductFormCodeId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<string> GetProdctFormList()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.uvwMaterial.Select(u => u.t_prod).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateSubProduct(ProductFormCodeModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES028.Where(y => y.ProductForm == model.ProductForm && y.SubProductForm == model.SubProductForm).FirstOrDefault();
                    if (data != null)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public bool GetRoleGroup(string PsNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string strrolegroup = string.Empty;
                    var data = db.SP_DES_GET_USER_ROLE_ROLEGROUP(PsNo).Select(x => x.RoleGroup).ToList();
                    if (data.Contains("ITA"))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

    }
}
