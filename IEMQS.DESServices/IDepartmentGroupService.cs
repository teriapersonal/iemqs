﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;

namespace IEMQS.DESServices
{
    public interface IDepartmentGroupService
    {
        Int64 AddEdit(DINGroupModel model, out string errorMsg,out string infoMsg);
        DINGroupModel GetDepartmentGroupbyId(Int64 Id);      
        List<DINGroupModel> GetAllDepartmentGroup();
        List<SP_DES_GET_DIN_GROUP_Result> GetDepartmentGroupList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal);
        List<SP_DES_GET_TYPE_Result> GetTypeList();
        List<SP_DES_GET_PS_NUMBER_BY_DEPARTMENT_Result> GetPSNumberList(string dept);       
        List<SP_DES_GET_PS_NUMBER_BY_DEPARTMENT_Result> GetAllFunctionByPSNo(string dept);
        List<SP_DES_GET_PSNO_ON_TERMS_AND_DEPT_Result> GetALLPSNOByNameandDepartment(string terms,string dept);
        Int64 DeleteDepartmentGroup(Int64 Id, out string errorMsg);
        bool CheckGroupNameAlreadyExist(string name, Int64? DINgroupId);        
    }
}
