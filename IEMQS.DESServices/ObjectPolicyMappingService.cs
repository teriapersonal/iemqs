﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class ObjectPolicyMappingService : IObjectPolicyMappingService
    {
        public Int64 AddEdit(ObjectPolicyMappingModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    #region Check Validation
                    
                    var check = db.DES007.Where(x => x.ObjectId == model.ObjectId && x.PolicyId == model.PolicyId && x.MappingId != model.MappingId).FirstOrDefault();
                    if (check != null)
                    {
                        errorMsg = "Object policy mapping is already exist";
                        return 0;
                    }

                    var checkdublicateCode = db.DES007.Where(x => x.ObjectId == model.ObjectId && x.MappingId != model.MappingId).FirstOrDefault();
                    if (checkdublicateCode != null)
                    {
                        errorMsg = "Object Name is already exist";
                        return 0;
                    }

                    #endregion

                    var temp = db.DES007.Find(model.MappingId);
                    DES007 _DES007 = temp == null ? new DES007() : temp;

                    _DES007.ObjectId = model.ObjectId;
                    _DES007.PolicyId = model.PolicyId;                   

                    if (temp == null)
                    {
                        _DES007.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES007.CreatedOn = DateTime.Now;
                        db.DES007.Add(_DES007);
                    }
                    else
                    {
                        _DES007.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES007.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES007.MappingId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<ObjectPolicyMappingModel> GetAllObjectPolicyMapping()
        {            
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES007.OrderBy(x => x.MappingId).Select(x => new ObjectPolicyMappingModel
                    {
                        MappingId = x.MappingId,
                        ObjectId = x.ObjectId,
                        PolicyId = x.PolicyId
                    }).ToList();
                }
            }
            catch (Exception ex) 
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public ObjectPolicyMappingModel GetObjectPolicyMappingbyId(long Id)
        {
            throw new NotImplementedException();
        }

        public List<SP_DES_GET_OBJECT_POLICY_MAPPING_Result> GetObjectPolicyMappingList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_OBJECT_POLICY_MAPPING(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteObjectPolicyMapping(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES007.Find(Id);
                    if (check != null)
                    {
                        db.DES007.Remove(check);
                        db.SaveChanges();
                        return check.MappingId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

    }
}
