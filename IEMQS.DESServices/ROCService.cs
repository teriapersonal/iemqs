﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace IEMQS.DESServices
{
    public class ROCService : IROCService
    {
        public List<DOCRevModel> BindDocRevision(string DocNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES059.Where(x => x.DocumentNo == DocNo).Select(x => new DOCRevModel
                    {
                        RevisionId = x.DocumentRevision,
                        DocRev = "R" + x.DocumentRevision.ToString()
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);                
                throw;
            }
        }

        #region ROC
        public Int64 Add(long documentId, long AgencyId, string Project, out string errorMsg)
        {
            try
            {
                errorMsg = "";
                long ROCId = 0;
                DES069 _DES069 = new DES069();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {                   
                    var data = db.DES069.Where(x => x.DocumentId == documentId && x.AgencyId == AgencyId && x.Project == Project && x.IsLatest == true).FirstOrDefault();
                    if (data == null)
                    {

                        var d59 = db.DES059.Where(x => x.DocumentId == documentId).Select(x => new ROCModel
                        {
                            DocumentId = x.DocumentId,
                            DocNo = x.DocumentNo,
                            DocRevision = x.DocumentRevision,
                            DocTitle = x.DocumentTitle,
                            Project = x.Project,
                            EquipmentNo = x.DES051.EquipmentDiameter,
                            AgencyName = db.DES027.Where(y => y.AgencyId == AgencyId).Select(y => y.AgencyName).FirstOrDefault(),
                        }).FirstOrDefault();
                        if (d59 != null)
                        {
                            var getProjectData = db.DES051.Where(x => x.Project == Project).FirstOrDefault();
                            int cnt = db.DES069.Where(x => x.DocumentId == documentId && x.AgencyId == AgencyId && x.Status == ObjectStatus.Completed.ToString()).Count();
                            string ROCRefNo = GenerateROCRef(d59.DocumentId, d59.AgencyName, cnt);
                            int Version = 1;
                            _DES069.EquipmentNo = getProjectData.EquipmentNo;
                            _DES069.ROCRefNo = ROCRefNo;
                            _DES069.Project = d59.Project;
                            _DES069.AgencyId = AgencyId;
                            _DES069.Version = Version;
                            _DES069.DocNo = d59.DocNo;
                            _DES069.DocTitle = d59.DocTitle;
                            _DES069.DocRev = d59.DocRevision;
                            _DES069.DocumentId = d59.DocumentId;
                            _DES069.Status = ObjectStatus.Created.ToString();
                            _DES069.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            _DES069.CreatedOn = DateTime.Now;
                            _DES069.IsLatest = true;
                            _DES069.IsROCCheckIn = true;
                            _DES069.ROCCheckInBy = CommonService.objClsLoginInfo.UserName;
                            _DES069.ROCCheckInOn = DateTime.Now;
                            db.DES069.Add(_DES069);
                            db.SaveChanges();
                            ROCId = _DES069.ROCId;
                        }

                    }
                    else if (data.Status == ObjectStatus.Completed.ToString())
                    {
                        errorMsg = "Agency have alredy created Roc in completed state. Please revice to edit.";
                        return 0;
                    }
                    else
                    {
                        ROCId = data.ROCId;
                    }
                    AddEditCreateROCCommentsHistory(ROCId, null, null, "ROC " + _DES069.ROCRefNo + " has been Created");
                }

                return ROCId;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public ROCModel Edit(long rocId, string Rev, bool IsEdit)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data1 = db.DES069.Where(x => x.ROCId == rocId).FirstOrDefault();
                    double? LastSrnoCnt = db.DES071.Where(x => x.DocumentId == data1.DocumentId).Select(x => x.SrNo).Max();
                    ROCModel rOCModel = new ROCModel();
                    {
                        rOCModel.ROCId = data1.ROCId;
                        rOCModel.LastSrNo = (LastSrnoCnt == null) ? 1 : LastSrnoCnt + 0.1;
                        rOCModel.EquipmentNo = data1.EquipmentNo;
                        rOCModel.ROCRefNo = data1.ROCRefNo;
                        rOCModel.Project = data1.Project;
                        rOCModel.Version = data1.Version;
                        rOCModel.AgencyId = data1.AgencyId;
                        rOCModel.AgencyName = data1.DES027.AgencyName;
                        rOCModel.DocNo = data1.DocNo;
                        rOCModel.DocTitle = data1.DocTitle;
                        rOCModel.DocRev = data1.DocRev;
                        rOCModel.SubmittedDate = data1.SubmittedDate;
                        rOCModel.DocumentId = data1.DocumentId;
                        rOCModel.IsLatest = true;
                        rOCModel.Status = data1.Status;
                        rOCModel.CreatedBy = data1.CreatedBy;
                        rOCModel.CreatedOn = data1.CreatedOn;
                    }
                    return rOCModel;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ROC_Result> GetROCList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? DocumentId, Int64? FolderId, string Project,string Roles)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ROC(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, FolderId, DocumentId, Project,Roles).ToList();
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("rocID=" + x.ROCId);
                    });
                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        #endregion

        #region ROC Comment Add/Edit
        public long AddEdit(ROCModel model)
        {
            throw new NotImplementedException();
        }

        public List<SP_DES_GET_ROCCommentList_Result> GetROCCommentsList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64 ROCId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ROCCommentList(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, ROCId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);                
                throw;
            }
        }

        public Int64 AddROCComment(ROCCommentModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var objDES071 = db.DES069.Where(x => x.ROCId == model.ROCId).FirstOrDefault();
                    DES071 _DES071 = new DES071();
                    _DES071.ROCId = model.ROCId;
                    _DES071.Status = model.Status;
                    _DES071.DocumentId = model.DocumentId;
                    _DES071.CommentLocation = model.CommentLocation;
                    _DES071.AgencyComments = model.AgencyComments;
                    _DES071.AgencyCommentsDate = model.AgencyCommentsDate;
                    _DES071.SrNo = model.SrNo;
                    _DES071.DocumentRevision = model.DocumentRevision;
                    _DES071.LandTComment = model.LandTComment;
                    _DES071.LandTCommentDate = model.LandTCommentDate;
                    _DES071.AgencyResponse = model.AgencyResponse;
                    _DES071.AgencyResponseDate = model.AgencyResponseDate;
                    _DES071.ClientDocNo = model.ClientDocNo;
                    _DES071.ClientDocRev = model.ClientDocRev;
                    _DES071.CustomerSrNo = model.CustomerSrNo;
                    _DES071.CommfromCustDepts = model.CommfromCustDepts;
                    _DES071.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _DES071.CreatedOn = DateTime.Now;
                    db.DES071.Add(_DES071);

                    db.SaveChanges();
                    AddEditCreateROCCommentsHistory(model.ROCId, null, null, "ROC " + objDES071.ROCRefNo + " Comment has been added");
                    return _DES071.ROCCommentsId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool EditROCComment(ROCModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (model.ROCCommentList != null)
                    {
                        foreach (var _DES071 in model.ROCCommentList)
                        {
                            if (!_DES071.IsStatusClosed)
                            {
                                DES071 objDES071 = db.DES071.Where(x => x.ROCCommentsId == _DES071.ROCCommentsId).FirstOrDefault();
                                if (objDES071 != null)
                                {
                                    objDES071.Status = _DES071.Status;
                                    objDES071.ClientDocNo = _DES071.ClientDocNo;
                                    objDES071.ClientDocRev = _DES071.ClientDocRev;
                                    objDES071.CommentLocation = _DES071.CommentLocation;
                                    objDES071.AgencyComments = _DES071.AgencyComments;
                                    objDES071.AgencyCommentsDate = _DES071.AgencyCommentsDate;
                                    objDES071.LandTComment = _DES071.LandTComment;
                                    objDES071.LandTCommentDate = _DES071.LandTCommentDate;
                                    objDES071.AgencyResponse = _DES071.AgencyResponse;
                                    objDES071.AgencyResponseDate = _DES071.AgencyResponseDate;
                                    objDES071.CustomerSrNo = _DES071.CustomerSrNo;
                                    objDES071.CommfromCustDepts = _DES071.CommfromCustDepts;
                                    objDES071.EditedBy = CommonService.objClsLoginInfo.UserName;
                                    objDES071.EditedOn = DateTime.Now;
                                    db.SaveChanges();
                                }
                            }
                        }
                    }
                    if (model.Version != null)
                    {
                        var data = db.DES069.FirstOrDefault(x => x.ROCId == model.ROCId);
                        data.Version = model.Version;
                        db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {            
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public double? GetSrNo(long? ROCId, int? DocumentRevision)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    double? LastSrnoCnt = db.DES071.Where(x => x.DocumentRevision == DocumentRevision && x.ROCId == ROCId).Select(x => x.SrNo).Max();
                    double? LastSrNo = (LastSrnoCnt == null) ? 1 : LastSrnoCnt + 0.1;
                    return LastSrNo;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateAgencySaveAsDraft(Int64? ROCId, Int64 AgencyId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES069 = db.DES069.Find(ROCId);
                    if (_DES069 != null)
                    {
                        _DES069.AgencyId = AgencyId;
                        db.SaveChanges();
                        AddEditCreateROCCommentsHistory(ROCId, null, null, "ROC " + _DES069.ROCRefNo + "'s agency saved as Draft");
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool UpdateROCStatusSubmit(Int64? ROCId, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.DES071.Where(x => x.ROCId == ROCId).FirstOrDefault();
                    if (data != null)
                    {
                        errorMsg = "";
                        var _DES069 = db.DES069.Find(ROCId);
                        if (_DES069 != null)
                        {
                            _DES069.Status = ObjectStatus.Completed.ToString();
                            _DES069.SubmittedDate = DateTime.Now;
                            _DES069.IsROCCheckIn = false;
                            _DES069.ROCCheckInBy = null;
                            _DES069.ROCCheckInOn = DateTime.Now;
                            db.SaveChanges();
                            AddEditCreateROCCommentsHistory(ROCId, null, null, "Submit");
                        }
                        return true;                        
                    }
                    else
                    {
                        errorMsg = "Please add atleast one document.";
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);               
                throw;
            }
        }

        public Int64 DeleteROCComment(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var _DES071 = db.DES071.Find(Id);
                    if (_DES071 != null)
                    {
                        var RocCommentID = _DES071.ROCCommentsId;
                        db.DES071.Remove(_DES071);
                        db.SaveChanges();
                        return RocCommentID;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        #endregion

        #region ROC Comments Attach

        public Int64 AddEditCreateROCCommentsAttach(ROCCommentsAttachModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    var temp = db.DES072.Find(model.ROCCommentsAttachId);
                    DES072 _DES072 = temp == null ? new DES072() : temp;

                    _DES072.ROCCommentsId = model.ROCCommentsId;
                    _DES072.DocumentPath = model.DocumentPath;
                    _DES072.DocumentTitle = model.DocumentTitle;
                    _DES072.DocumentFormat = model.DocumentFormat;
                    _DES072.IsAgencyComment = model.IsAgencyComment;
                    _DES072.IsLandTComment = model.IsLandTComment;
                    _DES072.IsAgencyResponse = model.IsAgencyResponse;
                    _DES072.FileLocation = model.FileLocation;

                    if (temp == null)
                    {
                        _DES072.CreatedBy = model.CreatedBy;
                        _DES072.CreatedOn = DateTime.Now;
                        db.DES072.Add(_DES072);
                    }
                    else
                    {
                        _DES072.EditedBy = model.EditedBy;
                        _DES072.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    AddEditCreateROCCommentsHistory(null, model.ROCCommentsId, null, "ROC " + model.DocumentTitle + "'s added.");

                    return _DES072.ROCCommentsAttachId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ROCCommentsAttach_Result> GetROCCommentsAttachList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64 ROCCommentsId, bool? IsAgencyComment = null, bool? IsLandTComment = null, bool? IsAgencyResponse = null)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ROCCommentsAttach(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, ROCCommentsId, IsAgencyComment, IsLandTComment, IsAgencyResponse).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteROCCommentsAttach(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    var objDES072 = db.DES072.Find(Id);
                    var getId = objDES072.DES071.ROCId;
                    var title = objDES072.DocumentTitle;
                    if (objDES072 != null)
                    {
                        db.DES072.Remove(objDES072);
                        db.SaveChanges();
                        AddEditCreateROCCommentsHistory(getId, null, null, "ROC " + title + "'s has been deleted");
                        return objDES072.ROCCommentsAttachId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        #endregion

        #region ROC Commented Document

        public Int64 AddEditCommentedDocument(ROCCommentedDocModel model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var temp = db.DES070.Find(model.ROCCommentedDocId);
                    DES070 _DES070 = temp == null ? new DES070() : temp;

                    _DES070.ROCId = model.ROCId;
                    _DES070.DocumentPath = model.DocumentPath;
                    _DES070.DocumentTitle = model.DocumentTitle;
                    _DES070.DocumentFormat = model.DocumentFormat;
                    _DES070.FileLocation = model.FileLocation;
                    if (temp == null)
                    {
                        _DES070.CreatedBy = model.CreatedBy;
                        _DES070.CreatedOn = DateTime.Now;
                        db.DES070.Add(_DES070);
                    }
                    else
                    {
                        _DES070.EditedBy = model.EditedBy;
                        _DES070.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();

                    AddEditCreateROCCommentsHistory(model.ROCId, null, null, "ROC " + model.DocumentTitle + "'s comment added");
                    return _DES070.ROCCommentedDocId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ROCCommentedDOC_Result> GetROCCommentedDocumentList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64 ROCId, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {                   
                    var data = db.SP_DES_GET_ROCCommentedDOC(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, ROCId, Project).ToList();
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.DocumentId);
                    });

                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteROCCommentedDocument(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    var objDES070 = db.DES070.Find(Id);
                    var CId = objDES070.ROCCommentedDocId;
                    if (objDES070 != null)
                    {
                        db.DES070.Remove(objDES070);
                        db.SaveChanges();
                        AddEditCreateROCCommentsHistory(null, CId, null, "Delete ROC Commented Document");
                        return objDES070.ROCCommentedDocId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        #endregion


        public static string GenerateROCRef(long? DocumentId, string AgencyName, int AgencyCompletedCount)
        {
            return DocumentId + "-" + "ROC" + "-" + Regex.Replace(AgencyName, @"\s+", "") + "-" + String.Format("{0:000}", AgencyCompletedCount + 1);
        }

        public void AddEditCreateROCCommentsHistory(long? ROCId, long? ROCCommentedDocId, long? ROCCommentsAttachId, string Description)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DES073 _DES073 = new DES073();

                    _DES073.ROCId = ROCId ?? null;
                    _DES073.ROCCommentedDocId = ROCCommentedDocId ?? null;
                    _DES073.ROCCommentsAttachId = ROCCommentsAttachId ?? null;
                    _DES073.Description = Description;
                    _DES073.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _DES073.CreatedOn = DateTime.Now;
                    db.DES073.Add(_DES073);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<ROCFileAttachModel> GetAllAttachmentPath(long ROCCommentsId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES072.Where(x => x.ROCCommentsId == ROCCommentsId).Select(x => new ROCFileAttachModel
                    {
                        ROCCommentsAttachId = x.ROCCommentsAttachId,
                        DocumentPath = x.DocumentPath
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64? AddRevisedData(ROCModel model)
        {
            DES069 _DES069 = new DES069();
            Int64? NewROCId;
            using (IEMQSDESEntities db = new IEMQSDESEntities())
            {
                var _D69 = db.DES069.Find(model.ROCId);
                if (_D69 != null)
                {
                    _D69.IsLatest = false;
                    db.SaveChanges();
                }
                _DES069.EquipmentNo = _D69.EquipmentNo;
                _DES069.ROCRefNo = _D69.ROCRefNo;
                _DES069.Project = model.Project;
                _DES069.Version = _D69.Version + 1;
                _DES069.AgencyId = _D69.AgencyId;
                _DES069.DocNo = _D69.DocNo;
                _DES069.DocTitle = _D69.DocTitle;
                _DES069.DocRev = _D69.DocRev;
                _DES069.SubmittedDate = _D69.SubmittedDate;
                _DES069.DocumentId = _D69.DocumentId;
                _DES069.Status = ObjectStatus.Created.ToString();
                _DES069.CreatedBy = CommonService.objClsLoginInfo.UserName;
                _DES069.CreatedOn = DateTime.Now;
                _DES069.FolderId = model.FolderId;
                _DES069.IsLatest = true;
                _DES069.IsROCCheckIn = true;
                _DES069.ROCCheckInBy= CommonService.objClsLoginInfo.UserName;
                _DES069.ROCCheckInOn = DateTime.Now;
                db.DES069.Add(_DES069);
                db.SaveChanges();
                NewROCId = _DES069.ROCId;

                var _D71 = db.DES071.Where(x => x.ROCId == model.ROCId).ToList();
                if (_D71 != null)
                {
                    foreach (var rOCComments in _D71)
                    {
                        DES071 _DES071 = new DES071();
                        _DES071.ROCId = NewROCId;
                        _DES071.Status = rOCComments.Status;
                        _DES071.SrNo = rOCComments.SrNo;
                        _DES071.DocumentId = rOCComments.DocumentId;
                        _DES071.CommentLocation = rOCComments.CommentLocation;
                        _DES071.AgencyComments = rOCComments.AgencyComments;
                        _DES071.AgencyCommentsDate = rOCComments.AgencyCommentsDate;
                        _DES071.LandTComment = rOCComments.LandTComment;
                        _DES071.LandTCommentDate = rOCComments.LandTCommentDate;
                        _DES071.ClientDocNo = rOCComments.ClientDocNo;
                        _DES071.ClientDocRev = rOCComments.ClientDocRev;
                        _DES071.AgencyResponse = rOCComments.AgencyResponse;
                        _DES071.AgencyResponseDate = rOCComments.AgencyResponseDate;
                        _DES071.CustomerSrNo = rOCComments.CustomerSrNo;
                        _DES071.CommfromCustDepts = rOCComments.CommfromCustDepts;
                        _DES071.DocumentFormat = rOCComments.DocumentFormat;
                        _DES071.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES071.CreatedOn = DateTime.Now;
                        _DES071.DocumentRevision = rOCComments.DocumentRevision;
                        db.DES071.Add(_DES071);
                        db.SaveChanges();
                        Int64 NewROCCommentsId;
                        NewROCCommentsId = _DES071.ROCCommentsId;
                        var _D72 = db.DES072.Where(x => x.ROCCommentsId == rOCComments.ROCCommentsId).ToList();
                        int i = 0;
                        foreach (var rOCCommentsAttachId in _D72)
                        {
                            DES072 _DES072 = new DES072();
                            _DES072.ROCCommentsId = NewROCCommentsId;
                            _DES072.DocumentPath = model.newPathAfterRevise[i].ToString();
                            _DES072.DocumentTitle = rOCCommentsAttachId.DocumentTitle;
                            _DES072.DocumentFormat = rOCCommentsAttachId.DocumentFormat;
                            _DES072.IsAgencyComment = rOCCommentsAttachId.IsAgencyComment;
                            _DES072.IsLandTComment = rOCCommentsAttachId.IsLandTComment;
                            _DES072.IsAgencyResponse = rOCCommentsAttachId.IsAgencyResponse;
                            _DES072.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            _DES072.CreatedOn = DateTime.Now;
                            _DES072.FileLocation = model.CurrentLocation;
                            db.DES072.Add(_DES072);
                            db.SaveChanges();
                            model.newPathAfterRevise.RemoveAt(i);
                        }
                        NewROCCommentsId = 0;
                    }
                }
                AddEditCreateROCCommentsHistory(NewROCId, null, null, "ROC " + _D69.ROCRefNo + "'s has been revised");
            }
            return NewROCId;
        }

        public List<string> GetAllAttachmentPathForRev(Int64? RocId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DES071 _DES071 = new DES071();
                    var getDCommentsData = db.DES071.Where(x => x.ROCId == RocId).Select(x => x.ROCCommentsId).ToList();
                    List<string> files = new List<string>();

                    if (getDCommentsData.Count > 0)
                    {
                        foreach (var getIdFromDCommentsData in getDCommentsData)
                        {
                            var selectDOCPath = db.DES072.Where(x => x.ROCCommentsId == getIdFromDCommentsData).Select(x => x.DocumentPath).ToList();
                            if (selectDOCPath.Count > 0)
                            {
                                foreach (var item in selectDOCPath)
                                {
                                    files.Add(item);
                                }
                            }
                        }
                    }
                    return files;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }            
        }

        public List<SP_DES_GET_ROC_BY_ROCID_Result> GetROCListByROCID(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string RocNO)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ROC_BY_ROCID(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, RocNO).ToList();
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("rocID=" + x.ROCId);
                    });
                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ROC_HISTORY_Result> GetROCHistory(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64? ROCId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ROC_HISTORY(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, ROCId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public void ExportToExcel(Int64? RocId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    TempROCModel temp = new TempROCModel();
                    var GetROCData = db.DES069.Where(x => x.ROCId == RocId).FirstOrDefault();
                    List<DES071> _DES071ist = db.DES071.Where(x => x.ROCId == RocId).ToList();
                    var getAgencyName = db.DES027.Where(x => x.AgencyId == GetROCData.AgencyId).Select(x => x.AgencyName).FirstOrDefault();
                    
                    temp.Equipment_No = GetROCData.EquipmentNo;
                    temp.ROC_Ref_No = GetROCData.ROCRefNo;
                    temp.Project_No = GetROCData.Project;
                    temp.Doc_No = GetROCData.DocNo;
                    temp.Reviewing_Agency = getAgencyName;
                    temp.ROC_Version = GetROCData.Version;
                    temp.Doc_Title = GetROCData.DocTitle;
                    temp.Create_Date = GetROCData.CreatedOn == null ? "" : GetROCData.CreatedOn.Value.ToShortDateString();
                    temp.Submit_Date = GetROCData.SubmittedDate == null ? "" : GetROCData.SubmittedDate.Value.ToShortDateString(); ;                    
                    if (_DES071ist != null && _DES071ist.Count > 0)
                    {
                        var data = _DES071ist.Select(x => new TempROCCommentModel
                        {
                            Status = x.Status,
                            SR_No = x.SrNo,
                            Customer_Doc_No = x.ClientDocNo,
                            Customer_Doc_Rev = x.ClientDocRev,
                            Comment_Location = x.CommentLocation,
                            Reviewing_Agency_Comment_Clarifications = x.AgencyComments,
                            Date = x.AgencyCommentsDate == null ? "" : x.AgencyCommentsDate.Value.ToShortDateString(),
                            L_T_HEIC_Reply_Resolution = x.LandTComment,
                            L_T_Reply_Resolution_Date = x.LandTCommentDate == null ? "" : x.LandTCommentDate.Value.ToShortDateString(),
                            Reviewing_Agency_Response = x.AgencyResponse,
                            Reviewing_Agency_Response_Date = x.AgencyResponseDate == null ? "" : x.AgencyResponseDate.Value.ToShortDateString(),
                            Customer_Sr_No = x.CustomerSrNo,
                            Comments_from_Customer_Depts = x.CommfromCustDepts

                        }).ToList();
                        temp.ROCCommentList = data;

                        foreach (var item in data)
                        {
                            List<DES072> _DES072ist = db.DES072.Where(x => x.ROCCommentsId == item.ROCCommentsId).ToList();
                            if (_DES072ist != null && _DES072ist.Count > 0)
                            {
                                var commentData = _DES072ist.Select(x => new TempROCCommentedDocModel
                                {
                                    ROCCommentedDocId = x.ROCCommentsAttachId,
                                    DocumentPath = x.DocumentPath,
                                    DocumentTitle = x.DocumentTitle,
                                    DocumentFormat = x.DocumentFormat,
                                }).ToList();
                                temp.ROCCommentedDOCList = commentData;
                            }
                        }
                    }

                    HttpContext.Current.Response.Clear();
                    HttpContext.Current.Response.Buffer = true;
                    HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + temp.Project_No + "_" + temp.ROC_Ref_No + ".xls");
                    HttpContext.Current.Response.Charset = "";
                    HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";

                    //style to format numbers to string
                    string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                    HttpContext.Current.Response.Write(style);
                    HttpContext.Current.Response.Output.Write(ConvertListToHtmlTable(temp).ToString());
                    HttpContext.Current.Response.Flush();
                    HttpContext.Current.Response.End();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        private StringBuilder ConvertListToHtmlTable(TempROCModel list)
        {
            StringBuilder sb = new StringBuilder();
            PropertyInfo[] Props = typeof(TempROCModel).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            sb.Append("<Table border='1'><thead bgcolor='gray'><tr>");

            int ii = 1;
            foreach (PropertyInfo prop in Props)
            {
                if ((prop.Name != "ROCId") && (prop.Name != "ROCCommentList") && (prop.Name != "ROCCommentedDOCList"))
                {
                    if (ii == 1)
                    {
                        sb.Append("<tr>");
                    }
                    if (ii < 5)
                    {
                        sb.AppendLine("<td align='left'><b>");
                        sb.Append(prop.Name.Replace("_", " "));
                        sb.AppendLine("</b></td><td align='left'>");
                        sb.Append(prop.GetValue(list, null));
                        sb.AppendLine("</td>");
                    }
                    else
                    {
                        sb.Append("</tr>");
                        ii = 1;
                        continue;
                    }
                    ii++;
                }
            }
            sb.Append("<tr>");
            sb.Append("</tr>");

            if (list.ROCCommentList.Count > 0)
            {
                PropertyInfo[] PropsA = typeof(TempROCCommentModel).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                sb.Append("<Table border='1'><thead bgcolor='gray'><tr>");
                foreach (PropertyInfo prop in PropsA)
                {
                    if (prop.Name != "ROCCommentsId")
                    {
                        if (prop.Name == "SR_No")
                        {
                            sb.Append("<th align='left'>" + prop.Name.Replace("SR_No", "SR No.") + " </th>");
                        }
                        else if (prop.Name == "Reviewing_Agency_Comment_Clarifications")
                        {
                            sb.Append("<th align='left'>" + prop.Name.Replace("Reviewing_Agency_Comment_Clarifications", "Reviewing Agency Comment / Clarifications") + " </th>");
                        }
                        else if (prop.Name == "L_T_HEIC_Reply_Resolution")
                        {
                            sb.Append("<th align='left'>" + prop.Name.Replace("L_T_HEIC_Reply_Resolution", "L&T HEIC Reply / Resolution") + " </th>");
                        }
                        else if (prop.Name == "L_T_Reply_Resolution_Date")
                        {
                            sb.Append("<th align='left'>" + prop.Name.Replace("L_T_Reply_Resolution_Date", "Date") + " </th>");
                        }
                        else if (prop.Name == "Reviewing_Agency_Response_Date")
                        {
                            sb.Append("<th align='left'>" + prop.Name.Replace("Reviewing_Agency_Response_Date", "Date") + " </th>");
                        }
                        else if (prop.Name == "Customer_Sr_No")
                        {
                            sb.Append("<th align='left'>" + prop.Name.Replace("Customer_Sr_No", "Customer Sr.No.") + " </th>");
                        }
                        else if (prop.Name == "Comments_from_Customer_Depts")
                        {
                            sb.Append("<th align='left'>" + prop.Name.Replace("Comments_from_Customer_Depts", "Comments from - Customer Depts") + " </th>");
                        }
                        else
                        {
                            sb.Append("<th align='left'>" + prop.Name.Replace("_", " ") + "</th>");
                        }
                    }
                }
                sb.Append("</tr></thead><tbody>");
                sb.Append("<tr>");
                foreach (TempROCCommentModel item in list.ROCCommentList)
                {
                    for (int i = 0; i < PropsA.Length; i++)
                    {
                        if (PropsA[i].Name != "ROCCommentsId")
                        {
                            sb.Append("<td align='left' >" + PropsA[i].GetValue(item, null) + "</td>");
                        }
                    }
                    sb.Append("</tr>");
                }
            }

            sb.Append("</tbody></table>");
            return sb;
        }


        public bool UpdateROCStatusCheckin(Int64? ROCId, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var data = db.DES069.Where(x => x.ROCId == ROCId).FirstOrDefault();
                    if (data != null)
                    {
                        data.IsROCCheckIn = true;
                        data.ROCCheckInBy = CommonService.objClsLoginInfo.UserName;
                        data.ROCCheckInOn = DateTime.Now;
                        db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public bool UpdateIsCheckInROC(Int64? RocId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var getRocID = db.DES069.Where(x => x.ROCId == RocId).FirstOrDefault();
                    getRocID.IsROCCheckIn = false;
                    getRocID.ROCCheckInBy = null;
                    db.Entry(getRocID).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
        public List<string> GetJEPData(Int64? RocId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<string> getData = new List<string>();
                    var getRocID = db.DES069.Where(x => x.ROCId == RocId).FirstOrDefault();
                    var getJEPData = db.DES075.Where(x => x.DocNo == getRocID.DocNo && x.RevisionNo == getRocID.DocRev).FirstOrDefault();

                    getData.Add(getJEPData.CustomerDocNo);
                    getData.Add(getJEPData.CustomerDocRevision);
                    return getData;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
        

    }
}
