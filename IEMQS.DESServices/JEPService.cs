﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using System.Data;
using System.Globalization;
using System.Web;
using System.Text;
using System.Reflection;
using System.Web.UI.WebControls;
using OfficeOpenXml;

namespace IEMQS.DESServices
{
    public class JEPService : IJEPService
    {
        public JEPModel GetManitainJEPbyId(Int64 Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    JEPModel model = new JEPModel();
                    model = db.DES074.Where(x => x.JEPId == Id).Select(x => new JEPModel
                    {
                        JEPId = x.JEPId,
                        JEPNumber = x.JEPNumber,
                        Customer = x.Customer,
                        CustomerPONo = x.CustomerPONo,
                        Licensor = x.Licensor,
                        ContractNo = x.ContractNo,
                        Project = x.Project,
                        ProjectDescription = x.ProjectDescription,
                        Purchaser = x.Purchaser,
                        MRNo = x.MRNo,
                        ProjectManager = x.ProjectManager,
                        CreatedBy = x.CreatedBy,
                        CreatedOn = x.CreatedOn,
                        EditedBy = x.EditedBy,
                        EditedOn = x.EditedOn,
                        Status = x.Status,
                        Department = x.Department,
                        RoleGroup = x.RoleGroup,
                        FolderId = x.FolderId,
                        JEPDescription = x.JEPDescription,
                        IsJEPCheckIn = x.IsJEPCheckIn,
                    }).FirstOrDefault();
                    if (!string.IsNullOrEmpty(model.RoleGroup))
                    {
                        List<string> list = CommonService.objClsLoginInfo.ListRoles;
                        var list1 = db.SP_DES_GET_ROLE().Where(x => list.Contains(x.Role)).Select(x => x.RoleGroup).Distinct().ToList();
                        List<string> rolelist = model.RoleGroup.Split(',').Select(x => x.Trim()).ToList();
                        if (rolelist != null && rolelist.Count > 0 && list1 != null && list1.Count > 0)
                        {
                            model.Authorized = rolelist.Count(x => list1.Contains(x));
                        }
                    }
                    return model;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 AddEdit(JEPModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    string PsName;
                    var temp = db.DES074.Find(model.JEPId);


                    DES074 _DES074 = temp == null ? new DES074() : temp;

                    _DES074.Purchaser = model.Purchaser;
                    _DES074.MRNo = model.MRNo;
                    _DES074.JEPDescription = model.JEPDescription;

                    if (temp == null)
                    {
                        _DES074.IsJEPCheckIn = true;
                        _DES074.JEPCheckInBy = CommonService.objClsLoginInfo.UserName;
                        _DES074.JEPCheckInOn = DateTime.Now;

                        _DES074.CustomerPONo = model.CustomerPONo;
                        _DES074.Licensor = model.Licensor;
                        _DES074.ProjectManager = model.ProjectManager;
                        _DES074.JEPNumber = GenerateJEPNumber(model.Project);
                        _DES074.Project = model.Project;
                        _DES074.Customer = model.Customer;
                        _DES074.ContractNo = model.ContractNo;
                        _DES074.ProjectDescription = model.ProjectDescription;
                        _DES074.Status = ObjectStatus.Draft.ToString();
                        _DES074.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES074.CreatedOn = DateTime.Now;
                        _DES074.Department = model.Department;
                        _DES074.RoleGroup = model.RoleGroup;
                        _DES074.FolderId = model.FolderId;
                        _DES074.JEPDescription = model.JEPDescription;

                        #region getData From LifeCycle
                        var policy = GetPolicySteps(ObjectName.JEP.ToString(), model.BUId, null); // Get Polocy Steps
                        List<DES093> _DES093List = new List<DES093>();
                        if (policy != null)
                        {
                            foreach (var item in policy)
                            {
                                _DES074.PolicyID = item.PolicyId;

                                _DES093List.Add(new DES093
                                {
                                    PolicyID = item.PolicyId,
                                    PolicyStep = item.PolicyStep,
                                    PolicyOrderID = item.PolicyOrderID,
                                    IsCompleted = false,
                                    DesignationLevels = item.DesignationLevels,
                                    ColorCode = item.ColorCode
                                });
                            }
                        }
                        #endregion
                        _DES074.DES093 = _DES093List;
                        db.DES074.Add(_DES074);
                    }
                    else
                    {
                        _DES074.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES074.EditedOn = DateTime.Now;

                        if (temp.IsJEPCheckIn != true)
                        {
                            _DES074.IsJEPCheckIn = true;
                            _DES074.JEPCheckInBy = CommonService.objClsLoginInfo.UserName;
                            _DES074.JEPCheckInOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();
                    IsCheckInJEP(CommonService.objClsLoginInfo.UserName, _DES074.JEPId, out PsName);
                    return _DES074.JEPId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteMaintainJEP(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES074.Find(Id);
                    if (check != null)
                    {
                        db.DES074.Remove(check);
                        db.SaveChanges();
                        return check.JEPId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public JEPModel SetDefaultAddData(JEPModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES051 = db.DES051.Where(w => w.Project == model.Project).FirstOrDefault();
                    model.JEPNumber = GenerateJEPNumber(model.Project);
                    if (_DES051.Customer != null)
                    {
                        var Customer = db.SP_DES_GET_CUSTOMERNAMEBYCODE(_DES051.Customer).FirstOrDefault();
                        model.Customer = Customer ?? _DES051.Customer;
                    }
                    else
                    {
                        model.Customer = "";
                    }

                    model.ContractNo = _DES051.Contract;
                    model.ProjectDescription = _DES051.ProjectDescription;
                    model.CustomerPONo = _DES051.CustomerPONumber;
                    model.Licensor = _DES051.Licensor;
                    model.ProjectManager = new clsManager().GetUserNameFromPsNo(_DES051.ProjectPMG);
                    model.ProjectDescription = _DES051.ProjectDescription;

                    if (!string.IsNullOrEmpty(model.RoleGroup))
                    {
                        List<string> list = CommonService.objClsLoginInfo.ListRoles;
                        var list1 = db.SP_DES_GET_ROLE().Where(x => list.Contains(x.Role)).Select(x => x.RoleGroup).Distinct().ToList();
                        List<string> rolelist = model.RoleGroup.Split(',').Select(x => x.Trim()).ToList();
                        if (rolelist != null && rolelist.Count > 0 && list1 != null && list1.Count > 0)
                        {
                            model.Authorized = rolelist.Count(x => list1.Contains(x));
                        }
                    }
                }
                return model;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SelectListItem> GetContractProjectList(string Contract)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES051.Where(x => x.Contract == Contract && x.Status != ObjectStatus.Created.ToString()).Select(x => new SelectListItem
                    {
                        Text = x.Project,
                        Value = x.Project
                    }).ToList();

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }

        }

        public JEPModel GetJEPdetail(Int64? JEPId)
        {
            try
            {
                JEPModel model = new JEPModel();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    model = db.DES074.Where(w => w.JEPId == JEPId).Select(x => new JEPModel
                    {
                        JEPId = x.JEPId,
                        JEPNumber = x.JEPNumber,
                        Customer = x.Customer,
                        ContractNo = x.ContractNo,
                        ProjectDescription = x.ProjectDescription,
                        CustomerPONo = x.CustomerPONo,
                        Licensor = x.Licensor,
                        ProjectManager = x.ProjectManager,
                        Project = x.Project,
                        Purchaser = x.Purchaser,
                        MRNo = x.MRNo,
                        RoleGroup = x.RoleGroup,
                        JEPDescription = x.JEPDescription,
                        IsJEPCheckIn = x.IsJEPCheckIn ?? false,
                        JEPCheckInBy = x.JEPCheckInBy,
                        JEPCheckInOn = x.JEPCheckInOn,
                    }).FirstOrDefault();

                    if (!string.IsNullOrEmpty(model.JEPCheckInBy))
                    {
                        model.JEPCheckInByName = new clsManager().GetUserNameFromPsNo(model.JEPCheckInBy);
                    }
                    if (!string.IsNullOrEmpty(model.RoleGroup))
                    {
                        List<string> list = CommonService.objClsLoginInfo.ListRoles;
                        var list1 = db.SP_DES_GET_ROLE().Where(x => list.Contains(x.Role)).Select(x => x.RoleGroup).Distinct().ToList();
                        List<string> rolelist = model.RoleGroup.Split(',').Select(x => x.Trim()).ToList();
                        if (rolelist != null && rolelist.Count > 0 && list1 != null && list1.Count > 0)
                        {
                            model.Authorized = rolelist.Count(x => list1.Contains(x));
                        }
                    }

                    if (model.Authorized > 0)
                        if (model.IsJEPCheckIn != true)
                        {

                            var _DES074 = db.DES074.Where(x => x.JEPId == JEPId).FirstOrDefault();
                            _DES074.IsJEPCheckIn = true;
                            _DES074.JEPCheckInBy = CommonService.objClsLoginInfo.UserName;
                            _DES074.JEPCheckInOn = DateTime.Now;
                            db.Entry(_DES074).State = System.Data.Entity.EntityState.Modified;
                            db.SaveChanges();
                        }

                }
                return model;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public string GenerateJEPNumber(string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    int iCount = db.DES074.Count(w => w.Project == Project);
                    iCount = iCount + 1;
                    return Project + "-JEP-" + string.Format("{0:000}", iCount);
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<ProductModel> GetAllProductByBUId(Int64 BUId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES029.Where(w => w.BUId == BUId).OrderBy(x => x.ProductId).Select(x => new ProductModel
                    {
                        ProductId = x.ProductId,
                        BUId = x.BUId,
                        Product = x.Product + " - " + x.Description
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_JEPLISTPROJECTWISE_Result> GetJEPListByProject(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, Int64? FolderId, string Roles)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_JEPLISTPROJECTWISE(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, FolderId, Roles).ToList();
                    recordsTotal = 0; // data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.GString = CommonService.Encrypt("JEPId=" + x.JEPId);
                        x.QString = CommonService.Encrypt("Id=" + x.JEPId);
                        x.JEPCheckInByName = new clsManager().GetUserNameFromPsNo((x.JEPCheckInBy));
                    });
                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_DOCUMENTLIST_Result> GetDocumentList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64 JEPId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_DOCUMENTLIST(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, JEPId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 AddEditDocumentList(JEPDocumentModel model, Int64 BUId, string Project, out string Msg)
        {
            try
            {
                Int64 JEPDocumentDetailsId = 0;
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    Msg = "";

                    var dataDES074 = db.DES074.Where(x => x.JEPId == model.JEPId).FirstOrDefault();
                    var temp = db.DES075.Find(model.JEPDocumentDetailsId);
                    DES075 _DES075 = temp == null ? new DES075() : temp;

                    _DES075.JEPId = model.JEPId;
                    _DES075.DocDescription = model.DocDescription;
                    _DES075.RevisionNo = model.RevisionNo;
                    _DES075.CustomerDocNo = model.CustomerDocNo;
                    _DES075.CustomerDocRevision = model.CustomerDocRevision;
                    _DES075.DocumentTypeId = model.DocumentTypeId;
                    _DES075.Department = model.Department;
                    _DES075.PlannedDate = model.PlannedDate;
                    _DES075.Weightage = model.Weightage;
                    _DES075.MilestoneDocument = model.MilestoneDocument;
                    if (temp == null)
                    {
                        DES075 _obj075 = db.DES075.Where(w => w.DocNo == model.DocNo).FirstOrDefault();
                        if (_obj075 != null)
                        {
                            Msg = "Document already exits.";
                            return 0;
                        }
                        DES059 _obj059 = db.DES059.Where(w => w.DocumentNo == model.DocNo).FirstOrDefault();
                        if (_obj059 != null)
                        {
                            Msg = "Document already exits.";
                            return 0;
                        }

                        _DES075.DocNo = model.DocNo;
                        _DES075.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES075.CreatedOn = DateTime.Now;
                        db.DES075.Add(_DES075);
                    }
                    else
                    {
                        _DES075.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES075.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    JEPDocumentDetailsId = _DES075.JEPDocumentDetailsId;

                    if (dataDES074.Status == ObjectStatus.Completed.ToString())
                    {
                        #region Notification 
                        try
                        {
                            db.SP_DES_JEP_PUSH_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, NotificationObject.AddDocument.ToString(), model.JEPId, model.Department, null);
                        }
                        catch
                        {
                        }
                        #endregion
                    }
                }
                return JEPDocumentDetailsId;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<JEPProjectwiseDocument> GetJEPTemplatebyProject(string ProjectNo)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES030.Where(w => w.Project == ProjectNo).Select(x => new JEPProjectwiseDocument
                    {
                        JEPTemplateId = x.JEPTemplateId,
                        TemplateName = x.TemplateName
                    }).ToList();

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public string GenerateDocNo(string Project, string DocumentNo, string DrgSeriesNo)
        {
            try
            {
                string docno = "";
                switch (DocumentNo.Length)
                {
                    case 1:
                        docno = "000" + DocumentNo;
                        break;
                    case 2:
                        docno = "00" + DocumentNo;
                        break;
                    case 3:
                        docno = "0" + DocumentNo;
                        break;
                    default:
                        docno = DocumentNo;
                        break;
                }
                //Console.WriteLine(DocumentNo);
                return DrgSeriesNo + "-" + docno;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool AddDocumentListByProductANDTemplate(string Project, Int64 Product, Int64 JEPTemplateId, Int64 JEPId, string project, Int64? BUId, out string Msg)
        {
            Msg = "";
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    List<DES031> objDES031List = db.DES031.Where(w => w.JEPTemplateId == JEPTemplateId).ToList();
                    foreach (var objDES031 in objDES031List)
                    {
                        if (objDES031 != null)
                        {
                            string BU = db.DES001.Where(x => x.BUId == BUId).Select(x => x.BU).FirstOrDefault();

                            string strDrgNo = db.DES051.Find(project)?.DrawingSeriesNo;
                            string strDocNo = strDrgNo + objDES031.DocumentNo;//GenerateDocNo(Project, objDES031.DocumentNo, strDrgNo);
                            DES075 _obj075 = db.DES075.Where(w => w.DocNo == strDocNo).FirstOrDefault();
                            DES059 _obj059 = db.DES059.Where(w => w.DocumentNo == strDocNo).FirstOrDefault();
                            if (_obj075 == null && _obj059 == null)
                            {
                                DES075 _DES075 = new DES075();
                                _DES075.JEPId = JEPId;
                                _DES075.DocNo = strDocNo;
                                _DES075.DocDescription = objDES031.DocumentTitle;
                                _DES075.RevisionNo = 0;
                                _DES075.CustomerDocNo = "";//
                                _DES075.DocumentTypeId = null;//
                                _DES075.Department = null;//
                                _DES075.PlannedDate = DateTime.Now;
                                _DES075.Weightage = "0";

                                _DES075.CreatedBy = CommonService.objClsLoginInfo.UserName;
                                _DES075.CreatedOn = DateTime.Now;
                                db.DES075.Add(_DES075);
                            }
                            else
                            {
                                Msg += strDocNo + ",";
                            }
                        }

                        db.SaveChanges();
                    }

                    if (!string.IsNullOrEmpty(Msg))
                    {
                        Msg += "Document already exits and other documents added successfully.";
                    }
                    else
                    {
                        Msg = "Documents added successfully.";
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
                throw;
            }
        }

        public Int64? DeleteDocument(Int64? Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES075.Find(Id);
                    if (check != null)
                    {
                        if (check.DES085 != null && check.DES085.Count > 0)
                        {
                            errorMsg = "Document is already in used. you can not delete";
                            return 0;
                        }
                        else if (check.DES082 != null && check.DES082.Count > 0)
                        {
                            errorMsg = "Document is already in used. you can not delete";
                            return 0;
                        }
                        else if (check.DES077 != null && check.DES077.Count > 0)
                        {
                            errorMsg = "Document is already in used. you can not delete";
                            return 0;
                        }
                        else if (check.DES090 != null && check.DES090.Count > 0)
                        {
                            errorMsg = "Document is already in used. you can not delete";
                            return 0;
                        }
                        else
                        {
                            db.DES075.Remove(check);
                        }
                        db.SaveChanges();
                        AddCustomerFeedbackHistory(null, Id, null, null, Id + " JEP Document has Deleted", check.CreatedBy);
                        return check.JEPDocumentDetailsId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool AddDocumentListByProjectwise(Int64 JEPDocumentDetailsId, Int64 JEPId, string project, Int64? BUId, out string Msg)
        {
            Msg = "";
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<DES031> objDES031List = db.DES031.Where(w => w.JEPTemplateId == JEPDocumentDetailsId).ToList();
                    if (objDES031List != null && objDES031List.Count > 0)
                    {
                        foreach (var objDES031 in objDES031List)
                        {
                            DES030 _objDES030 = db.DES030.Where(w => w.JEPTemplateId == objDES031.JEPTemplateId).FirstOrDefault();
                            if (_objDES030 != null)
                            {
                                string BU = db.DES001.Where(x => x.BUId == BUId).Select(x => x.BU).FirstOrDefault();
                                string strDrgNo = db.DES051.Find(project)?.DrawingSeriesNo;
                                string strDocNo = strDrgNo + objDES031.DocumentNo;// GenerateDocNo(project, objDES031.DocumentNo, strDrgNo);
                                DES075 _obj075 = db.DES075.Where(w => w.DocNo == strDocNo).FirstOrDefault();
                                DES059 _obj059 = db.DES059.Where(w => w.DocumentNo == strDocNo).FirstOrDefault();
                                if (_obj075 == null && _obj059 == null)
                                {
                                    DES075 _DES075 = new DES075();
                                    _DES075.JEPId = JEPId;
                                    _DES075.DocNo = strDocNo;
                                    _DES075.DocDescription = objDES031.DocumentTitle;
                                    _DES075.RevisionNo = 0;
                                    _DES075.CustomerDocNo = "";//
                                    _DES075.DocumentTypeId = null;//
                                    _DES075.Department = null;//
                                    _DES075.PlannedDate = DateTime.Now;
                                    _DES075.Weightage = "0";

                                    _DES075.CreatedBy = CommonService.objClsLoginInfo.UserName;
                                    _DES075.CreatedOn = DateTime.Now;
                                    db.DES075.Add(_DES075);

                                }
                                else
                                {
                                    Msg += strDocNo + ",";
                                    // return false;
                                }
                            }
                        }
                    }

                    db.SaveChanges();
                    Msg += "Document already exits and other documents added successfully.";
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
                throw;
            }
        }

        public List<SP_DES_GET_ROLE_Result> GetAllRole()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.SP_DES_GET_ROLE().ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        /// <summary>
        /// FINAL SUBMIT CALL | UPDATE STATUS DRAFT TO CREATE
        /// </summary>
        /// <param name="JEPId"></param>
        /// <returns></returns>
        /// 

        public bool AddEditJEPStatus(Int64 JEPId, string Project, out string errmsg)
        {
            errmsg = string.Empty;
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var status = calculateWeightage(JEPId, out errmsg);
                    if (status)
                    {
                        var temp = db.DES074.Find(JEPId);
                        DES074 _DES074 = temp == null ? new DES074() : temp;
                        _DES074.Status = ObjectStatus.Created.ToString();
                        _DES074.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES074.EditedOn = DateTime.Now;
                        _DES074.IsJEPCheckIn = false;
                        _DES074.JEPCheckInBy = CommonService.objClsLoginInfo.UserName;
                        _DES074.JEPCheckInOn = DateTime.Now;

                        var dataOfLifeCycle = db.DES093.Where(yy => yy.JEPId == JEPId).OrderBy(x => x.PolicyOrderID).FirstOrDefault();
                        if (dataOfLifeCycle != null)
                        {
                            dataOfLifeCycle.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            dataOfLifeCycle.CreatedOn = DateTime.Now;
                            dataOfLifeCycle.IsCompleted = true;
                        }
                        db.SaveChanges();

                        #region Notification
                        try
                        {
                            if (temp != null)
                                db.SP_DES_JEP_PUSH_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, NotificationObject.Create.ToString(), temp.JEPId, null, null);
                        }
                        catch
                        {
                        }
                        #endregion

                        return true;
                    }
                    return status;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
                throw;
            }
        }

        public bool calculateWeightage(Int64 JEPId, out string errmsg)
        {
            errmsg = string.Empty;
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    var _DES075List = db.DES075.Where(x => x.JEPId == JEPId).ToList();
                    //double totalWeight = 0;
                    //foreach (var item in _DES075List)
                    //{
                    //    double i;
                    //    if (double.TryParse(item.Weightage, out i))
                    //        totalWeight += i;
                    //}

                    //if (totalWeight != 100)
                    //{
                    //    errmsg += "<br /> Total Weightage should be 100%.";
                    //}
                    if (_DES075List.Count == 0)
                    {
                        errmsg += "Please add atleast one document";
                    }
                    if (_DES075List.Count(x => x.Department == null) > 0)
                    {
                        errmsg += "<br /> Please maintain department for all documents.";
                    }
                    if (_DES075List.Count(x => x.DocumentTypeId == null) > 0)
                    {
                        errmsg += "<br /> Document Type is required";
                    }
                    if (_DES075List.Count(x => x.PlannedDate == null) > 0)
                    {
                        errmsg += "<br /> Planned Date is required";
                    }

                    if (!string.IsNullOrEmpty(errmsg))
                    {
                        return false;
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
                throw;
            }
        }
        public List<SP_DES_GET_GLOBAL_JEP_DOC_BY_PROJECT_Result> GetAllGlobalDocumentListByProject(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project, string DocumentNo, string CurrentProject)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_GLOBAL_JEP_DOC_BY_PROJECT(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, DocumentNo, CurrentProject).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_PREVIEW_JEP_DOC_BY_PRODUCT_Result> BindpreviewProductJEPDoc(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, Int64 JEPTemplateId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_PREVIEW_JEP_DOC_BY_PRODUCT(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, JEPTemplateId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool AddLinkDocument(Int64 DocumentId, Int64 JEPId, out string Msg)
        {
            Msg = "";
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DES059 _obj059 = db.DES059.Where(w => w.DocumentId == DocumentId).FirstOrDefault();
                    if (_obj059 != null)
                    {
                        DES075 _obj = db.DES075.Where(w => w.DocNo == _obj059.DocumentNo).FirstOrDefault();
                        if (_obj == null)
                        {
                            DES075 _DES075 = new DES075();
                            _DES075.JEPId = JEPId;
                            _DES075.DocNo = _obj059.DocumentNo;
                            _DES075.DocDescription = _obj059.DocumentTitle;
                            _DES075.RevisionNo = _obj059.DocumentRevision;
                            _DES075.CustomerDocNo = _obj059.ClientDocumentNumber;//
                            _DES075.CustomerDocRevision = _obj059.ClientVersion;//

                            _DES075.Department = null;
                            _DES075.DocumentTypeId = _obj059.DocumentTypeId;//
                            _DES075.DocumentId = _obj059.DocumentId;//
                            _DES075.PlannedDate = DateTime.Now;
                            _DES075.Weightage = "0";

                            _DES075.CreatedBy = CommonService.objClsLoginInfo.UserName;
                            _DES075.CreatedOn = DateTime.Now;
                            db.DES075.Add(_DES075);
                            db.SaveChanges();
                            return true;
                        }
                        else
                        {
                            Msg = "Document already exits.";
                            return false;
                        }
                    }
                    else
                    {
                        Msg = "Document not exits.";
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                Msg = "";
                return false;
                throw;
            }
        }

        public bool AddJEPLinkDocuments(JEPGlobalDoc Model, string Project, out string Msg, out string SuccessMsg)
        {
            Msg = "";
            SuccessMsg = "";
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    foreach (var item in Model.JEPDocumentList)
                    {
                        DES059 _obj059 = db.DES059.Where(w => w.DocumentId == item.JEPDocListId).FirstOrDefault();
                        if (_obj059 != null)
                        {
                            DES075 _obj = db.DES075.Where(w => w.DocNo == _obj059.DocumentNo && w.DES074.Project == Project).FirstOrDefault();
                            if (_obj == null)
                            {
                                DES075 _DES075 = new DES075();
                                _DES075.JEPId = Model.JEPId;
                                _DES075.DocNo = _obj059.DocumentNo;
                                _DES075.DocDescription = _obj059.DocumentTitle;
                                _DES075.RevisionNo = _obj059.DocumentRevision;
                                _DES075.CustomerDocNo = _obj059.ClientDocumentNumber;//
                                _DES075.CustomerDocRevision = _obj059.ClientVersion;//

                                _DES075.Department = _obj059.Department;
                                _DES075.DocumentTypeId = _obj059.DocumentTypeId;//
                                _DES075.DocumentId = _obj059.DocumentId;//
                                _DES075.PlannedDate = DateTime.Now;
                                _DES075.Weightage = "0";
                                _DES075.MilestoneDocument = _obj059.MilestoneDocument ?? false;//
                                _DES075.CreatedBy = CommonService.objClsLoginInfo.UserName;
                                _DES075.CreatedOn = DateTime.Now;
                                db.DES075.Add(_DES075);
                                SuccessMsg += _obj059.DocumentNo + ",";

                                _obj059.IsJEP = true;
                            }
                            else
                            {
                                Msg += _obj059.DocumentNo + ",";
                            }
                        }
                        db.SaveChanges();
                    }
                    if (!string.IsNullOrEmpty(Msg))
                    {
                        string str1 = Msg.Substring(Msg.Length - 1);
                        if (str1 == ",")
                        {
                            Msg = Msg.Remove(Msg.Length - 1);
                        }
                        Msg += " Document already exits.";
                    }

                    if (!string.IsNullOrEmpty(SuccessMsg))
                    {
                        SuccessMsg += " Documents added successfully.";
                    }

                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                Msg = "";
                return false;
                throw;
            }
        }

        public List<JEPPolicyStepsModel> GetPolicySteps(string Type, Int64? BUId, Int64? JEPId)
        {
            try
            {
                List<JEPPolicyStepsModel> listObjectPolicyStepsModel = new List<JEPPolicyStepsModel>();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (JEPId > 0)
                    {
                        var PoliicyData = db.DES093.Where(x => x.JEPId == JEPId).ToList();

                        foreach (var x in PoliicyData)
                        {
                            JEPPolicyStepsModel objectPolicyStepsModel = new JEPPolicyStepsModel();
                            objectPolicyStepsModel.JEPLifeCycleId = x.JEPLifeCycleId;
                            objectPolicyStepsModel.PolicyId = x.PolicyID;
                            objectPolicyStepsModel.PolicyStep = x.PolicyStep;
                            objectPolicyStepsModel.PolicyOrderID = x.PolicyOrderID;
                            objectPolicyStepsModel.IsCompleted = x.IsCompleted ?? false;
                            objectPolicyStepsModel.CreatedBy = new clsManager().GetUserNameFromPsNo((x.CreatedBy));
                            objectPolicyStepsModel.CreatedOn = x.CreatedOn;
                            objectPolicyStepsModel.ColorCode = x.ColorCode;
                            objectPolicyStepsModel.DesignationLevels = x.DesignationLevels;
                            listObjectPolicyStepsModel.Add(objectPolicyStepsModel);
                        }
                    }
                    else
                    {
                        listObjectPolicyStepsModel = db.SP_DES_GET_OBJECT_POLICY_BY_OBJECTNAME(Type, BUId).Select(x => new JEPPolicyStepsModel
                        {
                            PolicyId = x.PolicyId,
                            PolicyStep = x.StepName,
                            PolicyOrderID = x.OrderNo,
                            ColorCode = x.ColorCode,
                            DesignationLevels = x.DesignationLevels
                        }).ToList();
                    }
                }
                return listObjectPolicyStepsModel;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_JEP_POLICY_BYLEVEL_Result> GetJEPUserLevel(string RoleGroup, string Roles, Int64? JEPId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_JEP_POLICY_BYLEVEL(RoleGroup, Roles, JEPId).ToList();
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return null;
            }
        }

        public Int64? ReviseJEPPolicy(string ReviseJEPPolicy, Int64 JEPId, string Project, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var data = db.DES093.Where(yy => yy.JEPId == JEPId).ToList();
                    var JEPdata = db.DES074.Where(yy => yy.JEPId == JEPId).FirstOrDefault();
                    if (JEPdata.Status != ObjectStatus.Completed.ToString())
                    {
                        if (data != null)
                        {
                            foreach (var item in data)
                            {
                                item.IsCompleted = false;
                                item.CreatedBy = "";
                                item.CreatedOn = null;
                                item.EditedBy = CommonService.objClsLoginInfo.UserName;
                                item.EditedOn = DateTime.Now;
                                db.Entry(item).State = System.Data.Entity.EntityState.Modified;
                            }
                        }

                        if (!string.IsNullOrEmpty(ReviseJEPPolicy))
                        {
                            if (JEPdata != null)
                            {
                                JEPdata.ReturnNotes = ReviseJEPPolicy;
                                db.Entry(JEPdata).State = System.Data.Entity.EntityState.Modified;
                            }
                        }
                        var JEPData = db.DES074.Where(yy => yy.JEPId == JEPId).FirstOrDefault();
                        if (JEPData != null)
                        {
                            JEPData.IsJEPCheckIn = false;
                            JEPdata.JEPCheckInBy = null;
                            JEPData.Status = ObjectStatus.Draft.ToString();
                            db.Entry(JEPData).State = System.Data.Entity.EntityState.Modified;
                        }

                        #region Notification
                        try
                        {
                            if (JEPData != null)
                                db.SP_DES_JEP_PUSH_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, NotificationObject.Return.ToString(), JEPdata.JEPId, null, null);
                        }
                        catch
                        {
                        }
                        #endregion

                        db.SaveChanges();
                    }
                    else
                    {
                        var getLastPolicy = data.Last();
                        var PsName = new clsManager().GetUserNameFromPsNo(getLastPolicy.CreatedBy);
                        errorMsg = "Document is already " + getLastPolicy.PolicyStep + " by " + PsName;
                    }
                    return JEPId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64? UpdateJEPPolicy(Int64 JEPLifeCycleId, string Project, out string errorMsg, out string infoMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    infoMsg = "";
                    var data = db.DES093.Where(yy => yy.JEPLifeCycleId == JEPLifeCycleId).FirstOrDefault();

                    if (data != null)
                    {
                        var _DES074Data = db.DES074.Where(yy => yy.JEPId == data.JEPId).FirstOrDefault();
                        var datalist = db.DES093.Where(yy => yy.JEPId == data.JEPId).ToList().Count;
                        var list = db.DES093.Where(x => x.JEPId == data.JEPId && x.IsCompleted != true).OrderBy(x => x.PolicyOrderID).FirstOrDefault();
                        if (data.IsCompleted != true)
                        {
                            if (list.JEPLifeCycleId == data.JEPLifeCycleId)
                            {
                                if (list.PolicyOrderID > 1 && datalist != list.PolicyOrderID)
                                {
                                    var JEPData = db.DES074.Where(x => x.JEPId == data.JEPId).FirstOrDefault();
                                    JEPData.Status = ObjectStatus.InProcess.ToString();
                                    JEPData.EditedBy = CommonService.objClsLoginInfo.UserName;
                                    JEPData.EditedOn = DateTime.Now;
                                    db.Entry(JEPData).State = System.Data.Entity.EntityState.Modified;
                                }
                                if (datalist == list.PolicyOrderID)
                                {
                                    var JEPData = db.DES074.Where(x => x.JEPId == data.JEPId).FirstOrDefault();
                                    JEPData.Status = ObjectStatus.Completed.ToString();
                                    JEPData.EditedBy = CommonService.objClsLoginInfo.UserName;
                                    JEPData.EditedOn = DateTime.Now;
                                    db.Entry(JEPData).State = System.Data.Entity.EntityState.Modified;

                                    var getDepartment = db.DES075.Where(x => x.JEPId == data.JEPId).Select(x => x.Department).Distinct().ToList();
                                    if (getDepartment != null)
                                    {
                                        foreach (var item in getDepartment)
                                        {
                                            #region Notification 
                                            try
                                            {
                                                if (data != null)
                                                    db.SP_DES_JEP_PUSH_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, NotificationObject.LastStep.ToString(), data.JEPId, item, null);
                                            }
                                            catch
                                            {
                                            }
                                        }
                                        #endregion
                                    }
                                }

                                data.CreatedBy = CommonService.objClsLoginInfo.UserName;
                                data.CreatedOn = DateTime.Now;
                                data.IsCompleted = true;
                                db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                                db.SaveChanges();
                                infoMsg = "JEP " + data.PolicyStep + " Successfully";
                                if (data.PolicyStep.Contains(ObjectStatus.Approve.ToString()))
                                {
                                    var D75 = db.DES075.Where(x => x.JEPId == data.JEPId).ToList();
                                    D75.ForEach(a => a.IsApproved = true);
                                    db.SaveChanges();
                                }

                                if (_DES074Data != null)
                                {
                                    _DES074Data.IsJEPCheckIn = false;
                                    _DES074Data.JEPCheckInBy = null;
                                    db.Entry(_DES074Data).State = System.Data.Entity.EntityState.Modified;
                                    db.SaveChanges();
                                }

                            }
                            else
                            {
                                errorMsg = "You Must Complete Before Steps !";
                            }
                        }
                        else
                        {
                            var PsName = new clsManager().GetUserNameFromPsNo(data.CreatedBy);
                            infoMsg = "JEP has been already" + data.PolicyStep + " by " + PsName;
                        }
                    }


                    #region Notification 
                    try
                    {
                        if (data != null)
                            db.SP_DES_JEP_PUSH_NOTIFICATION(Project, CommonService.objClsLoginInfo.UserName, NotificationObject.LifeCycle.ToString(), data.JEPId, null, null);
                    }
                    catch
                    {
                    }

                    #endregion


                    return data.JEPId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        #region Customer feedback

        public List<CustomerFeedback> GetDocumentDetailsWithRevision(Int64? AgencyId, Int64? JEPId, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    db.SP_DES_ADD_DATA_CUSTOMERFEEDBACK_BY_AGENCY(AgencyId, CommonService.objClsLoginInfo.UserName);
                    return (from d85 in db.DES085
                            join d75 in db.DES075 on d85.JEPDocumentDetailsId equals d75.JEPDocumentDetailsId
                            join d74 in db.DES074 on d75.JEPId equals d74.JEPId
                            join d76 in db.DES076 on new { d85.AgencyId, d74.Project } equals new { d76.AgencyId, d76.Project }
                            join d77 in db.DES077 on new { d76.AgencyDocId, d75.JEPDocumentDetailsId } equals new { d77.AgencyDocId, d77.JEPDocumentDetailsId }
                            //join d80 in db.DES080 on d77.AgencyDocListId equals d80.AgencyDocListId
                            where
                            //d80.Status == "Created" && 
                            d75.JEPId == JEPId &&
                            d74.Project == Project &&
                            //d75.JEPDocumentDetailsId > 0 && 
                            d85.AgencyId == AgencyId
                            select new CustomerFeedback
                            {
                                JEPDocumentDetailsId = d75.JEPDocumentDetailsId,
                                DocNo = d75.DocNo + " (R" + d75.RevisionNo + ")",
                                AgencyId = d85.AgencyId,
                            }).Distinct().ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public long AddEditCustomerFeedback(CustomerFeedback model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var temp = db.DES085.Find(model.JEPCustomerFeedbackId);

                    DES085 _DES085 = temp == null ? new DES085() : temp;
                    var getDES075Data = db.DES075.Where(x => x.JEPDocumentDetailsId == model.JEPDocumentDetailsId).FirstOrDefault();
                    _DES085.Remark = model.Remark;

                    if (temp == null)
                    {
                        _DES085.JEPDocumentDetailsId = model.JEPDocumentDetailsId;
                        _DES085.AgencyId = model.AgencyId;
                        _DES085.ReceiptDate = model.ReceiptDate;
                        _DES085.ApprovalCode = model.ApprovalId;
                        _DES085.RefTransmittalNo = model.RefTransmittalNo;
                        _DES085.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES085.CreatedOn = DateTime.Now;
                        _DES085.ReturnTransmittalNo = model.ReturnTransmittalNo;
                        db.DES085.Add(_DES085);
                    }
                    else
                    {
                        _DES085.JEPDocumentDetailsId = temp.JEPDocumentDetailsId;
                        _DES085.AgencyId = temp.AgencyId;
                        _DES085.ApprovalCode = model.ApprovalId;
                        _DES085.RefTransmittalNo = temp.RefTransmittalNo;
                        _DES085.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES085.EditedOn = DateTime.Now;

                        _DES085.ReceiptDate = model.ReceiptDate;
                        _DES085.ReturnTransmittalNo = model.ReturnTransmittalNo;
                        _DES085.CustomerDocNo = model.CustomerDocNo;
                        AddCustomerFeedbackHistory(model.JEPId, model.JEPDocumentDetailsId, model.JEPCustomerFeedbackId, null, model.JEPCustomerFeedbackId + " JEP Customer Feedback has been Edited", model.CreatedBy);
                    }

                    if (model.GroupId != null)
                    {
                        //DES086 _DES086 = new DES086();
                        var jepCustomerFeedbackDocData = db.DES086.Where(x => x.GroupId == model.GroupId).ToList();

                        foreach (var item in jepCustomerFeedbackDocData)
                        {
                            item.JEPCustomerFeedbackId = _DES085.JEPCustomerFeedbackId;
                        }
                    }
                    db.SaveChanges();

                    if (model.JEPCustomerFeedbackId == null)
                        AddCustomerFeedbackHistory(model.JEPId, model.JEPDocumentDetailsId, _DES085.JEPCustomerFeedbackId, null, _DES085.JEPCustomerFeedbackId + " JEP Customer Feedback has been Added", _DES085.CreatedBy);



                    #region Notification
                    try
                    {
                        if (temp != null)
                            db.SP_DES_JEP_PUSH_NOTIFICATION(model.Project, CommonService.objClsLoginInfo.UserName, NotificationObject.CustomerFeedBack.ToString(), model.JEPDocumentDetailsId, getDES075Data.Department, null);
                    }
                    catch
                    {
                    }

                    #endregion

                    return _DES085.JEPCustomerFeedbackId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public CustomerFeedback JEPGetCustomerDetail(Int64? Id, Int64? CFId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (CFId > 0)
                    {
                        var data = (from a in db.DES074
                                    from b in db.DES085
                                    where a.JEPId == Id && b.JEPCustomerFeedbackId == CFId
                                    select new CustomerFeedback
                                    {
                                        JEPId = a.JEPId,
                                        JEPNumber = a.JEPNumber,
                                        Customer = a.Customer,
                                        CustomerPONo = a.CustomerPONo,
                                        Licensor = a.Licensor,
                                        ContractNo = a.ContractNo,
                                        Project = a.Project,
                                        ProjectDescription = a.ProjectDescription,
                                        Purchaser = a.Purchaser,
                                        MRNo = a.MRNo,
                                        ProjectManager = a.ProjectManager,
                                        ReceiptDate = b.ReceiptDate,
                                        RefTransmittalDate = b.RefTransmittalDate,
                                        AgencyId = b.AgencyId,
                                        JEPDocumentDetailsId = b.JEPDocumentDetailsId,
                                        ApprovalId = b.ApprovalCode,
                                        Remark = b.Remark,
                                        JEPCustomerFeedbackId = b.JEPCustomerFeedbackId,
                                        ReturnTransmittalNo = b.ReturnTransmittalNo,
                                        CustomerDocNo = b.CustomerDocNo,
                                    }).FirstOrDefault();

                        if (!string.IsNullOrEmpty(data.RoleGroup))
                        {
                            List<string> list = CommonService.objClsLoginInfo.ListRoles;
                            var list1 = db.SP_DES_GET_ROLE().Where(x => list.Contains(x.Role)).Select(x => x.RoleGroup).Distinct().ToList();
                            List<string> rolelist = data.RoleGroup.Split(',').Select(x => x.Trim()).ToList();
                            if (rolelist != null && rolelist.Count > 0 && list1 != null && list1.Count > 0)
                            {
                                data.Authorized = rolelist.Count(x => list1.Contains(x));
                            }
                        }

                        if (data != null)
                            return data;
                    }
                    else
                    {
                        var model = db.DES074.Where(x => x.JEPId == Id).FirstOrDefault();
                        CustomerFeedback data = new CustomerFeedback();
                        if (model != null)
                        {
                            data.JEPId = model.JEPId;
                            data.JEPNumber = model.JEPNumber;
                            data.Customer = model.Customer;
                            data.CustomerPONo = model.CustomerPONo;
                            data.Licensor = model.Licensor;
                            data.ContractNo = model.ContractNo;
                            data.Project = model.Project;
                            data.ProjectDescription = model.ProjectDescription;
                            data.Purchaser = model.Purchaser;
                            data.MRNo = model.MRNo;
                            data.ProjectManager = model.ProjectManager;
                            data.CreatedBy = model.CreatedBy;
                            data.CreatedOn = model.CreatedOn;
                            data.EditedBy = model.EditedBy;
                            data.EditedOn = model.EditedOn;
                            data.Department = model.Department;
                            data.FolderId = model.FolderId;
                            data.RoleGroup = model.RoleGroup;
                            if (!string.IsNullOrEmpty(model.RoleGroup))
                            {
                                List<string> list = CommonService.objClsLoginInfo.ListRoles;
                                var list1 = db.SP_DES_GET_ROLE().Where(x => list.Contains(x.Role)).Select(x => x.RoleGroup).Distinct().ToList();
                                List<string> rolelist = model.RoleGroup.Split(',').Select(x => x.Trim()).ToList();
                                if (rolelist != null && rolelist.Count > 0 && list1 != null && list1.Count > 0)
                                {
                                    data.Authorized = rolelist.Count(x => list1.Contains(x));
                                }
                            }
                        }
                        return data;
                    }
                    return null;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public long UploadCustomerFeedbackFiles(DocumentMapping model, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";

                    DES086 _DES086 = new DES086();

                    _DES086.JEPCustomerFeedbackId = model.JEPCustomerFeedbackId;
                    _DES086.GroupId = model.GroupId;
                    _DES086.DocumentTitle = model.DocumentTitle;
                    _DES086.DocumentFormat = model.DocumentFormat;
                    _DES086.DocumentPath = model.DocumentPath;
                    _DES086.FileLocation = model.FileLocation;
                    _DES086.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _DES086.CreatedOn = DateTime.Now;
                    db.DES086.Add(_DES086);
                    db.SaveChanges();
                    AddCustomerFeedbackHistory(model.JEPQueryStringValue, null, null, _DES086.JEPCustFeedbackDocumentId, _DES086.JEPCustFeedbackDocumentId + " JEP Customer Feedback Document has been Uploaded", _DES086.CreatedBy);
                    return _DES086.JEPCustFeedbackDocumentId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<Agencies> GetAllAgency()
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES027.OrderBy(x => x.AgencyId).Select(x => new Agencies
                    {
                        AgencyId = x.AgencyId,
                        AgencyName = x.AgencyName,
                        AgencyDescription = x.AgencyDescription,
                        BUId = x.BUId
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_CUSTOMER_FEEDBACK_DOCUMENT_Result> GetCustomerFeedbackDocumentList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string GroupId, Int64? JEPcustomerFeedbackId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_CUSTOMER_FEEDBACK_DOCUMENT(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, GroupId, JEPcustomerFeedbackId).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public CustomerFeedback GetCustomerFeedbackByDOCId_AGSId(Int64? JEPDocumentDetailsId, Int64? AgencyId, string Project)
        {
            try
            {
                CustomerFeedback _model = new CustomerFeedback();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var _DES085 = db.DES085.Where(x => x.AgencyId == AgencyId && x.JEPDocumentDetailsId == JEPDocumentDetailsId).FirstOrDefault();
                    if (_DES085 != null)
                    {
                        _model.AgencyId = _DES085.AgencyId;
                        _model.ReceiptDate = _DES085.ReceiptDate;
                        _model.ReceiptDateStr = _DES085.ReceiptDate != null ? _DES085.ReceiptDate.Value.ToString("dd/MM/yyyy") : "";
                        _model.RefTransmittalDate = _DES085.RefTransmittalDate;
                        _model.RefTransmittalNo = _DES085.RefTransmittalNo;
                        _model.RefTransmittalDateStr = _DES085.RefTransmittalDate != null ? _DES085.RefTransmittalDate.Value.ToString("dd/MM/yyyy") : "";
                        _model.ApprovalId = _DES085.ApprovalCode;
                        _model.Remark = _DES085.Remark;
                        _model.JEPCustomerFeedbackId = _DES085.JEPCustomerFeedbackId;
                        _model.ReturnTransmittalNo = _DES085.ReturnTransmittalNo;
                        _model.CustomerDocNo = _DES085.CustomerDocNo;
                        _model.RevisionNo = Convert.ToInt32(_DES085.DES075.RevisionNo);
                        _model.DocNo = _DES085.DES075.DocNo;
                        _model.DocumentTitle = _DES085.DES075.DocDescription;
                        var strActualSubDate = _DES085.DES027.DES079.Where(bb => bb.Project == Project && bb.TransmittalNo == _DES085.RefTransmittalNo).Select(bb => bb.CreatedOn).FirstOrDefault();
                        _model.ActualSubDate = strActualSubDate != null ? strActualSubDate.Value.ToString("dd/MM/yyyy") : "";
                        var strPlannedSubDate = _DES085.DES075.DES077.Select(bb => bb.PlannedSubmitDate).FirstOrDefault();
                        _model.PlannedSubDate = strPlannedSubDate != null ? strPlannedSubDate.Value.ToString("dd/MM/yyyy") : "";
                    }
                    //_model.PlannedSubDate = strPlannedSubDate != null ? strPlannedSubDate.ToString("dd/MM/yyyy") : "";

                    //return db.DES085.Where(x => x.AgencyId == AgencyId && x.JEPDocumentDetailsId == JEPDocumentDetailsId).Select(x => new CustomerFeedback
                    //{
                    //    AgencyId = x.AgencyId,
                    //    ReceiptDate = x.ReceiptDate,
                    //    ReceiptDateStr = x.ReceiptDate != null ? x.ReceiptDate.Value.ToString("dd/MM/yyyy") : "",
                    //    RefTransmittalDate = x.RefTransmittalDate,
                    //    RefTransmittalNo = x.RefTransmittalNo,
                    //    RefTransmittalDateStr = x.RefTransmittalDate != null ? x.RefTransmittalDate.Value.ToString("dd/MM/yyyy") : "",
                    //    ApprovalId = x.ApprovalCode,
                    //    Remark = x.Remark,
                    //    JEPCustomerFeedbackId = x.JEPCustomerFeedbackId,
                    //    ReturnTransmittalNo = x.ReturnTransmittalNo,
                    //    CustomerDocNo = x.CustomerDocNo,
                    //    RevisionNo = Convert.ToInt32(x.DES075.RevisionNo),
                    //    DocNo = x.DES075.DocNo,
                    //    DocumentTitle = x.DES075.DocDescription,
                    //    ActualSubDate = x.DES027.DES076.Where(vv => vv.AgencyId == AgencyId).Select(vv => vv.CreatedOn).FirstOrDefault().Value.ToString("dd/MM/yyyy"),
                    //    PlannedSubDate = x.DES075.DES077.Where(bb => bb.DES076.AgencyDocId == bb.AgencyDocId).Select(bb => bb.PlannedSubmitDate).FirstOrDefault().Value.ToString("dd/MM/yyyy"),
                    //}).FirstOrDefault();
                    return _model;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool DeleteCustomerFeedbackDocs(Int64? Id, out string errorMsg, Int64? JEPId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES086.Find(Id);

                    if (check != null)
                    {
                        db.DES086.Remove(check);
                        db.SaveChanges();
                        AddCustomerFeedbackHistory(JEPId, null, null, Id, Id + "JEP Customer Feedback Document has Deleted", check.CreatedBy);
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public void AddCustomerFeedbackHistory(Int64? JEPId, Int64? JEPDocumentDetailsId, Int64? JEPCustomerFeedbackId, Int64? JEPCustFeedbackDocumentId, string Description, string CreatedBy)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    DES087 _DES087 = new DES087();
                    _DES087.JEPId = JEPId;
                    _DES087.JEPDocumentDetailsId = JEPDocumentDetailsId;
                    _DES087.JEPCustomerFeedbackId = JEPCustomerFeedbackId;
                    _DES087.JEPCustFeedbackDocumentId = JEPCustFeedbackDocumentId;
                    _DES087.Description = Description;
                    _DES087.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _DES087.CreatedOn = DateTime.Now;
                    db.DES087.Add(_DES087);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_ALL_CUSTOMER_FEEDBACK_LIST_Result> GetAllCustomerFeedbackList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, Int64? JepId, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_ALL_CUSTOMER_FEEDBACK_LIST(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, JepId).ToList();

                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("CFId=" + x.JEPCustomerFeedbackId + "&Id=" + x.JEPId);
                    });

                    recordsTotal = rt ?? 0;

                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public long? ReviseJEPPolicy(long JEPId, out string errorMsg)
        {
            throw new NotImplementedException();
        }

        #endregion

        public bool EditJEPDocument(JEPModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    if (model.JEPDocumentList != null)
                    {
                        foreach (var _des075 in model.JEPDocumentList)
                        {

                            DES075 objDES075 = db.DES075.Where(x => x.JEPDocumentDetailsId == _des075.JEPDocumentDetailsId).FirstOrDefault();
                            if (objDES075 != null)
                            {
                                objDES075.JEPId = model.JEPId;
                                objDES075.CustomerDocNo = _des075.CustomerDocNo;
                                objDES075.CustomerDocRevision = _des075.CustomerDocRevision;
                                objDES075.Department = _des075.Department;
                                objDES075.DocDescription = _des075.DocDescription;
                                objDES075.DocNo = _des075.DocNo;
                                objDES075.DocumentId = _des075.DocumentId;
                                objDES075.DocumentTypeId = _des075.DocumentTypeId;
                                objDES075.MilestoneDocument = _des075.MilestoneDocument;
                                objDES075.PlannedDate = _des075.PlannedDate;
                                objDES075.RevisionNo = _des075.RevisionNo;
                                objDES075.Weightage = _des075.Weightage;
                                objDES075.EditedBy = CommonService.objClsLoginInfo.UserName;
                                objDES075.EditedOn = DateTime.Now;
                                db.SaveChanges();
                            }
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
            }

        }

        public List<Tuple<Int64, double>> GetWeightage(Int64? JEPId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<Tuple<Int64, double>> list = new List<Tuple<long, double>>();
                    if (JEPId > 0)
                    {
                        var _DES075List = db.DES075.Where(x => x.JEPId == JEPId).ToList();
                        foreach (var item in _DES075List)
                        {
                            double i;
                            if (double.TryParse(item.Weightage, out i))
                            {
                                list.Add(new Tuple<long, double>(item.JEPDocumentDetailsId, Convert.ToDouble(item.Weightage)));
                            }
                        }
                    }
                    return list;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public List<SP_DES_GET_CUSTOMER_FEEDBACK_Result> GetCustomerFeedbackGrid(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, string Project, Int64? Agency, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_CUSTOMER_FEEDBACK(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project, Agency).ToList();
                    recordsTotal = data.Select(x => x.TableCount).FirstOrDefault() ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<TransmittalModel> GetTransmittalAgencyList(string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<TransmittalModel> lstmodel = new List<TransmittalModel>();
                    var list = db.DES079.Where(x => x.Project == Project).Select(x => x.AgencyId).Distinct().ToList();
                    var listagency = GetAllAgency().ToList();
                    foreach (var item in list)
                    {
                        TransmittalModel _model = new TransmittalModel();
                        _model.AgencyId = Convert.ToInt64(item);
                        _model.AgencyName = listagency.Where(z => z.AgencyId == item).Select(z => z.AgencyName).FirstOrDefault();
                        lstmodel.Add(_model);
                    }
                    return lstmodel;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public int AddCustFeedbackByAgency(Int64? Agency)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_ADD_DATA_CUSTOMERFEEDBACK_BY_AGENCY(Agency, CommonService.objClsLoginInfo.UserName);
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool EditCustFeedback(CustomerFeedback model, out string errmsg)
        {
            errmsg = string.Empty;
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {

                    if (model.JEPCustFeedbackList != null)
                    {
                        foreach (var _des085 in model.JEPCustFeedbackList)
                        {
                            DES085 objDES085 = db.DES085.Find(_des085.JEPCustomerFeedbackId);


                            if (objDES085.ApprovalCode != _des085.ApprovalId)
                            {
                                try
                                {
                                    db.SP_DES_JEP_PUSH_NOTIFICATION(model.Project, CommonService.objClsLoginInfo.UserName, NotificationObject.CustomerFBApCode.ToString(), _des085.JEPCustomerFeedbackId, _des085.Department, _des085.ApprovalId);
                                }
                                catch
                                {
                                }
                            }

                            if (objDES085 != null)
                            {
                                objDES085.JEPCustomerFeedbackId = Convert.ToInt64(_des085.JEPCustomerFeedbackId);
                                objDES085.AgencyId = _des085.AgencyId;
                                objDES085.ApprovalCode = _des085.ApprovalId;
                                if (_des085.ApprovalId > 0 && _des085.ReceiptDate == null)
                                {
                                    objDES085.ReceiptDate = DateTime.Now;
                                }
                                else
                                {
                                    objDES085.ReceiptDate = _des085.ReceiptDate;
                                }
                                objDES085.RefTransmittalNo = _des085.RefTransmittalNo;
                                objDES085.Remark = _des085.Remark;
                                objDES085.EditedBy = CommonService.objClsLoginInfo.UserName;
                                objDES085.EditedOn = DateTime.Now;
                                objDES085.ReturnTransmittalNo = _des085.ReturnTransmittalNo;
                                db.SaveChanges();
                            }
                        }
                    }
                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
            }

        }

        //public bool IsValidateExcelRecords(string Project, Int64 JepID, string DocNo, string Revision, string srcFolder, string FileName, string DestFolder, out string strMsg)
        //{
        //    bool IsValidated = false;
        //    strMsg = "";
        //    string errorMsg = "";
        //    bool Result = false;
        //    try
        //    {

        //        using (IEMQSDESEntities db = new IEMQSDESEntities())
        //        {
        //            //Check docno  exist or not in JEP
        //            var JEPRev = db.DES075.Where(x => x.DocNo == DocNo).Select(x => x.RevisionNo).FirstOrDefault();
        //            var DocRev = db.DES059.Where(x => x.DocumentNo == DocNo).Select(x => x.DocumentRevision).FirstOrDefault();
        //            if (JEPRev != null)
        //            {
        //                //Check RevisionNo is same or not
        //                Result = TempInsert(Revision, JEPRev, Project, DestFolder, DocNo, out errorMsg);
        //                if (!string.IsNullOrEmpty(errorMsg))
        //                {
        //                    strMsg = errorMsg;
        //                }
        //                return Result;
        //            }
        //            else if (DocRev != null)
        //            {
        //                //Check docno  exist or not in DOC
        //                Result = TempInsert(Revision, DocRev, Project, DestFolder, DocNo, out errorMsg);
        //                if (!string.IsNullOrEmpty(errorMsg))
        //                {
        //                    strMsg = errorMsg;
        //                }
        //                return Result;
        //            }
        //            else
        //            {
        //                return true;
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        CommonService.SendErrorToText(ex);
        //        return false;
        //    }
        //}
        public bool TempInsert(string SrcRevision, int? DestRevision, string Project, string DestFolder, string DocNo, out string strMsg)
        {
            strMsg = "";
            try
            {
                if (SrcRevision == "R" + DestRevision)
                {
                    string[] FolderName = null;
                    if (DestFolder.Contains('\\'))
                    {
                        FolderName = DestFolder.Split('\\');
                    }
                    else if (DestFolder.Contains('/'))
                    {
                        FolderName = DestFolder.Split('/');
                    }

                    if (FolderName.Length > 0)
                    {
                        Int64? ParentId = CheckDestFolder(Project, FolderName);
                        if (!(ParentId > 0))
                        {
                            strMsg += "Destination folder does not exist for DocumentNo " + DocNo + "<br />";
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                else
                {
                    strMsg += "RevisionNo " + SrcRevision + " not matched with current record for DocumentNo " + DocNo + "<br />";
                }
                return false;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
            }
        }
        public Int64? CheckDestFolder(string Project, string[] FoldName)
        {
            try
            {
                string strMsg = "";
                Int64? ParentID = 0;
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    for (int i = 0; i < FoldName.Length; i++)
                    {

                        var getDataByProject = db.DES054.Where(x => x.ProjectNo == Project).ToList();
                        if (i == 0)
                        {

                            ParentID = getDataByProject.Where(xx => xx.FolderName == FoldName[i].ToString() && xx.ParentProjectFolderId == null).Select(xx => xx.ProjectFolderId).FirstOrDefault();

                            if (!(ParentID > 0))
                            {
                                ParentID = 0;
                                break;
                            }
                        }
                        else
                        {
                            ParentID = getDataByProject.Where(xx => xx.FolderName == FoldName[i].ToString() && xx.ParentProjectFolderId != null).Select(xx => xx.ProjectFolderId).FirstOrDefault();
                            if (!(ParentID > 0))
                            {
                                ParentID = 0;
                                break;
                            }
                        }

                    }
                }
                return ParentID;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public string AddImportedData(DataTable dt, string ProjectNo, IpLocation ipLocation, string RoleGroup, string PSNo, Int64? BUId, int Location, Int64 JEPId, string FileName, string CurrentDepartment, out List<string> errorStrMsg, out string docuemntMSG)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    docuemntMSG = "";
                    errorStrMsg = new List<string>();
                    string IPAddress = string.Empty;
                    int FileLocation = ipLocation.Location;
                    var getIPAddress = ipLocation.Ip.Substring("https:".Length);
                    var index = getIPAddress.IndexOf(":");
                    if (index > 0)
                    {
                        IPAddress = getIPAddress.Substring(0, index);
                    }



                    //If there is no error in data
                    var dataDES074 = db.DES074.Where(x => x.JEPId == JEPId).FirstOrDefault();
                    List<DES075> AddDES075 = new List<DES075>();
                    List<DES109> AddDES109 = new List<DES109>();
                    var departmentlist = new clsManager().getBUsByID(3);
                    var data = GetManitainJEPbyId(JEPId);
                    for (int j = 0; j < dt.Rows.Count; j++)
                    {
                        int Revision = 0, wt = 0; bool Milestone = false;
                        if (dt.Rows[j]["Revision"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["Revision"].ToString())))
                        {
                            string s = dt.Rows[j]["Revision"].ToString().Replace('R', ' ').Replace('r', ' ');
                            int.TryParse(s, out Revision);
                        }
                        //string Weightage = "0";
                        //if (dt.Rows[j]["Weightage"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["Weightage"].ToString())))
                        //{
                        //    int.TryParse(dt.Rows[j]["Weightage"].ToString(), out wt);
                        //    Weightage = wt.ToString();
                        //}

                        if (dt.Rows[j]["Milestone"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["Milestone"].ToString())))
                        {

                            if (dt.Rows[j]["Milestone"].ToString().ToLower() == "yes")
                            {

                                Milestone = true;
                            }
                            else
                            {
                                Milestone = false;
                            }
                        }

                        DateTime? PlannedDate = null;
                        string[] formats = { "dd/MM/yyyy" , "dd/M/yyyy", "d/M/yyyy", "d/MM/yyyy","dd/MM/yy", "dd/M/yy", "d/M/yy", "d/MM/yy"
                    , "dd/MM/yyyy HH:mm:ss" , "dd/M/yyyy HH:mm:ss", "d/M/yyyy HH:mm:ss", "d/MM/yyyy HH:mm:ss","dd/MM/yy HH:mm:ss", "dd/M/yy HH:mm:ss", "d/M/yy HH:mm:ss", "d/MM/yy HH:mm:ss"};

                        string[] formats2 = { "dd-MM-yyyy" , "dd-M-yyyy", "d-M-yyyy", "d-MM-yyyy","dd-MM-yy", "dd-M-yy", "d-M-yy", "d-MM-yy"
                    , "dd-MM-yyyy HH:mm:ss" , "dd-M-yyyy HH:mm:ss", "d-M-yyyy HH:mm:ss", "d-MM-yyyy HH:mm:ss","dd-MM-yy HH:mm:ss", "dd-M-yy HH:mm:ss", "d-M-yy HH:mm:ss", "d-MM-yy HH:mm:ss"};


                        if (dt.Rows[j]["Planned Date"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["Planned Date"].ToString())))
                        {
                            DateTime PDate;
                            // var Datetime = dt.Rows[j]["Planned Date"].ToString().Split(' ')[0];	
                            if (DateTime.TryParseExact(dt.Rows[j]["Planned Date"].ToString(), formats, CultureInfo.InvariantCulture, DateTimeStyles.None, out PDate))
                                PlannedDate = PDate;
                            else if (DateTime.TryParseExact(dt.Rows[j]["Planned Date"].ToString(), formats2, CultureInfo.InvariantCulture, DateTimeStyles.None, out PDate))
                                PlannedDate = PDate;
                        }



                        string DocNo = null, Description = null, SourceFolder = null, DFileName = null, DestinationFolder = "", CustomerDOCNo = null, CustomerRevNo = null, ReturnNotes = null, DocType = null, Department = null, DepartmentName = null;
                        Int64? FolderId = null, TypeId = null;
                        if (dt.Rows[j]["DocNo"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["DocNo"].ToString())))
                        {
                            DocNo = dt.Rows[j]["DocNo"].ToString().Trim();
                        }
                        if (dt.Rows[j]["Type"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["Type"].ToString())))
                        {
                            DocType = dt.Rows[j]["Type"].ToString().Trim();
                        }
                        if (dt.Rows[j]["Description"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["Description"].ToString())))
                        {
                            Description = dt.Rows[j]["Description"].ToString().Trim();
                        }
                        if (dt.Rows[j]["Source Folder"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["Source Folder"].ToString())))
                        {
                            SourceFolder = dt.Rows[j]["Source Folder"].ToString().Trim();
                        }
                        if (dt.Rows[j]["FileName"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["FileName"].ToString())))
                        {
                            DFileName = dt.Rows[j]["FileName"].ToString().Trim();
                        }
                        if (dt.Rows[j]["Destination Folder"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["Destination Folder"].ToString())))
                        {
                            DestinationFolder = dt.Rows[j]["Destination Folder"].ToString().Trim();
                        }

                        if (dt.Rows[j]["Customer DOC No"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["Customer DOC No"].ToString())))
                        {
                            CustomerDOCNo = dt.Rows[j]["Customer DOC No"].ToString().Trim();
                        }
                        if (dt.Rows[j]["Customer Rev No"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["Customer Rev No"].ToString())))
                        {
                            CustomerRevNo = dt.Rows[j]["Customer Rev No"].ToString().Trim();
                        }
                        if (dt.Rows[j]["Return Notes"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["Return Notes"].ToString())))
                        {
                            ReturnNotes = dt.Rows[j]["Return Notes"].ToString().Trim();
                        }

                        if (dt.Rows[j]["Department"] != null && (!string.IsNullOrEmpty(dt.Rows[j]["Department"].ToString())))
                        {
                            DepartmentName = dt.Rows[j]["Department"].ToString().Trim();
                            if (!string.IsNullOrEmpty(DepartmentName))
                            {
                                Department = DepartmentName.Split('-').FirstOrDefault();
                            }
                        }


                        #region Validations

                        TypeId = db.DES026.Where(x => x.Lookup == DocType && x.Type == LookupType.DOCType.ToString()).Select(x => x.Id).FirstOrDefault();
                        if (!(TypeId > 0))
                        {
                            errorStrMsg.Add(DocNo + " documnet type is not available in system.");
                        }

                        if (!string.IsNullOrEmpty(DocNo))
                        {
                            if (DocNo.Length > 200)
                            {
                                errorStrMsg.Add(DocNo + " documnet no exceeded max length 200");
                            }

                            char[] k = { '/', '\\', ':', '*', '?', '"', '<', '>', '|', '@', ',', '*', '?', '[', ']', '#', '$', '{', '}', '"', '.' };
                            if (DocNo.IndexOfAny(k) > -1)
                            {
                                errorStrMsg.Add(DocNo + " document not contain special characters");
                            }
                            var JEPDOC = db.DES075.Where(x => x.DocNo == DocNo).OrderByDescending(x => x.RevisionNo).FirstOrDefault();
                            if (JEPDOC != null && JEPDOC.JEPDocumentDetailsId > 0)
                            {
                                if (JEPDOC.DES074.Project != ProjectNo)
                                    errorStrMsg.Add(DocNo + " documnet no used in other project JEP.");
                                else if (JEPDOC.JEPId != JEPId)
                                    errorStrMsg.Add(DocNo + " documnet no used in other JEP.");
                                else if (JEPDOC.RevisionNo != Revision)
                                    errorStrMsg.Add(DocNo + " revision not match with current revision.");
                                else if (JEPDOC.DocumentId > 0)
                                {
                                    var JEPDOCDetail = db.DES059.Find(JEPDOC.DocumentId);
                                    if (JEPDOCDetail != null && JEPDOCDetail.Status != ObjectStatus.Draft.ToString())
                                    {
                                        errorStrMsg.Add(DocNo + " is in " + db.DES062.Where(x => x.DocumentId == JEPDOC.DocumentId && x.IsCompleted == true).OrderByDescending(x => x.PolicyOrderID).Select(x => x.PolicyStep).FirstOrDefault() + " state. user can not add file(s)");
                                    }
                                }
                            }
                            else
                            {
                                var DocRev = db.DES059.Where(x => x.DocumentNo == DocNo).FirstOrDefault();
                                if (DocRev != null && DocRev.DocumentId > 0)
                                {
                                    errorStrMsg.Add(DocNo + " documnet no used in out side the JEP.");
                                }
                            }
                        }
                        else
                        {
                            errorStrMsg.Add("Documnet no is required");
                        }

                        if (PlannedDate == null)
                        {
                            errorStrMsg.Add(DocNo + " PlannedDate is required");
                        }

                        if (!string.IsNullOrEmpty(DestinationFolder))
                        {
                            string[] FolderName = DestinationFolder.Split(new char[] { '/', '\\' });

                            if (FolderName.Length > 0)
                            {
                                FolderId = CheckDestFolder(ProjectNo, FolderName);
                            }
                        }
                        //if (!(FolderId > 0))
                        //{
                        //    errorStrMsg.Add(DocNo + " destination folder not found in project.");
                        //}

                        if (!string.IsNullOrEmpty(Description))
                        {
                            if (Description.Length > 100)
                            {
                                errorStrMsg.Add(DocNo + " Description exceeded max length 100");
                            }
                        }
                        else
                        {
                            errorStrMsg.Add(DocNo + " Description is required");
                        }

                        if (!string.IsNullOrEmpty(CustomerDOCNo))
                        {
                            if (CustomerDOCNo.Length > 50)
                            {
                                errorStrMsg.Add(DocNo + " Customer Doc No exceeded max length 50");
                            }
                        }

                        if (!string.IsNullOrEmpty(CustomerRevNo))
                        {
                            if (CustomerRevNo.Length > 20)
                            {
                                errorStrMsg.Add(DocNo + " Customer Rev No exceeded max length 20");
                            }
                        }

                        if (!string.IsNullOrEmpty(Department))
                        {
                            Department = Department.Trim();
                            if (!(departmentlist.Count(x => x.t_dimx == Department) > 0))
                                errorStrMsg.Add(DocNo + " Department not found in system.");
                        }
                        else
                        {
                            errorStrMsg.Add(DocNo + " Department is required");
                        }

                        #endregion
                        var d75 = db.DES075.Where(x => x.DocNo == DocNo).FirstOrDefault();

                        if (d75 == null && (!(AddDES075.Where(x => x.DocNo == DocNo).Count() > 0)))
                        {

                            if (data.Authorized > 0)
                            {
                                DES075 _DES075 = new DES075
                                {
                                    JEPId = JEPId,
                                    DocNo = DocNo,
                                    DocDescription = Description,
                                    RevisionNo = Revision,
                                    DocumentTypeId = TypeId,
                                    CustomerDocRevision = CustomerRevNo,
                                    CustomerDocNo = CustomerDOCNo,
                                    Department = Department,
                                    PlannedDate = PlannedDate,
                                    //Weightage = Weightage,
                                    DocumentId = null,
                                    CreatedBy = CommonService.objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now,
                                    MilestoneDocument = Milestone,
                                    IsApproved = false
                                };                             
                                AddDES075.Add(_DES075);                             
                            }
                            else
                            {
                                errorStrMsg.Add(DocNo + " you are not authorized to add document");
                            }

                        }

                        if ((!string.IsNullOrEmpty(SourceFolder)) || (!string.IsNullOrEmpty(DFileName)) || (!string.IsNullOrEmpty(DestinationFolder)) || FolderId > 0)
                        {
                            DES109 _DES109 = new DES109
                            {
                                Project = ProjectNo,
                                JEPId = JEPId,
                                JEPNumber = data.JEPNumber,
                                DocNo = DocNo,
                                DocTypeId = d75?.DocumentTypeId ?? db.DES026.Where(x => x.Lookup == DocType).Select(x => x.Id).FirstOrDefault(),
                                DocRevision = d75?.RevisionNo ?? Revision,
                                DocDescription = d75?.DocDescription ?? Description,
                                SrcFolder = SourceFolder,
                                FileName = DFileName,
                                DestFolder = DestinationFolder,
                                CustomerDocNo = d75?.CustomerDocNo ?? CustomerDOCNo,
                                CustomerRevNo = d75?.CustomerDocRevision ?? CustomerRevNo,
                                Department = d75?.Department ?? Department,
                                PlannedDate = d75?.PlannedDate ?? PlannedDate,
                                //Weightage = Weightage,
                                Milestone = d75?.MilestoneDocument ?? Milestone,
                                BUId = BUId,
                                Location = Location,
                                FolderId = FolderId,
                                // ClientVersion = "",
                                CreatedBy = CommonService.objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now,
                                RoleGroup = RoleGroup,
                                IsLatestVersion = true,
                                ReturnNotes = ReturnNotes,
                                VersionGUID = Guid.NewGuid().ToString(),
                                FileLocation = FileLocation,
                                //  EmailTo = EmailTo,
                                IsUploaded = 0,
                                //JEPDocumentDetailsId = _DES075.JEPDocumentDetailsId,
                                LocationIP = IPAddress
                            };

                            AddDES109.Add(_DES109);
                        }
                    }

                    //int? Weightage75 = 0;
                    //var jepWt = db.DES075.Where(x => x.JEPId == JEPId).ToList();
                    //if (jepWt != null)
                    //    foreach (var jepdtl in jepWt)
                    //    {
                    //        int tempWt = 0;
                    //        int.TryParse(jepdtl.Weightage, out tempWt);
                    //        Weightage75 = Weightage75 + tempWt;
                    //    }
                    //if (AddDES109 != null)
                    //    foreach (var jepdtl in AddDES109)
                    //    {
                    //        int tempWt = 0;
                    //        int.TryParse(jepdtl.Weightage, out tempWt);
                    //        Weightage75 = Weightage75 + tempWt;
                    //    }

                    //if (Weightage75 > 100)
                    //    errorStrMsg.Add(" JEP  exceeded max weightage percentage 100 to " + Weightage75);

                    if (AddDES109 != null && AddDES109.Count > 0)
                    {
                        var files = AddDES109.Where(x => x.Department != CurrentDepartment).ToList();
                        if (files != null && files.Count > 0)
                            errorStrMsg.Add(string.Join(",", files.Select(x => x.DocNo)) + " user can not upload other department file(s).");
                    }

                    if (errorStrMsg.Count > 0)
                    {
                        return FileName + " not Uploaded";
                    }
                    else
                    {
                        if(AddDES075.Count()>0)
                        {
                            if (dataDES074.Status == ObjectStatus.Completed.ToString())
                            {
                                #region Notification 
                                try
                                {
                                    db.SP_DES_JEP_PUSH_NOTIFICATION(ProjectNo, CommonService.objClsLoginInfo.UserName, NotificationObject.AddDocument.ToString(), JEPId, dataDES074.Department, null);
                                }
                                catch
                                {
                                }
                                #endregion
                            }
                        }
                        db.DES075.AddRange(AddDES075);
                        db.DES109.AddRange(AddDES109);
                        db.SaveChanges();
                        docuemntMSG = "Document Uploded successfully";
                        return FileName + " Uploaded successfully ";

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool IsCheckInJEP(string PsNo, Int64? JEPId, out string PsName)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    PsName = "";
                    var temp = db.DES074.Find(JEPId);
                    if (temp != null && temp.IsJEPCheckIn == true)
                    {
                        if (temp.JEPCheckInBy != PsNo)
                        {
                            return false;
                        }
                        else
                        {
                            temp.JEPCheckInBy = new clsManager().GetUserNameFromPsNo(PsName);
                            return true;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
        public bool UpdateIsCheckInJEP(Int64? JEPId, bool IsJEPCheckIn)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES074.Find(JEPId);
                    if (temp != null)
                    {
                        temp.JEPCheckInBy = CommonService.objClsLoginInfo.UserName;
                        temp.JEPCheckInOn = DateTime.Now;
                        if (!IsJEPCheckIn)
                        {
                            temp.JEPCheckInBy = temp.JEPCheckInBy;
                            temp.JEPCheckInOn = null;
                        }
                        temp.IsJEPCheckIn = IsJEPCheckIn;
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool IsCheckInAgency(string PsNo, Int64? AgencyId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES027.Find(AgencyId);
                    if (temp != null && temp.IsAgencyCheckIn == true)
                    {
                        if (temp.AgencyCheckInBy != PsNo)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public string AgencyCheckInByPersonName(Int64? AgencyId)
        {
            try
            {
                string test = string.Empty;
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES027.Find(AgencyId);
                    if (temp != null)
                    {
                        test = new clsManager().GetUserNameFromPsNo(temp.AgencyCheckInBy);
                    }
                }
                return test;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public ProjectModel GetProjectDetails(string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES051.Where(x => x.Project == Project).Select(x => new ProjectModel
                    {
                        ProjectDescription = x.ProjectDescription,
                        ProductEndUser = x.ProductEndUser,
                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }


        public bool IsJEPHasDocument(Int64? JEPId, string Department, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var getJEPDOC = db.DES075.Where(x => x.JEPId == JEPId).ToList();
                    if (getJEPDOC.Count() > 0)
                    {
                        var getJEPDOCByDepartment = db.DES075.Where(x => x.JEPId == JEPId && x.Department == Department).ToList();
                        if (getJEPDOCByDepartment.Count() > 0)
                        {
                            return true;
                        }
                        else
                        {
                            errorMsg = "Dpartment related Document is not found";
                            return false;
                        }
                    }
                    else
                    {
                        errorMsg = "Please add atleast one Document";
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool ExportToExcel(Int64? JEPId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    List<TempJEPDocumentModel> temp = new List<TempJEPDocumentModel>();
                    var CurrentUSerDepartment = CommonService.objClsLoginInfo.Department;
                    var GetJEPNumber = db.DES074.Where(x => x.JEPId == JEPId).FirstOrDefault();
                    List<DES075> _DES075ist = db.DES075.Where(x => x.JEPId == JEPId && x.Department == CurrentUSerDepartment).ToList();

                    if (_DES075ist != null && _DES075ist.Count > 0)
                    {
                        var data = _DES075ist.Select(x => new TempJEPDocumentModel
                        {

                            DocumentTypeName = db.DES026.Where(xx => xx.Id == x.DocumentTypeId).Select(xx => xx.Lookup).FirstOrDefault(),
                            DocNo = x.DocNo,
                            RevisionNo = x.RevisionNo ?? 0,
                            DocDescription = x.DocDescription,
                            SourceFolder = null,
                            FileName = null,
                            DestinationFolder = null,
                            CustomerDocNo = x.CustomerDocNo,
                            CustomerDocRevision = x.CustomerDocRevision,
                            PlannedDate = x.PlannedDate,
                            Department = new clsManager().getDepartmentDescription(x.Department),
                            MilestoneDocument = x.MilestoneDocument ?? false,
                            ReturnNotes = null
                        }).ToList();
                        temp = data;
                    }

                    else
                    {
                        return false;
                    }

                    DataTable dt = ToDataTable(temp);

                    dt.Columns["DocumentTypeName"].ColumnName = "Type";
                    dt.Columns["DocNo"].ColumnName = "DocNo";
                    dt.Columns["RevisionNo"].ColumnName = "Revision";
                    dt.Columns["DocDescription"].ColumnName = "Description";
                    dt.Columns["SourceFolder"].ColumnName = "Source Folder";
                    dt.Columns["FileName"].ColumnName = "FileName";
                    dt.Columns["DestinationFolder"].ColumnName = "Destination Folder";
                    dt.Columns["CustomerDocNo"].ColumnName = "Customer DOC No";
                    dt.Columns["CustomerDocRevision"].ColumnName = "Customer Rev No";
                    dt.Columns["PlannedDate"].ColumnName = "Planned Date";
                    dt.Columns["Department"].ColumnName = "Department";
                    dt.Columns["MilestoneDocument"].ColumnName = "Milestone";
                    dt.Columns["ReturnNotes"].ColumnName = "Return Notes";


                    string sFilename = GetJEPNumber.JEPNumber + ".xlsx";
                    MemoryStream ms = DataTableToExcelXlsx(dt, "Sheet1");
                    ms.WriteTo(HttpContext.Current.Response.OutputStream);
                    HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                    HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment;filename=" + sFilename);
                    HttpContext.Current.Response.StatusCode = 200;
                    HttpContext.Current.Response.End();

                    //HttpContext.Current.Response.StatusCode = 200;

                    //HttpContext.Current.Response.Clear();
                    //HttpContext.Current.Response.Buffer = true;
                    //HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + GetJEPNumber.JEPNumber + ".xls");
                    //HttpContext.Current.Response.Charset = "";
                    //HttpContext.Current.Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";

                    ////style to format numbers to string
                    //string style = @"<style> .textmode { mso-number-format:\@; } </style>";
                    //HttpContext.Current.Response.Write(style);
                    //HttpContext.Current.Response.Output.Write(ConvertListToHtmlTable(temp).ToString());

                    //HttpContext.Current.Response.Flush();
                    //HttpContext.Current.Response.End();
                    ////DataTable table = ToDataTable(temp);
                    ////table.Columns["DocumentTypeId"].ColumnName = "SubjectMarks";


                    ////using (XLWorkbook wb = new XLWorkbook())
                    ////{
                    ////    wb.Worksheets.Add(table, "Customers");

                    ////    //wb.SaveAs(folderPath + "DataGridViewExport.xlsx");
                    ////    string myName = "test" +DateTime.Now.ToShortDateString() + ".xlsx";
                    ////    MemoryStream stream = GetStream(wb);// The method is defined below
                    ////    System.Web.HttpContext.Current.Response.Clear();
                    ////    System.Web.HttpContext.Current.Response.Buffer = true;
                    ////    System.Web.HttpContext.Current.Response.AddHeader("content-disposition",
                    ////    "attachment; filename=" + myName);
                    ////    System.Web.HttpContext.Current.Response.ContentType = "application/vnd.ms-excel";
                    ////    System.Web.HttpContext.Current.Response.BinaryWrite(stream.ToArray());
                    ////    System.Web.HttpContext.Current.Response.End();
                    ////}

                    return true;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public static MemoryStream DataTableToExcelXlsx(DataTable table, string sheetName)
        {
            MemoryStream Result = new MemoryStream();
            ExcelPackage pack = new ExcelPackage();
            ExcelWorksheet ws = pack.Workbook.Worksheets.Add(sheetName);
            int col = 1;
            int row = 1;
            foreach (DataColumn column in table.Columns)
            {
                ws.Cells[row, col].Value = column.ColumnName.ToString();


                ws.DefaultColWidth = 30;

                using (ExcelRange Rng = ws.Cells[row, col])
                {
                    Rng.Value = column.ColumnName.ToString();
                    Rng.Style.Font.Size = 13;
                    Rng.Style.Font.Bold = true;

                }
                col++;
            }
            col = 1;
            row = 2;
            foreach (DataRow rw in table.Rows)
            {
                foreach (DataColumn cl in table.Columns)
                {
                    using (ExcelRange Rng = ws.Cells[row, col])
                    {
                        if (rw[cl.ColumnName] != DBNull.Value)
                        {
                            if (cl.ColumnName == "Revision")
                            {
                                ws.Cells[row, col].Value = "R" + rw[cl.ColumnName].ToString();
                            }
                            else if (cl.ColumnName == "Milestone")
                            {
                                if (rw[cl.ColumnName].ToString().ToLower() != "true")
                                {
                                    ws.Cells[row, col].Value = "No";
                                }
                                else
                                {
                                    ws.Cells[row, col].Value = "Yes";
                                }
                            }
                            else if (cl.ColumnName == "Planned Date")
                            {

                                ws.Cells[row, col].Value = rw[cl.ColumnName].ToString().Split(' ')[0];
                            }

                            else
                            {
                                ws.Cells[row, col].Value = rw[cl.ColumnName].ToString();
                            }

                        }
                        Rng.Style.Font.Size = 13;
                    }

                    col++;
                }
                row++;
                col = 1;
            }
            pack.SaveAs(Result);
            return Result;
        }



        //public MemoryStream GetStream(XLWorkbook excelWorkbook)
        //{
        //    MemoryStream fs = new MemoryStream();
        //    excelWorkbook.SaveAs(fs);
        //    fs.Position = 0;
        //    return fs;
        //}

        private StringBuilder ConvertListToHtmlTable(List<TempJEPDocumentModel> list)
        {
            StringBuilder sb = new StringBuilder();
            PropertyInfo[] Props = typeof(TempROCModel).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            //sb.Append("<Table border='1'><thead bgcolor='gray'><tr>");

            if (list.Count > 0)
            {
                PropertyInfo[] PropsA = typeof(TempJEPDocumentModel).GetProperties(BindingFlags.Public | BindingFlags.Instance);
                sb.Append("<Table border='1'><thead bgcolor='gray'><tr>");
                foreach (PropertyInfo prop in PropsA)
                {

                    if (prop.Name == "DocumentTypeId")
                    {
                        sb.Append("<th style='font-size:30px; background-color:#C6EFCE;' align='left'>" + prop.Name.Replace("DocumentTypeId", "Type") + " </th>");
                    }
                    else if (prop.Name == "DocNo")
                    {
                        sb.Append("<th style='font-size:30px; background-color:#C6EFCE;' align='left'>" + prop.Name.Replace("DocNo", "DocNo") + " </th>");
                    }
                    else if (prop.Name == "RevisionNo")
                    {
                        sb.Append("<th style='font-size:30px; background-color:#C6EFCE;' align='left'>" + prop.Name.Replace("RevisionNo", "Revision") + " </th>");
                    }
                    else if (prop.Name == "DocDescription")
                    {
                        sb.Append("<th style='font-size:30px; background-color:#C6EFCE;' align='left'>" + prop.Name.Replace("DocDescription", "Description") + " </th>");
                    }
                    else if (prop.Name == "SourceFolder")
                    {
                        sb.Append("<th style='font-size:30px; background-color:#C6EFCE;' align='left'>" + prop.Name.Replace("SourceFolder", "Source Folder") + " </th>");
                    }
                    else if (prop.Name == "FileName")
                    {
                        sb.Append("<th style='font-size:30px; background-color:#C6EFCE;' align='left'>" + prop.Name.Replace("FileName", "FileName") + " </th>");
                    }
                    else if (prop.Name == "DestinationFolder")
                    {
                        sb.Append("<th style='font-size:30px; background-color:#C6EFCE;' align='left'>" + prop.Name.Replace("DestinationFolder", "Destination Folder") + " </th>");
                    }
                    else if (prop.Name == "CustomerDocNo")
                    {
                        sb.Append("<th style='font-size:30px; background-color:#C6EFCE;' align='left'>" + prop.Name.Replace("CustomerDocNo", "Customer DOC No") + " </th>");
                    }
                    else if (prop.Name == "CustomerDocRevision")
                    {
                        sb.Append("<th style='font-size:30px; background-color:#C6EFCE;' align='left'>" + prop.Name.Replace("CustomerDocRevision", "Customer Rev No") + " </th>");
                    }
                    else if (prop.Name == "PlannedDate")
                    {
                        sb.Append("<th style='font-size:30px; background-color:#C6EFCE;' align='left'>" + prop.Name.Replace("PlannedDate", "Planned Date") + " </th>");
                    }
                    else if (prop.Name == "Department")
                    {
                        sb.Append("<th style='font-size:30px; background-color:#C6EFCE;' align='left'>" + prop.Name.Replace("Department", "Department") + " </th>");
                    }
                    else if (prop.Name == "MilestoneDocument")
                    {
                        sb.Append("<th style='font-size:30px; background-color:#C6EFCE;' align='left'>" + prop.Name.Replace("MilestoneDocument", "Milestone") + " </th>");
                    }
                    else if (prop.Name == "ReturnNotes")
                    {
                        sb.Append("<th style='font-size:30px; background-color:#C6EFCE;' align='left'>" + prop.Name.Replace("MilestoneDocument", "Return Notes") + " </th>");
                    }
                    else
                    {
                        sb.Append("<th align='left'>" + prop.Name.Replace("_", " ") + "</th>");
                    }
                }
                sb.Append("</tr></thead><tbody>");
                sb.Append("<tr>");
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    foreach (TempJEPDocumentModel item in list)
                    {
                        for (int i = 0; i < PropsA.Length; i++)
                        {
                            if (PropsA[i].Name == "DocumentTypeId")
                            {
                                if (PropsA[i].GetValue(item, null) != null)
                                {
                                    var getValue = Convert.ToInt64(PropsA[i].GetValue(item, null));
                                    var getDES26 = db.DES026.Where(x => x.Id == getValue).Select(x => x.Lookup).FirstOrDefault();
                                    sb.Append("<td style='font-size:30px;' align='left' >" + getDES26 + "</td>");
                                }
                            }
                            else if (PropsA[i].Name == "RevisionNo")
                            {
                                if (PropsA[i].GetValue(item, null) != null)
                                {
                                    var getValue = "R" + (PropsA[i].GetValue(item, null));

                                    sb.Append("<td style='font-size:30px;' align='left' >" + getValue + "</td>");
                                }
                            }
                            else if (PropsA[i].Name == "MilestoneDocument")
                            {
                                if (PropsA[i].GetValue(item, null) != null)
                                {
                                    var getValue = (PropsA[i].GetValue(item, null));

                                    if (getValue.ToString().ToLower() != "true")
                                    {
                                        sb.Append("<td style='font-size:30px;' align='left' >No</td>");
                                    }
                                    else
                                    {
                                        sb.Append("<td style='font-size:30px;' align='left' >Yes</td>");
                                    }

                                }
                            }
                            else
                            {
                                sb.Append("<td style='font-size:30px;' align='left' >" + PropsA[i].GetValue(item, null) + "</td>");
                            }
                        }
                        sb.Append("</tr>");
                    }
                }
            }

            sb.Append("</tbody></table>");
            return sb;
        }

        public static DataTable ToDataTable<T>(List<T> items)
        {
            DataTable dataTable = new DataTable(typeof(T).Name);

            //Get all the properties
            PropertyInfo[] Props = typeof(T).GetProperties(BindingFlags.Public | BindingFlags.Instance);
            foreach (PropertyInfo prop in Props)
            {
                //Defining type of data column gives proper data table 
                var type = (prop.PropertyType.IsGenericType && prop.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(prop.PropertyType) : prop.PropertyType);
                //Setting column names as Property names
                if (prop.Name != "DocumentTypeId")
                {
                    dataTable.Columns.Add(prop.Name, type);
                }

            }
            foreach (T item in items)
            {
                var values = new object[Props.Length];
                for (int i = 0; i < Props.Length; i++)
                {
                    if (Props[i].Name != "DocumentTypeId")
                    {
                        //inserting property values to datatable rows

                        values[i] = Props[i].GetValue(item, null);


                    }
                }
                dataTable.Rows.Add(values);
            }
            //put a breakpoint here and check datatable
            return dataTable;
        }
    }
}