﻿using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IEMQS.DESServices
{
    public class GlobalJEPTemplateService : IGlobalJEPTemplateService
    {

        #region Global JEP Template

        public Int64 AddEdit(GlobalJEPTemplateModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES030.Find(model.JEPTemplateId);


                    DES030 _DES030 = temp == null ? new DES030() : temp;
                    if (temp != null)
                    {
                        db.DES031.RemoveRange(_DES030.DES031);
                    }

                    _DES030.Project = model.Project;
                    _DES030.TemplateName = model.TemplateName;
                    _DES030.BUId = model.BUId;
                    _DES030.ProductId = model.ProductId;
                    List<DES031> _DES031List = new List<DES031>();
                    foreach (var d31 in model.JEPTemplateDocList)
                    {
                        if (!d31.IsDelete && d31.DocumentNo != null && d31.DocumentTitle != null)
                            _DES031List.Add(new DES031
                            {
                                DocumentNo = d31.DocumentNo,
                                DocumentTitle = d31.DocumentTitle,
                                CreatedBy = CommonService.objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now,
                            });
                    }
                    _DES030.DES031 = _DES031List;
                    if (temp == null)
                    {
                        _DES030.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES030.CreatedOn = DateTime.Now;
                        db.DES030.Add(_DES030);
                    }
                    else
                    {
                        _DES030.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES030.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES030.JEPTemplateId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<GlobalJEPTemplateModel> GetAllGlobalJEPTemplate(Int64 BuId, Int64 ProductId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES030.Where(x => x.BUId == BuId && x.ProductId == ProductId).Select(x => new GlobalJEPTemplateModel
                    {
                        JEPTemplateId = x.JEPTemplateId,
                        Project = x.Project,
                        TemplateName = x.TemplateName,
                        BUId = x.BUId,
                        ProductId = x.ProductId,
                        Product = x.DES029.Product + "-" + x.DES029.Description
                        //BU = x.BU + "-" + x.PBU
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public GlobalJEPTemplateModel GetGlobalJEPTemplatebyId(Int64 Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES030.Where(x => x.JEPTemplateId == Id).Select(x => new GlobalJEPTemplateModel
                    {
                        JEPTemplateId = x.JEPTemplateId,
                        Project = x.Project,
                        TemplateName = x.TemplateName,
                        BUId = x.BUId,
                        ProductId = x.ProductId,
                        JEPTemplateDocList = x.DES031.Select(y => new JEPTemplateDocumentModel
                        {
                            JEPTemplateDocumentId = y.JEPTemplateDocumentId,
                            JEPTemplateId = y.JEPTemplateId,
                            DocumentNo = y.DocumentNo,
                            DocumentTitle = y.DocumentTitle,
                        }).ToList(),

                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<GlobalJEPTemplateModel> GetAllGlobalJEPByProduct(Int64 BuId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES029.Where(x => x.BUId == BuId).Select(x => new GlobalJEPTemplateModel
                    {
                        BUId = x.BUId,
                        ProductId = x.ProductId,
                        Product = x.Product + "-" + x.Description
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<GlobalJEPTemplateModel> GetAllGlobalJEPTemplateByProductId(Int64 ProductId,Int64? BUId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES030.Where(x => x.Project == null && x.ProductId == ProductId && x.BUId==BUId).Select(x => new GlobalJEPTemplateModel
                    {
                        JEPTemplateId = x.JEPTemplateId,
                        Project = x.Project,
                        TemplateName = x.TemplateName,
                        BUId = x.BUId,
                        ProductId = x.ProductId,
                        Product = x.DES029.Product + "-" + x.DES029.Description
                        //BU = x.BU + "-" + x.PBU
                    }).ToList();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_GLOBAL_JEP_TEMPLATE_Result> GetGlobalJEPTemplateList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_GLOBAL_JEP_TEMPLATE(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex).ToList();
                    recordsTotal = 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.JEPTemplateId);
                        x.JEPTemplateCheckInByName = new clsManager().GetUserNameFromPsNo((x.JEPTemplateCheckInBy));
                    });
                    recordsTotal = rt ?? 0;
                    return data;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteGlobalJEPTemplate(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES030.Find(Id);
                    if (check != null)
                    {
                        db.DES031.RemoveRange(check.DES031);
                        db.DES030.Remove(check);
                        db.SaveChanges();
                        return check.JEPTemplateId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool CheckTemplateNameAlreadyExist(string name, Int64? jeptemplateId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    name = name.Trim();
                    var data = db.DES030.Where(x => x.TemplateName == name && (x.JEPTemplateId != jeptemplateId || jeptemplateId == null)).ToList();
                    if (data != null && data.Count > 0)
                    {
                        return true;
                    }
                    else
                        return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                return false;
            }
        }

        #endregion

        #region JEP Template

        public Int64 AddEditProjectTemplate(GlobalJEPTemplateModel model)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES030.Find(model.JEPTemplateId);


                    DES030 _DES030 = temp == null ? new DES030() : temp;
                    if (temp != null)
                    {
                        db.DES031.RemoveRange(_DES030.DES031);
                    }

                    _DES030.Project = model.Project;
                    _DES030.TemplateName = model.TemplateName;
                    _DES030.BUId = model.BUId;
                    _DES030.ProductId = model.ProductId;
                    List<DES031> _DES031List1 = new List<DES031>();
                    foreach (var d31 in model.JEPTemplateDocList)
                    {
                        if (!d31.IsDelete && d31.DocumentNo != null && d31.DocumentTitle != null)
                            _DES031List1.Add(new DES031
                            {
                                DocumentNo = d31.DocumentNo,
                                DocumentTitle = d31.DocumentTitle,
                            });
                    }
                    _DES030.DES031 = _DES031List1;
                    if (temp == null)
                    {
                        _DES030.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _DES030.CreatedOn = DateTime.Now;
                        db.DES030.Add(_DES030);
                    }
                    else
                    {
                        _DES030.EditedBy = CommonService.objClsLoginInfo.UserName;
                        _DES030.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    return _DES030.JEPTemplateId;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
        
        public GlobalJEPTemplateModel GetJEPTemplatebyId(Int64 Id)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    return db.DES030.Where(x => x.JEPTemplateId == Id).Select(x => new GlobalJEPTemplateModel
                    {
                        JEPTemplateId = x.JEPTemplateId,
                        Project = x.Project,
                        TemplateName = x.TemplateName,
                        BUId = x.BUId,
                        ProductId = x.ProductId,
                        JEPTemplateDocList = x.DES031.Select(y => new JEPTemplateDocumentModel
                        {
                            JEPTemplateDocumentId = y.JEPTemplateDocumentId,
                            JEPTemplateId = y.JEPTemplateId,
                            DocumentNo = y.DocumentNo,
                            DocumentTitle = y.DocumentTitle,
                        }).ToList(),

                    }).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public List<SP_DES_GET_JEP_TEMPLATE_Result> GetJEPTemplateList(string sSearch, int startIndex, int displayLength, int sortColumnIndex, string sortDirection, out int recordsTotal, string Project)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var data = db.SP_DES_GET_JEP_TEMPLATE(startIndex, displayLength, sSearch, sortDirection, sortColumnIndex, Project).ToList();
                    recordsTotal = 0;
                    int? rt = 0;
                    data.ForEach(x =>
                    {
                        rt = x.TableCount;
                        x.QString = CommonService.Encrypt("Id=" + x.JEPTemplateId);
                        x.JEPTemplateCheckInByName = new clsManager().GetUserNameFromPsNo((x.JEPTemplateCheckInBy));
                    });
                    recordsTotal = rt ?? 0;
                    return data;

                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public Int64 DeleteJEPTemplate(Int64 Id, out string errorMsg)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    errorMsg = "";
                    var check = db.DES030.Find(Id);
                    if (check != null)
                    {
                        db.DES031.RemoveRange(check.DES031);
                        db.DES030.Remove(check);
                        db.SaveChanges();
                        return check.JEPTemplateId;
                    }
                    return 0;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        #endregion

        public bool UpdateIsCheckInJEPTemplate(Int64? JEPTemplateId, bool IsJEPTemplateCheckIn)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES030.Find(JEPTemplateId);
                    if (temp != null)
                    {
                        temp.JEPTemplateCheckInBy = CommonService.objClsLoginInfo.UserName;
                        temp.JEPTemplateCheckInOn = DateTime.Now;
                        if (!IsJEPTemplateCheckIn)
                        {
                            temp.JEPTemplateCheckInBy = temp.JEPTemplateCheckInBy;
                            temp.JEPTemplateCheckInOn = null;
                        }
                        temp.IsJEPTemplateCheckIn = IsJEPTemplateCheckIn;
                        db.Entry(temp).State = System.Data.Entity.EntityState.Modified;
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }

        public bool IsCheckInJEPTemplate(string PsNo, Int64? JEPTemplateId)
        {
            try
            {
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    var temp = db.DES030.Find(JEPTemplateId);
                    if (temp != null && temp.IsJEPTemplateCheckIn == true)
                    {
                        if (temp.JEPTemplateCheckInBy != PsNo)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }
                    }
                }
                return false;
            }
            catch (Exception ex)
            {
                CommonService.SendErrorToText(ex);
                throw;
            }
        }
    }
}
