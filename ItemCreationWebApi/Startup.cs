﻿using ItemCreationWebApi.Provider;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[assembly: OwinStartup(typeof(ItemCreationWebApi.Startup))]

namespace ItemCreationWebApi
{
    public class Startup
    {
        public void ConfigureAuth(IAppBuilder app)
        {
            double expiryTime = 60;
            if (System.Configuration.ConfigurationManager.AppSettings["TokenExpirationTimeInMinutes"] != null)
            {
                if (System.Configuration.ConfigurationManager.AppSettings["TokenExpirationTimeInMinutes"].ToString() != "")
                {
                    expiryTime = Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["TokenExpirationTimeInMinutes"].ToString());
                }
            }

            var OAuthOptions = new OAuthAuthorizationServerOptions
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(expiryTime),
                Provider = new SimpleAuthorizationServerProvider(),
                //RefreshTokenProvider = new RefreshTokenProvider()
            };

            app.UseOAuthBearerTokens(OAuthOptions);
            app.UseOAuthAuthorizationServer(OAuthOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());

            HttpConfiguration config = new HttpConfiguration();
            WebApiConfig.Register(config);
        }

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}
