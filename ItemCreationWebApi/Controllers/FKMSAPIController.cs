﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ItemCreationWebApi.Controllers
{
    public class FKMSAPIController : ApiController
    {
        public readonly IEMQSEntitiesContext db;

        public FKMSAPIController()
        {
            if (db == null)
            {
                db = new IEMQSEntitiesContext();
            }
        }

        [HttpPost]
        [Authorize]
        // POST: ICC/api/MaintainItemAPI       
        public HttpResponseMessage Post([System.Web.Mvc.Bind(Include = "PCLNumber,PCLRevision,Id,Project,MaterialType,MaterialOwner,Department,PosNo_FixNo,ItemId,PCLNo,Stage,MaterialLocation,MaterialSubLocation,Qty,TransactionType,ReceivedfromInKit,UserPSNo,Location,KitNo,IsAcknowledged")]FKM130 model)
        {
            var objResponseData = new ResponseData();
            try
            {
                var ReqFields = new List<string>();
                if (String.IsNullOrWhiteSpace(model.UserPSNo))
                {
                    ReqFields.Add("UserPsno");
                }
                if (String.IsNullOrWhiteSpace(model.Location))
                {
                    ReqFields.Add("Location");
                }
                if (String.IsNullOrWhiteSpace(model.MaterialOwner) || !clsImplementationEnum.getMaterialOwners().Contains(model.MaterialOwner))
                {
                    ReqFields.Add("MaterialOwner");
                }
                if (String.IsNullOrWhiteSpace(model.MaterialType) || !clsImplementationEnum.getMaterialTypes().Contains(model.MaterialType))
                {
                    ReqFields.Add("MaterialType");
                }
                if (String.IsNullOrWhiteSpace(model.MaterialLocation) && model.TransactionType == clsImplementationEnum.TransactionType.Received.GetStringValue())
                {
                    ReqFields.Add("MaterialLocation");
                }
                if (String.IsNullOrWhiteSpace(model.Stage) || !clsImplementationEnum.getStageTypes().Contains(model.Stage))
                {
                    ReqFields.Add("Stage");
                }
                if (String.IsNullOrWhiteSpace(model.TransactionType) || !clsImplementationEnum.getTransactionTypes().Contains(model.TransactionType))
                {
                    ReqFields.Add("TransactionType");
                }
                if (model.Qty < 0)
                {
                    ReqFields.Add("Qty");
                }
                if (ReqFields.Any())
                {
                    objResponseData.Status = false;
                    objResponseData.Message = string.Join(",", ReqFields) + " Is Invalid or Required";
                    return Request.CreateResponse(HttpStatusCode.OK, objResponseData);
                }

                if (model.MaterialType == clsImplementationEnum.MaterialType.FXR.GetStringValue())
                {
                    var result = db.Database.ExecuteSqlCommand(@"INSERT INTO FKM130 ( Project,MaterialType,MaterialOwner,Department,PosNo_FixNo,ItemId,PCLNo,Stage,MaterialLocation,MaterialSubLocation,Qty,TransactionType,ReceivedfromInKit,KitNo,CreatedBy,CreatedOn) 
 Values('" + model.Project.Trim() + "', '" + model.MaterialType + "','" + model.MaterialOwner + "','" + model.Department + "','" + model.FindNo + "','" + model.ItemId + "','" + model.PCLNo + "','" + model.Stage + "','" + model.MaterialLocation + "','" + model.MaterialSubLocation + "','" + model.Qty + "','" + model.TransactionType + "','" + model.ReceivedfromInKit + "','" + model.KitNo + "','" + model.UserPSNo + "',GETDATE() )");
                    db.SaveChanges();
                    objResponseData.Status = true;
                    objResponseData.Message = "success";
                }
                else
                {
                    var objPCLData = db.SP_FKMS_GET_PCR_PCL_DETAILS(model.Location, 0, 1, "", " Project = '" + model.Project + "' AND PCLNumber = '" + model.PCLNumber + "' AND PCLRevision = '" + model.PCLRevision + "' AND t_prtn = '" + model.FindNo + "' ").FirstOrDefault();
                    if (objPCLData != null)
                    {
                        // add track record in FKM128
                        /*db.FKM130.Add(new FKM130()
                        {
                           Project = objPCLData.Project
                           MaterialType = model.MaterialType, // NodeTypes.PLT.GetStringValue()
                           MaterialOwner = model.MaterialOwner, // "PPS"
                           Department = objPCLData.SFCDept,
                           PosNo_FixNo = objPCLData.FindNo,
                           ItemId = objPCLData.Item,
                           PCLNo = objPCLData.PCLNumber,
                           Stage = model.Stage, //"2nd Stage Cleared"
                           MaterialLocation =  model.MaterialLocation;//objPCLData.MaterialLocation
                           //MaterialSubLocation = objPCLData.MaterialSubLocation
                           Qty = model.Qty,
                           TransactionType = model.TransactionType,
                           ReceivedfromInKit = model.ReceivedfromInKit, //"QC"
                           KitNo = model.KitNo, //"QC"
                            CreatedBy = UserPSNo,
                            CreatedOn = DateTime.Now,
                        });*/
                        var result = db.Database.ExecuteSqlCommand(@"INSERT INTO FKM130 ( Project,MaterialType,MaterialOwner,Department,PosNo_FixNo,ItemId,PCLNo,Stage,MaterialLocation,MaterialSubLocation,Qty,TransactionType,ReceivedfromInKit,KitNo,CreatedBy,CreatedOn) 
 Values('" + objPCLData.Project.Trim() + "', '" + model.MaterialType + "','" + model.MaterialOwner + "','" + objPCLData.SFCDept.Trim() + "','" + objPCLData.FindNo + "','" + objPCLData.Item.Trim() + "','" + objPCLData.PCLNumber.Trim() + "','" + model.Stage + "','" + model.MaterialLocation + "','" + model.MaterialSubLocation + "','" + model.Qty + "','" + model.TransactionType + "','" + model.ReceivedfromInKit + "','" + model.KitNo + "','" + model.UserPSNo + "',GETDATE() )");
                        db.SaveChanges();

                        objResponseData.Status = true;
                        objResponseData.Message = "success";
                    }
                    else
                    {
                        objResponseData.Status = false;
                        objResponseData.Message = "Record not found!!";
                    }
                }
            }
            catch (Exception ex)
            {
                objResponseData.Status = false;
                objResponseData.Message = ex.Message.ToString();
                return Request.CreateResponse(HttpStatusCode.BadRequest, objResponseData);
            }
            return Request.CreateResponse(HttpStatusCode.OK, objResponseData);
        }
        public class ResponseData
        {
            public bool Status { get; set; }
            public string Message { get; set; }
        }
    }

    #region Temp EDMX Mapper
    public class FKM130
    {
        public int Id { get; set; }
        public string Project { get; set; }
        public string MaterialType { get; set; }
        public string MaterialOwner { get; set; }
        public string Department { get; set; }
        public string PosNo_FixNo { get; set; }
        public string ItemId { get; set; }
        public string PCLNo { get; set; }
        public string Stage { get; set; }
        public string MaterialLocation { get; set; }
        public string MaterialSubLocation { get; set; }
        public int Qty { get; set; }
        public string TransactionType { get; set; }
        public string ReceivedfromInKit { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string KitNo { get; set; }
        public bool IsAcknowledged { get; set; }
        public string UserPSNo { get; set; }
        public string Location { get; set; }
        public string FindNo { get; set; }
        public string PCLNumber { get; set; }
        public string PCLRevision { get; set; }
    }
    #endregion
}
