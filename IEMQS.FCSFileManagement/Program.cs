﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using IEMQS.DESCore;
using IEMQS.DESCore.Data;
using IEMQS.DESServices;

namespace IEMQS.FCSFileManagement
{
    class Program
    {
        public string myNetworkPath = string.Empty;

        static void Main(string[] args)
        {
            #region Hazira to Central

            //Transfer_DOC_Object_FIle_Hazira_To_Central();

            //TransferCustomerFeedbackFile_Hazira_To_Central();
            //TransferDCRFiles_Hazira_To_Central();
            ////Transfer_ROC_Files_Hazira_To_Central(); //Currently not in use as this functionality is absolute as pe Bhavin R P on 3/04/2020
            //Transfer_ROC_Attached_Files_Hazira_To_Central();
            #endregion

            #region Powai to Central
            //Transfer_DOC_Object_FIle_POWAI_To_Central();

            //TransferCustomerFeedbackFile_Powai_To_Hazira();
            //TransferDCRFiles_Powai_To_Hazira();
            ////Transfer_ROC_Files_Powai_To_Hazira();
            //Transfer_ROC_Attached_Files_Powai_To_Hazira();

            //MoveHaziraRecordofCustomerFeedbacktoTransferTable();
            //MoveHaziraRecordofDCRFilestoTransferTable();
            //MoveHaziraRecordofROCFilesTOTransferTable();
            //MoveHaziraRecordofROC_Commented_FilesTOTransferTable();
            //Console.ReadLine();
            #endregion

            #region Ranoli to Central
            //Transfer_DOC_Object_FIle_RANOLI_To_Central();

            //TransferCustomerFeedbackFile_Ranoli_To_Hazira();
            //TransferDCRFiles_Ranoli_To_Hazira();
            //////Transfer_ROC_Files_Ranoli_To_Hazira();
            //Transfer_ROC_Attached_Files_Ranoli_To_Hazira();
            #endregion

            #region PLM FCS TO 2.0 FCS
            var ApplicationLocation = ConfigurationManager.AppSettings["ApplicationLocation"].ToString();
            Transfer_PLM_FILES_TO_20_FCS(1, "");
            Transfer_PLM_FILES_TO_20_FCS(2, "");
            //Transfer_PLM_FILES_TO_20_FCS(2, "");
            #endregion
        }

        private static void Transfer_DOC_Object_FIle_Hazira_To_Central()
        {
            try
            {
                string HaziraPathdb = ConfigurationValues.GetConfigvalues("FCS_HAZIRA_FILE_PATH");


                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = HaziraPathdb; //@"//172.16.7.242/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"H:\FCSFiles\";

                string FixedSourcePath = HaziraPathdb;// @"//172.16.7.242/FCSFiles/";

                string PowaiPath = HaziraPathdb;// @"\\172.16.7.242\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using...");
                    List<SP_DES_GET_DOC_OBJECT_FILE_FOR_TRANSFER_Result> listDES060s = new List<SP_DES_GET_DOC_OBJECT_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    listDES060s = ObjFCSMangmt.GetDOCObjectFileForTransfer((int)(LocationType.Hazira));
                    Console.WriteLine("After db operation get...");
                    if (listDES060s.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + listDES060s.Count());

                        foreach (SP_DES_GET_DOC_OBJECT_FILE_FOR_TRANSFER_Result fCSFileTransfer in listDES060s)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.MainDocumentPath;
                                fileName = fCSFileTransfer.Document_name;
                                targetPath = @"H:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, PowaiPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(PowaiPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.UpdateCentralFlag(fCSFileTransfer.DocumentMappingId, (int)(LocationType.Hazira));

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.DocumentMappingId), (int)(LocationType.Powai));

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.DocumentMappingId, (int)LocationType.Hazira, "DOC");
                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Hazira)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Hazira);
                }


                //// To copy all the files in one directory to another directory.
                //// Get the files in the source folder. (To recursively iterate through
                //// all subfolders under the current directory, see
                //// "How to: Iterate Through a Directory Tree.")
                //// Note: Check for target path was performed previously
                ////       in this code example.
                //if (System.IO.Directory.Exists(sourcePath))
                //{
                //    string[] files = System.IO.Directory.GetFiles(sourcePath);

                //    // Copy the files and overwrite destination files if they already exist.
                //    foreach (string s in files)
                //    {
                //        // Use static Path methods to extract only the file name from the path.
                //        fileName = System.IO.Path.GetFileName(s);
                //        destFile = System.IO.Path.Combine(targetPath, fileName);
                //        System.IO.File.Copy(s, destFile, true);
                //    }
                //}
                //else
                //{
                //    Console.WriteLine("Source path does not exist!");
                //}

                // Keep console window open in debug mode.

                //Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Hazira)));
                //Console.ReadLine();
            }
            //Console.ReadLine();
        }
        private static void Transfer_PLM_POWAI_FILES_TO_20_POWAI_FCS()
        {
            try
            {
                string PowaiPathdb = @"//10.7.66.185/FCSFiles/PLM_STORE/Powai/";//ConfigurationValues.GetConfigvalues("FCS_HAZIRA_FILE_PATH");//10.7.66.57
                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = PowaiPathdb;// @"//192.168.27.9/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"//172.16.7.242/FCSFiles";

                string FixedSourcePath = PowaiPathdb;// @"//192.168.27.9/FCSFiles/";

                string PowaiPath = PowaiPathdb;// @"\\192.168.27.9\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using for transfer PLM Powai file from Powai PLM FCS to Powai 2.0 FCS...");
                    List<SP_DES_GET_PLM_DOC_OBJECT_FILE_FOR_TRANSFER_TO_IEMQS_FCS_Result> customerfeedbackfiles = new List<SP_DES_GET_PLM_DOC_OBJECT_FILE_FOR_TRANSFER_TO_IEMQS_FCS_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetPLMDOCObjectFilesforTransfer((int)LocationType.Powai);
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());

                        foreach (SP_DES_GET_PLM_DOC_OBJECT_FILE_FOR_TRANSFER_TO_IEMQS_FCS_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            try
                            {
                                Console.ReadLine();
                                sourcePath = fCSFileTransfer.PLMFolderPath;
                                fileName = fCSFileTransfer.Document_name;
                                targetPath = @"//172.16.7.242/FCSFiles/";

                                string directoryName = string.Empty;
                                directoryName = fCSFileTransfer.Project + "-" + fCSFileTransfer.DocumentNo + "-" + "R" + fCSFileTransfer.DocumentRevision + "/";
                                Console.WriteLine("directoryName : " + directoryName);

                                string[] pahtarray = sourcePath.Split('/'); //sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                string extraFname = pahtarray[pahtarray.Count() - 1];

                                //sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                //Console.WriteLine("sourcePath Path : " + sourcePath);
                                //sourcePath = sourcePath.Replace(FixedSourcePath, PowaiPath);
                                //Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                //fileName = extraFname;
                                fileName = DateTime.UtcNow.Ticks.ToString().Substring(5) + "_" + fileName;
                                Console.WriteLine("File Name : " + fileName);
                                sourcePath = FixedSourcePath + pahtarray[0] + "/" + pahtarray[1];// + "/" + extraFname;
                                Console.WriteLine("New Updted Source Path : " + sourcePath);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, extraFname);
                                Console.WriteLine("sourceFile : " + sourceFile);
                                string extractSourcePath = sourcePath.Substring(PowaiPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += directoryName;
                                //Console.ReadLine();

                                string destFile = System.IO.Path.Combine(targetPath, extraFname); //Step 1
                                Console.WriteLine("Target Path : " + targetPath);
                                string RenamedFile = Path.Combine(targetPath, fileName);
                                Console.WriteLine("destFileh : " + destFile);
                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.

                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.

                                //using (new Tools.Impersonator("Fcsadmin", "LTHED.COM", "Passw0rd"))
                                //{ 
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    //Rename file after transfer.
                                    File.Move(destFile, RenamedFile);
                                    // ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.ROCCommentsAttachId, (int)LocationType.Hazira, "ROC-Attached");

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.JEPCustFeedbackDocumentId), (int)(LocationType.Powai));
                                    //Console.WriteLine("File data has been recorded in Transfer table.");
                                    if (fCSFileTransfer.DocumentType.ToUpper() == "DOC")
                                    {
                                        ObjFCSMangmt.UpdateCentralANDMaindocumentPathFORPLMFiles(RenamedFile, fCSFileTransfer.Id);
                                    }
                                    else if (fCSFileTransfer.DocumentType.ToUpper() == "DCR")
                                    {
                                        //ObjFCSMangmt.UpdateCentralANDMaindocumentPathFORPLMFiles(RenamedFile, fCSFileTransfer.Id);
                                        ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.Id, LocationType.Hazira.GetHashCode(), "DCR");
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }
                                // }
                                Console.ReadLine();


                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Hazira)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    //ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Hazira);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private static void Transfer_PLM_FILES_TO_20_FCS(int ApplicationLocation, string Project)
        {
            string PowaiPLMFCSPath = ConfigurationManager.AppSettings["PowaiPLMFCSPath"];
            string RanoliPLMFCSPath = ConfigurationManager.AppSettings["RanoliPLMFCSPath"];
            string HaziraPLMFCSPath = ConfigurationManager.AppSettings["HaziraPLMFCSPath"];
            NetworkCredential PLMcredentials = new NetworkCredential(@"" + ConfigurationManager.AppSettings["PLMUserName"] + "", ConfigurationManager.AppSettings["PLMPassword"], ConfigurationManager.AppSettings["PLMDomain"]);
            NetworkCredential HaziraFCScredentials = new NetworkCredential(@"" + ConfigurationManager.AppSettings["HaziraFCSUserName"] + "", ConfigurationManager.AppSettings["HaziraFCSPassword"], ConfigurationManager.AppSettings["HaziraFCSDomain"]);
            NetworkCredential PowaiFCScredentials = new NetworkCredential(@"" + ConfigurationManager.AppSettings["PowaiFCSUserName"] + "", ConfigurationManager.AppSettings["PowaiFCSPassword"], ConfigurationManager.AppSettings["PowaiFCSDomain"]);
            NetworkCredential RanoliFCScredentials = new NetworkCredential(@"" + ConfigurationManager.AppSettings["RanoliFCSUserName"] + "", ConfigurationManager.AppSettings["RanoliFCSPassword"], ConfigurationManager.AppSettings["RanoliFCSDomain"]);
            string HaziraFCSPath = ConfigurationManager.AppSettings["HaziraFCSPath"];
            string PowaiFCSPath = ConfigurationManager.AppSettings["PowaiFCSPath"];
            string RanoliFCSPath = ConfigurationManager.AppSettings["RanoliFCSPath"];
            var FCSSourcePath = string.Empty;
            var FCSDestinationPath = string.Empty;

            NetworkCredential FCSDestinationNetworkCredential = null;
            try
            {
                if (ApplicationLocation == LocationType.Powai.GetHashCode())
                {
                    FCSSourcePath = PowaiPLMFCSPath;
                    FCSDestinationPath = PowaiFCSPath;
                    FCSDestinationNetworkCredential = PowaiFCScredentials;
                }
                else if (ApplicationLocation == LocationType.Hazira.GetHashCode())
                {
                    FCSSourcePath = HaziraPLMFCSPath;
                    FCSDestinationPath = HaziraFCSPath;
                    FCSDestinationNetworkCredential = HaziraFCScredentials;
                }
                else if (ApplicationLocation == LocationType.Ranoli.GetHashCode())
                {
                    FCSSourcePath = RanoliPLMFCSPath;
                    FCSDestinationPath = RanoliFCSPath;
                    FCSDestinationNetworkCredential = RanoliFCScredentials;
                }
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    List<SP_DES_GET_PLM_DOC_OBJECT_FILE_FOR_TRANSFER_TO_IEMQS_FCS_Result> customerfeedbackfiles = new List<SP_DES_GET_PLM_DOC_OBJECT_FILE_FOR_TRANSFER_TO_IEMQS_FCS_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetPLMDOCObjectFilesforTransfer(ApplicationLocation).Where(i => i.Project.ToLower() == (Project == "" ? i.Project.ToLower() : Project.ToLower())).ToList();
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());
                        var FileNo = 0;
                        foreach (SP_DES_GET_PLM_DOC_OBJECT_FILE_FOR_TRANSFER_TO_IEMQS_FCS_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            FileNo = FileNo + 1;
                            var sourcePath = fCSFileTransfer.PLMFolderPath;
                            var fileName = fCSFileTransfer.Document_name;

                            var directoryName = string.Empty;
                            if (fCSFileTransfer.DocumentType.ToUpper() == "DOC")
                            {
                                directoryName = fCSFileTransfer.Project + "-" + fCSFileTransfer.DocumentNo + "-" + "R" + fCSFileTransfer.DocumentRevision + "\\";
                            }
                            else if (fCSFileTransfer.DocumentType.ToUpper() == "DCR")
                            {
                                directoryName = fCSFileTransfer.DocumentNo + "-DCRSupportDocument" + "\\";
                            }
                            Console.WriteLine("FileNo : " + FileNo.ToString());
                            Console.WriteLine("directoryName : " + directoryName);

                            string[] pahtarray = sourcePath.Split('/'); //sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                            string extraFname = pahtarray[pahtarray.Count() - 1];
                            Console.WriteLine("File Name : " + extraFname);

                            fileName = DateTime.UtcNow.Ticks.ToString().Substring(5) + "_" + fileName;
                            Console.WriteLine("File Name : " + fileName);

                            sourcePath = FCSSourcePath + "\\" + pahtarray[0] + "\\" + pahtarray[1];// + "/" + extraFname;
                            Console.WriteLine("New Updted Source Path : " + sourcePath);
                            // Use Path class to manipulate file and directory paths.
                            string sourceFile = System.IO.Path.Combine(sourcePath, extraFname);
                            Console.WriteLine("sourceFile : " + sourceFile);


                            Console.WriteLine("Target Path : " + FCSDestinationPath);

                            directoryName = FCSDestinationPath + "\\" + directoryName;

                            string destFile = System.IO.Path.Combine(directoryName, extraFname); //Step 1
                            Console.WriteLine("Target Path : " + FCSDestinationPath);
                            string RenamedFile = Path.Combine(directoryName, fileName);
                            Console.WriteLine("destFileh : " + destFile);
                            Console.WriteLine("Up to Step 1");

                            try
                            {
                                using (new ConnectToSharedFolder(sourcePath, PLMcredentials))
                                {
                                    Console.WriteLine("PLM FCS Connected");
                                    //var fileList = Directory.GetFiles(sourcePath);
                                    using (new ConnectToSharedFolder(FCSDestinationPath, FCSDestinationNetworkCredential))
                                    {
                                        Console.WriteLine("Location FCS Connected");
                                        System.IO.Directory.CreateDirectory(directoryName); //Step 2


                                        Console.WriteLine("Up to Step 2");

                                        Console.WriteLine("File " + fileName + " is for copying.");
                                        System.IO.File.Copy(sourceFile, destFile, true);
                                        Console.WriteLine("File " + fileName + " is copied");

                                        Console.WriteLine("File " + fileName + " has been transfered.");

                                        //Rename file after transfer.
                                        File.Move(destFile, RenamedFile);

                                        Console.WriteLine("File " + fileName + " has been Moved.");

                                        if (fCSFileTransfer.DocumentType.ToUpper() == "DOC")
                                        {
                                            ObjFCSMangmt.UpdateCentralANDMaindocumentPathFORPLMFiles(RenamedFile, fCSFileTransfer.Id);
                                        }
                                        else if (fCSFileTransfer.DocumentType.ToUpper() == "DCR")
                                        {
                                            ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.Id, ApplicationLocation, "DCR");
                                            ObjFCSMangmt.UpdateDCRDocumentPathFORPLMFiles(RenamedFile, fCSFileTransfer.Id);
                                        }
                                        Console.WriteLine("File " + fileName + " Document Path Updated Successfully!");
                                        Console.WriteLine("");
                                        Console.WriteLine("");
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static void Transfer_ROC_Attached_Files_Hazira_To_Central()
        {
            try
            {
                string HaziraPathdb = ConfigurationValues.GetConfigvalues("FCS_HAZIRA_FILE_PATH");
                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = HaziraPathdb;// @"//192.168.27.9/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"H:\FCSFiles\";

                string FixedSourcePath = HaziraPathdb;// @"//192.168.27.9/FCSFiles/";

                string HaziraPath = HaziraPathdb;// @"\\192.168.27.9\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using for transfer ROC file from Hazira to Central...");
                    List<SP_DES_GET_ROC_COMMENTED_ATTACHED_FILE_FOR_TRANSFER_Result> customerfeedbackfiles = new List<SP_DES_GET_ROC_COMMENTED_ATTACHED_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetROC_ATTACHED_FileForTransfer((int)LocationType.Hazira);
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());

                        foreach (SP_DES_GET_ROC_COMMENTED_ATTACHED_FILE_FOR_TRANSFER_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.DocumentPath;
                                fileName = fCSFileTransfer.DocumentTitle;
                                targetPath = @"H:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, HaziraPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(HaziraPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.ROCCommentsAttachId, (int)LocationType.Hazira, "ROC-Attached");

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.JEPCustFeedbackDocumentId), (int)(LocationType.Powai));
                                    Console.WriteLine("File data has been recorded in Transfer table.");

                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Hazira)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Hazira);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Hazira)));
                //Console.ReadLine();
            }
        }

        private static void Transfer_ROC_Files_Hazira_To_Central()
        {
            try
            {
                string HaziraPathdb = ConfigurationValues.GetConfigvalues("FCS_HAZIRA_FILE_PATH");
                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = HaziraPathdb;// @"//192.168.27.9/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"H:\FCSFiles\";

                string FixedSourcePath = HaziraPathdb;// @"//192.168.27.9/FCSFiles/";

                string HaziraPath = HaziraPathdb;// @"\\192.168.27.9\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using for transfer ROC file from Hazira to Central...");
                    List<SP_DES_GET_ROC_COMMENTED_FILE_FOR_TRANSFER_Result> customerfeedbackfiles = new List<SP_DES_GET_ROC_COMMENTED_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetROCFileForTransfer((int)LocationType.Hazira);
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());

                        foreach (SP_DES_GET_ROC_COMMENTED_FILE_FOR_TRANSFER_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.DocumentPath;
                                fileName = fCSFileTransfer.DocumentTitle;
                                targetPath = @"H:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, HaziraPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(HaziraPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.ROCCommentedDocId, (int)LocationType.Hazira, "ROC");

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.JEPCustFeedbackDocumentId), (int)(LocationType.Powai));
                                    Console.WriteLine("File data has been recorded in Transfer table.");

                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Hazira)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Hazira);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Hazira)));
                //Console.ReadLine();
            }
        }

        private static void TransferDCRFiles_Hazira_To_Central()
        {
            try
            {
                string HaziraPathdb = ConfigurationValues.GetConfigvalues("FCS_HAZIRA_FILE_PATH");
                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = HaziraPathdb;// @"//192.168.27.9/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"H:\FCSFiles\";

                string FixedSourcePath = HaziraPathdb;// @"//192.168.27.9/FCSFiles/";

                string HaziraPath = HaziraPathdb;// @"\\192.168.27.9\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using for transfer DCR file from Hazira to Central...");
                    List<SP_DES_GET_DCR_OBJECT_FILE_FOR_TRANSFER_Result> customerfeedbackfiles = new List<SP_DES_GET_DCR_OBJECT_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetDCRFileForTransfer((int)LocationType.Hazira);
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());

                        foreach (SP_DES_GET_DCR_OBJECT_FILE_FOR_TRANSFER_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.DocumentPath;
                                fileName = fCSFileTransfer.DocumentTitle;
                                targetPath = @"H:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, HaziraPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(HaziraPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.DCRSupportedDocId, (int)LocationType.Hazira, "DCR");

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.JEPCustFeedbackDocumentId), (int)(LocationType.Powai));
                                    Console.WriteLine("File data has been recorded in Transfer table.");

                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Hazira)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Hazira);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Hazira)));
                //Console.ReadLine();
            }
        }

        private static void TransferCustomerFeedbackFile_Hazira_To_Central()
        {
            try
            {
                string HaziraPathdb = ConfigurationValues.GetConfigvalues("FCS_HAZIRA_FILE_PATH");
                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = HaziraPathdb;// @"//192.168.27.9/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"H:\FCSFiles\";

                string FixedSourcePath = HaziraPathdb;// @"//192.168.27.9/FCSFiles/";

                string HaziraPath = HaziraPathdb;// @"\\192.168.27.9\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using for transfer Customer feedback file from Hazira to Central...");
                    List<SP_DES_GET_CUSTOMER_FEEDBACK_FILE_FOR_TRANSFER_Result> customerfeedbackfiles = new List<SP_DES_GET_CUSTOMER_FEEDBACK_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetCustomerFeedbackFileForTransfer((int)LocationType.Hazira);
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());

                        foreach (SP_DES_GET_CUSTOMER_FEEDBACK_FILE_FOR_TRANSFER_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.DocumentPath;
                                fileName = fCSFileTransfer.DocumentTitle;
                                targetPath = @"H:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, HaziraPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(HaziraPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.JEPCustFeedbackDocumentId, (int)LocationType.Hazira, "JEP-CustomerFeedback");

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.JEPCustFeedbackDocumentId), (int)(LocationType.Powai));
                                    Console.WriteLine("File data has been recorded in Transfer table.");

                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Hazira)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Hazira);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Hazira)));
                //Console.ReadLine();
            }
        }

        private static void MoveHaziraRecordofROC_Commented_FilesTOTransferTable()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    List<DES072> dES086s = new List<DES072>();
                    dES086s = ObjFCSMangmt.GetROCCommentedFileRecordofHazira((int)LocationType.Hazira);
                    if (dES086s.Count() > 0)
                    {
                        foreach (DES072 dES in dES086s)
                        {
                            ObjFCSMangmt.AddLogForTransfrredFiles(dES.ROCCommentsAttachId, (int)LocationType.Hazira, "ROC-Attached");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        private static void MoveHaziraRecordofROCFilesTOTransferTable()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    List<DES070> dES086s = new List<DES070>();
                    dES086s = ObjFCSMangmt.GetROCFileRecordofHazira((int)LocationType.Hazira);
                    if (dES086s.Count() > 0)
                    {
                        foreach (DES070 dES in dES086s)
                        {
                            ObjFCSMangmt.AddLogForTransfrredFiles(dES.ROCCommentedDocId, (int)LocationType.Hazira, "ROC");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        private static void Transfer_ROC_Attached_Files_Ranoli_To_Hazira()
        {
            try
            {
                string RanoliPsthdb = ConfigurationValues.GetConfigvalues("FCS_RANOLI_FILE_PATH");
                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = RanoliPsthdb;// @"//192.168.27.9/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"H:\FCSFiles\";

                string FixedSourcePath = RanoliPsthdb;// @"//192.168.27.9/FCSFiles/";

                string RanoliPath = RanoliPsthdb;// @"\\192.168.27.9\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using for transfer ROC file from Ranoli to Hazira...");
                    List<SP_DES_GET_ROC_COMMENTED_ATTACHED_FILE_FOR_TRANSFER_Result> customerfeedbackfiles = new List<SP_DES_GET_ROC_COMMENTED_ATTACHED_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetROC_ATTACHED_FileForTransfer((int)LocationType.Ranoli);
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());

                        foreach (SP_DES_GET_ROC_COMMENTED_ATTACHED_FILE_FOR_TRANSFER_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.DocumentPath;
                                fileName = fCSFileTransfer.DocumentTitle;
                                targetPath = @"H:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, RanoliPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(RanoliPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.ROCCommentsAttachId, (int)LocationType.Ranoli, "ROC-Attached");

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.JEPCustFeedbackDocumentId), (int)(LocationType.Powai));
                                    Console.WriteLine("File data has been recorded in Transfer table.");

                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Ranoli)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Ranoli);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Powai)));
                //Console.ReadLine();
            }
        }

        private static void Transfer_ROC_Attached_Files_Powai_To_Hazira()
        {
            try
            {
                string PowaiPathdb = ConfigurationValues.GetConfigvalues("FCS_POWAI_FILE_PATH");

                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = PowaiPathdb;// @"//172.16.7.242/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"H:\FCSFiles\";

                string FixedSourcePath = PowaiPathdb;// @"//172.16.7.242/FCSFiles/";

                string PowaiPath = PowaiPathdb;// @"\\172.16.7.242\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using for transfer ROC Files from Powai to Hazira...");
                    List<SP_DES_GET_ROC_COMMENTED_ATTACHED_FILE_FOR_TRANSFER_Result> customerfeedbackfiles = new List<SP_DES_GET_ROC_COMMENTED_ATTACHED_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetROC_ATTACHED_FileForTransfer((int)LocationType.Powai);
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());

                        foreach (SP_DES_GET_ROC_COMMENTED_ATTACHED_FILE_FOR_TRANSFER_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.DocumentPath;
                                fileName = fCSFileTransfer.DocumentTitle;
                                targetPath = @"H:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, PowaiPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(PowaiPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.ROCCommentsAttachId, (int)LocationType.Powai, "ROC-Attached");

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.JEPCustFeedbackDocumentId), (int)(LocationType.Powai));
                                    Console.WriteLine("File data has been recorded in Transfer table.");

                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Powai)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Powai);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Powai)));
                //Console.ReadLine();
            }
        }

        private static void Transfer_ROC_Files_Ranoli_To_Hazira()
        {
            try
            {
                string RanoliPsthdb = ConfigurationValues.GetConfigvalues("FCS_RANOLI_FILE_PATH");
                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = RanoliPsthdb;// @"//192.168.27.9/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"C:/FCSFiles/";

                string FixedSourcePath = RanoliPsthdb;// @"//192.168.27.9/FCSFiles/";

                string RanoliPath = RanoliPsthdb;// @"\\192.168.27.9\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using for transfer ROC file from Ranoli to Hazira...");
                    List<SP_DES_GET_ROC_COMMENTED_FILE_FOR_TRANSFER_Result> customerfeedbackfiles = new List<SP_DES_GET_ROC_COMMENTED_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetROCFileForTransfer((int)LocationType.Ranoli);
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());

                        foreach (SP_DES_GET_ROC_COMMENTED_FILE_FOR_TRANSFER_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.DocumentPath;
                                fileName = fCSFileTransfer.DocumentTitle;
                                targetPath = @"C:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, RanoliPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(RanoliPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.ROCCommentedDocId, (int)LocationType.Ranoli, "ROC");

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.JEPCustFeedbackDocumentId), (int)(LocationType.Powai));
                                    Console.WriteLine("File data has been recorded in Transfer table.");

                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Ranoli)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Ranoli);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Powai)));
                //Console.ReadLine();
            }
        }

        private static void Transfer_ROC_Files_Powai_To_Hazira()
        {
            try
            {
                string PowaiPathdb = ConfigurationValues.GetConfigvalues("FCS_POWAI_FILE_PATH");

                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = PowaiPathdb;// @"//172.16.7.242/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"C:/FCSFiles/";

                string FixedSourcePath = PowaiPathdb;// @"//172.16.7.242/FCSFiles/";

                string PowaiPath = PowaiPathdb;// @"\\172.16.7.242\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using for transfer ROC Files from Powai to Hazira...");
                    List<SP_DES_GET_ROC_COMMENTED_FILE_FOR_TRANSFER_Result> customerfeedbackfiles = new List<SP_DES_GET_ROC_COMMENTED_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetROCFileForTransfer((int)LocationType.Powai);
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());

                        foreach (SP_DES_GET_ROC_COMMENTED_FILE_FOR_TRANSFER_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.DocumentPath;
                                fileName = fCSFileTransfer.DocumentTitle;
                                targetPath = @"C:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, PowaiPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(PowaiPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.ROCCommentedDocId, (int)LocationType.Powai, "ROC");

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.JEPCustFeedbackDocumentId), (int)(LocationType.Powai));
                                    Console.WriteLine("File data has been recorded in Transfer table.");

                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Powai)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Powai);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Powai)));
                //Console.ReadLine();
            }
        }

        private static void MoveHaziraRecordofCustomerFeedbacktoTransferTable()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    List<DES086> dES086s = new List<DES086>();
                    dES086s = ObjFCSMangmt.GetCustomerFeedbackRecordofHazira((int)LocationType.Hazira);
                    if (dES086s.Count() > 0)
                    {
                        foreach (DES086 dES in dES086s)
                        {
                            ObjFCSMangmt.AddLogForTransfrredFiles(dES.JEPCustFeedbackDocumentId, (int)LocationType.Hazira, "JEP-CustomerFeedback");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }

        private static void MoveHaziraRecordofDCRFilestoTransferTable()
        {
            try
            {
                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    List<DES091> dES086s = new List<DES091>();
                    dES086s = ObjFCSMangmt.GetDCRRecordofHazira((int)LocationType.Hazira);
                    if (dES086s.Count() > 0)
                    {
                        foreach (DES091 dES in dES086s)
                        {
                            ObjFCSMangmt.AddLogForTransfrredFiles(dES.DCRSupportedDocId, (int)LocationType.Hazira, "DCR");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;

            }
        }
        private static void TransferDCRFiles_Ranoli_To_Hazira()
        {
            try
            {
                string RanoliPsthdb = ConfigurationValues.GetConfigvalues("FCS_RANOLI_FILE_PATH");
                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = RanoliPsthdb;// @"//192.168.27.9/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"H:\FCSFiles\";

                string FixedSourcePath = RanoliPsthdb;// @"//192.168.27.9/FCSFiles/";

                string RanoliPath = RanoliPsthdb;// @"\\192.168.27.9\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using for transfer DCR file from Ranoli to Hazira...");
                    List<SP_DES_GET_DCR_OBJECT_FILE_FOR_TRANSFER_Result> customerfeedbackfiles = new List<SP_DES_GET_DCR_OBJECT_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetDCRFileForTransfer((int)LocationType.Ranoli);
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());

                        foreach (SP_DES_GET_DCR_OBJECT_FILE_FOR_TRANSFER_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.DocumentPath;
                                fileName = fCSFileTransfer.DocumentTitle;
                                targetPath = @"H:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, RanoliPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(RanoliPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.DCRSupportedDocId, (int)LocationType.Ranoli, "DCR");

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.JEPCustFeedbackDocumentId), (int)(LocationType.Powai));
                                    Console.WriteLine("File data has been recorded in Transfer table.");

                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Ranoli)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Ranoli);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Powai)));
                //Console.ReadLine();
            }
        }

        private static void TransferDCRFiles_Powai_To_Hazira()
        {
            try
            {
                string PowaiPathdb = ConfigurationValues.GetConfigvalues("FCS_POWAI_FILE_PATH");

                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = PowaiPathdb;// @"//172.16.7.242/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"H:/FCSFiles/";

                string FixedSourcePath = PowaiPathdb;// @"//172.16.7.242/FCSFiles/";

                string PowaiPath = PowaiPathdb;// @"\\172.16.7.242\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using for transfer DCR from Powai to Hazira...");
                    List<SP_DES_GET_DCR_OBJECT_FILE_FOR_TRANSFER_Result> customerfeedbackfiles = new List<SP_DES_GET_DCR_OBJECT_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetDCRFileForTransfer((int)LocationType.Powai);
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());

                        foreach (SP_DES_GET_DCR_OBJECT_FILE_FOR_TRANSFER_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.DocumentPath;
                                fileName = fCSFileTransfer.DocumentTitle;
                                targetPath = @"H:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, PowaiPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(PowaiPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.DCRSupportedDocId, (int)LocationType.Powai, "DCR");

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.JEPCustFeedbackDocumentId), (int)(LocationType.Powai));
                                    Console.WriteLine("File data has been recorded in Transfer table.");

                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Powai)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Powai);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Powai)));
                //Console.ReadLine();
            }
        }

        private static void TransferCustomerFeedbackFile_Powai_To_Hazira()
        {
            try
            {
                string PowaiPathdb = ConfigurationValues.GetConfigvalues("FCS_POWAI_FILE_PATH");

                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = PowaiPathdb;// @"//172.16.7.242/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"H:\FCSFiles\";

                string FixedSourcePath = PowaiPathdb;// @"//172.16.7.242/FCSFiles/";

                string PowaiPath = PowaiPathdb;// @"\\172.16.7.242\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using for transfer Customer feedback file from Powai to Hazira...");
                    List<SP_DES_GET_CUSTOMER_FEEDBACK_FILE_FOR_TRANSFER_Result> customerfeedbackfiles = new List<SP_DES_GET_CUSTOMER_FEEDBACK_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetCustomerFeedbackFileForTransfer((int)LocationType.Powai);
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());

                        foreach (SP_DES_GET_CUSTOMER_FEEDBACK_FILE_FOR_TRANSFER_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.DocumentPath;
                                fileName = fCSFileTransfer.DocumentTitle;
                                targetPath = @"H:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, PowaiPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(PowaiPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.JEPCustFeedbackDocumentId, (int)LocationType.Powai, "JEP-CustomerFeedback");

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.JEPCustFeedbackDocumentId), (int)(LocationType.Powai));
                                    Console.WriteLine("File data has been recorded in Transfer table.");

                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Hazira)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Powai);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Powai)));
                //Console.ReadLine();
            }
        }

        private static void Transfer_DOC_Object_FIle_POWAI_To_Central()
        {
            try
            {

                string PowaiPathdb = ConfigurationValues.GetConfigvalues("FCS_POWAI_FILE_PATH");


                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = PowaiPathdb; //@"//172.16.7.242/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"H:\FCSFiles\";

                string FixedSourcePath = PowaiPathdb;// @"//172.16.7.242/FCSFiles/";

                string PowaiPath = PowaiPathdb;// @"\\172.16.7.242\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using...");
                    List<SP_DES_GET_DOC_OBJECT_FILE_FOR_TRANSFER_Result> listDES060s = new List<SP_DES_GET_DOC_OBJECT_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    listDES060s = ObjFCSMangmt.GetDOCObjectFileForTransfer((int)(LocationType.Powai));
                    Console.WriteLine("After db operation get...");
                    if (listDES060s.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + listDES060s.Count());

                        foreach (SP_DES_GET_DOC_OBJECT_FILE_FOR_TRANSFER_Result fCSFileTransfer in listDES060s)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.MainDocumentPath;
                                fileName = fCSFileTransfer.Document_name;
                                targetPath = @"H:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, PowaiPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(PowaiPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.UpdateCentralFlag(fCSFileTransfer.DocumentMappingId, (int)(LocationType.Powai));

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.DocumentMappingId), (int)(LocationType.Powai));

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.DocumentMappingId, (int)LocationType.Powai, "DOC");
                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Powai)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Powai);
                }


                //// To copy all the files in one directory to another directory.
                //// Get the files in the source folder. (To recursively iterate through
                //// all subfolders under the current directory, see
                //// "How to: Iterate Through a Directory Tree.")
                //// Note: Check for target path was performed previously
                ////       in this code example.
                //if (System.IO.Directory.Exists(sourcePath))
                //{
                //    string[] files = System.IO.Directory.GetFiles(sourcePath);

                //    // Copy the files and overwrite destination files if they already exist.
                //    foreach (string s in files)
                //    {
                //        // Use static Path methods to extract only the file name from the path.
                //        fileName = System.IO.Path.GetFileName(s);
                //        destFile = System.IO.Path.Combine(targetPath, fileName);
                //        System.IO.File.Copy(s, destFile, true);
                //    }
                //}
                //else
                //{
                //    Console.WriteLine("Source path does not exist!");
                //}

                // Keep console window open in debug mode.

                //Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Powai)));
                //Console.ReadLine();
            }
            //Console.ReadLine();

        }

        private static void Transfer_DOC_Object_FIle_RANOLI_To_Central()
        {
            try
            {
                string RanoliPsthdb = ConfigurationValues.GetConfigvalues("FCS_RANOLI_FILE_PATH");


                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = RanoliPsthdb; //@"//172.16.7.242/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"H:\FCSFiles\";

                string FixedSourcePath = RanoliPsthdb;// @"//172.16.7.242/FCSFiles/";

                string PowaiPath = RanoliPsthdb;// @"\\172.16.7.242\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using...");
                    List<SP_DES_GET_DOC_OBJECT_FILE_FOR_TRANSFER_Result> listDES060s = new List<SP_DES_GET_DOC_OBJECT_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    listDES060s = ObjFCSMangmt.GetDOCObjectFileForTransfer((int)(LocationType.Ranoli));
                    Console.WriteLine("After db operation get...");
                    if (listDES060s.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + listDES060s.Count());

                        foreach (SP_DES_GET_DOC_OBJECT_FILE_FOR_TRANSFER_Result fCSFileTransfer in listDES060s)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.MainDocumentPath;
                                fileName = fCSFileTransfer.Document_name;
                                targetPath = @"H:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, PowaiPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(PowaiPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.UpdateCentralFlag(fCSFileTransfer.DocumentMappingId, (int)(LocationType.Ranoli));

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.DocumentMappingId), (int)(LocationType.Powai));

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.DocumentMappingId, (int)LocationType.Ranoli, "DOC");
                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Ranoli)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Ranoli);
                }


                //// To copy all the files in one directory to another directory.
                //// Get the files in the source folder. (To recursively iterate through
                //// all subfolders under the current directory, see
                //// "How to: Iterate Through a Directory Tree.")
                //// Note: Check for target path was performed previously
                ////       in this code example.
                //if (System.IO.Directory.Exists(sourcePath))
                //{
                //    string[] files = System.IO.Directory.GetFiles(sourcePath);

                //    // Copy the files and overwrite destination files if they already exist.
                //    foreach (string s in files)
                //    {
                //        // Use static Path methods to extract only the file name from the path.
                //        fileName = System.IO.Path.GetFileName(s);
                //        destFile = System.IO.Path.Combine(targetPath, fileName);
                //        System.IO.File.Copy(s, destFile, true);
                //    }
                //}
                //else
                //{
                //    Console.WriteLine("Source path does not exist!");
                //}

                // Keep console window open in debug mode.

                //Console.ReadKey();
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Ranoli)));
                //Console.ReadLine();
            }

        }

        private static void TransferCustomerFeedbackFile_Ranoli_To_Hazira()
        {
            try
            {
                string RanoliPsthdb = ConfigurationValues.GetConfigvalues("FCS_RANOLI_FILE_PATH");
                string fileName = "AquaTrols_Test_log_old.ldf";
                //string sourcePath = @"\\10.1.1.9\Developers\Mitul";
                string sourcePath = RanoliPsthdb;// @"//192.168.27.9/FCSFiles/";
                //string targetPath = @"\\10.1.1.9\Developers\JatinTest";
                string targetPath = @"H:\FCSFiles\";

                string FixedSourcePath = RanoliPsthdb;// @"//192.168.27.9/FCSFiles/";

                string RanoliPath = RanoliPsthdb;// @"\\192.168.27.9\FCSFiles\";

                using (IEMQSDESEntities dbContext = new IEMQSDESEntities())
                {
                    Console.WriteLine("Inside Using for transfer Customer feedback file from Ranoli to Hazira...");
                    List<SP_DES_GET_CUSTOMER_FEEDBACK_FILE_FOR_TRANSFER_Result> customerfeedbackfiles = new List<SP_DES_GET_CUSTOMER_FEEDBACK_FILE_FOR_TRANSFER_Result>();
                    FCSFileTransferManagementService ObjFCSMangmt = new FCSFileTransferManagementService();
                    customerfeedbackfiles = ObjFCSMangmt.GetCustomerFeedbackFileForTransfer((int)LocationType.Ranoli);
                    Console.WriteLine("After db operation get...");
                    if (customerfeedbackfiles.Count > 0)
                    {
                        Console.WriteLine("Total Files Found : " + customerfeedbackfiles.Count());

                        foreach (SP_DES_GET_CUSTOMER_FEEDBACK_FILE_FOR_TRANSFER_Result fCSFileTransfer in customerfeedbackfiles)
                        {
                            try
                            {
                                sourcePath = fCSFileTransfer.DocumentPath;
                                fileName = fCSFileTransfer.DocumentTitle;
                                targetPath = @"H:\FCSFiles\";

                                string extraFname = sourcePath.Substring((sourcePath.Remove(sourcePath.Length - fileName.Length)).Length - 14);

                                sourcePath = sourcePath.Remove(sourcePath.Length - ((fileName.Length) + 14));
                                Console.WriteLine("sourcePath Path : " + sourcePath);
                                sourcePath = sourcePath.Replace(FixedSourcePath, RanoliPath);
                                Console.WriteLine("New Updted Source Path : " + sourcePath);

                                //Console.ReadLine();


                                Console.WriteLine("File Name : " + extraFname);
                                fileName = extraFname;
                                Console.WriteLine("File Name : " + fileName);
                                // Use Path class to manipulate file and directory paths.
                                string sourceFile = System.IO.Path.Combine(sourcePath, fileName);
                                string extractSourcePath = sourcePath.Substring(RanoliPath.Length);

                                Console.WriteLine("extractSourcePath Path : " + extractSourcePath);
                                //Console.ReadLine();
                                //extractSourcePath = extractSourcePath.Remove(2);
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();
                                //targetPath = System.IO.Path.Combine(targetPath, extractSourcePath);
                                targetPath += extractSourcePath;
                                //Console.ReadLine();
                                string destFile = System.IO.Path.Combine(targetPath, fileName); //Step 1

                                Console.WriteLine("Up to Step 1");
                                //Console.ReadLine();
                                // To copy a folder's contents to a new location:
                                // Create a new target folder. 
                                // If the directory already exists, this method does not create a new directory.
                                Console.WriteLine("Target Path : " + targetPath);
                                //Console.ReadLine();


                                // To copy a file to another location and 
                                // overwrite the destination file if it already exists.
                                if (System.IO.Directory.Exists(sourcePath))
                                {
                                    System.IO.Directory.CreateDirectory(targetPath); //Step 2


                                    Console.WriteLine("Up to Step 2");

                                    Console.WriteLine("File " + fileName + " is for copying.");
                                    System.IO.File.Copy(sourceFile, destFile, true);
                                    Console.WriteLine("File " + fileName + " is copied");

                                    Console.WriteLine("File " + fileName + " has been transfered.");

                                    ObjFCSMangmt.AddLogForTransfrredFiles(fCSFileTransfer.JEPCustFeedbackDocumentId, (int)LocationType.Ranoli, "JEP-CustomerFeedback");

                                    //ObjFCSMangmt.AddTransferFileLog(GetTranferFiledata(fileName, sourcePath, targetPath, fCSFileTransfer.JEPCustFeedbackDocumentId), (int)(LocationType.Powai));
                                    Console.WriteLine("File data has been recorded in Transfer table.");

                                }
                                else
                                {
                                    Console.WriteLine("Source path does not exist:: " + sourcePath);
                                    throw new FileNotFoundException("Source path does not exist:: " + sourcePath);
                                }



                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine("There is Error on file transfer : " + fileName + "error is : " + ex.Message.ToString());
                                ObjFCSMangmt.AddExceptionLogs(GetExceptionData(ex, "From Inside forloop for file name : " + fileName, (int)(LocationType.Ranoli)));
                                continue;
                            }
                            //Console.ReadLine();
                        }



                    }
                    else
                    {
                        Console.WriteLine("No file founds for transfer.");
                    }

                    ObjFCSMangmt.UpdateFCSSchedularRunDate(DateTime.Now, (int)LocationType.Ranoli);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("There is Error : " + ex.Message.ToString());
                new FCSFileTransferManagementService().AddExceptionLogs(GetExceptionData(ex, "From outSide of forloop", (int)(LocationType.Ranoli)));
                //Console.ReadLine();
            }
        }
        private static DES088 GetExceptionData(Exception ex, string sourcePoint, int location)
        {
            try
            {
                DES088 ObjdES088 = new DES088()
                {
                    ErrorMessage = ex.Message,
                    SourcePoint = sourcePoint,
                    CreatedOn = DateTime.Now,
                    IsDeleted = false,
                    Location = location
                };
                return ObjdES088;
            }
            catch (Exception exe)
            {
                throw exe;
            }
        }

        private static DES078 GetTranferFiledata(string transferFilename, string sourcePlace, string destinationPlace, long documentMappingId)
        {
            try
            {
                DES078 dES078 = new DES078()
                {
                    TransfredFilename = transferFilename,
                    SourcePlace = sourcePlace,
                    DestinationPlace = destinationPlace,
                    DocumentMappingId = documentMappingId,
                    isDeleted = false,
                    Createdon = DateTime.Now,
                    CreatedBy = 1,
                    EditedBy = 1,
                    EditedOn = DateTime.Now
                };

                return dES078;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }

    public class ConnectToSharedFolder : IDisposable
    {
        readonly string _networkName;

        public ConnectToSharedFolder(string networkName, NetworkCredential credentials)
        {
            _networkName = networkName;

            var netResource = new NetResource
            {
                Scope = ResourceScope.GlobalNetwork,
                ResourceType = ResourceType.Disk,
                DisplayType = ResourceDisplaytype.Share,
                RemoteName = networkName
            };

            var userName = string.IsNullOrEmpty(credentials.Domain)
                ? credentials.UserName
                : string.Format(@"{0}\{1}", credentials.Domain, credentials.UserName);

            var result = WNetAddConnection2(
                netResource,
                credentials.Password,
                userName,
                0);

            if (result != 0)
            {
                throw new Win32Exception(result, "Error connecting to remote share : Error No - " + result);
            }
        }

        ~ConnectToSharedFolder()
        {
            Dispose(false);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            WNetCancelConnection2(_networkName, 0, true);
        }

        [DllImport("mpr.dll")]
        private static extern int WNetAddConnection2(NetResource netResource,
            string password, string username, int flags);

        [DllImport("mpr.dll")]
        private static extern int WNetCancelConnection2(string name, int flags,
            bool force);

        [StructLayout(LayoutKind.Sequential)]
        public class NetResource
        {
            public ResourceScope Scope;
            public ResourceType ResourceType;
            public ResourceDisplaytype DisplayType;
            public int Usage;
            public string LocalName;
            public string RemoteName;
            public string Comment;
            public string Provider;
        }

        public enum ResourceScope : int
        {
            Connected = 1,
            GlobalNetwork,
            Remembered,
            Recent,
            Context
        };

        public enum ResourceType : int
        {
            Any = 0,
            Disk = 1,
            Print = 2,
            Reserved = 8,
        }

        public enum ResourceDisplaytype : int
        {
            Generic = 0x0,
            Domain = 0x01,
            Server = 0x02,
            Share = 0x03,
            File = 0x04,
            Group = 0x05,
            Network = 0x06,
            Root = 0x07,
            Shareadmin = 0x08,
            Directory = 0x09,
            Tree = 0x0a,
            Ndscontainer = 0x0b
        }
    }
}
