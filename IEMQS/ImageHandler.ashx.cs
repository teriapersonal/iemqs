﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;

namespace IEMQS
{
    /// <summary>
    /// Summary description for ImageHandler
    /// </summary>
    public class ImageHandler : IHttpHandler
    {
        string conn = ConfigurationManager.ConnectionStrings["phzdvsqlCon"].ToString();
        public void ProcessRequest(HttpContext context)
        {
            try
            {
                string type = context.Request.QueryString["type"] != null ? context.Request.QueryString["type"].ToString() : clsImplementationEnum.ImageType.UserProfile.GetStringValue();
                if (type == clsImplementationEnum.ImageType.UserProfile.ToString())
                {
                    string psno = context.Request.QueryString["psno"].ToString();
                    SqlConnection objConn = new SqlConnection(conn);
                    objConn.Open();
                    string sTSQL = "select ImageData from Images where PSNo=@psno";
                    SqlCommand objCmd = new SqlCommand(sTSQL, objConn);
                    objCmd.CommandType = CommandType.Text;
                    objCmd.Parameters.AddWithValue("@psno", psno.ToString());
                    object data = objCmd.ExecuteScalar();
                    if (data != null)
                    {
                        objConn.Close();
                        objCmd.Dispose();
                        context.Response.BinaryWrite((byte[])data);
                    }
                    else
                    {
                        string path = HttpContext.Current.Server.MapPath("~/Images/noimage.png");
                        byte[] photo = File.ReadAllBytes(path);
                        context.Response.BinaryWrite(photo);
                    }
                }
                else if (type == clsImplementationEnum.ImageType.SharePoint.ToString())
                {
                    string folderPath = context.Request.QueryString["fp"].ToString();
                    var Files = (new clsFileUpload()).GetDocuments(folderPath);
                    var fileList = Files.FirstOrDefault();
                    if (fileList != null)
                    {
                        string FilePath = fileList.URL.Substring(fileList.URL.IndexOf(folderPath));
                        Stream responseStream = (new clsFileUpload()).GetFileStream(folderPath, fileList.Name);
                        var bytes = (new clsFileUpload()).GetByteFromStream(responseStream);
                        context.Response.BinaryWrite(bytes);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                string path = HttpContext.Current.Server.MapPath("~/Images/noimage.png");
                byte[] photo = File.ReadAllBytes(path);
                context.Response.BinaryWrite(photo);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}