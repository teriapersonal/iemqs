﻿var _actions = {
    Undo: 'Undo',
    Actions: 'Actions',
    GeneratePCR: 'GeneratePCR',
    RetractPCR: 'RetractPCR',
    BulkUpdate: 'BulkUpdate',
    PCRDetail: 'PCRDetail',
    FetchData: 'FetchData'
};

function fnOnActionFetchData(caller) {
    var result = { Continue: false, Data: null };
    return fnGetAllViewColumnValues(_actionType.CustomToolbarAction)
        .then(
            values => {
                result.Continue = true;
                result.Data = values;
                result.ActionType = _actionType.Reload;
                var parentSelector = $$('.' + _classes.TopSection);
                fnHandle({ ParentSelector: parentSelector });
                fnSetAll(values);
                return result;
            },
            err => {                
                if (err != null) {
                    fnHandleAll(err);
                }
                result.Data = err;
                return result;
            }
        )
        .catch(err => {
            console.log('caught ' + err);
        });
}

function fnSetAll(items) {
    var sessionId = fnGetActiveSessionId();    
    for (var cnt = 0; cnt < items.length; cnt++) {
        fnSetInState(_stateTypes.Clean, _visibleProcessName, sessionId, items[cnt].SectionName, [items[cnt].Name], [items[cnt].Value]);        
    }
    fnSetNavigateProperty(_navigateStates.Current);
}


function fnIsActionAllowed(toolbarItem, toolbarChildItem = undefined, viewType = undefined) {
    if (viewType === undefined || viewType === null || viewType === '') {
        var sessionId = fnGetActiveSessionId();
        viewType = fnGetPropertyValue(_visibleProcessName, sessionId, _propertyNames.ViewType);
    }
    var toolbarItemName = $(toolbarItem).attr(_attributes.Name);
    var isAllowed = true;
    var toolbarChildItemName = toolbarChildItem !== null && toolbarChildItem !== undefined ? toolbarChildItem.Name : null;
    switch (toolbarItemName) {
        case _actions.Actions: {
            switch (toolbarChildItemName) {
                case _actions.GeneratePCR: {
                    // TODO find if selected rows in table are all having null PCR value
                    isAllowed = false;
                    break;
                }
                case _actions.RetractPCR: {
                    isAllowed = false;
                    // TODO find if selected rows in table are all having not null PCR value
                    break;
                }
                case _actions.FetchData: {
                    if (viewType === _viewTypes.New) {
                        isAllowed = $$('div.' + _classes.TopSection).find('.' + _classes.InvalidEditableItem).length > 0 ? false : true;
                        if (isAllowed === true) {
                            var viewColumns = $$('div.' + _classes.TopSection).find('.' + _classes.TextInputField);
                            if (viewColumns.length > 0) {
                                for (var cnt = 0; cnt < viewColumns.length; cnt++) {
                                    var viewColumn = viewColumns[cnt];
                                    if (viewColumn.value === null || viewColumn.value === '') {
                                        isAllowed = false; break;
                                    }
                                }
                            }
                        }
                    }
                    break;
                }
                case _actions.PCRDetail: {
                    isAllowed = false;
                    // TODO find if any rows in table is selected
                    break;
                }
                case _actions.BulkUpdate: {
                    isAllowed = false;
                    // TODO find if any rows in table is selected
                    break;
                }
            }
            break;
        }
    }
    return isAllowed;
}

function fnOnNewView(caller) {
    var eventName = _viewTypes.New;
    var topSection = fnGetTopSectionSelector();
    var toolbarButtonSelector = $$('.' + _classes.Toolbar).find('div.' + _classes.Button + '[' + _attributes.Name + '!=' + '"' + _actions.Undo + '"]')
        .not('.' + _classes.SystemButton)
        .not('[' + _attributes.Name + '=' + '"' + _actions.Actions + '"]');
    var imgMandatorySelector = $$(topSection).find('img.' + _classes.Mandatory);
    var triggerInputFieldSelector = $$(topSection).find('div.' + _classes.TriggerInputField + '.' + _classes.TriggerInputFieldDisabled);
    
    var inputSelector = $$(topSection).find('input.' + _classes.TextInputField + '.' + _classes.TextInputFieldReadonly);
    
    var buttonSelector = $$(topSection).find('div.' + _classes.TriggerButton + '.' + _classes.ButtonDisabled);
    var tableSelector = $$(_selectors.Table).DataTable();
    
    fnAddEventActions(1, toolbarButtonSelector, [_classes.ButtonDisabled], [_classes.ButtonDisabled], 1, eventName);
    fnAddEventActions(2, imgMandatorySelector, [_classes.Hide], [_classes.Hide], 2, eventName);
    fnAddEventActions(2, triggerInputFieldSelector, [_classes.TriggerInputFieldDisabled], [_classes.TriggerInputFieldDisabled], 3, eventName);
    fnAddEventActions(2, inputSelector, [_classes.TextInputFieldReadonly], [_classes.TextInputFieldReadonly], 4, eventName);
    fnAddEventActions(4, inputSelector, [_attributes.Disabled], [_attributes.Disabled, _attributes.Disabled], 5, eventName);
    fnAddEventActions(4, inputSelector, [_attributes.Readonly], [_attributes.Readonly, _attributes.Readonly], 6, eventName);
    fnAddEventActions(2, buttonSelector, [_classes.ButtonDisabled], [_classes.ButtonDisabled], 7, eventName);
    
    inputSelector.val('');
    inputSelector.trigger('change');
    tableSelector
        .clear()
        .draw();
    var sessionId = fnGetActiveSessionId();
    fnSetPropertyValue(_visibleProcessName, sessionId, _propertyNames.ViewType, eventName);
    return Promise.resolve(null);
}



/*
    { Id: 1, Name: 'addClass', Selector: 'Jquery', Parameters: 1, Type: 'function', RevertId: 2 },
    { Id: 2, Name: 'removeClass', Selector: 'jquery', Parameters: 1, Type: 'function', RevertId: 1 },
    { Id: 3, Name: 'setAttribute', Selector: 'js', Parameters: 2, Type: 'function', RevertId: 4 },
    { Id: 4, Name: 'removeAttribute', Selector: 'js', Parameters: 1, Type: 'function', RevertId: 3 },
    { Id: 5, Name: ['style', 'display'], Selector: 'js', Parameters: 1, Type: 'property', RevertId: 0, RevertedValue: '' }
 */