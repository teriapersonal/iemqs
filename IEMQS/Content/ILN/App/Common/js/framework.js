﻿
function fnConstructAction(id, selector, values) {
    var obj = null;
    for (var cnt = 0; cnt < _revertPossibleActions.length; cnt++) {
        if (_revertPossibleActions[cnt].Id === id) {
            obj = _revertPossibleActions[cnt];
            break;
        }
    }
    if (obj != null) {        
        fnExecute(selector, obj, values);        
    }
}

function fnAddEventActions(id, selector, values, revertedValues, order, eventName) {
    var eventObject = new EventActionObject(id, selector, values, revertedValues, order, eventName);
    var sessionId = fnGetActiveSessionId();
    var sessionProperties = fnGetActiveSessionProperties(_visibleProcessName, sessionId);
    var arrEventActions = sessionProperties[_propertyNames.EventActionsArray];
    if (arrEventActions != null && arrEventActions.length > 0) {
        for (var cnt = 0; cnt < arrEventActions.length; cnt++) {
            var obj = arrEventActions[cnt];
            if (obj[eventName] != null) {
                obj[eventName].push(eventObject);
            }
        }
    } else {
        var arr = [];
        arr.push(eventObject);
        var obj = {};
        obj[eventName] = arr;
        arrEventActions.push(obj);
        
    }
    sessionProperties[_propertyNames.EventActionsArray] = arrEventActions;
    fnConstructAction(id, selector, values);
}

function fnRevertLastAction() {
    fnSetNavigateProperty(_navigateStates.Current);
    return  Promise.resolve(
        {
            Continue: true,
            Data: null,
            Error: null
        }
    );
}

function fnExecute(selector, obj, values) {
    var isJs = obj.Selector.toLowerCase() === 'js';
    var elements = $$(selector);
    switch (obj.Type.toLowerCase()) {
        case 'function': {
            var name = obj.Name;
            var fn = (el, va) => {
                if (va.length == 2) {
                    el[name](va[0], va[1]);
                }
                else if (va.length == 1) {
                    el[name](va[0]);
                }
            }
            if (isJs === true) {
                for (var cnt = 0; cnt < elements.length; cnt++) {
                    var element = elements[cnt];
                    fn(element, values);
                }
            } else {
                fn(elements, values);
            }
           
            break;
        }
        case 'property': { // worst scenario can be style.position.left //3 [style,position,left]
            var name = obj.Name;
            var value = values[0];
            var fn = (el, va) => {
                if (name.length == 4) {
                    el[name[0]][name[1]][name[2]][name[3]] = va;
                }
                else if (name.length == 3) {
                    el[name[0]][name[1]][name[2]] = va;
                }
                else if (name.length == 2) {
                    el[name[0]][name[1]] = va;
                }
                else if (name.length == 1) {
                    el[name[0]] = va;
                }
            }
            if (isJs === true) {
                for (var cnt = 0; cnt < elements.length; cnt++) {
                    var element = elements[cnt];
                    fn(element, value);
                }
            } else {
                fn(elements, value);
            }
            
            break;
        }
    }
}

function fnOnToolbarButtonClick(event, caller, itemType) {
    console.log('i', itemType);
    if ($(caller).hasClass(_classes.ButtonDisabled) === false) {
        switch (itemType.toLowerCase()) {
            case _toolbarItemType.Button: {
                var toolbarAction = $(caller).attr(_attributes.Action);
                fnDispatchToolbarClick(toolbarAction, event, caller);
                break;
            }
            case _toolbarItemType.SplitButton: {
                fnOpenSplitMenu(event, caller);
                break;
            }
            case _toolbarItemType.MenuButton: {
                fnOpenMenu(event, caller);
                break;
            }
        }
    } else {
        fnActionNotAllowed();
    }      
}

function fnOpenSplitMenu(event, caller) {    
    var element = $(caller).parent().find('div')[0];
    fnCall(element, -10, event);
}

function fnOpenMenu(element, caller) {    
    var element = caller;
    fnCall(element, -5, event);    
}

function fnCall(element, topPosition, event) {    
    var attrValue = $(element).attr(_attributes.ChildMenuItems);
    var offsetPosition = $(element).offset();
    var sessionId = fnGetActiveSessionId();
    var viewType = fnGetPropertyValue(_visibleProcessName, sessionId, _propertyNames.ViewType);
    fnOpenPopup(element, viewType, attrValue, topPosition, offsetPosition.left);      
    event.stopPropagation();
}

function fnOpenPopup(caller, viewType, childMenuItems, topPosition, leftPosition) {    
    if (childMenuItems != null) {
        var target = $(_selectors.AnyMenu);
        target.css({
            top: topPosition,
            left: leftPosition
        });
        var arr = fnGetJSONParsedData(childMenuItems);
        
        if (arr != null && arr.length > 0) {           
            var div =
                $("<div/>")
                    .addClass(_classes.PopupContent);
            var tbody = $("<tbody/>");            
            for (var cnt = 0; cnt < arr.length; cnt++) {                
                var action = arr[cnt].Action;
                tr =
                    $("<tr/>")
                        .attr(_attributes.Action, action == null ? '' : JSON.stringify(action))
                        .addClass(_classes.MenuItem);
                if (action != null) {
                    var isAllowed = fnIsActionAllowed(caller, arr[cnt], viewType);
                    if (isAllowed === false) {
                        tr
                            .addClass(_classes.DisabledMenuItem);
                    }
                   tr
                    .off("click")
                    .on("click", function (event) {
                        var that = this;
                        if (isAllowed === true) {
                            fnDispatchToolbarClick($(that).attr(_attributes.Action), event, $(that));
                        }
                        else {
                            fnActionNotAllowed();
                        }
                    });
                }
                
                var lbl =
                    $("<label/>")
                        .addClass(_classes.LnLabel)
                        .text(arr[cnt].DisplayText);
                
                var td =
                    $("<td/>")
                        .append(lbl);
                tr
                    .append("<td/>")
                    .append(td)                
                    .appendTo(tbody);
            }
            $("<table/>")
                .append(tbody)
                .appendTo(div);
            
            target
                .html('')
                .append(div)
                .show();
        }       
    }
}

function fnConfigureCloseMenu() {
    $(document).on("click", function (event) {
        var element = $(event.srcElement);
        if (element.parents('.' + _classes.AnyMenu).length == 0) {
            $('.' + _classes.AnyMenu).hide();
        }
    })
}

function fnDispatchToolbarClick(toolbarAction, event, caller) {
    if (toolbarAction !== null && toolbarAction !== '') {
        toolbarAction = JSON.parse(toolbarAction);
        if (toolbarAction !== null) {
            var actualAction = toolbarAction.ActualValue;
            switch (actualAction.Type) {
                case 0: { // JS Only                    
                    functionNames = actualAction.FunctionNames;
                    //JS function used to print etc
                    window[functionNames[0]](caller)
                        .then(
                            fnResult =>
                            {
                                if (fnResult !== null)
                                    console.log(fnResult);
                            },
                            err => {
                                console.log(err);
                            }
                        );
                    break;
                }
                case 1: // JS and Server
                case 2: // Server
                {
                    switch (actualAction.ServerActionType) {
                        case 0: // Common Toolbar action
                            {
                                switch (actualAction.Type) {
                                    case 1: { //JS and Server
                                        functionNames = actualAction.FunctionNames;                                        
                                        window[functionNames[0]](caller)
                                            .then(
                                                fnResult => {
                                                    if (fnResult !== null && fnResult.Continue === true) {
                                                        console.log('ok');
                                                        fnDispatchToolbarServerAction(fnResult.Data, functionNames[1])
                                                            .then(
                                                                result => {
                                                                    showLoading(false);
                                                                    if (result.Continue === true) {
                                                                        fnAfterToolBarAction(toolbarAction.PostValue, fnResult.ActionType);
                                                                    } else {
                                                                        fnAlert(result.Message, _messageType.Error);
                                                                    }
                                                                });
                                                    } else {
                                                        console.log('not ok');
                                                    }                                                   
                                                },
                                                err => {
                                                    console.log(err);
                                                }
                                            );                                        
                                        break;
                                    }
                                    case 2: { //Server only                                        
                                        fnDispatchToolbarServerAction({}, toolbarAction.PostValue, functionNames[0])
                                            .then(
                                                result => {
                                                    showLoading(false);
                                                    if (result.Continue === true) {
                                                        fnAfterToolBarAction(toolbarAction.PostValue);
                                                    } else {
                                                        fnAlert(result.Message, _messageType.Error);
                                                    }
                                                });
                                        break;
                                    }
                                }
                                break;
                            }
                        case 1: //Reload screen
                            {
                                switch (actualAction.Type) {
                                    case 1: { //JS and Server
                                        functionNames = actualAction.FunctionNames;                                        
                                        window[functionNames[0]](caller)
                                            .then(
                                                fnResult => {                                                    
                                                    if (fnResult.Continue === true) {
                                                        console.log('ok');
                                                        fnReloadTab(fnResult.ActionType)
                                                            .then(
                                                                html => {                                                                    
                                                                    switch (toolbarAction.PostValue.Type) {
                                                                        case 7: {// Show Message only
                                                                            fnAlert(toolbarAction.PostValue.Message);
                                                                            break;
                                                                        }
                                                                    }
                                                                },
                                                                err => {
                                                                    fnAlert(err, _messageType.Error);
                                                                }
                                                            );
                                                    }
                                                    
                                                },
                                                err => {
                                                    console.log(err);
                                                }
                                            );  
                                        
                                        break;
                                    }
                                    case 2: { //Server only
                                        fnReloadTab()
                                            .then(
                                                html => {
                                                    switch (toolbarAction.PostValue.Type) {
                                                        case 7: {// Show Message only
                                                            fnAlert(toolbarAction.PostValue.Message);
                                                            break;
                                                        }
                                                    }
                                                },
                                                err => {
                                                    fnAlert(err, _messageType.Error);
                                                }
                                            );
                                        break;
                                    }
                                }
                                break;
                            }
                    }
                    break;
                }
            }
        }
    }
    console.log(toolbarAction);
}

function fnAfterToolBarAction(postAction, actionType) {    
    if (postAction != null) {
        switch (postAction.Type) {
            case 0: { // Not required
                // TODO
                break;
            }
            case 3: { //Reload tab
                fnReloadTab(actionType);
                break;
            }
            case 4: { //Reload tab and showMessage
                fnReloadTab(actionType)
                    .then(
                        html => {
                            fnAlert(postAction.Message);
                        },
                        err => {
                            fnAlert(err, _messageType.Error);
                        }
                    );
                break;
            }
            case 7: { //Show message only
                fnAlert(postAction.Message);
                break;
            }
            case 8: { //Show message and execute other JS action
                fnAlert(postAction.Message);
                window[postAction.FunctionName]().then(
                    result => {
                    });
                break;
            }
        }
    }

}

function fnDispatchToolbarServerAction(data, actionName) {
    var controller = _actionControllers.Toolbar.Controller;
    var action = _actionControllers.Toolbar.Action;
    data = JSON.stringify({ ActionName: actionName, Parameters: data });
    return fnCallToolbarAction(controller, action, data, 'json');    
}

function fnReloadTab(actionType) {
    actionType = actionType == undefined ? _actionType.Reload : actionType;
    return fnLoadTab(_visibleProcessName, null, actionType)
        .then(
            html => {
                return html;
            },
            err => {
                console.log(err);
                return err;
            }
        );
}



function fnOnNavigate(caller) {
    var element = $$(caller);
    var navigateTo = element.attr(_attributes.Param);
    fnSetNavigateProperty(navigateTo);
    return Promise.resolve({
        Continue: true,
        Data: null
    });
}

function fnSetNavigateProperty(navigateTo) {
    var sessionId = fnGetActiveSessionId();
    var toolbar = fnGetPropertyValue(_visibleProcessName, sessionId, _propertyNames.Toolbar);
    toolbar[_propertyNames.NavigateToQuick] = navigateTo;
}

function fnOnRowSelect(cellNodes, isSelected, rowIndex, itemColName, dependentItemColName) {
    setTimeout(() => {
        if (_isDialogBoxOpened === true) {
            if (itemColName !== undefined && dependentItemColName !== undefined && isSelected === true) {
                var itemColValue = cellNodes.column(itemColName + ':' + 'name').data()[rowIndex];
                var dependentItemColValue = cellNodes.column(dependentItemColName + ':' + 'name').data()[rowIndex]; 
                var parentElementSelector = $(_selectors.DialogBox);
                $(_selectors.ItemValueHidden, parentElementSelector).val(itemColValue);
                $(_selectors.ItemDescValueHidden, parentElementSelector).val(dependentItemColValue);
            }
        }  
    }, 200);     
}

function fnGetRelatedElements(elementReference, sectionName, actionType, isInput = false) {
    var parent = $$(elementReference).parents('.' + _classes.EditableItem);
    var referencedColumnName = isInput === false ? $$(elementReference).attr(_attributes.Rel) : $$(elementReference).attr(_attributes.ReferenceColumnName);
    var item = isInput === false ? parent.find('input[' + _attributes.ReferenceColumnName + ' = ' + '"' + referencedColumnName + '"' + ']') : $$(elementReference);
    var columnName = item.attr(_attributes.ColumnName);
    
    var referencedTableName = item.attr(_attributes.ReferenceTableName);
    var dependentItem = parent.find('label.' + _classes.TextOutputField);
        
    return {
        Parent: parent,
        ColumnName: columnName,
        ReferencedColumnName: referencedColumnName,
        ReferencedTableName: referencedTableName,
        Item: item,
        DependentItem: dependentItem,
        Container: parent.parents('[' + _attributes.Section + ' = ' + '"' + sectionName + '"' + ']'),
        ActionType: actionType,
        SessionId: fnGetActiveSessionId(),
        SectionName: sectionName
    };
}

function fnGetViewColumns(items, usePromise = true, checkDependency = true) {    
    var isDependent = k => {
        var va = k === null || k === '' || k === undefined || k.length === 0 ? false : true;
        return va;
    };
    var viewColumnsValues = null;
    var viewColumnsNames = null;
    var colName = items.Item.attr(_attributes.ColumnName);
    var currentItemDependsOnValue = items.Parent.attr(_attributes.DependentOnColumnName);
    var isCurrentItemDependentOnAny = isDependent(currentItemDependsOnValue);
    var sectionName = items.SectionName;
    var result = null;
    var viewColumns = {
        Name: null,
        Value: null,
        Valid: false,
        Item: null,
        Message: null        
    };    
    var index = 0;
    var validate = (item, colName, actionType, isIndependent) => {
        var value = item.val();
        var isItemValid = fnIsValueCleanOrInLookUp(sectionName, value, colName, actionType, isIndependent);   
        viewColumns.Item = item;
        if (isItemValid === false) {           
            viewColumns.Valid = false;
            viewColumns.Message = "Invalid value " + value;
        } else {
            viewColumns.Valid = true;
            viewColumns.Message = null;
        }        
    };

    var getItemValue = item => {
        var value = item.val();
        if (value.length === 0) {
            return null;
        }
        return value;
    };

    if (checkDependency === false) {
        if (items.ActionType === _actionType.Undo || items.ActionType === _actionType.Reload || items.ActionType === _actionType.Navigate) {
            var sessionId = items.SessionId;
            viewColumns.Value = fnGetCleanValue(_visibleProcessName, sessionId, sectionName, colName);
            viewColumns.Valid = true;
            viewColumns.Item = items.Item;
            viewColumns.Message = 'Clean';
        } else {
            validate(items.Item, colName, items.ActionType, !isCurrentItemDependentOnAny);
            viewColumns.Value = getItemValue(items.Item);
        }
        viewColumns.Name = colName;        
    }
    else {
        if (isCurrentItemDependentOnAny === false) {
            validate(items.Item, colName, items.ActionType, true);
            viewColumns.Name = colName;
            viewColumns.Value = getItemValue(items.Item);
        }
        else {
            var isThisItemDependentOnAny = isCurrentItemDependentOnAny;
            var dependsOnValue = currentItemDependsOnValue;
            while (isThisItemDependentOnAny == true) { //runs when checkDependency is true and isCurrentItemDependentOnAny is true
                var editableItem = items.Container.find('.' + _classes.EditableItem + '[' + _attributes.ColumnName + ' = "' + dependsOnValue + '"]');
                var inputItem = editableItem.find('input');
                var value = getItemValue(inputItem);
                validate(inputItem, dependsOnValue, null, false);
                if (viewColumns.Valid === false)
                    break;

                viewColumnsValues = index === 0 ? value : viewColumnsValues + ',' + value;
                viewColumnsNames = index === 0 ? dependsOnValue : viewColumnsNames + ',' + dependsOnValue;
                dependsOnValue = editableItem.attr(_attributes.DependentOnColumnName);
                isThisItemDependentOnAny = isDependent(dependsOnValue);
                index++;
            }
            if (viewColumns.Valid === true) {
                validate(items.Item, colName, items.ActionType, true);
                if (viewColumns.Valid === true) {
                    viewColumns.Name = viewColumnsNames;
                    viewColumns.Value = viewColumnsValues;
                }                
            }
        }
        
    }   
   
    if (viewColumns.Valid === true) {
        if (usePromise === true) {            
            result = Promise.resolve(viewColumns);
        } else {
            result = viewColumns;
        }
    } else {
        if (usePromise === true) {
            result = Promise.reject(viewColumns);
        } else {
            result = viewColumns;
        }
    }    
    return result;
}

function fnGetAllViewColumnValues(actionType) {
    if (actionType === _actionType.Load) {
        return Promise.resolve(null);
    }
    var result = [];
    var topSection = fnGetTopSectionSelector();
    if (topSection !== null || topSection.length > 0) {
        var sectionName = topSection.attr(_attributes.Section);
        var editableItems = topSection.find('div.' + _classes.EditableItem);
            if (editableItems !== undefined && editableItems !== null && editableItems.length > 0) {
                for (var c = 0; c < editableItems.length; c++) {
                    var inputElement = $$(editableItems[c]).find('input');
                    var items = fnGetRelatedElements(inputElement, sectionName, actionType, true);
                    var eachResult = fnGetViewColumns(items, false, false);
                    eachResult.SectionName = sectionName;
                    result.push(eachResult);
                }
            }
    }    
    var arr = result.filter(x => x.Valid === false);
    if (arr !== null && arr.length > 0) 
        return Promise.reject(result);
    
    return Promise.resolve(result);        
}

function fnGetTopSectionSelector() {
    //return $$('div[' + _attributes.Section + '=' + '"' + _propertyNames.ColumnsData + '"' + ']');
    return $$('div.' + _classes.TopSection);
}

function fnGetBottomSectionSelector() {    
    return $$('div.' + _classes.BottomSection);
}

function fnHandle(r) {
    if (r.ParentSelector !== undefined) {
        $$(r.ParentSelector).find('.' + _classes.InvalidEditableItem).removeClass(_classes.InvalidEditableItem);
    } else {
        var item = null;
        var parentItem = null;
        var isValid = null;
        if (r.Data === undefined) {
            item = r.Item.Item;
            isValid = r.Item.Valid;
        } else {
            item = r.Data.Original.Item;
            isValid = r.Data.Original.Valid;
        }        
        parentItem = item.parent();
        if (isValid === false) {
            fnAddClassIfNotAdded(parentItem, _classes.InvalidEditableItem);
        } else {
            fnRemoveClass(parentItem, _classes.InvalidEditableItem);
        }
    }
    return null;
}

function fnHandleAll(r) {
    if (r && r.length) {
        for (var cnt = 0; cnt < r.length; cnt++) {
            fnHandle({ Item: r[cnt] });
        }
    }    
    return null;
}

function fnIsValueCleanOrInLookUp(sectionName, value, columnName, actionType, isIndependent) {
    var sessionId = fnGetActiveSessionId();
    var dirtyValue = fnGetDirtyValue(_visibleProcessName, sessionId, sectionName, columnName);
    if (dirtyValue === null || fnIsEmptyObject(dirtyValue)) {
        return true;// only clean value is in state which should be valid
    }

    if (_actionType.FocussedIn === actionType)
        return true;

    if (isIndependent === true && _actionType.Dialog === actionType) //dont validate, when user tries to open dialog box for item which doesnt depend on any item
        return true;
    
    var values = fnGetLatestOrLookUpValues(_visibleProcessName, sessionId, sectionName, columnName, true);
    if (values === null || fnIsEmptyObject(values)) 
        return false;
    
    var lookUp = values[_propertyNames.LookUpValues];
    if (lookUp.TableData === null || lookUp.TableData.length === 0)
        return false;
    var arr = JSON.parse(lookUp.TableData);
    var arrFound = [];
    if (_actionType.AutoComplete === actionType) {
        arrFound = arr.filter(x => (x[columnName]).toString().indexOf(value) > -1);
        return arrFound !== null && arrFound.length > 0;
    }
    arrFound = arr.filter(x => x[columnName] === value);
    return arrFound !== null && arrFound.length === 1;
}

function fnOnOpenDialogbox(elementReference, sectionName) {    
    if ($$(elementReference).hasClass(_classes.ButtonDisabled) === false) {
        var items = fnGetRelatedElements(elementReference, sectionName, _actionType.Dialog, false);        
        var itemRefColumnName = items.ReferencedColumnName;
        var dependentItemRefColumnName = items.DependentItem.attr(_attributes.ReferenceColumnName);        
        var parentElementSelector = $(_selectors.DialogBox);        
        _isDialogBoxOpened = true;
        var open = jsonData => {
            {
                var eventNames = {
                    show: 'show.bs.modal',
                    hide: 'hide.bs.modal'
                };
                $(parentElementSelector).off(eventNames.show);
                $(parentElementSelector).on(eventNames.show, function (event) {
                    var data = jsonData.TableData;
                    var configs = jsonData.DataTablesColumnsConfig;
                    configs = fnGetJSONParsedData(configs);
                    if (configs.Columns && configs.Columns.length > 0) {
                        configs[_propertyNames.CheckboxConfig] = {
                            itemDisplayName: itemRefColumnName,
                            dependentItemDisplayName: dependentItemRefColumnName
                        };
                        configs = JSON.stringify(configs);
                    }
                    fnBindDataTable(configs, data, null, $(this));
                });
                $(parentElementSelector).off(eventNames.hide);
                $(parentElementSelector).on(eventNames.hide, function (event) {
                    var itemDisplayNameValue = $(_selectors.ItemValueHidden, parentElementSelector).val();
                    var dependentItemDisplayNameValue = $(_selectors.ItemDescValueHidden, parentElementSelector).val();
                    $(_selectors.ItemValueHidden, parentElementSelector).val(null);
                    $(_selectors.ItemDescValueHidden, parentElementSelector).val(null);
                    items.Item.val(itemDisplayNameValue);
                    items.Item.trigger('change');
                    items.DependentItem.text(dependentItemDisplayNameValue);
                    var table = $(this).find(_selectors.Table);
                    table
                        .empty()
                        .removeClass()
                        .addClass(_classes.DataTableDisplay);
                    var dataTableObject = table.DataTable();
                    dataTableObject.destroy();
                    _isDialogBoxOpened = false;
                });
                parentElementSelector.modal('show');
            }
        };
        fnGetLookUp(items)
            .then(
                jsonData => {
                    open(jsonData);
                },
                err => {

                }
            )
            .catch(err => {

            });
    }
}

function fnGetLookUp(items) {
    return fnGetViewColumns(items)
        .then(
            viewColumns => {                
                return fnGetLookUpFromState(items.SessionId, items.SectionName, items.ColumnName)
                    .then
                    (
                        lookUp => {                            
                            var foundInState = true;
                            if (lookUp === null || fnIsEmptyObject(lookUp)) {
                                foundInState = false;
                            } else {
                                var savedViewColumnsValues = lookUp[_propertyNames.ViewColumnsValues];
                                var requestedViewColumnsValues = viewColumns.Value;
                                if (savedViewColumnsValues === null) {                                    
                                    foundInState = true;
                                }
                                else {
                                    if (requestedViewColumnsValues !== null) {
                                        if (savedViewColumnsValues === requestedViewColumnsValues) {
                                            foundInState = true;
                                        } else {
                                            foundInState = false;
                                        }
                                    } else {                                        
                                        foundInState = true;
                                    }                                    
                                }                                                              
                            }
                            if (foundInState === false) {
                                var values = viewColumns.Value;
                                var processName = 'DialogBox';
                                var controller = _actionControllers.Main.Controller;
                                var action = _actionControllers.Main.Action + fnPrepareQueryString(processName, values, items.ReferencedTableName, items.ReferencedColumnName);

                                return fnGetData(controller, action, 'json')
                                    .then(
                                        data => {                                            
                                            console.log('got from server');
                                            var v = {};
                                            v[_propertyNames.LookUpValues] = data;
                                            v[_propertyNames.ViewColumnsValues] = values;
                                            fnSetInState(_stateTypes.Lookup, _visibleProcessName, items.SessionId, items.SectionName, [items.ColumnName], [v]);
                                            showLoading(false);
                                            return new Promise((resolve, reject) => {
                                                return resolve(data);
                                            });
                                        },
                                        err => {
                                            showLoading(false);
                                            return Promise.reject(err);
                                        }
                                    )
                                    .catch(err => {
                                        showLoading(false);
                                        return Promise.reject(err);
                                    });
                            }
                            console.log('reading from state');
                            return Promise.resolve(lookUp[_propertyNames.LookUpValues]);
                        },
                        err => {
                            //console.log('reading from state failed');
                            return Promise.reject(err);
                        }
                    )
                    .catch(err => {
                        //console.log('reading from state failed');
                        return Promise.reject(err);
                    });
            },
            err => {
                //console.log('view columns error ' + err);//TODO show
                var obj = {};
                obj.Type = 0;
                obj.Original = err;
                return Promise.reject(obj);
            }
        )
        .catch(err => {
            //console.log('view columns error ' + err);// TODO show
            return Promise.reject(err);
        });
}

function fnIsEmptyObject(obj) {
    return Object.keys(obj).length === 0 && obj.constructor === Object
}

function fnGetStringValue(arr) {
    var value = null;
    if (arr != null && arr.length && arr.length > 0) {
        for (var cnt = 0; cnt < arr.length; cnt++) {
            value = cnt === 0 ? arr[cnt].Value : value + ',' + arr[cnt].Value;
        }
    }
    return value;
}

function fnGetTabData(processName, actionType) {
    return fnGetAllViewColumnValues(actionType)
        .then(
            viewColumnValues => {                
                var controller = _actionControllers.Main.Controller;
                viewColumnValues = fnGetStringValue(viewColumnValues);
                var action = _actionControllers.Main.Action + fnPrepareQueryString(processName, viewColumnValues);
                return fnGetData(controller, action, 'html')
                    .then(
                        html => {
                            showLoading(false);
                            return new Promise((resolve, reject) => {
                                return resolve(html);
                            });
                        },
                        err => {
                            showLoading(false);
                            return Promise.reject(err);
                        }
                    )
                    .catch(err => {
                        showLoading(false);
                        return Promise.reject(err);
                    });
            },
            err => {                
                return Promise.reject(err);
            }
        )
        .catch(err => {
            return Promise.reject(err);
        });
}

function fnGetLookUpFromState(sessionId, sectionName, columnName) {
    var value = fnGetLatestOrLookUpValues(_visibleProcessName, sessionId, sectionName, columnName, true);
    return Promise.resolve(value);
}

function fnAddClassIfNotAdded(element, className) {
    if ($(element).hasClass(className) === false) {
        $(element).addClass(className);
    }
}

function fnRemoveClass(element, className) {
    if ($(element).hasClass(className) === true) {
        $(element).fnRemoveClass(className);
    }
}

function fnBindEvents(processName, parentElementSelector) {
    var sectionName = $$(parentElementSelector).attr(_attributes.Section);
    var eventNamesWhenFocusedIn = 'focusin';
    var eventNamesWhenFocusedOut = 'focusout';
    var eventNamesOnChange = 'change clear keyup paste';
    var cls = _classes.AutoComplete;
    var interactiveElementsArray = ['input[type=text]', 'input[type=number]'];
    var allSelectors = null;
    var autoCompleteSelectors = null;
    interactiveElementsArray.forEach((value, index) => {
        if (index === 0) {
            allSelectors = value;
            autoCompleteSelectors = value + '.' + cls;
        } else {
            allSelectors += ',' + value;
            autoCompleteSelectors += ',' + value + '.' + cls;
        }
    });

    var checkForMandatory = element => {
        element = $$(element);
        var value = element.val();
        if (value === null || value.length === 0) {
            var attrIsMandatory = element.attr(_attributes.IsMandatory);
            if (attrIsMandatory !== null && attrIsMandatory !== undefined && attrIsMandatory.toString().toUpperCase() == true.toString().toUpperCase()) {
                // TODO
                console.log('mandatory');
                return false;
            }
        }
        return true;
    };

    var getLookUp = (element, actionType) => {
        element = $$(element);
        var attrIsLookUp = element.attr(_attributes.IsLookUpType);
        if (attrIsLookUp !== null && attrIsLookUp !== undefined && attrIsLookUp.toString().toUpperCase() == true.toString().toUpperCase()) { 
            var items = fnGetRelatedElements(element, sectionName, actionType, true);           
            return fnGetLookUp(items);
        }
        return Promise.resolve(null);
    };   
    var getLookUpWrapper = (element, actionType) => {
        getLookUp(element, actionType)
            .then(
                result => {
                    fnHandle({ ParentSelector: parentElementSelector });
                    console.log(result);
                },
                err => {
                    if (err != null) {
                        fnHandle({ IsError: true, Data: err });
                    }
                }
            )
            .catch(err => {
                console.log(JSON.stringify(err));
            });
    };

    var onChange = element => {
        element = $$(element);
        var columnName = element.attr(_attributes.ColumnName);
        var dirtyValue = element.val();
        var sessionId = fnGetActiveSessionId();
        fnSetInState(_stateTypes.Dirty, processName, sessionId, sectionName, [columnName], [dirtyValue]);
        var items = fnGetRelatedElements(element, sectionName, null, true);
        var container = items.Container;
        var dependentItem = container.find('div.' + _classes.EditableItem + '[' + _attributes.DependentOnColumnName + '=' + '"' + columnName + '"]');
        if (dependentItem !== null && dependentItem.length > 0) {
            var cleanValue = fnGetCleanValue(processName, sessionId, sectionName, columnName);
            if (cleanValue != dirtyValue) {
                var dependentInput = dependentItem.find('input');
                dependentInput.val('');
                dependentInput.trigger('change');
            }
        }
        var isValid = checkForMandatory(element);
        if (!isValid) {
           //TODO   
        }
        //getLookUpWrapper(this, null);           
    };

    allSelectors = $$(parentElementSelector).find($$(allSelectors)); //.not('.' + _classes.TextInputFieldFilter));
    $$(allSelectors)
        .off(eventNamesOnChange)
        .off(eventNamesWhenFocusedIn)
        .off(eventNamesWhenFocusedOut)
        .on(eventNamesWhenFocusedIn, function (event) {
            console.log('focussed in');
            getLookUpWrapper(this, _actionType.FocussedIn);           
            event.preventDefault();
        })
        .on(eventNamesWhenFocusedOut, function (event) {            
            console.log('focussed out');
            //getLookUpWrapper(this, null);           
            event.preventDefault();
        })
        .on(eventNamesOnChange, function (event) {
            console.log('change called');
            onChange(this);
            event.preventDefault();
        });

    autoCompleteSelectors = $$(parentElementSelector).find($$(autoCompleteSelectors)); //.not('.' + _classes.TextInputFieldFilter));
    $$(autoCompleteSelectors).each(function () {
        var element = $(this);
        var columnName = element.attr(_attributes.ColumnName);
        element.autocomplete({
            autoFocus: true,
            delay: 300,
            select: function (event, ui) {
                console.log('selected');
                onChange(element);
            },

            source: (request, response) => {
                getLookUp(element, _actionType.AutoComplete)
                    .then(
                        result => {
                            console.log('autocomplete called');
                            console.log(request.term);
                            var data = JSON.parse(result.TableData);
                            var filteredData = data.filter(x => x[columnName].indexOf(request.term) > -1);
                            fnHandle({ ParentSelector: parentElementSelector });
                            response($.map(filteredData, item => {
                                return { label: item[columnName], value: item[columnName], id: item[columnName] };
                            }));
                        },
                        err => {
                            if (err != null) {
                                fnHandle({ IsError: true, Data: err });
                            }
                        }
                    )
                    .catch(err => {
                        console.log('caught get lookup error ' + err);
                    });
            }
        });
    });
}

function fnActionNotAllowed() {
    fnAlert('Action not allowed', _messageType.Error);
}

function fnAlert(message, type = _messageType.Info) {
    switch (type) {
        case _messageType.Info: {
            toastr.success(message);
            break;
        }
        case _messageType.Warning: {
            toastr.warning(message);
            break;
        }
        case _messageType.Error: {
            toastr.error(message);
        }
    }
}