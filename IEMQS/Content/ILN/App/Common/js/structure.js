﻿var _classes = {
    ArrowDown: 'arrow-down',
    AutoComplete: 'autocomplete',
    Toolbar: 'toolbar',
    Tab: 'ln-tab',
    TabContents: 'tab-contents',
    TabContent: 'ln-tab-content',
    TabPanel: 'tabs-panel',
    TabResizePanel: 'tabs-resize-panel',
    TabSelected: 'ln-tab-selected',
    HorizontalDataGrid: 'horizontal-data-grid',
    ToolbarContainer: 'toolbar-container',
    ResizedMaximumWidth: 'resized-maximum-width',
    VisibleTab: "tab-visible",
    TopSection: 'top-section',
    BottomSection: 'bottom-section',
    Mandatory: 'mandatory',
    MenuItem: 'menu-item',
    DisabledMenuItem: 'disabled-menu-item',
    PopupContent: 'popup-content',
    Button: 'button',
    ButtonDisabled: 'button-disabled',
    Hide:' hide',
    LnLabel: 'ln-label',
    AnyMenu: 'menu',
    Image: 'gwt-image',
    InvalidEditableItem: 'invalid-editable-item',
    EditableItem: 'editable-item',
    DataTableDisplay: 'display',
    SystemButton: 'system-button',
    VerticalDragger: 'v-dragger',
    TextInputField: 'text-input-field',
    TextInputFieldReadonly: 'text-input-field-readonly',
    TextInputFieldEmpty: 'text-input-field-empty',
    TextInputFieldFilter: 'text-input-field-filter',
    TriggerButton: 'trigger-button',
    TriggerInputField: 'trigger-input-field',
    TriggerInputFieldDisabled: 'trigger-input-field-disabled',
    TextOutputField: 'text-output-field',
    DialogBoxSelectedItemValue: 'itemValue',
    DialogBoxSelectedItemDescValue: 'descValue'
};

var _filterClasses = {
    Filter: {
        FilterContainer: {
            Row: 'filter-container-row',
            Column: 'filter-container-column',
            Control: 'filter-control',
            InputControl: _classes.TriggerInputField + ' date-input-field',
            FilterImage: 'gwt-image-Operator',
            MenuButton: 'button menu-button',
            GridMenuButton: 'grid-menu-button',
            ArrowDownImage: _classes.ArrowDown + ' arrow-down arrow-down-customized',
            Input: _classes.TextInputField + ' ' + _classes.TextInputFieldEmpty + ' ' + _classes.TextInputFieldFilter
        },
        FilterMenu: {
            Main: _classes.AnyMenu,
            MenuButtonMenu: 'menu-button-menu',
            PopUpContent: 'popup-content',
            Row: _classes.MenuItem
        },
        Common: {
            Image: _classes.Image,
            Label: _classes.LnLabel
        }
    }
}

var _actionType = {
    Load: 'L',
    Reload: 'R',
    Undo: 'U',
    Navigate: 'N',
    FocussedIn: 'F',
    AutoComplete: 'A',
    Dialog: 'D',
    CustomToolbarAction: 'C'
};

var _toolbarItemType = {
    Button: 'button',
    SplitButton: 'splitbutton',
    MenuButton: 'menubutton'
};

var _messageType = {
    Info: 'Info',
    Warning: 'Warning',
    Error: 'Error'
};

var _attributes = {
    TabName: 'data-tabName',
    Rel: 'data-rel',
    ChildMenuItems: 'data-childmenu-toolbar-items',
    Action: 'data-action',
    Section: 'data-section',
    ColumnName: 'data-ColumnName',
    ColumnDisplayName: 'data-columnDisplayName',
    ReferenceColumnName: 'data-refColumnName',
    ReferenceTableName: 'data-refTableName',
    DependentOnColumnName: 'data-dependentOnColumnName',
    Index: 'data-index',
    Param: 'data-param',
    IsMandatory: 'data-isMandatory',
    IsLookUpType: 'data-isLookUpType',
    VisibleMode: 'data-visibleMode',
    Name: 'data-name',
    Disabled: 'disabled',
    Readonly: 'readonly'
};

var _selectors = {
    TabContentsContainer: 'div.' + _classes.TabContents,
    TabHeadingPanel: 'div.' + _classes.TabPanel,
    Tab: 'div.' + _classes.Tab,
    TabContent: 'div.' + _classes.TabContent,
    Loader: 'div#divLoader',
    Dragger: 'div.' + _classes.VerticalDragger,
    ResizePanel: 'div.' + _classes.TabResizePanel,
    AnyMenu: 'div#common-menu',
    Table: 'table.' + _classes.DataTableDisplay,
    DialogBox: 'div#ilnDialogBox',
    ItemValueHidden: 'input[type=hidden].' + _classes.DialogBoxSelectedItemValue,
    ItemDescValueHidden: 'input[type=hidden].' + _classes.DialogBoxSelectedItemDescValue
};

var _actionControllers = {
    Main: {
        Controller: 'Main',
        Action: 'CommonViewResult'
    },
    Toolbar: {
        Controller: 'ToolbarAction',
        Action: 'Index'
    }
};

var _navigateStates = {
    Current: 'CURRENT',
    First: 'FIRST',
    Next: 'NEXT',
    Prev: 'PREV',
    Last: 'LAST'
};

var _elementTypes = {
    Script: 'script',
    Link: 'link'
}

var _propertyNames = {
    ViewColumns: 'ViewColumns',
    ColumnsData: 'ColumnsData',
    SplitTabs: 'SplitTabs',
    Data: 'Data',
    Toolbar: 'Toolbar',
    NavigateTo: 'NavigateTo',
    NavigateToQuick: 'NavigateToQuick',
    ColName: 'ColName',
    ColValue: 'ColValue',
    SplitData: 'SplitData',
    CleanValue: 'Clean',
    DirtyValue: 'Dirty',
    LookUp: 'LookUp',
    LookUpValues: 'Values',
    ViewColumnsValues: 'ViewVolumnValues',
    CheckboxConfig: 'CheckboxConfig',
    EventActionsArray: 'EventActionsArray',
    ViewType: 'ViewType'
};

var _stateTypes = {
    Clean: 0,
    Dirty: 1,
    Lookup: 2
}

var _viewTypes = {
    Current: 0,
    New: 1
}

var _columnDefsPropertyNames = {
    NonSearchable: 'searchable',
    InVisible: 'visible',
    NonSortable: 'orderable',
    Checkbox: 'checkboxes',
    Class: 'className',
    Type: 'type',
    TypeConfigList: 'TypeConfigList'
}

var _revertPossibleActions = [
    { Id: 1, Name: 'addClass', Selector: 'Jquery', Parameters: 1, Type: 'function', RevertId: 2 },
    { Id: 2, Name: 'removeClass', Selector: 'jquery', Parameters: 1, Type: 'function', RevertId: 1 },
    { Id: 3, Name: 'setAttribute', Selector: 'js', Parameters: 2, Type: 'function', RevertId: 4 },
    { Id: 4, Name: 'removeAttribute', Selector: 'js', Parameters: 1, Type: 'function', RevertId: 3 },
    { Id: 5, Name: ['style', 'display'], Selector: 'js', Parameters: 1, Type: 'property', RevertId: 0, RevertedValue: 'none' },
    { Id: 6, Name: ['style', 'display'], Selector: 'js', Parameters: 1, Type: 'property', RevertId: 0, RevertedValue: '' }
];

var _callBackFunction = null;
var _isDialogBoxOpened = false;
var _appName = null;
var _visibleProcessName = null;
var _intervalTabLoad = null;
var _arrayStartPositions = [];
var _tabs = [];


class EventActionObject {
    constructor(id, selector, values, revertedValues, order, eventName) {
        this.Id = id;
        this.Selector = selector;
        this.Values = values;
        this.RevertedValues = revertedValues;
        this.Order = order;
        this.EventName = eventName;
    }
}

/* Events starts */
function fnOnTabChange(labelRef) {
    var tabRef = $(labelRef).parent();
    fnUnSelectPreviousTabHeadings();
    var tabName = tabRef.attr(_attributes.TabName);
    fnShowSelectedTabHeading(tabName);
    fnAddTabContent(tabName, null);
}

function fnOnCloseTab(ref) {
    var parent = $(ref).parent();
    var tabName = parent.attr(_attributes.TabName);
    var length = $(_selectors.TabContentsContainer).find(_selectors.TabContent).length;
    if (length == 1) {
        fnAlert('Cant close the only tab', _messageType.Warning);
    } else {
        var tabContent = fnGetTabsByName($(_selectors.TabContentsContainer), _selectors.TabContent, tabName);
        tabContent.remove();
        parent.remove();
        length = $(_selectors.TabContentsContainer).find(_selectors.TabContent).length;
        var tab = $($(_selectors.TabContentsContainer).find(_selectors.TabContent)[length - 1]);
        tabName = tab.attr(_attributes.TabName);
        fnShowTab(tab, tabName);
        fnUnSelectPreviousTabHeadings();
        fnShowSelectedTabHeading(tabName);
        fnPopTabObject(tabName);
    }
}

function fnOnResizeScreen(resizer, topPosition, leftPosition) {
    var classPortlet = 'portlet';
    var classPortletMaximized = 'portlet-maximize';
    var classbootBoxModal = 'bootbox.modal';
    var classFA = 'fa';
    var classExpand = classFA + '-' + 'expand';
    var classCollapse = classFA + '-' + 'compress';
    var selectorPortlet = '.' + classPortlet;
    var selectorBootBoxModal = '.' + classbootBoxModal;
    var selectorHTMLBody = 'html,body';
    var selectorDataTable = '.' + 'dataTable[id]';
    var elementPanel = $(resizer).closest(selectorPortlet);
    var elementBootboxModal = $(selectorBootBoxModal);
    var offsetPosition = elementPanel.offset();
    var titleValue = 'screen';
    var titleMaximizeValue = 'Maximize ' + titleValue;
    var titleMinimizeValue = 'Minimize ' + titleValue;
    var styleOverflow = 'overflow';
    var styleOverflowY = 'overflow-y';
    var stylePosition = 'position';
    var attrStyle = 'style';
    var attrTitle = 'title';
    var valueAbsolute = 'absolute';
    var valueEmpty = '';
    var valueHidden = 'hidden';
    var widthMax = '100vw';
    var heightMax = '100vh';
    var attrDataVisibleMode = _attributes.VisibleMode;
    var modeValueNormal = 'normal';
    var modeValueMaximized = 'maximized';
    var isAlreadyMaximized = () => {
        var value = elementPanel.attr(attrDataVisibleMode);
        if (value != null && value != undefined) {
            return value == modeValueMaximized;
        }
        return false;
    };

    var getSelectorI = classValue => {
        return '<i class = "' + classFA + ' ' + classValue + '"' + '></i>';
    };

    var selectorIExpand = getSelectorI(classExpand);
    var selectorICollapse = getSelectorI(classCollapse);
    var fn = null;
    console.log(offsetPosition);
    if (!isAlreadyMaximized()) { // action "maximize view" triggered for normal screen
        $(resizer).html(selectorICollapse).prop(attrTitle, titleMinimizeValue);
        $(selectorHTMLBody).animate({ scrollTop: 0 }, 1000).css(styleOverflow, valueHidden); // hide main scrollbar
        elementPanel.attr(attrDataVisibleMode, modeValueMaximized);
        if (elementBootboxModal.length > 0) { //  if table in Modal Popup
            elementBootboxModal.animate({ scrollTop: 0 }, 1000)
                .attr(attrStyle, elementBootboxModal.attr(attrStyle) + "; " + styleOverflowY + ": " + valueHidden + "!important");
            // used this cuz JQuery.css funtion not add "!important" in property
            elementPanel.css(stylePosition, valueAbsolute);
            fn = () => {
                elementPanel.addClass(classPortletMaximized).animate({
                    width: widthMax,
                    height: heightMax,
                    top: topPosition,//-87,
                    left: leftPosition //-351
                }, 1000);
            }
        } else {
            fn = () => {
                elementPanel.addClass(classPortletMaximized).animate({
                    width: widthMax,
                    height: heightMax,
                    top: offsetPosition.top * -1,
                    left: offsetPosition.left * -1,
                }, 1000);
            }
        }

    } else { // action "return to normal view" triggered for already maximized screen
        $(resizer).html(selectorIExpand).prop(attrTitle, titleMaximizeValue);
        fn = () => {
            elementPanel.animate({
                width: '100%',
                height: '100%',
                top: offsetPosition.top * -1,
                left: offsetPosition.left * -1
            }, 1000);
        }
        setTimeout(() => {
            elementPanel.attr(attrDataVisibleMode, modeValueNormal);
            elementPanel.removeClass(classPortletMaximized);
            $(selectorHTMLBody).css(styleOverflow, valueEmpty).animate({ scrollTop: offsetPosition.top - 70 }, 1000); // auto main scrollbar;
            if (elementBootboxModal.length > 0) {
                elementBootboxModal.css(styleOverflowY, valueEmpty);
                elementPanel.css(stylePosition, valueEmpty);
            }
        }, 1000);
    }
    if (fn != null) {
        fn();
    }
    setTimeout(() => {
        $(selectorDataTable, elementPanel).DataTable().columns.adjust();
    }, 1000);

}
/* Events ends */

function fnCheckLoadTab(processName, processDescription) {
    var loader = $(_selectors.Loader);
    if (loader.length > 0) {
        if (_intervalTabLoad != null) {
            window.clearInterval(_intervalTabLoad);
            _intervalTabLoad = null;
        }
        fnLoadTab(processName, processDescription)
            .then(
                result => {
                    
                }
            )

    } else {
        if (_intervalTabLoad == null) {
            _intervalTabLoad = window.setInterval(() => {
                fnCheckLoadTab(processName, processDescription);
            }, 100);
        }
    }
}

function fnLoadTab(processName, processDescription, actionType = _actionType.Load) {
    console.log('processName', processName);
    var isTabAlreadyLoaded = fnIsTabAlreadyLoaded($(_selectors.TabContentsContainer), _selectors.TabContent, processName);
    if (isTabAlreadyLoaded === false || actionType === _actionType.Reload || actionType === _actionType.Navigate || actionType === _actionType.Undo) {
        return fnGetTabData(processName, actionType)
            .then(
                html => {                    
                    if (isTabAlreadyLoaded === true) {
                        fnRefreshTabContent(processName, html);
                    } else {
                        fnAddTabContent(processName, html);
                        fnAddTabHeading(processName, processDescription);
                    }
                    fnBindEvents(processName, fnGetTopSectionSelector());
                    showLoading(false);
                    return Promise.resolve(html);
                },
                err => {                    
                    showLoading(false);
                    console.log(err);
                    return Promise.reject(err);
                }
            );
    } else {
        if (isTabAlreadyLoaded === true)
            fnAlert(processDescription + ' tab already loaded.');        
    }
    return Promise.resolve(null);
}

function fnIsTabAlreadyLoaded(container, selector, tabName) {
    var tabs = fnGetTabsByName(container, selector, tabName);
    return tabs.length > 0;
}

function fnGetTabsByName(container, selector, tabName) {
    return container.find(selector + "[" + _attributes.TabName + "=" + "'" + tabName + "']");
}

function fnPrepareQueryString(processName, viewColumnValues, referredTableName = null, referredColumnName = null) {
    var query =
        '?appName=' + _appName +
        '&processName=' + processName;
    if (referredColumnName !== null) {
        query += '&referredColumnName=' + referredColumnName;
    }
    if (referredTableName !== null) {
        query += '&referredTableName=' + referredTableName;
    }
    if (viewColumnValues !== null)
        query += '&viewColumnValues=' + viewColumnValues;
    query = fnGetQueryStringValues(query, viewColumnValues !== null, referredColumnName != null && referredTableName != null);
    return query;
}

function fnGetQueryStringValues(query, hasViewColumns, isForLookUp = false) {
    value = fnGetQueryStringNavigatorType(hasViewColumns, isForLookUp);
    if (value != null) {
        query += '&navigatorType=' + value;
    }
    value = fnGetQueryStringNavigatorIndex(hasViewColumns, isForLookUp);
    if (value != null) {
        query += '&navigatorIndex=' + value;
    }
    value = fnGetQueryStringIndexName();
    if (value != null) {
        query += '&indexName=' + value;
    }
    return query;
}

function fnGetQueryStringNavigatorType(hasViewColumns = true, isForLookUp = false) {
    if (isForLookUp === true) {
        return null;
    }
    if (hasViewColumns === false) {
        return null;
    }
    var sessionId = fnGetActiveSessionId();
    if (sessionId === null)
        return _navigateStates.Current;
    return fnGetPropertyValue(_visibleProcessName, sessionId, _propertyNames.Toolbar)[_propertyNames.NavigateToQuick];
}

function fnGetQueryStringNavigatorIndex(hasViewColumns = true, isForLookUp = false) {
    if (isForLookUp === true) {
        return 1;
    }
    if (hasViewColumns === false) {
        var sessionId = fnGetActiveSessionId();
        if (sessionId === null)
            return 1;
        return fnGetPropertyValue(_visibleProcessName, sessionId, _propertyNames.Toolbar)[_propertyNames.NavigateTo];
    }
    return null;
}

function fnGetQueryStringIndexName() {
    var value = null; // 'Index1';//TODO

    return value;
}

function fnAddTabHeading(tabName, tabText) {
    var container = $(_selectors.TabHeadingPanel);
    var selector = "div." + _classes.Tab;
    var previousTabsCount = container.find(selector).length;
    var isAlreadyAdded = fnIsTabAlreadyLoaded(container, selector, tabName);
    if (isAlreadyAdded === false) {
        var classes = _classes.Tab + ' ' + _classes.TabSelected;
        var div = $("<div class='" + classes + "'></div>");
        div.attr(_attributes.TabName, tabName);
        div.append(
            "<label onclick= 'fnOnTabChange(this);' class='" + _classes.LnLabel + "'>" + tabText + "</label >" +
            "<div onclick = 'fnOnCloseTab(this)' tabindex = '" + (previousTabsCount + 1) + "' class= 'button close' >" +
            "<img class = '" + _classes.Image + "'" +
            " src='data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAgAAAAICAYAAADED76LAAAAJElEQVQoU2NkIAAYofL/oTQGn2gFMIvQTWKAmUBQAfXcgOFpADKnCgn2sjncAAAAAElFTkSuQmCC' > " +
            // "x" +
            "</div>"
        );
        fnUnSelectPreviousTabHeadings(container);
        container.append(div);
    }
}

function fnUnSelectPreviousTabHeadings(container) {
    container = container == undefined ? $(_selectors.TabHeadingPanel) : container;
    container.find(_selectors.Tab).removeClass(_classes.TabSelected);
}

function fnGetTabObject(screenId, sessionId, occurenceId) {
    if (_tabs === null || _tabs.length === 0) {
        return null;
    }
    var returned = null;
    for (var cnt = 0; cnt < _tabs.length; cnt++) {
        if (_tabs[cnt].ScreenId === screenId && _tabs[cnt].SessionId === sessionId && _tabs[cnt].OccurenceId === occurenceId) {
            returned = fnExistingTab(cnt);
            break;
        }
    }
    return returned;
}

function fnExistingTab(cnt) {
    return { TabObject: _tabs[cnt], TabIndex: cnt };
}

function fnGetTabObjectByProcessName(processName) {
    if (_tabs === null || _tabs.length === 0) {
        return null;
    }
    var returned = null;
    for (var cnt = 0; cnt < _tabs.length; cnt++) {
        if (_tabs[cnt].ProcessName === processName) {
            returned = fnExistingTab(cnt);
            break;
        }
    }
    return returned;
}

function fnGetHighestOrderOfTab() {    
    if (_tabs === null || _tabs.length === 0)
        return 0;
    
    var returned = 0;
    for (var cnt = 0; cnt < _tabs.length; cnt++) {
        if (_tabs[cnt].Order > returned) {
            returned = _tabs[cnt].Order;
        }
    }
    return returned;
}

function fnGetMainSessionId() {
    if (_visibleProcessName === null)
        return null;    
    var obj = fnGetTabObjectByProcessName(_visibleProcessName).TabObject;
    return obj.SessionId;
}

function fnGetActiveSessionId() {
    if (_visibleProcessName === null)
        return null;    
    var obj = fnGetTabObjectByProcessName(_visibleProcessName).TabObject;
    return obj.SessionId;//TODO compute and change
}

function fnPushTabObject(processName, screenId, sessionId, occurenceId, mainSessionObject, satelliteSessionsObject = null) {
    if (_tabs === null || _tabs.length === 0) {
        _tabs = [];
    }
    var obj = fnGetTabObject(screenId, sessionId, occurenceId);
    if (obj === null) {
        var order = fnGetHighestOrderOfTab();
        _tabs.push({
            ScreenId: screenId,
            SessionId: sessionId,
            OccurenceId: occurenceId,
            ProcessName: processName,
            MainSession: mainSessionObject,
            SatelliteSessions: satelliteSessionsObject,
            State: null,
            Order: order + 1
        });
    }
}

function fnPopTabObject(processName) {
    var obj = fnGetTabObjectByProcessName(processName);
    if (obj !== null) {
        _tabs = _tabs.splice(obj.TabIndex, 1);
    }
}

function fnSetApplicationName(appName) {
    _appName = appName;
}

function fnShowSelectedTabHeading(tabName) {
    var tab = fnGetTabsByName($(_selectors.TabHeadingPanel), _selectors.Tab, tabName);
    tab.addClass(_classes.TabSelected);
}

function fnGetVisibleTab() {
    return $(_selectors.TabContentsContainer).find($('div.' + _classes.VisibleTab));
}

/* 
  This function hide all previous tab contents and does either or both of two operations.    
   -showParticularTabByName: When given not null, will show that tab content matching given tab name.
      Called when user switches tab by clicking on tab heading
   -partialViewHTML: When not null, will render new tab content. 
      Called from ajax when user clicks another tab from left menu.
*/
function fnAddTabContent(showParticularTabByName, partialViewHTML) {
    var tabContentsContainer = $(_selectors.TabContentsContainer);
    var previousTabContents = tabContentsContainer.find(_selectors.TabContent);
    previousTabContents.hide();
    previousTabContents.removeClass(_classes.VisibleTab);
    if (partialViewHTML !== null) {
        tabContentsContainer.append(partialViewHTML);
    }
    if (showParticularTabByName != null) {
        var tab = fnGetTabsByName(tabContentsContainer, _selectors.TabContent, showParticularTabByName);
        fnShowTab(tab, showParticularTabByName);
    }
    fnAssignDraggerEvent();
}

function fnRefreshTabContent(showParticularTabByName, partialViewHTML) {
    var tabContentsContainer = $(_selectors.TabContentsContainer);
    if (partialViewHTML !== null) {
        var previousTabContent = tabContentsContainer.find('.' + _classes.VisibleTab);
        previousTabContent.replaceWith(partialViewHTML);
    }    
    if (showParticularTabByName != null) {
        var tab = fnGetTabsByName(tabContentsContainer, _selectors.TabContent, showParticularTabByName);
        fnShowTab(tab, showParticularTabByName);
    }
    fnAssignDraggerEvent();    
}

function fnExecuteCallbackFunction(input) {
    if (_callBackFunction != null) {
        _callBackFunction(input);
        _callBackFunction = null;
    }
}

function fnShowTab(tabRef, tabName) {
    tabRef.addClass(_classes.VisibleTab);
    _visibleProcessName = tabName;
    tabRef.show();
}

function fnTriggerResizeScreen(resizer) {
    resizer = resizer == null || undefined ? $(_selectors.ResizePanel).find('label') : resizer;
    var topPosition = -68;
    var leftPosition = 225;
    fnOnResizeScreen(resizer, topPosition, leftPosition);
}

function $$(selector) {
    return $(selector, fnGetVisibleTab());
}

/* Events Starts */
function fnOnMouseDown(e) {
    fnAddStartPositionPerProcess(e.clientY, _visibleProcessName);
}

function fnOnMouseMove(e) {

    if (e.buttons === 1) {
        var topDiv = fnGetTopSectionSelector();
        var bottomDiv = fnGetBottomSectionSelector();
        var upperHeight = topDiv.height();
        var lowerHeight = bottomDiv.height();
        var endY = e.clientY;
        var startY = fnGetStartPositionObjectPerProcess(_visibleProcessName).startY;
        var diff = endY - startY;
        upperHeight = upperHeight + diff;
        lowerHeight = lowerHeight + diff;
        topDiv.height(upperHeight);
        bottomDiv.height(lowerHeight);
    }
}
/* Events Ends */

function fnAddStartPositionPerProcess(startY, processName) {
    var found = fnGetStartPositionObjectPerProcess(processName);
    if (found != null) {
        found.startY = startY;
    } else {
        _arrayStartPositions.push({ startY: startY, processName: processName })
    }
}

function fnGetStartPositionObjectPerProcess(processName) {
    var found = null;
    for (var cnt = 0; cnt < _arrayStartPositions.length; cnt++) {
        if (_arrayStartPositions[cnt].processName == processName) {
            found = _arrayStartPositions[cnt];
            break;
        }
    }
    return found;;
}

function fnGetCommonViewResult(controller, action, processName, getJson = false, refresh = false) {
    var dataType = getJson === true ? 'json' : 'html';
    $.ajax({
        type: "POST",
        url: WebsiteURL + "/" + _appName + "/" + controller + "/" + action,
        datatype: dataType,
        beforeSend: () => {
            showLoading(true);
        },
        complete: () => {
            showLoading(false);
        },
        success: result => {
            if (getJson === false) {
                if (refresh === true) {
                    fnRefreshTabContent(processName, result);
                }
                else {
                    fnAddTabContent(processName, result);
                }
                fnExecuteCallbackFunction();
            }
            else {
                fnExecuteCallbackFunction(result);
            }        
            showLoading(false);
        },
        error: e => {
            console.log(e);
            showLoading(false);
        }
    });
}

function fnGetData(controller, action, dataType) {
    console.log(action);
    showLoading(true);
    return new Promise((resolve, reject) => {
        $.ajax({
            url: WebsiteURL + "/" + _appName + "/" + controller + "/" + action,
            dataType: dataType,
            type: 'POST'
        })
        .done(resolve)
        .fail(reject);
    });
}

function fnCallToolbarAction(controller, action, data, dataType) {
    console.log(action);
    showLoading(true);
    return new Promise((resolve, reject) => {
        $.ajax({
            url: WebsiteURL + "/" + _appName + "/" + controller + "/" + action,
            dataType: dataType,
            data: { input: data },
            type: 'POST'
        })
        .done(resolve)
        .fail(reject);
    });
}

function fnAssignDraggerEvent() {
    var dragger = $$(_selectors.Dragger);
    var mouseMove = 'mousemove';
    var mouseDown = 'mousedown';
    dragger.off(mouseMove);
    dragger.off(mouseDown);
    dragger.on(mouseDown, function (event) {
        fnOnMouseDown(event);
    });
    dragger.on(mouseMove, function (event) {
        fnOnMouseMove(event);
    });
}

function fnLoadScriptIfNotLoaded(scriptPath) {
    fnLoadElementIfNotLoaded(_elementTypes.Script, 'src', scriptPath);
}

function fnLoadStyleIfNotLoaded(stylePath) {
    fnLoadElementIfNotLoaded(_elementTypes.Link, 'href', stylePath);
}

function fnLoadElementIfNotLoaded(elementType, attrName, attrValue) {
    attrValue = WebsiteURL + "/" + attrValue;
    if ($(document).find(elementType + "[" + attrName + "$=" + "'" + attrValue + "']").length == 0) {
        var element = $("<" + elementType + "></" + elementType + ">");
        element.attr(attrName, attrValue);
        if (elementType == _elementTypes.Link) {
            element.attr('rel', 'stylesheet');
        }
        $(document).find("head").append($(element));
    }
}

function fnLoadScripts(pathsArray) {
    for (var i = 0; i < pathsArray.length; i++) {
        fnLoadScriptIfNotLoaded(pathsArray[i]);
    }
}

function fnLoadStyles(pathsArray) {
    for (var i = 0; i < pathsArray.length; i++) {
        fnLoadStyleIfNotLoaded(pathsArray[i]);
    }
}

function fnGetJSONParsedData(input) {
    return JSON.parse(input.toString().replace(/&quot;/g, '"'));
}

function fnSplitString(input) {
    var arr = input.toString().split(',');
    return arr;
}

function fnSplitStringAsIntegerArray(input) {
    var arr = input.toString().split(',');
    var integerArr = [];
    for (var cnt = 0; cnt < arr.length; cnt++) {
        var value = parseInt(arr[cnt]);
        integerArr.push(value);
    }
    return integerArr;
}

function fnSetConfig(colIndexes, propName, propValue) {
    var arr = fnSplitStringAsIntegerArray(colIndexes);
    var obj = {};
    obj[propName] = propValue;
    obj.targets = arr;
    return obj;
}

function fnSetConfigs(colIndexes, propNames, propValues) {
    var arr = fnSplitStringAsIntegerArray(colIndexes);
    var obj = {};
    for (var x = 0; x < propNames.length; x++) {
        var propName = propNames[x];
        var propValue = propValues[x];
        obj[propName] = propValue;
    }
    obj.targets = arr;
    return obj;
}

function fnGetConfig(columnDefs, propertyName) {
    var arr = columnDefs;
    var obj = null;
    var found = null;
    for (var cnt = 0; cnt < arr.length; cnt++) {
        obj = arr[cnt];
        for (var x in obj) {
            if (x == propertyName) {
                found = obj;
                break;
            }
        }
    }
    return found;
}

function fnBindDataTable(configs, data, processName, parentElementSelector = null) {
    
    configs = fnGetJSONParsedData(configs);
    var columnConfigs = configs.ColumnConfigs;
    var columnDefs = [];
    var columnDef = null;
    for (var columnConfigPropertyName in columnConfigs) {
        if (columnConfigPropertyName != _columnDefsPropertyNames.TypeConfigList) {
            columnDef = fnSetConfig(columnConfigs[columnConfigPropertyName].ColIndexes, _columnDefsPropertyNames[columnConfigPropertyName], columnConfigs[columnConfigPropertyName].Value);
            columnDefs.push(columnDef);
        } else {
            for (var a = 0; a < columnConfigs[columnConfigPropertyName].length; a++) {
                columnDef = fnSetConfigs(columnConfigs[columnConfigPropertyName][a].ColIndexes, [_columnDefsPropertyNames.Type, _columnDefsPropertyNames.Class], [columnConfigs[columnConfigPropertyName][a].Value, columnConfigs[columnConfigPropertyName][a].Value]);
                columnDefs.push(columnDef);
            }            
        }       
    }    
    data = fnGetJSONParsedData(data);
    parentElementSelector = parentElementSelector === null ? _selectors.TabContent + "[" + _attributes.TabName + "='" + processName + "']" : parentElementSelector;
    var parentElement = $(parentElementSelector);
    var table = parentElement.find(_selectors.Table);
    var options = {
        columnFilter: fnGetColumnFilterInitOptions(parentElementSelector),
        order: [[1, 'asc']]
    };
    var columns = configs.Columns;

    if (fnGetConfig(columnDefs, _columnDefsPropertyNames.Checkbox) != null) {
        var colsArray = [];
        colsArray.push({
            title: '',
            data: 'Id',
            name: 'Id'
        });
        columns = colsArray.concat(columns);
        for (var c = 0; c < data.length; c++) {
            data[c].Id = c + 1;
        }
    }    
    $.extend(options, {
        columns: columns,
        columnDefs: columnDefs,
        data: data,
        keys: configs.keys,
        select: configs.select,
        checkboxConfig: configs.CheckboxConfig,
        scroll: true
    });    
    
    table
        .empty()
        .removeClass()
        .addClass('display')
        .DataTable(options);    
    return data;
}

function fnGetColumnFilterInitOptions(parentElementSelector) {
    return {
        SelectorContainer: parentElementSelector,
        Classes: _filterClasses.Filter
    }
}

function fnFindActiveSessionAndSplitTabId(processTab, sessionId) {
    if (processTab != null) {
        return {
            Session: processTab.SessionId === sessionId ? processTab.MainSession : null, //TODO
            SplitTabId: null //TODO
        };
    }
}

function fnSetInState(state, processName, sessionId, mainProperty, propertyNames, propertyValues) {
    var target = fnGetTargetSection(processName, sessionId, mainProperty);
    for (var i = 0; i < propertyNames.length; i++) {
        var propertyName = propertyNames[i];
        var propertyValue = propertyValues[i];
        if (target[propertyName] === undefined) {
            target[propertyName] = {};
            target[propertyName][_propertyNames.DirtyValue] = null;
            target[propertyName][_propertyNames.CleanValue] = null;
            target[propertyName][_propertyNames.LookUp] = null;
        }        
        switch (state) {
            case _stateTypes.Clean: {
                target[propertyName][_propertyNames.CleanValue] = propertyValue;
                target[propertyName][_propertyNames.DirtyValue] = null; //clean dirty data when refreshed
                target[propertyName][_propertyNames.LookUp] = {}; // will be populated later
                break;
            }
            case _stateTypes.Dirty: {
                target[propertyName][_propertyNames.DirtyValue] = propertyValue;
                break;
            }
            case _stateTypes.Lookup: {
                target[propertyName][_propertyNames.LookUp] = propertyValue;
                //populate list so that this can be used when user types, we can show dropdown.
                break;
            }
        }        
    }
}

function fnGetTargetSection(processName, sessionId, mainProperty) {
    var processTab = fnGetTabObjectByProcessName(processName).TabObject;
    var sessionAndSplitTabId = fnFindActiveSessionAndSplitTabId(processTab, sessionId);
    var session = sessionAndSplitTabId.Session;
    //var splitTabId = sessionAndSplitTabId.SplitTabId;
    var target;
    if (session[_propertyNames.SplitTabs] === false) {
        target = session[_propertyNames.Data][mainProperty];
    } else {
        target = session[_propertyNames.SplitData];//TODO find by splitTabId
    }
    return target;
}

function fnGetActiveSessionProperties(processName, sessionId) {
    var processTab = fnGetTabObjectByProcessName(processName).TabObject;
    var sessionAndSplitTabId = fnFindActiveSessionAndSplitTabId(processTab, sessionId);
    var session = sessionAndSplitTabId.Session;
    //var splitTabId = sessionAndSplitTabId.SplitTabId;
    var target = null;
    if (session[_propertyNames.SplitTabs] === false) {
        target = session;
    } else {
        target = session[_propertyNames.SplitData];//TODO find by splitTabId
    }
    return target;
}

function fnGetLatestOrLookUpValues(processName, sessionId, mainProperty, propertyName, isLookUp = false) {
    var target = fnGetTargetSection(processName, sessionId, mainProperty);
    if (isLookUp === true) {
        if (target[propertyName][_propertyNames.LookUp] != null && target[propertyName][_propertyNames.LookUp] != {}) {
            return target[propertyName][_propertyNames.LookUp];
        }
        return null;
    }
    if (target[propertyName][_propertyNames.DirtyValue] != null) {
        return target[propertyName][_propertyNames.DirtyValue];
    }
    return target[propertyName][_propertyNames.CleanValue];    
}

function fnGetDirtyValue(processName, sessionId, mainProperty, propertyName) {
    var target = fnGetTargetSection(processName, sessionId, mainProperty); 
    if (target.hasOwnProperty(propertyName) === false)
        return null;
    return target[propertyName][_propertyNames.DirtyValue];
}

function fnGetCleanValue(processName, sessionId, mainProperty, propertyName) {
    var target = fnGetTargetSection(processName, sessionId, mainProperty);
    if (target.hasOwnProperty(propertyName) === false)
        return null;
    return target[propertyName][_propertyNames.CleanValue];
}

function fnGetPropertyValue(processName, sessionId, propertyName) {
    var processTab = fnGetTabObjectByProcessName(processName).TabObject;
    var sessionAndSplitTabId = fnFindActiveSessionAndSplitTabId(processTab, sessionId);
    var session = sessionAndSplitTabId.Session;
    //var splitTabId = sessionAndSplitTabId.SplitTabId;
    return session[propertyName];
}

function fnSetPropertyValue(processName, sessionId, propertyName, propertyValue) {
    var processTab = fnGetTabObjectByProcessName(processName).TabObject;
    var sessionAndSplitTabId = fnFindActiveSessionAndSplitTabId(processTab, sessionId);
    var session = sessionAndSplitTabId.Session;
    //var splitTabId = sessionAndSplitTabId.SplitTabId;
    session[propertyName] = propertyValue;
}

function fnOnInitMultiOccurenceScreen(configs, data, processName, screenId, occurenceTypeId, mainSession, viewColumnValues) {
    
    fnBindDataTable(configs, data, processName);
    var mainSessionId = mainSession.Id;
    var mainSessionObject = {};
    mainSessionObject.Id = mainSessionId;
    mainSessionObject[_propertyNames.SplitTabs] = false;
    mainSessionObject[_propertyNames.ViewColumns] = mainSession.MultiOccurenceInstance.ViewColumns;
    mainSessionObject[_propertyNames.Toolbar] = {};
    mainSessionObject[_propertyNames.ViewType] = _viewTypes.Current;
    mainSessionObject[_propertyNames.Toolbar][_propertyNames.NavigateToQuick] = mainSession.MultiOccurenceInstance.ViewColumns !== null && mainSession.MultiOccurenceInstance.ViewColumns.length > 0 ? _navigateStates.Current : null;
    mainSessionObject[_propertyNames.Toolbar][_propertyNames.NavigateTo] = mainSession.MultiOccurenceInstance.ViewColumns !== null && mainSession.MultiOccurenceInstance.ViewColumns.length > 0 ? 1 : null;
    mainSessionObject[_propertyNames.Data] = {}; // every leaf propery has two states - Dirty and Clean
    mainSessionObject[_propertyNames.Data][_propertyNames.ColumnsData] = {};
    mainSessionObject[_propertyNames.EventActionsArray] = [];
    fnPushTabObject(processName, screenId, mainSessionId, occurenceTypeId, mainSessionObject, null);

    var propertyNames = [];
    var propertyValues = [];
    if (viewColumnValues != null && viewColumnValues.length > 0) {
        for (var cnt = 0; cnt < viewColumnValues.length; cnt++) {
            var viewColumn = viewColumnValues[cnt];
            var propertyName = viewColumn[_propertyNames.ColName];
            var propertyValue = viewColumn[_propertyNames.ColValue];
            propertyNames.push(propertyName);
            propertyValues.push(propertyValue);
        }
        fnSetInState(_stateTypes.Clean, processName, mainSessionId, _propertyNames.ColumnsData, propertyNames, propertyValues);
    }    
}

/*
 *  
            ScreenId: screenId,
            SessionId: sessionId,
            OccurenceId: occurenceId,
            ProcessName: processName,
            MainSession: {
              Id:1,
              SplitTabs:false,
              ViewColumns:[],
              Data: {
                  ColumnsData: {
                    t_cprj:{Clean:'',Dirty:''},
                    t_cspa:{Clean:'',Dirty:''}
                   } 
              },
              Toolbar:{
                  NavigateTo:
                  NavigateToQuick:
              },
              EventActionsArray:[]
            },
            SatelliteSessions: satelliteSessionsObject,
            State: null,
            Order: order + 1
 * 
 * 
 * */