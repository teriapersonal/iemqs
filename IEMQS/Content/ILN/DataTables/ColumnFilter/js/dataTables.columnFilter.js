/*! 
 * 
 * 
 *
 * @version     1.0.0
 * @author      Nishant Teria
 * @copyright   (c) Terias
 * @license     MIT
 */
(function( factory ){
    /* eslint-disable */
       if ( typeof define === 'function' && define.amd ) {
          // AMD
          define( ['jquery', 'datatables.net'], function ( $ ) {
             return factory( $, window, document );
          } );
       }
       else if ( typeof exports === 'object' ) {
          // CommonJS
          module.exports = function (root, $) {
             if ( ! root ) {
                root = window;
             }
    
             if ( ! $ || ! $.fn.dataTable ) {
                $ = require('datatables.net')(root, $).$;
             }
    
             return factory( $, root, root.document );
          };
       }
       else {
          // Browser
          factory( jQuery, window, document );
       }
    /* eslint-enable */
    }(function( $, window, document ) {
        'use strict';
        var $1;
        $1 = $;
        var dataTable = $1.fn.dataTable;
        var DataTable$ = $1.fn.DataTable;
      
        class ColumnFilter {
            constructor(settings, opts) {
                var that = this;
                
                that.initOptions = $1.extend(true, {}, ColumnFilter.defaults, opts);
                var apiDataTable$ = new DataTable$.Api(settings);
                var settingsArray = apiDataTable$.settings();
                var settingsObject = settingsArray[0];
                // Get settings object
                that.InstanceValues = {
                    DataTableSettings: settingsObject,
                    Api: apiDataTable$,
                    Classes: that.initOptions.Classes,
                    SelectorContainer: that.initOptions.SelectorContainer,
                    SearchFunction: null,
                    SearchedOperatorType: null,
                    SearchedValue: null,
                    SearchedColIndex: null,
                    SearchedColType: null
                };

                // Check if ColumnFilter have already been initialised on this table
                if (settingsObject._columnFilter) {
                    return;
                }
                settingsObject._columnFilter = this;
                if (!settingsObject._bInitComplete) {
                    that._setUp(apiDataTable$);
                }
                else {
                    apiDataTable$.one('init.dt.dtColumnFilter', function () {
                        that._setUp(apiDataTable$);
                    });                    
                }
                return that;
            }

            _getNode() {
                var self = this;
                return self._$$('tr' + '.' + self.InstanceValues.Classes.FilterContainer.Row);
            }

            /**
             * _setUp private function
             * @description
             *   This is main function called from init, so as to create the filter and customize search
             *   so as to consider chosen operator.
             * @returns  Nothing
             */
            _setUp() {
                var self = this;
                var $table = $(self.InstanceValues.Api.table().node());
                $table.one('destroy.dt.dtColumnFilter', function (e, settings) {
                    self._destroy();
                });
                self._createFilterContainer();
                self._customizeSearch();
            }

            _destroy() {
                var self = this;
                self._getNode().remove();
            }

            _$$(selector) {
                var self = this;
                var selectorContainer = self.InstanceValues.SelectorContainer;
                return $1(selector, selectorContainer);
            }

            /**
             * _createFilterContainer private function
             
             * @description
             *  In Thead of table, adds new row <tr> with class self.InstanceValues.Classes.FilterContainer.Row
             *     after cloning from header row in THead.
             *  In this new row, for each TH column,
             *     adds filter container div element with class self.InstanceValues.Classes.FilterContainer.Column        *
             * @returns Nothing
             */
            _createFilterContainer() {
                var self = this;
                var header = self._$$(self.InstanceValues.DataTableSettings.nTHead);
                var headerRowCloned = self._$$(self._$$(header).find('tr')).clone();
                var classesFilterContainer = self.InstanceValues.Classes.FilterContainer;
                var classFilterContainerRow = classesFilterContainer.Row;
                var classFilterContainerColumn = classesFilterContainer.Column;
                var classFilterControl = classesFilterContainer.Control;
                var classImage = self.InstanceValues.Classes.Common.Image;
                var classGridMenuButton = classesFilterContainer.GridMenuButton;
                var classesFilterImage = classImage + ' ' + classesFilterContainer.FilterImage;
                var classesArrowDownImage = classImage + ' ' + classesFilterContainer.ArrowDownImage;
                var classesGridMenuButton = classesFilterContainer.MenuButton + ' ' + classGridMenuButton;
                var classesInput = classesFilterContainer.Input;
                var attrNameOperator = ColumnFilter.DataAttributes.Operator;
                var sourceImages = ColumnFilter.ImageSources;
                var prefix = ColumnFilter.ImageSrcPrefix;
                var srcArrowDownImage = prefix + sourceImages.ArrowDownImage;
                var allColTypesArray = Object.values(ColumnFilter.ColumnTypes);
                var getFilterHtml = function (input) {
                    var html = '';
                    switch (input) {
                        case ColumnFilter.ColumnTypes.String: {
                            html = '<input type = "text" class = "' + classesInput + '" maxlength = "38" >';
                            break;
                        }
                        case ColumnFilter.ColumnTypes.Number: {
                            html = '<input type = "number" class = "' + classesInput + '" maxlength = "10" >';
                            break;
                        }
                        case ColumnFilter.ColumnTypes.Date: {
                            html = '<input type = "date" />';
                            break;
                        }
                    }
                    return html;
                };
                var showFilter = function (input, array) {
                    var returned = null;
                    for (var c = 0; c < array.length; c++) {
                        if (input != null) {
                            if (input.toString().indexOf(array[c]) > -1) {
                                returned = array[c];
                                break;
                            }
                        }
                    }
                    return returned;
                };
                headerRowCloned.find('th').each(function (colIdx) {
                    self._$$(this)
                        .removeClass()
                        .removeAttr('aria-controls')
                        .removeAttr('aria-label')
                        .removeAttr('aria-sort')
                        .html('');
                    
                    var input = ColumnFilter.ColumnTypes.String;
                    var cell = self.InstanceValues.Api.cell(0, colIdx);
                    var node = cell != null && cell.length > 0 && cell.node !== undefined ? cell.node() : null;
                    input = node != null && node != undefined ? node.className : null;

                    var colType = input !== null ? showFilter(input, allColTypesArray) : null;
                    if (colType !== null) {                    
                        var defaultOperatorConfig = self._getDefaultOperatorConfig(colType);
                        var defaultOperatorValue = defaultOperatorConfig.Value;
                        var defaultOperatorImageSrc = prefix + defaultOperatorConfig.ImageSrc;
                        var filterControlContainerColumn = $1(
                            '<div class = "' + classFilterContainerColumn + '" >' +
                                '<div class = "' + classFilterControl + '" >' +
                                     '<div title = "' + defaultOperatorConfig.DisplayName + '" class = "' + classesGridMenuButton + '" >' +
                                             '<img ' + attrNameOperator + ' = "' + defaultOperatorValue + '"' + ' class = "' + classesFilterImage + '" src = "' + defaultOperatorImageSrc + '" >' +
                                            '<img class = "' + classesArrowDownImage + '" src = "' + srcArrowDownImage + '">' +
                                     '</div>' +
                                        getFilterHtml(colType) +
                                '</div>' +
                            '</div>');
                        
                        filterControlContainerColumn
                            .appendTo(self._$$(this));
                        self._setEvents(filterControlContainerColumn, colType, colIdx);
                    }

                });
                headerRowCloned
                    .addClass(classFilterContainerRow)
                    .appendTo(self._$$(header));
            }

            /**
             * _setEvents private function
             * @param
             *   self : The reference to ColumnFilter function
             *   filterControlContainerColumn: (Type - self._$$(element)) The reference to newly added <div class="filter-container-column" > element in TH of new row
             *   colType: (Type - Enum) String/Boolean/Number/Date etc. Used to decide what operators will show in filter menu.
             *   colIdx: (Type - Integer) ColumnIndex of the column for which events are to be binded.
             * @description Sets following events
             *  On click of filterControlContainerColumn element in TH, opens the filter menu at that location.
             *  On click of operator from filter menu that opens in above step,
             *    closes the filter menu and sets src and data-operator of control in filterControlContainerColumn element
             *  On any change in textbox of filter, sets the following values in instance of datatable-
             *      - operator chosen from filter menu
             *      - value typed by user
             *       And calls .draw() method of table which internally calls the custom search function
             * On focus in in textbox of filter, closes the filter menu.
             * @returns Nothing
             */
            _setEvents(filterControlContainerColumn, colType, colIdx) {
                var self = this;
                var api = self.InstanceValues.Api;
                var selectorFilterImage = 'img' + '.' + self.InstanceValues.Classes.FilterContainer.FilterImage;
                var selectorRow = 'tr' + '.' + self.InstanceValues.Classes.FilterMenu.Row;
                var selectorGridMenuButton = 'div' + '.' + self.InstanceValues.Classes.FilterContainer.GridMenuButton;
                var attrNameOperator = ColumnFilter.DataAttributes.Operator;
                filterControlContainerColumn.find(selectorGridMenuButton)
                    .off("click.dtColumnFilter")
                    .on("click.dtColumnFilter", function (event) {
                        var filterImage = self._$$(this).find(selectorFilterImage);
                        var offset = filterImage.offset();
                        var isAlreadyOpened = self._isAlreadyOpened(colIdx);                        
                        var filterMenu = self._getFilterMenu(colType, isAlreadyOpened);                            
                        if (isAlreadyOpened) {
                            filterMenu.hide();
                            self._setMenuState(colIdx, false);
                        }
                        else {
                            filterMenu
                                .css({
                                    left: offset.left + 5,
                                    top: offset.top
                                })
                                .show();
                            var rows = filterMenu.find(selectorRow);
                            rows
                                     .off("click.dtColumnFilter")
                                     .on("click.dtColumnFilter", function (e) {
                                        var img = self._$$(this).find('img');
                                        var operatorValue = img.attr(attrNameOperator);
                                        var operatorDisplayName = self._getMatchingOperatorConfig(operatorValue).DisplayName;
                                        filterImage
                                            .attr('src', img.attr('src'))
                                            .attr(attrNameOperator, img.attr(attrNameOperator))
                                            .parent()
                                                .attr('title', operatorDisplayName);
                                        filterMenu
                                            .hide();
                                        self._setMenuState(colIdx, false);
                                        self.InstanceValues.SearchedOperatorType = operatorValue;
                                        self.InstanceValues.SearchedColType = colType;
                                        self.InstanceValues.SearchedColIndex = colIdx;
                                        api
                                            .draw();
                                    });
                                self._setMenuState(colIdx, true);
                        }
                        event.stopPropagation();
                    });
                filterControlContainerColumn
                    .find('input')
                    .off('focusin.dtColumnFilter')
                    .on('focusin.dtColumnFilter', function () {
                        self
                            ._getFilterMenu(colType, true)
                                .hide();
                        self.
                            _setMenuState(colIdx, false);
                    })
                    .off('keyup.dtColumnFilter change.dtColumnFilter clear.dtColumnFilter')
                    .on('keyup.dtColumnFilter change.dtColumnFilter clear.dtColumnFilter', function () {
                        self.InstanceValues.SearchedOperatorType = self._$$(this).parent().find(selectorFilterImage).attr(attrNameOperator);
                        self.InstanceValues.SearchedValue = this.value;
                        self.InstanceValues.SearchedColIndex = colIdx;
                        self.InstanceValues.SearchedColType = colType;
                        self
                            ._getFilterMenu(colType, true)
                                .hide();
                        self.
                            _setMenuState(colIdx, false);
                        api
                            .draw();
                    });
            }

            /**
             * _customizeSearch private function, called when .draw() is called on table.
             * @description Customizes default search in dataTables to search by using operators.
             * @returns Nothing
             */
            _customizeSearch() {
                var self = this;
                var operatorTypes = self._getOperators();
                var isValidDate = function(value) {
                    var dateWrapper = new Date(value);
                    return !isNaN(dateWrapper.getDate());
                }

                if ($1.fn.dataTable.ext.search.indexOf(self.InstanceValues.SearchFunction) === -1) {
                    // Custom search function
                    self.InstanceValues.SearchFunction = function (settings, searchData, rowIndex, origData, counter) {
                        var SearchedColIndex = self.InstanceValues.SearchedColIndex;
                        if (SearchedColIndex == null || SearchedColIndex == undefined) {
                            return true;
                        }
                        
                        var searchedOperatorType = self.InstanceValues.SearchedOperatorType;
                        var searchedColType = self.InstanceValues.SearchedColType;  
                        var searchedValue = self.InstanceValues.SearchedValue;
                        searchedValue = searchedValue === null ? '' : searchedValue.toString();
                        if (searchedValue === '') {
                            if (searchedOperatorType !== operatorTypes.IsEmpty && searchedOperatorType !== operatorTypes.IsNotEmpty) {
                                return true;
                            }
                        }
                        var returned = false;
                        var valueInDataObject = searchData[SearchedColIndex];
                        valueInDataObject = valueInDataObject === null ? '' : valueInDataObject.toString();
                        switch (searchedOperatorType) {
                            case operatorTypes.IsEmpty: {
                                if (valueInDataObject.toString().length === 0) {
                                    returned = true;
                                }
                                break;
                            }
                            case operatorTypes.IsNotEmpty: {
                                if (valueInDataObject.toString().length > 0) {
                                    returned = true;
                                }
                                break;
                            }
                            case operatorTypes.Contains: {
                                if (valueInDataObject.indexOf(searchedValue) > -1) {
                                    returned = true;
                                }
                                break;
                            }
                            case operatorTypes.DoesNotContain: {
                                if (valueInDataObject.indexOf(searchedValue) == -1) {
                                    returned = true;
                                }
                                break;
                            }
                            case operatorTypes.Equals: {
                                if (valueInDataObject === searchedValue) {
                                    returned = true;
                                }
                                break;
                            }
                            case operatorTypes.DoesNotEqual: {
                                if (valueInDataObject !== searchedValue) {
                                    returned = true;
                                }
                                break;
                            }
                            case operatorTypes.StartsWith: {
                                if (valueInDataObject.indexOf(searchedValue) == 0) {
                                    returned = true;
                                }
                                break;
                            }
                            case operatorTypes.EndsWith: {
                                var len1 = valueInDataObject.length;//14
                                var len2 = searchedValue.length;//4
                                //abcdefghijklmn 14 11 
                                //klmn 4

                                if (len1 - len2 === valueInDataObject.lastIndexOf(searchedValue) - 1) {
                                    returned = true;
                                }
                                break;
                            }
                            case operatorTypes.DoesNotStartWith: {
                                if (valueInDataObject.indexOf(searchedValue) !== 0) {
                                    returned = true;
                                }
                                break;
                            }
                            case operatorTypes.DoesNotEndWith: {
                                var len1 = valueInDataObject.length;//14
                                var len2 = searchedValue.length;//4
                                //abcdefghijklmn 14 11 
                                //klmn 4

                                if (len1 - len2 !== valueInDataObject.lastIndexOf(searchedValue) - 1) {
                                    returned = true;
                                }
                                break;
                            }
                            case operatorTypes.GreaterThan: {
                                if (valueInDataObject != null) {
                                    if (searchedColType === ColumnFilter.ColumnTypes.Number) {
                                        if (Number(searchedValue) < Number(valueInDataObject)) {
                                            returned = true;
                                        }
                                    }
                                    else if (searchedColType === ColumnFilter.ColumnTypes.Date) {
                                        if (isValidDate(valueInDataObject) && isValidDate(searchedValue)) {
                                            if (new Date(searchedValue).getTime() < new Date(valueInDataObject).getTime()) {
                                                returned = true;
                                            }
                                        }
                                    }
                                    
                                }
                                break;
                            }
                            case operatorTypes.GreaterThanOrEquals: {
                                if (valueInDataObject != null) {
                                    if (searchedColType === ColumnFilter.ColumnTypes.Number) {
                                        if (Number(searchedValue) <= Number(valueInDataObject)) {
                                            returned = true;
                                        }
                                    }
                                    else if (searchedColType === ColumnFilter.ColumnTypes.Date) {
                                        if (isValidDate(valueInDataObject) && isValidDate(searchedValue)) {
                                            if (new Date(searchedValue).getTime() <= new Date(valueInDataObject).getTime()) {
                                                returned = true;
                                            }
                                        }
                                    }
                                    
                                }
                                break;
                            }
                            case operatorTypes.LessThan: {
                                if (valueInDataObject != null) {
                                    if (searchedColType === ColumnFilter.ColumnTypes.Number) {
                                        if (Number(searchedValue) > Number(valueInDataObject)) {
                                            returned = true;
                                        }
                                    }
                                    else if (searchedColType === ColumnFilter.ColumnTypes.Date) {
                                        if (isValidDate(valueInDataObject) && isValidDate(searchedValue)) {
                                            if (new Date(searchedValue).getTime() > new Date(valueInDataObject).getTime()) {
                                                returned = true;
                                            }
                                        }
                                    }
                                    
                                }
                                break;
                            }
                            case operatorTypes.LessThanOrEquals: {
                                if (valueInDataObject != null) {
                                    if (searchedColType === ColumnFilter.ColumnTypes.Number) {
                                        if (Number(searchedValue) >= Number(valueInDataObject)) {
                                            returned = true;
                                        }
                                    }
                                    else if (searchedColType === ColumnFilter.ColumnTypes.Date) {
                                        if (isValidDate(valueInDataObject) && isValidDate(searchedValue)) {
                                            if (new Date(searchedValue).getTime() >= new Date(valueInDataObject).getTime()) {
                                                returned = true;
                                            }
                                        }
                                    }                                    
                                }
                                break;
                            }                            
                        }
                        return returned;
                    };
                    $1.fn.dataTable.ext.search.push(self.InstanceValues.SearchFunction);
                }
            }

            /**
             * _getFilterMenu private function
             * @param
             *  colType: (Type - Enum) String\Boolean\Number\DateTime etc
             *  hideExisting: (Type - Boolean) True\False. Default is false
             * @description
             *   Filter menu element is supposed to be present in document.
             *   When hideExisting is true, then it just finds the filter menu and hides it.
             *   When hideExisting is false (default), it just destroys previous instance of filter menu and
             *      creates fresh considering the operators corresponding to colType.
             * @returns
                Returns the filter menu element.
            */
            _getFilterMenu(colType, hideExisting = false) {
                var self = this;
                var classImage = self.InstanceValues.Classes.Common.Image;
                var classLabel = self.InstanceValues.Classes.Common.Label;
                var classesMenu = self.InstanceValues.Classes.FilterMenu;
                var classFilterMenu = classesMenu.MenuButtonMenu;
                var classPopUpContent = classesMenu.PopUpContent;
                var classRow = classesMenu.Row;
                var selectorContainer = self.InstanceValues.SelectorContainer;
                var selectorFilterMenu = 'div' + '.' + classFilterMenu;
                var filterMenu = self._$$(selectorFilterMenu);
                if (hideExisting === true) { //used when required to hide existing menu
                    return filterMenu;
                }
                // else destroy existing and create new
                if (filterMenu.length > 0) {
                    filterMenu.remove();
                }
                var prefix = ColumnFilter.ImageSrcPrefix;
                var operatorConfigurations = self._getOperatorConfigs(colType);
                
                var div1 = $1("<div/>");
                div1
                    .addClass(classesMenu.Main)
                    .addClass(classFilterMenu);
                var div2 =
                    $1("<div/>")
                        .addClass(classPopUpContent);
                var tbl = $1("<table/>");
                var tbody = $1("<tbody/>");
                for (var cnt = 0; cnt < operatorConfigurations.length; cnt++) {
                    var src = prefix + operatorConfigurations[cnt].ImageSrc;
                    var tr = $1("<tr/>")
                        .addClass(classRow);
                    var td1 = $1("<td/>");
                    var img = $1("<img/>");
                    img
                        .attr(ColumnFilter.DataAttributes.Operator, operatorConfigurations[cnt].Value)
                        .attr('src', src)
                        .addClass(classImage);
                    
                    td1
                        .append(img)
                        .appendTo(tr);
                    
                    var td2 = $1("<td/>");
                    var lbl = $1("<label/>")
                        .addClass(classLabel)
                        .text(operatorConfigurations[cnt].DisplayName);
                        
                    td2
                        .append(lbl)
                        .appendTo(tr);                    
                    tr.appendTo(tbody);
                }
                tbl
                    .append(tbody)
                    .appendTo(div2);
                
                div1
                    .append(div2)
                    .appendTo($1(selectorContainer));               
                return self._$$(selectorFilterMenu);
            }

            /**
             * _getOperatorConfigs private function
             * @param
             *  colType: (Type - Enum) Boolean/String/Date/Number etc
             * @description
             *   ColumnFilter.OperatorConfigurations contains list of operator details having
             *     Display name like "Does Not Equal"
             *     Value like "DoesNotEqual"
             *     ImageSrc for this operator
             *     AppliesTo (array of enum like Number\String etc)
             *     IsDefault - like Equals is considered as default operator for String.
             *   This method is used to get the list of operator configs for matching colType.
             * @returns
             *   Returns the list of ColumnFilter.OperatorConfigurations for the matched colType.
             */
            _getOperatorConfigs(colType) {
                var values = ColumnFilter.OperatorConfigurations;
                var returned = [];
                var doesExist = function (appliesTo, input) {                    
                    if (typeof appliesTo == 'string')
                        return appliesTo === input;
                    var val = false;
                    for (var a = 0; a < appliesTo.length; a++) {
                        if (appliesTo[a] == input) {
                            val = true;
                            break;
                        }
                    }
                    return val;
                }
                for (var cnt = 0; cnt < values.length; cnt++) {
                    if (doesExist(values[cnt].AppliesTo, colType)) {
                        returned.push(values[cnt]);
                    }
                }
                return returned;
            }

            /**
             * _getDefaultOperatorConfig private function
             * @param
             *  colType: (Type - Enum) Boolean/String/Date/Number etc
             * @description
             *   ColumnFilter.OperatorConfigurations contains list of operator details having
             *     Display name like "Does Not Equal"
             *     Value like "DoesNotEqual"
             *     ImageSrc for this operator
             *     AppliesTo (Array of enum like Number\String etc)
             *     IsDefault - like Equals is considered as default operator for String.
             *   This method is used to get the default operator config based upon colType.
             * @returns
             *   Returns the single object from ColumnFilter.OperatorConfigurations array
             *    where colType matches and IsDefault is true.
             */
            _getDefaultOperatorConfig(colType) {
                var self = this;
                var values = self._getOperatorConfigs(colType);
                var returned = null;
                for (var cnt = 0; cnt < values.length; cnt++) {
                    if (values[cnt].IsDefault === true) {
                        returned = values[cnt];
                        break;
                    }
                }
                return returned;
            }

            /**
             * _getMatchingOperatorConfig private function
             * @param
             *  value: The value field like 'Equals' or 'DoesNotEqual'
             * @description
             *   ColumnFilter.OperatorConfigurations contains list of operator details having
             *     Display name like "Does Not Equal"
             *     Value like "DoesNotEqual"
             *     ImageSrc for this operator
             *     AppliesTo (Array of enum like Number\String etc)
             *     IsDefault - like Equals is considered as default operator for String.
             *   This method is used to get the matching operator config based upon Value.
             * @returns
             *     Returns the single object from ColumnFilter.OperatorConfigurations array
             *          where input value matches Value
             */
            _getMatchingOperatorConfig(value) {
                var values = ColumnFilter.OperatorConfigurations;
                var returned = null;
                for (var cnt = 0; cnt < values.length; cnt++) {
                    if (values[cnt].Value === value) {
                        returned = values[cnt];
                        break;
                    }
                }
                return returned;
            }

            /**
             * _getOperators private function
             * @param none
             * @description
             *  Iterates the array of objects in ColumnFilter.OperatorConfigurations to form an object
             *   holding all values from property "Value" of each element in array.
             *   The returned object is used in switch case.       *
             * @returns object with following structure { Equals: 'Equals', DoesNotEqual:'DoesNotEqual'.... }
             */
            _getOperators() {
                var values = ColumnFilter.OperatorConfigurations;
                var returned = {};
                for (var cnt = 0; cnt < values.length; cnt++) {
                    returned[values[cnt].Value] = values[cnt].Value;
                }
                return returned;
            }

            _isAlreadyOpened(colIndex) {
                if (this.menuState.ColIndex == colIndex) {
                    return this.menuState.Opened;
                }
                return false;
            }

            _setMenuState(colIndex, state) {
                this.menuState.ColIndex = colIndex;
                this.menuState.Opened = state;
            }

            menuState = {
                ColIndex: -1,
                Opened: false
            };
        }

        ColumnFilter.defaults = {
            IsCaseSensitive: false
        };

        ColumnFilter.ColumnTypes = {
            String: 'string',
            Enum: 'enum',
            Number: 'numeric',
            DateTime: 'datetime',
            Date: 'date',
            Boolean: 'bool'
        };

        ColumnFilter.DataAttributes = {
            Operator: 'data-operator'
        };

        ColumnFilter.ImageSrcPrefix = 'data:image/png;base64,';
        
        ColumnFilter.ImageSources = {
            ArrowDownImage: 'iVBORw0KGgoAAAANSUhEUgAAAAYAAAAGCAYAAADgzO9IAAAAKElEQVR42mNgwAKMjIwKsAoaGxv/xyqIIoEsCJdAFwRLAAUL0QVBGACRih4O/Qn69wAAAABJRU5ErkJggg==',
            CalendarImage:'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAASElEQVQoU2NkQID/DAwMjEh8GBNFHFkB0RpACkEApBnGRrYIWZwRhUOqBizOxxCC24DNs+iqwX6EOWnEaQCFBrYIQw8lcMAAANQVGA1kMf1wAAAAAElFTkSuQmCC'
        };

        ColumnFilter.OperatorConfigurations = [
            {
                DisplayName: 'Equals',
                Value: 'Equals',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAKUlEQVQoU2NkIBEwkqieAaThPyma6KOBFBeB/UASoI8fSA5Wkv1AkgYAaUQGB3BV678AAAAASUVORK5CYII=',
                AppliesTo: [ColumnFilter.ColumnTypes.String, ColumnFilter.ColumnTypes.Number, ColumnFilter.ColumnTypes.Date, ColumnFilter.ColumnTypes.Enum],
                IsDefault: true
            },
            {
                DisplayName: 'Does Not Equal',
                Value: 'DoesNotEqual',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAKCAYAAACALL/6AAAAmklEQVQoU3XQMQ4BURSF4W961Gql2IUFkOiwAZvQqBUajUZBorYM0dqBRCMisQDymJGZl3m3e+fd/9x7bqa+xthgjmW5JUsAJzTQjf8D8I7EQjtigBva+JrHQHhvMUIzMvsD8dQnHujUrRtn2GGCKfYpIGR4oYUD+rl70Mr5KhmKSXcssEoc45c8rzVm6OGSOHcFOOOKYao56B8RnBsLly5g2gAAAABJRU5ErkJggg==',
                AppliesTo: [ColumnFilter.ColumnTypes.String, ColumnFilter.ColumnTypes.Number, ColumnFilter.ColumnTypes.Date, ColumnFilter.ColumnTypes.Enum],
                IsDefault: false
            },
            {
                DisplayName: 'Is Empty',
                Value: 'IsEmpty',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAKCAYAAACALL/6AAAA10lEQVQoU03RzSoGUBAG4OdzJ/ZK2cglyG9fSNnYSNb+cwGfv2yVlSwIhfKTG1BSVi7AlYjma07mbObMnHln3vO+Hf9nDhsYzdIb9vFUenQyOcQm7nGBH3SxjBOsN1AA5nGDJVyWaSs4y3wSz3EPwBe+MV2at5POWlIcxkgD/GKqcN1BDxEPMI6XHN7fEIAovmIrm3ZzQwydwUMFfCatd5yiNgfgEYMYapQmCp3V8tF4X8QVFnDbABGP0oNz3GEgJZ3FcUre16T5EPfYtIexVOsjPbiuxv0BAqQqCzsWwWQAAAAASUVORK5CYII=',
                AppliesTo: [ColumnFilter.ColumnTypes.String, ColumnFilter.ColumnTypes.Date],
                IsDefault: false
            },
            {
                DisplayName: 'Is Not Empty',
                Value: 'IsNotEmpty',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAKCAYAAACALL/6AAAAv0lEQVQoU53RMUqDQRCG4ee/ioVVbCTiFYIgWiQhMUUsPEK8hKg3CKRSNIGQgEiOkEAqOwvxJob9mcUljeBWu9/MO/PNbOX3tDHCaUhrPGBa5KjicYdbzDEO7QaXuI9YLSegg1cM8FRWQz+0lDPLwAe+cIFDTAK6xicWOMBRBn7QwgpbHAeQ7s2IvYeb2tJfwBneSiBZ+sZ5YSkVGoalZVhq7A99hee9oXuhdfN6/7XWXDStLv3FSQgbPOKl7LoDktEqCwY7bOUAAAAASUVORK5CYII=',
                AppliesTo: [ColumnFilter.ColumnTypes.String, ColumnFilter.ColumnTypes.Date],
                IsDefault: false
            },
            {
                DisplayName: 'Contains',
                Value: 'Contains',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAKCAYAAACALL/6AAAAWklEQVQoU2NkgIB3DAwMQlA2OoUix8jAwPCfgYEBRMPAVwYGBlYGBgY2JDG4GnQNIAlkADMIrwYMRciuGBgbCDoJ5MnPDAwMvDiCFUUOOThxqMcebEQpBikCAGNxGQvc6pUZAAAAAElFTkSuQmCC',
                AppliesTo: ColumnFilter.ColumnTypes.String,
                IsDefault: false
            },
            {
                DisplayName: 'Does Not Contain',
                Value: 'DoesNotContain',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAKCAYAAACALL/6AAAAb0lEQVQoU2NkgID/DAwMjFC2LwMDw2oGBgYOKB+FAilCVgyS3A/V7IBFDmwquoYfDAwM4QwMDBuJ0YDuHHTDMGxAdg6638B+QXcSsnMIasAWOnidhO4cnDbAwhnknFAGBobNWOIGHhewyMIWR1jFAJ9gHAsLojsAAAAAAElFTkSuQmCC',
                AppliesTo: ColumnFilter.ColumnTypes.String,
                IsDefault: false
            },
            {
                DisplayName: 'Ends With',
                Value: 'EndsWith',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAKCAYAAACALL/6AAAATUlEQVQoU5WQQQoAIAgEp/7/5Sj2YJhR4p50dURtFNWADgzHyfOawPYUeEOxZA0xvwo22QZdC/vCKz6gH6DGdKV4k0HH0aXHxhemcBlYN9YQCTNWpPMAAAAASUVORK5CYII=',
                AppliesTo: [ColumnFilter.ColumnTypes.String, ColumnFilter.ColumnTypes.Number],
                IsDefault: false
            },
            {
                DisplayName: 'Does Not End With',
                Value: 'DoesNotEndWith',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAKCAYAAACALL/6AAAAf0lEQVQoU5XQsQ0CQQxE0Xd90ACVEEILpER0giiBCiC+EhA5BVAI6E67K+NbCeFwxt8ee7CsLfbYdTxDEd/BfGKDV9Amf+6tQPVOOCa9DlsAcUtv8zw0Gncc8Ej61ykROJc4LW+6r0VaYcS6NExGD2rAFRfcem/MWv7ST+Zv4AOtRBYJkwlDuAAAAABJRU5ErkJggg==',
                AppliesTo: [ColumnFilter.ColumnTypes.String, ColumnFilter.ColumnTypes.Number],
                IsDefault: false
            },
            {
                DisplayName: 'Starts With',
                Value: 'StartsWith',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAAVUlEQVQoU6WRAQoAIQgEx17ex48L4xRPipCESMOdtRKKIcV+rgUd0GXRgCfUkh2s2fYXfArNZxGJ8UpR5OcnwaR+YHdYUTNdwVtBHvHncv2sx38sOwx2MxAI0jEbYAAAAABJRU5ErkJggg==',
                AppliesTo: [ColumnFilter.ColumnTypes.String, ColumnFilter.ColumnTypes.Number],
                IsDefault: false
            },
            {
                DisplayName: 'Does Not Start With',
                Value: 'DoesNotStartWith',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAKCAYAAACALL/6AAAAf0lEQVQoU43RvQ0CMQwF4C97sACTXAkrXEvFJogRmABqRkDXMwCDgHI6IyviUNzF78d+TtFXO4zYl8R/o30H/MSA1y9C7lXBCccwq2B1bitEGZt7/wSV/MABU54Q7uGW3c/LOt98a6E3uGO7uMUmpQ0Y06644LYWru83mrt3iT4PWRYLJmYTUgAAAABJRU5ErkJggg==',
                AppliesTo: [ColumnFilter.ColumnTypes.String, ColumnFilter.ColumnTypes.Number],
                IsDefault: false
            },            
            {
                DisplayName: 'Greater Than',
                Value: 'GreaterThan',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAKCAYAAACALL/6AAAAPElEQVQoU2NkgID/UBpGMaLx4VxkCaI0oZtEUBM2q/FqwuVWnJootoEkPxBUDApbmJOIUoysAVc8YYgDANpoCQuFGdK/AAAAAElFTkSuQmCC',
                AppliesTo: [ColumnFilter.ColumnTypes.Number, ColumnFilter.ColumnTypes.Date],
                IsDefault: false
            },
            {
                DisplayName:'Greater Than or Equals',
                Value: 'GreaterThanOrEquals',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAMCAYAAABWdVznAAAASElEQVQoU2NkQID/SGwQkxGND+aiCxLUhM0UvJqwWsvAwIBTEy4NIOdi1UQVG0jyA0mhRFAxzsjBFmEwMWRPo9uArg+slmQNAKgtCw0+5tAJAAAAAElFTkSuQmCC',
                AppliesTo: [ColumnFilter.ColumnTypes.Number, ColumnFilter.ColumnTypes.Date],
                IsDefault: false
            },
            {
                DisplayName: 'Less Than',
                Value: 'LessThan',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAKCAYAAACALL/6AAAAO0lEQVQoU2NkwAT/0YQYkfkoHAYGBryKQRqRNRBUjKyBKMUwDUQrJtsGWCAQZRNFoUSUTeg2YIlHVCEA7mgJC1XpL5gAAAAASUVORK5CYII=',
                AppliesTo: [ColumnFilter.ColumnTypes.Number, ColumnFilter.ColumnTypes.Date],
                IsDefault: false
            },
            {
                DisplayName: 'Less Than or Equals',
                Value: 'LessThanOrEquals',
                ImageSrc: 'iVBORw0KGgoAAAANSUhEUgAAAAwAAAAKCAYAAACALL/6AAAAPklEQVQoU2NkQAX/0fiMaHwGZAGCikGaYRqIUgzTQLRism2A+Ysom9BDgaAmjGBDD0ZswYpuKrIarPFAkgYArFAJC+eFcMEAAAAASUVORK5CYII=',
                AppliesTo: [ColumnFilter.ColumnTypes.Number, ColumnFilter.ColumnTypes.Date],
                IsDefault: false
            }
        ];
        
	    // Attach a listener to the document which listens for DataTables initialisation
	    // events so we can automatically initialise
        $1(document).on('preInit.dt.dtColumnFilter', function (e, settings, json) {
            if (e.namespace !== 'dt') {
	            return;
            }
            var init = settings.oInit.columnFilter;
            var defaults = dataTable.defaults.columnFilter;

            if ( init || defaults ) {
                var opts = $1.extend( {}, init, defaults );

                if ( init !== false ) {
                    new ColumnFilter( settings, opts );
                }
            }
        });        
       
        /**
            * Version information
            *
            * @name ColumnFilter.version
            * @static
        */
       ColumnFilter.version = '1.0.0';

       $1.fn.DataTable.ColumnFilter = ColumnFilter;
       $1.fn.dataTable.ColumnFilter = ColumnFilter;

    return ColumnFilter;
}));

