﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using IEMQSImplementation;

namespace IEMQS.WCFService
{
    [ServiceContract]
    public interface IIEMQSService
    {
        [OperationContract]
        string DoWork();

        [OperationContract]
        string GetWinNotification(string psno);
    }
}
