﻿using System.Web.Mvc;

namespace IEMQS.Areas.CTQ
{
    public class CTQAreaRegistration : AreaRegistration 
    {
        public override string AreaName
        {
            get
            {
                return "CTQ";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "CTQ_default",
                "CTQ/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}