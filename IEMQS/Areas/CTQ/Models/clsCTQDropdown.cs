﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.CTQ.Models
{
    public class clsCTQDropdown
    {
        public string t_entu { get; set; }
        public string t_cprj { get; set; }
        public int t_dtyp { get; set; }
        public int CTQRev { get; set; }
    }
}