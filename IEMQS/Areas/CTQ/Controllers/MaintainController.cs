﻿
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;

namespace IEMQS.Areas.CTQ.Controllers
{
    public class MaintainController : clsBase
    {


        /// <summary>
        /// Created By :Nikita Vibhandik
        /// Created Date :27/06/2017
        /// Description: Maintain CTQ Form 
        /// </summary>
        /// <returns></returns>
        // GET: CTQ/MaintainCTQ
        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]

        #region Main Form (Maintain CTQ)
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult CTQDetails(int? id)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("CTQ001");
            CTQ001 objCTQ001 = new CTQ001();
            var user = objClsLoginInfo.UserName;
            var project = Manager.getProjectsByUser(user);
            ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
            //header id for view mode
            if (id != null)
            {
                var objHeader = Convert.ToInt32(id);
                string draftStatus = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                string returnedStatus = clsImplementationEnum.CTQStatus.Returned.GetStringValue();

                objCTQ001 = db.CTQ001.Where(x => x.HeaderId == objHeader).FirstOrDefault();
                objCTQ001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objCTQ001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objCTQ001.Project = db.COM001.Where(i => i.t_cprj == objCTQ001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objCTQ001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objCTQ001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.Project = objCTQ001.Project;
                ViewBag.Action = "edit";
            }
            else
            {
                objCTQ001.Status = "Draft";
                objCTQ001.CTQRev = 0;
                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1 && a.t_actv == 1
                                && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx + "-" + b.t_desc).FirstOrDefault();
                objCTQ001.Location = location;
            }
            return View(objCTQ001);
        }

        //[HttpPost]
        public ActionResult GetActionByDept(string term, string location)
        {
            var lstActionByDesc = (from ctq003 in db.CTQ003
                                   join com002 in db.COM002 on ctq003.Department equals com002.t_dimx
                                   where ctq003.Location == location && ((ctq003.Department.Contains(term)) || (com002.t_desc.Contains(term)))
                                   select new { deptCode = ctq003.Department, deptDescription = ctq003.Department + "-" + com002.t_desc }).Distinct().ToList();
            return Json(lstActionByDesc, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult GetActionByDept1(string data)
        {
            var location = data;
            var lstActionByDesc = (from ctq003 in db.CTQ003
                                   join com002 in db.COM002 on ctq003.Department equals com002.t_dimx
                                   where ctq003.Location == location
                                   select new { id = ctq003.Department, text = ctq003.Department + "-" + com002.t_desc }).Distinct().ToList();
            return Json(lstActionByDesc, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public bool checkProjectExist(string projectcode, string location)
        {
            bool Flag = false;
            string locationCode = location.Split('-')[0].Trim();
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                CTQ001 objCTQ001 = db.CTQ001.Where(x => x.Project == projectcode && x.Location == locationCode).FirstOrDefault();
                if (objCTQ001 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public bool checkActionByDeptExist(string ActionByDeptcode, string location)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(ActionByDeptcode))
            {
                CTQ003 objCTQ003 = db.CTQ003.Where(x => x.Department == ActionByDeptcode && x.Location == location).FirstOrDefault();
                if (objCTQ003 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }


        [HttpPost]
        public ActionResult GetBU(string projectcode)
        {
            ResponceMsgProjectBU objResponseMsg = new ResponceMsgProjectBU();
            CTQ001 objCTQ001 = new CTQ001();
            string Project = projectcode.Split('-')[0].Trim();
            string BUid = db.COM001.Where(i => i.t_cprj == Project).FirstOrDefault().t_entu;
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == BUid
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            objResponseMsg.BU = BUDescription.BUDesc;
            objResponseMsg.Project = Project;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetProject()
        {
            CTQ001 objCTQ001 = new CTQ001();
            var user = objClsLoginInfo.UserName;
            var project = Manager.getProjectsByUser(user);
            ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
            return Json(objCTQ001, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SaveCTQHeader(FormCollection fc, CTQ001 ctq001)
        {
            CTQ001 objCTQ001 = new CTQ001();
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            string Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
            try
            {
                string location = fc["Location"].Split('-')[0].Trim();
                objCTQ001.Project = ctq001.Project;
                objCTQ001.BU = fc["BU"].Split('-')[0].Trim();
                objCTQ001.Location = location;
                objCTQ001.CTQNo = ctq001.CTQNo;
                objCTQ001.CTQRev = 0;
                objCTQ001.Status = Status;
                objCTQ001.CreatedOn = DateTime.Now;
                objCTQ001.CreatedBy = objClsLoginInfo.UserName;
                db.CTQ001.Add(objCTQ001);
                db.SaveChanges();
                
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert;
                objResponseMsg.HeaderId = objCTQ001.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Copy data
        [HttpPost]
        public ActionResult GetCopyDetails(string projectid) //partial for CTQ Lines Details
        {
            //List<Projects> project = Manager.getProjectsByUser(objClsLoginInfo.UserName);
            //ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
            return PartialView("_CopyProjectPartial");
        }

        [HttpPost]
        public ActionResult copyCTQ(string projCode, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1 && a.t_actv == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();


                CTQ001 objCTQ001 = db.CTQ001.Where(x => x.Project == projCode && x.Location == currentLoc).FirstOrDefault();
                CTQ001 objExistingCTQ001 = db.CTQ001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objCTQ001 != null)
                {
                    if (objCTQ001.Location == currentLoc)
                    {
                        if (objCTQ001.Status == clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() || objCTQ001.Status == clsImplementationEnum.CTQStatus.Returned.GetStringValue())
                        {
                            objResponseMsg = InsertNewCTQ(objExistingCTQ001, objCTQ001, true);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "CTQ already exists.";
                        }
                    }
                    else
                    {
                        CTQ001 objDesCTQ001 = new CTQ001();
                        string BU = db.COM001.Where(i => i.t_cprj == projCode).FirstOrDefault().t_entu;
                        objDesCTQ001.Project = projCode;
                        objDesCTQ001.BU = BU;
                        objDesCTQ001.Location = currentLoc;
                        objDesCTQ001.CTQNo = projCode + "_CTQ";
                        objResponseMsg = InsertNewCTQ(objExistingCTQ001, objDesCTQ001, false);
                    }
                }
                else
                {
                    CTQ001 objDesCTQ001 = new CTQ001();
                    string BU = db.COM001.Where(i => i.t_cprj == projCode).FirstOrDefault().t_entu;
                    objDesCTQ001.Project = projCode;
                    objDesCTQ001.BU = BU;
                    objDesCTQ001.Location = currentLoc;
                    objDesCTQ001.CTQNo = projCode + "_CTQ";
                    objResponseMsg = InsertNewCTQ(objExistingCTQ001, objDesCTQ001, false);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg InsertNewCTQ(CTQ001 objSrcCTQ001, CTQ001 objDesCTQ001, bool IsEdited)
        {
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            string user_dept = (from deptr in db.COM003
                                where deptr.t_psno == objClsLoginInfo.UserName && deptr.t_actv == 1
                                select deptr.t_depc).FirstOrDefault();

            var dept = (from a in db.COM002
                        where a.t_dimx == user_dept
                        select new { a.t_dimx }).FirstOrDefault();
            var department = dept.t_dimx;

            try
            {

                if (IsEdited)
                {
                    #region old code
                    //objDesCTQ001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    //objDesCTQ001.CTQRev = 0;
                    //objDesCTQ001.EditedBy = objClsLoginInfo.UserName;
                    //objDesCTQ001.EditedOn = DateTime.Now;
                    //List<CTQ002> lstDesCTQ001 = db.CTQ002.Where(x => x.HeaderId == objDesCTQ001.HeaderId).ToList();

                    //if (lstDesCTQ001 != null && lstDesCTQ001.Count > 0)
                    //{
                    //    db.CTQ002.RemoveRange(lstDesCTQ001);
                    //}
                    //db.SaveChanges();
                    //List<CTQ002> lstSrcCTQ002 = db.CTQ002.Where(x => x.HeaderId == objSrcCTQ001.HeaderId).ToList();
                    //db.CTQ002.AddRange(
                    //                    lstSrcCTQ002.Select(x =>
                    //                    new CTQ002
                    //                    {
                    //                        HeaderId = objDesCTQ001.HeaderId,
                    //                        Project = objDesCTQ001.Project,
                    //                        BU = objDesCTQ001.BU,
                    //                        Location = objDesCTQ001.Location,
                    //                        CTQRev = Convert.ToInt32(objDesCTQ001.CTQRev),
                    //                        LineRev = 0,
                    //                        Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(),
                    //                        CTQDesc = x.CTQDesc,
                    //                        ActionBy = x.ActionBy,
                    //                        ActionDesc = x.ActionDesc,
                    //                        Department = department,
                    //                        Specification = x.Specification,
                    //                        CSRMOMDoc = x.CSRMOMDoc,
                    //                        ClauseNo = x.ClauseNo,
                    //                        CreatedBy = objClsLoginInfo.UserName,
                    //                        CreatedOn = DateTime.Now,
                    //                    })
                    //                  );
                    //db.SaveChanges();
                    #endregion

                    #region new changes 25/08/17
                    CTQ002 objDesCTQ002 = new CTQ002();
                    var lstsrcCTQ002 = db.CTQ002.Where(x => x.HeaderId == objSrcCTQ001.HeaderId && x.Department == department).ToList();
                    objDesCTQ002 = db.CTQ002.Where(i => i.HeaderId == objDesCTQ001.HeaderId).FirstOrDefault();
                    if (lstsrcCTQ002.Any())
                    {
                        foreach (var item in lstsrcCTQ002)
                        {
                            objDesCTQ002 = new CTQ002();
                            objDesCTQ002.HeaderId = objDesCTQ001.HeaderId;
                            objDesCTQ002.Project = objDesCTQ001.Project;
                            objDesCTQ002.BU = objDesCTQ001.BU;
                            objDesCTQ002.Location = objDesCTQ001.Location;
                            objDesCTQ002.CTQRev = Convert.ToInt32(objDesCTQ001.CTQRev);
                            objDesCTQ002.LineRev = 0;
                            objDesCTQ002.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                            objDesCTQ002.CTQDesc = item.CTQDesc;
                            objDesCTQ002.ActionBy = item.ActionBy;
                            objDesCTQ002.ActionDesc = item.ActionDesc;
                            objDesCTQ002.Department = department;
                            objDesCTQ002.Specification = item.Specification;
                            objDesCTQ002.CSRMOMDoc = item.CSRMOMDoc;
                            objDesCTQ002.ClauseNo = item.ClauseNo;
                            objDesCTQ002.CreatedBy = objClsLoginInfo.UserName;
                            objDesCTQ002.CreatedOn = DateTime.Now;
                            db.CTQ002.Add(objDesCTQ002);
                            db.SaveChanges();
                        }
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Details not available for your department";
                    }
                    #endregion
                }
                else
                {
                    objDesCTQ001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    objDesCTQ001.CTQRev = 0;
                    objDesCTQ001.CreatedBy = objClsLoginInfo.UserName;
                    objDesCTQ001.CreatedOn = DateTime.Now;
                    db.CTQ001.Add(objDesCTQ001);
                    db.SaveChanges();
                    #region Add new lines
                    List<CTQ002> lstSrcCTQ002 = db.CTQ002.Where(x => x.HeaderId == objSrcCTQ001.HeaderId && x.Department == department).ToList();
                    if (lstSrcCTQ002 != null && lstSrcCTQ002.Count > 0)
                    {
                        db.CTQ002.AddRange(
                                        lstSrcCTQ002.Select(x =>
                                        new CTQ002
                                        {
                                            HeaderId = objDesCTQ001.HeaderId,
                                            Project = objDesCTQ001.Project,
                                            BU = objDesCTQ001.BU,
                                            Location = objDesCTQ001.Location,
                                            CTQRev = Convert.ToInt32(objDesCTQ001.CTQRev),
                                            LineRev = 0,
                                            Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(),
                                            CTQDesc = x.CTQDesc,
                                            ActionBy = x.ActionBy,
                                            ActionDesc = x.ActionDesc,
                                            Department = department,
                                            Specification = x.Specification,
                                            CSRMOMDoc = x.CSRMOMDoc,
                                            ClauseNo = x.ClauseNo,
                                            CreatedBy = objClsLoginInfo.UserName,
                                            CreatedOn = DateTime.Now,
                                        })
                                      );
                    }

                    db.SaveChanges();
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
                }
                objResponseMsg.HeaderId = objDesCTQ001.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return objResponseMsg;
        }

        #endregion

        #region Tab
        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadCTQHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1 && a.t_actv == 1
                                && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx).FirstOrDefault();

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (ctq1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or CTQNo like '%" + param.sSearch + "%' or CTQRev like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhere += Manager.MakeDefaultWhere(user, "ctq1.BU", "ctq1.Location");
                var lstResult = db.SP_CTQ_GETHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.CTQNo),
                           Convert.ToString("R"+uc.CTQRev),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.Location),
                           //Convert.ToString(uc.HeaderId),
                           "<center>"
                          + Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/CTQ/Maintain/CTQDetails?Id="+uc.HeaderId,false)
                          + HTMLActionString(uc.HeaderId,uc.Status,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/CTQ/Maintain/ShowTimeline?HeaderID="+ uc.HeaderId +"\");") +"   "
                          + Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Detail", "fa fa-print", "PrintReport("+uc.HeaderId+")","",false)
                          //+ "<i title='Print'  class='iconspace fa fa-print' onClick='PrintReport("+uc.HeaderId+")'></i>"+" "
                          + HTMLActionString(uc.HeaderId,"","HistoryCTQ","History","fa fa-history","HistoryCTQ(\""+ uc.HeaderId +"\", \"Maintain\");",!(uc.CTQRev > 0 || uc.Status.Equals(clsImplementationEnum.CTQStatus.Approved.GetStringValue()))) +"</center>",


                           Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Datatable functions for CTQ Lines
        public JsonResult LoadCTQLinesData(JQueryDataTableParamModel param)
        {
            try
            {
                //CTQ002 objCTQ002 = db.CTQ002.Where(x => x.HeaderId == param.CTQLineHeaderId).FirstOrDefault();
                //string Department = "";
                //if (objCTQ002 != null)
                //{
                //    Department = objCTQ002.Department;
                //}
                //else
                //{ Department = "0"; }
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string condition = string.Empty;
                condition += "where HeaderId='" + param.CTQLineHeaderId + "' and Department='" + objClsLoginInfo.Department + "' ";
                string SendForCompile = clsImplementationEnum.CTQStatus.SendForCompile.GetStringValue();
                bool isSend = db.CTQ002.Where(x => x.HeaderId == param.CTQLineHeaderId && x.Status == SendForCompile && x.Department == objClsLoginInfo.Department).Any(); 
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and (Specification like '%" + param.sSearch + "%' or CSRMOMDoc like '%" + param.sSearch + "%' or CSRMOMDoc like '%" + param.sSearch + "%' or ClauseNo like '%" + param.sSearch + "%' or CTQRev like '%" + param.sSearch + "%'  or ActionBy like '%" + param.sSearch + "%'  or ActionDesc like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    condition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstCTQ002 = db.SP_CTQ_GET_CTQ_LINE_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, condition
                                ).ToList();

                var data = (from uc in lstCTQ002
                            select new[]
                              {
                                      Convert.ToString(uc.LineId),
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Specification),
                                Convert.ToString(uc.CSRMOMDoc),
                                Convert.ToString(uc.ClauseNo),
                                Convert.ToString(uc.CTQDesc),
                                Convert.ToString(uc.ActionDesc),
                                Convert.ToString(getActiondeptName(uc.ActionBy)),
                            
                                //Convert.ToString(uc.Department),
                                Convert.ToString(uc.ReturnRemark),
                                Convert.ToString(uc.Status),
                                Convert.ToString("R"+uc.LineRev),
                                Convert.ToString(uc.CreatedBy),
                                       Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                                //Convert.ToString(uc.LineId),
                                "<center><nobr>" + HTMLActionString(uc.LineId,uc.Status,"Edit","Edit Record","fa fa-pencil-square-o", (uc.Status == clsImplementationEnum.CTQStatus.Approved.GetStringValue() ? "createRevision("+ uc.LineId +");" : "getDetails("+ uc.LineId +");"), (uc.Status == clsImplementationEnum.CTQStatus.SendForCompile.GetStringValue() || uc.Status == clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() ? true : false))
                                +"   "+HTMLActionString(uc.LineId,uc.Status,"Delete","Delete Record","fa fa-trash-o", "DeleteRecord("+ uc.LineId +");", (uc.Status == clsImplementationEnum.CTQStatus.SendForCompile.GetStringValue() || uc.Status == clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() || uc.Status == clsImplementationEnum.CTQStatus.Approved.GetStringValue() ? true : false))
                                +"   "+HTMLActionString(uc.LineId,uc.Status,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/CTQ/Maintain/ShowTimeline?HeaderID="+ uc.LineId +"&LineId="+ uc.LineId +"\");")
                                + "</nobr></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstCTQ002.Count > 0 && lstCTQ002.FirstOrDefault().TotalCount > 0 ? lstCTQ002.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstCTQ002.Count > 0 && lstCTQ002.FirstOrDefault().TotalCount > 0 ? lstCTQ002.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition,
                    isSend = isSend
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region CTQ Deatils Form 
        public string getActiondeptName(string act)
        {
            string actionbydept = string.Empty;
            List <string> lstActionbydept = new List<string>();
            var depts = act.Split(',');
            foreach (var item in depts)
            {
                string userdept = (from deptr in db.COM002
                                   where deptr.t_dimx == item
                                   select deptr.t_dimx + "" + " - " + "" + deptr.t_desc).FirstOrDefault();
                if (userdept != null && userdept != "")
                {
                    lstActionbydept.Add( userdept );
                }
                else
                {
                    lstActionbydept.Add(item);
                }
            }
            if (lstActionbydept.Count > 0) {
                actionbydept = "* " + string.Join(",", lstActionbydept.ToList()).Replace(",", "<br/> * ");
            }
            return actionbydept;
        }
         
        [SessionExpireFilter]
        [HttpPost]
        public ActionResult GetCTQDetails(string projectid, string BU, string location, string ctq, int LineId) //partial for CTQ Lines Details
        {
            CTQ002 objCTQ002 = new CTQ002();
            CTQ001 objCTQ001 = new CTQ001();
            //user department
            string userdept = (from deptr in db.COM003
                               where deptr.t_psno == objClsLoginInfo.UserName && deptr.t_actv == 1
                               select deptr.t_depc).FirstOrDefault();

            var department = (from a in db.COM002
                              where a.t_dimx == userdept
                              select new { a.t_dimx, Desc = a.t_dimx + "" + " - " + "" + a.t_desc }).FirstOrDefault();

            string strLoc = location.Split('-')[0];
            string strProject = projectid.Split('-')[0];

            objCTQ001 = db.CTQ001.Where(x => x.Project == strProject && x.Location == strLoc).FirstOrDefault();
            if (LineId > 0)
            {
                objCTQ002 = db.CTQ002.Where(x => x.LineId == LineId).FirstOrDefault();
                objCTQ002.CTQ001.CTQRev = objCTQ002.CTQRev;
                objCTQ002.HeaderId = objCTQ001.HeaderId;
                objCTQ002.Project = projectid;
                var headerdept = (from a in db.COM002
                                  where a.t_dimx == objCTQ002.Department
                                  select new { a.t_dimx, Desc = a.t_dimx + "" + " - " + "" + a.t_desc }).FirstOrDefault();

                objCTQ002.Department = headerdept.Desc;
                //objCTQ002.ActionBy = db.COM002.Where(i => i.t_dimx == objCTQ002.ActionBy).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.ActionBy = objCTQ002.ActionBy;
                ViewBag.Action = "line edit";
            }
            else
            {
                objCTQ002.CTQRev = Convert.ToInt32(objCTQ001.CTQRev);
                objCTQ002.LineRev = 0;
                objCTQ002.HeaderId = objCTQ001.HeaderId;
                objCTQ002.Project = projectid;
                objCTQ002.Department = department.Desc;
                ViewBag.Action = "";
            }
            ViewBag.Project = projectid;
            ViewBag.BU = BU;
            ViewBag.Location = location;
            ViewBag.CTQNo = ctq;


            return PartialView("_CTQDetailsHtmlPartial", objCTQ002);
        }

        //save ctq Lines
        [HttpPost]
        public ActionResult SaveCTQLines(FormCollection fc, CTQ002 ctq002)
        {
            CTQ002 objCTQ002 = new CTQ002();
            CTQ001 objCTQ001 = new CTQ001();

            //status from enum
            string draftStatus = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int headerID = ctq002.HeaderId;
                objCTQ001 = db.CTQ001.Where(x => x.HeaderId == headerID).FirstOrDefault();
                if (ctq002.LineId > 0)
                {
                    var ctqstatus = (from header in db.CTQ002
                                     where header.LineId == ctq002.LineId
                                     select header.Status).FirstOrDefault();

                    objCTQ002 = db.CTQ002.Where(x => x.LineId == ctq002.LineId).FirstOrDefault();
                    objCTQ002.HeaderId = headerID;
                    objCTQ002.Project = fc["Project"].Split('-')[0].Trim();
                    objCTQ002.BU = fc["BU"].Split('-')[0].Trim();
                    objCTQ002.Location = fc["Location"].Split('-')[0].Trim();
                    //Status wise filter
                    if (ctqstatus == clsImplementationEnum.CTQStatus.Approved.GetStringValue())
                    {
                        objCTQ002.CTQRev = Convert.ToInt32(objCTQ001.CTQRev) + 1;
                        objCTQ001.CTQRev = Convert.ToInt32(objCTQ001.CTQRev) + 1;
                        objCTQ002.LineRev = Convert.ToInt32(objCTQ002.LineRev) + 1;
                    }
                    if (ctqstatus != clsImplementationEnum.CTQStatus.Returned.GetStringValue())
                    {
                        objCTQ002.Status = draftStatus;
                        objCTQ001.Status = draftStatus;
                    }
                    objCTQ002.CTQDesc = ctq002.CTQDesc;
                    objCTQ002.ActionBy = fc["ddlActionBy"].Split('-')[0].Trim();
                    //objCTQ002.ActionBy = fc["ddlActionBy"];
                    objCTQ002.ActionDesc = ctq002.ActionDesc;
                    objCTQ002.Department = fc["Department"].Split('-')[0].Trim();
                    objCTQ002.Specification = ctq002.Specification;
                    objCTQ002.CSRMOMDoc = ctq002.CSRMOMDoc;
                    objCTQ002.ClauseNo = ctq002.ClauseNo;
                    objCTQ002.EditedBy = objClsLoginInfo.UserName;
                    objCTQ002.EditedOn = DateTime.Now;
                    objCTQ001.EditedBy = objClsLoginInfo.UserName;
                    objCTQ001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    objCTQ002.HeaderId = headerID;
                    objCTQ002.Project = fc["Project"].Split('-')[0].Trim();
                    objCTQ002.BU = fc["BU"].Split('-')[0].Trim();
                    objCTQ002.Location = fc["Location"].Split('-')[0].Trim();
                    objCTQ002.CTQRev = Convert.ToInt32(objCTQ001.CTQRev);
                    objCTQ002.CTQDesc = ctq002.CTQDesc;
                    //objCTQ002.ActionBy = fc["ActionBy"].Split('-')[0];
                    objCTQ002.ActionBy = fc["ddlActionBy"];
                    objCTQ002.ActionDesc = ctq002.ActionDesc;
                    objCTQ002.Department = fc["Department"].Split('-')[0].Trim();
                    objCTQ002.Specification = ctq002.Specification;
                    objCTQ002.CSRMOMDoc = ctq002.CSRMOMDoc;
                    objCTQ002.ClauseNo = ctq002.ClauseNo;
                    if (objCTQ001.Status == clsImplementationEnum.CTQStatus.Approved.GetStringValue())
                    {
                        objCTQ002.Status = draftStatus;
                        objCTQ001.Status = draftStatus;
                        objCTQ001.CTQRev = Convert.ToInt32(objCTQ001.CTQRev) + 1;
                    }
                    else if (objCTQ001.Status == clsImplementationEnum.CTQStatus.Returned.GetStringValue())
                    {
                        objCTQ002.Status = draftStatus;
                        objCTQ001.Status = draftStatus;
                    }
                    else
                    {
                        objCTQ002.Status = draftStatus;
                    }
                    objCTQ002.LineRev = 0;
                    objCTQ002.CreatedBy = objClsLoginInfo.UserName;
                    objCTQ002.CreatedOn = DateTime.Now;
                    db.CTQ002.Add(objCTQ002);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                if (ctq002.LineId > 0)
                {
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update;
                }
                else
                {
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Insert;
                }
                objResponseMsg.Status = objCTQ001.Status;
                objResponseMsg.CTQRev = Convert.ToInt32(objCTQ001.CTQRev);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //send Ctq lines to compiler
        [HttpPost]
        public ActionResult SendtoCompiler(List<int> strSelectedLines)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string draftStatus = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
            string compileStatus = clsImplementationEnum.CTQStatus.SendForCompile.GetStringValue();
            string returnedStatus = clsImplementationEnum.CTQStatus.Returned.GetStringValue();



            foreach (var itm in strSelectedLines)
            {
                try
                {
                    CTQ002 objCTQ002 = db.CTQ002.Where(u => u.LineId == itm).SingleOrDefault();
                    string location = objCTQ002.Location;
                    objCTQ002.Status = compileStatus;
                    objCTQ002.SendToCompiledBy = objClsLoginInfo.UserName;
                    objCTQ002.SendToCompiledOn = DateTime.Now;
                    objCTQ002.SendToCompiler = true;
                    db.SaveChanges();

                    int headerID = (from header in db.CTQ002
                                    where header.LineId == objCTQ002.LineId
                                    select header.HeaderId).FirstOrDefault();
                    CTQ001 objCTQ001 = db.CTQ001.Where(u => u.HeaderId == headerID).SingleOrDefault();

                    int lstHeader = db.CTQ002.Where(p => p.HeaderId == headerID).Count();
                    int lstStatus = db.CTQ002.Where(p => (p.HeaderId == headerID) && (p.Status != draftStatus && p.Status != returnedStatus)).Count();

                    //dept qise ctq003
                    var lstCtq003 = (from dbo in db.CTQ003
                                     where dbo.Location == objCTQ001.Location && dbo.BU == objCTQ001.BU && dbo.IsMandatory == true
                                     select dbo.Department.Trim()).Distinct().ToList();

                    var lstCtq002 = (from dbo in db.CTQ002
                                     where dbo.HeaderId == headerID
                                     select dbo.Department.Trim()).Distinct().ToList();
                    bool isCompile = true;
                    foreach (var lst in lstCtq003)
                    {
                        if (!lstCtq002.Contains(lst))
                        {
                            isCompile = false;
                        }
                    }

                    if (isCompile == true)
                    {
                        if (lstHeader == lstStatus)
                        {
                            objCTQ001.Status = compileStatus;
                            db.SaveChanges();

                            #region Send Notification
                            (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue()+","+ clsImplementationEnum.UserRoleName.QA3.GetStringValue(), objCTQ001.Project, objCTQ001.BU, objCTQ001.Location, "CTQ " + objCTQ001.CTQNo + " has been sent to Compile", clsImplementationEnum.NotificationType.Information.GetStringValue());
                            #endregion
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.CompilerAction.ToString();
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
                }
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult pendingdept(int headerID)
        {
            CTQ001 objCTQ001 = db.CTQ001.Where(u => u.HeaderId == headerID).SingleOrDefault();
            var lstCtq003 = (from dbo in db.CTQ003
                             where dbo.Location == objCTQ001.Location && dbo.BU == objCTQ001.BU && dbo.IsMandatory == true
                             select dbo.Department.Trim()).Distinct().ToList();

            var lstCtq002 = (from dbo in db.CTQ002
                             where dbo.HeaderId == headerID
                             select dbo.Department.Trim()).Distinct().ToList();
            string strDept = string.Empty;
            string deptDescription;
            foreach (var lst in lstCtq003)
            {
                if (!lstCtq002.Contains(lst))
                {
                    deptDescription = db.COM002.Where(a => a.t_dimx == lst).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                    if (strDept.Length == 0)
                    {
                        strDept += deptDescription;
                    }
                    else
                    {
                        strDept += "," + deptDescription;
                    }
                }
            }
            string[] dept = null;
            if (!string.IsNullOrWhiteSpace(strDept))
            {
                dept = strDept.Split(',');
                ViewBag.Deprtment = dept.ToList();
            }
            else
            {
                dept = null;
                ViewBag.Deprtment = null;
            }
            return PartialView("~/Areas/CTQ/Views/Shared/_PendingDeptPartial.cshtml");
        }

        //delete ctq lines
        [HttpPost]
        public ActionResult DeleteCTQLines(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CTQ002 objCTQ002 = db.CTQ002.Where(x => x.LineId == Id).FirstOrDefault();
                db.CTQ002.Remove(objCTQ002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region History Details
        [HttpPost]
        public ActionResult GetCTQHistoryDetails(int HeaderId, string _FromController) //partial for CTQ History Lines Details
        {
            CTQ001_Log objCTQ001_Log = new CTQ001_Log();
            objCTQ001_Log = db.CTQ001_Log.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            ViewBag.FromController = _FromController;
            return PartialView("_CTQHistoryDetailsHtmlPartial", objCTQ001_Log);
        }

        public ActionResult CTQLogDetails(int? Id, string from)
        {
            CTQ001_Log objLogCTQ001 = new CTQ001_Log();

            objLogCTQ001 = db.CTQ001_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objLogCTQ001 != null)
            {
                string strBU = db.COM001.Where(i => i.t_cprj == objLogCTQ001.Project).FirstOrDefault().t_entu;
                objLogCTQ001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == strBU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objLogCTQ001.Project = db.COM001.Where(i => i.t_cprj == objLogCTQ001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objLogCTQ001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objLogCTQ001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            ViewBag.FromController = from;
            return View(objLogCTQ001);
        }
        #endregion

        #region Datatable functions for CTQ History
        [HttpPost]
        public JsonResult LoadCTQHistoryHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                string sqlQuery = string.Empty;
                sqlQuery += "where HeaderId='" + param.CTQHeaderId + "'";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    sqlQuery += " and (Project like '%" + param.sSearch + "%' or CTQNo like '%" + param.sSearch + "%' or CTQRev like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    sqlQuery += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstlogCTQ002 = db.SP_CTQ_HEADER_HISTORY_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, sqlQuery
                                ).ToList();

                var data = (from uc in lstlogCTQ002
                            select new[]
                              {
                             Convert.ToString(uc.Project),
                           Convert.ToString(uc.CTQNo),
                           Convert.ToString("R"+uc.CTQRev),
                           Convert.ToString(uc.Status),
                           Convert.ToString(uc.Id),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstlogCTQ002.Count > 0 && lstlogCTQ002.FirstOrDefault().TotalCount > 0 ? lstlogCTQ002.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstlogCTQ002.Count > 0 && lstlogCTQ002.FirstOrDefault().TotalCount > 0 ? lstlogCTQ002.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = sqlQuery
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LoadCTQHistoryLinesData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string condition = string.Empty;
                condition += "where RefId=" + param.CTQHeaderId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and (Specification like '%" + param.sSearch + "%' or CSRMOMDoc like '%" + param.sSearch + "%' or CSRMOMDoc like '%" + param.sSearch + "%' or ClauseNo like '%" + param.sSearch + "%' or CTQRev like '%" + param.sSearch + "%'  or ActionBy like '%" + param.sSearch + "%'  or ActionDesc like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    condition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstlogCTQ002 = db.SP_CTQ_LINE_HISTORY_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, condition
                                ).ToList();

                var data = (from uc in lstlogCTQ002
                            select new[]
                              {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Specification),
                                Convert.ToString(uc.CSRMOMDoc),
                                Convert.ToString(uc.ClauseNo),
                                Convert.ToString(uc.CTQDesc),
                                Convert.ToString(uc.ActionDesc),
                                Convert.ToString(getActiondeptName(uc.ActionBy)),
                                Convert.ToString(uc.Department),
                                Convert.ToString(uc.ReturnRemark),
                                Convert.ToString(uc.Status),
                                Convert.ToString("R"+uc.LineRev),
                                Convert.ToString(uc.CreatedBy),
                                         Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                                "<center>" + HTMLActionString(uc.Id,uc.Status,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/CTQ/Maintain/ShowHistoryTimeline?HeaderID="+ uc.Id +"&LineId="+ uc.Id +"\");") + "</center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstlogCTQ002.Count > 0 && lstlogCTQ002.FirstOrDefault().TotalCount > 0 ? lstlogCTQ002.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstlogCTQ002.Count > 0 && lstlogCTQ002.FirstOrDefault().TotalCount > 0 ? lstlogCTQ002.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Timeline
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "CTQ Timeline";

            if (LineId > 0)
            {
                model.Title = "CTQ Lines";
                CTQ002 objCTQ002 = db.CTQ002.Where(x => x.LineId == LineId).FirstOrDefault();
                model.CreatedBy = objCTQ002.CreatedBy != null ? Manager.GetUserNameFromPsNo(objCTQ002.CreatedBy) : null;
                model.CreatedOn = objCTQ002.CreatedOn;
                model.EditedBy = objCTQ002.EditedBy != null ? Manager.GetUserNameFromPsNo(objCTQ002.EditedBy) : null;
                model.EditedOn = objCTQ002.EditedOn;
                model.SubmittedBy = objCTQ002.SendToCompiledBy != null ? Manager.GetUserNameFromPsNo(objCTQ002.SendToCompiledBy) : null;
                model.SubmittedOn = objCTQ002.SendToCompiledOn;
                model.CompiledBy = objCTQ002.CompiledBy != null ? Manager.GetUserNameFromPsNo(objCTQ002.CompiledBy) : null;
                model.CompiledOn = objCTQ002.CompiledOn;
                model.ApprovedBy = objCTQ002.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objCTQ002.ApprovedBy) : null;
                model.ApprovedOn = objCTQ002.ApprovedOn;

            }
            else
            {
                model.Title = "CTQ Header";
                CTQ001 objCTQ001 = db.CTQ001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objCTQ001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objCTQ001.CreatedBy) : null;
                model.CreatedOn = objCTQ001.CreatedOn;
                model.EditedBy = objCTQ001.EditedBy != null ? Manager.GetUserNameFromPsNo(objCTQ001.EditedBy) : null;
                model.EditedOn = objCTQ001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "CTQ Timeline";

            if (LineId > 0)
            {
                model.Title = "CTQ Lines";
                CTQ002_Log objCTQ002 = db.CTQ002_Log.Where(x => x.Id == LineId).FirstOrDefault();
                model.CreatedBy = objCTQ002.CreatedBy != null ? Manager.GetUserNameFromPsNo(objCTQ002.CreatedBy) : null;
                model.CreatedOn = objCTQ002.CreatedOn;
                model.EditedBy = objCTQ002.EditedBy != null ? Manager.GetUserNameFromPsNo(objCTQ002.EditedBy) : null;
                model.EditedOn = objCTQ002.EditedOn;
                model.SubmittedBy = objCTQ002.SendToCompiledBy != null ? Manager.GetUserNameFromPsNo(objCTQ002.SendToCompiledBy) : null;
                model.SubmittedOn = objCTQ002.SendToCompiledOn;
                model.CompiledBy = objCTQ002.CompiledBy != null ? Manager.GetUserNameFromPsNo(objCTQ002.CompiledBy) : null;
                model.CompiledOn = objCTQ002.CompiledOn;
                model.ApprovedBy = objCTQ002.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objCTQ002.ApprovedBy) : null;
                model.ApprovedOn = objCTQ002.ApprovedOn;

            }
            else
            {
                model.Title = "CTQ Header";
                CTQ001_Log objCTQ001 = db.CTQ001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objCTQ001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objCTQ001.CreatedBy) : null;
                model.CreatedOn = objCTQ001.CreatedOn;
                model.EditedBy = objCTQ001.EditedBy != null ? Manager.GetUserNameFromPsNo(objCTQ001.EditedBy) : null;
                model.EditedOn = objCTQ001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion

        #region ExcelHelper

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload, FormCollection fc)
        {

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            //Traveler - Template Ref Document with example
            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);

                bool isError;
                string project = fc["ProjectNo"];
                DataSet ds = ToAddRefDoc(package, project, out isError);
                if (!isError)
                {
                    try
                    {
                        //import Line Data
                        List<TVL002> lstTVL002 = new List<TVL002>();
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            TVL002 obj = new TVL002();
                            string proj = item.Field<string>("Project No").ToString();
                            obj.ProjectNo = db.TVL001.Where(x => x.ProjectNo.Trim().ToLower() == proj.Trim().ToLower()).Select(x => x.ProjectNo).FirstOrDefault();
                            obj.RefDocument = item.Field<string>("Ref Document").ToString().Trim();
                            obj.RefDocRevNo = Convert.ToInt32(item.Field<string>("Rev No"));
                            obj.PlanningRemarks = item.Field<string>("Description").ToString();
                            obj.DocType = getcategoryData("Traveler Doc Type", item.Field<string>("Doc Type"));
                            obj.IssueDate = Convert.ToDateTime(item.Field<string>("Date"));
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;

                            lstTVL002.Add(obj);
                        }
                        db.TVL002.AddRange(lstTVL002);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Ref Doc imported successfully";
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                else
                {
                    objResponseMsg = ExportToExcel(ds);
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Excel file is not valid";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public clsHelper.ResponceMsgWithFileName ExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/TRAVELER/Traveler - Error Template Ref Document.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            //operation no.
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(6)))
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(6).ToString() + " )";
                                excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }

                            //activity
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(7)))
                            {
                                excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(7).ToString() + " )";
                                excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }

                            //ref document
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(8)))
                            {
                                excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>(8).ToString() + " )";
                                excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }

                            //rev no
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(9)))
                            {
                                excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>(9).ToString() + " )";
                                excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }

                            //date
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(10)))
                            {
                                excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>(10).ToString() + " )";
                                excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }

                            //description
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(11)))
                            {
                                excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>(11).ToString() + " )";
                                excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }

                            i++;
                        }

                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public string getcategoryData(string category, string data)
        {
            string intervention = (from glb2 in db.GLB002
                                   join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                   where glb1.Category == category && glb2.Description == data
                                   select glb2.Description).FirstOrDefault();
            return intervention;
        }

        public DataSet ToAddRefDoc(ExcelPackage package, string project, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dt = new DataTable();
            isError = false;
            try
            {
                #region Validation for Header
                #endregion

                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dt.Columns.Add(firstRowCell.Text);
                }
                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dt.NewRow();
                    if (excelWorksheet.Cells[rowNumber, 1].Value != null
                       || excelWorksheet.Cells[rowNumber, 2].Value != null
                       || excelWorksheet.Cells[rowNumber, 3].Value != null
                       )
                    {
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                    }
                    else
                    { break; }
                    dt.Rows.Add(newRow);
                }
                #endregion

                #region Validation for Doc
                dt.Columns.Add("ProjectNoErrorMsg", typeof(string));
                dt.Columns.Add("DocTypeErrorMsg", typeof(string));
                dt.Columns.Add("RefDocumentErrorMsg", typeof(string));
                dt.Columns.Add("RefDocRevErrorMsg", typeof(string));
                dt.Columns.Add("DateErrorMsg", typeof(string));
                dt.Columns.Add("DescErrorMsg", typeof(string));

                IQueryable<CategoryData> lstDocType = (from glb2 in db.GLB002
                                                       join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                                       where glb1.Category == "Traveler Doc Type"
                                                       select glb2).Select(i => new { i.Description }).Distinct().Select(i => new CategoryData { Description = i.Description }).Distinct();

                foreach (DataRow item in dt.Rows)
                {
                    string errorMessage = string.Empty;
                    int RevNo;
                    DateTime IssueDate;
                    //project no
                    string projectno = item.Field<string>("Project No");
                    if (string.IsNullOrWhiteSpace(projectno))
                    {
                        item["ProjectNoErrorMsg"] = "Project No is Required";
                        isError = true;
                    }
                    else
                    {
                        if (projectno.Trim() != project.Trim())
                        {
                            item["ProjectNoErrorMsg"] = "Please enter valid Project";
                            isError = true;
                        }
                        //if (projectno.Trim().Length > 12)
                        //{
                        //    item["ProjectNoErrorMsg"] = "Project No have maximum limit of 12 characters";
                        //    isError = true;
                        //}
                        //else
                        //{
                        //    bool isProjectExist = db.TVL001.Any(x => x.ProjectNo.Trim() == projectno.Trim());
                        //    if (!isProjectExist)
                        //    {
                        //        item["ProjectNoErrorMsg"] = "Project No  is Invalid";
                        //        isError = true;
                        //    }
                        //}
                    }

                    //doc type
                    string doctype = item.Field<string>("Doc Type");
                    if (string.IsNullOrWhiteSpace(doctype))
                    {
                        item["DocTypeErrorMsg"] = "Doc Type is Required";
                        isError = true;
                    }
                    else
                    {
                        if (!lstDocType.Any(x => x.Description.ToLower() == doctype.ToLower()))
                        {
                            item["DocTypeErrorMsg"] = "Doc Type is Invalid"; isError = true;
                        }
                    }

                    //ref doc
                    string Refdoc = item.Field<string>("Ref Document");
                    int? RefRev = Convert.ToInt32(item.Field<string>("Rev No"));
                    bool isExist = false;

                    if (string.IsNullOrWhiteSpace(Refdoc))
                    {
                        item["RefDocumentErrorMsg"] = "Ref Document is Required";
                        isError = true;
                    }
                    else
                    {
                        isExist = db.TVL002.Where(x => x.RefDocument.Trim() == Refdoc.Trim() && x.RefDocRevNo == RefRev).Any();
                        if (Refdoc.Trim().Length > 100)
                        {
                            item["RefDocumentErrorMsg"] = "Ref Doc have maximum limit of 100 characters";
                            isError = true;
                        }
                        if (isExist)
                        {
                            item["RefDocumentErrorMsg"] += "Ref Doc is already exists";
                            isError = true;
                        }
                    }

                    //ref doc rev
                    if (!int.TryParse(item.Field<string>("Rev No"), out RevNo))
                    {
                        item["RefDocRevErrorMsg"] = item.Field<string>("Rev No") + " is Not Valid, Please Enter Only Numeric Value";
                        isError = true;
                    }

                    //issue date
                    if (!DateTime.TryParse(item.Field<string>("Date"), out IssueDate))
                    {
                        item["DateErrorMsg"] = item.Field<string>("Date") + " is Not Valid, Please Enter Proper Date(dd/mm/yyyy).";
                        isError = true;
                    }

                    if (string.IsNullOrWhiteSpace(item.Field<string>("Description")))
                    {
                        item["DescErrorMsg"] = "Description is Required";
                        isError = true;
                    }
                    else
                    {
                        if (item.Field<string>("Description").Trim().Length > 300)
                        {
                            item["DescErrorMsg"] = "Description have maximum limit of 300 characters";
                            isError = true;
                        }
                    }
                }
                #endregion

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dt);

            return ds;
        }

        #endregion

        [NonAction]
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (disabled)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;opacity: 0.5;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;margin-left:5px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";

            return htmlControl;
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                #region Maintain header
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_CTQ_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Location = li.Location,
                                      BU = li.BU,
                                      CTQNo = li.CTQNo,
                                      CTQRev = li.CTQRev,
                                      CreatedBy = li.CreatedBy,
                                      EditedBy = li.EditedBy,
                                      Status = li.Status
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_CTQ_GET_CTQ_LINE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      CTQRev = li.CTQRev,
                                      LineRev = li.LineRev,
                                      Status = li.Status,
                                      SrNo = li.SrNo,
                                      Specification = li.Specification,
                                      CSRMOMDoc = li.CSRMOMDoc,
                                      ClauseNo = li.ClauseNo,
                                      CTQDesc = li.CTQDesc,
                                      Department = li.Department,
                                      ActionDesc = li.ActionDesc,
                                      ActionBy = li.ActionBy,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      ReturnRemark = li.ReturnRemark,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                #endregion

                #region Maintain header
                if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_CTQ_HEADER_HISTORY_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Location = li.Location,
                                      BU = li.BU,
                                      CTQNo = li.CTQNo,
                                      CTQRev = li.CTQRev,
                                      CreatedBy = li.CreatedBy,
                                      EditedBy = li.EditedBy,
                                      Status = li.Status
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_CTQ_LINE_HISTORY_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      CTQRev = li.CTQRev,
                                      LineRev = li.LineRev,
                                      Status = li.Status,
                                      SrNo = li.SrNo,
                                      Specification = li.Specification,
                                      CSRMOMDoc = li.CSRMOMDoc,
                                      ClauseNo = li.ClauseNo,
                                      CTQDesc = li.CTQDesc,
                                      Department = li.Department,
                                      ActionDesc = li.ActionDesc,
                                      ActionBy = li.ActionBy,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      ReturnRemark = li.ReturnRemark,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public class ResponceMsgWithStatus : clsHelper.ResponseMsg
        {
            public int CTQRev;
            public string Status;
        }
        public class ResponceMsgWithHeaderId : clsHelper.ResponseMsg
        {
            public int HeaderId;
        }
        public class ResponceMsgProjectBU : clsHelper.ResponseMsg
        {
            public string BU;
            public string Project;
        }


    }
}

