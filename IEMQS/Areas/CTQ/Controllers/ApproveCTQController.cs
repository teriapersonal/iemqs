﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.CTQ.Controllers
{
    public class ApproveCTQController : clsBase
    {
        /// <summary>
        /// Added by Dharmesh For Make Approve CTQ
        /// </summary>
        /// <returns></returns>
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult CTQLineDetails(string ID)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("CTQ001");
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(ID))
            {
                HeaderId = Convert.ToInt32(ID);
            }
            CTQ001 objCTQ001 = db.CTQ001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objCTQ001 != null)
            {
                ViewBag.HeaderID = objCTQ001.HeaderId;
                ViewBag.Status = objCTQ001.Status;
                ViewBag.CTQNo = objCTQ001.CTQNo;

                objCTQ001.Project = Convert.ToString(db.COM001.Where(x => x.t_cprj == objCTQ001.Project).Select(x => x.t_cprj + " - " + x.t_dsca).FirstOrDefault());
                objCTQ001.BU = Convert.ToString(db.COM002.Where(x => x.t_dimx == objCTQ001.BU).Select(x => x.t_dimx + " - " + x.t_desc).FirstOrDefault());
                objCTQ001.Location = Convert.ToString(db.COM002.Where(x => x.t_dimx == objCTQ001.Location).Select(x => x.t_dimx + " - " + x.t_desc).FirstOrDefault());
            }
            return View(objCTQ001);
        }

        [HttpPost]
        public JsonResult LoadCTQHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (ctq1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or CTQNo like '%" + param.sSearch + "%' or CTQRev like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "ctq1.BU", "ctq1.Location");
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_CTQ_GETHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.CTQNo),
                           Convert.ToString("R"+uc.CTQRev),
                            Convert.ToString(uc.Status),
                           Convert.ToString(uc.Location),
                           "<center>"
                            + Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/CTQ/ApproveCTQ/CTQLineDetails?Id="+uc.HeaderId,false)
                            +HTMLActionString(uc.HeaderId,uc.Status,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/CTQ/Maintain/ShowTimeline?HeaderID="+ uc.HeaderId +"\");")+"   "
                            + Helper.GenerateActionIcon(uc.HeaderId, "Print", "Print Detail", "fa fa-print", "PrintReport("+uc.HeaderId+")","",false)
                            +HTMLActionString(uc.HeaderId,"","HistoryCTQ","History","fa fa-history","HistoryCTQ(\""+ uc.HeaderId +"\", \"ApproveCTQ\");",!(uc.CTQRev > 0 || uc.Status.Equals(clsImplementationEnum.CTQStatus.Approved.GetStringValue())))+"</center>",//Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.HeaderId),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadCTQHeaderLinesData(JQueryDataTableParamModel param)
        {
            try
            {
                string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() };
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                int ctqHeaderId = Convert.ToInt32(param.CTQHeaderId);
                CTQ001 objCTQ001 = db.CTQ001.FirstOrDefault(x => x.HeaderId == ctqHeaderId);

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Status like '%" + param.sSearch + "%' or Specification like '%" + param.sSearch + "%' or CSRMOMDoc like '%" + param.sSearch + "%' or ClauseNo like '%" + param.sSearch + "%' or CTQDesc like '%" + param.sSearch + "%' or Department like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_CTQ_GET_HEADER_LINES
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                //  (arrayCTQStatus.Contains(Convert.ToString(uc.Status))? "<input type='text' name='txtReturnRemark' class='form-control input-sm input-small input-inline' value='"+Convert.ToString(uc.ReturnRemark)+"' />":Convert.ToString(uc.ReturnRemark)),
                               Convert.ToString(uc.ROW_NO),
                               Convert.ToString(uc.Specification),
                               Convert.ToString(uc.CSRMOMDoc),
                               Convert.ToString(uc.ClauseNo),
                               Convert.ToString(uc.CTQDesc),
                               Convert.ToString(uc.ActionDesc),
                               Convert.ToString((new MaintainController()).getActiondeptName(uc.ActionBy)),
                               GetRemarkStyle(objCTQ001.Status,uc.ReturnRemark),
                               Convert.ToString(uc.Status),
                               Convert.ToString("R"+uc.LineRev),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.HeaderId),
                               "<center>" + HTMLActionString(uc.LineId, uc.Status, "Timeline", "Timeline", "fa fa-clock-o", "ShowTimeline(\"/CTQ/Maintain/ShowTimeline?HeaderID=" + uc.HeaderId + "&LineId=" + uc.LineId + "\");") + "</center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetRemarkStyle(string status, string returnRemarks)
        {

            if (status == clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue())
            {
                return "<input type='text' maxlength='100' name='txtReturnRemark' class='form-control input-sm input-small input-inline' value='" + returnRemarks + "' />";

            }
            else
            {
                return "<input type='text' readonly= 'readonly' name='txtReturnRemark' class='form-control input-sm input-small input-inline' value='" + returnRemarks + "' />";
            }
        }

        [HttpPost]
        public ActionResult GetCTQHeaderLinesHtml(int HeaderId, string strCTQNo)
        {
            ViewBag.HeaderId = HeaderId;
            ViewBag.strCTQNo = strCTQNo;
            ViewBag.CTQStatus = db.CTQ001.Where(x => x.HeaderId == HeaderId).Select(x => x.Status).FirstOrDefault();
            return PartialView("_GetCTQHeaderLinesHtml");
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public ActionResult UpdateReturnRemarks(int LineID, string changeText)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CTQ002 objCTQ002 = db.CTQ002.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objCTQ002 != null)
                {
                    if (!string.IsNullOrWhiteSpace(changeText))
                    {
                        objCTQ002.ReturnRemark = changeText;
                    }
                    else
                    {
                        objCTQ002.ReturnRemark = null;
                    }
                    objCTQ002.CompiledBy = objClsLoginInfo.UserName;
                    objCTQ002.CompiledOn = DateTime.Now;
                    objCTQ002.EditedBy = objClsLoginInfo.UserName;
                    objCTQ002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line remarks successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Line not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            for (int i = 0; i < arrayHeaderIds.Length; i++)
            {
                int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg ApproveHeaderByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue().ToUpper();
                List<CTQ002> lstCTQ002 = db.CTQ002.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper().Trim() == Status.ToUpper().Trim()).ToList();
                List<CTQ002_Log> lstCTQ002_Log = new List<CTQ002_Log>();
                if (lstCTQ002 != null && lstCTQ002.Count > 0)
                {
                    foreach (var objData in lstCTQ002)
                    {
                        CTQ002 objCTQ002 = lstCTQ002.Where(x => x.LineId == objData.LineId).FirstOrDefault();

                        objCTQ002.Status = clsImplementationEnum.CTQStatus.Approved.GetStringValue();
                        objCTQ002.ApprovedBy = objClsLoginInfo.UserName;
                        objCTQ002.ApprovedOn = DateTime.Now;
                        objCTQ002.EditedBy = objClsLoginInfo.UserName;
                        objCTQ002.EditedOn = DateTime.Now;

                        #region Line Log

                        CTQ002_Log objCTQ002_Log = new CTQ002_Log();

                        objCTQ002_Log.LineId = objCTQ002.LineId;
                        objCTQ002_Log.HeaderId = objCTQ002.HeaderId;
                        objCTQ002_Log.LineRev = objCTQ002.LineRev;
                        objCTQ002_Log.Status = objCTQ002.Status;
                        objCTQ002_Log.SrNo = objCTQ002.SrNo;
                        objCTQ002_Log.Specification = objCTQ002.Specification;
                        objCTQ002_Log.CSRMOMDoc = objCTQ002.CSRMOMDoc;
                        objCTQ002_Log.ClauseNo = objCTQ002.ClauseNo;
                        objCTQ002_Log.CTQDesc = objCTQ002.CTQDesc;
                        objCTQ002_Log.Department = objCTQ002.Department;
                        objCTQ002_Log.CreatedBy = objCTQ002.CreatedBy;
                        objCTQ002_Log.CreatedOn = objCTQ002.CreatedOn;
                        objCTQ002_Log.EditedBy = objCTQ002.EditedBy;
                        objCTQ002_Log.EditedOn = objCTQ002.EditedOn;
                        objCTQ002_Log.SendToCompiledBy = objCTQ002.SendToCompiledBy;
                        objCTQ002_Log.SendToCompiledOn = objCTQ002.SendToCompiledOn;
                        objCTQ002_Log.CompiledBy = objCTQ002.CompiledBy;
                        objCTQ002_Log.CompiledOn = objCTQ002.CompiledOn;
                        objCTQ002_Log.ApprovedBy = objCTQ002.ApprovedBy;
                        objCTQ002_Log.ApprovedOn = objCTQ002.ApprovedOn;
                        objCTQ002_Log.ReturnRemark = objCTQ002.ReturnRemark;
                        objCTQ002_Log.Project = objCTQ002.Project;
                        objCTQ002_Log.BU = objCTQ002.BU;
                        objCTQ002_Log.Location = objCTQ002.Location;
                        objCTQ002_Log.CTQRev = objCTQ002.CTQRev;
                        objCTQ002_Log.ActionDesc = objCTQ002.ActionDesc;
                        objCTQ002_Log.ActionBy = objCTQ002.ActionBy;
                        objCTQ002_Log.SendToCompiler = objCTQ002.SendToCompiler;
                        lstCTQ002_Log.Add(objCTQ002_Log);
                        #endregion

                    }
                    CTQ001 objCTQ001 = db.CTQ001.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();

                    objCTQ001.Status = clsImplementationEnum.CTQStatus.Approved.GetStringValue();
                    objCTQ001.EditedBy = objClsLoginInfo.UserName;
                    objCTQ001.EditedOn = DateTime.Now;

                    #region Header Log
                    CTQ001_Log objCTQ001_Log = db.CTQ001_Log.Where(x => x.HeaderId == objCTQ001.HeaderId).FirstOrDefault();

                    if (objCTQ001_Log == null)
                    {
                        objCTQ001_Log = new CTQ001_Log();
                        objCTQ001_Log.HeaderId = objCTQ001.HeaderId;
                        objCTQ001_Log.Project = objCTQ001.Project;
                        objCTQ001_Log.BU = objCTQ001.BU;
                        objCTQ001_Log.Location = objCTQ001.Location;
                        objCTQ001_Log.CTQRev = objCTQ001.CTQRev;
                        objCTQ001_Log.CTQNo = objCTQ001.CTQNo;
                        objCTQ001_Log.Status = objCTQ001.Status;
                        objCTQ001_Log.CreatedBy = objCTQ001.CreatedBy;
                        objCTQ001_Log.CreatedOn = objCTQ001.CreatedOn;
                        objCTQ001_Log.EditedBy = objCTQ001.EditedBy;
                        objCTQ001_Log.EditedOn = objCTQ001.EditedOn;
                        db.CTQ001_Log.Add(objCTQ001_Log);
                    }
                    else
                    {
                        objCTQ001_Log.Status = clsImplementationEnum.CTQStatus.Superseded.GetStringValue();
                        CTQ001_Log objCTQ001_Logs = new CTQ001_Log();
                        objCTQ001_Logs.HeaderId = objCTQ001.HeaderId;
                        objCTQ001_Logs.Project = objCTQ001.Project;
                        objCTQ001_Logs.BU = objCTQ001.BU;
                        objCTQ001_Logs.Location = objCTQ001.Location;
                        objCTQ001_Logs.CTQRev = objCTQ001.CTQRev;
                        objCTQ001_Logs.CTQNo = objCTQ001.CTQNo;
                        objCTQ001_Logs.Status = objCTQ001.Status;
                        objCTQ001_Logs.CreatedBy = objCTQ001.CreatedBy;
                        objCTQ001_Logs.CreatedOn = objCTQ001.CreatedOn;
                        objCTQ001_Logs.EditedBy = objCTQ001.EditedBy;
                        objCTQ001_Logs.EditedOn = objCTQ001.EditedOn;
                        db.CTQ001_Log.Add(objCTQ001_Logs);
                    }
                    #endregion

                    //db.CTQ002_Log.AddRange(lstCTQ002_Log);
                    db.SaveChanges();

                    if (lstCTQ002_Log != null && lstCTQ002_Log.Count > 0)
                    {
                        lstCTQ002_Log.ForEach(x => x.RefId = objCTQ001_Log.Id);
                        db.CTQ002_Log.AddRange(lstCTQ002_Log);
                        db.SaveChanges();
                    }


                    #region Send Notification
                    string roles = clsImplementationEnum.UserRoleName.WE3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.ENGG3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QA3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QC3.GetStringValue() + "," + clsImplementationEnum.UserRoleName.NDE3.GetStringValue();
                    (new clsManager()).SendNotification(roles, objCTQ001.Project, objCTQ001.BU, objCTQ001.Location, "CTQ " + objCTQ001.CTQNo + " has been Approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    #endregion
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Lines successfully approved.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        [NonAction]
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (disabled)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;opacity: 0.5;' Title='" + buttonTooltip + "' class='disabledicon " + className + "' ></i>";
            }
            else
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='iconspace " + className + "'" + onClickEvent + " ></i>";

            return htmlControl;
        }

        [HttpPost]
        public ActionResult ReturnLines(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue().ToUpper();
                List<CTQ002> lstCTQ002 = db.CTQ002.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper().Trim() == Status.ToUpper().Trim() && x.ReturnRemark != null).ToList();
                if (lstCTQ002 != null && lstCTQ002.Count > 0)
                {
                    foreach (var objData in lstCTQ002)
                    {
                        CTQ002 objCTQ002 = lstCTQ002.Where(x => x.LineId == objData.LineId).FirstOrDefault();
                        objCTQ002.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                        objCTQ002.EditedBy = objClsLoginInfo.UserName;
                        objCTQ002.EditedOn = DateTime.Now;
                    }
                    CTQ001 objCTQ001 = db.CTQ001.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                    objCTQ001.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objCTQ001.EditedBy = objClsLoginInfo.UserName;
                    objCTQ001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Lines successfully returned.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please add remarks for returning Lines.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_CTQ_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Location = li.Location,
                                      BU = li.BU,
                                      CTQNo = li.CTQNo,
                                      CTQRev = li.CTQRev,
                                      CreatedBy = li.CreatedBy,
                                      EditedBy = li.EditedBy,
                                      Status = li.Status
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else
                {
                    var lst = db.SP_CTQ_GET_HEADER_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      CTQRev = li.CTQRev,
                                      LineRev = li.LineRev,
                                      Status = li.Status,
                                      SrNo = li.SrNo,
                                      Specification = li.Specification,
                                      CSRMOMDoc = li.CSRMOMDoc,
                                      ClauseNo = li.ClauseNo,
                                      CTQDesc = li.CTQDesc,
                                      Department = li.Department,
                                      ActionDesc = li.ActionDesc,
                                      ActionBy = li.ActionBy,
                                      EditedBy = li.EditedBy,
                                      SendToCompiledBy = li.SendToCompiledBy,
                                      CompiledBy = li.CompiledBy,
                                      ApprovedBy = li.ApprovedBy,
                                      ReturnRemark = li.ReturnRemark,
                                      SendToCompiler = li.SendToCompiler,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}