﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.MCR.Controllers
{
    public class MaintainMCRController : clsBase
    {
        #region Header Line
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            ViewBag.Title = clsImplementationEnum.MCRIndexTitle.maintainMCR.GetStringValue();
            ViewBag.IndexType = clsImplementationEnum.MCRIndexType.maintain.GetStringValue();
            return View();
        }
        //load tab wise partial
        [HttpPost]
        public ActionResult LoadDataGridPartial(string status, string title, string indextype)
        {
            ViewBag.Status = status;
            ViewBag.GridTitle = title;
            ViewBag.IndexDataFor = indextype;
            return PartialView("_GetIndexGridDataPartial");
        }
        //bind datatable for Data Grid
        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = "1=1 ";

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string _urlform = string.Empty;
                string indextype = param.Department;
                #endregion

                if (string.IsNullOrWhiteSpace(indextype))
                {
                    indextype = clsImplementationEnum.MCRIndexType.maintain.GetStringValue();
                }

                //search Condition 
                string[] columnName = { "QualityProject", "Project", "BU", "Location", "LnTInspCertNo", "bu.t_desc", "location.t_desc" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                //strWhereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                var lstResult = db.SP_MCR_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.LnTInspCertNo),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/MCR/MaintainMCR/Details/"+uc.HeaderId+"?urlForm="+ indextype ,false)
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion

        #region Add header
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Details(int id = 0, string urlForm = "")
        {
            MCR001 objMCR001 = new MCR001();
            List<string> lstrole = objClsLoginInfo.GetUserRoleList();
            //ModelWQRequest objModelWQRequest = new ModelWQRequest();
            if (id > 0)
            {
                objMCR001 = db.MCR001.Where(x => x.HeaderId == id).FirstOrDefault();

                var objModelTechniqueNo = new NDE.Models.NDEModels().GetQMSProjectDetail(objMCR001.QualityProject);
                var projectCode = db.QMS010.Where(i => i.QualityProject.Equals(objMCR001.QualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
                if (!string.IsNullOrEmpty(projectCode))
                {
                    objModelTechniqueNo.project = (from a in db.COM001
                                                   where a.t_cprj.Equals(projectCode, StringComparison.OrdinalIgnoreCase)
                                                   select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();

                    var projectDetail = new clsManager().getProjectWiseBULocation(projectCode);
                    objModelTechniqueNo.BU = projectDetail.QCPBU;
                }
                objMCR001.BU = objModelTechniqueNo.BU;
                objMCR001.Project = objModelTechniqueNo.project.projectDescription;
                ViewBag.Action = "edit";
            }
            else
                objMCR001.Location = objClsLoginInfo.Location;
            objMCR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objMCR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

            ViewBag.formRedirect = urlForm;
            ViewBag.Title = "Maintain Material Clearance Record";
            return View(objMCR001);
        }

        [HttpPost]
        public ActionResult SaveHeader(MCR001 mcr001)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objMCR001 = new MCR001();
                if (mcr001.HeaderId > 0)
                {
                    objMCR001 = db.MCR001.FirstOrDefault(o => o.HeaderId == mcr001.HeaderId);
                    objMCR001.EditedBy = objClsLoginInfo.UserName;
                    objMCR001.EditedOn = DateTime.Now;
                }
                else
                {
                    objMCR001.CreatedBy = objClsLoginInfo.UserName;
                    objMCR001.CreatedOn = DateTime.Now;
                }
                objMCR001.BU = mcr001.BU.Split('-')[0].Trim();
                objMCR001.Location = mcr001.Location.Split('-')[0].Trim();
                objMCR001.QualityProject = mcr001.QualityProject;
                objMCR001.Project = mcr001.Project.Split('-')[0];
                objMCR001.LnTInspCertNo = mcr001.LnTInspCertNo;
                objMCR001.TPI1 = mcr001.TPI1;
                objMCR001.TPI2 = mcr001.TPI2;
                objMCR001.TPI3 = mcr001.TPI3;
                objMCR001.TPI4 = mcr001.TPI4;
                objMCR001.Equipment = mcr001.Equipment;
                if (mcr001.HeaderId > 0)
                {
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                }
                else
                {

                    db.MCR001.Add(objMCR001);
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                }
                db.SaveChanges();
                objResponseMsg.HeaderId = objMCR001.HeaderId;
                objResponseMsg.Key = true;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FetchHBOM(int? HeaderID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (HeaderID > 0)
                {
                    var objMCR001 = db.MCR001.FirstOrDefault(w => w.HeaderId == HeaderID);
                    //var lstMCRPositiNo = db.MCR002.Where(w => w.HeaderId == HeaderID).Select(s => s.PositionNo).ToList(); //expect exist FineNo
                    int SequenceNo = 1;
                    var lstHBOM = db.SP_MCR_HBOM_DATA_V2(1, int.MaxValue, "", " Project ='" + objMCR001.Project + "'", HeaderID, objMCR001.Project).ToList(); // && !lstMCRPositiNo.Contains(w.FindNo)
                    if (lstHBOM.Count == 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No HBOM found for this Project!";
                    }
                    else
                    {
                        var lstMCR002 = lstHBOM.Select(s => new MCR002()
                        {
                            HeaderId = objMCR001.HeaderId,
                            PositionNo = s.FindNo,
                            QualityProject = objMCR001.QualityProject,
                            Project = s.Project,
                            BU = objMCR001.BU,
                            Location = objMCR001.Location,
                            SequenceNo = SequenceNo,
                            Part = s.Part,
                            PartCategory = clsImplementationEnum.MCRPartCategory.PP.GetStringValue(),
                            Description = s.Description,
                            NoOfPieces = s.NoOfPieces,
                            Quantity = s.BOMQty + "",
                            UOM = s.UOM,
                            MaterialSpecification = s.MaterialDescription,
                            Length = s.Length + "",
                            Width = s.Width + "",
                            Thickness = s.Thickness + "",
                            Status = clsImplementationEnum.MCRStatus.Draft.GetStringValue(),
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now,
                            SubmittedBy = objClsLoginInfo.UserName,
                            SubmittedOn = DateTime.Now
                        }).ToList();

                        if (lstMCR002.Count > 0)
                        {
                            db.MCR002.AddRange(lstMCR002);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = lstMCR002.Count + " New HBOM Added Successfully";
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "No HBOM found for this Project!";
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "MCR not Found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion 

        #region Lines
        [HttpPost]
        public ActionResult GetLineDetails(int HeaderId, string PositionNo, string LnTPlateNo, int LineId, bool IsGet,string Project)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            var data = new MCR002();
            try
            {
                if (PositionNo != null && PositionNo != "")
                {
                    var objMCR002 = db.MCR002.Where(w => w.HeaderId == HeaderId && w.PositionNo == PositionNo).OrderByDescending(o => o.LineId).FirstOrDefault();
                    //(0, 1, "", whereCondition).ToList();
                    if (objMCR002 != null)
                    {
                        //data = objMCR002;
                        data.HeaderId = objMCR002.HeaderId;
                        /*data.QualityProject = objMCR002.QualityProject;
                        data.Project = objMCR002.Project;
                        data.BU = objMCR002.BU;
                        data.Location = objMCR002.Location;*/
                        data.PositionNo = objMCR002.PositionNo;
                        data.SequenceNo = objMCR002.SequenceNo + 1;
                        data.Part = objMCR002.Part;
                        data.Description = objMCR002.Description;
                        data.NoOfPieces = objMCR002.NoOfPieces;
                        data.Quantity = objMCR002.Quantity;
                        data.UOM = objMCR002.UOM;
                        /*data.MCRQuantity = objMCR002.MCRQuantity;*/
                        data.DrawingNo = objMCR002.DrawingNo;
                        data.DrawingRevNo = objMCR002.DrawingRevNo;
                        data.MaterialSpecification = objMCR002.MaterialSpecification;
                        data.Length = objMCR002.Length;
                        data.Width = objMCR002.Width;
                        data.Thickness = objMCR002.Thickness;
                        /*  data.TestCertificateNo = objMCR002.TestCertificateNo;
                          data.TCDate =Convert.ToDateTime(objMCR002.TCDate.Value.ToString("dd/MM/yyyy"));
                          data.HeatNo = objMCR002.HeatNo;
                          data.BatchNo = objMCR002.BatchNo;
                          data.MillPlate = objMCR002.MillPlate;
                          data.LnTPlateNo = objMCR002.LnTPlateNo;
                           data.ManufacturerName = objMCR002.ManufacturerName;
                            data.ApproverRemarks = objMCR002.ApproverRemarks;*/
                        data.Status = clsImplementationEnum.MCRStatus.Draft.GetStringValue();
                        data.PartCategory = clsImplementationEnum.MCRPartCategory.PP.GetStringValue();
                        // data.PONo = objMCR002.PONo;
                    }
                    else
                    {
                        var objMCR001 = db.MCR001.FirstOrDefault(f => f.HeaderId == HeaderId);
                        data = db.SP_MCR_HBOM_DATA_V2(0, 1, "", "HeaderId = " + HeaderId + " AND FindNo = '" + PositionNo + "'", null, objMCR001.Project).Select(s => new MCR002()
                        {
                            HeaderId = HeaderId,
                            PositionNo = s.FindNo,
                            QualityProject = objMCR001.QualityProject,
                            Project = s.Project,
                            BU = objMCR001.BU,
                            Location = objMCR001.Location,
                            SequenceNo = 1,
                            Part = s.Part,
                            PartCategory = clsImplementationEnum.MCRPartCategory.PP.GetStringValue(),
                            Description = s.Description,
                            NoOfPieces = s.NoOfPieces,
                            Quantity = s.BOMQty + "",
                            UOM = s.UOM,
                            MaterialSpecification = s.MaterialDescription,
                            Length = s.Length + "",
                            Width = s.Width + "",
                            Thickness = s.Thickness + "",
                            Status = clsImplementationEnum.MCRStatus.Draft.GetStringValue(),
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now,
                            SubmittedBy = objClsLoginInfo.UserName,
                            SubmittedOn = DateTime.Now
                        }).FirstOrDefault();

                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Invalid Position No.";
                    }
                }
                else
                {
                    if (IsGet != true)
                    {
                        if (LnTPlateNo != null && LnTPlateNo != "")
                        {
                            var objMCR002 = db.MCR002.Where(x => x.LnTPlateNo + "" == LnTPlateNo).OrderByDescending(x => x.LineId).FirstOrDefault();
                            if (objMCR002 != null)
                            {
                                data.Thickness = objMCR002.Thickness;
                                data.TestCertificateNo = objMCR002.TestCertificateNo;
                                if (objMCR002.TCDate != null)
                                {
                                    data.TCDate = Convert.ToDateTime(objMCR002.TCDate.Value.ToString("dd/MM/yyyy"));
                                }
                                else
                                {
                                    data.TCDate = null;

                                }
                                data.HeatNo = objMCR002.HeatNo;
                                data.BatchNo = objMCR002.BatchNo;
                                data.MillPlate = objMCR002.MillPlate;
                                data.LnTPlateNo = objMCR002.LnTPlateNo;
                                data.ManufacturerName = objMCR002.ManufacturerName;
                                data.PONo = objMCR002.PONo;
                            }
                            else
                            {
                                data.LnTPlateNo = LnTPlateNo;
                                data.Thickness = null;
                            }
                        }
                        else
                        {
                            data.LnTPlateNo = null;
                        }
                    }
                    else
                    {
                        if (LnTPlateNo != null && LnTPlateNo != "")
                        {
                            var objMCR002 = db.MCR002.Where(x => x.LnTPlateNo + "" == LnTPlateNo).OrderByDescending(x => x.LineId).FirstOrDefault();
                            var CommperobjMCRR002 = db.MCR002.Where(x => x.LineId == LineId).FirstOrDefault();
                            if (objMCR002 != null)
                            {
                                if (CommperobjMCRR002.Thickness == null && CommperobjMCRR002.Thickness != "")
                                {
                                    data.Thickness = objMCR002.Thickness;
                                }
                                else
                                {
                                    data.Thickness = CommperobjMCRR002.Thickness;
                                }
                                if (CommperobjMCRR002.TestCertificateNo == null && CommperobjMCRR002.TestCertificateNo != "")
                                {
                                    data.TestCertificateNo = objMCR002.TestCertificateNo;
                                }
                                else
                                {
                                    data.TestCertificateNo = CommperobjMCRR002.TestCertificateNo;
                                }
                                if (CommperobjMCRR002.TCDate == null || CommperobjMCRR002.TCDate.ToString() != "")
                                {
                                    if (objMCR002.TCDate != null)
                                    {
                                        data.TCDate = Convert.ToDateTime(objMCR002.TCDate.Value.ToString("dd/MM/yyyy"));
                                    }
                                    else
                                    {
                                        data.TCDate = null;
                                    }
                                }
                                else
                                {
                                    data.TCDate = CommperobjMCRR002.TCDate;
                                }
                                if (CommperobjMCRR002.HeatNo == null && CommperobjMCRR002.HeatNo != "")
                                {
                                    data.HeatNo = objMCR002.HeatNo;
                                }
                                else
                                {
                                    data.HeatNo = CommperobjMCRR002.HeatNo;
                                }
                                if (CommperobjMCRR002.BatchNo == null && CommperobjMCRR002.BatchNo != "")
                                {
                                    data.BatchNo = objMCR002.BatchNo;
                                }
                                else
                                {
                                    data.BatchNo = CommperobjMCRR002.BatchNo;
                                }
                                if (CommperobjMCRR002.MillPlate == null && CommperobjMCRR002.MillPlate != "")
                                {
                                    data.MillPlate = objMCR002.MillPlate;
                                }
                                else
                                {
                                    data.MillPlate = CommperobjMCRR002.MillPlate;
                                }

                                data.LnTPlateNo = objMCR002.LnTPlateNo;


                                if (CommperobjMCRR002.ManufacturerName == null && CommperobjMCRR002.ManufacturerName != "")
                                {
                                    data.ManufacturerName = objMCR002.ManufacturerName;
                                }
                                else
                                {
                                    data.ManufacturerName = CommperobjMCRR002.ManufacturerName;
                                }
                                if (CommperobjMCRR002.PONo == null && CommperobjMCRR002.PONo != "")
                                {
                                    data.PONo = objMCR002.PONo;
                                }
                                else
                                {
                                    data.PONo = CommperobjMCRR002.PONo;
                                }

                            }
                            else
                            {
                                data.LnTPlateNo = LnTPlateNo;
                            }
                        }
                        else
                        {
                            data.LnTPlateNo = null;
                        }

                    }
                }

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }

            List<SelectPlateNoList> lstPlateNoList = new List<SelectPlateNoList>();
            //if (!string.IsNullOrEmpty(data.Part))
            //    lstPlateNoList = GetPlateNoList(data.Part).ToList();

            //if (!string.IsNullOrEmpty(data.Part) && !string.IsNullOrEmpty(data.PositionNo) && !string.IsNullOrEmpty(data.Project))
            if (!string.IsNullOrEmpty(data.Part) && !string.IsNullOrEmpty(data.PositionNo) )
                lstPlateNoList = GetPlateNoList(data.Part, data.PositionNo,Project.Split('-')[0]).ToList();
              //  lstPlateNoList = GetPlateNoList(data.PositionNo).ToList();

            string lstSupplier = string.Empty;
            if (!string.IsNullOrEmpty(data.PONo))
                lstSupplier = GetSupplier(data.PONo);

            //return Json(new { Key = objResponseMsg.Key, Value = objResponseMsg.Value, data = data, lstPlateNoList = lstPlateNoList, lstSupplier = lstSupplier }, JsonRequestBehavior.AllowGet);

            return Json(new { Key = objResponseMsg.Key, Value = objResponseMsg.Value, data = data, lstPlateNoList = lstPlateNoList, lstSupplier = lstSupplier,changeValue =Convert.ToString( TempData["change"]) }, JsonRequestBehavior.AllowGet);

        }

        //public List<SelectPlateNoList> GetPlateNoList(string Part)
        //{
        //    List<SelectPlateNoList> lstReturn = new List<SelectPlateNoList>();
        //    var LNLinkedServer = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue());
        //    string LNCompanyId = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNCompanyId.GetStringValue());

        //    if (!string.IsNullOrWhiteSpace(Part))
        //    {
        //        string query = @"Select distinct RTRIM(LTRIM(t_stkn)) t_stkn  from " + LNLinkedServer + ".dbo.tltsfc500" + LNCompanyId + " Where RTRIM(LTRIM(t_item)) = '"+ Part + "' order by RTRIM(LTRIM(t_stkn))";
        //        lstReturn = db.Database.SqlQuery<SelectPlateNoList>(query).ToList();
        //    }
        //    return lstReturn;
        //}

        public List<SelectPlateNoList> GetPlateNoList(string Part,string PositionNo,string Project)
        {
            List<SelectAlternateStoke> lstAlternateStoke = new List<SelectAlternateStoke>();
            List<SelectPlateNoList> lstReturn = new List<SelectPlateNoList>();
            var LNLinkedServer = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue());
            string LNCompanyId = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNCompanyId.GetStringValue());

            TempData["change"] = "";

            //if (!string.IsNullOrWhiteSpace(Part) && !string.IsNullOrWhiteSpace(PositionNo) && !string.IsNullOrWhiteSpace(Project))
            if (!string.IsNullOrWhiteSpace(Part) && !string.IsNullOrWhiteSpace(PositionNo))
            {

                string query1 = "select distinct RTRIM(LTRIM(t_alts)) t_alts from " + LNLinkedServer + ".dbo.tltsfc503" + LNCompanyId + " where RTRIM(LTRIM(t_sitm)) = '" + Part + "' and RTRIM(LTRIM(t_prtn)) = '" + PositionNo + "' and RTRIM(LTRIM(t_cprj)) = '" + Project + "'   ";

               var lstaltstoke = db.Database.SqlQuery<SelectAlternateStoke>(query1).FirstOrDefault();

                if (lstaltstoke != null)
                {
                    if(lstaltstoke.t_alts.Equals("1"))
                    {
                        //string query = @"Select distinct RTRIM(LTRIM(t_stkn)) t_stkn  from " + LNLinkedServer + ".dbo.tltsfc503" + LNCompanyId + " Where RTRIM(LTRIM(t_prtn)) = '" + PositionNo + "' order by RTRIM(LTRIM(t_stkn))";

                        string query = @"Select distinct RTRIM(LTRIM(t_stkn)) t_stkn,RTRIM(LTRIM(t_oitm)) t_oitm  from " + LNLinkedServer + ".dbo.tltsfc503" + LNCompanyId + " Where RTRIM(LTRIM(t_sitm)) = '" + Part + "' and RTRIM(LTRIM(t_prtn)) = '" + PositionNo + "' and RTRIM(LTRIM(t_cprj)) = '" + Project + "' order by RTRIM(LTRIM(t_stkn))";

                        lstReturn = db.Database.SqlQuery<SelectPlateNoList>(query).ToList();
                        TempData["change"] = "1";
                    }
                    else
                    {
                        string query = @"Select distinct RTRIM(LTRIM(t_stkn)) t_stkn  from " + LNLinkedServer + ".dbo.tltsfc500" + LNCompanyId + " Where RTRIM(LTRIM(t_item)) = '" + Part + "' order by RTRIM(LTRIM(t_stkn))";
                        lstReturn = db.Database.SqlQuery<SelectPlateNoList>(query).ToList();
                        TempData["change"] = "";
                    }
                }
                else
                {
                    string query = @"Select distinct RTRIM(LTRIM(t_stkn)) t_stkn  from " + LNLinkedServer + ".dbo.tltsfc500" + LNCompanyId + " Where RTRIM(LTRIM(t_item)) = '" + Part + "' order by RTRIM(LTRIM(t_stkn))";
                    lstReturn = db.Database.SqlQuery<SelectPlateNoList>(query).ToList();
                    TempData["change"] = "";
                }
                  

                //string query = @"Select distinct RTRIM(LTRIM(t_stkn)) t_stkn  from " + LNLinkedServer + ".dbo.tltsfc503" + LNCompanyId + " Where RTRIM(LTRIM(t_pono)) = '"+ PositionNo + "' order by RTRIM(LTRIM(t_stkn))";
                //lstReturn = db.Database.SqlQuery<SelectPlateNoList>(query).ToList();
            }
            return lstReturn;
        }


        public string GetSupplier(string PoNo)
        {
            string suuplier = string.Empty;
            var LNLinkedServer = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue());
            string LNCompanyId = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNCompanyId.GetStringValue());

            if (!string.IsNullOrWhiteSpace(PoNo))
            {
                string query = @"Select RTRIM(LTRIM(t_otbp)) t_otbp from " + LNLinkedServer + ".dbo.ttdpur400" + LNCompanyId + " Where RTRIM(LTRIM(t_orno)) = '" + PoNo + "'";
                var lstReturn = db.Database.SqlQuery<SelectSupplier>(query).FirstOrDefault();

                if (lstReturn != null)
                    suuplier = lstReturn.t_otbp;
            }
            return suuplier;
        }

        [HttpPost]
        //public ActionResult GetPlateNoListPost(string Part)
        //{
        //    var objResponceMsg = new clsHelper.ResponceMsgWithObject();
        //    try
        //    {
        //        if (!string.IsNullOrEmpty(Part))
        //        {
        //           var lstMillPlateNo = GetPlateNoList(Part);
        //            objResponceMsg.Key = true;
        //            objResponceMsg.data = lstMillPlateNo;
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponceMsg.Key = false;
        //        objResponceMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
        //    }
        //    return Json(objResponceMsg, JsonRequestBehavior.AllowGet);
        //}
        
        public ActionResult GetPlateNoListPost(string Part,string PositionNo,string Project)
        {
            var objResponceMsg = new clsHelper.ResponceMsgWithObject();
            try
            {
                if (!string.IsNullOrEmpty(Part) && !string.IsNullOrEmpty(PositionNo) && !string.IsNullOrEmpty(Project))
                {
                    string Proj = Project.Split('-')[0];

                   var lstMillPlateNo = GetPlateNoList(Part, PositionNo,Proj);
                   objResponceMsg.Value =Convert.ToString(TempData["change"]);
                    objResponceMsg.Key = true;
                    objResponceMsg.data = lstMillPlateNo;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponceMsg.Key = false;
                objResponceMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponceMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GETMillPlateNo(string PlateNo)
        {
            var objResponceMsg = new clsHelper.ResponceMsgWithObject();
            try
            {
                if (!string.IsNullOrEmpty(PlateNo))
                {
                    var LNLinkedServer = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue());
                    string LNCompanyId = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNCompanyId.GetStringValue());

                    string query1 = @"Select distinct RTRIM(LTRIM(t_valu)) t_valu from " + LNLinkedServer + ".dbo.twhltc220" + LNCompanyId + " Where RTRIM(LTRIM(t_ltft)) = 'MPN' and RTRIM(LTRIM(t_clot)) = '" + PlateNo + "'";

                    var lstMillPlateNo = db.Database.SqlQuery<SelectMillPlateNoList>(query1).FirstOrDefault();

                    string query2 = @"Select distinct RTRIM(LTRIM(t_valu)) t_valu from " + LNLinkedServer + ".dbo.twhltc220" + LNCompanyId + " Where RTRIM(LTRIM(t_ltft)) = 'HTN' and RTRIM(LTRIM(t_clot)) = '" + PlateNo + "'";

                    var lstHeatNo = db.Database.SqlQuery<SelectMillPlateNoList>(query2).FirstOrDefault();

                    objResponceMsg.Key = true;
                    objResponceMsg.data = new { lstMillPlateNo = lstMillPlateNo, lstHeatNo = lstHeatNo };
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponceMsg.Key = false;
                objResponceMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponceMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSupplierPost(string PoNo)
        {
            var objResponceMsg = new clsHelper.ResponceMsgWithObject();
            try
            {
                if (!string.IsNullOrEmpty(PoNo))
                {
                    string splr = GetSupplier(PoNo);
                    objResponceMsg.Key = true;
                    objResponceMsg.Value = splr;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponceMsg.Key = false;
                objResponceMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponceMsg, JsonRequestBehavior.AllowGet);
        }

        public class SelectPlateNoList
        {
            public string t_stkn { get; set; }
            public string t_htno { get; set; }
            public string t_oitm { get; set; }
        }

        public class SelectAlternateStoke
        {
            public string t_alts { get; set; }
        }

        public class SelectSupplier
        {
            public string t_otbp { get; set; }
        }

        public class SelectMillPlateNoList
        {
            public string t_valu { get; set; }
        }

        [HttpPost]
        public ActionResult LoadLineDataGridPartial(MCR001 mcr001)
        {
            ViewBag.lstPositionNo = db.MCR002.Where(w => w.HeaderId == mcr001.HeaderId).Select(s => s.PositionNo).Distinct().ToList();
            ViewBag.lstPartCategory = clsImplementationEnum.getMCRPartCategories();
            return PartialView("_GetLineGridDataPartial", mcr001);
        }

        public ActionResult loadLineDataTable(JQueryDataTableParamModel param, string ForApprove)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                bool isEditEnabled = true;
                string whereCondition = "HeaderID = " + param.Headerid;
                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string _urlform = string.Empty;
                string indextype = param.Department;
                #endregion
                string[] columnName = {  "PositionNo","Part","Description","Quantity","MCRQuantity","DrawingNo","DrawingRevNo","MaterialSpecification","Length",
                    "Width","Thickness","TestCertificateNo","TCDate","HeatNo","BatchNo","LnTPlateNo","ManufacturerName","PartCategory","ApproverRemarks","SequenceNo","NoOfPieces","UOM","PONo","Status","ApprovedBy","ApprovedOn"};

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }


                var lstMCR = db.SP_MCR_LINE_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstMCR.Select(i => i.TotalCount).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "0",
                                    Helper.GenerateHTMLTextbox(newRecordId,"PositionNo",disabled: true),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Part","","GetPlateNoList(\"0\")",disabled: true),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Description",disabled: true),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Quantity",disabled: true),
                                    Helper.GenerateHTMLTextbox(newRecordId,"MCRQuantity",maxlength: "4"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"DrawingNo",maxlength: "50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"DrawingRevNo",maxlength: "10"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"LnTPlateNo",maxlength: "150"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"MaterialSpecification",maxlength: "50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Length",maxlength: "50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Width",maxlength: "50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Thickness",maxlength: "50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"TestCertificateNo",maxlength: "100"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"TCDate",maxlength: "30"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"HeatNo",maxlength: "100"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"BatchNo",maxlength: "100"),
                                    //Helper.GenerateHTMLTextbox(newRecordId,"MillPlate",maxlength: "100"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"ManufacturerName",maxlength: "100"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"PartCategory"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"ApproverRemarks","","",false,"",true,"300"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"SequenceNo",disabled: true),
                                    Helper.GenerateHTMLTextbox(newRecordId,"NoOfPieces",disabled: true),
                                    Helper.GenerateHTMLTextbox(newRecordId,"UOM",disabled: true),
                                    Helper.GenerateHTMLTextbox(newRecordId,"PONo","","GetSupplier(\"0\")",maxlength: "100"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Supplier","","",true,"",true),
                                    Helper.GenerateHTMLTextbox(newRecordId,"Status", clsImplementationEnum.MCRStatus.Draft.GetStringValue(),"",false,"",true),
                                    Helper.GenerateHTMLTextbox(newRecordId,"ApprovedBy",disabled: true),
                                    Helper.GenerateHTMLTextbox(newRecordId,"ApprovedOn",disabled: true),
                                    GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveRecord(0);" ),
                                };
                var approvedStatus = clsImplementationEnum.MCRStatus.Approved.GetStringValue();
                var res = (from h in lstMCR
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    h.PositionNo,
                                    h.Part,
                                    h.Description,
                                    h.Quantity,
                                    (h.MCRQuantity== null ? "": h.MCRQuantity.Value.ToString("0.##")),
                                    h.DrawingNo,
                                    h.DrawingRevNo,
                                    h.LnTPlateNo,
                                    h.MaterialSpecification,
                                    h.Length,
                                    h.Width,
                                    h.Thickness,
                                    h.TestCertificateNo,
                                    (h.TCDate!=null?h.TCDate.Value.ToString("yyyy-MM-dd"):string.Empty),
                                    h.HeatNo,
                                    h.BatchNo,
                                    //h.MillPlate
                                    h.ManufacturerName,
                                    h.PartCategory,
                                    h.ApproverRemarks,
                                    h.SequenceNo+"",
                                    h.NoOfPieces+"",
                                    h.UOM,
                                    h.PONo,
                                    h.Supplier,
                                    h.Status,
                                    h.ApprovedBy,
                                    h.ApprovedOn!=null?h.ApprovedOn.Value.ToString("dd/MM/yyyy"):string.Empty,
                                    h.Status == approvedStatus ? "": GenerateGridButton(h.LineId, "Approve", "Approve Record", "fa fa-check", "ApproveRecord("+h.LineId+");", (string.IsNullOrWhiteSpace(h.ApproverRemarks) || string.IsNullOrWhiteSpace(h.ManufacturerName) || string.IsNullOrWhiteSpace(h.MCRQuantity+"") || string.IsNullOrWhiteSpace(h.TCDate+"") || string.IsNullOrWhiteSpace(h.TestCertificateNo) ) )
                                    + GenerateGridButton(h.LineId, "Delete", "Delete Record", "fa fa-trash", "DeleteRecord("+h.LineId+");" ), // + Helper.GenerateGridButton(newRecordId, "Edit", "Edit Rocord", "fa fa-plus", "SaveRecord("+h.LineId+");" )
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveLines(FormCollection fc, int Id = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int LineId = Id;
                int tempint = 0;
                MCR002 objMCR002 = new MCR002();
                if (LineId > 0)
                {
                    objMCR002 = db.MCR002.FirstOrDefault(x => x.LineId == LineId);
                    if (!string.IsNullOrWhiteSpace(fc["Status" + LineId]))
                    {
                        if (objMCR002.Status != fc["Status" + LineId] && fc["Status" + LineId] == clsImplementationEnum.MCRStatus.Approved.GetStringValue())
                        {
                            objMCR002.ApprovedBy = objClsLoginInfo.UserName;
                            objMCR002.ApprovedOn = DateTime.Now;
                        }

                        objMCR002.Status = fc["Status" + LineId];
                    }
                }
                else
                {
                    objMCR002.HeaderId = Convert.ToInt32(fc["HeaderId"]);
                    var objMCR001 = db.MCR002.FirstOrDefault(x => x.HeaderId == objMCR002.HeaderId);
                    objMCR002.QualityProject = objMCR001.QualityProject;
                    objMCR002.Project = objMCR001.Project;
                    objMCR002.BU = objMCR001.BU;
                    objMCR002.Location = objMCR001.Location;
                    objMCR002.PositionNo = fc["PositionNo" + LineId];
                    if (int.TryParse(fc["SequenceNo" + LineId], out tempint))
                        objMCR002.SequenceNo = tempint;
                    else
                        objMCR002.SequenceNo = null;
                    objMCR002.Part = fc["Part" + LineId];
                    objMCR002.Description = fc["Description" + LineId];
                    if (int.TryParse(fc["NoOfPieces" + LineId], out tempint))
                        objMCR002.NoOfPieces = tempint;
                    else
                        objMCR002.NoOfPieces = null;
                    objMCR002.Quantity = fc["Quantity" + LineId];
                    objMCR002.UOM = fc["UOM" + LineId];
                    objMCR002.Length = fc["Length" + LineId];
                    objMCR002.Width = fc["Width" + LineId];
                    objMCR002.Thickness = fc["Thickness" + LineId];
                    objMCR002.Status = fc["Status" + LineId];
                    objMCR002.Description = fc["Description" + LineId];
                }

                objMCR002.PartCategory = fc["PartCategory" + LineId];
                objMCR002.PONo = fc["PONo" + LineId];
                if (int.TryParse(fc["MCRQuantity" + LineId], out tempint))
                    objMCR002.MCRQuantity = tempint;
                else
                    objMCR002.MCRQuantity = null;
                objMCR002.TestCertificateNo = fc["TestCertificateNo" + LineId];

                DateTime tempDate;
                if (DateTime.TryParse(fc["TCDate" + LineId], out tempDate) && tempDate < DateTime.Now)
                    objMCR002.TCDate = tempDate;
                else if (objMCR002.TCDate != tempDate)
                    objMCR002.TCDate = null;

                objMCR002.HeatNo = fc["HeatNo" + LineId];
                objMCR002.BatchNo = fc["BatchNo" + LineId];
                //objMCR002.MillPlate = fc["MillPlate" + LineId];
                objMCR002.LnTPlateNo = fc["LnTPlateNo" + LineId];
                objMCR002.ManufacturerName = fc["ManufacturerName" + LineId];
                objMCR002.ApproverRemarks = fc["ApproverRemarks" + LineId];
                objMCR002.MaterialSpecification = fc["MaterialSpecification" + LineId];

                if (LineId > 0)
                {
                    objMCR002.EditedBy = objClsLoginInfo.UserName;
                    objMCR002.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {
                    objMCR002.CreatedBy = objClsLoginInfo.UserName;
                    objMCR002.CreatedOn = DateTime.Now;
                    objMCR002.SubmittedBy = objClsLoginInfo.UserName;
                    objMCR002.SubmittedOn = DateTime.Now;
                    db.MCR002.Add(objMCR002);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                db.SaveChanges();
                if (objMCR002.DrawingNo != fc["DrawingNo" + LineId] || objMCR002.DrawingRevNo != fc["DrawingRevNo" + LineId])
                {
                    var match = "";
                    if (objMCR002.PositionNo.Length == 4)
                        match = objMCR002.PositionNo.Substring(0, 2);
                    if (objMCR002.PositionNo.Length == 3 || objMCR002.PositionNo.Length == 2)
                        match = objMCR002.PositionNo.Substring(0, 1);
                    if (!String.IsNullOrWhiteSpace(match))
                    {
                        var lstMCR002 = db.MCR002.Where(w => w.HeaderId == objMCR002.HeaderId && w.PositionNo.Length == objMCR002.PositionNo.Length && w.PositionNo.StartsWith(match));
                        foreach (var item in lstMCR002)
                        {
                            if (objMCR002.DrawingNo != fc["DrawingNo" + LineId])
                                item.DrawingNo = fc["DrawingNo" + LineId];
                            if (objMCR002.DrawingRevNo != fc["DrawingRevNo" + LineId])
                                item.DrawingRevNo = fc["DrawingRevNo" + LineId];
                        }
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateLineData(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                columnValue = columnValue == null ? null : columnValue.Replace("'", "''");
                if (columnName == "TCDate")
                {
                    var objMCR002 = db.MCR002.FirstOrDefault(x => x.LineId + "" == rowId);
                    DateTime tempDate;
                    if (DateTime.TryParse(columnValue, out tempDate) && tempDate <= DateTime.Now)
                        objMCR002.TCDate = tempDate;
                    else if (objMCR002.TCDate != tempDate)
                        objMCR002.TCDate = null;
                    db.SaveChanges();
                }
                else if (columnName == "DrawingNo" || columnName == "DrawingRevNo")
                {
                    var objMCR002 = db.MCR002.FirstOrDefault(x => x.LineId + "" == rowId);
                    var match = "";
                    if (objMCR002.PositionNo.Length == 4)
                        match = objMCR002.PositionNo.Substring(0, 2);
                    if (objMCR002.PositionNo.Length == 3 || objMCR002.PositionNo.Length == 2 || objMCR002.PositionNo.Length == 1 )
                        match = objMCR002.PositionNo.Substring(0, 1);
                    if (!String.IsNullOrWhiteSpace(match))
                    {
                        var lstMCR002 = db.MCR002.Where(w => w.HeaderId == objMCR002.HeaderId && w.PositionNo.Length == objMCR002.PositionNo.Length && w.PositionNo.StartsWith(match));
                        if (columnName == "DrawingNo")
                        {
                            foreach (var item in lstMCR002)
                            {
                                if (item.LineId == Convert.ToInt32(rowId))
                                {
                                    item.DrawingNo = columnValue;
                                }
                                else if (item.DrawingNo == null || item.DrawingNo == "")
                                {
                                    item.DrawingNo = columnValue;
                                }

                            }
                        }
                        else
                        {
                            foreach (var item in lstMCR002)
                            {
                                if (item.LineId == Convert.ToInt32(rowId))
                                {
                                    item.DrawingRevNo = columnValue;
                                }
                                else if (item.DrawingRevNo == null || item.DrawingRevNo == "")
                                {
                                    item.DrawingRevNo = columnValue;
                                }
                            }
                        }
                        db.SaveChanges();
                    }
                }
                else if (columnName == "LnTPlateNo")
                {
                    var objMCR002 = db.MCR002.Where(x => x.LnTPlateNo + "" == columnValue).ToList().OrderByDescending(x => x.LineId).FirstOrDefault();
                    int LineId = Convert.ToInt32(rowId);
                    var CommperobjMCRR002 = db.MCR002.Where(x => x.LineId == LineId).FirstOrDefault();

                    if (objMCR002 != null)
                    {
                        if (CommperobjMCRR002.Thickness == null && CommperobjMCRR002.Thickness != "")
                        {
                            CommperobjMCRR002.Thickness = objMCR002.Thickness;
                        }
                        if (CommperobjMCRR002.TestCertificateNo == null && CommperobjMCRR002.TestCertificateNo != "")
                        {
                            CommperobjMCRR002.TestCertificateNo = objMCR002.TestCertificateNo;
                        }
                        if (CommperobjMCRR002.TCDate == null || CommperobjMCRR002.TCDate.ToString() != "")
                        {
                            if (objMCR002.TCDate != null)
                            {
                                CommperobjMCRR002.TCDate = Convert.ToDateTime(objMCR002.TCDate.Value.ToString("dd/MM/yyyy"));
                            }
                            else
                            {
                                CommperobjMCRR002.TCDate = null;
                            }
                        }
                        if (CommperobjMCRR002.HeatNo == null && CommperobjMCRR002.HeatNo != "")
                        {
                            CommperobjMCRR002.HeatNo = objMCR002.HeatNo;
                        }
                        if (CommperobjMCRR002.BatchNo == null && CommperobjMCRR002.BatchNo != "")
                        {
                            CommperobjMCRR002.BatchNo = objMCR002.BatchNo;
                        }
                        if (CommperobjMCRR002.MillPlate == null && CommperobjMCRR002.MillPlate != "")
                        {
                            CommperobjMCRR002.MillPlate = objMCR002.MillPlate;
                        }
                        CommperobjMCRR002.LnTPlateNo = objMCR002.LnTPlateNo;
                        if (CommperobjMCRR002.ManufacturerName == null && CommperobjMCRR002.ManufacturerName != "")
                        {
                            CommperobjMCRR002.ManufacturerName = objMCR002.ManufacturerName;
                        }
                        if (CommperobjMCRR002.PONo == null && CommperobjMCRR002.PONo != "")
                        {
                            CommperobjMCRR002.PONo = objMCR002.PONo;
                        }
                    }
                    else
                    {
                        CommperobjMCRR002.LnTPlateNo = columnValue;
                    }
                    db.SaveChanges();
                }
                else
                    db.SP_MCR_LINE_ROW_UPDATE(Convert.ToInt32(rowId), columnName, columnValue);
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Value = ex.Message;
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLine(int LineId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objMCR002 = db.MCR002.FirstOrDefault(f => f.LineId == LineId);

                if (objMCR002 != null && objMCR002.Status != clsImplementationEnum.MCRStatus.Approved.GetStringValue())
                {
                    db.MCR002.Remove(objMCR002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Deleted Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found or already deleted";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Utility
        public int GetNextSequenceNo(int HeaderId, string PositionNo)
        {
            var SequenceNo = db.MCR002.Where(w => w.HeaderId == HeaderId && w.PositionNo == PositionNo).OrderByDescending(x => x.HeaderId).Select(x => x.SequenceNo).FirstOrDefault();
            return SequenceNo.GetValueOrDefault() + 1;
        }

        //[HttpPost]
        //public JsonResult GetAllBU(string term)
        //{
        //    List<CategoryData> lstBU = new List<CategoryData>();
        //    int BU = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());
        //    //var lstBUs = Manager.getBUsByID(BU);
        //    if (!string.IsNullOrWhiteSpace(term))
        //    {
        //        lstBU = (from ath1 in db.ATH001
        //                 join com2 in db.COM002 on ath1.BU equals com2.t_dimx
        //                 where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim()
        //                        && ath1.BU != ""
        //                        && (ath1.BU.Contains(term) || com2.t_desc.ToLower().Contains(term.ToLower()))
        //                 select new CategoryData { id = ath1.BU, text = com2.t_desc }).Distinct().ToList();
        //    }
        //    else
        //    {
        //        lstBU = (from ath1 in db.ATH001
        //                 join com2 in db.COM002 on ath1.BU equals com2.t_dimx
        //                 where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim()
        //                        && ath1.BU != ""
        //                 select new CategoryData { id = ath1.BU, text = com2.t_desc }).Distinct().ToList();
        //    }
        //    return Json(lstBU, JsonRequestBehavior.AllowGet);
        //}

        //[HttpPost]
        //public JsonResult GetAllLocation(string term)
        //{
        //    List<CategoryData> lstLOC = new List<CategoryData>();
        //    int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());

        //    var lstLocations = Manager.getBUsByID(LOC);

        //    lstLOC = (from li in lstLocations
        //              where li.t_dimx.ToLower().Contains(term.ToLower()) || li.t_desc.ToLower().Contains(term.ToLower())
        //              select new CategoryData
        //              {
        //                  id = li.t_dimx,
        //                  text = li.t_desc,
        //              }).ToList();
        //    return Json(lstLOC, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult GetQMSProjectDetail(string QMSProject, string location)
        {
            var objModelCategory = new NDE.Models.ModelCategory();

            var objModelTechniqueNo = new NDE.Models.ModelNDETechnique();
            if (!string.IsNullOrEmpty(QMSProject))
            {
                var objMCR001 = db.MCR001.FirstOrDefault(a => a.QualityProject == QMSProject);
                if (objMCR001 != null)
                {
                    objModelCategory.Value = "This Quality Project already created <br/> <a href='?id=" + objMCR001.HeaderId + "'> <b>Click here</b> to open this Quality Project </a>";
                    objModelCategory.Key = false;
                    return Json(objModelCategory);
                }
                objModelTechniqueNo = new NDE.Models.NDEModels().GetQMSProjectDetail(QMSProject);

                var projectCode = db.QMS010.Where(i => i.QualityProject.Equals(QMSProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
                if (!string.IsNullOrEmpty(projectCode))
                {
                    objModelTechniqueNo.project = (from a in db.COM001
                                                   where a.t_cprj.Equals(projectCode, StringComparison.OrdinalIgnoreCase)
                                                   select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();

                    var projectDetail = new clsManager().getProjectWiseBULocation(projectCode);

                    objModelTechniqueNo.BU = projectDetail.QCPBU;
                }
                objModelCategory.CodeValue = Manager.GetEquipmentNoByProject(projectCode); // CodeValue Used as Equipment for temperary

            }


            objModelCategory.project = objModelTechniqueNo.project;
            objModelCategory.BU = objModelTechniqueNo.BU;
            objModelCategory.Key = true;

            return Json(objModelCategory);
        }


        public JsonResult GetProjectValueEdit(string search, string contract, string selectedBU, int headerId)
        {
            List<CategoryData> lstProject = new List<CategoryData>();
            if (!string.IsNullOrWhiteSpace(search))
            {
                lstProject = (from cm001 in db.COM001
                              join cm004 in db.COM004 on cm001.t_cprj equals cm004.t_cprj
                              where cm004.t_cono == contract && cm001.t_entu == selectedBU && (cm001.t_cprj.Contains(search) || cm001.t_dsca.Contains(search))
                              select new CategoryData { id = cm001.t_cprj, text = cm001.t_cprj, Description = cm001.t_dsca }).ToList();
            }
            else
            {
                lstProject = (from cm001 in db.COM001
                              join cm004 in db.COM004 on cm001.t_cprj equals cm004.t_cprj
                              where cm004.t_cono == contract && cm001.t_entu == selectedBU
                              select new CategoryData { id = cm001.t_cprj, text = cm001.t_cprj, Description = cm001.t_dsca }).ToList();
            }
            string result = db.CFA001.Where(s => s.HeaderId == headerId).Select(s => s.ProjectNo).FirstOrDefault();
            var lstresult = result.Split(',').ToList();
            var lstProjectid = lstProject.Select(x => x.id).ToList();
            foreach (var lst in lstresult)
            {
                if (!lstProjectid.Contains(lst))
                {
                    lstProject.Add(new CategoryData { id = lst, text = lst, Description = "newData" });
                }
            }

            return Json(lstProject, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isDisabled = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' title='" + buttonTooltip + "' " + (isDisabled ? "disabled" : "") + "  class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> </a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                #region Maintain header
                if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_MCR_LINE_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    var newlst = (from h in lst
                                  select new
                                  {
                                      PositionNo = h.PositionNo,
                                      SequenceNo = h.SequenceNo + "",
                                      Part = h.Part,
                                      Description = h.Description,
                                      NoOfPieces = h.NoOfPieces + "",
                                      Quantity = h.Quantity,
                                      UOM = h.UOM,
                                      MCRQuantity = h.MCRQuantity,
                                      DrawingNo = h.DrawingNo,
                                      DrawingRevNo = h.DrawingRevNo,
                                      LnTPlateNo = h.LnTPlateNo,
                                      MaterialSpecification = h.MaterialSpecification,
                                      Length = h.Length,
                                      Width = h.Width,
                                      Thickness = h.Thickness,
                                      TestCertificateNo = h.TestCertificateNo,
                                      TCDate = h.TCDate,
                                      HeatNo = h.HeatNo,
                                      BatchnMillPlateNo = h.BatchNo,
                                      //MillPlate = h.MillPlate,
                                      ManufacturerName = h.ManufacturerName,
                                      PartCategory = h.PartCategory,
                                      PONo = h.PONo,
                                      h.Supplier,
                                      ApproverRemarks = h.ApproverRemarks,
                                      Status = h.Status,
                                      ApprovedBy = h.ApprovedBy,
                                      ApprovedOn = h.ApprovedOn != null ? h.ApprovedOn.Value.ToString("dd/MM/yyyy") : string.Empty,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_MCR_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    var newlst = (from h in lst
                                  select new
                                  {
                                      HeaderId = h.HeaderId + "",
                                      QualityProject = h.QualityProject,
                                      Project = h.Project,
                                      LnTInspCertNo = h.LnTInspCertNo,
                                      BU = h.BU,
                                      Location = h.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                #endregion


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult PrintReportMCR(string Print="")
        {
            ViewBag.IsPrint=Print;
            return View();
        }
        public JsonResult GetProjects(string search)
        {
            try
            {
                var isSearchEmpty = String.IsNullOrWhiteSpace(search);
                List<mcrreport> lstProjects = new List<mcrreport>();
                lstProjects = (from c1 in db.MCR001
                               join c2 in db.COM001 on c1.Project equals c2.t_cprj
                               where isSearchEmpty || (c2.t_dsca.ToLower().Contains(search.ToLower()) || c2.t_cprj.Contains(search.ToLower()))
                               select new mcrreport
                               {
                                   id = c2.t_cprj,
                                   text = c2.t_cprj + " - " + c2.t_dsca
                               }).Distinct().Take(isSearchEmpty ? 10 : int.MaxValue).ToList();

                //if (lstProjects.Count > 0)
                //{
                //    lstProjects.Insert(0, new mcrreport { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                //}

                return Json(lstProjects, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetTestcertificateNo(string search, string fromproject, string toproject)
        {
            try
            {
                var isSearchEmpty = String.IsNullOrWhiteSpace(search);
                List<mcrreport> lstPositionNo = new List<mcrreport>();
                if (fromproject == toproject)
                {
                    lstPositionNo = (from c1 in db.MCR001
                                     join c2 in db.MCR002 on c1.HeaderId equals c2.HeaderId
                                     where c1.Project == fromproject && (isSearchEmpty ? true : c2.TestCertificateNo.Contains(search))
                                     select new mcrreport
                                     {
                                         id = c2.TestCertificateNo,
                                         text = c2.TestCertificateNo,
                                     }).Distinct().Take(isSearchEmpty ? 10 : int.MaxValue).ToList();
                }
                else {
                    lstPositionNo = (from c1 in db.MCR001
                                     join c2 in db.MCR002 on c1.HeaderId equals c2.HeaderId
                                     where (c1.Project.CompareTo(fromproject) >= 0 && c1.Project.CompareTo(toproject) <= 0) && (isSearchEmpty ? true : c2.TestCertificateNo.Contains(search))
                                     select new mcrreport
                                     {
                                         id = c2.TestCertificateNo,
                                         text = c2.TestCertificateNo,
                                     }).Distinct().Take(isSearchEmpty ? 10 : int.MaxValue).ToList();
                }
                if (lstPositionNo.Count > 0)
                {
                    lstPositionNo.Insert(0, new mcrreport { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                }

                return Json(lstPositionNo, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetPositionNo(string search)
        {
            try
            {
                var isSearchEmpty = String.IsNullOrWhiteSpace(search);
                List<mcrreport> lstPositionNo = new List<mcrreport>();
                lstPositionNo = (from c1 in db.MCR001
                                 join c2 in db.MCR002 on c1.HeaderId equals c2.HeaderId
                                 where isSearchEmpty || (c2.PositionNo.ToLower().Contains(search.ToLower()))
                                 select new mcrreport
                                 {
                                     id = c2.PositionNo,
                                     text = c2.PositionNo
                                 }).Distinct().Take(isSearchEmpty ? 10 : int.MaxValue).ToList();

                //if (lstPositionNo.Count > 0)
                //{
                //    lstPositionNo.Insert(0, new mcrreport { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                //}

                return Json(lstPositionNo, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetPositionNobyTestCertificate(string search, string fromproject, string toproject, string testcertificate)
        {
            try
            {
                var isSearchEmpty = String.IsNullOrWhiteSpace(search);
                List<mcrreport> lstPositionNo = new List<mcrreport>();
                string[] arr = null; 
                if (!string.IsNullOrWhiteSpace(testcertificate))
                {
                    arr = testcertificate.Split(',');
                }

                lstPositionNo = (from c1 in db.MCR001
                                 join c2 in db.MCR002 on c1.HeaderId equals c2.HeaderId
                                 where (isSearchEmpty || (c2.PositionNo.ToLower().Contains(search.ToLower()))) 
                                       && (fromproject == toproject ? c1.Project == fromproject : (c1.Project.CompareTo(fromproject) >= 0 && c1.Project.CompareTo(toproject) <= 0))
                                       && (testcertificate == "ALL" ? true : arr.Contains(c2.TestCertificateNo))
                                 select new mcrreport
                                 {
                                     id = c2.PositionNo,
                                     text = c2.PositionNo
                                 }).Distinct().Take(isSearchEmpty ? 10 : int.MaxValue).ToList();

                //if (lstPositionNo.Count > 0)
                //{
                //    lstPositionNo.Insert(0, new mcrreport { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                //}

                return Json(lstPositionNo, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetStatus(string search)
        {
            try
            {
                var isSearchEmpty = String.IsNullOrWhiteSpace(search);
                //List<mcrreport> lstDepartment = new List<mcrreport>();
                var lstStatus = clsImplementationEnum.GetMCRStatus().ToList();

                var lstStatusAll = (from lst in lstStatus
                                    where isSearchEmpty || lst.ToLower().IndexOf(search.ToLower()) > -1
                                    select new
                                    {
                                        id = lst,
                                        text = lst
                                    }).Take(isSearchEmpty ? 10 : int.MaxValue).ToList();
                /*if (lstStatusAll.Count > 0)
                {
                    lstStatusAll.Insert(0, new { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                }*/
                return Json(lstStatusAll, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetPartCategory(string search)
        {
            try
            {
                var isSearchEmpty = String.IsNullOrWhiteSpace(search);
                List<mcrreport> lstPartCategory = new List<mcrreport>();
                lstPartCategory = (from c1 in db.MCR001
                                   join c2 in db.MCR002 on c1.HeaderId equals c2.HeaderId
                                   where isSearchEmpty || (c2.PartCategory.ToLower().Contains(search.ToLower()))
                                   select new mcrreport
                                   {
                                       id = c2.PartCategory,
                                       text = c2.PartCategory
                                   }).Distinct().Take(isSearchEmpty ? 10 : int.MaxValue).ToList();

                /*if (lstPartCategory.Count > 0)
                {
                    lstPartCategory.Insert(0, new mcrreport { id = "ALL", text = "ALL" });//Set "ALL" as first data of control
                }*/

                return Json(lstPartCategory, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }



        public JsonResult GetUserLocation()
        {
            try
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();

                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                                  ).Distinct().ToList();
                return Json(listLocation, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
    }
}