﻿using System.Web.Mvc;

namespace IEMQS.Areas.MCR
{
    public class MCRAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "MCR";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MCR_default",
                "MCR/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}