﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.ADD.Controllers
{
    public class AssignedTaskController : clsBase
    {
        // GET: ADD/AssignedTask
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult GetIndexGridDataPartial(string route = null, string tab = null)
        {
            ViewBag.tab = tab;
            return PartialView("_GetIndexGridDataPartial");
        }
        public ActionResult loadTaskDataTable(JQueryDataTableParamModel param, string tab)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                if (!string.IsNullOrEmpty(tab) && tab.ToLower().Equals("pending"))
                    whereCondition += " and Assignee = '" + objClsLoginInfo.UserName.ToString() + "' and (TaskStatus='Submitted For Approval') "+" and (add3.IsStopped is null or add3.IsStopped = 0)";
                if (!string.IsNullOrEmpty(tab) && tab.ToLower().Equals("all"))
                    whereCondition += " and Assignee = '" + objClsLoginInfo.UserName.ToString() + "'";

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string[] columnName = { "LineId", "RouteNo", "TaskName", "TaskDescription", "TaskStatus", "AssigneeComments", "AssignedBy" };
                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {

                    whereCondition += " and (RouteNo like '%" + param.sSearch
                         + "%' or TaskName like '%" + param.sSearch
                         + "%' or TaskDescription like '%" + param.sSearch
                         + "%' or TaskStatus like '%" + param.sSearch
                         + "%' or AssigneeComments like '%" + param.sSearch
                         + "%' or AssignedBy like '%" + param.sSearch + "%')";

                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstTask = db.SP_ADD_GET_TASK_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstTask.Select(i => i.TotalCount).FirstOrDefault();
                var res = from r in lstTask
                          select new[] {
                              Convert.ToString(r.LineId),
                              Convert.ToString(r.RouteNo),
                              Convert.ToString(r.TaskName),
                              Convert.ToString(r.TaskDescription),
                              Convert.ToString(r.TaskStatus),
                              Convert.ToString(r.AssigneeComments),
                              Convert.ToString(r.AssignedBy),
                              "<nobr><center>"+
                           "<a class='iconspace' href='javascript:void(O);' title='View' onclick='showTaskDetails(" + r.LineId + ")'><i class='fa fa-eye'></i></a>"+
                           "</center></nobr>",

                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GetTaskDetails(int Id = 0)
        {
            ADD002 objADD002 = db.ADD002.FirstOrDefault(x => (x.LineId == Id));
            if (objADD002 != null)
            {
               
            }

            return PartialView("_GetTaskDetailsPartial", objADD002);
        }

        public JsonResult GetAssigneeFromADD(int LineId, string term)
        {
            List<Employee> lstEmployee = new List<Employee>();
            try
            {
                lstEmployee = (from ath1 in db.ATH001
                               join com3 in db.COM003 on ath1.Employee equals com3.t_psno
                               join ath4 in db.ATH004 on ath1.Role equals ath4.Id
                               where com3.t_actv == 1
                               select
                               new Employee { isActive = com3.t_actv, psnum = com3.t_psno, name = com3.t_psno + " - " + com3.t_name }).Distinct().ToList();

                lstEmployee = lstEmployee.Where(q => q.name.ToLower().Contains(term.ToLower())).ToList();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstEmployee, JsonRequestBehavior.AllowGet);
        }


        public ActionResult approveTask(ADD002 obj)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ADD002 objADD002 = db.ADD002.FirstOrDefault(x => x.LineId == obj.LineId);
                objADD002.AssigneeComments = obj.AssigneeComments;
                objADD002.ApprovedBy = objClsLoginInfo.UserName;
                objADD002.ApprovedOn = DateTime.Now;
                objADD002.TaskStatus = clsImplementationEnum.ADDTaskStatus.Approved.GetStringValue();
                db.SaveChanges();

                ADD001 objADD001 = db.ADD001.FirstOrDefault(x => x.HeaderId == objADD002.HeaderId);
                var lstAssignee = db.ADD002.Where(i => i.HeaderId == objADD002.HeaderId && i.RouteNo == objADD002.RouteNo).ToList();
                string approved = clsImplementationEnum.ADDTaskStatus.Approved.GetStringValue();
                objADD001 = db.ADD001.Where(i => i.HeaderId == objADD002.HeaderId).FirstOrDefault();

                if (lstAssignee.All(a => a.TaskStatus == clsImplementationEnum.ADDTaskStatus.Approved.GetStringValue()))
                {
                    ApprovedByAssignee(objADD002.HeaderId);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.ADDMessage.Approved;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        private bool ApprovedByAssignee(int? headerId)
        {
            ADD001 objADD001 = db.ADD001.FirstOrDefault(i => i.HeaderId == headerId);
            if (objADD001 != null)
            {
                objADD001.Status = clsImplementationEnum.ADDStatus.Approved.GetStringValue();

                db.SaveChanges();
                #region Approve Log Details
                var lstADD001_Log = db.TVL010_Log.Where(x => x.HeaderId == headerId).ToList();
                if (lstADD001_Log.Count() > 0)
                {
                    lstADD001_Log.ForEach(i => { i.Status = clsImplementationEnum.PlanStatus.Superseded.GetStringValue(); });
                }
                ADD001_Log objADD001_Log = new ADD001_Log();
                objADD001_Log.HeaderId = objADD001.HeaderId;
                objADD001_Log.DocumentName = objADD001.DocumentName;
                objADD001_Log.RevNo = objADD001.RevNo;
                objADD001_Log.Description = objADD001.Description;
                objADD001_Log.Title = objADD001.Title;
                objADD001_Log.EffectiveDate = objADD001.EffectiveDate;
                objADD001_Log.ModelType = objADD001.ModelType;
                objADD001_Log.AIApprovalRequired = objADD001.AIApprovalRequired;
                objADD001_Log.AIApprovalTaken = objADD001.AIApprovalTaken;
                objADD001_Log.Status = objADD001.Status;
                objADD001_Log.PEApprovalRequired = objADD001.PEApprovalRequired;
                objADD001_Log.PEApprovalTaken = objADD001.PEApprovalTaken;
                objADD001_Log.DrawingSize = objADD001.DrawingSize;
                objADD001_Log.CustomerComment = objADD001.CustomerComment;
                objADD001_Log.PageSize = objADD001.PageSize;
                objADD001_Log.PageNo = objADD001.PageNo;
                objADD001_Log.DOBApprovalRequired = objADD001.DOBApprovalRequired;
                objADD001_Log.DOBApprovalTaken = objADD001.DOBApprovalTaken;
                objADD001_Log.Remarks = objADD001.Remarks;
                objADD001_Log.ReviewedBy = objADD001.ReviewedBy;
                objADD001_Log.ReviewedOn = objADD001.ReviewedOn;
                objADD001_Log.SubmittedBy = objADD001.SubmittedBy;
                objADD001_Log.SubmittedOn = objADD001.SubmittedOn;
                objADD001_Log.CreatedBy = objADD001.CreatedBy;
                objADD001_Log.CreatedOn = objADD001.CreatedOn;
                objADD001_Log.EditedBy = objADD001.EditedBy;
                objADD001_Log.EditedOn = objADD001.EditedOn;
                objADD001_Log.Comments = objADD001.Comments;
                db.ADD001_Log.Add(objADD001_Log);
                db.SaveChanges();

                var lstADD002_Log = db.ADD002_Log.Where(x => x.HeaderId == headerId).ToList();

                if (lstADD002_Log.Count > 0)
                {
                    lstADD002_Log.ForEach(i => { i.TaskStatus = clsImplementationEnum.CommonStatus.Superseded.GetStringValue(); });
                }

                foreach (ADD002 objSRCADD002 in objADD001.ADD002)
                {

                    ADD002_Log objDestADD002 = new ADD002_Log();
                    objDestADD002.RefId = objADD001_Log.Id;
                    objDestADD002.LineId = objSRCADD002.LineId;
                    objDestADD002.HeaderId = objSRCADD002.HeaderId;
                    objDestADD002.RevNo = objSRCADD002.RevNo;
                    objDestADD002.Assignee = objSRCADD002.Assignee;
                    objDestADD002.Comments = objSRCADD002.Comments;
                    objDestADD002.AssignedBy = objSRCADD002.AssignedBy;
                    objDestADD002.AssignedOn = objSRCADD002.AssignedOn;
                    objDestADD002.TaskName = objSRCADD002.TaskName;
                    objDestADD002.TaskDescription = objSRCADD002.TaskDescription;
                    objDestADD002.TaskStatus = objSRCADD002.TaskStatus;
                    objDestADD002.AssigneeComments = objSRCADD002.AssigneeComments;
                    objDestADD002.ApprovedBy = objSRCADD002.ApprovedBy;
                    objDestADD002.ApprovedOn = objSRCADD002.ApprovedOn;
                    objDestADD002.RejectedBy = objSRCADD002.RejectedBy;
                    objDestADD002.RejectedOn = objSRCADD002.RejectedOn;
                    objDestADD002.CreatedBy = objSRCADD002.CreatedBy;
                    objDestADD002.CreatedOn = objSRCADD002.CreatedOn;
                    objDestADD002.EditedBy = objSRCADD002.EditedBy;
                    objDestADD002.EditedOn = objSRCADD002.EditedOn;
                    objDestADD002.RouteNo = objSRCADD002.RouteNo;
                    db.ADD002_Log.Add(objDestADD002);
                }
                db.SaveChanges();
                #endregion
                return true;
            }
            return false;
        }
        public ActionResult transferTask(int Id, string psno)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ADD002 objADD002 = db.ADD002.FirstOrDefault(x => x.LineId == Id);

                objADD002.Assignee = psno;
                objADD002.AssignedBy = objClsLoginInfo.UserName;
                objADD002.AssignedOn = DateTime.Now;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.ADDMessage.Transfered;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult rejectTask(ADD002 obj)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ADD001 objADD001 = db.ADD001.FirstOrDefault(x => x.HeaderId == obj.HeaderId);
                ADD002 objADD002 = db.ADD002.FirstOrDefault(x => x.LineId == obj.LineId);
                if (objADD002 != null)
                {
                    objADD002.TaskStatus = clsImplementationEnum.ADDTaskStatus.Return.GetStringValue();
                    objADD002.AssigneeComments = obj.AssigneeComments;
                    objADD002.RejectedBy = objClsLoginInfo.UserName;
                    objADD002.RejectedOn = DateTime.Now;
                    ADD003 objADD003 = db.ADD003.FirstOrDefault(x => x.RouteNo == objADD002.RouteNo);
                    objADD003.IsStopped = true;
                    if (objADD001 != null)
                    {
                        objADD001.Status = clsImplementationEnum.ADDStatus.ReturnByAssignee.GetStringValue();
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.ADDMessage.Reject;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excel
        
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

               
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_ADD_GET_TASK_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      RouteNo = Convert.ToString(uc.RouteNo),
                                      TaskName = Convert.ToString(uc.TaskName),
                                      TaskDescription = Convert.ToString(uc.TaskDescription),
                                      TaskStatus = Convert.ToString(uc.TaskStatus),
                                      AssigneeComments = Convert.ToString(uc.AssigneeComments),
                                      AssignedBy = Convert.ToString(uc.AssignedBy),
                                      
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}