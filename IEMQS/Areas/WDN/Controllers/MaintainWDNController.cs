﻿using IEMQS.Areas.PDIN.Controllers;
using IEMQS.Models;
using IEMQS.DESCore;
using IEMQS.DESServices;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Collections;
using IEMQS.Areas.Utility.Controllers;

namespace IEMQS.Areas.WDN.Controllers
{
    public class MaintainWDNController : clsBase
    {
        public const string WDINReleased = "WDIN Released Successfully!";
        public const string WDINRevise = "WDIN Revise Successfully!";
        public const string MaintainOneDocument = "Please Maintain Atlease One Document For Release WDIN!";
        public const string WDINAlreadyRelease = "WDIN Already Released!";
        public const string WDINNotExist = "WDIN Not Exist!";
        public const string WDINDocumentNotExist = "WDIN Document Not Exist!";
        public const string DocumentIsInRelease = "WDIN Document Is In Release Status! So You Can Not Delete This Document!";
        public const string DocumentDeleted = "Document Deleted Successfully!";
        public const string WDINNotInReleaseStatus = "WDIN Is Not In Release Status! So You Can Not Revise WDIN!";
        // GET: WDN/MaintainWDN
        [SessionExpireFilter]
        public ActionResult Index()
        {
            ViewBag.IsDisplayOnly = false;
            ViewBag.Title = "Maintain DIN";
            return View();
        }

        [SessionExpireFilter]
        public ActionResult DisplayWDN()
        {
            ViewBag.IsDisplayOnly = true;
            ViewBag.Title = "Display DIN";
            return View("Index");
        }

        [HttpPost]
        public ActionResult GetGridDataPartial(string status, string title, bool IsDisplayOnly = false)
        {
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            ViewBag.Status = status;
            ViewBag.GridTitle = title;

            return PartialView("_GetGridDataPartial");
        }

        public ActionResult LoadWDNHeaderData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;
                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                string whereCondition = "1=1";

                if (status.ToLower() == "pending")
                    whereCondition += " and status in ('" + clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue() + "')";
                else
                    whereCondition += " and status in ('" + clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue() + "')";

                whereCondition += IsDisplayOnly ? (" and isLatest =1 ") : "";

                string[] columnName = { "Project", "Location", "IProject", "Status", "IssueNo", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = db.SP_WDN_GETHEADERDETAILS(objClsLoginInfo.UserName, StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.Project),
                               Convert.ToString(h.Location),
                               Convert.ToString(h.IssueNo),
                               Convert.ToString(h.Status),
                               Convert.ToString(h.CreatedBy),
                               Convert.ToString(h.CreatedOn),
                               Convert.ToString(h.IProject),
                               "<center>"+ (IsDisplayOnly ? Helper.GenerateActionIcon(h.HeaderId, "View", "View Detail", "fa fa-eye", "PrintWDIN("+h.HeaderId+")","",false,false,"color:#337ab7") : Helper.GenerateActionIcon(h.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/WDN/MaintainWDN/WDNDetails/"+h.HeaderId ,false))
                               //+Helper.GenerateActionIcon(h.HeaderId, "Print", "Print", "fa fa-print", "PrintWDIN("+h.HeaderId+")", "",false)
                               +"</center>"
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult WDNDetails(int? id)
        {
            ViewBag.IsDisplayOnly = false;
            WDN001 objWDN001 = null;
            int? HeaderId = id.HasValue ? id : 0;
            if (HeaderId > 0)
            {
                objWDN001 = db.WDN001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                ViewBag.Project = Manager.GetProjectAndDescription(objWDN001.Project);
                var MaxIssueNoHeaderId = db.WDN001.Where(x => x.Project == objWDN001.Project && x.Location == objWDN001.Location).Max(i => i.HeaderId);
                ViewBag.IsMaxIssueNoHeaderId = MaxIssueNoHeaderId == objWDN001.HeaderId;
            }
            else
            {
                objWDN001 = new WDN001();
                objWDN001.IssueNo = 1;
                objWDN001.Status = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue();
                ViewBag.IsMaxIssueNoHeaderId = false;
            }
            return View(objWDN001);
        }

        [SessionExpireFilter]
        public ActionResult WDNDisplayDetails(int? id)
        {
            ViewBag.IsDisplayOnly = true;
            WDN001 objWDN001 = null;
            int? HeaderId = id;

            if (HeaderId > 0)
            {
                var objFKM102 = db.FKM102.Where(x => x.HeaderId == HeaderId).ToList();
                objWDN001 = db.WDN001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                ViewBag.Project = Manager.GetProjectAndDescription(objWDN001.Project);
            }
            else
            {
                objWDN001 = new WDN001();
                objWDN001.IssueNo = 1;
                objWDN001.Status = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue();
            }
            ViewBag.IsMaxIssueNoHeaderId = false;
            return View("WDNDetails", objWDN001);
        }

        [SessionExpireFilter, HttpPost]
        public ActionResult SaveNewRecord(WDN001 model, string IProjectt)
        {
            WDN001 objwdn001 = null;
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                if (model.HeaderId > 0)
                {
                    objwdn001 = db.WDN001.Where(x => x.HeaderId == model.HeaderId).FirstOrDefault();
                    List<string> OldDepartmentList = new List<string>();
                    List<string> NewDepartmentList = new List<string>();
                    List<string> lstDeptDef = new List<string>();
                    string DeptDef = string.Empty;
                    var objOldDepartment = db.WDN001.Where(x => x.Project == objwdn001.Project && x.Location == objwdn001.Location && x.IssueNo != objwdn001.IssueNo).OrderByDescending(x => x.IssueNo).FirstOrDefault();
                    objwdn001.IProject = IProjectt;
                    objwdn001.Departments = model.Departments;
                    objwdn001.DeptDef = getDepartmentDifferance(objOldDepartment?.Departments, model.Departments);
                    objwdn001.ModifiedBy = objClsLoginInfo.UserName;
                    objwdn001.ModifiedOn = DateTime.Now;
                    objwdn001.CodeStamp = model.CodeStamp;
                    objwdn001.CustTPIA = model.CustTPIA;
                    objwdn001.SafetyNuclear = model.SafetyNuclear;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    objResponseMsg.HeaderId = objwdn001.HeaderId;
                }
                else
                {
                    objwdn001 = new WDN001();
                    objwdn001.Project = model.Project;
                    objwdn001.IProject = IProjectt;
                    objwdn001.Departments = model.Departments;
                    objwdn001.IssueNo = model.IssueNo;
                    objwdn001.Status = model.Status;
                    objwdn001.Location = model.Location;
                    objwdn001.CodeStamp = model.CodeStamp;
                    objwdn001.CustTPIA = model.CustTPIA;
                    objwdn001.SafetyNuclear = model.SafetyNuclear;
                    objwdn001.CreatedOn = DateTime.Now;
                    objwdn001.CreatedBy = objClsLoginInfo.UserName;
                    objwdn001.ModifiedBy = objClsLoginInfo.UserName;
                    objwdn001.ModifiedOn = DateTime.Now;
                    db.WDN001.Add(objwdn001);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    objResponseMsg.HeaderId = objwdn001.HeaderId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg);
        }

        public string getDepartmentDifferance(string OldDepartment, string NewDepartment)
        {
            string DeptDef = string.Empty;
            List<string> OldDepartmentList = new List<string>();
            List<string> NewDepartmentList = new List<string>();
            List<string> lstDeptDef = new List<string>();
            if (OldDepartment != null)
            {
                OldDepartmentList = OldDepartment.Split(',').ToList();
            }
            NewDepartmentList = NewDepartment.Split(',').ToList();
            foreach (var lst in NewDepartmentList)
            {
                if (!OldDepartmentList.Contains(lst))
                {
                    lstDeptDef.Add(lst);
                }
            }
            if (lstDeptDef.Count() > 0)
            {
                DeptDef = string.Join(",", lstDeptDef);
            }
            return DeptDef;
        }

        [HttpPost]
        public ActionResult GetHeaderData(string project, string location)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var ExistsPrject = db.COM001.Any(x => x.t_cprj == project);
                if (ExistsPrject == false)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Invalid Project..! Please Try Again..!";
                }
                else
                {
                    bool obj = db.WDN001.Where(x => x.Project.ToLower() == project.ToLower() && x.Location.ToLower() == location.ToLower()).Any();
                    if (obj == true)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Project already Exists..!";
                    }
                    else
                    {
                        var lstProjects = CheckProjectExistWDNFor(project, 0, location).ToList();
                        if (lstProjects != null && lstProjects.Count > 0)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Project Already Exist Or Maintain As Identical Project!";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                        objResponseMsg.Key = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProjectAlreadyExistOrInIproject(int HeaderId, string Location)
        {
            List<JPPProjects> lstProjects = new List<JPPProjects>();
            try
            {
                lstProjects = CheckProjectExistWDNFor("", HeaderId, Location).ToList();
            }
            catch (Exception)
            {

                throw;
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetProjectResult(string term, string location = "", string project = "")
        {
            List<Projects> lstProjects = new List<Projects>();
            var objAccessProjects = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, location).ToList();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProjects = (from c1 in db.COM001
                               where objAccessProjects.Contains(c1.t_cprj) && (c1.t_dsca.Contains(term) || c1.t_cprj.Contains(term))
                               select new Projects
                               {
                                   Value = c1.t_cprj,
                                   projectCode = c1.t_cprj,
                                   projectDescription = c1.t_cprj + " - " + c1.t_dsca,
                                   Text = c1.t_cprj + " - " + c1.t_dsca
                               }
                              ).Distinct().ToList();
            }
            else
            {
                lstProjects = (from c1 in db.COM001
                               where objAccessProjects.Contains(c1.t_cprj)
                               select new Projects
                               {
                                   Value = c1.t_cprj,
                                   projectCode = c1.t_cprj,
                                   projectDescription = c1.t_cprj + " - " + c1.t_dsca,
                                   Text = c1.t_cprj + " - " + c1.t_dsca
                               }).Distinct().ToList();
            }
            if (!string.IsNullOrEmpty(project))
            {
                string contractno = (from cm004 in db.COM004
                                     join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                     where cm005.t_sprj == project
                                     select cm004.t_cono).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(contractno))
                {
                    string[] arrcontIds = contractno.Split(',');
                    lstProjects = (from cm001 in db.COM001
                                   join cm005 in db.COM005 on cm001.t_cprj equals cm005.t_sprj
                                   where arrcontIds.Contains(cm005.t_cono) && cm001.t_psts == 2
                                   select new Projects
                                   {
                                       Value = cm001.t_cprj,
                                       projectCode = cm001.t_cprj,
                                       projectDescription = cm001.t_cprj + " - " + cm001.t_dsca,
                                       Text = cm001.t_cprj + " - " + cm001.t_dsca
                                   }).Distinct().ToList();
                }
                lstProjects = lstProjects.Where(i => i.Value != project).ToList();
            }
            else
            {
                lstProjects = lstProjects.Where(i => i.Value != project).Take(10).ToList();
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        public IQueryable<JPPProjects> CheckProjectExistWDNFor(string Project = "", int HeaderId = 0, string Location = "")
        {
            var query = string.Format("SELECT * FROM dbo.fn_GetExistingWDNProjectList() WHERE HeaderId!={1} and project=case when '{0}'='' then project else '{0}' end and Location=case when '{2}'='' then Location else '{2}' end", Project, HeaderId, Location);
            return db.Database.SqlQuery<JPPProjects>(query).AsQueryable();
        }

        public JsonResult LoadDataGridData(JQueryDataTableParamModel param, string role)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = "1=1 ";

                string[] columnName = { "DocumentDesc", "DocumentNo", "RevNo" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);

                int headerid = Convert.ToInt32(param.Headerid);
                var IsDisplayOnly = Convert.ToBoolean(param.IsDisplayOnly);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_WDN_GETLINEDETAILS(headerid, StartIndex, EndIndex, strSortOrder, strWhere).ToList();
                var objWDN001 = db.WDN001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                int? IsMaxIssueNoHeaderId = db.WDN001.Where(x => x.Project == objWDN001.Project && x.Location == objWDN001.Location).Max(i => i.HeaderId);
                IsMaxIssueNoHeaderId = IsMaxIssueNoHeaderId.HasValue ? IsMaxIssueNoHeaderId : headerid;
                bool IsMaxHeaderId = IsMaxIssueNoHeaderId == headerid;
                bool IsDraft = objWDN001.Status == clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue();
                //var statusApproved = clsImplementationEnum.DENStatus.Approved.GetStringValue();
                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.LineId),
                               uc.DocumentDesc,
                               uc.DocumentNo,
                               uc.Lastrevno.HasValue && uc.Lastrevno.Value==uc.RevNo?"": Convert.ToString(uc.Lastrevno),
                               Convert.ToString(uc.RevNo),

                               /*Convert.ToString(uc.Department1),
                               Convert.ToString(uc.Department2),
                               Convert.ToString(uc.Department3),
                               Convert.ToString(uc.Department4),
                               Convert.ToString(uc.Department5),

                               Convert.ToString(uc.Department6),
                               Convert.ToString(uc.Department7),
                               Convert.ToString(uc.Department8),
                               Convert.ToString(uc.Department9),
                               Convert.ToString(uc.Department10),

                               Convert.ToString(uc.Department11),
                               Convert.ToString(uc.Department12),
                               Convert.ToString(uc.Department13),
                               Convert.ToString(uc.Department14),
                               Convert.ToString(uc.Department15),*/
                               Convert.ToString(uc.Notes),
                              !IsDisplayOnly ? ("<center>"+
                               ((IsMaxHeaderId && IsDraft) ?  "<i id=\"btnDelete\" name=\"btnAction\" Title=\"Delete\" class=\"fa fa-trash-o " +(uc.RefLineId.HasValue ? "disabledIcon\"" : "iconspace\"" )+"   " +(uc.RefLineId.HasValue ? "" : "onclick=\"DeleteLine('"+uc.LineId+"')\"" ) + "></i>":"" )+
                               GenerateGridButton(uc.LineId, "Attachments", "View"+((uc.CreatedBy.Trim() == objClsLoginInfo.UserName.Trim()) ? "/Edit":"")+" Attachments", "fa fa-paperclip "+(uc.HasAttachment.HasValue && uc.HasAttachment.Value?" hasattachment ":""), "showAttachments(this, "+ uc.RevNo + ","+ uc.LineId + ","+(uc.CreatedBy.Trim() != objClsLoginInfo.UserName.Trim()).ToString().ToLower()+",'"+(uc.HasAttachment!=null ? uc.HasAttachment.Value : false)+"')")+
                               "</center>"):GenerateGridButton(uc.LineId, "Attachments", "View"+((uc.CreatedBy.Trim() == objClsLoginInfo.UserName.Trim()) ? "/Edit":"")+" Attachments", "fa fa-paperclip "+(uc.HasAttachment.HasValue && uc.HasAttachment.Value?" hasattachment ":""), "showAttachments(this, "+ uc.RevNo + ","+ uc.LineId + ","+(uc.CreatedBy.Trim() != objClsLoginInfo.UserName.Trim()).ToString().ToLower()+",'"+(uc.HasAttachment!=null ? uc.HasAttachment.Value : false)+"')")

                               //("<span class='editable'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='DeleteLine("+uc.LineId+")'><i class='fa fa-trash'></i></a></span>")
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, string hid, bool isReadOnly = false)
        {
            try
            {
                int headerid = Convert.ToInt32(hid);
                var data = new List<string[]>();

                if (id == 0)
                {
                    var QPitems = new SelectList(new List<string>());
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        "0",
                                        Helper.GenerateTextbox(newRecordId, "DocumentDesc",  "", "", false, "", "70","DocumentDesc"),
                                        Helper.GenerateTextbox(newRecordId, "DocumentNo",  "", "", false, "", "50","DocumentNo"),
                                        Helper.GenerateTextbox(newRecordId, "Lastrevno",  "", "", true, "", "3","numeric33 Lastrevno"),
                                        Helper.GenerateTextbox(newRecordId, "RevNo",  "", "", false, "", "3","numeric33 RevNo"),
                                        
                                        /*Helper.HTMLAutoComplete(newRecordId,"Department1","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment1", "","Department1"),
                                        Helper.HTMLAutoComplete(newRecordId,"Department2","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment2", "","Department2"),
                                        Helper.HTMLAutoComplete(newRecordId,"Department3","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment3", "","Department3"),
                                        Helper.HTMLAutoComplete(newRecordId,"Department4","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment4", "","Department4"),
                                        Helper.HTMLAutoComplete(newRecordId,"Department5","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment5", "","Department5"),

                                        Helper.HTMLAutoComplete(newRecordId,"Department6","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment6", "","Department6"),
                                        Helper.HTMLAutoComplete(newRecordId,"Department7","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment7", "","Department7"),
                                        Helper.HTMLAutoComplete(newRecordId,"Department8","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment8", "","Department8"),
                                        Helper.HTMLAutoComplete(newRecordId,"Department9","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment9", "","Department9"),
                                        Helper.HTMLAutoComplete(newRecordId,"Department10","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment10", "","Department10"),

                                        Helper.HTMLAutoComplete(newRecordId,"Department11","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment11", "","Department11"),
                                        Helper.HTMLAutoComplete(newRecordId,"Department12","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment12", "","Department12"),
                                        Helper.HTMLAutoComplete(newRecordId,"Department13","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment13", "","Department13"),
                                        Helper.HTMLAutoComplete(newRecordId,"Department14","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment14", "","Department14"),
                                        Helper.HTMLAutoComplete(newRecordId,"Department15","","",false,"","",false,"","","Select","Department") + GenerateHidden(newRecordId, "txtDepartment15", "","Department15"),*/

                                        Helper.GenerateTextArea(newRecordId, "Notes",  "", "", false, "", "100","Notes"),

                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "Savedata()" ),
                                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_WDN_GETLINEDETAILS(headerid, 1, 0, "", "LineId = " + id).Take(1).ToList();
                    if (isReadOnly)
                    {
                        data = (from uc in lstResult
                                select new[]
                               {
                                Convert.ToString(uc.LineId),
                                uc.DocumentDesc,
                               uc.DocumentNo,
                               uc.Lastrevno.HasValue && uc.Lastrevno.Value==uc.RevNo?"": Convert.ToString(uc.Lastrevno),
                               Convert.ToString(uc.RevNo),

                               /*Convert.ToString(uc.Department1),
                               Convert.ToString(uc.Department2),
                               Convert.ToString(uc.Department3),
                               Convert.ToString(uc.Department4),
                               Convert.ToString(uc.Department5),

                               Convert.ToString(uc.Department6),
                               Convert.ToString(uc.Department7),
                               Convert.ToString(uc.Department8),
                               Convert.ToString(uc.Department9),
                               Convert.ToString(uc.Department10),

                               Convert.ToString(uc.Department11),
                               Convert.ToString(uc.Department12),
                               Convert.ToString(uc.Department13),
                               Convert.ToString(uc.Department14),
                               Convert.ToString(uc.Department15),*/
                               Convert.ToString(uc.Notes),

                              (!(uc.RefLineId.HasValue && uc.RefLineId.Value>0) ? "<span class='iconspace'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='DeleteLine("+uc.LineId+")'><i class='fa fa-trash'></i></a></span>" : "")+
                              "<span class='iconspace'><a id='Cancel' name='Cancel' title='Cancel' class='blue action' onclick='ReloadGrid()'><i class='fa fa-close'></i></a></span>" +
                              GenerateGridButton(uc.LineId, "Attachments", "View"+((uc.CreatedBy.Trim() == objClsLoginInfo.UserName.Trim()) ? "/Edit":"")+" Attachments", "fa fa-paperclip "+(uc.HasAttachment.HasValue && uc.HasAttachment.Value?" hasattachment ":""), "showAttachments(this, "+ uc.RevNo + ","+ uc.LineId + ","+(uc.CreatedBy.Trim() != objClsLoginInfo.UserName.Trim()).ToString().ToLower()+",'"+(uc.HasAttachment!=null ? uc.HasAttachment.Value : false)+"')"),

                          }).ToList();
                    }
                    else
                    {
                        data = (from uc in lstResult
                                select new[] {
                                        uc.LineId.ToString(),
                                        !uc.RefLineId.HasValue ? Helper.GenerateTextbox(uc.LineId, "DocumentDesc",uc.DocumentDesc, "UpdateData(this, "+ uc.LineId+",true)", false, "", "70","DocumentDesc") : uc.DocumentDesc,
                                        uc.RefLineId.HasValue && uc.RefLineId.Value>0 ? uc.DocumentNo : Helper.GenerateTextbox(uc.LineId, "DocumentNo",uc.DocumentNo, "UpdateData(this, "+ uc.LineId+",true)", false, "", "50","DocumentNo"),
                                        uc.Lastrevno.HasValue && uc.Lastrevno.Value == uc.RevNo ? "": Helper.GenerateTextbox(uc.LineId, "Lastrevno",  Convert.ToString(uc.Lastrevno), "", true, "", "3","Lastrevno numeric33"),
                                        Helper.GenerateTextbox(uc.LineId, "RevNo",  Convert.ToString(uc.RevNo), "UpdateData(this, "+ uc.LineId+",true)", false, "", "3","RevNo numeric33"),

                                        /*Helper.HTMLAutoComplete(uc.LineId, "Department1",uc.Department1,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment1", uc.dept1 ,"Department1"),
                                        Helper.HTMLAutoComplete(uc.LineId, "Department2",uc.Department2,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment2", uc.dept2 ,"Department2"),
                                        Helper.HTMLAutoComplete(uc.LineId, "Department3",uc.Department3,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment3", uc.dept3 ,"Department3"),
                                        Helper.HTMLAutoComplete(uc.LineId, "Department4",uc.Department4,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment4", uc.dept4 ,"Department4"),
                                        Helper.HTMLAutoComplete(uc.LineId, "Department5",uc.Department5,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment5", uc.dept5 ,"Department5"),

                                        Helper.HTMLAutoComplete(uc.LineId, "Department6",uc.Department6,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment6", uc.dept6,"Department6"),
                                        Helper.HTMLAutoComplete(uc.LineId, "Department7",uc.Department7,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment7", uc.dept7,"Department7"),
                                        Helper.HTMLAutoComplete(uc.LineId, "Department8",uc.Department8,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment8", uc.dept8,"Department8"),
                                        Helper.HTMLAutoComplete(uc.LineId, "Department9",uc.Department9,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment9", uc.dept9,"Department9"),
                                        Helper.HTMLAutoComplete(uc.LineId, "Department10",uc.Department10,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment10", uc.dept10,"Department10"),

                                        Helper.HTMLAutoComplete(uc.LineId, "Department11",uc.Department11,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment11", uc.dept11,"Department11"),
                                        Helper.HTMLAutoComplete(uc.LineId, "Department12",uc.Department12,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment12", uc.dept12,"Department12"),
                                        Helper.HTMLAutoComplete(uc.LineId, "Department13",uc.Department13,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment13", uc.dept13,"Department13"),
                                        Helper.HTMLAutoComplete(uc.LineId, "Department14",uc.Department14,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment14", uc.dept14,"Department14"),
                                        Helper.HTMLAutoComplete(uc.LineId, "Department15",uc.Department15,"", false, "", "",false,"","","Select","Department") + GenerateHidden(uc.LineId, "txtDepartment15", uc.dept15,"Department15"),*/
                                        Helper.GenerateTextArea(uc.LineId, "Notes",  Convert.ToString(uc.Notes), "UpdateData(this, "+ uc.LineId+")", false, "", "100",""),

                                        (!(uc.RefLineId.HasValue && uc.RefLineId.Value>0) ? "<span class='iconspace'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='DeleteLine("+uc.LineId+")'><i class='fa fa-trash'></i></a></span>" : "")+
                                        "<span class='iconspace'><a id='Cancel' name='Cancel' title='Cancel' class='blue action' onclick='ReloadGrid()'><i class='fa fa-close'></i></a></span>" +
                                        GenerateGridButton(uc.LineId, "Attachments", "View"+((uc.CreatedBy.Trim() == objClsLoginInfo.UserName.Trim()) ? "/Edit":"")+" Attachments", "fa fa-paperclip "+(uc.HasAttachment.HasValue && uc.HasAttachment.Value?" hasattachment ":""), "showAttachments(this, "+ uc.RevNo + ","+ uc.LineId + ","+(uc.CreatedBy.Trim() != objClsLoginInfo.UserName.Trim()).ToString().ToLower()+",'"+(uc.HasAttachment!=null ? uc.HasAttachment.Value : false)+"')"),

                          }).ToList();
                    }
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost, SessionExpireFilter]
        public ActionResult SavelineNewRecord(FormCollection fc)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var wdn002 = new WDN002();

                int headerid = Convert.ToInt32(!string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0);

                if (headerid > 0)
                {
                    var objwdn001 = db.WDN001.Where(x => x.HeaderId == headerid).FirstOrDefault();

                    if (objwdn001 != null)
                    {
                        wdn002.DocumentDesc = fc["DocumentDesc" + 0];
                        wdn002.DocumentNo = fc["DocumentNo" + 0];
                        wdn002.RevNo = Convert.ToInt32(!string.IsNullOrEmpty(fc["RevNo" + 0]) ? Convert.ToInt32(fc["RevNo" + 0]) : 0);

                        /*wdn002.Department1 = Convert.ToString(fc["txtDepartment1" + 0]);
                        wdn002.Department2 = Convert.ToString(fc["txtDepartment2" + 0]);
                        wdn002.Department3 = Convert.ToString(fc["txtDepartment3" + 0]);
                        wdn002.Department4 = Convert.ToString(fc["txtDepartment4" + 0]);
                        wdn002.Department5 = Convert.ToString(fc["txtDepartment5" + 0]);
                        wdn002.Department6 = Convert.ToString(fc["txtDepartment6" + 0]);
                        wdn002.Department7 = Convert.ToString(fc["txtDepartment7" + 0]);
                        wdn002.Department8 = Convert.ToString(fc["txtDepartment8" + 0]);
                        wdn002.Department9 = Convert.ToString(fc["txtDepartment9" + 0]);
                        wdn002.Department10 = Convert.ToString(fc["txtDepartment10" + 0]);

                        wdn002.Department11 = Convert.ToString(fc["txtDepartment11" + 0]);
                        wdn002.Department12 = Convert.ToString(fc["txtDepartment12" + 0]);
                        wdn002.Department13 = Convert.ToString(fc["txtDepartment13" + 0]);
                        wdn002.Department14 = Convert.ToString(fc["txtDepartment14" + 0]);
                        wdn002.Department15 = Convert.ToString(fc["txtDepartment15" + 0]);*/
                        wdn002.Notes = Convert.ToString(fc["Notes" + 0]);
                        wdn002.Location = objwdn001.Location;
                        wdn002.Project = objwdn001.Project;
                        wdn002.IssueStatus = objwdn001.Status;
                        wdn002.IssueNo = objwdn001.IssueNo;
                        wdn002.HeaderId = objwdn001.HeaderId;

                        wdn002.CreatedOn = DateTime.Now;
                        wdn002.CreatedBy = objClsLoginInfo.UserName;

                        db.WDN002.Add(wdn002);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Data save successfully.";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Main Project Not Availble";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Main Project Not Initiatied";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        [HttpPost, SessionExpireFilter]
        public ActionResult DeleteRecord(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objWDN002 = db.WDN002.Where(x => x.LineId == Id).FirstOrDefault();
                if (objWDN002 != null)
                {
                    if (objWDN002.IssueStatus == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = DocumentIsInRelease;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    db.WDN002.Remove(objWDN002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = DocumentDeleted;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = WDINDocumentNotExist;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost, SessionExpireFilter]
        public ActionResult ReleaseWDIN(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objWDN001 = db.WDN001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objWDN001 != null)
                {
                    if (objWDN001.Status == clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue())
                    {
                        if (objWDN001.WDN002.Count > 0)
                        {
                            foreach (var item in objWDN001.WDN002)
                            {
                                #region Get Documents
                                //var folderPath = "WDN002/" + item.LineId + "/R" + item.RevNo;
                                //var existing = (new clsFileUpload()).GetDocuments(folderPath);

                                //if (!existing.Any())
                                FileUploadController _obj = new FileUploadController();
                                if (!_obj.CheckAnyDocumentsExits("WDN002//" + item.LineId + "//R" + item.RevNo, item.LineId))
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = "Attachment(s) is Required in" + " " + item.DocumentDesc;
                                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                                }
                                #endregion
                            }
                            objWDN001.Status = clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue();
                            objWDN001.ReleaseBy = objClsLoginInfo.UserName;
                            objWDN001.ReleasedOn = DateTime.Now;
                            objWDN001.WDN002.ToList().ForEach(x =>
                            {
                                x.IssueStatus = clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue();
                            });
                            db.SaveChanges();

                            #region Send Mail
                            string emailTo = !string.IsNullOrWhiteSpace(objWDN001.Departments) ? FetchDepartment(objWDN001.Departments) : string.Empty;
                            if (!string.IsNullOrWhiteSpace(emailTo))
                            {
                                Hashtable _ht = new Hashtable();
                                EmailSend _objEmail = new EmailSend();
                                _ht["[Issueno]"] = objWDN001.IssueNo;
                                _ht["[Project]"] = objWDN001.Project;
                                _ht["[IdenticalProject]"] = objWDN001.IProject;
                                _ht["[Link]"] = WebsiteURL + "/WDN/MaintainWDN/PrintWDIN?HeaderId=" + objWDN001.HeaderId;
                                MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.WDIN.ReleasedWDN).SingleOrDefault();
                                _objEmail.MailToAdd = emailTo;// Manager.GetMailIdFromPsNo(objWDN001.CreatedBy);
                                _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                            }
                            #endregion

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = WDINReleased;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = MaintainOneDocument;
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = WDINAlreadyRelease;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = WDINNotExist;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string FetchDepartment(string dept)
        {
            var lstdept = dept.Split(',').ToArray();
            string[] arrRole = { clsImplementationEnum.UserRoleName.WE2.GetStringValue(), clsImplementationEnum.UserRoleName.WE3.GetStringValue(),
                clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), clsImplementationEnum.UserRoleName.MFG1.GetStringValue(), clsImplementationEnum.UserRoleName.STR2.GetStringValue()
                , clsImplementationEnum.UserRoleName.STR3.GetStringValue(), clsImplementationEnum.UserRoleName.PLNG2.GetStringValue(), clsImplementationEnum.UserRoleName.PLNG3.GetStringValue()
                , clsImplementationEnum.UserRoleName.QC2.GetStringValue(), clsImplementationEnum.UserRoleName.QC3.GetStringValue(), clsImplementationEnum.UserRoleName.QI2.GetStringValue()
                , clsImplementationEnum.UserRoleName.QI3.GetStringValue(), clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), clsImplementationEnum.UserRoleName.PMG3.GetStringValue() };
            var lspDeptwisePerson = (from a in db.COM003
                                     join c in db.ATH001 on a.t_psno equals c.Employee
                                     join d in db.ATH004 on c.Role equals d.Id
                                     join b in db.COM007 on c.Employee equals b.t_emno
                                     where lstdept.Contains(a.t_depc) && a.t_actv == 1
                                            && arrRole.Contains(d.Role)
                                     select b.t_mail).Distinct().ToList();
            string DeptwisePerson = lspDeptwisePerson.Count() > 0 ? string.Join(";", lspDeptwisePerson) : "";
            return DeptwisePerson;
        }
        [HttpPost, SessionExpireFilter]
        public ActionResult ReviseDIN(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objWDN001 = db.WDN001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objWDN001 != null)
                {
                    if (objWDN001.Status == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue())
                    {
                        WDN001 objNewWDN001 = new WDN001();
                        objNewWDN001 = objWDN001;
                        objNewWDN001.Location = objWDN001.Location;
                        objNewWDN001.Project = objWDN001.Project;
                        objNewWDN001.IProject = objWDN001.IProject;
                        objNewWDN001.Departments = objWDN001.Departments;
                        objNewWDN001.IssueNo = objWDN001.IssueNo + 1;
                        objNewWDN001.CodeStamp = objWDN001.CodeStamp;
                        objNewWDN001.CustTPIA = objWDN001.CustTPIA;
                        objNewWDN001.SafetyNuclear = objWDN001.SafetyNuclear;
                        objNewWDN001.Status = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue();
                        objNewWDN001.CreatedBy = objClsLoginInfo.UserName;
                        objNewWDN001.CreatedOn = DateTime.Now;
                        objNewWDN001.ReleaseBy = null;
                        objNewWDN001.ReleasedOn = null;
                        objNewWDN001.ModifiedBy = objClsLoginInfo.UserName;
                        objNewWDN001.ModifiedOn = DateTime.Now;
                        //List<wdncopy> listwdncopy = new List<wdncopy>();
                        foreach (var objWDN002 in objWDN001.WDN002)
                        {
                            var SourceLineId = objWDN002.LineId;
                            WDN002 objNewWDN002 = new WDN002();
                            objNewWDN002 = objWDN002;
                            objNewWDN002.RevNo = objWDN002.RevNo;
                            objNewWDN002.HeaderId = objNewWDN001.HeaderId;
                            objNewWDN002.IssueNo = objNewWDN001.IssueNo;
                            objNewWDN002.IssueStatus = objNewWDN001.Status;
                            objNewWDN002.CreatedBy = objClsLoginInfo.UserName;
                            objNewWDN002.CreatedOn = DateTime.Now;
                            objNewWDN002.EditedBy = null;
                            objNewWDN002.EditedOn = null;
                            objNewWDN002.RefLineId = SourceLineId;
                            objNewWDN002.HasAttachment = objWDN002.HasAttachment;
                            db.Entry(objNewWDN002).State = System.Data.Entity.EntityState.Added;
                            objNewWDN001.WDN002.Add(objNewWDN002);
                            //listwdncopy.Add(new wdncopy { SourceLineId = SourceLineId, destidata = objNewWDN002 });
                        }
                        db.Entry(objNewWDN001).State = System.Data.Entity.EntityState.Added;
                        db.WDN001.Add(objNewWDN001);
                        db.SaveChanges();
                        /*observation 26239 */
                        foreach (var item in objNewWDN001.WDN002)
                        {
                            #region copy documents
                            string srcfolderPath = "WDN002//" + item.RefLineId + "//R" + item.RevNo;
                            string destfolderPath = "WDN002//" + item.LineId + "//R" + item.RevNo;
                            //(new clsFileUpload()).CopyFolderContentsAsync(srcfolderPath, destfolderPath);
                            FileUploadController _objFUC = new FileUploadController();
                            _objFUC.CopyDataOnFCSServerAsync(srcfolderPath, destfolderPath, srcfolderPath, (int)item.RefLineId, destfolderPath, item.LineId, CommonService.GetUseIPConfig, CommonService.objClsLoginInfo.UserName, 0);
                            #endregion
                        }
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = objNewWDN001.HeaderId.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = WDINNotInReleaseStatus;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = WDINNotExist;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string GetAttachment(int id, string attachment, int rev)
        {
            //var folderPath = "WDN002/" + id + "/R" + rev;
            //var existing = (new clsFileUpload()).GetDocuments(folderPath);

            //if (!existing.Any())
            //{
            //    attachment = "False";
            //}
            //else
            //{
            //    attachment = "True";
            //}
            FileUploadController _obj = new FileUploadController();
            attachment = _obj.CheckAnyDocumentsExits("WDN002//" + id + "//R" + rev, id).ToString();
            return attachment;
        }
        public ActionResult UpdateAttachmentUploadForDocument(int LineId, int RevId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objWDN002 = db.WDN002.Where(i => i.LineId == LineId).FirstOrDefault();
                //var IsAnyFile = (new clsFileUpload()).CheckAnyFileUploadInFolder("WDN002/" + LineId.ToString() + "/R" + RevId.ToString());
                FileUploadController _obj = new FileUploadController();
                if (_obj.CheckAnyDocumentsExits("WDN002//" + LineId + "//R" + RevId, LineId))
                {
                    objWDN002.HasAttachment = true;
                }
                else
                {
                    objWDN002.HasAttachment = false;
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost, SessionExpireFilter]
        public ActionResult UpdateDetails(int id, string columnName, string columnValue)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    if (columnName.ToLower() == "revno")
                    {
                        db.SP_COMMON_TABLE_UPDATE("WDN002", id, "LineId", "HasAttachment", "false", objClsLoginInfo.UserName);
                    }

                    db.SP_COMMON_TABLE_UPDATE("WDN002", id, "LineId", columnName, columnValue, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
                else
                {
                    db.SP_COMMON_TABLE_UPDATE("WDN002", id, "LineId", columnName, columnValue, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Copy WDN 
        [HttpPost, SessionExpireFilter]
        public ActionResult CopyWDNPartial(string location, int HeaderId, string Project)
        {
            ViewBag.Location = location;
            ViewBag.Project = Project;
            return PartialView("_CopyWDNPartial");
        }

        [HttpPost, SessionExpireFilter]
        public ActionResult CopyToDestination(int headerId, string destProject, string destLocation, string souProject, string souLocation)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            objResponseMsg.Key = false;

            WDN001 objSrcWDN001 = new WDN001();
            WDN001 objDestWDN001 = new WDN001();

            try
            {
                if (!string.IsNullOrEmpty(souProject) && !string.IsNullOrEmpty(souLocation))
                {
                    if (!db.WDN001.Any(x => x.Project.ToLower() == souProject.ToLower() && x.Location.ToLower() == souLocation.ToLower()))
                    {
                        objResponseMsg.Value = "Source Project Doesn't Exists..!";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }

                if (!string.IsNullOrEmpty(destProject) && !string.IsNullOrEmpty(destLocation))
                {
                    var lstProjects = CheckProjectExistWDNFor(destProject, 0, destLocation).ToList();
                    if (lstProjects != null && lstProjects.Count > 0)
                    {
                        objResponseMsg.Value = "Project Already Exist Or Maintain As Identical Project!";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }

                int IssueNo = 1;
                string IProject = string.Empty;
                string Departments = null;
                bool? CodeStamp = false;
                string CustTPIA = "";
                bool SafetyNuclear = false;
                if (headerId > 0)
                {
                    objSrcWDN001 = db.WDN001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    //observation 26734 - When DIN is copy it should being in Issue NO 1
                    //IssueNo = objSrcWDN001.IssueNo;
                    IProject = objSrcWDN001.IProject;
                    Departments = objSrcWDN001.Departments;
                    CodeStamp = objSrcWDN001.CodeStamp;
                    CustTPIA = objSrcWDN001.CustTPIA;
                    SafetyNuclear = objSrcWDN001.SafetyNuclear;
                }

                if (!string.IsNullOrEmpty(destProject))
                {
                    #region Copy Header
                    //objDestWDN001 = objSrcWDN001;
                    objDestWDN001.Location = destLocation;
                    objDestWDN001.IssueNo = IssueNo;
                    //objDestWDN001.IProject = IProject;
                    objDestWDN001.Departments = Departments;
                    objDestWDN001.CodeStamp = CodeStamp;
                    objDestWDN001.CustTPIA = CustTPIA;
                    objDestWDN001.SafetyNuclear = SafetyNuclear;
                    objDestWDN001.Project = destProject;
                    objDestWDN001.Status = clsImplementationEnum.WPSHeaderStatus.Draft.GetStringValue();
                    objDestWDN001.CreatedBy = objClsLoginInfo.UserName;
                    objDestWDN001.CreatedOn = DateTime.Now;
                    objDestWDN001.ReleaseBy = null;
                    objDestWDN001.ReleasedOn = null;
                    objDestWDN001.ModifiedBy = objClsLoginInfo.UserName;
                    objDestWDN001.ModifiedOn = DateTime.Now;

                    List<wdncopy> listwdncopy = new List<wdncopy>();
                    foreach (var item in objSrcWDN001.WDN002)
                    {
                        WDN002 objDescWDN002 = new WDN002();

                        objDescWDN002.DocumentDesc = item.DocumentDesc;
                        objDescWDN002.DocumentNo = item.DocumentNo;
                        objDescWDN002.RevNo = item.RevNo;
                        objDescWDN002.RefLineId = item.LineId;
                        objDescWDN002.HasAttachment = item.HasAttachment;

                        /*objDescWDN002.Department1 = item.Department1;
                        objDescWDN002.Department2 = item.Department2;
                        objDescWDN002.Department3 = item.Department3;
                        objDescWDN002.Department4 = item.Department4;
                        objDescWDN002.Department5 = item.Department5;
                        objDescWDN002.Department6 = item.Department6;
                        objDescWDN002.Department7 = item.Department7;
                        objDescWDN002.Department8 = item.Department8;
                        objDescWDN002.Department9 = item.Department9;
                        objDescWDN002.Department10 = item.Department10;

                        objDescWDN002.Department11 = item.Department11;
                        objDescWDN002.Department12 = item.Department12;
                        objDescWDN002.Department13 = item.Department13;
                        objDescWDN002.Department14 = item.Department14;
                        objDescWDN002.Department15 = item.Department15;*/

                        objDescWDN002.Location = objDestWDN001.Location;
                        objDescWDN002.Project = objDestWDN001.Project;
                        objDescWDN002.IssueStatus = objDestWDN001.Status;
                        objDescWDN002.IssueNo = objDestWDN001.IssueNo;
                        objDescWDN002.HeaderId = objDestWDN001.HeaderId;

                        objDescWDN002.CreatedBy = objClsLoginInfo.UserName;
                        objDescWDN002.CreatedOn = DateTime.Now;

                        objDestWDN001.WDN002.Add(objDescWDN002);
                        listwdncopy.Add(new wdncopy { sourcedata = item, destidata = objDescWDN002 });
                    }

                    db.WDN001.Add(objDestWDN001);
                    db.SaveChanges();

                    foreach (var item in listwdncopy)
                    {
                        #region copy documents
                        string srcfolderPath = "WDN002//" + item.sourcedata.LineId + "//R" + item.sourcedata.RevNo;
                        string destfolderPath = "WDN002//" + item.destidata.LineId + "//R" + item.sourcedata.RevNo;
                        //(new clsFileUpload()).CopyFolderContentsAsync(srcfolderPath, destfolderPath);
                        FileUploadController _objFUC = new FileUploadController();
                        _objFUC.CopyDataOnFCSServerAsync(srcfolderPath, destfolderPath, srcfolderPath, item.sourcedata.LineId, destfolderPath, item.destidata.LineId, CommonService.GetUseIPConfig, CommonService.objClsLoginInfo.UserName, 0);

                        #endregion
                    }

                    #endregion
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format("Data Copied to Project :{0} Successfully", destProject);
                    objResponseMsg.HeaderId = objDestWDN001.HeaderId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public JsonResult GetDepartments(string search, string location)
        {
            try
            {
                List<SelectItemList> lstline = db.FN_GET_LOCATIONWISE_DEPARTMENT(location, "").Select(x => new SelectItemList { id = x.t_dimx, text = x.t_dimx + "-" + x.t_desc, }).ToList();

                if (lstline != null)
                {
                    var item = lstline.Where(x => search == "" || x.text.Trim().ToLower().Contains(search.Trim().ToLower())).Select(x => new
                    {
                        Value = x.id,
                        Text = x.text
                    }).Distinct().Take(10).ToList();
                    return Json(item, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("", JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetDepartmentsForDropdown(string location = "")
        {
            try
            {
                var lstline = db.FN_GET_LOCATIONWISE_DEPARTMENT(location, "").Select(x => new { Value = x.t_dimx, Text = x.t_dimx + "-" + x.t_desc, }).Distinct().ToList();
                return Json(lstline, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateWDINDepartment(string project, string department)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<string> OldDepartmentList = new List<string>();
            List<string> NewDepartmentList = new List<string>();
            List<string> RemoveDepartmentList = new List<string>();
            var RELEASEDStatus = clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue();
            try
            {
                var objWDN001 = db.WDN001.Where(r => r.Project == project && r.Status == RELEASEDStatus).OrderByDescending(x => x.HeaderId).FirstOrDefault();
                if (objWDN001 != null)
                {
                    var OldDepartment = objWDN001.Departments;
                    if (!string.IsNullOrEmpty(department))
                    {
                        if (OldDepartment != null)
                        {
                            OldDepartmentList = OldDepartment.Split(',').ToList();
                        }
                        NewDepartmentList = department.Split(',').ToList();

                        RemoveDepartmentList = OldDepartmentList.Where(p => !NewDepartmentList.Any(p2 => p2 == p)).ToList();

                        if (RemoveDepartmentList.Count > 0)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "This department is belong to Released DIN, You cannot remove.";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public static string GenerateHidden(int rowId, string columnName, string columnValue = "", string className = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputValue = columnValue;

            htmlControl = "<input type='hidden' id='" + inputID + "' value='" + inputValue + "' name='" + inputID + "' colname='" + columnName + "' class='" + className + rowId + "'/>";

            return htmlControl;
        }

        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=\"" + onClickMethod + "\"" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='iconspace ' " + onClickEvent + " ><i class='" + className + "'></i></a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        #region Print
        [SessionExpireFilter]
        public ActionResult PrintWDIN(string HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            return View();
        }
        [HttpPost]
        public string GetWDINLinesHTMLNew(int headerid)
        {
            string rtn = string.Empty;
            int srno = 1;
            List<MaxIssueRevModel> listMaxIssueRevModel = new List<MaxIssueRevModel>();
            List<IssueNoModel> IssueNoList = null;
            string ProjectName = string.Empty;
            string CustomerName = string.Empty;
            int NoOfIssueDisplayOnPrint = 1000;
            int MinIssueNo = 0;

            string url = Request.Url.AbsoluteUri;           // http://localhost:1302/TESTERS/Default6.aspx
            string path = Request.Url.AbsolutePath;         // /TESTERS/Default6.aspx
            //string host = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host;                 // localhost
            var host = System.Configuration.ConfigurationManager.AppSettings["File_Upload_URL"];
            var objHeader = db.WDN001.FirstOrDefault(x => x.HeaderId == headerid);
            string project = db.WDN001.FirstOrDefault(x => x.HeaderId == headerid).Project;
            try
            {
                var RELEASEDStatus = clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue();
                var DraftStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue().ToLower();
                var ApprovedStatus = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                var projectDetails = db.COM001.Where(x => x.t_cprj == project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                string jobnos = project;
                #region Setdata
                var projects = db.WDN001.Where(x => x.Project == project && (x.IProject != null && x.IProject != string.Empty)).Select(x => x.IProject).Distinct().ToList();
                jobnos = project + (projects.Count > 0 ? (", " + string.Join(", ", projects)) : "");
                var lstProjects = jobnos.Split(',');
                lstProjects = lstProjects.Distinct().ToArray();
                jobnos = string.Join(",", lstProjects);
                List<string> lstProjectname = new List<string>();
                foreach (var item in lstProjects)
                {
                    lstProjectname.Add(db.COM001.Where(x => x.t_cprj == item.Trim()).Select(x => x.t_cprj + "-" + x.t_dsca).FirstOrDefault());
                }
                ProjectName = string.Join(", ", lstProjectname);

                var objWDN001 = db.WDN001.Where(i => i.Project == project && i.Location == objHeader.Location && i.Status == RELEASEDStatus).OrderByDescending(i => i.IssueNo).FirstOrDefault();

                CustomerName = (from cm004 in db.COM004
                                join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                                where cm005.t_sprj == project
                                select cm004.t_ofbp + " - " + cm006.t_nama).FirstOrDefault();

                //Get Current PDIN Issue No For Project
                //var CurrentPDINNo = db.WDN001.Where(i => i.Project == project && i.Location == objHeader.Location).Max(i => i.IssueNo);
                List<PDINDocumentRevNoEnt> listPDINDocumentRevNoEnt = new List<PDINDocumentRevNoEnt>();
                #endregion

                #region Department Logic (Set Rows)
                List<WDINDocumentListEnt> allDocumentList = new List<WDINDocumentListEnt>();
                List<WDINDocumentListEnt> documentList = new List<WDINDocumentListEnt>();
                //Get All Distinct Document list For Project
                allDocumentList = objWDN001.WDN002.OrderBy(i => i.LineId).Select(i => new WDINDocumentListEnt { DocumentNo = i.DocumentNo, Description = i.DocumentDesc, RefId = i.RefLineId }).ToList();

                //Get Current Issue No PDIN Department
                //var Department = objWDN001.Departments;
                string[] arrayDepartment = new string[0];
                if (!string.IsNullOrEmpty(objWDN001.Departments))
                {
                    arrayDepartment = objWDN001.Departments.Split(',');
                }

                //Department Code and Description
                int a = 0;
                List<WDINDept> DepartmentList = db.COM002.Where(i => arrayDepartment.Contains(i.t_dimx)).Select(i => new WDINDept { Code = i.t_dimx, Name = i.t_desc, Init = i.t_init }).Distinct().ToList();
                string[] arrDept = DepartmentList.Select(x => x.Name).ToArray();
                DepartmentList.ForEach(x => x.Id = (a = a + 1));

                int a1 = 0;
                allDocumentList.ForEach(x => x.Id = (a1 = a1 + 1));

                if (allDocumentList.Count < DepartmentList.Count)
                {
                    foreach (var obj in DepartmentList)
                    {
                        if (obj.Id == (allDocumentList.Count() + 1))
                        {
                            allDocumentList.Add(new WDINDocumentListEnt { Id = obj.Id, DepartmentDesc = obj.Name });
                        }
                    }
                }
                string dept = string.Empty;
                if (allDocumentList.Count >= DepartmentList.Count)
                {
                    foreach (var obj in allDocumentList)
                    {
                        int arr = Convert.ToInt32(obj.Id);
                        dept = ((arrDept.Count() - 1) >= arr) ? arrDept[arr] : string.Empty;
                        documentList.Add(new WDINDocumentListEnt { Id = obj.Id, DocumentNo = obj.DocumentNo, Description = obj.Description, RefId = obj.RefId, DepartmentDesc = dept });
                    }
                }
                dept = arrDept.Count() > 0 ? arrDept[0] : string.Empty;

                documentList.Insert(0, new WDINDocumentListEnt { Id = 0, DocumentNo = "SWP / TP / WPS", Description = "SWP / TP / WPS", RefId = 0, DepartmentDesc = dept });
                //Display Only Released Issue So we get only released Issue No
                #endregion

                IssueNoList = (from p1 in db.WDN001
                               join c3 in db.COM003 on p1.ModifiedBy equals c3.t_psno
                               where p1.Project == project && p1.Status == RELEASEDStatus && p1.Location == objHeader.Location //&& c3.t_actv == 1
                               orderby p1.IssueNo descending
                               select new IssueNoModel { IssueNo = p1.IssueNo, IssueBy = p1.ModifiedBy, IssueDate = p1.ModifiedOn, t_init = c3.t_init }
                                   ).Take(NoOfIssueDisplayOnPrint).OrderBy(i => i.IssueNo).ToList();
                if (IssueNoList.Count > 0)
                {
                    MinIssueNo = IssueNoList.Min(i => i.IssueNo);
                }

                //Get All IssueNo and related Details For other action.
                //We use this objects for diplay issueno, revno ,historyid
                var objWDN002 = (from p1 in db.WDN001
                                 join p2 in db.WDN002 on p1.HeaderId equals p2.HeaderId
                                 //      join c3 in db.COM003 on p1.ReleaseBy equals c3.t_psno
                                 where p1.Project.Trim() == project.Trim() && p1.Location == objHeader.Location && p1.Status == RELEASEDStatus //&& c3.t_actv == 1 
                                 select new { p1.Project, p1.IssueNo, p1.ReleaseBy, p1.ReleasedOn, p2.DocumentNo, p2.DocumentDesc, p2.RevNo, p1.Status, DocumentStatus = p2.IssueStatus, RefId = p2.RefLineId }
                                  ).ToList();

                //Get Current IssueNo Details like LineId,IsApplicable,Status,DocumentNo and RevNo
                var objWDN002CurentIssueDtl = (from p1 in db.WDN001
                                               join p2 in db.WDN002 on p1.HeaderId equals p2.HeaderId
                                               where p1.Project.Trim() == project.Trim() && p1.Location == objHeader.Location && p1.IssueNo == p2.IssueNo
                                               && p1.IssueNo == objWDN001.IssueNo
                                               select new { p2.LineId, p2.IssueStatus, p2.DocumentNo, p2.RevNo, p1.CodeStamp, p1.SafetyNuclear, p2.Notes }
                                  ).ToList();
                listPDINDocumentRevNoEnt = (from t in db.WDN002
                                            group t by new { t.Project, t.Location, t.DocumentNo, t.IssueStatus }
                                         into grp
                                            where grp.Key.Project == project && grp.Key.Location == objHeader.Location && grp.Key.IssueStatus == ApprovedStatus
                                            select new PDINDocumentRevNoEnt
                                            {
                                                Project = grp.Key.Project,
                                                DocumentNo = grp.Key.DocumentNo,
                                                RevNo = grp.Max(t => t.RevNo)
                                            }
                      ).ToList();
                //Get Max CreatedOn Date of Department For Document

                rtn += "<table id='tblWDINLinesDetails' class='table table-bordered'><thead>";

                #region Project & Customer
                //Create Issue By Row   
                int JDcolspan = 0;
                int JNcolspan = 0;
                int total = IssueNoList.Count() + 2;
                JDcolspan = (total > 2) ? total - 2 : 1;
                JNcolspan = (total > 2) ? total - JDcolspan : 1;
                rtn += "<tr>" //+"<th class='wdinheader' rowspan='' colspan='3' style='width:40% !important'><img alt='L&T Logo' src='" + WebsiteURL + "/Images/Logo.jpg' style='height:90px;' />WELDING DOCUMENT INDEX/ISSUE NOTE(WDIN)</th>"
                  + "<th class='wdinheader' colspan='3' style='padding: 0px !important;height: 90px;'>"
                   + "<table style='width: 100% !important;height: 100% !important;'> "
                   + "<tr>"
                   + "<th style='padding: 5px !important;text-align:center'><img alt='L&T Logo' src='" + WebsiteURL + "/Images/Logo.jpg' style='height:90px;' /> <br>" + (objHeader.Location == "HZW" ? "HZMC" : "") + "</th><th style='padding: 5px !important;text-align:center' ><span style='font-size: 14px;'>WELDING DOCUMENT INDEX/ISSUE NOTE(WDIN) </span></th></tr>"
                   + "</table>"
                   + "</th>"
                   + "<th class='wdinheader' colspan='" + (total + 1) + "' style='padding: 0px !important;height: 90px;'>"
                   + "<table style='width: 100% !important;height: 100% !important;'> "
                   + "<tr>"
                   + "<th class='center' style='border-right: 1px solid; border-bottom: 1px solid;padding: 5px !important;'><span style='font-weight: 180;' > JOB DESCRIPTION </span> <br/> " + ProjectName + "</th>"//"<th  class='center' style='padding: 5px !important;border-bottom: 1px solid;' colspan = '" + JNcolspan + "'><span style='font-weight: 180;'> JOB NO </span><br/> " + jobnos
                   + "</th></tr>"
                   + "<tr><th class='wdinheader center' style='padding: 5px !important;' colspan='" + (total + 1) + "'><span style='font-weight: 180;'> CUSTOMER</span><br/>" + CustomerName + "</th></tr>"
                   + "</table>"
                   + "</th>"
                   + "</tr>";
                //<th class='' colspan='" + JDcolspan + "'  style='width:40% !important'> <span style='font-weight: 180;'> JOB DESCRIPTION </span> <br/> " + ProjectName + "</th><th class='' colspan = " + JNcolspan + "><span style='font-weight: 180;'> JOB NO </span><br/> " + jobnos + "</th></tr>"
                rtn += "<tr><th class='width22percent' colspan='2' style='text-align:left' >NO. OF DOCUMENT SETS ISSUED</th><th class='' rowspan='2' style='width: 1% !important;border-bottom: 1px solid;'>SR. NO</th><th class='' style='width: 17% !important; ' rowspan='2' >DESCRIPTION</th><th class='' style='width: 17% !important; ' rowspan='2' >DOCUMENT NO</th>";
                rtn += "<th class='center' rowspan='2' colspan='" + IssueNoList.Count + "'>INDEX OF LATEST REVISION</th></tr>";
                rtn += "<tr><th class=''  style='text-align:left;border-bottom: 1px solid;' >TO HZMC DEPTS.</th><th class='' style='border-bottom: 1px solid;'>DOC</th></tr>";
                #endregion

                #region Header
                /*
             
                */
                #endregion
                rtn += "</thead>";

                //Start table tbody section
                rtn += "<tbody>";


                //loop all document list for generate document details row
                #region tbody
                foreach (var item in documentList)
                {
                    if (!string.IsNullOrWhiteSpace(item.DepartmentDesc) || !string.IsNullOrWhiteSpace(item.DocumentNo))
                    {
                        var objLastApprovedRevDtl = listPDINDocumentRevNoEnt.Where(i => i.DocumentNo == item.DocumentNo).FirstOrDefault();
                        //Get Current Issue Details For Document from objWDN002CurentIssueDtl list
                        var objLine = objWDN002CurentIssueDtl.Where(i => i.DocumentNo == item.DocumentNo).FirstOrDefault();

                        rtn += "<tr>";
                        rtn += "<td class='' style=\"text-align:left;\">" + item.DepartmentDesc + "</td>";
                        rtn += "<td class='' style=\"text-align:left;\">" + (!string.IsNullOrWhiteSpace(item.DepartmentDesc) ? "1" : string.Empty) + "</td>";
                        rtn += "<td class='srno' style=\"text-align:left;\">" + (!string.IsNullOrWhiteSpace(item.DocumentNo) ? srno.ToString() : string.Empty) + "</td>";
                        rtn += "<td class='' style=\"text-align:left;\">" + item.Description + "</td>";
                        rtn += "<td class='' style=\"text-align:left;\">" + item.DocumentNo + "</td>";
                        if (item.Id == 0)
                        {
                            rtn += "<td class='center' colspan=" + IssueNoList.Count + ">REFER SYSTEM FOR LATEST REVISION ONLINE</td>";
                        }
                        #region IssueNo Logic

                        //Loop through IssueNo list and create column for issueNo
                        if (item.Id != 0)
                        {
                            if (IssueNoList.Count == 0)
                            {
                                rtn += "<td class='center' ><b></b></td>";
                            }
                            var objLastIssueRev = objWDN002.Where(r => r.DocumentNo == item.DocumentNo && r.Project == project).OrderByDescending(i => i.IssueNo).FirstOrDefault();

                            foreach (var issue in IssueNoList)
                            {

                                //Here we maintain one list for RevNo & IssueNo for Document which is add in listMaxIssueRevModel
                                //We check whether there is any revNo find in listMaxIssueRevModel for document.
                                //This list is used for display arrow or revno.
                                var currentRevNo = listMaxIssueRevModel.Where(i => i.DocumentNo == item.DocumentNo).OrderByDescending(i => i.MaxRevNo).FirstOrDefault();

                                //Get All Document Details For Document and IssueNo
                                var obj2 = objWDN002.Where(r => r.IssueNo == issue.IssueNo && r.DocumentNo == item.DocumentNo && r.Project == project).FirstOrDefault();

                                //If There is not any record for document in listMaxIssueRevModel then display revisionNo
                                if (currentRevNo == null)
                                {
                                    //check obj2 is not null. if is null then display blank cell otherwise display revno base on other logic 
                                    if (obj2 != null)
                                    {
                                        //If obj2.Status is null then display blank cell other wise check other condition
                                        if (obj2.Status != null)
                                        {
                                            //If Status is released and ref history id is grether than 0 then add that revision in listMaxIssueRevModel and generate history link base on other condition
                                            if (obj2.Status.ToLower() == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue().ToLower() &&
                                                obj2.DocumentStatus == obj2.DocumentStatus)
                                            {
                                                listMaxIssueRevModel.Add(new MaxIssueRevModel { DocumentNo = obj2.DocumentNo, IssueNo = obj2.IssueNo, MaxRevNo = (obj2.RevNo == null ? 0 : obj2.RevNo) });
                                                //add item in listMaxIssueRevModel 
                                                if (objLastIssueRev.RevNo == obj2.RevNo)
                                                {
                                                    var folderPath = "WDN002//" + objLine.LineId + "//R" + obj2.RevNo;
                                                    //var filename = (new clsFileUpload()).GetDocuments(folderPath, true).Select(x => x.Name).FirstOrDefault();
                                                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                                    var filename = _objFUC.GetAllDocumentsByTableNameTableId(folderPath, objLine.LineId).FirstOrDefault();
                                                    //var sHREF = host + "/" + folderPath + "/" + filename.Document_name;
                                                    //rtn += "<td class='center'> <a target='_blank' href='" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? sHREF : "") + "' alt ='" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? filename.MainDocumentPath : "No Attachment available") + "'><b>R" + obj2.RevNo + "</b></a></td>";

                                                    if (filename != null)
                                                        rtn += "<td class='center'> <a onclick='fn_view_document_UV(\"" + filename.MainDocumentPath + "\");'><b>R" + obj2.RevNo + "</b></a></td>";
                                                    else
                                                        rtn += "<td class='center'> <a onclick='fn_view_document_UV('');'><b>R" + obj2.RevNo + "</b></a></td>";
                                                }
                                                else
                                                {
                                                    rtn += "<td class='center'><b>R" + obj2.RevNo + "</b></td>";
                                                }
                                            }
                                            else
                                            {
                                                rtn += "<td class='center' ><b></b></td>";
                                            }
                                        }
                                        else
                                        {
                                            rtn += "<td class='center' ><b></b></td>";
                                        }
                                    }
                                    else
                                    {
                                        rtn += "<td class='center' ><b></b></td>";
                                    }
                                }
                                //If we find any item in listMaxIssueRevModel then execute this else part
                                else
                                {
                                    //check obj2 is not null. if is null then display blank cell otherwise display revno base on other logic 
                                    if (obj2 != null)
                                    {
                                        //if listMaxIssueRevModel currentRevNo is same as loop issueno revision then diplay arrow other wise diplay RevisionNo
                                        if (currentRevNo.MaxRevNo == (obj2.RevNo == null ? 0 : obj2.RevNo))
                                        {
                                            //If RefHistoryId is grether than 0 then display arrow otherwise display blank cell
                                            if (obj2.RefId > 0)
                                            {
                                                //Display right arrow for continous revision means revision not change in next issue
                                                rtn += "<td class='center'> <img  src = '" + WebsiteURL + "/Images/right-arrow.png'/> </td>";
                                            }
                                            else
                                            {
                                                if (objLastIssueRev.RevNo == obj2.RevNo)
                                                {
                                                    var folderPath = "WDN002//" + objLine.LineId + "//R" + obj2.RevNo;
                                                    //var filename = (new clsFileUpload()).GetDocuments(folderPath, true).Select(x => x.Name).FirstOrDefault();

                                                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                                    var filename = _objFUC.GetAllDocumentsByTableNameTableId(folderPath, objLine.LineId).FirstOrDefault();

                                                    //var sHREF = host + "/" + folderPath + "/" + filename.Document_name;
                                                    //rtn += " < td class='center'> <a href = '" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? sHREF : "") + "' alt ='" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? filename.MainDocumentPath : "No Attachment available") + "'><b>R" + obj2.RevNo + "</b></a></td>";

                                                    if (filename != null)
                                                        rtn += "<td class='center'> <a onclick='fn_view_document_UV(\"" + filename.MainDocumentPath + "\");'><b>R" + obj2.RevNo + "</b></a></td>";
                                                    else
                                                        rtn += "<td class='center'> <a onclick='fn_view_document_UV('');'><b>R" + obj2.RevNo + "</b></a></td>";
                                                }
                                                else
                                                {
                                                    rtn += "<td class='center'><b>R" + obj2.RevNo + "</b></td>";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //If obj2.Status is null then display blank cell other wise check other condition
                                            if (obj2.Status != null)
                                            {
                                                //If Status is released then add that revision in listMaxIssueRevModel
                                                //add item in listMaxIssueRevModel 
                                                listMaxIssueRevModel.Add(new MaxIssueRevModel { DocumentNo = obj2.DocumentNo, IssueNo = obj2.IssueNo, MaxRevNo = (obj2.RevNo == null ? 0 : obj2.RevNo) });
                                                if (objLastIssueRev.RevNo == obj2.RevNo)
                                                {
                                                    //Display only Revision No
                                                    var folderPath = "WDN002//" + objLine.LineId + "//R" + obj2.RevNo;
                                                    //var filename = (new clsFileUpload()).GetDocuments(folderPath, true).Select(x => x.Name).FirstOrDefault();
                                                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                                    var filename = _objFUC.GetAllDocumentsByTableNameTableId(folderPath, objLine.LineId).FirstOrDefault();

                                                    //var sHREF = host + "/" + folderPath + "/" + filename.Document_name;
                                                    //rtn += "<td class='center'> <a target='_blank' href='" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? sHREF : "") + "' alt ='" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? filename.MainDocumentPath : "No Attachment available") + "' onclick='fn_view_document_UV(" + filename.MainDocumentPath + ");'><b>R" + obj2.RevNo + "</b></a></td>";

                                                    if (filename != null)
                                                        rtn += "<td class='center'> <a onclick='fn_view_document_UV(\"" + filename.MainDocumentPath + "\");'><b>R" + obj2.RevNo + "</b></a></td>";
                                                    else
                                                        rtn += "<td class='center'> <a onclick='fn_view_document_UV('');'><b>R" + obj2.RevNo + "</b></a></td>";
                                                }
                                                else
                                                {
                                                    rtn += "<td class='center'><b>R" + obj2.RevNo + "</b></td>";
                                                }
                                            }
                                            else
                                            {
                                                rtn += "<td class='center'><b>R" + obj2.RevNo + "</b></td>";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        rtn += "<td class='center' ><b></b></td>";
                                    }
                                }
                            }
                        }
                    }
                    #endregion
                    rtn += "</tr>";
                    srno++;
                }
                #endregion

                #region Set ASME

                if (objWDN002CurentIssueDtl.FirstOrDefault()?.CodeStamp == true || objWDN002CurentIssueDtl.FirstOrDefault()?.SafetyNuclear == true)
                {
                    rtn += "<tr><td class='' style=\"text-align:center;\"> &nbsp </td><td class='' style=\"text-align:center;\">&nbsp </td><td class='srno' style=\"text-align:center;\">&nbsp </td><td class='' style=\"text-align:center;\">&nbsp </td><td class='' style=\"text-align:center;\">&nbsp </td><td class='center' colspan=" + IssueNoList.Count + "> &nbsp</td></tr>";
                    rtn += "<tr><td class='' style=\"text-align:center;\" rowspan = 2 > <h4> " + (objWDN002CurentIssueDtl.Select(c => c.CodeStamp).FirstOrDefault() == true ? "ASME" : (objWDN002CurentIssueDtl.Select(c => c.SafetyNuclear).FirstOrDefault() == true ? "Safety Nuclear" : "")) + " </h4> </td><td class='' style=\"text-align:center;\">&nbsp </td><td class='srno' style=\"text-align:center;\">&nbsp </td><td class='' style=\"text-align:center;\">&nbsp </td><td class='' style=\"text-align:center;\">&nbsp </td><td class='center' colspan=" + IssueNoList.Count + "> &nbsp</td></tr>";
                    rtn += "<tr><td class='' style=\"text-align:center;\">&nbsp </td><td class='srno' style=\"text-align:center;\">&nbsp </td><td class='' style=\"text-align:center;\">&nbsp </td><td class='' style=\"text-align:center;\">&nbsp </td><td class='center' colspan=" + IssueNoList.Count + "> &nbsp</td></tr>";
                }
                #endregion

                #region Notes
                int n = 1;
                var lst = objWDN002CurentIssueDtl.Where(x => (x.Notes != null && x.Notes != "")).Select(x => new WDINDocumentListEnt { Notes = x.Notes }).ToList();
                lst.ForEach(x => x.Notes = !string.IsNullOrWhiteSpace(x.Notes) ? " <li>" + x.Notes + "</li>" : x.Notes);

                rtn += "<tr><th class='wdinheader' rowspan='3' colspan='3'  style='text-align:left;border-top: 1px solid;' > NOTES : <br/>"
                  + "<ol><li> <img  src = '" + WebsiteURL + "/Images/right-arrow.png'/> &nbsp &nbsp REVISIONS CARRIED </li>"
                  + string.Join("", lst.Select(x => x.Notes).ToList())
                  + "</ol></th>";
                #endregion

                #region IssueBy

                rtn += "<th class='wdinheader' colspan=2 > ISSUED NO. </th>";
                foreach (var issue in IssueNoList)
                {
                    rtn += "<th class='wdinheader' style=\"text-align:center;\">" + issue.IssueNo.ToString() + "</th>";
                }
                if (IssueNoList.Count == 0)
                {
                    rtn += "<th class='wdinheader' style=\"text-align:center;\"></th>";
                }
                rtn += "</tr>";
                //End Issue By Row
                #endregion

                #region IssueDate
                rtn += "<tr><th class='wdinheader'  colspan=2>ISSUED BY </th>";
                //Loop through IssueNoList and create table column header to display Issue Date
                foreach (var issue in IssueNoList)
                {
                    rtn += "<th class='wdinheader' style=\"text-align:center;\">" + issue.t_init + "</th>";
                }
                if (IssueNoList.Count == 0)
                {
                    rtn += "<th class='wdinheader' style=\"text-align:center;\"></th>";
                }
                rtn += "</tr>";
                #endregion

                #region Released DIN
                rtn += "<tr><th class='wdinheader'  colspan=2> DATE </th>";
                //Loop through IssueNoList and create table column header to display Issue No
                foreach (var issue in IssueNoList)
                {
                    rtn += "<th class='wdinheader' colspan='1' style=\"text-align:center;\">" + (issue.IssueDate.HasValue ? issue.IssueDate.Value.ToString("dd/MM/yy") : string.Empty) + "</th>";
                }
                if (IssueNoList.Count == 0)
                {
                    rtn += "<th class='wdinheader' style=\"text-align:center;\"></th>";
                }
                rtn += "</tr>";
                #endregion

                rtn += "</tbody></table>";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rtn;
        }

        #endregion

        #region Display WDIN
        [SessionExpireFilter]
        public ActionResult DisplayWDIN(string HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            ViewBag.Title = "Display DIN";
            return View();
        }
        [HttpPost]
        public string GetWDINLinesHTML(int headerid, bool? ForPrint)
        {
            string rtn = string.Empty;
            int srno = 1;
            List<MaxIssueRevModel> listMaxIssueRevModel = new List<MaxIssueRevModel>();
            List<IssueNoModel> IssueNoList = null;
            string ProjectName = string.Empty;
            string CustomerName = string.Empty;
            int NoOfIssueDisplayOnPrint = 1000;
            int MinIssueNo = 0;

            string url = Request.Url.AbsoluteUri;           // http://localhost:1302/TESTERS/Default6.aspx
            string path = Request.Url.AbsolutePath;         // /TESTERS/Default6.aspx
            //string host = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host;                 // localhost
            var host = System.Configuration.ConfigurationManager.AppSettings["File_Upload_URL"];
            var objHeader = db.WDN001.FirstOrDefault(x => x.HeaderId == headerid);
            string project = db.WDN001.FirstOrDefault(x => x.HeaderId == headerid).Project;
            var FixColumnNo = ForPrint.HasValue && ForPrint.Value ? 6 : 5;
            NoOfIssueDisplayOnPrint = ForPrint.HasValue && ForPrint.Value ? 10 : NoOfIssueDisplayOnPrint;
            IssueNoModel CarryForwardIssueNo = null;
            try
            {
                var RELEASEDStatus = clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue();
                var DraftStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue().ToLower();
                var ApprovedStatus = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                var projectDetails = db.COM001.Where(x => x.t_cprj == project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                string jobnos = project;

                #region Setdata
                var projects = db.WDN001.Where(x => x.Project == project && (x.IProject != null && x.IProject != string.Empty)).Select(x => x.IProject).Distinct().ToList();
                jobnos = project + (projects.Count > 0 ? (", " + string.Join(", ", projects)) : "");
                var lstProjects = jobnos.Split(',');
                lstProjects = lstProjects.Distinct().ToArray();
                jobnos = string.Join(",", lstProjects);
                List<string> lstProjectname = new List<string>();
                foreach (var item in lstProjects)
                {
                    lstProjectname.Add(db.COM001.Where(x => x.t_cprj == item.Trim()).Select(x => x.t_cprj + "-" + x.t_dsca).FirstOrDefault());
                }
                ProjectName = string.Join(", ", lstProjectname);

                var objWDN001 = db.WDN001.Where(i => i.Project == project && i.Location == objHeader.Location && i.Status == RELEASEDStatus).OrderByDescending(i => i.IssueNo).FirstOrDefault();

                CustomerName = (from cm004 in db.COM004
                                join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                                join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                                where cm005.t_sprj == project
                                select cm004.t_ofbp + " - " + cm006.t_nama).FirstOrDefault();

                //Get Current PDIN Issue No For Project
                //var CurrentPDINNo = db.WDN001.Where(i => i.Project == project && i.Location == objHeader.Location).Max(i => i.IssueNo);
                List<PDINDocumentRevNoEnt> listPDINDocumentRevNoEnt = new List<PDINDocumentRevNoEnt>();
                #endregion

                #region Department Logic (Set Rows)
                List<WDINDocumentListEnt> allDocumentList = new List<WDINDocumentListEnt>();
                List<WDINDocumentListEnt> documentList = new List<WDINDocumentListEnt>();
                //Get All Distinct Document list For Project
                allDocumentList = objWDN001.WDN002.OrderBy(i => i.LineId).Select(i => new WDINDocumentListEnt { DocumentNo = i.DocumentNo, Description = i.DocumentDesc, RefId = i.RefLineId }).ToList();

                //Get Current Issue No PDIN Department
                //var Department = objWDN001.Departments;
                string[] arrayDepartment = new string[0];
                if (!string.IsNullOrEmpty(objWDN001.Departments))
                {
                    arrayDepartment = objWDN001.Departments.Split(',');
                }

                //Department Code and Description
                int a = 0;
                List<WDINDept> DepartmentList = db.COM002.Where(i => arrayDepartment.Contains(i.t_dimx)).Select(i => new WDINDept { Code = i.t_dimx, Name = i.t_desc, Init = i.t_init }).Distinct().ToList();
                string[] arrDept = DepartmentList.Select(x => x.Name).ToArray();
                DepartmentList.ForEach(x => x.Id = (a = a + 1));

                int a1 = 0;
                allDocumentList.ForEach(x => x.Id = (a1 = a1 + 1));

                if (allDocumentList.Count < DepartmentList.Count)
                {
                    foreach (var obj in DepartmentList)
                    {
                        if (obj.Id == (allDocumentList.Count() + 1))
                        {
                            allDocumentList.Add(new WDINDocumentListEnt { Id = obj.Id, DepartmentDesc = obj.Name });
                        }
                    }
                }
                string dept = string.Empty;
                if (allDocumentList.Count >= DepartmentList.Count)
                {
                    foreach (var obj in allDocumentList)
                    {
                        int arr = Convert.ToInt32(obj.Id);
                        dept = ((arrDept.Count() - 1) >= arr) ? arrDept[arr] : string.Empty;
                        documentList.Add(new WDINDocumentListEnt { Id = obj.Id, DocumentNo = obj.DocumentNo, Description = obj.Description, RefId = obj.RefId, DepartmentDesc = dept });
                    }
                }
                dept = arrDept.Count() > 0 ? arrDept[0] : string.Empty;

                documentList.Insert(0, new WDINDocumentListEnt { Id = 0, DocumentNo = "SWP / TP / WPS", Description = "SWP / TP / WPS", RefId = 0, DepartmentDesc = dept });
                //Display Only Released Issue So we get only released Issue No
                #endregion

                IssueNoList = (from p1 in db.WDN001
                               join c3 in db.COM003 on p1.ModifiedBy equals c3.t_psno
                               where p1.Project == project && p1.Status == RELEASEDStatus && c3.t_actv == 1 && p1.Location == objHeader.Location
                               orderby p1.IssueNo descending
                               select new IssueNoModel { IssueNo = p1.IssueNo, IssueBy = p1.ModifiedBy, IssueDate = p1.ModifiedOn, t_init = c3.t_init }
                                   ).Take(NoOfIssueDisplayOnPrint).OrderBy(i => i.IssueNo).ToList();
                if (IssueNoList.Count > 0)
                {
                    MinIssueNo = IssueNoList.Min(i => i.IssueNo);
                }

                //Get All IssueNo and related Details For other action.
                //We use this objects for diplay issueno, revno ,historyid
                IssueNoList = (from p1 in db.WDN001
                               join c3 in db.COM003 on p1.ModifiedBy equals c3.t_psno
                               where p1.Project == project && p1.Status == RELEASEDStatus && p1.Location == objHeader.Location //&& c3.t_actv == 1
                               orderby p1.IssueNo descending
                               select new IssueNoModel { IssueNo = p1.IssueNo, IssueBy = p1.ModifiedBy, IssueDate = p1.ModifiedOn, t_init = c3.t_init }
                                   ).Take(NoOfIssueDisplayOnPrint).OrderBy(i => i.IssueNo).ToList();
                if (IssueNoList.Count > 0)
                {
                    MinIssueNo = IssueNoList.Min(i => i.IssueNo);
                }

                //Get All IssueNo and related Details For other action.
                //We use this objects for diplay issueno, revno ,historyid
                var objWDN002 = (from p1 in db.WDN001
                                 join p2 in db.WDN002 on p1.HeaderId equals p2.HeaderId
                                 //      join c3 in db.COM003 on p1.ReleaseBy equals c3.t_psno
                                 where p1.Project.Trim() == project.Trim() && p1.Location == objHeader.Location && p1.Status == RELEASEDStatus //&& c3.t_actv == 1 
                                 select new { p1.Project, p1.IssueNo, p1.ReleaseBy, p1.ReleasedOn, p2.DocumentNo, p2.DocumentDesc, p2.RevNo, p1.Status, DocumentStatus = p2.IssueStatus, RefId = p2.RefLineId }
                                  ).ToList();

                //Get Current IssueNo Details like LineId,IsApplicable,Status,DocumentNo and RevNo
                var objWDN002CurentIssueDtl = (from p1 in db.WDN001
                                               join p2 in db.WDN002 on p1.HeaderId equals p2.HeaderId
                                               where p1.Project.Trim() == project.Trim() && p1.Location == objHeader.Location && p1.IssueNo == p2.IssueNo
                                               && p1.IssueNo == objWDN001.IssueNo
                                               select new { p2.LineId, p2.IssueStatus, p2.DocumentNo, p2.RevNo, p1.CodeStamp, p1.SafetyNuclear, p2.Notes }
                                  ).ToList();
                listPDINDocumentRevNoEnt = (from t in db.WDN002
                                            group t by new { t.Project, t.Location, t.DocumentNo, t.IssueStatus }
                                         into grp
                                            where grp.Key.Project == project && grp.Key.Location == objHeader.Location && grp.Key.IssueStatus == ApprovedStatus
                                            select new PDINDocumentRevNoEnt
                                            {
                                                Project = grp.Key.Project,
                                                DocumentNo = grp.Key.DocumentNo,
                                                RevNo = grp.Max(t => t.RevNo)
                                            }
                      ).ToList();
                //Get Max CreatedOn Date of Department For Document

                int total = IssueNoList.Count();
                var ProjectNameColSpan = 1;
                var ProjectNoColSpan = 1;
                if (total > 0)
                {
                    if (total % 2 == 0)
                    {
                        ProjectNameColSpan = total / 2;
                        ProjectNoColSpan = total / 2;
                    }
                    else
                    {
                        ProjectNameColSpan = (total / 2) + 1;
                        ProjectNoColSpan = (total / 2) - 1;
                    }
                    //ProjectNameColSpan = ProjectNameColSpan + 1;
                }
                rtn += "<table id='tblWDINLinesDetails' class='table table-bordered'>";

                #region Header
                rtn += "<thead>";
                rtn += "<tr>";
                rtn += "<th colspan=\"" + FixColumnNo + "\" rowspan=\"2\"><div style=\"width:30%;display:inline-block;\"><img alt='L&T Logo' src='" + WebsiteURL + "/Images/Logo.jpg' style='height:90px;' /></div><div style=\"width:70%;display:inline-block;\"><span style='font-size: 14px;'>WELDING DOCUMENT INDEX/ISSUE NOTE(WDIN) </span></div></th>";
                rtn += "<th style=\"text-align:center;\" colspan=\"" + (total) + "\"><span style='font-weight: 180;' > JOB DESCRIPTION </span> <br/> " + ProjectName + "</th>";
                //rtn += "<th style=\"text-align:center;\" colspan=\"" + ProjectNoColSpan + "\"><span style='font-weight: 180;' > JOB NO </span> <br/> " + jobnos + "</th>";
                rtn += "</tr>";

                rtn += "<tr>";
                rtn += "<th style=\"text-align:center;\" colspan=\"" + (total + 1) + "\"><span style='font-weight: 180;'> CUSTOMER</span><br/>" + CustomerName + "</th>";
                rtn += "</tr>";

                rtn += "<tr>";
                rtn += "<th colspan=\"" + FixColumnNo + "\">NO. OF DOCUMENT SETS ISSUED</th><th colspan=\"" + total + "\"></th>";
                rtn += "</tr>";

                rtn += "<tr>";

                string str = "";
                str = Convert.ToString(objHeader.Location);
                if (str == "HZW")
                {
                    str = "HZMC";
                }

                if (str == "PEW")
                {
                    str = "PHEW";
                }

                if (str == "RNW")
                {
                    str = "VHEW";
                }


                //rtn += "<th>TO HZMC DEPTS.</th><th>DOC</th><th>SR. NO</th><th>DESCRIPTION</th><th><nobr>Document No</nobr></th>";
                rtn += "<th>TO " + str + " DEPTS.</th><th>DOC</th><th>SR. NO</th><th>DESCRIPTION</th><th><nobr>Document No</nobr></th>";

                if (ForPrint.HasValue && ForPrint.Value)
                {
                    rtn += "<th>C.F.</th>";//C.F
                }
                rtn += "<th style=\"text-align:center;\" colspan=\"" + (total) + "\">INDEX OF LATEST REVISION</th>";
                rtn += "</tr>";

                rtn += "</thead>";
                #endregion

                #region Body
                rtn += "<tbody>";
                foreach (var item in documentList)
                {
                    if (!string.IsNullOrWhiteSpace(item.DepartmentDesc) || !string.IsNullOrWhiteSpace(item.DocumentNo))
                    {
                        var objLastApprovedRevDtl = listPDINDocumentRevNoEnt.Where(i => i.DocumentNo == item.DocumentNo).FirstOrDefault();
                        //Get Current Issue Details For Document from objWDN002CurentIssueDtl list
                        var objLine = objWDN002CurentIssueDtl.Where(i => i.DocumentNo == item.DocumentNo).FirstOrDefault();

                        rtn += "<tr>";
                        rtn += "<td class='dept' style=\"text-align:left;\"><nobr>" + item.DepartmentDesc + "</nobr></td>";
                        rtn += "<td class='deptdesc' style=\"text-align:left;\">" + (!string.IsNullOrWhiteSpace(item.DepartmentDesc) ? "1" : string.Empty) + "</td>";
                        rtn += "<td class='srno' style=\"text-align:left;\">" + (!string.IsNullOrWhiteSpace(item.DocumentNo) ? srno.ToString() : string.Empty) + "</td>";
                        rtn += "<td class='descriptions' style=\"text-align:left;\">" + item.Description + "</td>";
                        rtn += "<td class='documentno' style=\"text-align:left;\">" + item.DocumentNo + "</td>";

                        if (item.Id == 0)
                        {
                            if (ForPrint.HasValue && ForPrint.Value)
                            {
                                rtn += "<td></td>";//C.F
                            }
                            rtn += "<td class='center' colspan=" + total + ">REFER SYSTEM FOR LATEST REVISION ONLINE</td>";
                        }
                        else
                        {
                            var objLastIssueRev = objWDN002.Where(r => r.DocumentNo == item.DocumentNo && r.Project == project).OrderByDescending(i => i.IssueNo).FirstOrDefault();

                            if (ForPrint.HasValue && ForPrint.Value)
                            {
                                CarryForwardIssueNo = (from p1 in db.WDN001
                                                       join p2 in db.WDN002 on p1.HeaderId equals p2.HeaderId
                                                       join c3 in db.COM003 on p1.ModifiedBy equals c3.t_psno
                                                       where p1.Project == project
                                                            && p2.IssueStatus.ToLower() == RELEASEDStatus
                                                            && p2.IssueNo < MinIssueNo
                                                            && p2.DocumentNo == item.DocumentNo && c3.t_actv == 1
                                                       orderby p1.IssueNo descending
                                                       select new IssueNoModel { IssueNo = p1.IssueNo, IssueBy = p1.ModifiedBy, IssueDate = p1.ModifiedOn, t_init = c3.t_init }
                                      ).OrderByDescending(i => i.IssueNo).Take(1).FirstOrDefault();
                                if (CarryForwardIssueNo == null)
                                {
                                    rtn += "<td class=''></td>";
                                }
                                else
                                {
                                    var carryForward = objWDN002.Where(i => i.IssueNo == CarryForwardIssueNo.IssueNo && i.DocumentNo == item.DocumentNo).FirstOrDefault();
                                    if (carryForward != null && carryForward.RevNo != null)
                                    {
                                        ////add item in listMaxIssueRevModel 
                                        //listMaxIssueRevModel.Add(new MaxIssueRevModel { DocumentNo = carryForward.DocumentNo, IssueNo = carryForward.IssueNo, MaxRevNo = (carryForward.RevNo == null ? 0 : carryForward.RevNo) });
                                        listMaxIssueRevModel.Add(new MaxIssueRevModel { DocumentNo = carryForward.DocumentNo, IssueNo = carryForward.IssueNo, MaxRevNo = (carryForward.RevNo == null ? 0 : carryForward.RevNo) });
                                        if (objLastIssueRev.RevNo == carryForward.RevNo)
                                        {
                                            var folderPath = "WDN002//" + objLine.LineId + "//R" + carryForward.RevNo;
                                            //var filename = (new clsFileUpload()).GetDocuments(folderPath, true).Select(x => x.Name).FirstOrDefault();
                                            Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                            var filename = _objFUC.GetAllDocumentsByTableNameTableId(folderPath, objLine.LineId).FirstOrDefault();

                                            //var sHREF = host + "/" + folderPath + "/" + filename.Document_name;
                                            //rtn += "<td class='center'> <a target='_blank' href='" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? sHREF : "") + "' alt ='" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? filename.MainDocumentPath : "No Attachment available") + "'><b>R" + carryForward.RevNo + "</b></a></td>";

                                            if (filename != null)
                                                rtn += "<td class='center'> <a onclick='fn_view_document_UV(\"" + filename.MainDocumentPath + "\");'><b>R" + carryForward.RevNo + "</b></a></td>";
                                            else
                                                rtn += "<td class='center'> <a onclick='fn_view_document_UV('');'><b>R" + carryForward.RevNo + "</b></a></td>";
                                        }
                                        else
                                        {
                                            rtn += "<td class='center'>R" + carryForward.RevNo + "</td>";
                                        }
                                    }
                                    else
                                    {
                                        rtn += "<td class=''></td>";
                                    }
                                }

                                //rtn += "<td></td>";//C.F
                            }
                            if (total == 0)
                            {
                                rtn += "<td class='center' ><b></b></td>";
                            }

                            foreach (var issue in IssueNoList)
                            {
                                //Here we maintain one list for RevNo & IssueNo for Document which is add in listMaxIssueRevModel
                                //We check whether there is any revNo find in listMaxIssueRevModel for document.
                                //This list is used for display arrow or revno.
                                var currentRevNo = listMaxIssueRevModel.Where(i => i.DocumentNo == item.DocumentNo).OrderByDescending(i => i.MaxRevNo).FirstOrDefault();

                                //Get All Document Details For Document and IssueNo
                                var obj2 = objWDN002.Where(r => r.IssueNo == issue.IssueNo && r.DocumentNo == item.DocumentNo && r.Project == project).FirstOrDefault();

                                //If There is not any record for document in listMaxIssueRevModel then display revisionNo
                                if (currentRevNo == null)
                                {
                                    //check obj2 is not null. if is null then display blank cell otherwise display revno base on other logic 
                                    if (obj2 != null)
                                    {
                                        //If obj2.Status is null then display blank cell other wise check other condition
                                        if (obj2.Status != null)
                                        {
                                            //If Status is released and ref history id is grether than 0 then add that revision in listMaxIssueRevModel and generate history link base on other condition
                                            if (obj2.Status.ToLower() == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue().ToLower() &&
                                                obj2.DocumentStatus == obj2.DocumentStatus)
                                            {
                                                listMaxIssueRevModel.Add(new MaxIssueRevModel { DocumentNo = obj2.DocumentNo, IssueNo = obj2.IssueNo, MaxRevNo = (obj2.RevNo == null ? 0 : obj2.RevNo) });
                                                //add item in listMaxIssueRevModel 
                                                if (objLastIssueRev.RevNo == obj2.RevNo)
                                                {
                                                    var folderPath = "WDN002//" + objLine.LineId + "//R" + obj2.RevNo;
                                                    //var filename = (new clsFileUpload()).GetDocuments(folderPath, true).Select(x => x.Name).FirstOrDefault();
                                                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                                    var filename = _objFUC.GetAllDocumentsByTableNameTableId(folderPath, objLine.LineId).FirstOrDefault();

                                                    //var sHREF = host + "/" + folderPath + "/" + filename.Document_name;
                                                    //rtn += "<td class='center'> <a target='_blank' href='" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? sHREF : "") + "' alt ='" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? filename.MainDocumentPath : "No Attachment available") + "'><b>R" + obj2.RevNo + "</b></a></td>";

                                                    if (filename != null)
                                                        rtn += "<td class='center'> <a onclick='fn_view_document_UV(\"" + filename.MainDocumentPath + "\");'><b>R" + obj2.RevNo + "</b></a></td>";
                                                    else
                                                        rtn += "<td class='center'> <a onclick='fn_view_document_UV('');'><b>R" + obj2.RevNo + "</b></a></td>";
                                                }
                                                else
                                                {
                                                    rtn += "<td class='center'><b>R" + obj2.RevNo + "</b></td>";
                                                }
                                            }
                                            else
                                            {
                                                rtn += "<td class='center' ><b></b></td>";
                                            }
                                        }
                                        else
                                        {
                                            rtn += "<td class='center' ><b></b></td>";
                                        }
                                    }
                                    else
                                    {
                                        rtn += "<td class='center' ><b></b></td>";
                                    }
                                }
                                //If we find any item in listMaxIssueRevModel then execute this else part
                                else
                                {
                                    //check obj2 is not null. if is null then display blank cell otherwise display revno base on other logic 
                                    if (obj2 != null)
                                    {
                                        //if listMaxIssueRevModel currentRevNo is same as loop issueno revision then diplay arrow other wise diplay RevisionNo
                                        if (currentRevNo.MaxRevNo == (obj2.RevNo == null ? 0 : obj2.RevNo))
                                        {
                                            //If RefHistoryId is grether than 0 then display arrow otherwise display blank cell
                                            if (obj2.RefId > 0)
                                            {
                                                //Display right arrow for continous revision means revision not change in next issue
                                                rtn += "<td class='center'> <img  src = '" + WebsiteURL + "/Images/right-arrow.png'/>  </td>";
                                            }
                                            else
                                            {
                                                if (objLastIssueRev.RevNo == obj2.RevNo)
                                                {
                                                    var folderPath = "WDN002//" + objLine.LineId + "//R" + obj2.RevNo;
                                                    //var filename = (new clsFileUpload()).GetDocuments(folderPath, true).Select(x => x.Name).FirstOrDefault();
                                                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                                    var filename = _objFUC.GetAllDocumentsByTableNameTableId(folderPath, objLine.LineId).FirstOrDefault();

                                                    //var sHREF = host + "/" + folderPath + "/" + filename.Document_name;
                                                    //rtn += " < td class='center'> <a href = '" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? sHREF : "") + "' alt ='" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? filename.MainDocumentPath : "No Attachment available") + "'><b>R" + obj2.RevNo + "</b></a></td>";

                                                    if (filename != null)
                                                        rtn += "<td class='center'> <a onclick='fn_view_document_UV(\"" + filename.MainDocumentPath + "\");'><b>R" + obj2.RevNo + "</b></a></td>";
                                                    else
                                                        rtn += "<td class='center'> <a onclick='fn_view_document_UV('');'><b>R" + obj2.RevNo + "</b></a></td>";
                                                }
                                                else
                                                {
                                                    rtn += "<td class='center'><b>R" + obj2.RevNo + "</b></td>";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //If obj2.Status is null then display blank cell other wise check other condition
                                            if (obj2.Status != null)
                                            {
                                                //If Status is released then add that revision in listMaxIssueRevModel
                                                //add item in listMaxIssueRevModel 
                                                listMaxIssueRevModel.Add(new MaxIssueRevModel { DocumentNo = obj2.DocumentNo, IssueNo = obj2.IssueNo, MaxRevNo = (obj2.RevNo == null ? 0 : obj2.RevNo) });
                                                if (objLastIssueRev.RevNo == obj2.RevNo)
                                                {
                                                    //Display only Revision No
                                                    var folderPath = "WDN002//" + objLine.LineId + "//R" + obj2.RevNo;
                                                    //var filename = (new clsFileUpload()).GetDocuments(folderPath, true).Select(x => x.Name).FirstOrDefault();
                                                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                                                    var filename = _objFUC.GetAllDocumentsByTableNameTableId(folderPath, objLine.LineId).FirstOrDefault();

                                                    //var sHREF = host + "/" + folderPath + "/" + filename.Document_name;
                                                    //rtn += "<td class='center'> <a target='_blank' href='" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? sHREF : "") + "' alt ='" + (!string.IsNullOrWhiteSpace(filename.MainDocumentPath) ? filename.MainDocumentPath : "No Attachment available") + "'><b>R" + obj2.RevNo + "</b></a></td>";

                                                    if (filename != null)
                                                        rtn += "<td class='center'> <a onclick='fn_view_document_UV(\"" + filename.MainDocumentPath + "\");'><b>R" + obj2.RevNo + "</b></a></td>";
                                                    else
                                                        rtn += "<td class='center'> <a onclick='fn_view_document_UV('');'><b>R" + obj2.RevNo + "</b></a></td>";
                                                }
                                                else
                                                {
                                                    rtn += "<td class='center'><b>R" + obj2.RevNo + "</b></td>";
                                                }
                                            }
                                            else
                                            {
                                                rtn += "<td class='center'><b>R" + obj2.RevNo + "</b></td>";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        rtn += "<td class='center' ><b></b></td>";
                                    }
                                }
                            }
                        }
                        rtn += "</tr>";
                    }
                    srno++;
                }

                #endregion

                #region Set ASME

                if (objWDN002CurentIssueDtl.FirstOrDefault()?.CodeStamp == true || objWDN002CurentIssueDtl.FirstOrDefault()?.SafetyNuclear == true)
                {
                    rtn += "<tr><td> &nbsp </td><td>&nbsp </td><td>&nbsp </td><td>&nbsp </td><td>&nbsp </td>";
                    if (ForPrint.HasValue && ForPrint.Value)
                    {
                        rtn += "<td></td>";//C.F
                    }
                    rtn += "<td class='center' colspan=" + IssueNoList.Count + "> &nbsp</td></tr>";
                    rtn += "<tr><td class='' style=\"text-align:center;\"> <h4> " + (objWDN002CurentIssueDtl.Select(c => c.CodeStamp).FirstOrDefault() == true ? "ASME" : (objWDN002CurentIssueDtl.Select(c => c.SafetyNuclear).FirstOrDefault() == true ? "Safety Nuclear" : "")) + " </h4> </td><td class='' style=\"text-align:center;\">&nbsp </td><td class='srno' style=\"text-align:center;\">&nbsp </td><td class='' style=\"text-align:center;\">&nbsp </td><td class='' style=\"text-align:center;\">&nbsp </td>";
                    if (ForPrint.HasValue && ForPrint.Value)
                    {
                        rtn += "<td></td>";//C.F
                    }
                    rtn += "<td class='center' colspan=" + IssueNoList.Count + "> &nbsp</td></tr>";
                    rtn += "<tr><td> &nbsp </td><td>&nbsp </td><td>&nbsp </td><td>&nbsp </td><td>&nbsp </td>";
                    if (ForPrint.HasValue && ForPrint.Value)
                    {
                        rtn += "<td></td>";//C.F
                    }
                    rtn += "<td class='center' colspan=" + IssueNoList.Count + "> &nbsp</td></tr>";
                }
                #endregion

                rtn += "<tr style=\"font-weight:bold\"><td style=\"border-left: none;border-right: none;\"></td><td style=\"border-left: none;border-right: none;\"></td><td style=\"border-left: none;border-right: none;\"></td><td></td><td>ISSUED NO.</td>";
                if (ForPrint.HasValue && ForPrint.Value)
                {
                    rtn += "<td></td>";//C.F
                }
                foreach (var issue in IssueNoList)
                {
                    rtn += "<td>" + issue.IssueNo.ToString() + "</td>";
                }
                rtn += "</tr>";

                rtn += "<tr style=\"font-weight:bold\"><td style=\"border-left: none;border-right: none;\"></td><td style=\"border-left: none;border-right: none;\"></td><td style=\"border-left: none;border-right: none;\"></td><td></td><td>ISSUED BY</td>";
                if (ForPrint.HasValue && ForPrint.Value)
                {
                    rtn += "<td></td>";//C.F
                }
                foreach (var issue in IssueNoList)
                {
                    rtn += "<td>" + issue.t_init + "</td>";
                }
                rtn += "</tr>";

                rtn += "<tr style=\"font-weight:bold\"><td style=\"border-left: none;border-right: none;\"></td><td style=\"border-left: none;border-right: none;\"></td><td style=\"border-left: none;border-right: none;\"></td><td></td><td>DATE</td>";
                if (ForPrint.HasValue && ForPrint.Value)
                {
                    rtn += "<td></td>";//C.F
                }
                foreach (var issue in IssueNoList)
                {
                    rtn += "<td>" + (issue.IssueDate.HasValue ? issue.IssueDate.Value.ToString("dd/MM/yy") : string.Empty) + "</td>";
                }
                rtn += "</tr>";

                var lst = objWDN002CurentIssueDtl.Where(x => (x.Notes != null && x.Notes != "")).Select(x => new WDINDocumentListEnt { Notes = x.Notes }).ToList();
                lst.ForEach(x => x.Notes = !string.IsNullOrWhiteSpace(x.Notes) ? " <li>" + x.Notes + "</li>" : x.Notes);
                rtn += "<tr style=\"font-weight:bold\"><td class='wdinheader' colspan=\"" + FixColumnNo + "\"  style='text-align:left;' > NOTES : <br/>"
                 + "<ol><li> <img  src = '" + WebsiteURL + "/Images/right-arrow.png'/> &nbsp &nbsp REVISIONS CARRIED </li>"
                 + string.Join("", lst.Select(x => x.Notes).ToList())
                 + "</ol></td>";
                //foreach (var issue in IssueNoList)
                //{
                //    rtn += "<td></td>";
                //}
                rtn += "<td colspan=\"" + IssueNoList.Count + "\"></td>";
                rtn += "</tr>";

                rtn += "</tbody>";

                #region Footer
                //#region Notes
                //int n = 1;
                //var lst = objWDN002CurentIssueDtl.Where(x => (x.Notes != null && x.Notes != "")).Select(x => new WDINDocumentListEnt { Notes = x.Notes }).ToList();
                //lst.ForEach(x => x.Notes = !string.IsNullOrWhiteSpace(x.Notes) ? (n = n + 1) + ". &nbsp" + x.Notes : x.Notes);
                //rtn += "<tfoot>";
                //rtn += "<tr><th rowspan='3' colspan='3' > NOTES : <br/>"
                //  + "1. &nbsp <i style='font-weight: 100; font-size: 10px;' class='fa fa-arrow-right' aria-hidden='true'></i>&nbsp &nbsp REVISIONS CARRIED <br>"
                //  + string.Join(" <br/>", lst.Select(x => x.Notes).ToList())
                //  + "</th>";
                //#endregion

                //#region Issue No
                //rtn += "<th class=\"descriptions\"> ISSUED NO. </th>";
                //if (total == 0)
                //{
                //    rtn += "<th class='center'></th>";
                //}
                //else
                //{
                //    foreach (var issue in IssueNoList)
                //    {
                //        rtn += "<th class='center'>" + issue.IssueNo.ToString() + "</th>";
                //    }
                //}
                //rtn += "</tr>";
                ////End Issue By Row
                //#endregion

                //#region Issue By
                //rtn += "<tr><th class=\"descriptions\">ISSUED BY </th>";
                ////Loop through IssueNoList and create table column header to display Issue Date
                //if (total == 0)
                //{
                //    rtn += "<th class='center'></th>";
                //}
                //else
                //{
                //    foreach (var issue in IssueNoList)
                //    {
                //        rtn += "<th class='center'>" + issue.t_init + "</th>";
                //    }
                //}
                //rtn += "</tr>";
                //#endregion

                //#region Issue Date
                //rtn += "<tr><th class=\"descriptions\"> DATE </th>";
                ////Loop through IssueNoList and create table column header to display Issue No                
                //if (total == 0)
                //{
                //    rtn += "<th class='center' style=\"text-align:center;\"></th>";
                //}
                //else
                //{
                //    foreach (var issue in IssueNoList)
                //    {
                //        rtn += "<th class='center'>" + (issue.IssueDate.HasValue ? issue.IssueDate.Value.ToString("dd/MM/yy") : string.Empty) + "</th>";
                //    }
                //}
                //rtn += "</tr>";
                //#endregion
                //rtn += "</tfoot>";
                #endregion
                rtn += "</table>";
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rtn;
        }

        #endregion
    }

    public class wdncopy
    {
        public WDN002 sourcedata { get; set; }
        public WDN002 destidata { get; set; }
        public int SourceLineId { get; set; }
    }
    public class WDINDocumentListEnt
    {
        public string DocumentNo { get; set; }
        public string Description { get; set; }
        public int? RefId { get; set; }
        public int? Id { get; set; }
        public string Plan { get; set; }
        public string Department { get; set; }
        public string DepartmentDesc { get; set; }
        public string Notes { get; set; }
    }

    public class WDINDept
    {
        public int? Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Init { get; set; }
    }
}