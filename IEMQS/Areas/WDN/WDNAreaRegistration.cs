﻿using System.Web.Mvc;

namespace IEMQS.Areas.WDN
{
    public class WDNAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "WDN";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "WDN_default",
                "WDN/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}