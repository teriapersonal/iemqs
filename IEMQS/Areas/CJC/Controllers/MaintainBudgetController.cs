﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
namespace IEMQS.Areas.CJC.Controllers
{
    public class MaintainBudgetController : clsBase
    {
        // GET: CJC/MaintainBudget
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.DiaList = (from u in db.CJC003
                               where u.IsActive == true
                               select new { CatID = u.Id, CatDesc = u.Description }).ToList();

            return View();
        }

        [SessionExpireFilter]
        public PartialViewResult LoadIndexGridDataPartial(string project)
        {
            ViewBag.Project = project;
            return PartialView("_LoadIndexGridDataPartial");
        }

        public ActionResult GetIndexGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1 ";
                if (!string.IsNullOrWhiteSpace(param.Project))
                { whereCondition += " and Project='" + param.Project + "'"; }
                if (!string.IsNullOrWhiteSpace(param.FilterDocNo))
                { whereCondition += " and ProductType='" + param.FilterDocNo + "'"; }

                string[] columnName = { "Project", "(cjc16.Project + ' - ' + com003.t_dsca)", "EstimatedValue", "CreatedBy", "CONVERT(nvarchar(20),EditedOn,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                var lstLines = db.SP_CJC_GET_MAINTAIN_BUDGET_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.Id),
                                        Convert.ToString(c.ProjectDescription),
                                        Convert.ToString(c.ProductType),
                                        c.isCJCCreated == 1 ? Convert.ToString(c.EstimatedValue) : Helper.GenerateNumericTextbox(c.Id, "EstimatedValue",Convert.ToString(c.EstimatedValue),"UpdateData(this,"+ c.Id +",\""+c.Project+"\",true )",false,"","",""),
                                        c.isCJCCreated == 1 ? Helper.GenerateActionCheckbox(c.Id,"LockAt80Perc",Convert.ToBoolean(c.LockAt80Perc) ? true :false,"",true) :  Helper.GenerateActionCheckbox(c.Id,"LockAt80Perc",Convert.ToBoolean(c.LockAt80Perc) ? true :false,"UpdateData(this,"+ c.Id +",\""+c.Project+"\",true )",false,""),
                                        Convert.ToString(c.CreatedBy),
                                        Convert.ToDateTime(c.EditedOn).ToString("dd/MM/yyyy"),
                                        Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.Id +");","",(c.isCJCCreated == 1)),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProject(CJC016 obj)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (obj != null)
                {
                    if (db.CJC016.Any(x => x.Project == obj.Project && x.ProductType == obj.ProductType && x.EstimatedValue == obj.EstimatedValue && x.Id != obj.Id))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    CJC016 objCJC016 = null;
                    if (obj.Id > 0)
                    {
                        objCJC016 = db.CJC016.Where(x => x.Id == obj.Id).FirstOrDefault();
                        if (objCJC016 != null)
                        {
                            objCJC016.Project = obj.Project;
                            objCJC016.ProductType = obj.ProductType;
                            objCJC016.EstimatedValue = obj.EstimatedValue;
                            objCJC016.LockAt80Perc = obj.LockAt80Perc;
                            objCJC016.EditedBy = objClsLoginInfo.UserName;
                            objCJC016.EditedOn = DateTime.Now;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                        }
                    }
                    else
                    {

                        objCJC016 = new CJC016();
                        objCJC016.Project = obj.Project;
                        objCJC016.ProductType = obj.ProductType;
                        objCJC016.EstimatedValue = obj.EstimatedValue;
                        objCJC016.LockAt80Perc = obj.LockAt80Perc;
                        objCJC016.CreatedBy = objClsLoginInfo.UserName;
                        objCJC016.CreatedOn = DateTime.Now;
                        objCJC016.EditedBy = objClsLoginInfo.UserName;
                        objCJC016.EditedOn = DateTime.Now;
                        db.CJC016.Add(objCJC016);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult UpdateDetails(int lineId, string columnName, string columnValue, string project)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string tableName = "CJC016";
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    if (columnName == "EstimatedValue")
                    {
                        double EstimatedValue = Convert.ToDouble(columnValue);
                        var ProductType = db.CJC016.Where(x => x.Id == lineId).FirstOrDefault().ProductType;
                        if (!db.CJC016.Any(x => x.Project == project && x.ProductType == ProductType && x.EstimatedValue == EstimatedValue && x.Id != lineId))
                        {
                            db.SP_COMMON_TABLE_UPDATE(tableName, lineId, "Id", columnName, columnValue, objClsLoginInfo.UserName);
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                    else
                    {
                        db.SP_COMMON_TABLE_UPDATE(tableName, lineId, "Id", columnName, columnValue, objClsLoginInfo.UserName);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteData(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CJC016 objCJC016 = db.CJC016.Where(x => x.Id == Id).FirstOrDefault();
                if (objCJC016 != null)
                {
                    db.CJC016.Remove(objCJC016);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_CJC_GET_MAINTAIN_BUDGET_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  Project = li.ProjectDescription,
                                  ProductType = li.ProductType,
                                  EstimatedValue = li.EstimatedValue,
                                  //CreatedBy = li.CreatedBy,
                                  LastModifiedOn = Convert.ToDateTime(li.EditedOn).ToString("dd/MM/yyyy"),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}