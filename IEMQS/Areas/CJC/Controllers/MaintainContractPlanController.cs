﻿using IEMQS.Areas.IPI.Models;
using IEMQS.Models;
using IEMQSImplementation;
using IEMQSImplementation.Models;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.CJC.Controllers
{
    public class MaintainContractPlanController : clsBase
    {
        // GET: CJC/MaintainContractPlan
        const string ActionAddNew = "ADD_NEW";
        const string ActionEdit = "EDIT";

        #region Index

        [SessionExpireFilter]
        public ActionResult Index()
        {
            ViewBag.Title = "Auto Billing";
            return View();
        }

        public ActionResult LoadDataGridPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_LoadIndexGridDataPartial");
        }

        public ActionResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1 and CreatedBy = '" + objClsLoginInfo.UserName + "'";
                string[] columnName = { "Project", "ShopDesc", "DiaDesc", "Location", "LocationDesc", "PurchaseOdrerNo", "PurchaseOdrerLineNo", "BillNo", "CONVERT(nvarchar(20),BillDate,103)", "Status", "CONVERT(nvarchar(20),CreatedOn,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string[] arrPendingStatus = { clsImplementationEnum.AutoCJCStatus.Pending_with_Authoriser.GetStringValue(), clsImplementationEnum.AutoCJCStatus.Pending_with_A_cs.GetStringValue(), clsImplementationEnum.AutoCJCStatus.Pending_with_Certifier.GetStringValue(), clsImplementationEnum.AutoCJCStatus.Pending_with_Initiator.GetStringValue() };
                var lstLines = db.SP_CJC_GET_CONTRACT_PLAN_HEADER_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.HeaderId),
                                        Convert.ToString(c.Project),
                                        Convert.ToString(c.DiaDesc),
                                        Convert.ToString(c.LocationDesc),
                                        /*Convert.ToString(c.ShopDesc),
                                        Convert.ToString(c.PurchaseOdrerNo),
                                        Convert.ToString(c.PurchaseOdrerLineNo),
                                        Convert.ToString(c.BillNo),
                                        c.BillDate.HasValue ? Convert.ToDateTime(c.BillDate).ToString("dd/MM/yyyy"): "",
                                        Convert.ToString(c.CJCNo),*/
                                        Convert.ToString(c.TotalCJCAmount),
                                        Convert.ToString(c.Status != clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue() ? (string.IsNullOrWhiteSpace(c.LNCJCStatus) ?clsImplementationEnum.AutoCJCStatus.Deleted.GetStringValue(): c.LNCJCStatus) : c.Status),
                                        Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                        Helper.GenerateActionIcon(c.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/CJC/MaintainContractPlan/CJCDetails/"+c.HeaderId ,false)+
                                        Helper.GenerateActionIcon(c.HeaderId, "Print", "Print Report", "fa fa-print", "PrintReport('"+c.HeaderId+"')","" ,false)
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region CJC Detail Page

        [SessionExpireFilter]
        public ActionResult CJCDetails(int Id = 0)
        {
            CJC030 objCJC030 = new CJC030();
            var JobDiaList = (from u in db.CJC003
                              where u.IsActive == true
                              select new { Key = u.Id, Value = u.Description }).ToList();
            var Shops = db.CJC015
                            .Where(x => x.VendorCode == objClsLoginInfo.UserName)
                            .Select(x => new { Key = x.ShopCode, Value = x.ShopCode }).Distinct().ToList();

            int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
            var LocationList = (from com2 in db.COM002
                                where com2.t_dtyp == LOC
                                select new { Key = com2.t_dimx, Value = com2.t_desc, CatID = com2.t_dimx, CatDesc = com2.t_desc, }).Distinct().ToList();

            ViewBag.BusinessPartnerName = objClsLoginInfo.UserName + "-" + objClsLoginInfo.Name;
            if (Id > 0)
            {
                objCJC030 = db.CJC030.Where(x => x.HeaderId == Id).FirstOrDefault();
                if (objCJC030 != null)
                {
                    string status = (objCJC030.Status != clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue()) ? Manager.GetAutoCJCStatus(objCJC030.CJCNo) : objCJC030.Status;
                    objCJC030.Status = string.IsNullOrWhiteSpace(status) ? clsImplementationEnum.AutoCJCStatus.Deleted.GetStringValue() : status;
                    ViewBag.Location = LocationList.Where(x => x.Key == objCJC030.Location).Select(x => x.Value).FirstOrDefault();
                    ViewBag.Project = Manager.GetProjectAndDescription(objCJC030.Project);
                    //ViewBag.Shop = Shops.Where(x => x.Key == objCJC030.Shop).Select(x => x.Value).FirstOrDefault();
                    ViewBag.JobDia = JobDiaList.Where(x => x.Key == objCJC030.Dia).Select(x => x.Value).FirstOrDefault();
                    //ViewBag.Initiator = Manager.GetPsidandDescription(objCJC030.Initiator);
                    //ViewBag.BusinessPartner = db.COM006.Where(i => i.t_bpid == objCJC030.BusinessPartner).Select(i => i.t_bpid + " - " + i.t_nama).FirstOrDefault();
                    //ViewBag.POLine = db.SP_COMMON_GET_LN_PO_LINES(1, int.MaxValue, "", " ln.t_orno = '" + objCJC030.PurchaseOdrerNo + "' and ln.pono = " + objCJC030.PurchaseOdrerLineNo + "")?.Select(x => x.item).FirstOrDefault();
                    ViewBag.Action = "edit";
                }
            }
            else
            {
                var objHZWLoc = LocationList.Where(x => x.Key == "HZW").FirstOrDefault();
                if (objHZWLoc != null)
                {
                    ViewBag.Location = objHZWLoc.Value;
                    objCJC030.Location = "HZW";
                }
                objCJC030.Revision = 0;
                objCJC030.BusinessPartner = objClsLoginInfo.UserName;
                objCJC030.Shop = objClsLoginInfo.UserName;
                ViewBag.Shop = Shops.Where(x => x.Key == objCJC030.Shop).Select(x => x.Value).FirstOrDefault();
                objCJC030.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                ViewBag.Action = "new";
            }


            ViewBag.Locations = LocationList;
            ViewBag.ContractorShops = Shops;
            ViewBag.DiaList = JobDiaList;
            return View(objCJC030);
        }

        [HttpPost]
        public void removeTempValue()
        {
            TempData["FetchSeamList"] = null;
            TempData["ProjectLines"] = null;
        }

        [HttpPost]
        public ActionResult IsProjectDetailsExist(CJC030 cjc20)
        {
            bool isDia = true;
            bool isDuplicate = false;
            ResponceMsgWithCJC objResponseMsg = new ResponceMsgWithCJC();
            var obj = db.CJC030.Where(x => x.Project == cjc20.Project).FirstOrDefault();
            if (obj != null)
            {
                if (obj.Dia != cjc20.Dia)
                {
                    isDia = false;
                }
                //else if (obj.Dia == cjc20.Dia && obj.PurchaseOdrerNo == cjc20.PurchaseOdrerNo && obj.PurchaseOdrerLineNo == cjc20.PurchaseOdrerLineNo)
                //{
                //    isDuplicate = true;
                //}
            }
            objResponseMsg.Key = isDia;
            objResponseMsg.dataKey = isDuplicate;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public void RetenTempDataValue()
        {
            TempData.Keep("FetchSeamList");
            TempData.Keep("ProjectLines");
        }

        #endregion

        #region Declared global variable/array which are used multiple times
        double pi = Math.PI;
        string[] arrSeamTypeNW = { clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue() };
        string[] arrSeamTypeNC2 = { clsImplementationEnum.AutoCJCSeamType.AW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.NP.GetStringValue() };
        string[] arrSeamTypeWp = { clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.CW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.LW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.OW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.CR.GetStringValue() ,clsImplementationEnum.AutoCJCSeamType.HX.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PTCLW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PTCCW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PTCOW.GetStringValue(),
                                       clsImplementationEnum.AutoCJCSeamType.PTCBW.GetStringValue()};
        string[] arrSetUpMinThk = { clsImplementationEnum.AutoCJCSeamType.PTCLW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PTCCW.GetStringValue() };

        string[] arrSetUpNonOverlayRate = { clsImplementationEnum.AutoCJCSeamCategory.SC002.GetStringValue(), clsImplementationEnum.AutoCJCSeamCategory.SC003.GetStringValue() };
        string[] arrSetUpNozzleSizeRate = { clsImplementationEnum.AutoCJCSeamCategory.SC010.GetStringValue() };

        #endregion

        #region Seam Line Details

        public ActionResult LoadSeamLineGridDataPartial()
        {
            ViewBag.SeamCategory = Manager.GetSubCatagorywithoutBULocation("Seam Category").Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList();
            ViewBag.NozzleCategory = Manager.GetSubCatagorywithoutBULocation("Nozzle Category").Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList();

            List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();

            ViewBag.WeldingProcessList = lstWeldingProcess.AsEnumerable().Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList();

            List<CategoryData> lstCriticalSeam = Manager.GetSubCatagorywithoutBULocation("Critical Seam").ToList();
            ViewBag.CriticalSeam = lstCriticalSeam.AsEnumerable().Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList();

            return PartialView("_LoadSeamLineGridDataPartial");
        }

        public ActionResult ReadExcel1()
        {
            var lstSeamCategoryList = Manager.GetSubCatagorywithoutBULocation("Seam Category").Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList();
            var lstNozzleCategoryList = Manager.GetSubCatagorywithoutBULocation("Nozzle Category").Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList();

            List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();
            var lstWeldingProcessList = lstWeldingProcess.AsEnumerable().Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList();

            List<CategoryData> lstCriticalSeam = Manager.GetSubCatagorywithoutBULocation("Critical Seam").ToList();
            var CriticalSeams = lstCriticalSeam.AsEnumerable().Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList();


            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            //int HeaderId = Convert.ToInt32(Request.Form["HeaderId"]);
            int Jobdia = Convert.ToInt32(Request.Form["Jobdia"]);
            bool isFetchfromtemp = Convert.ToBoolean(Request.Form["isFetchfromtemp"]);
            int headerId = Convert.ToInt32(Request.Form["headerId"]);

            if (Request.Files.Count == 0)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please select a file first!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            HttpPostedFileBase upload = Request.Files[0];
            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);

                bool isError;
                DataSet ds = ToAddSeamDetailsHeader(package, Jobdia, out isError);
                if (!isError)
                {
                    if (isFetchfromtemp)
                    {
                        DataTable dt = (DataTable)TempData["FetchSeamList"];

                        List<DataRow> dtImported = (from u in ds.Tables[0].Rows.Cast<DataRow>()
                                                    select u).ToList();

                        foreach (DataRow dr in dtImported)
                        {
                            DataRow dtSeamLines = (from u in dt.Rows.Cast<DataRow>()
                                                   where
                                                   Convert.ToString(u["QualityProject"]) == dr["QualityProjectNo"].ToString() &&
                                                   Convert.ToString(u["SeamNo"]) == dr["Seam No"].ToString()
                                                   select u).FirstOrDefault();

                            if (dtSeamLines != null)
                            {

                                bool IsSeamFetchedBySeamType = true;

                                string qualityProjectNo = dr["QualityProjectNo"].ToString();
                                string seamNo = dr["Seam No"].ToString();
                                string SeamJointType = Convert.ToString(dtSeamLines["SeamJointType"]);
                                double MinThickness = Convert.ToDouble(dtSeamLines["MinThickness"]);
                                string NozzleSize = Convert.ToString(dr["NozzleSize"]);
                                string SeamSubCategory = Convert.ToString(dr["SeamSubCategory"]);
                                //string SetupRate = Convert.ToString(dtSeamLines["SetupRate"]);
                                string NozzleCategory = SeamSubCategory;
                                string weldingProcess = Convert.ToString(dr["Welding Process"]);
                                string SeamCategory = Convert.ToString(dr["SeamCategory"]);
                                //string SeamCategoryCode = db.GLB002.Where(x => x.Description == Convert.ToString(dr["SeamCategory"])).Select(x => x.Code).FirstOrDefault();
                                string SeamCategoryCode = GetCategoryCodeByDescription("Seam Category", SeamCategory).Code;

                                //string SeamType = Convert.ToString(dtSeamLines["SeamType"]);
                                int SeamLength = Convert.ToInt32(dr["SeamLength"]);

                                string SeamNo = Convert.ToString(dr["Seam No"]);

                                string SeamType = GetSeamCategory(qualityProjectNo, seamNo);

                                double? Factor = !string.IsNullOrWhiteSpace(Convert.ToString(dr["CriticalSeam"])) ? FetchFactorData(CriticalSeams.Where(x => x.Value == Convert.ToString(dr["CriticalSeam"])).FirstOrDefault().Key) : 0;

                                //if (SeamType == clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue())
                                //{
                                //    if (!string.IsNullOrWhiteSpace(NozzleSize))
                                //    {
                                //        #region Nozzle Size 
                                //        dtSeamLines["NozzleSize"] = NozzleSize;
                                //        #endregion
                                //    }
                                //}

                                if (SeamType == clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue() ||
                                    SeamType == clsImplementationEnum.AutoCJCSeamType.AW.GetStringValue() ||
                                    SeamType == clsImplementationEnum.AutoCJCSeamType.PW.GetStringValue() ||
                                    SeamType == clsImplementationEnum.AutoCJCSeamType.NP.GetStringValue())
                                {
                                    if (!string.IsNullOrWhiteSpace(SeamSubCategory))
                                    {
                                        dtSeamLines["NozzleCategoryDesc"] = SeamSubCategory;
                                        dtSeamLines["NozzleCategory"] = Convert.ToString(dr["SeamSubCategory"]);
                                    }
                                }

                                if (SeamType != clsImplementationEnum.AutoCJCSeamType.AW.GetStringValue() &&
                                    SeamType != clsImplementationEnum.AutoCJCSeamType.PW.GetStringValue() &&
                                    SeamType != clsImplementationEnum.AutoCJCSeamType.NP.GetStringValue() &&
                                    SeamType != clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue())
                                {
                                    if (!string.IsNullOrWhiteSpace(weldingProcess))
                                    {
                                        dtSeamLines["WeldingProcessDesc"] = weldingProcess;
                                        dtSeamLines["WeldingProcess"] = lstWeldingProcessList.Where(x => x.Value == Convert.ToString(dr["Welding Process"])).FirstOrDefault().Key;
                                    }
                                }


                                if (!string.IsNullOrWhiteSpace(Convert.ToString(dr["CriticalSeam"])))
                                {
                                    dtSeamLines["CriticalSeamDesc"] = Convert.ToString(dr["CriticalSeam"]);
                                    dtSeamLines["CriticalSeam"] = CriticalSeams.Where(x => x.Value == Convert.ToString(dr["CriticalSeam"])).FirstOrDefault().Key;
                                }

                                #region Setup Rate

                                if (IsSeamFetchedBySeamType)
                                {
                                    dtSeamLines["SetupRate"] = SetUpRateCalculation(null, 0, true, string.Empty, string.Empty, "Add", string.Empty, SeamCategoryCode, MinThickness, Jobdia, "", NozzleCategory, (string.IsNullOrWhiteSpace(NozzleSize) ? 0 : Convert.ToDouble(NozzleSize)), SeamType);
                                }

                                #endregion

                                #region Welding Rate
                                double? WeldingRate = WeldRateCalculation(null, 0, true, string.Empty, string.Empty, "Add", string.Empty, SeamCategoryCode, MinThickness, Jobdia, weldingProcess, SeamJointType, NozzleCategory, (string.IsNullOrWhiteSpace(NozzleSize) ? 0 : Convert.ToDouble(NozzleSize)), SeamType);
                                dtSeamLines["WeldingRate"] = WeldingRate;
                                #endregion

                                if (Factor > 0)
                                    dtSeamLines["Factor"] = Factor;
                                else
                                    dtSeamLines["Factor"] = 1;

                                #region Amount 
                                dtSeamLines["Amount"] = claimSeamAmount(0, SeamCategoryCode, SeamLength, (!string.IsNullOrWhiteSpace(Convert.ToString(dtSeamLines["Unit"])) ? Convert.ToDouble(dtSeamLines["Unit"]) : 0),
                                                                                                 (WeldingRate ?? 0),
                                                                                                 Factor ?? Convert.ToDouble(Factor),
                                                                                                 (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SetupRate"])) ? Convert.ToDouble(dr["SetupRate"]) : 0),
                                                                                                 (!string.IsNullOrWhiteSpace(Convert.ToString(dr["NozzleSize"])) ? Convert.ToDouble(dr["NozzleSize"]) : 0));

                                #endregion
                                dt.AcceptChanges();
                            }
                        }

                        TempData["FetchSeamList"] = dt;
                        RetenTempDataValue();
                    }
                    else
                    {

                        List<DataRow> dtImported = (from u in ds.Tables[0].Rows.Cast<DataRow>()
                                                    select u).ToList();

                        foreach (DataRow dr in dtImported)
                        {
                            var seamNo = dr["Seam No"].ToString();
                            var qualityProjectNo = dr["QualityProjectNo"].ToString();

                            CJC031 objDr = db.CJC031.Where(x => x.SeamNo == seamNo && x.QualityProject == qualityProjectNo && x.HeaderId == headerId).FirstOrDefault();

                            if (objDr != null)
                            {

                                bool IsSeamFetchedBySeamType = true;

                                string SeamType = GetSeamCategory(qualityProjectNo, seamNo);

                                string SeamCategoryCode = GetCategoryCodeByDescription("Seam Category", Convert.ToString(dr["SeamCategory"])).Code;

                                double? Factor = !string.IsNullOrWhiteSpace(Convert.ToString(dr["CriticalSeam"])) ? FetchFactorData(CriticalSeams.Where(x => x.Value == Convert.ToString(dr["CriticalSeam"])).FirstOrDefault().Key) : 0;


                                #region Nozzle Size 
                               /* if (SeamType == clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue())
                                {
                                    #region Nozzle Size 
                                    //objDr.NozzleSize = CalcultaionOfNozzleSize(0, objDr.SeamCategory, SeamType, Convert.ToInt32(objDr.SeamLength));
                                    if (!string.IsNullOrEmpty(Convert.ToString(dr["NozzleSize"])))
                                        objDr.NozzleSize = Convert.ToDouble(dr["NozzleSize"]);
                                    #endregion
                                }
                                */

                                if (SeamType == clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue() ||
                                    SeamType == clsImplementationEnum.AutoCJCSeamType.AW.GetStringValue() ||
                                    SeamType == clsImplementationEnum.AutoCJCSeamType.PW.GetStringValue() ||
                                    SeamType == clsImplementationEnum.AutoCJCSeamType.NP.GetStringValue())
                                {
                                    objDr.NozzleCategory = Convert.ToString(dr["SeamSubCategory"]);
                                }
                                #endregion

                                if (SeamType != clsImplementationEnum.AutoCJCSeamType.AW.GetStringValue() &&
                                    SeamType != clsImplementationEnum.AutoCJCSeamType.PW.GetStringValue() &&
                                    SeamType != clsImplementationEnum.AutoCJCSeamType.NP.GetStringValue() &&
                                    SeamType != clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue())
                                {
                                    objDr.WeldingProcess = lstWeldingProcessList.Where(x => x.Value == Convert.ToString(dr["Welding Process"])).FirstOrDefault().Key;
                                }

                                if (!string.IsNullOrEmpty(Convert.ToString(dr["CriticalSeam"])))
                                {
                                    //objDr.CriticalSeam = Convert.ToString(dr["CriticalSeam"]);
                                    objDr.CriticalSeam = CriticalSeams.Where(x => x.Value == Convert.ToString(dr["CriticalSeam"])).FirstOrDefault().Key;

                                }


                                #region Setup Rate

                                if (IsSeamFetchedBySeamType && (string.IsNullOrWhiteSpace(Convert.ToString(dr["SetupRate"]))))
                                {
                                    objDr.SetupRate = Convert.ToDouble(SetUpRateCalculation(null, 0, true, string.Empty, string.Empty, "Add", string.Empty, SeamCategoryCode, Convert.ToDouble(dr["MinThickness"]), Jobdia, "", objDr.NozzleCategory, (string.IsNullOrWhiteSpace(objDr.NozzleSize.ToString()) ? 0 : Convert.ToDouble(objDr.NozzleSize)), SeamType));
                                }

                                #endregion

                                #region Welding Rate
                                objDr.WeldingRate = WeldRateCalculation(null, 0, true, string.Empty, string.Empty, "Add", string.Empty, SeamCategoryCode, Convert.ToDouble(objDr.MinThickness), Jobdia, objDr.WeldingProcess, objDr.SeamJointType, objDr.NozzleCategory, (string.IsNullOrWhiteSpace(objDr.NozzleSize.ToString()) ? 0 : Convert.ToDouble(objDr.NozzleSize)), SeamType);
                                #endregion

                                if (Factor > 0)
                                    objDr.Factor = Factor;
                                else
                                    objDr.Factor = 1;

                                #region Amount 
                                objDr.Amount = claimSeamAmount(0, SeamCategoryCode, Convert.ToInt32(objDr.SeamLength), (!string.IsNullOrWhiteSpace(Convert.ToString(objDr.Unit)) ? Convert.ToDouble(objDr.Unit) : 0),
                                                                                                 (objDr.WeldingRate ?? 0),
                                                                                                 Factor ?? Convert.ToDouble(Factor),
                                                                                                 (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SetupRate"])) ? Convert.ToDouble(dr["SetupRate"]) : 0),
                                                                                                 (!string.IsNullOrWhiteSpace(Convert.ToString(dr["NozzleSize"])) ? Convert.ToDouble(dr["NozzleSize"]) : 0));
                                #endregion

                                db.SaveChanges();
                            }


                        }

                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Seams data copied successfully";

                }
                else
                {
                    objResponseMsg = ExportToOtherExcel(ds);
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please upload valid excel file!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        public ActionResult GetWPSWeldingProcessList(string QualityProject, string SeamNo)
        {
            List<string> list = new List<string>();
            list = db.WPS025.Where(x => x.QualityProject == QualityProject && x.SeamNo == SeamNo && x.RefTokenId > 0).Select(x => x.WeldingProcess).Distinct().ToList();
            if (list.Count > 0)
            {
                List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();
                lstWeldingProcess = (from a in lstWeldingProcess where list.Contains(a.Value) select a).Distinct().ToList();
                return Json(lstWeldingProcess.AsEnumerable().Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult FetchSeamCategorywiseNozzleCategory(string seamcategory, int dia)
        {
            List<CategoryData> list = new List<CategoryData>();
            list = db.CJC011.Where(x => x.SeamCategory == seamcategory && x.Dia == dia).Where(x => x.NozzleCategory != "" && x.NozzleCategory != null).Select(x => new CategoryData { Value = x.NozzleCategory, CategoryDescription = x.NozzleCategory }).Distinct().ToList();
            if (list.Count > 0)
            {
                return Json(list.AsEnumerable().Select(x => new { Key = x.Value, Value = x.CategoryDescription }).ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LoadFetchSeamGridDataTable(JQueryDataTableParamModel param, int HeaderId)
        {
            try
            {
                string HeaderCJCNo = !string.IsNullOrWhiteSpace(param.CJCNo) ? param.CJCNo : "";
                string HeaderCJCStatus = !string.IsNullOrWhiteSpace(HeaderCJCNo) ? Manager.GetAutoCJCStatus(HeaderCJCNo) : "";
                string ActionType = "";
                if (HeaderId > 0)
                {
                    ActionType = ActionEdit;
                }
                else
                {
                    ActionType = ActionAddNew;
                }

                int Jobdia = Convert.ToInt32(param.CTQHeaderId);
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int? totalRecords = 0;
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                string whereCondition = " 1=1";// AND  Shop='" + param.Shop + "'";
                //whereCondition += "  AND ((IsSetUpCleared=1 and(SetupCJCStatus like '%Returned%' or ISNULL(SetupCJCStatus,'') = '')) or (IsPWHTCleared=1 AND ( WeldCJCStatus like '%Returned%' or ISNULL(WeldCJCStatus,'') = '')))";
                //if (param.Status.Contains("Returned"))
                //{
                //    whereCondition += "  AND ((IsSetUpCleared=1 and  SetupCJCStatus <> 'Draft' and ISNULL(SetupCJCNo,'') = '') or (IsPWHTCleared=1 AND WeldCJCStatus <> 'Draft' and ISNULL(WeldCJCNo,'') = ''))";
                //}
                //else
                //{
                //    whereCondition += "  AND ((IsSetUpCleared=1 and ISNULL(SetupCJCNo,'') = '') or (IsPWHTCleared=1 AND ISNULL(WeldCJCNo,'') = ''))";

                //}
                DataTable dtSeamLines = new DataTable();
                if (TempData["FetchSeamList"] == null)
                {
                    var lstLines = db.SP_CJC_GET_CONTRACT_PLAN_APPLICABLE_SEAM_DATA(0, 0, strSortOrder, whereCondition, param.Project, param.Location).ToList();
                    dtSeamLines = Helper.ToDataTable(lstLines);
                    dtSeamLines.Columns.Add("EditLineId", typeof(Int32));
                    dtSeamLines.AsEnumerable().ToList<DataRow>().ForEach(r =>
                    {
                        r["EditLineId"] = 0;
                    });
                    dtSeamLines = CalculateAndUpdateDataTable(dtSeamLines, HeaderCJCNo, ActionType, Jobdia);
                    dtSeamLines = MergeEditLineDataWithMainDT(dtSeamLines, HeaderId);
                    TempData["FetchSeamList"] = dtSeamLines;
                }
                else
                {
                    dtSeamLines = (DataTable)TempData["FetchSeamList"];
                }

                string searchExpression = "1=1 ";
                string sortExpression = "";

                string[] columnName = { "QualityProject","SeamNo"
                                ,"SetupCJCNo"
                                ,"WeldCJCNo"
                                ,"SeamCategory"
                                ,"SeamCategoryDesc"
                               // ,"NozzleSize"
                                ,"NozzleCategory"
                                ,"NozzleCategoryDesc"
                                ,"SeamJointType"
                                ,"Convert(SeamLength, System.String)"
                                ,"Convert(Part1Position, System.String)"
                                ,"Convert(Part2Position, System.String)"
                                ,"Convert(Part3Position, System.String)"
                                ,"Convert(Part1Thickness, System.String)"
                                ,"Convert(Part2Thickness, System.String)"
                                ,"Convert(Part3Thickness, System.String)"
                                ,"Convert(MinThickness, System.String)"
                                ,"Convert(Unit, System.String)"
                                ,"WeldingProcess"
                                ,"WeldingProcessDesc"
                                //,"SetupRate"
                                //,"WeldingRate"
                                ,"CriticalSeamDesc"
                                //,"Factor"
                                //,"Amount"
                };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    searchExpression += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    searchExpression += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    sortExpression = "" + sortColumnName + " " + sortDirection + " ";
                }

                DataView dataView = dtSeamLines.AsDataView();
                if (!string.IsNullOrWhiteSpace(searchExpression))
                    dataView.RowFilter = searchExpression;
                if (!string.IsNullOrWhiteSpace(sortExpression))
                    dataView.Sort = sortExpression;

                totalRecords = dataView.ToTable().Rows.Count;

                List<DataRow> filteredList = dataView.ToTable().Select().Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();

                var res = (from c in filteredList
                           select new[] {
                                    Convert.ToString(c["ROW_NO"]),
                                    Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "LineId", Convert.ToString(c["LineId"])),
                                    Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "HeaderId", Convert.ToString(c["HeaderId"])),
                                    Convert.ToString(c["QualityProject"]),
                                    Convert.ToString(c["SeamNo"]),
                                    "",
                                    WebUtility.HtmlEncode(Convert.ToString(c["SeamCategoryDesc"]))+""+Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "SeamCategory",Convert.ToString(c["SeamCategory"])),
                                    Convert.ToString(c["NozzleSize"]),// Helper.GenerateTextbox(Convert.ToInt32(c["ROW_NO"]), "NozzleSize", Convert.ToString(c["NozzleSize"]),"",!arrSeamTypeNW.Contains(Convert.ToString(c["SeamType"])), "","","clsMandatory", false,""),
                                    Helper.HTMLAutoComplete(Convert.ToInt32(c["ROW_NO"]), "txtNozzleCategory",(arrSeamTypeNW.Contains(Convert.ToString(c["SeamType"])) || arrSeamTypeNC2.Contains(Convert.ToString(c["SeamType"]))) ? Convert.ToString(c["NozzleCategoryDesc"]) : "","", false,"","NozzleCategory",EnableDisableNozzleCategorySelection(HeaderCJCNo,HeaderCJCStatus,Convert.ToString(c["SetupCJCNo"]),Convert.ToString(c["WeldCJCNo"]),Convert.ToString(c["SeamType"])),"","","Select","clsMandatory")+""+Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "NozzleCategory",(arrSeamTypeNW.Contains(Convert.ToString(c["SeamType"])) || arrSeamTypeNC2.Contains(Convert.ToString(c["SeamType"]))) ? Convert.ToString(c["NozzleCategoryDesc"]) : ""),
                                    Convert.ToString(c["SeamJointType"]),
                                    Convert.ToString(c["SeamLength"]),
                                    Convert.ToString(c["Part1Position"]),
                                    Convert.ToString(c["Part2Position"]),
                                    Convert.ToString(c["Part3Position"]),
                                    Convert.ToString(c["Part1Thickness"]),
                                    Convert.ToString(c["Part2Thickness"]),
                                    Convert.ToString(c["Part3Thickness"]),
                                    !string.IsNullOrWhiteSpace(Convert.ToString(c["MinThickness"])) ? Convert.ToString(c["MinThickness"]) : "",
                                    Convert.ToString(c["Unit"]),
                                    Helper.HTMLAutoComplete(Convert.ToInt32(c["ROW_NO"]),"txtWeldingProcess",arrSeamTypeWp.Contains(Convert.ToString(c["SeamType"])) ? Convert.ToString(c["WeldingProcessDesc"]) : "","", false,"","WeldingProcess",EnableDisableWeldingProcessSelection(HeaderCJCNo,HeaderCJCStatus,Convert.ToString(c["SetupCJCNo"]),Convert.ToString(c["WeldCJCNo"]),Convert.ToString(c["SeamType"])),"","","Select","clsMandatory")+""+Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "WeldingProcess",arrSeamTypeWp.Contains(Convert.ToString(c["SeamType"])) ? Convert.ToString(c["WeldingProcessDesc"]) : ""),
                                    Convert.ToString(c["SetupRate"]),
                                    Convert.ToString(c["WeldingRate"]),
                                    Helper.HTMLAutoComplete(Convert.ToInt32(c["ROW_NO"]), "txtCriticalSeam", Convert.ToString(c["CriticalSeamDesc"]),"", false,"","CriticalSeam",EnableDisableCriticalSeamSelection(HeaderCJCNo,HeaderCJCStatus,Convert.ToString(c["SetupCJCNo"]),Convert.ToString(c["WeldCJCNo"])),"","","Select","clsMandatory")+""+Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "CriticalSeam",Convert.ToString(c["CriticalSeam"])),
                                    Convert.ToString(c["Factor"]),
                                    Helper.GenerateLabelFor(Convert.ToInt32(c["ROW_NO"]),"Amount",Convert.ToString(c["Amount"]),"","clsAmount"),
                                    !string.IsNullOrWhiteSpace(Convert.ToString(c["SetupCJCNo"])) ? Convert.ToString(c["SetupCJCNo"]) : "",
                                    !string.IsNullOrWhiteSpace(Convert.ToString(c["WeldCJCNo"])) ? Convert.ToString(c["WeldCJCNo"]) : "",
                                    "",
                                    Convert.ToString(c["IsSetUpCleared"]),
                                    Convert.ToString(c["IsPWHTCleared"]),
                                    Convert.ToString(c["Shop"]),
                                    Convert.ToString(c["SetupCJCStatus"]),
                                    Convert.ToString(c["WeldCJCStatus"]),
                                    Convert.ToString(c["SeamType"]),
                                    Convert.ToString(c["SeamQuantity"]),
                                    Helper.HTMLActionString(Convert.ToInt32(c["ROW_NO"]),"Delete","Delete Record","fa fa-trash-o","DeleteRowDatatable(this,"+Convert.ToInt32(c["ROW_NO"])+","+Convert.ToInt32(c["EditLineId"])+");")
                                   +Helper.HTMLActionString(Convert.ToInt32(c["ROW_NO"]),"Copy","Copy Record","fa fa-files-o","CopySeam(this,'"+Convert.ToString(c["QualityProject"])+"','"+Convert.ToString(c["SeamNo"])+"','"+Convert.ToString(c["SeamType"])+"',0,true)","",(string.IsNullOrWhiteSpace(Convert.ToString(c["SeamCategory"])))),
                        }).ToList();

                RetenTempDataValue();

                return Json(
                            new
                            {
                                sEcho = param.sEcho,
                                iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                                iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                                aaData = res,
                                strSortOrder = strSortOrder,
                                whereCondition = whereCondition
                            },
                            "application/json",
                            Encoding.UTF8,
                            JsonRequestBehavior.AllowGet
                        );
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }

                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region copy seam details
        [HttpPost]
        public ActionResult GetSeamCopyDetails(string qualityproject, string seamno, string seamtype, int headerid = 0, bool isFecthData = false)
        {
            ViewBag.QualityProject = qualityproject;
            ViewBag.SeamNo = seamno;
            ViewBag.Seamtype = seamtype;
            ViewBag.headerid = headerid;
            ViewBag.isFecthData = isFecthData;
            DataTable dt = (DataTable)TempData["FetchSeamList"];
            if (dt != null)
            {
                var dr = (from u in dt.Rows.Cast<DataRow>()
                          where Convert.ToString(u["SeamType"]) == seamtype
                          select new { SeamNo = Convert.ToString(u["SeamNo"]), Qproject = Convert.ToString(u["QualityProject"]) }).OrderBy(x => x.Qproject).Distinct().ToList();

                if (dr != null)
                {
                    ViewBag.Seams = dr.OrderBy(i => i.SeamNo).Select(i => i.SeamNo + "-" + i.Qproject).ToList();
                    //ViewBag.Seams = dr.OrderBy(i => new { i.Qproject, i.SeamNo }).Select(i => i.SeamNo + "||" + i.Qproject).ToList();
                }
            }
            RetenTempDataValue();
            return PartialView("_LoadSeamCopyDetailPartial");
        }

        [HttpPost]
        public ActionResult SeamCopyDetails(int? headerid, string sourceQProject, string sourceSeamno, string fromQProject, string toQProject, string fromSeam, string toSeam, int Jobdia, bool isFetchfromtemp)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (isFetchfromtemp)
                {
                    DataTable dt = (DataTable)TempData["FetchSeamList"];
                    List<DataRow> dtSeamLines = (from u in dt.Rows.Cast<DataRow>()
                                                 where //Convert.ToInt32(u["ROW_NO"]) 
                                                 (fromQProject != toQProject ? (Convert.ToString(u["QualityProject"]).CompareTo(fromQProject) >= 0 && Convert.ToString(u["QualityProject"]).CompareTo(toQProject) <= 0) : Convert.ToString(u["QualityProject"]) == fromQProject) &&
                                                 (fromSeam != toSeam ? (Convert.ToString(u["SeamNo"]).CompareTo(fromSeam) >= 0 && Convert.ToString(u["SeamNo"]).CompareTo(toSeam) <= 0) : Convert.ToString(u["SeamNo"]) == fromSeam)
                                                 select u).ToList();

                    DataRow objDr = (from u in dt.Rows.Cast<DataRow>()
                                     where Convert.ToString(u["SeamNo"]) == sourceSeamno && Convert.ToString(u["QualityProject"]) == sourceQProject
                                     select u).FirstOrDefault();
                    if (string.IsNullOrWhiteSpace(Convert.ToString(objDr["NozzleCategory"])) || string.IsNullOrWhiteSpace(Convert.ToString(objDr["WeldingProcess"])) || string.IsNullOrWhiteSpace(Convert.ToString(objDr["CriticalSeam"])))
                    {
                        foreach (DataRow dr in dtSeamLines)
                        {
                            //changes the Column Value
                            bool IsSeamFetchedBySeamType = true;
                            //DataRow objDr = (from u in dt.Rows.Cast<DataRow>()
                            //                 where //Convert.ToInt32(u["ROW_NO"]) 
                            //                 Convert.ToString(u["SeamNo"]) == Convert.ToString(dr["SeamNo"]) && Convert.ToString(u["QualityProject"]) == Convert.ToString(dr["QualityProject"])
                            //                 select u).FirstOrDefault();
                            string SeamJointType = Convert.ToString(dr["SeamJointType"]);
                            double MinThickness = Convert.ToDouble(dr["MinThickness"]);

                            string NozzleSize = Convert.ToString(dr["NozzleSize"]);

                            bool IsSetUpCleared = Convert.ToBoolean(dr["IsSetUpCleared"]);
                            bool IsPWHTCleared = Convert.ToBoolean(dr["IsPWHTCleared"]);

                            string SetUpCJCStatus = Convert.ToString(dr["SetUpCJCStatus"]);
                            string WeldCJCStatus = Convert.ToString(dr["WeldCJCStatus"]);

                            string SetupRate = Convert.ToString(dr["SetupRate"]);
                            string WeldingRate = Convert.ToString(dr["WeldingRate"]);

                            string Amount = Convert.ToString(dr["Amount"]);

                            dr["NozzleCategory"] = Convert.ToString(objDr["NozzleCategory"]);
                            dr["NozzleCategoryDesc"] = Convert.ToString(objDr["NozzleCategoryDesc"]);
                            string NozzleCategory = Convert.ToString(dr["NozzleCategory"]);
                            dr["WeldingProcess"] = Convert.ToString(objDr["WeldingProcess"]);
                            dr["WeldingProcessDesc"] = Convert.ToString(objDr["WeldingProcessDesc"]);
                            dr["CriticalSeam"] = Convert.ToString(objDr["CriticalSeam"]);
                            dr["CriticalSeamDesc"] = Convert.ToString(objDr["CriticalSeamDesc"]);
                            dr["Factor"] = Convert.ToString(objDr["Factor"]);
                            string weldingProcess = Convert.ToString(dr["WeldingProcess"]);

                            string SeamCategory = Convert.ToString(dr["SeamCategory"]);
                            string SeamType = Convert.ToString(dr["SeamType"]);
                            int SeamLength = Convert.ToInt32(dr["SeamLength"]);

                            #region Nozzle Size 
                              dr["NozzleSize"] = CalcultaionOfNozzleSize(0, SeamCategory, SeamType, SeamLength);
                            #endregion

                            #region Setup Rate

                            if (IsSeamFetchedBySeamType && (string.IsNullOrWhiteSpace(SetupRate)))
                            {
                                dr["SetupRate"] = SetUpRateCalculation(null, 0, true, string.Empty, string.Empty, "Add", string.Empty, SeamCategory, MinThickness, Jobdia, "", NozzleCategory, (string.IsNullOrWhiteSpace(NozzleSize) ? 0 : Convert.ToDouble(NozzleSize)), SeamType);
                            }

                            #endregion

                            #region Welding Rate
                            dr["WeldingRate"] = WeldRateCalculation(null, 0, true, string.Empty, string.Empty, "Add", string.Empty, SeamCategory, MinThickness, Jobdia, weldingProcess, SeamJointType, NozzleCategory, (string.IsNullOrWhiteSpace(NozzleSize) ? 0 : Convert.ToDouble(NozzleSize)), SeamType);
                            #endregion

                            #region Amount 
                            string Factor = Convert.ToString(dr["Factor"]);

                            dr["Amount"] = claimSeamAmount(0, SeamCategory, SeamLength, (!string.IsNullOrWhiteSpace(Convert.ToString(dr["Unit"])) ? Convert.ToDouble(dr["Unit"]) : 0),
                                                                                             (!string.IsNullOrWhiteSpace(Convert.ToString(dr["WeldingRate"])) ? Convert.ToDouble(dr["WeldingRate"]) : 0),
                                                                                             (!string.IsNullOrWhiteSpace(Factor) ? Convert.ToDouble(Factor) : 0),
                                                                                             (!string.IsNullOrWhiteSpace(Convert.ToString(dr["SetupRate"])) ? Convert.ToDouble(dr["SetupRate"]) : 0),
                                                                                             (!string.IsNullOrWhiteSpace(Convert.ToString(dr["NozzleSize"])) ? Convert.ToDouble(dr["NozzleSize"]) : 0));

                            #endregion
                        }
                        dt.AcceptChanges();
                        RetenTempDataValue();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Details not available";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    var obj = db.CJC030.Where(x => x.HeaderId == headerid).FirstOrDefault();
                    if (obj != null)
                    {
                        List<CJC031> dtSeamLines = (from u in db.CJC031
                                                    where u.HeaderId == headerid &&//Convert.ToInt32(u["ROW_NO"]) 
                                                    (u.QualityProject.CompareTo(fromQProject) >= 0 && u.QualityProject.CompareTo(toQProject) <= 0) &&
                                                    (u.SeamNo.CompareTo(fromSeam) >= 0 && u.SeamNo.CompareTo(toSeam) <= 0)
                                                    select u).ToList();
                        CJC031 objDr = (from u in db.CJC031
                                        where u.SeamNo == sourceSeamno && u.QualityProject == sourceQProject
                                        select u).FirstOrDefault();

                        foreach (CJC031 dr in dtSeamLines)
                        {
                            //changes the Column Value
                            bool IsSeamFetchedBySeamType = true;

                            string SeamJointType = dr.SeamJointType;
                            double MinThickness = Convert.ToDouble(dr.MinThickness);

                            string NozzleSize = Convert.ToString(dr.NozzleSize);

                            string SetupRate = Convert.ToString(dr.SetupRate);
                            string WeldingRate = Convert.ToString(dr.WeldingRate);

                            string Amount = Convert.ToString(dr.Amount);
                            string SeamType = db.CJC031.Where(a => a.SeamNo == dr.SeamNo && a.QualityProject == dr.QualityProject).Select(x => x.SeamCategory).FirstOrDefault();
                            dr.NozzleCategory = Convert.ToString(objDr.NozzleCategory);
                            string NozzleCategory = dr.NozzleCategory;
                            dr.WeldingProcess = Convert.ToString(objDr.WeldingProcess);
                            dr.CriticalSeam = Convert.ToString(objDr.CriticalSeam);
                            dr.Factor = objDr.Factor;
                            string weldingProcess = dr.WeldingProcess;

                            string SeamCategory = dr.SeamCategory;
                            int SeamLength = Convert.ToInt32(dr.SeamLength);

                            #region Nozzle Size 
                                dr.NozzleSize = CalcultaionOfNozzleSize(0, SeamCategory, SeamType, SeamLength);
                            #endregion

                            #region Setup Rate

                            if (IsSeamFetchedBySeamType && (string.IsNullOrWhiteSpace(SetupRate)))
                            {
                                var resultSetupRate = SetUpRateCalculation(null, 0, true, string.Empty, string.Empty, "Add", string.Empty, SeamCategory, MinThickness, Jobdia, "", NozzleCategory, (string.IsNullOrWhiteSpace(NozzleSize) ? 0 : Convert.ToDouble(NozzleSize)), SeamType);
                                dr.SetupRate = string.IsNullOrWhiteSpace(resultSetupRate) ? 0 : Convert.ToDouble(resultSetupRate);
                            }

                            #endregion

                            #region Welding Rate
                            dr.WeldingRate = WeldRateCalculation(null, 0, true, string.Empty, string.Empty, "Add", string.Empty, SeamCategory, MinThickness, Jobdia, weldingProcess, SeamJointType, NozzleCategory, (string.IsNullOrWhiteSpace(NozzleSize) ? 0 : Convert.ToDouble(NozzleSize)), SeamType);
                            #endregion

                            #region Amount 
                            string Factor = Convert.ToString(dr.Factor);

                            dr.Amount = claimSeamAmount(0, SeamCategory, SeamLength, (!string.IsNullOrWhiteSpace(Convert.ToString(dr.Unit)) ? Convert.ToDouble(dr.Unit) : 0),
                                                                                             (!string.IsNullOrWhiteSpace(Convert.ToString(dr.WeldingRate)) ? Convert.ToDouble(dr.WeldingRate) : 0),
                                                                                             (!string.IsNullOrWhiteSpace(Factor) ? Convert.ToDouble(Factor) : 0),
                                                                                             (!string.IsNullOrWhiteSpace(Convert.ToString(dr.SetupRate)) ? Convert.ToDouble(dr.SetupRate) : 0),
                                                                                             (!string.IsNullOrWhiteSpace(Convert.ToString(dr.NozzleSize)) ? Convert.ToDouble(dr.NozzleSize) : 0));

                            #endregion
                        }
                        db.SaveChanges();

                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Seams data copied successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult ExportToExcelSeamData(string Project, string Location, int HeaderId, int Jobdia, bool IsFetchFromSeamClick)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string HeaderCJCNo = "";
                string HeaderCJCStatus = "";
                string ActionType = "";
                if (HeaderId > 0)
                {
                    ActionType = ActionEdit;
                }
                else
                {
                    ActionType = ActionAddNew;
                }
                string strFileName = string.Empty;

                DataTable dtSeamLines = new DataTable();
                List<SP_CJC_GET_CONTRACT_PLAN_APPLICABLE_SEAM_DATA_Result> lstLines = null;
                List<SP_CJC_GET_CONTRACT_PLAN_SEAM_LINE_DETAILS_Result> lstSeamLines = null;
                List<CJCSeamExportExcel> newlst = new List<CJCSeamExportExcel>();
                if (IsFetchFromSeamClick)
                {
                    lstLines = db.SP_CJC_GET_CONTRACT_PLAN_APPLICABLE_SEAM_DATA(0, 0, string.Empty, " 1=1", Project, Location).ToList();
                    dtSeamLines = Helper.ToDataTable(lstLines);
                    dtSeamLines.Columns.Add("EditLineId", typeof(Int32));
                    dtSeamLines.AsEnumerable().ToList<DataRow>().ForEach(r =>
                    {
                        r["EditLineId"] = 0;
                    });
                    dtSeamLines = CalculateAndUpdateDataTable(dtSeamLines, HeaderCJCNo, ActionType, Jobdia);
                    dtSeamLines = MergeEditLineDataWithMainDT(dtSeamLines, HeaderId);

                    if (lstLines != null && lstSeamLines != null)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                   
                    foreach (DataRow dr in dtSeamLines.Rows)
                    {
                        newlst.Add(new CJCSeamExportExcel
                        {
                            QualityProjectNo = Convert.ToString(dr["QualityProject"]),
                            SeamNo = Convert.ToString(dr["SeamNo"]),
                            SeamCategory = Convert.ToString(dr["SeamCategoryDesc"]),
                            NozzleSize = Convert.ToString(dr["NozzleSize"]),
                            SeamSubCategory = Convert.ToString(dr["NozzleCategoryDesc"]),
                            SeamJointType = Convert.ToString(dr["SeamJointType"]),
                            SeamLength = Convert.ToString(dr["SeamLength"]),
                            Part1Position = Convert.ToString(dr["Part1Position"]),
                            Part2Position = Convert.ToString(dr["Part2Position"]),
                            Part3Position = Convert.ToString(dr["Part3Position"]),
                            Part1Thickness = Convert.ToString(dr["Part1Thickness"]),
                            Part2Thickness = Convert.ToString(dr["Part2Thickness"]),
                            Part3Thickness = Convert.ToString(dr["Part3Thickness"]),
                            MinThickness = Convert.ToString(dr["MinThickness"]),
                            MeasuredValue = Convert.ToString(dr["Unit"]),
                            WeldingProcess = Convert.ToString(dr["WeldingProcess"]),
                            SetupRate = Convert.ToString(dr["SetupRate"]),
                            WeldingRate = Convert.ToString(dr["WeldingRate"]),
                            CriticalSeam = Convert.ToString(dr["CriticalSeamDesc"]),
                            Factor = Convert.ToString(dr["Factor"]),
                            Amount = Convert.ToString(dr["Amount"])
                        });
                    }

                }
                else
                {
                    string whereCondition = " 1=1 and HeaderId=" + HeaderId;
                    lstSeamLines = db.SP_CJC_GET_CONTRACT_PLAN_SEAM_LINE_DETAILS(0, 0, "", whereCondition, Project, Location).ToList();

                     newlst = (from uc in lstSeamLines
                                  select new CJCSeamExportExcel
                                  {
                                      QualityProjectNo = Convert.ToString(uc.QualityProject),
                                      SeamNo = Convert.ToString(uc.SeamNo),
                                      SeamCategory = Convert.ToString(uc.SeamCategoryDesc),
                                      NozzleSize = Convert.ToString(uc.NozzleSize),
                                      SeamSubCategory = Convert.ToString(uc.NozzleCategoryDesc),
                                      SeamJointType = Convert.ToString(uc.SeamJointType),
                                      SeamLength = Convert.ToString(uc.SeamLength),
                                      Part1Position = Convert.ToString(uc.Part1Position),
                                      Part2Position = Convert.ToString(uc.Part2Position),
                                      Part3Position = Convert.ToString(uc.Part3Position),
                                      Part1Thickness = Convert.ToString(uc.Part1Thickness),
                                      Part2Thickness = Convert.ToString(uc.Part2Thickness),
                                      Part3Thickness = Convert.ToString(uc.Part3Thickness),
                                      MinThickness = Convert.ToString(uc.MinThickness),
                                      MeasuredValue = Convert.ToString(uc.Unit),
                                      WeldingProcess = Convert.ToString(uc.WeldingProcess),
                                      SetupRate = Convert.ToString(uc.SetupRate),
                                      WeldingRate = Convert.ToString(uc.WeldingRate),
                                      CriticalSeam = Convert.ToString(uc.CriticalSeamDesc),
                                      Factor = Convert.ToString(uc.Factor),
                                      Amount = Convert.ToString(uc.Amount)
                                  }).ToList();
                }

                
                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public bool EnableDisableWeldingProcessSelection(string HeaderCJCNo, string HeaderCJCStatus, string SetUpCJCNo, string WeldCJCNo, string SeamType)
        {
            bool IsDisabled = true;
            if (string.IsNullOrWhiteSpace(HeaderCJCNo))
            {
                if (arrSeamTypeWp.Contains(SeamType))
                    IsDisabled = false;
            }
            else
            {
                if (arrSeamTypeWp.Contains(SeamType))
                {
                    if (string.IsNullOrWhiteSpace(SetUpCJCNo) && string.IsNullOrWhiteSpace(WeldCJCNo))
                    {
                        IsDisabled = false;
                    }
                    else
                    {
                        if (HeaderCJCNo == SetUpCJCNo || HeaderCJCNo == WeldCJCNo)
                        {
                            IsDisabled = false;
                        }
                    }
                }
            }
            return IsDisabled;
        }

        public bool EnableDisableNozzleCategorySelection(string HeaderCJCNo, string HeaderCJCStatus, string SetUpCJCNo, string WeldCJCNo, string SeamType)
        {
            bool IsDisabled = true;
            if (string.IsNullOrWhiteSpace(HeaderCJCNo))
            {
                if (arrSeamTypeNW.Contains(SeamType) || arrSeamTypeNC2.Contains(SeamType))
                    IsDisabled = false;
            }
            else
            {
                if (arrSeamTypeNW.Contains(SeamType) || arrSeamTypeNC2.Contains(SeamType))
                {
                    if (string.IsNullOrWhiteSpace(SetUpCJCNo) && string.IsNullOrWhiteSpace(WeldCJCNo))
                    {
                        IsDisabled = false;
                    }
                    else
                    {
                        if (HeaderCJCNo == SetUpCJCNo || HeaderCJCNo == WeldCJCNo)
                        {
                            IsDisabled = false;
                        }
                    }
                }
            }
            return IsDisabled;
        }

        public bool EnableDisableCriticalSeamSelection(string HeaderCJCNo, string HeaderCJCStatus, string SetUpCJCNo, string WeldCJCNo)
        {
            bool IsDisabled = true;
            if (string.IsNullOrWhiteSpace(HeaderCJCNo))
            {
                IsDisabled = false;
            }
            else
            {
                if (string.IsNullOrWhiteSpace(SetUpCJCNo) && string.IsNullOrWhiteSpace(WeldCJCNo))
                {
                    IsDisabled = false;
                }
                else
                {
                    if (HeaderCJCNo == SetUpCJCNo || HeaderCJCNo == WeldCJCNo)
                    {
                        IsDisabled = false;
                    }
                }
            }
            return IsDisabled;
        }

        public DataTable MergeEditLineDataWithMainDT(DataTable dtSeamLines, int HeaderId)
        {
            int CurrentRowNo = 1;
            if (dtSeamLines.Rows.Count > 0)
                CurrentRowNo = dtSeamLines.Rows.Count + 1;

            if (HeaderId > 0)
            {
                CJC030 objCJC030 = db.CJC030.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objCJC030 != null)
                {
                    string whereCondition = " 1=1 and HeaderId=" + HeaderId;
                    var lstLines = db.SP_CJC_GET_CONTRACT_PLAN_SEAM_LINE_DETAILS(0, 0, "", whereCondition, objCJC030.Project, objCJC030.Location).ToList();
                    int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();
                    foreach (var item in lstLines)
                    {
                        DataRow drExisting = (from u in dtSeamLines.AsEnumerable()
                                              where Convert.ToString(u["QualityProject"]) == item.QualityProject && Convert.ToString(u["SeamNo"]) == item.SeamNo
                                              select u).FirstOrDefault();
                        if (drExisting != null)
                        {
                            drExisting["EditLineId"] = item.LineId;
                            dtSeamLines.AcceptChanges();
                        }
                        else
                        {
                            DataRow drNewRow = dtSeamLines.NewRow();
                            drNewRow["EditLineId"] = item.LineId;
                            drNewRow["ROW_NO"] = CurrentRowNo;

                            drNewRow["Project"] = objCJC030.Project;
                            drNewRow["HeaderId"] = HeaderId;
                            drNewRow["QualityProject"] = item.QualityProject;
                            drNewRow["SeamNo"] = item.SeamNo;
                            drNewRow["SetupCJCNo"] = item.SetupCJCNo;
                            drNewRow["WeldCJCNo"] = item.WeldCJCNo;
                            drNewRow["SeamCategory"] = item.SeamCategory;
                            drNewRow["SeamCategoryDesc"] = item.SeamCategoryDesc;

                            drNewRow["NozzleSize"] = item.NozzleSize;
                            drNewRow["NozzleCategory"] = item.NozzleCategory;
                            drNewRow["NozzleCategoryDesc"] = item.NozzleCategoryDesc;

                            drNewRow["SeamJointType"] = item.SeamJointType;
                            drNewRow["SeamLength"] = item.SeamLength;

                            drNewRow["Part1Position"] = item.Part1Position;
                            drNewRow["Part2Position"] = item.Part2Position;
                            drNewRow["Part3Position"] = item.Part3Position;

                            drNewRow["Part1Thickness"] = item.Part1Thickness;
                            drNewRow["Part2Thickness"] = item.Part2Thickness;
                            drNewRow["Part3Thickness"] = item.Part3Thickness;

                            drNewRow["MinThickness"] = item.MinThickness;
                            drNewRow["Unit"] = item.Unit;
                            drNewRow["WeldingProcess"] = item.WeldingProcess;
                            drNewRow["WeldingProcessDesc"] = item.WeldingProcessDesc;

                            drNewRow["SetupRate"] = item.SetupRate;
                            drNewRow["WeldingRate"] = item.WeldingRate;
                            drNewRow["CriticalSeam"] = item.CriticalSeam;
                            drNewRow["CriticalSeamDesc"] = item.CriticalSeamDesc;

                            drNewRow["Factor"] = item.Factor;
                            drNewRow["Amount"] = item.Amount;

                            drNewRow["Revision"] = item.Revision;
                            drNewRow["Status"] = item.Status;
                            drNewRow["IsSetUpCleared"] = item.IsSetUpCleared;
                            drNewRow["IsPWHTCleared"] = item.IsPWHTCleared;

                            drNewRow["Shop"] = item.Shop;
                            drNewRow["SetupCJCStatus"] = item.SetupCJCStatus;
                            drNewRow["WeldCJCStatus"] = item.WeldCJCStatus;
                            drNewRow["IsSeamDisabled"] = true;

                            drNewRow["SeamType"] = item.SeamType;
                            drNewRow["IsSeamFetchedBySeamType"] = item.IsSeamFetchedBySeamType;

                            dtSeamLines.Rows.Add(drNewRow);

                            dtSeamLines.AsEnumerable().ToList<DataRow>().ForEach(r =>
                            {
                                r["TotalCount"] = CurrentRowNo;
                            });

                            CurrentRowNo++;
                        }
                    }
                }
            }
            return dtSeamLines;
        }

        [HttpPost]
        public ActionResult EditSeamLineData(CJC031 cjc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                double ClaimAmount = 0;
                double? Factor = 0;
                DataTable dt = (DataTable)TempData["FetchSeamList"];
                int? rowid = cjc.LineId;
                if (dt != null)
                {
                    DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                  where Convert.ToInt32(u["ROW_NO"]) == rowid
                                  select u).FirstOrDefault();

                    if (dr != null)
                    {
                        //changes the Column Value
                        dr["SeamCategory"] = cjc.SeamCategory;
                        dr["NozzleSize"] = cjc.NozzleSize;
                        dr["NozzleCategory"] = cjc.NozzleCategory;
                        dr["WeldingProcess"] = cjc.WeldingProcess;

                        dr["SeamCategoryDesc"] = !string.IsNullOrWhiteSpace(cjc.SeamCategory) ? GetCategory("Seam Category", cjc.SeamCategory).CategoryDescription : "";
                        dr["NozzleCategoryDesc"] = !string.IsNullOrWhiteSpace(cjc.NozzleCategory) ? GetCategory("Nozzle Category", cjc.NozzleCategory).CategoryDescription : "";
                        dr["WeldingProcessDesc"] = !string.IsNullOrWhiteSpace(cjc.WeldingProcess) ? GetCategory("Welding Process", cjc.WeldingProcess).CategoryDescription : "";

                        dr["SetupRate"] = cjc.SetupRate;
                        dr["WeldingRate"] = cjc.WeldingRate;

                        dr["CriticalSeamDesc"] = !string.IsNullOrWhiteSpace(cjc.CriticalSeam) ? GetCategory("Critical Seam", cjc.CriticalSeam).CategoryDescription : "";
                        dr["CriticalSeam"] = cjc.CriticalSeam;

                        Factor = !string.IsNullOrWhiteSpace(cjc.CriticalSeam) ? FetchFactorData(cjc.CriticalSeam) : 0;
                        if (Factor > 0)
                        {
                            dr["Factor"] = Factor;
                        }
                        else
                        {
                            dr["Factor"] = 1;
                            Factor = 1;
                        }
                        dr["Unit"] = cjc.Unit;
                        ClaimAmount = claimSeamAmount(0, cjc.SeamCategory, Convert.ToInt32(Convert.ToString(dr["Seamlength"])), Convert.ToDouble(cjc.Unit), Convert.ToDouble(cjc.WeldingRate), Convert.ToDouble(Factor), Convert.ToDouble(cjc.SetupRate), Convert.ToDouble(cjc.NozzleSize));
                        if (ClaimAmount > 0)
                        {
                            dr["Amount"] = ClaimAmount;
                        }
                        else { dr["Amount"] = null; }
                    }
                    dt.AcceptChanges();
                }
                RetenTempDataValue();
                objResponseMsg.Key = true;
                objResponseMsg.ItemNumber1 = Factor.ToString();
                objResponseMsg.ItemNumber2 = ClaimAmount.ToString();
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult EditSeamLineUtilityData(CJC031 cjc)
        {
            ResponceMsgWithCJC objResponseMsg = new ResponceMsgWithCJC();
            try
            {
                double ClaimAmount = 0;
                double? Factor = 0;
                int? LineId = cjc.LineId;
                CJC031 objCJC031 = db.CJC031.Where(x => x.LineId == cjc.LineId).FirstOrDefault();
                if (objCJC031 != null)
                {
                    //changes the Column Value
                    objCJC031.SeamCategory = cjc.SeamCategory;
                    objCJC031.NozzleSize = cjc.NozzleSize;
                    objCJC031.NozzleCategory = cjc.NozzleCategory;
                    objCJC031.WeldingProcess = cjc.WeldingProcess;
                    objCJC031.SetupRate = cjc.SetupRate;
                    objCJC031.WeldingRate = cjc.WeldingRate;
                    objCJC031.CriticalSeam = cjc.CriticalSeam;

                    Factor = !string.IsNullOrWhiteSpace(cjc.CriticalSeam) ? FetchFactorData(cjc.CriticalSeam) : 0;
                    if (Factor > 0)
                    {
                        objCJC031.Factor = Factor;
                    }
                    else
                    {
                        objCJC031.Factor = null;
                        Factor = null;
                    }
                    objCJC031.Unit = cjc.Unit;
                    if (cjc.WeldingRate > 0 && Factor > 0)
                    {
                        ClaimAmount = claimSeamAmount(0, cjc.SeamCategory, Convert.ToInt32(objCJC031.SeamLength.ToString()), Convert.ToDouble(cjc.Unit), Convert.ToDouble(cjc.WeldingRate), Convert.ToDouble(Factor), Convert.ToDouble(cjc.SetupRate), Convert.ToDouble(cjc.NozzleSize));
                    }
                    if (ClaimAmount > 0)
                    {
                        objCJC031.Amount = ClaimAmount;
                    }
                    else { objCJC031.Amount = null; }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.ItemNumber1 = Factor.ToString();
                objResponseMsg.ItemNumber2 = ClaimAmount.ToString();
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult DeleteDTSeamLineData(int rowid, int editlineid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                DataTable dt = (DataTable)TempData["FetchSeamList"];
                if (dt != null)
                {
                    DataRow dr = (from u in dt.AsEnumerable()
                                  where Convert.ToInt32(u["ROW_NO"]) == rowid
                                  select u).FirstOrDefault();
                    if (dr != null)
                    {
                        dr.Delete();

                        if (editlineid > 0)
                        {
                            CJC031 objCJC031 = db.CJC031.Where(x => x.LineId == editlineid).FirstOrDefault();
                            if (objCJC031 != null)
                            {
                                db.CJC031.Remove(objCJC031);
                                db.SaveChanges();

                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete;
                        }
                    }
                    dt.AcceptChanges();
                }
                RetenTempDataValue();
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        public ActionResult LoadSeamLineGridDataTable(JQueryDataTableParamModel param, int HeaderId)
        {
            try
            {
                string HeaderCJCStatus = !string.IsNullOrWhiteSpace(param.CJCNo) ? Manager.GetAutoCJCStatus(param.CJCNo) : "";

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                bool IsEditable = param.Flag;
                string whereCondition = " 1=1 and HeaderId=" + HeaderId;

                string[] columnName = { "QualityProject","SeamNo"
                                ,"SetupCJCNo"
                                ,"WeldCJCNo"
                                ,"SeamCategory"
                                ,"SeamCategoryDesc"
                                ,"NozzleSize"
                                ,"NozzleCategory"
                                ,"NozzleCategoryDesc"
                                ,"SeamJointType"
                                ,"SeamLength"
                                ,"Part1Position"
                                ,"Part2Position"
                                ,"Part3Position"
                                ,"Part1Thickness"
                                ,"Part2Thickness"
                                ,"Part3Thickness"
                                ,"MinThickness"
                                ,"Unit"
                                ,"WeldingProcess"
                                ,"WeldingProcessDesc"
                                ,"SetupRate"
                                ,"WeldingRate"
                                ,"CriticalSeamDesc"
                                ,"Factor"
                                ,"Amount"
                };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string CJCNo = param.CJCNo;
                var lstLines = db.SP_CJC_GET_CONTRACT_PLAN_SEAM_LINE_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition, param.Project, param.Location).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                    Convert.ToString(c.ROW_NO),
                                    Helper.GenerateHidden(c.LineId, "LineId", Convert.ToString(c.LineId)),
                                    Helper.GenerateHidden(c.LineId, "HeaderId",Convert.ToString(c.HeaderId)),
                                    Convert.ToString(c.QualityProject),
                                    Convert.ToString(c.SeamNo),
                                    "",
                                    WebUtility.HtmlEncode(c.SeamCategoryDesc)+""+Helper.GenerateHidden(c.LineId, "SeamCategory",c.SeamCategory),
                                    c.NozzleSize.HasValue ? Convert.ToString(c.NozzleSize) : "",// Helper.GenerateTextbox(c.LineId, "NozzleSize", Convert.ToString(c.NozzleSize),"",!arrSeamTypeNW.Contains(Convert.ToString(c.SeamType)), "","","clsMandatory", false,""),
                                    IsEditable ?  Helper.HTMLAutoComplete(c.LineId, "txtNozzleCategory",(arrSeamTypeNW.Contains(c.SeamType) || arrSeamTypeNC2.Contains(c.SeamType)) ? c.NozzleCategoryDesc : "","", false,"","NozzleCategory",EnableDisableNozzleCategorySelection(CJCNo,HeaderCJCStatus,c.SetupCJCNo,c.WeldCJCNo,c.SeamType),"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "NozzleCategory",(arrSeamTypeNW.Contains(c.SeamType) || arrSeamTypeNC2.Contains(c.SeamType)) ? Convert.ToString(c.NozzleCategory) : ""):Convert.ToString(c.NozzleCategoryDesc),
                                    c.SeamJointType,
                                    Convert.ToString(c.SeamLength),
                                    c.Part1Position,
                                    c.Part2Position,
                                    c.Part3Position,
                                    c.Part1Thickness,
                                    c.Part2Thickness,
                                    c.Part3Thickness,
                                    !string.IsNullOrWhiteSpace(Convert.ToString(c.MinThickness)) ? Convert.ToString(c.MinThickness) : "",
                                    Convert.ToString(c.Unit),
                                    (arrSeamTypeWp.Contains(c.SeamType) ? Convert.ToString(c.WeldingProcess): string.Empty) + Helper.GenerateHidden(c.LineId, "WeldingProcess",arrSeamTypeWp.Contains(c.SeamType) ? c.WeldingProcess : ""),//IsEditable ? Helper.HTMLAutoComplete(c.LineId, "txtWeldingProcess",arrSeamTypeWp.Contains(c.SeamType) ? c.WeldingProcessDesc : "","", false,"","WeldingProcess",EnableDisableWeldingProcessSelection(CJCNo,HeaderCJCStatus,c.SetupCJCNo,c.WeldCJCNo,c.SeamType),"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "WeldingProcess",arrSeamTypeWp.Contains(c.SeamType) ? c.WeldingProcess : "") : c.WeldingProcessDesc,
                                    Convert.ToString(c.SetupRate),
                                    Convert.ToString(c.WeldingRate),
                                    IsEditable ? Helper.HTMLAutoComplete(c.LineId, "txtCriticalSeam",c.CriticalSeamDesc,"", false,"","CriticalSeam",EnableDisableCriticalSeamSelection(CJCNo,HeaderCJCStatus,c.SetupCJCNo,c.WeldCJCNo),"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "CriticalSeam",c.CriticalSeam) : c.CriticalSeamDesc,
                                    Convert.ToString(c.Factor),
                                    IsEditable ? Helper.GenerateLabelFor(c.LineId,"Amount", Convert.ToString(c.Amount),"","clsAmount") : Convert.ToString(c.Amount),
                                    !string.IsNullOrWhiteSpace(c.SetupCJCNo) ? c.SetupCJCNo : "",
                                    !string.IsNullOrWhiteSpace(c.WeldCJCNo) ? c.WeldCJCNo : "",
                                    "",
                                    Convert.ToString(c.IsSetUpCleared),
                                    Convert.ToString(c.IsPWHTCleared),
                                    "",
                                    c.SetupCJCStatus,
                                    c.WeldCJCStatus,
                                    c.SeamType,
                                    Convert.ToString(c.SeamQuantity),
                                    Helper.HTMLActionString(c.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteRowDatatable(this,"+c.LineId+","+c.LineId+");","", !IsEditable ),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Project Details

        public ActionResult LoadProjectLineGridDataPartial()
        {
            ViewBag.ProjectActivityList = (from cjc012 in db.CJC012
                                           join glb002 in db.GLB002 on cjc012.ProjectActivity equals glb002.Code
                                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                           where glb001.Category.Equals("Project Activity", StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true
                                           select glb002).Select(i => new { Key = i.Code, Value = i.Description }).Distinct().ToList();

            return PartialView("_LoadProjectLineGridDataPartial");
        }

        public ActionResult LoadFetchProjectLineGridDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1 AND HeaderId=" + (!string.IsNullOrWhiteSpace(param.Headerid) ? Convert.ToInt32(param.Headerid) : 0);
                DataTable dtProjectLines = new DataTable();
                if (TempData["ProjectLines"] == null)
                {
                    var lstLines = db.SP_CJC_GET_CONTRACT_PLAN_PROJECT_LINE_DETAILS(0, 0, strSortOrder, whereCondition).ToList();
                    dtProjectLines = Helper.ToDataTable(lstLines);
                    TempData["ProjectLines"] = dtProjectLines;
                }
                else
                {
                    dtProjectLines = (DataTable)TempData["ProjectLines"];
                }

                string searchExpression = "1=1 ";
                string sortExpression = "";

                string[] columnName = { "ActivityDesc", "Convert(Quantity,System.String)", "UOM", "Convert(Rate, System.String)", "Convert(Amount, System.String)", "CJCNo" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    searchExpression += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    searchExpression += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    sortExpression = sortColumnName + " " + sortDirection + " ";
                }
                string Contingency_amount = clsImplementationEnum.AutoCJCProjectActivity.Contingency_amount.GetStringValue();
                DataView dataView = dtProjectLines.AsDataView();
                if (!string.IsNullOrWhiteSpace(searchExpression))
                    dataView.RowFilter = searchExpression;
                if (!string.IsNullOrWhiteSpace(sortExpression))
                    dataView.Sort = sortExpression;

                int? totalRecords = dataView.ToTable().Rows.Count;

                List<DataRow> filteredList = dataView.ToTable().Select().Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();

                var res = (from c in filteredList
                           select new[] {
                                    Convert.ToString(c["ROW_NO"]),
                                    Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "Id", ""),
                                    Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "HeaderId", Convert.ToString(HeaderId)),
                                    Helper.HTMLAutoComplete(Convert.ToInt32(c["ROW_NO"]), "txtActivity",Convert.ToString(c["ActivityDesc"]),"", false,"width:420px;","Activity",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(Convert.ToInt32(c["ROW_NO"]), "Activity",Convert.ToString(c["Activity"].ToString())),
                                    Helper.GenerateNumericTextbox(Convert.ToInt32(c["ROW_NO"]), "txtQuantity",Convert.ToString(c["Quantity"]), "",false, "", ""),
                                    Helper.GenerateLabelFor(Convert.ToInt32(c["ROW_NO"]),"UOM",Convert.ToString(c["UOM"]),""," clsProjUOM"),
                                    Helper.GenerateLabelFor(Convert.ToInt32(c["ROW_NO"]),"Rate",Convert.ToString(c["Rate"]),""," clsProjRate"),
                                    Helper.GenerateLabelFor(Convert.ToInt32(c["ROW_NO"]),"ProjAmount",Convert.ToString(c["Amount"]),""," clsProjAmount"),
                                    Helper.GenerateTextArea(Convert.ToInt32(c["ROW_NO"]),"Remarks",Convert.ToString(c["Remarks"]),"EditRemarkRow("+Convert.ToInt32(c["ROW_NO"])+")",false ,"height: 33px !important;","200"," clsProjRemarks"),
                                    Helper.GenerateLabelFor(Convert.ToInt32(c["ROW_NO"]),"CJCNo",Convert.ToString(c["CJCNo"]),""," clsProjCJCNo"),
                                    Helper.HTMLActionString(Convert.ToInt32(c["ROW_NO"]),"Delete","Delete Record","fa fa-trash-o","DeleteActivityRowDatatable(this,"+Convert.ToInt32(c["ROW_NO"]) +");"),
                          }).ToList();

                TempData["ProjectLines"] = dtProjectLines;
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult LoadProjectLineGridDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1 and HeaderId=" + (!string.IsNullOrWhiteSpace(param.Headerid) ? Convert.ToInt32(param.Headerid) : 0);
                string[] columnName = { "ActivityDesc", "Quantity", "UOM", "Rate", "Amount", "CJCNo" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }
                bool IsEditable = param.Flag;
                var lstLines = db.SP_CJC_GET_CONTRACT_PLAN_PROJECT_LINE_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();
                string Contingency_amount = clsImplementationEnum.AutoCJCProjectActivity.Contingency_amount.GetStringValue();
                var res = (from c in lstLines
                           select new[] {
                                    Convert.ToString(c.ROW_NO),
                                    Helper.GenerateHidden(c.LineId, "Id", Convert.ToString(c.LineId)),
                                    Helper.GenerateHidden(c.LineId, "HeaderId", Convert.ToString(c.HeaderId)),
                                    IsEditable ? Helper.HTMLAutoComplete(c.LineId, "txtActivity",c.ActivityDesc,"", false,"width:420px;","Activity",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "Activity", c.Activity) : c.ActivityDesc,
                                    IsEditable ? Helper.GenerateNumericTextbox(c.LineId, "txtQuantity",Convert.ToString(c.Quantity), "",false, "", ""):Convert.ToString(c.Quantity),
                                    Helper.GenerateLabelFor(c.LineId,"UOM",Convert.ToString(c.UOM),""," clsProjUOM"),
                                    Helper.GenerateLabelFor(c.LineId,"Rate",Convert.ToString(c.Rate),""," clsProjRate"),
                                    Helper.GenerateLabelFor(c.LineId,"ProjAmount",Convert.ToString(c.Amount),""," clsProjAmount"),
                                    IsEditable ? Helper.GenerateTextArea(c.LineId,"Remarks",Convert.ToString(c.Remarks),"EditRemarkRow("+Convert.ToInt32(c.LineId)+")",false,"height: 33px !important;","200"," clsProjRemarks") : Convert.ToString(c.Remarks),
                                    Helper.GenerateLabelFor(c.LineId,"CJCNo",Convert.ToString(c.CJCNo),""," clsProjCJCNo"),
                                    Helper.HTMLActionString(c.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteActivityRowDatatable(this,"+c.LineId +");","",!IsEditable),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetProjectDetailInlineEditableRow(int id, int HeaderId = 0, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "Id", Convert.ToString(id)),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                                    Helper.HTMLAutoComplete(newRecordId, "txtActivity","","", false,"","Activity",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(newRecordId, "Activity",Convert.ToString("")),
                                    Helper.GenerateNumericTextbox(newRecordId, "txtQuantity","1", "", false, "", ""),
                                    Helper.GenerateLabelFor(newRecordId,"UOM","",""," clsProjUOM"),
                                    Helper.GenerateLabelFor(newRecordId,"Rate","",""," clsProjRate"),
                                    Helper.GenerateLabelFor(newRecordId,"ProjAmount","",""," clsProjAmount"),
                                    Helper.GenerateTextArea(newRecordId,"Remarks","","",false,"height: 33px !important;","200",""," clsProjRemarks"),
                                    Helper.GenerateLabelFor(newRecordId,"CJCNo","",""," clsProjCJCNo"),
                                    Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveActivityRecord(0);"),
                    };
                    data.Add(newRecord);
                }

                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetProjectActivityData(string ProjectActivity, int Jobdia, int LineId, string Project, int headerid)
        {
            double? Rate = 0;
            string UOM = string.Empty;
            CJC012 obj = new CJC012();
            bool isActivityexists = (from a in db.CJC030
                                     join b in db.CJC032 on a.HeaderId equals b.HeaderId
                                     where a.Project == Project && b.LineId != LineId && b.Activity == ProjectActivity
                                     select b.LineId).Any();
            bool isActivityAlreadyexists = (from a in db.CJC030
                                            join b in db.CJC032 on a.HeaderId equals b.HeaderId
                                            where a.Project == Project && b.LineId != LineId && b.HeaderId == headerid && b.Activity == ProjectActivity
                                            select b.LineId).Any();
            if (!isActivityexists)
            {
                obj = db.CJC012.Where(x => x.ProjectActivity == ProjectActivity && x.Dia == Jobdia).FirstOrDefault();
                if (obj != null)
                {
                    UOM = obj.UOM;
                    Rate = obj.Rate;
                }
            }

            var objData = new
            {
                isActivityAlreadyexists = isActivityAlreadyexists,
                isActivityexists = isActivityexists,
                UOM = UOM,
                Rate = Rate
            };
            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveProjectLineData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (fc != null)
                {
                    int HeaderId = Convert.ToInt32(fc["HeaderId"]);
                    int LineId = Convert.ToInt32(fc["LineId"]);

                    string Activity = fc["Activity" + LineId];
                    string UOM = fc["txtUOM" + LineId];
                    string CJCNo = fc["txtCJCNo" + LineId];
                    string Remarks = fc["Remarks" + LineId];

                    double? Quantity = null;
                    if (fc["txtQuantity" + LineId] != null && fc["txtQuantity" + LineId] != "")
                        Quantity = Convert.ToDouble(fc["txtQuantity" + LineId]);

                    double? Rate = null;
                    if (fc["txtRate" + LineId] != null && fc["txtRate" + LineId] != "")
                        Rate = Convert.ToDouble(fc["txtRate" + LineId]);

                    double? Amount = null;
                    if (fc["txtAmount" + LineId] != null && fc["txtAmount" + LineId] != "")
                        Amount = Convert.ToDouble(fc["txtAmount" + LineId]);


                    CJC032 objCJC032 = null;
                    if (LineId > 0)
                    {
                        objCJC032 = db.CJC032.Where(x => x.LineId == LineId).FirstOrDefault();
                        if (objCJC032 != null)
                        {
                            if (!db.CJC032.Any(x => x.HeaderId == HeaderId && x.Activity == Activity && x.LineId != LineId))
                            {
                                objCJC032.Activity = Activity;
                                objCJC032.Quantity = Quantity;
                                objCJC032.UOM = UOM;
                                objCJC032.Rate = Rate;
                                objCJC032.Amount = Amount;
                                objCJC032.Remarks = Remarks;
                                objCJC032.EditedBy = objClsLoginInfo.UserName;
                                objCJC032.EditedOn = DateTime.Now;

                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
                        }
                    }
                    else
                    {
                        if (!db.CJC032.Any(x => x.HeaderId == HeaderId && x.Activity == Activity))
                        {
                            objCJC032 = new CJC032();

                            objCJC032.HeaderId = HeaderId;
                            objCJC032.Activity = Activity;
                            objCJC032.Quantity = Quantity;
                            objCJC032.UOM = UOM;
                            objCJC032.Rate = Rate;
                            objCJC032.Amount = Amount;
                            objCJC032.Remarks = Remarks;

                            objCJC032.CreatedBy = objClsLoginInfo.UserName;
                            objCJC032.CreatedOn = DateTime.Now;

                            db.CJC032.Add(objCJC032);

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteProjectLineData(int rowid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                CJC032 objCJC032 = db.CJC032.Where(x => x.LineId == rowid).FirstOrDefault();
                if (objCJC032 != null)
                {
                    db.CJC032.Remove(objCJC032);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveActivityLineData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                DataTable dt = TempData["ProjectLines"] as DataTable;
                int? rowid = Convert.ToInt32(fc["MainId"]);
                var strdtSort = " ROW_NO DESC";
                var strWhere = " Activity ='" + fc["Activity" + rowid] + "'";
                DataRow drsr = dt.Select("", strdtSort).FirstOrDefault();
                DataRow dr = dt.NewRow();
                if (!dt.Select(strWhere).Any())
                {
                    // insert in the desired place
                    dr["ROW_NO"] = dt.Rows.Count + 1;
                    dr["TotalCount"] = dt.Rows.Count + 1;
                    dr["Activity"] = fc["Activity" + rowid];
                    dr["ActivityDesc"] = !string.IsNullOrWhiteSpace(fc["Activity" + rowid]) ? GetCategory("Project Activity", fc["Activity" + rowid]).CategoryDescription : "";
                    dr["Quantity"] = fc["txtQuantity" + rowid];
                    dr["UOM"] = fc["UOM" + rowid];
                    dr["Remarks"] = fc["Remarks" + rowid];
                    dr["Rate"] = !string.IsNullOrWhiteSpace(fc["Rate" + rowid]) ? Convert.ToDouble(fc["Rate" + rowid]) : 0;
                    dr["Amount"] = !string.IsNullOrWhiteSpace(fc["Amount" + rowid]) ? Convert.ToDouble(fc["Amount" + rowid]) : 0;
                    dt.Rows.InsertAt(dr, dt.Rows.Count + 1);
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                }
                objResponseMsg.Key = true;
                TempData["ProjectLines"] = dt;
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult SaveActivityUtilityData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int rowid = 0;
                int HeaderId = Convert.ToInt32(fc["HeaderId"]);
                string Activity = fc["Activity" + rowid];
                double? Quantity = null;
                if (fc["txtQuantity" + rowid] != null)
                    Quantity = Convert.ToDouble(fc["txtQuantity" + rowid]);

                string UOM = fc["UOM" + rowid];

                double? Rate = null;
                if (fc["Rate" + rowid] != null)
                    Rate = Convert.ToDouble(fc["Rate" + rowid]);

                double? Amount = null;
                if (fc["Amount" + rowid] != null)
                    Amount = Convert.ToDouble(fc["Amount" + rowid]);
                string Remarks = fc["Remarks" + rowid];
                if (!db.CJC032.Any(x => x.HeaderId == HeaderId && x.Activity == Activity))
                {
                    CJC032 objCJC032 = new CJC032();
                    objCJC032.HeaderId = HeaderId;
                    objCJC032.Activity = Activity;
                    objCJC032.Quantity = Quantity;
                    objCJC032.UOM = UOM;
                    objCJC032.Rate = Rate;
                    objCJC032.Amount = Amount;
                    objCJC032.Remarks = Remarks;
                    objCJC032.CreatedBy = objClsLoginInfo.UserName;
                    objCJC032.CreatedOn = DateTime.Now;
                    db.CJC032.Add(objCJC032);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult EditActivityLineData(CJC032 cjc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                DataTable dt = (DataTable)TempData["ProjectLines"];
                int? rowid = cjc.LineId;
                if (dt != null)
                {
                    DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                  where Convert.ToInt32(u["ROW_NO"]) == rowid
                                  select u).FirstOrDefault();

                    if (dr != null)
                    {
                        var recExistCount = (from u in dt.AsEnumerable()
                                             where Convert.ToInt32(u["ROW_NO"]) != rowid && Convert.ToString(u["Activity"]) == cjc.Activity
                                             select u).Count();

                        if (recExistCount <= 0)
                        {
                            dr["Activity"] = cjc.Activity;
                            dr["ActivityDesc"] = !string.IsNullOrWhiteSpace(cjc.Activity) ? GetCategory("Project Activity", cjc.Activity).CategoryDescription : "";
                            dr["Quantity"] = cjc.Quantity;
                            dr["UOM"] = cjc.UOM;
                            dr["Rate"] = cjc.Rate;
                            dr["Amount"] = cjc.Amount;
                            dr["Remarks"] = cjc.Remarks;
                        }
                        //else
                        //{
                        //    objResponseMsg.Key = false;
                        //    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                        //}
                    }
                    objResponseMsg.Key = true;
                    dt.AcceptChanges();
                    TempData["ProjectLines"] = dt;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult EditActivityUtilityData(CJC032 cjc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                CJC032 objCJC032 = db.CJC032.Where(x => x.LineId == cjc.LineId).FirstOrDefault();
                if (objCJC032 != null)
                {
                    if (!db.CJC032.Any(x => x.HeaderId == objCJC032.HeaderId && x.LineId != cjc.LineId && x.Activity == cjc.Activity))
                    {
                        objCJC032.Activity = cjc.Activity;
                        objCJC032.Quantity = cjc.Quantity;
                        objCJC032.UOM = cjc.UOM;
                        objCJC032.Rate = cjc.Rate;
                        objCJC032.Amount = cjc.Amount;
                        objCJC032.Remarks = cjc.Remarks;
                        objCJC032.EditedBy = objClsLoginInfo.UserName;
                        objCJC032.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult EditActivityRemarksLineData(int lineid, string Remarks)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                DataTable dt = (DataTable)TempData["ProjectLines"];
                int? rowid = lineid;
                if (dt != null)
                {
                    DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                  where Convert.ToInt32(u["ROW_NO"]) == rowid
                                  select u).FirstOrDefault();

                    if (dr != null)
                    {
                        dr["Remarks"] = Remarks;
                    }
                    objResponseMsg.Key = true;
                    dt.AcceptChanges();
                    TempData["ProjectLines"] = dt;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult EditActivityRemarksUtilityData(int lineid, string Remarks)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                CJC032 objCJC032 = db.CJC032.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objCJC032 != null)
                {
                    objCJC032.Remarks = Remarks;
                    objCJC032.EditedBy = objClsLoginInfo.UserName;
                    objCJC032.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }


        [HttpPost]
        public ActionResult DeleteDTActivityLineData(int rowid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                DataTable dt = (DataTable)TempData["ProjectLines"];
                if (dt != null)
                {
                    DataRow dr = (from u in dt.AsEnumerable()
                                  where Convert.ToInt32(u["ROW_NO"]) == rowid
                                  select u).FirstOrDefault();
                    if (dr != null)
                    {
                        dr.Delete();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete;
                    }
                    dt.AcceptChanges();
                    TempData["ProjectLines"] = dt;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Utility

        public CategoryData GetCategory(string categoryCode, string Code)
        {
            CategoryData objGLB002 = new CategoryData();

            objGLB002 = (from glb002 in db.GLB002
                         join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                         where glb001.Category.Equals(categoryCode, StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(Code) && glb002.IsActive == true
                         select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Description }).FirstOrDefault();
            if (objGLB002 == null)
            {
                objGLB002 = new CategoryData();
                objGLB002.Code = Code;
                objGLB002.CategoryDescription = Code;
            }
            return objGLB002;
        }

        public CategoryData GetCategoryCodeByDescription(string categoryCode, string desc)
        {
            CategoryData objGLB002 = new CategoryData();

            objGLB002 = (from glb002 in db.GLB002
                         join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                         where glb001.Category.Equals(categoryCode, StringComparison.OrdinalIgnoreCase) && glb002.Description.ToLower() == desc.ToLower()// && glb002.IsActive == true
                         select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Description }).FirstOrDefault();
            if (objGLB002 == null)
            {
                objGLB002 = new CategoryData();
                objGLB002.Code = desc;
                objGLB002.CategoryDescription = desc;
            }
            return objGLB002;
        }
        [HttpPost]
        public JsonResult GetInitiatorData(string term, string location)
        {
            string whereCondition = " Location='" + location + "' ";

            if (!string.IsNullOrWhiteSpace(term))
            {
                whereCondition += " and (PsNo like '%" + term + "%' or UPPER(InitiaorName) like '%" + term.ToUpper() + "%')";
            }

            var lstLNInitiator = db.SP_CJC_GET_LN_INITIATOR(1, 10, "", whereCondition).ToList();
            var lstlstInitiator = (from a in lstLNInitiator
                                   select new { Value = a.PsNo, Text = a.InitiaorName }).Distinct().ToList();
            return Json(lstlstInitiator, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetProjectResult(string term)
        {
            List<Projects> lstProjects = new List<Projects>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProjects = (from q1 in db.QMS001
                               join c1 in db.COM001 on q1.Project equals c1.t_cprj
                               where (q1.Project.Contains(term) || c1.t_cprj.Contains(term)) && q1.IsLTFPS != true
                               select new Projects
                               {
                                   Value = q1.Project,
                                   projectCode = q1.Project,
                                   projectDescription = q1.Project + " - " + c1.t_dsca,
                                   Text = q1.Project + " - " + c1.t_dsca
                               }
                              ).Distinct().ToList();
            }
            else
            {
                lstProjects = (from q1 in db.QMS001
                               join c1 in db.COM001 on q1.Project equals c1.t_cprj
                               where q1.Project.Contains(c1.t_cprj) && q1.IsLTFPS != true
                               select new Projects
                               {
                                   Value = q1.Project,
                                   projectCode = q1.Project,
                                   projectDescription = q1.Project + " - " + c1.t_dsca,
                                   Text = q1.Project + " - " + c1.t_dsca
                               }
                              ).Distinct().Take(10).ToList();
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDiaByProjectResult(string project, int HeaderId)
        {
            string dia = string.Empty;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string draft = clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue();
            if (!string.IsNullOrWhiteSpace(project))
            {
                var diaDescription = (from c16 in db.CJC016
                                      join c3 in db.CJC003 on c16.ProductType equals c3.Id
                                      where c16.Project == project
                                      orderby c16.EditedOn descending
                                      select new { id = c3.Id, desc = c3.Description }).FirstOrDefault();
                var objCJC030 = db.CJC030.Where(x => x.Project == project && x.HeaderId != HeaderId && x.Status == draft).FirstOrDefault();
                if (objCJC030 != null)
                {
                    objResponseMsg.HeaderId = objCJC030.HeaderId;
                }
                if (diaDescription != null)
                {
                    objResponseMsg.Value = diaDescription.id + "#" + diaDescription.desc;
                }
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPurchaseOdrerData(string term, string location)
        {
            string whereCondition = " AutoCJCFlag=1";

            if (!string.IsNullOrWhiteSpace(term))
            {
                whereCondition += " and (UPPER(PurchaseOdrerNo) like '%" + term.ToUpper() + "%')";
            }

            var lstPOResult = db.SP_CJC_GET_PO_FOR_AUTOCJC(1, int.MaxValue, "", whereCondition).ToList();
            var lstOrder = db.SP_COMMON_GET_LN_APPROVED_PO(1, int.MaxValue, "", " BP ='" + objClsLoginInfo.UserName + "' and t_loca='" + location + "'").Select(x => x.PONo).ToList();

            var lstPONo = (from a in lstPOResult
                           where lstOrder.Contains(a.PurchaseOdrerNo)
                           select new { Value = a.PurchaseOdrerNo, Text = a.PurchaseOdrerNo }).Take(10).Distinct().ToList();

            return Json(lstPONo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPurchaseOdrerLineData(string term, string pono, string location)
        {
            string whereCondition = " ln.t_orno='" + pono + "' and ln.t_loca=" + location;

            if (!string.IsNullOrWhiteSpace(term))
            {
                whereCondition += " and (ln.pono like '%" + term + "%' or UPPER(ln.t_item) like '%" + term.ToUpper() + "%')";
            }

            var lstPOResult = db.SP_COMMON_GET_LN_PO_LINES(1, 10, "", whereCondition).ToList();
            var lstPOLines = (from a in lstPOResult
                              select new { Value = a.POLineNo, Text = a.item }).Distinct().ToList();
            return Json(lstPOLines, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityProject(string term, string Project)
        {
            List<ddlValue> lstQProject = null;
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstQProject = db.QMS012_Log
                                     .Where(x => x.Project == Project && x.QualityProject.Contains(term))
                                     .Select(x => new ddlValue { Value = x.QualityProject, Text = x.QualityProject }).Distinct().Take(10).ToList();
            }
            else
            {
                lstQProject = db.QMS012_Log
                             .Where(x => x.Project == Project)
                             .Select(x => new ddlValue { Value = x.QualityProject, Text = x.QualityProject }).Distinct().Take(10).ToList();
            }
            return Json(lstQProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSeamFromQualityProject(string term, string qProject)
        {
            string approved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
            List<ddlValue> lstSeam = null;
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstSeam = db.QMS012_Log
                                .Where(x => x.QualityProject == qProject && x.SeamNo.Contains(term) && x.Status == approved)
                                .OrderByDescending(x => x.RefId)
                                .Select(x => new ddlValue { Value = x.SeamNo, Text = x.SeamNo + "-" + x.SeamDescription }).Distinct().Take(10).ToList();
            }
            else
            {
                lstSeam = db.QMS012_Log
                         .Where(x => x.QualityProject == qProject && x.Status == approved)
                         .OrderByDescending(x => x.RefId)
                         .Select(x => new ddlValue { Value = x.SeamNo, Text = x.SeamNo + "-" + x.SeamDescription }).Distinct().Take(10).ToList();
            }
            return Json(lstSeam, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FetchSeamListData(string seamno, string qProject)
        {
            CJC031 objModelCJC031 = SeamData(qProject, seamno);

            return Json(objModelCJC031, JsonRequestBehavior.AllowGet);
        }

        public CJC031 SeamData(string qproject, string seamno)
        {
            CJC031 objFetchCJC031 = new CJC031();
            string approved = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
            var objSeamData = (from q12 in db.QMS012_Log
                               join q11 in db.QMS011_Log on q12.HeaderId equals q11.HeaderId
                               where q11.QualityProject == qproject.Trim() && q12.SeamNo == seamno && q12.Status == approved
                               orderby q11.Id descending
                               select q12).FirstOrDefault();
            if (objSeamData != null)
            {
                objFetchCJC031.MinThickness = 0;
                objFetchCJC031.NozzleSize = (objSeamData.SeamLengthArea / Math.PI) / 25.4;
                objFetchCJC031.SeamJointType = objSeamData.WeldType;
                objFetchCJC031.SeamLength = objSeamData.SeamLengthArea;

                if (!string.IsNullOrWhiteSpace(objSeamData.Position))
                {
                    var lstPosition = objSeamData.Position.Split(',');
                    objFetchCJC031.Part1Position = (lstPosition.Count() > 0) ? objSeamData.Position.Split(',')[0] : "";
                    objFetchCJC031.Part2Position = (lstPosition.Count() > 1) ? objSeamData.Position.Split(',')[1] : "";
                    objFetchCJC031.Part3Position = (lstPosition.Count() > 2) ? objSeamData.Position.Split(',')[2] : "";
                }
                if (!string.IsNullOrWhiteSpace(objSeamData.Thickness))
                {
                    var lstThickness = objSeamData.Thickness.Split(',');
                    List<double> minthickness = new List<double>();
                    objFetchCJC031.Part1Thickness = (lstThickness.Count() > 0) ? objSeamData.Thickness.Split(',')[0] : "";
                    objFetchCJC031.Part2Thickness = (lstThickness.Count() > 1) ? objSeamData.Thickness.Split(',')[1] : "";
                    objFetchCJC031.Part3Thickness = (lstThickness.Count() > 2) ? objSeamData.Thickness.Split(',')[2] : "";
                    if (lstThickness.Count() > 0)
                    {
                        if (lstThickness.Count() > 0)
                            minthickness.Add(Convert.ToDouble(objFetchCJC031.Part1Thickness));
                        if (lstThickness.Count() > 1)
                            minthickness.Add(Convert.ToDouble(objFetchCJC031.Part2Thickness));
                        if (lstThickness.Count() > 2)
                            minthickness.Add(Convert.ToDouble(objFetchCJC031.Part3Thickness));
                    }

                    objFetchCJC031.MinThickness = minthickness.Min(x => x);
                }
            }

            return objFetchCJC031;
        }

        public double? FetchFactorData(string CriticalSeam)
        {
            double? Factor = 1;
            if (!string.IsNullOrWhiteSpace(CriticalSeam))
            {
                var obj = db.CJC014.Where(i => i.CriticalSeam == CriticalSeam).FirstOrDefault();
                Factor = obj?.Factor;
            }
            return Factor;
        }
        #endregion

        #region Seam Utility Grid
        [HttpPost]
        public JsonResult GetSeamDetailInlineEditableRow(int id, int HeaderId = 0, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                          clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                          Helper.GenerateHidden(newRecordId, "LineId", Convert.ToString(id)),
                          Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                          Helper.HTMLAutoComplete(newRecordId, "txtQualityProject","","", false,"","QualityProject",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(newRecordId, "QualityProject",Convert.ToString("")),
                          Helper.HTMLAutoComplete(newRecordId, "txtSeamNo","","", false,"","SeamNo",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(newRecordId, "SeamNo",Convert.ToString("")),
                          "",
                          Helper.HTMLAutoComplete(newRecordId, "txtSeamCategory","","", false,"","SeamCategory",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(newRecordId, "SeamCategory",Convert.ToString("")),
                          Helper.GenerateHTMLTextbox(newRecordId, "NozzleSize",  "", "",true, "", false,"","",""),
                          Helper.HTMLAutoComplete(newRecordId, "txtNozzleCategory","","", false,"","NozzleCategory",false)+""+Helper.GenerateHidden(newRecordId, "NozzleCategory",Convert.ToString("")),
                          Helper.GenerateHTMLTextbox(newRecordId, "SeamJointType",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "SeamLength",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part1Position",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part2Position",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part3Position",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part1Thickness",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part2Thickness",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Part3Thickness",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "MinThickness",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "Unit",  "", "",true, "", false,"","",""),
                          Helper.HTMLAutoComplete(newRecordId, "txtWeldingProcess","","", false,"","WeldingProcess",false)+""+Helper.GenerateHidden(newRecordId, "WeldingProcess",Convert.ToString("")),
                          Helper.GenerateHTMLTextbox(newRecordId, "SetupRate",  "", "",true, "", false,"","",""),
                          Helper.GenerateHTMLTextbox(newRecordId, "WeldingRate",  "", "",true, "", false,"","",""),
                          Helper.HTMLAutoComplete(newRecordId, "txtCriticalSeam","","", false,"","CriticalSeam",false)+""+Helper.GenerateHidden(newRecordId, "CriticalSeam",Convert.ToString("")),
                         Helper.GenerateHTMLTextbox(newRecordId, "Factor",  "1", "",true, "", false,"","",""),
                          Helper.GenerateTextbox(newRecordId, "Amount",  "", "",false, "","","clsMandatory", false,"",""),
                          "",
                          "",
                          "",
                          "",
                          "",
                          "",
                          Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveRecord(0);"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstLines = db.SP_CJC_GET_CONTRACT_PLAN_SEAM_LINE_DETAILS(1, 0, "", "LineId = " + id, "", "").Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(c.LineId, "LineId", Convert.ToString(c.LineId)),
                                    Helper.GenerateHidden(c.LineId, "HeaderId", Convert.ToString(c.HeaderId)),
                                    isReadOnly ? c.QualityProject.ToString():Helper.HTMLAutoComplete(c.LineId, "txtQualityProject",c.QualityProject,"", true,"","QualityProject",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "QualityProject",Convert.ToString(c.QualityProject)),
                                    isReadOnly ? c.SeamNo.ToString():Helper.HTMLAutoComplete(c.LineId, "txtSeamNo",c.SeamNo,"", true,"","SeamNo",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "SeamNo",Convert.ToString(c.SeamNo)),
                                    isReadOnly ? c.Status.ToString():"",
                                    isReadOnly ? c.SeamCategory.ToString():Helper.HTMLAutoComplete(c.LineId, "txtSeamCategory",WebUtility.HtmlEncode(c.SeamCategoryDesc),"", false,"","SeamCategory",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(c.LineId, "SeamCategory",Convert.ToString(c.SeamCategory)),
                                    isReadOnly ? c.NozzleSize.ToString():Helper.GenerateTextbox(c.LineId, "NozzleSize", c.NozzleSize.ToString(), "",true, "","","clsMandatory", false,""),
                                    isReadOnly ? c.NozzleCategory.ToString():Helper.HTMLAutoComplete(c.LineId, "txtNozzleCategory",c.NozzleCategoryDesc,"", false,"","NozzleCategory",false)+""+Helper.GenerateHidden(c.LineId, "NozzleCategory",Convert.ToString(c.NozzleCategory)),
                                    isReadOnly ? c.SeamJointType.ToString():Helper.GenerateTextbox(c.LineId, "SeamJointType",  c.SeamJointType, "",true, "","","", false,"",""),
                                    isReadOnly ? c.SeamLength.ToString():Helper.GenerateTextbox(c.LineId, "SeamLength",  c.SeamLength.ToString(), "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part1Position.ToString():Helper.GenerateTextbox(c.LineId, "Part1Position",  c.Part1Position, "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part2Position.ToString():Helper.GenerateTextbox(c.LineId, "Part2Position",  c.Part2Position , "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part3Position.ToString():Helper.GenerateTextbox(c.LineId, "Part3Position",  c.Part3Position , "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part1Thickness.ToString():Helper.GenerateTextbox(c.LineId, "Part1Thickness",  c.Part1Thickness, "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part2Thickness.ToString():Helper.GenerateTextbox(c.LineId, "Part2Thickness",  c.Part2Thickness, "",true, "","","", false,"",""),
                                    isReadOnly ? c.Part3Thickness.ToString():Helper.GenerateTextbox(c.LineId, "Part3Thickness",  c.Part3Thickness, "",true, "","","", false,"",""),
                                    isReadOnly ? c.MinThickness.ToString():Helper.GenerateTextbox(c.LineId, "MinThickness", Convert.ToString(c.MinThickness), "",true,"","", "", false,"",""),
                                    isReadOnly ? c.Unit.ToString():Helper.GenerateTextbox(c.LineId, "Unit",  c.Unit.ToString(), "",true, "","","", false,"",""),
                                     Convert.ToString(c.WeldingProcess) ,//isReadOnly ? c.WeldingProcess.ToString():Helper.HTMLAutoComplete(c.LineId, "txtWeldingProcess",c.WeldingProcessDesc.ToString(),"", false,"","WeldingProcess",false)+""+Helper.GenerateHidden(c.LineId, "WeldingProcess",Convert.ToString(c.WeldingProcess)),
                                    isReadOnly ? (c.SetupRate.HasValue ? c.SetupRate.ToString() :""):Helper.GenerateTextbox(c.LineId, "SetupRate", c.SetupRate.ToString(), "",true,"", "","", false,"",""),
                                    isReadOnly ? (c.WeldingRate.HasValue ?  c.WeldingRate.ToString():""):Helper.GenerateTextbox(c.LineId, "WeldingRate", c.WeldingRate.ToString(), "",true,"","", "", false,"",""),
                                    isReadOnly ? c.CriticalSeam.ToString():Helper.HTMLAutoComplete(c.LineId, "txtCriticalSeam",c.CriticalSeam,"", false,"","CriticalSeam",false)+""+Helper.GenerateHidden(c.LineId, "CriticalSeam",Convert.ToString(c.CriticalSeam)),
                                    isReadOnly ? (c.Factor.HasValue ?c.Factor.ToString():""):Helper.GenerateTextbox(c.LineId, "Factor",  c.Factor.ToString(), "",true, "","","", false,"",""),
                                    isReadOnly ? (c.Amount.HasValue? c.Amount.ToString():""):Helper.GenerateTextbox(c.LineId, "Amount",  c.Amount.ToString(), "",false, "","","clsMandatory", false,"",""),
                                    !string.IsNullOrWhiteSpace(c.SetupCJCNo)? c.SetupCJCNo.ToString() :"",
                                    !string.IsNullOrWhiteSpace(c.WeldCJCNo)?   c.WeldCJCNo.ToString():"",
                                    "R"+ c.Revision.ToString(),
                          c.CreatedBy,
                          Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                          Helper.HTMLActionString(c.LineId,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.LineId + ");") + Helper.HTMLActionString(c.LineId,"Update","Update Record","fa fa-floppy-o","SaveRecord(" + c.LineId + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.LineId,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.LineId + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.LineId +");"),
                        }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveSeamLineData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (fc != null)
                {
                    int HeaderId = Convert.ToInt32(fc["MainHeaderId"]);
                    int LineId = Convert.ToInt32(fc["MainLineId"]);

                    CJC031 objCJC031 = null;
                    CJC030 objCJC030 = db.CJC030.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    string QualityProject = fc["QualityProject" + LineId]; ;
                    string SeamNo = fc["SeamNo" + LineId];
                    if (LineId > 0)
                    {

                        objCJC031 = db.CJC031.Where(x => x.LineId == LineId).FirstOrDefault();
                        if (objCJC031 != null)
                        {
                            if (!db.CJC031.Any(x => x.HeaderId == HeaderId && x.QualityProject == QualityProject && x.SeamNo == SeamNo && x.LineId != LineId))
                            {
                                objCJC031.HeaderId = HeaderId;
                                objCJC031.QualityProject = fc["QualityProject" + LineId];
                                objCJC031.SeamNo = fc["SeamNo" + LineId];
                                objCJC031.SetupCJCNo = fc["SetupCJCNo" + LineId];
                                objCJC031.WeldCJCNo = fc["WeldCJCNo" + LineId];
                                objCJC031.SeamCategory = fc["SeamCategory" + LineId];
                                objCJC031.NozzleSize = Convert.ToDouble(fc["NozzleSize" + LineId]);
                                objCJC031.NozzleCategory = fc["NozzleCategory" + LineId];
                                objCJC031.SeamJointType = fc["SeamJointType" + LineId];
                                objCJC031.SeamLength = Convert.ToInt32(fc["SeamLength" + LineId]);
                                objCJC031.Part1Position = fc["Part1Position" + LineId];
                                objCJC031.Part2Position = fc["Part2Position" + LineId];
                                objCJC031.Part3Position = fc["Part3Position" + LineId];
                                objCJC031.Part1Thickness = fc["Part1Thickness" + LineId];
                                objCJC031.Part2Thickness = fc["Part2Thickness" + LineId];
                                objCJC031.Part3Thickness = fc["Part3Thickness" + LineId];
                                objCJC031.MinThickness = Convert.ToDouble(fc["MinThickness" + LineId]);
                                objCJC031.Unit = Convert.ToDouble(fc["Unit" + LineId]);
                                objCJC031.WeldingProcess = fc["WeldingProcess" + LineId];
                                objCJC031.SetupRate = Convert.ToDouble(fc["SetupRate" + LineId]);
                                objCJC031.WeldingRate = Convert.ToDouble(fc["WeldingRate" + LineId]);
                                objCJC031.CriticalSeam = fc["CriticalSeam" + LineId];
                                objCJC031.Factor = Convert.ToDouble(fc["Factor" + LineId]);
                                objCJC031.Amount = Convert.ToDouble(fc["Amount" + LineId]);
                                objCJC031.Revision = objCJC030.Revision;
                                objCJC031.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                objCJC031.EditedBy = objClsLoginInfo.UserName;
                                objCJC031.EditedOn = DateTime.Now;
                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
                        }
                    }
                    else
                    {
                        if (!db.CJC031.Any(x => x.HeaderId == HeaderId && x.QualityProject == QualityProject && x.SeamNo == SeamNo))
                        {
                            objCJC031 = new CJC031();
                            objCJC031.HeaderId = HeaderId;
                            objCJC031.QualityProject = fc["QualityProject" + LineId];
                            objCJC031.SeamNo = fc["SeamNo" + LineId];
                            objCJC031.SetupCJCNo = fc["SetupCJCNo" + LineId];
                            objCJC031.WeldCJCNo = fc["WeldCJCNo" + LineId];
                            objCJC031.SeamCategory = fc["SeamCategory" + LineId];
                            objCJC031.NozzleSize = Convert.ToDouble(fc["NozzleSize" + LineId]);
                            objCJC031.NozzleCategory = fc["NozzleCategory" + LineId];
                            objCJC031.SeamJointType = fc["SeamJointType" + LineId];
                            objCJC031.SeamLength = Convert.ToInt32(fc["SeamLength" + LineId]);
                            objCJC031.Part1Position = fc["Part1Position" + LineId];
                            objCJC031.Part2Position = fc["Part2Position" + LineId];
                            objCJC031.Part3Position = fc["Part3Position" + LineId];
                            objCJC031.Part1Thickness = fc["Part1Thickness" + LineId];
                            objCJC031.Part2Thickness = fc["Part2Thickness" + LineId];
                            objCJC031.Part3Thickness = fc["Part3Thickness" + LineId];
                            objCJC031.MinThickness = Convert.ToDouble(fc["MinThickness" + LineId]);
                            if (!(string.IsNullOrWhiteSpace(fc["Unit" + LineId])))
                            {
                                objCJC031.Unit = Convert.ToDouble(fc["Unit" + LineId]);
                            }
                            objCJC031.WeldingProcess = fc["WeldingProcess" + LineId];
                            objCJC031.SetupRate = Convert.ToDouble(fc["SetupRate" + LineId]);
                            objCJC031.WeldingRate = Convert.ToDouble(fc["WeldingRate" + LineId]);
                            objCJC031.CriticalSeam = fc["CriticalSeam" + LineId];
                            objCJC031.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(fc["Factor" + LineId]) ? fc["Factor" + LineId] : "0");
                            objCJC031.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(fc["Amount" + LineId]) ? fc["Amount" + LineId] : "0");
                            objCJC031.Revision = objCJC030.Revision;
                            objCJC031.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                            objCJC031.CreatedBy = objClsLoginInfo.UserName;
                            objCJC031.CreatedOn = DateTime.Now;
                            db.CJC031.Add(objCJC031);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteSeamLineData(int rowid, int editlineid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CJC031 objCJC031 = db.CJC031.Where(x => x.LineId == editlineid).FirstOrDefault();
                if (objCJC031 != null)
                {
                    db.CJC031.Remove(objCJC031);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Generate CJC

        //save/update header data and create CJC no by pushing data into LN
        [HttpPost]
        public ActionResult GenerateCJCHeader(CJC030 cjc030)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            clsHelper.ResponseMsg obj = new clsHelper.ResponseMsg();
            clsHelper.ResponseMsg objServiceRes = new clsHelper.ResponseMsg();
            try
            {
                CJC030 objCJC030 = new CJC030();
                if (cjc030.HeaderId == 0)
                {
                    #region Insert
                    IEMQSEntitiesContext context = new IEMQSEntitiesContext();
                    using (DbContextTransaction dbTran = context.Database.BeginTransaction())
                    {
                        try
                        {
                            objCJC030.CJCNo = objServiceRes.Value;
                            objCJC030.Project = cjc030.Project;
                            objCJC030.Shop = cjc030.Shop;
                            objCJC030.Dia = cjc030.Dia;
                            objCJC030.Location = cjc030.Location;
                            objCJC030.BusinessPartner = cjc030.BusinessPartner;
                            objCJC030.PurchaseOdrerNo = cjc030.PurchaseOdrerNo;
                            objCJC030.PurchaseOdrerLineNo = cjc030.PurchaseOdrerLineNo;
                            objCJC030.Initiator = cjc030.Initiator;
                            objCJC030.BillNo = cjc030.BillNo;
                            objCJC030.BillDate = cjc030.BillDate;
                            objCJC030.Revision = 0;
                            objCJC030.Status = clsImplementationEnum.AutoCJCStatus.Submitted.GetStringValue();
                            objCJC030.TotalCJCAmount = cjc030.TotalCJCAmount;
                            objCJC030.CreatedBy = objClsLoginInfo.UserName;
                            objCJC030.CreatedOn = DateTime.Now;

                            context.CJC030.Add(objCJC030);
                            context.SaveChanges();

                            #region Insert Seam

                            DataTable dtSeam = (DataTable)TempData["FetchSeamList"];
                            if (dtSeam != null)
                            {
                                List<DataRow> dtFinal = (from u in dtSeam.Rows.Cast<DataRow>()
                                                         where !string.IsNullOrWhiteSpace(u["SeamCategory"].ToString())
                                                                && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                         select u).ToList();

                                if (dtFinal != null && dtFinal.Count > 0)
                                {
                                    List<CJC031> lstAddSeam = new List<CJC031>();
                                    foreach (DataRow dtRow in dtFinal)
                                    {
                                        #region Assign Values 

                                        CJC031 objCJC031 = new CJC031();
                                        objCJC031.HeaderId = objCJC030.HeaderId;
                                        objCJC031.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                        objCJC031.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                        objCJC031.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                        objCJC031.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["NozzleSize"])) ? Convert.ToString(dtRow["NozzleSize"]) : "0");
                                        objCJC031.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                        objCJC031.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                        objCJC031.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamLength"])) ? Convert.ToString(dtRow["SeamLength"]) : "0");
                                        objCJC031.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                        objCJC031.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                        objCJC031.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                        objCJC031.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                        objCJC031.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                        objCJC031.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                        objCJC031.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                        if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                        {
                                            objCJC031.Unit = Convert.ToDouble(dtRow["Unit"]);
                                        }
                                        objCJC031.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                        objCJC031.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? Convert.ToString(dtRow["SetupRate"]) : "0");
                                        objCJC031.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? Convert.ToString(dtRow["WeldingRate"]) : "0");
                                        objCJC031.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                        objCJC031.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                        objCJC031.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                        objCJC031.Revision = objCJC030.Revision;
                                        objCJC031.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                        objCJC031.CreatedBy = objClsLoginInfo.UserName;
                                        objCJC031.CreatedOn = DateTime.Now;
                                        lstAddSeam.Add(objCJC031);

                                        #endregion
                                    }

                                    if (lstAddSeam.Count > 0)
                                    {
                                        context.CJC031.AddRange(lstAddSeam);
                                        context.SaveChanges();
                                    }
                                }
                            }

                            #endregion

                            #region Insert Activity

                            DataTable dtActivity = (DataTable)TempData["ProjectLines"];
                            if (dtActivity != null)
                            {
                                List<DataRow> dtFinal = (from u in dtActivity.Rows.Cast<DataRow>()
                                                         where !string.IsNullOrWhiteSpace(u["Activity"].ToString())
                                                               && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                         select u).ToList();

                                if (dtFinal != null && dtFinal.Count > 0)
                                {
                                    List<CJC032> lstAddActivity = new List<CJC032>();
                                    foreach (DataRow dtRow in dtFinal)
                                    {
                                        CJC032 objCJC032 = new CJC032();
                                        objCJC032.HeaderId = objCJC030.HeaderId;
                                        objCJC032.Activity = Convert.ToString(dtRow["Activity"]);
                                        objCJC032.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                        objCJC032.UOM = Convert.ToString(dtRow["UOM"]);
                                        objCJC032.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                        objCJC032.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                        objCJC032.Revision = 0;
                                        objCJC032.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                        objCJC032.CreatedBy = objClsLoginInfo.UserName;
                                        objCJC032.CreatedOn = DateTime.Now;
                                        lstAddActivity.Add(objCJC032);
                                    }
                                    if (lstAddActivity.Count > 0)
                                    {
                                        context.CJC032.AddRange(lstAddActivity);
                                        context.SaveChanges();
                                    }
                                }
                            }
                            #endregion

                            objServiceRes = null;// GenerateCJCIntoLN(cjc030);
                            if (objServiceRes.Key)
                            {
                                string CJCNo = objServiceRes.Value;

                                objCJC030.CJCNo = CJCNo;

                                context.CJC031.Where(x => x.HeaderId == objCJC030.HeaderId && string.IsNullOrEmpty(x.SetupCJCNo) && x.SetupRate > 0).ToList().ForEach(x =>
                                {
                                    x.SetupCJCNo = CJCNo;
                                    x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                });

                                context.CJC031.Where(x => x.HeaderId == objCJC030.HeaderId && string.IsNullOrEmpty(x.WeldCJCNo) && x.WeldingRate > 0).ToList().ForEach(x =>
                                {
                                    x.WeldCJCNo = CJCNo;
                                    x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                });

                                context.CJC032.Where(x => x.HeaderId == objCJC030.HeaderId && x.Amount > 0).ToList().ForEach(x =>
                                {
                                    x.CJCNo = CJCNo;
                                    x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                });

                                context.SaveChanges();
                                dbTran.Commit();

                                TempData["FetchSeamList"] = null;
                                TempData["ProjectLines"] = null;

                                objResponseMsg.Key = true;
                                objResponseMsg.Value = "CJC : '" + CJCNo + "' generated successfully";
                                objResponseMsg.Status = objCJC030.Status;
                                objResponseMsg.HeaderId = objCJC030.HeaderId;
                            }
                            else
                            {
                                dbTran.Rollback();
                                RetenTempDataValue();
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = objServiceRes.Value;
                            }
                        }
                        catch (DbEntityValidationException dex)
                        {
                            dbTran.Rollback();
                            RetenTempDataValue();
                            throw dex;
                        }
                        catch (Exception ex)
                        {
                            dbTran.Rollback();
                            RetenTempDataValue();
                            throw ex;
                        }
                        finally
                        {
                            context = null;
                        }
                    }

                    #endregion
                }
                string existingCJCNo = string.Empty;
                if (cjc030.HeaderId > 0)
                {
                    IEMQSEntitiesContext context = new IEMQSEntitiesContext();
                    using (DbContextTransaction dbTran = context.Database.BeginTransaction())
                    {
                        try
                        {
                            objCJC030 = context.CJC030.Where(o => o.HeaderId == cjc030.HeaderId).FirstOrDefault();
                            if (objCJC030 != null)
                            {
                                objCJC030.Status = clsImplementationEnum.AutoCJCStatus.Submitted.GetStringValue();
                                objCJC030.EditedBy = objClsLoginInfo.UserName;
                                objCJC030.EditedOn = DateTime.Now;
                                existingCJCNo = objCJC030.CJCNo;
                                context.SaveChanges();

                                #region Insert Seam

                                DataTable dtSeam = (DataTable)TempData["FetchSeamList"];
                                if (dtSeam != null)
                                {
                                    List<DataRow> dtFinal = (from u in dtSeam.Rows.Cast<DataRow>()
                                                             where !string.IsNullOrWhiteSpace(u["SeamCategory"].ToString())
                                                                    && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                             select u).ToList();
                                    //List<DataRow> dtFinalLines = (from u in dtSeam.Rows.Cast<DataRow>()
                                    //                         where !string.IsNullOrWhiteSpace(u["SeamCategory"].ToString())
                                    //                                && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                    //                         select u).ToList();
                                    if (dtFinal != null && dtFinal.Count > 0)
                                    {
                                        List<CJC031> lstAddSeam = new List<CJC031>();
                                        List<CJC031> lstDeleteCJC031 = new List<CJC031>();
                                        var LineIds = dtFinal.Where(x => Convert.ToInt32(x["LineId"]) != 0).Select(x => Convert.ToInt32(x["LineId"]));

                                        lstDeleteCJC031 = db.CJC031.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == objCJC030.HeaderId).ToList();
                                        if (lstDeleteCJC031.Count > 0)
                                        {
                                            db.CJC031.RemoveRange(lstDeleteCJC031);
                                        }
                                        db.SaveChanges();

                                        foreach (DataRow dtRow in dtFinal)
                                        {
                                            CJC031 objCJC031 = new CJC031();
                                            var QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                            var SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                            objCJC031 = db.CJC031.Where(o => o.HeaderId == objCJC030.HeaderId && o.SeamNo == SeamNo && o.QualityProject == QualityProject).FirstOrDefault();
                                            int EditLineId = objCJC031 != null ? objCJC031.LineId : 0;
                                            if (EditLineId == 0)
                                            {
                                                #region Assign Values 

                                                objCJC031 = new CJC031();
                                                objCJC031.HeaderId = objCJC030.HeaderId;
                                                objCJC031.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                                objCJC031.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                                objCJC031.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                                objCJC031.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["NozzleSize"])) ? Convert.ToString(dtRow["NozzleSize"]) : "0");
                                                objCJC031.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                                objCJC031.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                                objCJC031.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamLength"])) ? Convert.ToString(dtRow["SeamLength"]) : "0");
                                                objCJC031.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                                objCJC031.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                                objCJC031.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                                objCJC031.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                                objCJC031.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                                objCJC031.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                                objCJC031.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                                if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                                {
                                                    objCJC031.Unit = Convert.ToDouble(dtRow["Unit"]);
                                                }
                                                objCJC031.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                                objCJC031.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? Convert.ToString(dtRow["SetupRate"]) : "0");
                                                objCJC031.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? Convert.ToString(dtRow["WeldingRate"]) : "0");
                                                objCJC031.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                                objCJC031.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                                objCJC031.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                                objCJC031.Revision = objCJC030.Revision;
                                                objCJC031.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                                objCJC031.CreatedBy = objClsLoginInfo.UserName;
                                                objCJC031.CreatedOn = DateTime.Now;
                                                lstAddSeam.Add(objCJC031);

                                                #endregion
                                            }
                                            else
                                            {
                                                #region Assign Values 

                                                objCJC031 = context.CJC031.Where(o => o.LineId == EditLineId).FirstOrDefault();
                                                if (objCJC031 != null)
                                                {
                                                    objCJC031.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                                    objCJC031.SeamNo = dtRow["SeamNo"].ToString();
                                                    objCJC031.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                                    objCJC031.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(dtRow["NozzleSize"].ToString()) ? dtRow["NozzleSize"].ToString() : "0");
                                                    objCJC031.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                                    objCJC031.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                                    objCJC031.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(dtRow["SeamLength"].ToString()) ? dtRow["SeamLength"].ToString() : "0");
                                                    objCJC031.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                                    objCJC031.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                                    objCJC031.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                                    objCJC031.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                                    objCJC031.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                                    objCJC031.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                                    objCJC031.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                                    if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                                    {
                                                        objCJC031.Unit = Convert.ToDouble(dtRow["Unit"]);
                                                    }
                                                    objCJC031.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                                    objCJC031.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? dtRow["SetupRate"].ToString() : "0");
                                                    objCJC031.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? dtRow["WeldingRate"].ToString() : "0");
                                                    objCJC031.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                                    objCJC031.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                                    objCJC031.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                                    objCJC031.Revision = objCJC030.Revision;
                                                    objCJC031.EditedBy = objClsLoginInfo.UserName;
                                                    objCJC031.EditedOn = DateTime.Now;
                                                }

                                                #endregion
                                            }
                                        }

                                        if (lstAddSeam.Count > 0)
                                        {
                                            context.CJC031.AddRange(lstAddSeam);
                                            context.SaveChanges();
                                        }
                                    }
                                }

                                #endregion

                                #region Insert Activity

                                DataTable dtActivity = (DataTable)TempData["ProjectLines"];
                                if (dtActivity != null)
                                {
                                    List<DataRow> dtFinal = (from u in dtActivity.Rows.Cast<DataRow>()
                                                             where !string.IsNullOrWhiteSpace(u["Activity"].ToString())
                                                                   && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                             select u).ToList();

                                    if (dtFinal != null && dtFinal.Count > 0)
                                    {
                                        List<CJC032> lstAddActivity = new List<CJC032>();
                                        List<CJC032> lstDeleteCJC032 = new List<CJC032>();

                                        var LineIds = dtFinal.Where(x => Convert.ToInt32(x["LineId"]) != 0).Select(x => Convert.ToInt32(x["LineId"]));

                                        lstDeleteCJC032 = db.CJC032.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == objCJC030.HeaderId).ToList();
                                        if (lstDeleteCJC032.Count > 0)
                                        {
                                            db.CJC032.RemoveRange(lstDeleteCJC032);
                                        }
                                        db.SaveChanges();

                                        foreach (DataRow dtRow in dtFinal)
                                        {
                                            string Activity = Convert.ToString(dtRow["Activity"]);
                                            if (!context.CJC032.Any(x => x.HeaderId == objCJC030.HeaderId && x.Activity == Activity))
                                            {
                                                CJC032 objCJC032 = new CJC032();
                                                objCJC032.HeaderId = objCJC030.HeaderId;
                                                objCJC032.Activity = Activity;
                                                objCJC032.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                                objCJC032.UOM = Convert.ToString(dtRow["UOM"]);
                                                objCJC032.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                                objCJC032.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                                objCJC032.Revision = 0;
                                                objCJC032.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                                objCJC032.CreatedBy = objClsLoginInfo.UserName;
                                                objCJC032.CreatedOn = DateTime.Now;
                                                lstAddActivity.Add(objCJC032);
                                            }
                                            else
                                            {
                                                CJC032 objCJC032 = context.CJC032.Where(x => x.HeaderId == objCJC030.HeaderId && x.Activity == Activity).FirstOrDefault();
                                                if (objCJC032 != null)
                                                {
                                                    objCJC032.Activity = Activity;
                                                    objCJC032.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                                    objCJC032.UOM = Convert.ToString(dtRow["UOM"]);
                                                    objCJC032.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                                    objCJC032.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                                    objCJC032.Revision = 0;
                                                    objCJC032.EditedBy = objClsLoginInfo.UserName;
                                                    objCJC032.EditedOn = DateTime.Now;
                                                }
                                            }
                                        }
                                        if (lstAddActivity.Count > 0)
                                        {
                                            context.CJC032.AddRange(lstAddActivity);
                                            context.SaveChanges();
                                        }
                                    }
                                }

                                #endregion

                                objServiceRes = null;//GenerateCJCIntoLN(cjc030);
                                if (objServiceRes.Key)
                                {
                                    string CJCNo = objServiceRes.Value;

                                    objCJC030.CJCNo = CJCNo;

                                    context.CJC031.Where(x => x.HeaderId == objCJC030.HeaderId && string.IsNullOrEmpty(x.SetupCJCNo) && x.SetupRate > 0).ToList().ForEach(x =>
                                    {
                                        x.SetupCJCNo = CJCNo;
                                        x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                    });

                                    context.CJC031.Where(x => x.HeaderId == objCJC030.HeaderId && string.IsNullOrEmpty(x.WeldCJCNo) && x.WeldingRate > 0).ToList().ForEach(x =>
                                    {
                                        x.WeldCJCNo = CJCNo;
                                        x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                    });

                                    context.CJC032.Where(x => x.HeaderId == objCJC030.HeaderId && x.Amount > 0).ToList().ForEach(x =>
                                    {
                                        x.CJCNo = CJCNo;
                                        x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                    });

                                    context.SaveChanges();
                                    dbTran.Commit();

                                    TempData["FetchSeamList"] = null;
                                    TempData["ProjectLines"] = null;

                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "CJC : '" + CJCNo + "' generated successfully";
                                    objResponseMsg.Status = objCJC030.Status;
                                    objResponseMsg.HeaderId = objCJC030.HeaderId;
                                }
                                else
                                {
                                    dbTran.Rollback();
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = objServiceRes.Value;
                                }
                            }
                            else
                            {
                                dbTran.Rollback();
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Record not found";
                            }
                        }
                        catch (DbEntityValidationException dex)
                        {
                            dbTran.Rollback();
                            throw dex;
                        }
                        catch (Exception ex)
                        {
                            dbTran.Rollback();
                            throw ex;
                        }
                        finally
                        {
                            context = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.Message;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //header data and reclaim amount with new CJC no by pushing data into LN
        [HttpPost]
        public ActionResult ReclaimCJC(CJC030 cjc030)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            clsHelper.ResponseMsg obj = new clsHelper.ResponseMsg();
            clsHelper.ResponseMsg objServiceRes = new clsHelper.ResponseMsg();
            try
            {
                CJC030 objCJC030 = new CJC030();
                string existingCJCNo = string.Empty;
                if (cjc030.HeaderId > 0)
                {
                    IEMQSEntitiesContext context = new IEMQSEntitiesContext();
                    using (DbContextTransaction dbTran = context.Database.BeginTransaction())
                    {
                        try
                        {
                            objCJC030 = context.CJC030.Where(o => o.HeaderId == cjc030.HeaderId).FirstOrDefault();
                            if (objCJC030 != null)
                            {
                                objCJC030.Status = clsImplementationEnum.AutoCJCStatus.Submitted.GetStringValue();
                                objCJC030.EditedBy = objClsLoginInfo.UserName;
                                objCJC030.EditedOn = DateTime.Now;
                                existingCJCNo = objCJC030.CJCNo;
                                context.SaveChanges();

                                #region Insert Seam

                                DataTable dtSeam = (DataTable)TempData["FetchSeamList"];
                                if (dtSeam != null)
                                {
                                    List<DataRow> dtFinal = (from u in dtSeam.Rows.Cast<DataRow>()
                                                             where !string.IsNullOrWhiteSpace(u["SeamCategory"].ToString())
                                                                    && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                             select u).ToList();

                                    if (dtFinal != null && dtFinal.Count > 0)
                                    {
                                        List<CJC031> lstAddSeam = new List<CJC031>();
                                        foreach (DataRow dtRow in dtFinal)
                                        {
                                            CJC031 objCJC031 = new CJC031();
                                            var QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                            var SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                            objCJC031 = db.CJC031.Where(o => o.HeaderId == objCJC030.HeaderId && o.SeamNo == SeamNo && o.QualityProject == QualityProject).FirstOrDefault();
                                            int EditLineId = objCJC031 != null ? objCJC031.LineId : 0;//Convert.ToInt32(dtRow["EditLineId"]);
                                            if (EditLineId == 0)
                                            {
                                                #region Assign Values 

                                                objCJC031 = new CJC031();
                                                objCJC031.HeaderId = objCJC030.HeaderId;
                                                objCJC031.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                                objCJC031.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                                objCJC031.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                                objCJC031.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["NozzleSize"])) ? Convert.ToString(dtRow["NozzleSize"]) : "0");
                                                objCJC031.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                                objCJC031.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                                objCJC031.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamLength"])) ? Convert.ToString(dtRow["SeamLength"]) : "0");
                                                objCJC031.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                                objCJC031.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                                objCJC031.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                                objCJC031.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                                objCJC031.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                                objCJC031.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                                objCJC031.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                                if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                                {
                                                    objCJC031.Unit = Convert.ToDouble(dtRow["Unit"]);
                                                }
                                                objCJC031.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                                objCJC031.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? Convert.ToString(dtRow["SetupRate"]) : "0");
                                                objCJC031.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? Convert.ToString(dtRow["WeldingRate"]) : "0");
                                                objCJC031.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                                objCJC031.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                                objCJC031.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                                objCJC031.Revision = objCJC030.Revision;
                                                objCJC031.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                                objCJC031.CreatedBy = objClsLoginInfo.UserName;
                                                objCJC031.CreatedOn = DateTime.Now;
                                                lstAddSeam.Add(objCJC031);

                                                #endregion
                                            }
                                            else
                                            {
                                                #region Assign Values 

                                                objCJC031 = context.CJC031.Where(o => o.LineId == EditLineId).FirstOrDefault();
                                                if (objCJC031 != null)
                                                {
                                                    objCJC031.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                                    objCJC031.SeamNo = dtRow["SeamNo"].ToString();
                                                    objCJC031.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                                    objCJC031.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(dtRow["NozzleSize"].ToString()) ? dtRow["NozzleSize"].ToString() : "0");
                                                    objCJC031.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                                    objCJC031.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                                    objCJC031.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(dtRow["SeamLength"].ToString()) ? dtRow["SeamLength"].ToString() : "0");
                                                    objCJC031.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                                    objCJC031.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                                    objCJC031.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                                    objCJC031.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                                    objCJC031.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                                    objCJC031.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                                    objCJC031.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                                    if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                                    {
                                                        objCJC031.Unit = Convert.ToDouble(dtRow["Unit"]);
                                                    }
                                                    objCJC031.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                                    objCJC031.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? dtRow["SetupRate"].ToString() : "0");
                                                    objCJC031.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? dtRow["WeldingRate"].ToString() : "0");
                                                    objCJC031.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                                    objCJC031.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                                    objCJC031.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                                    objCJC031.Revision = objCJC030.Revision;
                                                    objCJC031.EditedBy = objClsLoginInfo.UserName;
                                                    objCJC031.EditedOn = DateTime.Now;
                                                }

                                                #endregion
                                            }
                                        }

                                        if (lstAddSeam.Count > 0)
                                        {
                                            context.CJC031.AddRange(lstAddSeam);
                                            context.SaveChanges();
                                        }
                                    }
                                }

                                #endregion

                                #region Insert Activity

                                DataTable dtActivity = (DataTable)TempData["ProjectLines"];
                                if (dtActivity != null)
                                {
                                    List<DataRow> dtFinal = (from u in dtActivity.Rows.Cast<DataRow>()
                                                             where !string.IsNullOrWhiteSpace(u["Activity"].ToString())
                                                                   && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                             select u).ToList();

                                    if (dtFinal != null && dtFinal.Count > 0)
                                    {
                                        List<CJC032> lstAddActivity = new List<CJC032>();
                                        foreach (DataRow dtRow in dtFinal)
                                        {
                                            string Activity = Convert.ToString(dtRow["Activity"]);
                                            if (!context.CJC032.Any(x => x.HeaderId == objCJC030.HeaderId && x.Activity == Activity))
                                            {
                                                CJC032 objCJC032 = new CJC032();
                                                objCJC032.HeaderId = objCJC030.HeaderId;
                                                objCJC032.Activity = Activity;
                                                objCJC032.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                                objCJC032.UOM = Convert.ToString(dtRow["UOM"]);
                                                objCJC032.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                                objCJC032.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                                objCJC032.Revision = 0;
                                                objCJC032.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                                objCJC032.CreatedBy = objClsLoginInfo.UserName;
                                                objCJC032.CreatedOn = DateTime.Now;
                                                lstAddActivity.Add(objCJC032);
                                            }
                                            else
                                            {
                                                CJC032 objCJC032 = context.CJC032.Where(x => x.HeaderId == objCJC030.HeaderId && x.Activity == Activity).FirstOrDefault();
                                                if (objCJC032 != null)
                                                {
                                                    objCJC032.Activity = Activity;
                                                    objCJC032.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                                    objCJC032.UOM = Convert.ToString(dtRow["UOM"]);
                                                    objCJC032.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                                    objCJC032.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                                    objCJC032.Revision = 0;
                                                    objCJC032.EditedBy = objClsLoginInfo.UserName;
                                                    objCJC032.EditedOn = DateTime.Now;
                                                }
                                            }
                                        }
                                        if (lstAddActivity.Count > 0)
                                        {
                                            context.CJC032.AddRange(lstAddActivity);
                                            context.SaveChanges();
                                        }
                                    }
                                }

                                #endregion

                                objServiceRes = null;//GenerateCJCIntoLN(cjc030, true);
                                if (objServiceRes.Key)
                                {
                                    string CJCNo = objServiceRes.Value;

                                    objCJC030.CJCNo = CJCNo;

                                    context.CJC031.Where(x => x.HeaderId == objCJC030.HeaderId && x.SetupCJCNo == existingCJCNo && x.SetupRate > 0).ToList().ForEach(x =>
                                    {
                                        x.SetupCJCNo = CJCNo;
                                        x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                    });

                                    context.CJC031.Where(x => x.HeaderId == objCJC030.HeaderId && x.WeldCJCNo == existingCJCNo && x.WeldingRate > 0).ToList().ForEach(x =>
                                    {
                                        x.WeldCJCNo = CJCNo;
                                        x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                    });

                                    context.CJC032.Where(x => x.HeaderId == objCJC030.HeaderId && x.CJCNo == existingCJCNo && x.Amount > 0).ToList().ForEach(x =>
                                    {
                                        x.CJCNo = CJCNo;
                                        x.Status = clsImplementationEnum.AutoCJCStatus.Submitted.ToString();
                                    });

                                    dbTran.Commit();

                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "CJC : '" + CJCNo + "' reclaimed successfully";
                                    objResponseMsg.Status = objCJC030.Status;
                                    objResponseMsg.HeaderId = objCJC030.HeaderId;
                                }
                                else
                                {
                                    dbTran.Rollback();
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = objServiceRes.Value;
                                }
                            }
                            else
                            {
                                dbTran.Rollback();
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Record not found";
                            }
                        }
                        catch (DbEntityValidationException dex)
                        {
                            dbTran.Rollback();
                            throw dex;
                        }
                        catch (Exception ex)
                        {
                            dbTran.Rollback();
                            throw ex;
                        }
                        finally
                        {
                            context = null;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.Message;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //insert/update Headerlines with "Draft" status 
        [HttpPost]
        public ActionResult SaveAsDraft(CJC030 cjc030)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            clsHelper.ResponseMsg obj = new clsHelper.ResponseMsg();
            try
            {
                CJC030 objCJC030 = new CJC030();
                string existingCJCNo = string.Empty;
                if (cjc030.HeaderId > 0)
                {
                    #region Update as Draft

                    objCJC030 = db.CJC030.Where(o => o.HeaderId == cjc030.HeaderId).FirstOrDefault();
                    if (objCJC030 != null)
                    {
                        #region update Header
                        objCJC030.BusinessPartner = cjc030.BusinessPartner;
                        objCJC030.PurchaseOdrerNo = cjc030.PurchaseOdrerNo;
                        objCJC030.PurchaseOdrerLineNo = cjc030.PurchaseOdrerLineNo;
                        objCJC030.Initiator = cjc030.Initiator;
                        objCJC030.BillNo = cjc030.BillNo;
                        objCJC030.BillDate = cjc030.BillDate;
                        objCJC030.Revision = 0;
                        objCJC030.Status = clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue();
                        objCJC030.TotalCJCAmount = cjc030.TotalCJCAmount;
                        objCJC030.EditedBy = objClsLoginInfo.UserName;
                        objCJC030.EditedOn = DateTime.Now;
                        existingCJCNo = objCJC030.CJCNo;
                        db.SaveChanges();
                        #endregion

                        #region Insert Seam

                        DataTable dtSeam = (DataTable)TempData["FetchSeamList"];
                        if (dtSeam != null)
                        {
                            List<DataRow> dtFinal = (from u in dtSeam.Rows.Cast<DataRow>()
                                                     where !string.IsNullOrWhiteSpace(u["SeamCategory"].ToString())
                                                            && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                     select u).ToList();

                            if (dtFinal != null && dtFinal.Count > 0)
                            {
                                List<CJC031> lstAddSeam = new List<CJC031>();
                                List<CJC031> lstDeleteCJC031 = new List<CJC031>();

                                var LineIds = dtFinal.Where(x => Convert.ToInt32(x["LineId"]) != 0).Select(x => Convert.ToInt32(x["LineId"]));

                                lstDeleteCJC031 = db.CJC031.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == objCJC030.HeaderId).ToList();
                                if (lstDeleteCJC031.Count > 0)
                                {
                                    db.CJC031.RemoveRange(lstDeleteCJC031);
                                }
                                db.SaveChanges();
                                foreach (DataRow dtRow in dtFinal)
                                {
                                    CJC031 objCJC031 = new CJC031();
                                    var QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                    var SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                    objCJC031 = db.CJC031.Where(o => o.HeaderId == objCJC030.HeaderId && o.SeamNo == SeamNo && o.QualityProject == QualityProject).FirstOrDefault();
                                    int EditLineId = objCJC031 != null ? objCJC031.LineId : 0;// Convert.ToInt32(dtRow["EditLineId"]);
                                    if (EditLineId == 0)
                                    {
                                        #region Assign Values 

                                        objCJC031 = new CJC031();
                                        objCJC031.HeaderId = objCJC030.HeaderId;
                                        objCJC031.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                        objCJC031.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                        objCJC031.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                        objCJC031.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["NozzleSize"])) ? Convert.ToString(dtRow["NozzleSize"]) : "0");
                                        objCJC031.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                        objCJC031.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                        objCJC031.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamLength"])) ? Convert.ToString(dtRow["SeamLength"]) : "0");
                                        objCJC031.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                        objCJC031.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                        objCJC031.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                        objCJC031.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                        objCJC031.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                        objCJC031.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                        objCJC031.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                        if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                        {
                                            objCJC031.Unit = Convert.ToDouble(dtRow["Unit"]);
                                        }
                                        objCJC031.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                        objCJC031.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? Convert.ToString(dtRow["SetupRate"]) : "0");
                                        objCJC031.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? Convert.ToString(dtRow["WeldingRate"]) : "0");
                                        objCJC031.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                        objCJC031.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                        objCJC031.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                        objCJC031.Revision = objCJC030.Revision;
                                        objCJC031.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                        objCJC031.CreatedBy = objClsLoginInfo.UserName;
                                        objCJC031.CreatedOn = DateTime.Now;
                                        lstAddSeam.Add(objCJC031);

                                        #endregion
                                    }
                                    else
                                    {
                                        #region Assign Values 

                                        if (objCJC031 != null)
                                        {
                                            objCJC031.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                            objCJC031.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                            objCJC031.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                            objCJC031.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(dtRow["NozzleSize"].ToString()) ? dtRow["NozzleSize"].ToString() : "0");
                                            objCJC031.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                            objCJC031.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                            objCJC031.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(dtRow["SeamLength"].ToString()) ? dtRow["SeamLength"].ToString() : "0");
                                            objCJC031.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                            objCJC031.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                            objCJC031.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                            objCJC031.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                            objCJC031.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                            objCJC031.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                            objCJC031.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                            if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                            {
                                                objCJC031.Unit = Convert.ToDouble(dtRow["Unit"]);
                                            }
                                            objCJC031.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                            objCJC031.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? dtRow["SetupRate"].ToString() : "0");
                                            objCJC031.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? dtRow["WeldingRate"].ToString() : "0");
                                            objCJC031.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                            objCJC031.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                            objCJC031.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                            objCJC031.Revision = objCJC030.Revision;
                                            objCJC031.EditedBy = objClsLoginInfo.UserName;
                                            objCJC031.EditedOn = DateTime.Now;
                                        }

                                        #endregion
                                    }
                                }

                                if (lstAddSeam.Count > 0)
                                {
                                    db.CJC031.AddRange(lstAddSeam);
                                    db.SaveChanges();
                                }
                            }
                        }

                        #endregion

                        #region Insert Activity

                        DataTable dtActivity = (DataTable)TempData["ProjectLines"];
                        if (dtActivity != null)
                        {
                            List<DataRow> dtFinal = (from u in dtActivity.Rows.Cast<DataRow>()
                                                     where !string.IsNullOrWhiteSpace(u["Activity"].ToString())
                                                           && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                     select u).ToList();

                            if (dtFinal != null && dtFinal.Count > 0)
                            {
                                List<CJC032> lstAddActivity = new List<CJC032>();
                                List<CJC032> lstDeleteCJC032 = new List<CJC032>();

                                var LineIds = dtFinal.Where(x => Convert.ToInt32(x["LineId"]) != 0).Select(x => Convert.ToInt32(x["LineId"]));

                                lstDeleteCJC032 = db.CJC032.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == objCJC030.HeaderId).ToList();
                                if (lstDeleteCJC032.Count > 0)
                                {
                                    db.CJC032.RemoveRange(lstDeleteCJC032);
                                }
                                db.SaveChanges();
                                foreach (DataRow dtRow in dtFinal)
                                {
                                    string Activity = Convert.ToString(dtRow["Activity"]);
                                    if (!db.CJC032.Any(x => x.HeaderId == objCJC030.HeaderId && x.Activity == Activity))
                                    {
                                        CJC032 objCJC032 = new CJC032();
                                        objCJC032.HeaderId = objCJC030.HeaderId;
                                        objCJC032.Activity = Activity;
                                        objCJC032.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                        objCJC032.UOM = Convert.ToString(dtRow["UOM"]);
                                        objCJC032.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                        objCJC032.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                        objCJC032.Revision = 0;
                                        objCJC032.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                        objCJC032.CreatedBy = objClsLoginInfo.UserName;
                                        objCJC032.CreatedOn = DateTime.Now;
                                        lstAddActivity.Add(objCJC032);
                                    }
                                    else
                                    {
                                        CJC032 objCJC032 = db.CJC032.Where(x => x.HeaderId == objCJC030.HeaderId && x.Activity == Activity).FirstOrDefault();
                                        if (objCJC032 != null)
                                        {
                                            objCJC032.Activity = Activity;
                                            objCJC032.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                            objCJC032.UOM = Convert.ToString(dtRow["UOM"]);
                                            objCJC032.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                            objCJC032.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                            objCJC032.Revision = 0;
                                            objCJC032.EditedBy = objClsLoginInfo.UserName;
                                            objCJC032.EditedOn = DateTime.Now;
                                        }
                                    }
                                }
                                if (lstAddActivity.Count > 0)
                                {
                                    db.CJC032.AddRange(lstAddActivity);
                                    db.SaveChanges();
                                }
                            }
                        }

                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record not found";
                    }
                    #endregion
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                }
                else
                {
                    #region Insert Record as Draft
                    objCJC030.CJCNo = existingCJCNo;
                    objCJC030.Project = cjc030.Project;
                    objCJC030.Shop = cjc030.Shop;
                    objCJC030.Dia = cjc030.Dia;
                    objCJC030.Location = cjc030.Location;
                    objCJC030.BusinessPartner = cjc030.BusinessPartner;
                    objCJC030.PurchaseOdrerNo = cjc030.PurchaseOdrerNo;
                    objCJC030.PurchaseOdrerLineNo = cjc030.PurchaseOdrerLineNo;
                    objCJC030.Initiator = cjc030.Initiator;
                    objCJC030.BillNo = cjc030.BillNo;
                    objCJC030.BillDate = cjc030.BillDate;
                    objCJC030.Revision = 0;
                    objCJC030.Status = clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue();
                    objCJC030.TotalCJCAmount = cjc030.TotalCJCAmount;
                    objCJC030.CreatedBy = objClsLoginInfo.UserName;
                    objCJC030.CreatedOn = DateTime.Now;

                    db.CJC030.Add(objCJC030);
                    db.SaveChanges();

                    #region Insert Seam

                    DataTable dtSeam = (DataTable)TempData["FetchSeamList"];
                    if (dtSeam != null)
                    {
                        List<DataRow> dtFinal = (from u in dtSeam.Rows.Cast<DataRow>()
                                                 where !string.IsNullOrWhiteSpace(u["SeamCategory"].ToString())
                                                        && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                 select u).ToList();

                        if (dtFinal != null && dtFinal.Count > 0)
                        {
                            List<CJC031> lstAddSeam = new List<CJC031>();

                            foreach (DataRow dtRow in dtFinal)
                            {
                                #region Assign Values 

                                CJC031 objCJC031 = new CJC031();
                                objCJC031.HeaderId = objCJC030.HeaderId;
                                objCJC031.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                objCJC031.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                objCJC031.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                objCJC031.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["NozzleSize"])) ? Convert.ToString(dtRow["NozzleSize"]) : "0");
                                objCJC031.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                objCJC031.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                objCJC031.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamLength"])) ? Convert.ToString(dtRow["SeamLength"]) : "0");
                                objCJC031.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                objCJC031.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                objCJC031.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                objCJC031.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                objCJC031.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                objCJC031.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                objCJC031.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                {
                                    objCJC031.Unit = Convert.ToDouble(dtRow["Unit"]);
                                }
                                objCJC031.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                objCJC031.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? Convert.ToString(dtRow["SetupRate"]) : "0");
                                objCJC031.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? Convert.ToString(dtRow["WeldingRate"]) : "0");
                                objCJC031.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                objCJC031.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                objCJC031.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                objCJC031.Revision = objCJC030.Revision;
                                objCJC031.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                objCJC031.CreatedBy = objClsLoginInfo.UserName;
                                objCJC031.CreatedOn = DateTime.Now;
                                lstAddSeam.Add(objCJC031);

                                #endregion
                            }

                            if (lstAddSeam.Count > 0)
                            {
                                db.CJC031.AddRange(lstAddSeam);
                                db.SaveChanges();
                            }
                        }
                    }

                    #endregion

                    #region Insert Activity

                    DataTable dtActivity = (DataTable)TempData["ProjectLines"];
                    if (dtActivity != null)
                    {
                        List<DataRow> dtFinal = (from u in dtActivity.Rows.Cast<DataRow>()
                                                 where !string.IsNullOrWhiteSpace(u["Activity"].ToString())
                                                       && !string.IsNullOrWhiteSpace(u["Amount"].ToString()) && Convert.ToDecimal(u["Amount"].ToString()) > 0
                                                 select u).ToList();

                        if (dtFinal != null && dtFinal.Count > 0)
                        {
                            List<CJC032> lstAddActivity = new List<CJC032>();
                            List<CJC032> lstDeleteCJC032 = new List<CJC032>();

                            var LineIds = dtFinal.Where(x => Convert.ToInt32(x["LineId"]) != 0).Select(x => Convert.ToInt32(x["LineId"]));

                            lstDeleteCJC032 = db.CJC032.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == objCJC030.HeaderId).ToList();
                            if (lstDeleteCJC032.Count > 0)
                            {
                                db.CJC032.RemoveRange(lstDeleteCJC032);
                            }
                            db.SaveChanges();

                            foreach (DataRow dtRow in dtFinal)
                            {
                                CJC032 objCJC032 = new CJC032();
                                objCJC032.HeaderId = objCJC030.HeaderId;
                                objCJC032.Activity = Convert.ToString(dtRow["Activity"]);
                                objCJC032.Quantity = Convert.ToDouble(Convert.ToString(dtRow["Quantity"]));
                                objCJC032.UOM = Convert.ToString(dtRow["UOM"]);
                                objCJC032.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                objCJC032.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                objCJC032.Revision = 0;
                                objCJC032.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                objCJC032.CreatedBy = objClsLoginInfo.UserName;
                                objCJC032.CreatedOn = DateTime.Now;
                                lstAddActivity.Add(objCJC032);
                            }
                            if (lstAddActivity.Count > 0)
                            {
                                db.CJC032.AddRange(lstAddActivity);
                                db.SaveChanges();
                            }
                        }
                    }
                    #endregion

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "CJC saved successfully";
                    objResponseMsg.Status = objCJC030.Status;
                    objResponseMsg.HeaderId = objCJC030.HeaderId;

                    #endregion
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                }
                objResponseMsg.Key = true;
                objResponseMsg.Status = objCJC030.Status;
                objResponseMsg.HeaderId = objCJC030.HeaderId;
                RetenTempDataValue();
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.Message;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public clsHelper.ResponseMsg SaveSeamLineData(CJC030 cjc20)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (cjc20 != null)
                {
                    int HeaderId = cjc20.HeaderId;

                    CJC030 objCJC030 = db.CJC030.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    DataTable dt = (DataTable)TempData["FetchSeamList"];
                    List<CJC031> lstAddSeam = new List<CJC031>();
                    foreach (DataRow dtRow in dt.Rows)
                    {
                        CJC031 objCJC031 = new CJC031();
                        double amnt = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                        string QualityProject = Convert.ToString(dtRow["QualityProject"]);
                        string SeamNo = Convert.ToString(dtRow["SeamNo"]);
                        if (!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamCategory"])) && amnt > 0)
                        {
                            if (!db.CJC031.Any(x => x.HeaderId == HeaderId && x.QualityProject == QualityProject && x.SeamNo == SeamNo))
                            {
                                objCJC031 = new CJC031();
                                objCJC031.HeaderId = HeaderId;
                                objCJC031.QualityProject = Convert.ToString(dtRow["QualityProject"]);
                                objCJC031.SeamNo = Convert.ToString(dtRow["SeamNo"]);
                                objCJC031.SetupCJCNo = Convert.ToString(dtRow["SetupCJCNo"]);
                                objCJC031.WeldCJCNo = Convert.ToString(dtRow["WeldCJCNo"]);
                                objCJC031.SeamCategory = Convert.ToString(dtRow["SeamCategory"]);
                                objCJC031.NozzleSize = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["NozzleSize"])) ? Convert.ToString(dtRow["NozzleSize"]) : "0");
                                objCJC031.NozzleCategory = Convert.ToString(dtRow["NozzleCategory"]);
                                objCJC031.SeamJointType = Convert.ToString(dtRow["SeamJointType"]);
                                objCJC031.SeamLength = Convert.ToInt32(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SeamLength"])) ? Convert.ToString(dtRow["SeamLength"]) : "0");
                                objCJC031.Part1Position = Convert.ToString(dtRow["Part1Position"]);
                                objCJC031.Part2Position = Convert.ToString(dtRow["Part2Position"]);
                                objCJC031.Part3Position = Convert.ToString(dtRow["Part3Position"]);
                                objCJC031.Part1Thickness = Convert.ToString(dtRow["Part1Thickness"]);
                                objCJC031.Part2Thickness = Convert.ToString(dtRow["Part2Thickness"]);
                                objCJC031.Part3Thickness = Convert.ToString(dtRow["Part3Thickness"]);
                                objCJC031.MinThickness = Convert.ToDouble(dtRow["MinThickness"]);
                                if (!(string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Unit"]))))
                                {
                                    objCJC031.Unit = Convert.ToDouble(dtRow["Unit"]);
                                }
                                objCJC031.WeldingProcess = Convert.ToString(dtRow["WeldingProcess"]);
                                objCJC031.SetupRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["SetupRate"])) ? Convert.ToString(dtRow["SetupRate"]) : "0");
                                objCJC031.WeldingRate = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["WeldingRate"])) ? Convert.ToString(dtRow["WeldingRate"]) : "0");
                                objCJC031.CriticalSeam = Convert.ToString(dtRow["CriticalSeam"]);
                                objCJC031.Factor = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Factor"])) ? dtRow["Factor"] : "0");
                                objCJC031.Amount = Convert.ToDouble(!string.IsNullOrWhiteSpace(Convert.ToString(dtRow["Amount"])) ? dtRow["Amount"] : "0");
                                objCJC031.Revision = objCJC030.Revision;
                                objCJC031.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                objCJC031.CreatedBy = objClsLoginInfo.UserName;
                                objCJC031.CreatedOn = DateTime.Now;

                                if (objCJC031.SetupRate > 0)
                                    objCJC031.SetupCJCNo = objCJC030.CJCNo;

                                if (objCJC031.WeldingRate > 0)
                                    objCJC031.WeldCJCNo = objCJC030.CJCNo;

                                lstAddSeam.Add(objCJC031);

                                //objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                            }
                        }
                    }
                    if (lstAddSeam.Count > 0)
                    {
                        db.CJC031.AddRange(lstAddSeam);
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            finally
            {
                RetenTempDataValue();
            }
            return objResponseMsg;

        }

        [HttpPost]
        public clsHelper.ResponseMsg SaveActivityData(CJC030 cjc20)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (cjc20 != null)
                {
                    int HeaderId = cjc20.HeaderId;

                    CJC030 objCJC030 = db.CJC030.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    DataTable dt = TempData["ProjectLines"] as DataTable;
                    List<CJC032> lstAddSeam = new List<CJC032>();
                    foreach (DataRow dtRow in dt.Rows)
                    {
                        CJC032 objCJC032 = new CJC032();
                        double amnt = Convert.ToDouble(!string.IsNullOrWhiteSpace(dtRow["Amount"].ToString()) ? dtRow["Amount"] : "0");
                        string Activity = dtRow["Activity"].ToString();
                        if (!string.IsNullOrWhiteSpace(dtRow["Activity"].ToString()) && amnt > 0)
                        {
                            if (!db.CJC032.Any(x => x.HeaderId == HeaderId && x.Activity == Activity))
                            {
                                objCJC032 = new CJC032();
                                objCJC032.HeaderId = HeaderId;
                                objCJC032.Activity = Convert.ToString(dtRow["Activity"]);
                                objCJC032.Quantity = Convert.ToDouble(dtRow["Quantity"]);
                                objCJC032.UOM = Convert.ToString(dtRow["UOM"]);
                                objCJC032.Rate = Convert.ToDouble(Convert.ToString(dtRow["Rate"]));
                                objCJC032.Amount = Convert.ToDouble(Convert.ToString(dtRow["Amount"]));
                                objCJC032.CJCNo = Convert.ToString(dtRow["CJCNo"]);
                                objCJC032.Revision = 0;
                                objCJC032.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                                objCJC032.CreatedBy = objClsLoginInfo.UserName;
                                objCJC032.CreatedOn = DateTime.Now;
                                objCJC032.CJCNo = objCJC030.CJCNo;
                                lstAddSeam.Add(objCJC032);
                                //objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                            }
                        }
                    }
                    if (lstAddSeam.Count > 0)
                    {
                        db.CJC032.AddRange(lstAddSeam);
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.dataValue = ex.ToString();
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message;
            }
            return objResponseMsg;
        }


        #endregion

        #region Calculations

        [HttpPost]
        public ActionResult CalculateTotalClaimAmount(string Project = "", int dia = 0, bool isSaveAsDraft = false, string status = "")
        {
            ResponceMsgWithCJC objData = new ResponceMsgWithCJC();
            double SeamAmount = 0;
            double totalAmount = 0, ActivityAmount = 0;
            try
            {
                DataTable dtSeam = new DataTable();
                DataTable dtActivity = new DataTable();
                if (TempData["FetchSeamList"] != null)
                {
                    string returned = clsImplementationEnum.AutoCJCStatus.Returned.GetStringValue();
                    string rejected = clsImplementationEnum.AutoCJCStatus.Rejected.GetStringValue();
                    string draft = clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue();
                    dtSeam = (DataTable)TempData["FetchSeamList"];
                    if (dtSeam != null && dtSeam.Rows.Count > 0)
                    {
                        SeamAmount = (from u in dtSeam.AsEnumerable()
                                      where Convert.ToString(u["Amount"]) != "" && Convert.ToString(u["Amount"]) != "0" && (Convert.ToString(u["SetupCJCStatus"]) == "" || u["SetupCJCStatus"].ToString().Contains(draft) || Convert.ToString(u["WeldCJCStatus"]) == "" || u["WeldCJCStatus"].ToString().Contains(draft))
                                      select Convert.ToDouble(u["Amount"])).Sum();
                    }
                }
                if (TempData["ProjectLines"] != null)
                {
                    dtActivity = (DataTable)TempData["ProjectLines"];
                    string Contingency_amount = clsImplementationEnum.AutoCJCProjectActivity.Contingency_amount.GetStringValue();

                    var isNotMaintained = (from u in dtActivity.AsEnumerable()
                                           where (Convert.ToString(u["Amount"]) != "" && Convert.ToString(u["Amount"]) != "0")
                                           && Convert.ToString(u["Activity"]) == Contingency_amount && string.IsNullOrWhiteSpace(Convert.ToString(u["Remarks"]))
                                           select u).ToList();
                    if (isNotMaintained.Count() > 0)
                    {
                        objData.ActionKey = false;
                        objData.ActionValue = "Please maintain Remarks for Project Activity : Contingency amount";
                        return Json(objData, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        if (dtActivity != null && dtActivity.Rows.Count > 0)
                        {
                            ActivityAmount = (from u in dtActivity.AsEnumerable()
                                              where Convert.ToString(u["Amount"]) != "" && Convert.ToString(u["Amount"]) != "0"
                                              select Convert.ToDouble(u["Amount"])).Sum();
                        }
                    }
                }
                totalAmount = SeamAmount + ActivityAmount;
                objData.TotalAmount = Convert.ToDouble(string.Format("{0:F2}", totalAmount));

                if (isSaveAsDraft)
                {
                    objData.ActionKey = true; objData.Key = true;
                }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CalculateTotalReClaimAmount(int HeaderId, bool IsFetchSeamClick, string Project = "", int dia = 0, bool isSaveAsDraft = false, string status = "")
        {
            ResponceMsgWithCJC objData = new ResponceMsgWithCJC();
            double? SeamAmount = 0;
            double? totalAmount = 0, ActivityAmount = 0;
            try
            {
                if (HeaderId > 0)
                {
                    if (IsFetchSeamClick)
                    {
                        DataTable dtSeam = new DataTable();
                        if (TempData["FetchSeamList"] != null)
                        {
                            string returned = clsImplementationEnum.AutoCJCStatus.Returned.GetStringValue();
                            string draft = clsImplementationEnum.AutoCJCStatus.DRAFT.GetStringValue();
                            dtSeam = (DataTable)TempData["FetchSeamList"];
                            if (dtSeam != null && dtSeam.Rows.Count > 0)
                            {
                                SeamAmount = (from u in dtSeam.AsEnumerable()
                                              where Convert.ToString(u["Amount"]) != "" && Convert.ToString(u["Amount"]) != "0" && (Convert.ToString(u["SetupCJCStatus"]) == "" || u["SetupCJCStatus"].ToString().Contains(draft) || Convert.ToString(u["WeldCJCStatus"]) == "" || u["WeldCJCStatus"].ToString().Contains(draft))
                                              select Convert.ToDouble(u["Amount"])).Sum();
                            }
                        }
                    }
                    else
                    {
                        var lstSeam = (from u in db.CJC031
                                       where u.HeaderId == HeaderId && u.Amount > 0
                                       select u.Amount).ToList();
                        if (lstSeam != null && lstSeam.Count > 0)
                            SeamAmount = lstSeam.Sum();
                    }
                    string Contingency_amount = clsImplementationEnum.AutoCJCProjectActivity.Contingency_amount.GetStringValue();
                    var isNotMaintained = (from u in db.CJC032
                                           where u.HeaderId == HeaderId && u.Amount > 0
                                           && u.Activity == Contingency_amount && (u.Remarks == "" || u.Remarks == null)
                                           select u).ToList();
                    if (isNotMaintained.Count() > 0)
                    {
                        objData.ActionKey = false;
                        objData.ActionValue = "Please maintain Remarks for Project Activity : Contingency amount";
                        return Json(objData, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        var lstActivity = (from u in db.CJC032
                                           where u.HeaderId == HeaderId && u.Amount > 0
                                           select u.Amount).ToList();
                        if (lstActivity != null && lstActivity.Count > 0)
                            ActivityAmount = lstActivity.Sum();
                    }
                }
                totalAmount = SeamAmount + ActivityAmount;
                objData.TotalAmount = Convert.ToDouble(string.Format("{0:F2}", totalAmount));
                if (isSaveAsDraft)
                { objData.ActionKey = true; objData.Key = true; }
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            finally
            {
                RetenTempDataValue();
            }
            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public double? CalculateSetUpNonOverlayRate(List<CJC011> objCJC011List, string SeamCategory, double Minthk, int Jobdia, string typethickness, string seamtype)
        {
            double? SetUpRate = 0;
            CJC011 obj = new CJC011();
            string id = db.Database.SqlQuery<string>("SELECT dbo.FN_CJC_FETCH_ID_FROM_MASTER(" + Minthk + ",'NonOverlay')").FirstOrDefault();
            string[] arr = !string.IsNullOrWhiteSpace(id) ? id.Split(',') : null;
            if (arr != null && arr.Count() > 0)
            {
                if (objCJC011List != null)
                    obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && arr.Contains(x.NonOverlayThickness.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                else
                    obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && arr.Contains(x.NonOverlayThickness.ToString()) && x.SeamType == seamtype).FirstOrDefault();

                if (obj != null)
                {
                    SetUpRate = obj?.Rate;
                }
            }

            return SetUpRate;// Json(SetUpRate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public double? CalculateSetUpNozzleSizeRate(List<CJC011> objCJC011List, string SeamCategory, string nozzlecategory, double nozzlesize, string typethickness, string seamtype, int Jobdia)
        {
            double? SetUpRate = 0;
            CJC011 obj = new CJC011();
            if (nozzlesize >= 0)
            {
                string id = db.Database.SqlQuery<string>("SELECT dbo.FN_CJC_FETCH_ID_FROM_MASTER(" + nozzlesize + ",'NozzleSize')").FirstOrDefault();
                string[] arr = !string.IsNullOrWhiteSpace(id) ? id.Split(',') : null;
                if (arr != null && arr.Count() > 0)
                {
                    if (objCJC011List != null)
                        obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && x.NozzleCategory == nozzlecategory && arr.Contains(x.NozzleSize.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                    else
                        obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && x.NozzleCategory == nozzlecategory && arr.Contains(x.NozzleSize.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                    if (obj != null)
                    {
                        SetUpRate = obj?.Rate;
                    }
                }
            }
            return SetUpRate;// Json(SetUpRate, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public double? CalculateWeldNonOverlayRate(List<CJC011> objCJC011List, string SeamCategory, double Minthk, int Jobdia, string weldprocess, string jointtype, double nozzlesize, string nozzlecategory, bool IsPWHT, string seamtype)
        {
            //NonOverlay
            double? WeldRate = 0;
            CJC011 obj = new CJC011();
            if (IsPWHT)
            {
                //calculation depend on Seam  category- weld - min thickness

                string[] arr_Seam_WeldProc_MinThick = { clsImplementationEnum.AutoCJCSeamType.LW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.CW.GetStringValue() };
                string[] arr_Seam_WeldProc_MinThick2 = { clsImplementationEnum.AutoCJCSeamType.BU.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.HF.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PTCLW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PTCCW.GetStringValue() };
                if (arr_Seam_WeldProc_MinThick.Contains(seamtype) || arr_Seam_WeldProc_MinThick2.Contains(seamtype))
                {
                    if (!string.IsNullOrWhiteSpace(weldprocess))
                    {
                        string id = db.Database.SqlQuery<string>("SELECT dbo.FN_CJC_FETCH_ID_FROM_MASTER(" + Minthk + ",'NonOverlay')").FirstOrDefault();
                        string[] arr = !string.IsNullOrWhiteSpace(id) ? id.Split(',') : null;
                        if (arr != null && arr.Count() > 0)
                        {
                            if (arr_Seam_WeldProc_MinThick.Contains(seamtype))
                            {
                                string no = clsImplementationEnum.yesno.No.GetStringValue();
                                if (objCJC011List != null)
                                {
                                    if (objCJC011List.Any(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && arr.Contains(x.NonOverlayThickness.ToString())))//.Setup == no ? true : false)
                                    {
                                        obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.WeldingProcess == weldprocess && x.Dia == Jobdia && arr.Contains(x.NonOverlayThickness.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                                    }
                                }
                                else
                                {
                                    if (db.CJC011.Any(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && arr.Contains(x.NonOverlayThickness.ToString())))//.Setup == no ? true : false)
                                    {
                                        obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.WeldingProcess == weldprocess && x.Dia == Jobdia && arr.Contains(x.NonOverlayThickness.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                                    }
                                }
                            }

                            if (arr_Seam_WeldProc_MinThick2.Contains(seamtype))
                            {
                                if (objCJC011List != null)
                                {
                                    obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && x.WeldingProcess == weldprocess && arr.Contains(x.NonOverlayThickness.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                                }
                                else
                                {
                                    obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && x.WeldingProcess == weldprocess && arr.Contains(x.NonOverlayThickness.ToString()) && x.SeamType == seamtype).FirstOrDefault();
                                }
                            }

                        }
                    }
                }
                if (seamtype == clsImplementationEnum.AutoCJCSeamType.HX.GetStringValue())
                {
                    //calculation depend on Seam category - weld proc
                    if (objCJC011List != null)
                        obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.WeldingProcess == weldprocess && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                    else
                        obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.WeldingProcess == weldprocess && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                }
                if (!string.IsNullOrWhiteSpace(jointtype))
                {
                    string[] arr_AW_PW_NP = { clsImplementationEnum.AutoCJCSeamType.AW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.NP.GetStringValue() };
                    string[] arr_jointtype_wp = { clsImplementationEnum.AutoCJCSeamType.CR.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.OW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PTCOW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PTCBW.GetStringValue() };
                    //calculation depend on Seam category -Joint type
                    if (arr_AW_PW_NP.Contains(seamtype))
                    {
                        if (objCJC011List != null)
                            obj = objCJC011List.Where(x => x.NozzleCategory == nozzlecategory && x.WeldType == jointtype && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                        else
                            obj = db.CJC011.Where(x => x.NozzleCategory == nozzlecategory && x.WeldType == jointtype && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                    }
                    else if (arr_jointtype_wp.Contains(seamtype))
                    {
                        //calculation depend on Seam category -Joint type - weld proc
                        if (objCJC011List != null)
                            obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.WeldType == jointtype && x.WeldingProcess == weldprocess && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                        else
                            obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.WeldType == jointtype && x.WeldingProcess == weldprocess && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                    }
                }

                //calculation depend on Seam category -nozzle category - weld proc
                if (seamtype == clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue())
                {
                    if (!string.IsNullOrWhiteSpace(nozzlecategory))
                    {
                        string id = db.Database.SqlQuery<string>("SELECT dbo.FN_CJC_FETCH_ID_FROM_MASTER(" + nozzlesize + ",'NozzleSize')").FirstOrDefault();
                        string idMinthk = db.Database.SqlQuery<string>("SELECT dbo.FN_CJC_FETCH_ID_FROM_MASTER(" + Minthk + ",'NonOverlay')").FirstOrDefault();
                        string[] arr = !string.IsNullOrWhiteSpace(id) ? id.Split(',') : null;
                        if (arr != null && arr.Count() > 0)
                        {
                            if (seamtype == clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue())
                            {
                                if (objCJC011List != null)
                                    obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && arr.Contains(x.NozzleSize != null ? x.NozzleSize.ToString() : "") && x.NozzleCategory == nozzlecategory && x.WeldingProcess == weldprocess && x.SeamType == seamtype && x.Dia == Jobdia).FirstOrDefault();
                                else
                                    obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && arr.Contains(x.NozzleSize != null ? x.NozzleSize.ToString() : "") && x.NozzleCategory == nozzlecategory && x.WeldingProcess == weldprocess && x.SeamType == seamtype && x.Dia == Jobdia).FirstOrDefault();
                            }
                        }
                    }
                }

                if (seamtype == clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue())
                {
                    if (objCJC011List != null)
                        obj = objCJC011List.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                    else
                        obj = db.CJC011.Where(x => x.SeamCategory == SeamCategory && x.Dia == Jobdia && x.SeamType == seamtype).FirstOrDefault();
                }

                WeldRate = obj?.Rate;
            }
            return WeldRate;// Json(WeldRate, JsonRequestBehavior.AllowGet);
        }

        //Check if calculations are applicable or not
        public bool IsCalculationApplicable(int headerid, int lineid, string cjcno, string qproject, string seamno, string SeamCategory, bool SetupRate, string SetUpCJCStatus, string WeldCJCStatus)
        {
            string yes = clsImplementationEnum.yesno.Yes.GetStringValue();
            bool isSetupYes = false;
            var objCJC011 = db.CJC011.Where(x => x.SeamCategory == SeamCategory).FirstOrDefault();
            if (objCJC011 != null)
                isSetupYes = objCJC011.Setup == yes ? true : false;

            bool flag = false;
            if (string.IsNullOrWhiteSpace(cjcno))
            {
                if (SetupRate)
                {
                    flag = isSetupYes;
                }
                else
                {
                    flag = true;
                }
            }
            else
            {
                if (headerid > 0)
                {
                    string HeaderCJCNo = "";
                    CJC030 objCJC030 = db.CJC030.Where(x => x.HeaderId == headerid).FirstOrDefault();
                    if (objCJC030 != null)
                        HeaderCJCNo = objCJC030.CJCNo;

                    if (cjcno.Trim().ToLower() == HeaderCJCNo.Trim().ToLower())
                    {
                        if (SetupRate)
                        {
                            flag = isSetupYes;
                        }
                        else
                        {
                            flag = true;
                        }
                    }
                }
            }

            return flag;
        }

        //Calculate Seam Amount
        public double claimSeamAmount(int rowid, string SeamCategory = "", int Seam_length = 0, double mm_mtr = 0, double Welding_rate = 0, double Factor = 0, double set_up_rate = 0, double Nozzle_size = 0)
        {
            double TotalAmount = 0;
            switch (SeamCategory)
            {
                #region R4 sheet
                case "SC002": //Long Seam
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC003": //Circ. Seam
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC006": //Overlay Long seam
                    TotalAmount = Seam_length * Welding_rate * Factor;
                    break;
                case "SC010": //Nozzle # Shell/D'end/Cone
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC032": //External / Internal
                    TotalAmount = mm_mtr * Welding_rate * Factor;
                    break;
                case "SC033": //Build up
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC034": //Hard Facing
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC035": //Refractory Anchor
                    TotalAmount = (mm_mtr * Welding_rate) * Factor;//(Note : mm_mtr == seam quantity)
                    break;
                case "SC036": //Hex Mesh
                    TotalAmount = (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC037": //Long Seam PTC
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC038": //Circ. Seam PTC
                    TotalAmount = (mm_mtr * set_up_rate) + (mm_mtr * Welding_rate) * Factor;
                    break;
                case "SC039": //PTC For Overlay
                    TotalAmount = Seam_length * Welding_rate * Factor;
                    break;
                case "SC040": //PTC Buttering
                    TotalAmount = Seam_length * Welding_rate * Factor;
                    break;
                    #endregion

                    #region R3 Sheet (Not in use)
                    //case "SC004": //External 
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC005": //Internal
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC007": //Overlay Circ. seam
                    //    TotalAmount = Seam_length * Welding_rate * Factor;
                    //    break;
                    //case "SC008": //Nozzle Long seam
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC009": //Nozzle Circ. Seam
                    //    TotalAmount = Nozzle_size * Welding_rate * Factor;
                    //    break;
                    //case "SC011": // Wrapper Plate # Shell assly
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC012": //Saddle # wrapper plate assly
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC013": //Groove grinding + WEEP hole/pad+ Leak path grinding
                    //    TotalAmount = Seam_length * Welding_rate * Factor;
                    //    break;
                    //case "SC014": //Liner Setup & Welding
                    //    TotalAmount = Seam_length * Welding_rate * Factor;
                    //    break;
                    //case "SC015": //Weep tup set up & Welding
                    //    TotalAmount = Seam_length * Welding_rate * Factor;
                    //    break;
                    //case "SC018": //Other Externals with fillet weld
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC017": // Other Externals with groove weld
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC029": // Externals platform / pipe supprt / insulation cleats with groove + concave
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC030":// Externals platform / pipe supprt / insulation cleats with Fillet + concave
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC019": // Externals platform / pipe supprt / insulation cleats with groove weld
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC020": // Externals platform / pipe supprt / insulation cleats with Fillet weld
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC021": // Internals with groove weld
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC031": // Internals with fillet weld 
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC022": // Internals with fillet weld + Concave 
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    //case "SC023": // Internals with groove weld + concave 
                    //    TotalAmount = mm_mtr * Welding_rate * Factor;
                    //    break;
                    #endregion
            }
            if (TotalAmount > 0)
            {
                TotalAmount = Convert.ToDouble(string.Format("{0:F2}", TotalAmount));
            }
            if (rowid > 0)
            {
                //update in DataTable
                DataTable dt = (DataTable)TempData["FetchSeamList"];
                if (dt != null)
                {
                    DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                  where Convert.ToInt32(u["ROW_NO"]) == rowid
                                  select u).FirstOrDefault();

                    if (dr != null)
                    {
                        dr["Amount"] = TotalAmount;
                        dt.AcceptChanges();
                    }
                }
                RetenTempDataValue();
            }
            return TotalAmount;
        }

        #endregion

        #region RuntimeCalculations

        //Nozzle Size
        public double? CalcultaionOfNozzleSize(int rowid, string SeamCategory, string SeamType, int SeamLength)
        {
            if (arrSeamTypeNW.Contains(SeamType))
            {
                if (SeamLength > 0)
                {

                    var NozzleSizeamt = (Convert.ToDouble(SeamLength) / pi) / 25.4;
                    var NozzleSize = Double.Parse(NozzleSizeamt.ToString("0.0000"));
                    if (rowid > 0)
                    {
                        DataTable dt = (DataTable)TempData["FetchSeamList"];
                        if (dt != null)
                        {
                            DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                          where Convert.ToInt32(u["ROW_NO"]) == rowid
                                          select u).FirstOrDefault();

                            if (dr != null)
                            {
                                //changes the Column Value
                                dr["NozzleSize"] = NozzleSize;
                                dt.AcceptChanges();
                            }

                            RetenTempDataValue();
                        }
                    }
                    return NozzleSize;
                }
                else { return null; }
            }
            else { return null; }
        }

        //Unit /Measure Value
        public double? CalculateUnit(int rowid, string SeamCategory, int SeamLength, double minthickness, string SeamType)
        {
            string[] arrSeamTypeMinThk ={ clsImplementationEnum.AutoCJCSeamType.LW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.CW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.AW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.NP.GetStringValue()
                                         ,clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.BU.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.HF.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PTCLW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PTCCW.GetStringValue()};

            if (SeamLength > 0 && !string.IsNullOrWhiteSpace(SeamCategory))
            {

                if (arrSeamTypeMinThk.Contains(SeamType))
                {
                    double unit = (Convert.ToDouble(SeamLength) / 1000) * minthickness;
                    if (rowid > 0)
                    {
                        DataTable dt = (DataTable)TempData["FetchSeamList"];
                        if (dt != null)
                        {
                            DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                          where Convert.ToInt32(u["ROW_NO"]) == rowid
                                          select u).FirstOrDefault();

                            if (dr != null)
                            {
                                //changes the Column Value
                                dr["Unit"] = unit;
                                dt.AcceptChanges();
                            }
                        }
                        RetenTempDataValue();
                    }
                    return unit;
                }
                else if (SeamType == clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue())
                {
                    double unit = 0;
                    if (rowid > 0)
                    {
                        DataTable dt = (DataTable)TempData["FetchSeamList"];
                        if (dt != null)
                        {
                            DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                          where Convert.ToInt32(u["ROW_NO"]) == rowid
                                          select u).FirstOrDefault();

                            if (dr != null)
                            {
                                unit = Convert.ToDouble(dr["SeamQuantity"]);
                                //changes the Column Value
                                dr["Unit"] = dr["SeamQuantity"];
                                dt.AcceptChanges();
                            }
                        }
                        RetenTempDataValue();
                    }
                    return unit;
                }
                else if (SeamType == clsImplementationEnum.AutoCJCSeamType.HX.GetStringValue() || SeamType == clsImplementationEnum.AutoCJCSeamType.CR.GetStringValue() || SeamType == clsImplementationEnum.AutoCJCSeamType.OW.GetStringValue() ||
                    SeamType == clsImplementationEnum.AutoCJCSeamType.PTCOW.GetStringValue() || SeamType == clsImplementationEnum.AutoCJCSeamType.PTCBW.GetStringValue())
                {
                    double unit = (Convert.ToDouble(SeamLength) / 1000);
                    if (rowid > 0)
                    {
                        DataTable dt = (DataTable)TempData["FetchSeamList"];
                        if (dt != null)
                        {
                            DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                          where Convert.ToInt32(u["ROW_NO"]) == rowid
                                          select u).FirstOrDefault();

                            if (dr != null)
                            {
                                //changes the Column Value
                                dr["Unit"] = unit;
                                dt.AcceptChanges();
                            }
                        }
                        RetenTempDataValue();
                    }
                    return unit;
                }
                else { return null; }
            }
            else { return null; }
        }

        //runtime check calcuation is applicable or not
        public string SetUpRateCalculation(List<CJC011> objCJC011List, int rowid, bool isSetUpCleared, string SetupCJCNo, string HeaderCJCNo, string ActionType, string SetUpCJCStatus, string SeamCategory, double Minthk, int Jobdia, string typethickness, string nozzlecategory, double nozzlesize, string seamtype)
        {
            string result = "";
            string yes = clsImplementationEnum.yesno.Yes.GetStringValue();
            bool isSetupYes = false;


            CJC011 objCJC011 = null;
            if (objCJC011List != null)
                objCJC011 = objCJC011List.Where(x => x.SeamCategory == SeamCategory).FirstOrDefault();
            else
                objCJC011 = db.CJC011.Where(x => x.SeamCategory == SeamCategory).FirstOrDefault();

            if (objCJC011 != null)
                isSetupYes = objCJC011.Setup == yes ? true : false;

            bool IsCalculateSetUpRate = false;
            string returned = clsImplementationEnum.AutoCJCStatus.Returned.GetStringValue();
            string rejected = clsImplementationEnum.AutoCJCStatus.Rejected.GetStringValue();
            if (isSetUpCleared && isSetupYes)
            {
                if (string.IsNullOrWhiteSpace(SetupCJCNo))
                {
                    IsCalculateSetUpRate = true;
                }
                else
                {
                    if (ActionType == ActionEdit)
                    {
                        if (SetupCJCNo.Trim().ToLower() == HeaderCJCNo.Trim().ToLower())
                        {
                            IsCalculateSetUpRate = true;
                        }
                    }
                }
            }

            if (IsCalculateSetUpRate)
            {
                if (arrSetUpNonOverlayRate.Contains(SeamCategory) || arrSetUpMinThk.Contains(seamtype))
                {
                    var setuprate = CalculateSetUpNonOverlayRate(objCJC011List, SeamCategory, Minthk, Jobdia, typethickness, seamtype);
                    if (rowid > 0)
                    {
                        DataTable dt = (DataTable)TempData["FetchSeamList"];
                        if (dt != null)
                        {
                            DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                          where Convert.ToInt32(u["ROW_NO"]) == rowid
                                          select u).FirstOrDefault();

                            if (dr != null)
                            {
                                dr["SetupRate"] = setuprate;
                                dt.AcceptChanges();
                            }
                        }
                        RetenTempDataValue();
                    }
                    result = setuprate + "";
                }
                else if (arrSetUpNozzleSizeRate.Contains(SeamCategory))
                {
                    var setuprate = CalculateSetUpNozzleSizeRate(objCJC011List, SeamCategory, nozzlecategory, nozzlesize, typethickness, seamtype, Jobdia);
                    if (rowid > 0)
                    {
                        DataTable dt = (DataTable)TempData["FetchSeamList"];
                        if (dt != null)
                        {
                            DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                          where Convert.ToInt32(u["ROW_NO"]) == rowid
                                          select u).FirstOrDefault();

                            if (dr != null)
                            {
                                dr["SetupRate"] = setuprate;
                                dt.AcceptChanges();
                            }
                        }
                        RetenTempDataValue();
                    }
                    result = setuprate + "";
                }
            }

            return result;
        }

        public double? WeldRateCalculation(List<CJC011> objCJC011List, int rowid, bool isWeldCleared, string WeldCJCNo, string HeaderCJCNo, string ActionType, string WeldCJCStatus, string SeamCategory, double Minthk, int Jobdia, string weldprocess, string jointtype, string nozzlecategory, double nozzlesize, string seamtype)
        {
            double? result = null;
            bool IsCalculateWeldRate = false;
            if (isWeldCleared)
            {
                if (string.IsNullOrWhiteSpace(WeldCJCNo))
                {
                    IsCalculateWeldRate = true;
                }
                else
                {
                    if (ActionType == ActionEdit)
                    {
                        if (WeldCJCNo.Trim().ToLower() == HeaderCJCNo.Trim().ToLower())
                        {
                            IsCalculateWeldRate = true;
                        }
                    }
                }
            }

            if (IsCalculateWeldRate)
            {
                var weldingrate = CalculateWeldNonOverlayRate(objCJC011List, SeamCategory, Minthk, Jobdia, weldprocess, jointtype, nozzlesize, nozzlecategory, isWeldCleared, seamtype);
                if (rowid > 0)
                {
                    DataTable dt = (DataTable)TempData["FetchSeamList"];
                    if (dt != null)
                    {
                        DataRow dr = (from u in dt.Rows.Cast<DataRow>()
                                      where Convert.ToInt32(u["ROW_NO"]) == rowid
                                      select u).FirstOrDefault();

                        if (dr != null)
                        {
                            dr["WeldingRate"] = weldingrate;
                            dt.AcceptChanges();
                        }
                    }
                    RetenTempDataValue();
                }
                result = weldingrate;
            }
            return result;
        }

        public DataTable CalculateAndUpdateDataTable(DataTable dtSeams, string HeaderCJCNo, string ActionType, int Jobdia)
        {
            if (dtSeams != null && dtSeams.Rows.Count > 0)
            {
                var objCJC011List = db.CJC011.ToList();
                foreach (DataRow row in dtSeams.Rows)
                {
                    int RowId = Convert.ToInt32(row["ROW_NO"]);
                    string SeamCategory = Convert.ToString(row["SeamCategory"]);
                    string SeamType = Convert.ToString(row["SeamType"]);
                    int SeamLength = Convert.ToInt32(row["SeamLength"]);
                    string SeamJointType = Convert.ToString(row["SeamJointType"]);
                    double MinThickness = Convert.ToDouble(row["MinThickness"]);
                    string NozzleCategory = Convert.ToString(row["NozzleCategory"]);
                    string NozzleSize = Convert.ToString(row["NozzleSize"]);

                    bool IsSeamFetchedBySeamType = Convert.ToBoolean(row["IsSeamFetchedBySeamType"]);
                    bool IsSetUpCleared = Convert.ToBoolean(row["IsSetUpCleared"]);
                    bool IsPWHTCleared = Convert.ToBoolean(row["IsPWHTCleared"]);

                    string SetupCJCNo = Convert.ToString(row["SetupCJCNo"]);
                    string WeldCJCNo = Convert.ToString(row["WeldCJCNo"]);

                    string SetUpCJCStatus = Convert.ToString(row["SetUpCJCStatus"]);
                    string WeldCJCStatus = Convert.ToString(row["WeldCJCStatus"]);

                    string SetupRate = Convert.ToString(row["SetupRate"]);
                    string WeldingRate = Convert.ToString(row["WeldingRate"]);

                    string Amount = Convert.ToString(row["Amount"]);
                    string Factor = Convert.ToString(row["Factor"]);

                    string weldingProcess = Convert.ToString(row["WeldingProcess"]);
                    #region Nozzle Size 

                    if (IsSeamFetchedBySeamType)
                    {
                       row["NozzleSize"] = CalcultaionOfNozzleSize(0, SeamCategory, SeamType, SeamLength);
                    }

                    #endregion

                    #region Measured Value/Unit

                    if (IsSeamFetchedBySeamType)
                    {
                        if (SeamType == clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue())
                        {
                            double Part1Thickness = Convert.ToDouble(row["Part1Thickness"]);
                            double Part2Thickness = string.IsNullOrWhiteSpace(Convert.ToString(row["Part2Thickness"])) ? 0 : Convert.ToDouble(row["Part2Thickness"]);
                            double Part3Thickness = string.IsNullOrWhiteSpace(Convert.ToString(row["Part3Thickness"])) ? 0 : Convert.ToDouble(row["Part3Thickness"]);
                            double[] arrThickness = { Part1Thickness, Part2Thickness, Part3Thickness };
                            double maxVal = arrThickness.Max();
                            row["Unit"] = CalculateUnit(0, SeamCategory, SeamLength, maxVal, SeamType);
                        }
                        else
                        {
                            row["Unit"] = CalculateUnit(0, SeamCategory, SeamLength, MinThickness, SeamType);
                        }
                    }

                    #endregion

                    #region Setup Rate

                    if (IsSeamFetchedBySeamType && (string.IsNullOrWhiteSpace(SetupRate)))
                    {
                        row["SetupRate"] = SetUpRateCalculation(objCJC011List, 0, IsSetUpCleared, SetupCJCNo, HeaderCJCNo, ActionType, SetUpCJCStatus, SeamCategory, MinThickness, Jobdia, "", NozzleCategory, (string.IsNullOrWhiteSpace(NozzleSize) ? 0 : Convert.ToDouble(NozzleSize)), SeamType);
                    }

                    #endregion

                    #region Welding Rate

                    if (IsSeamFetchedBySeamType && (string.IsNullOrWhiteSpace(WeldingRate)))
                    {
                        row["WeldingRate"] = WeldRateCalculation(objCJC011List, 0, IsPWHTCleared, WeldCJCNo, HeaderCJCNo, ActionType, WeldCJCStatus, SeamCategory, MinThickness, Jobdia, weldingProcess, SeamJointType, NozzleCategory, (string.IsNullOrWhiteSpace(NozzleSize) ? 0 : Convert.ToDouble(NozzleSize)), SeamType);
                    }

                    #endregion

                    #region Amount 

                    if (IsSeamFetchedBySeamType && (string.IsNullOrWhiteSpace(Amount) || Amount == "0"))
                    {
                        row["Amount"] = claimSeamAmount(0, SeamCategory, SeamLength, (!string.IsNullOrWhiteSpace(Convert.ToString(row["Unit"])) ? Convert.ToDouble(row["Unit"]) : 0),
                                                                                     (!string.IsNullOrWhiteSpace(Convert.ToString(row["WeldingRate"])) ? Convert.ToDouble(row["WeldingRate"]) : 0),
                                                                                     (!string.IsNullOrWhiteSpace(Factor) ? Convert.ToDouble(Factor) : 0),
                                                                                     (!string.IsNullOrWhiteSpace(Convert.ToString(row["SetupRate"])) ? Convert.ToDouble(row["SetupRate"]) : 0),
                                                                                     (!string.IsNullOrWhiteSpace(Convert.ToString(row["NozzleSize"])) ? Convert.ToDouble(row["NozzleSize"]) : 0));
                    }

                    #endregion

                }
                dtSeams.AcceptChanges();
            }
            return dtSeams;
        }

        public bool IsCJCinBudget(string project, double claimamount, int dia)
        {
            bool flg = true;
            var objCJC016 = db.CJC016.Where(a => a.Project == project && a.ProductType == dia).OrderByDescending(x => x.EditedOn).FirstOrDefault();
            double? EstimatedValue = objCJC016?.EstimatedValue;
            double? claimamount1 = db.CJC030.Where(a => a.Project == project && (a.CJCNo != "" || a.CJCNo != null)).Sum(x => x.TotalCJCAmount);
            double? totalClaimAmt = claimamount1 + claimamount;
            if (EstimatedValue > 0 && totalClaimAmt > 0)
            {
                if (objCJC016.LockAt80Perc == true)
                {
                    int percentage = (int)Math.Round(Convert.ToDouble((100 * totalClaimAmt)) / Convert.ToDouble(EstimatedValue));
                    if (percentage > 80)
                    {
                        flg = false;
                    }
                }

                if (totalClaimAmt > EstimatedValue)
                { flg = false; }
            }
            return flg;
        }
        #endregion

        public DataSet ToAddSeamDetailsHeader(ExcelPackage package, int dia, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtLinesExcel = new DataTable();
            isError = false;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtLinesExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtLinesExcel.NewRow();

                    foreach (var cell in row)
                    {
                        newRow[cell.Start.Column - 1] = cell.Text;
                    }

                    dtLinesExcel.Rows.Add(newRow);
                }
                #endregion

                #region Error columns for Header Data

                #region Error columns for Header Data
                dtLinesExcel.Columns.Add("QualityProjectNoErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("SeamNoErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("SeamCategoryErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("NozzleSizeErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("SeamSubCategoryErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("SeamJointTypeErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("SeamLengthErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Part1PositionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Part2PositionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Part3PositionErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Part1ThicknessErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Part2ThicknessErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("Part3ThicknessErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("MinThicknessErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("MeasuredValueErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("WeldingProcessErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("SetupRateErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("WeldingRateErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("CriticalSeamErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("FactorErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("AmountErrorMsg", typeof(string));

                #endregion

                //dtLinesExcel.Columns.Add("NozzleSizeErrorMsg", typeof(string));
                //dtLinesExcel.Columns.Add("SeamSubCategoryErrorMsg", typeof(string));
                //dtLinesExcel.Columns.Add("WeldingProcessErrorMsg", typeof(string));
                ////dtLinesExcel.Columns.Add("CriticalSeamErrorMsg", typeof(string));

                #endregion

                #region Validate 

                foreach (DataRow item in dtLinesExcel.Rows)
                {
                    bool isBackGouging = false;
                    #region string variables
                    //List<string> lstWppNo = new List<string>();
                    //string seam = string.Empty;
                    //string errorMessage = string.Empty;
                    string QualityProjectNo = item.Field<string>("QualityProjectNo");
                    string SeamNo = item.Field<string>("Seam No");
                    string SeamCategory = item.Field<string>("SeamCategory");
                    string NozzleSize = item.Field<string>("NozzleSize");
                    string SeamSubCategory = item.Field<string>("SeamSubCategory");
                    string SeamJointType = item.Field<string>("SeamJointType");
                    string SeamLength = item.Field<string>("SeamLength");
                    string Part1Position = item.Field<string>("Part1Position");
                    string Part2Position = item.Field<string>("Part2Position");
                    string Part3Position = item.Field<string>("Part3Position");
                    string Part1Thickness = item.Field<string>("Part1Thickness");
                    string Part2Thickness = item.Field<string>("Part2Thickness");
                    string Part3Thickness = item.Field<string>("Part3Thickness");
                    string MinThickness = item.Field<string>("MinThickness");
                    string MeasuredValue = item.Field<string>("MeasuredValue");
                    string WeldingProcess = item.Field<string>("Welding Process");
                    string SetupRate = item.Field<string>("SetupRate");
                    string WeldingRate = item.Field<string>("WeldingRate");
                    string CriticalSeam = item.Field<string>("CriticalSeam");
                    string Factor = item.Field<string>("Factor");
                    string Amount = item.Field<string>("Amount");


                    string seamType = GetSeamCategory(QualityProjectNo, SeamNo);

                    #endregion

                    #region Validate header

                    //if (seamType == clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue())
                    //{
                    //    if (string.IsNullOrWhiteSpace(NozzleSize))
                    //    {
                    //        item["NozzleSizeErrorMsg"] = "NozzleSize is Required"; isError = true;
                    //    }
                    //}

                    if (seamType == clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue() ||
                        seamType == clsImplementationEnum.AutoCJCSeamType.AW.GetStringValue() ||
                        seamType == clsImplementationEnum.AutoCJCSeamType.PW.GetStringValue() ||
                        seamType == clsImplementationEnum.AutoCJCSeamType.NP.GetStringValue())
                    {
                        if (string.IsNullOrWhiteSpace(SeamSubCategory))
                        {
                            item["SeamSubCategoryErrorMsg"] = "SeamSubCategory is Required"; isError = true;
                        }
                        else
                        {
                            string SeamCategoryCode = GetCategoryCodeByDescription("Seam Category", SeamCategory).Code;

                            List<CategoryData> list = new List<CategoryData>();
                            list = db.CJC011.Where(x => x.SeamCategory == SeamCategoryCode && x.Dia == dia)
                                .Where(x => x.NozzleCategory != "" && x.NozzleCategory != null).
                                Select(x => new CategoryData { Value = x.NozzleCategory, CategoryDescription = x.NozzleCategory })
                                .Distinct().ToList();


                            if (!list.Any(x => SeamSubCategory.Contains(x.Value)))
                            {
                                item["SeamSubCategoryErrorMsg"] = SeamSubCategory + " is not valid"; isError = true;
                            }

                        }
                    }

                    if (seamType != clsImplementationEnum.AutoCJCSeamType.AW.GetStringValue() &&
                        seamType != clsImplementationEnum.AutoCJCSeamType.PW.GetStringValue() &&
                        seamType != clsImplementationEnum.AutoCJCSeamType.NP.GetStringValue() &&
                        seamType != clsImplementationEnum.AutoCJCSeamType.RA.GetStringValue())
                    {
                        if (string.IsNullOrWhiteSpace(WeldingProcess))
                        {
                            item["WeldingProcessErrorMsg"] = "WeldingProcess is Required"; isError = true;
                        }
                        else
                        {
                            if (string.IsNullOrWhiteSpace(GetCategoryCodeByDescription("Welding Process", WeldingProcess).Code))
                            {
                                item["WeldingProcessErrorMsg"] = WeldingProcess + " is not valid"; ; isError = true;
                            }
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(CriticalSeam))
                    {
                        //item["CriticalSeamErrorMsg"] = "CriticalSeam is Required"; isError = true;
                        if (string.IsNullOrWhiteSpace(GetCategoryCodeByDescription("Critical Seam", CriticalSeam).Code))
                        {
                            item["CriticalSeamErrorMsg"] = CriticalSeam + " is not valid"; ; isError = true;
                        }
                    }


                    #endregion
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtLinesExcel);
            return ds;
        }

        public clsHelper.ResponceMsgWithFileName ExportToOtherExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/CJC/Seam Detail - Error Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();

                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            #region Generate for HEader
                            ///WPP No.
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(21))) { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(21).ToString() + " )"; excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(22))) { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(22).ToString() + " )"; excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(23))) { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>(23).ToString() + " )"; excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(24))) { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>(24).ToString() + " )"; excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(25))) { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>(25).ToString() + " )"; excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(26))) { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>(26).ToString() + " )"; excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(27))) { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>(27).ToString() + " )"; excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(28))) { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7) + " (Note: " + item.Field<string>(28).ToString() + " )"; excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(29))) { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8) + " (Note: " + item.Field<string>(29).ToString() + " )"; excelWorksheet.Cells[i, 9].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(30))) { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9) + " (Note: " + item.Field<string>(30).ToString() + " )"; excelWorksheet.Cells[i, 10].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(31))) { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10) + " (Note: " + item.Field<string>(31).ToString() + " )"; excelWorksheet.Cells[i, 11].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(32))) { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11) + " (Note: " + item.Field<string>(32).ToString() + " )"; excelWorksheet.Cells[i, 12].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(33))) { excelWorksheet.Cells[i, 13].Value = item.Field<string>(12) + " (Note: " + item.Field<string>(33).ToString() + " )"; excelWorksheet.Cells[i, 13].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 13].Value = item.Field<string>(12); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(34))) { excelWorksheet.Cells[i, 14].Value = item.Field<string>(13) + " (Note: " + item.Field<string>(34).ToString() + " )"; excelWorksheet.Cells[i, 14].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 14].Value = item.Field<string>(13); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(35))) { excelWorksheet.Cells[i, 15].Value = item.Field<string>(14) + " (Note: " + item.Field<string>(35).ToString() + " )"; excelWorksheet.Cells[i, 15].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 15].Value = item.Field<string>(14); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(36))) { excelWorksheet.Cells[i, 16].Value = item.Field<string>(15) + " (Note: " + item.Field<string>(36).ToString() + " )"; excelWorksheet.Cells[i, 16].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 16].Value = item.Field<string>(15); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(37))) { excelWorksheet.Cells[i, 17].Value = item.Field<string>(16) + " (Note: " + item.Field<string>(37).ToString() + " )"; excelWorksheet.Cells[i, 17].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 17].Value = item.Field<string>(16); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(38))) { excelWorksheet.Cells[i, 18].Value = item.Field<string>(17) + " (Note: " + item.Field<string>(38).ToString() + " )"; excelWorksheet.Cells[i, 18].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 18].Value = item.Field<string>(17); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(39))) { excelWorksheet.Cells[i, 19].Value = item.Field<string>(18) + " (Note: " + item.Field<string>(39).ToString() + " )"; excelWorksheet.Cells[i, 19].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 19].Value = item.Field<string>(18); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(40))) { excelWorksheet.Cells[i, 20].Value = item.Field<string>(19) + " (Note: " + item.Field<string>(40).ToString() + " )"; excelWorksheet.Cells[i, 20].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 20].Value = item.Field<string>(19); }
                            #endregion

                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public string GetSeamCategory(string qualityProject, string seamNo)
        {
            return db.QMS012.Where(x => x.QualityProject == qualityProject && x.SeamNo == seamNo).Select(x => x.SeamCategory).FirstOrDefault();
        }

        public class ResponceMsgWithCJC : clsHelper.ResponseMsgWithStatus
        {
            public string SeamCategoryDesc;
            public string NozzleCategoryDesc;
            public string WeldingProcessDesc;
            public string CriticalSeamDesc;
            public double? TotalAmount;
        }

    }
}