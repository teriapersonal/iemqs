﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace IEMQS.Areas.CJC.Controllers
{
    public class MaintainPOforAutoCJCController : clsBase
    {
        // GET: CJC/MaintainPOforAutoCJC


        #region Index Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "Maintain PO for Auto Billing";

            return View();
        }

        //load tab wise partial
        [HttpPost]
        public ActionResult LoadDataGridPartial()
        {
            var yesno = clsImplementationEnum.getyesno().ToArray();
            ViewBag.AutoCJC = yesno.Select(x => new { CatID = x.ToString(), CatDesc = x.ToString() }).ToList();
            return PartialView("_LoadIndexGridDataPartial");
        }

        //bind datatable for Data Grid
        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition += "1=1 ";

                //search Condition 

                string[] columnName = { "PurchaseOdrerNo", "AutoCJCFlagdesc", "AutoCJCFlagdesc", "CreatedBy" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch.ToUpper());
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_CJC_GET_PO_FOR_AUTOCJC(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            uc.ROW_NO.ToString(),
                            Convert.ToString(uc.Id),
                            uc.PurchaseOdrerNo.ToString(),
                            (uc.AutoCJCFlagdesc),
                            uc.CreatedBy,
                            Convert.ToString(uc.CreatedOn.Value!=null ?uc.CreatedOn.Value.ToString("dd/MM/yyyy"):string.Empty),
                            Helper.HTMLActionString(uc.Id, "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + uc.Id + ");") + Helper.HTMLActionString(uc.Id, "Update", "Update Record", "fa fa-floppy-o", "SaveRecord(" + uc.Id + ");", "", false, "display:none") + Helper.HTMLActionString(uc.Id, "Cancel", "Cancel Edit", "fa fa-ban", "CancelEditLine(" + uc.Id + "); ", "", false, "display:none") + Helper.HTMLActionString(uc.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteLine(" + uc.Id + ");")
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool CheckDuplicate(int id, string po)
        {
            bool flg = false;
            if (db.CJC013.Where(x => x.PurchaseOdrerNo == po && x.Id != id).Any())
            { flg = true; }
            return flg;
        }
        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();
                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                         clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                          Helper.GenerateHidden(newRecordId, "Id", Convert.ToString(id)),
                          Helper.HTMLAutoComplete(newRecordId,"txtPurchaseOdrerNo","","",  false,"","PurchaseOdrerNo",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(newRecordId, "PurchaseOdrerNo", Convert.ToString(newRecordId)),
                          Helper.HTMLAutoComplete(newRecordId,"txtAutoCJCFlag","","",  false,"","AutoCJCFlag",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(newRecordId, "AutoCJCFlag", Convert.ToString(newRecordId)),
                          "","",
                          Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveRecord(0);"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_CJC_GET_PO_FOR_AUTOCJC(1, 0, "", "Id = " + id).ToList();

                    data = (from uc in lstResult
                            select new[]
                           {
                        clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                        Helper.GenerateHidden(uc.Id, "Id", Convert.ToString(uc.Id)),
                        isReadOnly? uc.PurchaseOdrerNo :Helper.HTMLAutoComplete(uc.Id, "txtPurchaseOdrerNo", uc.PurchaseOdrerNo, "",false,"","PurchaseOdrerNo",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(uc.Id, "PurchaseOdrerNo", Convert.ToString(uc.PurchaseOdrerNo)),
                        isReadOnly?uc.AutoCJCFlagdesc :Helper.HTMLAutoComplete(uc.Id, "txtAutoCJCFlag",uc.AutoCJCFlagdesc, "",false,"","AutoCJCFlag",false,"","","Select","clsMandatory")+""+Helper.GenerateHidden(uc.Id, "AutoCJCFlag", Convert.ToString(uc.AutoCJCFlag)),
                        uc.CreatedBy,
                        Convert.ToString(uc.CreatedOn.Value!=null ?uc.CreatedOn.Value.ToString("dd/MM/yyyy"):string.Empty),
                        Helper.HTMLActionString(uc.Id, "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + uc.Id + ");") + Helper.HTMLActionString(uc.Id, "Update", "Update Record", "fa fa-floppy-o", "SaveRecord(" + uc.Id + ");", "", false, "display:none") + Helper.HTMLActionString(uc.Id, "Cancel", "Cancel Edit", "fa fa-ban", "CancelEditLine(" + uc.Id + "); ", "", false, "display:none") + Helper.HTMLActionString(uc.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteLine(" + uc.Id + ");")
                          }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveLines(FormCollection fc)
        {
            CJC013 objCJC013 = objCJC013 = new CJC013();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int Id = Convert.ToInt32(fc["MainId"]);
                    string PurchaseOdrerNo = fc["PurchaseOdrerNo" + Id];
                    bool AutoCJCFlag = fc["AutoCJCFlag" + Id] == clsImplementationEnum.yesno.Yes.GetStringValue() ? true : false;
                    if (CheckDuplicate(Id, PurchaseOdrerNo))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (Id > 0)
                    {
                        objCJC013 = db.CJC013.Where(x => x.Id == Id).FirstOrDefault();
                        if (objCJC013 != null)
                        {
                            objCJC013.PurchaseOdrerNo = PurchaseOdrerNo;
                            objCJC013.AutoCJCFlag = AutoCJCFlag;
                            objCJC013.EditedBy = objClsLoginInfo.UserName;
                            objCJC013.EditedOn = DateTime.Now;
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();

                    }
                    else
                    {
                        objCJC013 = new CJC013();
                        objCJC013.PurchaseOdrerNo = PurchaseOdrerNo;
                        objCJC013.AutoCJCFlag = AutoCJCFlag;
                        objCJC013.CreatedBy = objClsLoginInfo.UserName;
                        objCJC013.CreatedOn = DateTime.Now;
                        db.CJC013.Add(objCJC013);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CJC013 objCJC013 = db.CJC013.Where(x => x.Id == Id).FirstOrDefault();
                if (objCJC013 != null)
                {
                    if (!db.CJC020.Any(x => x.PurchaseOdrerNo == objCJC013.PurchaseOdrerNo))
                    {
                        db.CJC013.Remove(objCJC013);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "CJC is generated for PO No. you are not allowed to delete.";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult GetOrderNo(string term)
        {
            if (!string.IsNullOrEmpty(term))
            {
                term = " Upper(t_orno) like '%" + term.ToUpper() + "%'";
            }
            var lstOrder = db.SP_COMMON_GET_LN_APPROVED_PO(1, int.MaxValue, "", term).ToList();
            var lstPurchaseOrder = (from a in lstOrder
                                        //where a.PONo.Contains(term)
                                    select new { Text = a.PONo, Value = a.PONo }).Distinct().Take(10).ToList();
            return Json(lstPurchaseOrder, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            clsHelper.ResponceMsgWithFileName objexport = new clsHelper.ResponceMsgWithFileName();
            try
            {
                string strFileName = string.Empty;

                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_CJC_GET_PROJECT_RATE_MATRIX_Result> lst = db.SP_CJC_GET_PROJECT_RATE_MATRIX(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      ProjectActivity = uc.ProjectActivityDesc.ToString(),
                                      UOM = uc.UOMDesc.ToString(),
                                      Rate = uc.Rate.ToString(),
                                      Dia = uc.DiaDesc.ToString(),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}