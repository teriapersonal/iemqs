﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.CJC.Controllers
{
    public class MaintainMasterController : clsBase
    {
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        #region Non Overlay Thickness

        [SessionExpireFilter]
        public ActionResult LoadNonOverlayThicknessGridDataPartial()
        {
            return PartialView("_LoadNonOverlayThicknessGridDataPartial");
        }

        public ActionResult LoadNonOverlayThicknessGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1";
                string[] columnName = { "Description", "MinValue", "MaxValue", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                var lstLines = db.SP_CJC_GET_NON_OVERLAY_THICKNESS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.Id),
                                        c.Description,
                                        c.MinValue.HasValue?Convert.ToString(c.MinValue):"",
                                        c.MaxValue.HasValue?Convert.ToString(c.MaxValue):"",
                                        c.CreatedBy,
                                        Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                        Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine1(" + c.Id + ");") + Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine11(" + c.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine11(" + c.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine1("+ c.Id +");"),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow1(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "Id", Convert.ToString(id)),
                                    Helper.GenerateHTMLTextbox(newRecordId, "txtDescriptionNonOverlay","", "", false, "", false,"15","",""),
                                    Helper.GenerateNumericTextbox(newRecordId, "txtMinValueNonOverlay",  "", "", false, "", "","number"),
                                    Helper.GenerateNumericTextbox(newRecordId, "txtMaxValueNonOverlay",  "", "",false, "", "","number"),
                                    "",
                                    "",
                                    Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord1();"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstLines = db.SP_CJC_GET_NON_OVERLAY_THICKNESS(1, 0, "", "Id = " + id).Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(c.Id, "Id", Convert.ToString(id)),
                                    isReadOnly ? c.Description : Helper.GenerateHTMLTextbox(c.Id, "txtDescriptionNonOverlay",c.Description, "",false, "", false,"15","",""),
                                    isReadOnly ? Convert.ToString(c.MinValue) : Helper.GenerateNumericTextbox(c.Id, "txtMinValueNonOverlay", c.MinValue.HasValue?c.MinValue.Value.ToString():"", "",false, "", "","number"),
                                    isReadOnly ? Convert.ToString(c.MaxValue) : Helper.GenerateNumericTextbox(c.Id, "txtMaxValueNonOverlay",  c.MaxValue.HasValue?c.MaxValue.Value.ToString():"", "", false, "", "","number"),
                                    c.CreatedBy,
                                    Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                    Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine1(" + c.Id + ");") + Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine1(" + c.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine1(" + c.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine1("+ c.Id +");"),
                        }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveData1(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int Id = Convert.ToInt32(fc["Id"]);

                    string Description = fc["txtDescriptionNonOverlay" + Id];

                    double? MinValue = null;
                    if (fc["txtMinValueNonOverlay" + Id] != null && fc["txtMinValueNonOverlay" + Id] != "")
                        MinValue = Convert.ToDouble(fc["txtMinValueNonOverlay" + Id]);

                    double? MaxValue = null;
                    if (fc["txtMaxValueNonOverlay" + Id] != null && fc["txtMaxValueNonOverlay" + Id] != "")
                        MaxValue = Convert.ToDouble(fc["txtMaxValueNonOverlay" + Id]);

                    CJC001 objCJC001 = null;
                    if (Id > 0)
                    {
                        objCJC001 = db.CJC001.Where(x => x.Id == Id).FirstOrDefault();
                        if (objCJC001 != null)
                        {
                            if (!db.CJC001.Any(x => x.Description == Description && x.Id != Id))
                            {
                                objCJC001.Description = Description;
                                objCJC001.MinValue = MinValue;
                                objCJC001.MaxValue = MaxValue;

                                objCJC001.EditedBy = objClsLoginInfo.UserName;
                                objCJC001.EditedOn = DateTime.Now;

                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                        }
                    }
                    else
                    {
                        if (!db.CJC001.Any(x => x.Description == Description))
                        {
                            objCJC001 = new CJC001();

                            objCJC001.Description = Description;
                            objCJC001.MinValue = MinValue;
                            objCJC001.MaxValue = MaxValue;
                            objCJC001.IsActive = true;

                            objCJC001.CreatedBy = objClsLoginInfo.UserName;
                            objCJC001.CreatedOn = DateTime.Now;

                            db.CJC001.Add(objCJC001);

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteData1(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CJC001 objCJC001 = db.CJC001.Where(x => x.Id == Id).FirstOrDefault();
                if (objCJC001 != null)
                {
                    db.CJC001.Remove(objCJC001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel1(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_CJC_GET_NON_OVERLAY_THICKNESS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var newlst = (from li in lst
                              select new
                              {
                                  Description = li.Description,
                                  MinimumValue = li.MinValue,
                                  MaximumValue = li.MaxValue,
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Nozzle

        [SessionExpireFilter]
        public ActionResult LoadNozzleGridDataPartial()
        {
            return PartialView("_LoadNozzleGridDataPartial");
        }

        public ActionResult LoadNozzleGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1";
                string[] columnName = { "Description", "MinValue", "MaxValue", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                var lstLines = db.SP_CJC_GET_NOZZLE_SIZE(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.Id),
                                        c.Description,
                                        c.MinValue.HasValue?Convert.ToString(c.MinValue):"",
                                        c.MaxValue.HasValue?Convert.ToString(c.MaxValue):"",
                                        c.CreatedBy,
                                        Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                        Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine2(" + c.Id + ");") + Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine2(" + c.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine2(" + c.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine2("+ c.Id +");"),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow2(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "Id", Convert.ToString(id)),
                                    Helper.GenerateHTMLTextbox(newRecordId, "txtDescriptionNozzle","", "", false, "", false,"15","",""),
                                    Helper.GenerateNumericTextbox(newRecordId, "txtMinValueNozzle",  "", "", false, "", "","number"),
                                    Helper.GenerateNumericTextbox(newRecordId, "txtMaxValueNozzle",  "", "",false, "", "","number"),
                                    "",
                                    "",
                                    Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord2();"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstLines = db.SP_CJC_GET_NOZZLE_SIZE(1, 0, "", "Id = " + id).Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(c.Id, "Id", Convert.ToString(id)),
                                    isReadOnly ? c.Description : Helper.GenerateHTMLTextbox(c.Id, "txtDescriptionNozzle",c.Description, "",false, "", false,"15","",""),
                                    isReadOnly ? Convert.ToString(c.MinValue) : Helper.GenerateNumericTextbox(c.Id, "txtMinValueNozzle", c.MinValue.HasValue?c.MinValue.Value.ToString():"", "",false, "", "","number"),
                                    isReadOnly ? Convert.ToString(c.MaxValue) : Helper.GenerateNumericTextbox(c.Id, "txtMaxValueNozzle",  c.MaxValue.HasValue?c.MaxValue.Value.ToString():"", "", false, "", "","number"),
                                    c.CreatedBy,
                                    Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                    Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine2(" + c.Id + ");") + Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine2(" + c.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine2(" + c.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine2("+ c.Id +");"),
                        }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveData2(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int Id = Convert.ToInt32(fc["Id"]);

                    string Description = fc["txtDescriptionNozzle" + Id];

                    double? MinValue = null;
                    if (fc["txtMinValueNozzle" + Id] != null && fc["txtMinValueNozzle" + Id] != "")
                        MinValue = Convert.ToDouble(fc["txtMinValueNozzle" + Id]);

                    double? MaxValue = null;
                    if (fc["txtMaxValueNozzle" + Id] != null && fc["txtMaxValueNozzle" + Id] != "")
                        MaxValue = Convert.ToDouble(fc["txtMaxValueNozzle" + Id]);

                    CJC002 objCJC002 = null;
                    if (Id > 0)
                    {
                        objCJC002 = db.CJC002.Where(x => x.Id == Id).FirstOrDefault();
                        if (objCJC002 != null)
                        {
                            if (!db.CJC002.Any(x => x.Description == Description && x.Id != Id))
                            {
                                objCJC002.Description = Description;
                                objCJC002.MinValue = MinValue;
                                objCJC002.MaxValue = MaxValue;

                                objCJC002.EditedBy = objClsLoginInfo.UserName;
                                objCJC002.EditedOn = DateTime.Now;

                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                        }
                    }
                    else
                    {
                        if (!db.CJC002.Any(x => x.Description == Description))
                        {
                            objCJC002 = new CJC002();

                            objCJC002.Description = Description;
                            objCJC002.MinValue = MinValue;
                            objCJC002.MaxValue = MaxValue;
                            objCJC002.IsActive = true;

                            objCJC002.CreatedBy = objClsLoginInfo.UserName;
                            objCJC002.CreatedOn = DateTime.Now;

                            db.CJC002.Add(objCJC002);

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteData2(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CJC002 objCJC002 = db.CJC002.Where(x => x.Id == Id).FirstOrDefault();
                if (objCJC002 != null)
                {
                    db.CJC002.Remove(objCJC002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel2(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_CJC_GET_NOZZLE_SIZE(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var newlst = (from li in lst
                              select new
                              {
                                  Description = li.Description,
                                  MinimumValue = li.MinValue,
                                  MaximumValue = li.MaxValue,
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Job Dia

        [SessionExpireFilter]
        public ActionResult LoadJobDiaGridDataPartial()
        {
            return PartialView("_LoadJobDiaGridDataPartial");
        }

        public ActionResult LoadJobDiaGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1";
                string[] columnName = { "Description", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                var lstLines = db.SP_CJC_GET_JOB_DIA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.Id),
                                        c.Description,
                                        //c.MinValue.HasValue?Convert.ToString(c.MinValue):"",
                                        //c.MaxValue.HasValue?Convert.ToString(c.MaxValue):"",
                                        c.CreatedBy,
                                        Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                        Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine3(" + c.Id + ");") + Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine3(" + c.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine3(" + c.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine3("+ c.Id +");"),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow3(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "Id", Convert.ToString(id)),
                                    Helper.GenerateHTMLTextbox(newRecordId, "txtDescriptionJobDia","", "", false, "", false,"50","",""),
                                    //Helper.GenerateNumericTextbox(newRecordId, "txtMinValueJobDia",  "", "", false, "", "","number"),
                                    //Helper.GenerateNumericTextbox(newRecordId, "txtMaxValueJobDia",  "", "",false, "", "","number"),
                                    "",
                                    "",
                                    Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord3();"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstLines = db.SP_CJC_GET_JOB_DIA(1, 0, "", "Id = " + id).Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(c.Id, "Id", Convert.ToString(id)),
                                    isReadOnly ? c.Description : Helper.GenerateHTMLTextbox(c.Id, "txtDescriptionJobDia",c.Description, "",false, "", false,"50","",""),
                                    //isReadOnly ? Convert.ToString(c.MinValue) : Helper.GenerateNumericTextbox(c.Id, "txtMinValueJobDia", c.MinValue.HasValue?c.MinValue.Value.ToString():"", "",false, "", "","number"),
                                    //isReadOnly ? Convert.ToString(c.MaxValue) : Helper.GenerateNumericTextbox(c.Id, "txtMaxValueJobDia",  c.MaxValue.HasValue?c.MaxValue.Value.ToString():"", "", false, "", "","number"),
                                    c.CreatedBy,
                                    Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                    Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine3(" + c.Id + ");") + Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine3(" + c.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine3(" + c.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine3("+ c.Id +");"),
                        }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveData3(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int Id = Convert.ToInt32(fc["Id"]);

                    string Description = fc["txtDescriptionJobDia" + Id];

                    double? MinValue = null;
                    if (fc["txtMinValueJobDia" + Id] != null && fc["txtMinValueJobDia" + Id] != "")
                        MinValue = Convert.ToDouble(fc["txtMinValueJobDia" + Id]);

                    double? MaxValue = null;
                    if (fc["txtMaxValueJobDia" + Id] != null && fc["txtMaxValueJobDia" + Id] != "")
                        MaxValue = Convert.ToDouble(fc["txtMaxValueJobDia" + Id]);

                    CJC003 objCJC003 = null;
                    if (Id > 0)
                    {
                        objCJC003 = db.CJC003.Where(x => x.Id == Id).FirstOrDefault();
                        if (objCJC003 != null)
                        {
                            if (!db.CJC003.Any(x => x.Description == Description && x.Id != Id))
                            {
                                objCJC003.Description = Description;
                                //objCJC003.MinValue = MinValue;
                                //objCJC003.MaxValue = MaxValue;

                                objCJC003.EditedBy = objClsLoginInfo.UserName;
                                objCJC003.EditedOn = DateTime.Now;

                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                        }
                    }
                    else
                    {
                        if (!db.CJC003.Any(x => x.Description == Description))
                        {
                            objCJC003 = new CJC003();

                            objCJC003.Description = Description;
                            //objCJC003.MinValue = MinValue;
                            //objCJC003.MaxValue = MaxValue;
                            objCJC003.IsActive = true;

                            objCJC003.CreatedBy = objClsLoginInfo.UserName;
                            objCJC003.CreatedOn = DateTime.Now;

                            db.CJC003.Add(objCJC003);

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteData3(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CJC003 objCJC003 = db.CJC003.Where(x => x.Id == Id).FirstOrDefault();
                if (objCJC003 != null)
                {
                    db.CJC003.Remove(objCJC003);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel3(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_CJC_GET_JOB_DIA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var newlst = (from li in lst
                              select new
                              {
                                  ProductName = li.Description,
                                  //MinimumValue = li.MinValue,
                                  //MaximumValue = li.MaxValue,
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}