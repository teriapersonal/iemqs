﻿using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.CJC.Controllers
{
    public class MaintainSeamRateController : clsBase
    {
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        public PartialViewResult LoadIndexGridDataPartial()
        {
            List<CategoryData> lstSeamCategory = Manager.GetSubCatagorywithoutBULocation("Seam Category").ToList();
            List<string> lstSeamType = clsImplementationEnum.GetSeamTypeForCJC().OrderBy(x => x.ToString()).ToList();
            List<CategoryData> lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();
            //List<CategoryData> lstNozzleCategory = Manager.GetSubCatagorywithoutBULocation("Nozzle Category").ToList();

            List<CategoryData> lstNozzleCategoryNW = new List<CategoryData>();
            lstNozzleCategoryNW.Add(new CategoryData { Value = "Radial", CategoryDescription = "Radial" });
            lstNozzleCategoryNW.Add(new CategoryData { Value = "Angular", CategoryDescription = "Angular" });
            lstNozzleCategoryNW.Add(new CategoryData { Value = "Offset", CategoryDescription = "Offset" });
            lstNozzleCategoryNW.Add(new CategoryData { Value = "With Pad", CategoryDescription = "With Pad" });
            lstNozzleCategoryNW.Add(new CategoryData { Value = "SS Nozzle", CategoryDescription = "SS Nozzle" });

            List<CategoryData> lstNozzleCategoryAWPWNP = new List<CategoryData>();
            lstNozzleCategoryAWPWNP.Add(new CategoryData { Value = "External", CategoryDescription = "External" });
            lstNozzleCategoryAWPWNP.Add(new CategoryData { Value = "Internal", CategoryDescription = "Internal" });

            ViewBag.SeamCategoryList = lstSeamCategory.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();
            //ViewBag.NozzleCategoryList = lstNozzleCategory.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();
            ViewBag.WeldingProcessList = lstWeldingProcess.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();
            ViewBag.SeamTypeList = lstSeamType.AsEnumerable().Select(x => new { CatID = x.ToString(), CatDesc = x.ToString() }).ToList();
            ViewBag.NozzleCategoryNWList = lstNozzleCategoryNW.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();
            ViewBag.NozzleCategoryAWPWNPList = lstNozzleCategoryAWPWNP.AsEnumerable().Select(x => new { CatID = x.Value, CatDesc = x.CategoryDescription }).ToList();

            ViewBag.NonOverlayThicknessList = (from u in db.CJC001
                                               where u.IsActive == true
                                               select new { CatID = u.Id, CatDesc = u.Description }).ToList();

            ViewBag.NozzleSizeList = (from u in db.CJC002
                                      where u.IsActive == true
                                      select new { CatID = u.Id, CatDesc = u.Description }).ToList();

            ViewBag.DiaList = (from u in db.CJC003
                               where u.IsActive == true
                               select new { CatID = u.Id, CatDesc = u.Description }).ToList();

            ViewBag.WeldTypeList = (from c in db.BOM003
                                    group c by new
                                    {
                                        c.JointTypeCode,
                                        c.JointTypeDescription,
                                    } into gcs
                                    select new
                                    {
                                        CatID = gcs.Key.JointTypeCode,
                                        CatDesc = gcs.Key.JointTypeDescription,
                                    }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoList = (from u in lstYesNoNA
                                 select new { CatID = u, CatDesc = u }).ToList();

            return PartialView("_LoadIndexGridDataPartial");
        }

        public ActionResult GetIndexGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1";
                string[] columnName = { "SeamCategoryDesc", "SeamType", "WeldingProcessDesc", "NozzleCategoryDesc", "NonOverlayThicknessDesc", "NozzleSizeDesc", "Rate", "DiaDesc", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                var lstLines = db.SP_CJC_GET_SEAM_RATE_MATRIX(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.Id),
                                        c.SeamCategoryDesc,
                                        c.SeamType,
                                        c.WeldTypeDesc,
                                        c.Setup,
                                        c.NonOverlayThicknessDesc,
                                        c.NozzleSizeDesc,
                                        c.WeldingProcessDesc,
                                        c.NozzleCategory,
                                        Convert.ToString(c.Rate),
                                        c.DiaDesc,
                                        c.CreatedBy,
                                        Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                        Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.Id + ");") + Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.Id +");"),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();
                string[] arr_NC = { clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.AW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.PW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.NP.GetStringValue() };
                string[] arr_WP_NW = { clsImplementationEnum.AutoCJCSeamType.NW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.CW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.LW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.OW.GetStringValue(), clsImplementationEnum.AutoCJCSeamType.CR.GetStringValue(),
                                       clsImplementationEnum.AutoCJCSeamType.HX.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PTCLW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PTCCW.GetStringValue(),clsImplementationEnum.AutoCJCSeamType.PTCOW.GetStringValue(),
                                       clsImplementationEnum.AutoCJCSeamType.PTCBW.GetStringValue()};

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "Id", Convert.ToString(id)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtSeamCategory","","",  false,"","SeamCategory",false) +""+Helper.GenerateHidden(newRecordId, "SeamCategory", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtSeamType","","",  false,"","SeamType",false) +""+Helper.GenerateHidden(newRecordId, "SeamType", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtWeldType","","",  false,"","WeldType",false)+""+Helper.GenerateHidden(newRecordId, "WeldType", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtSetup","","",  false,"","Setup",false)+""+Helper.GenerateHidden(newRecordId, "Setup", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtNonOverlayThickness","","",  false,"","NonOverlayThickness",false)+""+Helper.GenerateHidden(newRecordId, "NonOverlayThickness", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtNozzleSize","","", false,"","NozzleSize",false)+""+Helper.GenerateHidden(newRecordId, "NozzleSize", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtWeldingProcess","","", false,"","WeldingProcess",false)+""+Helper.GenerateHidden(newRecordId, "WeldingProcess", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtNozzleCategory","", "", false,"","NozzleCategory",true)+""+Helper.GenerateHidden(newRecordId, "NozzleCategory", Convert.ToString(newRecordId)),
                                    Helper.GenerateNumericTextbox(newRecordId, "txtRate",  "", "", false, "", ""),
                                    Helper.HTMLAutoComplete(newRecordId, "txtDia","","", false,"","Dia",false)+""+Helper.GenerateHidden(newRecordId, "Dia", Convert.ToString(newRecordId)),
                                    "",
                                    "",
                                     Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstLines = db.SP_CJC_GET_SEAM_RATE_MATRIX(1, 0, "", "Id = " + id).Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.ROW_NO),
                                    Helper.GenerateHidden(c.Id, "Id", Convert.ToString(id)),
                                    isReadOnly ? c.SeamCategoryDesc : Helper.HTMLAutoComplete(c.Id, "txtSeamCategory",WebUtility.HtmlEncode(c.SeamCategoryDesc),"", false,"","SeamCategory",false)+""+Helper.GenerateHidden(c.Id, "SeamCategory",Convert.ToString(c.SeamCategory)),
                                    isReadOnly ? c.SeamType : Helper.HTMLAutoComplete(c.Id, "txtSeamType",WebUtility.HtmlEncode(c.SeamType),"", false,"","SeamType",false)+""+Helper.GenerateHidden(c.Id, "SeamType",Convert.ToString(c.SeamType)),
                                    isReadOnly ? c.WeldTypeDesc : Helper.HTMLAutoComplete(c.Id, "txtWeldType",c.WeldTypeDesc,"", false,"","WeldType",false)+""+Helper.GenerateHidden(c.Id, "WeldType",Convert.ToString(c.WeldType)),
                                    isReadOnly ? c.Setup : Helper.HTMLAutoComplete(c.Id, "txtSetup",c.Setup,"", false,"","Setup",false)+""+Helper.GenerateHidden(c.Id, "Setup",Convert.ToString(c.Setup)),
                                    isReadOnly ? c.NonOverlayThicknessDesc : Helper.HTMLAutoComplete(c.Id, "txtNonOverlayThickness",c.NonOverlayThicknessDesc,"", false,"","NonOverlayThickness",false)+""+Helper.GenerateHidden(c.Id, "NonOverlayThickness", Convert.ToString(c.NonOverlayThickness)),
                                    isReadOnly ? c.NozzleSizeDesc : Helper.HTMLAutoComplete(c.Id,"txtNozzleSize",c.NozzleSizeDesc,"",false,"","NozzleSize",false)+""+Helper.GenerateHidden(c.Id, "NozzleSize", Convert.ToString(c.NozzleSize)),
                                    isReadOnly ? c.WeldingProcessDesc : Helper.HTMLAutoComplete(c.Id,"txtWeldingProcess",c.WeldingProcessDesc,"", false,"","WeldingProcess",!arr_WP_NW.Contains(c.SeamType))+""+Helper.GenerateHidden(c.Id, "WeldingProcess", Convert.ToString(c.WeldingProcess)),
                                    isReadOnly ? c.NozzleCategory : Helper.HTMLAutoComplete(c.Id,"txtNozzleCategory",c.NozzleCategory,"", false,"","NozzleCategory",!arr_NC.Contains(c.SeamType))+""+Helper.GenerateHidden(c.Id, "NozzleCategory", Convert.ToString(c.NozzleCategory)),
                                    isReadOnly ? Convert.ToString(c.Rate) : Helper.GenerateNumericTextbox(c.Id,"txtRate",  c.Rate.HasValue?c.Rate.Value.ToString():"", "", false, "", ""),
                                    isReadOnly ? c.DiaDesc : Helper.HTMLAutoComplete(c.Id, "txtDia",c.DiaDesc,"", false,"","Dia",false)+""+Helper.GenerateHidden(c.Id, "Dia", Convert.ToString(c.Dia)),
                                    c.CreatedBy,
                                    Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                    Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.Id + ");") + Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.Id +");"),
                        }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveData(FormCollection fc, int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    string SeamCategory = fc["SeamCategory" + Id];
                    string WeldingProcess = fc["WeldingProcess" + Id];
                    string NozzleCategory = fc["NozzleCategory" + Id];
                    string WeldType = fc["WeldType" + Id];
                    string Setup = fc["Setup" + Id];
                    string SeamType = fc["SeamType" + Id];

                    int? NonOverlayThickness = null;
                    if (fc["NonOverlayThickness" + Id] != null && fc["NonOverlayThickness" + Id] != "")
                        NonOverlayThickness = Convert.ToInt32(fc["NonOverlayThickness" + Id]);

                    int? NozzleSize = null;
                    if (fc["NonOverlayThickness" + Id] != null && fc["NozzleSize" + Id] != "")
                        NozzleSize = Convert.ToInt32(fc["NozzleSize" + Id]);

                    double? Rate = null;
                    if (fc["txtRate" + Id] != null && fc["txtRate" + Id] != "")
                        Rate = Convert.ToDouble(fc["txtRate" + Id]);

                    int? Dia = null;
                    if (fc["Dia" + Id] != null && fc["Dia" + Id] != "")
                        Dia = Convert.ToInt32(fc["Dia" + Id]);

                    CJC011 objCJC011 = null;
                    if (Id > 0)
                    {
                        if (!db.CJC011.Any(x => x.SeamCategory == SeamCategory && x.NonOverlayThickness == NonOverlayThickness && x.NozzleCategory == NozzleCategory && x.NozzleSize == NozzleSize
                                                  && x.WeldingProcess == WeldingProcess && x.Dia == Dia && x.WeldType == WeldType && x.Id != Id))
                        {
                            objCJC011 = db.CJC011.Where(x => x.Id == Id).FirstOrDefault();
                            if (objCJC011 != null)
                            {
                                objCJC011.SeamCategory = SeamCategory;
                                objCJC011.WeldingProcess = WeldingProcess;
                                objCJC011.NozzleCategory = NozzleCategory;
                                objCJC011.NonOverlayThickness = NonOverlayThickness;
                                objCJC011.NozzleSize = NozzleSize;
                                objCJC011.Rate = Rate;
                                objCJC011.Dia = Dia;
                                objCJC011.WeldType = WeldType;
                                objCJC011.Setup = Setup;
                                objCJC011.SeamType = SeamType;
                                objCJC011.EditedBy = objClsLoginInfo.UserName;
                                objCJC011.EditedOn = DateTime.Now;

                                db.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                    else
                    {
                        if (!db.CJC011.Any(x => x.SeamCategory == SeamCategory && x.NonOverlayThickness == NonOverlayThickness && x.NozzleCategory == NozzleCategory && x.NozzleSize == NozzleSize
                                                && x.WeldingProcess == WeldingProcess && x.Dia == Dia && x.WeldType == WeldType))
                        {
                            objCJC011 = new CJC011();

                            objCJC011.SeamCategory = SeamCategory;
                            objCJC011.WeldingProcess = WeldingProcess;
                            objCJC011.NozzleCategory = NozzleCategory;
                            objCJC011.NonOverlayThickness = NonOverlayThickness;
                            objCJC011.NozzleSize = NozzleSize;
                            objCJC011.Rate = Rate;
                            objCJC011.Dia = Dia;
                            objCJC011.WeldType = WeldType;
                            objCJC011.Setup = Setup;
                            objCJC011.SeamType = SeamType;
                            objCJC011.CreatedBy = objClsLoginInfo.UserName;
                            objCJC011.CreatedOn = DateTime.Now;

                            db.CJC011.Add(objCJC011);

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteData(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CJC011 objCJC011 = db.CJC011.Where(x => x.Id == Id).FirstOrDefault();
                if (objCJC011 != null)
                {
                    db.CJC011.Remove(objCJC011);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_CJC_GET_SEAM_RATE_MATRIX(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  SeamCategory = li.SeamCategoryDesc,
                                  SeamType = li.SeamType,
                                  WeldType = li.WeldTypeDesc,
                                  Setup = li.Setup,
                                  NonOverlayThickness = li.NonOverlayThicknessDesc,
                                  NozzleSize = li.NozzleSizeDesc,
                                  WeldingProcess = li.WeldingProcessDesc,
                                  NozzleCategory = li.NozzleCategory,
                                  Rate = li.Rate,
                                  ProductType = li.DiaDesc,
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }



        public ActionResult ReadExcelFile()
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (Request.Files.Count == 0)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please select a file first!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

            HttpPostedFileBase upload = Request.Files[0];

            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                //DataTable dt = Manager.ExcelToDataTable(package);
                bool isError;
                DataSet ds = ToAddSeamDetailsHeader(package, out isError);
                if (!isError)
                {
                    var seamCategoryList = Manager.GetSubCatagorywithoutBULocation("Seam Category").ToList();


                    var weldTypeList = (from c in db.BOM003
                                        group c by new
                                        {
                                            c.JointTypeCode,
                                            c.JointTypeDescription,
                                        } into gcs
                                        select new
                                        {
                                            CatID = gcs.Key.JointTypeCode,
                                            CatDesc = gcs.Key.JointTypeDescription,
                                        }).ToList();


                    var nonOverlayThicknessList = (from u in db.CJC001
                                                   where u.IsActive == true
                                                   select new { CatID = u.Id, CatDesc = u.Description }).ToList();


                    var nozzleSizeList = (from u in db.CJC002
                                          where u.IsActive == true
                                          select new { CatID = u.Id, CatDesc = u.Description }).ToList();

                    var weldingProcessList = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();



                    //dia is displayed as ProductType in grid.
                    var diaList = (from u in db.CJC003
                                   where u.IsActive == true
                                   select new { CatID = u.Id, CatDesc = u.Description }).ToList();


                    List<DataRow> dtImported = (from u in ds.Tables[0].Rows.Cast<DataRow>()
                                                select u).ToList();

                    foreach (DataRow dr in dtImported)
                    {
                        try
                        {
                            string SeamCategory = "";
                            if (!string.IsNullOrEmpty(dr["SeamCategory"].ToString()))
                            {
                                var tempSeamCategory = seamCategoryList.Where(x => x.CategoryDescription == dr["SeamCategory"].ToString()).FirstOrDefault();
                                if (tempSeamCategory != null)
                                    SeamCategory = tempSeamCategory.Value;
                            }

                            string WeldingProcess = "";
                            if (!string.IsNullOrEmpty(dr["Welding Process"].ToString()))
                            {
                                var tempWeldingProcess = weldingProcessList.Where(x => x.CategoryDescription == dr["Welding Process"].ToString()).FirstOrDefault();
                                if (tempWeldingProcess != null)
                                    WeldingProcess = tempWeldingProcess.Value;
                            }


                            string NozzleCategory = dr["NozzleCategory"].ToString();

                            string WeldType = "";
                            if (!string.IsNullOrEmpty(dr["WeldType"].ToString().Trim()))
                            {
                                var a = dr["WeldType"].ToString();
                                var b = dr["WeldType"].ToString().Trim();

                                var tempWeldType = weldTypeList.Where(x => x.CatDesc == dr["WeldType"].ToString().Trim()).FirstOrDefault();
                                if (tempWeldType != null)
                                    WeldType = tempWeldType.CatID;
                            }


                            string Setup = dr["Setup"].ToString();
                            string SeamType = dr["SeamType"].ToString();

                            int? NonOverlayThickness = null;
                            if (dr["NonOverlayThickness"] != null && dr["NonOverlayThickness"].ToString() != "")
                            {
                                var tempNonOverlayThickness = nonOverlayThicknessList.Where(x => x.CatDesc == dr["NonOverlayThickness"].ToString()).FirstOrDefault();
                                if (tempNonOverlayThickness != null)
                                    NonOverlayThickness = tempNonOverlayThickness.CatID;
                            }

                            int? NozzleSize = null;
                            if (dr["NozzleSize"] != null && dr["NozzleSize"].ToString() != "")
                            {
                                var tempNozzleSize = nozzleSizeList.Where(x => x.CatDesc == dr["NozzleSize"].ToString()).FirstOrDefault();
                                if (tempNozzleSize != null)
                                    NozzleSize = tempNozzleSize.CatID;
                            }

                            double? Rate = null;
                            if (dr["Rate"] != null && dr["Rate"].ToString() != "")
                                Rate = Convert.ToDouble(dr["Rate"]);

                            int? Dia = null;
                            if (dr["ProductType"] != null && dr["ProductType"].ToString() != "")
                            {
                                var tempDia = diaList.Where(x => x.CatDesc == dr["ProductType"].ToString()).FirstOrDefault();
                                if (tempDia != null)
                                    Dia = tempDia.CatID;
                            }


                            CJC011 objCJC011 = db.CJC011.Where(x => x.SeamCategory == SeamCategory
                                                        && x.SeamType == SeamType
                                                        && x.WeldType == WeldType
                                                        && x.NonOverlayThickness == NonOverlayThickness
                                                        && x.NozzleSize == NozzleSize
                                                        && x.WeldingProcess == WeldingProcess
                                                        && x.NozzleCategory == NozzleCategory
                                                        && x.Dia == Dia
                                                        ).FirstOrDefault();

                            if (objCJC011 == null)
                            {
                                objCJC011 = new CJC011();

                                objCJC011.SeamCategory = SeamCategory;
                                objCJC011.WeldingProcess = WeldingProcess;
                                objCJC011.NozzleCategory = NozzleCategory;
                                objCJC011.NonOverlayThickness = NonOverlayThickness;
                                objCJC011.NozzleSize = NozzleSize;
                                objCJC011.Rate = Rate;
                                objCJC011.Dia = Dia;
                                objCJC011.WeldType = WeldType;
                                objCJC011.Setup = Setup;
                                objCJC011.SeamType = SeamType;
                                objCJC011.CreatedBy = objClsLoginInfo.UserName;
                                objCJC011.CreatedOn = DateTime.Now;

                                db.CJC011.Add(objCJC011);
                            }
                            else
                            {
                                objCJC011.SeamCategory = SeamCategory;
                                objCJC011.WeldingProcess = WeldingProcess;
                                objCJC011.NozzleCategory = NozzleCategory;
                                objCJC011.NonOverlayThickness = NonOverlayThickness;
                                objCJC011.NozzleSize = NozzleSize;
                                objCJC011.Rate = Rate;
                                objCJC011.Dia = Dia;
                                objCJC011.WeldType = WeldType;
                                objCJC011.Setup = Setup;
                                objCJC011.SeamType = SeamType;
                                objCJC011.EditedBy = objClsLoginInfo.UserName;
                                objCJC011.EditedOn = DateTime.Now;
                            }

                            db.SaveChanges();
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Seam rate list import successfully";

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please upload valid excel file!";
                }

                //return Json("");//validateAndSave(dt, HeaderId);
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please upload valid excel file!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        public DataSet ToAddSeamDetailsHeader(ExcelPackage package, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtLinesExcel = new DataTable();
            isError = false;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtLinesExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtLinesExcel.NewRow();

                    foreach (var cell in row)
                    {
                        newRow[cell.Start.Column - 1] = cell.Text;
                    }

                    dtLinesExcel.Rows.Add(newRow);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtLinesExcel);
            return ds;
        }




    }
}