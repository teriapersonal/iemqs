﻿using System.Web.Mvc;

namespace IEMQS.Areas.PLN
{
    public class PLNAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PLN";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PLN_default",
                "PLN/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}