﻿using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PLN.Controllers
{
    public class ApproveStoolPlanController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadStoolPlanListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_StoolPlanListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadStoolPlanHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue() + "') and pln017.ApprovedBy in('" + objClsLoginInfo.UserName + "')";
                }
                else
                {
                    strWhere += "1=1";
                }                
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln017.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln017.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var strSortOrder = "";
                var lstResult = db.SP_FETCH_STOOLPLAN_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               uc.CreatedOn.HasValue ?  Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.EditedBy),
                               uc.EditedOn.HasValue ?  Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.SubmittedBy),
                               uc.SubmittedOn.HasValue ?  Convert.ToString(uc.SubmittedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ApprovedBy),
                               uc.ApprovedOn.HasValue ?  Convert.ToString(uc.ApprovedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ReturnRemark),
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.HeaderId)
                                //"<center><a title='View' href='/PLN/ApproveStoolPlan/ApproveStoolPlan?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ApproveStoolPlan(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN017");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN017 objPLN017 = db.PLN017.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN017.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadStoolPlanCreateFormPartial(int HeaderID = 0)
        {
            PLN017 objPLN017 = db.PLN017.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN017.ApprovedBy != null)
            {
                if (objPLN017.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                {
                    ViewBag.IsDisplayMode = true;
                }
                else
                {
                    ViewBag.IsDisplayMode = false;
                }
            }
            else
            {
                ViewBag.IsDisplayMode = true;
            }
            if (objPLN017 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN017.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN017.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN017.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN017.ApprovedBy = objPLN017.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN017.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN017.Customer = objPLN017.Customer + " - " + customerName;
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN017/" + objPLN017.HeaderId + "/" + objPLN017.RevNo));

                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN017.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, HeaderID, clsImplementationEnum.PlanList.Stool_Plan.GetStringValue());
            }
            else
            {
                objPLN017 = new PLN017();
                objPLN017.ProcessPlan = clsImplementationEnum.PlanList.Stool_Plan.GetStringValue();
                objPLN017.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN017.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadStoolPlanCreateFormPartial", objPLN017);
        }

        [HttpPost]
        public ActionResult ReturnStoolPlan(int HeaderId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN017 objPLN017 = db.PLN017.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN017 != null)
                {
                    objPLN017.Status = clsImplementationEnum.PlanStatus.Returned.GetStringValue();
                    objPLN017.ReturnRemark = Remarks;
                    objPLN017.EditedBy = objClsLoginInfo.UserName;
                    objPLN017.EditedOn = DateTime.Now;



                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Plan successfully returned.";
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN017.HeaderId, objPLN017.Status, objPLN017.RevNo, objPLN017.Project, objPLN017.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN017.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN017.Project, clsImplementationEnum.PlanList.Stool_Plan.GetStringValue(), objPLN017.RevNo.Value.ToString(), objPLN017.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Stool_Plan.GetStringValue(), objPLN017.HeaderId.ToString(), false),
                                                        objPLN017.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please add remarks for returning Plan.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            for (int i = 0; i < arrayHeaderIds.Length; i++)
            {
                int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg ApproveHeaderByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN017 objPLN017 = db.PLN017.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN017 != null)
                {
                    objPLN017.Status = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                    objPLN017.ApprovedBy = objClsLoginInfo.UserName;
                    objPLN017.ApprovedOn = DateTime.Now;



                    #region Header Log                    
                    var lstPLN017_Log = db.PLN017_Log.Where(x => x.HeaderId == objPLN017.HeaderId).ToList();
                    PLN017_Log objPLN017_Log = new PLN017_Log();
                    if (objPLN017_Log != null)
                    {
                        objPLN017_Log = lstPLN017_Log.FirstOrDefault();
                        lstPLN017_Log.ForEach(i => { i.Status = clsImplementationEnum.PlanStatus.Superseded.GetStringValue(); });
                    }

                    objPLN017_Log = db.PLN017_Log.Add(new PLN017_Log
                    {
                        HeaderId = objPLN017.HeaderId,
                        Project = objPLN017.Project,
                        Document = objPLN017.Document,
                        Customer = objPLN017.Customer,
                        RevNo = objPLN017.RevNo,
                        Status = objPLN017.Status,
                        CDD = objPLN017.CDD,
                        Product = objPLN017.Product,
                        ProcessLicensor = objPLN017.ProcessLicensor,
                        ProcessPlan = objPLN017.ProcessPlan,
                        Attachment = objPLN017.Attachment,
                        JobNo = objPLN017.JobNo,
                        CreatedBy = objPLN017.CreatedBy,
                        CreatedOn = objPLN017.CreatedOn,
                        EditedBy = objPLN017.EditedBy,
                        EditedOn = objPLN017.EditedOn,
                        ApprovedBy = objPLN017.ApprovedBy,
                        ApprovedOn = objPLN017.ApprovedOn,
                        ReturnRemark = objPLN017.ReturnRemark,
                        ReviseRemark = objPLN017.ReviseRemark,
                        SubmittedBy = objPLN017.SubmittedBy,
                        SubmittedOn = objPLN017.SubmittedOn,
                    });
                    #endregion
                    db.SaveChanges();
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    int newId = db.PLN017_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objPLN017.HeaderId, objPLN017.Status, objPLN017.RevNo, objPLN017.Project, objPLN017.Document, newId);

                    int? currentRevisionNull = db.PLN017.Where(q => q.HeaderId == HeaderId).FirstOrDefault().RevNo;
                    int currentRevision = ((currentRevisionNull == null) ? 0 : currentRevisionNull.Value);
                    //clsUpload.CopyFolderContents("PLN017/" + HeaderId + "/" + currentRevision, "PLN017/" + HeaderId + "/" + (currentRevision + 1));
                    var oldfolderPath = "PLN017//" + HeaderId + "//" + currentRevision;
                    var newfolderPath = "PLN017//" + HeaderId + "//" + (currentRevision + 1);
                    //(new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);

                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(oldfolderPath, newfolderPath, oldfolderPath, HeaderId, newfolderPath, HeaderId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Plan successfully approved.";

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN017.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN017.Project, clsImplementationEnum.PlanList.Stool_Plan.GetStringValue(), objPLN017.RevNo.Value.ToString(), objPLN017.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Stool_Plan.GetStringValue(), objPLN017.HeaderId.ToString(), false),
                                                        objPLN017.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Plan not available approved.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN017_Log objPLN017_Log = db.PLN017_Log.Where(x => x.Id == Id && x.ApprovedBy == currentUser).FirstOrDefault();
                if (objPLN017_Log == null)
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadStoolPlanHistoryFormPartial(int Id = 0)
        {
            PLN017_Log objPLN017_Log = db.PLN017_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN017_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN017_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN017_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN017_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN017_Log.ApprovedBy = objPLN017_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN017_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN017_Log.Customer = objPLN017_Log.Customer + " - " + customerName;
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN017/" + objPLN017_Log.HeaderId + "/" + objPLN017_Log.RevNo));
            }
            else
            {
                objPLN017_Log = new PLN017_Log();
                objPLN017_Log.ProcessPlan = clsImplementationEnum.PlanList.Stool_Plan.GetStringValue();
                objPLN017_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN017_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadStoolPlanHistoryFormPartial", objPLN017_Log);
        }

        /// Created By :Deepak tiwari
        /// Created Date :26/08/2017
        /// Description: Modification of add Save functionality when status is send for approval
        /// </summary>
        [HttpPost]
        //public ActionResult SaveHeader(PLN017 pln017, bool hasAttachments, Dictionary<string, string> Attach)
        public ActionResult SaveHeader(PLN017 pln017)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln017.HeaderId > 0)
                {
                    PLN017 objPLN017 = db.PLN017.Where(x => x.HeaderId == pln017.HeaderId).FirstOrDefault();
                    if (objPLN017 != null)
                    {
                        objPLN017.Product = pln017.Product;
                        objPLN017.ProcessLicensor = pln017.ProcessLicensor;
                        objPLN017.JobNo = pln017.JobNo;
                        objPLN017.ApprovedBy = pln017.ApprovedBy.Split('-')[0].ToString().Trim();
                        objPLN017.EditedBy = objClsLoginInfo.UserName;
                        objPLN017.EditedOn = DateTime.Now;
                        if (objPLN017.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN017.RevNo = Convert.ToInt32(objPLN017.RevNo) + 1;
                            objPLN017.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN017.HeaderId;
                        objResponseMsg.Status = objPLN017.Status;
                        objResponseMsg.Revision = objPLN017.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN017.HeaderId, objPLN017.Status, objPLN017.RevNo, objPLN017.Project, objPLN017.Document);

                       
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Stool_Plan.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Stool_Plan, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

    }
}