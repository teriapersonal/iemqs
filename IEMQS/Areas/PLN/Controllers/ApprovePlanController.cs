﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PLN.Controllers
{
    public class ApprovePlanController : clsBase
    {
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ViewPlan(int HeaderID = 0)
        {
            ViewBag.HeaderID = HeaderID;
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN001");
            string currentUser = objClsLoginInfo.UserName;
            PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            

            return View();
        }

        [HttpPost]
        public ActionResult LoadPlanListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_PlanListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadPlanHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);                
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue() + "')  and pln001.ApprovedBy in('" + objClsLoginInfo.UserName + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
               // strWhere += " and pln001.ApprovedBy=('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "[Plan]", "Document", "RevNo", "[Status]", "pln001.CreatedBy +' - '+com003c.t_name", "pln001.EditedBy +' - '+com003e.t_name", "pln001.ApprovedBy +' - '+com003a.t_name" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_FETCH_PLAN_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.PlanName),
                               Convert.ToString(uc.Document),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                               GetButtonsStatusWise(uc.Status,uc.HeaderId,(lstResult.Count >0 && lstResult.Where(x=>x.Document==uc.Document).Select(x=>x.RevNo).Max() == uc.RevNo && uc.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue()?true:false))
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadPlanCreateFormPartial(int HeaderID)
        {
            PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN001 != null)
            {
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN001.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                ViewBag.ApproverName = objPLN001.ApprovedBy + " - " + ApproverName;
                if (objPLN001.ApprovedBy != null)
                {
                    if (objPLN001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
            }
            return PartialView("_LoadPlanCreateFormPartial", objPLN001);
        }

        [HttpPost]
        public JsonResult LoadPlanHeaderLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]", "pln002.CreatedBy +' - '+com003.t_name", "pln002.EditedBy +' - '+com003.t_name" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                var lstResult = db.SP_FETCH_PLAN_HEADERS_LINES
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                LineOperation(objPLN001.Status,uc.LineId,uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = "",
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            for (int i = 0; i < arrayHeaderIds.Length; i++)
            {
                int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg ApproveHeaderByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN001 != null)
                {
                    objPLN001.Status = clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue();
                    objPLN001.ApprovedBy = objClsLoginInfo.UserName;
                    objPLN001.ApprovedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Lines successfully approved.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Lines not available approved.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        [HttpPost]
        public ActionResult ReturnPlan(int HeaderId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue().ToUpper();
                PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN001 != null)
                {
                    objPLN001.Status = clsImplementationEnum.PlanStatus.Returned.GetStringValue();
                    objPLN001.ReturnRemark = Remarks;
                    objPLN001.EditedBy = objClsLoginInfo.UserName;
                    objPLN001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Lines successfully returned.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please add remarks for returning Lines.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string GetButtonsStatusWise(string status, int headerId, bool IsVisisble)
        {
            string strButtons = string.Empty;
            if (status == clsImplementationEnum.PlanStatus.Approved.GetStringValue() && IsVisisble)
            {
                strButtons = "<center><a title=\"View\" href=\""+WebsiteURL+"/PLN/ApprovePlan/ViewPlan?HeaderID=" + headerId + "\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a></center>";
            }
            else
            {
                strButtons = "<center><a title=\"View\" href=\"" + WebsiteURL + "/PLN/ApprovePlan/ViewPlan?HeaderID=" + headerId + "\"><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a></center>";
            }
            return strButtons;
        }

        public string LineOperation(string status, int LineId, int HeaderId)
        {
            string strButtons = string.Empty;
            PLN001 objPLN001 = db.PLN001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objPLN001.Status == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue() && objPLN001.ApprovedBy==objClsLoginInfo.UserName)
            {
                strButtons = "<center><i id=\"btnEdit\" name=\"btnAction\" style=\"cursor: pointer;\" Title=\"Edit Record\" class=\"fa fa-pencil - square - o\"  onclick=\"GetPlanLinesForm(" + LineId + "," + HeaderId + ")\" ></i>&nbsp;&nbsp;<i id=\"btnDelete\" name=\"btnAction\" style=\"cursor: pointer;\" Title=\"Delete Record\" class=\"fa fa-trash - o\" onClick=\"DeleteRecord(" + LineId + ")\"></i></center>";
            }
            return strButtons;
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_PLAN_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      PlanName = li.PlanName,
                                      Document = li.Document,
                                      RevNo = "R" + li.RevNo,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                      ReturnRemark = li.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_FETCH_PLAN_HEADERS_LINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      ROW_NO = li.ROW_NO,
                                      RelatedTo = li.RelatedTo,
                                      CheckPointDesc = li.CheckPointDesc,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}