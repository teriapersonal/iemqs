﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PLN.Controllers
{
    public class ApproveLockingCleatController : clsBase
    {
        // GET: PLN/ApproveLockingCleat
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]

        #region Index Page
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadHeaderDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue() + "') and ApprovedBy in('" + objClsLoginInfo.UserName + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                //strWhere += " and ApprovedBy='" + objClsLoginInfo.UserName + "'";
                string[] columnName = { "Project", "Document", "Customer", "JointType", "RevNo", "Status" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_PLN_LOCKINGCLEAT_HEADER_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.Document),
                            Convert.ToString(uc.Customer),
                            Convert.ToString("R"+uc.RevNo),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.SubmittedBy),
                            uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                            Convert.ToString(uc.ApprovedBy),
                            uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                            "<nobr><center>"+
                            "<a class='iconspace' title='View' href='"+WebsiteURL +"/PLN/ApproveLockingCleat/ViewHeader?HeaderID="+uc.HeaderId+"'><i class='fa fa-eye'></i></a>"+
                            (uc.RevNo > 0 ? HTMLHistoryString(uc.HeaderId,uc.Status,"History","History","iconspace fa fa-history","GetHistoryDetails(\"" + uc.HeaderId + "\");"): HTMLHistoryString(uc.HeaderId,uc.Status,"History","History","disabledicon fa fa-history","") )+
                            "<i title='Show Timeline' class='iconspace fa fa-clock-o'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainLockingCleat/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>"+
                            "<i title='Print Report' class='iconspace fa fa-print' onClick='PrintReport("+uc.HeaderId+")'></i>"+
                            "</center></nobr>",
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string HTMLHistoryString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            return htmlControl;
        }
        #endregion

        #region View Header
        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        public ActionResult ViewHeader(int headerID)
        {
            PLN019 objPLN019 = new PLN019();
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN019");
            if (headerID > 0)
            {
                objPLN019 = db.PLN019.Where(x => x.HeaderId == headerID ).FirstOrDefault();
                if (objPLN019 != null)
                {
                    objPLN019.Project = db.COM001.Where(i => i.t_cprj == objPLN019.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.HeaderID = objPLN019.HeaderId;
                    ViewBag.Project = objPLN019.Project;
                    string ApproverName = db.COM003.Where(x => x.t_psno == objPLN019.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                    ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPLN019.Customer + "'").FirstOrDefault();
                    ViewBag.ApproverName = objPLN019.ApprovedBy + " - " + ApproverName;

                    var PlanningDinID = db.PDN002.Where(x => x.RefId == headerID && x.DocumentNo == objPLN019.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                    ViewBag.PlanningDinID = PlanningDinID;
                }
                
            }

            return View(objPLN019);
        }
        [HttpPost]
        public ActionResult LoadHeaderForm(int headerID)
        {
            PLN019 objPLN019 = new PLN019();

            if (headerID > 0)
            {
                objPLN019 = db.PLN019.Where(x => x.HeaderId == headerID).FirstOrDefault();
                if (objPLN019.ApprovedBy != null)
                {
                    if (objPLN019.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
                objPLN019.Project = db.COM001.Where(i => i.t_cprj == objPLN019.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Project = objPLN019.Project;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN019.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPLN019.Customer + "'").FirstOrDefault();
                ViewBag.ApproverName = objPLN019.ApprovedBy + " - " + ApproverName;
                ViewBag.Action = "headeredit";

                var PlanningDinID = db.PDN002.Where(x => x.RefId == headerID && x.DocumentNo == objPLN019.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, headerID, clsImplementationEnum.PlanList.Locking_Cleat.GetStringValue());
            }
            else
            {
                objPLN019.RevNo = 0;
                objPLN019.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
            }
            return PartialView("_LoadHeaderForm", objPLN019);
        }


        [SessionExpireFilter]
        public ActionResult LoadTabWiseData(int headerID)
        {
            ViewBag.HeaderID = headerID;
            return PartialView("_LoadTabWiseDataPartial");
        }
        [SessionExpireFilter]
        public ActionResult LoadLockingCleatData(int headerID)
        {
            PLN019 objPLN019 = new PLN019();

            if (headerID > 0)
            {
                objPLN019 = db.PLN019.Where(x => x.HeaderId == headerID).FirstOrDefault();
                ViewBag.Action = "headeredit";
                if (objPLN019.ApprovedBy != null)
                {
                    if (objPLN019.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
            }
            return PartialView("_LoadLockingCleatDataPartial", objPLN019);
        }

        [SessionExpireFilter]
        public ActionResult LoadLoadingDetailsData(int headerID, int lineID)
        {
            PLN020 objPLN020 = new PLN020();
            ViewBag.HeaderId = headerID;
            ViewBag.Material = clsImplementationEnum.GetLockingCleatMaterial().ToArray();
            ViewBag.WeldType = clsImplementationEnum.GetWeldType().ToArray();
            PLN019 objPLN019 = new PLN019();
            objPLN019 = db.PLN019.Where(x => x.HeaderId == headerID).FirstOrDefault();
            if (objPLN019 != null)
            {
                if (lineID > 0)
                {
                    objPLN020 = db.PLN020.Where(x => x.LineId == lineID).FirstOrDefault();
                    ViewBag.Action = "line_edit";
                }
                else
                {
                    string docNum = MakeLCDocNumber(headerID);
                    objPLN020.DocNo = Convert.ToInt32(docNum);
                    objPLN020.LCDocument = "LC" + docNum + "_" + objPLN019.Document;
                }

                ViewBag.HeaderStatus = objPLN019.Status;

            }
            return PartialView("_LoadLoadingDetailsHTML", objPLN020);
        }
        public string MakeTWDocNumber(int header)
        {
            var lstPLN021 = db.PLN021.Where(x => x.HeaderId == header).ToList();
            string docNumber = string.Empty;

            if (lstPLN021 != null || lstPLN021.Count() > 0)
            {
                var twDocNumber = (from a in db.PLN021
                                   where a.HeaderId.Equals(header) //, StringComparison.OrdinalIgnoreCase) && a.Method == method && a.QualityProject.Equals(qms, StringComparison.OrdinalIgnoreCase)
                                   select a).Max(a => a.DocNo);
                docNumber = (twDocNumber + 1).ToString();

            }
            else
            {
                docNumber = "01";
            }

            return docNumber.PadLeft(2, '0');
        }
        [SessionExpireFilter]
        public ActionResult LoadTackWeldLineData(int headerID, int lineID)
        {
            PLN021 objPLN021 = new PLN021();
            PLN019 objPLN019 = new PLN019();
            ViewBag.HeaderId = headerID;
            ViewBag.Material = clsImplementationEnum.GetTackWeldMaterial().ToArray();
            ViewBag.WeldType = clsImplementationEnum.GetTackWeldYieldStrength().ToArray();
            objPLN019 = db.PLN019.Where(x => x.HeaderId == headerID).FirstOrDefault();
            if (objPLN019 != null)
            {
                if (lineID > 0)
                {
                    objPLN021 = db.PLN021.Where(x => x.LineId == lineID).FirstOrDefault();
                    ViewBag.Action = "headeredit";
                }
                else
                {
                    string docNum = MakeTWDocNumber(headerID);
                    objPLN021.DocNo = Convert.ToInt32(docNum);
                    objPLN021.TWDocument = "TW" + docNum + "_" + objPLN019.Document;
                    ViewBag.Action = "lineadd";
                }
                ViewBag.HeaderStatus = objPLN019.Status;

            }
            return PartialView("_LoadTackWeldLineDetailsHTML", objPLN021);
        }

        [HttpPost]
        public JsonResult LoadLCLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.Headerid);
                string strWhere = string.Empty;

                string[] columnName = { "LCDocument", "JointType", "RevNo", "LineStatus", "ReturnRemark" };
                strWhere += "HeaderId=" + Convert.ToInt32(param.Headerid);
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                PLN019 objPLN019 = db.PLN019.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
               
                bool IsDisable = true;
                
                if (objPLN019.Status.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower() && objPLN019.ApprovedBy.Trim() == objClsLoginInfo.UserName.Trim())
                {
                    IsDisable = false;
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_PLN_LOCKINGCLEAT_LC_LINES_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.LineId),
                            Convert.ToString(uc.LCDocument),
                            Convert.ToString(uc.JointType),
                            Convert.ToString(uc.LineStatus),
                            //Convert.ToString(uc.ReturnRemark),
                             Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "ReturnRemark", Convert.ToString(uc.ReturnRemark),"UpdateLCData(this, "+ uc.LineId +");", ( !string.Equals(param.MTStatus,clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()) && (string.Equals(uc.LineStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue()) && !IsDisable))?false:true, "", false, "100"),
                            //"<center>" + HTMLActionString(uc.HeaderId,uc.LineStatus,"Edit","Edit Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");") +"</center>",
                            "<center><a title='View' class='iconspace' onClick = 'ViewLockingCleat(" +uc.HeaderId +"," + uc.LineId +")' ><i class='fa fa-eye'></i></a></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateLCData(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, "PLN020", objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateTWData(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, "PLN021", objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //header approval
        [HttpPost]
        public ActionResult ApproveHeader(int strHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN019 objPLN019 = db.PLN019.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
                if (objPLN019 != null)
                {
                    var result = db.SP_PLN_LOCKINGCLEAT_HEADER_APPROVE(strHeaderId, objClsLoginInfo.UserName).FirstOrDefault();
                    if (result.RESULT.ToString() == "IS")
                    {
                        string strApprove = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        int newId = db.PLN019_Log.Where(q => q.HeaderId == strHeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                        Manager.UpdatePDN002(objPLN019.HeaderId, strApprove, objPLN019.RevNo, objPLN019.Project, objPLN019.Document, newId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PLCMessages.Approve.ToString();

                        IEMQSEntitiesContext db1 = new IEMQSEntitiesContext();
                        var objPLN019Update = db1.PLN019.Where(u => u.HeaderId == strHeaderId).FirstOrDefault();
                        #region Send Notification
                        string rolePLNG3 = clsImplementationEnum.UserRoleName.PLNG3.GetStringValue();
                        (new clsManager()).SendNotification(rolePLNG3,
                                                            objPLN019Update.Project,
                                                            "",
                                                            "",
                                                            Manager.GetPDINDocumentNotificationMsg(objPLN019Update.Project, clsImplementationEnum.PlanList.Locking_Cleat.GetStringValue(), objPLN019Update.RevNo.Value.ToString(), objPLN019Update.Status),
                                                            clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                            Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Locking_Cleat.GetStringValue(), objPLN019Update.HeaderId.ToString(), false),
                                                            objPLN019Update.CreatedBy);
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.LockingCleatMessages.MustApproveLines.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //return header
        [HttpPost]
        public ActionResult ReturnHeader(int strHeaderId, string remark)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN019 objPLN019 = db.PLN019.Where(x => x.HeaderId == strHeaderId).FirstOrDefault();
                objPLN019.Status = clsImplementationEnum.CommonStatus.Returned.GetStringValue();
                objPLN019.ReturnRemark = remark;
                db.SaveChanges();
                Manager.UpdatePDN002(objPLN019.HeaderId, objPLN019.Status, objPLN019.RevNo, objPLN019.Project, objPLN019.Document);
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Approve.ToString();


                #region Send Notification
                string rolePLNG3 = clsImplementationEnum.UserRoleName.PLNG3.GetStringValue();
                (new clsManager()).SendNotification(rolePLNG3,
                                                    objPLN019.Project,
                                                    "",
                                                    "",
                                                    Manager.GetPDINDocumentNotificationMsg(objPLN019.Project, clsImplementationEnum.PlanList.Locking_Cleat.GetStringValue(), objPLN019.RevNo.Value.ToString(), objPLN019.Status),
                                                    clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                    Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Locking_Cleat.GetStringValue(), objPLN019.HeaderId.ToString(), false),
                                                    objPLN019.CreatedBy);
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveLockingCleat(string strLineIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayLineIds = strLineIds.Split(',').ToArray();
            try
            {
                for (int i = 0; i < arrayLineIds.Length; i++)
                {
                    int lineId = Convert.ToInt32(arrayLineIds[i]);
                    //db.SP_PCL_APPROVE(HeaderId, objClsLoginInfo.UserName);
                    PLN020 objPLN020 = db.PLN020.Where(x => x.LineId == lineId).FirstOrDefault();
                    objPLN020.LineStatus = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                    objPLN020.ApprovedBy = objClsLoginInfo.UserName;
                    objPLN020.ApprovedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Approve.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public ActionResult RejectLockingCleat(string strLineIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayLineIds = strLineIds.Split(',').ToArray();
            try
            {
                for (int i = 0; i < arrayLineIds.Length; i++)
                {
                    int lineId = Convert.ToInt32(arrayLineIds[i]);

                    //db.SP_PCL_APPROVE(HeaderId, objClsLoginInfo.UserName);
                    PLN020 objPLN020 = db.PLN020.Where(x => x.LineId == lineId).FirstOrDefault();
                    if (!string.IsNullOrWhiteSpace(objPLN020.ReturnRemark))
                    {
                        objPLN020.LineStatus = clsImplementationEnum.CommonStatus.Reject.GetStringValue();
                        objPLN020.ApprovedBy = objClsLoginInfo.UserName;
                        objPLN020.ApprovedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PLCMessages.Returned.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Reject Remark is required";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult ApproveTackWeld(string strLineIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayLineIds = strLineIds.Split(',').ToArray();
            try
            {
                //foreach (var lst in arrayLineIds)
                //{
                //    if (!lstQMS025.Contains(lst))
                //    {
                //        strStageName += lst + ",";
                //        isStageExists = false;
                //    }
                //}

                for (int i = 0; i < arrayLineIds.Length; i++)
                {
                    int lineId = Convert.ToInt32(arrayLineIds[i]);
                    //db.SP_PCL_APPROVE(HeaderId, objClsLoginInfo.UserName);
                    PLN021 objPLN021 = db.PLN021.Where(x => x.LineId == lineId).FirstOrDefault();
                    objPLN021.LineStatus = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                    objPLN021.ApprovedBy = objClsLoginInfo.UserName;
                    objPLN021.ApprovedOn = DateTime.Now; db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PLCMessages.Approve.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public ActionResult RejectTackWeld(string strLineIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayLineIds = strLineIds.Split(',').ToArray();
            try
            {
                for (int i = 0; i < arrayLineIds.Length; i++)
                {
                    int lineId = Convert.ToInt32(arrayLineIds[i]);
                    //db.SP_PCL_APPROVE(HeaderId, objClsLoginInfo.UserName);
                    PLN021 objPLN021 = db.PLN021.Where(x => x.LineId == lineId).FirstOrDefault();
                    if (!string.IsNullOrWhiteSpace(objPLN021.ReturnRemark))
                    {
                        objPLN021.LineStatus = "Rejected";// clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        objPLN021.ApprovedBy = objClsLoginInfo.UserName;
                        objPLN021.ApprovedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PLCMessages.Returned.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Reject Remark is required";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadTWLineData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                string[] columnName = { "TWDocument", "JointDetails", "RevNo", "LineStatus", "ReturnRemark" };
                strWhere += "HeaderId=" + Convert.ToInt32(param.Headerid);
                int HeaderId = Convert.ToInt32(param.Headerid);
                PLN019 objPLN019 = db.PLN019.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                bool IsDisable = true;

                if (objPLN019.Status.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower() && objPLN019.ApprovedBy.Trim() == objClsLoginInfo.UserName.Trim())
                {
                    IsDisable = false;
                }
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PLN_LOCKINGCLEAT_TW_LINES_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.LineId),
                            Convert.ToString(uc.TWDocument),
                            //Convert.ToString(uc.Customer),
                            Convert.ToString(uc.JointDetails),
                            Convert.ToString(uc.LineStatus),
                            Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "ReturnRemark", Convert.ToString(uc.ReturnRemark),"UpdateTWData(this, "+ uc.LineId +");", ( !string.Equals(param.MTStatus,clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()) && (string.Equals(uc.LineStatus,clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue()) && !IsDisable) )?false:true, "", false, "100"),
                            //"<center>" + HTMLActionString(uc.HeaderId,uc.LineStatus,"Edit","Edit Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");") +"</center>",
                            "<center><a title='View' onClick = 'ViewTackWeld(" +uc.HeaderId +"," + uc.LineId +")' ><i style='margin-right:5px;' class='fa fa-eye'></i></a></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult LoadTackWeldsData(int headerID)
        {
            PLN019 objPLN019 = new PLN019();
            ViewBag.Header = headerID;
            if (headerID > 0)
            {

                objPLN019 = db.PLN019.Where(x => x.HeaderId == headerID).FirstOrDefault();
                ViewBag.Action = "headeredit";
                if (objPLN019.ApprovedBy != null)
                {
                    if (objPLN019.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
            }
            
            return PartialView("_LoadTackWeldsDataPartial", objPLN019);
        }

        [HttpPost]
        public ActionResult SaveLockingCleat(PLN020 pln020, FormCollection fc)
        {
            MaintainLockingCleatController.CustomResponceMsg objResponseMsg = new MaintainLockingCleatController.CustomResponceMsg();
            try
            {
                PLN020 objPLN020 = new PLN020();
                PLN019 objPLN019 = new PLN019();
                bool IsEditable = false;
                var lstPLN020 = db.PLN020.Where(x => x.HeaderId == pln020.HeaderId && x.JointType == pln020.JointType && x.LineId != pln020.LineId).ToList();
                if (lstPLN020.Count() > 0)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.DuplicateJointType.ToString();
                }
                else
                {
                    if (pln020.LineId > 0)
                    {
                        objPLN020 = db.PLN020.Where(x => x.LineId == pln020.LineId).FirstOrDefault();
                        IsEditable = true;
                    }
                    objPLN019 = db.PLN019.Where(x => x.HeaderId == pln020.HeaderId).FirstOrDefault();
                    objPLN020.JointType = pln020.JointType;
                    objPLN020.ShellThickness = pln020.ShellThickness;
                    objPLN020.LineStatus = clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue();
                    objPLN020.Load = pln020.Load;
                    objPLN020.FactorofSafety = pln020.FactorofSafety;
                    objPLN020.LoadwithFOS = pln020.LoadwithFOS;
                    objPLN020.NumberofCleats = pln020.NumberofCleats;
                    objPLN020.Loadpercleat_MT = pln020.Loadpercleat_MT;
                    objPLN020.Loadpercleat_N = pln020.Loadpercleat_N;
                    objPLN020.Thickness = pln020.Thickness;
                    objPLN020.Dimention_a = pln020.Dimention_a;
                    objPLN020.Dimention_b = pln020.Dimention_b;
                    objPLN020.Dimention_c = pln020.Dimention_c;
                    objPLN020.Dimention_d = pln020.Dimention_d;
                    objPLN020.Material = pln020.Material;
                    objPLN020.YieldStrength = pln020.YieldStrength;
                    objPLN020.WeldType = pln020.WeldType;
                    objPLN020.SizeofWeld = pln020.SizeofWeld;
                    objPLN020.Throatthickness = pln020.Throatthickness;
                    objPLN020.Lengthofweld = pln020.Lengthofweld;
                    //if (objPLN019.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue())
                    //{
                    //    objPLN019.RevNo = Convert.ToInt32(objPLN019.RevNo) + 1;
                    //    objPLN019.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    //}
                    if (IsEditable)
                    {
                        objPLN020.EditedBy = objClsLoginInfo.UserName;
                        objPLN020.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }
                    else
                    {
                        objPLN020.HeaderId = objPLN019.HeaderId;
                        objPLN020.Project = objPLN019.Project;
                        objPLN020.Document = objPLN019.Document;
                        objPLN020.Customer = objPLN019.Customer;
                        objPLN020.RevNo = objPLN019.RevNo;
                        objPLN020.CDD = objPLN019.CDD;
                        //string docNum = MakeLCDocNumber(objPLN019.HeaderId);
                        objPLN020.DocNo = pln020.DocNo;//Convert.ToInt32(docNum);
                        objPLN020.LCDocument = pln020.LCDocument;
                        objPLN020.CreatedBy = objClsLoginInfo.UserName;
                        objPLN020.CreatedOn = DateTime.Now;
                        db.PLN020.Add(objPLN020);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }
                    objResponseMsg.HeaderID = objPLN019.HeaderId;
                    objResponseMsg.Status = objPLN019.Status;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult SaveTackWeld(PLN021 pln021)
        {
            MaintainLockingCleatController.CustomResponceMsg objResponseMsg = new MaintainLockingCleatController.CustomResponceMsg();
            try
            {
                PLN021 objPLN021 = new PLN021();
                PLN019 objPLN019 = new PLN019();
                bool IsEditable = false;
                var lstPLN021 = db.PLN021.Where(x => x.HeaderId == pln021.HeaderId && x.JointDetails == pln021.JointDetails && x.LineId != pln021.LineId).ToList();
                if (lstPLN021.Count() > 0)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Joint details already exists";
                }
                else
                {
                    if (pln021.LineId > 0)
                    {
                        objPLN021 = db.PLN021.Where(x => x.LineId == pln021.LineId).FirstOrDefault();
                        IsEditable = true;
                    }

                    objPLN019 = db.PLN019.Where(x => x.HeaderId == pln021.HeaderId).FirstOrDefault();
                    objPLN021.JointDetails = pln021.JointDetails;
                    objPLN021.WeightofSection = pln021.WeightofSection;
                    objPLN021.Factorofsafety = pln021.Factorofsafety;
                    objPLN021.LoadwithFOS_MT = pln021.LoadwithFOS_MT;
                    objPLN021.LoadwithFOS_N = pln021.LoadwithFOS_N;
                    objPLN021.DistCorrWeightSect = pln021.DistCorrWeightSect;
                    objPLN021.InsideRadiousofweld = pln021.InsideRadiousofweld;
                    objPLN021.Weldsize = pln021.Weldsize;
                    objPLN021.OutsideRadiusofweld = pln021.OutsideRadiusofweld;
                    objPLN021.Numoftacks = pln021.Numoftacks;
                    objPLN021.Materialofweld = pln021.Materialofweld;
                    objPLN021.Yieldstrength = pln021.Yieldstrength;
                    objPLN021.AllowtensileStress = pln021.AllowtensileStress;
                    objPLN021.AllowShearStress = pln021.AllowShearStress;
                    objPLN021.maxTensileLoad = pln021.maxTensileLoad;
                    objPLN021.DirectShearLoad = pln021.DirectShearLoad;
                    objPLN021.EquivalentTensileLoad = pln021.EquivalentTensileLoad;
                    objPLN021.EquivalentShearLoad = pln021.EquivalentShearLoad;
                    objPLN021.WeldLengthDuetoTensileLoad = pln021.WeldLengthDuetoTensileLoad;
                    objPLN021.WeldLengthDuetoShearLoad = pln021.WeldLengthDuetoShearLoad;
                    objPLN021.DocNo = pln021.DocNo;
                    objPLN021.WeldLenghtRequired = pln021.WeldLenghtRequired;
                    objPLN021.LineStatus = clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue();

                    //if (objPLN019.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue())
                    //{
                    //    objPLN019.RevNo = Convert.ToInt32(objPLN019.RevNo) + 1;
                    //    objPLN019.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    //}
                    if (IsEditable)
                    {
                        objPLN021.EditedBy = objClsLoginInfo.UserName;
                        objPLN021.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                    else
                    {
                        objPLN021.HeaderId = objPLN019.HeaderId;
                        objPLN021.Project = objPLN019.Project;
                        objPLN021.Document = objPLN019.Document;
                        objPLN021.Customer = objPLN019.Customer;
                        objPLN021.RevNo = objPLN019.RevNo;
                        objPLN021.CDD = objPLN019.CDD;
                        //string docNum = MakeTWDocNumber(objPLN019.HeaderId);
                        objPLN021.TWDocument = pln021.TWDocument;
                        objPLN021.DocNo = pln021.DocNo;//Convert.ToInt32(docNum);
                        objPLN021.CreatedBy = objClsLoginInfo.UserName;
                        objPLN021.CreatedOn = DateTime.Now;
                        db.PLN021.Add(objPLN021);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }
                    db.SaveChanges();
                    objResponseMsg.HeaderID = objPLN019.HeaderId;
                    objResponseMsg.Status = objPLN019.Status;
                    objResponseMsg.LineID = objPLN021.LineId;
                    objResponseMsg.rev = objPLN019.RevNo;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string MakeLCDocNumber(int header)
        {
            var lstPLN020 = db.PLN020.Where(x => x.HeaderId == header).ToList();
            string docNumber = string.Empty;

            if (lstPLN020 != null || lstPLN020.Count() > 0)
            {
                var twDocNumber = (from a in db.PLN020
                                   where a.HeaderId.Equals(header)
                                   select a).Max(a => a.DocNo);
                docNumber = (twDocNumber + 1).ToString();
            }
            else
            {
                docNumber = "01";
            }

            return docNumber.PadLeft(2, '0');
        }
        #endregion

        #region Approve
        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            try
            {
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int headerId = Convert.ToInt32(arrayHeaderIds[i]);
                    db.SP_PLN_LOCKING_CLEAT_APPROVE(headerId, objClsLoginInfo.UserName);
                    PLN019 objPLN019 = db.PLN019.Where(x => x.HeaderId == headerId).FirstOrDefault();
                    int newId = db.PLN019_Log.Where(q => q.HeaderId == headerId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objPLN019.HeaderId, clsImplementationMessage.CommonMessages.Approve.ToString(), objPLN019.RevNo, objPLN019.Project, objPLN019.Document, newId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Approve.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Export Excell
        //Excel funtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {//header grid data
                    var lst = db.SP_PLN_LOCKINGCLEAT_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      Document = uc.Document,
                                      Customer = uc.Customer,
                                      JointType = uc.JointType,
                                      RevNo = "R" + uc.RevNo,
                                      Status = uc.Status,
                                      SubmittedBy = uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.lockingcleat.GetStringValue())
                {  //locking cleat lines
                    var lst = db.SP_PLN_LOCKINGCLEAT_LC_LINES_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      LCDocument = uc.LCDocument,
                                      JointType = uc.JointType,
                                      LineStatus = uc.LineStatus,
                                      ReturnRemark = uc.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.tackWeld.GetStringValue())
                {  //tack weld lines
                    var lst = db.SP_PLN_LOCKINGCLEAT_TW_LINES_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      TWDocument = uc.TWDocument,
                                      JointDetails = uc.JointDetails,
                                      LineStatus = uc.LineStatus,
                                      ReturnRemark = uc.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_PLN_LOCKINGCLEAT_HEADER_HISTORY_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      Document = uc.Document,
                                      Customer = uc.Customer,
                                      JointType = uc.JointType,
                                      RevNo = "R" + uc.RevNo,
                                      Status = uc.Status,
                                      SubmittedBy = uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.historylockingcleat.GetStringValue())
                {
                    var lst = db.SP_PLN_LOCKINGCLEAT_LC_LINES_DETAILS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      LCDocument = uc.LCDocument,
                                      JointType = uc.JointType,
                                      LineStatus = uc.LineStatus,
                                      ReturnRemark = uc.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.historytackWeld.GetStringValue())
                {  //tack weld lines
                    var lst = db.SP_PLN_LOCKINGCLEAT_TW_LINES_DETAILS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      TWDocument = uc.TWDocument,
                                      JointDetails = uc.JointDetails,
                                      LineStatus = uc.LineStatus,
                                      ReturnRemark = uc.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //fetch Planning din Id by using plan header id and plan
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Locking_Cleat.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Locking_Cleat, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderId;
        }


    }
}