﻿using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace IEMQS.Areas.PLN.Controllers
{
    public class ApproveReferenceSketchController : clsBase
    {
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadReferenceSketchListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_ReferenceSketchListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadRererenceSketchHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue() + "') and pln003.ApprovedBy in ('" + objClsLoginInfo.UserName + "')";
                }
                else
                {
                    strWhere += "1=1";
                }

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln003.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln003.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }


                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_REFERENCESKETCH_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.Project),
                                Convert.ToString(uc.Document),
                                Convert.ToString(uc.Customer),
                                Convert.ToString(uc.Product),
                                Convert.ToString(uc.ProcessLicensor),
                                Convert.ToString("R"+uc.RevNo),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.CDD),
                                Convert.ToString(uc.ProcessPlan),
                                Convert.ToString(uc.CreatedBy),
                                uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.EditedBy),
                                uc.EditedOn.HasValue ? Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                Convert.ToString(uc.ApprovedBy),
                                uc.ApprovedOn.HasValue ? Convert.ToString(uc.ApprovedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.ReturnRemark),
                                Convert.ToString(uc.HeaderId),
                               // "<center><a title='View' href='/PLN/ApproveReferenceSketch/ApproveReferenceSketch?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                               //GetButtonsStatusWise(uc.Status,uc.HeaderId,(lstResult.Count >0 && lstResult.Select(x=>x.RevNo).Max() == uc.RevNo && uc.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue()?true:false))
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (HeaderId > 0)
                {

                    PLN003 objPLN003 = db.PLN003.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN003 != null)
                    {
                        latestRv = objPLN003.RevNo;
                    }
                    else
                    {
                        latestRv = 0;
                    }
                }
                else
                {
                    string strhydrotest = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                    string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                    var latestRecord = db.PLN001
                             .Where(x
                                        => x.Plan == strhydrotest &&
                                           x.Status == strStatus
                                    )
                             .OrderByDescending(x => x.HeaderId)
                             .Select(x => new { x.HeaderId, x.RevNo })
                             .FirstOrDefault();
                    latestRv = latestRecord.RevNo;
                }

                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_REFERENCESKETCH_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      HeaderId = li.HeaderId,
                                      Project = li.Project,
                                      Documents = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      ProcessPlan = li.ProcessPlan,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn == null || li.SubmittedOn.Value == DateTime.MinValue ? "NA" : li.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = li.ApprovedBy,
                                      ApproveOn = li.ApprovedOn == null || li.ApprovedOn.Value == DateTime.MinValue ? "NA" : li.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ReturnRemark = li.ReturnRemark

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_FETCH_REFERENCESKETCH_HEADER_LINE(HeaderId, latestRv, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RowNo = li.ROW_NO,
                                      RelatedTo = li.RelatedTo,
                                      CheckPointDesc = li.CheckPointDesc,
                                      YesNo = li.Yes_No,
                                      Remarks = li.Remarks,
                                      PreparedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      CheckedBy = li.EditedBy,
                                      CheckedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      DocumentNo = li.Document,

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {  //tack weld lines
                    var lst = db.SP_FETCH_REFERENCESKETCH_HEADERS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Documents = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      ProcessPlan = li.ProcessPlan,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn == null || li.SubmittedOn.Value == DateTime.MinValue ? "NA" : li.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = li.ApprovedBy,
                                      ApproveOn = li.ApprovedOn == null || li.ApprovedOn.Value == DateTime.MinValue ? "NA" : li.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ReturnRemark = li.ReturnRemark

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_FETCH_REFERENCESKETCH_HEADER_LINE_HISTORY(HeaderId, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RowNo = li.ROW_NO,
                                      RelatedTo = li.RelatedTo,
                                      CheckPointDesc = li.CheckPointDesc,
                                      YesNo = li.Yes_No,
                                      Remarks = li.Remarks,
                                      PreparedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      CheckedBy = li.EditedBy,
                                      CheckedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      DocumentNo = li.Document,

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ApproveReferenceSketch(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN003");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN003 objPLN003 = db.PLN003.Where(x => x.HeaderId == HeaderID).FirstOrDefault();                
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadRefSketchCreateFormPartial(int HeaderID = 0)
        {
            PLN003 objPLN003 = db.PLN003.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN003.ApprovedBy != null)
            {
                if (objPLN003.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                {
                    ViewBag.IsDisplayMode = true;
                }
                else
                {
                    ViewBag.IsDisplayMode = false;
                }
            }
            else
            {
                ViewBag.IsDisplayMode = true;
            }
            if (objPLN003 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN003.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN003.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN003.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN003.ApprovedBy = objPLN003.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN003.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN003.Customer = objPLN003.Customer + " - " + customerName;
                string strRefSketch = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strRefSketch &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    PLN004 objPLN004 = db.PLN004.Where(x => x.HeaderId == objPLN003.HeaderId).FirstOrDefault();
                    if (objPLN004 != null)
                    {
                        if (latestRecord.RevNo == objPLN004.PlanRevNo)
                        {
                            ViewBag.isLatest = "true";
                        }
                        else
                        {
                            ViewBag.isLatest = "false";
                        }
                    }
                    else
                    {
                        ViewBag.isLatest = "false";
                    }
                }
                else
                {
                    ViewBag.isLatest = "true";
                }
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN003/" + objPLN003.HeaderId + "/" + objPLN003.RevNo));

                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN003.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, HeaderID, clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue());
            }
            else
            {
                objPLN003 = new PLN003();
                objPLN003.ProcessPlan = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                objPLN003.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN003.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadRefSketchCreateFormPartial", objPLN003);
        }

        [HttpPost]
        public JsonResult LoadCheckListData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN003 objPLN001 = db.PLN003.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string strRefSketch = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strRefSketch &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln004.HeaderId = " + HeaderId + " and ";
                    PLN004 objPLN004 = db.PLN004.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN004 != null)
                    {
                        PlanRev = objPLN004.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_REFERENCESKETCH_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,false,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,false),
                                Convert.ToString(uc.CreatedBy),
                                uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.EditedBy),
                                uc.EditedOn.HasValue ? Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string LineOperation(bool flag, int LineId, int HeaderId)
        {
            if (flag)
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" checked=\"checked\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
            else
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
        }

        [HttpPost]
        public ActionResult ReturnReferenceSketch(int HeaderId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN003 objPLN003 = db.PLN003.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN003 != null)
                {
                    objPLN003.Status = clsImplementationEnum.PlanStatus.Returned.GetStringValue();
                    objPLN003.ReturnRemark = Remarks;
                    objPLN003.EditedBy = objClsLoginInfo.UserName;
                    objPLN003.EditedOn = DateTime.Now;

                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    string unReleasedStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue();
                    PDN002 objPDN002 = db.PDN002.Where(x => x.RefId == objPLN003.HeaderId && x.Status == unReleasedStatus).FirstOrDefault();
                    if (objPDN002 != null)
                    {
                        objPDN002.RevNo = objPLN003.RevNo;
                        objPDN002.Status = objPLN003.Status;
                    }
                    //--

                    db.SaveChanges();
                    Manager.UpdatePDN002(objPLN003.HeaderId, objPLN003.Status, objPLN003.RevNo, objPLN003.Project, objPLN003.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully returned.";


                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN003.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN003.Project, clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue(), objPLN003.RevNo.Value.ToString(), objPLN003.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue(), objPLN003.HeaderId.ToString(), false),
                                                        objPLN003.CreatedBy);
                    #endregion                    
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please add remarks for returning Checklist.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            for (int i = 0; i < arrayHeaderIds.Length; i++)
            {
                int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg ApproveHeaderByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN003 objPLN003 = db.PLN003.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN003 != null)
                {
                    objPLN003.Status = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                    objPLN003.ApprovedBy = objClsLoginInfo.UserName;
                    objPLN003.ApprovedOn = DateTime.Now;

                    #region Header Log
                    var lstPLN003_Log = db.PLN003_Log.Where(x => x.HeaderId == objPLN003.HeaderId).ToList();
                    PLN003_Log objPLN003_Log = new PLN003_Log();
                    if (lstPLN003_Log != null)
                    {
                        objPLN003_Log = lstPLN003_Log.FirstOrDefault();
                        lstPLN003_Log.ForEach(i => { i.Status = clsImplementationEnum.PlanStatus.Superseded.GetStringValue(); });
                    }
                    objPLN003_Log = db.PLN003_Log.Add(new PLN003_Log
                    {
                        HeaderId = objPLN003.HeaderId,
                        Project = objPLN003.Project,
                        Document = objPLN003.Document,
                        Customer = objPLN003.Customer,
                        RevNo = objPLN003.RevNo,
                        Status = objPLN003.Status,
                        CDD = objPLN003.CDD,
                        Product = objPLN003.Product,
                        ProcessLicensor = objPLN003.ProcessLicensor,
                        ProcessPlan = objPLN003.ProcessPlan,
                        Attachment = objPLN003.Attachment,
                        JobNo = objPLN003.JobNo,
                        CreatedBy = objPLN003.CreatedBy,
                        CreatedOn = objPLN003.CreatedOn,
                        EditedBy = objPLN003.EditedBy,
                        EditedOn = objPLN003.EditedOn,
                        ApprovedBy = objPLN003.ApprovedBy,
                        ApprovedOn = objPLN003.ApprovedOn,
                        ReturnRemark = objPLN003.ReturnRemark,
                        ReviseRemark = objPLN003.ReviseRemark,
                        SubmittedBy = objPLN003.SubmittedBy,
                        SubmittedOn = objPLN003.SubmittedOn,
                    });
                    #endregion

                    db.SaveChanges();
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    int newId = db.PLN003_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objPLN003.HeaderId, objPLN003.Status, objPLN003.RevNo, objPLN003.Project, objPLN003.Document, newId);
                    List<PLN004> lstPLN004 = db.PLN004.Where(x => x.HeaderId == HeaderId).ToList();
                    List<PLN004_Log> lstPLN004_Log = new List<PLN004_Log>();
                    if (lstPLN004 != null && lstPLN004.Count > 0)
                    {
                        lstPLN004_Log = lstPLN004.Select(x => new PLN004_Log()
                        {
                            RefId = objPLN003_Log.Id,
                            LineId = x.LineId,
                            HeaderId = x.HeaderId,
                            Project = x.Project,
                            Document = x.Document,
                            RevNo = x.RevNo,
                            Plan = x.Plan,
                            PlanRevNo = x.PlanRevNo,
                            CheckListId = x.CheckListId,
                            Yes_No = x.Yes_No,
                            CreatedBy = x.CreatedBy,
                            CreatedOn = x.CreatedOn,
                            EditedBy = x.EditedBy,
                            EditedOn = x.EditedOn,
                            Remarks = x.Remarks
                        }).ToList();
                        db.PLN004_Log.AddRange(lstPLN004_Log);
                        db.SaveChanges();
                    }


                    int? currentRevisionNull = db.PLN003.Where(q => q.HeaderId == HeaderId).FirstOrDefault().RevNo;
                    int currentRevision = ((currentRevisionNull == null) ? 0 : currentRevisionNull.Value);
                    //clsUpload.CopyFolderContents("PLN003/" + HeaderId + "/" + currentRevision, "PLN003/" + HeaderId + "/" + (currentRevision + 1));
                    var oldfolderPath = "PLN003//" + HeaderId + "//" + currentRevision;
                    var newfolderPath = "PLN003//" + HeaderId + "//" + (currentRevision + 1);
                    //(new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);

                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(oldfolderPath, newfolderPath, oldfolderPath, HeaderId, newfolderPath, HeaderId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully approved.";


                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN003.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN003.Project, clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue(), objPLN003.RevNo.Value.ToString(), objPLN003.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue(), objPLN003.HeaderId.ToString(), false),
                                                        objPLN003.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Checklist not available approved.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN003_Log objPLN003_Log = db.PLN003_Log.Where(x => x.Id == Id && x.ApprovedBy == currentUser).FirstOrDefault();
                if (objPLN003_Log == null)
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadRefSketchHistoryFormPartial(int Id = 0)
        {
            PLN003_Log objPLN003_Log = db.PLN003_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN003_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN003_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN003_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN003_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN003_Log.ApprovedBy = objPLN003_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN003_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN003_Log.Customer = objPLN003_Log.Customer + " - " + customerName;
                string strRefSketch = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strRefSketch &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    //if (latestRecord.HeaderId == objPLN003.HeaderId)
                    //{

                    //}
                }
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN003/" + objPLN003_Log.HeaderId + "/" + objPLN003_Log.RevNo));
            }
            else
            {
                objPLN003_Log = new PLN003_Log();
                objPLN003_Log.ProcessPlan = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                objPLN003_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN003_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            if (objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.SHOP.GetStringValue()))
            {
                ViewBag.Isshopuser = true;
            }
            else
            {
                ViewBag.Isshopuser = false;
            }
                return PartialView("_LoadRefSketchHistoryFormPartial", objPLN003_Log);
        }

        [HttpPost]
        public JsonResult LoadCheckListHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                int Id = Convert.ToInt32(param.CTQHeaderId);
                PLN003_Log objPLN003_Log = db.PLN003_Log.Where(x => x.Id == Id).FirstOrDefault();

                strWhere += "1=1 and pln004.RefId = " + objPLN003_Log.Id + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                var lstResult = db.SP_FETCH_REFERENCESKETCH_HEADER_LINE_HISTORY
                                (
                                     objPLN003_Log.HeaderId,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,true,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,true),
                                Convert.ToString(uc.CreatedBy),
                                uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.EditedBy),
                                uc.EditedOn.HasValue ? Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        /// Created By :Deepak tiwari
        /// Created Date :26/08/2017
        /// Description: Modification of add Save functionality when status is send for approval
        /// </summary>
        [HttpPost]
        public ActionResult SaveHeader(PLN003 pln003, string strHeaderId, string strLineId, string strIsLatest)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                //string HeaderId = fc["hdHeaderId"];
                //string LineId = fc["hdLineId"];
                int NewHeaderId = 0;
                if (pln003.HeaderId > 0)
                {
                    PLN003 objPLN003 = db.PLN003.Where(x => x.HeaderId == pln003.HeaderId).FirstOrDefault();
                    if (objPLN003 != null)
                    {
                        objPLN003.Product = pln003.Product;
                        objPLN003.ProcessLicensor = pln003.ProcessLicensor;
                        objPLN003.JobNo = pln003.JobNo;
                        objPLN003.ApprovedBy = pln003.ApprovedBy.Split('-')[0].ToString().Trim(); ;
                        objPLN003.EditedBy = objClsLoginInfo.UserName;
                        objPLN003.EditedOn = DateTime.Now;
                        if (objPLN003.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                        {
                            objPLN003.RevNo = Convert.ToInt32(objPLN003.RevNo) + 1;
                            objPLN003.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        if (!Convert.ToBoolean(strIsLatest))
                        {
                            List<PLN004> lstPLN004 = db.PLN004.Where(x => x.HeaderId == objPLN003.HeaderId).ToList();
                            if (lstPLN004 != null && lstPLN004.Count > 0)
                            {
                                db.PLN004.RemoveRange(lstPLN004);
                            }
                            string strRefSketch = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                            string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                            var latestRecord = db.PLN001
                                     .Where(x
                                                => x.Plan == strRefSketch &&
                                                   x.Status == strStatus
                                            )
                                     .OrderByDescending(x => x.HeaderId)
                                     .Select(x => new { x.HeaderId, x.RevNo })
                                     .FirstOrDefault();
                            List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == latestRecord.HeaderId).ToList();
                            if (lstPLN002 != null && lstPLN002.Count > 0)
                            {
                                List<PLN004> NewlstPLN004 = lstPLN002.Select(x => new PLN004()
                                {
                                    LineId = x.LineId,
                                    HeaderId = objPLN003.HeaderId,
                                    Project = objPLN003.Project,
                                    Document = objPLN003.Document,
                                    RevNo = objPLN003.RevNo,
                                    Plan = objPLN003.ProcessPlan,
                                    PlanRevNo = x.RevNo,
                                    CheckListId = x.LineId,
                                    //Yes_No = false,
                                    Yes_No = clsImplementationEnum.ChecklistYesNo.No.GetStringValue(),
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                }).ToList();
                                db.PLN004.AddRange(NewlstPLN004);
                            }
                        }
                        else
                        {
                            List<PLN004> lstPLN004 = db.PLN004.Where(x => x.HeaderId == objPLN003.HeaderId).ToList();
                            if (lstPLN004.Count > 0)
                            {
                                List<PLN004> lstTruePLN004 = new List<PLN004>();

                                foreach (PLN004 objPLN004 in lstPLN004)
                                {
                                    //objPLN004.Yes_No = (!string.IsNullOrWhiteSpace(strLineId) && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Length > 0 && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Contains(objPLN004.LineId) ? true : false);
                                    objPLN004.EditedBy = objClsLoginInfo.UserName;
                                    objPLN004.EditedOn = DateTime.Now;
                                }
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN003.HeaderId;
                        objResponseMsg.Status = objPLN003.Status;
                        objResponseMsg.Revision = objPLN003.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN003.HeaderId, objPLN003.Status, objPLN003.RevNo, objPLN003.Project, objPLN003.Document);


                        var folderPath = "PLN003/" + NewHeaderId + "/" + objPLN003.RevNo;

                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult LoadCheckListDataNew(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN003 objPLN001 = db.PLN003.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string HeaderStatus = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                bool IsDisable = true;
                if (objPLN001 != null)
                {
                    HeaderStatus = objPLN001.Status;
                }
                if (HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower())
                {
                    IsDisable = false;
                }
                if (objPLN001.ApprovedBy != null)
                {
                    if (objPLN001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        IsDisable = true;
                    }
                    else
                    {
                        IsDisable = false;
                    }
                }
                else
                {
                    IsDisable = true;
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string strRefSketch = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strRefSketch &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln004.HeaderId = " + HeaderId + " and ";
                    PLN004 objPLN004 = db.PLN004.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN004 != null)
                    {
                        PlanRev = objPLN004.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                var lstResult = db.SP_FETCH_REFERENCESKETCH_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                (new PLNCommonFunction()).CheckListLineOperationYesNoNew(uc.Yes_No,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Yes_No","UpdateData(this)"),
                                (new PLNCommonFunction()).CheckListLineOperationRemarksNew(uc.Remarks,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Remarks","UpdateData(this)"),
                                Convert.ToString(uc.CreatedBy),
                                uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.EditedBy),
                                uc.EditedOn.HasValue ? Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                //getChecklistDetails(uc.CheckListId.ToString())[0],
                                //getChecklistDetails(uc.CheckListId.ToString())[1],
                                //getChecklistDetails(uc.CheckListId.ToString())[2],
                                //getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public string[] getChecklistDetails(string lineId)
        {
            string[] checklist = new string[4];
            int LineID = Int32.Parse(lineId);
            var data = (from p2 in db.PLN002
                        join
                            p1 in db.PLN001 on p2.HeaderId equals p1.HeaderId
                        where p2.LineId == LineID
                        select p1).FirstOrDefault();
            if (data != null)
            {
                checklist[0] = Manager.GetUserNameFromPsNo(data.CreatedBy); //prepared by
                checklist[1] = Manager.GetUserNameFromPsNo(data.ApprovedBy); // approved by
                checklist[2] = data.ApprovedOn.ToString(); // approved on
                checklist[3] = data.Document; // document no
            }
            return checklist;
        }

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Reference_Sketch, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}