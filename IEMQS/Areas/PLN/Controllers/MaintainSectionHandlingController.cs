﻿using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IEMQS.Areas.Utility.Models;
using System.Web.Mvc;
using static IEMQS.Areas.HTC.Controllers.MaintainHTCController;
using System.Globalization;
namespace IEMQS.Areas.PLN.Controllers
{
    public class MaintainSectionHandlingController : clsBase
    {
        //modified by nikita on 29/08/2017
        //added method updatePDN002
        //task asigned by Satish PAwar
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadSectionHandlingListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_SectionHandlingListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadSectionHandlingHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.PlanStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                //strWhere += " and pln009.CreatedBy in('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln009.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln009.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_FETCH_SECTIONHANDLING_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {

                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                               "<nobr><center>"+"<a title='View' href='"+WebsiteURL+"/PLN/MaintainSectionHandling/CreateSectionHandling?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='' class='iconspace fa fa-eye'></i></a>"+
                               Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/PLN/MaintainSectionHandling/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblSectionHandlingHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                               (uc.RevNo>0 ?"<a title=\"History\" onclick=\"history('"+uc.HeaderId+"')\"><i style='' class='iconspace fa fa-history'></i></a>":"<a title=\"History\"><i style='' class='disabledicon fa fa-history'></i></a>")+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainSectionHandling/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='' class='iconspace fa fa-clock-o'></i></a>"
                               + "</center></nobr>",
                               // "<a title='View' href='/PLN/MaintainSectionHandling/CreateSectionHandling?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+(uc.RevNo>0 ?"<a title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Initiator','/PLN/MaintainSectionHandling/GetHistoryDetails','Section Handling')\"><i style='margin-left:5px;' class='fa fa-history'></i></a>":"")+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainSectionHandling/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='margin-left:5px;cursor:pointer;' class='fa fa-clock-o'></i></a>"

                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Section_Handling_Plan);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Section Handling TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                PLN009_Log objPLN009_Log = db.PLN009_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN009_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN009_Log.CreatedBy) : null;
                model.CreatedOn = objPLN009_Log.CreatedOn;
                model.EditedBy = objPLN009_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN009_Log.EditedBy) : null;
                model.EditedOn = objPLN009_Log.EditedOn;
                model.SubmittedBy = objPLN009_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN009_Log.SubmittedBy) : null;
                model.SubmittedOn = objPLN009_Log.SubmittedOn;

                model.ApprovedBy = objPLN009_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN009_Log.ApprovedBy) : null;
                model.ApprovedOn = objPLN009_Log.ApprovedOn;

            }
            else
            {

                PLN009_Log objPLN009_Log = db.PLN009_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN009_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN009_Log.CreatedBy) : null;
                model.CreatedOn = objPLN009_Log.CreatedOn;
                model.EditedBy = objPLN009_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN009_Log.EditedBy) : null;
                model.EditedOn = objPLN009_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "Section Handling Timeline";

            if (HeaderId > 0)
            {
                PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN009.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN009.CreatedBy) : null;
                model.CreatedOn = objPLN009.CreatedOn;
                model.EditedBy = objPLN009.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN009.EditedBy) : null;
                model.EditedOn = objPLN009.EditedOn;
                model.SubmittedBy = objPLN009.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN009.SubmittedBy) : null;
                model.SubmittedOn = objPLN009.SubmittedOn;
                model.ApprovedBy = objPLN009.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN009.ApprovedBy) : null;
                model.ApprovedOn = objPLN009.ApprovedOn;

            }
            else
            {
                PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN009.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN009.CreatedBy) : null;
                model.CreatedOn = objPLN009.CreatedOn;
                model.EditedBy = objPLN009.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN009.EditedBy) : null;
                model.EditedOn = objPLN009.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult CreateSectionHandling(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN009");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                //if (objPLN009 == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN009.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderID && x.CreatedBy == currentUser).FirstOrDefault();
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadSectionHandlingCreateFormPartial(int HeaderID = 0)
        {
            PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN009 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN009.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN009.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ApproverName))
                {
                    ViewBag.ApproverName = objPLN009.ApprovedBy + " - " + ApproverName;
                }
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN009.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objPLN009.Customer + " - " + customerName;
                string strSectionHandling = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN009/" + objPLN009.HeaderId + "/" + objPLN009.RevNo));
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strSectionHandling &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    PLN010 objPLN010 = db.PLN010.Where(x => x.HeaderId == objPLN009.HeaderId).FirstOrDefault();
                    if (objPLN010 != null)
                    {
                        if (latestRecord.RevNo == objPLN010.PlanRevNo)
                        {
                            ViewBag.isLatest = "true";
                        }
                        else
                        {
                            ViewBag.isLatest = "false";
                        }
                    }
                    else
                    {
                        ViewBag.isLatest = "false";
                    }
                }
                else
                {
                    ViewBag.isLatest = "true";
                }
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN009.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objPLN009.HeaderId, objPLN009.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            else
            {
                objPLN009 = new PLN009();
                objPLN009.ProcessPlan = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                ViewBag.isLatest = "true";
                objPLN009.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN009.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadSectionHandlingCreateFormPartial", objPLN009);
        }

        [HttpPost]
        public JsonResult LoadCheckListData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string strSectionHandling = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strSectionHandling &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln010.HeaderId = " + HeaderId + " and ";
                    PLN010 objPLN010 = db.PLN010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN010 != null)
                    {
                        PlanRev = objPLN010.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
            
                var lstResult = db.SP_FETCH_SECTIONHANDLING_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     "",
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,false,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,false),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                getChecklistDetails(uc.CheckListId.ToString())[0],
                                getChecklistDetails(uc.CheckListId.ToString())[1],
                                getChecklistDetails(uc.CheckListId.ToString())[2],
                                getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [NonAction]
        public string[] getChecklistDetails(string lineId)
        {
            string[] checklist = new string[4];
            int LineID = Int32.Parse(lineId);
            var data = (from p2 in db.PLN002
                        join
                            p1 in db.PLN001 on p2.HeaderId equals p1.HeaderId
                        where p2.LineId == LineID
                        select p1).FirstOrDefault();
            if (data != null)
            {
                checklist[0] = Manager.GetUserNameFromPsNo(data.CreatedBy); //prepared by
                checklist[1] = Manager.GetUserNameFromPsNo(data.ApprovedBy); // approved by
                checklist[2] = data.ApprovedOn.ToString(); // approved on
                checklist[3] = data.Document; // document no
            }
            return checklist;
        }

        [HttpPost]
        public ActionResult SaveHeader(PLN009 pln009, string strHeaderId, string strLineId, string strIsLatest)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln009.HeaderId > 0)
                {
                    PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == pln009.HeaderId).FirstOrDefault();
                    if (objPLN009 != null)
                    {
                        objPLN009.Product = pln009.Product;
                        objPLN009.ProcessLicensor = pln009.ProcessLicensor;
                        objPLN009.JobNo = pln009.JobNo;
                        objPLN009.ReviseRemark = pln009.ReviseRemark;
                        objPLN009.ApprovedBy = pln009.ApprovedBy;
                        objPLN009.EditedBy = objClsLoginInfo.UserName;
                        objPLN009.EditedOn = DateTime.Now;
                        if (objPLN009.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN009.RevNo = Convert.ToInt32(objPLN009.RevNo) + 1;
                            objPLN009.ReturnRemark = null;
                            objPLN009.ApprovedOn = null;

                            objPLN009.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        if (!Convert.ToBoolean(strIsLatest))
                        {
                            List<PLN010> lstPLN010 = db.PLN010.Where(x => x.HeaderId == objPLN009.HeaderId).ToList();
                            if (lstPLN010 != null && lstPLN010.Count > 0)
                            {
                                db.PLN010.RemoveRange(lstPLN010);
                            }
                            string strSectionHandling = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                            string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                            var latestRecord = db.PLN001
                                     .Where(x
                                                => x.Plan == strSectionHandling &&
                                                   x.Status == strStatus
                                            )
                                     .OrderByDescending(x => x.HeaderId)
                                     .Select(x => new { x.HeaderId, x.RevNo })
                                     .FirstOrDefault();
                            List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == latestRecord.HeaderId).ToList();
                            if (lstPLN002 != null && lstPLN002.Count > 0)
                            {
                                List<PLN010> NewlstPLN010 = lstPLN002.Select(x => new PLN010()
                                {
                                    LineId = x.LineId,
                                    HeaderId = objPLN009.HeaderId,
                                    Project = objPLN009.Project,
                                    Document = objPLN009.Document,
                                    RevNo = objPLN009.RevNo,
                                    Plan = objPLN009.ProcessPlan,
                                    PlanRevNo = x.RevNo,
                                    CheckListId = x.LineId,
                                    //Yes_No = false,
                                    //Yes_No = clsImplementationEnum.ChecklistYesNo.No.GetStringValue(), as per observation 15003
                                    Yes_No = string.Empty,
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                }).ToList();
                                db.PLN010.AddRange(NewlstPLN010);
                            }
                        }
                        else
                        {
                            List<PLN010> lstPLN010 = db.PLN010.Where(x => x.HeaderId == objPLN009.HeaderId).ToList();
                            if (lstPLN010.Count > 0)
                            {
                                List<PLN010> lstTruePLN010 = new List<PLN010>();

                                foreach (PLN010 objPLN010 in lstPLN010)
                                {
                                    //objPLN010.Yes_No = (!string.IsNullOrWhiteSpace(strLineId) && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Length > 0 && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Contains(objPLN010.LineId) ? true : false);
                                    objPLN010.EditedBy = objClsLoginInfo.UserName;
                                    objPLN010.EditedOn = DateTime.Now;
                                }
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN009.HeaderId;
                        objResponseMsg.Status = objPLN009.Status;
                        objResponseMsg.Revision = objPLN009.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN009.HeaderId, objPLN009.Status, objPLN009.RevNo, objPLN009.Project, objPLN009.Document);


                        //if (hasAttachments)
                        //{
                        var folderPath = "PLN009/" + NewHeaderId + "/" + objPLN009.RevNo;
                        //    var existing = clsUpload.getDocs(folderPath);
                        //    var toDelete = new Dictionary<string, string>();
                        //    foreach (var item in existing)
                        //    {
                        //        if (Attach.Where(q => q.Key.Equals(item.Key)).Count() <= 0)
                        //            toDelete.Add(item.Key, item.Value);
                        //        else
                        //        {
                        //            if (Attach.Where(q => q.Key.Equals(item.Key)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                        //                toDelete.Add(item.Key, item.Value);
                        //        }
                        //    }
                        //    foreach (var item in toDelete)
                        //        clsUpload.DeleteFile(folderPath, item.Key);
                        //    var toUpload = Attach.Where(q => q.Value.Substring(0, 3) != "URL");

                        //    foreach (var attch in toUpload)
                        //    {
                        //        var base64Data = attch.Value;
                        //        var dataBytes = Helper.FromBase64(attch.Value);
                        //        //var dataBytes = Encoding.ASCII.GetBytes(stringData);
                        //        clsUpload.Upload(attch.Key, dataBytes, folderPath);
                        //    }
                        //}
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                    PLN009 objPLN009 = db.PLN009.Add(new PLN009
                    {
                        Project = pln009.Project,
                        Document = pln009.Document,
                        Customer = pln009.Customer,
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CDD = Helper.ToNullIfTooEarlyForDb(pln009.CDD),
                        Product = pln009.Product,
                        ProcessLicensor = pln009.ProcessLicensor,
                        ProcessPlan = pln009.ProcessPlan,
                        JobNo = pln009.JobNo,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ApprovedBy = pln009.ApprovedBy
                    });
                    db.SaveChanges();
                    NewHeaderId = objPLN009.HeaderId;
                    if (!string.IsNullOrWhiteSpace(strHeaderId))
                    {
                        int planHeaderId = Convert.ToInt32(strHeaderId);
                        List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == planHeaderId).ToList();
                        List<PLN010> lstPLN010 = new List<PLN010>();
                        if (lstPLN002.Count > 0)
                        {
                            List<PLN002> lstTruePLN002 = new List<PLN002>();
                            if (!string.IsNullOrWhiteSpace(strLineId))
                            {
                                int[] arrayLineId = Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s));
                                lstTruePLN002 = lstPLN002.Where(x => arrayLineId.Contains(x.LineId)).ToList();
                            }
                            foreach (PLN002 objPLN002 in lstPLN002)
                            {
                                PLN010 objPLN010 = new PLN010();
                                objPLN010.HeaderId = objPLN009.HeaderId;
                                objPLN010.Project = objPLN009.Project;
                                objPLN010.Document = objPLN009.Document;
                                objPLN010.RevNo = objPLN009.RevNo;
                                objPLN010.Plan = objPLN009.ProcessPlan;
                                objPLN010.PlanRevNo = objPLN002.RevNo;
                                objPLN010.CheckListId = objPLN002.LineId;
                                //objPLN010.Yes_No = (lstTruePLN002.Where(x => x.LineId == objPLN002.LineId).FirstOrDefault() != null ? true : false);
                                objPLN010.CreatedBy = objClsLoginInfo.UserName;
                                objPLN010.CreatedOn = DateTime.Now;
                                lstPLN010.Add(objPLN010);
                            }
                        }
                        if (lstPLN010 != null && lstPLN010.Count > 0)
                        {
                            db.PLN010.AddRange(lstPLN010);
                            db.SaveChanges();
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.Status = objPLN009.Status;
                    objResponseMsg.Revision = objPLN009.RevNo;
                    objResponseMsg.HeaderID = NewHeaderId;

                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN009.HeaderId, objPLN009.Status, objPLN009.RevNo, objPLN009.Project, objPLN009.Document);


                    //if (hasAttachments)
                    //{
                    var folderPath = "PLN009/" + NewHeaderId + "/" + objPLN009.RevNo;
                    //    var existing = clsUpload.getDocs(folderPath);
                    //    var toDelete = new Dictionary<string, string>();
                    //    foreach (var item in existing)
                    //    {
                    //        if (Attach.Where(q => q.Key.Equals(item.Key)).Count() <= 0)
                    //            toDelete.Add(item.Key, item.Value);
                    //        else
                    //        {
                    //            if (Attach.Where(q => q.Key.Equals(item.Key)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                    //                toDelete.Add(item.Key, item.Value);
                    //        }
                    //    }
                    //    foreach (var item in toDelete)
                    //        clsUpload.DeleteFile(folderPath, item.Key);
                    //    var toUpload = Attach.Where(q => q.Value.Substring(0, 3) != "URL");

                    //    foreach (var attch in toUpload)
                    //    {
                    //        var base64Data = attch.Value;
                    //        var dataBytes = Helper.FromBase64(attch.Value);
                    //        //var dataBytes = Encoding.ASCII.GetBytes(stringData);
                    //        clsUpload.Upload(attch.Key, dataBytes, folderPath);
                    //    }
                    //}
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHeaderLine(int LineId, int HeaderId, string ActualId, string YesNo, string Remarks)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                var splitid = ActualId.Split(',');
                if (Convert.ToInt32(splitid[0]) == 0)
                {
                    PLN010 objPLN010 = db.PLN010.Where(x => x.CheckListId == LineId && x.HeaderId == HeaderId).FirstOrDefault();
                    objPLN010.Yes_No = YesNo;
                    objPLN010.Remarks = Remarks;
                    objPLN010.EditedBy = objClsLoginInfo.UserName;
                    objPLN010.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    int ActualLineId = Convert.ToInt32(splitid[0]);
                    PLN010 objPLN010 = db.PLN010.Where(x => x.LineId == ActualLineId && x.HeaderId == HeaderId).FirstOrDefault();
                    objPLN010.Yes_No = YesNo;
                    objPLN010.Remarks = Remarks;
                    objPLN010.EditedBy = objClsLoginInfo.UserName;
                    objPLN010.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrLineStatus = { clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(), clsImplementationEnum.PlanStatus.Returned.GetStringValue() };
                PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                if (objPLN009 != null)
                {
                    objPLN009.Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue();
                    objPLN009.SubmittedBy = objClsLoginInfo.UserName;
                    objPLN009.SubmittedOn = DateTime.Now;

                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN009.HeaderId, objPLN009.Status, objPLN009.RevNo, objPLN009.Project, objPLN009.Document);


                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully sent for approval.";

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                        objPLN009.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN009.Project, clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue(), objPLN009.RevNo.Value.ToString(), objPLN009.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue(), objPLN009.HeaderId.ToString(), true),
                                                        objPLN009.ApprovedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //added by nikita on 31/08/2017
        [HttpPost]
        public ActionResult RetractStatus(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN009 objPLN009 = db.PLN009.Where(u => u.HeaderId == headerId).SingleOrDefault();
                if (objPLN009 != null)
                {
                    objPLN009.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                    objPLN009.EditedBy = objClsLoginInfo.UserName;
                    objPLN009.EditedOn = DateTime.Now;
                    objPLN009.SubmittedOn = null;
                    objPLN009.SubmittedBy = null;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                    Manager.UpdatePDN002(objPLN009.HeaderId, objPLN009.Status, objPLN009.RevNo, objPLN009.Project, objPLN009.Document);
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string LineOperation(bool flag, int LineId, int HeaderId)
        {
            if (flag)
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" checked=\"checked\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
            else
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
        }

        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            string strCdd = Manager.GetContractWiseCdd(projectCode);
            if (!string.IsNullOrWhiteSpace(strCdd))
            {
                DateTime dtCdd = Convert.ToDateTime(strCdd);
                objApproverModel.Cdd = dtCdd.Date.ToShortDateString();
            }
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHistoryDetails(int headerid = 0)
        {
            //ViewBag.Role = strRole;
            ViewBag.HeaderId = headerid;
            return PartialView("_GetHistoryDetailsPartial");
        }

        [HttpPost]
        public JsonResult LoadSectionHandlingHistory(JQueryDataTableParamModel param, string HeaderId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                //if (param.CTQCompileStatus.ToUpper() == "INITIATOR")
                //{
                //    strWhere += "1=1 and pln009.CreatedBy = '" + objClsLoginInfo.UserName + "'";

                //}
                //else
                //{
                //    strWhere += "1=1 and pln009.ApprovedBy = '" + objClsLoginInfo.UserName + "'";
                //}
                if (!string.IsNullOrWhiteSpace(HeaderId))
                {
                    strWhere += " pln009.HeaderId = " + Convert.ToInt32(HeaderId);
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln009.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln009.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                #region Sorting

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstResult = db.SP_FETCH_SECTIONHANDLING_HEADERS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                               "<center>"+
                               "<a title='View' href='"+WebsiteURL+"/PLN/ApproveSectionHandling/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'>"+"<i style='margin-left:5px;' class='fa fa-eye'></i></a>"+
                               "<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainSectionHandling/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')>"+
                               "<i style = 'margin-left:5px;'  class='fa fa-clock-o'></i>"+
                               "</a>" +
                               "</center>",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN009_Log objPLN009_Log = db.PLN009_Log.Where(x => x.Id == Id).FirstOrDefault();
                //if (objPLN009_Log == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadSectionHandlingHistoryFormPartial(int Id = 0)
        {
            PLN009_Log objPLN009_Log = db.PLN009_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN009_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN009_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN009_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN009_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN009_Log.ApprovedBy = objPLN009_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN009_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN009_Log.Customer = objPLN009_Log.Customer + " - " + customerName;
                string strSectionHandling = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strSectionHandling &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    //if (latestRecord.HeaderId == objPLN003.HeaderId)
                    //{

                    //}
                }
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN009/" + objPLN009_Log.HeaderId + "/" + objPLN009_Log.RevNo));
            }
            else
            {
                objPLN009_Log = new PLN009_Log();
                objPLN009_Log.ProcessPlan = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                objPLN009_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN009_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            //var urlPrefix = "http://" + Request.Url.Authority + "/PLN/MaintainSectionHandling/ViewHistory?Id=";// AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            var urlPrefix = WebsiteURL + "/PLN/MaintainSectionHandling/ViewHistory?Id=";
            if (!objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.SHOP.GetStringValue()))
            {
                ViewBag.RevPrev = (db.PLN009_Log.Any(q => (q.HeaderId == objPLN009_Log.HeaderId && q.RevNo == (objPLN009_Log.RevNo - 1))) ? urlPrefix + db.PLN009_Log.Where(q => (q.HeaderId == objPLN009_Log.HeaderId && q.RevNo == (objPLN009_Log.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.PLN009_Log.Any(q => (q.HeaderId == objPLN009_Log.HeaderId && q.RevNo == (objPLN009_Log.RevNo + 1))) ? urlPrefix + db.PLN009_Log.Where(q => (q.HeaderId == objPLN009_Log.HeaderId && q.RevNo == (objPLN009_Log.RevNo + 1))).FirstOrDefault().Id : null);
                ViewBag.Isshopuser = false;
            }
            else
            {
                ViewBag.Isshopuser = true;
            }
            return PartialView("_LoadSectionHandlingHistoryFormPartial", objPLN009_Log);
        }

        [HttpPost]
        public JsonResult LoadCheckListHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int Id = Convert.ToInt32(param.CTQHeaderId);
                PLN009_Log objPLN009_Log = db.PLN009_Log.Where(x => x.Id == Id).FirstOrDefault();

                strWhere += "1=1 and pln010.RefId = " + objPLN009_Log.Id + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var lstResult = db.SP_FETCH_SECTIONHANDLING_HEADER_LINE_HISTORY
                                (
                                     objPLN009_Log.HeaderId,
                                     StartIndex,
                                     EndIndex,
                                     "",
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,true,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,true),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                PLN009 objPLN009 = db.PLN009.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objPLN009 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public JsonResult LoadCheckListDataNew(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                Session["Headerid"] = Convert.ToInt32(param.CTQHeaderId);
                PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string HeaderStatus = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                bool IsDisable = false;
                if (objPLN009 != null)
                {
                    HeaderStatus = objPLN009.Status;
                }
                if (HeaderId == 0 || (HeaderId > 0 && (HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower() || HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.Approved.GetStringValue().ToLower())))
                {
                    IsDisable = true;
                }
                string strSectionHandling = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strSectionHandling &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln010.HeaderId = " + HeaderId + " and ";
                    PLN010 objPLN010 = db.PLN010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN010 != null)
                    {
                        PlanRev = objPLN010.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                #region Sorting

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstResult = db.SP_FETCH_SECTIONHANDLING_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                (new PLNCommonFunction()).CheckListLineOperationYesNoNew(uc.Yes_No,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Yes_No","UpdateData(this)"),
                                (new PLNCommonFunction()).CheckListLineOperationRemarksNew(uc.Remarks,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Remarks","UpdateData(this)"),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                //getChecklistDetails(uc.CheckListId.ToString())[0],
                                //getChecklistDetails(uc.CheckListId.ToString())[1],
                                //getChecklistDetails(uc.CheckListId.ToString())[2],
                                //getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateData(string id, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var splitid = id.Split(',');
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("pln010", "LineId", splitid[0], columnName, columnValue);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN009 objPLN009 = db.PLN009.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objPLN009 != null)
                {
                    objPLN009.RevNo = Convert.ToInt32(objPLN009.RevNo) + 1;
                    objPLN009.ReturnRemark = null;
                    objPLN009.ApprovedOn = null;
                    objPLN009.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objPLN009.ReviseRemark = strRemarks;
                    objPLN009.EditedBy = objClsLoginInfo.UserName;
                    objPLN009.EditedOn = DateTime.Now;
                    objPLN009.SubmittedBy = null;
                    objPLN009.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPLN009.HeaderId, objPLN009.Status, objPLN009.RevNo, objPLN009.Project, objPLN009.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objPLN009.HeaderId;
                    objResponseMsg.Status = objPLN009.Status;
                    objResponseMsg.rev = objPLN009.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_SECTIONHANDLING_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = Convert.ToString(li.Project),
                                      Document = Convert.ToString(li.Document),
                                      Customer = Convert.ToString(li.Customer),
                                      Product = Convert.ToString(li.Product),
                                      ProcessLicensor = Convert.ToString(li.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + li.RevNo),
                                      Status = Convert.ToString(li.Status),
                                      CDD = Convert.ToString(li.CDD),
                                      ProcessPlan = Convert.ToString(li.ProcessPlan),
                                      CreatedBy = Convert.ToString(li.CreatedBy),
                                      CreatedOn = Convert.ToString(li.CreatedOn),
                                      EditedBy = Convert.ToString(li.EditedBy),
                                      EditedOn = Convert.ToString(li.EditedOn),
                                      SubmittedBy = Convert.ToString(li.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),

                                      ApprovedBy = Convert.ToString(li.ApprovedBy),
                                      ApprovedOn = Convert.ToString(li.ApprovedOn),
                                      ReturnRemark = Convert.ToString(li.ReturnRemark),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    string strSectionHandling = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                    string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                    var latestRecord = db.PLN001
                             .Where(x
                                        => x.Plan == strSectionHandling &&
                                           x.Status == strStatus
                                    )
                             .OrderByDescending(x => x.HeaderId)
                             .Select(x => new { x.HeaderId, x.RevNo })
                             .FirstOrDefault();
                    // var lst = db.SP_FETCH_SECTIONHANDLING_HEADER_LINE(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    var lst = db.SP_FETCH_SECTIONHANDLING_HEADER_LINE(headerid, latestRecord.RevNo, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RelatedTo = Convert.ToString(li.RelatedTo),
                                      CheckPointDesc = Convert.ToString(li.CheckPointDesc),
                                      //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                      Remarks = li.Remarks,

                                      PreparedBy = getChecklistDetails(li.CheckListId.ToString())[0],
                                      CheckedBy = getChecklistDetails(li.CheckListId.ToString())[1],
                                      CheckedOn = getChecklistDetails(li.CheckListId.ToString())[2],
                                      DocumentNo = getChecklistDetails(li.CheckListId.ToString())[3]

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {  //tack weld lines
                    var lst = db.SP_FETCH_SECTIONHANDLING_HEADERS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      //Convert.ToString(li.Project),
                                      //Convert.ToString(uc.Document),
                                      //Convert.ToString(uc.Customer),
                                      //Convert.ToString(uc.Product),
                                      //Convert.ToString(uc.ProcessLicensor),
                                      //Convert.ToString("R" + uc.RevNo),
                                      //Convert.ToString(uc.Status),
                                      //Convert.ToString(uc.CDD),
                                      //Convert.ToString(uc.ProcessPlan),
                                      //Convert.ToString(uc.CreatedBy),
                                      //Convert.ToString(uc.CreatedOn),
                                      //Convert.ToString(uc.EditedBy),
                                      //Convert.ToString(uc.EditedOn),
                                      //Convert.ToString(uc.SubmittedBy),
                                      //Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                      //Convert.ToString(uc.ApprovedBy),
                                      //Convert.ToString(uc.ApprovedOn),
                                      //Convert.ToString(uc.ReturnRemark),
                                      Project = li.Project,
                                      Documents = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      ProcessPlan = li.ProcessPlan,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn == null || li.SubmittedOn.Value == DateTime.MinValue ? "NA" : li.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = li.ApprovedBy,
                                      ApproveOn = li.ApprovedOn == null || li.ApprovedOn.Value == DateTime.MinValue ? "NA" : li.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                       ReturnRemark = li.ReturnRemark

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_FETCH_SECTIONHANDLING_HEADER_LINE_HISTORY(HeaderId,1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RowNo = li.ROW_NO,
                                      RelatedTo=li.RelatedTo,
                                      CheckPoint=li.CheckPointDesc,
                                      YesNo=li.Yes_No,
                                      Remarks = li.Remarks,
                                     
                                      PreparedBy = li.CreatedBy,
                                      PreparedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),


                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        //public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        string strFileName = string.Empty;
        //        if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
        //        {
        //            var lst = db.SP_FETCH_SECTIONHANDLING_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
        //            if (!lst.Any())
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "No Data Found";
        //                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //            }
        //            var newlst = (from li in lst
        //                          select new
        //                          {
        //                              Project = Convert.ToString(li.Project),
        //                              Document = Convert.ToString(li.Document),
        //                              Customer = Convert.ToString(li.Customer),
        //                              Product = Convert.ToString(li.Product),
        //                              ProcessLicensor = Convert.ToString(li.ProcessLicensor),
        //                              RevNo = Convert.ToString("R" + li.RevNo),
        //                              Status = Convert.ToString(li.Status),
        //                              CDD = Convert.ToString(li.CDD),
        //                              ProcessPlan = Convert.ToString(li.ProcessPlan),
        //                              CreatedBy = Convert.ToString(li.CreatedBy),
        //                              CreatedOn = Convert.ToString(li.CreatedOn),
        //                              EditedBy = Convert.ToString(li.EditedBy),
        //                              EditedOn = Convert.ToString(li.EditedOn),
        //                              SubmittedBy = Convert.ToString(li.SubmittedBy),
        //                              SubmittedOn = Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),

        //                              ApprovedBy = Convert.ToString(li.ApprovedBy),
        //                              ApprovedOn = Convert.ToString(li.ApprovedOn),
        //                              ReturnRemark = Convert.ToString(li.ReturnRemark),
        //                          }).ToList();

        //            strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
        //        }

        //        else
        //        {
        //            int headerid = Convert.ToInt32(Session["Headerid"]);
        //            string strSectionHandling = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
        //            string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
        //            var latestRecord = db.PLN001
        //                     .Where(x
        //                                => x.Plan == strSectionHandling &&
        //                                   x.Status == strStatus
        //                            )
        //                     .OrderByDescending(x => x.HeaderId)
        //                     .Select(x => new { x.HeaderId, x.RevNo })
        //                     .FirstOrDefault();
        //            // var lst = db.SP_FETCH_SECTIONHANDLING_HEADER_LINE(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
        //            var lst = db.SP_FETCH_SECTIONHANDLING_HEADER_LINE(headerid, latestRecord.RevNo, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
        //            if (!lst.Any())
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "No Data Found";
        //                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //            }
        //            var newlst = (from li in lst
        //                          select new
        //                          {
        //                              RelatedTo = Convert.ToString(li.RelatedTo),
        //                              CheckPointDesc = Convert.ToString(li.CheckPointDesc),
        //                              //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
        //                              Remarks = li.Remarks,

        //                              PreparedBy = getChecklistDetails(li.CheckListId.ToString())[0],
        //                              CheckedBy = getChecklistDetails(li.CheckListId.ToString())[1],
        //                              CheckedOn = getChecklistDetails(li.CheckListId.ToString())[2],
        //                              DocumentNo = getChecklistDetails(li.CheckListId.ToString())[3]

        //                          }).ToList();

        //            strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
        //        }

        //        objResponseMsg.Key = true;
        //        objResponseMsg.Value = strFileName;
        //        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = "Error in excel generate, Please try again";
        //        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //    }
        //}
    }
}