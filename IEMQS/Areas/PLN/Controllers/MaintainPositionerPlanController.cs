﻿using IEMQS.Areas.MSW.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.Utility.Models;
namespace IEMQS.Areas.PLN.Controllers
{
    public class MaintainPositionerPlanController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult LoadPositionerPlanListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_PositionerPlanListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadPositionerPlanHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.PlanStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                //strWhere += " and pln013.CreatedBy in('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln013.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln013.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_POSITIONERPLAN_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               uc.CreatedOn.HasValue ?  Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.EditedBy),
                               uc.EditedOn.HasValue ?  Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.SubmittedBy),
                               uc.SubmittedOn.HasValue ?  Convert.ToString(uc.SubmittedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ApprovedBy),
                               uc.ApprovedOn.HasValue ?  Convert.ToString(uc.ApprovedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ReturnRemark),
                                 "<nobr><center>"+"<a title='View' href='"+WebsiteURL+"/PLN/MaintainPositionerPlan/CreatePositionerPlan?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='' class='iconspace fa fa-eye'></i></a>"+
                               Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/PLN/MaintainPositionerPlan/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblPositionerPlanHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                               (uc.RevNo>0 ?"<a title=\"History\" onclick=\"histroy('"+uc.HeaderId+"')\"><i style='' class='iconspace fa fa-history'></i></a>":"<a title=\"History\"><i style='' class='disabledicon fa fa-history'></i></a>")+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainPositionerPlan/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='' class='iconspace fa fa-clock-o'></i></a>"
                               + "</center></nobr>",
                             //   "<center><a title='View' href='/PLN/MaintainPositionerPlan/CreatePositionerPlan?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Positioner_Plan);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Position Plan TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                PLN013_Log objPLN013_Log = db.PLN013_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN013_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN013_Log.CreatedBy) : null;
                model.CreatedOn = objPLN013_Log.CreatedOn;
                model.EditedBy = objPLN013_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN013_Log.EditedBy) : null;
                model.EditedOn = objPLN013_Log.EditedOn;
                model.SubmittedBy = objPLN013_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN013_Log.SubmittedBy) : null;
                model.SubmittedOn = objPLN013_Log.SubmittedOn;

                model.ApprovedBy = objPLN013_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN013_Log.ApprovedBy) : null;
                model.ApprovedOn = objPLN013_Log.ApprovedOn;

            }
            else
            {

                PLN013_Log objPLN013_Log = db.PLN013_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN013_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN013_Log.CreatedBy) : null;
                model.CreatedOn = objPLN013_Log.CreatedOn;
                model.EditedBy = objPLN013_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN013_Log.EditedBy) : null;
                model.EditedOn = objPLN013_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "Positioner Plan Timeline";

            if (HeaderId > 0)
            {
                PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN013.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN013.CreatedBy) : null;
                model.CreatedOn = objPLN013.CreatedOn;
                model.EditedBy = objPLN013.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN013.EditedBy) : null;
                model.EditedOn = objPLN013.EditedOn;
                model.SubmittedBy = objPLN013.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN013.SubmittedBy) : null;
                model.SubmittedOn = objPLN013.SubmittedOn;
                model.ApprovedBy = objPLN013.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN013.ApprovedBy) : null;
                model.ApprovedOn = objPLN013.ApprovedOn;

            }
            else
            {
                PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN013.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN013.CreatedBy) : null;
                model.CreatedOn = objPLN013.CreatedOn;
                model.EditedBy = objPLN013.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN013.EditedBy) : null;
                model.EditedOn = objPLN013.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult CreatePositionerPlan(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN013");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                //if (objPLN013 == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}

                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN013.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderID && x.CreatedBy == currentUser).FirstOrDefault();
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadPositionerPlanCreateFormPartial(int HeaderID = 0)
        {
            PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN013 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN013.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN013.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ApproverName))
                {
                    ViewBag.ApproverName = objPLN013.ApprovedBy + " - " + ApproverName;
                }
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN013.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objPLN013.Customer + " - " + customerName;
                string strPositionerPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN013/" + objPLN013.HeaderId + "/" + objPLN013.RevNo));
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strPositionerPlan &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    PLN014 objPLN014 = db.PLN014.Where(x => x.HeaderId == objPLN013.HeaderId).FirstOrDefault();
                    if (objPLN014 != null)
                    {
                        if (latestRecord.RevNo == objPLN014.PlanRevNo)
                        {
                            ViewBag.isLatest = "true";
                        }
                        else
                        {
                            ViewBag.isLatest = "false";
                        }
                    }
                    else
                    {
                        ViewBag.isLatest = "false";
                    }
                }
                else
                {
                    ViewBag.isLatest = "true";
                }
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN013.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objPLN013.HeaderId, objPLN013.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            else
            {
                objPLN013 = new PLN013();
                objPLN013.ProcessPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                ViewBag.isLatest = "true";
                objPLN013.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN013.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadPositionerPlanCreateFormPartial", objPLN013);
        }

        [HttpPost]
        public JsonResult LoadCheckListData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string strPositionerPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strPositionerPlan &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln014.HeaderId = " + HeaderId + " and ";
                    PLN014 objPLN014 = db.PLN014.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN014 != null)
                    {
                        PlanRev = objPLN014.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
               
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_POSITIONERPLAN_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,false,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,false),
                                Convert.ToString(uc.CreatedBy),
                                uc.CreatedOn.HasValue ?  Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.EditedBy),
                                uc.EditedOn.HasValue ?  Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                getChecklistDetails(uc.CheckListId.ToString())[0],
                                getChecklistDetails(uc.CheckListId.ToString())[1],
                                getChecklistDetails(uc.CheckListId.ToString())[2],
                                getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [NonAction]
        public string[] getChecklistDetails(string lineId)
        {
            string[] checklist = new string[4];
            int LineID = Int32.Parse(lineId);
            var data = (from p2 in db.PLN002
                        join
                            p1 in db.PLN001 on p2.HeaderId equals p1.HeaderId
                        where p2.LineId == LineID
                        select p1).FirstOrDefault();
            if (data != null)
            {
                checklist[0] = Manager.GetUserNameFromPsNo(data.CreatedBy); //prepared by
                checklist[1] = Manager.GetUserNameFromPsNo(data.ApprovedBy); // approved by
                checklist[2] = data.ApprovedOn.ToString(); // approved on
                checklist[3] = data.Document; // document no
            }
            return checklist;
        }

        [HttpPost]
        public ActionResult SaveHeader(PLN013 pln013, string strHeaderId, string strLineId, string strIsLatest)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln013.HeaderId > 0)
                {
                    PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == pln013.HeaderId).FirstOrDefault();
                    if (objPLN013 != null)
                    {
                        objPLN013.Product = pln013.Product;
                        objPLN013.ProcessLicensor = pln013.ProcessLicensor;
                        objPLN013.JobNo = pln013.JobNo;
                        objPLN013.ApprovedBy = pln013.ApprovedBy;
                        objPLN013.ReviseRemark = pln013.ReviseRemark;
                        objPLN013.EditedBy = objClsLoginInfo.UserName;
                        objPLN013.EditedOn = DateTime.Now;
                        if (objPLN013.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN013.RevNo = Convert.ToInt32(objPLN013.RevNo) + 1;
                            objPLN013.ReturnRemark = null;
                            objPLN013.ApprovedOn = null;
                            objPLN013.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        if (!Convert.ToBoolean(strIsLatest))
                        {
                            List<PLN014> lstPLN014 = db.PLN014.Where(x => x.HeaderId == objPLN013.HeaderId).ToList();
                            if (lstPLN014 != null && lstPLN014.Count > 0)
                            {
                                db.PLN014.RemoveRange(lstPLN014);
                            }
                            string strPositionerPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                            string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                            var latestRecord = db.PLN001
                                     .Where(x
                                                => x.Plan == strPositionerPlan &&
                                                   x.Status == strStatus
                                            )
                                     .OrderByDescending(x => x.HeaderId)
                                     .Select(x => new { x.HeaderId, x.RevNo })
                                     .FirstOrDefault();
                            List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == latestRecord.HeaderId).ToList();
                            if (lstPLN002 != null && lstPLN002.Count > 0)
                            {
                                List<PLN014> NewlstPLN014 = lstPLN002.Select(x => new PLN014()
                                {
                                    LineId = x.LineId,
                                    HeaderId = objPLN013.HeaderId,
                                    Project = objPLN013.Project,
                                    Document = objPLN013.Document,
                                    RevNo = objPLN013.RevNo,
                                    Plan = objPLN013.ProcessPlan,
                                    PlanRevNo = x.RevNo,
                                    CheckListId = x.LineId,
                                    //Yes_No = false,
                                    //Yes_No = clsImplementationEnum.ChecklistYesNo.No.GetStringValue(), as per observation 15003
                                    Yes_No = string.Empty,
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                }).ToList();
                                db.PLN014.AddRange(NewlstPLN014);
                            }
                        }
                        else
                        {
                            List<PLN014> lstPLN014 = db.PLN014.Where(x => x.HeaderId == objPLN013.HeaderId).ToList();
                            if (lstPLN014.Count > 0)
                            {
                                List<PLN014> lstTruePLN014 = new List<PLN014>();

                                foreach (PLN014 objPLN014 in lstPLN014)
                                {
                                    //objPLN014.Yes_No = (!string.IsNullOrWhiteSpace(strLineId) && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Length > 0 && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Contains(objPLN014.LineId) ? true : false);
                                    objPLN014.EditedBy = objClsLoginInfo.UserName;
                                    objPLN014.EditedOn = DateTime.Now;
                                }
                            }
                        }

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN013.HeaderId;
                        objResponseMsg.Status = objPLN013.Status;
                        objResponseMsg.Revision = objPLN013.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN013.HeaderId, objPLN013.Status, objPLN013.RevNo, objPLN013.Project, objPLN013.Document);

                        //if (hasAttachments)
                        //{
                        var folderPath = "PLN013/" + NewHeaderId + "/" + objPLN013.RevNo;
                        //    var existing = clsUpload.getDocs(folderPath);
                        //    var toDelete = new Dictionary<string, string>();
                        //    foreach (var item in existing)
                        //    {
                        //        if (Attach.Where(q => q.Key.Equals(item.Key)).Count() <= 0)
                        //            toDelete.Add(item.Key, item.Value);
                        //        else
                        //        {
                        //            if (Attach.Where(q => q.Key.Equals(item.Key)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                        //                toDelete.Add(item.Key, item.Value);
                        //        }
                        //    }
                        //    foreach (var item in toDelete)
                        //        clsUpload.DeleteFile(folderPath, item.Key);
                        //    var toUpload = Attach.Where(q => q.Value.Substring(0, 3) != "URL");

                        //    foreach (var attch in toUpload)
                        //    {
                        //        var base64Data = attch.Value;
                        //        var dataBytes = Helper.FromBase64(attch.Value);
                        //        //var dataBytes = Encoding.ASCII.GetBytes(stringData);
                        //        clsUpload.Upload(attch.Key, dataBytes, folderPath);
                        //    }
                        //}
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                    PLN013 objPLN013 = db.PLN013.Add(new PLN013
                    {
                        Project = pln013.Project,
                        Document = pln013.Document,
                        Customer = pln013.Customer,
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CDD = Helper.ToNullIfTooEarlyForDb(pln013.CDD),
                        Product = pln013.Product,
                        ProcessLicensor = pln013.ProcessLicensor,
                        ProcessPlan = pln013.ProcessPlan,
                        JobNo = pln013.JobNo,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ApprovedBy = pln013.ApprovedBy
                    });
                    db.SaveChanges();
                    NewHeaderId = objPLN013.HeaderId;
                    if (!string.IsNullOrWhiteSpace(strHeaderId))
                    {
                        int planHeaderId = Convert.ToInt32(strHeaderId);
                        List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == planHeaderId).ToList();
                        List<PLN014> lstPLN014 = new List<PLN014>();
                        if (lstPLN002.Count > 0)
                        {
                            List<PLN002> lstTruePLN002 = new List<PLN002>();
                            if (!string.IsNullOrWhiteSpace(strLineId))
                            {
                                int[] arrayLineId = Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s));
                                lstTruePLN002 = lstPLN002.Where(x => arrayLineId.Contains(x.LineId)).ToList();
                            }
                            foreach (PLN002 objPLN002 in lstPLN002)
                            {
                                PLN014 objPLN014 = new PLN014();
                                objPLN014.HeaderId = objPLN013.HeaderId;
                                objPLN014.Project = objPLN013.Project;
                                objPLN014.Document = objPLN013.Document;
                                objPLN014.RevNo = objPLN013.RevNo;
                                objPLN014.Plan = objPLN013.ProcessPlan;
                                objPLN014.PlanRevNo = objPLN002.RevNo;
                                objPLN014.CheckListId = objPLN002.LineId;
                                //objPLN014.Yes_No = (lstTruePLN002.Where(x => x.LineId == objPLN002.LineId).FirstOrDefault() != null ? true : false);
                                objPLN014.CreatedBy = objClsLoginInfo.UserName;
                                objPLN014.CreatedOn = DateTime.Now;
                                lstPLN014.Add(objPLN014);
                            }
                        }
                        if (lstPLN014 != null && lstPLN014.Count > 0)
                        {
                            db.PLN014.AddRange(lstPLN014);
                            db.SaveChanges();
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.Status = objPLN013.Status;
                    objResponseMsg.Revision = objPLN013.RevNo;
                    objResponseMsg.HeaderID = NewHeaderId;

                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN013.HeaderId, objPLN013.Status, objPLN013.RevNo, objPLN013.Project, objPLN013.Document);

                    //if (hasAttachments)
                    //{
                    var folderPath = "PLN013/" + NewHeaderId + "/" + objPLN013.RevNo;
                    //    var existing = clsUpload.getDocs(folderPath);
                    //    var toDelete = new Dictionary<string, string>();
                    //    foreach (var item in existing)
                    //    {
                    //        if (Attach.Where(q => q.Key.Equals(item.Key)).Count() <= 0)
                    //            toDelete.Add(item.Key, item.Value);
                    //        else
                    //        {
                    //            if (Attach.Where(q => q.Key.Equals(item.Key)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                    //                toDelete.Add(item.Key, item.Value);
                    //        }
                    //    }
                    //    foreach (var item in toDelete)
                    //        clsUpload.DeleteFile(folderPath, item.Key);
                    //    var toUpload = Attach.Where(q => q.Value.Substring(0, 3) != "URL");

                    //    foreach (var attch in toUpload)
                    //    {
                    //        var base64Data = attch.Value;
                    //        var dataBytes = Helper.FromBase64(attch.Value);
                    //        //var dataBytes = Encoding.ASCII.GetBytes(stringData);
                    //        clsUpload.Upload(attch.Key, dataBytes, folderPath);
                    //    }
                    //}
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHeaderLine(int LineId, int HeaderId, string ActualId, string YesNo, string Remarks)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                var splitid = ActualId.Split(',');
                if (Convert.ToInt32(splitid[0]) == 0)
                {
                    PLN014 objPLN014 = db.PLN014.Where(x => x.CheckListId == LineId && x.HeaderId == HeaderId).FirstOrDefault();
                    objPLN014.Yes_No = YesNo;
                    objPLN014.Remarks = Remarks;
                    objPLN014.EditedBy = objClsLoginInfo.UserName;
                    objPLN014.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    int ActualLineId = Convert.ToInt32(splitid[0]);
                    PLN014 objPLN014 = db.PLN014.Where(x => x.LineId == ActualLineId && x.HeaderId == HeaderId).FirstOrDefault();
                    objPLN014.Yes_No = YesNo;
                    objPLN014.Remarks = Remarks;
                    objPLN014.EditedBy = objClsLoginInfo.UserName;
                    objPLN014.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrLineStatus = { clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(), clsImplementationEnum.PlanStatus.Returned.GetStringValue() };
                PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                if (objPLN013 != null)
                {
                    objPLN013.Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue();
                    objPLN013.SubmittedBy = objClsLoginInfo.UserName;
                    objPLN013.SubmittedOn = DateTime.Now;
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN013.HeaderId, objPLN013.Status, objPLN013.RevNo, objPLN013.Project, objPLN013.Document);

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully sent for approval.";

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                        objPLN013.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN013.Project, clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue(), objPLN013.RevNo.Value.ToString(), objPLN013.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue(), objPLN013.HeaderId.ToString(), true),
                                                        objPLN013.ApprovedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //added by nikita on 31/08/2017
        [HttpPost]
        public ActionResult RetractStatus(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN013 objPLN013 = db.PLN013.Where(u => u.HeaderId == headerId).SingleOrDefault();
                if (objPLN013 != null)
                {
                    objPLN013.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                    objPLN013.EditedBy = objClsLoginInfo.UserName;
                    objPLN013.EditedOn = DateTime.Now;
                    objPLN013.SubmittedOn = null;
                    objPLN013.SubmittedBy = null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                    Manager.UpdatePDN002(objPLN013.HeaderId, objPLN013.Status, objPLN013.RevNo, objPLN013.Project, objPLN013.Document);
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string LineOperation(bool flag, int LineId, int HeaderId)
        {
            if (flag)
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" checked=\"checked\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
            else
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
        }

        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            string strCdd = Manager.GetContractWiseCdd(projectCode);
            if (!string.IsNullOrWhiteSpace(strCdd))
            {
                DateTime dtCdd = Convert.ToDateTime(strCdd);
                objApproverModel.Cdd = dtCdd.Date.ToShortDateString();
            }
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHistoryDetails(string strRole, int headerid)
        {
            ViewBag.Role = strRole;
            ViewBag.HeaderId = headerid;
            Session["Headerid"] = Convert.ToInt32(headerid);
            return PartialView("_GetHistoryDetailsPartial");
        }

        [HttpPost]
        public JsonResult LoadPositionerPlanHistory(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                var HeaderId = Convert.ToInt32(Request["HeaderId"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                //if (param.CTQCompileStatus.ToUpper() == "INITIATOR")
                //{
                //    strWhere += "1=1  and HeaderId='" + Convert.ToInt32(Session["Headerid"]) + "'";

                //}
                //else
                //{
                //    strWhere += "1=1 and pln013.ApprovedBy = '" + objClsLoginInfo.UserName + "'";
                //}
                strWhere += "1=1  and HeaderId='" + HeaderId + "'";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln013.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln013.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_POSITIONERPLAN_HEADERS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               uc.CreatedOn.HasValue ?  Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.EditedBy),
                               uc.EditedOn.HasValue ?  Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.SubmittedBy),
                               uc.SubmittedOn.HasValue ?  Convert.ToString(uc.SubmittedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ApprovedBy),
                               uc.ApprovedOn.HasValue ?  Convert.ToString(uc.ApprovedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ReturnRemark),
                               (param.CTQCompileStatus.ToUpper() == "INITIATOR"?"<center><a title='View' href='"+WebsiteURL+"/PLN/MaintainPositionerPlan/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainPositionerPlan/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>":"<center><a title='View' href='"+WebsiteURL+"/PLN/ApprovePositionerPlan/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainPositionerPlan/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN013_Log objPLN013_Log = db.PLN013_Log.Where(x => x.Id == Id && x.CreatedBy == currentUser).FirstOrDefault();
                //if (objPLN013_Log == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadPositionerPlanHistoryFormPartial(int Id = 0)
        {
            PLN013_Log objPLN013_Log = db.PLN013_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN013_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN013_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN013_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN013_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN013_Log.ApprovedBy = objPLN013_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN013_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN013_Log.Customer = objPLN013_Log.Customer + " - " + customerName;
                string strPositionerPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strPositionerPlan &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    //if (latestRecord.HeaderId == objPLN003.HeaderId)
                    //{

                    //}
                }
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN013/" + objPLN013_Log.HeaderId + "/" + objPLN013_Log.RevNo));
            }
            else
            {
                objPLN013_Log = new PLN013_Log();
                objPLN013_Log.ProcessPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                objPLN013_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN013_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            //var urlPrefix = "http://" + Request.Url.Authority + "/PLN/MaintainPositionerPlan/ViewHistory?Id=";// AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            var urlPrefix = WebsiteURL + "/PLN/MaintainPositionerPlan/ViewHistory?Id=";
            if (!objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.SHOP.GetStringValue()))
            {
                ViewBag.RevPrev = (db.PLN013_Log.Any(q => (q.HeaderId == objPLN013_Log.HeaderId && q.RevNo == (objPLN013_Log.RevNo - 1))) ? urlPrefix + db.PLN013_Log.Where(q => (q.HeaderId == objPLN013_Log.HeaderId && q.RevNo == (objPLN013_Log.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.PLN013_Log.Any(q => (q.HeaderId == objPLN013_Log.HeaderId && q.RevNo == (objPLN013_Log.RevNo + 1))) ? urlPrefix + db.PLN013_Log.Where(q => (q.HeaderId == objPLN013_Log.HeaderId && q.RevNo == (objPLN013_Log.RevNo + 1))).FirstOrDefault().Id : null);
                ViewBag.Isshopuser = false;
            }
            else
            {
                ViewBag.Isshopuser = true;
            }
            return PartialView("_LoadPositionerPlanHistoryFormPartial", objPLN013_Log);
        }

        [HttpPost]
        public JsonResult LoadCheckListHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int Id = Convert.ToInt32(param.CTQHeaderId);
                PLN013_Log objPLN013_Log = db.PLN013_Log.Where(x => x.Id == Id).FirstOrDefault();

                strWhere += "1=1 and pln014.RefId = " + objPLN013_Log.Id + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_POSITIONERPLAN_HEADER_LINE_HISTORY
                                (
                                     objPLN013_Log.HeaderId,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,true,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,true),
                                Convert.ToString(uc.CreatedBy),
                                uc.CreatedOn.HasValue ?  Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.EditedBy),
                                uc.EditedOn.HasValue ?  Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                PLN013 objPLN013 = db.PLN013.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objPLN013 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public JsonResult LoadCheckListDataNew(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string HeaderStatus = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                bool IsDisable = false;
                if (objPLN013 != null)
                {
                    HeaderStatus = objPLN013.Status;
                }
                if (HeaderId == 0 || (HeaderId > 0 && (HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower() || HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.Approved.GetStringValue().ToLower())))
                {
                    IsDisable = true;
                }
                string strPositionerPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strPositionerPlan &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln014.HeaderId = " + HeaderId + " and ";
                    PLN014 objPLN014 = db.PLN014.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN014 != null)
                    {
                        PlanRev = objPLN014.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_POSITIONERPLAN_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                (new PLNCommonFunction()).CheckListLineOperationYesNoNew(uc.Yes_No,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Yes_No","UpdateData(this)"),
                                (new PLNCommonFunction()).CheckListLineOperationRemarksNew(uc.Remarks,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Remarks","UpdateData(this)"),
                                Convert.ToString(uc.CreatedBy),
                                uc.CreatedOn.HasValue ?  Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.EditedBy),
                                uc.EditedOn.HasValue ?  Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                //getChecklistDetails(uc.CheckListId.ToString())[0],
                                //getChecklistDetails(uc.CheckListId.ToString())[1],
                                //getChecklistDetails(uc.CheckListId.ToString())[2],
                                //getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN013 objPLN013 = db.PLN013.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objPLN013 != null)
                {
                    objPLN013.RevNo = Convert.ToInt32(objPLN013.RevNo) + 1;
                    objPLN013.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objPLN013.ReviseRemark = strRemarks;
                    objPLN013.EditedBy = objClsLoginInfo.UserName;
                    objPLN013.EditedOn = DateTime.Now;
                    objPLN013.ReturnRemark = null;
                    objPLN013.ApprovedOn = null;
                    objPLN013.SubmittedBy = null;
                    objPLN013.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPLN013.HeaderId, objPLN013.Status, objPLN013.RevNo, objPLN013.Project, objPLN013.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objPLN013.HeaderId;
                    objResponseMsg.Status = objPLN013.Status;
                    objResponseMsg.rev = objPLN013.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateData(string id, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var splitid = id.Split(',');
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("pln014", "LineId", splitid[0], columnName, columnValue);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    #region Header
                    List<SP_FETCH_POSITIONERPLAN_HEADERS_Result> lst = db.SP_FETCH_POSITIONERPLAN_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = Convert.ToString(li.Project),
                                      Document = Convert.ToString(li.Document),
                                      Customer = Convert.ToString(li.Customer),
                                      Product = Convert.ToString(li.Product),
                                      ProcessLicensor = Convert.ToString(li.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + li.RevNo),
                                      Status = Convert.ToString(li.Status),
                                      CDD = Convert.ToString(li.CDD),
                                      ProcessPlan = Convert.ToString(li.ProcessPlan),
                                      CreatedBy = Convert.ToString(li.CreatedBy),
                                      CreatedOn = li.CreatedOn.HasValue ? Convert.ToString(li.CreatedOn.Value.ToShortDateString()) : "",
                                      EditedBy = Convert.ToString(li.EditedBy),
                                      EditedOn = li.EditedOn.HasValue ? Convert.ToString(li.EditedOn.Value.ToShortDateString()) : "",
                                      SubmittedBy = Convert.ToString(li.SubmittedBy),
                                      SubmittedOn = li.SubmittedOn.HasValue ? Convert.ToString(li.SubmittedOn.Value.ToShortDateString()) : "",
                                      ApprovedBy = Convert.ToString(li.ApprovedBy),
                                      ApprovedOn = li.ApprovedOn.HasValue ? Convert.ToString(li.ApprovedOn.Value.ToShortDateString()) : "",
                                      Remark = Convert.ToString(li.ReturnRemark),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                    #endregion
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    #region Lines
                    int? PlanRev = 0;
                    if (HeaderId > 0)
                    {
                        PLN014 objPLN014 = db.PLN014.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                        if (objPLN014 != null)
                        {
                            PlanRev = objPLN014.PlanRevNo;
                        }
                        else
                        {
                            PlanRev = 0;
                        }
                    }
                    else
                    {
                        string strPositionerPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                        string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                        var latestRecord = db.PLN001
                                 .Where(x
                                            => x.Plan == strPositionerPlan &&
                                               x.Status == strStatus
                                        )
                                 .OrderByDescending(x => x.HeaderId)
                                 .Select(x => new { x.HeaderId, x.RevNo })
                                 .FirstOrDefault();
                        PlanRev = latestRecord.RevNo;
                    }

                    var lst = db.SP_FETCH_POSITIONERPLAN_HEADER_LINE(HeaderId, PlanRev, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      RelatedTo = Convert.ToString(uc.RelatedTo),
                                      CheckPointDesc = Convert.ToString(uc.CheckPointDesc),
                                      Yes_No = uc.Yes_No,
                                      Remarks = uc.Remarks,
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                      EditedBy = Convert.ToString(uc.EditedBy),
                                      EditedOn = uc.EditedOn.HasValue ? Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                      //PreparedBy = getChecklistDetails(uc.CheckListId.ToString())[0],
                                      //CheckedBy = getChecklistDetails(uc.CheckListId.ToString())[1],
                                      //CheckedOn = getChecklistDetails(uc.CheckListId.ToString())[2],
                                      //DocumentNo = getChecklistDetails(uc.CheckListId.ToString())[3]
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                    #endregion
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    #region Header History
                    var lst = db.SP_FETCH_POSITIONERPLAN_HEADERS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project=Convert.ToString(uc.Project),
                                      Document = Convert.ToString(uc.Document),
                                      Customer = Convert.ToString(uc.Customer),
                                      Product = Convert.ToString(uc.Product),
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      Status = Convert.ToString(uc.Status),
                                      CDD = Convert.ToString(uc.CDD),
                                      ProcessPlan = Convert.ToString(uc.ProcessPlan),
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                      EditedBy = Convert.ToString(uc.EditedBy),
                                      EditedOn = uc.EditedOn.HasValue ? Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = uc.SubmittedOn.HasValue ? Convert.ToString(uc.SubmittedOn.Value.ToShortDateString()) : "",
                                      ApprovedBy = Convert.ToString(uc.ApprovedBy),
                                      ApprovedOn = uc.ApprovedOn.HasValue ? Convert.ToString(uc.ApprovedOn.Value.ToShortDateString()) : "",
                                      Remark = Convert.ToString(uc.ReturnRemark),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                    #endregion
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    #region History Lines
                    PLN013_Log objPLN013_Log = db.PLN013_Log.Where(x => x.Id == HeaderId).FirstOrDefault();

                    var lst = db.SP_FETCH_POSITIONERPLAN_HEADER_LINE_HISTORY(objPLN013_Log.HeaderId, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      RelatedTo=Convert.ToString(uc.RelatedTo),
                                      CheckPointDesc = Convert.ToString(uc.CheckPointDesc),                                      
                                      Yes_No = uc.Yes_No,
                                      Remarks = uc.Remarks,
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                      EditedBy = Convert.ToString(uc.EditedBy),
                                      EditedOn = uc.EditedOn.HasValue ? Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}