﻿using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQS.Areas.HTC.Controllers.MaintainHTCController;
using IEMQS.Areas.Utility.Models;
using System.Globalization;

namespace IEMQS.Areas.PLN.Controllers
{
    public class MaintainHydroTestController : clsBase
    {
        /// <summary>
        /// modified by nikita on 29/08/2017 (task Assigned by Satish Pawar)
        /// added  Manager.UpdatePDN002 method for update table PDN002 (old code is commented)
        /// </summary>
        /// <returns></returns>
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadHydroTestListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_HydroTestListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadHydroTestHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.PlanStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                //strWhere += " and pln007.CreatedBy in('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln007.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln007.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_FETCH_HYDROTEST_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                 Convert.ToString(uc.SubmittedBy),
                                uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                Convert.ToString(uc.ApprovedBy),
                                uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),  
                               Convert.ToString(uc.ReturnRemark),
                                  "<nobr><center>"+"<a title='View' href='"+WebsiteURL+"/PLN/MaintainHydroTest/CreateHydroTest?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='' class='iconspace fa fa-eye'></i></a>"+
                                Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/PLN/MaintainHydroTest/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblHydroTestHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                                (uc.RevNo>0 ?"<a title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Initiator','/PLN/MaintainHydroTest/GetHistoryDetails','Hydro Test')\"><i style='' class='iconspace fa fa-history'></i></a>":"<a title=\"History\"><i style='margin-left:5px;' class='disabledicon fa fa-history'></i></a>")+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainHydroTest/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='' class='iconspace fa fa-clock-o'></i></a>"
                                + "</center></nobr>",

                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Hydro_Test_Plan);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Hydro Test TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                PLN007_Log objPLN007_Log = db.PLN007_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN007_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN007_Log.CreatedBy) : null;
                model.CreatedOn = objPLN007_Log.CreatedOn;
                model.EditedBy = objPLN007_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN007_Log.EditedBy) : null;
                model.EditedOn = objPLN007_Log.EditedOn;
                model.SubmittedBy = objPLN007_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN007_Log.SubmittedBy) : null;
                model.SubmittedOn = objPLN007_Log.SubmittedOn;

                model.ApprovedBy = objPLN007_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN007_Log.ApprovedBy) : null;
                model.ApprovedOn = objPLN007_Log.ApprovedOn;

            }
            else
            {

                PLN007_Log objPLN007_Log = db.PLN007_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN007_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN007_Log.CreatedBy) : null;
                model.CreatedOn = objPLN007_Log.CreatedOn;
                model.EditedBy = objPLN007_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN007_Log.EditedBy) : null;
                model.EditedOn = objPLN007_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "Hydro Test Timeline";

            if (HeaderId > 0)
            {
                PLN007 objPLN007 = db.PLN007.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN007.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN007.CreatedBy) : null;
                model.CreatedOn = objPLN007.CreatedOn;
                model.EditedBy = objPLN007.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN007.EditedBy) : null;
                model.EditedOn = objPLN007.EditedOn;
                model.SubmittedBy = objPLN007.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN007.SubmittedBy) : null;
                model.SubmittedOn = objPLN007.SubmittedOn;
                model.ApprovedBy = objPLN007.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN007.ApprovedBy) : null;
                model.ApprovedOn = objPLN007.ApprovedOn;

            }
            else
            {
                PLN007 objPLN007 = db.PLN007.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN007.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN007.CreatedBy) : null;
                model.CreatedOn = objPLN007.CreatedOn;
                model.EditedBy = objPLN007.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN007.EditedBy) : null;
                model.EditedOn = objPLN007.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult CreateHydroTest(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN007");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN007 objPLN007 = db.PLN007.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                //if (objPLN007 == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN007.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                objPLN007 = db.PLN007.Where(x => x.HeaderId == HeaderID && x.CreatedBy == currentUser).FirstOrDefault();
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadHydroTestCreateFormPartial(int HeaderID = 0)
        {
            PLN007 objPLN007 = db.PLN007.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN007 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN007.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN007.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ApproverName))
                {
                    ViewBag.ApproverName = objPLN007.ApprovedBy + " - " + ApproverName;
                }
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN007.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objPLN007.Customer + " - " + customerName;
                string strHydroTest = clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN007/" + objPLN007.HeaderId + "/" + objPLN007.RevNo));
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strHydroTest &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    PLN008 objPLN008 = db.PLN008.Where(x => x.HeaderId == objPLN007.HeaderId).FirstOrDefault();
                    if (objPLN008 != null)
                    {
                        if (latestRecord.RevNo == objPLN008.PlanRevNo)
                        {
                            ViewBag.isLatest = "true";
                        }
                        else
                        {
                            ViewBag.isLatest = "false";
                        }
                    }
                    else
                    {
                        ViewBag.isLatest = "false";
                    }
                }
                else
                {
                    ViewBag.isLatest = "true";
                }

                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN007.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objPLN007.HeaderId, objPLN007.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            else
            {
                objPLN007 = new PLN007();
                objPLN007.ProcessPlan = clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue();
                ViewBag.isLatest = "true";
                objPLN007.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN007.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadHydroTestCreateFormPartial", objPLN007);
        }

        [HttpPost]
        public JsonResult LoadCheckListData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN007 objPLN007 = db.PLN007.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string strHydroTest = clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strHydroTest &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln008.HeaderId = " + HeaderId + " and ";
                    PLN008 objPLN008 = db.PLN008.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN008 != null)
                    {
                        PlanRev = objPLN008.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_FETCH_HYDROTEST_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,false,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,false),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                getChecklistDetails(uc.CheckListId.ToString())[0],
                                getChecklistDetails(uc.CheckListId.ToString())[1],
                                getChecklistDetails(uc.CheckListId.ToString())[2],
                                getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public string[] getChecklistDetails(string lineId)
        {
            string[] checklist = new string[4];
            int LineID = Int32.Parse(lineId);
            var data = (from p2 in db.PLN002
                        join
                            p1 in db.PLN001 on p2.HeaderId equals p1.HeaderId
                        where p2.LineId == LineID
                        select p1).FirstOrDefault();
            if (data != null)
            {
                checklist[0] = Manager.GetUserNameFromPsNo(data.CreatedBy); //prepared by
                checklist[1] = Manager.GetUserNameFromPsNo(data.ApprovedBy); // approved by
                checklist[2] = data.ApprovedOn.ToString(); // approved on
                checklist[3] = data.Document; // document no
            }
            return checklist;
        }

        [HttpPost]
        public ActionResult SaveHeader(PLN007 pln007, string strHeaderId, string strLineId, string strIsLatest)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln007.HeaderId > 0)
                {
                    PLN007 objPLN007 = db.PLN007.Where(x => x.HeaderId == pln007.HeaderId).FirstOrDefault();
                    if (objPLN007 != null)
                    {
                        objPLN007.Product = pln007.Product;
                        objPLN007.ProcessLicensor = pln007.ProcessLicensor;
                        objPLN007.JobNo = pln007.JobNo;
                        objPLN007.ApprovedBy = pln007.ApprovedBy;
                        objPLN007.ReviseRemark = pln007.ReviseRemark;
                        objPLN007.EditedBy = objClsLoginInfo.UserName;
                        objPLN007.EditedOn = DateTime.Now;
                        if (objPLN007.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN007.RevNo = Convert.ToInt32(objPLN007.RevNo) + 1;
                            objPLN007.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                            objPLN007.ReturnRemark = null;
                            objPLN007.ApprovedOn = null;
                        }
                        if (!Convert.ToBoolean(strIsLatest))
                        {
                            List<PLN008> lstPLN008 = db.PLN008.Where(x => x.HeaderId == objPLN007.HeaderId).ToList();
                            if (lstPLN008 != null && lstPLN008.Count > 0)
                            {
                                db.PLN008.RemoveRange(lstPLN008);
                            }
                            string strHydroTest = clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue();
                            string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                            var latestRecord = db.PLN001
                                     .Where(x
                                                => x.Plan == strHydroTest &&
                                                   x.Status == strStatus
                                            )
                                     .OrderByDescending(x => x.HeaderId)
                                     .Select(x => new { x.HeaderId, x.RevNo })
                                     .FirstOrDefault();
                            List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == latestRecord.HeaderId).ToList();
                            if (lstPLN002 != null && lstPLN002.Count > 0)
                            {
                                List<PLN008> NewlstPLN008 = lstPLN002.Select(x => new PLN008()
                                {
                                    LineId = x.LineId,
                                    HeaderId = objPLN007.HeaderId,
                                    Project = objPLN007.Project,
                                    Document = objPLN007.Document,
                                    RevNo = objPLN007.RevNo,
                                    Plan = objPLN007.ProcessPlan,
                                    PlanRevNo = x.RevNo,
                                    CheckListId = x.LineId,
                                    //Yes_No = false,
                                    //Yes_No = clsImplementationEnum.ChecklistYesNo.No.GetStringValue(), as per observation 15003
                                    Yes_No = string.Empty,
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                }).ToList();
                                db.PLN008.AddRange(NewlstPLN008);
                            }
                        }
                        else
                        {
                            List<PLN008> lstPLN008 = db.PLN008.Where(x => x.HeaderId == objPLN007.HeaderId).ToList();
                            if (lstPLN008.Count > 0)
                            {
                                List<PLN008> lstTruePLN008 = new List<PLN008>();

                                foreach (PLN008 objPLN008 in lstPLN008)
                                {
                                    //objPLN008.Yes_No = (!string.IsNullOrWhiteSpace(strLineId) && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Length > 0 && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Contains(objPLN008.LineId) ? true : false);
                                    objPLN008.EditedBy = objClsLoginInfo.UserName;
                                    objPLN008.EditedOn = DateTime.Now;
                                }
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN007.HeaderId;
                        objResponseMsg.Status = objPLN007.Status;
                        objResponseMsg.Revision = objPLN007.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN007.HeaderId, objPLN007.Status, objPLN007.RevNo, objPLN007.Project, objPLN007.Document);


                        //if (hasAttachments)
                        //{
                        var folderPath = "PLN007/" + NewHeaderId + "/" + objPLN007.RevNo;
                        //    var existing = clsUpload.getDocs(folderPath);
                        //    var toDelete = new Dictionary<string, string>();
                        //    foreach (var item in existing)
                        //    {
                        //        if (Attach.Where(q => q.Key.Equals(item.Key)).Count() <= 0)
                        //            toDelete.Add(item.Key, item.Value);
                        //        else
                        //        {
                        //            if (Attach.Where(q => q.Key.Equals(item.Key)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                        //                toDelete.Add(item.Key, item.Value);
                        //        }
                        //    }
                        //    foreach (var item in toDelete)
                        //        clsUpload.DeleteFile(folderPath, item.Key);
                        //    var toUpload = Attach.Where(q => q.Value.Substring(0, 3) != "URL");

                        //    foreach (var attch in toUpload)
                        //    {
                        //        var base64Data = attch.Value;
                        //        var dataBytes = Helper.FromBase64(attch.Value);
                        //        //var dataBytes = Encoding.ASCII.GetBytes(stringData);
                        //        clsUpload.Upload(attch.Key, dataBytes, folderPath);
                        //    }
                        //}
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {

                    PLN007 objPLN007 = db.PLN007.Add(new PLN007
                    {
                        Project = pln007.Project,
                        Document = pln007.Document,
                        Customer = pln007.Customer,
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CDD = Helper.ToNullIfTooEarlyForDb(pln007.CDD),
                        Product = pln007.Product,
                        ReviseRemark = pln007.ReviseRemark,
                        ProcessLicensor = pln007.ProcessLicensor,
                        ProcessPlan = pln007.ProcessPlan,
                        JobNo = pln007.JobNo,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ApprovedBy = pln007.ApprovedBy
                    });
                    db.SaveChanges();
                    NewHeaderId = objPLN007.HeaderId;
                    if (!string.IsNullOrWhiteSpace(strHeaderId))
                    {
                        int planHeaderId = Convert.ToInt32(strHeaderId);
                        List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == planHeaderId).ToList();
                        List<PLN008> lstPLN008 = new List<PLN008>();
                        if (lstPLN002.Count > 0)
                        {
                            List<PLN002> lstTruePLN002 = new List<PLN002>();
                            if (!string.IsNullOrWhiteSpace(strLineId))
                            {
                                int[] arrayLineId = Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s));
                                lstTruePLN002 = lstPLN002.Where(x => arrayLineId.Contains(x.LineId)).ToList();
                            }
                            foreach (PLN002 objPLN002 in lstPLN002)
                            {
                                PLN008 objPLN008 = new PLN008();
                                objPLN008.HeaderId = objPLN007.HeaderId;
                                objPLN008.Project = objPLN007.Project;
                                objPLN008.Document = objPLN007.Document;
                                objPLN008.RevNo = objPLN007.RevNo;
                                objPLN008.Plan = objPLN007.ProcessPlan;
                                objPLN008.PlanRevNo = objPLN002.RevNo;
                                objPLN008.CheckListId = objPLN002.LineId;
                                //objPLN008.Yes_No = (lstTruePLN002.Where(x => x.LineId == objPLN002.LineId).FirstOrDefault() != null ? true : false);
                                objPLN008.CreatedBy = objClsLoginInfo.UserName;
                                objPLN008.CreatedOn = DateTime.Now;
                                lstPLN008.Add(objPLN008);
                            }
                        }
                        if (lstPLN008 != null && lstPLN008.Count > 0)
                        {
                            db.PLN008.AddRange(lstPLN008);
                            db.SaveChanges();
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.Status = objPLN007.Status;
                    objResponseMsg.Revision = objPLN007.RevNo;
                    objResponseMsg.HeaderID = NewHeaderId;

                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN007.HeaderId, objPLN007.Status, objPLN007.RevNo, objPLN007.Project, objPLN007.Document);


                    //if (hasAttachments)
                    //{
                    var folderPath = "PLN007/" + NewHeaderId + "/" + objPLN007.RevNo;
                    //    var existing = clsUpload.getDocs(folderPath);
                    //    var toDelete = new Dictionary<string, string>();
                    //    foreach (var item in existing)
                    //    {
                    //        if (Attach.Where(q => q.Key.Equals(item.Key)).Count() <= 0)
                    //            toDelete.Add(item.Key, item.Value);
                    //        else
                    //        {
                    //            if (Attach.Where(q => q.Key.Equals(item.Key)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                    //                toDelete.Add(item.Key, item.Value);
                    //        }
                    //    }
                    //    foreach (var item in toDelete)
                    //        clsUpload.DeleteFile(folderPath, item.Key);
                    //    var toUpload = Attach.Where(q => q.Value.Substring(0, 3) != "URL");

                    //    foreach (var attch in toUpload)
                    //    {
                    //        var base64Data = attch.Value;
                    //        var dataBytes = Helper.FromBase64(attch.Value);
                    //        //var dataBytes = Encoding.ASCII.GetBytes(stringData);
                    //        clsUpload.Upload(attch.Key, dataBytes, folderPath);
                    //    }
                    //}
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHeaderLine(int LineId, int HeaderId, string ActualId, string YesNo, string Remarks)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                var splitid = ActualId.Split(',');
                if (Convert.ToInt32(splitid[0]) == 0)
                {
                    PLN008 objPLN008 = db.PLN008.Where(x => x.CheckListId == LineId && x.HeaderId == HeaderId).FirstOrDefault();
                    objPLN008.Yes_No = YesNo;
                    objPLN008.Remarks = Remarks;
                    objPLN008.EditedBy = objClsLoginInfo.UserName;
                    objPLN008.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    int ActualLineId = Convert.ToInt32(splitid[0]);
                    PLN008 objPLN008 = db.PLN008.Where(x => x.LineId == ActualLineId && x.HeaderId == HeaderId).FirstOrDefault();
                    objPLN008.Yes_No = YesNo;
                    objPLN008.Remarks = Remarks;
                    objPLN008.EditedBy = objClsLoginInfo.UserName;
                    objPLN008.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrLineStatus = { clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(), clsImplementationEnum.PlanStatus.Returned.GetStringValue() };
                PLN007 objPLN007 = db.PLN007.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                if (objPLN007 != null)
                {
                    objPLN007.Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue();
                    objPLN007.SubmittedBy = objClsLoginInfo.UserName;
                    objPLN007.SubmittedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully sent for approval.";
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN007.HeaderId, objPLN007.Status, objPLN007.RevNo, objPLN007.Project, objPLN007.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                       objPLN007.Project,
                                                       "",
                                                       "",
                                                       Manager.GetPDINDocumentNotificationMsg(objPLN007.Project, clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue(), objPLN007.RevNo.Value.ToString(), objPLN007.Status),
                                                       clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                       Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue(), objPLN007.HeaderId.ToString(), true),
                                                       objPLN007.ApprovedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //added by nikita on 31/08/2017
        [HttpPost]
        public ActionResult RetractStatus(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN007 objPLN007 = db.PLN007.Where(u => u.HeaderId == headerId).SingleOrDefault();
                if (objPLN007 != null)
                {
                    objPLN007.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                    objPLN007.EditedBy = objClsLoginInfo.UserName;
                    objPLN007.EditedOn = DateTime.Now;
                    objPLN007.SubmittedOn = null;
                    objPLN007.SubmittedBy = null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                    Manager.UpdatePDN002(objPLN007.HeaderId, objPLN007.Status, objPLN007.RevNo, objPLN007.Project, objPLN007.Document);
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string LineOperation(bool flag, int LineId, int HeaderId)
        {
            if (flag)
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" checked=\"checked\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
            else
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
        }

        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            string strCdd = Manager.GetContractWiseCdd(projectCode);
            if (!string.IsNullOrWhiteSpace(strCdd))
            {
                DateTime dtCdd = Convert.ToDateTime(strCdd);
                objApproverModel.Cdd = dtCdd.Date.ToShortDateString();
            }
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHistoryDetails(string strRole, string HeaderId)
        {
            ViewBag.Role = strRole;
            ViewBag.HeaderId = HeaderId;
            return PartialView("_GetHistoryDetailsPartial");
        }

        [HttpPost]
        public JsonResult LoadHydroTestHistory(JQueryDataTableParamModel param, string HeaderId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                //if (param.CTQCompileStatus.ToUpper() == "INITIATOR")
                //{
                //    strWhere += "1=1 and pln007.CreatedBy = '" + objClsLoginInfo.UserName + "'";

                //}
                //else
                //{
                //    strWhere += "1=1 and pln007.ApprovedBy = '" + objClsLoginInfo.UserName + "'";
                //}
                if (!string.IsNullOrWhiteSpace(HeaderId))
                {
                    strWhere += " pln007.HeaderId = '" + HeaderId + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln007.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln007.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_FETCH_HYDROTEST_HEADERS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                               Convert.ToString(uc.SubmittedBy),
                               uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                               Convert.ToString(uc.ApprovedBy),
                               uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),     "<a title='View' class='iconspace' href='"+WebsiteURL+"/PLN/MaintainLockingCleat/AddHeader?HeaderID="+uc.HeaderId+"'><i class='fa fa-eye'></i></a>" +
                               Convert.ToString(uc.ReturnRemark),
                               (param.CTQCompileStatus.ToUpper() == "INITIATOR"?"<center><a title='View' href='"+WebsiteURL+"/PLN/MaintainHydroTest/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainHydroTest/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>":"<center><a title='View' href='"+WebsiteURL+"/PLN/ApproveHydroTest/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainHydroTest/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN007_Log objPLN007_Log = db.PLN007_Log.Where(x => x.Id == Id).FirstOrDefault();
                //if (objPLN007_Log == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadHydroTestHistoryFormPartial(int Id = 0)
        {
            PLN007_Log objPLN007_Log = db.PLN007_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN007_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN007_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN007_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN007_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN007_Log.ApprovedBy = objPLN007_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN007_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN007_Log.Customer = objPLN007_Log.Customer + " - " + customerName;
                string strHydroTest = clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strHydroTest &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    //if (latestRecord.HeaderId == objPLN003.HeaderId)
                    //{

                    //}
                }
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN007/" + objPLN007_Log.HeaderId + "/" + objPLN007_Log.RevNo));
            }
            else
            {
                objPLN007_Log = new PLN007_Log();
                objPLN007_Log.ProcessPlan = clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue();
                objPLN007_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN007_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            //var urlPrefix = "http://" + Request.Url.Authority + "/PLN/MaintainHydroTest/ViewHistory?Id=";// AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            var urlPrefix = WebsiteURL + "/PLN/MaintainHydroTest/ViewHistory?Id=";
            if (!objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.SHOP.GetStringValue()))
            {
                ViewBag.RevPrev = (db.PLN007_Log.Any(q => (q.HeaderId == objPLN007_Log.HeaderId && q.RevNo == (objPLN007_Log.RevNo - 1))) ? urlPrefix + db.PLN007_Log.Where(q => (q.HeaderId == objPLN007_Log.HeaderId && q.RevNo == (objPLN007_Log.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.PLN007_Log.Any(q => (q.HeaderId == objPLN007_Log.HeaderId && q.RevNo == (objPLN007_Log.RevNo + 1))) ? urlPrefix + db.PLN007_Log.Where(q => (q.HeaderId == objPLN007_Log.HeaderId && q.RevNo == (objPLN007_Log.RevNo + 1))).FirstOrDefault().Id : null);
                ViewBag.Isshopuser = false;
            }
            else
            {
                ViewBag.Isshopuser = true;
            }           
            return PartialView("_LoadHydroTestHistoryFormPartial", objPLN007_Log);
        }

        [HttpPost]
        public JsonResult LoadCheckListHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int Id = Convert.ToInt32(param.CTQHeaderId);
                PLN007_Log objPLN007_Log = db.PLN007_Log.Where(x => x.Id == Id).FirstOrDefault();

                strWhere += "1=1 and pln008.RefId = " + objPLN007_Log.Id + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_HYDROTEST_HEADER_LINE_HISTORY
                                (
                                     objPLN007_Log.HeaderId,
                                     StartIndex,
                                     EndIndex,
                                     "",
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,false,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,false),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                PLN007 objPLN007 = db.PLN007.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objPLN007 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        [HttpPost]
        public JsonResult LoadCheckListDataNew(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN007 objPLN007 = db.PLN007.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string HeaderStatus = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                bool IsDisable = false;
                if (objPLN007 != null)
                {
                    HeaderStatus = objPLN007.Status;
                }
                if (HeaderId == 0 || (HeaderId > 0 && (HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower() || HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.Approved.GetStringValue().ToLower())))
                {
                    IsDisable = true;
                }
                string strHydroTest = clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strHydroTest &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln008.HeaderId = " + HeaderId + " and ";
                    PLN008 objPLN008 = db.PLN008.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN008 != null)
                    {
                        PlanRev = objPLN008.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_HYDROTEST_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                (new PLNCommonFunction()).CheckListLineOperationYesNoNew(uc.Yes_No,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Yes_No","UpdateData(this)"),
                                (new PLNCommonFunction()).CheckListLineOperationRemarksNew(uc.Remarks,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Remarks","UpdateData(this)"),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                //getChecklistDetails(uc.CheckListId.ToString())[0],
                                //getChecklistDetails(uc.CheckListId.ToString())[1],
                                //getChecklistDetails(uc.CheckListId.ToString())[2],
                                //getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateData(string id, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var splitid = id.Split(',');
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("pln008", "LineId", splitid[0], columnName, columnValue);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN007 objPLN007 = db.PLN007.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objPLN007 != null)
                {
                    objPLN007.RevNo = Convert.ToInt32(objPLN007.RevNo) + 1;
                    objPLN007.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objPLN007.ReviseRemark = strRemarks;
                    objPLN007.EditedBy = objClsLoginInfo.UserName;
                    objPLN007.EditedOn = DateTime.Now;
                    objPLN007.ReturnRemark = null;
                    objPLN007.ApprovedOn = null;
                    objPLN007.SubmittedBy = null;
                    objPLN007.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPLN007.HeaderId, objPLN007.Status, objPLN007.RevNo, objPLN007.Project, objPLN007.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objPLN007.HeaderId;
                    objResponseMsg.Status = objPLN007.Status;
                    objResponseMsg.rev = objPLN007.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excel
        //Excel funtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (HeaderId > 0)
                {
                    
                    PLN008 objPLN008 = db.PLN008.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN008 != null)
                    {
                        latestRv = objPLN008.PlanRevNo;
                    }
                    else
                    {
                        latestRv = 0;
                    }
                }
                else
                {
                    string strhydrotest = clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue();
                    string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                    var latestRecord = db.PLN001
                             .Where(x
                                        => x.Plan == strhydrotest &&
                                           x.Status == strStatus
                                    )
                             .OrderByDescending(x => x.HeaderId)
                             .Select(x => new { x.HeaderId, x.RevNo })
                             .FirstOrDefault();
                    latestRv = latestRecord.RevNo;
                }

                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                { //header grid data
                    var lst = db.SP_FETCH_HYDROTEST_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      Document = uc.Document,
                                      Customer = uc.Customer,
                                      Product = uc.Product,
                                      ProcessLicensor = uc.ProcessLicensor,
                                      RevNo = "R" + uc.RevNo,
                                      Status = uc.Status,
                                      CDD = uc.CDD,
                                      ProcessPlan = uc.ProcessPlan,
                                      CreatedBy = uc.CreatedBy,
                                      CreatedOn = uc.CreatedOn,
                                      EditedBy = uc.EditedBy,
                                      EditedOn = uc.EditedOn,
                                      SubmittedBy = uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ReturnRemark = uc.ReturnRemark
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                { //history header
                    var lst = db.SP_FETCH_HYDROTEST_HEADERS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      Document = uc.Document,
                                      Customer = uc.Customer,
                                      Product = uc.Product,
                                      ProcessLicensor = uc.ProcessLicensor,
                                      RevNo = "R" + uc.RevNo,
                                      Status = uc.Status,
                                      CDD = uc.CDD,
                                      ProcessPlan = uc.ProcessPlan,
                                      CreatedBy = uc.CreatedBy,
                                      CreatedOn = uc.CreatedOn,
                                      EditedBy = uc.EditedBy,
                                      EditedOn = uc.EditedOn,
                                      SubmittedBy = uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ReturnRemark = uc.ReturnRemark
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {  //line grid data
                    var lst = db.SP_FETCH_HYDROTEST_HEADER_LINE(HeaderId, latestRv, 1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      ROW_NO = uc.ROW_NO,
                                      RelatedTo = Convert.ToString(uc.RelatedTo),
                                      CheckPointDesc = Convert.ToString(uc.CheckPointDesc),
                                      Yes_No = uc.Yes_No, //(new PLNCommonFunction()).CheckListLineOperationYesNoNew(uc.Yes_No, uc.LineId, uc.HeaderId, IsDisable, uc.ActualId, "Yes_No", "UpdateData(this)"),
                                      Remarks = uc.Remarks,
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = Convert.ToString(uc.CreatedOn),
                                      EditedBy = Convert.ToString(uc.EditedBy),
                                      EditedOn = Convert.ToString(uc.EditedOn),
                                      //PreparedBy = getChecklistDetails(uc.CheckListId.ToString())[0],
                                      //CheckedBy = getChecklistDetails(uc.CheckListId.ToString())[1],
                                      //CheckedOn = getChecklistDetails(uc.CheckListId.ToString())[2],
                                      //Document = getChecklistDetails(uc.CheckListId.ToString())[3],
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {  //history line grid data


                    var lst = db.SP_FETCH_HYDROTEST_HEADER_LINE_HISTORY(HeaderId, 1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      ROW_NO = uc.ROW_NO,
                                      RelatedTo = Convert.ToString(uc.RelatedTo),
                                      CheckPointDesc = Convert.ToString(uc.CheckPointDesc),
                                      Yes_No = uc.Yes_No, //(new PLNCommonFunction()).CheckListLineOperationYesNoNew(uc.Yes_No, uc.LineId, uc.HeaderId, IsDisable, uc.ActualId, "Yes_No", "UpdateData(this)"),
                                      Remarks = uc.Remarks,
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = Convert.ToString(uc.CreatedOn),
                                      EditedBy = Convert.ToString(uc.EditedBy),
                                      EditedOn = Convert.ToString(uc.EditedOn),
                                      PreparedBy = getChecklistDetails(uc.CheckListId.ToString())[0],
                                      CheckedBy = getChecklistDetails(uc.CheckListId.ToString())[1],
                                      CheckedOn = getChecklistDetails(uc.CheckListId.ToString())[2],
                                      Document = getChecklistDetails(uc.CheckListId.ToString())[3],
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}