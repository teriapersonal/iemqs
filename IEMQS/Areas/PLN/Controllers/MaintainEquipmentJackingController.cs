﻿using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.Utility.Models;
using static IEMQS.Areas.HTC.Controllers.MaintainHTCController;

namespace IEMQS.Areas.PLN.Controllers
{
    public class MaintainEquipmentJackingController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadEquipmentJackingListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_EquipmentJackingListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadEquipmentJackingHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.PlanStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                //strWhere += " and pln016.CreatedBy in('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln016.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln016.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_FETCH_EQUIPMENTJACKINGPLAN_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                 Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                                "<nobr><center>"+"<a class=\"iconspace\" href=\""+WebsiteURL+"/PLN/MaintainEquipmentJacking/CreateEquipmentJacking?HeaderID=" + uc.HeaderId + "\"><i class=\"fa fa-eye\"></i></a>"+
                                Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/PLN/MaintainEquipmentJacking/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblEquipmentJackingHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                                "<i class='iconspace fa fa-clock-o' title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainEquipmentJacking/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>" +
                                (uc.RevNo > 0 ? "<i  title=\"History\" class=\"iconspace fa fa-history\" onClick=\"ViewHistoryForProject(" + uc.HeaderId + ")\"></i>" : "<i  title=\"History\" class=\"disabledicon fa fa-history\" \"></i>") +
                                //"<i title=\"Print Report\" class=\"iconspace fa fa-print\" onClick=\"PrintReport(" + uc.HeaderId + ")\"></i> " +
                                "</center></nobr>"
                                //"<a title='View' href='/PLN/MaintainEquipmentJacking/CreateEquipmentJacking?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+(uc.RevNo>0 ?"<a title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Initiator','/PLN/MaintainEquipmentJacking/GetHistoryDetails','Equipment Jacking')\"><i style='margin-left:5px;' class='fa fa-history'></i></a>":"")+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainEquipmentJacking/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='margin-left:5px;cursor:pointer;' class='fa fa-clock-o'></i></a>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Equipment_Jacking_Plan);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Equipment Jacking TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                PLN016_Log objPLN016_Log = db.PLN016_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN016_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN016_Log.CreatedBy) : null;
                model.CreatedOn = objPLN016_Log.CreatedOn;
                model.EditedBy = objPLN016_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN016_Log.EditedBy) : null;
                model.EditedOn = objPLN016_Log.EditedOn;
                model.SubmittedBy = objPLN016_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN016_Log.SubmittedBy) : null;
                model.SubmittedOn = objPLN016_Log.SubmittedOn;

                model.ApprovedBy = objPLN016_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN016_Log.ApprovedBy) : null;
                model.ApprovedOn = objPLN016_Log.ApprovedOn;

            }
            else
            {

                PLN016_Log objPLN016_Log = db.PLN016_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN016_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN016_Log.CreatedBy) : null;
                model.CreatedOn = objPLN016_Log.CreatedOn;
                model.EditedBy = objPLN016_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN016_Log.EditedBy) : null;
                model.EditedOn = objPLN016_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "Equipment Timeline";

            if (HeaderId > 0)
            {
                PLN016 objPLN016 = db.PLN016.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN016.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN016.CreatedBy) : null;
                model.CreatedOn = objPLN016.CreatedOn;
                model.EditedBy = objPLN016.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN016.EditedBy) : null;
                model.EditedOn = objPLN016.EditedOn;
                model.SubmittedBy = objPLN016.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN016.SubmittedBy) : null;
                model.SubmittedOn = objPLN016.SubmittedOn;
                model.ApprovedBy = objPLN016.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN016.ApprovedBy) : null;
                model.ApprovedOn = objPLN016.ApprovedOn;

            }
            else
            {
                PLN016 objPLN016 = db.PLN016.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN016.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN016.CreatedBy) : null;
                model.CreatedOn = objPLN016.CreatedOn;
                model.EditedBy = objPLN016.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN016.EditedBy) : null;
                model.EditedOn = objPLN016.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [HttpPost]
        public JsonResult LoadCheckListDataNew(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN016 objPLN016 = db.PLN016.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string HeaderStatus = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                bool IsDisable = false;
                if (objPLN016 != null)
                {
                    HeaderStatus = objPLN016.Status;
                }
                if (HeaderId == 0 || (HeaderId > 0 && (HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower() || HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.Approved.GetStringValue().ToLower())))
                {
                    IsDisable = true;
                }
                string strEquipment = clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x => x.Plan == strEquipment &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln022.HeaderId = " + HeaderId + " and ";
                    PLN022 objPLN022 = db.PLN022.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN022 != null)
                    {
                        PlanRev = objPLN022.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                #region Sorting
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                string strWhere1 = (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null);
                var lstResult = db.SP_FETCH_EQUIPMENTJACKINGPLAN_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    strWhere1
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                               (new PLNCommonFunction()).CheckListLineOperationYesNoNew(uc.Yes_No,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Yes_No","UpdateData(this)"),
                                (new PLNCommonFunction()).CheckListLineOperationRemarksNew(uc.Remarks,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Remarks","UpdateData(this)"),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                //getChecklistDetails(uc.CheckListId.ToString())[0],
                                //getChecklistDetails(uc.CheckListId.ToString())[1],
                                //getChecklistDetails(uc.CheckListId.ToString())[2],
                                //getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere1
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateData(string id, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var splitid = id.Split(',');
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("pln022", "LineId", splitid[0], columnName, columnValue);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadCheckListHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int Id = Convert.ToInt32(param.CTQHeaderId);
                PLN016_Log objPLN016_Log = db.PLN016_Log.Where(x => x.Id == Id).FirstOrDefault();

                strWhere += "1=1 and pln022.RefId = " + objPLN016_Log.Id + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                #region Sorting
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_FETCH_EQUIPMENTHANDLING_HEADER_LINE_HISTORY
                                (
                                     objPLN016_Log.HeaderId,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,true,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,true),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [NonAction]
        public string[] getChecklistDetails(string lineId)
        {
            string[] checklist = new string[4];
            int LineID = Int32.Parse(lineId);
            var data = (from p2 in db.PLN002
                        join
                            p1 in db.PLN001 on p2.HeaderId equals p1.HeaderId
                        where p2.LineId == LineID
                        select p1).FirstOrDefault();
            if (data != null)
            {
                checklist[0] = data.CreatedBy + "-" + Manager.GetUserNameFromPsNo(data.CreatedBy); //prepared by
                checklist[1] = data.ApprovedBy + "-" + Manager.GetUserNameFromPsNo(data.ApprovedBy); // approved by
                checklist[2] = data.ApprovedOn.Value != null ? data.ApprovedOn.Value.ToString("dd/MM/yyyy") : string.Empty; // approved on
                checklist[3] = data.Document; // document no
            }
            return checklist;
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult CreateEquipmentJacking(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN016");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN016 objPLN016 = db.PLN016.Where(x => x.HeaderId == HeaderID && x.CreatedBy == currentUser).FirstOrDefault();
                //if (objPLN016 == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadEquipmentJackingCreateFormPartial(int HeaderID = 0)
        {
            PLN016 objPLN016 = db.PLN016.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN016 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN016.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN016.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ApproverName))
                {
                    ViewBag.ApproverName = objPLN016.ApprovedBy + " - " + ApproverName;
                }
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN016.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objPLN016.Customer + " - " + customerName;
                string strEquipmentJacking = clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN016/" + objPLN016.HeaderId + "/" + objPLN016.RevNo));

                var latestRecord = db.PLN001
                        .Where(x
                                   => x.Plan == strEquipmentJacking &&
                                      x.Status == strStatus
                               )
                        .OrderByDescending(x => x.HeaderId)
                        .Select(x => new { x.HeaderId, x.RevNo })
                        .FirstOrDefault();
                if (latestRecord != null)
                {
                    PLN022 objPLN022 = db.PLN022.Where(x => x.HeaderId == objPLN016.HeaderId).FirstOrDefault();
                    if (objPLN022 != null)
                    {
                        if (latestRecord.RevNo == objPLN022.PlanRevNo)
                        {
                            ViewBag.isLatest = "true";
                        }
                        else
                        {
                            ViewBag.isLatest = "false";
                        }
                    }
                    else
                    {
                        ViewBag.isLatest = "false";
                    }
                }
                else
                {
                    ViewBag.isLatest = "true";
                }

                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN016.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objPLN016.HeaderId, objPLN016.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            else
            {
                objPLN016 = new PLN016();
                objPLN016.ProcessPlan = clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue();
                objPLN016.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN016.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadEquipmentJackingCreateFormPartial", objPLN016);
        }

        [HttpPost]
        public ActionResult SaveHeader(PLN016 pln016, string strHeaderId, string strLineId, string strIsLatest)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln016.HeaderId > 0)
                {
                    PLN016 objPLN016 = db.PLN016.Where(x => x.HeaderId == pln016.HeaderId).FirstOrDefault();
                    if (objPLN016 != null)
                    {
                        objPLN016.Product = pln016.Product;
                        objPLN016.ProcessLicensor = pln016.ProcessLicensor;
                        objPLN016.JobNo = pln016.JobNo;
                        objPLN016.ReviseRemark = pln016.ReviseRemark;
                        objPLN016.ApprovedBy = pln016.ApprovedBy;
                        objPLN016.EditedBy = objClsLoginInfo.UserName;
                        objPLN016.EditedOn = DateTime.Now;
                        if (objPLN016.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN016.RevNo = Convert.ToInt32(objPLN016.RevNo) + 1;
                            objPLN016.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                            objPLN016.ReturnRemark = null;
                            objPLN016.ApprovedOn = null;
                        }
                        if (!Convert.ToBoolean(strIsLatest))
                        {
                            List<PLN022> lstPLN022 = db.PLN022.Where(x => x.HeaderId == objPLN016.HeaderId).ToList();
                            if (lstPLN022 != null && lstPLN022.Count > 0)
                            {
                                db.PLN022.RemoveRange(lstPLN022);
                            }
                            string strEquipment = clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue();
                            string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                            var latestRecord = db.PLN001
                                     .Where(x
                                                => x.Plan == strEquipment &&
                                                   x.Status == strStatus
                                            )
                                     .OrderByDescending(x => x.HeaderId)
                                     .Select(x => new { x.HeaderId, x.RevNo })
                                     .FirstOrDefault();
                            List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == latestRecord.HeaderId).ToList();
                            if (lstPLN002 != null && lstPLN002.Count > 0)
                            {
                                List<PLN022> NewlstPLN022 = lstPLN002.Select(x => new PLN022()
                                {
                                    LineId = x.LineId,
                                    HeaderId = objPLN016.HeaderId,
                                    Project = objPLN016.Project,
                                    Document = objPLN016.Document,
                                    RevNo = objPLN016.RevNo,
                                    Plan = objPLN016.ProcessPlan,
                                    PlanRevNo = x.RevNo,
                                    CheckListId = x.LineId,
                                    //Yes_No = false,
                                    //Yes_No = clsImplementationEnum.ChecklistYesNo.No.GetStringValue(), as per observation 15003
                                    Yes_No = string.Empty,
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                }).ToList();
                                db.PLN022.AddRange(NewlstPLN022);
                            }
                        }
                        else
                        {
                            List<PLN022> lstPLN022 = db.PLN022.Where(x => x.HeaderId == objPLN016.HeaderId).ToList();
                            if (lstPLN022.Count > 0)
                            {
                                List<PLN022> lstTruePLN022 = new List<PLN022>();

                                foreach (PLN022 objPLN022 in lstPLN022)
                                {
                                    //objPLN022.Yes_No = (!string.IsNullOrWhiteSpace(strLineId) && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Length > 0 && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Contains(objPLN022.LineId) ? true : false);
                                    objPLN022.EditedBy = objClsLoginInfo.UserName;
                                    objPLN022.EditedOn = DateTime.Now;
                                }
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN016.HeaderId;
                        objResponseMsg.Status = objPLN016.Status;
                        objResponseMsg.Revision = objPLN016.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN016.HeaderId, objPLN016.Status, objPLN016.RevNo, objPLN016.Project, objPLN016.Document);

                        //var folderPath = "PLN016/" + NewHeaderId + "/" + objPLN016.RevNo;
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                    PLN016 objPLN016 = db.PLN016.Add(new PLN016
                    {
                        Project = pln016.Project,
                        Document = pln016.Document,
                        Customer = pln016.Customer,
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CDD = Helper.ToNullIfTooEarlyForDb(pln016.CDD),
                        Product = pln016.Product,
                        ProcessLicensor = pln016.ProcessLicensor,
                        ProcessPlan = pln016.ProcessPlan,
                        JobNo = pln016.JobNo,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ApprovedBy = pln016.ApprovedBy
                    });
                    db.SaveChanges();
                    NewHeaderId = objPLN016.HeaderId;
                    if (!string.IsNullOrWhiteSpace(strHeaderId))
                    {
                        int planHeaderId = Convert.ToInt32(strHeaderId);
                        List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == planHeaderId).ToList();
                        List<PLN022> lstPLN022 = new List<PLN022>();
                        if (lstPLN002.Count > 0)
                        {
                            List<PLN002> lstTruePLN002 = new List<PLN002>();
                            if (!string.IsNullOrWhiteSpace(strLineId))
                            {
                                int[] arrayLineId = Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s));
                                lstTruePLN002 = lstPLN002.Where(x => arrayLineId.Contains(x.LineId)).ToList();
                            }
                            foreach (PLN002 objPLN002 in lstPLN002)
                            {
                                PLN022 objPLN022 = new PLN022();
                                objPLN022.HeaderId = objPLN016.HeaderId;
                                objPLN022.Project = objPLN016.Project;
                                objPLN022.Document = objPLN016.Document;
                                objPLN022.RevNo = objPLN016.RevNo;
                                objPLN022.Plan = objPLN016.ProcessPlan;
                                objPLN022.PlanRevNo = objPLN002.RevNo;
                                objPLN022.CheckListId = objPLN002.LineId;
                                //objPLN022.Yes_No = (lstTruePLN002.Where(x => x.LineId == objPLN002.LineId).FirstOrDefault() != null ? true : false);
                                objPLN022.CreatedBy = objClsLoginInfo.UserName;
                                objPLN022.CreatedOn = DateTime.Now;
                                lstPLN022.Add(objPLN022);
                            }
                        }
                        if (lstPLN022 != null && lstPLN022.Count > 0)
                        {
                            db.PLN022.AddRange(lstPLN022);
                            db.SaveChanges();
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.Status = objPLN016.Status;
                    objResponseMsg.Revision = objPLN016.RevNo;
                    objResponseMsg.HeaderID = NewHeaderId;

                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN016.HeaderId, objPLN016.Status, objPLN016.RevNo, objPLN016.Project, objPLN016.Document);

                    //var folderPath = "PLN016/" + NewHeaderId + "/" + objPLN016.RevNo;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHeaderLine(int LineId, int HeaderId, string ActualId, string YesNo, string Remarks)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                var splitid = ActualId.Split(',');
                if (Convert.ToInt32(splitid[0]) == 0)
                {
                    PLN022 objPLN022 = db.PLN022.Where(x => x.CheckListId == LineId && x.HeaderId == HeaderId).FirstOrDefault();
                    objPLN022.Yes_No = YesNo;
                    objPLN022.Remarks = Remarks;
                    objPLN022.EditedBy = objClsLoginInfo.UserName;
                    objPLN022.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                else
                {
                    int ActualLineId = Convert.ToInt32(splitid[0]);
                    PLN022 objPLN022 = db.PLN022.Where(x => x.LineId == ActualLineId && x.HeaderId == HeaderId).FirstOrDefault();
                    objPLN022.Yes_No = YesNo;
                    objPLN022.Remarks = Remarks;
                    objPLN022.EditedBy = objClsLoginInfo.UserName;
                    objPLN022.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrLineStatus = { clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(), clsImplementationEnum.PlanStatus.Returned.GetStringValue() };
                PLN016 objPLN016 = db.PLN016.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                if (objPLN016 != null)
                {
                    objPLN016.Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue();
                    objPLN016.SubmittedBy = objClsLoginInfo.UserName;
                    objPLN016.SubmittedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Plan successfully sent for approval.";
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN016.HeaderId, objPLN016.Status, objPLN016.RevNo, objPLN016.Project, objPLN016.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                        objPLN016.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN016.Project, clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue(), objPLN016.RevNo.Value.ToString(), objPLN016.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue(), objPLN016.HeaderId.ToString(), true),
                                                        objPLN016.ApprovedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Plan not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RetractStatus(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN016 objPLN016 = db.PLN016.Where(u => u.HeaderId == headerId).SingleOrDefault();
                if (objPLN016 != null)
                {
                    objPLN016.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                    objPLN016.EditedBy = objClsLoginInfo.UserName;
                    objPLN016.EditedOn = DateTime.Now;
                    objPLN016.SubmittedOn = null;
                    objPLN016.SubmittedBy = null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                    Manager.UpdatePDN002(objPLN016.HeaderId, objPLN016.Status, objPLN016.RevNo, objPLN016.Project, objPLN016.Document);
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            string strCdd = Manager.GetContractWiseCdd(projectCode);
            if (!string.IsNullOrWhiteSpace(strCdd))
            {
                DateTime dtCdd = Convert.ToDateTime(strCdd);
                objApproverModel.Cdd = dtCdd.Date.ToShortDateString();
            }
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHistoryDetails(string strRole, string HeaderId)
        {
            ViewBag.Role = strRole;
            ViewBag.HeaderId = HeaderId;
            return PartialView("_GetHistoryDetailsPartial");
        }

        [HttpPost]
        public JsonResult LoadEquipmentJackingHistory(JQueryDataTableParamModel param, string HeaderId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                //if (param.CTQCompileStatus.ToUpper() == "INITIATOR")
                //{
                //    strWhere += "1=1 and pln016.CreatedBy = '" + objClsLoginInfo.UserName + "'";

                //}
                //else
                //{
                //    strWhere += "1=1 and pln016.ApprovedBy = '" + objClsLoginInfo.UserName + "'";
                //}
                if (!string.IsNullOrWhiteSpace(HeaderId))
                {
                    strWhere += " pln016.HeaderId = '" + HeaderId + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln016.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln016.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                #region Sorting
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstResult = db.SP_FETCH_EQUIPMENTJACKINGPLAN_HEADERS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                    Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),

                                Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                               (param.CTQCompileStatus.ToUpper() == "INITIATOR"?"<center><a title='View' href='"+WebsiteURL+"/PLN/MaintainEquipmentJacking/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainEquipmentJacking/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>":"<center><a title='View' href='"+WebsiteURL+"/PLN/ApproveEquipmentJacking/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainEquipmentJacking/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN016_Log objPLN016_Log = db.PLN016_Log.Where(x => x.Id == Id && x.CreatedBy == currentUser).FirstOrDefault();
                //if (objPLN016_Log == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadEquipmentJackingHistoryFormPartial(int Id = 0)
        {
            PLN016_Log objPLN016_Log = db.PLN016_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN016_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN016_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN016_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN016_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN016_Log.ApprovedBy = objPLN016_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN016_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN016_Log.Customer = objPLN016_Log.Customer + " - " + customerName;
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN016/" + objPLN016_Log.HeaderId + "/" + objPLN016_Log.RevNo));
            }
            else
            {
                objPLN016_Log = new PLN016_Log();
                objPLN016_Log.ProcessPlan = clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue();
                objPLN016_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN016_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            //var urlPrefix = "http://" + Request.Url.Authority + "/PLN/MaintainEquipmentJacking/ViewHistory?Id=";// AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            var urlPrefix = WebsiteURL + "/PLN/MaintainEquipmentJacking/ViewHistory?Id=";
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.PLN016_Log.Any(q => (q.HeaderId == objPLN016_Log.HeaderId && q.RevNo == (objPLN016_Log.RevNo - 1))) ? urlPrefix + db.PLN016_Log.Where(q => (q.HeaderId == objPLN016_Log.HeaderId && q.RevNo == (objPLN016_Log.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.PLN016_Log.Any(q => (q.HeaderId == objPLN016_Log.HeaderId && q.RevNo == (objPLN016_Log.RevNo + 1))) ? urlPrefix + db.PLN016_Log.Where(q => (q.HeaderId == objPLN016_Log.HeaderId && q.RevNo == (objPLN016_Log.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return PartialView("_LoadEquipmentJackingHistoryFormPartial", objPLN016_Log);
        }


        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN016 objPLN016 = db.PLN016.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objPLN016 != null)
                {
                    objPLN016.RevNo = Convert.ToInt32(objPLN016.RevNo) + 1;
                    objPLN016.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objPLN016.ReviseRemark = strRemarks;
                    objPLN016.EditedBy = objClsLoginInfo.UserName;
                    objPLN016.EditedOn = DateTime.Now;
                    objPLN016.ReturnRemark = null;
                    objPLN016.ApprovedOn = null;
                    objPLN016.SubmittedBy = null;
                    objPLN016.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPLN016.HeaderId, objPLN016.Status, objPLN016.RevNo, objPLN016.Project, objPLN016.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objPLN016.HeaderId;
                    objResponseMsg.Status = objPLN016.Status;
                    objResponseMsg.rev = objPLN016.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_EQUIPMENTJACKINGPLAN_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      RevNo = "R" + li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      ProcessPlan = li.ProcessPlan,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn.HasValue ? Convert.ToString(li.CreatedOn) : "",
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn.HasValue ? Convert.ToString(li.EditedOn) : "",
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn.HasValue ? Convert.ToString(li.SubmittedOn) : "",
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn.HasValue ? Convert.ToString(li.ApprovedOn) : "",
                                      ReturnRemark = li.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    int? PlanRev = 0;
                    if (HeaderId > 0)
                    {
                        PLN022 objPLN022 = db.PLN022.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                        if (objPLN022 != null)
                        {
                            PlanRev = objPLN022.PlanRevNo;
                        }
                        else
                        {
                            PlanRev = 0;
                        }
                    }
                    else
                    {

                        string strEquipment = clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue();
                        string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                        var latestRecord = db.PLN001
                                 .Where(x
                                            => x.Plan == strEquipment &&
                                               x.Status == strStatus
                                        )
                                 .OrderByDescending(x => x.HeaderId)
                                 .Select(x => new { x.HeaderId, x.RevNo })
                                 .FirstOrDefault();
                        PlanRev = latestRecord.RevNo;
                    }
                    var lst = db.SP_FETCH_EQUIPMENTJACKINGPLAN_HEADER_LINE(HeaderId, PlanRev, 1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      RelatedTo = li.RelatedTo,
                                      CheckPointDesc = li.CheckPointDesc,
                                      Yes_No = li.Yes_No,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      PreparedBy = getChecklistDetails(li.CheckListId.ToString())[0],
                                      CheckedBy = getChecklistDetails(li.CheckListId.ToString())[1],
                                      CheckedOn = getChecklistDetails(li.CheckListId.ToString())[2],
                                      DocumentNo = getChecklistDetails(li.CheckListId.ToString())[3],
                                      IsLatest = li.IsLatest
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_EQUIPMENTJACKINGPLAN_HEADERS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      RevNo = "R" + li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      ProcessPlan = li.ProcessPlan,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn.HasValue ? Convert.ToString(li.CreatedOn) : "",
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn.HasValue ? Convert.ToString(li.EditedOn) : "",
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn.HasValue ? Convert.ToString(li.SubmittedOn) : "",
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn.HasValue ? Convert.ToString(li.ApprovedOn) : "",
                                      ReturnRemark = li.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}