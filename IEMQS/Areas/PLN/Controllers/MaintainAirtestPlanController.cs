﻿using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.Utility.Models;
using System.Globalization;

namespace IEMQS.Areas.PLN.Controllers
{
    public class MaintainAirtestPlanController : clsBase
    {
        //modified by nikita on 29/08/2017
        //added method updatepdn002 and retractstatus
        //task assigned by satish pawar
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadAirtestPlanListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_AirtestPlanListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadAirtestPlanHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.PlanStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                //strWhere += " and pln015.CreatedBy in('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln015.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln015.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_FETCH_AIRTESTPLAN_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                Convert.ToString(uc.SubmittedBy),
                                uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                Convert.ToString(uc.ApprovedBy),
                                uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),     "<a title='View' class='iconspace' href='"+WebsiteURL+"/PLN/MaintainLockingCleat/AddHeader?HeaderID="+uc.HeaderId+"'><i class='fa fa-eye'></i></a>" +

                               Convert.ToString(uc.ReturnRemark),
                                //"<center><a title='View' href='/PLN/MaintainAirtestPlan/CreateAirtestPlan?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                                 "<a title='View' href='"+WebsiteURL+"/PLN/MaintainAirtestPlan/CreateAirtestPlan?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+" "+
                                      Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/PLN/MaintainAirtestPlan/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblAirtestPlanHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                                 "<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainAirtestPlan/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='margin-left:5px;cursor:pointer;' class='fa fa-clock-o'></i></a>"
                                 +(uc.RevNo>0 ?"<a title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Initiator','/PLN/MaintainAirtestPlan/GetHistoryDetails','Airtest Plan')\"><i style='margin-left:5px;' class='fa fa-history'></i></a>":"<i title='History' style = 'margin-left:5px;opacity:0.5' class='fa fa-history'></i>")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = "",

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Air_Test_Plan);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Air Test Plan TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                PLN015_Log objPLN015_Log = db.PLN015_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN015_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN015_Log.CreatedBy) : null;
                model.CreatedOn = objPLN015_Log.CreatedOn;
                model.EditedBy = objPLN015_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN015_Log.EditedBy) : null;
                model.EditedOn = objPLN015_Log.EditedOn;
                model.SubmittedBy = objPLN015_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN015_Log.SubmittedBy) : null;
                model.SubmittedOn = objPLN015_Log.SubmittedOn;

                model.ApprovedBy = objPLN015_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN015_Log.ApprovedBy) : null;
                model.ApprovedOn = objPLN015_Log.ApprovedOn;

            }
            else
            {

                PLN015_Log objPLN015_Log = db.PLN015_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN015_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN015_Log.CreatedBy) : null;
                model.CreatedOn = objPLN015_Log.CreatedOn;
                model.EditedBy = objPLN015_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN015_Log.EditedBy) : null;
                model.EditedOn = objPLN015_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "Air Test Plan Timeline";

            if (HeaderId > 0)
            {
                PLN015 objPLN015 = db.PLN015.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN015.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN015.CreatedBy) : null;
                model.CreatedOn = objPLN015.CreatedOn;
                model.EditedBy = objPLN015.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN015.EditedBy) : null;
                model.EditedOn = objPLN015.EditedOn;
                model.SubmittedBy = objPLN015.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN015.SubmittedBy) : null;
                model.SubmittedOn = objPLN015.SubmittedOn;
                model.ApprovedBy = objPLN015.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN015.ApprovedBy) : null;
                model.ApprovedOn = objPLN015.ApprovedOn;

            }
            else
            {
                PLN015 objPLN015 = db.PLN015.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN015.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN015.CreatedBy) : null;
                model.CreatedOn = objPLN015.CreatedOn;
                model.EditedBy = objPLN015.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN015.EditedBy) : null;
                model.EditedOn = objPLN015.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult CreateAirtestPlan(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN015");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN015 objPLN015 = db.PLN015.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                //if (objPLN015 == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN015.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                objPLN015 = db.PLN015.Where(x => x.HeaderId == HeaderID && x.CreatedBy == currentUser).FirstOrDefault();                
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadAirtestPlanCreateFormPartial(int HeaderID = 0)
        {
            PLN015 objPLN015 = db.PLN015.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN015 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN015.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN015.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ApproverName))
                {
                    ViewBag.ApproverName = objPLN015.ApprovedBy + " - " + ApproverName;
                }
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN015.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objPLN015.Customer + " - " + customerName;
                string strAirtestPlan = clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN015/" + objPLN015.HeaderId + "/" + objPLN015.RevNo));
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strAirtestPlan &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    PLN014 objPLN014 = db.PLN014.Where(x => x.HeaderId == objPLN015.HeaderId).FirstOrDefault();
                    if (objPLN014 != null)
                    {
                        if (latestRecord.RevNo == objPLN014.PlanRevNo)
                        {
                            ViewBag.isLatest = "true";
                        }
                        else
                        {
                            ViewBag.isLatest = "false";
                        }
                    }
                    else
                    {
                        ViewBag.isLatest = "false";
                    }
                }
                else
                {
                    ViewBag.isLatest = "true";
                }
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN015.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.IsAccessible = Manager.CheckProjectAccesibleByPSNO(objClsLoginInfo.UserName, objPLN015.Project);

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objPLN015.HeaderId, objPLN015.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            else
            {
                objPLN015 = new PLN015();
                objPLN015.ProcessPlan = clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue();
                ViewBag.isLatest = "true";
                objPLN015.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN015.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadAirtestPlanCreateFormPartial", objPLN015);
        }

        [HttpPost]
        public ActionResult SaveHeader(PLN015 pln015, string strHeaderId, string strLineId, string strIsLatest)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln015.HeaderId > 0)
                {
                    PLN015 objPLN015 = db.PLN015.Where(x => x.HeaderId == pln015.HeaderId).FirstOrDefault();
                    if (objPLN015 != null)
                    {
                        objPLN015.Product = pln015.Product;
                        objPLN015.ProcessLicensor = pln015.ProcessLicensor;
                        objPLN015.JobNo = pln015.JobNo;
                        objPLN015.ApprovedBy = pln015.ApprovedBy;
                        objPLN015.EditedBy = objClsLoginInfo.UserName;
                        objPLN015.EditedOn = DateTime.Now;
                        objPLN015.ReviseRemark = pln015.ReviseRemark;
                        if (objPLN015.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN015.RevNo = Convert.ToInt32(objPLN015.RevNo) + 1;
                            objPLN015.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                            objPLN015.ReturnRemark = null;
                            objPLN015.ApprovedOn = null;
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN015.HeaderId;
                        objResponseMsg.Status = objPLN015.Status;
                        objResponseMsg.Revision = objPLN015.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN015.HeaderId, objPLN015.Status, objPLN015.RevNo, objPLN015.Project, objPLN015.Document);


                        //var folderPath = "PLN015/" + NewHeaderId + "/" + objPLN015.RevNo;
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                    PLN015 objPLN015 = db.PLN015.Add(new PLN015
                    {
                        Project = pln015.Project,
                        Document = pln015.Document,
                        Customer = pln015.Customer,
                        ReviseRemark = pln015.ReviseRemark,
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CDD = Helper.ToNullIfTooEarlyForDb(pln015.CDD),
                        Product = pln015.Product,
                        ProcessLicensor = pln015.ProcessLicensor,
                        ProcessPlan = pln015.ProcessPlan,
                        JobNo = pln015.JobNo,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ApprovedBy = pln015.ApprovedBy
                    });
                    db.SaveChanges();
                    NewHeaderId = objPLN015.HeaderId;

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.Status = objPLN015.Status;
                    objResponseMsg.Revision = objPLN015.RevNo;
                    objResponseMsg.HeaderID = NewHeaderId;

                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN015.HeaderId, objPLN015.Status, objPLN015.RevNo, objPLN015.Project, objPLN015.Document);

                    //var folderPath = "PLN015/" + NewHeaderId + "/" + objPLN015.RevNo;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrLineStatus = { clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(), clsImplementationEnum.PlanStatus.Returned.GetStringValue() };
                PLN015 objPLN015 = db.PLN015.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                if (objPLN015 != null)
                {
                    objPLN015.Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue();
                    objPLN015.SubmittedBy = objClsLoginInfo.UserName;
                    objPLN015.SubmittedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully sent for approval.";
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN015.HeaderId, objPLN015.Status, objPLN015.RevNo, objPLN015.Project, objPLN015.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                        objPLN015.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN015.Project, clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue(), objPLN015.RevNo.Value.ToString(), objPLN015.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue(), objPLN015.HeaderId.ToString(), true),
                                                        objPLN015.ApprovedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RetractStatus(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN015 objPLN015 = db.PLN015.Where(u => u.HeaderId == headerId).SingleOrDefault();
                if (objPLN015 != null)
                {
                    objPLN015.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                    objPLN015.EditedBy = objClsLoginInfo.UserName;
                    objPLN015.EditedOn = DateTime.Now;
                    objPLN015.SubmittedOn = null;
                    objPLN015.SubmittedBy = null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                    Manager.UpdatePDN002(objPLN015.HeaderId, objPLN015.Status, objPLN015.RevNo, objPLN015.Project, objPLN015.Document);
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            string strCdd = Manager.GetContractWiseCdd(projectCode);
            if (!string.IsNullOrWhiteSpace(strCdd))
            {
                DateTime dtCdd = Convert.ToDateTime(strCdd);
                objApproverModel.Cdd = dtCdd.Date.ToShortDateString();
            }
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHistoryDetails(string strRole, string HeaderId)
        {
            ViewBag.Role = strRole;
            ViewBag.HeaderId = HeaderId;
            return PartialView("_GetHistoryDetailsPartial");
        }

        [HttpPost]
        public JsonResult LoadAirtestPlanHistory(JQueryDataTableParamModel param, string HeaderId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                //if (param.CTQCompileStatus.ToUpper() == "INITIATOR")
                //{
                //    strWhere += "1=1 and pln015.CreatedBy = '" + objClsLoginInfo.UserName + "'";

                //}
                //else
                //{
                //    strWhere += "1=1 and pln015.ApprovedBy = '" + objClsLoginInfo.UserName + "'";
                //}
                if (!string.IsNullOrWhiteSpace(HeaderId))
                {
                    strWhere += " HeaderId = '" + HeaderId + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln015.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln015.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_AIRTESTPLAN_HEADERS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                Convert.ToString(uc.SubmittedBy),
                                uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                Convert.ToString(uc.ApprovedBy),
                                uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),     "<a title='View' class='iconspace' href='"+WebsiteURL+"/PLN/MaintainLockingCleat/AddHeader?HeaderID="+uc.HeaderId+"'><i class='fa fa-eye'></i></a>" +

                               Convert.ToString(uc.ReturnRemark),
                               (param.CTQCompileStatus.ToUpper() == "INITIATOR"?"<center><a title='View' href='"+WebsiteURL+"/PLN/MaintainAirtestPlan/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainAirtestPlan/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>":"<center><a title='View' href='"+WebsiteURL+"/PLN/ApproveAirtestPlan/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainAirtestPlan/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN015 objPLN015 = db.PLN015.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objPLN015 != null)
                {
                    objPLN015.RevNo = Convert.ToInt32(objPLN015.RevNo) + 1;
                    objPLN015.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objPLN015.ReviseRemark = strRemarks;
                    objPLN015.EditedBy = objClsLoginInfo.UserName;
                    objPLN015.EditedOn = DateTime.Now;
                    objPLN015.ReturnRemark = null;
                    objPLN015.ApprovedOn = null;
                    objPLN015.SubmittedBy = null;
                    objPLN015.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPLN015.HeaderId, objPLN015.Status, objPLN015.RevNo, objPLN015.Project, objPLN015.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objPLN015.HeaderId;
                    objResponseMsg.Status = objPLN015.Status;
                    objResponseMsg.rev = objPLN015.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public class CustomResponceMsg : clsHelper.ResponseMsg
        {
            public string Project;
            public string Customer;
            public int HeaderID;
            public int LineID;
            public string Status;
            public string CDD;
            public int? rev;
        }
        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN015_Log objPLN015_Log = db.PLN015_Log.Where(x => x.Id == Id && x.CreatedBy == currentUser).FirstOrDefault();
                //if (objPLN015_Log == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadAirtestPlanHistoryFormPartial(int Id = 0)
        {
            PLN015_Log objPLN015_Log = db.PLN015_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN015_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN015_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN015_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN015_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN015_Log.ApprovedBy = objPLN015_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN015_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN015_Log.Customer = objPLN015_Log.Customer + " - " + customerName;
                string strAirtestPlan = clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strAirtestPlan &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    //if (latestRecord.HeaderId == objPLN003.HeaderId)
                    //{

                    //}
                }
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN015/" + objPLN015_Log.HeaderId + "/" + objPLN015_Log.RevNo));
            }
            else
            {
                objPLN015_Log = new PLN015_Log();
                objPLN015_Log.ProcessPlan = clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue();
                objPLN015_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN015_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            //var urlPrefix = "http://" + Request.Url.Authority + "/PLN/MaintainAirtestPlan/ViewHistory?Id=";// AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            var urlPrefix = WebsiteURL + "/PLN/MaintainAirtestPlan/ViewHistory?Id=";
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.PLN015_Log.Any(q => (q.HeaderId == objPLN015_Log.HeaderId && q.RevNo == (objPLN015_Log.RevNo - 1))) ? urlPrefix + db.PLN015_Log.Where(q => (q.HeaderId == objPLN015_Log.HeaderId && q.RevNo == (objPLN015_Log.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.PLN015_Log.Any(q => (q.HeaderId == objPLN015_Log.HeaderId && q.RevNo == (objPLN015_Log.RevNo + 1))) ? urlPrefix + db.PLN015_Log.Where(q => (q.HeaderId == objPLN015_Log.HeaderId && q.RevNo == (objPLN015_Log.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return PartialView("_LoadAirtestPlanHistoryFormPartial", objPLN015_Log);
        }

        #region Export Excel
        //Excel funtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                { //header grid data
                    var lst = db.SP_FETCH_AIRTESTPLAN_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      Document = uc.Document,
                                      Customer = uc.Customer,
                                      Product = uc.Product,
                                      ProcessLicensor = uc.ProcessLicensor,
                                      RevNo = "R" + uc.RevNo,
                                      Status = uc.Status,
                                      CDD = uc.CDD,
                                      ProcessPlan = uc.ProcessPlan,
                                      CreatedBy = uc.CreatedBy,
                                      CreatedOn = uc.CreatedOn,
                                      EditedBy = uc.EditedBy,
                                      EditedOn = uc.EditedOn,
                                      SubmittedBy = uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ReturnRemark = uc.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                { //history header
                    var lst = db.SP_FETCH_AIRTESTPLAN_HEADERS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      Document = uc.Document,
                                      Customer = uc.Customer,
                                      Product = uc.Product,
                                      ProcessLicensor = uc.ProcessLicensor,
                                      RevNo = "R" + uc.RevNo,
                                      Status = uc.Status,
                                      CDD = uc.CDD,
                                      ProcessPlan = uc.ProcessPlan,
                                      CreatedBy = uc.CreatedBy,
                                      CreatedOn = uc.CreatedOn,
                                      EditedBy = uc.EditedBy,
                                      EditedOn = uc.EditedOn,
                                      SubmittedBy = uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ReturnRemark = uc.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}