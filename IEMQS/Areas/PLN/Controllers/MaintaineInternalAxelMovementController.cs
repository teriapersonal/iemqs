﻿using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.Utility.Models;
using System.Globalization;
namespace IEMQS.Areas.PLN.Controllers
{
    public class MaintaineInternalAxelMovementController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadInternalAxelMovementListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_InternalAxelMovementListDataPartial");



        }

        [HttpPost]
        public JsonResult LoadInternalAxelMovementHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.PlanStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                //strWhere += " and pln016.CreatedBy in('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln016.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln016.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_FETCH_INTERNALAXELMOVEMENT_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                    Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),

                                Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                                //"<center><a title='View' href='/PLN/MaintaineInternalAxelMovement/CreateInternalAxelMovement?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>",
                                 "<a title='View' href='"+WebsiteURL+"/PLN/MaintaineInternalAxelMovement/CreateInternalAxelMovement?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='' class='iconspace fa fa-eye'></i></a>"+
                                   Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/PLN/MaintaineInternalAxelMovement/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblInternalAxelMovementHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +" "+
                                   "<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintaineInternalAxelMovement/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='' class='iconspace fa fa-clock-o'></i></a>"
                                 +(uc.RevNo>0 ?"<a title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Initiator','/PLN/MaintaineInternalAxelMovement/GetHistoryDetails','Internal Axel Movement')\"><i style='' class='iconspace  fa fa-history'></i></a>":"<i title='History' style = '' class='disabledicon fa fa-history'></i>")

                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Internal_Axel_Movement_Plan);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_INTERNALAXELMOVEMENT_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Documents = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,

                                      //ProcessPlan = li.ProcessPlan,
                                      //CreatedBy = li.CreatedBy,
                                      //CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn == null || li.SubmittedOn.Value == DateTime.MinValue ? "NA" : li.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = li.ApprovedBy,
                                      ApproveOn = li.ApprovedOn == null || li.ApprovedOn.Value == DateTime.MinValue ? "NA" : li.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      // ReturnRemark = li.ReturnRemark

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                 else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {  //tack weld lines
                    var lst = db.SP_FETCH_INTERNALAXELMOVEMENT_HEADERS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Documents = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      //ProcessPlan = li.ProcessPlan,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn == null || li.EditedOn.Value == DateTime.MinValue ? "NA" : li.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn == null || li.SubmittedOn.Value == DateTime.MinValue ? "NA" : li.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = li.ApprovedBy,
                                      ApproveOn = li.ApprovedOn == null || li.ApprovedOn.Value == DateTime.MinValue ? "NA" : li.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      // ReturnRemark = li.ReturnRemark

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_WKG_GET_LINESHISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RowNo = li.ROW_NO,
                                      Remarks = li.Remarks,
                                      SeamNo = li.SeamNo,
                                      SeamDescription = li.SeamDescription,
                                      WEPType = li.WEPTYPE,
                                      LengthArea = li.LengthArea,
                                      Thk1 = li.Thk1,
                                      Thk2 = li.Thk2,
                                      Thk3 = li.Thk3,
                                      Thk4 = li.Thk4,
                                      Thk5 = li.Thk5,
                                      Overlay = li.Thk6,
                                      CsSS = li.CSSS,
                                      JointType = li.JointType,
                                      RootRunProcess = li.RootRunProcess,
                                      FaceSideProcess = li.FaceSideProcess,
                                      ChipBackProcess = li.ChipBackProcess,
                                      MinThk = li.MinThk,
                                      FacesideArea = li.FaceSideArea,
                                      CBArea = li.CBarea,
                                      WeldKGFaceside = li.WeldKgFaceSide,
                                      WeldKgCBSide = li.WeldKgCBSide,
                                      WeldKG = li.TotalWeldKg,
                                      PreparedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn == null || li.CreatedOn.Value == DateTime.MinValue ? "NA" : li.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),


                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Internal Axel TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                PLN018_Log objPLN018_Log = db.PLN018_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN018_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN018_Log.CreatedBy) : null;
                model.CreatedOn = objPLN018_Log.CreatedOn;
                model.EditedBy = objPLN018_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN018_Log.EditedBy) : null;
                model.EditedOn = objPLN018_Log.EditedOn;
                model.SubmittedBy = objPLN018_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN018_Log.SubmittedBy) : null;
                model.SubmittedOn = objPLN018_Log.SubmittedOn;

                model.ApprovedBy = objPLN018_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN018_Log.ApprovedBy) : null;
                model.ApprovedOn = objPLN018_Log.ApprovedOn;

            }
            else
            {

                PLN018_Log objPLN018_Log = db.PLN018_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN018_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN018_Log.CreatedBy) : null;
                model.CreatedOn = objPLN018_Log.CreatedOn;
                model.EditedBy = objPLN018_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN018_Log.EditedBy) : null;
                model.EditedOn = objPLN018_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "Internal Axel Timeline";

            if (HeaderId > 0)
            {
                PLN018 objPLN018 = db.PLN018.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN018.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN018.CreatedBy) : null;
                model.CreatedOn = objPLN018.CreatedOn;
                model.EditedBy = objPLN018.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN018.EditedBy) : null;
                model.EditedOn = objPLN018.EditedOn;
                model.SubmittedBy = objPLN018.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN018.SubmittedBy) : null;
                model.SubmittedOn = objPLN018.SubmittedOn;
                model.ApprovedBy = objPLN018.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN018.ApprovedBy) : null;
                model.ApprovedOn = objPLN018.ApprovedOn;

            }
            else
            {
                PLN018 objPLN018 = db.PLN018.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN018.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN018.CreatedBy) : null;
                model.CreatedOn = objPLN018.CreatedOn;
                model.EditedBy = objPLN018.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN018.EditedBy) : null;
                model.EditedOn = objPLN018.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult CreateInternalAxelMovement(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN018");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN018 objPLN018 = db.PLN018.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                //if (objPLN018 == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN018.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                objPLN018 = db.PLN018.Where(x => x.HeaderId == HeaderID && x.CreatedBy == currentUser).FirstOrDefault();
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }
        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN018 objPLN018 = db.PLN018.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objPLN018 != null)
                {
                    objPLN018.RevNo = Convert.ToInt32(objPLN018.RevNo) + 1;
                    objPLN018.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objPLN018.ReviseRemark = strRemarks;
                    objPLN018.EditedBy = objClsLoginInfo.UserName;
                    objPLN018.EditedOn = DateTime.Now;
                    objPLN018.ReturnRemark = null;
                    objPLN018.ApprovedOn = null;
                    objPLN018.SubmittedBy = null;
                    objPLN018.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPLN018.HeaderId, objPLN018.Status, objPLN018.RevNo, objPLN018.Project, objPLN018.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objPLN018.HeaderId;
                    objResponseMsg.Status = objPLN018.Status;
                    objResponseMsg.rev = objPLN018.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult LoadInternalAxelMovementCreateFormPartial(int HeaderID = 0)
        {
            PLN018 objPLN018 = db.PLN018.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN018 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN018.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN018.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(ApproverName))
                {
                    ViewBag.ApproverName = objPLN018.ApprovedBy + " - " + ApproverName;
                }
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN018.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objPLN018.Customer + " - " + customerName;
                string strInternalAxelMovement = clsImplementationEnum.PlanList.Internal_Axel_Movement_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN018/" + objPLN018.HeaderId + "/" + objPLN018.RevNo));

                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN018.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objPLN018.HeaderId, objPLN018.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            else
            {
                objPLN018 = new PLN018();
                objPLN018.ProcessPlan = clsImplementationEnum.PlanList.Internal_Axel_Movement_Plan.GetStringValue();
                objPLN018.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN018.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadInternalAxelMovementCreateFormPartial", objPLN018);
        }

        [HttpPost]
        //public ActionResult SaveHeader(PLN018 pln018, bool hasAttachments, Dictionary<string, string> Attach)
        public ActionResult SaveHeader(PLN018 pln018)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln018.HeaderId > 0)
                {
                    PLN018 objPLN018 = db.PLN018.Where(x => x.HeaderId == pln018.HeaderId).FirstOrDefault();
                    if (objPLN018 != null)
                    {
                        objPLN018.Product = pln018.Product;
                        objPLN018.ProcessLicensor = pln018.ProcessLicensor;
                        objPLN018.JobNo = pln018.JobNo;
                        objPLN018.ReviseRemark = pln018.ReviseRemark;
                        objPLN018.ApprovedBy = pln018.ApprovedBy;
                        objPLN018.EditedBy = objClsLoginInfo.UserName;
                        objPLN018.EditedOn = DateTime.Now;
                        if (objPLN018.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN018.RevNo = Convert.ToInt32(objPLN018.RevNo) + 1;
                            objPLN018.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        objPLN018.ReturnRemark = null;
                        objPLN018.ApprovedOn = null; 
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN018.HeaderId;
                        objResponseMsg.Status = objPLN018.Status;
                        objResponseMsg.Revision = objPLN018.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN018.HeaderId, objPLN018.Status, objPLN018.RevNo, objPLN018.Project, objPLN018.Document);

                      
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                    PLN018 objPLN018 = db.PLN018.Add(new PLN018
                    {
                        Project = pln018.Project,
                        Document = pln018.Document,
                        Customer = pln018.Customer,
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CDD = Helper.ToNullIfTooEarlyForDb(pln018.CDD),
                        Product = pln018.Product,
                        ProcessLicensor = pln018.ProcessLicensor,
                        ProcessPlan = pln018.ProcessPlan,
                        JobNo = pln018.JobNo,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ApprovedBy = pln018.ApprovedBy
                    });
                    db.SaveChanges();
                    NewHeaderId = objPLN018.HeaderId;

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.Status = objPLN018.Status;
                    objResponseMsg.Revision = objPLN018.RevNo;
                    objResponseMsg.HeaderID = NewHeaderId;

                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN018.HeaderId, objPLN018.Status, objPLN018.RevNo, objPLN018.Project, objPLN018.Document);

                   
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult retrack(string headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int hd = Convert.ToInt32(headerid);
                if (hd > 0)
                {
                    PLN018 objPLN018 = db.PLN018.Where(x => x.HeaderId == hd).FirstOrDefault();
                    objPLN018.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    objPLN018.SubmittedOn = null;
                    objPLN018.SubmittedBy = null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    Manager.UpdatePDN002(objPLN018.HeaderId, objPLN018.Status, objPLN018.RevNo, objPLN018.Project, objPLN018.Document);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApproval(int HeaderId, string ApprovedBy)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrLineStatus = { clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(), clsImplementationEnum.PlanStatus.Returned.GetStringValue() };
                PLN018 objPLN018 = db.PLN018.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                if (objPLN018 != null)
                {
                    objPLN018.Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue();
                    //Update ApprovedBy on Sent for approval (added by Pooja Desai)
                    if (!string.IsNullOrWhiteSpace(ApprovedBy))
                        objPLN018.ApprovedBy = ApprovedBy.Split('-')[0].ToString().Trim();
                    else
                        objPLN018.ApprovedBy = "";
                    //Update ApprovedBy on Sent for approval (added by Pooja Desai)
                    objPLN018.SubmittedBy = objClsLoginInfo.UserName;
                    objPLN018.SubmittedOn = DateTime.Now; 
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Plan successfully sent for approval.";
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN018.HeaderId, objPLN018.Status, objPLN018.RevNo, objPLN018.Project, objPLN018.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                       objPLN018.Project,
                                                       "",
                                                       "",
                                                       Manager.GetPDINDocumentNotificationMsg(objPLN018.Project, clsImplementationEnum.PlanList.Internal_Axel_Movement_Plan.GetStringValue(), objPLN018.RevNo.Value.ToString(), objPLN018.Status),
                                                       clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                       Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Internal_Axel_Movement_Plan.GetStringValue(), objPLN018.HeaderId.ToString(), true),
                                                       objPLN018.ApprovedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Plan not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            string strCdd = Manager.GetContractWiseCdd(projectCode);
            if (!string.IsNullOrWhiteSpace(strCdd))
            {
                DateTime dtCdd = Convert.ToDateTime(strCdd);
                objApproverModel.Cdd = dtCdd.Date.ToShortDateString();
            }
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHistoryDetails(string strRole,string HeaderId)
        {
            ViewBag.Role = strRole;
            ViewBag.HeaderId = HeaderId;
            return PartialView("_GetHistoryDetailsPartial");
        }

        [HttpPost]
        public JsonResult LoadInternalAxelMovementHistory(JQueryDataTableParamModel param,string HeaderId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                //if (param.CTQCompileStatus.ToUpper() == "INITIATOR")
                //{
                //    strWhere += "1=1 and CreatedBy = '" + objClsLoginInfo.UserName + "'";

                //}
                //else
                //{
                //    strWhere += "1=1 and ApprovedBy = '" + objClsLoginInfo.UserName + "'";
                //}
                if (!string.IsNullOrWhiteSpace(HeaderId))
                {
                    strWhere += " HeaderId = '" + HeaderId + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln018.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln018.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var lstResult = db.SP_FETCH_INTERNALAXELMOVEMENT_HEADERS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                    Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),

                                Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                               (param.CTQCompileStatus.ToUpper() == "INITIATOR"?"<center><a title='View' href='"+WebsiteURL+"/PLN/MaintaineInternalAxelMovement/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintaineInternalAxelMovement/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>":"<center><a title='View' href='"+WebsiteURL+"/PLN/ApproveInternalAxelMovement/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintaineInternalAxelMovement/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN018_Log objPLN018_Log = db.PLN018_Log.Where(x => x.Id == Id && x.CreatedBy == currentUser).FirstOrDefault();
                //if (objPLN018_Log == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.Id = Id;
            return View();
        }
        public class CustomResponceMsg : clsHelper.ResponseMsg
        {
            public string Project;
            public string Customer;
            public int HeaderID;
            public int LineID;
            public string Status;
            public string CDD;
            public int? rev;
        }
        [HttpPost]
        public ActionResult LoadInternalAxelMovementHistoryFormPartial(int Id = 0)
        {
            PLN018_Log objPLN018_Log = db.PLN018_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN018_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN018_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN018_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN018_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN018_Log.ApprovedBy = objPLN018_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN018_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN018_Log.Customer = objPLN018_Log.Customer + " - " + customerName;
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN018/" + objPLN018_Log.HeaderId + "/" + objPLN018_Log.RevNo));
            }
            else
            {
                objPLN018_Log = new PLN018_Log();
                objPLN018_Log.ProcessPlan = clsImplementationEnum.PlanList.Internal_Axel_Movement_Plan.GetStringValue();
                objPLN018_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN018_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            //var urlPrefix = "http://" + Request.Url.Authority + "/PLN/MaintaineInternalAxelMovement/ViewHistory?Id=";// AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            var urlPrefix = WebsiteURL + "/PLN/MaintaineInternalAxelMovement/ViewHistory?Id=";
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.PLN018_Log.Any(q => (q.HeaderId == objPLN018_Log.HeaderId && q.RevNo == (objPLN018_Log.RevNo - 1))) ? urlPrefix + db.PLN018_Log.Where(q => (q.HeaderId == objPLN018_Log.HeaderId && q.RevNo == (objPLN018_Log.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.PLN018_Log.Any(q => (q.HeaderId == objPLN018_Log.HeaderId && q.RevNo == (objPLN018_Log.RevNo + 1))) ? urlPrefix + db.PLN018_Log.Where(q => (q.HeaderId == objPLN018_Log.HeaderId && q.RevNo == (objPLN018_Log.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return PartialView("_LoadInternalAxelMovementHistoryFormPartial", objPLN018_Log);
        }
    }
}