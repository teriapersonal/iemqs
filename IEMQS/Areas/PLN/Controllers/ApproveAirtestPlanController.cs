﻿using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PLN.Controllers
{
    public class ApproveAirtestPlanController : clsBase
    {
        //modified by nikita on 29/08/2017
        //added method updatePDN002
        //task assigned by Satish Pawar 
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadAirtestPlanListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_AirtestPlanListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadAirtestPlanHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue() + "') and pln015.ApprovedBy in('" + objClsLoginInfo.UserName + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                //strWhere += " and pln015.ApprovedBy in('" + objClsLoginInfo.UserName + "')";

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln015.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln015.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_FETCH_AIRTESTPLAN_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                               Convert.ToString(uc.SubmittedBy),
                               uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                               Convert.ToString(uc.ApprovedBy),
                               uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),     "<a title='View' class='iconspace' href='"+WebsiteURL+"/PLN/MaintainLockingCleat/AddHeader?HeaderID="+uc.HeaderId+"'><i class='fa fa-eye'></i></a>" +
                                 Convert.ToString(uc.ReturnRemark),
                                // "<center><a title='View' href='/PLN/ApproveAirtestPlan/ApproveAirtestPlan?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                                 "<a title='View' href='"+WebsiteURL+"/PLN/ApproveAirtestPlan/ApproveAirtestPlan?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+" "
                                 +"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainAirtestPlan/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='margin-left:5px;cursor:pointer;' class='fa fa-clock-o'></i></a>"
                                 +(uc.RevNo>0 ?"<a title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Initiator','/PLN/MaintainAirtestPlan/GetHistoryDetails','Airtest Plan')\"><i style='margin-left:5px;' class='fa fa-history'></i></a>":"<i title='History' style = 'margin-left:5px;opacity:0.5' class='fa fa-history'></i>")

                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ApproveAirtestPlan(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN015");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN015 objPLN015 = db.PLN015.Where(x => x.HeaderId == HeaderID).FirstOrDefault();                
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN015.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadAirtestPlanCreateFormPartial(int HeaderID = 0)
        {
            PLN015 objPLN015 = db.PLN015.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN015 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN015.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                if (objPLN015.ApprovedBy != null)
                {
                    if (objPLN015.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
                objPLN015.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN015.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN015.ApprovedBy = objPLN015.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN015.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN015.Customer = objPLN015.Customer + " - " + customerName;
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN015/" + objPLN015.HeaderId + "/" + objPLN015.RevNo));
               
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN015.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, HeaderID, clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue());
            }
            else
            {
                objPLN015 = new PLN015();
                objPLN015.ProcessPlan = clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue();
                objPLN015.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN015.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadAirtestPlanCreateFormPartial", objPLN015);
        }

        [HttpPost]
        public ActionResult ReturnAirtestPlan(int HeaderId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN015 objPLN015 = db.PLN015.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN015 != null)
                {
                    objPLN015.Status = clsImplementationEnum.PlanStatus.Returned.GetStringValue();
                    objPLN015.ReturnRemark = Remarks;
                    objPLN015.EditedBy = objClsLoginInfo.UserName;
                    objPLN015.EditedOn = DateTime.Now;




                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully returned.";
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN015.HeaderId, objPLN015.Status, objPLN015.RevNo, objPLN015.Project, objPLN015.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN015.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN015.Project, clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue(), objPLN015.RevNo.Value.ToString(), objPLN015.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue(), objPLN015.HeaderId.ToString(), false),
                                                        objPLN015.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please add remarks for returning Checklist.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            for (int i = 0; i < arrayHeaderIds.Length; i++)
            {
                int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg ApproveHeaderByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN015 objPLN015 = db.PLN015.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN015 != null)
                {
                    objPLN015.Status = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                    objPLN015.ApprovedBy = objClsLoginInfo.UserName;
                    objPLN015.ApprovedOn = DateTime.Now;




                    #region Header Log
                    var lstPLN015_Log = db.PLN015_Log.Where(x => x.HeaderId == objPLN015.HeaderId).ToList();
                    PLN015_Log objPLN015_Log = new PLN015_Log();
                    if (objPLN015_Log != null)
                    {
                        objPLN015_Log = lstPLN015_Log.FirstOrDefault();
                        lstPLN015_Log.ForEach(i => { i.Status = clsImplementationEnum.PlanStatus.Superseded.GetStringValue(); });
                    }

                    objPLN015_Log = db.PLN015_Log.Add(new PLN015_Log
                    {
                        HeaderId = objPLN015.HeaderId,
                        Project = objPLN015.Project,
                        Document = objPLN015.Document,
                        Customer = objPLN015.Customer,
                        RevNo = objPLN015.RevNo,
                        Status = objPLN015.Status,
                        CDD = objPLN015.CDD,
                        Product = objPLN015.Product,
                        ProcessLicensor = objPLN015.ProcessLicensor,
                        ProcessPlan = objPLN015.ProcessPlan,
                        Attachment = objPLN015.Attachment,
                        JobNo = objPLN015.JobNo,
                        CreatedBy = objPLN015.CreatedBy,
                        CreatedOn = objPLN015.CreatedOn,
                        EditedBy = objPLN015.EditedBy,
                        EditedOn = objPLN015.EditedOn,
                        ApprovedBy = objPLN015.ApprovedBy,
                        ApprovedOn = objPLN015.ApprovedOn,
                        ReturnRemark = objPLN015.ReturnRemark,
                        ReviseRemark = objPLN015.ReviseRemark,
                        SubmittedBy = objPLN015.SubmittedBy,
                        SubmittedOn = objPLN015.SubmittedOn
                    });

                    #endregion
                    db.SaveChanges();

                    int? currentRevisionNull = db.PLN015.Where(q => q.HeaderId == HeaderId).FirstOrDefault().RevNo;
                    int currentRevision = ((currentRevisionNull == null) ? 0 : currentRevisionNull.Value);
                    //clsUpload.CopyFolderContents("PLN015/" + HeaderId + "/" + currentRevision, "PLN015/" + HeaderId + "/" + (currentRevision + 1));
                    var oldfolderPath = "PLN015//" + HeaderId + "//" + currentRevision;
                    var newfolderPath = "PLN015//" + HeaderId + "//" + (currentRevision + 1);
                    //(new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);

                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(oldfolderPath, newfolderPath, oldfolderPath, HeaderId, newfolderPath, HeaderId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully approved.";
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    int newId = db.PLN015_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objPLN015.HeaderId, objPLN015.Status, objPLN015.RevNo, objPLN015.Project, objPLN015.Document, newId);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN015.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN015.Project, clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue(), objPLN015.RevNo.Value.ToString(), objPLN015.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue(), objPLN015.HeaderId.ToString(), false),
                                                        objPLN015.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Checklist not available approved.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN015_Log objPLN015_Log = db.PLN015_Log.Where(x => x.Id == Id && x.ApprovedBy == currentUser).FirstOrDefault();
                if (objPLN015_Log == null)
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadAirtestPlanHistoryFormPartial(int Id = 0)
        {
            PLN015_Log objPLN015_Log = db.PLN015_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN015_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN015_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN015_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN015_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN015_Log.ApprovedBy = objPLN015_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN015_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN015_Log.Customer = objPLN015_Log.Customer + " - " + customerName;
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN015/" + objPLN015_Log.HeaderId + "/" + objPLN015_Log.RevNo));
            }
            else
            {
                objPLN015_Log = new PLN015_Log();
                objPLN015_Log.ProcessPlan = clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue();
                objPLN015_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN015_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadAirtestPlanHistoryFormPartial", objPLN015_Log);
        }
        [HttpPost]
        public ActionResult GetHistoryDetails(string strRole, string HeaderId)
        {
            ViewBag.Role = strRole;
            ViewBag.HeaderId = HeaderId;
            return PartialView("_GetHistoryDetailsPartial");
        }
        [HttpPost]
        public JsonResult LoadAirtestPlanHistory(JQueryDataTableParamModel param, string HeaderId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                //if (param.CTQCompileStatus.ToUpper() == "INITIATOR")
                //{
                //    strWhere += "1=1 and pln015.CreatedBy = '" + objClsLoginInfo.UserName + "'";

                //}
                //else
                //{
                //    strWhere += "1=1 and pln015.ApprovedBy = '" + objClsLoginInfo.UserName + "'";
                //}
                if (!string.IsNullOrWhiteSpace(HeaderId))
                {
                    strWhere += " HeaderId = '" + HeaderId + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln015.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln015.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_FETCH_AIRTESTPLAN_HEADERS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                               Convert.ToString(uc.SubmittedBy),
                                uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                Convert.ToString(uc.ApprovedBy),
                                uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),     "<a title='View' class='iconspace' href='"+WebsiteURL+"/PLN/MaintainLockingCleat/AddHeader?HeaderID="+uc.HeaderId+"'><i class='fa fa-eye'></i></a>" +

                               Convert.ToString(uc.ReturnRemark),
                               (param.CTQCompileStatus.ToUpper() == "INITIATOR"?"<center><a title='View' href='"+WebsiteURL+"/PLN/ApproveAirtestPlan/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainAirtestPlan/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>":"<center><a title='View' href='"+WebsiteURL+"/PLN/ApproveAirtestPlan/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainAirtestPlan/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveHeader(PLN015 pln015, string strHeaderId, string strLineId, string strIsLatest)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln015.HeaderId > 0)
                {
                    PLN015 objPLN015 = db.PLN015.Where(x => x.HeaderId == pln015.HeaderId).FirstOrDefault();
                    if (objPLN015 != null)
                    {
                        objPLN015.Product = pln015.Product;
                        objPLN015.ProcessLicensor = pln015.ProcessLicensor;
                        objPLN015.JobNo = pln015.JobNo;
                        objPLN015.ApprovedBy = pln015.ApprovedBy.Split('-')[0].ToString().Trim(); ;
                        objPLN015.EditedBy = objClsLoginInfo.UserName;
                        objPLN015.EditedOn = DateTime.Now;
                        if (objPLN015.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN015.RevNo = Convert.ToInt32(objPLN015.RevNo) + 1;
                            objPLN015.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN015.HeaderId;
                        objResponseMsg.Status = objPLN015.Status;
                        objResponseMsg.Revision = objPLN015.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN015.HeaderId, objPLN015.Status, objPLN015.RevNo, objPLN015.Project, objPLN015.Document);


                        //var folderPath = "PLN015/" + NewHeaderId + "/" + objPLN015.RevNo;
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Air_Test_Plan, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
      
        #region Export Excel
        //Excel funtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                { //header grid data
                    var lst = db.SP_FETCH_AIRTESTPLAN_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      Document = uc.Document,
                                      Customer = uc.Customer,
                                      Product = uc.Product,
                                      ProcessLicensor = uc.ProcessLicensor,
                                      RevNo = "R" + uc.RevNo,
                                      Status = uc.Status,
                                      CDD = uc.CDD,
                                      ProcessPlan = uc.ProcessPlan,
                                      CreatedBy = uc.CreatedBy,
                                      CreatedOn = uc.CreatedOn,
                                      EditedBy = uc.EditedBy,
                                      EditedOn = uc.EditedOn,
                                      SubmittedBy =uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ReturnRemark = uc.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                { //history header
                    var lst = db.SP_FETCH_AIRTESTPLAN_HEADERS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      Document = uc.Document,
                                      Customer = uc.Customer,
                                      Product = uc.Product,
                                      ProcessLicensor = uc.ProcessLicensor,
                                      RevNo = "R" + uc.RevNo,
                                      Status = uc.Status,
                                      CDD = uc.CDD,
                                      ProcessPlan = uc.ProcessPlan,
                                      CreatedBy = uc.CreatedBy,
                                      CreatedOn = uc.CreatedOn,
                                      EditedBy = uc.EditedBy,
                                      EditedOn = uc.EditedOn,
                                      SubmittedBy = uc.SubmittedBy,
                                      SubmittedOn = uc.SubmittedOn == null || uc.SubmittedOn.Value == DateTime.MinValue ? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy,
                                      ApprovedOn = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ReturnRemark = uc.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}