﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PLN.Controllers
{
    public class ApproveFXRController : clsBase
    {

        #region Utility
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isVisible = true)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    if (isVisible)
                    {
                        htmlControl = "<a><i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i></a>";
                    }
                    else
                    {
                        htmlControl = "<a><i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; display:none' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i></a>";
                    }
                }
            }

            return htmlControl;
        }

        public int FindChildCount(string fixtureName, int HeaderId)
        {
            return db.FXR002.Where(x => x.FixtureName.ToLower() == fixtureName.ToLower() && x.HeaderId == HeaderId).Count();
        }
        #endregion
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        public ActionResult FixtureLineDetails(string HeaderID)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("FXR001");
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(HeaderID))
            {
                HeaderId = Convert.ToInt32(HeaderID);
            }
            FXR001 objFXR001 = db.FXR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objFXR001 != null)
            {                
                ViewBag.HeaderID = objFXR001.HeaderId;
                ViewBag.Status = objFXR001.Status;
                if (objFXR001.ApprovedBy != null)
                {
                    if (objFXR001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }

                var projectDetails = db.COM001.Where(x => x.t_cprj == objFXR001.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objFXR001.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objFXR001.ApprovedBy = objFXR001.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objFXR001.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.Customer = objFXR001.Customer + " - " + customerName;

                var objFXR002 = db.FXR002.Where(x => x.HeaderId == HeaderId).ToList();
                decimal RM = Math.Truncate((objFXR002.Select(x => x.Wt ?? 0).Sum() / 1000) * 1000) / 1000;
                decimal EM = Math.Truncate((objFXR002.Select(x => x.ReqWt ?? 0).Sum() / 1000) * 1000) / 1000;
                decimal RUM = Math.Truncate((RM - EM) * 1000) / 1000;
                decimal ROP = RM != 0 ? Math.Truncate((RUM / RM) * 1000) / 1000 : 0;

                ViewBag.ReqMaterial = RM;
                ViewBag.EstimatedMaterial = EM;
                ViewBag.ReuseMaterial = RUM;
                ViewBag.ReusePercentage = ROP;

                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderId && x.DocumentNo == objFXR001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, HeaderId, clsImplementationEnum.PlanList.Fixture.GetStringValue());
                //objFXR001.Document = JsonConvert.SerializeObject(Manager.getDocuments1("FXR001/" + objFXR001.HeaderId + "/" + objFXR001.RevNo));
            }
            return View(objFXR001);
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        #region History
        [HttpPost]
        public ActionResult GetHistoryDetails(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            return PartialView("_GetHistoryDetailsPartial");
        }

        [HttpPost]
        public JsonResult LoadFixtureHistory(JQueryDataTableParamModel param)
        {
            try
            {
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1 and HeaderId = " + HeaderId + " ";

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                //if (param.CTQCompileStatus.ToUpper() == "INITIATOR")
                //{
                //    strWhere += "1=1 and FXR001_Log.CreatedBy = '" + objClsLoginInfo.UserName + "'";

                //}
                //else
                //{
                //    strWhere += "1=1 and FXR001_Log.ApprovedBy = '" + objClsLoginInfo.UserName + "'";
                //}
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "FXR001_Log.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "FXR001_Log.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_FIXTURES_HEADER_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               //Convert.ToString(uc.ReturnRemark),
                               "<center><a title='View' href='"+WebsiteURL+"/PLN/ApproveFXR/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainFXR/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                FXR001_Log objFXR001_Log = db.FXR001_Log.Where(x => x.Id == Id).FirstOrDefault();
                if (objFXR001_Log == null)
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadFXRHistoryFormPartial(int Id = 0)
        {
            FXR001_Log objFXR001_Log = db.FXR001_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objFXR001_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objFXR001_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objFXR001_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                ViewBag.ApproverName = objFXR001_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objFXR001_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objFXR001_Log.Customer + " - " + customerName;
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();

                var objFXR002_Log = db.FXR002_Log.Where(x => x.HeaderId == objFXR001_Log.HeaderId).ToList();

                decimal RM = Math.Truncate((objFXR002_Log.Select(x => x.Wt ?? 0).Sum() / 1000) * 1000) / 1000;
                decimal EM = Math.Truncate((objFXR002_Log.Select(x => x.ReqWt ?? 0).Sum() / 1000) * 1000) / 1000;
                decimal RUM = Math.Truncate((RM - EM) * 1000) / 1000;
                decimal ROP = RM != 0 ? Math.Truncate((RUM / RM) * 1000) / 1000 : 0;

                ViewBag.ReqMaterial = RM;
                ViewBag.EstimatedMaterial = EM;
                ViewBag.ReuseMaterial = RUM;
                ViewBag.ReusePercentage = ROP;
            }
            else
            {
                objFXR001_Log = new FXR001_Log();
                objFXR001_Log.Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue();
                objFXR001_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            var urlPrefix = WebsiteURL + "/PLN/ApproveFXR/ViewHistory?Id=";// AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.FXR001_Log.Any(q => (q.HeaderId == objFXR001_Log.HeaderId && q.RevNo == (objFXR001_Log.RevNo - 1))) ? urlPrefix + db.FXR001_Log.Where(q => (q.HeaderId == objFXR001_Log.HeaderId && q.RevNo == (objFXR001_Log.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.FXR001_Log.Any(q => (q.HeaderId == objFXR001_Log.HeaderId && q.RevNo == (objFXR001_Log.RevNo + 1))) ? urlPrefix + db.FXR001_Log.Where(q => (q.HeaderId == objFXR001_Log.HeaderId && q.RevNo == (objFXR001_Log.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return PartialView("_LoadFixtureHistoryFormPartial", objFXR001_Log);
        }

        [HttpPost]
        public ActionResult LoadFixtureListHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int RefId = Convert.ToInt32(param.CTQHeaderId);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                strWhere = "1=1 and RefId In (" + RefId + ")";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch +
                               "%' or Document like '%" + param.sSearch +
                               "%' or RevNo like '%" + param.sSearch +
                               "%' or FixtureName like '%" + param.sSearch +
                               "%' or DescriptionofItem like '%" + param.sSearch +
                               "%' or Category like '%" + param.sSearch +
                               "%' or Material like '%" + param.sSearch +
                               "%' or MaterialType like '%" + param.sSearch +
                               "%' or StructuralType like '%" + param.sSearch +
                               "%' or PipeNormalBore like '%" + param.sSearch + "%')";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_FIXTURE_HEADER_LINES_HISTORY
                    (
                        0,
                        StartIndex,
                        EndIndex,
                        strSortOrder,
                        strWhere
                    ).ToList();

                var data = (from fx in lstResult
                            select new[]
                            {
                                "",
                                Convert.ToString(fx.ROW_NO),
                                fx.ParentId == 0 ? "<nobr><img onclick='ExpandCollapsChild("+ fx.LineId +", this)' src='"+WebsiteURL+"/Images/details_close.png' />"+Convert.ToString(fx.GROUP_NO) + "</nobr>" : "<span class='"+ fx.ParentId +" child' ></span>",
                                fx.ParentId == 0 ? Convert.ToString(fx.FixtureName) : "",
                                Convert.ToString(fx.QtyofFixture),
                                Convert.ToString(fx.DescriptionofItem),
                                Convert.ToString(fx.Category),
                                Convert.ToString(fx.Material),
                                Convert.ToString(fx.MaterialType),
                                Convert.ToString(fx.LengthOD),
                                Convert.ToString(fx.WidthOD),
                                Convert.ToString(fx.Thickness),
                                Convert.ToString(fx.Qty),
                                Convert.ToString(fx.Wt),
                                Convert.ToString(fx.Area),
                                Convert.ToString(fx.Unit),
                                Convert.ToString(fx.ReUse ? "Yes" : "No"),
                                Convert.ToString(fx.ReqArea),
                                Convert.ToString(fx.Unit2),
                                Convert.ToString(fx.ReqWt),
                                Convert.ToString(fx.FixRequiredDate),
                                Convert.ToString(fx.MaterialReqDate),
                                Convert.ToString(fx.LineId),
                                Convert.ToString(fx.HeaderId),
                                Convert.ToString(fx.ParentId),
                                }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpPost]
        public ActionResult GetFixtureHeaderLinesHtml(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            FXR001 objFXR001 = db.FXR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objFXR001.ApprovedBy != null)
            {
                if (objFXR001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                {
                    ViewBag.IsDisplayMode = true;
                }
                else
                {
                    ViewBag.IsDisplayMode = false;
                }
            }
            else
            {
                ViewBag.IsDisplayMode = true;
            }
            var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderId && x.DocumentNo == objFXR001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
            ViewBag.PlanningDinID = PlanningDinID;

            ViewBag.FXRStatus = db.FXR001.Where(x => x.HeaderId == HeaderId).Select(x => x.Status).FirstOrDefault();
            return PartialView("_GetHeaderLinesHtml");
        }

        [HttpPost]
        public JsonResult LoadFixtureHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue() + "') and ApprovedBy in('" + objClsLoginInfo.UserName + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "FXR001.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "FXR001.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_FIXTURES_HEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                  Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),

                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                               "<nobr>" + "<a title='View' href='"+WebsiteURL+"/PLN/ApproveFXR/FixtureLineDetails?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i class='iconspace fa fa-eye'></i></a> <a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainFXR/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='cursor:pointer;' class='iconspace fa fa-clock-o'></i></a>" + " " +(uc.RevNo > 0 ? HTMLActionString(uc.HeaderId,"","History","History Record","iconspace fa fa-history","ViewHistoryData('"+ Convert.ToInt32(uc.HeaderId) +"');") : HTMLActionString(uc.HeaderId,"","History","History Record","disabledicon fa fa-history","ViewHistoryData('"+ Convert.ToInt32(uc.HeaderId) +"');")) + " " + HTMLActionString(uc.HeaderId,"","Summary","Summary Record","iconspace fa fa-list-alt","ViewProjectSummary('"+ Convert.ToInt32(uc.HeaderId) +"');") + " " + HTMLActionString(uc.HeaderId,"","Print Report","Print Report","fa fa-print iconspace","PrintReport("+uc.HeaderId+");") + "</nobr>"
                               //"<center><a title='View' href='/PLN/ApproveFXR/FixtureLineDetails?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>&nbsp;&nbsp;<button class='btn btn-xs' onclick=ViewProjectSummary('"+ Convert.ToInt32(uc.HeaderId) +"')><i style='margin-left:5px;' class='fa fa-list-alt'></i></button><i style='margin-left:5px;cursor:pointer;' title='Print Report' class='fa fa-print' onClick='PrintReport("+uc.HeaderId+")'></i></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult LoadFXRHeaderLineData(JQueryDataTableParamModel param)
        {
            try
            {
                string[] arrayCTQStatus = { clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue() };
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int fxrHeaderId = Convert.ToInt32(param.CTQHeaderId);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strWhere = "1=1 and HeaderId In(" + fxrHeaderId + ")";
                FXR001 objFXR001 = db.FXR001.FirstOrDefault(x => x.HeaderId == fxrHeaderId);
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch +
                               "%' or Document like '%" + param.sSearch +
                               "%' or RevNo like '%" + param.sSearch +
                               "%' or FixtureName like '%" + param.sSearch +
                               "%' or DescriptionofItem like '%" + param.sSearch +
                               "%' or Category like '%" + param.sSearch +
                               "%' or Material like '%" + param.sSearch +
                               "%' or MaterialType like '%" + param.sSearch +
                               "%' or StructuralType like '%" + param.sSearch +
                               "%' or PipeNormalBore like '%" + param.sSearch + "%')";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_FIXTURE_HEADER_LINES
                                (
                               fxrHeaderId,
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from fx in lstResult
                            select new[]
                            {
                                "",
                                Convert.ToString(fx.ROW_NO),
                                fx.ParentId == 0 ? "<nobr><img onclick='ExpandCollapsChild("+ fx.LineId +", this)' src='"+WebsiteURL+"/Images/details_close.png' />"+Convert.ToString(fx.GROUP_NO) + "</nobr>" : "<span class='"+ fx.ParentId +" child' ></span>",
                                fx.ParentId == 0 ? Convert.ToString(fx.FixtureName) : "",
                                Convert.ToString(fx.QtyofFixture),
                                Convert.ToString(fx.DescriptionofItem),
                                Convert.ToString(fx.Category),
                                Convert.ToString(fx.Material),
                                Convert.ToString(fx.MaterialType),
                                Convert.ToString(fx.LengthOD),
                                Convert.ToString(fx.WidthOD),
                                Convert.ToString(fx.Thickness),
                                Convert.ToString(fx.Qty),
                                Convert.ToString(fx.Wt),
                                Convert.ToString(fx.Area),
                                Convert.ToString(fx.Unit),
                                Convert.ToString(fx.ReUse ? "Yes" : "No"),
                                Convert.ToString(fx.ReqArea),
                                Convert.ToString(fx.Unit2),
                                Convert.ToString(fx.ReqWt),
                                fx.FixRequiredDate.HasValue ? Convert.ToString(fx.FixRequiredDate.Value.ToShortDateString()) : "",
                                fx.MaterialReqDate.HasValue ? Convert.ToString(fx.MaterialReqDate.Value.ToShortDateString()) : "",
                                Convert.ToString(fx.LineId),
                                Convert.ToString(fx.HeaderId),
                                Convert.ToString(fx.ParentId),
                                Convert.ToString(fx.FixtureName),
                                Convert.ToString(fx.IsManual),
                                fx.ParentId == 0 ? FindChildCount(fx.FixtureName, fx.HeaderId) > 1 ? "true" : "false" : "false",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            for (int i = 0; i < arrayHeaderIds.Length; i++)
            {
                int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg ApproveHeaderByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (HeaderId > 0)
                {
                    FXR001 objFXR001 = db.FXR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objFXR001 != null)
                    {
                        db.SP_PLN_FIXTURE_APPROVE(objFXR001.HeaderId, objClsLoginInfo.UserName);
                        int newId = db.FXR001_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                        Manager.UpdatePDN002(objFXR001.HeaderId, clsImplementationEnum.CommonStatus.Approved.ToString(), objFXR001.RevNo, objFXR001.Project, objFXR001.Document, newId);
                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objFXR001.Project, "", "", Manager.GetPDINDocumentNotificationMsg(objFXR001.Project, clsImplementationEnum.PlanList.Fixture.GetStringValue(), objFXR001.RevNo.Value.ToString(), objFXR001.Status), clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(), Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Fixture.GetStringValue(), objFXR001.HeaderId.ToString(), false), objFXR001.CreatedBy);
                        #endregion

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Approve.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        [HttpPost]
        public ActionResult ReturnLines(int HeaderId, string ReturnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.FixtureStatus.SentForApproval.GetStringValue().ToUpper();
                List<FXR002> lstFXR002 = db.FXR002.Where(x => x.HeaderId == HeaderId).ToList();
                if (lstFXR002 != null && lstFXR002.Count > 0)
                {
                    foreach (var objData in lstFXR002)
                    {
                        FXR002 objFXR002 = lstFXR002.Where(x => x.LineId == objData.LineId).FirstOrDefault();
                        objFXR002.EditedBy = objClsLoginInfo.UserName;
                        objFXR002.EditedOn = DateTime.Now;
                    }
                    FXR001 objFXR001 = db.FXR001.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                    objFXR001.Status = clsImplementationEnum.FixtureStatus.Returned.GetStringValue();
                    objFXR001.ReturnRemark = ReturnRemarks;
                    objFXR001.EditedBy = objClsLoginInfo.UserName;
                    objFXR001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objFXR001.HeaderId, objFXR001.Status, objFXR001.RevNo, objFXR001.Project, objFXR001.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objFXR001.Project, "", "", Manager.GetPDINDocumentNotificationMsg(objFXR001.Project, clsImplementationEnum.PlanList.Fixture.GetStringValue(), objFXR001.RevNo.Value.ToString(), objFXR001.Status), clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(), Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Fixture.GetStringValue(), objFXR001.HeaderId.ToString(), false), objFXR001.CreatedBy);
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully returned.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please add remarks for returning Lines.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult checkNoOfFixture(string fixtureName, int lineId, int headerId, string action, string project)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();

            int count = db.FXR002.Where(x => x.FixtureName == fixtureName && x.Project == project).Count();
            if (count >= 10)
            {
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = "You can add maximum 10 fixture's";
            }
            else
            {
                objResponseMsgWithStatus.Key = true;
            }
            var data = new { status = objResponseMsgWithStatus, lineId = lineId, headerId = headerId, action = action };
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetFXRLinesSubCategroyPartial(int lineId, int headerId, string action)
        {
            FXR002 objFXR002 = null;
            if (lineId > 0 && headerId > 0)
            {
                ViewBag.StrAction = action;
                if (action == "add")
                {
                    objFXR002 = new FXR002();
                    FXR002 masterLine = db.FXR002.Where(x => x.LineId == lineId && x.HeaderId == headerId).FirstOrDefault();
                    if (masterLine != null)
                    {
                        objFXR002.LineId = masterLine.LineId;
                        objFXR002.HeaderId = masterLine.HeaderId;
                        objFXR002.Project = masterLine.Project;
                        objFXR002.Document = masterLine.Document;
                        objFXR002.RevNo = masterLine.RevNo;
                        objFXR002.FixtureName = masterLine.FixtureName;
                        ViewBag.RevNo = objFXR002.RevNo;
                    }
                }
                else if (action == "edit")
                {
                    objFXR002 = db.FXR002.Where(x => x.LineId == lineId && x.HeaderId == headerId).FirstOrDefault();
                    ViewBag.Category = objFXR002.Category;
                    ViewBag.Material = objFXR002.Material;
                    ViewBag.MaterialType = objFXR002.MaterialType;
                    ViewBag.Unit = objFXR002.Unit;
                    ViewBag.Unit2 = objFXR002.Unit2;
                    ViewBag.StructuralType = objFXR002.StructuralType;
                    ViewBag.PipeNormalBore = objFXR002.PipeNormalBore;
                    ViewBag.PipeSchedule = objFXR002.PipeSchedule;
                    ViewBag.RevNo = objFXR002.RevNo;
                }
                var QtyFXR = getFXRData(headerId.ToString(), lineId.ToString());
                if (QtyFXR.QtyofFixture != "")
                { objFXR002.QtyofFixture = Int32.Parse(QtyFXR.QtyofFixture); }
                if (QtyFXR.FixRequiredDate.HasValue)
                { objFXR002.FixRequiredDate = QtyFXR.FixRequiredDate; }
                if (QtyFXR.MaterialReqDate.HasValue)
                { objFXR002.MaterialReqDate = QtyFXR.MaterialReqDate; }

                List<string> lstFXRStatus = clsImplementationEnum.GetFixtureStatus().ToList();
                ViewBag.lstFXRStatus = lstFXRStatus.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstFXRCategory = clsImplementationEnum.GetFXRCategory().ToList();
                ViewBag.lstFXRCategory = lstFXRCategory.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstFXRMaterialType = clsImplementationEnum.GetFXRMaterialType().ToList();
                ViewBag.lstFXRMaterialType = lstFXRMaterialType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                //List<string> lstFXRUnit = clsImplementationEnum.GetFXRUnit().ToList();
                //ViewBag.lstFXRUnit = lstFXRUnit.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                //List<string> lstFXRUnit2 = clsImplementationEnum.GetFXRUnit2().ToList();
                //ViewBag.lstFXRUnit2 = lstFXRUnit2.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstFXRStructuralType = clsImplementationEnum.GetFXRStructuralType().ToList();
                ViewBag.lstFXRStructuralType = lstFXRStructuralType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstBore = db.FXR005.Select(x => x.Bore).Distinct().ToList();
                ViewBag.lstBore = lstBore.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

                List<string> lstSchedule = db.FXR005.Select(x => x.Schedule).Distinct().ToList();
                ViewBag.lstSchedule = lstSchedule.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            }

            return PartialView("_FXRLinesSubCategoryPartial", objFXR002);
        }

        [HttpPost]
        public ActionResult SaveSubCategory(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId"]);
                    string project = fc["Project"];

                    FXR001 objFXR001 = db.FXR001.Where(x => x.HeaderId == headerId && x.Project == project).FirstOrDefault();
                    FXR002 objFXR002 = null;
                    if (fc["strAction"] == "add")
                    {
                        string fixturename = fc["FixtureName"];
                        int parentFXRId = db.FXR002.Where(x => x.HeaderId == headerId && x.FixtureName == fixturename && x.ParentId == 0).Select(x => x.LineId).FirstOrDefault();
                        objFXR002 = new FXR002();
                        objFXR002.ParentId = parentFXRId;
                    }
                    else
                    {
                        int lineId = Convert.ToInt32(fc["LineId"]);
                        objFXR002 = db.FXR002.Where(x => x.LineId == lineId && x.HeaderId == headerId).FirstOrDefault();
                    }
                    var str = fc["RevNo"].ToString();
                    int revNo = Convert.ToInt32(str.Substring(1, str.Length - 1));

                    objFXR002.LineId = Convert.ToInt32(fc["LineId"]);
                    objFXR002.HeaderId = Convert.ToInt32(fc["HeaderId"]);
                    objFXR002.Project = fc["Project"];
                    objFXR002.Document = fc["Document"];
                    objFXR002.RevNo = revNo;
                    objFXR002.FixtureName = fc["FixtureName"];
                    objFXR002.QtyofFixture = Convert.ToInt32(fc["QtyofFixture"]);
                    objFXR002.DescriptionofItem = fc["DescriptionofItem"];
                    objFXR002.Category = fc["Category"];
                    objFXR002.Material = fc["Material"];
                    objFXR002.MaterialType = fc["MaterialType"];
                    objFXR002.LengthOD = fc["LengthOD"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["LengthOD"], CultureInfo.InvariantCulture);
                    objFXR002.WidthOD = fc["WidthOD"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["WidthOD"], CultureInfo.InvariantCulture);
                    objFXR002.Thickness = fc["Thickness"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Thickness"], CultureInfo.InvariantCulture);
                    objFXR002.Qty = fc["Qty"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Qty"], CultureInfo.InvariantCulture);
                    objFXR002.Wt = fc["Wt"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Wt"], CultureInfo.InvariantCulture);
                    objFXR002.Area = fc["Area"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["Area"], CultureInfo.InvariantCulture);
                    objFXR002.Unit = fc["Unit"];
                    objFXR002.ReqWt = fc["ReqWt"] == "" ? Decimal.Parse("0") : Convert.ToDecimal(fc["ReqWt"], CultureInfo.InvariantCulture);
                    objFXR002.Unit2 = fc["Unit2"];
                    string FxrReqDt = fc["FixRequiredDate"] != null ? fc["FixRequiredDate"].ToString() : "";
                    objFXR002.FixRequiredDate = DateTime.ParseExact(FxrReqDt, @"d/M/yyyy", CultureInfo.InvariantCulture);
                    string FxrMatDt = fc["MaterialReqDate"] != null ? fc["MaterialReqDate"].ToString() : "";
                    objFXR002.MaterialReqDate = DateTime.ParseExact(FxrMatDt, @"d/M/yyyy", CultureInfo.InvariantCulture);
                    objFXR002.ReqArea = (fc["ReqArea"] == "" || fc["ReqArea"] == null) ? Decimal.Parse("0") : Convert.ToDecimal(fc["ReqArea"], CultureInfo.InvariantCulture);
                    //if (fc["FixRequiredDate"] == "")
                    //{
                    //    objFXR002.FixRequiredDate = null;
                    //}
                    //else
                    //{
                    //    objFXR002.FixRequiredDate = Convert.ToDateTime(fc["FixRequiredDate"]);
                    //}
                    //if (fc["MaterialReqDate"] == "")
                    //{
                    //    objFXR002.MaterialReqDate = null;
                    //}
                    //else
                    //{
                    //    objFXR002.MaterialReqDate = Convert.ToDateTime(fc["MaterialReqDate"]);
                    //}
                    objFXR002.StructuralType = fc["StructuralType"] == "" ? null : fc["StructuralType"];
                    objFXR002.Size = Convert.ToString(fc["Size"]);
                    objFXR002.PipeNormalBore = fc["PipeNormalBore"] == "" ? null : fc["PipeNormalBore"];
                    objFXR002.PipeSchedule = fc["PipeSchedule"] == "" ? null : fc["PipeSchedule"];
                    var tempReUse = Convert.ToString(fc["txtReUse"]);
                    if (tempReUse == "True")
                    {
                        objFXR002.ReUse = true;
                    }
                    else
                    {
                        objFXR002.ReUse = false;
                    }


                    if (fc["strAction"] == "add")
                    {
                        objFXR002.CreatedBy = objClsLoginInfo.UserName;
                        objFXR002.CreatedOn = DateTime.Now;
                        db.FXR002.Add(objFXR002);
                        objResponseMsgWithStatus.Value = "Fixture's saved successfully";
                    }
                    else
                    {
                        objFXR002.EditedBy = objClsLoginInfo.UserName;
                        objFXR002.EditedOn = DateTime.Now;
                        objResponseMsgWithStatus.Value = "Fixture's update successfully";
                    }

                    objResponseMsgWithStatus.HeaderId = Convert.ToInt32(fc["HeaderId"]);
                    db.SaveChanges();

                    objResponseMsgWithStatus.Key = true;
                    objResponseMsgWithStatus.RevNo = Convert.ToString(objFXR001.RevNo);
                    objResponseMsgWithStatus.HeaderStatus = objFXR001.Status;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsgWithStatus.Key = false;
                objResponseMsgWithStatus.Value = ex.Message;
            }
            return Json(objResponseMsgWithStatus);
        }

        [HttpPost]
        public clsHelper.ResponseMsgWithStatus getFXRData(string headerID, string lineID)
        {
            clsHelper.ResponseMsgWithStatus obj = new clsHelper.ResponseMsgWithStatus();
            int LineID = Int32.Parse(lineID);
            int HeaderID = Int32.Parse(headerID);
            obj.Key = false;
            FXR002 objFXR002 = db.FXR002.Where(x => x.LineId == LineID && x.HeaderId == HeaderID).FirstOrDefault();
            if (objFXR002 != null)
            {
                string fixtureName = objFXR002.FixtureName;
                string project = objFXR002.Project;
                FXR002 obj1FXR002 = db.FXR002.Where(x => x.FixtureName == fixtureName && x.Project == project && x.QtyofFixture != null).FirstOrDefault();
                if (obj1FXR002 != null)
                {
                    obj.Key = true;
                    obj.QtyofFixture = obj1FXR002.QtyofFixture.ToString();
                    obj.FixRequiredDate = obj1FXR002.FixRequiredDate;
                    obj.MaterialReqDate = obj1FXR002.MaterialReqDate;
                }
                else
                {
                    obj.QtyofFixture = "";
                }
            }

            return obj;
        }

        [HttpPost]
        public ActionResult GetProjectSummary(int HeaderId)
        {
            //List<FXR002> objFXR002 = db.FXR002.Where(x => x.HeaderId == HeaderId).OrderBy(x => x.Category).ToList();
            //var model = from c in objFXR002
            //            group c by new
            //            {
            //                c.Category,
            //                c.Material,
            //                c.MaterialType,
            //                c.Thickness,
            //                c.LengthOD,
            //                c.WidthOD,
            //            } into gcs
            //            select new FXR002()
            //            {
            //                Category = gcs.Key.Category,
            //                Material = gcs.Key.Material,
            //                MaterialType = gcs.Key.MaterialType,
            //                Thickness = gcs.Key.Thickness,
            //                LengthOD = gcs.Key.LengthOD,
            //                WidthOD = gcs.Key.WidthOD,
            //                Wt = gcs.Sum(x => x.Wt),
            //                Qty = gcs.Sum(x => x.Qty)
            //            };
            //ViewBag.TotalWeight = objFXR002.Sum(x => x.Wt);

            var list = db.SP_FETCH_FIXTURES_SUMMARY_SHEET(HeaderId).ToList();
            if (list.Count > 0)
            {
                ViewBag.TotalWeight = list.Sum(x => x.Weight);
            }
            ViewBag.HeaderId = HeaderId;

            return PartialView("_ProjectSummary", list);
        }

        [HttpPost]
        public JsonResult DeleteFixture(string fixtureName, int lineId, string project)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsgWithStatus = new clsHelper.ResponseMsgWithStatus();

            int count = db.FXR002.Where(x => x.FixtureName == fixtureName && x.Project == project).Count();

            if (count <= 1)
            {
                FXR002 objFXR002 = db.FXR002.Where(x => x.LineId == lineId).FirstOrDefault();
                if (objFXR002 != null)
                {
                    objResponseMsgWithStatus.HeaderId = objFXR002.HeaderId;
                    db.FXR002.Remove(objFXR002);
                    db.SaveChanges();
                    objResponseMsgWithStatus.Key = true;
                    objResponseMsgWithStatus.Value = "Fixture name deleted successfully.";
                }
            }
            else
            {
                FXR002 objFXR002 = db.FXR002.Where(x => x.LineId == lineId).FirstOrDefault();
                if (objFXR002.ParentId == 0)
                {
                    objResponseMsgWithStatus.Key = false;
                    objResponseMsgWithStatus.Value = "You can not delete parent record...!";
                }
                else
                {
                    if (objFXR002 != null)
                    {
                        objResponseMsgWithStatus.HeaderId = objFXR002.HeaderId;
                        db.FXR002.Remove(objFXR002);
                        db.SaveChanges();
                        objResponseMsgWithStatus.Key = true;
                        objResponseMsgWithStatus.Value = "Fixture name deleted successfully.";
                    }
                }
            }
            return Json(objResponseMsgWithStatus, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult GetMaterialFromCategory(int Id)
        {
            List<BULocWiseCategoryModel> lstMaterial = new List<BULocWiseCategoryModel>();
            if (Id == 1)
            {
                var data = from q in db.FXR006.ToList()
                           select new { name = q.Type + "-" + q.Size };

                lstMaterial = data.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.name, CatID = x.name }).ToList();
            }
            else if (Id == 2)
            {
                var data = from q in db.FXR005.ToList()
                           select new { name = q.Bore + "-" + q.Schedule };

                lstMaterial = data.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.name, CatID = x.name }).ToList();
            }
            return Json(lstMaterial, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDensityValue(string name)
        {
            var density = db.FXR003.Where(x => x.MaterialType == name).FirstOrDefault().Density;
            return Json(density, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetWeightValue(string material, string type)
        {
            var mType = material.Split('-')[0];
            var mSize = material.Split('-')[1];
            decimal weight = 0;
            if (type.ToLower() == "structural")
            {
                weight = Convert.ToDecimal(db.FXR006.Where(x => x.Type == mType && x.Size == mSize).FirstOrDefault().Weight);
            }
            else
            {
                weight = Convert.ToDecimal(db.FXR005.Where(x => x.Bore == mType && x.Schedule == mSize).FirstOrDefault().Weight);
            }

            return Json(weight, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Fixture.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Fixture, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_FIXTURES_HEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      RevNo = "R" + li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn.HasValue ? Convert.ToString(li.CreatedOn) : "",
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn.HasValue ? Convert.ToString(li.EditedOn) : "",
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn.HasValue ? Convert.ToString(li.SubmittedOn) : "",
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn.HasValue ? Convert.ToString(li.ApprovedOn) : "",
                                      ReturnRemark = li.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_FETCH_FIXTURE_HEADER_LINES(0, 1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      FixtureName = li.FixtureName,
                                      QtyofFixture = li.QtyofFixture,
                                      DescriptionofItem = li.DescriptionofItem,
                                      Category = li.Category,
                                      Material = li.Material,
                                      MaterialType = li.MaterialType,
                                      LengthOD = li.LengthOD,
                                      WidthOD = li.WidthOD,
                                      Thickness = li.Thickness,
                                      Qty = li.Qty,
                                      Wt = li.Wt,
                                      Area = li.Area,
                                      Unit = li.Unit,
                                      ReUse = li.ReUse ? "Yes" : "No",
                                      ReqArea = li.ReqArea,
                                      Unit2 = li.Unit2,
                                      ReqWt = li.ReqWt,
                                      FixRequiredDate = li.FixRequiredDate.HasValue ? Convert.ToString(li.FixRequiredDate.Value.ToShortDateString()) : "",
                                      MaterialReqDate = li.MaterialReqDate.HasValue ? Convert.ToString(li.MaterialReqDate.Value.ToShortDateString()) : "",
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_FIXTURES_HEADER_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      RevNo = "R" + li.RevNo,
                                      Status = li.Status,
                                      CDD = li.CDD,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn.HasValue ? Convert.ToString(li.CreatedOn) : "",
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn.HasValue ? Convert.ToString(li.EditedOn) : "",
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn.HasValue ? Convert.ToString(li.SubmittedOn) : "",
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn.HasValue ? Convert.ToString(li.ApprovedOn) : "",
                                      ReturnRemark = li.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_FETCH_FIXTURE_HEADER_LINES_HISTORY(0, 1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      FixtureName = li.FixtureName,
                                      QtyofFixture = li.QtyofFixture,
                                      DescriptionofItem = li.DescriptionofItem,
                                      Category = li.Category,
                                      Material = li.Material,
                                      MaterialType = li.MaterialType,
                                      LengthOD = li.LengthOD,
                                      WidthOD = li.WidthOD,
                                      Thickness = li.Thickness,
                                      Qty = li.Qty,
                                      Wt = li.Wt,
                                      Area = li.Area,
                                      Unit = li.Unit,
                                      ReUse = li.ReUse ? "Yes" : "No",
                                      ReqArea = li.ReqArea,
                                      Unit2 = li.Unit2,
                                      ReqWt = li.ReqWt,
                                      FixRequiredDate = li.FixRequiredDate.HasValue ? Convert.ToString(li.FixRequiredDate.Value.ToShortDateString()) : "",
                                      MaterialReqDate = li.MaterialReqDate.HasValue ? Convert.ToString(li.MaterialReqDate.Value.ToShortDateString()) : "",
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}