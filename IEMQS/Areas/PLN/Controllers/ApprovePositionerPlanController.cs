﻿using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PLN.Controllers
{
    public class ApprovePositionerPlanController : clsBase
    {
        //modified by nikita on 29/08/2017
        //added method updatepdn002
        //task assigned by Satish Pawar
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadPositionerPlanListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_PositionerPlanListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadPositionerPlanHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue() + "') and pln013.ApprovedBy in('" + objClsLoginInfo.UserName + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
               // strWhere += " and pln013.ApprovedBy in('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln013.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln013.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_POSITIONERPLAN_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               uc.CreatedOn.HasValue ?  Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.EditedBy),
                               uc.EditedOn.HasValue ?  Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.SubmittedBy),
                               uc.SubmittedOn.HasValue ?  Convert.ToString(uc.SubmittedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ApprovedBy),
                               uc.ApprovedOn.HasValue ?  Convert.ToString(uc.ApprovedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ReturnRemark),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.HeaderId),
                               // "<center><a title='View' href='/PLN/ApprovePositionerPlan/ApprovePositionerPlan?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ApprovePositionerPlan(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN013");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderID).FirstOrDefault();               
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN013.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadPositionerPlanCreateFormPartial(int HeaderID = 0)
        {
            PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN013 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN013.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                if (objPLN013.ApprovedBy != null)
                {
                    if (objPLN013.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
                objPLN013.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN013.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN013.ApprovedBy = objPLN013.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN013.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN013.Customer = objPLN013.Customer + " - " + customerName;
                string strPositionerPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strPositionerPlan &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    PLN014 objPLN014 = db.PLN014.Where(x => x.HeaderId == objPLN013.HeaderId).FirstOrDefault();
                    if (objPLN014 != null)
                    {
                        if (latestRecord.RevNo == objPLN014.PlanRevNo)
                        {
                            ViewBag.isLatest = "true";
                        }
                        else
                        {
                            ViewBag.isLatest = "false";
                        }
                    }
                    else
                    {
                        ViewBag.isLatest = "false";
                    }
                }
                else
                {
                    ViewBag.isLatest = "true";
                }
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN013.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, HeaderID, clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue());

                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN013/" + objPLN013.HeaderId + "/" + objPLN013.RevNo));

            }
            else
            {
                objPLN013 = new PLN013();
                objPLN013.ProcessPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                objPLN013.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN013.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadPositionerPlanCreateFormPartial", objPLN013);
        }

        [HttpPost]
        public JsonResult LoadCheckListData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string strPositionerPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strPositionerPlan &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln014.HeaderId = " + HeaderId + " and ";
                    PLN014 objPLN014 = db.PLN014.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN014 != null)
                    {
                        PlanRev = objPLN014.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]", "pln002.CreatedBy +' - '+com003.t_name", "pln002.EditedBy +' - '+com003.t_name" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_POSITIONERPLAN_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,false,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,false),
                                Convert.ToString(uc.CreatedBy),
                                uc.CreatedOn.HasValue ?  Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.EditedBy),
                               uc.EditedOn.HasValue ?  Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string LineOperation(bool flag, int LineId, int HeaderId)
        {
            if (flag)
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" checked=\"checked\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
            else
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
        }


        [HttpPost]
        public ActionResult ReturnPositionerPlan(int HeaderId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN013 != null)
                {
                    objPLN013.Status = clsImplementationEnum.PlanStatus.Returned.GetStringValue();
                    objPLN013.ReturnRemark = Remarks;
                    objPLN013.EditedBy = objClsLoginInfo.UserName;
                    objPLN013.EditedOn = DateTime.Now;




                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully returned.";
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN013.HeaderId, objPLN013.Status, objPLN013.RevNo, objPLN013.Project, objPLN013.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN013.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN013.Project, clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue(), objPLN013.RevNo.Value.ToString(), objPLN013.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue(), objPLN013.HeaderId.ToString(), false),
                                                        objPLN013.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please add remarks for returning Checklist.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            for (int i = 0; i < arrayHeaderIds.Length; i++)
            {
                int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg ApproveHeaderByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN013 != null)
                {
                    objPLN013.Status = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                    objPLN013.ApprovedBy = objClsLoginInfo.UserName;
                    objPLN013.ApprovedOn = DateTime.Now;




                    #region Header Log
                    var lstPLN013_Log = db.PLN013_Log.Where(x => x.HeaderId == objPLN013.HeaderId).ToList();
                    PLN013_Log objPLN013_Log = new PLN013_Log();
                    if (objPLN013_Log != null)
                    {
                        objPLN013_Log = lstPLN013_Log.FirstOrDefault();
                        lstPLN013_Log.ForEach(i => { i.Status = clsImplementationEnum.PlanStatus.Superseded.GetStringValue(); });
                    }

                    objPLN013_Log = db.PLN013_Log.Add(new PLN013_Log
                    {
                        HeaderId = objPLN013.HeaderId,
                        Project = objPLN013.Project,
                        Document = objPLN013.Document,
                        Customer = objPLN013.Customer,
                        RevNo = objPLN013.RevNo,
                        Status = objPLN013.Status,
                        CDD = objPLN013.CDD,
                        Product = objPLN013.Product,
                        ProcessLicensor = objPLN013.ProcessLicensor,
                        ProcessPlan = objPLN013.ProcessPlan,
                        Attachment = objPLN013.Attachment,
                        JobNo = objPLN013.JobNo,
                        CreatedBy = objPLN013.CreatedBy,
                        CreatedOn = objPLN013.CreatedOn,
                        EditedBy = objPLN013.EditedBy,
                        EditedOn = objPLN013.EditedOn,
                        ApprovedBy = objPLN013.ApprovedBy,
                        ApprovedOn = objPLN013.ApprovedOn,
                        ReturnRemark = objPLN013.ReturnRemark,
                        ReviseRemark = objPLN013.ReviseRemark,
                        SubmittedBy = objPLN013.SubmittedBy,
                        SubmittedOn = objPLN013.SubmittedOn,
                    });

                    #endregion
                    db.SaveChanges();
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    int newId = db.PLN013_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objPLN013.HeaderId, objPLN013.Status, objPLN013.RevNo, objPLN013.Project, objPLN013.Document, newId);
                    List<PLN014> lstPLN014 = db.PLN014.Where(x => x.HeaderId == HeaderId).ToList();
                    List<PLN014_Log> lstPLN014_Log = new List<PLN014_Log>();
                    if (lstPLN014 != null && lstPLN014.Count > 0)
                    {
                        lstPLN014_Log = lstPLN014.Select(x => new PLN014_Log()
                        {
                            RefId = objPLN013_Log.Id,
                            LineId = x.LineId,
                            HeaderId = x.HeaderId,
                            Project = x.Project,
                            Document = x.Document,
                            RevNo = x.RevNo,
                            Plan = x.Plan,
                            PlanRevNo = x.PlanRevNo,
                            CheckListId = x.CheckListId,
                            Yes_No = x.Yes_No,
                            Remarks = x.Remarks,
                            CreatedBy = x.CreatedBy,
                            CreatedOn = x.CreatedOn,
                            EditedBy = x.EditedBy,
                            EditedOn = x.EditedOn
                        }).ToList();
                        db.PLN014_Log.AddRange(lstPLN014_Log);
                        db.SaveChanges();
                    }

                    int? currentRevisionNull = db.PLN013.Where(q => q.HeaderId == HeaderId).FirstOrDefault().RevNo;
                    int currentRevision = ((currentRevisionNull == null) ? 0 : currentRevisionNull.Value);
                    //clsUpload.CopyFolderContents("PLN013/" + HeaderId + "/" + currentRevision, "PLN013/" + HeaderId + "/" + (currentRevision + 1));
                    var oldfolderPath = "PLN013//" + HeaderId + "//" + currentRevision;
                    var newfolderPath = "PLN013//" + HeaderId + "//" + (currentRevision + 1);
                    //(new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);

                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(oldfolderPath, newfolderPath, oldfolderPath, HeaderId, newfolderPath, HeaderId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully approved.";

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN013.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN013.Project, clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue(), objPLN013.RevNo.Value.ToString(), objPLN013.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue(), objPLN013.HeaderId.ToString(), false),
                                                        objPLN013.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Checklist not available approved.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN013_Log objPLN013_Log = db.PLN013_Log.Where(x => x.Id == Id && x.ApprovedBy == currentUser).FirstOrDefault();
                if (objPLN013_Log == null)
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadPositionerPlanHistoryFormPartial(int Id = 0)
        {
            PLN013_Log objPLN013_Log = db.PLN013_Log.Where(x => x.Id == Id).FirstOrDefault();
            PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == Id).FirstOrDefault();
            if (objPLN013_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN013_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN013_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN013_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN013_Log.ApprovedBy = objPLN013_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN013_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN013_Log.Customer = objPLN013_Log.Customer + " - " + customerName;
                string strPositionerPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strPositionerPlan &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    PLN014 objPLN014 = db.PLN014.Where(x => x.HeaderId == objPLN013.HeaderId).FirstOrDefault();
                    if (objPLN014 != null)
                    {
                        if (latestRecord.RevNo == objPLN014.PlanRevNo)
                        {
                            ViewBag.isLatest = "true";
                        }
                        else
                        {
                            ViewBag.isLatest = "false";
                        }
                    }
                    else
                    {
                        ViewBag.isLatest = "false";
                    }
                }
                else
                {
                    ViewBag.isLatest = "true";
                }
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN013/" + objPLN013_Log.HeaderId + "/" + objPLN013_Log.RevNo));
            }
            else
            {
                objPLN013_Log = new PLN013_Log();
                objPLN013_Log.ProcessPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                objPLN013_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN013_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            if (objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.SHOP.GetStringValue()))
            {
               
                ViewBag.Isshopuser = true;
            }
            else
            {
                ViewBag.Isshopuser = false;
            }
            return PartialView("_LoadPositionerPlanHistoryFormPartial", objPLN013_Log);
        }

        [HttpPost]
        public JsonResult LoadCheckListHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int Id = Convert.ToInt32(param.CTQHeaderId);
                PLN013_Log objPLN013_Log = db.PLN013_Log.Where(x => x.Id == Id).FirstOrDefault();

                strWhere += "1=1 and pln014.RefId = " + objPLN013_Log.Id + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]", "pln014.CreatedBy +' - '+com003.t_name", "pln014.EditedBy +' - '+com003.t_name" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_POSITIONERPLAN_HEADER_LINE_HISTORY
                                (
                                     objPLN013_Log.HeaderId,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,true,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,true),
                                Convert.ToString(uc.CreatedBy),
                                uc.CreatedOn.HasValue ?  Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.EditedBy),
                               uc.EditedOn.HasValue ?  Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        /// Created By :Deepak tiwari
        /// Created Date :28/08/2017
        /// Description: Modification of add Save functionality when status is send for approval
        /// </summary>


        [HttpPost]
        public ActionResult SaveHeader(PLN013 pln013, string strHeaderId, string strLineId, string strIsLatest)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln013.HeaderId > 0)
                {
                    PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == pln013.HeaderId).FirstOrDefault();
                    if (objPLN013 != null)
                    {
                        objPLN013.Product = pln013.Product;
                        objPLN013.ProcessLicensor = pln013.ProcessLicensor;
                        objPLN013.JobNo = pln013.JobNo;
                        objPLN013.ApprovedBy = pln013.ApprovedBy.Split('-')[0].ToString().Trim(); ; ; ;
                        objPLN013.EditedBy = objClsLoginInfo.UserName;
                        objPLN013.EditedOn = DateTime.Now;
                        if (objPLN013.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN013.RevNo = Convert.ToInt32(objPLN013.RevNo) + 1;
                            objPLN013.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        if (!Convert.ToBoolean(strIsLatest))
                        {
                            List<PLN014> lstPLN014 = db.PLN014.Where(x => x.HeaderId == objPLN013.HeaderId).ToList();
                            if (lstPLN014 != null && lstPLN014.Count > 0)
                            {
                                db.PLN014.RemoveRange(lstPLN014);
                            }
                            string strPositionerPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                            string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                            var latestRecord = db.PLN001
                                     .Where(x
                                                => x.Plan == strPositionerPlan &&
                                                   x.Status == strStatus
                                            )
                                     .OrderByDescending(x => x.HeaderId)
                                     .Select(x => new { x.HeaderId, x.RevNo })
                                     .FirstOrDefault();
                            List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == latestRecord.HeaderId).ToList();
                            if (lstPLN002 != null && lstPLN002.Count > 0)
                            {
                                List<PLN014> NewlstPLN014 = lstPLN002.Select(x => new PLN014()
                                {
                                    LineId = x.LineId,
                                    HeaderId = objPLN013.HeaderId,
                                    Project = objPLN013.Project,
                                    Document = objPLN013.Document,
                                    RevNo = objPLN013.RevNo,
                                    Plan = objPLN013.ProcessPlan,
                                    PlanRevNo = x.RevNo,
                                    CheckListId = x.LineId,
                                    //Yes_No = false,
                                    Yes_No = clsImplementationEnum.ChecklistYesNo.No.GetStringValue(),
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                }).ToList();
                                db.PLN014.AddRange(NewlstPLN014);
                            }
                        }
                        else
                        {
                            List<PLN014> lstPLN014 = db.PLN014.Where(x => x.HeaderId == objPLN013.HeaderId).ToList();
                            if (lstPLN014.Count > 0)
                            {
                                List<PLN014> lstTruePLN014 = new List<PLN014>();

                                foreach (PLN014 objPLN014 in lstPLN014)
                                {
                                    //objPLN014.Yes_No = (!string.IsNullOrWhiteSpace(strLineId) && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Length > 0 && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Contains(objPLN014.LineId) ? true : false);                                    
                                    objPLN014.EditedBy = objClsLoginInfo.UserName;
                                    objPLN014.EditedOn = DateTime.Now;
                                }
                            }
                        }

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN013.HeaderId;
                        objResponseMsg.Status = objPLN013.Status;
                        objResponseMsg.Revision = objPLN013.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN013.HeaderId, objPLN013.Status, objPLN013.RevNo, objPLN013.Project, objPLN013.Document);


                        var folderPath = "PLN013/" + NewHeaderId + "/" + objPLN013.RevNo;


                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult LoadCheckListDataNew(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN013 objPLN013 = db.PLN013.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string HeaderStatus = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                bool IsDisable = true;
                if (objPLN013 != null)
                {
                    HeaderStatus = objPLN013.Status;
                }
                if (HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower() && objPLN013.ApprovedBy==objClsLoginInfo.UserName)
                {
                    IsDisable = false;
                }
                string strPositionerPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strPositionerPlan &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln014.HeaderId = " + HeaderId + " and ";
                    PLN014 objPLN014 = db.PLN014.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN014 != null)
                    {
                        PlanRev = objPLN014.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]", "pln002.CreatedBy +' - '+com003.t_name", "pln002.EditedBy +' - '+com003.t_name" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_POSITIONERPLAN_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                (new PLNCommonFunction()).CheckListLineOperationYesNoNew(uc.Yes_No,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Yes_No","UpdateData(this)"),
                                (new PLNCommonFunction()).CheckListLineOperationRemarksNew(uc.Remarks,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Remarks","UpdateData(this)"),
                                Convert.ToString(uc.CreatedBy),
                                uc.CreatedOn.HasValue ?  Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.EditedBy),
                               uc.EditedOn.HasValue ?  Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                //getChecklistDetails(uc.CheckListId.ToString())[0],
                                //getChecklistDetails(uc.CheckListId.ToString())[1],
                                //getChecklistDetails(uc.CheckListId.ToString())[2],
                                //getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public string[] getChecklistDetails(string lineId)
        {
            string[] checklist = new string[4];
            int LineID = Int32.Parse(lineId);
            var data = (from p2 in db.PLN002
                        join
                            p1 in db.PLN001 on p2.HeaderId equals p1.HeaderId
                        where p2.LineId == LineID
                        select p1).FirstOrDefault();
            if (data != null)
            {
                checklist[0] = Manager.GetUserNameFromPsNo(data.CreatedBy); //prepared by
                checklist[1] = Manager.GetUserNameFromPsNo(data.ApprovedBy); // approved by
                checklist[2] = data.ApprovedOn.ToString(); // approved on
                checklist[3] = data.Document; // document no
            }
            return checklist;
        }

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //fetch Planning din Id by using plan header id and plan
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Positioner_Plan, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        
    }
}