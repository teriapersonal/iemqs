﻿using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PLN.Controllers
{
    public class ApproveSectionHandlingController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadSectionHandlingListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_SectionHandlingListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadSectionHandlingHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue() + "') and pln009.ApprovedBy in('" + objClsLoginInfo.UserName + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln009.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln009.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_FETCH_SECTIONHANDLING_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                 Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                                Convert.ToString(uc.HeaderId)
                               // "<a title='View' href='/PLN/ApproveSectionHandling/ApproveSectionHandling?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+(uc.RevNo>0 ?"<a title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Approver','/PLN/MaintainSectionHandling/GetHistoryDetails','Section Handling')\"><i style='margin-left:5px;' class='fa fa-history'></i></a>":"")+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainSectionHandling/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='margin-left:5px;cursor:pointer;' class='fa fa-clock-o'></i></a>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ApproveSectionHandling(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN009");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
               
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN009.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadSectionHandlingCreateFormPartial(int HeaderID = 0)
        {
            PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN009.ApprovedBy != null)
            {
                if (objPLN009.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                {
                    ViewBag.IsDisplayMode = true;
                }
                else
                {
                    ViewBag.IsDisplayMode = false;
                }
            }
            else
            {
                ViewBag.IsDisplayMode = true;
            }
            if (objPLN009 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN009.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN009.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN009.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN009.ApprovedBy = objPLN009.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN009.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN009.Customer = objPLN009.Customer + " - " + customerName;
                string strSectionHandling = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strSectionHandling &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    PLN010 objPLN010 = db.PLN010.Where(x => x.HeaderId == objPLN009.HeaderId).FirstOrDefault();
                    if (objPLN010 != null)
                    {
                        if (latestRecord.RevNo == objPLN010.PlanRevNo)
                        {
                            ViewBag.isLatest = "true";
                        }
                        else
                        {
                            ViewBag.isLatest = "false";
                        }
                    }
                    else
                    {
                        ViewBag.isLatest = "false";
                    }
                }
                else
                {
                    ViewBag.isLatest = "true";
                }
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN009.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, HeaderID, clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue());
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN009/" + objPLN009.HeaderId + "/" + objPLN009.RevNo));

            }
            else
            {
                objPLN009 = new PLN009();
                objPLN009.ProcessPlan = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                objPLN009.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN009.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadSectionHandlingCreateFormPartial", objPLN009);
        }

        [HttpPost]
        public JsonResult LoadCheckListData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string strSectionHandling = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strSectionHandling &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln010.HeaderId = " + HeaderId + " and ";
                    PLN010 objPLN010 = db.PLN010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN010 != null)
                    {
                        PlanRev = objPLN010.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]", "pln002.CreatedBy +' - '+com003.t_name", "pln002.EditedBy +' - '+com003.t_name" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var lstResult = db.SP_FETCH_SECTIONHANDLING_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     "",
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,false,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,false),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string LineOperation(bool flag, int LineId, int HeaderId)
        {
            if (flag)
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" checked=\"checked\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
            else
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
        }


        [HttpPost]
        public ActionResult ReturnSectionHandling(int HeaderId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN009 != null)
                {
                    objPLN009.Status = clsImplementationEnum.PlanStatus.Returned.GetStringValue();
                    objPLN009.ReturnRemark = Remarks;
                    objPLN009.EditedBy = objClsLoginInfo.UserName;
                    objPLN009.EditedOn = DateTime.Now;



                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully returned.";
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN009.HeaderId, objPLN009.Status, objPLN009.RevNo, objPLN009.Project, objPLN009.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN009.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN009.Project, clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue(), objPLN009.RevNo.Value.ToString(), objPLN009.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue(), objPLN009.HeaderId.ToString(), false),
                                                        objPLN009.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please add remarks for returning Checklist.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            for (int i = 0; i < arrayHeaderIds.Length; i++)
            {
                int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg ApproveHeaderByApprover(int HeaderId)
        {
            // bool hasAttachments, Dictionary< string, string> Attach


            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN009 != null)
                {
                    objPLN009.Status = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                    objPLN009.ApprovedBy = objClsLoginInfo.UserName;
                    objPLN009.ApprovedOn = DateTime.Now;



                    #region Header Log
                    var lstPLN009_Log = db.PLN009_Log.Where(x => x.HeaderId == objPLN009.HeaderId).ToList();
                    PLN009_Log objPLN009_Log = new PLN009_Log();
                    if (objPLN009_Log != null)
                    {
                        objPLN009_Log = lstPLN009_Log.FirstOrDefault();
                        lstPLN009_Log.ForEach(i => { i.Status = clsImplementationEnum.PlanStatus.Superseded.GetStringValue(); });
                    }

                    objPLN009_Log = db.PLN009_Log.Add(new PLN009_Log
                    {
                        HeaderId = objPLN009.HeaderId,
                        Project = objPLN009.Project,
                        Document = objPLN009.Document,
                        Customer = objPLN009.Customer,
                        RevNo = objPLN009.RevNo,
                        Status = objPLN009.Status,
                        CDD = objPLN009.CDD,
                        Product = objPLN009.Product,
                        ProcessLicensor = objPLN009.ProcessLicensor,
                        ProcessPlan = objPLN009.ProcessPlan,
                        Attachment = objPLN009.Attachment,
                        JobNo = objPLN009.JobNo,
                        CreatedBy = objPLN009.CreatedBy,
                        CreatedOn = objPLN009.CreatedOn,
                        EditedBy = objPLN009.EditedBy,
                        EditedOn = objPLN009.EditedOn,
                        ApprovedBy = objPLN009.ApprovedBy,
                        ApprovedOn = objPLN009.ApprovedOn,
                        ReturnRemark = objPLN009.ReturnRemark,
                        ReviseRemark = objPLN009.ReviseRemark,
                        SubmittedBy = objPLN009.SubmittedBy,
                        SubmittedOn = objPLN009.SubmittedOn,

                    });

                    #endregion
                    db.SaveChanges();
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    int newId = db.PLN009_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objPLN009.HeaderId, objPLN009.Status, objPLN009.RevNo, objPLN009.Project, objPLN009.Document, newId);
                    List<PLN010> lstPLN010 = db.PLN010.Where(x => x.HeaderId == HeaderId).ToList();
                    List<PLN010_Log> lstPLN010_Log = new List<PLN010_Log>();
                    if (lstPLN010 != null && lstPLN010.Count > 0)
                    {
                        lstPLN010_Log = lstPLN010.Select(x => new PLN010_Log()
                        {
                            RefId = objPLN009_Log.Id,
                            LineId = x.LineId,
                            HeaderId = x.HeaderId,
                            Project = x.Project,
                            Document = x.Document,
                            RevNo = x.RevNo,
                            Plan = x.Plan,
                            PlanRevNo = x.PlanRevNo,
                            CheckListId = x.CheckListId,
                            Yes_No = x.Yes_No,
                            Remarks = x.Remarks,
                            CreatedBy = x.CreatedBy,
                            CreatedOn = x.CreatedOn,
                            EditedBy = x.EditedBy,
                            EditedOn = x.EditedOn
                        }).ToList();
                        db.PLN010_Log.AddRange(lstPLN010_Log);
                        db.SaveChanges();
                    }

                    int? currentRevisionNull = db.PLN009.Where(q => q.HeaderId == HeaderId).FirstOrDefault().RevNo;
                    int currentRevision = ((currentRevisionNull == null) ? 0 : currentRevisionNull.Value);
                    //clsUpload.CopyFolderContents("PLN009/" + HeaderId + "/" + currentRevision, "PLN009/" + HeaderId + "/" + (currentRevision + 1));
                    var oldfolderPath = "PLN009//" + HeaderId + "//" + currentRevision;
                    var newfolderPath = "PLN009//" + HeaderId + "//" + (currentRevision + 1);
                    //(new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);

                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(oldfolderPath, newfolderPath, oldfolderPath, HeaderId, newfolderPath, HeaderId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully approved.";

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN009.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN009.Project, clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue(), objPLN009.RevNo.Value.ToString(), objPLN009.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue(), objPLN009.HeaderId.ToString(), false),
                                                        objPLN009.CreatedBy);
                    #endregion

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Checklist not available approved.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN009_Log objPLN009_Log = db.PLN009_Log.Where(x => x.Id == Id).FirstOrDefault();
                if (objPLN009_Log == null)
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadSectionHandlingHistoryFormPartial(int Id = 0)
        {
            PLN009_Log objPLN009_Log = db.PLN009_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN009_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN009_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN009_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN009_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN009_Log.ApprovedBy = objPLN009_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN009_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN009_Log.Customer = objPLN009_Log.Customer + " - " + customerName;
                string strSectionHandling = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strSectionHandling &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    //if (latestRecord.HeaderId == objPLN003.HeaderId)
                    //{

                    //}
                }
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN009/" + objPLN009_Log.HeaderId + "/" + objPLN009_Log.RevNo));
            }
            else
            {
                objPLN009_Log = new PLN009_Log();
                objPLN009_Log.ProcessPlan = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                objPLN009_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN009_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            if (objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.SHOP.GetStringValue()))
            {
                ViewBag.Isshopuser = true;
            }
            else
            {
                ViewBag.Isshopuser = false;
            }
            return PartialView("_LoadSectionHandlingHistoryFormPartial", objPLN009_Log);
        }

        [HttpPost]
        public JsonResult LoadCheckListHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int Id = Convert.ToInt32(param.CTQHeaderId);
                PLN009_Log objPLN009_Log = db.PLN009_Log.Where(x => x.Id == Id).FirstOrDefault();

                strWhere += "1=1 and pln010.RefId = " + objPLN009_Log.Id + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]", "pln010.CreatedBy +' - '+com003.t_name", "pln010.EditedBy +' - '+com003.t_name" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                #region Sorting

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstResult = db.SP_FETCH_SECTIONHANDLING_HEADER_LINE_HISTORY
                                (
                                     objPLN009_Log.HeaderId,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,true,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,true),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        // Modification by deepak tiwari


        [HttpPost]
        public ActionResult SaveHeader(PLN009 pln009, string strHeaderId, string strLineId, string strIsLatest)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln009.HeaderId > 0)
                {
                    PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == pln009.HeaderId).FirstOrDefault();
                    if (objPLN009 != null)
                    {
                        //objPLN009.Document = pln009.Document;
                        objPLN009.Product = pln009.Product;
                        objPLN009.ProcessLicensor = pln009.ProcessLicensor;
                        objPLN009.JobNo = pln009.JobNo;
                        objPLN009.ApprovedBy = pln009.ApprovedBy.Split('-')[0].ToString().Trim();
                        objPLN009.EditedBy = objClsLoginInfo.UserName;
                        objPLN009.EditedOn = DateTime.Now;
                        if (objPLN009.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN009.RevNo = Convert.ToInt32(objPLN009.RevNo) + 1;
                            objPLN009.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        if (!Convert.ToBoolean(strIsLatest))
                        {
                            List<PLN010> lstPLN010 = db.PLN010.Where(x => x.HeaderId == objPLN009.HeaderId).ToList();
                            if (lstPLN010 != null && lstPLN010.Count > 0)
                            {
                                db.PLN010.RemoveRange(lstPLN010);
                            }
                            string strSectionHandling = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                            string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                            var latestRecord = db.PLN001
                                     .Where(x
                                                => x.Plan == strSectionHandling &&
                                                   x.Status == strStatus
                                            )
                                     .OrderByDescending(x => x.HeaderId)
                                     .Select(x => new { x.HeaderId, x.RevNo })
                                     .FirstOrDefault();
                            List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == latestRecord.HeaderId).ToList();
                            if (lstPLN002 != null && lstPLN002.Count > 0)
                            {
                                List<PLN010> NewlstPLN010 = lstPLN002.Select(x => new PLN010()
                                {
                                    LineId = x.LineId,
                                    HeaderId = objPLN009.HeaderId,
                                    Project = objPLN009.Project,
                                    Document = objPLN009.Document,
                                    RevNo = objPLN009.RevNo,
                                    Plan = objPLN009.ProcessPlan,
                                    PlanRevNo = x.RevNo,
                                    CheckListId = x.LineId,
                                    //Yes_No = false,
                                    Yes_No = clsImplementationEnum.ChecklistYesNo.No.GetStringValue(),
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                }).ToList();
                                db.PLN010.AddRange(NewlstPLN010);
                            }
                        }
                        else
                        {
                            List<PLN010> lstPLN010 = db.PLN010.Where(x => x.HeaderId == objPLN009.HeaderId).ToList();
                            if (lstPLN010.Count > 0)
                            {
                                List<PLN010> lstTruePLN010 = new List<PLN010>();

                                foreach (PLN010 objPLN010 in lstPLN010)
                                {
                                    //objPLN010.Yes_No = (!string.IsNullOrWhiteSpace(strLineId) && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Length > 0 && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Contains(objPLN010.LineId) ? true : false);
                                    objPLN010.EditedBy = objClsLoginInfo.UserName;
                                    objPLN010.EditedOn = DateTime.Now;
                                }
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN009.HeaderId;
                        objResponseMsg.Status = objPLN009.Status;
                        objResponseMsg.Revision = objPLN009.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN009.HeaderId, objPLN009.Status, objPLN009.RevNo, objPLN009.Project, objPLN009.Document);

                        var folderPath = "PLN009/" + NewHeaderId + "/" + objPLN009.RevNo;                        
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                //else
                //{
                //    PLN009 objPLN009 = db.PLN009.Add(new PLN009
                //    {
                //        Project = pln009.Project,
                //        Document = pln009.Document,
                //        Customer = pln009.Customer,
                //        RevNo = 0,
                //        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                //        CDD = Helper.ToNullIfTooEarlyForDb(pln009.CDD),
                //        Product = pln009.Product,
                //        ProcessLicensor = pln009.ProcessLicensor,
                //        ProcessPlan = pln009.ProcessPlan,
                //        JobNo = pln009.JobNo,
                //        CreatedBy = objClsLoginInfo.UserName,
                //        CreatedOn = DateTime.Now,
                //        ApprovedBy = pln009.ApprovedBy
                //    });
                //    db.SaveChanges();
                //    NewHeaderId = objPLN009.HeaderId;
                //    if (!string.IsNullOrWhiteSpace(strHeaderId))
                //    {
                //        int planHeaderId = Convert.ToInt32(strHeaderId);
                //        List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == planHeaderId).ToList();
                //        List<PLN010> lstPLN010 = new List<PLN010>();
                //        if (lstPLN002.Count > 0)
                //        {
                //            List<PLN002> lstTruePLN002 = new List<PLN002>();
                //            if (!string.IsNullOrWhiteSpace(strLineId))
                //            {
                //                int[] arrayLineId = Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s));
                //                lstTruePLN002 = lstPLN002.Where(x => arrayLineId.Contains(x.LineId)).ToList();
                //            }
                //            foreach (PLN002 objPLN002 in lstPLN002)
                //            {
                //                PLN010 objPLN010 = new PLN010();
                //                objPLN010.HeaderId = objPLN009.HeaderId;
                //                objPLN010.Project = objPLN009.Project;
                //                objPLN010.Document = objPLN009.Document;
                //                objPLN010.RevNo = objPLN009.RevNo;
                //                objPLN010.Plan = objPLN009.ProcessPlan;
                //                objPLN010.PlanRevNo = objPLN002.RevNo;
                //                objPLN010.CheckListId = objPLN002.LineId;
                //                objPLN010.Yes_No = (lstTruePLN002.Where(x => x.LineId == objPLN002.LineId).FirstOrDefault() != null ? true : false);
                //                objPLN010.CreatedBy = objClsLoginInfo.UserName;
                //                objPLN010.CreatedOn = DateTime.Now;
                //                lstPLN010.Add(objPLN010);
                //            }
                //        }
                //        if (lstPLN010 != null && lstPLN010.Count > 0)
                //        {
                //            db.PLN010.AddRange(lstPLN010);
                //            db.SaveChanges();
                //        }
                //    }
                //    objResponseMsg.Key = true;
                //    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                //    objResponseMsg.Status = objPLN009.Status;
                //    objResponseMsg.Revision = objPLN009.RevNo;
                //    objResponseMsg.HeaderID = NewHeaderId;

                //    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                //    string unReleasedStatus = clsImplementationEnum.PlanningDinStatus.UNRELEASED.GetStringValue();
                //    PDN002 objPDN002 = db.PDN002.Where(x => x.RefId == objPLN009.HeaderId && x.Status == unReleasedStatus).FirstOrDefault();
                //    if (objPDN002 != null)
                //    {
                //        objPDN002.RevNo = objPLN009.RevNo;
                //        objPDN002.Status = objPLN009.Status;
                //        db.SaveChanges();
                //    }

                //    var folderPath = "PLN009/" + NewHeaderId + "/" + objPLN009.RevNo;

                //    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                //}

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult LoadCheckListDataNew(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                Session["Headerid"] = Convert.ToInt32(param.CTQHeaderId);
                PLN009 objPLN009 = db.PLN009.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string HeaderStatus = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                bool IsDisable = true;
                if (objPLN009 != null)
                {
                    HeaderStatus = objPLN009.Status;
                }
                if (HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower())
                {
                    IsDisable = false;
                }
                if (objPLN009.ApprovedBy != null)
                {
                    if (objPLN009.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        IsDisable = true;
                    }
                    else
                    {
                        IsDisable = false;
                    }
                }
                else
                {
                    IsDisable = true;
                }
                string strSectionHandling = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strSectionHandling &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln010.HeaderId = " + HeaderId + " and ";
                    PLN010 objPLN010 = db.PLN010.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN010 != null)
                    {
                        PlanRev = objPLN010.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]", "pln002.CreatedBy +' - '+com003.t_name", "pln002.EditedBy +' - '+com003.t_name" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                #region Sorting

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstResult = db.SP_FETCH_SECTIONHANDLING_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                (new PLNCommonFunction()).CheckListLineOperationYesNoNew(uc.Yes_No,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Yes_No","UpdateData(this)"),
                                (new PLNCommonFunction()).CheckListLineOperationRemarksNew(uc.Remarks,uc.LineId,uc.HeaderId,IsDisable,uc.ActualId,"Remarks","UpdateData(this)"),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                //getChecklistDetails(uc.CheckListId.ToString())[0],
                                //getChecklistDetails(uc.CheckListId.ToString())[1],
                                //getChecklistDetails(uc.CheckListId.ToString())[2],
                                //getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public string[] getChecklistDetails(string lineId)
        {
            string[] checklist = new string[4];
            int LineID = Int32.Parse(lineId);
            var data = (from p2 in db.PLN002
                        join
                            p1 in db.PLN001 on p2.HeaderId equals p1.HeaderId
                        where p2.LineId == LineID
                        select p1).FirstOrDefault();
            if (data != null)
            {
                checklist[0] = Manager.GetUserNameFromPsNo(data.CreatedBy); //prepared by
                checklist[1] = Manager.GetUserNameFromPsNo(data.ApprovedBy); // approved by
                checklist[2] = data.ApprovedOn.ToString(); // approved on
                checklist[3] = data.Document; // document no
            }
            return checklist;
        }

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Section_Handling_Plan, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_SECTIONHANDLING_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = Convert.ToString(li.Project),
                                      Document = Convert.ToString(li.Document),
                                      Customer = Convert.ToString(li.Customer),
                                      Product = Convert.ToString(li.Product),
                                      ProcessLicensor = Convert.ToString(li.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + li.RevNo),
                                      Status = Convert.ToString(li.Status),
                                      CDD = Convert.ToString(li.CDD),
                                      ProcessPlan = Convert.ToString(li.ProcessPlan),
                                      CreatedBy = Convert.ToString(li.CreatedBy),
                                      CreatedOn = Convert.ToString(li.CreatedOn),
                                      EditedBy = Convert.ToString(li.EditedBy),
                                      EditedOn = Convert.ToString(li.EditedOn),
                                      SubmittedBy = Convert.ToString(li.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),

                                      ApprovedBy = Convert.ToString(li.ApprovedBy),
                                      ApprovedOn = Convert.ToString(li.ApprovedOn),
                                      ReturnRemark = Convert.ToString(li.ReturnRemark),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    string strSectionHandling = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                    string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                    var latestRecord = db.PLN001
                             .Where(x
                                        => x.Plan == strSectionHandling &&
                                           x.Status == strStatus
                                    )
                             .OrderByDescending(x => x.HeaderId)
                             .Select(x => new { x.HeaderId, x.RevNo })
                             .FirstOrDefault();
                    // var lst = db.SP_FETCH_SECTIONHANDLING_HEADER_LINE(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    var lst = db.SP_FETCH_SECTIONHANDLING_HEADER_LINE(headerid, latestRecord.RevNo, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      RelatedTo = Convert.ToString(li.RelatedTo),
                                      CheckPointDesc = Convert.ToString(li.CheckPointDesc),
                                      //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                      Remarks = li.Remarks,

                                      PreparedBy = getChecklistDetails(li.CheckListId.ToString())[0],
                                      CheckedBy = getChecklistDetails(li.CheckListId.ToString())[1],
                                      CheckedOn = getChecklistDetails(li.CheckListId.ToString())[2],
                                      DocumentNo = getChecklistDetails(li.CheckListId.ToString())[3]

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

    }
}
