﻿using IEMQS.Areas.MSW.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.Utility.Models;

namespace IEMQS.Areas.PLN.Controllers
{
    public class MaintainStoolPlanController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult LoadStoolPlanListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_StoolPlanListDataPartial");
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "Stool Plan TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                PLN017_Log objPLN017_Log = db.PLN017_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN017_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN017_Log.CreatedBy) : null;
                model.CreatedOn = objPLN017_Log.CreatedOn;
                model.EditedBy = objPLN017_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN017_Log.EditedBy) : null;
                model.EditedOn = objPLN017_Log.EditedOn;
                model.SubmittedBy = objPLN017_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN017_Log.SubmittedBy) : null;
                model.SubmittedOn = objPLN017_Log.SubmittedOn;

                model.ApprovedBy = objPLN017_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN017_Log.ApprovedBy) : null;
                model.ApprovedOn = objPLN017_Log.ApprovedOn;

            }
            else
            {

                PLN017_Log objPLN017_Log = db.PLN017_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN017_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN017_Log.CreatedBy) : null;
                model.CreatedOn = objPLN017_Log.CreatedOn;
                model.EditedBy = objPLN017_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN017_Log.EditedBy) : null;
                model.EditedOn = objPLN017_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "Stoolplan Timeline";

            if (HeaderId > 0)
            {
                PLN017 objPLN017 = db.PLN017.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN017.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN017.CreatedBy) : null;
                model.CreatedOn = objPLN017.CreatedOn;
                model.EditedBy = objPLN017.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN017.EditedBy) : null;
                model.EditedOn = objPLN017.EditedOn;
                model.SubmittedBy = objPLN017.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objPLN017.SubmittedBy) : null;
                model.SubmittedOn = objPLN017.SubmittedOn;
                model.ApprovedBy = objPLN017.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objPLN017.ApprovedBy) : null;
                model.ApprovedOn = objPLN017.ApprovedOn;

            }
            else
            {
                PLN017 objPLN017 = db.PLN017.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPLN017.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPLN017.CreatedBy) : null;
                model.CreatedOn = objPLN017.CreatedOn;
                model.EditedBy = objPLN017.EditedBy != null ? Manager.GetUserNameFromPsNo(objPLN017.EditedBy) : null;
                model.EditedOn = objPLN017.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Stool_Plan);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult LoadStoolPlanHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.PlanStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                //strWhere += " and pln017.CreatedBy in('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln017.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln017.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_FETCH_STOOLPLAN_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               uc.CreatedOn.HasValue ?  Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.EditedBy),
                               uc.EditedOn.HasValue ?  Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.SubmittedBy),
                               uc.SubmittedOn.HasValue ?  Convert.ToString(uc.SubmittedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ApprovedBy),
                               uc.ApprovedOn.HasValue ?  Convert.ToString(uc.ApprovedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ReturnRemark),
                              "<nobr><center>"+"<a title='View' href='"+WebsiteURL+"/PLN/MaintainStoolPlan/CreateStoolPlan?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='' class='iconspace fa fa-eye'></i></a>"+
                               Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/PLN/MaintainStoolPlan/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblStoolPlanHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                               (uc.RevNo>0 ?"<i title=\"History\" onclick=\"histroy('"+uc.HeaderId+"')\" class=\"iconspace fa fa-history\"></i>":"<i title=\"History\" style='' class='disabledicon fa fa-history'></i>")+"<i title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainStoolPlan/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "') style='' class='iconspace fa fa-clock-o'></i>"
                               + "</center></nobr>",
                                //"<center><a title='View' href='/PLN/MaintainStoolPlan/CreateStoolPlan?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult CreateStoolPlan(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN017");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN017 objPLN017 = db.PLN017.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                //if (objPLN017 == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN017.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                objPLN017 = db.PLN017.Where(x => x.HeaderId == HeaderID && x.CreatedBy == currentUser).FirstOrDefault();
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadStoolPlanCreateFormPartial(int HeaderID = 0)
        {
            PLN017 objPLN017 = db.PLN017.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN017 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN017.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN017.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(objPLN017.ApprovedBy))
                {
                    ViewBag.ApproverName = objPLN017.ApprovedBy + " - " + ApproverName;
                }
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN017.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objPLN017.Customer + " - " + customerName;
                string strStoolPlan = clsImplementationEnum.PlanList.Stool_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN017/" + objPLN017.HeaderId + "/" + objPLN017.RevNo));

                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN017.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objPLN017.HeaderId, objPLN017.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            else
            {
                objPLN017 = new PLN017();
                objPLN017.ProcessPlan = clsImplementationEnum.PlanList.Stool_Plan.GetStringValue();
                objPLN017.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN017.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadStoolPlanCreateFormPartial", objPLN017);
        }

        [HttpPost]
        //public ActionResult SaveHeader(PLN017 pln017, bool hasAttachments, Dictionary<string, string> Attach)
        public ActionResult SaveHeader(PLN017 pln017)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln017.HeaderId > 0)
                {
                    PLN017 objPLN017 = db.PLN017.Where(x => x.HeaderId == pln017.HeaderId).FirstOrDefault();
                    if (objPLN017 != null)
                    {
                        objPLN017.Product = pln017.Product;
                        objPLN017.ProcessLicensor = pln017.ProcessLicensor;
                        objPLN017.JobNo = pln017.JobNo;
                        objPLN017.ApprovedBy = pln017.ApprovedBy;
                        objPLN017.ReviseRemark = pln017.ReviseRemark;
                        objPLN017.EditedBy = objClsLoginInfo.UserName;
                        objPLN017.EditedOn = DateTime.Now;
                        if (objPLN017.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN017.RevNo = Convert.ToInt32(objPLN017.RevNo) + 1;
                            objPLN017.ReturnRemark = null;
                            objPLN017.ApprovedOn = null;

                            objPLN017.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN017.HeaderId;
                        objResponseMsg.Status = objPLN017.Status;
                        objResponseMsg.Revision = objPLN017.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN017.HeaderId, objPLN017.Status, objPLN017.RevNo, objPLN017.Project, objPLN017.Document);


                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                    PLN017 objPLN017 = db.PLN017.Add(new PLN017
                    {
                        Project = pln017.Project,
                        Document = pln017.Document,
                        Customer = pln017.Customer,
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CDD = Helper.ToNullIfTooEarlyForDb(pln017.CDD),
                        Product = pln017.Product,
                        ProcessLicensor = pln017.ProcessLicensor,
                        ProcessPlan = pln017.ProcessPlan,
                        JobNo = pln017.JobNo,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        ApprovedBy = pln017.ApprovedBy
                    });
                    db.SaveChanges();
                    NewHeaderId = objPLN017.HeaderId;

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    objResponseMsg.Status = objPLN017.Status;
                    objResponseMsg.Revision = objPLN017.RevNo;
                    objResponseMsg.HeaderID = NewHeaderId;

                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN017.HeaderId, objPLN017.Status, objPLN017.RevNo, objPLN017.Project, objPLN017.Document);

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SendForApproval(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrLineStatus = { clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(), clsImplementationEnum.PlanStatus.Returned.GetStringValue() };
                PLN017 objPLN017 = db.PLN017.Where(x => x.HeaderId == HeaderId && arrLineStatus.Contains(x.Status)).FirstOrDefault();
                if (objPLN017 != null)
                {
                    objPLN017.Status = clsImplementationEnum.PTMTCTQStatus.SendForApproval.GetStringValue();
                    objPLN017.SubmittedBy = objClsLoginInfo.UserName;
                    objPLN017.SubmittedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Plan successfully sent for approval.";
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN017.HeaderId, objPLN017.Status, objPLN017.RevNo, objPLN017.Project, objPLN017.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                        objPLN017.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN017.Project, clsImplementationEnum.PlanList.Stool_Plan.GetStringValue(), objPLN017.RevNo.Value.ToString(), objPLN017.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Stool_Plan.GetStringValue(), objPLN017.HeaderId.ToString(), true),
                                                        objPLN017.ApprovedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Plan not available for approval.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //added by nikita on 31/08/2017
        [HttpPost]
        public ActionResult RetractStatus(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PLN017 objPLN017 = db.PLN017.Where(u => u.HeaderId == headerId).SingleOrDefault();
                if (objPLN017 != null)
                {
                    objPLN017.Status = clsImplementationEnum.PLCStatus.DRAFT.GetStringValue();
                    objPLN017.EditedBy = objClsLoginInfo.UserName;
                    objPLN017.EditedOn = DateTime.Now;
                    objPLN017.SubmittedOn = null;
                    objPLN017.SubmittedBy = null;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                    Manager.UpdatePDN002(objPLN017.HeaderId, objPLN017.Status, objPLN017.RevNo, objPLN017.Project, objPLN017.Document);
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            string strCdd = Manager.GetContractWiseCdd(projectCode);
            if (!string.IsNullOrWhiteSpace(strCdd))
            {
                DateTime dtCdd = Convert.ToDateTime(strCdd);
                objApproverModel.Cdd = dtCdd.Date.ToShortDateString();
            }
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHistoryDetails(string strRole, int headerid)
        {
            ViewBag.Role = strRole;
            ViewBag.HeaderId = headerid;
            Session["HeaderId"] = Convert.ToInt32(headerid);
            return PartialView("_GetHistoryDetailsPartial");
        }

        [HttpPost]
        public JsonResult LoadStoolPlanHistory(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                var HeaderId = Convert.ToInt32(Request["HeaderId"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "INITIATOR")
                {
                    strWhere += "1=1  and HeaderId='" + Convert.ToInt32(Session["HeaderId"]) + "'";

                }
                else
                {
                    strWhere += "1=1 and pln017.ApprovedBy = '" + objClsLoginInfo.UserName + "' and HeaderId='" + Convert.ToInt32(Session["HeaderId"]) + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln017.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln017.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_STOOLPLAN_HEADERS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               uc.CreatedOn.HasValue ?  Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.EditedBy),
                               uc.EditedOn.HasValue ?  Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.SubmittedBy),
                               uc.SubmittedOn.HasValue ?  Convert.ToString(uc.SubmittedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ApprovedBy),
                               uc.ApprovedOn.HasValue ?  Convert.ToString(uc.ApprovedOn.Value.ToShortDateString()) : "",
                               Convert.ToString(uc.ReturnRemark),
                               (param.CTQCompileStatus.ToUpper() == "INITIATOR"?"<center><a title='View' href='"+WebsiteURL+"/PLN/MaintainStoolPlan/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainStoolPlan/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>":"<center><a title='View' href='"+WebsiteURL+"/PLN/ApproveStoolPlan/ViewHistory?Id="+Convert.ToInt32(uc.Id)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainStoolPlan/ShowHistoryTimeline?HeaderID=" + Convert.ToInt32(uc.Id) + "')><i style = 'margin-left:5px;'  class='fa fa-clock-o'></i></a></center>")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN017_Log objPLN017_Log = db.PLN017_Log.Where(x => x.Id == Id && x.CreatedBy == currentUser).FirstOrDefault();
                //if (objPLN017_Log == null)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }
            ViewBag.Id = Id;
            return View();
        }
        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                PLN017 objPLN017 = db.PLN017.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objPLN017 != null)
                {
                    objPLN017.RevNo = Convert.ToInt32(objPLN017.RevNo) + 1;
                    objPLN017.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objPLN017.ReviseRemark = strRemarks;
                    objPLN017.EditedBy = objClsLoginInfo.UserName;
                    objPLN017.EditedOn = DateTime.Now;
                    objPLN017.ReturnRemark = null;
                    objPLN017.ApprovedOn = null;
                    objPLN017.SubmittedBy = null;
                    objPLN017.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objPLN017.HeaderId, objPLN017.Status, objPLN017.RevNo, objPLN017.Project, objPLN017.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objPLN017.HeaderId;
                    objResponseMsg.Status = objPLN017.Status;
                    objResponseMsg.rev = objPLN017.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult LoadStoolPlanHistoryFormPartial(int Id = 0)
        {
            PLN017_Log objPLN017_Log = db.PLN017_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN017_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN017_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN017_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN017_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN017_Log.ApprovedBy = objPLN017_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN017_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN017_Log.Customer = objPLN017_Log.Customer + " - " + customerName;
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN017/" + objPLN017_Log.HeaderId + "/" + objPLN017_Log.RevNo));
            }
            else
            {
                objPLN017_Log = new PLN017_Log();
                objPLN017_Log.ProcessPlan = clsImplementationEnum.PlanList.Stool_Plan.GetStringValue();
                objPLN017_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN017_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            //var urlPrefix = "http://" + Request.Url.Authority + "/PLN/MaintainStoolPlan/ViewHistory?Id=";// AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            var urlPrefix = WebsiteURL + "/PLN/MaintainStoolPlan/ViewHistory?Id=";
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.PLN017_Log.Any(q => (q.HeaderId == objPLN017_Log.HeaderId && q.RevNo == (objPLN017_Log.RevNo - 1))) ? urlPrefix + db.PLN017_Log.Where(q => (q.HeaderId == objPLN017_Log.HeaderId && q.RevNo == (objPLN017_Log.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.PLN017_Log.Any(q => (q.HeaderId == objPLN017_Log.HeaderId && q.RevNo == (objPLN017_Log.RevNo + 1))) ? urlPrefix + db.PLN017_Log.Where(q => (q.HeaderId == objPLN017_Log.HeaderId && q.RevNo == (objPLN017_Log.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return PartialView("_LoadStoolPlanHistoryFormPartial", objPLN017_Log);
        }

        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_FETCH_STOOLPLAN_HEADERS_Result> lst = db.SP_FETCH_STOOLPLAN_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = Convert.ToString(li.Project),
                                      Document = Convert.ToString(li.Document),
                                      Customer = Convert.ToString(li.Customer),
                                      Product = Convert.ToString(li.Product),
                                      ProcessLicensor = Convert.ToString(li.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + li.RevNo),
                                      Status = Convert.ToString(li.Status),
                                      CDD = Convert.ToString(li.CDD),
                                      ProcessPlan = Convert.ToString(li.ProcessPlan),
                                      CreatedBy = Convert.ToString(li.CreatedBy),
                                      CreatedOn = li.CreatedOn.HasValue ? Convert.ToString(li.CreatedOn.Value.ToShortDateString()) : "",
                                      EditedBy = Convert.ToString(li.EditedBy),
                                      EditedOn = li.EditedOn.HasValue ? Convert.ToString(li.EditedOn.Value.ToShortDateString()) : "",
                                      SubmittedBy = Convert.ToString(li.SubmittedBy),
                                      SubmittedOn = li.SubmittedOn.HasValue ? Convert.ToString(li.SubmittedOn.Value.ToShortDateString()) : "",
                                      ApprovedBy = Convert.ToString(li.ApprovedBy),
                                      ApprovedOn = li.ApprovedOn.HasValue ? Convert.ToString(li.ApprovedOn.Value.ToShortDateString()) : "",
                                      Remark = Convert.ToString(li.ReturnRemark),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_STOOLPLAN_HEADERS_HISTORY(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(uc.Project),
                                      Document = Convert.ToString(uc.Document),
                                      Customer = Convert.ToString(uc.Customer),
                                      Product = Convert.ToString(uc.Product),
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      Status = Convert.ToString(uc.Status),
                                      CDD = Convert.ToString(uc.CDD),
                                      ProcessPlan = Convert.ToString(uc.ProcessPlan),
                                      CreatedBy = Convert.ToString(uc.CreatedBy),
                                      CreatedOn = uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                                      EditedBy = Convert.ToString(uc.EditedBy),
                                      EditedOn = uc.EditedOn.HasValue ? Convert.ToString(uc.EditedOn.Value.ToShortDateString()) : "",
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = uc.SubmittedOn.HasValue ? Convert.ToString(uc.SubmittedOn.Value.ToShortDateString()) : "",
                                      ApprovedBy = Convert.ToString(uc.ApprovedBy),
                                      ApprovedOn = uc.ApprovedOn.HasValue ? Convert.ToString(uc.ApprovedOn.Value.ToShortDateString()) : "",
                                      Remark = Convert.ToString(uc.ReturnRemark),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}