﻿using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PLN.Controllers
{
    public class ApproveEquipmentController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadEquipmentListDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_EquipmentListDataPartial");
        }

        [HttpPost]
        public JsonResult LoadEquipmentHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue() + "') and pln011.ApprovedBy in('" + objClsLoginInfo.UserName + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
               // strWhere += " and pln011.ApprovedBy in('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pln011.Project+' - '+com1.t_dsca",
                                                "Document",
                                                "pln011.Customer+' - '+com6.t_nama",
                                                "Status",
                                                "Product",
                                                "ProcessLicensor",
                                                "ProcessPlan",
                                                "JobNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_FETCH_EQUIPMENTHANDLING_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Document),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.CDD),
                               Convert.ToString(uc.ProcessPlan),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn),
                               Convert.ToString(uc.EditedBy),
                               Convert.ToString(uc.EditedOn),
                                    Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),

                                Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(uc.ApprovedOn),
                               Convert.ToString(uc.ReturnRemark),
                               Convert.ToString(uc.HeaderId),
                              //  "<a title='View' href='/PLN/ApproveEquipment/ApproveEquipment?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a>"+(uc.RevNo>0 ?"<a title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Approver','/PLN/MaintainEquipment/GetHistoryDetails','Equipment Handling')\"><i style='margin-left:5px;' class='fa fa-history'></i></a>":"")+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PLN/MaintainEquipment/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='margin-left:5px;cursor:pointer;' class='fa fa-clock-o'></i></a>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ApproveEquipment(int HeaderID = 0)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("PLN011");
            if (HeaderID > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == HeaderID).FirstOrDefault();                
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN011.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
            }
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadEquipmentCreateFormPartial(int HeaderID = 0)
        {
            PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPLN011 != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN011.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                if (objPLN011.ApprovedBy != null)
                {
                    if (objPLN011.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
                objPLN011.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN011.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN011.ApprovedBy = objPLN011.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN011.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN011.Customer = objPLN011.Customer + " - " + customerName;
                string strEquipment = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strEquipment &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    PLN012 objPLN012 = db.PLN012.Where(x => x.HeaderId == objPLN011.HeaderId).FirstOrDefault();
                    if (objPLN012 != null)
                    {
                        if (latestRecord.RevNo == objPLN012.PlanRevNo)
                        {
                            ViewBag.isLatest = "true";
                        }
                        else
                        {
                            ViewBag.isLatest = "false";
                        }
                    }
                    else
                    {
                        ViewBag.isLatest = "false";
                    }
                }
                else
                {
                    ViewBag.isLatest = "true";
                }
                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objPLN011.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, HeaderID, clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue());

                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN011/" + objPLN011.HeaderId + "/" + objPLN011.RevNo));

            }
            else
            {
                objPLN011 = new PLN011();
                objPLN011.ProcessPlan = clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue();
                objPLN011.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN011.RevNo = 0;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadEquipmentCreateFormPartial", objPLN011);
        }

        [HttpPost]
        public JsonResult LoadCheckListData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string strEquipment = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strEquipment &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln012.HeaderId = " + HeaderId + " and ";
                    PLN012 objPLN012 = db.PLN012.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN012 != null)
                    {
                        PlanRev = objPLN012.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]", "pln002.CreatedBy +' - '+com003.t_name", "pln002.EditedBy +' - '+com003.t_name" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_EQUIPMENTHANDLING_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,false,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,false),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string LineOperation(bool flag, int LineId, int HeaderId)
        {
            if (flag)
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" checked=\"checked\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
            else
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\">" +
                       "<input type=\"checkbox\" name=\"chkYesNo\" sHeaderId=\"" + HeaderId + "\" sLineId=\"" + LineId + "\" />" +
                       "<span></ span >" +
                       "</label>";
            }
        }

        [HttpPost]
        public ActionResult ReturnEquipment(int HeaderId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN011 != null)
                {                    
                    objPLN011.Status = clsImplementationEnum.PlanStatus.Returned.GetStringValue();
                    objPLN011.ReturnRemark = Remarks;
                    objPLN011.EditedBy = objClsLoginInfo.UserName;
                    objPLN011.EditedOn = DateTime.Now; 

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully returned.";
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    Manager.UpdatePDN002(objPLN011.HeaderId, objPLN011.Status, objPLN011.RevNo, objPLN011.Project, objPLN011.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN011.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN011.Project, clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue(), objPLN011.RevNo.Value.ToString(), objPLN011.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue(), objPLN011.HeaderId.ToString(), false),
                                                        objPLN011.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please add remarks for returning Checklist.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApproverSelected(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
            for (int i = 0; i < arrayHeaderIds.Length; i++)
            {
                int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg = ApproveHeaderByApprover(HeaderId);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg ApproveHeaderByApprover(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string Status = clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToUpper();
                PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == HeaderId && x.Status.ToUpper() == Status).FirstOrDefault();
                if (objPLN011 != null)
                {                   
                    objPLN011.Status = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                    objPLN011.ApprovedBy = objClsLoginInfo.UserName;
                    objPLN011.ApprovedOn = DateTime.Now;

                    #region Header Log
                    var lstPLN011_Log = db.PLN011_Log.Where(x => x.HeaderId == objPLN011.HeaderId).ToList();
                    PLN011_Log objPLN011_Log = new PLN011_Log();
                    if (objPLN011_Log != null)
                    {
                        objPLN011_Log = lstPLN011_Log.FirstOrDefault();
                        lstPLN011_Log.ForEach(i => { i.Status = clsImplementationEnum.PlanStatus.Superseded.GetStringValue(); });
                    }

                    objPLN011_Log = db.PLN011_Log.Add(new PLN011_Log
                    {
                        HeaderId = objPLN011.HeaderId,
                        Project = objPLN011.Project,
                        Document = objPLN011.Document,
                        Customer = objPLN011.Customer,
                        RevNo = objPLN011.RevNo,
                        Status = objPLN011.Status,
                        CDD = objPLN011.CDD,
                        Product = objPLN011.Product,
                        ProcessLicensor = objPLN011.ProcessLicensor,
                        ProcessPlan = objPLN011.ProcessPlan,
                        Attachment = objPLN011.Attachment,
                        JobNo = objPLN011.JobNo,
                        CreatedBy = objPLN011.CreatedBy,
                        CreatedOn = objPLN011.CreatedOn,
                        EditedBy = objPLN011.EditedBy,
                        EditedOn = objPLN011.EditedOn,
                        ApprovedBy = objPLN011.ApprovedBy,
                        ApprovedOn = objPLN011.ApprovedOn,
                        ReturnRemark = objPLN011.ReturnRemark,
                        ReviseRemark = objPLN011.ReviseRemark,
                        SubmittedBy = objPLN011.SubmittedBy,
                        SubmittedOn = objPLN011.SubmittedOn
                    });

                    #endregion
                    db.SaveChanges();
                    //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    int newId = db.PLN011_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objPLN011.HeaderId, objPLN011.Status, objPLN011.RevNo, objPLN011.Project, objPLN011.Document, newId);

                    List<PLN012> lstPLN012 = db.PLN012.Where(x => x.HeaderId == HeaderId).ToList();
                    List<PLN012_Log> lstPLN012_Log = new List<PLN012_Log>();
                    if (lstPLN012 != null && lstPLN012.Count > 0)
                    {
                        lstPLN012_Log = lstPLN012.Select(x => new PLN012_Log()
                        {
                            RefId = objPLN011_Log.Id,
                            LineId = x.LineId,
                            HeaderId = x.HeaderId,
                            Project = x.Project,
                            Document = x.Document,
                            RevNo = x.RevNo,
                            Plan = x.Plan,
                            PlanRevNo = x.PlanRevNo,
                            CheckListId = x.CheckListId,
                            Yes_No = x.Yes_No,
                            Remarks = x.Remarks,
                            CreatedBy = x.CreatedBy,
                            CreatedOn = x.CreatedOn,
                            EditedBy = x.EditedBy,
                            EditedOn = x.EditedOn
                        }).ToList();
                        db.PLN012_Log.AddRange(lstPLN012_Log);
                        db.SaveChanges();
                    }

                    int? currentRevisionNull = db.PLN011.Where(q => q.HeaderId == HeaderId).FirstOrDefault().RevNo;
                    int currentRevision = ((currentRevisionNull == null) ? 0 : currentRevisionNull.Value);
                    //clsUpload.CopyFolderContents("PLN011/" + HeaderId + "/" + currentRevision, "PLN011/" + HeaderId + "/" + (currentRevision + 1));
                    var oldfolderPath = "PLN011//" + HeaderId + "//" + currentRevision;
                    var newfolderPath = "PLN011//" + HeaderId + "//" + (currentRevision + 1);
                    //(new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);

                    Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(oldfolderPath, newfolderPath, oldfolderPath, HeaderId, newfolderPath, HeaderId, DESServices.CommonService.GetUseIPConfig, DESServices.CommonService.objClsLoginInfo.UserName, 0);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checklist successfully approved.";

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objPLN011.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objPLN011.Project, clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue(), objPLN011.RevNo.Value.ToString(), objPLN011.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue(), objPLN011.HeaderId.ToString(), false),
                                                        objPLN011.CreatedBy);
                    #endregion
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Checklist not available approved.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return objResponseMsg;
        }

        [SessionExpireFilter]//, AllowAnonymous, UserPermissions
        public ActionResult ViewHistory(int Id = 0)
        {
            if (Id > 0)
            {
                string currentUser = objClsLoginInfo.UserName;
                PLN011_Log objPLN011_Log = db.PLN011_Log.Where(x => x.Id == Id).FirstOrDefault();
                if (objPLN011_Log == null)
                {
                    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.Id = Id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadEquipmentHistoryFormPartial(int Id = 0)
        {
            PLN011_Log objPLN011_Log = db.PLN011_Log.Where(x => x.Id == Id).FirstOrDefault();
            if (objPLN011_Log != null)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPLN011_Log.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                objPLN011_Log.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string ApproverName = db.COM003.Where(x => x.t_psno == objPLN011_Log.ApprovedBy && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objPLN011_Log.ApprovedBy = objPLN011_Log.ApprovedBy + " - " + ApproverName;
                string customerName = db.COM006.Where(x => x.t_bpid == objPLN011_Log.Customer).Select(x => x.t_nama).FirstOrDefault();
                objPLN011_Log.Customer = objPLN011_Log.Customer + " - " + customerName;
                string strEquipment = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strEquipment &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();
                if (latestRecord != null)
                {
                    //if (latestRecord.HeaderId == objPLN003.HeaderId)
                    //{

                    //}
                }
                ViewBag.Documents = JsonConvert.SerializeObject(Manager.getDocuments1("PLN011/" + objPLN011_Log.HeaderId + "/" + objPLN011_Log.RevNo));
            }
            else
            {
                objPLN011_Log = new PLN011_Log();
                objPLN011_Log.ProcessPlan = clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue();
                objPLN011_Log.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                objPLN011_Log.RevNo = 0;
            }
            // ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadEquipmentHistoryFormPartial", objPLN011_Log);
        }

        [HttpPost]
        public JsonResult LoadCheckListHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int Id = Convert.ToInt32(param.CTQHeaderId);
                PLN011_Log objPLN011_Log = db.PLN011_Log.Where(x => x.Id == Id).FirstOrDefault();

                strWhere += "1=1 and pln012.RefId = " + objPLN011_Log.Id + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]", "pln012.CreatedBy", "pln012.EditedBy" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_EQUIPMENTHANDLING_HEADER_LINE_HISTORY
                                (
                                     objPLN011_Log.HeaderId,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                Manager.CheckListLineOperationYesNo(uc.Yes_No,uc.LineId,uc.HeaderId,true,uc.ActualId),
                                Manager.CheckListLineOperationRemarks(uc.Remarks,uc.LineId,uc.HeaderId,true),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }



        /// Created By :Deepak tiwari
        /// Created Date :25/08/2017
        /// Description: Modification of add Save functionality when status is send for approval
        /// </summary>
        public ActionResult SaveHeader(PLN011 pln011, string strHeaderId, string strLineId, string strIsLatest)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int NewHeaderId = 0;
                if (pln011.HeaderId > 0)
                {
                    PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == pln011.HeaderId).FirstOrDefault();
                    if (objPLN011 != null)
                    {
                        objPLN011.Document = pln011.Document;
                        objPLN011.Product = pln011.Product;
                        objPLN011.ProcessLicensor = pln011.ProcessLicensor;
                        objPLN011.JobNo = pln011.JobNo;
                        objPLN011.ApprovedBy = pln011.ApprovedBy.Split('-')[0].ToString().Trim(); ;
                        objPLN011.EditedBy = objClsLoginInfo.UserName;
                        objPLN011.EditedOn = DateTime.Now;
                        if (objPLN011.Status == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                        {
                            objPLN011.RevNo = Convert.ToInt32(objPLN011.RevNo) + 1;
                            objPLN011.Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                        }                        
                        if (!Convert.ToBoolean(strIsLatest))
                        {
                            List<PLN012> lstPLN012 = db.PLN012.Where(x => x.HeaderId == objPLN011.HeaderId).ToList();
                            if (lstPLN012 != null && lstPLN012.Count > 0)
                            {
                                db.PLN012.RemoveRange(lstPLN012);
                            }
                            string strEquipment = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                            string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                            var latestRecord = db.PLN001
                                     .Where(x
                                                => x.Plan == strEquipment &&
                                                   x.Status == strStatus
                                            )
                                     .OrderByDescending(x => x.HeaderId)
                                     .Select(x => new { x.HeaderId, x.RevNo })
                                     .FirstOrDefault();
                            List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == latestRecord.HeaderId).ToList();
                            if (lstPLN002 != null && lstPLN002.Count > 0)
                            {
                                List<PLN012> NewlstPLN012 = lstPLN002.Select(x => new PLN012()
                                {
                                    LineId = x.LineId,
                                    HeaderId = objPLN011.HeaderId,
                                    Project = objPLN011.Project,
                                    Document = objPLN011.Document,
                                    RevNo = objPLN011.RevNo,
                                    Plan = objPLN011.ProcessPlan,
                                    PlanRevNo = x.RevNo,
                                    CheckListId = x.LineId,
                                    //Yes_No = false,
                                    Yes_No = clsImplementationEnum.ChecklistYesNo.No.GetStringValue(),
                                    CreatedBy = objClsLoginInfo.UserName,
                                    CreatedOn = DateTime.Now
                                }).ToList();
                                db.PLN012.AddRange(NewlstPLN012);
                            }
                        }
                        else
                        {
                            List<PLN012> lstPLN012 = db.PLN012.Where(x => x.HeaderId == objPLN011.HeaderId).ToList();
                            if (lstPLN012.Count > 0)
                            {
                                List<PLN012> lstTruePLN012 = new List<PLN012>();

                                foreach (PLN012 objPLN012 in lstPLN012)
                                {
                                    //objPLN012.Yes_No = (!string.IsNullOrWhiteSpace(strLineId) && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Length > 0 && Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s)).Contains(objPLN012.LineId) ? true : false);                                    
                                    objPLN012.EditedBy = objClsLoginInfo.UserName;
                                    objPLN012.EditedOn = DateTime.Now;
                                }
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        NewHeaderId = objPLN011.HeaderId;
                        objResponseMsg.Status = objPLN011.Status;
                        objResponseMsg.Revision = objPLN011.RevNo;
                        objResponseMsg.HeaderID = NewHeaderId;

                        //Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                        Manager.UpdatePDN002(objPLN011.HeaderId, objPLN011.Status, objPLN011.RevNo, objPLN011.Project, objPLN011.Document);

                        //if (hasAttachments)
                        //{
                        var folderPath = "PLN011/" + NewHeaderId + "/" + objPLN011.RevNo;
                        //    var existing = clsUpload.getDocs(folderPath);
                        //    var toDelete = new Dictionary<string, string>();
                        //    foreach (var item in existing)
                        //    {
                        //        if (Attach.Where(q => q.Key.Equals(item.Key)).Count() <= 0)
                        //            toDelete.Add(item.Key, item.Value);
                        //        else
                        //        {
                        //            if (Attach.Where(q => q.Key.Equals(item.Key)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                        //                toDelete.Add(item.Key, item.Value);
                        //        }
                        //    }
                        //    foreach (var item in toDelete)
                        //        clsUpload.DeleteFile(folderPath, item.Key);
                        //    var toUpload = Attach.Where(q => q.Value.Substring(0, 3) != "URL");
                        //    foreach (var attch in toUpload)
                        //    {
                        //        var base64Data = attch.Value;
                        //        var dataBytes = Helper.FromBase64(attch.Value);
                        //        //var dataBytes = Encoding.ASCII.GetBytes(stringData);
                        //        clsUpload.Upload(attch.Key, dataBytes, folderPath);
                        //    }
                        //}
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }
                else
                {
                    //PLN011 objPLN011 = db.PLN011.Add(new PLN011
                    //{
                    //    Project = pln011.Project,
                    //    Document = pln011.Document,
                    //    Customer = pln011.Customer,
                    //    RevNo = 0,
                    //    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    //    CDD = Helper.ToNullIfTooEarlyForDb(pln011.CDD),
                    //    Product = pln011.Product,
                    //    ProcessLicensor = pln011.ProcessLicensor,
                    //    ProcessPlan = pln011.ProcessPlan,
                    //    JobNo = pln011.JobNo,
                    //    CreatedBy = objClsLoginInfo.UserName,
                    //    CreatedOn = DateTime.Now,
                    //    ApprovedBy = pln011.ApprovedBy
                    //});
                    //db.SaveChanges();
                    //NewHeaderId = objPLN011.HeaderId;
                    //if (!string.IsNullOrWhiteSpace(strHeaderId))
                    //{
                    //    int planHeaderId = Convert.ToInt32(strHeaderId);
                    //    List<PLN002> lstPLN002 = db.PLN002.Where(x => x.HeaderId == planHeaderId).ToList();
                    //    List<PLN012> lstPLN012 = new List<PLN012>();
                    //    if (lstPLN002.Count > 0)
                    //    {
                    //        List<PLN002> lstTruePLN002 = new List<PLN002>();
                    //        if (!string.IsNullOrWhiteSpace(strLineId))
                    //        {
                    //            int[] arrayLineId = Array.ConvertAll(strLineId.Split(',').ToArray(), s => int.Parse(s));
                    //            lstTruePLN002 = lstPLN002.Where(x => arrayLineId.Contains(x.LineId)).ToList();
                    //        }
                    //        foreach (PLN002 objPLN002 in lstPLN002)
                    //        {
                    //            PLN012 objPLN012 = new PLN012();
                    //            objPLN012.HeaderId = objPLN011.HeaderId;
                    //            objPLN012.Project = objPLN011.Project;
                    //            objPLN012.Document = objPLN011.Document;
                    //            objPLN012.RevNo = objPLN011.RevNo;
                    //            objPLN012.Plan = objPLN011.ProcessPlan;
                    //            objPLN012.PlanRevNo = objPLN002.RevNo;
                    //            objPLN012.CheckListId = objPLN002.LineId;
                    //            objPLN012.Yes_No = (lstTruePLN002.Where(x => x.LineId == objPLN002.LineId).FirstOrDefault() != null ? true : false);
                    //            objPLN012.CreatedBy = objClsLoginInfo.UserName;
                    //            objPLN012.CreatedOn = DateTime.Now;
                    //            lstPLN012.Add(objPLN012);
                    //        }
                    //    }
                    //    if (lstPLN012 != null && lstPLN012.Count > 0)
                    //    {
                    //        db.PLN012.AddRange(lstPLN012);
                    //        db.SaveChanges();
                    //    }
                    //}
                    //objResponseMsg.Key = true;
                    //objResponseMsg.Value = clsImplementationMessage.PlanMessages.Insert.ToString();
                    //objResponseMsg.Status = objPLN011.Status;
                    //objResponseMsg.Revision = objPLN011.RevNo;
                    //objResponseMsg.HeaderID = NewHeaderId;

                    ////Update Revision and Status in Planning Din Line Table(added by Dharmesh Vasani)
                    //string unReleasedStatus = clsImplementationEnum.PlanningDinStatus.UNRELEASED.GetStringValue();
                    //PDN002 objPDN002 = db.PDN002.Where(x => x.RefId == objPLN011.HeaderId && x.Status == unReleasedStatus).FirstOrDefault();
                    //if (objPDN002 != null)
                    //{
                    //    objPDN002.RevNo = objPLN011.RevNo;
                    //    objPDN002.Status = objPLN011.Status;
                    //    db.SaveChanges();
                    //}
                    ////--

                    ////if (hasAttachments)
                    ////{
                    //var folderPath = "PLN011/" + NewHeaderId + "/" + objPLN011.RevNo;
                    ////    var existing = clsUpload.getDocs(folderPath);
                    ////    var toDelete = new Dictionary<string, string>();
                    ////    foreach (var item in existing)
                    ////    {
                    ////        if (Attach.Where(q => q.Key.Equals(item.Key)).Count() <= 0)
                    ////            toDelete.Add(item.Key, item.Value);
                    ////        else
                    ////        {
                    ////            if (Attach.Where(q => q.Key.Equals(item.Key)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                    ////                toDelete.Add(item.Key, item.Value);
                    ////        }
                    ////    }
                    ////    foreach (var item in toDelete)
                    ////        clsUpload.DeleteFile(folderPath, item.Key);
                    ////    var toUpload = Attach.Where(q => q.Value.Substring(0, 3) != "URL");

                    ////    foreach (var attch in toUpload)
                    ////    {
                    ////        var base64Data = attch.Value;
                    ////        var dataBytes = Helper.FromBase64(attch.Value);
                    ////        //var dataBytes = Encoding.ASCII.GetBytes(stringData);
                    ////        clsUpload.Upload(attch.Key, dataBytes, folderPath);
                    ////    }
                    ////}
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Inline Grid New Method
        [HttpPost]
        public JsonResult LoadCheckListDataNew(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                PLN011 objPLN011 = db.PLN011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string HeaderStatus = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue();
                bool IsDisable = true;
                if (objPLN011 != null)
                {
                    HeaderStatus = objPLN011.Status;
                }
                if (HeaderStatus.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower() && objPLN011.ApprovedBy.Trim()==objClsLoginInfo.UserName.Trim())
                {
                    IsDisable = false;
                }

                string strEquipment = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue();
                string strStatus = clsImplementationEnum.PlanStatus.Approved.GetStringValue();
                var latestRecord = db.PLN001
                         .Where(x
                                    => x.Plan == strEquipment &&
                                       x.Status == strStatus
                                )
                         .OrderByDescending(x => x.HeaderId)
                         .Select(x => new { x.HeaderId, x.RevNo })
                         .FirstOrDefault();

                if (latestRecord == null)
                {
                    return Json(new
                    {
                        sEcho = Convert.ToInt32(param.sEcho),
                        iTotalRecords = 0,
                        iTotalDisplayRecords = 0,
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
                int? PlanRev = 0;
                if (HeaderId > 0)
                {
                    strWhere += "pln012.HeaderId = " + HeaderId + " and ";
                    PLN012 objPLN012 = db.PLN012.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objPLN012 != null)
                    {
                        PlanRev = objPLN012.PlanRevNo;
                    }
                    else
                    {
                        PlanRev = 0;
                    }
                }
                else
                {
                    PlanRev = latestRecord.RevNo;
                }
                strWhere += "1=1 and pln002.[Plan] in('" + clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue() + "') and pln002.RevNo = " + PlanRev + "";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = { "RelatedTo", "[CheckPoint]", "pln002.CreatedBy", "pln002.EditedBy" };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_FETCH_EQUIPMENTHANDLING_HEADER_LINE
                                (
                                     HeaderId,
                                     latestRecord.RevNo,
                                     StartIndex,
                                     EndIndex,
                                     strSortOrder,
                                    (!string.IsNullOrWhiteSpace(strWhere) ? strWhere : null)
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.RelatedTo),
                                Convert.ToString(uc.CheckPointDesc),
                                //LineOperation(Convert.ToBoolean(uc.Yes_No),uc.LineId,uc.HeaderId),
                                (new PLNCommonFunction()).CheckListLineOperationYesNoNew(uc.Yes_No, uc.LineId, uc.HeaderId, IsDisable, uc.ActualId, "Yes_No", "UpdateData(this)"),
                                (new PLNCommonFunction()).CheckListLineOperationRemarksNew(uc.Remarks, uc.LineId, uc.HeaderId,IsDisable, uc.ActualId, "Remarks", "UpdateData(this)"),
                                Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.CreatedOn),
                                Convert.ToString(uc.EditedBy),
                                Convert.ToString(uc.EditedOn),
                                //getChecklistDetails(uc.CheckListId.ToString())[0],
                                //getChecklistDetails(uc.CheckListId.ToString())[1],
                                //getChecklistDetails(uc.CheckListId.ToString())[2],
                                //getChecklistDetails(uc.CheckListId.ToString())[3],
                                Convert.ToString(uc.IsLatest),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [NonAction]
        public string[] getChecklistDetails(string lineId)
        {
            string[] checklist = new string[4];
            int LineID = Int32.Parse(lineId);
            var data = (from p2 in db.PLN002
                        join
                            p1 in db.PLN001 on p2.HeaderId equals p1.HeaderId
                        where p2.LineId == LineID
                        select p1).FirstOrDefault();
            if (data != null)
            {
                checklist[0] = Manager.GetUserNameFromPsNo(data.CreatedBy); //prepared by
                checklist[1] = Manager.GetUserNameFromPsNo(data.ApprovedBy); // approved by
                checklist[2] = data.ApprovedOn.ToString(); // approved on
                checklist[3] = data.Document; // document no
            }
            return checklist;
        }

        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            { 
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Equipment_Handling_Plan, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}