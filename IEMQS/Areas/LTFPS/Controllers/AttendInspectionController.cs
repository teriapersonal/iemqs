﻿using ICSharpCode.SharpZipLib.Zip;
using IEMQS.Models;
using IEMQSImplementation;
using Ionic.Zip;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Drawing;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Resources;
using System.Web;
using System.Web.Mvc;
using static IEMQS.Areas.PTR.Controllers.MaintainPTRController;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.Text;
using iTextSharp.text.html.simpleparser;
using System.ComponentModel;
using System.Security.Policy;

namespace IEMQS.Areas.LTFPS.Controllers
{
    public class AttendInspectionController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index(string tab = "")
        {
            ViewBag.Message = "";
            ViewBag.Tab = tab;
            return View();
        }

        [HttpPost]
        public ActionResult LoadAttendInspectionDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_LoadAttendInspectionDataPartial");
        }

        [HttpPost]
        public JsonResult LoadAttendInspectionData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string _urlFrom = "all";

                string strWhere = string.Empty;

                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and (tm.TestResult is null or tm.TestResult='Returned by TPI') and tm.OfferedBy is not null and tm.OfferedOn is not null and tm.OfferedtoCustomerBy is null ";
                    _urlFrom = "attend";
                }
                else if (param.CTQCompileStatus.ToUpper() == "PENDING TPI")
                {
                    strWhere += "1=1 and (tm.TestResult in('" + clsImplementationEnum.PartlistOfferInspectionStatus.CLEARED.GetStringValue() + "','" + clsImplementationEnum.PartlistOfferInspectionStatus.PARTIALLY_CLEARED.GetStringValue() + "')) and tm.OfferedBy is not null and tm.OfferedOn is not null and tm.OfferedtoCustomerBy is null and 1 in (select TPIOnlineApproval from Qms010 qms10 where qms10.QualityProject = tm.QualityProject) ";
                    _urlFrom = "offertpi";
                }
                else
                {
                    strWhere += "1=1 ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "QualityProject",
                                                "dbo.GET_BULOCATIONDEPARTMENTDESC_BY_CODE(tm.BU)",
                                                "OperationNo",
                                                "OperationDescription",
                                                "SeamNo",
                                                "PartNo",
                                                "TestType",
                                                "StageCode",
                                                "IterationNo",
                                                "RequestNo",
                                                "RequestNoSequence",
                                                "TestResult",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark",
                                                "LTFPSNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tm.BU", "tm.Location");
                var lstResult = db.SP_FETCH_LTFPS_RE_OFFERINSPECTION_HEADERS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.LTFPSNo),
                               Convert.ToString(uc.OperationNo),
                               Convert.ToString(uc.OperationDescription),
                               Convert.ToString(uc.SeamNo),
                               Convert.ToString(uc.PartNo),
                               Convert.ToString(uc.TestType),
                               Convert.ToString(uc.StageCode),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.TestResult),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               "<center style=\"white-space: nowrap;\">"+GetActionLink(uc.TableName,uc.RequestId,_urlFrom,uc.TestResult)+"</center>"
                               //"<center style=\"white-space: nowrap;\"><a title=\"View Details\" class='' href='/LTFPS/AttendInspection/ViewOfferedDetails?RequestId="+Convert.ToInt32(uc.RequestId)+"&from="+ _urlFrom +"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #region Attend TPI
        [SessionExpireFilter]
        public ActionResult TPIView()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LoadAttendTPIInspectionDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_LoadAttendTPIInspectionDataPartial");
        }
        [HttpPost]
        public JsonResult LoadAttendTPIInspectionData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string _urlFrom = "all";

                string strWhere = "1=1";
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " and tm.InspectionStatus != 'Cleared' and tm.LNTStatus = 'Cleared' and tm.OfferedtoCustomerBy IS NOT NULL and tm.TPIAgency1Result IS NULL and tm.TPIAgency2Result IS NULL and tm.TPIAgency3Result IS NULL and tm.TPIAgency4Result IS NULL and tm.TPIAgency5Result IS NULL and tm.TPIAgency6Result IS NULL ";
                    _urlFrom = "attend";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "QualityProject",
                                                "dbo.GET_BULOCATIONDEPARTMENTDESC_BY_CODE(tm.BU)",
                                                "OperationNo",
                                                "SeamNo",
                                                "PartNo",
                                                "StageCode",
                                                "IterationNo",
                                                "RequestNo",
                                                "RequestNoSequence",
                                                "TestResult",
                                                "Shop",
                                                "ShopLocation",
                                                "ShopRemark",
                                                "LTFPSNo"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "tm.BU", "tm.Location");

                var lstResult = db.SP_FETCH_LTFPS_RE_OFFERINSPECTION_HEADERS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.LTFPSNo),
                               Convert.ToString(uc.OperationNo),
                               Convert.ToString(uc.SeamNo),
                               Convert.ToString(uc.PartNo),
                               Convert.ToString(uc.StageCode),
                               Convert.ToString(uc.IterationNo),
                               Convert.ToString(uc.RequestNo),
                               Convert.ToString(uc.RequestNoSequence),
                               Convert.ToString(uc.TestResult),
                               Convert.ToString(uc.Shop),
                               Convert.ToString(uc.ShopLocation),
                               Convert.ToString(uc.ShopRemark),
                               "<center style=\"white-space: nowrap;\">"+GetTPIActionLink(uc.TableName,uc.RequestId,_urlFrom)+Manager.generateLTFPSGridButtons(uc.LTFPSNo,uc.QualityProject,uc.Project,uc.BU,uc.Location,!string.IsNullOrWhiteSpace(uc.SeamNo)?uc.SeamNo:uc.PartNo,uc.StageCode, Convert.ToString(uc.OperationNo),!string.IsNullOrWhiteSpace(uc.SeamNo)?"Seam Details":"Part Details",Convert.ToString(false),!string.IsNullOrWhiteSpace(uc.SeamNo)?"true":"false",!string.IsNullOrWhiteSpace(uc.SeamNo)?"Seam Details":"Part Details",!string.IsNullOrWhiteSpace(uc.SeamNo)?"Seam Request Details":"Part Request Details")+"</center>"
                               //"<center style=\"white-space: nowrap;\"><a title=\"View Details\" class='' href='/LTFPS/AttendInspection/ViewOfferedDetails?RequestId="+Convert.ToInt32(uc.RequestId)+"&from="+ _urlFrom +"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        //Bhadresh Start
        //[HttpPost]
        //public ActionResult FileUpload(HttpPostedFileBase file, int Id)
        //{
        //    try
        //    {
        //        if (ModelState.IsValid)
        //        {
        //            if (file == null)
        //            {
        //                ModelState.AddModelError("File", "Please Upload Your file");
        //                ViewBag.Message = "Please Upload Your file";
        //            }
        //            else if (file.ContentLength > 0)
        //            {
        //                int MaxContentLength = 1024 * 1024 * 3; //3 MB
        //                string[] AllowedFileExtensions = new string[] { ".pdf" };

        //                if (!AllowedFileExtensions.Contains(file.FileName.Substring(file.FileName.LastIndexOf('.'))))
        //                {
        //                    ModelState.AddModelError("File", "Please file of type: " + string.Join(", ", AllowedFileExtensions));
        //                    ViewBag.Message = "Please file of type: " + string.Join(", ", AllowedFileExtensions);
        //                }

        //                else if (file.ContentLength > MaxContentLength)
        //                {
        //                    ModelState.AddModelError("File", "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB");
        //                    ViewBag.Message = "Your file is too large, maximum allowed size is: " + MaxContentLength + " MB";

        //                }
        //                else
        //                {
        //                    string FileExt = Path.GetExtension(file.FileName);
        //                    string relativeFileName = DateTime.Now.Ticks.ToString() + FileExt;
        //                    string FileName = Path.GetFileName(file.FileName);
        //                    var filePath = Server.MapPath("../../../Resources/LTFPS001/" + relativeFileName);
        //                    file.SaveAs(filePath);
        //                    var qmsData50 = (from i in db.LTF002
        //                                     join j in db.QMS050 on i.LineId equals j.LTFPSHeaderId
        //                                     join q in db.QMS001 on j.Project equals q.Project
        //                                     where q.IsLTFPS == true && j.RequestId == Id
        //                                     select j).FirstOrDefault();
        //                    var qmsData55 = (from i in db.LTF002
        //                                     join j in db.QMS055 on i.LineId equals j.LTFPSHeaderId
        //                                     join q in db.QMS001 on j.Project equals q.Project
        //                                     where q.IsLTFPS == true && j.RequestId == Id
        //                                     select j).FirstOrDefault();
        //                    if (qmsData50 != null)
        //                    {
        //                        qmsData50.FilePath = FileName;
        //                        qmsData50.RelativeFilePath = relativeFileName;
        //                        db.Entry(qmsData50).State = EntityState.Modified;
        //                        db.SaveChanges();
        //                    }
        //                    if (qmsData55 != null)
        //                    {
        //                        qmsData55.FilePath = FileName;
        //                        qmsData55.RelativeFilePath = relativeFileName;
        //                        db.Entry(qmsData55).State = EntityState.Modified;
        //                        db.SaveChanges();
        //                    }
        //                    ModelState.Clear();
        //                    ViewBag.Message = "File uploaded successfully";
        //                }
        //            }
        //        }
        //        QMS050 qMS050 = new QMS050();
        //        qMS050.RequestId = Id;
        //        return View("Index", qMS050);
        //    }
        //    catch (Exception)
        //    {
        //        ViewBag.Message = "Something wrong occurs";
        //        throw;
        //    }
        //}
        [HttpPost]
        [SessionExpireFilter]
        public ActionResult DownloadLTFPS(string QualityProject, string LTFPSNo)
        {
            var objResponseMsg = new clsHelper.ResponceMsgWithObject();
            var data = (from i in db.LTF002
                        join j in db.QMS050 on i.LineId equals j.LTFPSHeaderId
                        join q in db.QMS001 on j.Project equals q.Project
                        where q.IsLTFPS == true && j.QualityProject == QualityProject && i.LTFPSNo == LTFPSNo && j.TestResult == "Cleared"
                        select
 new
 {
     OPNNo = i.OperationNo,
     OPNDescription = i.OperationDescription,
     TestResult = j.TestResult,
     HeaderId = j.RequestId,
     TableName = "QMS050"
 }).Distinct().ToList().
Union(from i in db.LTF002
      join j in db.QMS055 on i.LineId equals j.LTFPSHeaderId
      join q in db.QMS001 on j.Project equals q.Project

      where q.IsLTFPS == true && j.QualityProject == QualityProject && i.LTFPSNo == LTFPSNo && j.TestResult == "Cleared"
      select new
      {
          OPNNo = i.OperationNo,
          OPNDescription = i.OperationDescription,
          TestResult = j.TestResult,
          HeaderId = j.RequestId,
          TableName = "QMS055"
      }).Distinct().ToList().Distinct().ToList();


            var lstFolderPaths = data.Select(s => s.TableName + "/" + s.HeaderId + "/1");
            string FolderPaths = string.Join(",", lstFolderPaths);
            string Filename = string.Empty;
            int FileCount = 0;
            foreach (var item in data)
            {
                string folderPath = item.TableName + "/" + item.HeaderId + "/1";
                var Files = (new clsFileUpload()).GetDocuments(folderPath);
                if (Files.Length > 0)
                {
                    Filename = Files[0].Name;
                    FileCount++;
                }
            }
            if (FileCount == 0)
            {
                objResponseMsg.Value = "No Record Found";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

            objResponseMsg.Key = true;
            objResponseMsg.data = new { FolderPaths = FolderPaths, Filename = Filename };
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetFileUploadTableName(int Id)
        {
            try
            {

                string tableName = string.Empty;
                var qmsData50 = (from i in db.LTF002
                                 join j in db.QMS050 on i.LineId equals j.LTFPSHeaderId
                                 join q in db.QMS001 on j.Project equals q.Project
                                 where q.IsLTFPS == true && j.RequestId == Id
                                 select j).FirstOrDefault();
                var qmsData55 = (from i in db.LTF002
                                 join j in db.QMS055 on i.LineId equals j.LTFPSHeaderId
                                 join q in db.QMS001 on j.Project equals q.Project
                                 where q.IsLTFPS == true && j.RequestId == Id
                                 select j).FirstOrDefault();
                if (qmsData50 != null)
                {
                    tableName = "QMS050";
                }
                if (qmsData55 != null)
                {
                    tableName = "QMS055";
                }
                return Json(tableName);
            }
            catch (Exception)
            {
                throw;
            }
        }
        [HttpGet]
        public JsonResult CheckFileExists(string HeaderId, string tableName)
        {
            try
            {
                var user = objClsLoginInfo.UserName;
                string folderPath = tableName + "/" + HeaderId + "/1";
                var Files = (new clsFileUpload()).GetDocuments(folderPath);
                string FileName = string.Empty;
                if (Files.Length > 0)
                {
                    for (int i = 0; i < Files.Length; i++)
                    {
                        FileName = Files[i].Name + "$" + FileName;
                    }
                    FileName = FileName.TrimEnd('$');
                }
                int count = Files.Length;
                bool flag = false;
                List<UserRoleDetails> role = GetRoleByLocation(user, objClsLoginInfo.Location);
                //        $("#btnSubmit").attr("disabled", true);
                var RolesData = new[] { clsImplementationEnum.UserRoleName.QI3.GetStringValue(), clsImplementationEnum.UserRoleName.QC3.GetStringValue() };
                if ((from m in role where RolesData.Contains(m.UserRoleDesc) select m).Count() > 0
                    && count == 1)
                    flag = false;
                else
                    flag = true;

                if (role.Where(x => x.UserRoleDesc == clsImplementationEnum.UserRoleName.QC2.GetStringValue()).Count() > 0
                  )
                    flag = true;
                if (role.Where(x => x.UserRoleDesc == clsImplementationEnum.UserRoleName.QI2.GetStringValue()).Count() > 0
                 )
                    flag = true;
                if (role.Where(x => x.UserRoleDesc == clsImplementationEnum.UserRoleName.QI1.GetStringValue()).Count() > 0
                 )
                    flag = true;
                return Json(new
                {
                    counts = count,
                    data = flag,
                    FilesName = FileName
                });
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpGet]
        public JsonResult CheckFileExists1(string HeaderId, string tableName)
        {
            try
            {
                var user = objClsLoginInfo.UserName;
                string folderPath = "";
                string folderPath1 = tableName + "/" + HeaderId + "/";
                int Nofile = 0;
                for (int i = 1; i <= 1000; i++)
                {
                    folderPath = tableName + "/" + HeaderId + "/" + i.ToString();
                    var Files1 = (new clsFileUpload()).GetDocuments(folderPath);
                    if (Files1.Length > 0)
                    {
                        Nofile = Nofile + 1;
                    }
                    else
                    {
                        break;
                    }
                }
                folderPath = tableName + "/" + HeaderId + "/1";
                var Files = (new clsFileUpload()).GetDocuments(folderPath);
                string FileName = string.Empty;
                if (Files.Length > 0)
                {
                    for (int i = 0; i < Files.Length; i++)
                    {
                        FileName = Files[i].Name + "$" + FileName;
                    }
                    FileName = FileName.TrimEnd('$');
                }
                int count = Files.Length;
                bool flag = false;
                List<UserRoleDetails> role = GetRoleByLocation(user, objClsLoginInfo.Location);
                //        $("#btnSubmit").attr("disabled", true);
                var RolesData = new[] { clsImplementationEnum.UserRoleName.QI3.GetStringValue(), clsImplementationEnum.UserRoleName.QC3.GetStringValue() };
                if ((from m in role where RolesData.Contains(m.UserRoleDesc) select m).Count() > 0
                    && count == 1)
                    flag = false;
                else
                    flag = true;

                if (role.Where(x => x.UserRoleDesc == clsImplementationEnum.UserRoleName.QC2.GetStringValue()).Count() > 0
                  )
                    flag = true;
                if (role.Where(x => x.UserRoleDesc == clsImplementationEnum.UserRoleName.QI2.GetStringValue()).Count() > 0
                 )
                    flag = true;
                if (role.Where(x => x.UserRoleDesc == clsImplementationEnum.UserRoleName.QI1.GetStringValue()).Count() > 0
                 )
                    flag = true;
                return Json(new
                {
                    counts = Nofile,
                    data = flag,
                    FilesName = FileName
                });
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<UserRoleDetails> GetRoleByLocation(string User, string location)
        {
            //List<int> lstRole = (from a in db.ATH001
            //                     where a.Employee == User && a.Location == location
            //                     select a.Role).ToList();
            List<UserRoleDetails> role = (from a in db.ATH001
                                          join b in db.ATH004 on a.Role equals b.Id
                                          where a.Employee.Equals(User, StringComparison.OrdinalIgnoreCase) && a.Location.Equals(location)
                                          select new UserRoleDetails() { UserRoleId = a.Role, UserRoleDesc = b.Role }).Distinct().ToList();
            return role;
        }
        [HttpGet]
        public ActionResult DownloadSharePointFile(string fileName, string folderPath, string Type = "")
        {
            if (string.IsNullOrWhiteSpace(fileName) || string.IsNullOrWhiteSpace(folderPath))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var stream = (new clsFileUpload()).GetFileStream(folderPath, fileName);
            FileStreamResult fsr = new FileStreamResult(stream, "application/octet-stream");
            fsr.FileDownloadName = fileName;
            return fsr;
        }
        [HttpGet]
        public void DownloadPdfFile(string HeaderId, string tableName, string Nofile)
        {
            WebClient myWebClient = new WebClient();
            string Path1 = Request.Url.ToString().Replace(Request.Url.PathAndQuery, "");

            /*
            List<byte[]> Bytes = new List<byte[]>();
            for (int i = 1; i <= Convert.ToInt32(Nofile); i++)
            {

                byte[] fileBytes1 = myWebClient.DownloadData(Path1 + "/ImageHandler.ashx?type=SharePoint&fp=" + tableName + "/" + HeaderId + "/" + i.ToString());
                Bytes.Add(fileBytes1);

            }

            byte[] fileBytes = concatAndAddContent(Bytes);
            Response.Clear();
            MemoryStream ms = new MemoryStream(fileBytes);
            Response.ContentType = "application/pdf";
            //string folderPath = tableName + "/" + HeaderId + "/1";
            //var Files = (new clsFileUpload()).GetDocuments(folderPath);
            Response.AddHeader("content-disposition", "attachment;filename=" + tableName + ".pdf");
            Response.Buffer = true;
            ms.WriteTo(Response.OutputStream);
            Response.End();
            */

            Response.AddHeader("Content-Disposition", "attachment; filename=" + tableName + ".zip");
            Response.ContentType = "application/zip";
            //Convert Multiple pdf into zip file and download
            using (var zipStream = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(Response.OutputStream))
            {
                for (int i = 1; i <= Convert.ToInt32(Nofile); i++)
                {
                    byte[] fileBytes = myWebClient.DownloadData(Path1 + "/ImageHandler.ashx?type=SharePoint&fp=" + tableName + "/" + HeaderId + "/" + i.ToString());
                    string folderPath = tableName + "/" + HeaderId + "/" + i.ToString();
                    var Files = (new clsFileUpload()).GetDocuments(folderPath);
                    var fileEntry = new ICSharpCode.SharpZipLib.Zip.ZipEntry(Files[0].Name)
                    {
                        Size = fileBytes.Length
                    };

                    zipStream.PutNextEntry(fileEntry);
                    zipStream.Write(fileBytes, 0, fileBytes.Length);
                }

                zipStream.Flush();
                zipStream.Close();
            }




        }


        public static byte[] concatAndAddContent(List<byte[]> pdfByteContent)
        {

            using (var ms = new MemoryStream())
            {
                using (var doc = new Document())
                {
                    using (var copy = new PdfSmartCopy(doc, ms))
                    {
                        doc.Open();

                        //Loop through each byte array
                        foreach (var p in pdfByteContent)
                        {
                            // if(p[0]==0x25 && p[1]==0x50 && p[2]==0x44 && p[3] == 0x46)
                            //{
                            //Create a PdfReader bound to that byte array
                            if (IsPdf(p))
                            {
                                using (var reader = new PdfReader(p))
                                {

                                    //Add the entire document instead of page-by-page
                                    copy.AddDocument(reader);
                                }
                            }
                                
                            //}
                            
                        }

                        doc.Close();
                    }
                }

                //Return just before disposing
                return ms.ToArray();
            }
        }

        public static bool IsPdf(byte[] byArr)
        {
            bool YesPdf = true;
            PdfReader reader= null;
            try
            {
                reader = new PdfReader(byArr);
            }
            catch
            {
                YesPdf = false;
            }
            return YesPdf;
        }


        [HttpPost]
        public ActionResult GetUploadDocument(int? HeaderId)
        {
            QMS050 qMS050 = new QMS050();
            //if (HeaderId > 0)
            //    objPTR001_Log = db.PTR001_Log.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            qMS050.RequestId = Convert.ToInt32(HeaderId);
            return PartialView("_GetUploadDocument", qMS050);//, objPTR001_Log);
        }
        [HttpPost]
        public ActionResult GetReportFilters()
        {
            return PartialView("_GetReportFilters");
        }
        [HttpPost]
        public ActionResult GenerateExcelDetail(string QualityProject, string LTFPSNo)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            var data = (from i in db.LTF002
                        join j in db.QMS050 on i.LineId equals j.LTFPSHeaderId
                        join q in db.QMS001 on j.Project equals q.Project
                        where q.IsLTFPS == true && j.QualityProject == QualityProject && i.LTFPSNo == LTFPSNo
                        && j.TestResult == "Cleared"
                        select
 new
 {
     OPNNo = i.OperationNo,
     OPNDescription = i.OperationDescription,
     TestResult = j.TestResult,
     HeaderId = j.RequestId,
     TableName = "QMS050"
 }).Distinct().ToList().
Union(from i in db.LTF002
      join j in db.QMS055 on i.LineId equals j.LTFPSHeaderId
      join q in db.QMS001 on j.Project equals q.Project

      where q.IsLTFPS == true && j.QualityProject == QualityProject && i.LTFPSNo == LTFPSNo
      && j.TestResult == "Cleared"
      select new
      {
          OPNNo = i.OperationNo,
          OPNDescription = i.OperationDescription,
          TestResult = j.TestResult,
          HeaderId = j.RequestId,
          TableName = "QMS055"
      }).Distinct().ToList().Distinct().ToList();
            List<ExcelExport> lstExport = new List<ExcelExport>();
            foreach (var item in data)
            {
                ExcelExport excelExport = new ExcelExport();
                excelExport.OPNNo = Convert.ToString(item.OPNNo);
                excelExport.HeaderId = Convert.ToString(item.HeaderId);
                excelExport.TableName = Convert.ToString(item.TableName);
                excelExport.OPNDescription = item.OPNDescription;
                excelExport.TestResult = item.TestResult;
                lstExport.Add(excelExport);
            }
            string Path = Request.Url.ToString().Replace(Request.Url.PathAndQuery, "");

            string strFileName = GenerateExcelDetail(lstExport, objClsLoginInfo.UserName, QualityProject, LTFPSNo, Path);

            objResponseMsg.Key = true;
            objResponseMsg.Value = strFileName;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetQualityProjects()
        {
            List<ddlValue> lstQualityProject = new List<ddlValue>();
            //lstQualityProject = db.QMS050.Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList()
            //.Union(db.QMS055.Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList()).Distinct().ToList();
            lstQualityProject = (from i in db.LTF002
                                 join j in db.QMS050 on i.LineId equals j.LTFPSHeaderId
                                 join q in db.QMS001 on j.Project equals q.Project
                                 where q.IsLTFPS == true
                                 select
new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList().
Union(from i in db.LTF002
      join j in db.QMS055 on i.LineId equals j.LTFPSHeaderId
      join q in db.QMS001 on j.Project equals q.Project

      where q.IsLTFPS == true
      select new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList().Distinct().ToList();
            var returnlist = lstQualityProject.GroupBy(car => car.id).Select(g => g.First()).ToList();
            return Json(returnlist, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetLTFPSNo(string QualityProject)
        {
            List<ddlValue> lstQualityProject = new List<ddlValue>();
            lstQualityProject = (from i in db.LTF002
                                 join j in db.QMS050 on i.LineId equals j.LTFPSHeaderId
                                 join q in db.QMS001 on j.Project equals q.Project
                                 where j.QualityProject == QualityProject && q.IsLTFPS == true
                                 select
new ddlValue { id = i.LTFPSNo, text = i.LTFPSNo }).Distinct().ToList().
Union(from i in db.LTF002
      join j in db.QMS055 on i.LineId equals j.LTFPSHeaderId
      join q in db.QMS001 on j.Project equals q.Project

      where j.QualityProject == QualityProject && q.IsLTFPS == true
      select new ddlValue { id = i.LTFPSNo, text = i.LTFPSNo }).Distinct().ToList().Distinct().ToList();
            var returnlist = lstQualityProject.GroupBy(car => car.id).Select(g => g.First()).ToList();

            return Json(returnlist, JsonRequestBehavior.AllowGet);
        }
        //Bhadresh End
        public string GetActionLink(string FromTable, int RequestId, string from = "", string testResult = "")
        {
            string strAction = string.Empty;

            if (FromTable.ToUpper() == "PART")
            {
                if (testResult == "Cleared")
                    strAction += "<a title=\"View Details\" class='' href='" + WebsiteURL + "/IPI/PartlistOfferInspection/ViewOfferedDetails?RequestId=" + Convert.ToInt32(RequestId) + "&from=" + from + "'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title=\"Upload PDF\" class='' onclick='GetUploadDoc(" + RequestId + ")'><i style='margin-left:5px;' class='fa fa-upload'></i></a><a id='Download1' onclick='GetTableNames(" + RequestId + "); '><i style='margin-left:5px;' class='fa fa-download'></i></a>";
                else
                    strAction += "<a title=\"View Details\" class='' href='" + WebsiteURL + "/IPI/PartlistOfferInspection/ViewOfferedDetails?RequestId=" + Convert.ToInt32(RequestId) + "&from=" + from + "'><i style='margin-left:5px;' class='fa fa-eye'></i></a>";

            }
            else
            {
                if (testResult == "Cleared")
                    strAction += "<a href=\"" + WebsiteURL + "/IPI/MaintainSeamListOfferInspection/ApproveSeamListInspection/" + RequestId + "\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a><a title=\"Upload PDF\" class='' onclick='GetUploadDoc(" + RequestId + ")'><i style='margin-left:5px;' class='fa fa-upload'></i></a><a id='Download2' onclick='GetTableNames(" + RequestId + "); '><i style='margin-left:5px;' class='fa fa-download'></i></a>";
                else
                    strAction += "<a href=\"" + WebsiteURL + "/IPI/MaintainSeamListOfferInspection/ApproveSeamListInspection/" + RequestId + "\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>";
            }

            return strAction;
        }
        public string GetTPIActionLink(string FromTable, int RequestId, string from = "")
        {
            string strAction = string.Empty;

            if (FromTable.ToUpper() == "PART")
            {
                strAction += "<a title=\"View Details\" class='' href='" + WebsiteURL + "/IPI/PartlistOfferInspection/TPIViewDetails?RequestId=" + Convert.ToInt32(RequestId) + "&from=" + from + "'><i style='margin-left:5px;' class='fa fa-eye'></i></a>";
            }
            else
            {
                strAction += "<a href=\"" + WebsiteURL + "/IPI/MaintainSeamListOfferInspection/TPITestDetails/" + RequestId + "\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>";
            }

            return strAction;
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_FETCH_LTFPS_RE_OFFERINSPECTION_HEADERS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      li.QualityProject,
                                      li.BU,
                                      li.OperationNo,
                                      li.SeamNo,
                                      li.PartNo,
                                      li.StageCode,
                                      li.IterationNo,
                                      li.RequestNo,
                                      li.RequestNoSequence,
                                      li.TestResult,
                                      li.Shop,
                                      li.ShopLocation,
                                      li.ShopRemark
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public static string GenerateExcelDetail(List<ExcelExport> query, string userName, string QualityProject, string LTFPSNo, string path)
        {
            //try
            {
                using (ExcelPackage excelPackage = new ExcelPackage())
                {
                    ExcelWorksheet ws = excelPackage.Workbook.Worksheets.Add("Sheet1");
                    ws.Cells["A1"].Value = "Quality Project";
                    ws.Cells["B1"].Value = QualityProject;
                    ws.Cells["A2"].Value = "LTFPS No.";
                    ws.Cells["B2"].Value = LTFPSNo;

                    //Added by Dharmesh Vasani for Change Excel Column Title
                    ResourceManager rm = new ResourceManager("IEMQSImplementation.HeaderTitle", Assembly.GetExecutingAssembly());
                    //get our column headings
                    //var t = typeof(T);
                    //var Headings = t.GetProperties();
                    //for (int i = 0; i < Headings.Count(); i++)
                    //{
                    //    //Added by Dharmesh Vasani for Change Excel Column Title
                    //    ws.Cells[3, i + 1].Value = Headings[i].Name;// (!string.IsNullOrWhiteSpace(rm.GetString(Headings[i].Name)) ? rm.GetString(Headings[i].Name) : Headings[i].Name);
                    //    //For Date Format                        
                    //}
                    ws.Cells[3, 1].Value = "OPN NO";
                    ws.Cells[3, 2].Value = "OPN Description";
                    ws.Cells[3, 3].Value = "Test Result";
                    ws.Cells[3, 4].Value = "Document";

                    //populate our Data
                    //if (query.Count() > 0)
                    //{
                    //    ws.Cells["A4"].LoadFromCollection(query);
                    //}
                    //var data = query.Cast<ExcelExport>().ToList();
                    int startRow = 4;
                    if (query.Count() > 0)
                    {
                        //for (int innindex = 0; innindex < query.Count(); innindex++)
                        foreach (var item in query)
                        {
                            ws.Cells[startRow, 1].Value = item.OPNNo;
                            ws.Cells[startRow, 2].Value = item.OPNDescription;
                            ws.Cells[startRow, 3].Value = item.TestResult;
                            int DocPdf = 4;
                            // string folderPath = item.TableName + "/" + item.HeaderId + "/1", FileName = string.Empty;
                            int NoofFolders = NoOfFolders1(item.HeaderId, item.TableName);
                            string hyperlink = string.Empty;
                            string FileName1 = string.Empty;
                            if (NoofFolders > 0)
                            {
                                for (int i = 1; i <= NoofFolders; i++)
                                {
                                    string folderPath = item.TableName + "/" + item.HeaderId + "/" + i.ToString(), FileName = string.Empty;
                                    var Files = (new clsFileUpload()).GetDocuments(folderPath);
                                    if (Files.Length > 0)
                                    {
                                        string url = path + "/LTFPS/AttendInspection/DownloadSharePointFile?fileName=" + Files[0].Name + "&folderPath=" + item.TableName + "/" + item.HeaderId + "/1&Type=1";
                                        FileName1 = Files[0].Name;
                                        //hyperlink = String.Format(Files[0].URL, Type.Missing, "Document", "Document");
                                        hyperlink = String.Format(Files[0].URL, Type.Missing, "Document", "Document");
                                        if (!string.IsNullOrEmpty(hyperlink))
                                        {
                                            ws.Cells[startRow, DocPdf].Formula = "HYPERLINK(\"" + hyperlink + "\",\"" + FileName1 + "\")";
                                            DocPdf = DocPdf + 1;
                                        }
                                            
                                        //else
                                        //    ws.Cells[startRow, 4].Value = "";
                                       // startRow++;
                                    }
                                }
                                startRow++;
                            }
                            else
                            {
                                ws.Cells[startRow, 4].Value = "";
                                startRow++;
                            }
                        }
                    }

                    //End
                    string fileName;
                    string excelFilePath = ExcelFilePath(userName, out fileName);

                    ws.Cells.AutoFitColumns();

                    Byte[] bin = excelPackage.GetAsByteArray();
                    System.IO.File.WriteAllBytes(excelFilePath, bin);

                    return fileName;

                }
            }

        }

        public static string ExcelFilePath(string userName, out string fileName)
        {
            string folderPath = Path.Combine(System.Web.HttpContext.Current.Server.MapPath("~/Resources/Download/"));
            try
            {
                if (!Directory.Exists(folderPath))
                {
                    Directory.CreateDirectory(folderPath);
                }
                else
                {
                    DirectoryInfo directoryInfo = new DirectoryInfo(folderPath);

                    foreach (FileInfo file in directoryInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in directoryInfo.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                }
                fileName = string.Format("{0}_{1}.xlsx", userName, DateTime.Now.ToString("dd-MM-yyyy-hhmmss"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return Path.Combine(folderPath, fileName);
        }

        [HttpPost]
        public ActionResult UploadMultipleFiles()
        {
            string Massage = "Something went wrong";
            try
            {
                string TableName = Request["TableName"];
                string HeaderId = Request["HeaderId"];
                HttpFileCollectionBase files = Request.Files;

                string filePath = "";// TableName + "/" + HeaderId;
                string imageBlob = "";

                bool FileNameNotExisted = true;
                for (int i = 0; i < files.Count; i++)
                {
                    int Nofolders = NoOfFolders(HeaderId, TableName);
                    Nofolders = Nofolders + 1;
                    int existedFile = 0;
                    HttpPostedFileBase file = files[i];
                    string fname;
                    fname = file.FileName;


                    if (Nofolders > 0)
                    {
                        existedFile = CheckFileName(HeaderId, TableName, fname);
                        if (existedFile > 0)
                        {
                            Nofolders = existedFile;
                        }
                    }
                    if (FileNameNotExisted)
                    {
                        byte[] thePictureAsBytes = new byte[file.ContentLength];
                        using (BinaryReader theReader = new BinaryReader(file.InputStream))
                        {
                            thePictureAsBytes = theReader.ReadBytes(file.ContentLength);
                        }
                        imageBlob = Convert.ToBase64String(thePictureAsBytes);

                        //int base64 = imageBlob.IndexOf("base64,");
                        //imageBlob = imageBlob.Remove(0, base64 + 7);
                        var bytes = Convert.FromBase64String(imageBlob);
                        MemoryStream stream = new MemoryStream(bytes);
                        //var bytes = Convert.FromBase64String(imageBlob);
                        //var contents = new StreamContent(new MemoryStream(bytes));
                        clsFileUpload upload = new clsFileUpload();
                        filePath = TableName + "/" + HeaderId + "/" + (Nofolders).ToString();
                        bool t = upload.FileUpload(fname, filePath, stream, "", "", false);
                        if (t)
                        {
                            Massage = "Uploaded Successfully";
                        }
                    }


                }

            }
            catch (Exception ex)
            {
                Massage = ex.Message;
            }
            return Json(Massage, JsonRequestBehavior.AllowGet);

        }
        [HttpGet]
        protected void DownloadPdfFile1(string HeaderId, string tableName, string Nofile)
        {
            WebClient myWebClient = new WebClient();
            // Download home page data.
            // Download the Web resource and save it into a data buffer.
            string Path1 = Request.Url.ToString().Replace(Request.Url.PathAndQuery, "");
            //byte[] bytes = myWebClient.DownloadData(Path + "/ImageHandler.ashx?type=SharePoint&fp=" + tableName + "/" + HeaderId + "/1");
            Response.AddHeader("Content-Disposition", "attachment; filename=" + tableName + ".zip");
            Response.ContentType = "application/zip";

            using (var zipStream = new ICSharpCode.SharpZipLib.Zip.ZipOutputStream(Response.OutputStream))
            {
                for (int i = 1; i <= Convert.ToInt32(Nofile); i++)
                {
                    byte[] fileBytes = myWebClient.DownloadData(Path1 + "/ImageHandler.ashx?type=SharePoint&fp=" + tableName + "/" + HeaderId + "/" + i.ToString());
                    string folderPath = tableName + "/" + HeaderId + "/" + i.ToString();
                    var Files = (new clsFileUpload()).GetDocuments(folderPath);
                    var fileEntry = new ICSharpCode.SharpZipLib.Zip.ZipEntry(Files[0].Name)
                    {
                        Size = fileBytes.Length
                    };

                    zipStream.PutNextEntry(fileEntry);
                    zipStream.Write(fileBytes, 0, fileBytes.Length);
                }

                zipStream.Flush();
                zipStream.Close();
            }


        }

        [HttpGet]
        [Obsolete]
        public ActionResult DownloadLTFPS1(string QualityProject, string LTFPSNo)
        {
            var objResponseMsg = new clsHelper.ResponceMsgWithObject();
            //            var data = (from i in db.LTF002
            //                        join j in db.QMS050 on i.LineId equals j.LTFPSHeaderId
            //                        join q in db.QMS001 on j.Project equals q.Project
            //                        where q.IsLTFPS == true && j.QualityProject == QualityProject && i.LTFPSNo == LTFPSNo && j.TestResult == "Cleared"
            //                        select
            // new
            // {
            //     OPNNo = i.OperationNo,
            //     OPNDescription = i.OperationDescription,
            //     TestResult = j.TestResult,
            //     HeaderId = j.RequestId,
            //     TableName = "QMS050"
            // }).Distinct().ToList().
            //Union(from i in db.LTF002
            //      join j in db.QMS055 on i.LineId equals j.LTFPSHeaderId
            //      join q in db.QMS001 on j.Project equals q.Project

            //      where q.IsLTFPS == true && j.QualityProject == QualityProject && i.LTFPSNo == LTFPSNo && j.TestResult == "Cleared"
            //      select new
            //      {
            //          OPNNo = i.OperationNo,
            //          OPNDescription = i.OperationDescription,
            //          TestResult = j.TestResult,
            //          HeaderId = j.RequestId,
            //          TableName = "QMS055"
            //      }).Distinct().ToList().Distinct().ToList();
            var data = (from i in db.LTF002
                        join j in db.QMS050 on i.LineId equals j.LTFPSHeaderId
                        join q in db.QMS001 on j.Project equals q.Project
                        where q.IsLTFPS == true && j.QualityProject == QualityProject && i.LTFPSNo == LTFPSNo
                        && j.TestResult == "Cleared"
                        select
 new
 {
     OPNNo = i.OperationNo,
     OPNDescription = i.OperationDescription,
     TestResult = j.TestResult,
     HeaderId = j.RequestId,
     TableName = "QMS050"
 }).Distinct().ToList().
            Union(from i in db.LTF002
                  join j in db.QMS055 on i.LineId equals j.LTFPSHeaderId
                  join q in db.QMS001 on j.Project equals q.Project

                  where q.IsLTFPS == true && j.QualityProject == QualityProject && i.LTFPSNo == LTFPSNo
                  && j.TestResult == "Cleared"
                  select new
                  {
                      OPNNo = i.OperationNo,
                      OPNDescription = i.OperationDescription,
                      TestResult = j.TestResult,
                      HeaderId = j.RequestId,
                      TableName = "QMS055"
                  }).Distinct().ToList().Distinct().ToList();

            if (data.Count > 0)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append("<table border='1'>");
                sb.Append("<tr >");
                sb.Append("<td colspan='3' style='border: 1px solid #ccc '>Export to PDF Format</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan='3' style='border: 1px solid #ccc '>Quality Project No:" + QualityProject + "</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td colspan='3' style='border: 1px solid #ccc '>LTFPS No:" + LTFPSNo + "</td>");
                sb.Append("</tr>");
                sb.Append("<tr>");
                sb.Append("<td style='border: 1px solid #ccc '>OPN No</td>");
                sb.Append("<td style='border: 1px solid #ccc '>OPN Description</td>");
                sb.Append("<td style='border: 1px solid #ccc '>Test Result</td>");
                sb.Append("</tr>");

                string HeaderId = "";
                string ComHeaderIdTableName = "";
                string TableName = "";
                foreach (var item in data)
                {
                    string OperationNo = item.OPNNo.ToString();
                    string OPNDesc = item.OPNDescription.ToString();
                    string TestResult = item.TestResult.ToString();
                    HeaderId = item.HeaderId.ToString()+","+ HeaderId;
                    TableName = item.TableName.ToString();
                    ComHeaderIdTableName = item.HeaderId.ToString() + "&" + item.TableName.ToString() + "," + ComHeaderIdTableName;
                    sb.Append("<tr><td style='border: 1px solid #ccc '>" + OperationNo + "</td><td style='border: 1px solid #ccc '>" + OPNDesc + "</td><td style='border: 1px solid #ccc '>" + TestResult + "</td></tr>");
                }
                sb.Append("</table>");
                StringReader sr = new StringReader(sb.ToString());

                Document pdfDoc = new Document(PageSize.A4, 10f, 10f, 10f, 0f);
                HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    PdfWriter writer = PdfWriter.GetInstance(pdfDoc, memoryStream);
                    pdfDoc.Open();

                    htmlparser.Parse(sr);
                    pdfDoc.Close();

                    byte[] Inbytes = memoryStream.ToArray();
                    memoryStream.Close();

                    WebClient myWebClient = new WebClient();
                    string Path1 = Request.Url.ToString().Replace(Request.Url.PathAndQuery, "");


                    List<byte[]> Bytes = new List<byte[]>();
                    Bytes.Add(Inbytes);
                    int Nofile = 0;
                    ComHeaderIdTableName = ComHeaderIdTableName.TrimEnd(',');
                    string[] values = ComHeaderIdTableName.Split(',');// HeaderId.Split(',').Select(sValue => sValue.Trim()).ToArray();
                    for (int j = 0; j <values.Length; j++)
                    {
                        string TableAndHeader = values[j];
                        string Header= TableAndHeader.Substring(0, TableAndHeader.IndexOf("&"));
                        string TbalName = TableAndHeader.Substring(TableAndHeader.IndexOf("&")+1);
                        Nofile = NoOfFolders(Header.Trim(), TbalName.Trim());
                        //Nofile = NoOfFolders("1", "QMS055");
                        for (int i = 1; i <= Convert.ToInt32(Nofile); i++)
                        {

                            byte[] fileBytes1 = myWebClient.DownloadData(Path1 + "/ImageHandler.ashx?type=SharePoint&fp=" + TbalName + "/" + Header.Trim() + "/" + i.ToString());
                            Bytes.Add(fileBytes1);

                        }
                        break;
                    }
                    

                    byte[] fileBytes = concatAndAddContent(Bytes);
                    Response.Clear();
                    MemoryStream ms = new MemoryStream(fileBytes);
                    Response.ContentType = "application/pdf";
                    //string folderPath = tableName + "/" + HeaderId + "/1";
                    //var Files = (new clsFileUpload()).GetDocuments(folderPath);
                    Response.AddHeader("content-disposition", "attachment;filename=" + TableName + ".pdf");
                    Response.Buffer = true;
                    ms.WriteTo(Response.OutputStream);
                    Response.End();
                    return RedirectToAction("Index", "AttendInspection");
                   // return Json(new { Url.Action("Index", "AttendInspection") });
                }
            }
            else
            {
                return RedirectToAction("Index", "AttendInspection");
                //"window.Location='AttendInspection/Index'"
            }

        }

       

        public int NoOfFolders(string HeaderId, string tableName)
        {
            try
            {
                var user = objClsLoginInfo.UserName;
                string folderPath = "";
                string folderPath1 = tableName + "/" + HeaderId + "/";
                int Nofile = 0;
                for (int i = 1; i <= 1000; i++)
                {
                    folderPath = tableName + "/" + HeaderId + "/" + i.ToString();
                    var Files1 = (new clsFileUpload()).GetDocuments(folderPath);
                    if (Files1.Length > 0)
                    {
                        Nofile = Nofile + 1;
                    }
                    else
                    {
                        break;
                    }
                }
                return Nofile;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static int NoOfFolders1(string HeaderId, string tableName)
        {
            try
            {
               // var user = objClsLoginInfo.UserName;
                string folderPath = "";
                string folderPath1 = tableName + "/" + HeaderId + "/";
                int Nofile = 0;
                for (int i = 1; i <= 1000; i++)
                {
                    folderPath = tableName + "/" + HeaderId + "/" + i.ToString();
                    var Files1 = (new clsFileUpload()).GetDocuments(folderPath);
                    if (Files1.Length > 0)
                    {
                        Nofile = Nofile + 1;
                    }
                    else
                    {
                        break;
                    }
                }
                return Nofile;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int CheckFileName(string HeaderId, string tableName, string FileName)
        {
            try
            {
                var user = objClsLoginInfo.UserName;
                string folderPath = "";
                string folderPath1 = tableName + "/" + HeaderId + "/";
                int Nofile = 0;
                int returnVal = 0;
                for (int i = 1; i <= 1000; i++)
                {
                    folderPath = tableName + "/" + HeaderId + "/" + i.ToString();
                    var Files1 = (new clsFileUpload()).GetDocuments(folderPath);
                    if (Files1.Length > 0)
                    {
                        Nofile = Nofile + 1;
                        if (Files1[0].Name == FileName)
                        {
                            returnVal = Nofile;
                            break;
                        }

                    }
                    else
                    {
                        break;
                    }
                }
                return returnVal;
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
public class ExcelExport
{
    public string OPNNo { get; set; }
    public string OPNDescription { get; set; }
    public string TestResult { get; set; }
    public string TableName { get; set; }
    public string HeaderId { get; set; }
}