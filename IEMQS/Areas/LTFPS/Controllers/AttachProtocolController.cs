﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.Utility.Models;
using System.Data;

namespace IEMQS.Areas.LTFPS.Controllers
{
    public class AttachProtocolController : clsBase
    {
        // GET: LTFPS/AttachProtocol
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "Attach Protocol LTFPS";
            return View();
        }

        #region Index Page
        public ActionResult GetIndexGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetIndexGridDataPartial");
        }

        public ActionResult LoadLTFPSIndexDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "ltf001.BU", "ltf001.Location");

                if (status.ToUpper() == "PENDING")
                    whereCondition += " and (ltf001.IsProtocolAttched = 0 or ltf001.IsProtocolAttched is null) and ltf001.Status = '" + clsImplementationEnum.LTFPSHeaderStatus.Released.GetStringValue() + "' ";
                else
                    whereCondition += " and ltf001.Status = '" + clsImplementationEnum.LTFPSHeaderStatus.Released.GetStringValue() + "' ";

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (ltf001.QualityProject like '%" + param.sSearch + "%' or (ltf001.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or (LTRIM(RTRIM(ltf001.Location))+'-'+LTRIM(RTRIM(lcom2.t_desc))) like '%" + param.sSearch + "%' or (LTRIM(RTRIM(ltf001.BU))+'-'+LTRIM(RTRIM(bcom2.t_desc))) like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                //  var lstHeader = db.SP_IPI_GetQualityIDHeader(startIndex, endIndex, "", whereCondition).ToList();
                var lstHeader = db.SP_LTFPS_GET_PROJECT_INDEX_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        a.QualityProject,
                        a.Project,
                        a.BU,
                        a.Location,
                        Helper.HTMLActionString(Convert.ToInt32(a.HeaderId), "View", "View Details", "fa fa-eye", "", "/LTFPS/AttachProtocol/Details/" + Convert.ToString(a.HeaderId))
                    }.Distinct();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region Header Details Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id)
        {
            LTF001 objLTF001 = new LTF001();

            if (id > 0)
            {
                objLTF001 = db.LTF001.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objLTF001.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objLTF001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objLTF001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            else
            {
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objLTF001.Location = objClsLoginInfo.Location;
            }

            ViewBag.Title = "Attach Protocol LTFPS";

            return View(objLTF001);
        }

        public ActionResult GetLTFPSHeaderDataPartial(string Status, string QualityProject, string Location, string BU)
        {
            ViewBag.status = Status;
            ViewBag.qualityProject = QualityProject;
            ViewBag.location = Location;
            ViewBag.bu = BU;
            string status = clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue();
            if (db.LTF001.Any(x => x.QualityProject.Equals(QualityProject) && x.Location.Equals(Location) && x.BU.Equals(BU) && (x.RevNo > 0 || !x.Status.Equals(status) || x.LTF003.Any())))
                ViewBag.isIdentProjectEditable = false;
            else
                ViewBag.isIdentProjectEditable = true;

            return PartialView("_GetLTFPSHeaderDataPartial");
        }

        [HttpPost]
        public ActionResult GetLTFPSHeaderData(JQueryDataTableParamModel param, string qualityProj, string status, string location, string bu)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "ltf001.BU", "ltf001.Location");
                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(qualityProj))
                {
                    whereCondition += " AND UPPER(ltf001.QualityProject) = '" + qualityProj.Trim().ToUpper() + "' AND UPPER(ltf001.Location) = '" + location.Trim().ToUpper() + "'  AND UPPER(ltf001.BU) = '" + bu.Trim().ToUpper() + "' ";
                }

                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and (ltf001.IsProtocolAttched = 0 or ltf001.IsProtocolAttched is null) and ltf001.Status = '" + clsImplementationEnum.LTFPSHeaderStatus.Released.GetStringValue() + "' ";
                }
                else
                    whereCondition += " and ltf001.Status = '" + clsImplementationEnum.LTFPSHeaderStatus.Released.GetStringValue() + "' ";

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (ltf001.LTFPSNo like '%" + param.sSearch
                        + "%' or ltf001.RevNo like '%" + param.sSearch
                        + "%' or ltf001.Status like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_LTFPS_GET_HEADER_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Convert.ToString(uc.LTFPSNo),
                               uc.LTFPSDescription,
                               uc.IsReworkLTFPS == true? "YES":"NO",
                               "R" + Convert.ToString(uc.RevNo),
                               Convert.ToString(uc.Status),
                               Helper.HTMLActionString(uc.HeaderId,"CheckInOut",uc.IsProtocolAttched == true?"Check Out":"Check In" ,uc.IsProtocolAttched == true?"fa fa-check-square-o":"fa fa-square-o", "UpdateIsProtocolAttched("+ uc.HeaderId +",false);","",uc.IsProtocolAttched != true)+
                               Helper.HTMLActionString(uc.HeaderId,"Timeline","Timeline","fa fa-clock-o", "ViewCheckInOutHistory(\'"+ uc.LTFPSNo +"\');")
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateIsProtocolAttched(int headerId, bool IsProtocolAttched)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var Released = clsImplementationEnum.LTFPSHeaderStatus.Released.GetStringValue();
                var objLTF001 = db.LTF001.FirstOrDefault(f => f.HeaderId == headerId && f.Status == Released);
                objLTF001.IsProtocolAttched = IsProtocolAttched;
                var LTF005 = new LTF005()
                {
                    BU = objLTF001.BU,
                    Location = objLTF001.Location,
                    LTFPSNo = objLTF001.LTFPSNo,
                    Operation = "Check " + (IsProtocolAttched == true ? "In" : "Out"),
                    OperationBy = objClsLoginInfo.UserName,
                    OperationOn = DateTime.Now,
                    Project = objLTF001.Project,
                    QualityProject = objLTF001.QualityProject,
                    RevNo = objLTF001.RevNo
                };
                db.LTF005.Add(LTF005);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "LTFPS Checked " + (IsProtocolAttched ? "In" : "Out") + " successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "LTFPS Check " + (IsProtocolAttched ? "In" : "Out") + " operation fail...!";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SubmitLTFPSHeader(string strHeader)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<LTF001> lstLTF001s = new List<LTF001>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    List<string> docReuiredLTFPS = new List<string>();
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    var objFileUpload = new clsFileUpload();

                    //foreach (int headerId in headerIds)
                    //{
                    //    var objLTF002 = db.LTF002.Where(x => x.HeaderId == headerId && x.DocumentRequired);
                    //    foreach (var item in objLTF002)
                    //    {
                    //        string folderPath = "LTF002/" + item.LineId + "/R" + item.LTFPSRevNo;
                    //        if (!objFileUpload.CheckAttachmentUpload( folderPath))
                    //        {
                    //            docReuiredLTFPS.Add(item.LTFPSNo);
                    //            break;
                    //        }
                    //    }                       
                    //}

                    //if (docReuiredLTFPS.Count > 0)
                    //{
                    //    objResponseMsg.Key = false;
                    //    objResponseMsg.Value = "LTFPS " + String.Join(",", docReuiredLTFPS) + " not submitted because of required protocol not attached";
                    //}
                    //else
                    //{
                    foreach (int headerId in headerIds)
                    {
                        UpdateIsProtocolAttched(headerId, true);
                        #region Ready to Offer
                        db.SP_LTFPS_RELEASE_READY_TO_OFFER(headerId);
                        #endregion
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "LTFPS has been sucessfully submitted";
                    //}
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record sending for approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetCheckInOuHistoryDataPartial(string LTFPSNo)
        {
            ViewBag.LTFPSNo = LTFPSNo;
            List<LTF005> lstLTF005 = db.LTF005.Where(x => x.LTFPSNo == LTFPSNo).ToList();
            var data = (from ltf005 in lstLTF005
                        join com003 in db.COM003 on ltf005.OperationBy equals com003.t_psno
                        orderby ltf005.OperationOn descending
                        select new LTF005()
                        {
                            LTFPSNo = LTFPSNo,
                            RevNo = ltf005.RevNo,
                            Operation = ltf005.Operation,
                            OperationBy = ltf005.OperationBy + " - " + com003.t_name,
                            OperationOn = ltf005.OperationOn
                        }).ToList();
            return PartialView("_GetCheckInOutHistoryDataPartial", data);
        }
        #endregion

        #region Lines Details
        [HttpPost]
        public ActionResult GetLTFPSLinesPartial(int headerId)
        {
            LTF001 objLTF001 = new LTF001();

            if (headerId > 0)
                objLTF001 = db.LTF001.Where(i => i.HeaderId == headerId).FirstOrDefault();

            ViewBag.DocumentRequired = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "1", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "0", CategoryDescription = "No" } };
            ViewBag.TestFor = new List<CategoryData> { new CategoryData { Value = "Seam", Code = "Seam", CategoryDescription = "Seam" }, new CategoryData { Value = "Part", Code = "Part", CategoryDescription = "Part" }, new CategoryData { Value = "Assembly", Code = "Assembly", CategoryDescription = "Assembly" } };
            ViewBag.TestType = Manager.GetSubCatagories("LTFPS Test Type", objLTF001.BU, objLTF001.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.Agency = Manager.GetSubCatagories("LTFPS Agency", objLTF001.BU, objLTF001.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.TPI = Manager.GetSubCatagories("LTFPS TPI", objLTF001.BU, objLTF001.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();

            return PartialView("_GetLTFPSLinesDataPartial", objLTF001);
        }

        [HttpPost]
        public ActionResult GetLTFPSLinesData(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (ltf002.OperationNo like '%" + param.sSearch
                        + "%' or ltf002.RefDocument like '%" + param.sSearch
                        + "%' or ltf002.TestFor like '%" + param.sSearch
                        + "%' or ltf002.SeamNo like '%" + param.sSearch
                        + "%' or ltf002.PartNo like '%" + param.sSearch
                        + "%' or ltf002.OperationDescription like '%" + param.sSearch
                        + "%' or ltf002.TestType like '%" + param.sSearch
                        + "%' or ltf002.Stage like '%" + param.sSearch
                        + "%' or ltf002.Agency like '%" + param.sSearch
                        + "%' or ltf002.TPI like '%" + param.sSearch
                        + "%' or ltf002.DocumentRequired like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_LTFPS_GET_LINES_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                LTF001 objLTF001 = db.LTF001.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();
                //bool isEditable = (objLTF001.Status == clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue() || objLTF001.Status == clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue());

                List<SelectListItem> TestForList = new List<SelectListItem>() { new SelectListItem { Text = "Seam", Value = "Seam" }, new SelectListItem { Text = "Part", Value = "Part" }, new SelectListItem { Text = "Assembly", Value = "Assembly" } };
                List<SelectListItem> booleanList = new List<SelectListItem>() { new SelectListItem { Text = "No", Value = "0" }, new SelectListItem { Text = "Yes", Value = "1" } };
                List<SelectListItem> AgencyList = new List<SelectListItem>() { new SelectListItem { Text = "M1", Value = "M1" }, new SelectListItem { Text = "X", Value = "X" }, new SelectListItem { Text = "H", Value = "H" }, new SelectListItem { Text = "W", Value = "W" }, new SelectListItem { Text = "R", Value = "R" }, new SelectListItem { Text = "N", Value = "N" } };
                List<SelectListItem> TPIList = new List<SelectListItem>() { new SelectListItem { Text = "A1", Value = "A1" }, new SelectListItem { Text = "A2", Value = "A2" }, new SelectListItem { Text = "A3", Value = "A3" }, new SelectListItem { Text = "X", Value = "X" }, new SelectListItem { Text = "H", Value = "H" }, new SelectListItem { Text = "W", Value = "W" }, new SelectListItem { Text = "R", Value = "R" }, new SelectListItem { Text = "N", Value = "N" } };
                List<SelectListItem> TestTypeList = new List<SelectListItem>() { new SelectListItem { Text = "DT", Value = "DT" }, new SelectListItem { Text = "PT", Value = "PT" }, new SelectListItem { Text = "UT", Value = "UT" }, new SelectListItem { Text = "RT", Value = "RT" }, new SelectListItem { Text = "MT", Value = "MT" } };

                var StageList = (from a in db.QMS002
                                 where a.BU == objLTF001.BU && a.Location == objLTF001.Location
                                 select new { Code = a.StageCode, Desc = a.StageDesc }).ToList();

                var objFileUpload = new clsFileUpload();
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.OperationNo),
                               Convert.ToString(uc.RefDocument),
                               Convert.ToString(uc.DocRevNo),
                               uc.TestFor,
                               uc.SeamNo,
                               uc.PartNo,
                               Convert.ToString(uc.OperationDescription),
                               uc.TestType,
                               uc.Stage,
                               uc.Agency,
                               uc.TPI,
                               (uc.DocumentRequired ? "Yes" : "No"),
                               "<nobr>" + //HTMLActionString(uc.LineId, "Delete", "Delete Operation", "fa fa-trash-o", "DeleteLTFPSLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +")", "", !isEditable || (uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()) || uc.IsOperationOffered.Value) +
                               Helper.HTMLActionString(uc.LineId,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\'/LTFPS/MaintainLTFPS/ShowTimeline?HeaderID="+ uc.HeaderId +"&LineId="+ uc.LineId +"\');") +
                               Helper.GenerateActionIcon(uc.LineId,"Attachment","Attach Protocol","fa fa-paperclip", "ViewAttachment(" +uc.LineId+","+uc.LTFPSRevNo+","+(!uc.IsOperationOffered.Value && objLTF001.IsProtocolAttched != true ?"true":"false")+");",inputstyle: (uc.IsOperationOffered.Value || !uc.DocumentRequired || objFileUpload.CheckAttachmentUpload("LTF002/" + uc.LineId + "/R"+ uc.LTFPSRevNo)? "":"color:red"))
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Timeline
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "LTFPS Timeline";

            if (LineId > 0)
            {
                model.Title = "LTFPS Lines";
                LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == LineId).FirstOrDefault();
                model.CreatedBy = objLTF002.CreatedBy != null ? Manager.GetUserNameFromPsNo(objLTF002.CreatedBy) : null;
                model.CreatedOn = objLTF002.CreatedOn;
                model.EditedBy = objLTF002.EditedBy != null ? Manager.GetUserNameFromPsNo(objLTF002.EditedBy) : null;
                model.EditedOn = objLTF002.EditedOn;
            }
            else
            {
                model.Title = "LTFPS Header";
                LTF001 objLTF001 = db.LTF001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objLTF001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.CreatedBy) : null;
                model.CreatedOn = objLTF001.CreatedOn;
                model.EditedBy = objLTF001.EditedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.EditedBy) : null;
                model.EditedOn = objLTF001.EditedOn;
                model.SubmittedBy = objLTF001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.SubmittedBy) : null;
                model.SubmittedOn = objLTF001.SubmittedOn;
                if (objLTF001.WEReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.WEReturnedBy);
                    model.ReturnedOn = objLTF001.WEReturnedOn;
                }
                else if (objLTF001.QCReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.QCReturnedBy);
                    model.ReturnedOn = objLTF001.QCReturnedOn;
                }
                else if (objLTF001.CustomerReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.CustomerReturnedBy);
                    model.ReturnedOn = objLTF001.CustomerReturnedOn;
                }
                else if (objLTF001.PMGReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.PMGReturnedBy);
                    model.ReturnedOn = objLTF001.PMGReturnedOn;
                }
                else
                {
                    model.ReturnedBy = null;
                    model.ReturnedOn = null;
                }
                model.WEApprovedBy = objLTF001.WEApprovedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.WEApprovedBy) : null;
                model.WEApprovedOn = objLTF001.WEApprovedOn;
                model.QCApprovedBy = objLTF001.QCApprovedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.QCApprovedBy) : null;
                model.QCApprovedOn = objLTF001.QCApprovedOn;
                model.CustomerApprovedBy = objLTF001.CustomerApprovedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.CustomerApprovedBy) : null;
                model.CustomerApprovedOn = objLTF001.CustomerApprovedOn;
                model.PMGReleaseBy = objLTF001.PMGReleaseBy != null ? Manager.GetUserNameFromPsNo(objLTF001.PMGReleaseBy) : null;
                model.PMGReleaseOn = objLTF001.PMGReleaseOn;

            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "LTFPS Timeline";

            if (LineId > 0)
            {
                model.Title = "LTFPS Lines";
                LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == LineId).FirstOrDefault();
                model.CreatedBy = objLTF002.CreatedBy != null ? Manager.GetUserNameFromPsNo(objLTF002.CreatedBy) : null;
                model.CreatedOn = objLTF002.CreatedOn;
                model.EditedBy = objLTF002.EditedBy != null ? Manager.GetUserNameFromPsNo(objLTF002.EditedBy) : null;
                model.EditedOn = objLTF002.EditedOn;
            }
            else
            {
                model.Title = "LTFPS Header";
                LTF001 objLTF001 = db.LTF001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objLTF001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.CreatedBy) : null;
                model.CreatedOn = objLTF001.CreatedOn;
                model.EditedBy = objLTF001.EditedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.EditedBy) : null;
                model.EditedOn = objLTF001.EditedOn;
                model.SubmittedBy = objLTF001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.SubmittedBy) : null;
                model.SubmittedOn = objLTF001.SubmittedOn;
                if (objLTF001.WEReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.WEReturnedBy);
                    model.ReturnedOn = objLTF001.WEReturnedOn;
                }
                else if (objLTF001.QCReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.QCReturnedBy);
                    model.ReturnedOn = objLTF001.QCReturnedOn;
                }
                else if (objLTF001.CustomerReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.CustomerReturnedBy);
                    model.ReturnedOn = objLTF001.CustomerReturnedOn;
                }
                else if (objLTF001.PMGReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.PMGReturnedBy);
                    model.ReturnedOn = objLTF001.PMGReturnedOn;
                }
                else
                {
                    model.ReturnedBy = null;
                    model.ReturnedOn = null;
                }
                model.WEApprovedBy = objLTF001.WEApprovedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.WEApprovedBy) : null;
                model.WEApprovedOn = objLTF001.WEApprovedOn;
                model.QCApprovedBy = objLTF001.QCApprovedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.QCApprovedBy) : null;
                model.QCApprovedOn = objLTF001.QCApprovedOn;
                model.CustomerApprovedBy = objLTF001.CustomerApprovedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.CustomerApprovedBy) : null;
                model.CustomerApprovedOn = objLTF001.CustomerApprovedOn;
                model.PMGReleaseBy = objLTF001.PMGReleaseBy != null ? Manager.GetUserNameFromPsNo(objLTF001.PMGReleaseBy) : null;
                model.PMGReleaseOn = objLTF001.PMGReleaseOn;

            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion

        #region Common Methods

        [HttpPost]
        public ActionResult GetQualityProjectWiseProjectBUDetail(string qualityProject)
        {
            QualityProjectsDetails objProjectsDetails = new QualityProjectsDetails();
            var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();

            objProjectsDetails.ProjectCode = project;
            objProjectsDetails.projectDescription = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;

            var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);

            objProjectsDetails.BUCode = ProjectWiseBULocation.QCPBU.Split('-')[0].Trim();
            objProjectsDetails.BUDescription = ProjectWiseBULocation.QCPBU;

            objProjectsDetails.LocationCode = objClsLoginInfo.Location;
            objProjectsDetails.LocationDescription = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

            return Json(objProjectsDetails, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetIdenticalProjectsEdit(string param, string project, string location)
        {
            //List<ddlValue> identicalProject = new List<ddlValue>();
            var identicalProject = db.LTF004.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase)).Select(i => i.IdenticalProject).Distinct().ToList();
            return Json(identicalProject, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}