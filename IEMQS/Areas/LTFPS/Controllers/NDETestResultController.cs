﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.LTFPS.Controllers
{
    public class NDETestResultController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetTestResultDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetTestResultDataPartial");
        }

        public ActionResult LoadTestResultDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1 ";
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and (tm.RequestGeneratedBy != '' or tm.RequestGeneratedBy is not null) and (tm.ConfirmedBy != '' or tm.ConfirmedBy is not null) and (tm.TestResult is null or tm.TestResult = '') and RequestStatus='Not Attended' and ISNULL(tm.QCIsReturned,0)=0 and ISNULL(tm.NDEIsReturned,0)=0 ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = {
                                                "QualityProject",
                                                "dbo.GET_PROJECTDESC_BY_CODE(tm.Project)",
                                                "SeamNo",
                                                "PartAssemblyFindNumber",
                                                "PartAssemblyChildItemCode",
                                                "StageCode",
                                                "RequestStatus",
                                                "Jointtype",
                                                "Shop",
                                                "ShopLocation",
                                                "LTFPSNo",
                                                "InspectedOn",
                                                "InspectionStatus",
                                                "OperationNo"
                                           };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else if (status.ToUpper() == "TPI")
                {
                    whereCondition += " and TPIResultPending=1 and RequestStatus='Attended' ";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty; ;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_LTFPS_FETCH_REQUESTCONFIRMATION_HEADERS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           Convert.ToString(h.QualityProject),
                           Convert.ToString(h.Project),
                           Convert.ToString(h.RequestNo),
                           Convert.ToString(h.SeamNo),
                           Convert.ToString(h.PartAssemblyFindNumber),//Position No.
                           Convert.ToString(h.PartAssemblyChildItemCode),//Part No.
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString(h.LTFPSNo),
                           h.InspectedOn==null || h.InspectedOn.Value==DateTime.MinValue?"":h.InspectedOn.Value.ToString("dd/MM/yyyy" ,CultureInfo.InvariantCulture),
                           Convert.ToString(h.InspectionStatus),
                           Convert.ToString(h.OperationNo),
                           Convert.ToString(h.OfferedQuantity),
                           Convert.ToString(h.RequestNoSequence),
                           Convert.ToString(h.RequestStatus),
                           Convert.ToString(h.SeamLength),
                           Convert.ToString(h.OverlayArea),
                           Convert.ToString(h.OverlayThickness),
                           Convert.ToString(h.Jointtype),
                           Convert.ToString(h.ManufacturingCode),
                           Convert.ToString(h.Part1Position),
                           Convert.ToString(h.Part1thickness),
                           Convert.ToString(h.Part1Material),
                           Convert.ToString(h.Part2Position),
                           Convert.ToString(h.Part2thickness),
                           Convert.ToString(h.Part2Material),
                           Convert.ToString(h.Part3Position),
                           Convert.ToString(h.Part3thickness),
                           Convert.ToString(h.Part3Material),
                           Convert.ToString(h.AcceptanceStandard),
                           Convert.ToString(h.ApplicableSpecification),
                           Convert.ToString(h.InspectionExtent),
                           Convert.ToString(h.WeldingEngineeringRemarks),
                           Convert.ToString(h.WeldingProcess),
                           Convert.ToString(h.SeamStatus),
                           Convert.ToString(h.Shop),
                           Convert.ToString(h.ShopLocation),
                           Convert.ToString(h.ShopRemark),
                           Convert.ToString(h.SurfaceCondition),
                           Convert.ToString(h.WeldThicknessReinforcement),
                           Convert.ToString(h.SpotGenerationtype),
                           Convert.ToString(h.SpotLength),
                           Convert.ToString(h.PrintSummary),
                           Convert.ToString(h.NDETechniqueNo),
                           Convert.ToString(h.NDETechniqueRevisionNo),
                          "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\">"+GetActionLink(h.Tablename,h.RequestId,"") +  GenerateReportIcon(h.Tablename, h.RequestId) + Manager.generateLTFPSGridButtons(h.LTFPSNo,h.QualityProject,h.Project,h.BU,h.Location,!string.IsNullOrWhiteSpace(h.SeamNo)?h.SeamNo:h.PartAssemblyFindNumber,h.StageCode, Convert.ToString(h.OperationNo),!string.IsNullOrWhiteSpace(h.SeamNo)?"Seam Details":"Part Details",Convert.ToString(true),!string.IsNullOrWhiteSpace(h.SeamNo)?"true":"false",!string.IsNullOrWhiteSpace(h.SeamNo)?"Seam Details":"Part Details",!string.IsNullOrWhiteSpace(h.SeamNo)?"Seam Request Details":"Part Request Details")+"</center>"

                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GenerateReportIcon(string TableName, int RequestId)
        {
            string ActionHTML = string.Empty;
            string ReportType = string.Empty;

            QMS060 objQMS060 = db.QMS060.FirstOrDefault(c => c.RequestId == RequestId);
            if (objQMS060 != null && TableName.ToUpper() == "SEAM" && !string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) && objQMS060.NDETechniqueRevisionNo.HasValue)
            {
                QMS002 objQMS002 = db.QMS002.FirstOrDefault(c => c.BU == objQMS060.BU && c.Location == objQMS060.Location && c.StageCode == objQMS060.StageCode);
                if (objQMS002 != null)
                {
                    if (objQMS002.StageType == "RT" || objQMS002.StageType == "UT")
                    {
                        if (objQMS002.StageType == "RT")
                        {
                            ReportType = "RT";
                        }
                        else
                        {
                            ReportType = "ASCAN";
                        }
                        ActionHTML = "onclick=\"OpenNDEReport('" + ReportType + "','" + objQMS060.QualityProject + "','" + objQMS002.StageType + "','" + objQMS060.SeamNo + "','" + objQMS060.StageCode + "','" + objQMS060.StageSequence + "','" + objQMS060.IterationNo + "','" + objQMS060.RequestNo + "')\"";
                    }
                }
            }

            string NDEDetailsReport = TableName.ToUpper() == "SEAM" ? "<a onclick=\"OpenNDERequestDetails(" + RequestId + ")\" Title=\"Request Details\" ><i style=\"margin-left:5px;\" class=\"fa fa-newspaper-o\"></i></a>" : "<a  Title=\"Request Details\" ><i style=\"margin-left:5px;cursor: pointer; opacity:0.3;\" class=\"fa fa-newspaper-o\"></i></a>";

            if (!string.IsNullOrWhiteSpace(ActionHTML))
            {
                return "<a " + ActionHTML + " Title=\"View Report\" ><i style=\"margin-left:5px;\" class=\"fa fa-print\"></i></a>" + NDEDetailsReport;
            }
            else
            {
                return "<a Title=\"Report Not Available\" ><i style=\"cursor: pointer; opacity:0.3; margin-left:5px;\" class=\"fa fa-print\"></i></a>" + NDEDetailsReport;
            }

        }

        public string GetActionLink(string FromTable, int RequestId, string from = "")
        {
            string strAction = string.Empty;

            if (FromTable.ToUpper() == "PART" || FromTable.ToUpper() == "ASSEMBLY")
            {
                strAction += "<a title=\"View Details\" class='' href='" + WebsiteURL + "/IPI/PartlistNDEInspection/NDEViewDetails?RequestId=" + Convert.ToInt32(RequestId) + "'><i class='fa fa-eye'></i></a>";
            }
            else
            {
                strAction += "<a href=\"" + WebsiteURL + "/IPI/SeamListNDEInspection/NDESeamTestDetails/" + RequestId + "\" Title=\"View Details\" ><i style=\"margin-left:5px;\" class=\"fa fa-eye\"></i></a>";
            }
            return strAction;
        }
    }
}