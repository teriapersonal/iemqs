﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.LTFPS.Controllers
{
    public class NDEReportController : clsBase
    {
        // GET: LTFPS/NDEReport
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetProjectResult(string term)
        {
            var lstQualityProject = (from a in db.LTF002
                                     where a.QualityProject.Contains(term)
                                     select new { qualityCode = a.QualityProject, qualityDescription = a.QualityProject }).Distinct().ToList();
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetSeamNoResult(string term)
        {
            var lstRequestNo = (from a in db.LTF002
                                where a.SeamNo.Contains(term)
                                select new { seamNo = a.SeamNo }).Distinct().ToList();
            return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
        }
    }
}