﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.Utility.Models;
using System.IO;
using OfficeOpenXml;
using System.Data;
using System.Data.Entity.SqlServer;
using IEMQS.Areas.NDE.Models;

namespace IEMQS.Areas.LTFPS.Controllers
{
    public class MaintainLTFPSController : clsBase
    {
        // GET: LTFPS/MaintainLTFPS
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

            if (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Releaser.GetStringValue())
                ViewBag.Title = "Release LTFPS";
            else if (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                ViewBag.Title = "Approve LTFPS";
            else
                ViewBag.Title = "Maintain LTFPS";

            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            return View();
        }

        #region Index Page
        public ActionResult GetIndexGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetIndexGridDataPartial");
        }

        public ActionResult LoadLTFPSIndexDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "ltf001.BU", "ltf001.Location");

                if (status.ToUpper() == "PENDING")
                {
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PMG2.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Releaser.GetStringValue())
                    {
                        whereCondition += " and ltf001.CustomerApprovedBy IS NOT NULL and ltf001.PMGReleaseBy IS NULL and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.SentForApproval.GetStringValue() + "')";
                    }
                    else if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.CR1.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                    {
                        //whereCondition += " and ltf001.QCApprovedBy IS NOT NULL and ltf001.CustomerApprovedBy IS NULL and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue() + "')";
                        whereCondition += " and ltf001.CustomerApprovedBy IS NULL and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.SentForApproval.GetStringValue() + "')";
                    }
                    //else if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                    //{
                    //    whereCondition += " and ltf001.WEApprovedBy IS NOT NULL and ltf001.QCApprovedBy IS NULL and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue() + "')";
                    //}
                    //else if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.WE2.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                    //{
                    //    whereCondition += " and ltf001.SubmittedBy IS NOT NULL and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.SentForApproval.GetStringValue() + "')";
                    //}
                    else if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PMG3.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue())
                    {
                        whereCondition += " and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue() + "')";
                    }
                }

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (ltf001.QualityProject like '%" + param.sSearch + "%' or (ltf001.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or (LTRIM(RTRIM(ltf001.Location))+'-'+LTRIM(RTRIM(lcom2.t_desc))) like '%" + param.sSearch + "%' or (LTRIM(RTRIM(ltf001.BU))+'-'+LTRIM(RTRIM(bcom2.t_desc))) like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                //  var lstHeader = db.SP_IPI_GetQualityIDHeader(startIndex, endIndex, "", whereCondition).ToList();
                var lstHeader = db.SP_LTFPS_GET_PROJECT_INDEX_DATA(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        a.QualityProject,
                        a.Project,
                        a.BU,
                        a.Location,
                        HTMLActionString(Convert.ToInt32(a.HeaderId), "View", "View Details", "fa fa-eye", "", WebsiteURL + "/LTFPS/MaintainLTFPS/Details/" + Convert.ToString(a.HeaderId))
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #endregion

        #region Header Details Page
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id)
        {
            LTF001 objLTF001 = new LTF001();

            if (id > 0)
            {
                objLTF001 = db.LTF001.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objLTF001.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objLTF001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objLTF001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            else
            {
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objLTF001.Location = objClsLoginInfo.Location;
            }

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

            if (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Releaser.GetStringValue())
                ViewBag.Title = "Release LTFPS";
            else if (objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                ViewBag.Title = "Approve LTFPS";
            else
                ViewBag.Title = "Maintain LTFPS";

            return View(objLTF001);
        }

        public ActionResult GetLTFPSHeaderDataPartial(string Status, string QualityProject, string Location, string BU)
        {
            ViewBag.status = Status;
            ViewBag.qualityProject = QualityProject;
            ViewBag.location = Location;
            ViewBag.bu = BU;
            string status = clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue();
            if (db.LTF001.Any(x => x.QualityProject.Equals(QualityProject) && x.Location.Equals(Location) && x.BU.Equals(BU) && (x.RevNo > 0 || !x.Status.Equals(status) || x.LTF003.Any())))
                ViewBag.isIdentProjectEditable = false;
            else
                ViewBag.isIdentProjectEditable = true;

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            return PartialView("_GetLTFPSHeaderDataPartial");
        }

        [HttpPost]
        public ActionResult GetLTFPSHeaderData(JQueryDataTableParamModel param, string qualityProj, string status, string location, string bu)
        {
            try
            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "ltf001.BU", "ltf001.Location");
                string strSortOrder = string.Empty;
                bool isRemarkEditable = false;

                if (!string.IsNullOrEmpty(qualityProj))
                {
                    whereCondition += " AND UPPER(ltf001.QualityProject) = '" + qualityProj.Trim().ToUpper() + "' AND UPPER(ltf001.Location) = '" + location.Trim().ToUpper() + "'  AND UPPER(ltf001.BU) = '" + bu.Trim().ToUpper() + "'";
                }

                if (status.ToUpper() == "PENDING")
                {
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PMG2.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Releaser.GetStringValue())
                    {
                        //whereCondition += " and ltf001.PMGReleaseBy IS NULL  and (ltf001.CustomerApprovedBy IS NOT NULL and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue() + "') ) OR ( ltf001.CustomerApprovedBy IS NOT NULL and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.SentForApproval.GetStringValue() + "') )";

                        whereCondition += " and ltf001.PMGReleaseBy IS NULL  and ( (ltf001.CustomerApprovedBy IS NOT NULL and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue() + "') )  OR ( ltf001.CustomerApprovedBy IS NOT NULL and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.SentForApproval.GetStringValue() + "') ) )";
                        isRemarkEditable = true;
                    }
                    else if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.CR1.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                    {
                        //whereCondition += " and ltf001.QCApprovedBy IS NOT NULL and ltf001.CustomerApprovedBy IS NULL and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue() + "')";
                        whereCondition += " and ltf001.CustomerApprovedBy IS NULL and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.SentForApproval.GetStringValue() + "')";
                        isRemarkEditable = true;
                    }
                    //else if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                    //{
                    //    whereCondition += " and ltf001.WEApprovedBy IS NOT NULL and ltf001.QCApprovedBy IS NULL and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue() + "')";
                    //}
                    //else if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.WE2.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())
                    //{
                    //    whereCondition += " and ltf001.SubmittedBy IS NOT NULL and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.SentForApproval.GetStringValue() + "')";
                    //}
                    else if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.PMG3.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue())
                    {
                        whereCondition += " and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and ltf001.Status in('" + clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue() + "')";
                    }
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (ltf001.LTFPSNo like '%" + param.sSearch
                        + "%' or ltf001.RevNo like '%" + param.sSearch
                        + "%' or ltf001.Status like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_LTFPS_GET_HEADER_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                List<SelectListItem> BoolenList = new List<SelectListItem>() { new SelectListItem { Text = "Yes", Value = "Yes" }, new SelectListItem { Text = "No", Value = "No" } };

                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", ""),
                                    Helper.GenerateTextbox(newRecordId, "LTFPSNo", "", "", false, "", "50"),
                                    Helper.GenerateTextbox(newRecordId, "LTFPSDescription", "", "", false, "", "200"),
                                    Helper.GenerateCheckboxWithEvent(newRecordId,"IsReworkLTFPS", false, "", true, "IsReworkLTFPS"),
                                    "",
                                    "R0",
                                    clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue(),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLTFPSHeader();")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Convert.ToString(uc.LTFPSNo),
                               ((uc.Status == clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() && !uc.IsLTFPSOffered.Value) ? Helper.GenerateTextbox(uc.HeaderId, "LTFPSDescription", uc.LTFPSDescription, "UpdateLTFPSHeader(this, "+ uc.HeaderId +")", false, "", "200") : uc.LTFPSDescription,
                                Helper.GenerateCheckboxWithEvent(uc.HeaderId,"IsReworkLTFPS",uc.IsReworkLTFPS.Value, "UpdateLTFPSHeader(this, "+ uc.HeaderId +")", ((uc.Status == clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() && !uc.IsLTFPSOffered.Value && !uc.IsLTFPSLinked.Value), "IsReworkLTFPS"),
                                //((uc.Status == clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() && !uc.IsLTFPSOffered.Value && !uc.IsLTFPSLinked.Value) ? GenerateAutoCompleteOnBlur(uc.HeaderId,"txtIsReworkLTFPS",(uc.IsReworkLTFPS.Value ? "Yes" : "No"),"UpdateLTFPSHeader(this,"+ uc.HeaderId +")","",false,"","IsReworkLTFPS",(uc.IsReworkLTFPS.Value ? "1" : "0"), false)+""+Helper.GenerateHidden(uc.HeaderId,"IsReworkLTFPS",(uc.IsReworkLTFPS.Value ? "1" : "0")) : (uc.IsReworkLTFPS.Value ? "Yes" : "No"),
                                GenerateRemark(uc.HeaderId, "ReturnRemark", objUserRoleAccessDetails.UserDesignation, objUserRoleAccessDetails.UserRole, isRemarkEditable),
                               "R" + Convert.ToString(uc.RevNo),
                               Convert.ToString(uc.Status),
                               HTMLActionString(uc.HeaderId, "Link", "Link Assembly", "fa fa-link", "LinkAssembly("+ uc.HeaderId +")", "", objUserRoleAccessDetails.UserDesignation != clsImplementationEnum.UserAccessRole.Initiator.GetStringValue()) +
                               HTMLActionString(uc.HeaderId, "Delete", "Delete LTFPS", "fa fa-trash-o", "DeleteLTFPSHeaderWithConfirmation("+ uc.HeaderId +")", "", (objUserRoleAccessDetails.UserDesignation != clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() || !((uc.Status == clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue() || uc.Status == clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue()) && uc.RevNo == 0))) +
                               HTMLActionString(uc.HeaderId,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/LTFPS/MaintainLTFPS/ShowTimeline?HeaderID="+ uc.HeaderId +"\");") +
                               HTMLActionString(uc.HeaderId, "History", "History", "fa fa-history", "HistoryLTFPS("+ uc.HeaderId +")", "", uc.RevNo > 0 || uc.Status == clsImplementationEnum.LTFPSHeaderStatus.Released.GetStringValue() ? false : true) +
                               HTMLActionString(uc.HeaderId, "Revise", "Revise", "fa fa-retweet", "ReviseLTFPSHeaderWithConfirmation("+ uc.HeaderId +")", "", uc.Status == clsImplementationEnum.LTFPSHeaderStatus.Released.GetStringValue() && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() ? false : true)

                           }).ToList();


                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveLTFPSHeader(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            LTF001 objLTF001 = new LTF001();
            int newRowIndex = 0;
            try
            {
                string strProject = !string.IsNullOrEmpty(fc["Project"]) ? Convert.ToString(fc["Project"]).Split('-')[0].Trim() : string.Empty;
                string strBU = !string.IsNullOrEmpty(fc["BU"]) ? Convert.ToString(fc["BU"]).Split('-')[0].Trim() : string.Empty;
                string strLocation = !string.IsNullOrEmpty(fc["Location"]) ? Convert.ToString(fc["Location"]).Split('-')[0].Trim() : string.Empty;
                string strQualityProject = !string.IsNullOrEmpty(fc["txtQualityProject"]) ? fc["txtQualityProject"] : string.Empty;
                string strLTFPS = !string.IsNullOrEmpty(fc["LTFPSNo" + newRowIndex]) ? fc["LTFPSNo" + newRowIndex] : string.Empty;
                string strLTFPSDescription = !string.IsNullOrEmpty(fc["LTFPSDescription" + newRowIndex]) ? fc["LTFPSDescription" + newRowIndex] : string.Empty;
                bool isReworkLTFPS = !string.IsNullOrEmpty(fc["IsReworkLTFPS" + newRowIndex + "_0"]) ? Convert.ToString(fc["IsReworkLTFPS" + newRowIndex + "_0"]).ToLower().Contains("true") : false;

                if (fc != null)
                {
                    if (isLTFPSHeaderExist(strQualityProject, strProject, strBU, strLocation, strLTFPS))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "LTFPS " + strLTFPS + " for " + strQualityProject + " is already exist";
                    }
                    else
                    {
                        #region Add New LTFPS Header
                        objLTF001.QualityProject = strQualityProject;
                        objLTF001.Project = strProject;
                        objLTF001.BU = strBU;
                        objLTF001.Location = strLocation;
                        objLTF001.LTFPSNo = strLTFPS.ToUpper();
                        objLTF001.LTFPSDescription = strLTFPSDescription;
                        objLTF001.IsReworkLTFPS = isReworkLTFPS;
                        objLTF001.RevNo = 0;
                        objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue();
                        objLTF001.CreatedBy = objClsLoginInfo.UserName;
                        objLTF001.CreatedOn = DateTime.Now;

                        db.LTF001.Add(objLTF001);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "LTFPS added successfully";
                        objResponseMsg.HeaderId = objLTF001.HeaderId;
                        objResponseMsg.HeaderStatus = objLTF001.Status;
                        objResponseMsg.RevNo = Convert.ToString(objLTF001.RevNo);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateLTFPSHeader(int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            LTF001 objLTF001 = new LTF001();
            try
            {

                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_LTFPS_UPDATE_HEADER_COLUMN(headerId, columnName, columnValue);
                }

                if (headerId > 0)
                {
                    objLTF001 = db.LTF001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    objLTF001.EditedBy = objClsLoginInfo.UserName;
                    objLTF001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Saved Successfully";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLTFPSHeader(string headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int intheaderId = 0;
            try
            {
                if (!string.IsNullOrEmpty(headerId))
                {
                    intheaderId = Convert.ToInt32(headerId);
                    LTF001 objLTF001 = db.LTF001.FirstOrDefault(c => c.HeaderId == intheaderId);

                    if (objLTF001 != null)
                    {
                        LTF002 objParent = db.LTF002.FirstOrDefault(c => c.ReworkHeaderId == objLTF001.HeaderId);

                        if (objParent != null)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Can't able to delete bacause already attached with LTFPS: " + objParent.LTFPSNo;
                        }
                        else
                        {
                            db.LTF002.RemoveRange(objLTF001.LTF002);

                            db.LTF003.RemoveRange(objLTF001.LTF003);

                            db.LTF001.Remove(objLTF001);

                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "LTFPS deleted successfully";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "LTFPS not found";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReviseLTFPSHeader(string headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int intheaderId = 0;
            try
            {
                if (!string.IsNullOrEmpty(headerId))
                {
                    intheaderId = Convert.ToInt32(headerId);
                    LTF001 objLTF001 = db.LTF001.FirstOrDefault(c => c.HeaderId == intheaderId);

                    if (objLTF001 != null)
                    {
                        objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue();
                        objLTF001.RevNo = objLTF001.RevNo + 1;
                        objLTF001.EditedBy = objClsLoginInfo.UserName;
                        objLTF001.EditedOn = DateTime.Now;
                        objLTF001.LTF002.ToList().ForEach(x =>
                        {
                            x.LTFPSRevNo = objLTF001.RevNo.Value;
                            x.EditedBy = objClsLoginInfo.UserName;
                            x.EditedOn = DateTime.Now;
                        });
                        objLTF001.LTF003.ToList().ForEach(x =>
                        {
                            x.LTFPSRevNo = objLTF001.RevNo.Value;
                            x.EditedBy = objClsLoginInfo.UserName;
                            x.EditedOn = DateTime.Now;
                        });
                        db.SaveChanges();
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "LTFPS revised successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SubmitLTFPSHeader(string strHeader)
        {
            //clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            ResponceMsgWithMultiValue objResponseMsg = new ResponceMsgWithMultiValue();
            List<LTF001> lstLTF001s = new List<LTF001>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    List<string> submittedLTFPS = new List<string>();
                    List<string> exisingLTFPS = new List<string>();
                    List<string> pendingLTFPS = new List<string>();
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    string DeletedStatus = clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue();
                    foreach (int headerId in headerIds)
                    {
                        var ltf001 = db.LTF001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                        bool isStageCodeSeqExists = false;
                        //check duplicate Seam no,Stage & Stage seq
                        if (ltf001.IsReworkLTFPS != true)
                        {
                            foreach (var objLTF002 in ltf001.LTF002.Where(c => c.LineStatus != clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()).ToList())
                            {
                                if (!string.IsNullOrEmpty(objLTF002.SeamNo) && !string.IsNullOrEmpty(objLTF002.Stage) && !string.IsNullOrEmpty(Convert.ToString(objLTF002.StageSequence)))
                                {
                                    if (db.LTF002.Any(x => x.LTF001.IsReworkLTFPS != true && x.QualityProject.Equals(ltf001.QualityProject) && x.Location.Equals(ltf001.Location) && x.BU.Equals(ltf001.BU) && x.SeamNo.Equals(objLTF002.SeamNo) && x.Stage.Equals(objLTF002.Stage) && x.StageSequence == objLTF002.StageSequence && x.LineId != objLTF002.LineId && x.LineStatus != DeletedStatus))
                                    {
                                        isStageCodeSeqExists = true;
                                        exisingLTFPS.Add(objLTF002.SeamNo);
                                    }
                                }
                            }
                        }
                        if (!isStageCodeSeqExists)
                        {
                            var result = SendForApproval(headerId);
                            if (result)
                                submittedLTFPS.Add(ltf001.LTFPSNo);
                            else
                                pendingLTFPS.Add(ltf001.LTFPSNo);
                        }
                    }
                    if (submittedLTFPS.Count > 0)
                    {
                        objResponseMsg.submittedLTFPS = "LTFPS " + String.Join(",", submittedLTFPS) + " submitted successfully";
                        objResponseMsg.isIdenticalProjectEditable = false;
                    }
                    else
                        objResponseMsg.isIdenticalProjectEditable = true;
                    if (pendingLTFPS.Count > 0)
                        objResponseMsg.pendingLTFPS = "LTFPS " + String.Join(",", pendingLTFPS) + " not submitted because of seam/part number is required with operation or assembly not linked or stage not mapped for RT/UT or rework LTFPS not linked with any operation.";

                    if (exisingLTFPS.Count > 0)
                        objResponseMsg.exisingLTFPS = "You can't submit same stage and sequence with Seam No(s): " + String.Join(",", exisingLTFPS.Distinct());

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "LTFPS has been sucessfully submitted for approval";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record sending for approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApproveLTFPSHeader(string strHeader, string department)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<LTF001> lstLTF001s = new List<LTF001>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        //if (department == clsImplementationEnum.UserRoleName.WE2.GetStringValue())
                        //    ApproveByWE(headerId);
                        //else if (department == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        //    ApproveByQC(headerId);
                        //else 
                        if (department == clsImplementationEnum.UserRoleName.CR1.GetStringValue())
                            ApproveByCustomer(headerId);
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "LTFPS(s) has been sucessfully approved";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record for approve";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReleaseLTFPSHeader(string strHeader)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<LTF001> lstLTF001s = new List<LTF001>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        ReleaseByPMG(headerId);
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "LTFPS(s) has been sucessfully released";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record for release";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ReturnLTFPSHeader(string strHeader, string strReturnRemarks, string department)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<LTF001> lstLTF001s = new List<LTF001>();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    string[] returnRemarks = strReturnRemarks.Split(',');

                    for (int i = 0; i < headerIds.Length; i++)
                    {
                        //if (department == clsImplementationEnum.UserRoleName.WE2.GetStringValue())
                        //    ReturnByWE(headerIds[i], returnRemarks[i]);
                        //else if (department == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        //    ReturnByQC(headerIds[i], returnRemarks[i]);
                        //else 
                        if (department == clsImplementationEnum.UserRoleName.CR1.GetStringValue())
                            ReturnByCustomer(headerIds[i], returnRemarks[i]);
                        else if (department == clsImplementationEnum.UserRoleName.PMG2.GetStringValue())
                            ReturnByPMG(headerIds[i], returnRemarks[i]);
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "LTFPS(s) has been sucessfully returned";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record for return";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool SendForApproval(int headerId)
        {
            LTF001 objLTF001 = db.LTF001.FirstOrDefault(i => i.HeaderId == headerId);
            if (objLTF001 != null)
            {
                if (ValidateLTFPS(headerId))
                {
                    objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.SentForApproval.GetStringValue();
                    objLTF001.SubmittedBy = objClsLoginInfo.UserName;
                    objLTF001.SubmittedOn = DateTime.Now;

                    objLTF001.WEReturnedBy = null;
                    objLTF001.WEReturnedOn = null;
                    objLTF001.WEReturnRemarks = null;
                    objLTF001.WEApprovedBy = null;
                    objLTF001.WEApprovedOn = null;
                    objLTF001.QCReturnedBy = null;
                    objLTF001.QCReturnedOn = null;
                    objLTF001.QCReturnRemarks = null;
                    objLTF001.QCApprovedBy = null;
                    objLTF001.QCApprovedOn = null;
                    objLTF001.CustomerReturnedBy = null;
                    objLTF001.CustomerReturnedOn = null;
                    objLTF001.CustomerReturnRemarks = null;
                    objLTF001.CustomerApprovedBy = null;
                    objLTF001.CustomerApprovedOn = null;
                    objLTF001.PMGReturnedBy = null;
                    objLTF001.PMGReturnedOn = null;
                    objLTF001.PMGReturnRemarks = null;
                    objLTF001.PMGReleaseBy = null;
                    objLTF001.PMGReleaseOn = null;
                    db.SaveChanges();

                    bool isTPIOnline = db.QMS010.Where(x => x.QualityProject == objLTF001.QualityProject).Select(x => x.TPIOnlineApproval).FirstOrDefault();
                    if (!isTPIOnline)
                    {
                        //objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue();
                        objLTF001.CustomerApprovedBy = objClsLoginInfo.UserName;
                        objLTF001.CustomerApprovedOn = DateTime.Now;
                    }
                    db.SaveChanges();

                    #region Send Notification

                    if (isTPIOnline)
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.CR1.GetStringValue(), objLTF001.Project, objLTF001.BU, objLTF001.Location, "LTFPS: " + objLTF001.LTFPSNo + " has been submitted for your approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/LTFPS/MaintainLTFPS/Details/" + objLTF001.HeaderId);
                    else
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), objLTF001.Project, objLTF001.BU, objLTF001.Location, "LTFPS: " + objLTF001.LTFPSNo + " has been submitted for your approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/LTFPS/MaintainLTFPS/Details/" + objLTF001.HeaderId);

                    #endregion

                    return true;
                }
                else
                    return false;
            }
            return false;
        }

        private bool ApproveByWE(int headerId)
        {
            LTF001 objLTF001 = db.LTF001.FirstOrDefault(i => i.HeaderId == headerId);
            if (objLTF001 != null)
            {
                objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue();
                objLTF001.WEApprovedBy = objClsLoginInfo.UserName;
                objLTF001.WEApprovedOn = DateTime.Now;
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objLTF001.Project, objLTF001.BU, objLTF001.Location, "LTFPS: " + objLTF001.LTFPSNo + " has been submitted for your approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/LTFPS/MaintainLTFPS/Details/" + objLTF001.HeaderId);
                #endregion

                return true;
            }
            return false;
        }

        private bool ReturnByWE(int headerId, string returnRemark)
        {
            LTF001 objLTF001 = db.LTF001.FirstOrDefault(i => i.HeaderId == headerId);
            if (objLTF001 != null)
            {
                objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue();
                objLTF001.WEReturnRemarks = returnRemark;
                objLTF001.WEReturnedBy = objClsLoginInfo.UserName;
                objLTF001.WEReturnedOn = DateTime.Now;
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), objLTF001.Project, objLTF001.BU, objLTF001.Location, "LTFPS: " + objLTF001.LTFPSNo + " has been returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/LTFPS/MaintainLTFPS/Details/" + objLTF001.HeaderId);
                #endregion

                return true;
            }
            return false;
        }

        private bool ApproveByQC(int headerId)
        {
            LTF001 objLTF001 = db.LTF001.FirstOrDefault(i => i.HeaderId == headerId);
            if (objLTF001 != null)
            {
                bool isTPIOnline = db.QMS010.Where(x => x.QualityProject == objLTF001.QualityProject).Select(x => x.TPIOnlineApproval).FirstOrDefault();

                objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue();
                objLTF001.QCApprovedBy = objClsLoginInfo.UserName;
                objLTF001.QCApprovedOn = DateTime.Now;
                if (!isTPIOnline)
                {
                    objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue();
                    objLTF001.CustomerApprovedBy = objClsLoginInfo.UserName;
                    objLTF001.CustomerApprovedOn = DateTime.Now;
                }
                db.SaveChanges();

                #region Send Notification
                if (isTPIOnline)
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.CR1.GetStringValue(), objLTF001.Project, objLTF001.BU, objLTF001.Location, "LTFPS: " + objLTF001.LTFPSNo + " has been submitted for your approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/LTFPS/MaintainLTFPS/Details/" + objLTF001.HeaderId);
                else
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), objLTF001.Project, objLTF001.BU, objLTF001.Location, "LTFPS: " + objLTF001.LTFPSNo + " has been submitted for your approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/LTFPS/MaintainLTFPS/Details/" + objLTF001.HeaderId);
                #endregion

                return true;
            }
            return false;
        }

        private bool ReturnByQC(int headerId, string returnRemark)
        {
            LTF001 objLTF001 = db.LTF001.FirstOrDefault(i => i.HeaderId == headerId);
            if (objLTF001 != null)
            {
                objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue();
                objLTF001.QCReturnRemarks = returnRemark;
                objLTF001.QCReturnedBy = objClsLoginInfo.UserName;
                objLTF001.QCReturnedOn = DateTime.Now;
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), objLTF001.Project, objLTF001.BU, objLTF001.Location, "LTFPS: " + objLTF001.LTFPSNo + " has been returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/LTFPS/MaintainLTFPS/Details/" + objLTF001.HeaderId);
                #endregion

                return true;
            }
            return false;
        }

        private bool ApproveByCustomer(int headerId)
        {
            LTF001 objLTF001 = db.LTF001.FirstOrDefault(i => i.HeaderId == headerId);
            if (objLTF001 != null)
            {
                objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue();
                objLTF001.CustomerApprovedBy = objClsLoginInfo.UserName;
                objLTF001.CustomerApprovedOn = DateTime.Now;
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), objLTF001.Project, objLTF001.BU, objLTF001.Location, "LTFPS: " + objLTF001.LTFPSNo + " has been submitted for your approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/LTFPS/MaintainLTFPS/Details/" + objLTF001.HeaderId);
                #endregion

                return true;
            }
            return false;
        }

        private bool ReturnByCustomer(int headerId, string returnRemark)
        {
            LTF001 objLTF001 = db.LTF001.FirstOrDefault(i => i.HeaderId == headerId);
            if (objLTF001 != null)
            {
                objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue();
                objLTF001.CustomerReturnRemarks = returnRemark;
                objLTF001.CustomerReturnedBy = objClsLoginInfo.UserName;
                objLTF001.CustomerReturnedOn = DateTime.Now;
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), objLTF001.Project, objLTF001.BU, objLTF001.Location, "LTFPS: " + objLTF001.LTFPSNo + " has been returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/LTFPS/MaintainLTFPS/Details/" + objLTF001.HeaderId);
                #endregion

                return true;
            }
            return false;
        }

        private bool ReleaseByPMG(int headerId)
        {
            LTF001 objLTF001 = db.LTF001.FirstOrDefault(i => i.HeaderId == headerId);
            if (objLTF001 != null)
            {
                objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Released.GetStringValue();
                objLTF001.PMGReleaseBy = objClsLoginInfo.UserName;
                objLTF001.PMGReleaseOn = DateTime.Now;
                db.SaveChanges();

                #region Maintain Log Details

                List<LTF001_Log> lstLTF001_Log = db.LTF001_Log.Where(c => c.HeaderId == objLTF001.HeaderId).ToList();
                if (lstLTF001_Log.Count > 0)
                {
                    lstLTF001_Log.ForEach(x =>
                    {
                        x.Status = clsImplementationEnum.LTFPSHeaderStatus.Superseded.GetStringValue();
                    });
                }

                LTF001_Log objLTF001_Log = new LTF001_Log();
                objLTF001_Log.HeaderId = objLTF001.HeaderId;
                objLTF001_Log.QualityProject = objLTF001.QualityProject;
                objLTF001_Log.Project = objLTF001.Project;
                objLTF001_Log.BU = objLTF001.BU;
                objLTF001_Log.Location = objLTF001.Location;
                objLTF001_Log.LTFPSNo = objLTF001.LTFPSNo;
                objLTF001_Log.LTFPSDescription = objLTF001.LTFPSDescription;
                objLTF001_Log.RevNo = objLTF001.RevNo;
                objLTF001_Log.Status = objLTF001.Status;
                objLTF001_Log.CreatedBy = objLTF001.CreatedBy;
                objLTF001_Log.CreatedOn = objLTF001.CreatedOn;
                objLTF001_Log.EditedBy = objLTF001.EditedBy;
                objLTF001_Log.EditedOn = objLTF001.EditedOn;
                objLTF001_Log.SubmittedBy = objLTF001.SubmittedBy;
                objLTF001_Log.SubmittedOn = objLTF001.SubmittedOn;
                objLTF001_Log.WEReturnedBy = objLTF001.WEReturnedBy;
                objLTF001_Log.WEReturnedOn = objLTF001.WEReturnedOn;
                objLTF001_Log.WEReturnRemarks = objLTF001.WEReturnRemarks;
                objLTF001_Log.WEApprovedBy = objLTF001.WEApprovedBy;
                objLTF001_Log.WEApprovedOn = objLTF001.WEApprovedOn;
                objLTF001_Log.QCReturnedBy = objLTF001.QCReturnedBy;
                objLTF001_Log.QCReturnedOn = objLTF001.QCReturnedOn;
                objLTF001_Log.QCReturnRemarks = objLTF001.QCReturnRemarks;
                objLTF001_Log.QCApprovedBy = objLTF001.QCApprovedBy;
                objLTF001_Log.QCApprovedOn = objLTF001.QCApprovedOn;
                objLTF001_Log.CustomerReturnedBy = objLTF001.CustomerReturnedBy;
                objLTF001_Log.CustomerReturnedOn = objLTF001.CustomerReturnedOn;
                objLTF001_Log.CustomerReturnRemarks = objLTF001.CustomerReturnRemarks;
                objLTF001_Log.CustomerApprovedBy = objLTF001.CustomerApprovedBy;
                objLTF001_Log.CustomerApprovedOn = objLTF001.CustomerApprovedOn;
                objLTF001_Log.PMGReturnedBy = objLTF001.PMGReturnedBy;
                objLTF001_Log.PMGReturnedOn = objLTF001.PMGReturnedOn;
                objLTF001_Log.PMGReturnRemarks = objLTF001.PMGReturnRemarks;
                objLTF001_Log.PMGReleaseBy = objLTF001.PMGReleaseBy;
                objLTF001_Log.PMGReleaseOn = objLTF001.PMGReleaseOn;
                db.LTF001_Log.Add(objLTF001_Log);
                db.SaveChanges();

                List<LTF002> deletedLines = new List<LTF002>();

                foreach (LTF002 objLTF002 in objLTF001.LTF002)
                {
                    if (objLTF002.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue())
                    {
                        deletedLines.Add(objLTF002);
                        continue;
                    }

                    objLTF002.LineStatus = clsImplementationEnum.LTFPSLineStatus.Released.GetStringValue();

                    LTF002_Log objLTF002_Log = new LTF002_Log();
                    objLTF002_Log.RefId = objLTF001_Log.Id;
                    objLTF002_Log.LineId = objLTF002.LineId;
                    objLTF002_Log.HeaderId = objLTF002.HeaderId;
                    objLTF002_Log.QualityProject = objLTF002.QualityProject;
                    objLTF002_Log.Project = objLTF002.Project;
                    objLTF002_Log.BU = objLTF002.BU;
                    objLTF002_Log.Location = objLTF002.Location;
                    objLTF002_Log.LTFPSNo = objLTF002.LTFPSNo;
                    objLTF002_Log.LTFPSRevNo = objLTF002.LTFPSRevNo;
                    objLTF002_Log.OperationNo = objLTF002.OperationNo;
                    objLTF002_Log.RefDocument = objLTF002.RefDocument;
                    objLTF002_Log.DocRevNo = objLTF002.DocRevNo;
                    objLTF002_Log.TestFor = objLTF002.TestFor;
                    objLTF002_Log.SeamNo = objLTF002.SeamNo;
                    objLTF002_Log.PartNo = objLTF002.PartNo;
                    objLTF002_Log.OperationDescription = objLTF002.OperationDescription;
                    objLTF002_Log.TestType = objLTF002.TestType;
                    objLTF002_Log.Stage = objLTF002.Stage;
                    objLTF002_Log.StageSequence = objLTF002.StageSequence;
                    objLTF002_Log.Agency = objLTF002.Agency;
                    objLTF002_Log.TPI = objLTF002.TPI;
                    objLTF002_Log.DocumentRequired = objLTF002.DocumentRequired;
                    objLTF002_Log.InspectionStatus = objLTF002.InspectionStatus;
                    objLTF002_Log.LNTStatus = objLTF002.LNTStatus;
                    objLTF002_Log.CreatedBy = objLTF002.CreatedBy;
                    objLTF002_Log.CreatedOn = objLTF002.CreatedOn;
                    objLTF002_Log.EditedBy = objLTF002.EditedBy;
                    objLTF002_Log.EditedOn = objLTF002.EditedOn;
                    objLTF002_Log.RequestNo = objLTF002.RequestNo;
                    objLTF002_Log.IterationNo = objLTF002.IterationNo;
                    objLTF002_Log.LineStatus = objLTF002.LineStatus;
                    db.LTF002_Log.Add(objLTF002_Log);
                }

                db.LTF002.RemoveRange(deletedLines);

                db.SaveChanges();

                #endregion

                //#region Ready to Offer
                //db.SP_LTFPS_RELEASE_READY_TO_OFFER(objLTF001.HeaderId);
                //#endregion

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), objLTF001.Project, objLTF001.BU, objLTF001.Location, "LTFPS: " + objLTF001.LTFPSNo + " has been released", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/LTFPS/MaintainLTFPS/Details/" + objLTF001.HeaderId);
                #endregion

                return true;
            }
            return false;
        }

        private bool ReturnByPMG(int headerId, string returnRemark)
        {
            LTF001 objLTF001 = db.LTF001.FirstOrDefault(i => i.HeaderId == headerId);
            if (objLTF001 != null)
            {
                objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue();
                objLTF001.PMGReturnRemarks = returnRemark;
                objLTF001.PMGReturnedBy = objClsLoginInfo.UserName;
                objLTF001.PMGReturnedOn = DateTime.Now;
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), objLTF001.Project, objLTF001.BU, objLTF001.Location, "LTFPS: " + objLTF001.LTFPSNo + " has been returned", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/LTFPS/MaintainLTFPS/Details/" + objLTF001.HeaderId);
                #endregion

                return true;
            }
            return false;
        }

        public bool isLTFPSHeaderExist(string qualityProject, string project, string bu, string location, string ltfpsNo)
        {
            return db.LTF001.Any(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(bu, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.LTFPSNo.Equals(ltfpsNo, StringComparison.OrdinalIgnoreCase));
        }
        public bool ValidateLTFPS(int HeaderId)
        {
            LTF001 objLTF001 = db.LTF001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objLTF001 != null)
            {
                // check for rework LTFPS linked with any rework operation or not
                if (objLTF001.IsReworkLTFPS == true && !db.LTF002.Any(c => c.ReworkHeaderId == objLTF001.HeaderId))
                {
                    return false;
                }

                // check for assembly linkage or not
                if (!db.LTF003.Any(x => x.QualityProject.Equals(objLTF001.QualityProject) && x.LTFPSNo.Equals(objLTF001.LTFPSNo) && x.Location.Equals(objLTF001.Location) && x.LTFPSRevNo == objLTF001.RevNo && x.BU.Equals(objLTF001.BU)))
                    return false;

                foreach (var objLTF002 in objLTF001.LTF002.Where(c => c.LineStatus != clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()).ToList())
                {
                    // check for seam no required
                    if (objLTF002.TestFor == "Seam" && string.IsNullOrWhiteSpace(objLTF002.SeamNo))
                        return false;

                    // check for part no required
                    if (objLTF002.TestFor == "Part" && string.IsNullOrWhiteSpace(objLTF002.PartNo))
                        return false;

                    // check for stage required for UT and RT Stage type
                    if ((objLTF002.TestType.Equals("RT") || objLTF002.TestType.Equals("UT")) && (string.IsNullOrEmpty(objLTF002.Stage) || string.IsNullOrEmpty(Convert.ToString(objLTF002.StageSequence))))
                        return false;
                }
            }
            return true;
        }
        #endregion

        #region Assembly Linkage
        [SessionExpireFilter, AllowAnonymous]
        public ActionResult AssemblyLinkage(int id)
        {
            LTF001 objLTF001 = db.LTF001.Where(i => i.HeaderId == id).FirstOrDefault();
            return View(objLTF001);
        }
        public ActionResult loadLinkageAssembly(JQueryDataTableParamModel param)
        {
            try
            {
                int headerId = Convert.ToInt32(param.CTQHeaderId);
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                LTF001 objLTF001 = db.LTF001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                var result = db.QMS035.Where(x => x.QualityProject == objLTF001.QualityProject && x.BU == objLTF001.BU && x.Location == objLTF001.Location).Select(x => x.PartNo).Distinct().ToList();

                var items = (from li in result
                             select new SelectItemList
                             {
                                 id = li.ToString(),
                                 text = li.ToString()
                             }).ToList();
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                string Project = param.Project.Trim();
                string whereCondition = "1=1 and ltf003.HeaderId =" + headerId + " and ltf003.LTFPSRevNo=" + objLTF001.RevNo;
                string[] columnName = { "ltf003.AssemblyFindNo", "ltf003.AssemblyPartNo" };
                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstLines = db.SP_LTFPS_GET_LINKAGEASSEMBLY_DATA(StartIndex, EndIndex, strSortOrder, whereCondition, Project).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                #region inline grid code developed by Ajay Chauhan
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(headerId)),
                                    "",
                                    GenerateAutoComplete(newRecordId, "AssemblyFindNo","","", "", false,"","AssemblyFindNo",false)+""+Helper.GenerateHidden(newRecordId, "hdnAssemblyFindNo", Convert.ToString(newRecordId)),
                                    "<input type='text' class='form-control'name='AssemblyPartNo0' readonly='readonly' id='AssemblyPartNo0' />",
                                    "<input type='text' class='form-control' name='AssemblyDescription0' readonly='readonly' id='AssemblyDescription0' />",
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord();")
                                };

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Helper.GenerateHidden(c.HeaderId, "HeaderId", Convert.ToString(c.HeaderId)),
                                        Helper.GenerateHidden(c.LineId, "LineId", Convert.ToString(c.LineId)),
                                        c.AssemblyFindNo,
                                        c.AssemblyPartNo,
                                        c.AssemblyDescription,
                                        HTMLActionString(c.LineId, "Delete", "Delete Assembly", "fa fa-trash-o", "DeleteAssembly("+ c.HeaderId +","+c.LineId+")")

                           }).ToList();
                res.Insert(0, newRecord);
                #endregion

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteAssembly(int headerId, int lineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                LTF003 objLTF003 = db.LTF003.FirstOrDefault(x => x.LineId == lineId);
                db.LTF003.Remove(objLTF003);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Assembly deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public bool IsAssemblyfindNoExists(string findno, string qualityProject, string bu, string location, string ltfpsNo)
        {
            var isFindNoExist = db.LTF003.Where(x => x.QualityProject == qualityProject && x.BU == bu && x.Location == location && x.LTFPSNo == ltfpsNo && x.AssemblyFindNo == findno).Any();
            if (isFindNoExist)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        [HttpPost]
        public ActionResult SaveAssembly(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    int headerId = Convert.ToInt32(fc["HeaderId0"]);
                    LTF001 objLTF001 = db.LTF001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    LTF003 objLTF003 = new LTF003();
                    objLTF003.HeaderId = objLTF001.HeaderId;
                    objLTF003.QualityProject = objLTF001.QualityProject;
                    objLTF003.LTFPSNo = objLTF001.LTFPSNo;
                    objLTF003.LTFPSRevNo = Convert.ToInt32(objLTF001.RevNo);
                    objLTF003.Project = objLTF001.Project;
                    objLTF003.BU = objLTF001.BU;
                    objLTF003.Location = objLTF001.Location;
                    objLTF003.AssemblyFindNo = Convert.ToString(fc["AssemblyFindNo0"]);
                    objLTF003.AssemblyPartNo = Convert.ToString(fc["AssemblyPartNo0"]);
                    objLTF003.CreatedBy = objClsLoginInfo.UserName;
                    objLTF003.CreatedOn = DateTime.Now;

                    db.LTF003.Add(objLTF003);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Assembly saved successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult GetAssembyFindNo(string term, string project)
        {
            List<AssemblyFindNo> lstAssemblyFindNo = new List<AssemblyFindNo>();
            List<string> findNos = new List<string>();

            if (!string.IsNullOrWhiteSpace(term))
            {
                findNos = Manager.GetHBOMData(project).Where(x => x.ProductType == "ASM"
                && x.FindNo.Contains(term)).Select(s => s.FindNo).Distinct().ToList();

                //findNos = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == project
                //&& x.ProductType == "ASM"
                //&& x.FindNo.Contains(term)).Select(s => s.FindNo).Distinct().ToList();
            }
            else
            {

                findNos = Manager.GetHBOMData(project).Where(x => x.ProductType == "ASM").Select(s => s.FindNo).Distinct().Take(10).ToList();

                //findNos = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == project && x.ProductType == "ASM").Select(s => s.FindNo).Distinct().Take(10).ToList();
            }

            if (findNos.Count > 0)
            {
                lstAssemblyFindNo = (from x in findNos
                                     select new AssemblyFindNo
                                     {
                                         Value = x.ToString(),
                                         FindNo = x.ToString(),
                                         FindNoDescription = x.ToString()
                                     }).ToList();
            }

            //var objAccessProjects = db.QMS035.Where(x => x.QualityProject == qualityProject && x.BU == bu && x.Location == location).ToList();
            //if (!string.IsNullOrWhiteSpace(term))
            //{
            //    lstAssemblyFindNo = (from x in db.QMS035
            //                         where x.QualityProject == qualityProject && x.BU == bu && x.Location == location && (x.PartNo.Contains(term))
            //                         select new AssemblyFindNo
            //                         {
            //                             Value = x.PartNo,
            //                             FindNo = x.PartNo,
            //                             FindNoDescription = x.PartNo
            //                         }
            //                  ).Distinct().ToList();
            //}
            //else
            //{
            //    lstAssemblyFindNo = (from x in db.QMS035
            //                         where x.QualityProject == qualityProject && x.BU == bu && x.Location == location
            //                         select new AssemblyFindNo
            //                         {
            //                             Value = x.PartNo,
            //                             FindNo = x.PartNo,
            //                             FindNoDescription = x.PartNo
            //                         }
            //                 ).Distinct().Take(10).ToList();
            //}
            return Json(lstAssemblyFindNo, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetAssemblyPartAndDescription(string findNo, string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var result = Manager.GetHBOMData(project).Where(x => x.FindNo == findNo).FirstOrDefault();

                //var result = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == project && x.FindNo == findNo).FirstOrDefault();
                objResponseMsg.Key = true;
                var response = new
                {
                    Key = objResponseMsg.Key,
                    PartNo = result.Part,
                    Description = result.Description
                };
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public string FetchGetAssemblyPartAndDescription(string findNo, string project)
        {
            string Description = string.Empty;
            var result = Manager.GetHBOMData(project).Where(x => x.FindNo == findNo).FirstOrDefault();

            // var result = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == project && x.FindNo == findNo).FirstOrDefault();
            if (result != null)
            {
                Description = result.Description;
            }
            return Description;
        }
        #endregion

        #region Lines Details
        [HttpPost]
        public ActionResult GetLTFPSLinesPartial(int headerId)
        {
            LTF001 objLTF001 = new LTF001();

            if (headerId > 0)
                objLTF001 = db.LTF001.Where(i => i.HeaderId == headerId).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;

            ViewBag.DocumentRequired = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "1", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "0", CategoryDescription = "No" } };
            ViewBag.TestFor = new List<CategoryData> { new CategoryData { Value = "Seam", Code = "Seam", CategoryDescription = "Seam" }, new CategoryData { Value = "Part", Code = "Part", CategoryDescription = "Part" }, new CategoryData { Value = "Assembly", Code = "Assembly", CategoryDescription = "Assembly" } };
            ViewBag.TestType = Manager.GetSubCatagories("LTFPS Test Type", objLTF001.BU, objLTF001.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.Agency = Manager.GetSubCatagories("LTFPS Agency", objLTF001.BU, objLTF001.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.TPI = Manager.GetSubCatagories("LTFPS TPI", objLTF001.BU, objLTF001.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Code, CategoryDescription = i.Description }).ToList();

            return PartialView("_GetLTFPSLinesDataPartial", objLTF001);
        }

        [HttpPost]
        public ActionResult GetLTFPSLinesData(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();

                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (ltf002.OperationNo like '%" + param.sSearch
                        + "%' or ltf002.RefDocument like '%" + param.sSearch
                        + "%' or ltf002.TestFor like '%" + param.sSearch
                        + "%' or ltf002.SeamNo like '%" + param.sSearch
                        + "%' or ltf002.PartNo like '%" + param.sSearch
                        + "%' or ltf002.OperationDescription like '%" + param.sSearch
                        + "%' or ltf002.TestType like '%" + param.sSearch
                        + "%' or ltf002.Stage like '%" + param.sSearch
                        + "%' or ltf002.StageSequence like '%" + param.sSearch
                        + "%' or ltf002.Agency like '%" + param.sSearch
                        + "%' or ltf002.TPI like '%" + param.sSearch
                        + "%' or ltf002.DocumentRequired like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_LTFPS_GET_LINES_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                LTF001 objLTF001 = db.LTF001.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                bool isEditable = ((objLTF001.Status == clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue() || objLTF001.Status == clsImplementationEnum.LTFPSHeaderStatus.Returned.GetStringValue()) && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue());

                List<SelectListItem> TestForList = new List<SelectListItem>() { new SelectListItem { Text = "Seam", Value = "Seam" }, new SelectListItem { Text = "Part", Value = "Part" }, new SelectListItem { Text = "Assembly", Value = "Assembly" } };
                List<SelectListItem> booleanList = new List<SelectListItem>() { new SelectListItem { Text = "No", Value = "0" }, new SelectListItem { Text = "Yes", Value = "1" } };
                List<SelectListItem> AgencyList = new List<SelectListItem>() { new SelectListItem { Text = "M1", Value = "M1" }, new SelectListItem { Text = "X", Value = "X" }, new SelectListItem { Text = "H", Value = "H" }, new SelectListItem { Text = "W", Value = "W" }, new SelectListItem { Text = "R", Value = "R" }, new SelectListItem { Text = "N", Value = "N" } };
                List<SelectListItem> TPIList = new List<SelectListItem>() { new SelectListItem { Text = "A1", Value = "A1" }, new SelectListItem { Text = "A2", Value = "A2" }, new SelectListItem { Text = "A3", Value = "A3" }, new SelectListItem { Text = "X", Value = "X" }, new SelectListItem { Text = "H", Value = "H" }, new SelectListItem { Text = "W", Value = "W" }, new SelectListItem { Text = "R", Value = "R" }, new SelectListItem { Text = "N", Value = "N" } };
                List<SelectListItem> TestTypeList = new List<SelectListItem>() { new SelectListItem { Text = "DT", Value = "DT" }, new SelectListItem { Text = "PT", Value = "PT" }, new SelectListItem { Text = "UT", Value = "UT" }, new SelectListItem { Text = "RT", Value = "RT" }, new SelectListItem { Text = "MT", Value = "MT" } };

                var StageList = (from a in db.QMS002
                                 where a.BU == objLTF001.BU && a.Location == objLTF001.Location
                                 select new { Code = a.StageCode, Desc = a.StageDesc }).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OperationNo", "", "", false, "", false, "6"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RefDocument", "", "", false, "", false, "100"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "DocRevNo", "", "", false, "", false, "20"),
                                   GenerateAutoCompleteOnBlur(newRecordId,"txtTestFor","","UpdateLTFPSFirstLine(this,"+ newRecordId +","+ headerId +")","",false,"","TestFor")+""+Helper.GenerateHidden(newRecordId,"TestFor"),
                                   GenerateAutoCompleteOnBlur(newRecordId,"SeamNo","","UpdateLTFPSFirstLine(this,"+ newRecordId +","+ headerId +")","",true,"","SeamNo","",false),
                                   GenerateAutoCompleteOnBlur(newRecordId,"PartNo","","UpdateLTFPSFirstLine(this,"+ newRecordId +","+ headerId +")","",true,"","PartNo","",false),
                                   //Helper.GenerateDropdown(newRecordId, "TestFor", new SelectList(TestForList, "Value", "Text")," ","",false,"width: 110px !important;"),
                                   //Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "30"),
                                   //Helper.GenerateHTMLTextbox(newRecordId, "PartNo", "", "", false, "", false, "50"),
                                   Helper.GenerateTextArea(newRecordId, "OperationDescription", "", "", false, "width: 300px !important; height: 100px !important;", "1000"),
                                   GenerateAutoCompleteOnBlur(newRecordId,"txtTestType","","UpdateLTFPSFirstLine(this,"+ newRecordId +","+ headerId +")","",false,"","TestType")+""+Helper.GenerateHidden(newRecordId,"TestType"),
                                   GenerateAutoCompleteOnBlur(newRecordId,"Stage","","","",false,"","Stage","",true),
                                   GenerateAutoCompleteOnBlur(newRecordId,"StageSequence","","","",false,"","StageSequence","",true),
                                   //Helper.GenerateDropdown(newRecordId, "TestType", new SelectList(TestTypeList, "Value", "Text"), " "),
                                   //Helper.GenerateDropdown(newRecordId, "Stage", new SelectList(StageList, "Code", "Desc"), " ", "", false, "width: 200px !important;"),
                                   GenerateAutoCompleteOnBlur(newRecordId,"txtAgency","","","",false,"","Agency")+""+Helper.GenerateHidden(newRecordId,"Agency"),
                                   GenerateAutoCompleteOnBlur(newRecordId,"txtTPI","","","",false,"","TPI")+""+Helper.GenerateHidden(newRecordId,"TPI"),
                                   GenerateAutoCompleteOnBlur(newRecordId,"txtDocumentRequired","","","",false,"","DocumentRequired")+""+Helper.GenerateHidden(newRecordId,"DocumentRequired"),
                                    //Helper.GenerateDropdown(newRecordId, "Agency", new SelectList(AgencyList, "Value", "Text"), " "),
                                    //Helper.GenerateDropdown(newRecordId, "TPI", new SelectList(TPIList, "Value", "Text"), " ", "", false, "width: 80px !important;"),
                                    //Helper.GenerateDropdown(newRecordId, "DocumentRequired", new SelectList(booleanList, "Value", "Text"), " "),
                                    clsImplementationEnum.LTFPSLineStatus.Added.GetStringValue(),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLTFPSLine();")
                                };
                var lineAttachStatus = clsImplementationEnum.SeamListInspectionStatus.ReworkNotAttached.GetStringValue();
                var lineDittachStatus = clsImplementationEnum.SeamListInspectionStatus.ReworkAttached.GetStringValue();
                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.OperationNo),
                               //isEditable && !uc.IsOperationOffered.Value ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "OperationNo", Convert.ToString(uc.OperationNo), "UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", (uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()) , "4") : Convert.ToString(uc.OperationNo),
                               isEditable && !uc.IsOperationOffered.Value ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "RefDocument", Convert.ToString(uc.RefDocument), "UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", (uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()), "100") : Convert.ToString(uc.RefDocument),
                               isEditable && !uc.IsOperationOffered.Value ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "DocRevNo", Convert.ToString(uc.DocRevNo), "UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", (uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()), "20") : Convert.ToString(uc.DocRevNo),
                               isEditable && !uc.IsOperationOffered.Value ? GenerateAutoCompleteOnBlur(uc.LineId,"txtTestFor",uc.TestFor,"UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"width: 110px !important;","TestFor",uc.TestFor,(uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()))+""+Helper.GenerateHidden(uc.LineId,"TestFor",uc.TestFor) : uc.TestFor,
                               isEditable && !uc.IsOperationOffered.Value ? GenerateAutoCompleteOnBlur(uc.LineId,"SeamNo",uc.SeamNo,"UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")","", !string.IsNullOrWhiteSpace(uc.TestFor) ? false: true,"width: 110px !important;","SeamNo",uc.SeamNo,(uc.TestFor == null || uc.TestFor == "Seam"?false:true) || (uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue())) : uc.SeamNo,
                               isEditable && !uc.IsOperationOffered.Value ? GenerateAutoCompleteOnBlur(uc.LineId,"PartNo",uc.PartNo,"UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",!string.IsNullOrWhiteSpace(uc.TestFor) ? false: true,"width: 110px !important;","PartNo",uc.PartNo,((uc.TestFor == null || uc.TestFor == "Part" || uc.TestFor == "Assembly")?false:true) || (uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue())) : uc.PartNo,
                               //isEditable ? Helper.GenerateDropdownWithSelected(uc.LineId, "TestFor", new SelectList(TestForList, "Value", "Text"), uc.TestFor, "", "UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "width: 110px !important;"): uc.TestFor,
                               //isEditable ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo), "UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", false, "30") : Convert.ToString(uc.SeamNo),
                               //isEditable ? Helper.GenerateHTMLTextboxOnChanged(uc.LineId, "PartNo", Convert.ToString(uc.PartNo), "UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "", false, "30") : Convert.ToString(uc.PartNo),
                               isEditable && !uc.IsOperationOffered.Value ? Helper.GenerateTextArea(uc.LineId, "OperationDescription", Convert.ToString(uc.OperationDescription),"UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")", (uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()), "width: 300px !important; height: 100px !important;", "1000") : Convert.ToString(uc.OperationDescription),
                               isEditable && !uc.IsOperationOffered.Value ? GenerateAutoCompleteOnBlur(uc.LineId,"txtTestType",getCategoryDesc("LTFPS Test Type",objLTF001.BU,objLTF001.Location,uc.TestType),"UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"","TestType",uc.TestType, (uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()))+""+Helper.GenerateHidden(uc.LineId,"TestType",uc.TestType) : uc.TestType,
                               isEditable && !uc.IsOperationOffered.Value ? GenerateAutoCompleteOnBlur(uc.LineId,"Stage",uc.Stage,"UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"width: 200px !important;","Stage",uc.Stage,((uc.TestType.Equals("RT") || uc.TestType.Equals("UT")) ? false : true) || (uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue())) : uc.Stage,
                               isEditable && !uc.IsOperationOffered.Value ? GenerateAutoCompleteOnBlur(uc.LineId,"StageSequence",Convert.ToString(getSequenceRemark(uc.TestFor,uc.SeamNo,uc.Stage,uc.StageSequence,uc.QualityProject,uc.Project,uc.BU,uc.Location)),"UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"","StageSequence",Convert.ToString(getSequenceRemark(uc.TestFor,uc.SeamNo,uc.Stage,uc.StageSequence,uc.QualityProject,uc.Project,uc.BU,uc.Location)),((uc.TestType.Equals("RT") || uc.TestType.Equals("UT")) ? false : true) || (uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue())) : Convert.ToString(uc.StageSequence),
                               //isEditable ? Helper.GenerateDropdownWithSelected(uc.LineId, "TestType", new SelectList(TestTypeList, "Value", "Text"), uc.TestType, "", "UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "") : uc.TestType,
                               //isEditable ? Helper.GenerateDropdownWithSelected(uc.LineId, "Stage", new SelectList(StageList, "Code", "Desc"), uc.Stage, "", "UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "width: 200px !important;") : uc.Stage,
                               isEditable && !uc.IsOperationOffered.Value ? GenerateAutoCompleteOnBlur(uc.LineId,"txtAgency",getCategoryDesc("LTFPS Agency",objLTF001.BU,objLTF001.Location,uc.Agency),"UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"","Agency",uc.Agency,(uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()))+""+Helper.GenerateHidden(uc.LineId,"Agency",uc.Agency) : uc.Agency,
                               isEditable && !uc.IsOperationOffered.Value ? GenerateAutoCompleteOnBlur(uc.LineId,"txtTPI",getCategoryDesc("LTFPS TPI",objLTF001.BU,objLTF001.Location,uc.TPI),"UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"width: 80px !important;","TPI",uc.TPI,(uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()))+""+Helper.GenerateHidden(uc.LineId,"TPI",uc.TPI) : uc.TPI,
                               isEditable && !uc.IsOperationOffered.Value ? GenerateAutoCompleteOnBlur(uc.LineId,"txtDocumentRequired",(uc.DocumentRequired ? "Yes" : "No"),"UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")","",false,"","DocumentRequired",(uc.DocumentRequired ? "1" : "0"), (uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()))+""+Helper.GenerateHidden(uc.LineId,"DocumentRequired",(uc.DocumentRequired ? "1" : "0")) : (uc.DocumentRequired ? "Yes" : "No"),
                               //isEditable ? Helper.GenerateDropdownWithSelected(uc.LineId, "Agency", new SelectList(AgencyList, "Value", "Text"), uc.Agency, "", "UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "") : uc.Agency,
                               //isEditable ? Helper.GenerateDropdownWithSelected(uc.LineId, "TPI", new SelectList(TPIList, "Value", "Text"), uc.TPI, "", "UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")", false, "width: 80px !important;") : uc.TPI,
                               //isEditable ? Helper.GenerateDropdownWithSelected(uc.LineId, "DocumentRequired", new SelectList(booleanList, "Value", "Text"), (uc.DocumentRequired ? "1" : "0"), "", "UpdateLTFPSLine(this,"+ uc.LineId +","+ uc.HeaderId +")") : (uc.DocumentRequired ? "Yes" : "No"),
                               Convert.ToString(uc.LineStatus),
                               "<nobr>" + HTMLActionString(uc.LineId, "Delete", "Delete Operation", "fa fa-trash-o", "DeleteLTFPSLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +")", "", !isEditable || (uc.LineStatus == clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue()) || uc.IsOperationOffered.Value) +
                               HTMLActionString(uc.LineId,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/LTFPS/MaintainLTFPS/ShowTimeline?HeaderID="+ uc.HeaderId +"&LineId="+ uc.LineId +"\");") +
                               (uc.TestFor=="Seam"? "<i onclick=\"ViewSeamDetails('"+uc.SeamNo+"','"+uc.QualityProject+"')\" title=\"Seam Details\" class=\"fa fa-info\" style=\"cursor: pointer; margin-right:10px; \"></i>":"<i title=\"Seam Details\" class=\"fa fa-info\" style=\"cursor: pointer; margin-right:10px;opacity:0.3; \" ></i>") +
                               (uc.InspectionStatus == lineAttachStatus && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() ?  "<i title=\"Attach Rework LTFPS\" id=\"btnAttchment\" name=\"btnAction\" style=\"cursor:pointer; color: green; \"  Title=\"Add / Update Attachment\" class=\"fa fa-paperclip\"  onClick=\"attachedFunctionCall("+ uc.LineId +",'"+ uc.LTFPSNo +"', 'attach')\" ></i>&nbsp;&nbsp;" :  "") +
                               (uc.InspectionStatus == lineDittachStatus && objUserRoleAccessDetails.UserDesignation == clsImplementationEnum.UserAccessRole.Initiator.GetStringValue() ?  "<i title=\"Dittach Rework LTFPS\" id=\"btnAttchment\" name=\"btnAction\" style=\"cursor:pointer; color: red; \"  Title=\"Add / Update Attachment\" class=\"fa fa-paperclip\"  onClick=\"attachedFunctionCall("+ uc.LineId +",'"+ uc.LTFPSNo +"', 'dittach')\" ></i>" :  "") + "</nobr>"

                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string getSequenceRemark(string TestFor, string SeamPartNo, string Stage, int? seq, string qproject, string project, string bu, string location)
        {
            var stageSequance = string.Empty;

            if (TestFor.Equals("Seam"))
            {
                stageSequance = (from qms031 in db.QMS031
                                 where qms031.QualityProject.Equals(qproject) && qms031.Location.Equals(location) && qms031.BU.Equals(bu) && qms031.SeamNo.Equals(SeamPartNo) && qms031.StageCode.Equals(Stage)
                                     && qms031.StageSequance == seq
                                 select qms031.Remarks
                         ).FirstOrDefault();

            }
            else if (TestFor.Equals("Part") || TestFor.Equals("Assembly"))
            {
                stageSequance = (from qms036 in db.QMS036
                                 where qms036.QualityProject.Equals(qproject) && qms036.Location.Equals(location) && qms036.BU.Equals(bu) && qms036.PartNo.Equals(SeamPartNo) && qms036.StageCode.Equals(Stage)
                                    && qms036.StageSequence == seq
                                 select qms036.Remarks
                         ).FirstOrDefault();
            }
            if (string.IsNullOrWhiteSpace(stageSequance))
            {
                stageSequance = seq.ToString();
            }
            else
            {
                stageSequance = seq.ToString() + " - " + stageSequance;
            }
            return stageSequance;
        }
        [HttpPost]
        public ActionResult ReworkLTFPS(int LineId)
        {
            if (LineId > 0)
            {
                LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == LineId).FirstOrDefault();


                var data = db.LTF001.Where(x => !db.LTF002.Any(c => c.ReworkHeaderId == x.HeaderId) && x.IsReworkLTFPS == true && x.Project == objLTF002.Project && x.QualityProject == objLTF002.QualityProject).ToList();
                if (data.Count > 0)
                {
                    ViewBag.lstLTFSNo = data.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.LTFPSNo, CatID = x.HeaderId.ToString() }).ToList();
                }
                else
                {
                    ViewBag.lstLTFSNo = null;
                }
            }
            return PartialView("_ReworkLTFPS");
        }
        [HttpPost]
        public ActionResult IsApplicableForAttachRework(int LineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string NotAttended = clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue();
            var parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_LTFPS_PARALLEL_SEAM ('" + LineId + "')").FirstOrDefault().Split(',');
            objResponseMsg.Key = true;
            foreach (var item in parallelRequestId)
            {
                int LTFPSHeaderId = Convert.ToInt32(item);

                if (Convert.ToInt32(item) != LineId)
                {
                    var qms060 = db.QMS060.Where(x => x.LTFPSHeaderId == LTFPSHeaderId).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                    if (qms060 != null && qms060.RequestStatus == NotAttended)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Result of Parallel stage " + qms060.StageCode + " is not maintained yet by NDE.";
                        break;
                    }
                }
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FatchLTFPSNo(int LineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            int? HeaderId = db.LTF002.Where(x => x.LineId == LineId).FirstOrDefault().ReworkHeaderId;
            if (HeaderId > 0)
            {
                var ltfpsno = db.LTF001.Where(x => x.HeaderId == HeaderId).FirstOrDefault().LTFPSNo;
                objResponseMsg.Key = true;
                objResponseMsg.Value = ltfpsno;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AttachedDetachedReworkLTFPS(int LineId, int HeaderId, string ActionType)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (LineId > 0)
                {
                    var parallelLineIds = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_LTFPS_PARALLEL_SEAM ('" + LineId + "')").FirstOrDefault();

                    LTF002 objLTF0002 = db.LTF002.Where(x => x.LineId == LineId).FirstOrDefault();

                    if (ActionType == "attach")
                    {
                        var lstLTF001 = db.LTF001.FirstOrDefault(x => x.HeaderId == HeaderId);
                        var lstLTF002 = lstLTF001.LTF002.ToList();

                        objLTF0002.ReworkHeaderId = HeaderId;
                        objLTF0002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.ReworkAttached.GetStringValue();


                        if (lstLTF002.Count > 0)
                        {
                            if (parallelLineIds.Split(',').Count() > 1)
                            {
                                foreach (var reworkLine in lstLTF002)
                                {
                                    foreach (var item in parallelLineIds.Split(','))
                                    {
                                        int pId = Convert.ToInt32(item);
                                        LTF002 pLineLTF002 = db.LTF002.FirstOrDefault(x => x.LineId == pId);
                                        if (pLineLTF002.TestType == reworkLine.TestType)
                                        {
                                            reworkLine.ParentLineId = pLineLTF002.LineId;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                lstLTF002.Select(x => { x.ParentLineId = LineId; return x; }).ToList();
                            }
                        }



                        var lstLTF003 = db.LTF003.Where(x => x.HeaderId == objLTF0002.HeaderId).ToList();
                        if (lstLTF003.Count > 0)
                        {
                            lstLTF003.Select(x => { x.HeaderId = HeaderId; x.LTFPSNo = lstLTF001.LTFPSNo; x.LTFPSRevNo = lstLTF001.RevNo.Value; return x; }).ToList();
                            db.LTF003.AddRange(lstLTF003);
                        }
                        db.SaveChanges();
                        objResponseMsg.Value = "Rework successfully attached";
                    }
                    else if (ActionType == "dittach")
                    {
                        var headerId = objLTF0002.ReworkHeaderId;
                        if (headerId > 0)
                        {
                            var lstLTF002 = db.LTF002.Where(x => x.HeaderId == headerId).ToList();
                            objLTF0002.ReworkHeaderId = null;
                            objLTF0002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.ReworkNotAttached.GetStringValue();

                            if (lstLTF002.Count > 0)
                            {
                                lstLTF002.Select(x => { x.ParentLineId = null; return x; }).ToList();
                            }

                            var lstLTF003 = db.LTF003.Where(x => x.HeaderId == headerId).ToList();
                            if (lstLTF003.Count > 0)
                            {
                                db.LTF003.RemoveRange(lstLTF003);
                            }
                            db.SaveChanges();
                            objResponseMsg.Value = "Rework successfully detached";
                        }
                    }

                    List<int> LineIds = parallelLineIds.Split(',').Where(x => x != Convert.ToString(LineId)).Select(int.Parse).ToList();
                    foreach (var LId in LineIds)
                    {
                        LTF002 ParallarLTF0002 = db.LTF002.Where(x => x.LineId == LId).FirstOrDefault();
                        if (ActionType == "attach")
                        {
                            ParallarLTF0002.ReworkHeaderId = HeaderId;
                            ParallarLTF0002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.ReworkAttached.GetStringValue();
                        }
                        else if (ActionType == "dittach")
                        {
                            ParallarLTF0002.ReworkHeaderId = null;
                            ParallarLTF0002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.ReworkNotAttached.GetStringValue();
                        }
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;

                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Rework operation fail...!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occured, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveLTFPSLine(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            LTF002 objLTF002 = new LTF002();
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    LTF001 objLTF001 = db.LTF001.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    int intOprNo = !string.IsNullOrEmpty(fc["OperationNo" + newRowIndex]) ? Convert.ToInt32(fc["OperationNo" + newRowIndex]) : 0;
                    string strRefDocument = !string.IsNullOrEmpty(fc["RefDocument" + newRowIndex]) ? Convert.ToString(fc["RefDocument" + newRowIndex]).Trim() : string.Empty;
                    string strDocRevNo = !string.IsNullOrEmpty(fc["DocRevNo" + newRowIndex]) ? Convert.ToString(fc["DocRevNo" + newRowIndex]).Trim() : string.Empty;
                    string strTestFor = !string.IsNullOrEmpty(fc["TestFor" + newRowIndex]) ? Convert.ToString(fc["TestFor" + newRowIndex]).Trim() : string.Empty;
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + newRowIndex]) ? Convert.ToString(fc["SeamNo" + newRowIndex]).Trim() : string.Empty;
                    string strPartNo = !string.IsNullOrEmpty(fc["PartNo" + newRowIndex]) ? Convert.ToString(fc["PartNo" + newRowIndex]).Trim() : string.Empty;
                    string strOprDesc = !string.IsNullOrEmpty(fc["OperationDescription" + newRowIndex]) ? Convert.ToString(fc["OperationDescription" + newRowIndex]).Trim() : string.Empty;
                    string strTestType = !string.IsNullOrEmpty(fc["TestType" + newRowIndex]) ? Convert.ToString(fc["TestType" + newRowIndex]).Trim() : string.Empty;
                    string strStage = !string.IsNullOrEmpty(fc["Stage" + newRowIndex]) ? Convert.ToString(fc["Stage" + newRowIndex]).Trim() : string.Empty;
                    int? strStageSequence = !string.IsNullOrEmpty(fc["StageSequence" + newRowIndex]) ? Convert.ToInt32(fc["StageSequence" + newRowIndex].Split('-')[0]) : (int?)null;
                    string strAgency = !string.IsNullOrEmpty(fc["Agency" + newRowIndex]) ? Convert.ToString(fc["Agency" + newRowIndex]).Trim() : string.Empty;
                    string strTPI = !string.IsNullOrEmpty(fc["TPI" + newRowIndex]) ? Convert.ToString(fc["TPI" + newRowIndex]).Trim() : string.Empty;
                    bool boolDocumentRequired = !string.IsNullOrEmpty(fc["DocumentRequired" + newRowIndex]) ? (Convert.ToString(fc["DocumentRequired" + newRowIndex]) == "1" ? true : false) : false;

                    string readyToOfferStatus = clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue();
                    if (strTestFor.Equals("Seam") && !string.IsNullOrEmpty(strSeamNo))
                    {
                        if (objLTF001.LTF002.Any(x => x.TestFor == "Seam" && x.SeamNo == strSeamNo))
                        {
                            var ltf002 = db.LTF002.Where(x => x.HeaderId == objLTF001.HeaderId && x.TestFor == "Seam" && x.SeamNo == strSeamNo && x.InspectionStatus != readyToOfferStatus && !string.IsNullOrEmpty(x.InspectionStatus)).ToList();
                            var maxOfferedOpertaionNo = ltf002.Any() ? ltf002.Max(x => x.OperationNo) : 0;
                            if (intOprNo < maxOfferedOpertaionNo)
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Operation no should be greater than " + maxOfferedOpertaionNo;
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else if ((strTestFor == "Part" || strTestFor == "Assembly") && !string.IsNullOrEmpty(strPartNo))
                    {
                        if (objLTF001.LTF002.Any(x => (x.TestFor == "Part" || x.TestFor == "Assembly") && x.PartNo == strPartNo))
                        {
                            var ltf002 = db.LTF002.Where(x => x.HeaderId == objLTF001.HeaderId && (x.TestFor == "Part" || x.TestFor == "Assembly") && x.PartNo == strPartNo && x.InspectionStatus != readyToOfferStatus && !string.IsNullOrEmpty(x.InspectionStatus)).ToList();
                            var maxOfferedOpertaionNo = ltf002.Any() ? ltf002.Max(x => x.OperationNo) : 0;
                            if (intOprNo < maxOfferedOpertaionNo)
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Operation no should be greater than " + maxOfferedOpertaionNo;
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    if (isLTFPSLIneExist(objLTF001.HeaderId, objLTF001.LTFPSNo, intOprNo, strTestFor, strSeamNo, strPartNo, strTestType, strStage))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record is already exist";
                    }
                    else
                    {
                        LTF002 objParent = db.LTF002.FirstOrDefault(c => c.ReworkHeaderId == objLTF001.HeaderId);

                        #region Add New LTFPS Line
                        objLTF002.HeaderId = objLTF001.HeaderId;
                        objLTF002.QualityProject = objLTF001.QualityProject;
                        objLTF002.Project = objLTF001.Project;
                        objLTF002.BU = objLTF001.BU;
                        objLTF002.Location = objLTF001.Location;
                        objLTF002.LTFPSNo = objLTF001.LTFPSNo;
                        objLTF002.LTFPSRevNo = objLTF001.RevNo.Value;
                        objLTF002.OperationNo = intOprNo;
                        objLTF002.RefDocument = strRefDocument;
                        objLTF002.DocRevNo = strDocRevNo;
                        objLTF002.TestFor = strTestFor;
                        objLTF002.SeamNo = strSeamNo;
                        objLTF002.PartNo = strPartNo;
                        objLTF002.OperationDescription = strOprDesc;
                        objLTF002.TestType = strTestType;
                        objLTF002.Stage = strStage;
                        objLTF002.StageSequence = strStageSequence;
                        objLTF002.Agency = strAgency;
                        objLTF002.TPI = strTPI;
                        objLTF002.DocumentRequired = boolDocumentRequired;
                        objLTF002.CreatedBy = objClsLoginInfo.UserName;
                        objLTF002.CreatedOn = DateTime.Now;
                        objLTF002.LineStatus = clsImplementationEnum.LTFPSLineStatus.Added.GetStringValue();
                        if (objParent != null)
                        {
                            objLTF002.ParentLineId = objParent.LineId;
                        }
                        db.LTF002.Add(objLTF002);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Operation added successfully";
                        objResponseMsg.HeaderId = objLTF001.HeaderId;
                        objResponseMsg.HeaderStatus = objLTF001.Status;
                        objResponseMsg.RevNo = Convert.ToString(objLTF001.RevNo);
                        #endregion
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateLTFPSLine(int lineId, int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            LTF002 objLTF002 = new LTF002();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    objLTF002 = db.LTF002.Where(i => i.LineId == lineId).FirstOrDefault();
                    if (columnName == "TestFor")
                    {
                        objLTF002.Stage = null;
                        objLTF002.StageSequence = null;
                        if (columnValue == "Seam")
                            objLTF002.PartNo = null;
                        else if (columnValue == "Part" || columnValue == "Assembly")
                            objLTF002.SeamNo = null;
                        objLTF002.TestFor = columnValue;
                        objLTF002.Stage = null;
                    }
                    else if (columnName == "SeamNo")
                    {
                        objLTF002.SeamNo = columnValue;
                        objLTF002.Stage = null;
                        objLTF002.StageSequence = null;
                    }
                    else if (columnName == "PartNo")
                    {
                        objLTF002.PartNo = columnValue;
                        objLTF002.Stage = null;
                        objLTF002.StageSequence = null;
                    }
                    else if (columnName == "TestType")
                    {
                        objLTF002.TestType = columnValue;
                        objLTF002.Stage = null;
                        objLTF002.StageSequence = null;
                    }
                    else if (columnName == "Stage")
                    {
                        objLTF002.Stage = columnValue;
                        objLTF002.StageSequence = null;
                    }
                    else if (columnName == "StageSequence")
                    {
                        int seq = Convert.ToInt32(columnValue.Split('-')[0]);
                        objLTF002.StageSequence = seq;
                    }
                    else
                    {
                        db.SP_LTFPS_UPDATE_LINE_COLUMN(lineId, columnName, columnValue);
                    }

                    if (objLTF002.LTF001.RevNo > 0)
                    {
                        objLTF002.LineStatus = clsImplementationEnum.LTFPSLineStatus.Modified.GetStringValue();
                    }

                    objLTF002.EditedBy = objClsLoginInfo.UserName;
                    objLTF002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Saved Successfully";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLTFPSLine(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                LTF002 objLTF002 = db.LTF002.FirstOrDefault(c => c.LineId == lineId);

                if (objLTF002 != null)
                {
                    if (objLTF002.LTF001.RevNo > 0)
                    {
                        objLTF002.LineStatus = clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue();
                        objLTF002.EditedBy = objClsLoginInfo.UserName;
                        objLTF002.EditedOn = DateTime.Now;
                    }
                    else
                    {
                        db.LTF002.Remove(objLTF002);
                    }
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Operation deleted successfully";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool isLTFPSLIneExist(int headerId, string ltfpsNo, int oprNo, string testFor, string seamNo, string partNo, string testType, string stage)
        {
            return db.LTF002.Any(i => i.HeaderId == headerId
                                && i.LTFPSNo.Equals(ltfpsNo, StringComparison.OrdinalIgnoreCase)
                                && i.OperationNo == oprNo
                                && i.TestFor.Equals(testFor, StringComparison.OrdinalIgnoreCase)
                                && i.SeamNo.Equals(seamNo, StringComparison.OrdinalIgnoreCase)
                                && i.PartNo.Equals(partNo, StringComparison.OrdinalIgnoreCase)
                                && i.TestType.Equals(testType, StringComparison.OrdinalIgnoreCase)
                                && i.Stage.Equals(stage, StringComparison.OrdinalIgnoreCase));
        }

        public ActionResult GetSeamWisePartDataPartial(string QualityProject, string SeamNo)
        {
            SeamDetails objSeamDetails = new SeamDetails();
            QMS012 objQMS012 = db.QMS012.Where(x => x.QualityProject == QualityProject && x.SeamNo == SeamNo).FirstOrDefault();
            if (objQMS012 != null && !string.IsNullOrWhiteSpace(objQMS012.Position))
            {
                string[] arrayPosition = objQMS012.Position.Split(',').Select(x => x).ToArray();
                //var lst = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == objQMS012.Project && arrayPosition.Contains(x.FindNo)).Select(x => new PartDetails { FindNo = x.FindNo, Part = x.Part }).ToList();

                var lst = Manager.GetHBOMData(objQMS012.Project).Where(x => arrayPosition.Contains(x.FindNo)).Select(x => new PartDetails { FindNo = x.FindNo, Part = x.Part }).ToList();

                objSeamDetails.lstPartDetails = lst;
            }

            objSeamDetails.SeamNo = objQMS012.SeamNo;
            objSeamDetails.SeamListNo = objQMS012.SeamListNo;
            return PartialView("_GetSeamWisePartDataPartial", objSeamDetails);
        }

        #endregion

        #region History Detail Page

        [HttpPost]
        public ActionResult GetHistoryDataPartial(int HeaderId)
        {
            LTF001_Log objLTF001_Log = new LTF001_Log();
            objLTF001_Log = db.LTF001_Log.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            return PartialView("_GetLTFPSHistoryDataPartial", objLTF001_Log);
        }

        public ActionResult HistoryDetails(int? id)
        {
            LTF001_Log objLTF001 = new LTF001_Log();

            if (id > 0)
            {
                objLTF001 = db.LTF001_Log.Where(i => i.Id == id).FirstOrDefault();
                objLTF001.Project = db.COM001.Where(i => i.t_cprj.Equals(objLTF001.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objLTF001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objLTF001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objLTF001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objLTF001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.RevNo = "R" + Convert.ToString(objLTF001.RevNo);

                ViewBag.isReturned = false;
                if (objLTF001.WEReturnedBy != null)
                {
                    ViewBag.isReturned = true;
                    ViewBag.ReturnBy = objLTF001.WEReturnedBy + " - " + Manager.GetUserNameFromPsNo(objLTF001.WEReturnedBy);
                    ViewBag.ReturnDate = objLTF001.WEReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                }
                else if (objLTF001.QCReturnedBy != null)
                {
                    ViewBag.isReturned = true;
                    ViewBag.ReturnBy = objLTF001.QCReturnedBy + " - " + Manager.GetUserNameFromPsNo(objLTF001.QCReturnedBy);
                    ViewBag.ReturnDate = objLTF001.QCReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                }
                else if (objLTF001.CustomerReturnedBy != null)
                {
                    ViewBag.isReturned = true;
                    ViewBag.ReturnBy = objLTF001.CustomerReturnedBy + " - " + Manager.GetUserNameFromPsNo(objLTF001.CustomerReturnedBy);
                    ViewBag.ReturnDate = objLTF001.CustomerReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                }
                else if (objLTF001.PMGReturnedBy != null)
                {
                    ViewBag.isReturned = true;
                    ViewBag.ReturnBy = objLTF001.PMGReturnedBy + " - " + Manager.GetUserNameFromPsNo(objLTF001.PMGReturnedBy);
                    ViewBag.ReturnDate = objLTF001.PMGReturnedOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                }

                ViewBag.isReleased = false;
                if (objLTF001.PMGReleaseBy != null)
                {
                    ViewBag.isReturned = true;
                    ViewBag.ReleaseBy = objLTF001.PMGReleaseBy + " - " + Manager.GetUserNameFromPsNo(objLTF001.PMGReleaseBy);
                    ViewBag.ReleaseDate = objLTF001.PMGReleaseOn.Value.ToString("dd/MM/yyyy hh:mm tt");
                }

            }

            return View(objLTF001);
        }

        [HttpPost]
        public ActionResult GetLTFPSHistoryHeaderData(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND ltf001.HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (ltf001.LTFPSNo like '%" + param.sSearch
                        + "%' or ltf001.RevNo like '%" + param.sSearch
                        + "%' or ltf001.Status like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_LTFPS_GET_HISTORY_HEADER_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.LTFPSNo),
                               Convert.ToString(uc.RevNo),
                               Convert.ToString(uc.Status),
                               "<a target='_blank' href='"+ WebsiteURL + "/LTFPS/MaintainLTFPS/HistoryDetails?id=" + uc.Id + "'><i style='margin-right:10px;' class='fa fa-eye'></i></a>" +
                               HTMLActionString(uc.HeaderId,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/LTFPS/MaintainLTFPS/ShowHistoryTimeline?HeaderID="+ uc.Id +"\");"),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetLTFPSHistoryLinesData(JQueryDataTableParamModel param, string RefId)
        {
            try
            {
                int intRefId = Convert.ToInt32(RefId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(RefId))
                {
                    whereCondition += " AND RefId = " + RefId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (ltf002.OperationNo like '%" + param.sSearch
                        + "%' or ltf002.RefDocument like '%" + param.sSearch
                        + "%' or ltf002.TestFor like '%" + param.sSearch
                        + "%' or ltf002.SeamNo like '%" + param.sSearch
                        + "%' or ltf002.PartNo like '%" + param.sSearch
                        + "%' or ltf002.OperationDescription like '%" + param.sSearch
                        + "%' or ltf002.TestType like '%" + param.sSearch
                        + "%' or ltf002.Stage like '%" + param.sSearch
                        + "%' or ltf002.StageSequence like '%" + param.sSearch
                        + "%' or ltf002.Agency like '%" + param.sSearch
                        + "%' or ltf002.TPI like '%" + param.sSearch
                        + "%' or ltf002.DocumentRequired like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_LTFPS_GET_HISTORY_LINES_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.OperationNo),
                               Convert.ToString(uc.RefDocument),
                               Convert.ToString(uc.DocRevNo),
                               Convert.ToString(uc.TestFor),
                               Convert.ToString(uc.SeamNo),
                               Convert.ToString(uc.PartNo),
                               Convert.ToString(uc.OperationDescription),
                               Convert.ToString(uc.TestType),
                               Convert.ToString(uc.Stage),
                               Convert.ToString(uc.StageSequence),
                               Convert.ToString(uc.Agency),
                               Convert.ToString(uc.TPI),
                               Convert.ToString(uc.DocumentRequired),
                               HTMLActionString(uc.LineId,"Timeline","Timeline","fa fa-clock-o", "ShowTimeline(\"/LTFPS/MaintainLTFPS/ShowHistoryTimeline?HeaderID="+ uc.HeaderId +"&LineId="+ uc.Id +"\");"),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Copy

        [HttpPost]
        public ActionResult GetCopyDetails(string qualityProject, string location)
        {
            var status = clsImplementationEnum.LTFPSHeaderStatus.Released.GetStringValue();
            ViewBag.QualityProject = qualityProject;
            ViewBag.Location = location;
            var lstProjectByUser = Manager.getProjectsByUser(objClsLoginInfo.UserName.Trim());
            string userLocation = objClsLoginInfo.Location.Trim();
            var lstQualityProject = (from lst in lstProjectByUser
                                     join qms001 in db.QMS001 on lst.projectCode equals qms001.Project
                                     join qms010 in db.QMS010 on qms001.Project equals qms010.Project
                                     where qms001.IsLTFPS == true && qms010.Location == location
                                     select new { id = qms010.QualityProject, text = qms010.QualityProject }).Distinct().ToList();

            var lstIdenticalproject = db.LTF004.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.IdenticalProject).Select(i => new { id = i.IdenticalProject, text = i.IdenticalProject }).ToList();
            ViewBag.LTFPSNo = db.LTF001.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Status.Equals(status)).OrderBy(i => i.LTFPSNo).Select(i => new { id = i.LTFPSNo, rev = i.RevNo }).ToList();
            ViewBag.DestinationLTFPSNo = db.LTF001.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.LTFPSNo).Select(i => new { id = i.LTFPSNo, rev = i.RevNo }).ToList();
            ViewBag.IdenticalProject = lstIdenticalproject.Union(lstQualityProject).Where(x => x.id != qualityProject).Distinct(); // db.LTF004.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.IdenticalProject).Select(i => i.IdenticalProject).ToList();
            return PartialView("_LoadLTFPSCopyPartial");
        }

        [HttpPost]
        public ActionResult copyDetails(string sourceProject, string location, string LTFPSNo, int RevNo, string destProject, string destLTFPS, bool IsWithinProj, string destprojwiseLTFPS)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var seamStatus = clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue();
                bool IsCopy = false; LTF001 newObjLTF001 = new LTF001();
                var objLTF001 = db.LTF001.Where(i => i.QualityProject.Equals(sourceProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.LTFPSNo.Equals(LTFPSNo, StringComparison.OrdinalIgnoreCase) && i.RevNo == RevNo).FirstOrDefault();
                bool IsIdenticalproject = false;
                if (!string.IsNullOrWhiteSpace(destProject))
                {
                    IsIdenticalproject = db.LTF004.Where(i => i.QualityProject.Equals(sourceProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.IdenticalProject == destProject).Any();
                }
                if (objLTF001 != null)
                {
                    foreach (var ltf002 in objLTF001.LTF002)
                    {
                        if (ltf002.TestFor.Equals("Seam") && !string.IsNullOrEmpty(ltf002.SeamNo) && !string.IsNullOrEmpty(ltf002.Stage))
                        {
                            if (!db.QMS031.Any(x => x.QualityProject.Equals(ltf002.QualityProject) && x.Location.Equals(ltf002.Location) && x.BU.Equals(ltf002.BU) && x.SeamNo.Equals(ltf002.SeamNo) && x.StageCode.Equals(ltf002.Stage) && x.StageStatus.Equals(seamStatus)))
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Stage not approved";
                            }
                        }
                        else if ((ltf002.TestFor.Equals("Part") || ltf002.TestFor.Equals("Assembly")) && !string.IsNullOrEmpty(ltf002.PartNo) && !string.IsNullOrEmpty(ltf002.Stage))
                        {
                            if (!db.QMS036.Any(x => x.QualityProject.Equals(ltf002.QualityProject) && x.Location.Equals(ltf002.Location) && x.BU.Equals(ltf002.BU) && x.PartNo.Equals(ltf002.PartNo) && x.StageCode.Equals(ltf002.Stage) && x.StageStatus.Equals(seamStatus)))
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Stage not approved";
                            }
                        }
                    }

                    if (IsWithinProj)
                    {
                        newObjLTF001 = db.LTF001.Where(x => x.LTFPSNo == destLTFPS && x.QualityProject == sourceProject && x.Location == location).FirstOrDefault();
                        if (!(newObjLTF001.LTF002.Any()))
                        {
                            IsCopy = true;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "LTFPS No :" + destLTFPS + " already have operations.";
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(destprojwiseLTFPS))
                        {
                            newObjLTF001 = db.LTF001.Where(x => x.LTFPSNo == destprojwiseLTFPS && x.QualityProject == destProject && x.Location == location).FirstOrDefault();
                            if (!(newObjLTF001.LTF002.Any()))
                            {
                                IsCopy = true;
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "LTFPS No :" + destprojwiseLTFPS + " already have operations.";
                            }
                        }
                        else if (db.LTF001.Any(i => i.QualityProject.Equals(destProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.LTFPSNo.Equals(LTFPSNo, StringComparison.OrdinalIgnoreCase) && i.RevNo == RevNo))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Data already exists";
                        }
                        else
                        {
                            IsCopy = true;
                            newObjLTF001 = new LTF001();
                            newObjLTF001.QualityProject = destProject;
                            newObjLTF001.Project = objLTF001.Project;
                            newObjLTF001.BU = objLTF001.BU;
                            newObjLTF001.Location = objLTF001.Location;
                            newObjLTF001.LTFPSNo = objLTF001.LTFPSNo;
                            newObjLTF001.LTFPSDescription = objLTF001.LTFPSDescription;
                            newObjLTF001.RevNo = 0;
                            newObjLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue();
                            newObjLTF001.CreatedBy = objClsLoginInfo.UserName;
                            newObjLTF001.CreatedOn = DateTime.Now;
                            db.LTF001.Add(newObjLTF001);
                            db.SaveChanges();
                        }
                    }
                    if (IsCopy)
                    {
                        var headerId = newObjLTF001.HeaderId;
                        List<LTF002> lstLTF002 = new List<LTF002>();

                        foreach (var ltf002 in objLTF001.LTF002)
                        {
                            lstLTF002.Add(new LTF002
                            {
                                HeaderId = headerId,
                                QualityProject = ltf002.QualityProject,
                                Project = ltf002.Project,
                                BU = ltf002.BU,
                                Location = ltf002.Location,
                                LTFPSNo = IsWithinProj ? newObjLTF001.LTFPSNo : ltf002.LTFPSNo,
                                LTFPSRevNo = IsWithinProj ? Convert.ToInt32(newObjLTF001.RevNo) : 0,
                                OperationNo = ltf002.OperationNo,
                                OperationDescription = ltf002.OperationDescription,
                                RefDocument = ltf002.RefDocument,
                                DocRevNo = ltf002.DocRevNo,
                                TestFor = ltf002.TestFor,
                                SeamNo = (IsWithinProj || IsIdenticalproject) ? "" : ltf002.SeamNo,
                                PartNo = (IsWithinProj || IsIdenticalproject) ? "" : ltf002.PartNo,
                                TestType = ltf002.TestType,
                                Agency = ltf002.Agency,
                                TPI = ltf002.TPI,
                                DocumentRequired = ltf002.DocumentRequired,
                                CreatedBy = objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now,
                            });
                        }

                        db.LTF002.AddRange(lstLTF002);
                        db.SaveChanges();

                        List<LTF003> lstLTF003 = new List<LTF003>();
                        foreach (var ltf003 in objLTF001.LTF003)
                        {
                            lstLTF003.Add(new LTF003
                            {
                                HeaderId = headerId,
                                QualityProject = ltf003.QualityProject,
                                LTFPSNo = IsWithinProj ? newObjLTF001.LTFPSNo : ltf003.LTFPSNo,
                                LTFPSRevNo = IsWithinProj ? Convert.ToInt32(newObjLTF001.RevNo) : ltf003.LTFPSRevNo,
                                Project = ltf003.Project,
                                Location = ltf003.Location,
                                BU = ltf003.BU,
                                AssemblyFindNo = ltf003.AssemblyFindNo,
                                AssemblyPartNo = ltf003.AssemblyPartNo,
                                CreatedBy = objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now,
                            });
                        }
                        db.LTF003.AddRange(lstLTF003);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Data copied successfully";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Timeline
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "LTFPS Timeline";

            if (LineId > 0)
            {
                model.Title = "LTFPS Lines";
                LTF002 objLTF002 = db.LTF002.Where(x => x.LineId == LineId).FirstOrDefault();
                model.CreatedBy = objLTF002.CreatedBy != null ? Manager.GetUserNameFromPsNo(objLTF002.CreatedBy) : null;
                model.CreatedOn = objLTF002.CreatedOn;
                model.EditedBy = objLTF002.EditedBy != null ? Manager.GetUserNameFromPsNo(objLTF002.EditedBy) : null;
                model.EditedOn = objLTF002.EditedOn;
            }
            else
            {
                model.Title = "LTFPS Header";
                LTF001 objLTF001 = db.LTF001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objLTF001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.CreatedBy) : null;
                model.CreatedOn = objLTF001.CreatedOn;
                model.EditedBy = objLTF001.EditedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.EditedBy) : null;
                model.EditedOn = objLTF001.EditedOn;
                model.SubmittedBy = objLTF001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.SubmittedBy) : null;
                model.SubmittedOn = objLTF001.SubmittedOn;
                if (objLTF001.WEReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.WEReturnedBy);
                    model.ReturnedOn = objLTF001.WEReturnedOn;
                }
                else if (objLTF001.QCReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.QCReturnedBy);
                    model.ReturnedOn = objLTF001.QCReturnedOn;
                }
                else if (objLTF001.CustomerReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.CustomerReturnedBy);
                    model.ReturnedOn = objLTF001.CustomerReturnedOn;
                }
                else if (objLTF001.PMGReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.PMGReturnedBy);
                    model.ReturnedOn = objLTF001.PMGReturnedOn;
                }
                else
                {
                    model.ReturnedBy = null;
                    model.ReturnedOn = null;
                }
                model.WEApprovedBy = objLTF001.WEApprovedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.WEApprovedBy) : null;
                model.WEApprovedOn = objLTF001.WEApprovedOn;
                model.QCApprovedBy = objLTF001.QCApprovedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.QCApprovedBy) : null;
                model.QCApprovedOn = objLTF001.QCApprovedOn;
                model.CustomerApprovedBy = objLTF001.CustomerApprovedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.CustomerApprovedBy) : null;
                model.CustomerApprovedOn = objLTF001.CustomerApprovedOn;
                model.PMGReleaseBy = objLTF001.PMGReleaseBy != null ? Manager.GetUserNameFromPsNo(objLTF001.PMGReleaseBy) : null;
                model.PMGReleaseOn = objLTF001.PMGReleaseOn;

            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "LTFPS Timeline";

            if (LineId > 0)
            {
                model.Title = "LTFPS Lines";
                LTF002_Log objLTF002 = db.LTF002_Log.Where(x => x.Id == LineId).FirstOrDefault();
                model.CreatedBy = objLTF002.CreatedBy != null ? Manager.GetUserNameFromPsNo(objLTF002.CreatedBy) : null;
                model.CreatedOn = objLTF002.CreatedOn;
                model.EditedBy = objLTF002.EditedBy != null ? Manager.GetUserNameFromPsNo(objLTF002.EditedBy) : null;
                model.EditedOn = objLTF002.EditedOn;
            }
            else
            {
                model.Title = "LTFPS Header";
                LTF001_Log objLTF001 = db.LTF001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objLTF001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.CreatedBy) : null;
                model.CreatedOn = objLTF001.CreatedOn;
                model.EditedBy = objLTF001.EditedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.EditedBy) : null;
                model.EditedOn = objLTF001.EditedOn;
                model.SubmittedBy = objLTF001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.SubmittedBy) : null;
                model.SubmittedOn = objLTF001.SubmittedOn;
                if (objLTF001.WEReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.WEReturnedBy);
                    model.ReturnedOn = objLTF001.WEReturnedOn;
                }
                else if (objLTF001.QCReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.QCReturnedBy);
                    model.ReturnedOn = objLTF001.QCReturnedOn;
                }
                else if (objLTF001.CustomerReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.CustomerReturnedBy);
                    model.ReturnedOn = objLTF001.CustomerReturnedOn;
                }
                else if (objLTF001.PMGReturnedBy != null)
                {
                    model.ReturnedBy = Manager.GetUserNameFromPsNo(objLTF001.PMGReturnedBy);
                    model.ReturnedOn = objLTF001.PMGReturnedOn;
                }
                else
                {
                    model.ReturnedBy = null;
                    model.ReturnedOn = null;
                }
                model.WEApprovedBy = objLTF001.WEApprovedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.WEApprovedBy) : null;
                model.WEApprovedOn = objLTF001.WEApprovedOn;
                model.QCApprovedBy = objLTF001.QCApprovedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.QCApprovedBy) : null;
                model.QCApprovedOn = objLTF001.QCApprovedOn;
                model.CustomerApprovedBy = objLTF001.CustomerApprovedBy != null ? Manager.GetUserNameFromPsNo(objLTF001.CustomerApprovedBy) : null;
                model.CustomerApprovedOn = objLTF001.CustomerApprovedOn;
                model.PMGReleaseBy = objLTF001.PMGReleaseBy != null ? Manager.GetUserNameFromPsNo(objLTF001.PMGReleaseBy) : null;
                model.PMGReleaseOn = objLTF001.PMGReleaseOn;

            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion

        #region Common Methods

        [HttpPost]
        public ActionResult GetQualityProjectswiseLTFPS(string qualityProject, string location)
        {
            var lstQualityProject = db.LTF001.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.LTFPSNo).Select(i => new { id = i.LTFPSNo, rev = i.RevNo }).ToList();

            var objData = new
            {
                count = lstQualityProject.Count,
                lstQualityProject = lstQualityProject
            };

            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityProjects(string term)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<Projects> lstQualityProject = new List<Projects>();
            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(term.ToLower())
                                                                 && lstCOMLocation.Contains(i.Location)
                                                                 && lstCOMBU.Contains(i.BU)
                                                                 && db.QMS001.Any(c => c.Project == i.Project && c.Location == i.Location && c.BU == i.BU && c.IsLTFPS == true)
                                                               ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                        && lstCOMBU.Contains(i.BU)
                                                        && db.QMS001.Any(c => c.Project == i.Project && c.Location == i.Location && c.BU == i.BU && c.IsLTFPS == true)
                                                        ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Take(10).Distinct().ToList();
            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        public bool isLTFPSProject(string Project, string Location, string BU)
        {
            return db.QMS001.Any(c => c.Project == Project && c.Location == Location && c.BU == BU && c.IsLTFPS == true);
        }

        [HttpPost]
        public ActionResult GetQualityProjectWiseProjectBUDetail(string qualityProject)
        {
            QualityProjectsDetails objProjectsDetails = new QualityProjectsDetails();
            var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();

            objProjectsDetails.ProjectCode = project;
            objProjectsDetails.projectDescription = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;

            var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);

            objProjectsDetails.BUCode = ProjectWiseBULocation.QCPBU.Split('-')[0].Trim();
            objProjectsDetails.BUDescription = ProjectWiseBULocation.QCPBU;

            objProjectsDetails.LocationCode = objClsLoginInfo.Location;
            objProjectsDetails.LocationDescription = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

            return Json(objProjectsDetails, JsonRequestBehavior.AllowGet);
        }

        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PMG2.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Releaser.GetStringValue();
                }
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.CR1.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.CR1.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                }
                //else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                //{
                //    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                //    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                //}
                //else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                //{
                //    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.WE2.GetStringValue();
                //    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Approver.GetStringValue();
                //}
                else if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PMG3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }
        //[HttpPost]
        public ActionResult GetIdenticalProjects(string search, string param, string project, string location)
        {
            List<ddlValue> lstIdenticalProject = new List<ddlValue>();
            List<QMS010> objQMS010;
            var status = clsImplementationEnum.SeamListStatus.Approved.GetStringValue();
            if (db.QMS001.Any(i => i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.IsLTFPS))
            {
                if (!string.IsNullOrEmpty(search))
                {
                    objQMS010 = db.QMS010.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && !i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase) && i.QualityProject.ToLower().Contains(search.ToLower())).Distinct().ToList();
                }
                else
                {
                    objQMS010 = db.QMS010.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && !i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase)).Distinct().ToList();
                }
                foreach (var qms010 in objQMS010)
                {
                    var objLTF001 = db.LTF001.Where(i => i.QualityProject.Equals(param) && i.Location.Equals(location)).ToList();
                    bool isValid = true;
                    var lstLTF002 = db.LTF002.Where(i => i.QualityProject.Equals(param) && i.Location.Equals(location)).ToList();
                    foreach (var ltf002 in lstLTF002)
                    {
                        if (ltf002.TestFor.Equals("Seam") && !string.IsNullOrEmpty(ltf002.SeamNo))
                        {
                            if (!db.QMS012.Any(x => x.QualityProject.Equals(qms010.QualityProject) && x.SeamNo.Equals(ltf002.SeamNo) && x.Status.Equals(status)))
                            {
                                isValid = false;
                                break;
                            }
                            if (!string.IsNullOrEmpty(ltf002.Stage))
                            {
                                if (!db.QMS031.Any(x => x.QualityProject.Equals(qms010.QualityProject) && x.Location.Equals(qms010.Location) && x.BU.Equals(qms010.BU) && x.SeamNo.Equals(ltf002.SeamNo) && x.StageCode.Equals(ltf002.Stage) && x.StageStatus.Equals(status)))
                                {
                                    isValid = false;
                                    break;
                                }
                            }
                        }
                        else if ((ltf002.TestFor.Equals("Part") || ltf002.TestFor.Equals("Assembly")) && !string.IsNullOrEmpty(ltf002.PartNo) && !string.IsNullOrEmpty(ltf002.Stage))
                        {
                            if (!db.QMS036.Any(x => x.QualityProject.Equals(qms010.QualityProject) && x.Location.Equals(qms010.Location) && x.BU.Equals(qms010.BU) && x.PartNo.Equals(ltf002.PartNo) && x.StageCode.Equals(ltf002.Stage) && x.StageStatus.Equals(status)))
                            {
                                isValid = false;
                                break;
                            }
                        }
                    }
                    if (isValid)
                    {
                        lstIdenticalProject.Add(new ddlValue { id = qms010.QualityProject, text = qms010.QualityProject });
                    }
                }
            }
            return Json(lstIdenticalProject, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetIdenticalProjectsEdit(string param, string project, string location)
        {
            //List<ddlValue> identicalProject = new List<ddlValue>();
            var identicalProject = db.LTF004.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && i.QualityProject.Equals(param, StringComparison.OrdinalIgnoreCase)).Select(i => i.IdenticalProject).Distinct().ToList();
            return Json(identicalProject, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveIdenticalProject(string qualityProject, string strIdenticalProj)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<LTF004> lstExistingProject = new List<LTF004>();
            List<LTF004> lstLTF004 = new List<LTF004>();
            try
            {
                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx).FirstOrDefault();

                //if (!string.IsNullOrEmpty(strIdenticalProj))
                //{
                string[] arrIdenticalProj = string.IsNullOrEmpty(strIdenticalProj) ? new string[] { } : strIdenticalProj.Split(',');
                lstExistingProject = db.LTF004.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).ToList();
                //&& i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)

                if (lstExistingProject.Any())
                {
                    db.LTF004.RemoveRange(lstExistingProject);
                    db.SaveChanges();
                }
                foreach (var item in arrIdenticalProj)
                {
                    lstLTF004.Add(new LTF004
                    {
                        QualityProject = qualityProject,
                        Project = GetProject(qualityProject),
                        BU = GetBU(qualityProject),
                        Location = location,
                        IdenticalProject = item,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                }
                db.LTF004.AddRange(lstLTF004);
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Identical Project Saved Successfully";

                //}
                //else
                //{
                //    objResponseMsg.Key = false;
                //    objResponseMsg.Value = "Identical Project is not available";
                //}
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string GetProject(string qualityProject)
        {
            return db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
        }
        public string GetBU(string qualityProject)
        {
            var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
            return db.COM001.Where(i => i.t_cprj == project).FirstOrDefault().t_entu;
        }
        public string getCategoryDesc(string Key, string BU, string loc, string code)
        {
            string catDesc = (from glb002 in db.GLB002
                              join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                              where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && BU.Contains(glb002.BU) && loc.Equals(glb002.Location) && code.Equals(glb002.Code)
                              select glb002.Description).FirstOrDefault();
            return catDesc;
        }
        public JsonResult getSeamNo(int HeaderId)
        {
            var objLTF001 = db.LTF001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            var Status = clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue();
            var SeamNo = db.QMS012.Where(i => i.QualityProject.Equals(objLTF001.QualityProject) && i.Status.Equals(Status)).OrderBy(i => i.SeamNo).Distinct().Select(i => new CategoryData() { Value = i.SeamNo, Code = i.SeamNo, CategoryDescription = i.SeamNo }).ToList();
            return Json(SeamNo, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getPartNo(int HeaderId)
        {
            var objLTF001 = db.LTF001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            var Status = clsImplementationEnum.ICLPartStatus.Approved.GetStringValue();

            var PartNo = Manager.GetHBOMData(objLTF001.Project).OrderBy(i => i.FindNo).Select(i => new CategoryData() { Value = i.FindNo.ToString(), Code = i.FindNo.ToString(), CategoryDescription = i.FindNo.ToString() }).Distinct().ToList();

            //var PartNo = db.VW_IPI_GETHBOMLIST.Where(x => x.Project.Equals(objLTF001.Project)).OrderBy(i => i.FindNo).Select(i => new CategoryData() { Value = i.FindNo.ToString(), Code = i.FindNo.ToString(), CategoryDescription = i.FindNo.ToString() }).Distinct().ToList();
            return Json(PartNo, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getStage(string TestFor, string SeamPartNo, string TestType, int HeaderId)
        {
            var stageStatus = clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue();
            var objLTF001 = db.LTF001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            List<CategoryData> stage = new List<CategoryData>();
            if (TestFor.Equals("Seam"))
            {
                stage = (from qms031 in db.QMS031
                         join qms002 in db.QMS002
                         on new { qms031.Location, qms031.BU, qms031.StageCode } equals new { qms002.Location, qms002.BU, qms002.StageCode }
                         where qms031.QualityProject.Equals(objLTF001.QualityProject) && qms031.Location.Equals(objLTF001.Location) && qms031.BU.Equals(objLTF001.BU) && qms031.SeamNo.Equals(SeamPartNo) && qms031.StageStatus.Equals(stageStatus) && (qms002.StageType.Equals(TestType))
                         orderby qms031.StageCode
                         select new CategoryData() { Value = qms031.StageCode, Code = qms031.StageCode, CategoryDescription = qms031.StageCode }
                         ).Distinct().ToList();
            }
            else if (TestFor.Equals("Part") || TestFor.Equals("Assembly"))
            {
                stage = (from qms036 in db.QMS036
                         join qms002 in db.QMS002
                         on new { qms036.Location, qms036.BU, qms036.StageCode } equals new { qms002.Location, qms002.BU, qms002.StageCode }
                         where qms036.QualityProject.Equals(objLTF001.QualityProject) && qms036.Location.Equals(objLTF001.Location) && qms036.BU.Equals(objLTF001.BU) && qms036.PartNo.Equals(SeamPartNo) && qms036.StageStatus.Equals(stageStatus) && (qms002.StageType.Equals(TestType))
                         orderby qms036.StageCode
                         select new CategoryData() { Value = qms036.StageCode, Code = qms036.StageCode, CategoryDescription = qms036.StageCode }
                         ).Distinct().ToList();
            }
            return Json(stage, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getStageSequence(string TestFor, string SeamPartNo, string TestType, int HeaderId, string Stage)
        {
            var stageStatus = clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue();
            var objLTF001 = db.LTF001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            List<CategoryData> stage = new List<CategoryData>(); List<CategoryData> stageSeq = new List<CategoryData>();
            //var stageSeq = new List<int?>();
            if (TestFor.Equals("Seam"))
            {
                stageSeq = (from qms031 in db.QMS031
                            join qms002 in db.QMS002
                            on new { qms031.Location, qms031.BU, qms031.StageCode } equals new { qms002.Location, qms002.BU, qms002.StageCode }
                            where qms031.QualityProject.Equals(objLTF001.QualityProject) && qms031.Location.Equals(objLTF001.Location) && qms031.BU.Equals(objLTF001.BU) && qms031.SeamNo.Equals(SeamPartNo) && qms031.StageStatus.Equals(stageStatus) && (qms002.StageType.Equals(TestType) && qms031.StageCode.Equals(Stage))
                            orderby qms031.StageSequance
                            select new CategoryData { seq = qms031.StageSequance, text = qms031.StageSequance + " - " + qms031.Remarks }
                         ).Distinct().ToList();


            }
            else if (TestFor.Equals("Part") || TestFor.Equals("Assembly"))
            {
                stageSeq = (from qms036 in db.QMS036
                            join qms002 in db.QMS002
                            on new { qms036.Location, qms036.BU, qms036.StageCode } equals new { qms002.Location, qms002.BU, qms002.StageCode }
                            where qms036.QualityProject.Equals(objLTF001.QualityProject) && qms036.Location.Equals(objLTF001.Location) && qms036.BU.Equals(objLTF001.BU) && qms036.PartNo.Equals(SeamPartNo) && qms036.StageStatus.Equals(stageStatus) && (qms002.StageType.Equals(TestType) && qms036.StageCode.Equals(Stage))
                            orderby qms036.StageSequence
                            select new CategoryData { seq = qms036.StageSequence, text = qms036.StageSequence + " - " + qms036.Remarks }
                         ).Distinct().ToList();
            }

            stage = (from seq in stageSeq
                     select new CategoryData() { Value = Convert.ToString(seq.seq), Code = Convert.ToString(seq.seq), CategoryDescription = Convert.ToString(seq.text) }
                         ).Distinct().ToList();

            return Json(stage, JsonRequestBehavior.AllowGet);
        }
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
        public string GenerateAutoCompleteOnBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", string oldValue = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;
            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";
            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElementId + "' value='" + inputValue + "' data-oldvalue='" + oldValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }
        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;
            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";
            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' data-oldvalue='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public string GenerateRemark(int headerId, string columnName, string userAccessRole, string userRole, bool isEditable = false)
        {
            LTF001 objLTF001 = db.LTF001.FirstOrDefault(c => c.HeaderId == headerId);
            //bool isEditable = false;
            //if (objLTF001 != null)
            //{
            //    //if (objLTF001.Status == clsImplementationEnum.LTFPSHeaderStatus.SentForApproval.GetStringValue() && userAccessRole == clsImplementationEnum.UserAccessRole.Approver.GetStringValue() && userRole == clsImplementationEnum.UserRoleName.WE2.GetStringValue())
            //    //    isEditable = true;
            //    //else if (objLTF001.Status == clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue() && objLTF001.WEApprovedBy != null && objLTF001.QCApprovedBy == null && userAccessRole == clsImplementationEnum.UserAccessRole.Approver.GetStringValue() && userRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
            //    //    isEditable = true;
            //    //else 
            //    if (objLTF001.Status == clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue() && objLTF001.WEApprovedBy != null && objLTF001.QCApprovedBy != null && objLTF001.CustomerApprovedBy == null && userAccessRole == clsImplementationEnum.UserAccessRole.Approver.GetStringValue() && userRole == clsImplementationEnum.UserRoleName.CR1.GetStringValue())
            //        isEditable = true;
            //    else if (objLTF001.Status == clsImplementationEnum.LTFPSHeaderStatus.Approved.GetStringValue() && objLTF001.WEApprovedBy != null && objLTF001.QCApprovedBy != null && objLTF001.CustomerApprovedBy != null && objLTF001.PMGReleaseBy == null && userAccessRole == clsImplementationEnum.UserAccessRole.Releaser.GetStringValue() && userRole == clsImplementationEnum.UserRoleName.PMG2.GetStringValue())
            //        isEditable = true;

            //    if(userRole == clsImplementationEnum.UserRoleName.CR1.GetStringValue() && userAccessRole == clsImplementationEnum.UserAccessRole.Approver.GetStringValue())



            //    else
            //        isEditable = false;
            //}

            string remarkValue = (!string.IsNullOrWhiteSpace(objLTF001.WEReturnedBy) ? objLTF001.WEReturnRemarks : (!string.IsNullOrWhiteSpace(objLTF001.QCReturnedBy) ? objLTF001.QCReturnRemarks : (!string.IsNullOrWhiteSpace(objLTF001.CustomerReturnedBy) ? objLTF001.CustomerReturnRemarks : (!string.IsNullOrWhiteSpace(objLTF001.PMGReturnedBy) ? objLTF001.PMGReturnRemarks : ""))));

            if (isEditable)
            {
                return Helper.GenerateTextbox(objLTF001.HeaderId, columnName, remarkValue, "", false, "", "200");
            }
            else
            {
                return remarkValue;
            }

        }
        public class ResponceMsgWithMultiValue : clsHelper.ResponseMsg
        {
            public string pendingLTFPS { get; set; }
            public string submittedLTFPS { get; set; }
            public string exisingLTFPS { get; set; }
            public bool isIdenticalProjectEditable { get; set; }
        }
        #endregion

        #region ExcelHelper
        //[HttpPost]
        //public ActionResult ImportxcelPartitial(int headerId)
        //{
        //    LTF001 objLTF001 = new LTF001();

        //    if (headerId > 0)
        //        objLTF001 = db.LTF001.Where(i => i.HeaderId == headerId).FirstOrDefault();

        //    return PartialView("_GetLTFPSLinesDataPartial", objLTF001);
        //}

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload, string aQualityProject, string aProject, string aBU, string aLocation)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string qualityProject = string.Empty;
            string LTFPSNo = string.Empty;
            string LTFPSDes = string.Empty;
            if (upload != null && !string.IsNullOrWhiteSpace(aQualityProject) && !string.IsNullOrWhiteSpace(aProject) && !string.IsNullOrWhiteSpace(aBU) && !string.IsNullOrWhiteSpace(aLocation))
            {
                if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
                {
                    ExcelPackage package = new ExcelPackage(upload.InputStream);
                    ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
                    int rowcount = 1;
                    foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 3, 1])
                    {
                        switch (rowcount)
                        {
                            case 1:
                                qualityProject = firstRowCell.Text;
                                break;
                            case 2:
                                LTFPSNo = firstRowCell.Text;
                                break;
                            case 3:
                                LTFPSDes = firstRowCell.Text;
                                break;
                            default:
                                break;
                        }
                        rowcount++;
                    }
                    if (aQualityProject != qualityProject)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please enter valid QualityProject in sheet";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (string.IsNullOrWhiteSpace(LTFPSNo))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "LTFPS No is required";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    var checkExistingLTPFPSNo = db.LTF001.FirstOrDefault(x => x.BU == aBU && x.Location == aLocation && x.QualityProject == aQualityProject && x.LTFPSNo.ToUpper() == LTFPSNo.ToUpper());
                    if (checkExistingLTPFPSNo != null)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "LTFPS No is already exists.";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    bool isError;
                    DataTable dt = ToDataTable(package, qualityProject, aProject, aBU, aLocation, out isError);
                    if (!isError)
                    {
                        LTF001 objLTF001 = new LTF001();
                        objLTF001.QualityProject = aQualityProject;
                        objLTF001.LTFPSNo = LTFPSNo;
                        objLTF001.LTFPSDescription = LTFPSDes;
                        objLTF001.IsReworkLTFPS = false;
                        objLTF001.Project = aProject;
                        objLTF001.BU = aBU;
                        objLTF001.Location = aLocation;
                        objLTF001.Status = clsImplementationEnum.LTFPSHeaderStatus.Draft.GetStringValue();
                        objLTF001.RevNo = 0;
                        objLTF001.CreatedBy = objClsLoginInfo.UserName;
                        objLTF001.CreatedOn = DateTime.Now;
                        db.LTF001.Add(objLTF001);
                        db.SaveChanges();
                        List<LTF002> lstLTF002 = new List<LTF002>();
                        foreach (DataRow item in dt.Rows)
                        {
                            LTF002 objLTF002 = new LTF002();
                            objLTF002.HeaderId = objLTF001.HeaderId;
                            objLTF002.QualityProject = objLTF001.QualityProject;
                            objLTF002.LTFPSNo = objLTF001.LTFPSNo;
                            objLTF002.LTFPSRevNo = Convert.ToInt32(objLTF001.RevNo);
                            objLTF002.OperationNo = Convert.ToInt32(item.Field<string>("OPN. NO."));
                            objLTF002.RefDocument = item.Field<string>("REFERENCE DOCUMENT");
                            objLTF002.SeamNo = Manager.ReplaceIllegalCharForSeamNo(item.Field<string>("SEAMS"));
                            objLTF002.PartNo = item.Field<string>("ITEMS");
                            objLTF002.TestFor = !string.IsNullOrWhiteSpace(item.Field<string>("SEAMS")) ? "Seam" : (!string.IsNullOrWhiteSpace(item.Field<string>("ITEMS")) ? "Part" : null);
                            objLTF002.OperationDescription = item.Field<string>("OPERATION");
                            objLTF002.TestType = item.Field<string>("TEST TYPE");
                            objLTF002.Agency = item.Field<string>("AGENCY");
                            objLTF002.TPI = item.Field<string>("TPI");
                            objLTF002.DocumentRequired = item.Field<string>("REPORT REQ.").Trim().ToLower() == "y" || item.Field<string>("REPORT REQ.").Trim().ToLower() == "yes" ? true : false;
                            objLTF002.CreatedBy = objClsLoginInfo.UserName;
                            objLTF002.CreatedOn = DateTime.Now;
                            objLTF002.Project = objLTF001.Project;
                            objLTF002.BU = objLTF001.BU;
                            objLTF002.Location = objLTF001.Location;
                            objLTF002.LineStatus = "Added";
                            lstLTF002.Add(objLTF002);
                        }
                        db.LTF002.AddRange(lstLTF002);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "LTFPS imported successfully";
                    }
                    else
                    {
                        objResponseMsg = ExportToExcel(dt, qualityProject, LTFPSNo, LTFPSDes);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excel file is not valid";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please enter valid quality project";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public clsHelper.ResponceMsgWithFileName ExportToExcel(DataTable dt, string qualityProject, string LTFPSNo, string LTFPSDescription)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/LTFPS/LTFPS - ERROR TEMPLATE.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        excelWorksheet.Cells[1, 1].Value = qualityProject;
                        excelWorksheet.Cells[2, 1].Value = LTFPSNo;
                        excelWorksheet.Cells[3, 1].Value = LTFPSDescription;
                        int i = 5;
                        foreach (DataRow item in dt.Rows)
                        {
                            excelWorksheet.Cells[i, 1].Value = item.Field<string>(0);
                            excelWorksheet.Cells[i, 2].Value = item.Field<string>(1);
                            excelWorksheet.Cells[i, 3].Value = item.Field<string>(2);
                            excelWorksheet.Cells[i, 4].Value = item.Field<string>(3);
                            excelWorksheet.Cells[i, 5].Value = item.Field<string>(4);
                            excelWorksheet.Cells[i, 6].Value = item.Field<string>(5);
                            excelWorksheet.Cells[i, 7].Value = item.Field<string>(6);
                            excelWorksheet.Cells[i, 8].Value = item.Field<string>(7);
                            excelWorksheet.Cells[i, 9].Value = item.Field<string>(8);
                            excelWorksheet.Cells[i, 10].Value = item.Field<string>(9);
                            excelWorksheet.Cells[i, 11].Value = item.Field<string>(10);
                            excelWorksheet.Cells[i, 12].Value = item.Field<string>(11);
                            excelWorksheet.Cells[i, 13].Value = item.Field<string>(12);
                            excelWorksheet.Cells[i, 14].Value = item.Field<string>(13);
                            excelWorksheet.Cells[i, 15].Value = item.Field<string>(14);
                            excelWorksheet.Cells[i, 16].Value = item.Field<string>(15);
                            excelWorksheet.Cells[i, 17].Value = item.Field<string>(16);
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public DataTable ToDataTable(ExcelPackage package, string qualityProject, string project, string BU, string location, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtFromExcel = new DataTable();
            isError = false;
            foreach (var firstRowCell in excelWorksheet.Cells[4, 1, 4, excelWorksheet.Dimension.End.Column])
            {
                dtFromExcel.Columns.Add(firstRowCell.Text);
            }
            for (var rowNumber = 5; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
            {
                var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                var newRow = dtFromExcel.NewRow();
                foreach (var cell in row)
                {
                    newRow[cell.Start.Column - 1] = cell.Text;
                }
                dtFromExcel.Rows.Add(newRow);
            }
            dtFromExcel.Columns.Add("OperationNoErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("RefDocumentErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("SeamErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("ItemsErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("OperationErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("TestTypeErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("AggencyErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("TPTErrorMsgMsg", typeof(string));

            List<string> lstFindNo = Manager.GetHBOMData(project).Select(x => x.FindNo).ToList();
            //List<string> lstFindNo = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == project).Select(x => x.FindNo).ToList();

            List<string> lstTestTypeValues = (from glb2 in db.GLB002
                                              join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                              where glb1.Category == "LTFPS Test Type" && glb2.BU == BU && glb2.Location == location
                                              select glb2.Code).ToList();
            List<string> lstAgencyValues = (from glb2 in db.GLB002
                                            join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                            where glb1.Category == "LTFPS Agency" && glb2.BU == BU && glb2.Location == location
                                            select glb2.Code).ToList();
            List<string> lstTPIValues = (from glb2 in db.GLB002
                                         join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                         where glb1.Category == "LTFPS TPI" && glb2.BU == BU && glb2.Location == location
                                         select glb2.Code).ToList();

            //db.GLB002.Where(x => x.BU == BU && x.Location == location && lstFindNo.Contains(x.Category)).ToList();

            foreach (DataRow item in dtFromExcel.Rows)
            {


                string errorMessage = string.Empty;
                int opNo;
                if (!int.TryParse(item.Field<string>("OPN. NO."), out opNo))
                {
                    item["OperationNoErrorMsg"] = "Operation No is Not Valid, Please Enter Only Numeric Value";
                    isError = true;
                }

                if (string.IsNullOrWhiteSpace(item.Field<string>("REFERENCE DOCUMENT")))
                {
                    item["RefDocumentErrorMsg"] = "Reference Document Required";
                    isError = true;
                }
                else if (Convert.ToString(item.Field<string>("REFERENCE DOCUMENT")).Length > 100)
                {
                    item["RefDocumentErrorMsg"] = "Length exceeded. Ref. Document max length is 100";
                    isError = true;
                }

                //if (string.IsNullOrWhiteSpace(item.Field<string>("SEAMS")) && string.IsNullOrWhiteSpace(item.Field<string>("ITEMS")))
                //{
                //    item["SeamErrorMsg"] = "Seam Or Part Required";
                //    item["ItemsErrorMsg"] = "Seam Or Part Required";
                //    isError = true;
                //}
                //if (/*string.IsNullOrWhiteSpace(item.Field<string>("SEAMS")) &&*/ !string.IsNullOrWhiteSpace(item.Field<string>("ITEMS")))
                //{
                //    item["ItemsErrorMsg"] = "Valid Part No Required";
                //    isError = true;
                //}
                if (!string.IsNullOrWhiteSpace(item.Field<string>("SEAMS")))
                {
                    string seamNo = Manager.ReplaceIllegalCharForSeamNo(item.Field<string>("SEAMS"));
                    QMS012 objQMS012 = db.QMS012.Where(x => x.SeamNo == seamNo && x.QualityProject == qualityProject).FirstOrDefault();
                    if (objQMS012 == null)
                    {
                        item["SeamErrorMsg"] = seamNo + " Seam No not available for Quality Project " + qualityProject;
                        isError = true;
                    }
                    else if (objQMS012.Status != clsImplementationEnum.SeamListStatus.Approved.GetStringValue())
                    {
                        item["SeamErrorMsg"] = seamNo + " Seam No is not approved.";
                        isError = true;
                    }
                    else if (seamNo.Length > 30)
                    {
                        item["SeamErrorMsg"] = "Length exceeded. Seam No max length is 30";
                        isError = true;
                    }
                }
                else if (!string.IsNullOrWhiteSpace(item.Field<string>("ITEMS")))
                {
                    if (!lstFindNo.Contains(item.Field<string>("ITEMS")))
                    {
                        item["ItemsErrorMsg"] = "Part no is not available for this project";
                        isError = true;
                    }
                    else if (Convert.ToString(item.Field<string>("ITEMS")).Length > 30)
                    {
                        item["ItemsErrorMsg"] = "Length exceeded. Part no max length is 30";
                        isError = true;
                    }
                }
                //if (string.IsNullOrWhiteSpace(item.Field<string>("OPERATION")))
                //{
                //    item["OperationErrorMsg"] = "OPERATION Description is required";
                //    isError = true;
                //}
                if (!string.IsNullOrWhiteSpace(item.Field<string>("OPERATION")))
                {
                    if (Convert.ToString(item.Field<string>("OPERATION")).Length > 1000)
                    {
                        item["OperationErrorMsg"] = "Length exceeded. Operation max length is 1000";
                        isError = true;
                    }
                }

                if (string.IsNullOrWhiteSpace(item.Field<string>("TEST TYPE")))
                {
                    item["TestTypeErrorMsg"] = "TEST TYPE is required";
                    isError = true;
                }
                else if (!lstTestTypeValues.Contains(item.Field<string>("TEST TYPE")))
                {
                    item["TestTypeErrorMsg"] = "TEST TYPE is not valid";
                    isError = true;
                }

                if (string.IsNullOrWhiteSpace(item.Field<string>("AGENCY")))
                {
                    item["AggencyErrorMsg"] = "Agency is Required";
                    isError = true;
                }
                else if (!lstAgencyValues.Contains(item.Field<string>("AGENCY")))
                {
                    item["AggencyErrorMsg"] = "AGENCY is not valid";
                    isError = true;
                }

                if (string.IsNullOrWhiteSpace(item.Field<string>("TPI")))
                {
                    item["TPTErrorMsgMsg"] = "TPI is Required";
                    isError = true;
                }
                else if (!lstTPIValues.Contains(item.Field<string>("TPI")))
                {
                    item["TPTErrorMsgMsg"] = "TPI is not valid";
                    isError = true;
                }
            }
            return dtFromExcel;
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", string Project = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.AssemblyLinkage.GetStringValue())
                {
                    var lst = db.SP_LTFPS_GET_LINKAGEASSEMBLY_DATA(1, int.MaxValue, strSortOrder, whereCondition, Project).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      li.QualityProject,
                                      li.Project,
                                      li.BU,
                                      li.Location,
                                      li.LTFPSNo,
                                      li.LTFPSRevNo,
                                      li.AssemblyFindNo,
                                      li.AssemblyPartNo,
                                      li.AssemblyDescription
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

    }

}