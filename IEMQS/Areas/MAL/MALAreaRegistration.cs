﻿using System.Web.Mvc;

namespace IEMQS.Areas.MAL
{
    public class MALAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "MAL";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "MAL_default",
                "MAL/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}