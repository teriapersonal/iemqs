﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;

namespace IEMQS.Areas.MAL.Controllers
{
    public class PneumaticTestController : clsBase
    {
        // GET: MAL/FinalActivities
        string ControllerURL = "/MAL/PneumaticTest/";
        string Title = "PNEUMATIC TEST REPORT";

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            MAL024 objMAL024 = new MAL024();
            ViewBag.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
            if (id > 0)
            {
                objMAL024 = db.MAL024.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                string finalActivities = clsImplementationEnum.MALProtocolType.PneumaticTest.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL024.HeaderId && a.InspectionActivity == finalActivities).FirstOrDefault();
                ViewBag.Status = obj?.Status;
                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            {
                objMAL024.ProtocolNo = string.Empty;
                objMAL024.CreatedBy = objClsLoginInfo.UserName;
                objMAL024.CreatedOn = DateTime.Now;

                #region Remarks

                objMAL024.QCRemarks = "NO PRESSURE DROP AND NO LEAKAGE OBSERVED.";

                #endregion

                db.MAL024.Add(objMAL024);
                db.SaveChanges();
                ViewBag.isEditable = "true";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<MAL025> lstMAL025 = db.MAL025.Where(x => x.HeaderId == objMAL024.HeaderId).ToList();

            ViewBag.lstMAL025 = lstMAL025;

            #endregion

            return View(objMAL024);
        }
        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(MAL024 MAL024, int s = 0)
        {
            MAL024 objMAL024 = new MAL024();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                int? refHeaderId = MAL024.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    objMAL024 = db.MAL024.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                }

                #region Save Data
                objMAL024.Project = MAL024.Project;
                //objMAL024.BU = MAL024.BU;
                objMAL024.Location = objClsLoginInfo.Location;
                objMAL024.QualityProject = MAL024.QualityProject;
                objMAL024.EquipmentNo = MAL024.EquipmentNo;
                objMAL024.DrawingNo = MAL024.DrawingNo;
                objMAL024.DrawingRevisionNo = MAL024.DrawingRevisionNo;
                objMAL024.DCRNo = MAL024.DCRNo;
                objMAL024.InspectionDate = MAL024.InspectionDate;
                objMAL024.Project = MAL024.Project;
                objMAL024.ProtocolNo = MAL024.ProtocolNo;
                objMAL024.ProcedureNo = MAL024.ProcedureNo;
                objMAL024.TestDescription = MAL024.TestDescription;
                objMAL024.DateOfTest = MAL024.DateOfTest;
                objMAL024.TestPosition = MAL024.TestPosition;
                objMAL024.ReqTestPressure = MAL024.ReqTestPressure;
                objMAL024.ReqHoldingTime = MAL024.ReqHoldingTime;
                objMAL024.ReqTestTemperature = MAL024.ReqTestTemperature;
                objMAL024.ActTestPressure = MAL024.ActTestPressure;
                objMAL024.ActHoldingTime = MAL024.ActHoldingTime;
                objMAL024.ActTestTemperature = MAL024.ActTestTemperature;
                objMAL024.GraphAttached = MAL024.GraphAttached;
                objMAL024.QCRemarks = MAL024.QCRemarks;
                objMAL024.Result = MAL024.Result;
                //objMAL024.EditedBy = MAL024.EditedBy;
                //objMAL024.EditedOn = MAL024.EditedOn;
                //objMAL024.EditedBy = objClsLoginInfo.UserName;
                //objMAL024.EditedOn = DateTime.Now;

                if (refHeaderId == 0)
                {
                    objMAL024.CreatedBy = objClsLoginInfo.UserName;
                    objMAL024.CreatedOn = DateTime.Now;
                    db.MAL024.Add(objMAL024);
                }
                else
                {
                    objMAL024.EditedBy = objClsLoginInfo.UserName;
                    objMAL024.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                string finalActivities = clsImplementationEnum.MALProtocolType.PneumaticTest.GetStringValue();

                var obj = db.MAL000.Where(a => a.HeaderId == objMAL024.HeaderId && a.InspectionActivity == finalActivities).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o=>o.InspectionActivity == finalActivities).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL024.Project;
                    objMAL000.InspectionActivity = finalActivities;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL024.HeaderId;
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL024.HeaderId;
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);


        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    MAL024 objMAL024 = db.MAL024.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objMAL024 != null && string.IsNullOrWhiteSpace(objMAL024.ProtocolNo))
                    {
                        db.MAL024.Remove(objMAL024);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objMAL024.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<MAL025> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;

                List<MAL025> lstAddMAL025 = new List<MAL025>();
                List<MAL025> lstDeleteMAL025 = new List<MAL025>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL025 obj = db.MAL025.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL025();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        obj.GaugeSerialNumber = item.GaugeSerialNumber;
                        obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;
                        if (isAdded)
                        {
                            lstAddMAL025.Add(obj);
                        }
                    }
                    if (lstAddMAL025.Count > 0)
                    {
                        db.MAL025.AddRange(lstAddMAL025);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL025 = db.MAL025.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL025.Count > 0)
                    {
                        db.MAL025.RemoveRange(lstDeleteMAL025);
                    }
                    db.SaveChanges();

                }
                else
                {
                    lstDeleteMAL025 = db.MAL025.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL025.Count > 0)
                    {
                        db.MAL025.RemoveRange(lstDeleteMAL025);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);


        }
        #endregion





        [HttpPost]
        public string ProjectDetails(string ProjectNo)
        {
            if (ProjectNo != "")
            {
                NDEModels objNDEModels = new NDEModels();
                var objQproject = db.QMS010.Where(x => x.QualityProject == ProjectNo).FirstOrDefault();
                string strMCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;



                if (ProjectNo.Contains("/"))
                {
                    string[] arr = ProjectNo.Split('/');
                    ProjectNo = arr[0];
                }



                var Data = db.COM001.Where(w => w.t_cprj == ProjectNo).FirstOrDefault();
                string sValue = Data.t_cprj + "-" + Data.t_dsca;

                return (sValue + "|" + strMCode);
            }
            else
                return "";
        }

        [HttpPost]
        public ActionResult getDrgNo(string ProjectNo)
        {
            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(ProjectNo).ToList();
            lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            return Json(lstdrawing.Select(x => new
            {
                Value = x,
                Text = x
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string GetInspectionAgency(string qproject)
        {
            string Agency = string.Empty;
            string inspectionagency = Manager.GetInspectionAgency(qproject);
            if (!string.IsNullOrWhiteSpace(inspectionagency))
            {
                string[] arr = inspectionagency.Split(',').ToArray();
                Agency = string.Join("#", arr.Take(3).ToArray());
            }
            return Agency;
        }

        #endregion
    }
}