﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using Microsoft.SharePoint.Client.EventReceivers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.MAL.Controllers
{
    public class RubOffController : clsBase
    {
        // GET: MAL/RubOff

        string ControllerURL = "/MAL/RubOff/";
        string Title = "REPORT FOR RUB OFF STAMPING";

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            MAL015 objMAL015 = new MAL015();
            ViewBag.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
            if (id > 0)
            {
                objMAL015 = db.MAL015.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                ViewBag.lstMAL015_3 = db.MAL015_3.Where(x => x.HeaderId == objMAL015.HeaderId).ToList();

                string rubOff = clsImplementationEnum.MALProtocolType.RubOff.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL015.HeaderId && a.InspectionActivity == rubOff).FirstOrDefault();
                ViewBag.Status = obj?.Status;
                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            {
                List<MAL015_3> lstMAL015_3 = new List<MAL015_3>();

                MAL015_3 _obj = new MAL015_3();
                _obj.LineId = 0;
                _obj.RubOffDetails1 = "MANUFACTURED BY";
                _obj.RubOffDetails2 = "LARSEN AND TOUBRO LIMITED - HE  HZMC";
                lstMAL015_3.Add(_obj);

                _obj = new MAL015_3();
                _obj.LineId = 0;
                _obj.RubOffDetails1 = "PROJECT NO";
                lstMAL015_3.Add(_obj);

                _obj = new MAL015_3();
                _obj.LineId = 0;
                _obj.RubOffDetails1 = "EQUIPMENT NAME";
                lstMAL015_3.Add(_obj);

                _obj = new MAL015_3();
                _obj.LineId = 0;
                _obj.RubOffDetails1 = "EQUIPMENT NO";
                lstMAL015_3.Add(_obj);

                _obj = new MAL015_3();
                _obj.LineId = 0;
                _obj.RubOffDetails1 = "HYDRO TEST PRESSURE";
                lstMAL015_3.Add(_obj);

                _obj = new MAL015_3();
                _obj.LineId = 0;
                _obj.RubOffDetails1 = "HYDRO TEST DATE";
                lstMAL015_3.Add(_obj);

                _obj = new MAL015_3();
                _obj.LineId = 0;
                _obj.RubOffDetails1 = "RADIOGRAPHY";
                lstMAL015_3.Add(_obj);

                _obj = new MAL015_3();
                _obj.LineId = 0;
                _obj.RubOffDetails1 = "PWHT";
                lstMAL015_3.Add(_obj);

                _obj = new MAL015_3();
                _obj.LineId = 0;
                _obj.RubOffDetails1 = "INSPECTION BY";
                lstMAL015_3.Add(_obj);

                _obj = new MAL015_3();
                _obj.LineId = 0;
                _obj.RubOffDetails1 = "YEAR OF MANUFACTURE";
                lstMAL015_3.Add(_obj);


                ViewBag.lstMAL015_3 = lstMAL015_3;
                ViewBag.isEditable = "true";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.PrintedBy = objClsLoginInfo.UserName;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            #region bind lines
            List<MAL015_2> lstMAL015_2 = db.MAL015_2.Where(x => x.HeaderId == objMAL015.HeaderId).ToList();
            List<MAL015_4> lstMAL015_4 = db.MAL015_4.Where(x => x.HeaderId == objMAL015.HeaderId).ToList();
           
            ViewBag.lstMAL015_2 = lstMAL015_2;
            ViewBag.lstMAL015_4 = lstMAL015_4;
            #endregion
            
            if(objMAL015.ProjectNo!=null)
            {
                List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objMAL015.ProjectNo).ToList();
                ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            else
            {
                ViewBag.DrawingNo = "";
            }
            
            return View(objMAL015);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(MAL015 MAL015, int s = 0)
        {
            MAL015 objMAL015 = new MAL015();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = MAL015.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    objMAL015 = db.MAL015.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                }

                #region Save Data
                objMAL015.ProjectNo = MAL015.ProjectNo;
                objMAL015.Date = MAL015.Date;
                objMAL015.EquipmentNameandNo = MAL015.EquipmentNameandNo;
                objMAL015.InspectionAgency = MAL015.InspectionAgency;
                objMAL015.ManufacturingCode = MAL015.ManufacturingCode;
                objMAL015.Client = MAL015.Client;
                objMAL015.PoNo = MAL015.PoNo;
                objMAL015.PhotoHeaderDetails = MAL015.PhotoHeaderDetails;

                if (refHeaderId == 0)
                {
                    objMAL015.CreatedBy = objClsLoginInfo.UserName;
                    objMAL015.CreatedOn = DateTime.Now;
                    db.MAL015.Add(objMAL015);
                }
                else
                {
                    objMAL015.EditedBy = objClsLoginInfo.UserName;
                    objMAL015.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                string rubOff = clsImplementationEnum.MALProtocolType.RubOff.GetStringValue();

                var obj = db.MAL000.Where(a => a.HeaderId == objMAL015.HeaderId && a.InspectionActivity == rubOff).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o => o.Project == objMAL015.ProjectNo && o.InspectionActivity == rubOff).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL015.ProjectNo;
                    objMAL000.InspectionActivity = rubOff;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL015.HeaderId;
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL015.HeaderId;
                ViewBag.HeaderId= objMAL015.HeaderId; 
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<MAL012_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<MAL015_2> lstAddMAL015_2 = new List<MAL015_2>();
                List<MAL015_2> lstDeleteMAL015_2 = new List<MAL015_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL015_2 obj = db.MAL015_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL015_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.DrawingNo))
                        {
                            obj.DrawingNo = item.DrawingNo;
                            obj.RevisionNo = item.RevisionNo;
                        }
                        if (isAdded)
                        {
                            lstAddMAL015_2.Add(obj);
                        }
                    }
                    if (lstAddMAL015_2.Count > 0)
                    {
                        db.MAL015_2.AddRange(lstAddMAL015_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL015_2 = db.MAL015_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL015_2.Count > 0)
                    {
                        db.MAL015_2.RemoveRange(lstDeleteMAL015_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL015_2 = db.MAL015_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL015_2.Count > 0)
                    {
                        db.MAL015_2.RemoveRange(lstDeleteMAL015_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<MAL015_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<MAL015_3> lstAddMAL015_3 = new List<MAL015_3>();
                List<MAL015_3> lstDeleteMAL015_3 = new List<MAL015_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL015_3 obj = db.MAL015_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL015_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                                               
                            obj.RubOffDetails1 = item.RubOffDetails1;
                           obj.RubOffDetails2 = item.RubOffDetails2;

                        if (isAdded)
                        {
                            lstAddMAL015_3.Add(obj);
                        }
                    }
                    if (lstAddMAL015_3.Count > 0)
                    {
                        db.MAL015_3.AddRange(lstAddMAL015_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL015_3 = db.MAL015_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL015_3.Count > 0)
                    {
                        db.MAL015_3.RemoveRange(lstDeleteMAL015_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL015_3 = db.MAL015_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL015_3.Count > 0)
                    {
                        db.MAL015_3.RemoveRange(lstDeleteMAL015_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<MAL015_4> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<MAL015_4> lstAddMAL015_4 = new List<MAL015_4>();
                List<MAL015_4> lstDeleteMAL015_4 = new List<MAL015_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL015_4 obj = db.MAL015_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL015_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.ActualValue))
                        {
                            obj.ActualValue = item.ActualValue;
                        }
                        if (isAdded)
                        {
                            lstAddMAL015_4.Add(obj);
                        }
                    }
                    if (lstAddMAL015_4.Count > 0)
                    {
                        db.MAL015_4.AddRange(lstAddMAL015_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL015_4 = db.MAL015_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL015_4.Count > 0)
                    {
                        db.MAL015_4.RemoveRange(lstDeleteMAL015_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL015_4 = db.MAL015_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL015_4.Count > 0)
                    {
                        db.MAL015_4.RemoveRange(lstDeleteMAL015_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        [HttpPost]
        public string ProjectDetails(string ProjectNo)
        {
            if (ProjectNo != "")
            {
                NDEModels objNDEModels = new NDEModels();
                var objQproject = db.QMS010.Where(x => x.QualityProject == ProjectNo).FirstOrDefault();
                string strMCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;



                if (ProjectNo.Contains("/"))
                {
                    string[] arr = ProjectNo.Split('/');
                    ProjectNo = arr[0];
                }



                var Data = db.COM001.Where(w => w.t_cprj == ProjectNo).FirstOrDefault();
                string sValue = Data.t_cprj + "-" + Data.t_dsca;

                return (sValue + "|" + strMCode);
            }
            else
                return "";
        }

        [HttpPost]
        public ActionResult getDrgNo(string ProjectNo)
        {
            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(ProjectNo).ToList();
            lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            return Json(lstdrawing.Select(x => new
            {
                Value = x,
                Text = x
            }).ToList(), JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult DeletePhoto(int PhotoNo, int Headerid, string strTableName)
        {
            try
            {
                var Files = (new clsFileUpload()).GetDocuments(strTableName + "/" + Headerid + "/" + PhotoNo);
                foreach (var item in Files)
                {
                    (new clsFileUpload()).DeleteFile(strTableName + "/" + Headerid + "/" + PhotoNo, item.Name);
                }

                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }

        [HttpPost]
        public string GetInspectionAgency(string qproject)
        {
            string Agency = string.Empty;
            string inspectionagency = Manager.GetInspectionAgency(qproject);
            if (!string.IsNullOrWhiteSpace(inspectionagency))
            {
                string[] arr = inspectionagency.Split(',').ToArray();
                Agency = string.Join("#", arr.Take(3).ToArray());
            }
            return Agency;
        }

        [HttpPost]
        public string SaveImageInSharePoint( string ImagePath, string imageBlob)
        {
            string ImageName = "NewImg.png";
            string Massage = "Saving image Failed";
            var Files = (new clsFileUpload()).GetDocuments(ImagePath);
            if (Files.Length > 0)
            {
                ImageName = Files.FirstOrDefault().Name;
            }
            //imageBlob = imageBlob.Replace("data:image/png;base64,", string.Empty);
            //imageBlob = imageBlob.Replace("data:image/jpg;base64,", string.Empty);
            //imageBlob = imageBlob.Replace("data:image/jpeg;base64,", string.Empty);
            int base64 = imageBlob.IndexOf("base64,");
            imageBlob = imageBlob.Remove(0, base64+7);
            //byte[] bytearray = Encoding.UTF8.GetBytes(imageBlob);
            var bytes = Convert.FromBase64String(imageBlob);
            MemoryStream stream = new MemoryStream(bytes);
            //var bytes = Convert.FromBase64String(imageBlob);
            //var contents = new StreamContent(new MemoryStream(bytes));
            clsFileUpload upload = new clsFileUpload();
            
           bool t= upload.FileUpload(ImageName, ImagePath, stream, "","",false);
            if (t)
            {
                Massage = "Image Saved";
            }
            return Massage;
        }
    }
}