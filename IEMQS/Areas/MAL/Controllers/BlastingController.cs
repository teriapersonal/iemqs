﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.MAL.Controllers
{
    public class BlastingController : clsBase
    {
        // GET: MAL/Blasting
        string ControllerURL = "/MAL/Blasting/";
        string Title = "SURFACE PREPARATION REPORT";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            MAL005 objMAL005 = new MAL005();
            ViewBag.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
            if (id > 0)
            {
                objMAL005 = db.MAL005.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                string blasting = clsImplementationEnum.MALProtocolType.Blasting.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL005.HeaderId && a.InspectionActivity == blasting).FirstOrDefault();

                ViewBag.Status = obj?.Status;
                ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objMAL005.Project).Select(i => i.t_dsca).FirstOrDefault();
                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            { ViewBag.isEditable = "true"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<MAL006> lstMAL006 = db.MAL006.Where(x => x.HeaderId == objMAL005.HeaderId).ToList();
            List<MAL007> lstMAL007 = db.MAL007.Where(x => x.HeaderId == objMAL005.HeaderId).ToList();
            List<MAL008> lstMAL008 = db.MAL008.Where(x => x.HeaderId == objMAL005.HeaderId).ToList();

            ViewBag.lstMAL006 = lstMAL006;
            ViewBag.lstMAL007 = lstMAL007;
            ViewBag.lstMAL008 = lstMAL008;
            #endregion

            return View(objMAL005);
        }


        [HttpPost]
        public ActionResult SaveProtocolHeaderData(MAL005 MAL005, int s = 0)
        {
            MAL005 objMAL005 = new MAL005();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = MAL005.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    objMAL005 = db.MAL005.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                }

                #region Save Data
                objMAL005.Project = MAL005.Project;
                objMAL005.DocumentNo = MAL005.DocumentNo;
                objMAL005.ProcedureNo = MAL005.ProcedureNo;
                objMAL005.CoatingContractorName = MAL005.CoatingContractorName;
                objMAL005.Date = MAL005.Date;
                objMAL005.ReportNo = MAL005.ReportNo;
                objMAL005.ItemPreparedOrCoated = MAL005.ItemPreparedOrCoated;
                objMAL005.MaxRelativeHumidity = MAL005.MaxRelativeHumidity;
                objMAL005.MinDiffSteelDewPointTemp = MAL005.MinDiffSteelDewPointTemp;
                objMAL005.MaxDryBulbTemp = MAL005.MaxDryBulbTemp;
                objMAL005.EnvrCondRef = MAL005.EnvrCondRef;
                objMAL005.StartTimeActivity = MAL005.StartTimeActivity;
                objMAL005.EndTimeActivity = MAL005.EndTimeActivity;
                objMAL005.AbrasiveBlasting = MAL005.AbrasiveBlasting;
                objMAL005.AbrasiveMaterial = MAL005.AbrasiveMaterial;
                objMAL005.Manufacturer = MAL005.Manufacturer;
                objMAL005.Remarks = MAL005.Remarks;
                if (refHeaderId == 0)
                {
                    objMAL005.CreatedBy = objClsLoginInfo.UserName;
                    objMAL005.CreatedOn = DateTime.Now;
                    db.MAL005.Add(objMAL005);
                }
                else
                {
                    objMAL005.EditedBy = objClsLoginInfo.UserName;
                    objMAL005.EditedOn = DateTime.Now;
                }

                db.SaveChanges();
                string blasting = clsImplementationEnum.MALProtocolType.Blasting.GetStringValue();

                var obj = db.MAL000.Where(a => a.HeaderId == objMAL005.HeaderId && a.InspectionActivity == blasting).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o => o.Project == objMAL005.Project && o.InspectionActivity == blasting).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL005.Project;
                    objMAL000.InspectionActivity = blasting;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL005.HeaderId;
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                    }
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL005.HeaderId;
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<MAL006> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL006> lstAddMAL006 = new List<MAL006>();
                List<MAL006> lstDeleteMAL006 = new List<MAL006>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            MAL006 obj = db.MAL006.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.CondRefs = item.CondRefs;
                                obj.Date = item.Date;
                                obj.Time = item.Time;
                                obj.DryBulbTemp = item.DryBulbTemp;
                                obj.RelativeHumidity = item.RelativeHumidity;
                                obj.DewPointTemp = item.DewPointTemp;
                                obj.SteelTemp = item.SteelTemp;
                                obj.DiffSteelDewPointTemp = item.DiffSteelDewPointTemp;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            MAL006 obj = new MAL006();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.CondRefs))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.CondRefs = item.CondRefs;
                                obj.Date = item.Date;
                                obj.Time = item.Time;
                                obj.DryBulbTemp = item.DryBulbTemp;
                                obj.RelativeHumidity = item.RelativeHumidity;
                                obj.DewPointTemp = item.DewPointTemp;
                                obj.SteelTemp = item.SteelTemp;
                                obj.DiffSteelDewPointTemp = item.DiffSteelDewPointTemp;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddMAL006.Add(obj);
                            }
                        }
                    }
                    if (lstAddMAL006.Count > 0)
                    {
                        db.MAL006.AddRange(lstAddMAL006);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeleteMAL006 = db.MAL006.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeleteMAL006.Count > 0)
                    {
                        db.MAL006.RemoveRange(lstDeleteMAL006);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL006 = db.MAL006.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL006.Count > 0)
                    {
                        db.MAL006.RemoveRange(lstDeleteMAL006);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<MAL007> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL007> lstAddMAL007 = new List<MAL007>();
                List<MAL007> lstDeleteMAL007 = new List<MAL007>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL007 obj = db.MAL007.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new MAL007();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.HeaderId = item.HeaderId;
                        obj.TestDone = item.TestDone;
                        obj.Frequency = item.Frequency;
                        obj.Standard = item.Standard;
                        obj.Requirement = item.Requirement;
                        obj.Observed = item.Observed;
                        obj.Result = item.Result;
                        if (isAdded)
                        {
                            lstAddMAL007.Add(obj);
                        }
                    }
                    if (lstAddMAL007.Count > 0)
                    {
                        db.MAL007.AddRange(lstAddMAL007);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL007 = db.MAL007.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL007.Count > 0)
                    {
                        db.MAL007.RemoveRange(lstDeleteMAL007);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL007 = db.MAL007.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL007.Count > 0)
                    {
                        db.MAL007.RemoveRange(lstDeleteMAL007);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<MAL008> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL008> lstAddMAL008 = new List<MAL008>();
                List<MAL008> lstDeleteMAL008 = new List<MAL008>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL008 obj = db.MAL008.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL008();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Equipment))
                        {
                            obj.Equipment = item.Equipment;
                            obj.MachineMake = item.MachineMake;
                            obj.SerialNo = item.SerialNo;
                        }
                        if (isAdded)
                        {
                            lstAddMAL008.Add(obj);
                        }
                    }
                    if (lstAddMAL008.Count > 0)
                    {
                        db.MAL008.AddRange(lstAddMAL008);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL008 = db.MAL008.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL008.Count > 0)
                    {
                        db.MAL008.RemoveRange(lstDeleteMAL008);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL008 = db.MAL008.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL008.Count > 0)
                    {
                        db.MAL008.RemoveRange(lstDeleteMAL008);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult GetEquipementDetails(string term)
        {
            var lstResult = (from a in db.NDE019
                             where a.TestInstrument.Contains(term)
                             select new { TestInstrument = a.TestInstrument, Make = a.Make + "#" + a.InstrumentSrNo }).ToList();
            return Json(lstResult, JsonRequestBehavior.AllowGet);
        }

        #endregion



        public class clsProjects
        {
            public string Value { get; set; }
            public string Text { get; set; }
            public string BU { get; set; }
            public string projectCode { get; set; }
            public string projectDescription { get; set; }

        }
    }

}