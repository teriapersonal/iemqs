﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.MAL.Controllers
{
    public class CoatingController : clsBase
    {
        // GET: MAL/Blasting
        string ControllerURL = "/MAL/Coating/";
        string Title = "COATING REPORT";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            MAL001 objMAL001 = new MAL001();
            ViewBag.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
            if (id > 0)
            {
                objMAL001 = db.MAL001.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                string coating = clsImplementationEnum.MALProtocolType.Coating.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL001.HeaderId && a.InspectionActivity == coating).FirstOrDefault();

                ViewBag.Status = obj?.Status;
                ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objMAL001.Project).Select(i => i.t_dsca).FirstOrDefault();
                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            { ViewBag.isEditable = "true"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            #region bind lines
            List<MAL002> lstMAL002 = db.MAL002.Where(x => x.HeaderId == objMAL001.HeaderId).ToList();
            List<MAL003> lstMAL003 = db.MAL003.Where(x => x.HeaderId == objMAL001.HeaderId).ToList();
            List<MAL004> lstMAL004 = db.MAL004.Where(x => x.HeaderId == objMAL001.HeaderId).ToList();
            List<MAL004_2> lstMAL004_2 = db.MAL004_2.Where(x => x.HeaderId == objMAL001.HeaderId).ToList();
            List<MAL004_3> lstMAL004_3 = db.MAL004_3.Where(x => x.HeaderId == objMAL001.HeaderId).ToList();

            ViewBag.lstMAL002 = lstMAL002;
            ViewBag.lstMAL003 = lstMAL003;
            ViewBag.lstMAL004 = lstMAL004;
            ViewBag.lstMAL004_2 = lstMAL004_2;
            ViewBag.lstMAL004_3 = lstMAL004_3;
            #endregion

            return View(objMAL001);
        }


        [HttpPost]
        public ActionResult SaveProtocolHeaderData(MAL001 MAL001, int s = 0)
        {
            MAL001 objMAL001 = new MAL001();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = MAL001.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    objMAL001 = db.MAL001.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                }

                #region Save Data
                objMAL001.Project = MAL001.Project;
                objMAL001.DocumentNo = MAL001.DocumentNo;
                objMAL001.ProcedureNo = MAL001.ProcedureNo;
                objMAL001.CoatingContractorName = MAL001.CoatingContractorName;
                objMAL001.Date = MAL001.Date;
                objMAL001.ReportNo = MAL001.ReportNo;
                objMAL001.ItemPreparedOrCoated = MAL001.ItemPreparedOrCoated;
                objMAL001.MaxRelativeHumidity = MAL001.MaxRelativeHumidity;
                objMAL001.MinDiffSteelDewPointTemp = MAL001.MinDiffSteelDewPointTemp;
                objMAL001.MaxDryBulbTemp = MAL001.MaxDryBulbTemp;
                objMAL001.Remarks = MAL001.Remarks;
                if (refHeaderId == 0)
                {
                    objMAL001.CreatedBy = objClsLoginInfo.UserName;
                    objMAL001.CreatedOn = DateTime.Now;
                    db.MAL001.Add(objMAL001);
                }
                else
                {
                    objMAL001.EditedBy = objClsLoginInfo.UserName;
                    objMAL001.EditedOn = DateTime.Now;
                }

                db.SaveChanges();
                string blasting = clsImplementationEnum.MALProtocolType.Coating.GetStringValue();

                var obj = db.MAL000.Where(a => a.HeaderId == objMAL001.HeaderId && a.InspectionActivity == blasting).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o => o.Project == objMAL001.Project && o.InspectionActivity == blasting).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL001.Project;
                    objMAL000.InspectionActivity = blasting;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL001.HeaderId;
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL001.HeaderId;
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<MAL002> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL002> lstAddMAL002 = new List<MAL002>();
                List<MAL002> lstDeleteMAL002 = new List<MAL002>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            MAL002 obj = db.MAL002.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.CondRefs = item.CondRefs;
                                obj.Date = item.Date;
                                obj.Time = item.Time;
                                obj.DryBulbTemp = item.DryBulbTemp;
                                obj.RelativeHumidity = item.RelativeHumidity;
                                obj.DewPointTemp = item.DewPointTemp;
                                obj.SteelTemp = item.SteelTemp;
                                obj.DiffSteelDewPointTemp = item.DiffSteelDewPointTemp;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            MAL002 obj = new MAL002();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.CondRefs))
                            {
                                obj.CondRefs = item.CondRefs;
                                obj.Date = item.Date;
                                obj.Time = item.Time;
                                obj.DryBulbTemp = item.DryBulbTemp;
                                obj.RelativeHumidity = item.RelativeHumidity;
                                obj.DewPointTemp = item.DewPointTemp;
                                obj.SteelTemp = item.SteelTemp;
                                obj.DiffSteelDewPointTemp = item.DiffSteelDewPointTemp;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddMAL002.Add(obj);
                            }
                        }
                    }
                    if (lstAddMAL002.Count > 0)
                    {
                        db.MAL002.AddRange(lstAddMAL002);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeleteMAL002 = db.MAL002.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeleteMAL002.Count > 0)
                    {
                        db.MAL002.RemoveRange(lstDeleteMAL002);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL002 = db.MAL002.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL002.Count > 0)
                    {
                        db.MAL002.RemoveRange(lstDeleteMAL002);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<MAL004_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL004_2> lstAddMAL004_2 = new List<MAL004_2>();
                List<MAL004_2> lstDeleteMAL004_2 = new List<MAL004_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL004_2 obj = db.MAL004_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new MAL004_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.HeaderId = item.HeaderId;
                        obj.TestDone = item.TestDone;
                        obj.Frequency = item.Frequency;
                        obj.Standard = item.Standard;
                        obj.Requirement = item.Requirement;
                        obj.Observed = item.Observed;
                        obj.Result = item.Result;

                        if (isAdded)
                        {
                            lstAddMAL004_2.Add(obj);
                        }
                    }
                    if (lstAddMAL004_2.Count > 0)
                    {
                        db.MAL004_2.AddRange(lstAddMAL004_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL004_2 = db.MAL004_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL004_2.Count > 0)
                    {
                        db.MAL004_2.RemoveRange(lstDeleteMAL004_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL004_2 = db.MAL004_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL004_2.Count > 0)
                    {
                        db.MAL004_2.RemoveRange(lstDeleteMAL004_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<MAL004_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL004_3> lstAddMAL004_3 = new List<MAL004_3>();
                List<MAL004_3> lstDeleteMAL004_3 = new List<MAL004_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL004_3 obj = db.MAL004_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL004_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Equipment))
                        {
                            obj.Equipment = item.Equipment;
                            obj.MachineMake = item.MachineMake;
                            obj.SerialNo = item.SerialNo;
                        }
                        if (isAdded)
                        {
                            lstAddMAL004_3.Add(obj);
                        }
                    }
                    if (lstAddMAL004_3.Count > 0)
                    {
                        db.MAL004_3.AddRange(lstAddMAL004_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL004_3 = db.MAL004_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL004_3.Count > 0)
                    {
                        db.MAL004_3.RemoveRange(lstDeleteMAL004_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL004_3 = db.MAL004_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL004_3.Count > 0)
                    {
                        db.MAL004_3.RemoveRange(lstDeleteMAL004_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<MAL003> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL003> lstAddMAL003 = new List<MAL003>();
                List<MAL003> lstDeleteMAL003 = new List<MAL003>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL003 obj = db.MAL003.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL003();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.CondRefs))
                        {
                            obj.CondRefs = item.CondRefs;
                            obj.CoatNo = item.CoatNo;
                            obj.MethodOfApplication = item.MethodOfApplication;
                            obj.TypeOfCoat = item.TypeOfCoat;
                            obj.MinDFTRequired = item.MinDFTRequired;
                            obj.MinDFTActual = item.MinDFTActual;
                            obj.StartDate = item.StartDate;
                            obj.EndDate = item.EndDate;
                            obj.StartTime = item.StartTime;
                            obj.EndTime = item.EndTime;
                        }
                        if (isAdded)
                        {
                            lstAddMAL003.Add(obj);
                        }
                    }
                    if (lstAddMAL003.Count > 0)
                    {
                        db.MAL003.AddRange(lstAddMAL003);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL003 = db.MAL003.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL003.Count > 0)
                    {
                        db.MAL003.RemoveRange(lstDeleteMAL003);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL003 = db.MAL003.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL003.Count > 0)
                    {
                        db.MAL003.RemoveRange(lstDeleteMAL003);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<MAL004> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<MAL004> lstAddMAL004 = new List<MAL004>();
                List<MAL004> lstDeleteMAL004 = new List<MAL004>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL004 obj = db.MAL004.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL004();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.CoatingSpecification))
                        {
                            obj.CoatingSpecification = item.CoatingSpecification;
                            obj.Make = item.Make;
                            obj.PointName= item.PointName;
                            obj.PaintBatchNo = item.PaintBatchNo;
                            obj.DateOfExpiry = item.DateOfExpiry;
                        }
                        if (isAdded)
                        {
                            lstAddMAL004.Add(obj);
                        }
                    }
                    if (lstAddMAL004.Count > 0)
                    {
                        db.MAL004.AddRange(lstAddMAL004);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL004 = db.MAL004.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL004.Count > 0)
                    {
                        db.MAL004.RemoveRange(lstDeleteMAL004);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL004 = db.MAL004.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL004.Count > 0)
                    {
                        db.MAL004.RemoveRange(lstDeleteMAL004);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        [HttpPost]
        public ActionResult GetEquipementDetails(string term)
        {
            var lstResult = (from a in db.NDE019
                             where a.TestInstrument.Contains(term) || term == ""
                             select new { TestInstrument = a.TestInstrument, Make = a.Make + "#" + a.InstrumentSrNo }).ToList();
            return Json(lstResult, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public class clsProjects
        {
            public string Value { get; set; }
            public string Text { get; set; }
            public string BU { get; set; }
            public string projectCode { get; set; }
            public string projectDescription { get; set; }

        }

    }

}