﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;

namespace IEMQS.Areas.MAL.Controllers
{
    public class FinalActivitiesController : clsBase
    {
        // GET: MAL/FinalActivities
        string ControllerURL = "/MAL/FinalActivities/";
        string Title = "Final Activities Report";

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            MAL012 objMAL012 = new MAL012();
            ViewBag.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
            if (id > 0)
            {
                objMAL012 = db.MAL012.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;

                string finalActivities = clsImplementationEnum.MALProtocolType.FinalActivities.GetStringValue();
                var obj = db.MAL000.Where(a => a.HeaderId == objMAL012.HeaderId && a.InspectionActivity == finalActivities).FirstOrDefault();
                ViewBag.Status = obj?.Status;
                if (m == 1)
                { ViewBag.isEditable = "true"; }
                else
                { ViewBag.isEditable = "false"; }
            }
            else
            {
                ViewBag.isEditable = "true";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.PrintedBy = objClsLoginInfo.UserName;
            ViewBag.PhotoHeaderDetails= objMAL012.PhotoHeaderDetails;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            #region bind lines
            List<MAL012_2> lstMAL012_2 = db.MAL012_2.Where(x => x.HeaderId == objMAL012.HeaderId).ToList();
            List<MAL012_3> lstMAL012_3 = db.MAL012_3.Where(x => x.HeaderId == objMAL012.HeaderId).ToList();


            ViewBag.lstMAL012_2 = lstMAL012_2;
            ViewBag.lstMAL012_3 = lstMAL012_3;
            #endregion

            if(objMAL012.ProjectNo != null)
            {
                List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objMAL012.ProjectNo).ToList();
                ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            else
            {
                ViewBag.DrawingNo = "";
            }
            return View(objMAL012);
        }


        [HttpPost]
        public ActionResult SaveProtocolHeaderData(MAL012 MAL012, int s = 0)
        {
            MAL012 objMAL012 = new MAL012();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = MAL012.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    objMAL012 = db.MAL012.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                }

                #region Save Data
                objMAL012.ProjectNo = MAL012.ProjectNo;
                objMAL012.Date = MAL012.Date;
                objMAL012.EquipmentNameAndNo = MAL012.EquipmentNameAndNo;
                objMAL012.InspectionAgency = MAL012.InspectionAgency;
                objMAL012.ManufacturingCode = MAL012.ManufacturingCode;
                objMAL012.Client = MAL012.Client;
                objMAL012.PoNo = MAL012.PoNo;
                objMAL012.Stage = MAL012.Stage;
               // objMAL012.PhotoHeaderDetails = MAL012.PhotoHeaderDetails;

                if (refHeaderId == 0)
                {
                    objMAL012.CreatedBy = objClsLoginInfo.UserName;
                    objMAL012.CreatedOn = DateTime.Now;
                    db.MAL012.Add(objMAL012);
                }
                else
                {
                    objMAL012.EditedBy = objClsLoginInfo.UserName;
                    objMAL012.EditedOn = DateTime.Now;
                }

                db.SaveChanges();

                string finalActivities = clsImplementationEnum.MALProtocolType.FinalActivities.GetStringValue();

                var obj = db.MAL000.Where(a => a.HeaderId == objMAL012.HeaderId && a.InspectionActivity == finalActivities).FirstOrDefault();
                if (obj == null)
                {
                    int? srno = db.MAL000.Where(o => o.Project == objMAL012.ProjectNo && o.InspectionActivity == finalActivities).OrderByDescending(x => x.SerialNo).Select(a => a.SerialNo).FirstOrDefault();
                    MAL000 objMAL000 = new MAL000();
                    objMAL000.Project = objMAL012.ProjectNo;
                    objMAL000.InspectionActivity = finalActivities;
                    objMAL000.SerialNo = Convert.ToInt32((srno >= 0 ? srno : 0) + 1);
                    objMAL000.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();

                    objMAL000.CreatedBy = objClsLoginInfo.UserName;
                    objMAL000.CreatedOn = DateTime.Now;
                    objMAL000.HeaderId = objMAL012.HeaderId;
                    db.MAL000.Add(objMAL000);
                    db.SaveChanges();
                }
                else
                {
                    if (s == 1)
                    {
                        obj.Status = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                        obj.EditedBy = objClsLoginInfo.UserName;
                        obj.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Protocol submitted successfully";
                objResponseMsg.HeaderId = objMAL012.HeaderId;
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<MAL012_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<MAL012_2> lstAddMAL012_2 = new List<MAL012_2>();
                List<MAL012_2> lstDeleteMAL012_2 = new List<MAL012_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL012_2 obj = db.MAL012_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL012_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.DrawingNo))
                        {
                            obj.DrawingNo = item.DrawingNo;
                            obj.RevisionNo = item.RevisionNo;
                        }
                        if (isAdded)
                        {
                            lstAddMAL012_2.Add(obj);
                        }
                    }
                    if (lstAddMAL012_2.Count > 0)
                    {
                        db.MAL012_2.AddRange(lstAddMAL012_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL012_2 = db.MAL012_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL012_2.Count > 0)
                    {
                        db.MAL012_2.RemoveRange(lstDeleteMAL012_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL012_2 = db.MAL012_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL012_2.Count > 0)
                    {
                        db.MAL012_2.RemoveRange(lstDeleteMAL012_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<MAL012_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<MAL012_3> lstAddMAL012_3 = new List<MAL012_3>();
                List<MAL012_3> lstDeleteMAL012_3 = new List<MAL012_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        MAL012_3 obj = db.MAL012_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new MAL012_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Actual))
                        {
                            obj.Actual = item.Actual;
                        }
                        if (isAdded)
                        {
                            lstAddMAL012_3.Add(obj);
                        }
                    }
                    if (lstAddMAL012_3.Count > 0)
                    {
                        db.MAL012_3.AddRange(lstAddMAL012_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeleteMAL012_3 = db.MAL012_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL012_3.Count > 0)
                    {
                        db.MAL012_3.RemoveRange(lstDeleteMAL012_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeleteMAL012_3 = db.MAL012_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeleteMAL012_3.Count > 0)
                    {
                        db.MAL012_3.RemoveRange(lstDeleteMAL012_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult DeletePhoto(int PhotoNo, int Headerid, string strTableName)
        {
            try
            {
                var Files = (new clsFileUpload()).GetDocuments(strTableName + "/" + Headerid + "/" + PhotoNo);
                foreach (var item in Files)
                {
                    (new clsFileUpload()).DeleteFile(strTableName + "/" + Headerid + "/" + PhotoNo, item.Name);
                }

                return Json(true);
            }
            catch
            {
                return Json(false);
            }
        }
        #endregion


        [HttpPost]
        public string ProjectDetails(string ProjectNo)
        {
            if (ProjectNo != "")
            {
                NDEModels objNDEModels = new NDEModels();
                var objQproject = db.QMS010.Where(x => x.QualityProject == ProjectNo).FirstOrDefault();
                string strMCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;



                if (ProjectNo.Contains("/"))
                {
                    string[] arr = ProjectNo.Split('/');
                    ProjectNo = arr[0];
                }



                var Data = db.COM001.Where(w => w.t_cprj == ProjectNo).FirstOrDefault();
                string sValue = Data.t_cprj + "-" + Data.t_dsca;

                return (sValue + "|" + strMCode);
            }
            else
                return "";
        }

        [HttpPost]
        public ActionResult getDrgNo(string ProjectNo)
        {
            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(ProjectNo).ToList();
            lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            return Json(lstdrawing.Select(x => new
            {
                Value = x,
                Text = x
            }).ToList(), JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetInspectionAgency(string qproject)
        {
            string Agency = string.Empty;
            List<string> tpi = new List<string>();
            string inspectionagency = Manager.GetInspectionAgency(qproject);
            if (!string.IsNullOrWhiteSpace(inspectionagency))
            {
                string[] arr = inspectionagency.Split(',').ToArray();
                Agency = string.Join("#", arr.Take(3).ToArray());

                for (int i = 0; i < arr.Length; i++)
                {
                    string code = arr[i];
                    var Description = (from glb002 in db.GLB002
                                       join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                       where glb001.Category.Equals("Inspection Agency", StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(code) //&& glb002.IsActive == true
                                       select glb002.Description
                                     ).FirstOrDefault();
                    tpi.Add((string.IsNullOrWhiteSpace(Description)) ? arr[i] : Description);
                }
            }
            var obj = new
            {
                Agency = Agency,
                arr = string.Join(",", tpi)
            };
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        #endregion
        public ActionResult SaveFinal(string HelpSectionImages)
        {
            string fileSavePath = string.Empty;
            string virtualDirectoryImg = "UploadedFiles";
            string fileName = string.Empty;
            if (HelpSectionImages == "System.Web.HttpPostedFileWrapper")
            {
                if (HttpContext.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var httpPostedFile = HttpContext.Request.Files["HelpSectionImages"];
                    fileName = httpPostedFile.FileName;
                    string HeaderId = HttpContext.Request.Form["Headerid"];
                    string Qproject = HttpContext.Request.Form["Qproject"];
                    string ImgName = HttpContext.Request.Form["ImgName"];
                    if (httpPostedFile != null)
                    {
                        // OBtient le path du fichier 
                        if (ImgName == "")
                        {
                            Qproject = Qproject.Replace('/', '_');
                            ImgName = Qproject + "-" + DateTime.Now.ToString("ddMMyyyy HHmmss") + ".png";
                        }

                        fileSavePath = Path.Combine(HttpContext.Server.MapPath("~/Areas/MAL/FinalActImg/"), ImgName);

                        // Sauvegarde du fichier dans UploadedFiles sur le serveur
                        httpPostedFile.SaveAs(fileSavePath);
                    }

                    return Json(ImgName);
                }
                else
                {
                    return Json("Failed");
                }
            }
            else
            {
                string ImgName = HttpContext.Request.Form["ImgName"];
                var t = HelpSectionImages.Substring(22);  // remove data:image/png;base64,



                byte[] imageBytes = Convert.FromBase64String(t);

                //Save the Byte Array as Image File.
                string filePath = Server.MapPath(ImgName);
                System.IO.File.WriteAllBytes(filePath, imageBytes);
                return Json(ImgName);
            }
        }

        public static bool IsBase64(string base64String)
        {
            if (base64String.Replace(" ", "").Length % 4 != 0)
            {
                return false;
            }

            try
            {
                Convert.FromBase64String(base64String);
                return true;
            }
            catch (FormatException exception)
            {
                // Handle the exception
            }
            return false;
        }

        [HttpPost]
        public string SaveImageInSharePoint(string ImagePath, string imageBlob)
        {
            string ImageName = "NewImg.png";
            string Massage = "Saving image Failed";
            var Files = (new clsFileUpload()).GetDocuments(ImagePath);
            if (Files.Length > 0)
            {
                ImageName = Files.FirstOrDefault().Name;
            }
            //imageBlob = imageBlob.Replace("data:image/png;base64,", string.Empty);
            //imageBlob = imageBlob.Replace("data:image/jpg;base64,", string.Empty);
            int base64 = imageBlob.IndexOf("base64,");
            imageBlob = imageBlob.Remove(0, base64 + 7);
            //byte[] bytearray = Encoding.UTF8.GetBytes(imageBlob);
            var bytes = Convert.FromBase64String(imageBlob);
            MemoryStream stream = new MemoryStream(bytes);
            //var bytes = Convert.FromBase64String(imageBlob);
            //var contents = new StreamContent(new MemoryStream(bytes));
            clsFileUpload upload = new clsFileUpload();

            bool t = upload.FileUpload(ImageName, ImagePath, stream, "", "", false);
            if (t)
            {
                Massage = "Image Saved";
            }
            return Massage;
        }
    }
}