﻿using IEMQS.Areas.PDIN.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Collections;
using System.Web.Script.Serialization;
using System.Net;
using System.Net.Mail;
using IEMQS.Areas.Utility.Models;
using IEMQSImplementation.PQR;
using IEMQSImplementation.Models;
using Microsoft.Reporting.Map.WebForms.VirtualEarth;
using static IEMQSImplementation.clsHelper;

namespace IEMQS.Areas.PQR.Controllers
{
    public class MaintainPQRController : clsBase
    {
        // GET: PQR/PQR
        PQREntitiesContext PQRDB = new PQREntitiesContext();

        [SessionExpireFilter]
        public ActionResult Index()
        {           
            return View();
        }

        [SessionExpireFilter]
        public ActionResult Maintain()
        {
            var objclsLoginInfo = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
            if (objclsLoginInfo != null)
            {
                if (objclsLoginInfo.listMenuResult != null)
                {
                    int Cnt = objclsLoginInfo.listMenuResult.Where(x => x.Process == "Maintain PQR").ToList().Count();
                    if (Cnt == 0)
                        return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.PQRRole = "Maintain";
            ViewBag.Title = "Maintain PQR Detail";
            return View("Index");
        }
        [SessionExpireFilter]
        public ActionResult Approve()
        {
            var objclsLoginInfo = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
            if (objclsLoginInfo != null)
            {
                if (objclsLoginInfo.listMenuResult != null)
                {
                    int Cnt = objclsLoginInfo.listMenuResult.Where(x => x.Process == "Approve PQR").ToList().Count();
                    if (Cnt == 0)
                        return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }

            ViewBag.PQRRole = "Approve";
            ViewBag.Title = "Approve PQR data";
            return View("Index");
        }
        [SessionExpireFilter]

        public PartialViewResult LoadIndexGridDataPartial(string Status, string PQRRole)
        {
            ViewBag.Status = Status;
            ViewBag.PQRRole = PQRRole;
            var role = (from a in db.ATH001
                        join b in db.ATH004 on a.Role equals b.Id
                        where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                        select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

            //if (role.Where(i => i.RoleDesc.Equals("we3", StringComparison.OrdinalIgnoreCase)).ToList().Any())
            //{
            //    ViewBag.PQRRole = "Maintain";
            //}
            //else if (role.Where(i => i.RoleDesc.Equals("we2", StringComparison.OrdinalIgnoreCase)).ToList().Any() ||
            //    role.Where(i => i.RoleDesc.Equals("we1", StringComparison.OrdinalIgnoreCase)).ToList().Any())
            //{
            //    ViewBag.PQRRole = "Approve";
            //}

            return PartialView("_LoadIndexGridDataPartial");
        }

        public ActionResult GetIndexGridData(JQueryDataTableParamModel param, string Status, string PQRRole)
        {
            try
            {
                ViewBag.PQRRole = PQRRole;

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];

                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1";
                string[] columnName = { "PQRNo", "PQRFile", "JointType", "ParentMetal1", "GrNo1", "ParentMetal2", "GrNo2", "ThickNess", "WeldingProcess", "Consumable", "PWHTTemp", "PWHTTime", "DesignCode", "TPIAgency", "Remarks", "FinalFile", "BackUpFile", };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                if (Status.ToUpper() == "PENDING")
                {
                    if (ViewBag.PQRRole == "Maintain")
                        whereCondition += " and Status = '" + clsImplementationEnum.PQRStatus.Draft.GetStringValue() + "'";
                    else
                        whereCondition += " and Status = '" + clsImplementationEnum.PQRStatus.SendToApproval.GetStringValue() + "'";
                }
                else if (Status.ToUpper() == "ALL")
                {
                    if (ViewBag.PQRRole != "Maintain")
                        whereCondition += " and Status in ('" + clsImplementationEnum.PQRStatus.SendToApproval.GetStringValue() + "', '" + clsImplementationEnum.PQRStatus.Approved.GetStringValue() + "')";
                }

                var lstLines = PQRDB.SP_PQR_GET_PQRDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.HeaderId),
                                        Convert.ToString(c.HeaderId),
                                        Convert.ToString(c.PQRNo),
                                        Convert.ToString(c.PQRFile),
                                        Convert.ToString(c.JointType),
                                        Convert.ToString(c.ParentMetal1),
                                        Convert.ToString(c.GrNo1),
                                        Convert.ToString(c.ParentMetal2),
                                        Convert.ToString(c.GrNo2),
                                        Convert.ToString(c.ThickNess),
                                        Convert.ToString(c.WeldingProcess),
                                        Convert.ToString(c.Consumable),
                                        Convert.ToString(c.PWHTTemp),
                                        Convert.ToString(c.PWHTTime),
                                        Convert.ToString(c.DesignCode),
                                        Convert.ToString(c.TPIAgency),
                                        Convert.ToString(c.Remarks),
                                        Convert.ToString(c.Status),
                                        Convert.ToString(c.CreatedBy),
                                        Convert.ToString(c.CreatedOn.ToString("dd/MM/yyyy")),
                                         "<nobr>" + Helper.GenerateActionIcon(Convert.ToInt32(c.HeaderId), "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + c.HeaderId + ");","", Status.ToUpper() == "ALL" && c.Status != "Draft" && ViewBag.PQRRole == "Maintain" ? true : false,false, Status.ToUpper() == "ALL" && c.Status != "Draft" && ViewBag.PQRRole == "Maintain" ? "pointer-events:none;" : null) +
                                         Helper.GenerateActionIcon(Convert.ToInt32(c.HeaderId), "Update", "Update Record", "fa fa-floppy-o", "EditLine(" + c.HeaderId + ")","",false,false,"display:none") +
                                         Helper.GenerateActionIcon(Convert.ToInt32(c.HeaderId), "Cancel", "Cancel Edit", "fa fa-ban", "CancelEditLine(" + c.HeaderId + ")","",false,false,"display:none") +
                                         "<i title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/PQR/MaintainPQR/ShowTimeline?HeaderId=" + Convert.ToInt32(c.HeaderId) + "')  class='iconspace fa fa-clock-o' ></i>"+
                                         Helper.GenerateActionIcon(Convert.ToInt32(c.HeaderId), "Delete", "Delete Record", "fa fa-trash-o", "DeleteLine(" + c.HeaderId + ")","", Status.ToUpper() == "ALL" && c.Status != "Draft" && ViewBag.PQRRole == "Maintain" ? true : false, false,  Status.ToUpper() == "ALL" && c.Status != "Draft" && ViewBag.PQRRole == "Maintain" ? "pointer-events:none;" : null) 
                                        + "</nobr>"
                                        //Helper.GenerateActionIcon(Convert.ToInt32(c.HeaderId),"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.HeaderId + ");","", Status.ToUpper() == "ALL" && c.Status != "Draft" && ViewBag.PQRRole == "Maintain" ? true : false) + Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Update","Update Record","fa fa-floppy-o","EditLine(" + c.HeaderId + ");","",false,"display:none") + " " + Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.HeaderId + "); ","",false,"display:none") + " " + Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.HeaderId +");"),
                          }).ToList();

                return Json(new
                {
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,   
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();
                List<DropdownModel> lstConsumabledata = GetConsumableddlist();
                List<DropdownModel> lstWPdata = GetWPddlist(78);
                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(id)),
                                    "","",
                                    Helper.HTMLAutoComplete(newRecordId,"txtJointType","","",  false,"","JointType") +""+Helper.GenerateHidden(newRecordId, "JointType"),
                                    Helper.HTMLAutoComplete(newRecordId,"txtParentMetal1","","",  false,"","ParentMetal1") +""+Helper.GenerateHidden(newRecordId, "ParentMetal1"),
                                    Helper.HTMLAutoComplete(newRecordId,"txtGrNo1","","",  false,"","GrNo1",false,"5") +""+Helper.GenerateHidden(newRecordId, "GrNo1", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtParentMetal2","","",  false,"","ParentMetal2",false)+""+Helper.GenerateHidden(newRecordId, "ParentMetal2", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtGrNo2","","",  false,"","GrNo2",false,"5")+""+Helper.GenerateHidden(newRecordId, "GrNo2", Convert.ToString(newRecordId)),
                                    Helper.GenerateHTMLTextbox(newRecordId, "txtThickNess",  "", "",false, "", false,"5","","","numeric"),
                       //Helper.HTMLAutoComplete(newRecordId,"txtThickNess","","",  false,"","ThickNess",false)+""+Helper.GenerateHidden(newRecordId, "ThickNess", Convert.ToString(newRecordId)),
                                    MultiSelectDropdown(newRecordId,lstWPdata,"", false, "","txtWeldingProcess"),
                                    MultiSelectDropdown(newRecordId,lstConsumabledata,"", false, "","txtConsumable"),                      
                                    Helper.HTMLAutoComplete(newRecordId,"txtPWHTTemp","","", false,"","PWHTTemp",false,"10")+""+Helper.GenerateHidden(newRecordId, "PWHTTemp", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtPWHTTime","","",  false,"","PWHTTime",false,"20")+""+Helper.GenerateHidden(newRecordId, "PWHTTime", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtDesignCode","","", false,"","DesignCode",false)+""+Helper.GenerateHidden(newRecordId, "DesignCode", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtTPIAgency","","",  false,"","TPIAgency",false)+""+Helper.GenerateHidden(newRecordId, "TPIAgency", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtRemarks","","", false,"","Remarks",false,"100")+""+Helper.GenerateHidden(newRecordId, "Remarks", Convert.ToString(newRecordId)),
                                    "",
                                    //Helper.HTMLAutoComplete(newRecordId,"txtFinalFile","","", false,"","FinalFile",false)+""+Helper.GenerateHidden(newRecordId, "FinalFile", Convert.ToString(newRecordId)),
                                    //Helper.HTMLAutoComplete(newRecordId,"txtBackUpFile","","", false,"","BackUpFile",false)+""+Helper.GenerateHidden(newRecordId, "BackUpFile", Convert.ToString(newRecordId)),
                                    "",
                                    "",
                                     Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                    };
                    data.Add(newRecord);
                }
                else
                {

                    var lstLines = PQRDB.SP_PQR_GET_PQRDETAILS(1, 0, "", "HeaderId = " + id).Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                   clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.ROW_NO),
                                     Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "HeaderId", Convert.ToString(id)),
                                     Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "PQRNo", Convert.ToString(id)),
                                     Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"PQRFile","PQR File","fa fa-paperclip","OpenPQRFileModal(" + c.HeaderId + ");"),
                                    isReadOnly ? c.JointType : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtJointType",WebUtility.HtmlEncode(c.JointType),"", false,"","JointType",false)+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "JointType",Convert.ToString(c.JointType)),
                                    isReadOnly ? c.ParentMetal1 : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtParentMetal1",WebUtility.HtmlEncode(c.ParentMetal1),"", false,"","ParentMetal1",false)+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "ParentMetal1",Convert.ToString(c.ParentMetal1)),
                                    isReadOnly ? c.GrNo1 : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtGrNo1",WebUtility.HtmlEncode(c.GrNo1),"", false,"","GrNo1",false,"5")+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "GrNo1",Convert.ToString(c.GrNo1)),
                                    isReadOnly ? c.ParentMetal2 : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtParentMetal2",WebUtility.HtmlEncode(c.ParentMetal2),"", false,"","ParentMetal2",false)+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "ParentMetal2",Convert.ToString(c.ParentMetal2)),
                                    isReadOnly ? c.GrNo2 : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtGrNo2",c.GrNo2,"", false,"","GrNo2",false,"5")+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "GrNo2",Convert.ToString(c.GrNo2)),
                                    isReadOnly ? Convert.ToString(c.ThickNess) : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtThickNess", Convert.ToString(c.ThickNess),"", false,"","ThickNess",false,"5")+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "ThickNess",Convert.ToString(c.ThickNess)),
                                    isReadOnly ? c.WeldingProcess : MultiSelectDropdown(Convert.ToInt32(c.HeaderId),lstWPdata, c.WeldingProcess, false, "","txtWeldingProcess"),
                                    isReadOnly ? c.Consumable : MultiSelectDropdown(Convert.ToInt32(c.HeaderId),lstConsumabledata, c.Consumable, false, "","txtConsumable"),
                                    isReadOnly ? Convert.ToString(c.PWHTTemp) : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtPWHTTemp", Convert.ToString(c.PWHTTemp),"", false,"","PWHTTemp",false,"10")+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "PWHTTemp",Convert.ToString(c.PWHTTemp)),
                                    isReadOnly ? Convert.ToString(c.PWHTTime) : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtPWHTTime", Convert.ToString(c.PWHTTime),"", false,"","PWHTTime",false,"20")+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "PWHTTime",Convert.ToString(c.PWHTTime)),
                                    isReadOnly ? c.DesignCode : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtDesignCode",WebUtility.HtmlEncode(c.DesignCode),"", false,"","DesignCode",false)+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "DesignCode",Convert.ToString(c.DesignCode)),
                                    isReadOnly ? c.TPIAgency : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtTPIAgency",WebUtility.HtmlEncode(c.TPIAgency),"", false,"","TPIAgency",false)+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "TPIAgency",Convert.ToString(c.TPIAgency)),
                                    isReadOnly ? c.Remarks : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtRemarks",WebUtility.HtmlEncode(c.Remarks),"", false,"","Remarks",false,"100")+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "Remarks",Convert.ToString(c.Remarks)),
                                    Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "Status", Convert.ToString(id)),
                                    //isReadOnly ? Convert.ToString(c.FinalFile) : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtFinalFile",WebUtility.HtmlEncode(Convert.ToString(c.FinalFile)),"", false,"","FinalFile",false)+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "FinalFile",Convert.ToString(c.FinalFile)),
                                   // isReadOnly ?  Convert.ToString(c.BackUpFile) : Helper.HTMLAutoComplete(Convert.ToInt32(c.HeaderId), "txtBackUpFile",WebUtility.HtmlEncode(Convert.ToString(c.BackUpFile)),"", false,"","BackUpFile",false)+""+Helper.GenerateHidden(Convert.ToInt32(c.HeaderId), "BackUpFile",Convert.ToString(c.BackUpFile)),
                                    c.CreatedBy,
                                    Convert.ToDateTime(c.CreatedOn).ToString("dd/MM/yyyy"),
                                    Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.HeaderId + ");") +
                                    Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Update","Update Record","fa fa-floppy-o","EditLine(" + c.HeaderId + ");","",false,"display:none") + " " +
                                    Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.HeaderId + "); ","",false,"display:none") + " " +
                                    Helper.HTMLActionString(Convert.ToInt32(c.HeaderId),"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.HeaderId +");"),
                                }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public static string MultiSelectDropdown(int rowId, List<DropdownModel> list, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string ColumnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }

                string inputID = ColumnName + "" + rowId.ToString();
                multipleSelect += "<select name=\"" + inputID + "\" id=\"" + inputID + "\" multiple=\"multiple\"  style=\"width: 100 % \" colname=\"" + ColumnName + "\" class=\"form-control\" " + (Disabled ? "disabled" : "") + (OnChangeEvent != string.Empty ? " onchange=\"" + OnChangeEvent + "\"" : "") + " oldvalue=\"" + selectedValue + "\" >";
                foreach (var item in list)
                {
                    multipleSelect += "<option value=\"" + item.Code + "\" " + ((arrayVal.Contains(item.Code)) ? " selected" : "") + " >" + item.Description + "</option>";
                }
                foreach (var item in arrayVal)
                {
                    if (!list.Any(i => i.Code == item))
                    {
                        multipleSelect += "<option value=\"" + item + "\" selected>" + item + "</option>";
                    }
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }

        public ActionResult SaveData(FormCollection fc, int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    string JointType = fc["txtJointType" + Id];
                    string ParentMetal1 = fc["txtParentMetal1" + Id];
                    string GrNo1 = fc["txtGrNo1" + Id];
                    string ParentMetal2 = fc["txtParentMetal2" + Id];
                    string GrNo2 = fc["txtGrNo2" + Id];
                    string ThickNess = fc["txtThickNess" + Id];
                    string WeldingProcess = fc["txtWeldingProcess" + Id];
                    string Consumable = fc["txtConsumable" + Id];
                    string PWHTTemp = fc["txtPWHTTemp" + Id];
                    string PWHTTime = fc["txtPWHTTime" + Id];
                    string DesignCode = fc["txtDesignCode" + Id];
                    string TPIAgency = fc["txtTPIAgency" + Id];
                    string Remarks = fc["txtRemarks" + Id];

                    double? temp = null;
                    double? thick = null;
                    if (PWHTTemp != "")
                        temp = Convert.ToDouble(PWHTTemp);
                    if (ThickNess != "")
                        thick = Convert.ToDouble(ThickNess);

                    PQR001 objPQR001 = null;

                    if (Id > 0)
                    {
                        objPQR001 = PQRDB.PQR001.Where(x => x.HeaderId == Id).FirstOrDefault();
                        if (objPQR001 != null)
                        {
                            objPQR001.JointType = JointType;
                            objPQR001.ParentMetal1 =ParentMetal1;
                            objPQR001.ParentMetal2 = ParentMetal2;
                            objPQR001.WeldingProcess = WeldingProcess;
                            objPQR001.GrNo1 = GrNo1;
                            objPQR001.GrNo2 = GrNo2;
                            objPQR001.Consumable = Consumable;
                            objPQR001.DesignCode = DesignCode;
                            objPQR001.TPIAgency = TPIAgency;
                            objPQR001.PWHTTemp = temp;
                            objPQR001.PWHTTime = PWHTTime;
                            objPQR001.ThickNess = thick;
                            objPQR001.Remarks = Remarks;

                            objPQR001.EditedBy = objClsLoginInfo.UserName;
                            objPQR001.EditedOn = DateTime.Now;

                            PQRDB.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                        }

                    }
                    else
                    {             
                        objPQR001 = new PQR001();
                        objPQR001.HeaderId = Convert.ToInt32(8);
                        objPQR001.JointType = JointType;
                        objPQR001.ParentMetal1 =ParentMetal1;
                        objPQR001.ParentMetal2 = ParentMetal2;
                        objPQR001.WeldingProcess = WeldingProcess;
                        objPQR001.GrNo1 = GrNo1;
                        objPQR001.GrNo2 = GrNo2;
                        objPQR001.Consumable = Consumable;
                        objPQR001.DesignCode = DesignCode;
                        objPQR001.TPIAgency = TPIAgency;
                        objPQR001.PWHTTemp = temp;
                        objPQR001.PWHTTime = PWHTTime;
                        objPQR001.ThickNess = thick;
                        objPQR001.Remarks = Remarks;
                        objPQR001.Status = "Draft";
                        objPQR001.PQRFile = false;
                        objPQR001.FinalFile = false;
                        objPQR001.BackUpFile = false;

                        objPQR001.CreatedBy = objClsLoginInfo.UserName;
                        objPQR001.CreatedOn = DateTime.Now;

                        PQRDB.PQR001.Add(objPQR001);

                        PQRDB.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult SubmitPQR(string strHeader)
        {
            var objResponseMsg = new ResponceMsgWithObject();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    List<string> submittedPQR = new List<string>();
                    List<string> pendingPQR = new List<string>();
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        var PQR001 = PQRDB.PQR001.FirstOrDefault(x => x.HeaderId == headerId);
                        if (SendForApproval(headerId))
                            submittedPQR.Add(PQR001.JointType);  //Temporary code
                        else
                            pendingPQR.Add(PQR001.JointType);
                    }
                    objResponseMsg.data = new
                    {
                        submittedPQR = (submittedPQR.Count > 0 ? objResponseMsg.data = "PQR " + String.Join(",", submittedPQR) + " submitted successfully" : ""),
                        pendingPQR = (pendingPQR.Count > 0 ? objResponseMsg.data = "Fail to Submit PQR:" + String.Join(",", pendingPQR) + "." : "")
                    };
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PQR has been sucessfully submitted for approval";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record sending for approval";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ApprovePQR(string strHeader)
        {
            var objResponseMsg = new ResponceMsgWithObject();
            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    List<string> approvedPQR = new List<string>();
                    List<string> pendingPQR = new List<string>();
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    foreach (int headerId in headerIds)
                    {
                        var PQR001 = PQRDB.PQR001.FirstOrDefault(x => x.HeaderId == headerId);
                        if (ApproveByUser(headerId))
                            approvedPQR.Add(PQR001.JointType);
                        else
                            pendingPQR.Add(PQR001.JointType);
                    }
                    objResponseMsg.data = new
                    {
                        approvedPQR = (approvedPQR.Count > 0 ? objResponseMsg.data = "PQR " + String.Join(",", approvedPQR) + " Approved successfully" : ""),
                        pendingPQR = (pendingPQR.Count > 0 ? objResponseMsg.data = "Fail to Approve PQR:" + String.Join(",", pendingPQR) + "." : "")
                    };
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PQR has been sucessfully approved.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select atleast one record for approve";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private bool ApproveByUser(int headerId)
        {
            var lineStatus = clsImplementationEnum.PQRStatus.SendToApproval.GetStringValue();
            var objPQR001 = PQRDB.PQR001.FirstOrDefault(i => i.HeaderId == headerId && i.Status == lineStatus);
            if (objPQR001 != null)
            {
                objPQR001.Status = clsImplementationEnum.PQRStatus.Approved.GetStringValue();
                objPQR001.ApprovedBy = objClsLoginInfo.UserName;
                objPQR001.ApprovedOn = DateTime.Now;
                
                long lastpqrno = 0;
                var temp =  PQRDB.PQR001.Where(x => x.Status == "Approved").OrderByDescending(y => y.PQRNo).FirstOrDefault();
                if (temp != null)
                    lastpqrno = Convert.ToInt64(temp.PQRNo) + 1;
                else
                    lastpqrno = 1;

                objPQR001.PQRNo = lastpqrno;

                PQRDB.SaveChanges();
               

                #region Send Notification
                //(new clsManager()).SendNotificationByUserPSNumber(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),objPBP002.SubmittedBy, "PBP: " + objPBP002.Procedure + " is approved", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/PBP/MaintainPBP/Index?type="+objPBP002.Product);
                #endregion

                return true;
            }
            return false;
        }

        private bool SendForApproval(int headerId)
        {
            var objPQR = PQRDB.PQR001.FirstOrDefault(i => i.HeaderId == headerId);
            var lineStatus = new string[] { clsImplementationEnum.PQRStatus.Draft.GetStringValue()};
            if (objPQR != null && lineStatus.Contains(objPQR.Status))
            {
                objPQR.Status = clsImplementationEnum.PQRStatus.SendToApproval.GetStringValue();
                objPQR.SubmittedBy = objClsLoginInfo.UserName;
                objPQR.SubmittedOn = DateTime.Now;

                PQRDB.SaveChanges();

                #region Send Notification
                clsManager objManager = new clsManager();
                string[] psnolist = objManager.getEmployeeFromRole(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), objClsLoginInfo.Location).Select(x => x.psnum).ToArray();
                string[] psnolist1 = objManager.getEmployeeFromRole(clsImplementationEnum.UserRoleName.WE1.GetStringValue(), objClsLoginInfo.Location).Select(x => x.psnum).ToArray();
                string strPSNoList = string.Join(",", psnolist);
                string strPSNoList1 = string.Join(",", psnolist1);

                if (!string.IsNullOrEmpty(strPSNoList1))
                    strPSNoList = strPSNoList + "," + strPSNoList1;

                (new clsManager()).SendNotification(null, "", "", "", "PQR has been submitted, please review and approve.", clsImplementationEnum.NotificationType.Information.GetStringValue(), "", strPSNoList);

                #endregion

                return true;
            }
            return false;
        }


        [HttpPost]
        public ActionResult DeleteData(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PQR001 objPQR001 = PQRDB.PQR001.Where(x => x.HeaderId == Id).FirstOrDefault();
                if (objPQR001 != null)
                {
                    PQRDB.PQR001.Remove(objPQR001);
                    PQRDB.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ShowTimeline(int HeaderId)
        {
            TimelineViewModel model = new TimelineViewModel();
           
            if (HeaderId > 0)
            {
                PQR001 objPQR001 = PQRDB.PQR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                model.CreatedBy = Manager.GetUserNameFromPsNo(objPQR001.CreatedBy);
                model.CreatedOn = objPQR001.CreatedOn;

                model.EditedBy = Manager.GetUserNameFromPsNo(objPQR001.EditedBy);
                model.EditedOn = objPQR001.EditedOn;

                model.SubmittedBy = Manager.GetUserNameFromPsNo(objPQR001.SubmittedBy);
                model.SubmittedOn = objPQR001.SubmittedOn;               

                model.ApprovedBy = Manager.GetUserNameFromPsNo(objPQR001.ApprovedBy);
                model.ApprovedOn = objPQR001.ApprovedOn;

            }
            else
            {
                PQR001 objPQR001 = PQRDB.PQR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objPQR001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objPQR001.CreatedBy) : null;
                model.CreatedOn = objPQR001.CreatedOn;
                model.EditedBy = objPQR001.EditedBy != null ? Manager.GetUserNameFromPsNo(objPQR001.EditedBy) : null;
                model.EditedOn = objPQR001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }

        #region DropDown Data
        [HttpPost]
        public ActionResult GetDropDataResult(int Category, string term)
        {
            var lstLength = new List<Projects>();

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstLength = db.Database.SqlQuery<Projects>("Select distinct Code as Value, Code as Text from [dbo].[GLB002] Where Code like '%" + term + "%' and Category = " + Category + " and Location = '" + objClsLoginInfo.Location + "'  order by Code").ToList();
            }
            else
            {
                string _qry = "Select distinct Code as Value,Code as Text from [dbo].[GLB002] Where  Category = " + Category + " and Location = '" + objClsLoginInfo.Location + "' order by Code";
                lstLength = db.Database.SqlQuery<Projects>(_qry).ToList();
            }

            return Json(lstLength, JsonRequestBehavior.AllowGet);
        }


        private List<DropdownModel> GetConsumableddlist()
        {
            var lstLength = new List<DropdownModel>();
            lstLength = db.Database.SqlQuery<DropdownModel>("Select distinct AWSClass as Code,AWSClass as Description from [dbo].[WPS002] Where Location = '" + objClsLoginInfo.Location + "' order by AWSClass").ToList();
            return lstLength;
        }

        private List<DropdownModel> GetWPddlist(int Category)
        {
            var lstLength = new List<DropdownModel>();
            lstLength = db.Database.SqlQuery<DropdownModel>("Select distinct Code as Code , Code as Description from [dbo].[GLB002] Where Category = " + Category + " and Location = '" + objClsLoginInfo.Location + "' order by Code").ToList();
            return lstLength;
        }
        
        #endregion

    }
}