﻿using IEMQS.Areas.HTR.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.DESCore;
using IEMQS.DESServices;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace IEMQS.Areas.HTR.Controllers
{
    public class MaintainController : clsBase
    {
        // GET: HTR/Maintain
        [SessionExpireFilter]
        [AllowAnonymous]
        [UserPermissions]
        #region Header
        //index page (first grid)


        public ActionResult Index()
        {
            return View();
        }

        //index page (partial for grid)
        [HttpPost]
        public ActionResult LoadHTRHeaderDataPartial(string status, string title)
        {
            ViewBag.Status = status;
            ViewBag.Title = title;

            if (objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue()))
                ViewBag.IsAllowReWritePDFDocument = true;

            return PartialView("_LoadHTRHeaderDataPartial");
        }

        //datatable funtion for header (index page)
        [HttpPost]
        public JsonResult LoadHTRHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "') and ltrim(rtrim(CreatedBy))='" + objClsLoginInfo.UserName + "'";
                }
                else
                {
                    strWhere += " 1=1";
                }
                string[] columnName = { "Project", "HTRNo", "HTRDescription", "Department", "Treatment", "Location", "ConcertoMSP", "Furnace", "RevNo", "Status", "CreatedBy", "EditedBy", " (Select top 1 STUFF((select distinct ', '+ PartNo from htr002 where HeaderId = htr.HeaderId FOR XML PATH(''), TYPE ).value('.', 'NVARCHAR(MAX)') ,1,1,''))" };

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                //strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "nde10.BU", "nde10.Location");
                //var lstResult = db.SP_HTR_HEADER_GETDETAILS
                //                (
                //                 StartIndex, EndIndex, strSortOrder, strWhere
                //                ).ToList();
                var lstResult = db.SP_HTR_HEADER_GETDETAILS_PRJ_ACCESS
                                (
                                objClsLoginInfo.UserName, StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.HTRNo),
                            Convert.ToString(uc.PartDescription),
                            Convert.ToString("R" + uc.RevNo),
                            uc.HTRDescription,
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.CreatedOn.Value!=null ?uc.CreatedOn.Value.ToString("dd/MM/yyyy"):string.Empty),

                            Convert.ToString( uc.Department),
                            Convert.ToString( uc.ConcertoMSP),
                            Convert.ToString(uc.Treatment),
                            Convert.ToString(uc.Furnace),
                            Convert.ToString(uc.CreatedBy),
                            Convert.ToString(uc.EditedBy),
                            Convert.ToString(uc.ProjectDesc),
                              "<nobr><center>" +
                            "<a class='iconspace' title='View Details' href='"+WebsiteURL+"/HTR/Maintain/AddHeader?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i class='fa fa-eye'></i></a>"+
                            HTMLActionString(uc.HeaderId,uc.Status, "Delete", "Delete Document", "fa fa-trash-o", "HandleRequest('" +  Convert.ToInt32(uc.HeaderId) + "');", true, false)+
                            "<i class='iconspace fa fa-clock-o'  title='Show Timeline' href='javascript:void(0)' onclick=ShowTimeline('/HTR/Maintain/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>"+
                            (uc.Status.ToLower() != clsImplementationEnum.HTRStatus.Draft.GetStringValue().ToLower() ? HTMLActionString(uc.HeaderId,"","History","History Record","iconspace fa fa-history","ViewHistoryData('"+ Convert.ToInt32(uc.HeaderId) +"');", true, true) : HTMLActionString(uc.HeaderId,"","History","History Record","disabledicon fa fa-history","", true, true))+
                            (uc.Status.ToLower() == clsImplementationEnum.HTRStatus.Release.GetStringValue().ToLower() ? "<i id='Print' name='Print' style='cursor: pointer;' title='Print Report' class='iconspace fa fa-print' onclick='PrintReport("+ Convert.ToInt32(uc.HeaderId) +");'></i>" :  "<i id='Print' name='Print' style='cursor: pointer;' title='Print Report' class='disabledicon fa fa-print'></i>")+
                            (uc.Status.ToLower() == clsImplementationEnum.HTRStatus.Release.GetStringValue().ToLower() ? "<i id='btnCopyData' name='Copy' title='Copy Record' class='iconspace fa fa-files-o' onclick='CopyHTR("+ Convert.ToInt32(uc.HeaderId) +");'></i>" :  "<i id='btnCopyData' name='Copy' title='Copy Record' class='disabledicon fa fa-files-o'></i>" )+
                            Helper.HTMLActionString(uc.HeaderId, "Attachments", "File Attachments", "fa fa-paperclip", "showAttachments("+uc.HeaderId + ","+uc.RevNo+")","",false,  "color:#32CD32;" )+
                            "</center></nobr>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //timeline functionality for header grid
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "HTR History";
            model.Title = "HTR";
            HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objHTR001.CreatedBy);
            model.CreatedOn = objHTR001.CreatedOn;
            model.ReviewedBy = Manager.GetUserNameFromPsNo(objHTR001.MET);
            model.ReviewedOn = objHTR001.ReviewdOn;
            model.FurnaceBy = Manager.GetUserNameFromPsNo(objHTR001.FurnaceSupervisor);
            model.FurnaceOn = objHTR001.FurnaceOn;
            model.InspectBy = Manager.GetUserNameFromPsNo(objHTR001.Inspector);
            model.InspectOn = objHTR001.InspectOn;
            model.SubmittedBy = Manager.GetUserNameFromPsNo(objHTR001.SubmittedBy);
            model.SubmittedOn = objHTR001.SubmittedOn;
            if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue())
            {
                model.HTRType = objHTR001.HTRType;

            }
            else if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponentNuclear.GetStringValue())
            {
                model.HTRType = objHTR001.HTRType;

            }
            else if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.PTC.GetStringValue())
            {
                model.HTRType = objHTR001.HTRType;
            }
            else
            {
                model.HTRType = objHTR001.HTRType;
                model.HTRpqrStatus = objHTR001.Status;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        public JsonResult GetHTRProjects(string search, string param)
        {
            try
            {
                var items = (from htr in db.HTR001
                             join com1 in db.COM001 on htr.Project equals com1.t_cprj
                             where (search == null || htr.Project.Contains(search) || com1.t_dsca.Contains(search))
                             select new SelectItemList
                             {
                                 id = htr.Project,
                                 text = htr.Project + "-" + com1.t_dsca,
                             }).Distinct().Take(10).ToList();
                if (string.IsNullOrWhiteSpace(search))
                    items.Insert(0, new SelectItemList() { id = "ALL", text = "ALL" });
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ReWritePDFDocument(string Projects)
        {
            var objResponseMsg = new ResponceMsgWithHeaderID();

            if (!objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue()))
            {
                objResponseMsg.Value = "Access Denied!";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrWhiteSpace(Projects))
            {
                objResponseMsg.Value = "Please select any Project";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

            var lstHTR001 = new List<HTR001>();
            var lstFailHTR = new List<string>();
            var lstProjects = Projects.Split(',').Where(w => w != "ALL").ToArray();
            var lstBU = Manager.GetUserwiseBU(objClsLoginInfo.UserName);
            var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);

            if (Projects == "ALL")
                lstHTR001 = db.HTR001.Where(w => lstBU.Contains(w.BU) && lstLoc.Contains(w.Location)).ToList();
            else
                lstHTR001 = db.HTR001.Where(w => lstProjects.Contains(w.Project) && lstBU.Contains(w.BU) && lstLoc.Contains(w.Location)).ToList();

            foreach (var item in lstHTR001)
            {
                try
                {
                    var folderPath = "HTR001/LoadingSketch/" + item.HeaderId + "/R" + item.RevNo;
                    var HTRDocuments = (new clsFileUpload()).GetDocuments(folderPath).Select(s => s.Name);
                    Utility.Controllers.FileUploadController.FileUpload_WriteContentOnPDF(String.Join(",", HTRDocuments), folderPath, item.HTRNo + " (R" + item.RevNo + ")", 0, 0, 90, 0, "bottomleft");
                    Utility.Controllers.FileUploadController.FileUpload_WriteContentOnPDF(String.Join(",", HTRDocuments), folderPath, item.HTRNo + " (R" + item.RevNo + ")", 0, 0, 0, 0, "topright");
                }
                catch (Exception ex)
                {
                    lstFailHTR.Add(item.HTRNo);
                }
            }
            if (lstFailHTR.Any())
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "HTR NO [" + string.Join(", ", lstFailHTR) + "] : are fail to re-write HTR document.";
            }
            else
            {
                objResponseMsg.Key = true;
                objResponseMsg.Value = "HTR NO [" + string.Join(", ", lstProjects) + "] : are succesfully re-write HTR document.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetFurnaceSupervisorByFurnace(string term, string[] Furnace)
        {
            List<ApproverModel> lstApprovers;
            //var lstFurnaceSupervisor = db.HTR004.Where(w => w.Furnace == Furnace).Select(s => s.FurnaceSupervisor).FirstOrDefault();
            var lstFurnaceSupervisor = db.HTR004.Where(w => Furnace.Contains(w.Furnace)).Select(s => s.FurnaceSupervisor).FirstOrDefault();
            if (!String.IsNullOrWhiteSpace(lstFurnaceSupervisor))
            {
                var Codes = lstFurnaceSupervisor.Split(',');
                lstApprovers = Manager.GetApproverList(clsImplementationEnum.UserRoleName.FS3.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term).Distinct().Where(w => Codes.Contains(w.Code)).ToList().ToList();
            }
            else
            {
                lstApprovers = new List<ApproverModel>();//Manager.GetApproverList(clsImplementationEnum.UserRoleName.FS3.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term).Distinct().ToList();
            }
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Add Header
        //create header main view
        [SessionExpireFilter]
        public ActionResult AddHeader(int HeaderID = 0)
        {
          
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        //create header partial view
        [HttpPost]
        public ActionResult LoadHTRFormPartial(int HeaderID)
        {
            HTR001 objHTR001 = new HTR001();
            bool isInitiator = false;
            string user = objClsLoginInfo.UserName;

            List<Projects> project = Manager.getProjectsByUser(user);
            var location = objClsLoginInfo.Location;
            string userdept = (from deptr in db.COM003
                               where deptr.t_psno == objClsLoginInfo.UserName && deptr.t_actv == 1
                               select deptr.t_depc).FirstOrDefault();

            var department = (from a in db.COM002
                              where a.t_dimx == userdept
                              select new { a.t_dimx, Desc = a.t_dimx + "" + " - " + "" + a.t_desc }).FirstOrDefault();
            var role = (from a in db.ATH001
                        join b in db.ATH004 on a.Role equals b.Id
                        where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                        select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();
            if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(), StringComparison.OrdinalIgnoreCase) ||
                                i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue(), StringComparison.OrdinalIgnoreCase) ||
                                i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), StringComparison.OrdinalIgnoreCase) ||
                                i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase) ||
                                i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(), StringComparison.OrdinalIgnoreCase) ||
                                i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue(), StringComparison.OrdinalIgnoreCase) ||
                                i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), StringComparison.OrdinalIgnoreCase) ||
                                i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
            {
                ViewBag.ShowCheckList = true;
            }
            else
            {
                ViewBag.ShowCheckList = false;
            }
            if (HeaderID > 0)
            {
                objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
                objHTR001.Department = db.COM002.Where(a => a.t_dimx == objHTR001.Department).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objHTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objHTR001.Location).Select(a => a.t_desc).FirstOrDefault();
                string strInspector = db.COM003.Where(x => x.t_psno == objHTR001.Inspector).Select(x => x.t_name).FirstOrDefault();
                string strMET = db.COM003.Where(x => x.t_psno == objHTR001.MET).Select(x => x.t_name).FirstOrDefault();
                string strSupervisor = db.COM003.Where(x => x.t_psno == objHTR001.FurnaceSupervisor).Select(x => x.t_name).FirstOrDefault();
                ViewBag.Inspector = objHTR001.Inspector + " - " + strInspector;
                if (!string.IsNullOrEmpty(objHTR001.MET) && !string.IsNullOrEmpty(strMET))
                {
                    ViewBag.MET = objHTR001.MET + " - " + strMET;
                }
                else
                {
                    ViewBag.MET = string.Empty;
                }

                ViewBag.FurnaceSupervisor = objHTR001.FurnaceSupervisor + " - " + strSupervisor;
                //ViewBag.LoadingSketchPath = JsonConvert.SerializeObject(Manager.getDocuments("HTR001/LoadingSketch/" + objHTR001.HeaderId + "/R" + objHTR001.RevNo));
                //ViewBag.GeneralNotesPath = JsonConvert.SerializeObject(Manager.getDocuments("HTR001/GeneralNotes/" + objHTR001.HeaderId + "/R" + objHTR001.RevNo));
                //ViewBag.ThermocouplePath = JsonConvert.SerializeObject(Manager.getDocuments("HTR001/ThermocoupleAttachments/" + objHTR001.HeaderId + "/R" + objHTR001.RevNo));
                ViewBag.CreatedBy = objHTR001.CreatedBy;
                ViewBag.Action = "edit";
                if (objHTR001.CreatedBy.Trim() == objClsLoginInfo.UserName && objHTR001.Status == clsImplementationEnum.HTRStatus.Draft.GetStringValue())
                    isInitiator = true;

                if (role.Any(a => a.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)) && objHTR001.Status == clsImplementationEnum.HTRStatus.InReview.GetStringValue())
                    ViewBag.IsRetractable = true;
            }
            else
            {
                objHTR001.Location = location;
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && location.Contains(a.t_dimx)).Select(a => a.t_desc).FirstOrDefault();
                objHTR001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                objHTR001.RevNo = 0;
                objHTR001.Department = department?.Desc;
                isInitiator = true;
            }
            #region Commented By Ajay (role wise HTR Type)
            //Project flow creation role wise code
            //if (role.Where(i => i.RoleDesc.Equals("PMG2", StringComparison.OrdinalIgnoreCase) ||
            //                    i.RoleDesc.Equals("PMG3", StringComparison.OrdinalIgnoreCase) ||
            //                    i.RoleDesc.Equals("PLNG1", StringComparison.OrdinalIgnoreCase) ||
            //                    i.RoleDesc.Equals("PLNG3", StringComparison.OrdinalIgnoreCase) ||
            //                    i.RoleDesc.Equals("PLNG2", StringComparison.OrdinalIgnoreCase)  ||
            //                    i.RoleDesc.Equals( clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
            //{
            //    ViewBag.TypeOfHTR = clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue().ToList().ToArray();
            //}
            //else if (role.Where(i => i.RoleDesc.Equals("WE3", StringComparison.OrdinalIgnoreCase) ||
            //                    i.RoleDesc.Equals("MET3", StringComparison.OrdinalIgnoreCase)).ToList().Any())
            //{
            //    ViewBag.TypeOfHTR = clsImplementationEnum.TypeOfHTR.PQR.GetStringValue().ToArray();
            //}
            //else if (role.Where(i => i.RoleDesc.Equals("WE3", StringComparison.OrdinalIgnoreCase)).ToList().Any())
            //{
            //    ViewBag.TypeOfHTR = clsImplementationEnum.TypeOfHTR.PTC.GetStringValue().ToArray();
            //}
            #endregion
            ViewBag.isInitiator = isInitiator;
            ViewBag.TypeOfHTR = clsImplementationEnum.GetTypeOfHTR().ToArray();
            ViewBag.Treatment = clsImplementationEnum.GetTreatment().ToArray();
            ViewBag.Furnace = clsImplementationEnum.GetFurnace().ToArray();
            ViewBag.MinLoadingTemp = clsImplementationEnum.GetMinLoadingTemp().ToArray();
            ViewBag.MaxLoadingTemp = clsImplementationEnum.GetMaxLoadingTemp().ToArray();
            ViewBag.MinRateOfHeating = clsImplementationEnum.GetMinRateOfHeating().ToArray();
            ViewBag.MaxRateOfHeating = clsImplementationEnum.GetMaxRateOfHeating().ToArray();
            ViewBag.SoakingTimeTolerance = clsImplementationEnum.GetSoakingTimeTolerance().ToArray();
            ViewBag.MinRateOfCooling = clsImplementationEnum.GetMinRateOfCooling().ToArray();
            ViewBag.MaxRateOfCooling = clsImplementationEnum.GetMaxRateOfCooling().ToArray();
            ViewBag.MinUnloadingTemp = clsImplementationEnum.GetMinUnloadingTemp().ToArray();
            ViewBag.MaxUnloadingTemp = clsImplementationEnum.GetMaxUnloadingTemp().ToArray();
            ViewBag.CoolPart = clsImplementationEnum.GetCoolPart().ToArray();
            ViewBag.CoolBy = clsImplementationEnum.GetCoolBy().ToArray();
            ViewBag.VolTempOf = clsImplementationEnum.GetVolTempOf().ToArray();
            ViewBag.HeatingThermocouplesVariation = clsImplementationEnum.GetHeatingThermocouplesVariation().ToArray();
            ViewBag.CoolingThermocouplesVariation = clsImplementationEnum.GetCoolingThermocouplesVariation().ToArray();
            ViewBag.CoolingThermocouplesVariation = clsImplementationEnum.GetCoolingThermocouplesVariation().ToArray();
            ViewBag.FurnaceBedDistance = clsImplementationEnum.GetFurnaceBedDistance().ToArray();
            ViewBag.StandardProcedure = clsImplementationEnum.GetStandardProcedure().ToArray();
            ViewBag.WPSNo = clsImplementationEnum.GetWPSNo().ToArray();

            objHTR001.CreatedBy = objClsLoginInfo.UserName;
            return PartialView("_HTRFormPartial", objHTR001);
        }

        //create check list pop up for PCC role
        [HttpPost]
        public ActionResult LoadHTRCheckList(int HeaderId)
        {
            HTR001 objHTR001 = new HTR001();
            if (HeaderId > 0)
            {
                objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            }
            return PartialView("_LoadHTRCheckListPartial", objHTR001);
        }

        //public ActionResult SaveHeader(HTR001 htr001, FormCollection fc, string LinkedProject, bool hasLSAttachments, Dictionary<string, string> lsAttach, bool ISPromoted)
        #endregion

        #region CRUD Operations for header details
        //Insert/Update header details in HTR001 with attachments
        [HttpPost]
        public ActionResult SaveHeader(HTR001 htr001, FormCollection fc, bool ISPromoted)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                HTR001 objHTR001 = new HTR001();
                if (htr001.HeaderId > 0)
                {
                    objHTR001 = db.HTR001.Where(x => x.HeaderId == htr001.HeaderId).FirstOrDefault();
                }

                objHTR001.DONo = htr001.DONo;
                objHTR001.PoNo = htr001.PoNo;
                objHTR001.IdenticalProject = fc["IdenticalProject"];
                objHTR001.ConcertoMSP = htr001.ConcertoMSP;
                objHTR001.Treatment = htr001.Treatment;
                objHTR001.HTRReferenceNo = htr001.HTRReferenceNo;
                objHTR001.Furnace = fc["Furnace"];
                objHTR001.HTRDescription = htr001.HTRDescription;
                objHTR001.FurnaceCommentsRequired = htr001.FurnaceCommentsRequired;
                objHTR001.FurnaceSupervisor = htr001.FurnaceSupervisor;
                objHTR001.InspectionCommentsRequired = htr001.InspectionCommentsRequired;
                objHTR001.Inspector = htr001.Inspector;
                objHTR001.MET = htr001.MET;
                objHTR001.MinLoadingTemp = htr001.MinLoadingTemp;
                objHTR001.MaxLoadingTemp = htr001.MaxLoadingTemp;
                objHTR001.MinRateOfHeating = htr001.MinRateOfHeating;
                objHTR001.MaxRateOfHeating = htr001.MaxRateOfHeating;
                objHTR001.MinSoakingTemperature = htr001.MinSoakingTemperature;
                objHTR001.MaxSoakingTemperature = htr001.MaxSoakingTemperature;
                objHTR001.SoakingTimeHH = htr001.SoakingTimeHH;
                objHTR001.SoakingTimeMM = htr001.SoakingTimeMM;
                objHTR001.SoakingTimeTolerance = htr001.SoakingTimeTolerance;
                objHTR001.MinRateOfCooling = htr001.MinRateOfCooling;
                objHTR001.MaxRateOfCooling = htr001.MaxRateOfCooling;
                objHTR001.MinUnloadingTemp = htr001.MinUnloadingTemp;
                objHTR001.MaxUnloadingTemp = htr001.MaxUnloadingTemp;
                objHTR001.CoolPart = htr001.CoolPart;
                objHTR001.CoolBy = htr001.CoolBy;
                objHTR001.MaxDelay = htr001.MaxDelay;
                objHTR001.VolTempOf = htr001.VolTempOf;
                objHTR001.TempOfLiquidAfterQuenching = htr001.TempOfLiquidAfterQuenching;
                objHTR001.MinVol = htr001.MinVol;
                objHTR001.HeatingThermocouplesVariation = htr001.HeatingThermocouplesVariation;
                objHTR001.CoolingThermocouplesVariation = htr001.CoolingThermocouplesVariation;
                objHTR001.StandardProcedure = htr001.StandardProcedure;
                objHTR001.FurnaceBedDistance = htr001.FurnaceBedDistance;
                objHTR001.WPSNo = htr001.WPSNo;
                objHTR001.Comments = htr001.Comments;

                if (htr001.HeaderId > 0)
                {

                    if (objHTR001.Status == clsImplementationEnum.HTRStatus.Release.GetStringValue())
                    {
                        if (!ISPromoted)
                        {
                            int? oldrev = objHTR001.RevNo;
                            int? newrev = Convert.ToInt32(objHTR001.RevNo) + 1;
                            objHTR001.RevNo = newrev;
                            objHTR001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                            objHTR001.FurnaceComments = null;
                            objHTR001.InspectorComments = null;
                            objHTR001.ReviewerComments = null;
                            objHTR001.ReviewdOn = null;
                            objHTR001.FurnaceOn = null;
                            objHTR001.InspectOn = null;
                            objHTR001.SubmittedBy = null;
                            objHTR001.SubmittedOn = null;
                            var folderPath = "HTR001/LoadingSketch/" + objHTR001.HeaderId + "/R" + newrev;
                            var oldFolderPath = "HTR001/LoadingSketch/" + objHTR001.HeaderId + "/R" + oldrev;
                            (new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, folderPath);
                        }
                    }
                    objHTR001.chkl1 = htr001.chkl1;
                    objHTR001.chkl2 = htr001.chkl2;
                    objHTR001.chkl3 = htr001.chkl3;
                    objHTR001.chkl4 = htr001.chkl4;
                    objHTR001.chkl5 = htr001.chkl5;
                    objHTR001.chkl6 = htr001.chkl6;
                    objHTR001.chkl7 = htr001.chkl7;
                    objHTR001.chkl8 = htr001.chkl8;
                    objHTR001.chkl9 = htr001.chkl9;
                    objHTR001.chkl10 = htr001.chkl10;
                    objHTR001.chkl11 = htr001.chkl11;
                    objHTR001.chkl12 = htr001.chkl12;
                    objHTR001.chkl13 = htr001.chkl13;
                    objHTR001.chkl14 = htr001.chkl14;
                    objHTR001.chkl15 = htr001.chkl15;
                    objHTR001.chkl16 = htr001.chkl16;
                    objHTR001.chkl17 = htr001.chkl17;
                    objHTR001.chkl18 = htr001.chkl18;
                    objHTR001.chkl19 = htr001.chkl19;
                    objHTR001.chkl20 = htr001.chkl20;
                    objHTR001.chkl21 = htr001.chkl21;
                    objHTR001.chkl22 = htr001.chkl22;
                    objHTR001.chkl23 = htr001.chkl23;
                    objHTR001.chkl24 = htr001.chkl24;
                    objHTR001.EditedBy = objClsLoginInfo.UserName.Trim();
                    objHTR001.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    #region code for Linked Project (removed on 24/10/2017 - as per requirements [observation no. 13507])
                    //HTR003 objHTR003 = new HTR003();
                    //List<HTR003> lstDesHTR003 = db.HTR003.Where(x => x.HeaderId == objHTR001.HeaderId).ToList();
                    //if (lstDesHTR003 != null && lstDesHTR003.Count > 0)
                    //{
                    //    db.HTR003.RemoveRange(lstDesHTR003);
                    //}
                    //var strLinkedProject = LinkedProject.Split('-')[0];
                    //string[] arrayLinkedProject = strLinkedProject.Split(',').ToArray();
                    //HTR003 newobjHTR003 = db.HTR003.Where(x => x.HeaderId == objHTR001.HeaderId).FirstOrDefault();
                    //List<HTR003> lstHTR003 = new List<HTR003>();
                    //for (int i = 0; i < arrayLinkedProject.Length; i++)
                    //{
                    //    HTR003 objHTR003Add = new HTR003();
                    //    objHTR003Add.HeaderId = objHTR001.HeaderId;
                    //    objHTR003Add.Project = objHTR001.Project;
                    //    objHTR003Add.HTRNo = objHTR001.HTRNo;
                    //    objHTR003Add.RevNo = objHTR001.RevNo;
                    //    objHTR003Add.LinkedProject = arrayLinkedProject[i];
                    //    objHTR003Add.CreatedBy = objClsLoginInfo.UserName;
                    //    objHTR003Add.CreatedOn = DateTime.Now;
                    //    lstHTR003.Add(objHTR003Add);

                    //}
                    //if (lstHTR003 != null && lstHTR003.Count > 0)
                    //{
                    //    db.HTR003.AddRange(lstHTR003);
                    //}
                    ////attachments
                    //var folderGeneralNotesPath = "HTR001/GeneralNotes/" + objHTR001.HeaderId + "/R" + objHTR001.RevNo;
                    //var folderThermocouplePath = "HTR001/ThermocoupleAttachments/" + objHTR001.HeaderId + "/R" + objHTR001.RevNo;
                    //Manager.ManageDocuments(folderGeneralNotesPath, hasGNAttachments, gnAttach, objClsLoginInfo.UserName);
                    //Manager.ManageDocuments(folderThermocouplePath, hasTAAttachments, taAttach, objClsLoginInfo.UserName);
                    #endregion

                    db.SaveChanges();

                    objResponseMsg.HeaderId = objHTR001.HeaderId;
                    objResponseMsg.revno = objHTR001.RevNo;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update.ToString();
                }
                else
                {
                    string doc = htr001.HTRNo.Split('/')[2];
                    int docNo = Convert.ToInt32(doc);
                    objHTR001.HTRType = htr001.HTRType;
                    objHTR001.Project = htr001.Project.Split('-')[0].Trim();
                    objHTR001.Location = htr001.Location.Trim(); //.Split('-')[0];
                    objHTR001.Department = htr001.Department.Split('-')[0].Trim();
                    objHTR001.BU = db.COM001.Where(i => i.t_cprj == objHTR001.Project).FirstOrDefault().t_entu.Trim();
                    objHTR001.HTRNo = htr001.HTRNo;
                    objHTR001.RevNo = htr001.RevNo;
                    objHTR001.DocNo = docNo;
                    objHTR001.Status = clsImplementationEnum.HTRStatus.Draft.GetStringValue();
                    objHTR001.CreatedBy = objClsLoginInfo.UserName.Trim();
                    objHTR001.CreatedOn = DateTime.Now;
                    objHTR001.chkl1 = htr001.chkl1;
                    objHTR001.chkl2 = htr001.chkl2;
                    objHTR001.chkl3 = htr001.chkl3;
                    objHTR001.chkl4 = htr001.chkl4;
                    objHTR001.chkl5 = htr001.chkl5;
                    objHTR001.chkl6 = htr001.chkl6;
                    objHTR001.chkl7 = htr001.chkl7;
                    objHTR001.chkl8 = htr001.chkl8;
                    objHTR001.chkl9 = htr001.chkl9;
                    objHTR001.chkl10 = htr001.chkl10;
                    objHTR001.chkl11 = htr001.chkl11;
                    objHTR001.chkl12 = htr001.chkl12;
                    objHTR001.chkl13 = htr001.chkl13;
                    objHTR001.chkl14 = htr001.chkl14;
                    objHTR001.chkl15 = htr001.chkl15;
                    objHTR001.chkl16 = htr001.chkl16;
                    objHTR001.chkl17 = htr001.chkl17;
                    objHTR001.chkl18 = htr001.chkl18;
                    objHTR001.chkl19 = htr001.chkl19;
                    objHTR001.chkl20 = htr001.chkl20;
                    objHTR001.chkl21 = htr001.chkl21;
                    objHTR001.chkl22 = htr001.chkl22;
                    objHTR001.chkl23 = htr001.chkl23;
                    objHTR001.chkl24 = htr001.chkl24;
                    db.HTR001.Add(objHTR001);

                    db.SaveChanges();
                    #region code for Linked Project (removed on 24/10/2017 - as per requirements [observation no. 13507])
                    //HTR003 objHTR003 = new HTR003();
                    //var strLinkedProject = LinkedProject.Split('-')[0];
                    //string[] arrayLinkedProject = strLinkedProject.Split(',').ToArray();
                    //HTR003 newobjHTR003 = db.HTR003.Where(x => x.HeaderId == objHTR001.HeaderId).FirstOrDefault();
                    //List<HTR003> lstHTR003 = new List<HTR003>();
                    //for (int i = 0; i < arrayLinkedProject.Length; i++)
                    //{
                    //    HTR003 objHTR003Add = new HTR003();
                    //    objHTR003Add.HeaderId = objHTR001.HeaderId;
                    //    objHTR003Add.Project = objHTR001.Project;
                    //    objHTR003Add.HTRNo = objHTR001.HTRNo;
                    //    objHTR003Add.RevNo = objHTR001.RevNo;
                    //    objHTR003Add.LinkedProject = arrayLinkedProject[i];
                    //    objHTR003Add.CreatedBy = objClsLoginInfo.UserName;
                    //    objHTR003Add.CreatedOn = DateTime.Now;
                    //    lstHTR003.Add(objHTR003Add);

                    //}
                    //if (lstHTR003 != null && lstHTR003.Count > 0)
                    //{
                    //    db.HTR003.AddRange(lstHTR003);
                    //}
                    ////atachments
                    //var folderGeneralNotesPath = "HTR001/GeneralNotes/" + objHTR001.HeaderId + "/R" + objHTR001.RevNo;
                    //var folderThermocouplePath = "HTR001/ThermocoupleAttachments/" + objHTR001.HeaderId + "/R" + objHTR001.RevNo;
                    //Manager.ManageDocuments(folderGeneralNotesPath, hasGNAttachments, gnAttach, objClsLoginInfo.UserName);
                    //Manager.ManageDocuments(folderThermocouplePath, hasTAAttachments, taAttach, objClsLoginInfo.UserName);
                    #endregion

                    db.SaveChanges();

                    objResponseMsg.revno = objHTR001.RevNo;
                    objResponseMsg.HeaderId = objHTR001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Insert.ToString();

                }
                (new clsManager()).ScurveHTC(objHTR001.Project, objHTR001.Location);
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //Insert/Update check list data for PCC role
        [HttpPost]
        public ActionResult SaveCheckList(HTR001 htr001, FormCollection fc)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                HTR001 objHTR001 = new HTR001();
                if (htr001.HeaderId > 0)
                {
                    objHTR001 = db.HTR001.Where(x => x.HeaderId == htr001.HeaderId).FirstOrDefault();
                }

                objHTR001.Comments = htr001.Comments;

                if (htr001.HeaderId > 0)
                {
                    if (objHTR001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objHTR001.RevNo = Convert.ToInt32(objHTR001.RevNo) + 1;
                        objHTR001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                        objHTR001.FurnaceComments = null;
                        objHTR001.InspectorComments = null;
                        objHTR001.ReviewerComments = null;
                        objHTR001.ReviewdOn = null;
                        objHTR001.FurnaceOn = null;
                        objHTR001.InspectOn = null;
                        objHTR001.SubmittedBy = null;
                        objHTR001.SubmittedOn = null;
                    }
                    objHTR001.chkl1 = htr001.chkl1;
                    objHTR001.chkl2 = htr001.chkl2;
                    objHTR001.chkl3 = htr001.chkl3;
                    objHTR001.chkl4 = htr001.chkl4;
                    objHTR001.chkl5 = htr001.chkl5;
                    objHTR001.chkl6 = htr001.chkl6;
                    objHTR001.chkl7 = htr001.chkl7;
                    objHTR001.chkl8 = htr001.chkl8;
                    objHTR001.chkl9 = htr001.chkl9;
                    objHTR001.chkl10 = htr001.chkl10;
                    objHTR001.chkl11 = htr001.chkl11;
                    objHTR001.chkl12 = htr001.chkl12;
                    objHTR001.chkl13 = htr001.chkl13;
                    objHTR001.chkl14 = htr001.chkl14;
                    objHTR001.chkl15 = htr001.chkl15;
                    objHTR001.chkl16 = htr001.chkl16;
                    objHTR001.chkl17 = htr001.chkl17;
                    objHTR001.chkl18 = htr001.chkl18;
                    objHTR001.chkl19 = htr001.chkl19;
                    objHTR001.chkl20 = htr001.chkl20;
                    objHTR001.chkl21 = htr001.chkl21;
                    objHTR001.chkl22 = htr001.chkl22;
                    objHTR001.chkl23 = htr001.chkl23;
                    objHTR001.chkl24 = htr001.chkl24;
                    objHTR001.CreatedBy = objClsLoginInfo.UserName;
                    objHTR001.CreatedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objHTR001.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update.ToString();
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //revise header
        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                string status = clsImplementationEnum.HTRStatus.Release.GetStringValue();
                HTR001 objHTR001 = db.HTR001.Where(u => u.HeaderId == strHeaderId && u.Status == status).FirstOrDefault();
                if (objHTR001 != null)
                {
                    objHTR001.RevNo = Convert.ToInt32(objHTR001.RevNo) + 1;
                    objHTR001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                    objHTR001.FurnaceComments = null;
                    objHTR001.InspectorComments = null;
                    objHTR001.ReviewerComments = null;
                    objHTR001.ReviewdOn = null;
                    objHTR001.FurnaceOn = null;
                    objHTR001.InspectOn = null;
                    objHTR001.SubmittedBy = null;
                    objHTR001.SubmittedOn = null;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objHTR001.HeaderId;
                    objResponseMsg.Status = objHTR001.Status;
                    objResponseMsg.rev = objHTR001.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //retract header
        [HttpPost]
        public JsonResult RetractHeader(int strHeaderId)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                string status = clsImplementationEnum.HTRStatus.InReview.GetStringValue();
                HTR001 objHTR001 = db.HTR001.Where(u => u.HeaderId == strHeaderId && u.Status == status).FirstOrDefault();
                if (objHTR001 != null)
                {
                    objHTR001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                    objHTR001.SubmittedOn = null;
                    objHTR001.EditedBy = objClsLoginInfo.UserName;
                    objHTR001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objHTR001.HeaderId;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract;
                }
                else
                {
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //bind met result

        public string GethtrdataHistory(int HeaderId)
        {
            StringBuilder str = new StringBuilder();
            var IsNotExists = false;
            var IsNewEntry = false;
            //var IsDeleted = false;
            string seam = string.Empty;
            List<string> listtd = null;
            List<string> listlinetd = null;
            List<string> listtr = null;
            try
            {
                var objHTRheader = db.HTR001_Log.Where(i => i.HeaderId == HeaderId).FirstOrDefault();

                str.Append("<table id=\"tblHTRLogDetails\" class=\"table table-bordered\">");

                #region Header
                //Create Header Row
                str.Append("<thead  style='background-color:#b1eaef' >");
                str.Append("<tr>" +
                            "<th style=\"max-width:100px;width:100px;\">Action By</th>" +
                            "<th>Action on Header</th>" +
                            "<th>HTR Document <br> /HTR Component</th>" +
                            "<th>Field</th>" +
                            "<th>New Details</th>" +
                             "<th>Old Details</th>" +
                            "</tr>");
                str.Append("</thead>");
                #endregion

                #region Body
                str.Append("<tbody>");
                //str.Append("<tr><td style=\"max-width:100px;width:100px;\"><b>R0</b></td><td>New Entry</td><td></td><td></td></tr>");
                #endregion

                if (objHTRheader != null)
                {
                    listtr = new List<string>();
                    var htrprevlog = db.HTR001_Log.Where(i => i.HeaderId == HeaderId).OrderByDescending(i => i.Id).ToList();
                    if (htrprevlog.Count > 1)
                    {
                        for (int i = htrprevlog.Count - 1; i > 0; i--)
                        {
                            listtd = new List<string>();
                            #region header
                            var cur = htrprevlog[i];
                            var prev = htrprevlog[i - 1];
                            var oType = prev.GetType();
                            foreach (var oProperty in oType.GetProperties())
                            {
                                if (oProperty.Name.ToLower() != ("id").ToLower() && oProperty.Name.ToLower() != ("RefId").ToLower() &&
                                oProperty.Name.ToLower() != ("LineId").ToLower() && oProperty.Name.ToLower() != ("HeaderId").ToLower()
                                 && oProperty.Name.ToLower() != ("EditedBy").ToLower() && oProperty.Name.ToLower() != ("EditedOn").ToLower() &&
                                oProperty.Name.ToLower() != ("CreatedBy").ToLower() && oProperty.Name.ToLower() != ("CreatedOn").ToLower() &&
                                oProperty.Name.ToLower() != ("SubmittedBy").ToLower() && oProperty.Name.ToLower() != ("SubmittedOn").ToLower() &&
                                oProperty.Name.ToLower() != ("Status").ToLower() && oProperty.Name.ToLower() != ("FurnaceComments").ToLower() &&
                                oProperty.Name.ToLower() != ("InspectorComments").ToLower()
                                 && oProperty.Name.ToLower() != ("ReviewerComments").ToLower() &&
                                oProperty.Name.ToLower() != ("SubmittedOn").ToLower() && oProperty.Name.ToLower() != ("ReviewdOn").ToLower() &&
                                oProperty.Name.ToLower() != ("FurnaceOn").ToLower() && oProperty.Name.ToLower() != ("InspectOn").ToLower())
                                {
                                    var oOldValue = oProperty.GetValue(prev, null);
                                    var oNewValue = cur.GetType().GetProperty(oProperty.Name).GetValue(cur, null);

                                    // this will handle the scenario where either value is null
                                    if (!object.Equals(oOldValue, oNewValue))
                                    {
                                        // Handle the display values when the underlying value is null
                                        var sOldValue = oOldValue == null ? "" : oOldValue.ToString();
                                        var sNewValue = oNewValue == null ? "" : oNewValue.ToString();
                                        if (sOldValue.ToLower() == "false")
                                        { sOldValue = "No"; }
                                        if (sOldValue.ToLower() == "true")
                                        { sOldValue = "Yes"; }
                                        if (sNewValue.ToLower() == "false")
                                        { sNewValue = "No"; }
                                        if (sNewValue.ToLower() == "true")
                                        { sNewValue = "Yes"; }

                                        listtd.Add("<td>" + oProperty.Name + "</td><td>" + sOldValue + "</td><td>" + sNewValue + "</td>");
                                    }
                                }
                            }
                            #endregion

                            #region lines
                            var htrlinescurlog = db.HTR002_Log.Where(o => o.RefId == cur.Id).OrderByDescending(o => o.LineId).ToList();
                            var htrlinesPrevlog = db.HTR002_Log.Where(o => o.RefId == prev.Id).OrderByDescending(o => o.LineId).ToList();

                            listlinetd = new List<string>();
                            foreach (var curLine in htrlinescurlog)
                            {
                                IsNotExists = true;
                                foreach (var prevLine in htrlinesPrevlog)
                                {
                                    seam = curLine.SeamNo;

                                    #region existing entry result
                                    if (prevLine.LineId == curLine.LineId)
                                    {
                                        IsNotExists = false;

                                        var oTypeline = prevLine.GetType();
                                        foreach (var oProperty in oTypeline.GetProperties())
                                        {
                                            if (oProperty.Name.ToLower() != ("id").ToLower() && oProperty.Name.ToLower() != ("RevNo").ToLower() && oProperty.Name.ToLower() != ("RefId").ToLower() &&
                                            oProperty.Name.ToLower() != ("LineId").ToLower() && oProperty.Name.ToLower() != ("HeaderId").ToLower()
                                            && oProperty.Name.ToLower() != ("EditedBy").ToLower() && oProperty.Name.ToLower() != ("EditedOn").ToLower() &&
                                            oProperty.Name.ToLower() != ("CreatedBy").ToLower() && oProperty.Name.ToLower() != ("CreatedOn").ToLower())
                                            {
                                                var oOldValue = oProperty.GetValue(prevLine, null);
                                                var oNewValue = curLine.GetType().GetProperty(oProperty.Name).GetValue(curLine, null);

                                                // this will handle the scenario where either value is null
                                                if (!object.Equals(oOldValue, oNewValue))
                                                {
                                                    // Handle the display values when the underlying value is null

                                                    var sOldValue = oOldValue == null ? "" : oOldValue.ToString();
                                                    var sNewValue = oNewValue == null ? "" : oNewValue.ToString();

                                                    listlinetd.Add("<td>" + oProperty.Name + "</td><td>" + sOldValue + "</td><td>" + sNewValue + "</td>");
                                                }
                                            }

                                        }

                                        break;
                                    }
                                    #endregion
                                }
                                if (IsNotExists)
                                {
                                    listlinetd.Add("<td class='danger'>" + "Seam No (Deleted)" + "</td><td>" + "--" + "</td><td>" + seam + "</td>");

                                    #region new entry (all deatils)
                                    //   var oTypeline = curLine.GetType();
                                    //foreach (var oProperty in oTypeline.GetProperties())
                                    //{
                                    //    if (oProperty.Name.ToLower() != ("id").ToLower() && oProperty.Name.ToLower() != ("RefId").ToLower() &&
                                    //    oProperty.Name.ToLower() != ("LineId").ToLower() && oProperty.Name.ToLower() != ("HeaderId").ToLower()
                                    //    && oProperty.Name.ToLower() != ("EditedBy").ToLower() && oProperty.Name.ToLower() != ("EditedOn").ToLower() &&
                                    //    oProperty.Name.ToLower() != ("CreatedBy").ToLower() && oProperty.Name.ToLower() != ("CreatedOn").ToLower())
                                    //    {
                                    //        var oOldValue = oProperty.GetValue(curLine, null);

                                    //        // this will handle the scenario where either value is null
                                    //        // Handle the display values when the underlying value is null
                                    //        var sOldValue = oOldValue == null ? "null" : oOldValue.ToString();
                                    //        listlinetd.Add("<td>" + cur.EditedBy + "</td><td>" + cur.EditedOn + "</td><td>HTR Component</td><td>" + oProperty.Name + "</td><td>" + sOldValue + "</td><td>" + "--" + "</td>");
                                    //        //str.Append("<tr><td>R" + CurrentRevNo.ToString() + "</td><td>Modified</td><td>" + cur.SeamNo.ToString() + "</td><td>" + oProperty.Name + " was: " + sOldValue + "; is: " + sNewValue + "</td></tr>");
                                    //    }
                                    //}
                                    #endregion
                                }
                            }
                            #region Delete From Previous Revision
                            foreach (var prevLine in htrlinesPrevlog)
                            {
                                IsNewEntry = true;
                                foreach (var cLine in htrlinescurlog)
                                {
                                    if (prevLine.LineId == cLine.LineId)
                                    {
                                        IsNewEntry = false;
                                        break;
                                    }
                                }
                                if (IsNewEntry)
                                {
                                    listlinetd.Add("<td class='warning'>" + "Seam No (Added)" + "</td><td>" + prevLine.SeamNo + "</td><td>" + "--" + "</td>");
                                    #region deleted  entry (all deatils)
                                    //var oTypeline = prevLine.GetType();
                                    //foreach (var oProperty in oTypeline.GetProperties())
                                    //{
                                    //    if (oProperty.Name.ToLower() != ("id").ToLower() && oProperty.Name.ToLower() != ("RefId").ToLower() &&
                                    //    oProperty.Name.ToLower() != ("LineId").ToLower() && oProperty.Name.ToLower() != ("HeaderId").ToLower()
                                    //    && oProperty.Name.ToLower() != ("EditedBy").ToLower() && oProperty.Name.ToLower() != ("EditedOn").ToLower() &&
                                    //    oProperty.Name.ToLower() != ("CreatedBy").ToLower() && oProperty.Name.ToLower() != ("CreatedOn").ToLower())
                                    //    {
                                    //        var oOldValue = oProperty.GetValue(prevLine, null);

                                    //        // this will handle the scenario where either value is null
                                    //        // Handle the display values when the underlying value is null
                                    //        var sOldValue = oOldValue == null ? "null" : oOldValue.ToString();
                                    //        listlinetd.Add("<td>" + cur.EditedBy + "</td><td>" + cur.EditedOn + "</td><td>HTR Component</td><td>" + oProperty.Name + "</td><td>" + "Deleted" + "</td>><td>" + sOldValue + "</td>");
                                    //        //str.Append("<tr><td>R" + CurrentRevNo.ToString() + "</td><td>Modified</td><td>" + cur.SeamNo.ToString() + "</td><td>" + oProperty.Name + " was: " + sOldValue + "; is: " + sNewValue + "</td></tr>");
                                    //    }
                                    //}
                                    #endregion
                                }
                            }
                            #endregion


                            if (listtd.Count > 0 || listlinetd.Count > 0)
                            {
                                var hdrExist = false;
                                StringBuilder strtr = new StringBuilder();
                                var date = cur.EditedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);

                                for (int k = 0; k < listtd.Count; k++)
                                {
                                    string datalist = listtd[k];
                                    if (k == 0)
                                    {
                                        hdrExist = true;
                                        strtr.Append("<tr><td rowspan='" + (listtd.Count + listlinetd.Count).ToString() + "'>" + Manager.GetPsidandDescription(cur.EditedBy) + "</td><td rowspan='" + (listtd.Count + listlinetd.Count).ToString() + "'>" + date + "</td>");
                                        strtr.Append("<td  rowspan='" + (listtd.Count.ToString()) + "'>HTR Document</td>" + datalist + "</tr>");
                                    }
                                    else
                                    {
                                        strtr.Append("<tr>" + datalist + "</tr>");
                                    }
                                }

                                for (int k = 0; k < listlinetd.Count; k++)
                                {
                                    string datalist = listlinetd[k];
                                    if (k == 0)
                                    {
                                        if (!hdrExist)
                                        {
                                            strtr.Append("<tr><td rowspan='" + (listtd.Count + listlinetd.Count).ToString() + "'>" + cur.EditedBy + "</td><td rowspan='" + (listtd.Count + listlinetd.Count).ToString() + "'>" + date + "</td>");
                                            strtr.Append("<td  rowspan='" + (listlinetd.Count.ToString()) + "'>HTR Component</td>" + datalist + "</tr>");
                                        }
                                        else
                                        {
                                            strtr.Append("<tr><td  rowspan='" + (listlinetd.Count.ToString()) + "'>HTR Component</td>" + datalist + "</tr>");
                                        }
                                    }
                                    else
                                    {
                                        strtr.Append("<tr>" + datalist + "</tr>");
                                    }
                                }


                                listtr.Add(strtr.ToString());
                            }
                            #endregion
                        }
                        if (listtr.Count > 0)
                        {
                            foreach (var item in listtr)
                            {
                                str.Append(item);
                            }
                        }
                        else
                        {
                            //for no change in database
                            str.Append("<tr><td  colspan='6'>Changes not Available</td></tr>");
                        }

                    }
                    else
                    {
                        //for only one record in Log table
                        str.Append("<tr><td  colspan='6'>No Data Available</td></tr>");
                    }


                }
                else
                {
                    //if new/ no record in log table
                    str.Append("<tr><td  colspan='6'>No Data Available</td></tr>");
                }

                str.Append("</tbody></table>");
            }
            catch (Exception)
            {
                throw;
            }
            return str.ToString();
        }

        //pop up
        [HttpPost]
        public ActionResult getChangeSet(int Id) //
        {
            ViewBag.header = Id;
            return PartialView("~/Areas/HTR/Views/Shared/_HTRChangeSet.cshtml");
        }


        #endregion

        #region Line / HTR Components
        //editable grid for HTR Components/Line
        [HttpPost]
        public ActionResult LoadLinesFormPartial(int HeaderId)
        {
            HTR001 objHTR001 = new HTR001();
            ViewBag.EditFlag = false;
            objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objHTR001 != null)
            {

                if (objHTR001.CreatedBy.Trim() == objClsLoginInfo.UserName && objHTR001.Status == clsImplementationEnum.HTRStatus.Draft.GetStringValue())
                {
                    ViewBag.EditFlag = true;
                }
                //objHTR002.HeaderId = objHTR001.HeaderId;
            }
            else
            {
                objHTR001.HeaderId = HeaderId;
            }
            return PartialView("_LoadLineGridPartial", objHTR001);
        }

        //editable datatable for Line grid
        [HttpPost]
        public JsonResult LoadHTRLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                int headerid = Convert.ToInt32(param.CTQHeaderId);
                strWhere = "1=1 and a.HeaderId =" + headerid;
                string[] columnName = { "SeamNo", "PartNo", "HeatNo", "DrawingNo", "DrawingRev", "HTPReference", "PTCMTCPQR", "Description", "MaterialSpec", "SizeThickness", "Quantity", "TotalWeight", "InspectionAgency", "MaterialFrom", "MaterialTo", "ICSBy", "ProjectNumber" };

                HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                string buCode = db.COM001.Where(i => i.t_cprj == objHTR001.Project).FirstOrDefault().t_entu;
                var InspectionAgency = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                var items = (from li in InspectionAgency
                             select new SelectItemList { id = li.Code, text = li.CategoryDescription }).ToList();

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                bool ISEditflag = param.Flag;
                var lstResult = db.SP_HTR_LINE_GETDETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                        "",
                                        Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                        Helper.GenerateHidden(newRecordId, "LineId", param.CTQHeaderId),
                                        Helper.GenerateTextbox(newRecordId, "SeamNo",   "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "PartNo",   "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "HeatNo",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "DrawingNo",   "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "DrawingRev",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "HTPReference",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "PTCMTCPQR",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "Description",   "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "MaterialSpec",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "SizeThickness", "", "", false, "", "50"),
                                        Helper.GenerateNumericTextbox(newRecordId, "Quantity",  "", "", false, "", "10"),
                                        Helper.GenerateNumericTextbox(newRecordId, "TotalWeight",  "", "", false, "", "10"),
                                        MultiSelectDropdown(items,newRecordId,"InspectionAgency",false,"","","InspectionAgency"),
                                        Helper.GenerateTextbox(newRecordId, "MaterialFrom",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "MaterialTo",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "ICSBy",  "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId,"ProjectNumber" ,"", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "Revision",   "", "", true, "", "50"),
                                       // GenerateFileUpload(newRecordId, "UploadDoc",   "", "", true, "", "50"),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewLine();" ),
                                    };
                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ROW_NO),
                            Convert.ToString(uc.LineId),
                            Convert.ToString(uc.HeaderId),
                             //Convert.ToString("R"+uc.RevNo.ToString()),
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "SeamNo",   Convert.ToString(uc.SeamNo), "UpdateData(this,"+ uc.LineId +",true )", (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"):uc.SeamNo,
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "PartNo",   Convert.ToString(uc.PartNo), "UpdateData(this,"+ uc.LineId +",true)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"):uc.PartNo,
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "HeatNo",   Convert.ToString(uc.HeatNo), "UpdateData(this,"+ uc.LineId  +",true)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"):uc.HeatNo,
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "DrawingNo",   Convert.ToString(uc.DrawingNo), "UpdateData(this,"+ uc.LineId +",true)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"): Convert.ToString(uc.DrawingNo),
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "DrawingRev",   Convert.ToString(uc.DrawingRev), "UpdateData(this,"+ uc.LineId +",true)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"): Convert.ToString(uc.DrawingRev),
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "HTPReference",   Convert.ToString(uc.HTPReference), "UpdateData(this,"+ uc.LineId +",true)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"): Convert.ToString(uc.HTPReference),
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "PTCMTCPQR",   Convert.ToString(uc.PTCMTCPQR), "UpdateData(this,"+ uc.LineId +",true)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"): Convert.ToString(uc.PTCMTCPQR),
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "Description",   Convert.ToString(uc.Description), "UpdateData(this,"+ uc.LineId +",true)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"): Convert.ToString(uc.Description),
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "MaterialSpec",   Convert.ToString(uc.MaterialSpec), "UpdateData(this,"+ uc.LineId +",true)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"): Convert.ToString(uc.MaterialSpec),
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "SizeThickness",   Convert.ToString(uc.SizeThickness), "UpdateData(this,"+ uc.LineId +",true)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"): Convert.ToString(uc.SizeThickness),
                          ISEditflag ? Helper.GenerateNumericTextbox(uc.LineId, "Quantity",   Convert.ToString(uc.Quantity), "UpdateData(this,"+ uc.LineId +",true)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "10"): Convert.ToString(uc.Quantity),
                          ISEditflag ? Helper.GenerateNumericTextbox(uc.LineId, "TotalWeight",   Convert.ToString(uc.TotalWeight), "UpdateData(this,"+ uc.LineId +",true)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "10"): Convert.ToString(uc.TotalWeight),
                          //Helper.MultiSelectDropdown(items,uc.InspectionAgency,false,"UpdateData(this,'"+Convert.ToString(uc.LineId)+"')","InspectionAgency"),: Convert.ToString(uc.InspectionAgency) 
                          ISEditflag ? MultiSelectDropdown(items,uc.LineId,uc.InspectionAgency, (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"UpdateData(this,'"+ uc.LineId  +"',true);","","InspectionAgency") : uc.InspectionAgency,
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "MaterialFrom",   Convert.ToString(uc.MaterialFrom), "UpdateData(this,"+ uc.LineId+",true)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"): Convert.ToString(uc.MaterialFrom),
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "MaterialTo",   Convert.ToString(uc.MaterialTo), "UpdateData(this,"+ uc.LineId+",true)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"): Convert.ToString(uc.MaterialTo),
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "ICSBy",   Convert.ToString(uc.ICSBy), "UpdateData(this,"+ uc.LineId+",false)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"): Convert.ToString(uc.ICSBy) ,
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId,"ProjectNumber"  ,Convert.ToString(uc.ProjectNumber), "UpdateData(this,"+ uc.LineId+",false)",  (string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.DRAFT.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(param.MTStatus,clsImplementationEnum.CommonStatus.Returned.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true), "", "50"): Convert.ToString(uc.ProjectNumber),
                          ISEditflag ? GenerateTextboxOnChange(uc.LineId, "Revision",   Convert.ToString("R"+uc.RevNo.ToString()), "UpdateData(this,"+ uc.LineId +",false )",  true, "", "50"):"R"+uc.RevNo.ToString(),
                          //ISEditflag ? GenerateFileUploadOnChange(uc.LineId, "m-dropzone-three",   "","",  true, "", "50"):"",
                            
                          //"<center><i  data-modal='' id='btnDelete' name='btnAction' style='cursor:pointer;' Title='Delete Record' class='fa fa-trash-o' onClick='DeleteRecord(" + uc.LineId+ ")'></i></center>"                                                                                                                                                                                                                                                         
                            "<center>"+ HTMLActionString(uc.LineId,param.MTStatus, "Delete", "Delete Record", "fa fa-trash-o", "DeleteRecord(" + uc.LineId + ");",ISEditflag)+" "+
                            HTMLActionString(uc.LineId,param.MTStatus, "Revise", "Revise Record", "fa fa-repeat", "SetRevisionNo(" + uc.LineId + ");",true)+" "+
                            //"<div id='UploadFile' onclick='ShowPopUp("+uc.LineId+")'><i class='fa fa-upload'></i></div>"+"</center>",   
                            "<i class='fa fa-upload' id='UploadFile' onclick='ShowPopUp("+uc.LineId+")'></i>"+"</center>",                                                                                                                                                                                                                                                                                           
                            
                            //Convert.ToString(uc.LineId) //, "Delete", "Delete Rocord", "fa fa-trash-o", "DeleteQualityID("+ uc.LineId +");")
                            //Convert.ToString(uc.LineId),
            }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region CRUD Operations for  Lines/ HTR Components details
        //insert new line
        [HttpPost]
        public ActionResult InsertNewLine(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int newRowIndex = 0;
            int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
            string tableName = string.Empty;
            try
            {
                HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                HTR002 objHTR002 = new HTR002();
                objHTR002.HeaderId = objHTR001.HeaderId;
                objHTR002.Project = objHTR001.Project;
                objHTR002.HTRNo = objHTR001.HTRNo;
                objHTR002.RevNo = objHTR001.RevNo;
                objHTR002.HTRLineNo = fc["HTRLineNo" + newRowIndex];
                objHTR002.SeamNo = fc["SeamNo" + newRowIndex];
                objHTR002.PartNo = fc["PartNo" + newRowIndex];
                objHTR002.HeatNo = fc["HeatNo" + newRowIndex];
                objHTR002.DrawingNo = fc["DrawingNo" + newRowIndex];
                objHTR002.DrawingRev = fc["DrawingRev" + newRowIndex];
                objHTR002.HTPReference = fc["HTPReference" + newRowIndex];
                objHTR002.PTCMTCPQR = fc["PTCMTCPQR" + newRowIndex];
                objHTR002.Description = fc["Description" + newRowIndex];
                objHTR002.MaterialSpec = fc["MaterialSpec" + newRowIndex];
                objHTR002.SizeThickness = fc["SizeThickness" + newRowIndex];
                objHTR002.Quantity = fc["Quantity" + newRowIndex];
                objHTR002.TotalWeight = fc["TotalWeight" + newRowIndex];
                objHTR002.InspectionAgency = fc["ddlmultiple" + newRowIndex];
                objHTR002.MaterialFrom = fc["MaterialFrom" + newRowIndex];
                objHTR002.MaterialTo = fc["MaterialTo" + newRowIndex];
                objHTR002.ICSBy = fc["ICSBy" + newRowIndex];
                objHTR002.ProjectNumber = fc["ProjectNumber" + newRowIndex];
                if (objHTR001.Status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                {
                    objHTR001.RevNo = Convert.ToInt32(objHTR001.RevNo) + 1;
                    objHTR001.FurnaceComments = null;
                    objHTR001.InspectorComments = null;
                    objHTR001.ReviewerComments = null;
                    objHTR001.ReviewdOn = null;
                    objHTR001.FurnaceOn = null;
                    objHTR001.InspectOn = null;
                    objHTR001.SubmittedBy = null;
                    objHTR001.SubmittedOn = null;
                    objHTR001.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                    objHTR001.EditedBy = objClsLoginInfo.UserName;
                    objHTR001.EditedOn = DateTime.Now;
                }
                objHTR002.CreatedBy = objClsLoginInfo.UserName;
                objHTR002.CreatedOn = DateTime.Now;
                db.HTR002.Add(objHTR002);
                db.SaveChanges();
                objResponseMsg.Revision = Convert.ToInt32(objHTR001.RevNo);
                objResponseMsg.Status = objHTR001.Status;
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Insert;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //update new line
        [HttpPost]
        public ActionResult UpdateDetails(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string tableName = "HTR002";
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, tableName, objClsLoginInfo.UserName);
                    //ReviseData(headerId);
                    HTR002 objHTR002 = new HTR002();
                    objHTR002 = db.HTR002.Where(x => x.LineId == lineId).FirstOrDefault();
                    HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == objHTR002.HeaderId).FirstOrDefault();
                    if (objHTR001.Status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                    {
                        objHTR001.RevNo = Convert.ToInt32(objHTR001.RevNo) + 1;
                        objHTR001.FurnaceComments = null;
                        objHTR001.InspectorComments = null;
                        objHTR001.ReviewerComments = null;
                        objHTR001.ReviewdOn = null;
                        objHTR001.FurnaceOn = null;
                        objHTR001.InspectOn = null;
                        objHTR001.SubmittedBy = null;
                        objHTR001.SubmittedOn = null;
                        objHTR001.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                        objHTR001.EditedBy = objClsLoginInfo.UserName;
                        objHTR001.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    objResponseMsg.Revision = Convert.ToInt32(objHTR001.RevNo);
                    objResponseMsg.Status = objHTR001.Status;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //set Revise No from grid
        [HttpPost]
        public ActionResult SetReviseNo(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                HTR002 objHTR002 = db.HTR002.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objHTR002 != null)
                {
                    int RevNo = objHTR002.RevNo ?? 0;
                    objHTR002.RevNo = RevNo + 1;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "REVNo Revised successfully.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for REVNo.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //delete line from grid
        [HttpPost]
        public ActionResult DeleteLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                HTR002 objHTR002 = db.HTR002.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objHTR002 != null)
                {
                    db.HTR002.Remove(objHTR002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Promote header
        [HttpPost]
        public ActionResult SendForPromote(int strHeaderId, string strRemark)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string strStatus = string.Empty;
                HTR001 objHTR001 = db.HTR001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objHTR001 != null)
                {
                    if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue())
                    {
                        strStatus = clsImplementationEnum.HTRStatus.InReview.GetStringValue();
                    }
                    if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.JobComponentNuclear.GetStringValue())
                    {
                        strStatus = clsImplementationEnum.HTRStatus.InReview.GetStringValue();
                    }
                    if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.PQR.GetStringValue())
                    {
                        strStatus = clsImplementationEnum.HTRStatus.Release.GetStringValue();
                    }
                    if (objHTR001.HTRType == clsImplementationEnum.TypeOfHTR.PTC.GetStringValue())
                    {
                        strStatus = clsImplementationEnum.HTRStatus.InReview.GetStringValue();
                    }
                    db.SP_HTR_PROMOTE_DEMOTE(strHeaderId, strRemark, strStatus, objClsLoginInfo.UserName, "INITIATOR");


                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = string.Format(clsImplementationMessage.CommonMessages.Promote, objHTR001.HTRNo);
                }
                else
                {
                    objResponseMsg.ActionKey = true;
                    objResponseMsg.ActionValue = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Cancel/Delete header
        //  Cancel/Delete header
        [HttpPost]
        public ActionResult CancelHTR(int strHeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strStatus = string.Empty;
                HTR001 objHTR001 = db.HTR001.Where(u => u.HeaderId == strHeaderId).FirstOrDefault();
                string strReleaseStatus = clsImplementationEnum.HTRStatus.Release.GetStringValue();
                string strSuperseeded = clsImplementationEnum.HTRStatus.Superseded.GetStringValue();
                HTR001_Log objSourceHTR001 = db.HTR001_Log.Where(u => u.HeaderId == strHeaderId && (u.Status == strReleaseStatus || u.Status == strSuperseeded)).OrderByDescending(a => a.RevNo).FirstOrDefault();

                if (objHTR001 != null)
                {
                    if (string.Equals(objHTR001.Status, clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    {
                        //New Requiement: Observation No = 13502
                        if (objHTR001.RevNo > 0)
                        {
                            //objSourceHTR001.Comments = strRemark;
                            UpdateRevDetails(objSourceHTR001, objHTR001, true, objHTR001.RevNo); // update header from Log table(fetch previous records)
                        }
                        else
                        {  //hard delete header if rev is 0
                            HTR003 objHTR003 = db.HTR003.Where(u => u.HeaderId == strHeaderId).FirstOrDefault();
                            if (objHTR003 != null)
                            {
                                db.HTR003.Remove(objHTR003);
                                db.SaveChanges();
                            }
                            if (objHTR001.HTR002 != null)
                            {
                                db.HTR002.RemoveRange(objHTR001.HTR002);
                                db.SaveChanges();
                            }
                            db.HTR001.Remove(objHTR001);
                            db.SaveChanges();
                            //if RevNo =0 then update header status =Cancel and add cancel/Delete remarks 
                            //strStatus = clsImplementationEnum.HTRStatus.Cancel.GetStringValue();
                            //db.SP_HTR_PROMOTE_DEMOTE(strHeaderId, strRemark, strStatus, objClsLoginInfo.UserName, "INITIATOR");
                        }
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details has been deleted successfully";
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details not available.";
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //If HTR REVNo > 0 and status is in Draft then update HTR001 from HTR001_Log(fetch last rev (approved record)
        public clsHelper.ResponseMsg UpdateRevDetails(HTR001_Log objSrcHTR001, HTR001 objDesHTR001, bool IsEdited, int? latestRevNo)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();

            try
            {
                if (objSrcHTR001.Status == clsImplementationEnum.HTRStatus.Superseded.GetStringValue())
                {
                    objSrcHTR001.Status = clsImplementationEnum.HTRStatus.Release.GetStringValue();
                }
                objDesHTR001.Department = objSrcHTR001.Department;
                objDesHTR001.RevNo = objSrcHTR001.RevNo;
                //objDesHTR001.DocNo = objSrcHTR001.; ;
                objDesHTR001.HTRType = objSrcHTR001.HTRType;
                objDesHTR001.DONo = objSrcHTR001.DONo;
                objDesHTR001.PoNo = objSrcHTR001.PoNo;
                objDesHTR001.IdenticalProject = objSrcHTR001.IdenticalProject;

                objDesHTR001.ConcertoMSP = objSrcHTR001.ConcertoMSP;
                objDesHTR001.Treatment = objSrcHTR001.Treatment;
                objDesHTR001.HTRReferenceNo = objSrcHTR001.HTRReferenceNo;
                objDesHTR001.Furnace = objSrcHTR001.Furnace;
                objDesHTR001.HTRDescription = objSrcHTR001.HTRDescription;
                objDesHTR001.FurnaceCommentsRequired = objSrcHTR001.FurnaceCommentsRequired;
                objDesHTR001.FurnaceSupervisor = objSrcHTR001.FurnaceSupervisor;
                objDesHTR001.InspectionCommentsRequired = objSrcHTR001.InspectionCommentsRequired;
                objDesHTR001.Inspector = objSrcHTR001.Inspector;
                objDesHTR001.MET = objSrcHTR001.MET;
                objDesHTR001.MinLoadingTemp = objSrcHTR001.MinLoadingTemp;
                objDesHTR001.MaxLoadingTemp = objSrcHTR001.MaxLoadingTemp;
                objDesHTR001.MinRateOfHeating = objSrcHTR001.MinRateOfHeating;
                objDesHTR001.MaxRateOfHeating = objSrcHTR001.MaxRateOfHeating;
                objDesHTR001.MinSoakingTemperature = objSrcHTR001.MinSoakingTemperature;
                objDesHTR001.MaxSoakingTemperature = objSrcHTR001.MaxSoakingTemperature;
                objDesHTR001.SoakingTimeHH = objSrcHTR001.SoakingTimeHH;
                objDesHTR001.SoakingTimeMM = objSrcHTR001.SoakingTimeMM;
                objDesHTR001.SoakingTimeTolerance = objSrcHTR001.SoakingTimeTolerance;
                objDesHTR001.MinRateOfCooling = objSrcHTR001.MinRateOfCooling;
                objDesHTR001.MaxRateOfCooling = objSrcHTR001.MaxRateOfCooling;
                objDesHTR001.MinUnloadingTemp = objSrcHTR001.MinUnloadingTemp;
                objDesHTR001.MaxUnloadingTemp = objSrcHTR001.MaxUnloadingTemp;
                objDesHTR001.CoolPart = objSrcHTR001.CoolPart;
                objDesHTR001.CoolBy = objSrcHTR001.CoolBy;
                objDesHTR001.MaxDelay = objSrcHTR001.MaxDelay;
                objDesHTR001.VolTempOf = objSrcHTR001.VolTempOf;
                objDesHTR001.TempOfLiquidAfterQuenching = objSrcHTR001.TempOfLiquidAfterQuenching;
                objDesHTR001.MinVol = objSrcHTR001.MinVol;
                objDesHTR001.HeatingThermocouplesVariation = objSrcHTR001.HeatingThermocouplesVariation;
                objDesHTR001.CoolingThermocouplesVariation = objSrcHTR001.CoolingThermocouplesVariation;
                objDesHTR001.StandardProcedure = objSrcHTR001.StandardProcedure;
                objDesHTR001.FurnaceBedDistance = objSrcHTR001.FurnaceBedDistance;
                objDesHTR001.WPSNo = objSrcHTR001.WPSNo;
                objDesHTR001.Comments = objSrcHTR001.Comments;
                objDesHTR001.chkl1 = objSrcHTR001.chkl1;
                objDesHTR001.chkl2 = objSrcHTR001.chkl2;
                objDesHTR001.chkl3 = objSrcHTR001.chkl3;
                objDesHTR001.chkl4 = objSrcHTR001.chkl4;
                objDesHTR001.chkl5 = objSrcHTR001.chkl5;
                objDesHTR001.chkl6 = objSrcHTR001.chkl6;
                objDesHTR001.chkl7 = objSrcHTR001.chkl7;
                objDesHTR001.chkl8 = objSrcHTR001.chkl8;
                objDesHTR001.chkl9 = objSrcHTR001.chkl9;
                objDesHTR001.chkl10 = objSrcHTR001.chkl10;
                objDesHTR001.chkl11 = objSrcHTR001.chkl11;
                objDesHTR001.chkl12 = objSrcHTR001.chkl12;
                objDesHTR001.chkl13 = objSrcHTR001.chkl13;
                objDesHTR001.chkl14 = objSrcHTR001.chkl14;
                objDesHTR001.chkl15 = objSrcHTR001.chkl15;
                objDesHTR001.chkl16 = objSrcHTR001.chkl16;
                objDesHTR001.chkl17 = objSrcHTR001.chkl17;
                objDesHTR001.chkl18 = objSrcHTR001.chkl18;
                objDesHTR001.chkl19 = objSrcHTR001.chkl19;
                objDesHTR001.chkl20 = objSrcHTR001.chkl20;
                objDesHTR001.chkl21 = objSrcHTR001.chkl21;
                objDesHTR001.chkl22 = objSrcHTR001.chkl22;
                objDesHTR001.chkl23 = objSrcHTR001.chkl23;
                objDesHTR001.chkl24 = objSrcHTR001.chkl24;
                objDesHTR001.Status = clsImplementationEnum.HTRStatus.Release.GetStringValue();
                objDesHTR001.FurnaceComments = objSrcHTR001.FurnaceComments;
                objDesHTR001.InspectorComments = objSrcHTR001.InspectorComments;
                objDesHTR001.ReviewerComments = objSrcHTR001.ReviewerComments;
                objDesHTR001.ReviewdOn = objSrcHTR001.ReviewdOn;
                objDesHTR001.FurnaceOn = objSrcHTR001.FurnaceOn;
                objDesHTR001.InspectOn = objSrcHTR001.InspectOn;

                objDesHTR001.EditedBy = objSrcHTR001.EditedBy;
                objDesHTR001.EditedOn = objSrcHTR001.EditedOn;
                objDesHTR001.CreatedOn = objSrcHTR001.CreatedOn;
                objDesHTR001.CreatedBy = objSrcHTR001.CreatedBy;
                List<HTR003> lstDesHTR003 = db.HTR003.Where(x => x.HeaderId == objDesHTR001.HeaderId).ToList();
                if (lstDesHTR003 != null && lstDesHTR003.Count > 0)
                {
                    db.HTR003.RemoveRange(lstDesHTR003);
                }
                List<HTR002> lstDesHTR002 = db.HTR002.Where(x => x.HeaderId == objDesHTR001.HeaderId).ToList();
                if (lstDesHTR002 != null && lstDesHTR002.Count > 0)
                {
                    db.HTR002.RemoveRange(lstDesHTR002);
                }
                db.SaveChanges();

                List<HTR002_Log> lstSrcHTR002 = db.HTR002_Log.Where(x => x.RefId == objSrcHTR001.Id).ToList();
                db.HTR002.AddRange(
                                    lstSrcHTR002.Select(x =>
                                    new HTR002
                                    {
                                        HeaderId = objDesHTR001.HeaderId,
                                        Project = objDesHTR001.Project,
                                        HTRNo = objDesHTR001.HTRNo,
                                        RevNo = Convert.ToInt32(objDesHTR001.RevNo),
                                        HTRLineNo = x.HTRLineNo,
                                        SeamNo = x.SeamNo,
                                        PartNo = x.PartNo,
                                        HeatNo = x.HeatNo,
                                        DrawingNo = x.DrawingNo,
                                        DrawingRev = x.DrawingRev,
                                        HTPReference = x.HTPReference,
                                        PTCMTCPQR = x.PTCMTCPQR,
                                        Description = x.Description,
                                        MaterialSpec = x.MaterialSpec,
                                        SizeThickness = x.SizeThickness,
                                        Quantity = x.Quantity,
                                        TotalWeight = x.TotalWeight,
                                        InspectionAgency = x.InspectionAgency,
                                        MaterialFrom = x.MaterialFrom,
                                        MaterialTo = x.MaterialTo,
                                        ICSBy = x.ICSBy,
                                        EditedBy = x.EditedBy,
                                        EditedOn = x.EditedOn,
                                        CreatedBy = x.CreatedBy,
                                        CreatedOn = x.CreatedOn
                                    })
                                  );
                //List<HTR003> lstSrcHTR003 = db.HTR003.Where(x => x.HeaderId == objSrcHTR001.HeaderId).ToList();
                //db.HTR003.AddRange(
                //                    lstSrcHTR003.Select(x =>
                //                    new HTR003
                //                    {
                //                        HeaderId = objSrcHTR001.HeaderId,
                //                        Project = objSrcHTR001.Project,
                //                        HTRNo = objSrcHTR001.HTRNo,
                //                        RevNo = objSrcHTR001.RevNo,
                //                        LinkedProject = x.LinkedProject,
                //                        CreatedBy = objClsLoginInfo.UserName,
                //                        CreatedOn = DateTime.Now,
                //                    })
                //                    );
                db.SaveChanges();
                List<int> logid = db.HTR001_Log.Where(x => x.HeaderId == objDesHTR001.HeaderId && x.RevNo == latestRevNo).Select(x => x.Id).ToList();
                List<HTR003_Log> lstDesHTR003_log = db.HTR003_Log.Where(x => logid.Contains(x.RefId) && x.HeaderId == objDesHTR001.HeaderId).ToList();
                if (lstDesHTR003_log != null && lstDesHTR003_log.Count > 0)
                {
                    db.HTR003_Log.RemoveRange(lstDesHTR003_log);
                }
                List<HTR002_Log> lstDesHTR002_Log = db.HTR002_Log.Where(x => logid.Contains(x.RefId) && x.HeaderId == objDesHTR001.HeaderId).ToList();
                if (lstDesHTR002_Log != null && lstDesHTR002_Log.Count > 0)
                {
                    db.HTR002_Log.RemoveRange(lstDesHTR002_Log);
                }
                List<HTR001_Log> lstDesHTR001_Log = db.HTR001_Log.Where(x => x.HeaderId == objDesHTR001.HeaderId && logid.Contains(x.Id)).ToList();
                if (lstDesHTR001_Log != null && lstDesHTR001_Log.Count > 0)
                {
                    db.HTR001_Log.RemoveRange(lstDesHTR001_Log);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                objResponseMsg.HeaderId = objDesHTR001.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return objResponseMsg;
        }
        #endregion

        #region Maintain Furnace Supervisor with related furnace customization.
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult FurnaceManager()
        {
            ViewBag.lstFurnace = clsImplementationEnum.GetFurnace();
            ViewBag.lstFurnaceSupervisor = Manager.GetApproverList(clsImplementationEnum.UserRoleName.FS3.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, "").Distinct().Select(s => new { id = s.Code, text = s.Name }).ToList();
            return View();
        }

        public ActionResult loadFurnaceManagerDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                string whereCondition = "1=1";
                #region Datatable Sorting 
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string strSortOrder = string.Empty;
                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                string[] columnName = { "Furnace", "FurnaceSupervisor" };
                if (!String.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstHTR = db.SP_HTR_FURNACE_MANAGER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHTR.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstHTR
                           select new[] {
                                Convert.ToString(h.Id),
                                h.Furnace,
                                Helper.MultiSelectDropdown(new List<SelectItemList>(),h.Id,h.FurnaceSupervisor,false,"","","FurnaceSupervisor"),
                                Helper.GenerateGridButton(h.Id, "Delete", "Delete Record", "fa fa-trash", "DeleteRecord("+h.Id+");" )
                            }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    "0",
                                    Helper.GenerateHTMLTextbox(newRecordId,"Furnace","","",false,"",false),
                                    Helper.MultiSelectDropdown(new List<SelectItemList>(),newRecordId,"",false,"","","FurnaceSupervisor"),
                                    Helper.GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveRecord(0);" )
                                };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_HTR_FURNACE_MANAGER(1, 0, "", "Id = " + id).Take(1).ToList();
                    data = (from h in lstResult
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(h.Id),
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(), //h.Furnace,
                                    isReadOnly? h.FurnaceSupervisor : Helper.MultiSelectDropdown(new List<SelectItemList>(),h.Id,h.FurnaceSupervisor,false,"","","FurnaceSupervisor"),
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue()
                        }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveFurnaceManager(HTR004 model)
        {
            HTR004 objHTR004 = null;
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (model.Id > 0)
                {
                    objHTR004 = db.HTR004.FirstOrDefault(x => x.Id == model.Id);
                    objHTR004.FurnaceSupervisor = model.FurnaceSupervisor;
                    objHTR004.EditedBy = objClsLoginInfo.UserName;
                    objHTR004.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    objResponseMsg.HeaderId = objHTR004.Id;
                }
                else
                {
                    if (db.HTR004.Any(f => f.Furnace == model.Furnace))
                    {
                        objResponseMsg.Value = "This Furnace already added!";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    objHTR004 = new HTR004();
                    objHTR004.Furnace = model.Furnace;
                    objHTR004.FurnaceSupervisor = model.FurnaceSupervisor;
                    objHTR004.CreatedBy = objClsLoginInfo.UserName;
                    objHTR004.CreatedOn = DateTime.Now;

                    db.HTR004.Add(objHTR004);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    objResponseMsg.HeaderId = objHTR004.Id;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateFurnaceMasterData(int rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                columnValue = columnValue == null ? null : columnValue.Replace("'", "''");
                db.SP_COMMON_TABLE_UPDATE("HTR004", rowId, "Id", columnName, columnValue, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteFurnaceManager(int Id = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objHTR004 = db.HTR004.FirstOrDefault(f => f.Id == Id);

                if (objHTR004 != null)
                {
                    db.HTR004.Remove(objHTR004);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Deleted Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found or already deleted";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Common Functions
        public class CustomResponceMsg : clsHelper.ResponseMsg
        {
            public string Project;
            public string Customer;
            public int HeaderID;
            public int LineID;
            public string Status;
            public string CDD;
            public int? rev;
        }
        //Start : autocomplete for Location
        [HttpPost]
        public ActionResult GetLocation(string term)
        {
            int code = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
            var lstLocs = getLocByID(code);

            var lstLocations = (from a in lstLocs
                                select new { code = a.t_dimx, desc = a.t_desc }).ToList();
            return Json(lstLocations, JsonRequestBehavior.AllowGet);
        }

        public List<COM002> getLocByID(int LocId)
        {
            var lstLoc = db.COM002.Where(x => x.t_dtyp == LocId && x.t_dimx != "").Distinct().ToList();
            return lstLoc;
        }
        //End

        //Start : generate HTR number
        public string MakeAHTR(string code)
        {
            var lstobjHTR001 = db.HTR001.Where(x => x.Project == code).ToList();
            string HTRNumber = "";
            string AHTRNo = string.Empty;

            if (lstobjHTR001 != null && lstobjHTR001.Count() != 0)
            {
                var HTRNum = (from a in db.HTR001
                              where a.Project.Equals(code, StringComparison.OrdinalIgnoreCase)
                              select a).Max(a => a.DocNo);
                HTRNumber = (HTRNum + 1).ToString();
            }
            else
            {
                HTRNumber = "1";
            }
            AHTRNo = "HTR/" + code + "/" + HTRNumber.PadLeft(3, '0');
            return AHTRNo;
        }
        [HttpPost]
        public ActionResult GetNumber(string code)
        {   //generate HTR number
            if (!string.IsNullOrWhiteSpace(code))
            {

                HTR001 objHTR001 = new HTR001();
                NDE.Controllers.MaintainScanUTTechController.BUWiseDropdown objProjectDataModel = new NDE.Controllers.MaintainScanUTTechController.BUWiseDropdown();

                var lstProject = (from a in db.COM001
                                  where a.t_cprj == code
                                  select new { ProjectDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();

                var HTRNum = MakeAHTR(code);
                objProjectDataModel.TechNo = HTRNum;

                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        //End

        //Start :Editable Grid Components 
        [NonAction]
        public static string GenerateTextboxOnChange(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " spara='" + rowId + "' />";

            return htmlControl;
        }
        [NonAction]
        public static string GenerateFileUpload(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            htmlControl = "<div id='" + inputID + "' " + MaxCharcters + " onclick='ShowPopUp();'> Click To Upload</div>";

            return htmlControl;
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isVisible = true, bool atagrequired = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod : "";

            if (buttonName.ToLower() == "History".ToLower())
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }
            else
            {
                if (string.Equals(status, clsImplementationEnum.CommonStatus.DRAFT.GetStringValue()))
                {
                    if (isVisible)
                    {
                        htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='iconspace " + className + "'" + onClickEvent + " ></i>";
                    }
                    else
                    {
                        htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='disabledicon " + className + "'" + onClickEvent + " ></i>";
                    }
                    if (atagrequired)
                    {
                        htmlControl = "<a>" + htmlControl + "</a>";
                    }
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' Title='" + buttonTooltip + "' class='disabledicon " + className + "'" + onClickEvent + " ></i>";
                }
            }
            return htmlControl;
        }

        public static string MultiSelectDropdown(List<SelectItemList> list, int rowID, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string OnBlurEvent = "", string ColumnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }


                multipleSelect += "<select data-name='ddlmultiple' name='ddlmultiple" + rowID + "' id='ddlmultiple" + rowID + "' data-lineid='" + rowID + "' multiple='multiple'  style='width: 100 % ' colname='" + ColumnName + "' class='form-control multiselect-drodown' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange=" + OnChangeEvent + "" : "") + (OnBlurEvent != string.Empty ? " onblur='" + OnBlurEvent + "'" : "") + " >";
                foreach (var item in list)
                {
                    multipleSelect += "<option value='" + item.id + "' " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }
        //End 

        //Start : Linked Project Multi Select in header
        [HttpPost]
        public JsonResult GetLinkedProjectEdit(int headerId)
        {
            var result = db.HTR003.Where(s => s.HeaderId == headerId).ToList();
            var items = (from li in result
                         select new
                         {
                             id = li.LinkedProject,
                             text = (db.COM001.Where(x => x.t_cprj == li.LinkedProject).Select(x => x.t_cprj + "-" + x.t_dsca).FirstOrDefault())
                         });
            return Json(items, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetLinkedProjectValue(int headerId)
        {
            var result = db.HTR003.Where(s => s.HeaderId == headerId).Select(s => s.LinkedProject).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);

        }

        public JsonResult GetLinkedProjectHistoryValue(int headerId)
        {
            var result = db.HTR003_Log.Where(s => s.RefId == headerId).Select(s => s.LinkedProject).ToList();
            return Json(result, JsonRequestBehavior.AllowGet);

        }
        //End

        //Start : Multi Select for Inspection Agency in Lines
        public JsonResult GetInspectionAgency(string param)
        {
            try
            {
                int HeaderId = Convert.ToInt32(param);
                HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string buCode = db.COM001.Where(i => i.t_cprj == objHTR001.Project).FirstOrDefault().t_entu;
                var InspectionAgency = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();

                var lstInspectionAgency = (from li in InspectionAgency
                                           select new { id = li.Code, text = li.CategoryDescription }).ToList();

                return Json(lstInspectionAgency, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetInspectionAgencyValue(int headerId, int lineId)
        {
            try
            {
                string inspectionAgency = (db.HTR002.Where(x => x.HeaderId == headerId && x.LineId == lineId).Select(x => x.InspectionAgency).FirstOrDefault());
                string[] inspectionAgencys = null;

                if (!string.IsNullOrWhiteSpace(inspectionAgency))
                {
                    inspectionAgencys = inspectionAgency.Split(',');
                }
                return Json(inspectionAgencys, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        //End
        #endregion

        #region Copy data
        [HttpPost]
        public ActionResult GetCopyDetails(string projectid)
        {
            return PartialView("_HTRCopyPartial");
        }
        //Copy details 
        [HttpPost]
        public ActionResult copyDetails(string projCode, int HeaderId, string actionValue, string HtrNo)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string currentUser = objClsLoginInfo.UserName;
                var currentLoc = objClsLoginInfo.Location;

                HTR001 objHTR001 = db.HTR001.Where(x => x.Project == projCode && x.Location == currentLoc && x.HTRNo == HtrNo).FirstOrDefault();
                HTR001 objExistingHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                string docNo = MakeAHTR(projCode);
                if (objHTR001 != null && actionValue == "Edit")
                {
                    if (objHTR001.Location.Trim() == currentLoc.Trim())
                    {
                        if (objHTR001.Status == clsImplementationEnum.CTQStatus.DRAFT.GetStringValue())
                        {
                            objResponseMsg = InsertNewDetails(objExistingHTR001, objHTR001, true);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Document already exists.";
                        }
                    }
                    else
                    {
                        HTR001 objDesHTR001 = new HTR001();
                        objDesHTR001.Project = projCode;
                        objDesHTR001.Location = currentLoc;
                        objDesHTR001.HTRNo = docNo;
                        objResponseMsg = InsertNewDetails(objExistingHTR001, objDesHTR001, false);
                    }
                }
                else
                {
                    HTR001 objDesHTR001 = new HTR001();
                    objDesHTR001.Project = projCode;
                    objDesHTR001.Location = currentLoc;
                    objDesHTR001.HTRNo = docNo;
                    objResponseMsg = InsertNewDetails(objExistingHTR001, objDesHTR001, false);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //Insert /Update for Copied Data
        public clsHelper.ResponseMsg InsertNewDetails(HTR001 objSrcHTR001, HTR001 objDesHTR001, bool IsEdited)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            string user_dept = (from deptr in db.COM003
                                where deptr.t_psno == objClsLoginInfo.UserName && deptr.t_actv == 1
                                select deptr.t_depc).FirstOrDefault();

            var dept = (from a in db.COM002
                        where a.t_dimx == user_dept
                        select new { a.t_dimx }).FirstOrDefault();
            var department = dept.t_dimx;

            try
            {
                string doc = objDesHTR001.HTRNo.Split('/')[2];
                int docNo = Convert.ToInt32(doc);
                objDesHTR001.Department = department;
                objDesHTR001.RevNo = 0;
                objDesHTR001.DocNo = docNo;
                objDesHTR001.HTRType = objSrcHTR001.HTRType;
                objDesHTR001.DONo = objSrcHTR001.DONo;
                objDesHTR001.PoNo = objSrcHTR001.PoNo;
                objDesHTR001.IdenticalProject = objSrcHTR001.IdenticalProject;
                objDesHTR001.ConcertoMSP = objSrcHTR001.ConcertoMSP;
                objDesHTR001.Treatment = objSrcHTR001.Treatment;
                objDesHTR001.HTRReferenceNo = objSrcHTR001.HTRReferenceNo;
                objDesHTR001.Furnace = objSrcHTR001.Furnace;
                objDesHTR001.HTRDescription = objSrcHTR001.HTRDescription;
                objDesHTR001.FurnaceCommentsRequired = objSrcHTR001.FurnaceCommentsRequired;
                objDesHTR001.FurnaceSupervisor = objSrcHTR001.FurnaceSupervisor;
                objDesHTR001.InspectionCommentsRequired = objSrcHTR001.InspectionCommentsRequired;
                objDesHTR001.Inspector = objSrcHTR001.Inspector;
                objDesHTR001.MET = objSrcHTR001.MET;
                objDesHTR001.MinLoadingTemp = objSrcHTR001.MinLoadingTemp;
                objDesHTR001.MaxLoadingTemp = objSrcHTR001.MaxLoadingTemp;
                objDesHTR001.MinRateOfHeating = objSrcHTR001.MinRateOfHeating;
                objDesHTR001.MaxRateOfHeating = objSrcHTR001.MaxRateOfHeating;
                objDesHTR001.MinSoakingTemperature = objSrcHTR001.MinSoakingTemperature;
                objDesHTR001.MaxSoakingTemperature = objSrcHTR001.MaxSoakingTemperature;
                objDesHTR001.SoakingTimeHH = objSrcHTR001.SoakingTimeHH;
                objDesHTR001.SoakingTimeMM = objSrcHTR001.SoakingTimeMM;
                objDesHTR001.SoakingTimeTolerance = objSrcHTR001.SoakingTimeTolerance;
                objDesHTR001.MinRateOfCooling = objSrcHTR001.MinRateOfCooling;
                objDesHTR001.MaxRateOfCooling = objSrcHTR001.MaxRateOfCooling;
                objDesHTR001.MinUnloadingTemp = objSrcHTR001.MinUnloadingTemp;
                objDesHTR001.MaxUnloadingTemp = objSrcHTR001.MaxUnloadingTemp;
                objDesHTR001.CoolPart = objSrcHTR001.CoolPart;
                objDesHTR001.CoolBy = objSrcHTR001.CoolBy;
                objDesHTR001.MaxDelay = objSrcHTR001.MaxDelay;
                objDesHTR001.VolTempOf = objSrcHTR001.VolTempOf;
                objDesHTR001.TempOfLiquidAfterQuenching = objSrcHTR001.TempOfLiquidAfterQuenching;
                objDesHTR001.MinVol = objSrcHTR001.MinVol;
                objDesHTR001.HeatingThermocouplesVariation = objSrcHTR001.HeatingThermocouplesVariation;
                objDesHTR001.CoolingThermocouplesVariation = objSrcHTR001.CoolingThermocouplesVariation;
                objDesHTR001.StandardProcedure = objSrcHTR001.StandardProcedure;
                objDesHTR001.FurnaceBedDistance = objSrcHTR001.FurnaceBedDistance;
                objDesHTR001.WPSNo = objSrcHTR001.WPSNo;
                objDesHTR001.Comments = objSrcHTR001.Comments;
                objDesHTR001.chkl1 = objSrcHTR001.chkl1;
                objDesHTR001.chkl2 = objSrcHTR001.chkl2;
                objDesHTR001.chkl3 = objSrcHTR001.chkl3;
                objDesHTR001.chkl4 = objSrcHTR001.chkl4;
                objDesHTR001.chkl5 = objSrcHTR001.chkl5;
                objDesHTR001.chkl6 = objSrcHTR001.chkl6;
                objDesHTR001.chkl7 = objSrcHTR001.chkl7;
                objDesHTR001.chkl8 = objSrcHTR001.chkl8;
                objDesHTR001.chkl9 = objSrcHTR001.chkl9;
                objDesHTR001.chkl10 = objSrcHTR001.chkl10;
                objDesHTR001.chkl11 = objSrcHTR001.chkl11;
                objDesHTR001.chkl12 = objSrcHTR001.chkl12;
                objDesHTR001.chkl13 = objSrcHTR001.chkl13;
                objDesHTR001.chkl14 = objSrcHTR001.chkl14;
                objDesHTR001.chkl15 = objSrcHTR001.chkl15;
                objDesHTR001.chkl16 = objSrcHTR001.chkl16;
                objDesHTR001.chkl17 = objSrcHTR001.chkl17;
                objDesHTR001.chkl18 = objSrcHTR001.chkl18;
                objDesHTR001.chkl19 = objSrcHTR001.chkl19;
                objDesHTR001.chkl20 = objSrcHTR001.chkl20;
                objDesHTR001.chkl21 = objSrcHTR001.chkl21;
                objDesHTR001.chkl22 = objSrcHTR001.chkl22;
                objDesHTR001.chkl23 = objSrcHTR001.chkl23;
                objDesHTR001.chkl24 = objSrcHTR001.chkl24;
                objDesHTR001.BU = db.COM001.Where(i => i.t_cprj == objDesHTR001.Project).FirstOrDefault().t_entu;
                objDesHTR001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();

                if (IsEdited)
                {
                    objDesHTR001.EditedBy = objClsLoginInfo.UserName;
                    objDesHTR001.EditedOn = DateTime.Now;
                    //List<HTR003> lstDesHTR003 = db.HTR003.Where(x => x.HeaderId == objDesHTR001.HeaderId).ToList();
                    //if (lstDesHTR003 != null && lstDesHTR003.Count > 0)
                    //{
                    //    db.HTR003.RemoveRange(lstDesHTR003);
                    //}
                    List<HTR002> lstDesHTR002 = db.HTR002.Where(x => x.HeaderId == objDesHTR001.HeaderId).ToList();
                    if (lstDesHTR002 != null && lstDesHTR002.Count > 0)
                    {
                        db.HTR002.RemoveRange(lstDesHTR002);
                    }
                    db.SaveChanges();

                    List<HTR002> lstSrcHTR002 = db.HTR002.Where(x => x.HeaderId == objSrcHTR001.HeaderId).ToList();
                    db.HTR002.AddRange(
                                        lstSrcHTR002.Select(x =>
                                        new HTR002
                                        {
                                            HeaderId = objDesHTR001.HeaderId,
                                            Project = objDesHTR001.Project,
                                            HTRNo = objDesHTR001.HTRNo,
                                            RevNo = Convert.ToInt32(objDesHTR001.RevNo),
                                            HTRLineNo = x.HTRLineNo,
                                            SeamNo = x.SeamNo,
                                            PartNo = x.PartNo,
                                            HeatNo = x.HeatNo,
                                            DrawingNo = x.DrawingNo,
                                            DrawingRev = x.DrawingRev,
                                            HTPReference = x.HTPReference,
                                            PTCMTCPQR = x.PTCMTCPQR,
                                            Description = x.Description,
                                            MaterialSpec = x.MaterialSpec,
                                            SizeThickness = x.SizeThickness,
                                            Quantity = x.Quantity,
                                            TotalWeight = x.TotalWeight,
                                            InspectionAgency = x.InspectionAgency,
                                            MaterialFrom = x.MaterialFrom,
                                            MaterialTo = x.MaterialTo,
                                            ICSBy = x.ICSBy,
                                            EditedBy = objClsLoginInfo.UserName,
                                            EditedOn = DateTime.Now,
                                        })
                                      );
                    //List<HTR003> lstSrcHTR003 = db.HTR003.Where(x => x.HeaderId == objSrcHTR001.HeaderId).ToList();
                    //db.HTR003.AddRange(
                    //                    lstSrcHTR003.Select(x =>
                    //                    new HTR003
                    //                    {
                    //                        HeaderId = objSrcHTR001.HeaderId,
                    //                        Project = objSrcHTR001.Project,
                    //                        HTRNo = objSrcHTR001.HTRNo,
                    //                        RevNo = objSrcHTR001.RevNo,
                    //                        LinkedProject = x.LinkedProject,
                    //                        CreatedBy = objClsLoginInfo.UserName,
                    //                        CreatedOn = DateTime.Now,
                    //                    })
                    //                    );
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                }
                else
                {
                    objDesHTR001.CreatedBy = objClsLoginInfo.UserName;
                    objDesHTR001.CreatedOn = DateTime.Now;
                    db.HTR001.Add(objDesHTR001);
                    db.SaveChanges();
                    List<HTR002> lstSrcHTR002 = db.HTR002.Where(x => x.HeaderId == objSrcHTR001.HeaderId).ToList();
                    if (lstSrcHTR002 != null && lstSrcHTR002.Count > 0)
                    {
                        db.HTR002.AddRange(
                                        lstSrcHTR002.Select(x =>
                                        new HTR002
                                        {
                                            HeaderId = objDesHTR001.HeaderId,
                                            Project = objDesHTR001.Project,
                                            HTRNo = objDesHTR001.HTRNo,
                                            RevNo = Convert.ToInt32(objDesHTR001.RevNo),
                                            HTRLineNo = x.HTRLineNo,
                                            SeamNo = x.SeamNo,
                                            PartNo = x.PartNo,
                                            HeatNo = x.HeatNo,
                                            DrawingNo = x.DrawingNo,
                                            DrawingRev = x.DrawingRev,
                                            HTPReference = x.HTPReference,
                                            PTCMTCPQR = x.PTCMTCPQR,
                                            Description = x.Description,
                                            MaterialSpec = x.MaterialSpec,
                                            SizeThickness = x.SizeThickness,
                                            Quantity = x.Quantity,
                                            TotalWeight = x.TotalWeight,
                                            InspectionAgency = x.InspectionAgency,
                                            MaterialFrom = x.MaterialFrom,
                                            MaterialTo = x.MaterialTo,
                                            ICSBy = x.ICSBy,
                                            CreatedBy = objClsLoginInfo.UserName,
                                            CreatedOn = DateTime.Now,
                                        })
                                      );
                        //db.CTQ002.Add(lstSrcCTQ002);
                    }
                    //List<HTR003> lstSrcHTR003 = db.HTR003.Where(x => x.HeaderId == objSrcHTR001.HeaderId).ToList();
                    //db.HTR003.AddRange(
                    //                    lstSrcHTR003.Select(x =>
                    //                    new HTR003
                    //                    {
                    //                        HeaderId = objSrcHTR001.HeaderId,
                    //                        Project = objSrcHTR001.Project,
                    //                        HTRNo = objSrcHTR001.HTRNo,
                    //                        RevNo = objSrcHTR001.RevNo,
                    //                        LinkedProject = x.LinkedProject,
                    //                        CreatedBy = objClsLoginInfo.UserName,
                    //                        CreatedOn = DateTime.Now,
                    //                    })
                    //                    );
                    db.SaveChanges();
                    //var folderPath = "HTR001/LoadingSketch/" + objDesHTR001.HeaderId + "/R" + objDesHTR001.RevNo;
                    //var oldFolderPath = "HTR001/LoadingSketch/" + objSrcHTR001.HeaderId + "/R" + objSrcHTR001.RevNo;
                    //(new clsFileUpload()).CopyFolderContents(oldFolderPath, folderPath);
                    //// ----- Read and Add HTR Number on new PDF ----- //
                    //var HTRDocuments = (new clsFileUpload()).GetDocuments(oldFolderPath).Select(s => s.Name);
                    //try
                    //{
                    //    // ----- To check After file copied all files are availabled in destination -----//
                    //    var newHTRDocuments = (new clsFileUpload()).GetDocuments(folderPath).Select(s => s.Name);
                    //    List<string> fileNotFound = new List<string>(), fileFound = new List<string>();
                    //    foreach (var item in HTRDocuments)
                    //    {
                    //        if (newHTRDocuments.Contains(item))
                    //            fileFound.Add(item);
                    //        else
                    //            fileNotFound.Add(item);
                    //    }
                    //    objResponseMsg.ActionValue = "Copied Files ["+ String.Join(" | ", fileFound) + "] ; Not Found ["+String.Join(" | ", fileNotFound)+"]"; 
                    //    // ---------------------------------------- //
                    //    Utility.Controllers.FileUploadController.FileUpload_WriteContentOnPDF(String.Join(",", HTRDocuments), folderPath, objDesHTR001.HTRNo, 50, 50, 90,0,"bottomleft");
                    //    Utility.Controllers.FileUploadController.FileUpload_WriteContentOnPDF(String.Join(",", HTRDocuments), folderPath, objDesHTR001.HTRNo, 50, 50, 0,0,"topright");
                    //    /*foreach (var item in HTRDocuments.Where(w => w.Name.Contains(".pdf")))
                    //    {
                    //        using (var ms = new System.IO.MemoryStream())
                    //        {
                    //            var pdfdoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A3, 0, 0, 0, 0);
                    //            var writer = iTextSharp.text.pdf.PdfWriter.GetInstance(pdfdoc, ms);
                    //            pdfdoc.Open();

                    //            var templateReader = new iTextSharp.text.pdf.PdfReader(item.URL);
                    //            using (var rdr = new iTextSharp.text.pdf.PdfReader(item.URL))
                    //            {
                    //                iTextSharp.text.pdf.PdfImportedPage page;
                    //                for (int i = 1; i <= rdr.NumberOfPages; i++)
                    //                {
                    //                    page = writer.GetImportedPage(templateReader, i);
                    //                    writer.DirectContent.AddTemplate(page, 0, 0);
                    //                    var bf = iTextSharp.text.pdf.BaseFont.CreateFont();
                    //                    writer.DirectContent.BeginText();
                    //                    writer.DirectContent.SetFontAndSize(bf, 18f);
                    //                    writer.DirectContent.ShowTextAligned(iTextSharp.text.pdf.PdfContentByte.ALIGN_RIGHT, objDesHTR001.HTRNo, 50, 1150, 90);
                    //                    writer.DirectContent.EndText();
                    //                    pdfdoc.NewPage();
                    //                }
                    //            }
                    //            pdfdoc.Close();
                    //            byte[] bytes = ms.ToArray();
                    //            ms.Close();
                    //            System.IO.Stream stream = new System.IO.MemoryStream(bytes);
                    //            (new clsFileUpload()).FileUpload(item.Name, folderPath, stream, item.Uploader, item.Comments, item.onFTP); // Override file with added label on PDF
                    //        }
                    //    }*/
                    //}
                    //catch (Exception ex)
                    //{
                    //    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    //    objResponseMsg.Key = false;
                    //    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
                    //}
                    // ------------------------------------------ //
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.NDETechniquesMessage.Copy.ToString() + " To Document No : " + objDesHTR001.HTRNo + " of " + objDesHTR001.Project + " Project.";
                    //clsImplementationMessage.CommonMessages.Copy.ToString();
                }
                objResponseMsg.HeaderId = objDesHTR001.HeaderId;
                var folderPath = "HTR001/LoadingSketch/" + objDesHTR001.HeaderId + "/R" + objDesHTR001.RevNo;
                var oldFolderPath = "HTR001/LoadingSketch/" + objSrcHTR001.HeaderId + "/R" + objSrcHTR001.RevNo;
                (new clsFileUpload()).CopyFolderContents(oldFolderPath, folderPath);
                // ----- Read and Add HTR Number on new PDF ----- //
                var HTRDocuments = (new clsFileUpload()).GetDocuments(oldFolderPath).Select(s => s.Name);
                try
                {
                    // ----- To check After file copied all files are availabled in destination -----//
                    var newHTRDocuments = (new clsFileUpload()).GetDocuments(folderPath).Select(s => s.Name);
                    List<string> fileNotFound = new List<string>(), fileFound = new List<string>();
                    foreach (var item in HTRDocuments)
                    {
                        if (newHTRDocuments.Contains(item))
                            fileFound.Add(item);
                        else
                            fileNotFound.Add(item);
                    }
                    objResponseMsg.ActionValue = "Copied Files [" + String.Join(" | ", fileFound) + "] ; Not Found [" + String.Join(" | ", fileNotFound) + "]";
                    // ---------------------------------------- //
                    Utility.Controllers.FileUploadController.FileUpload_WriteContentOnPDF(String.Join(",", HTRDocuments), folderPath, objDesHTR001.HTRNo + " (R" + objDesHTR001.RevNo + ")", 0, 0, 90, 0, "bottomleft");
                    Utility.Controllers.FileUploadController.FileUpload_WriteContentOnPDF(String.Join(",", HTRDocuments), folderPath, objDesHTR001.HTRNo + " (R" + objDesHTR001.RevNo + ")", 0, 0, 0, 0, "topright");
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return objResponseMsg;
        }
        #endregion

        #region History
        [HttpPost]
        public ActionResult GetHTRHistoryDetails(int Id, string type) //partial for CTQ History Lines Details
        {
            HTR001_Log objHTR001Log = new HTR001_Log();
            ViewBag.type = type;
            objHTR001Log = db.HTR001_Log.Where(x => x.HeaderId == Id).FirstOrDefault();
            return PartialView("_HTRHistoryPartial", objHTR001Log);
        }

        public ActionResult ViewLogDetails(int? Id, string type)
        {
            HTR001_Log objHTR001 = new HTR001_Log();
            if (Id > 0)
            {
                objHTR001 = db.HTR001_Log.Where(i => i.Id == Id).FirstOrDefault();
                HTR002_Log objHTR002 = db.HTR002_Log.Where(i => i.RefId == Id).FirstOrDefault();
                objHTR001.Project = db.COM001.Where(i => i.t_cprj == objHTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objHTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objHTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objHTR001.Department = db.COM002.Where(a => a.t_dimx == objHTR001.Department).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objHTR001.Inspector = db.COM003.Where(x => x.t_psno == objHTR001.Inspector && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objHTR001.MET = db.COM003.Where(x => x.t_psno == objHTR001.MET && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();
                objHTR001.FurnaceSupervisor = db.COM003.Where(x => x.t_psno == objHTR001.FurnaceSupervisor && x.t_actv == 1).Select(x => x.t_name).FirstOrDefault();

                ViewBag.RefId = objHTR002.RefId;
                ViewBag.Action = "edit";
                ViewBag.type = type;
            }
            return View(objHTR001);
        }

        [HttpPost]
        public ActionResult GetHTRLinesHistoryViewForm(int LogID, int LineId, string project)
        {
            HTR001_Log objHTR001 = new HTR001_Log();
            HTR002_Log objHTR002 = new HTR002_Log();
            objHTR001 = db.HTR001_Log.Where(x => x.Id == LogID).FirstOrDefault();
            if (objHTR001 != null)
            {
                string buCode = db.COM001.Where(i => i.t_cprj == objHTR001.Project).FirstOrDefault().t_entu;
                var InspectionAgency = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                //List<GLB002> lstCTGLB002 = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).ToList();
                //ViewBag.Agency = lstCTGLB002.Count();
                // lstCTGLB002.AsEnumerable().Select(x => new SelectListItem() { Text = (x.Code + "-" + x.Description).ToString(), Value = x.Code.ToString() }).ToList();
                ViewBag.Agency = InspectionAgency.Any() ? InspectionAgency : null;


                if (InspectionAgency.Count > 0)
                    ViewBag.InspectionAgency = InspectionAgency;
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!InspectionAgency.Any())
                        lstCategory.Add("Inspection Agency");
                    ViewBag.Agency = null;
                    ViewBag.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }
            }

            if (LineId > 0)
            {
                objHTR002 = db.HTR002_Log.Where(x => x.LineId == LineId && x.RefId == LogID).FirstOrDefault();

                ViewBag.Action = "viewline";
            }
            else
            {
                objHTR002.RevNo = 0;
            }

            ViewBag.LineProject = project;//db.COM001.Where(i => i.t_cprj == objHTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.HTRNo = objHTR001.HTRNo;
            ViewBag.RevNo = objHTR001.RevNo;
            objHTR002.HeaderId = objHTR001.HeaderId;
            return PartialView("~/Areas/HTR/Views/Shared/_LoadHistoryLines.cshtml", objHTR002);
        }


        [HttpPost]
        public JsonResult LoadHTRHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                strWhere += "1=1 and HeaderId=" + param.Headerid;
                string[] columnName = { "Project", "HTRNo", "HTRDescription", "Department", "Location", "ConcertoMSP", "Treatment", "Furnace", "RevNo", "Status" };

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstResult = db.SP_HTR_HEADER_HISTORY_GETDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Project),
                            Convert.ToString(uc.HTRNo),
                            Convert.ToString( uc.Department),
                            Convert.ToString(uc.Location),
                            Convert.ToString( uc.ConcertoMSP),
                            Convert.ToString(uc.Treatment),
                            Convert.ToString(uc.Furnace),
                            uc.HTRDescription,
                            Convert.ToString("R" + uc.RevNo),
                            Convert.ToString(uc.Status),
                            getPromotedDemotedBy( Convert.ToString(uc.FurnaceOn), Convert.ToString(uc.ReviewdOn),Convert.ToString(uc.InspectOn),Convert.ToString(uc.SubmittedOn),Convert.ToString(uc.Status),Convert.ToString(uc.HTRType)),
                            Convert.ToString(uc.CreatedBy),
                             //Convert.ToString(uc.EditedBy),
                            
            "<center><a title='View Details' class='btn btn-xs' href='"+WebsiteURL+"/HTR/Maintain/ViewLogDetails?Id="+Convert.ToInt32(uc.Id)+"&type="+ param.MTStatus +"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string getPromotedDemotedBy(string FurnaceOn, string ReviewOn, string InspectorOn, string SubmittedOn, string status, string type)
        {
            string actionDate = string.Empty;
            if (status == clsImplementationEnum.HTRStatus.InReview.GetStringValue())
            {
                actionDate = ReviewOn;
            }
            else if (status == clsImplementationEnum.HTRStatus.WithFurnaceSupervisor.GetStringValue())
            {
                actionDate = FurnaceOn;
            }
            else if (status == clsImplementationEnum.HTRStatus.WithInspector.GetStringValue())
            {
                //if (type == clsImplementationEnum.TypeOfHTR.JobComponent.GetStringValue())
                //    actionDate = InspectorOn;
                //else
                actionDate = InspectorOn;
            }
            else if (status == clsImplementationEnum.HTRStatus.Release.GetStringValue())
            {
                if (type != clsImplementationEnum.TypeOfHTR.PQR.GetStringValue())
                    actionDate = InspectorOn;
                else
                    actionDate = SubmittedOn;
            }
            else
            {
                actionDate = SubmittedOn;
            }
            return actionDate;
        }
        [HttpPost]
        public JsonResult LoadHTRLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                //strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                strWhere += " RefId=" + param.CTQHeaderId;
                string[] columnName = { "SeamNo", "PartNo", "HeatNo", "DrawingNo", "DrawingRev", "HTPReference", "PTCMTCPQR", "Description", "MaterialSpec", "SizeThickness", "Quantity", "TotalWeight", "InspectionAgency", "MaterialFrom", "MaterialTo", "ICSBy", "ProjectNumber" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_HTR_LINE_HISTORY_GETDETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                            Convert.ToString(uc.LineId),
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.SeamNo),
                            Convert.ToString(uc.PartNo),
                            Convert.ToString(uc.HeatNo),
                            Convert.ToString(uc.DrawingNo),
                            Convert.ToString(uc.DrawingRev),
                            Convert.ToString(uc.HTPReference),
                            Convert.ToString(uc.PTCMTCPQR),
                            Convert.ToString(uc.Description),
                            Convert.ToString(uc.MaterialSpec),
                            Convert.ToString(uc.SizeThickness),
                            Convert.ToString(uc.Quantity),
                            Convert.ToString(uc.TotalWeight),
                            Convert.ToString(uc.InspectionAgency),
                            Convert.ToString(uc.MaterialFrom),
                            Convert.ToString(uc.MaterialTo),
                            Convert.ToString(uc.ICSBy),
                            Convert.ToString(uc.ProjectNumber)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetProjects(string search)
        {
            try
            {
                var items = Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, search).ToList();

                var item = from li in items
                           select new
                           {
                               id = li.projectCode,
                               text = li.projectDescription
                           };
                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetProjectsEdit(int? headerId)
        {
            try
            {
                var lst = Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, "").ToList();
                var item = from li in lst
                           select new
                           {
                               id = li.projectCode,
                               text = li.projectDescription
                           };
                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }



        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int? revno;
            public int HeaderId;
            public bool ActionKey;
            public string ActionValue;
        }

        #endregion

        #region Removed Functionality
        //old code (Lines Pop Up)
        [HttpPost]
        public ActionResult GetHTRLinesForm(int HeaderId, int LineId)
        {
            HTR001 objHTR001 = new HTR001();
            HTR002 objHTR002 = new HTR002();
            objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objHTR001 != null)
            {
                string buCode = db.COM001.Where(i => i.t_cprj == objHTR001.Project).FirstOrDefault().t_entu;
                var InspectionAgency = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                //List<GLB002> lstCTGLB002 = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).ToList();
                //ViewBag.Agency = lstCTGLB002.Count();
                // lstCTGLB002.AsEnumerable().Select(x => new SelectListItem() { Text = (x.Code + "-" + x.Description).ToString(), Value = x.Code.ToString() }).ToList();
                ViewBag.Agency = InspectionAgency.Any() ? InspectionAgency : null;


                if (InspectionAgency.Count > 0)
                    ViewBag.InspectionAgency = InspectionAgency;
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!InspectionAgency.Any())
                        lstCategory.Add("Inspection Agency");
                    ViewBag.Agency = null;
                    ViewBag.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }
            }

            if (LineId > 0)
            {
                objHTR002 = db.HTR002.Where(x => x.LineId == LineId && x.CreatedBy == objClsLoginInfo.UserName).FirstOrDefault();
                ViewBag.Action = "line edit";
            }
            else
            {
                objHTR002.RevNo = 0;
            }
            objHTR002.Project = db.COM001.Where(i => i.t_cprj == objHTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objHTR002.HTRNo = objHTR001.HTRNo;
            objHTR002.RevNo = objHTR001.RevNo;
            objHTR002.HeaderId = objHTR001.HeaderId;
            return PartialView("_LoadHTRLineDataPartial", objHTR002);
        }

        //old code (View Pop up)
        [HttpPost]
        public ActionResult GetHTRLinesViewForm(int HeaderId, int LineId, string project)
        {
            HTR001 objHTR001 = new HTR001();
            HTR002 objHTR002 = new HTR002();
            objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objHTR001 != null)
            {
                string buCode = db.COM001.Where(i => i.t_cprj == objHTR001.Project).FirstOrDefault().t_entu;
                var InspectionAgency = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                //List<GLB002> lstCTGLB002 = Manager.GetSubCatagories("Inspection Agency", buCode, objClsLoginInfo.Location).ToList();
                //ViewBag.Agency = lstCTGLB002.Count();
                // lstCTGLB002.AsEnumerable().Select(x => new SelectListItem() { Text = (x.Code + "-" + x.Description).ToString(), Value = x.Code.ToString() }).ToList();
                ViewBag.Agency = InspectionAgency.Any() ? InspectionAgency : null;


                if (InspectionAgency.Count > 0)
                    ViewBag.InspectionAgency = InspectionAgency;
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!InspectionAgency.Any())
                        lstCategory.Add("Inspection Agency");
                    ViewBag.Agency = null;
                    ViewBag.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }
            }

            if (LineId > 0)
            {
                objHTR002 = db.HTR002.Where(x => x.LineId == LineId).FirstOrDefault();

                ViewBag.Action = "viewline";
            }
            else
            {
                objHTR002.RevNo = 0;
            }

            ViewBag.LineProject = project;//db.COM001.Where(i => i.t_cprj == objHTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.HTRNo = objHTR001.HTRNo;
            ViewBag.RevNo = objHTR001.RevNo;
            objHTR002.HeaderId = objHTR001.HeaderId;
            return PartialView("_LoadHTRLineDataPartial", objHTR002);
        }

        //
        [HttpPost]
        public ActionResult LoadHTRLineData(int HeaderId, int LineId)
        {
            HTR001 objHTR001 = new HTR001();
            HTR002 objHTR002 = new HTR002();
            objHTR001 = db.HTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (LineId > 0)
            {
                objHTR002 = db.HTR002.Where(x => x.LineId == LineId && x.CreatedBy == objClsLoginInfo.UserName).FirstOrDefault();

                var Project = (from a in db.COM001
                               where a.t_cprj == objHTR002.Project
                               select new { pDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();
                ViewBag.Project = Project.pDesc;
                objHTR002.HeaderId = objHTR001.HeaderId;
                objHTR002.HTR001.RevNo = objHTR002.RevNo;
                ViewBag.Action = "line edit";
            }

            return PartialView("_LoadHTRLineDataPartial", objHTR002);
        }

        [HttpPost]
        public JsonResult LoadHTRLineGridData_R0(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                string headerid = param.CTQHeaderId;
                strWhere = " (1=1 and a.HeaderId IN(" + headerid + "))";
                //strWhere += "and CreatedBy=" + objClsLoginInfo.UserName;
                //string[] columnName = { "SeamNo", "PartNo", "HeatNo", "DrawingNo", "DrawingRev", "ConcertoMSP", "Furnace", "RevNo", "Status" };
                string[] columnName = { "SeamNo", "PartNo", "HeatNo", "DrawingNo", "DrawingRev", "a.RevNo" };

                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstResult = db.SP_HTR_LINE_GETDETAILS
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.SeamNo),
                                Convert.ToString(uc.PartNo),
                                Convert.ToString(uc.HeatNo),
                                Convert.ToString(uc.DrawingNo),
                                Convert.ToString(uc.DrawingRev),
                                Convert.ToString(uc.Description),
                                Convert.ToString(uc.InspectionAgency),
                                   Convert.ToString(uc.CreatedBy),
                                Convert.ToString(uc.LineId),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveHTRLines(HTR002 htr002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int? oldrev = 0;
                int? newrev = 0;
                bool isRevised = false;
                HTR001 objHTR001 = db.HTR001.Where(x => x.HeaderId == htr002.HeaderId).FirstOrDefault();
                HTR002 objHTR002 = new HTR002();
                bool IsEdited = false;
                if (htr002.LineId > 0)
                {
                    objHTR002 = db.HTR002.Where(x => x.LineId == htr002.LineId).FirstOrDefault();
                    IsEdited = true;
                }
                objHTR002.HeaderId = objHTR001.HeaderId;
                objHTR002.Project = objHTR001.Project;
                objHTR002.HTRNo = objHTR001.HTRNo;
                objHTR002.RevNo = objHTR001.RevNo;
                objHTR002.HTRLineNo = htr002.HTRLineNo;
                objHTR002.SeamNo = htr002.SeamNo;
                objHTR002.PartNo = htr002.PartNo;
                objHTR002.HeatNo = htr002.HeatNo;
                objHTR002.DrawingNo = htr002.DrawingNo;
                objHTR002.DrawingRev = htr002.DrawingRev;
                objHTR002.HTPReference = htr002.HTPReference;
                objHTR002.PTCMTCPQR = htr002.PTCMTCPQR;
                objHTR002.Description = htr002.Description;
                objHTR002.MaterialSpec = htr002.MaterialSpec;
                objHTR002.SizeThickness = htr002.SizeThickness;
                objHTR002.Quantity = htr002.Quantity;
                objHTR002.TotalWeight = htr002.TotalWeight;
                objHTR002.InspectionAgency = fc["InspectionAgency"];
                objHTR002.MaterialFrom = htr002.MaterialFrom;
                objHTR002.MaterialTo = htr002.MaterialTo;
                objHTR002.ICSBy = htr002.ICSBy;
                objHTR002.ProjectNumber = htr002.ProjectNumber;

                if (objHTR001.Status == clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue())
                {
                    oldrev = objHTR001.RevNo;
                    newrev = Convert.ToInt32(objHTR001.RevNo) + 1;
                    objHTR001.RevNo = Convert.ToInt32(objHTR001.RevNo) + 1;
                    objHTR001.FurnaceComments = null;
                    objHTR001.InspectorComments = null;
                    objHTR001.ReviewerComments = null;
                    objHTR001.ReviewdOn = null;
                    objHTR001.FurnaceOn = null;
                    objHTR001.InspectOn = null;
                    objHTR001.SubmittedBy = null;
                    objHTR001.SubmittedOn = null;
                    objHTR001.Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                    objHTR001.EditedBy = objClsLoginInfo.UserName;
                    objHTR001.EditedOn = DateTime.Now;
                    isRevised = true;
                }
                if (IsEdited)
                {
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Update;
                }
                else
                {
                    objHTR002.CreatedBy = objClsLoginInfo.UserName;
                    objHTR002.CreatedOn = DateTime.Now;
                    db.HTR002.Add(objHTR002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Insert;
                }
                if (isRevised)
                {
                    var folderPath = "HTR001/LoadingSketch/" + objHTR001.HeaderId + "/R" + newrev;
                    var oldFolderPath = "HTR001/LoadingSketch/" + objHTR001.HeaderId + "/R" + oldrev;
                    (new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, folderPath);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Excell
        //Code By : Ajay Chauhan on 09/11/2017, Export Funxtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_HTR_HEADER_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      HTRNo = li.HTRNo,
                                      RevNo = li.RevNo,
                                      Description = li.HTRDescription,
                                      Department = li.Department,
                                      Location = li.Location,
                                      ConcertoMSP = li.ConcertoMSP,
                                      Treatment = li.Treatment,
                                      Furnace = li.Furnace,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_HTR_LINE_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      SeamNo = li.SeamNo,
                                      PartNo = li.PartNo,
                                      HeatNo = li.HeatNo,
                                      DrawingNo = li.DrawingNo,
                                      DrawingRev = li.DrawingRev,
                                      HTPReference = li.HTPReference,
                                      PTCMTCPQR = li.PTCMTCPQR,
                                      Description = li.Description,
                                      MaterialSpec = li.MaterialSpec,
                                      SizeThickness = li.SizeThickness,
                                      Quantity = li.Quantity,
                                      TotalWeight = li.TotalWeight,
                                      InspectionAgency = li.InspectionAgency,
                                      MaterialFrom = li.MaterialFrom,
                                      MaterialTo = li.MaterialTo,
                                      ICSBy = li.ICSBy,
                                      ProjectNumber = li.ProjectNumber,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_HTR_HEADER_HISTORY_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      HTRNo = li.HTRNo,
                                      RevNo = li.RevNo,
                                      Description = li.HTRDescription,
                                      Department = li.Department,
                                      Location = li.Location,
                                      ConcertoMSP = li.ConcertoMSP,
                                      Treatment = li.Treatment,
                                      Furnace = li.Furnace,
                                      Status = li.Status,
                                      PromotedDemotedBy = getPromotedDemotedBy(Convert.ToString(li.FurnaceOn), Convert.ToString(li.ReviewdOn), Convert.ToString(li.InspectOn), Convert.ToString(li.SubmittedOn), Convert.ToString(li.Status), Convert.ToString(li.HTRType)),
                                      CreatedBy = li.CreatedBy,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_HTR_LINE_HISTORY_GETDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      SeamNo = li.SeamNo,
                                      PartNo = li.PartNo,
                                      HeatNo = li.HeatNo,
                                      DrawingNo = li.DrawingNo,
                                      DrawingRev = li.DrawingRev,
                                      HTPReference = li.HTPReference,
                                      PTCMTCPQR = li.PTCMTCPQR,
                                      Description = li.Description,
                                      MaterialSpec = li.MaterialSpec,
                                      SizeThickness = li.SizeThickness,
                                      Quantity = li.Quantity,
                                      TotalWeight = li.TotalWeight,
                                      InspectionAgency = li.InspectionAgency,
                                      MaterialFrom = li.MaterialFrom,
                                      MaterialTo = li.MaterialTo,
                                      ICSBy = li.ICSBy,
                                      ProjectNumber = li.ProjectNumber
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        [HttpPost]

        public ActionResult GetAllIdenticalProjects(string param, string Location)
        {
            //var ApprovedStatus = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
            //var listExistAllIQualityProject = db.WPS013.Select(i => i.IQualityProject).Distinct().ToList();

            //List<ddlValue> listidenticalProject = db.QMS010.Where(i => !i.Project.Equals(param, StringComparison.OrdinalIgnoreCase) && !listExistAllIQualityProject.Contains(i.QualityProject)).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject }).Distinct().ToList();
            //var lisIQualProjForCurrentQualityProject = db.WPS013.Where(i => i.Project == param && i.Location == Location).Select(i => i.IQualityProject).ToList();
            //var IdenticalProject = string.Join(",", lisIQualProjForCurrentQualityProject);
            //var IsDisabled = (db.WPS012.Where(i => i.Project == param && i.Location == Location && i.Status == ApprovedStatus).Any());
            //lisIQualProjForCurrentQualityProject.ForEach(QProject =>
            //{
            //    listidenticalProject.Add(new ddlValue { id = QProject, text = QProject });
            //});
            if (param != "")
            {
                ModelQualityId objModelQualityId = new ModelQualityId();
                string qualityProject = (from m in db.QMS010 where m.Project == param select m.QualityProject).FirstOrDefault();

                var project = db.QMS010.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
                if (project != null)
                {
                    objModelQualityId.Project = db.COM001.Where(i => i.t_cprj.Equals(project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();// project;
                    var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);
                    objModelQualityId.QCPBU = ProjectWiseBULocation.QCPBU;
                    //if (isQualityIdExist(qualityProject, project, ProjectWiseBULocation.QCPBU.Split('-')[0], location, qid))
                    //    objModelQualityId.headerAlreadyExist = true;

                    var identicalProject = db.QMS017.Where(i => i.QualityProject.Equals(qualityProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.IQualityProject).Distinct().ToList();
                    objModelQualityId.IdenticalProj = string.Join(",", identicalProject);

                    return Json(new { listidenticalProject = identicalProject, IdenticalProject = identicalProject }, JsonRequestBehavior.AllowGet);
                }
                return Json("");

            }
            else
                return Json("");

        }
        [HttpGet]
        public JsonResult GetCurrentLocationIP(string HTRNo)
        {
            IpLocation ipLocation = CommonService.GetUseIPConfig;
            int count = (from c in db.HTR002 where c.HTRNo == HTRNo select c).Count() + 1;

            return Json(new { Location = ipLocation.Ip, FilePath = ipLocation.File_Path, Count = count });
        }
    }
}