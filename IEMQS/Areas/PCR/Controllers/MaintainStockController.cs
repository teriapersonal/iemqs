﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsHelper;

namespace IEMQS.Areas.PCR.Controllers
{
    public class MaintainStockController : clsBase
    {
        public const string StockFetchSuccess = "Stock data successfully fetch from LN.";
        public const string NoRecordsFoundsInStockFetch = "No New Stock Found In LN.";
        public const string SomethingWrong = "Something went wrong. Try after some time.";
        public const string NotAuthorizeFetchData = "You Are Not Authorize To Fetch LN Data!";
        // GET: PCR/MaintainStock

        #region Index
        [SessionExpireFilter, UserPermissions, LastURLHistory]
        public ActionResult Index()
        {
            ViewBag.Title = "Maintain Stock";
            ViewBag.IsDisplay = false;
            return View();
        }
        [SessionExpireFilter, UserPermissions, LastURLHistory]
        public ActionResult DisplayStock()
        {
            ViewBag.Title = "Display Stock";
            ViewBag.IsDisplay = true;
            return View("Index");
        }
        [SessionExpireFilter]
        public ActionResult LoadStockDataTable(JQueryDataTableParamModel param, string status)
        {

            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + 10;// param.iDisplayLength;

                string whereCondition = "1=1";

                var lstLoc = string.Join(",", db.ATH001.Where(x => x.Employee == objClsLoginInfo.UserName).Select(x => x.Location).Distinct().ToList());

                whereCondition += Manager.MakeStringInCondition("Location", lstLoc);

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName ={ "StockNumber",
                                            "StockRevision",
                                            "ContractNo",
                                            "Length",
                                            "Width",
                                            "Thickness",
                                            "Materials",
                                            "QtyInKg",
                                            "PurchaseOrder",
                                            "POPosition",
                                            "StockStatusName",
                                            "StockPhysicalStatusName"
                                         };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_PCR_GET_STOCK_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new
                          {
                              StockNumber = Convert.ToString(h.StockNumber),
                              StockRevision = Convert.ToString(h.StockRevision),
                              ContractNo = Convert.ToString(h.ContractNo),
                              Length = Convert.ToString(h.Length),
                              Width = Convert.ToString(h.Width),
                              Thickness = Convert.ToString(h.Thickness),
                              Materials = Convert.ToString(h.Materials),
                              QtyInKg = Convert.ToString(h.QtyInKg),
                              PurchaseOrder = Convert.ToString(h.PurchaseOrder),
                              POPosition = Convert.ToString(h.POPosition),
                              StockStatus = Convert.ToString(h.StockStatus),
                              StockPhysicalStatus = Convert.ToString(h.StockPhysicalStatus),
                              StockId = Convert.ToString(h.StockId),
                              StockStatusName = Convert.ToString(h.StockStatusName),
                              StockPhysicalStatusName = Convert.ToString(h.StockPhysicalStatusName),
                              Item = Convert.ToString(h.Item)
                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public JsonResult FetchNewStockFromLN()
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {

                if (objClsLoginInfo.listMenuResult != null)
                {
                    var lstMenuResult = objClsLoginInfo.listMenuResult;
                    if (!lstMenuResult.Any(i => (string.IsNullOrEmpty(i.Area) ? "" : i.Area.ToLower()) == "PCR".ToLower()
                                        && (string.IsNullOrEmpty(i.Controller) ? "" : i.Controller.ToLower()) == "MaintainStock".ToLower()
                                        && (string.IsNullOrEmpty(i.Action) ? "" : i.Action.ToLower()) == "Index".ToLower()))
                    { 
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = NotAuthorizeFetchData;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }


                var affectedRows = db.SP_PCR_INSERT_UPDATE_LN_STOCK_IN_IEMQS();
                if (affectedRows > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = StockFetchSuccess;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = NoRecordsFoundsInStockFetch;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        public JsonResult UpdateLNStockDetails(int StockId)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                if (objClsLoginInfo.listMenuResult != null)
                {
                    var lstMenuResult = objClsLoginInfo.listMenuResult;
                    if (!lstMenuResult.Any(i => (string.IsNullOrEmpty(i.Area) ? "" : i.Area.ToLower()) == "PCR".ToLower()
                                        && (string.IsNullOrEmpty(i.Controller) ? "" : i.Controller.ToLower()) == "MaintainStock".ToLower() 
                                        && (string.IsNullOrEmpty(i.Action) ? "" : i.Action.ToLower()) == "StockDetails".ToLower()))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = NotAuthorizeFetchData;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                var affectedRows = db.SP_PCR_UPDATE_IEQMS_STOCK_FROM_LN(StockId);
                if (affectedRows > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = StockFetchSuccess;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = SomethingWrong;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Stock Details
        [SessionExpireFilter, UserPermissions]
        public ActionResult StockDetails(int? id)
        {
            ViewBag.IsDisplay = false;
            PCR001 objPCR001 = new PCR001();
            if (id.HasValue)
            {
                objPCR001 = db.PCR001.Where(i => i.StockId == id.Value).FirstOrDefault();
            }
            return View(objPCR001);
        }
        [SessionExpireFilter, UserPermissions]
        public ActionResult DisplayStockDetails(int? id)
        {
            ViewBag.IsDisplay = true;
            PCR001 objPCR001 = new PCR001();
            if (id.HasValue)
            {
                objPCR001 = db.PCR001.Where(i => i.StockId == id.Value).FirstOrDefault();
            }
            return View("StockDetails", objPCR001);
        }
        [SessionExpireFilter]
        public ActionResult _StockDetails(int? id)
        {
            SP_PCR_GET_STOCK_DETAILS_BY_STOCKID_Result objPCR001 = new SP_PCR_GET_STOCK_DETAILS_BY_STOCKID_Result();
            if (id.HasValue)
            {
                objPCR001 = db.SP_PCR_GET_STOCK_DETAILS_BY_STOCKID(id.Value).FirstOrDefault();
            }
            return View("_StockDetails", objPCR001);
        }
        [SessionExpireFilter]
        public ActionResult _StockOtherDetails(int? id)
        {
            SP_PCR_GET_STOCK_OTHER_DETAILS_BY_STOCKID_Result objPCR001 = new SP_PCR_GET_STOCK_OTHER_DETAILS_BY_STOCKID_Result();
            if (id.HasValue)
            {
                objPCR001 = db.SP_PCR_GET_STOCK_OTHER_DETAILS_BY_STOCKID(id.Value).FirstOrDefault();
            }
            return View("_StockOtherDetails", objPCR001);
        }
        [SessionExpireFilter]
        public ActionResult _StockLotFeatureDetails(int? id)
        {
            PCR001 objPCR001 = new PCR001();
            if (id.HasValue)
            {
                objPCR001 = db.PCR001.Where(i => i.StockId == id.Value).FirstOrDefault();
            }
            //ViewBag.Item = objPCR001.Item;
            //ViewBag.ActualLot = objPCR001.ActualLot;
            return View("_StockLotFeatureDetails", objPCR001);
        }
        [SessionExpireFilter]
        public ActionResult LoadStockLotFeatureDataTable(string Item, string ActualLot)
        {
            try
            {
                var lstHeader = db.SP_PCR_GET_STOCK_LOT_FEATURE_DETAILS(Item, ActualLot).ToList();
                int? totalRecords = lstHeader.Count();
                var res = from h in lstHeader
                          select new
                          {
                              Feature = Convert.ToString(h.Feature),
                              LotFeatureValue = Convert.ToString(h.LotFeatureValue),
                          };

                return Json(new
                {
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult _StockPurchasePegDistributionDetails(int? id)
        {
            PCR001 objPCR001 = new PCR001();
            if (id.HasValue)
            {
                objPCR001 = db.PCR001.Where(i => i.StockId == id.Value).FirstOrDefault();
            }
            return View("_StockPurchasePegDistributionDetails", objPCR001);
        }
        [SessionExpireFilter]
        public ActionResult LoadPurchasePegDistributionDataTable(string PONumber, string Position)
        {
            try
            {
                var lstHeader = db.SP_PCR_GET_PURCHASE_PEG_DISTRIBUTION_DETAILS(PONumber, Position).ToList();
                int? totalRecords = lstHeader.Count();
                var res = from h in lstHeader
                          select new
                          {
                              BusinessObjectRefference = Convert.ToString(h.BusinessObjectRefference),
                              DistributionLine = Convert.ToString(h.DistributionLine),
                              Project = Convert.ToString(h.Project),
                              ExciseExempt = Convert.ToString(h.ExciseExempt),
                              Element = Convert.ToString(h.Element),
                              Activity = Convert.ToString(h.Activity),
                              OrderQty = Convert.ToString(h.OrderQty),
                          };

                return Json(new
                {
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Export Excel
        [SessionExpireFilter]
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_PCR_GET_STOCK_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from h in lst
                                  select new
                                  {
                                      StockNumber = Convert.ToString(h.StockNumber),
                                      StockRevision = Convert.ToString(h.StockRevision),
                                      ContractNo = Convert.ToString(h.ContractNo),
                                      Length = Convert.ToString(h.Length),
                                      Width = Convert.ToString(h.Width),
                                      Thickness = Convert.ToString(h.Thickness),
                                      Materials = Convert.ToString(h.Materials),
                                      QtyInKg = Convert.ToString(h.QtyInKg),
                                      PurchaseOrder = Convert.ToString(h.PurchaseOrder),
                                      POPosition = Convert.ToString(h.POPosition),
                                      StockStatus = Convert.ToString(h.StockStatusName),
                                      StockPhysicalStatus = Convert.ToString(h.StockPhysicalStatusName),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public class StockResponseMsg : ResponseMsg
        {
            public bool IsInformation { get; set; }
        }
    }
}