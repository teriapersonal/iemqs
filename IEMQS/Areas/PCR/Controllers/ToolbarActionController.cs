﻿using IEMQSImplementation;
using IEMQSImplementation.ILN.Domain.Models;
using IEMQSImplementation.ILN.Domain.Enums;
using IEMQS.Areas.ILN.Framework;
using IEMQS.Models;
using IEMQS.Areas.PCR.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IEMQS.Areas.PCR.Controllers
{
    public class ToolbarActionController : clsBase
    {
        
        public async Task<JsonResult> Index(string input)
        {
            ActionInput actionInput = JsonConvert.DeserializeObject<ActionInput>(input);
            ActionOutput output = null;
            switch (actionInput.ActionName.ToUpper())
            {
                case "FETCHDATA":
                    {
                        List<Dictionary<string, object>> lst = JsonConvert.DeserializeObject<List<Dictionary<string, object>>>(actionInput.Parameters.ToString());
                        string projectValue = ReadFromInput(lst, "t_cprj");
                        string elementValue = ReadFromInput(lst, "t_cspa");
                        string locationValue = "PEW";
                        string initValue = "ER";
                        output = await (new DataAdapter()).FetchData(projectValue, elementValue, locationValue, initValue);


                        break;
                    }
            }            
            return Json(output, JsonRequestBehavior.AllowGet);
        }

        private string ReadFromInput(List<Dictionary<string,object>> list, string keyName)
        {
            Dictionary<string, object> dict = list.Find(x => x.Values.Contains(keyName));
            return Convert.ToString(dict["Value"].ToString());
        }
    }
}