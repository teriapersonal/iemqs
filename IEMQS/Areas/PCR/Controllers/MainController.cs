﻿using IEMQSImplementation;
using IEMQSImplementation.ILN.Domain.Models;
using IEMQSImplementation.ILN.Domain.Enums;
using IEMQS.Areas.ILN.Framework;
using IEMQS.Models;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace IEMQS.Areas.PCR.Controllers
{
    public class MainController : clsBase
    {        

        #region Main Route        
        public ActionResult Index(string processName)
        {
            string appName = "PCR";
            clsImplementationEnum.ILNAppName ilnAppName = (clsImplementationEnum.ILNAppName)Enum.Parse(typeof(clsImplementationEnum.ILNAppName), appName);
            ViewBag.AppName = appName;
            ViewBag.IsILNTabOpened = true;
            ViewResult viewResult;
            switch (ilnAppName)
            {
                case clsImplementationEnum.ILNAppName.PCR:
                    {
                        clsImplementationEnum.PCRProcess pCRProcess = (clsImplementationEnum.PCRProcess)Enum.Parse(typeof(clsImplementationEnum.PCRProcess), processName);
                        ViewBag.ILNProcess = pCRProcess.ToString();
                        ViewBag.ILNProcessDescription = pCRProcess.GetStringValue();
                        viewResult = View("~/Areas/" + appName + "/Views/Index.cshtml");
                        break;
                    }
                default:
                    {
                        ViewBag.ILNProcess = "Unknown";
                        ViewBag.ILNProcessDescription = null;
                        viewResult = View("~/Areas/ILN/Views/Index.cshtml");//default
                        break;
                    }
            }
            return viewResult;
           
        }
        #endregion

        #region Partial View Routes

        public virtual async Task<ActionResult> CommonViewResult(
            string appName, 
            string processName, 
            string referredTableName = null,
            string referredColumnName = null,
            string viewColumnValues = null,  
            string navigatorType = null, 
            int? navigatorIndex = null, 
            string indexName = null)
        {           
            Screen screen = await GetScreenData(appName, processName, referredTableName, referredColumnName, navigatorIndex, viewColumnValues, navigatorType, indexName);
            switch (screen.OccurenceType.Type)
            {
                case OccurenceTypeEnum.Multi:
                    {
                        if(screen.WindowTypeId == WindowTypeEnum.DynamicWindow)
                        {
                            return Json(screen, JsonRequestBehavior.AllowGet);
                        }
                        return PartialView("~/Areas/" + appName + "/Views/Main/_MultiOccurence.cshtml", screen);
                    }
            }
            return null;
        }



        #endregion

        #region Common Functions

        private async Task<Screen> GetScreenData(
            string appName, 
            string processName, 
            string referredTableName = null, 
            string referredColumnName = null, 
            int? navigatorIndex = null, 
            string viewColumnValues = null, 
            string navigatorType = null, 
            string indexName = null)
        {
            ScreenConfiguration screenConfiguration = new ScreenConfiguration(appName);
            Screen screen = screenConfiguration.GetScreen(processName, referredTableName, referredColumnName);           
            string userId = objClsLoginInfo.UserName;
            string procedureName = screen.OccurenceType.ProcedureName;
            switch (screen.OccurenceType.Type)
            {
                case OccurenceTypeEnum.Multi:
                    {
                        indexName = indexName ?? screen.MainSession.Table.Indices.Where(x => x.IsDefault == true).FirstOrDefault().Name;
                        Index index = screen.MainSession.Table.Indices.Where(x => x.Name == indexName).FirstOrDefault();
                        string indexColumnNames = string.Join(",", index.Columns.OrderBy(x => x.Order).ToList().Select(x => x.Name).ToList());
                        string tableName = screen.MainSession.TableName;
                        List<string> viewColumnsList = screen.MainSession.MultiOccurenceInstance.ViewColumns?.Select(x => x.Name).ToList();
                        string viewColumnNames = viewColumnsList != null ? string.Join(",", viewColumnsList) : null;
                        List<GridColumn> tabularColumns = GetGridColumns(screen.MainSession);
                        string columns = string.Join(",", tabularColumns.Select(x => x.Name).ToList());
                        int resultsCount = screen.OccurenceType.ResultsCount;
                        bool isLNTable = screen.MainSession.Table.IsLNTable;
                        screen.DataInDictionaryForm = await(new DataAdapter()).GetMultiOccurenceData(userId, procedureName, resultsCount, tableName, isLNTable, navigatorIndex, indexName, indexColumnNames, viewColumnNames, viewColumnValues, navigatorType, columns);
                        if (screen.DataInDictionaryForm.Count > 0)
                        {                            
                            SetViewColumns(screen.MainSession, screen.DataInDictionaryForm[0]);                            
                            if (screen.DataInDictionaryForm.Count > 1)
                            {
                                SetTableData(screen);
                                SetDataTablesConfig(screen, screen.MainSession, tabularColumns);
                                if (screen.DataInDictionaryForm.Count == 3)
                                {
                                    RefreshToolbarItemsState(screen);
                                }
                            }                           
                        }
                        break;
                    }
            }
            return screen;
        }

        private List<GridColumn> GetGridColumns(Session session)
        {
            List<string> viewColumnsList = session.MultiOccurenceInstance.ViewColumns?.Select(x => x.Name).ToList();            
            List<GridColumn> gridColumns = session.MultiOccurenceInstance.GridColumns;
            List<GridColumn> gridColumnsExclusive = null;
            if (viewColumnsList != null && viewColumnsList.Count > 0)
            {
                gridColumnsExclusive = new List<GridColumn>();
                foreach(GridColumn gridColumn in gridColumns)
                {
                    if(viewColumnsList.Contains(gridColumn.Name) == false)
                    {
                        gridColumnsExclusive.Add(gridColumn);
                    }
                }
                //gridColumns = gridColumns.Where(x => session.MultiOccurenceInstance.GridColumns.Select(a => a.Id).ToList().Except(session.MultiOccurenceInstance.ViewColumns.Select(a => a.Id).ToList()).Contains(x.Id)).ToList();
            }
            else
            {
                gridColumnsExclusive = gridColumns;
            }
            return gridColumnsExclusive?.OrderBy(x => x.DisplayOrder).ToList();
        }
        
        private void SetViewColumns(Session session, List<IDictionary<string, object>> data)
        {
            List<ViewColumn> viewColumns = session.MultiOccurenceInstance.ViewColumns;
            if(viewColumns!=null && viewColumns.Count > 0)
            {
                foreach (IDictionary<string, object> keyValues in data)
                {
                    string colName = Convert.ToString(keyValues["ColName"]);
                    if (string.IsNullOrEmpty(colName) == false)
                    {
                        ViewColumn viewColumn = viewColumns.Find(x => x.Name == colName);
                        if (viewColumn != null)
                        {
                            keyValues.Add("RefColName", viewColumn.ReferenceColumn);
                            keyValues.Add("RefTableName", viewColumn.ReferenceTable);
                            keyValues.Add("RefDescriptionColName", viewColumn.DescriptionColumn);
                            keyValues.Add("DependentOnColName", viewColumn.DependentOnColumn);
                            keyValues.Add("IsMandatory", viewColumn.IsMandatory);
                        }                        
                    }                   
                }
            }
            
        }

        private void RefreshToolbarItemsState(Screen screen)
        {
            var states = screen.DataInDictionaryForm[2];
            if (states.Count > 0)
            {
                foreach (List<ToolbarItem> lst in screen.ToolbarItems)
                {
                    foreach (ToolbarItem item in lst)
                    {
                        if (states[0].ContainsKey(item.Name))
                        {
                            item.State = Convert.ToString(states[0][item.Name]).ToUpper() == (Convert.ToString(true)).ToUpper() ? "Enabled" : "Disabled";
                        }
                    }
                }
            }
        }

        private void SetTableData(Screen screen)
        {
            screen.TableData = JsonConvert.SerializeObject(screen.DataInDictionaryForm[1]);
        }

        /// <summary>
        /// Prepare configuration used by datatables 
        /// </summary>
        private void SetDataTablesConfig(Screen screen, Session session, List<GridColumn> columns = null)
        {            
            dynamic configs = new ExpandoObject();
            dynamic columnConfigs = new ExpandoObject();
            List<int> colIndexes = null;
            columns = columns ?? GetGridColumns(session);
            MultiOccurence multiOccurence = session.MultiOccurenceInstance;
            bool isRowSelectable = multiOccurence.RowSelectionType.ToUpper() == RowSelectionTypeEnum.Multi.GetStringValue().ToUpper() || multiOccurence.RowSelectionType.ToUpper() == RowSelectionTypeEnum.Single.GetStringValue().ToUpper();
            bool isSelectAllRowsAllowed = multiOccurence.IsSelectAllRowsAllowedViewCheckbox;
            for (var cnt = 0; cnt < columns.Count; cnt++)
            {
                columns[cnt].DisplayOrder = multiOccurence.ShowCheckbox ? cnt + 1 : cnt;
            }

            colIndexes = columns.Where(x => (x.IsSearchable.HasValue == false) || (x.IsSearchable.HasValue && x.IsSearchable.Value == false)).ToList().Select(x => x.DisplayOrder).ToList();
            if (colIndexes != null && colIndexes.Count > 0)
            {
                columnConfigs.NonSearchable = new
                {
                    ColIndexes = string.Join(",", colIndexes),
                    Value = false
                };
            }
            colIndexes = columns.Where(x => (x.IsSortable.HasValue == false) || (x.IsSortable.HasValue && x.IsSortable.Value == false)).ToList().Select(x => x.DisplayOrder).ToList();
            if (colIndexes != null && colIndexes.Count > 0)
            {
                columnConfigs.NonSortable = new
                {
                    ColIndexes = string.Join(",", colIndexes),
                    Value = false
                };
            }
            colIndexes = columns.Where(x => (x.IsVisible.HasValue == false) || (x.IsVisible.HasValue && x.IsVisible.Value == false)).ToList().Select(x => x.DisplayOrder).ToList();
            if (colIndexes != null && colIndexes.Count > 0)
            {
                columnConfigs.InVisible = new
                {
                    ColIndexes = string.Join(",", colIndexes),
                    Value = false
                };
            }
            if (isRowSelectable)
            {
                configs.select = new
                {
                    style = multiOccurence.RowSelectionType
                };
            }
            else
            {
                configs.select = null;
            }
            if (multiOccurence.ShowCheckbox)
            {
                columnConfigs.Checkbox = new
                {
                    ColIndexes = 0,
                    Value = new
                    {
                        selectRow = isRowSelectable,
                        selectAll = isSelectAllRowsAllowed,
                        selectCallback = "fnOnRowSelect"
                    }
                };
            }
            
            List<dynamic> typeConfigList = new List<dynamic>();
            List<string> types = new List<string>() { "numeric", "string", "date" };
            foreach (string type in types)
            {
                colIndexes = columns.Where(x => x.Type.ToUpper() == type.ToUpper()).Select(x => x.DisplayOrder).ToList();
                if (colIndexes != null && colIndexes.Count > 0)
                {
                    dynamic typeConfig = new ExpandoObject();
                    typeConfig = new
                    {
                        ColIndexes = colIndexes,
                        Value = type
                    };
                    typeConfigList.Add(typeConfig);
                }                
            }
            columnConfigs.TypeConfigList = typeConfigList;

            configs.keys = multiOccurence.NavigateKeys;
            configs.ColumnConfigs = columnConfigs;
            configs.Columns = columns.Select(x => new { title = x.DisplayName, data = x.Name, name = x.Name }).ToList();
            screen.DataTablesColumnsConfig = JsonConvert.SerializeObject(configs);            
        }

        private bool CheckUserIsValid(string ActionName)
        {
            bool IsValid = false;
            if (objClsLoginInfo.listMenuResult != null)
            {
                var lstMenuResult = objClsLoginInfo.listMenuResult;
                if (lstMenuResult.Any(i => (string.IsNullOrEmpty(i.Action) ? "" : i.Action.ToLower()) == ActionName.ToLower()))
                {
                    IsValid = true;
                }
            }
            return IsValid;
        }

        #endregion
    }
}