﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQS.Areas.PCR.Controllers.MaintainStockController;

namespace IEMQS.Areas.PCR.Controllers
{
    public class MaintainPCRController : clsBase
    {
        public const string ProjectItemFetchSuccess = "Project item data successfully fetch from LN.";
        public const string NoRecordsFoundsInProjectItemFetch = "No new item found in LN.";
        public const string PCRDataUpdated = "PCR data updated successfully!";
        public const string PCRGenerateSuccess = "PCR generated successfully!";
        public const string PCRReleaseSuccess = "PCR released successfully!";
        public const string PCRRetractSuccess = "PCR retract successfully!";
        public const string NonPlateItemRestrict = "You can not update non-plate item details!";
        public const string BulkUpdatePCRDetails = "PCR type, date, location and W/C successfully update in bulk!";
        public const string PCRRequirementDateInvalid = "Please select PCR requirement date greater than today!";
        public const string MaterialPlannerPCRDataUpdated = "Material planner PCR data updated successfully!";
        public const string MaterialPlannerPCRForReleaseDataUpdated = "Material planner PCR For Release data updated successfully!";
        public const string UnAthorizedForUpdateData = "You are un-authorized for update data!";
        public const string MaterialPlannerPCRNotFound = "Material planner PCR details not found!";
        public const string PLTType = "PLT";
        public const string MaterialPlannerPCRAction = "MaterialPlannerPCR";
        public const string GeneratePCRAction = "GeneratePCR";
        public const string QCPCRAction = "QCPCR";
        public const string PCLGenerateSuccess = "PCL generated successfully!";
        public const string PCLReleaseSuccess = "PCL released successfully!";
        public const string PCLNotGenerated = "PCL not generated yet!";
        public const string PCRNotGenerated = "PCR not generated yet!";
        public const string PCLNotInGeneratedStatus = "PCL is not in PCL Generated status!";
        public const string NonPlateReturnBalanceDistribution = "You can not submit return balance distribution for non-plate";
        public const string ReturnBalanceDistributionSave = "Return balance distribution save successfully!";
        public const string ReturnBalanceDistributionUpdate = "Return balance distribution updated successfully!";
        public const string ReturnBalanceDistributionNotExist = "Return balance distribution line not exist!";
        public const string ReturnBalanceDistributionDelete = "Return balance distribution delete successfully!";
        public const string ReturnBalanceDistributionMaxLinesMaintain = "Return balance distribution maximun lines already maintain!";
        public const string PCLApprovedByQASuccess = "PCL Approved successfully!";
        public const string SomeDataInvalidForUpdate = "Some Data Is Invalid For Update!";
        public const string StockNotAttached = "Stock not attached yet!";
        public const string StockUnbindSuccess = "Stock unbind successfully!";
        public const string StockUnbindInReleaseToMaterialPlannerStatus = "You Can Only Unbind Stock In Release To Material Planner Status!";
        public const string PCRReturnSuccess = "PCR Return Successfully!";


        //// GET: PCR/MaintainPCR
        //[SessionExpireFilter]
        //public ActionResult Index()
        //{
        //    ViewBag.Location = db.COM002.Where(i => i.t_dtyp == 1).ToList().Select(i => new { CatID = i.t_dimx, CatDesc = i.t_dimx + "-" + i.t_desc }).ToList();
        //    ViewBag.WorkCenterDeliver = db.SP_GENERAL_GET_CODE_DESCRIPTION_FROM_LN_TABLE("ttirou001", "t_cwoc", "t_dsca").ToList().Select(i => new { CatID = i.Code, CatDesc = i.CodeDescription }).ToList();
        //    ViewBag.CuttingLocation = db.SP_GENERAL_GET_CODE_DESCRIPTION_FROM_LN_TABLE("tltsfc513", "t_loca", "t_cloc").ToList().Select(i => new { CatID = i.Code, CatDesc = i.Description }).ToList();
        //    ViewBag.PCRType = clsImplementationEnum.getPCRType().Select(i => i).ToList();
        //    ViewBag.PLTType = PLTType;
        //    return View();
        //}
       
        #region Generate PCR

        #region Display & Edit Grid & Fetch Data
        [SessionExpireFilter, UserPermissions, LastURLHistory]
        public ActionResult GeneratePCR()
        {
            ViewBag.Role = "PLNG3";
            ViewBag.PLTType = PLTType;
            return View("Index");
        }

        [SessionExpireFilter]
        public PartialViewResult _GeneratePCR()
        {
            bool IsValidUser = CheckUserIsValid(GeneratePCRAction);
            ViewBag.PCRIsValidUser = IsValidUser;
            ViewBag.Location = db.COM002.Where(i => i.t_dtyp == 1).ToList().Select(i => new { CatID = i.t_dimx, CatDesc = i.t_dimx + "-" + i.t_desc }).ToList();
            ViewBag.WorkCenterDeliver = db.SP_GENERAL_GET_CODE_DESCRIPTION_FROM_LN_TABLE("ttirou001", "t_cwoc", "t_dsca").ToList().Select(i => new { CatID = i.Code, CatDesc = i.CodeDescription }).ToList();
            ViewBag.CuttingLocation = db.SP_GENERAL_GET_CODE_DESCRIPTION_FROM_LN_TABLE("tltsfc513", "t_loca", "t_cloc").ToList().Select(i => new { CatID = i.Code, CatDesc = i.Description }).ToList();
            ViewBag.PCRType = clsImplementationEnum.getPCRType().Select(i => i).ToList();
            return PartialView("_GeneratePCR");
        }

        [SessionExpireFilter]
        public ActionResult LoadPCRDataTable(JQueryDataTableParamModel param, string status)
        {

            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                string Project = Request["Project"].ToString();
                whereCondition += " and Project='" + Project + "'";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName ={"Project",
                                          "PartNumber",
                                          "ChildItem",
                                          "Length",
                                          "Width",
                                          "LengthModified",
                                          "WidthModified",
                                          "NoOfPieces",
                                          "RequiredQuantity",
                                          "ISNULL(PCRNumber,'')",
                                          "Sequence",
                                          "PCRLineNo",
                                          "PCRType",
                                          "WCDeliver",
                                          "PlannerRemark",
                                          "Priority",
                                          "LocationName",
                                          "ItemRevision",
                                          "ItemType",
                                          "SubLocation",
                                          "ReturnRemark",
                                          "Description",
                                          "PCRTypeName",
                                          "WCDeliverName",
                                          "Thickness",
                                          "ItemMaterialGradeName",
                                          "ISNULL(p3.PCRLineStatus,'')"
                                         };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_PCR_GET_PCR_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var isDisplay = true;
                var distinctRecords = lstHeader.Select(i => new { i.PartGroupNo }).Distinct().ToList();

                var res = new List<dynamic>();
                foreach (var item in distinctRecords)
                {
                    var hasMultiRecords = false;
                    var list = (from h in lstHeader
                                where h.PartGroupNo == item.PartGroupNo
                                select h).OrderByDescending(i => i.Sequence).ToList();
                    if (list.Count > 1)
                    {
                        hasMultiRecords = true;
                    }
                    foreach (var h in list)
                    {
                        res.Add(new
                        {
                            Id = Convert.ToString(h.Id),
                            ChildItem = Convert.ToString(h.ChildItem),
                            Project = Convert.ToString(h.Project),
                            PartNumber = (h.SequenceSrNo == 1 ? (hasMultiRecords ? "<img data-gid=\"" + Convert.ToString(h.PartGroupNo.Value) + "\" onclick=\"ExpandCollapsChild(this)\" class=\"colexp\" src=\"/Images/details_open.png\" style=\"height:17px;\" data-isexpand=\"false\" />" : "") + Convert.ToString(h.PartNumber) : ""),
                            BudgetLine = Convert.ToString(h.PartNumber),
                            FindNo = Convert.ToString(h.PartNumber),
                            Length = Convert.ToString(h.Length),
                            Width = Convert.ToString(h.Width),
                            LengthModified = GetLengthModifiedColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.LengthModified, h.PCRTypeName, isDisplay),
                            WidthModified = GetWidthModifiedColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.WidthModified, h.PCRTypeName, isDisplay),
                            NoOfPieces = Convert.ToString(h.NoOfPieces),
                            RequiredQuantity = GetRequiredQuantityColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.RequiredQuantity, isDisplay),
                            PCRNumber = Convert.ToString(h.PCRNumber),
                            Sequence = Convert.ToString(h.Sequence),
                            PCRLineNo = Convert.ToString(h.PCRLineNo),
                            PCRType = GetPCRTypeColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.PCRTypeName, h.PCRType, isDisplay),
                            PCRRequirementDate = GetPCRRequirementDateColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.PCRRequirementDate, isDisplay),
                            WCDeliver = GetWCDeliverColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.WCDeliverName, h.WCDeliver, isDisplay),
                            PlannerRemark = GetPlannerRemarkColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.PlannerRemark, isDisplay),
                            CuttingLocation = GetCuttingLocationColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.CuttingLocation, isDisplay),
                            Priority = GetPriorityColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.Priority, isDisplay),//Convert.ToString(h.Priority),
                            Location = GetLocationColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.LocationName, h.Location, isDisplay),
                            ItemRevision = Convert.ToString(h.ItemRevision),
                            ItemType = Convert.ToString(h.ItemType),
                            SubLocation = GetSubLocationColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.SubLocation, isDisplay),
                            ReturnBy = Convert.ToString(h.ReturnBy),
                            ReturnRemark = Convert.ToString(h.ReturnRemark),
                            Description = Convert.ToString(h.Description),
                            PCRTypeName = Convert.ToString(h.PCRTypeName),
                            WCDeliverName = Convert.ToString(h.WCDeliverName),
                            //CuttingLocationName = Convert.ToString(h.CuttingLocationName),
                            ItemMaterialGrade = h.ItemMaterialGradeName,
                            Thickness = Convert.ToString(h.Thickness),
                            RequiredForJIGFIX = Convert.ToString(h.RequiredForJIGFIX),
                            //Action = Helper.HTMLActionString(Convert.ToInt32(h.Id), "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + h.Id + ");"),
                            //ElementCheckBox = GetElementCheckBoxSelectionColumnDetails(Convert.ToInt32(h.Id),h.ItemType,false),
                            IsEnableElementSelection = IsEnableElementSelection(h.PCRNumber, h.PCRType, h.RequiredQuantity, h.PCRRequirementDate, h.Location, h.WCDeliver, h.CuttingLocation, h.Priority),
                            SequenceSrNo = h.SequenceSrNo,
                            PartGroupNo = h.PartGroupNo.Value,
                            PCRLineId = h.PCRLineId,
                            PCRLineStatus = h.PCRLineStatus
                        });
                    }
                }

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, HttpPost]
        public JsonResult LoadPCREditableData(int id, string ProductCode, bool isReadOnly = false)
        {
            //var data = new List<string[]>();
            try
            {
                if (id > 0)
                {
                    var isDisplay = isReadOnly;
                    var objPCREditable = db.SP_PCR_GET_PCR_DATA(1, 0, "", "p4.Id = " + id).Take(1).ToList();
                    if (objPCREditable.FirstOrDefault().ItemType != PLTType)
                    {
                        isDisplay = true;
                    }
                    var data = (from h in objPCREditable
                                select new
                                {
                                    Id = Convert.ToString(h.Id),
                                    ChildItem = Convert.ToString(h.ChildItem),
                                    Project = Convert.ToString(h.Project),
                                    PartNumber = Convert.ToString(h.PartNumber),
                                    BudgetLine = Convert.ToString(h.PartNumber),
                                    FindNo = Convert.ToString(h.PartNumber),
                                    Length = Convert.ToString(h.Length),
                                    Width = Convert.ToString(h.Width),
                                    LengthModified = GetLengthModifiedColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.LengthModified, h.PCRTypeName, isDisplay),
                                    WidthModified = GetWidthModifiedColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.WidthModified, h.PCRTypeName, isDisplay),
                                    NoOfPieces = Convert.ToString(h.NoOfPieces),
                                    RequiredQuantity = GetRequiredQuantityColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.RequiredQuantity, isDisplay),
                                    PCRNumber = Convert.ToString(h.PCRNumber),
                                    Sequence = Convert.ToString(h.Sequence),
                                    PCRLineNo = Convert.ToString(h.PCRLineNo),
                                    PCRType = GetPCRTypeColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.PCRTypeName, h.PCRType, isDisplay),
                                    PCRRequirementDate = GetPCRRequirementDateColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.PCRRequirementDate, isDisplay),
                                    WCDeliver = GetWCDeliverColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.WCDeliverName, h.WCDeliver, isDisplay),
                                    PlannerRemark = GetPlannerRemarkColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.PlannerRemark, isDisplay),
                                    CuttingLocation = GetCuttingLocationColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.CuttingLocation, isDisplay),
                                    Priority = GetPriorityColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.Priority, isDisplay),// Convert.ToString(h.Priority),
                                    Location = GetLocationColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.LocationName, h.Location, isDisplay),
                                    ItemRevision = Convert.ToString(h.ItemRevision),
                                    ItemType = Convert.ToString(h.ItemType),
                                    SubLocation = GetSubLocationColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.SubLocation, isDisplay),
                                    ReturnBy = Convert.ToString(h.ReturnBy),
                                    ReturnRemark = Convert.ToString(h.ReturnRemark),
                                    Description = Convert.ToString(h.Description),
                                    PCRTypeName = Convert.ToString(h.PCRTypeName),
                                    WCDeliverName = Convert.ToString(h.WCDeliverName),
                                    //CuttingLocationName = Convert.ToString(h.CuttingLocationName),
                                    ItemMaterialGrade = h.ItemMaterialGradeName,
                                    Thickness = Convert.ToString(h.Thickness),
                                    RequiredForJIGFIX = Convert.ToString(h.RequiredForJIGFIX),
                                    //Action = Helper.HTMLActionString(Convert.ToInt32(h.Id), "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + h.Id + ");"),
                                    //ElementCheckBox = GetElementCheckBoxSelectionColumnDetails(Convert.ToInt32(h.Id),h.ItemType,isDisplay),
                                    IsEnableElementSelection = IsEnableElementSelection(h.PCRNumber, h.PCRType, h.RequiredQuantity, h.PCRRequirementDate, h.Location, h.WCDeliver, h.CuttingLocation, h.Priority),
                                    SequenceSrNo = h.SequenceSrNo,
                                    PartGroupNo = h.PartGroupNo.Value,
                                    PCRLineId = h.PCRLineId,
                                    PCRLineStatus = h.PCRLineStatus
                                }).ToList();

                    return Json(new
                    {
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new
                    {
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public JsonResult FetchProjectDataFromLN(string Project)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(GeneratePCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var affectedRows = db.SP_PCR_INSERT_PROJECT_FETCH_DATA_FOR_PCR(Project, objClsLoginInfo.UserName);
                if (affectedRows > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = ProjectItemFetchSuccess;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = NoRecordsFoundsInProjectItemFetch;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        
        [SessionExpireFilter]
        public PartialViewResult _MaterailPlanner()
        {
            ViewBag.IsILNTabOpened = true;
            return PartialView("_MaterailPlanner");
        }
        #endregion

        #region Save & Validate PCR
        [SessionExpireFilter, HttpPost]
        public JsonResult SavePCRDetails(PCR004 objPCR004)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(GeneratePCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (objPCR004.Id > 0)
                {
                    var objPCR004Update = db.PCR004.Where(i => i.Id == objPCR004.Id).FirstOrDefault();
                    if (objPCR004Update.ItemType == PLTType)
                    {
                        if (objPCR004.PCRRequirementDate != null && objPCR004.PCRRequirementDate.HasValue && objPCR004.PCRRequirementDate.Value < DateTime.Now)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.IsInformation = true;
                            objResponseMsg.Value = PCRRequirementDateInvalid;
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                        if (objPCR004Update != null)
                        {
                            objPCR004Update.PlannerRemark = objPCR004.PlannerRemark;
                            objPCR004Update.CuttingLocation = objPCR004.CuttingLocation;
                            objPCR004Update.SubLocation = objPCR004.SubLocation;
                            objPCR004Update.WCDeliver = objPCR004.WCDeliver;
                            objPCR004Update.Location = objPCR004.Location;
                            objPCR004Update.LengthModified = objPCR004.LengthModified;
                            objPCR004Update.WidthModified = objPCR004.WidthModified;
                            objPCR004Update.RequiredQuantity = objPCR004.RequiredQuantity;
                            objPCR004Update.Priority = objPCR004.Priority;
                            if (!string.IsNullOrEmpty(objPCR004.PCRType))
                            {
                                if (db.PCR000.Any(i => i.EnumName == "PCR Type" && i.LNDomainName == "ltsfc.pcr.type" && i.Text == objPCR004.PCRType))
                                {
                                    objPCR004Update.PCRType = db.PCR000.Where(i => i.EnumName == "PCR Type" && i.LNDomainName == "ltsfc.pcr.type" && i.Text == objPCR004.PCRType).FirstOrDefault().Value.Value.ToString();
                                }
                            }
                            if (objPCR004.PCRRequirementDate != null && objPCR004.PCRRequirementDate.HasValue)
                            {
                                objPCR004Update.PCRRequirementDate = objPCR004.PCRRequirementDate.Value;
                            }
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = PCRDataUpdated;
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = NonPlateItemRestrict;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, HttpPost]
        public JsonResult SaveGeneratePCR(string Ids)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(GeneratePCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
                var listLocation = listAllPCR004.Select(i => i.Location).Distinct().ToList();
                var Validate = ValidateGeneratePCRBudgetLineSelection(Ids);
                var objValidate = ((dynamic)Validate.Data);
                if (objValidate.Key != null && objValidate.Key)
                {
                    foreach (var Location in listLocation)
                    {
                        var listPCR004 = listAllPCR004.Where(i => i.Location == Location && i.ItemType == PLTType).ToList();
                        PCR002 objPCR002 = new PCR002();
                        objPCR002.CreatedBy = objClsLoginInfo.UserName;
                        objPCR002.CreatedOn = DateTime.Now;
                        objPCR002.Location = Location;
                        double MaxDocNo = 1;
                        var PCRNumber = GeneratePCRNo(Location, ref MaxDocNo);
                        objPCR002.PCRNumber = PCRNumber;
                        objPCR002.PCRDocNo = MaxDocNo;

                        foreach (var item in listPCR004)
                        {
                            PCR003 objPCR003 = new PCR003();
                            objPCR003.Area = Convert.ToDouble(decimal.Round((decimal)((item.LengthModified.Value * item.WidthModified.Value) / 1000000), 4, MidpointRounding.AwayFromZero));
                            objPCR003.ChildItem = item.ChildItem;
                            objPCR003.CreatedBy = objClsLoginInfo.UserName;
                            objPCR003.PCRCreatedBy = objClsLoginInfo.UserName;
                            objPCR003.PCRCreationDate = DateTime.Now;
                            objPCR003.CreatedOn = DateTime.Now;
                            objPCR003.ItemRevision = item.ItemRevision;
                            objPCR003.ItemType = item.ItemType;
                            objPCR003.Length = item.LengthModified;
                            objPCR003.PartNumber = item.PartNumber;
                            objPCR003.PCRCreationDate = DateTime.Now;
                            objPCR003.PCRLineRevNo = 0;
                            objPCR003.PCRLineStatus = clsImplementationEnum.PCRLineStatus.Created.GetStringValue();
                            objPCR003.PCRPlannerRemark = item.PlannerRemark;
                            objPCR003.PCRRequirementDate = item.PCRRequirementDate;
                            objPCR003.PCRType = item.PCRType;
                            objPCR003.Project = item.Project;
                            objPCR003.Thickness = item.Thickness;
                            objPCR003.WarehouseCuttingLocation = item.CuttingLocation;
                            objPCR003.WCDeliver = item.WCDeliver;
                            objPCR003.Width = item.WidthModified;
                            objPCR003.NoofPieces = item.RequiredQuantity;
                            objPCR003.RefId = item.Id;
                            objPCR003.ScrapQuantity = 0;
                            objPCR003.NestedQuantity = 0;
                            objPCR003.AreaConsumed = 0;
                            objPCR003.StockRevision = 0;
                            objPCR003.MovementType = clsImplementationEnum.MovementType.ProfileCutting.GetHashCode();
                            objPCR003.ManufacturingItemMinusPosition = 0;
                            objPCR003.UseAlternateStock = "No";
                            objPCR003.InspectionQty = 0;
                            objPCR003.RejectedQty = 0;
                            objPCR003.P1_OD = 0;
                            objPCR003.P2_Length = 0;
                            objPCR003.P3_Width = 0;
                            objPCR003.P4_OD = 0;
                            objPCR003.P5_ID = 0;
                            objPCR003.P6_OD = 0;
                            objPCR003.P7_ID = 0;
                            objPCR003.P8_Deg = 0;
                            objPCR003.P9 = 0;
                            objPCR003.P10 = 0;
                            objPCR003.P1_OD = 0;
                            objPCR003.P1_OD = 0;
                            objPCR002.PCR003.Add(objPCR003);
                        }
                        db.PCR002.Add(objPCR002);
                        db.SaveChanges();

                        foreach (var item in listPCR004)
                        {
                            if (item.NoOfPieces - item.RequiredQuantity > 0)
                            {
                                var objPCR004New = new PCR004();
                                objPCR004New.ChildItem = item.ChildItem;
                                objPCR004New.Description = item.Description;
                                objPCR004New.EditedBy = null;
                                objPCR004New.EditedOn = null;
                                objPCR004New.ItemMaterialGrade = item.ItemMaterialGrade;
                                objPCR004New.ItemRevision = item.ItemRevision;
                                objPCR004New.ItemType = item.ItemType;
                                objPCR004New.Length = item.Length;
                                objPCR004New.LengthModified = item.LengthModified;
                                objPCR004New.ManufacturingItem = item.ManufacturingItem;
                                objPCR004New.PartNumber = item.PartNumber;
                                objPCR004New.Project = item.Project;
                                objPCR004New.ReturnBy = null;
                                objPCR004New.ReturnDate = null;
                                objPCR004New.ReturnRemark = null;
                                objPCR004New.Thickness = item.Thickness;
                                objPCR004New.Width = item.Width;
                                objPCR004New.WidthModified = item.WidthModified;
                                objPCR004New.CreatedBy = objClsLoginInfo.UserName;
                                objPCR004New.CreatedOn = DateTime.Now;
                                objPCR004New.CuttingLocation = item.CuttingLocation;
                                objPCR004New.Location = item.Location;
                                objPCR004New.NoOfPieces = item.NoOfPieces; //item.NoOfPieces - item.RequiredQuantity;
                                //objPCR004New.PCRLineId = null;
                                objPCR004New.PCRLineNo = null;
                                objPCR004New.PCRNumber = null;
                                objPCR004New.PCRRequirementDate = null;
                                objPCR004New.PCRType = clsImplementationEnum.PCRType.Drawing.GetHashCode().ToString();
                                objPCR004New.PlannerRemark = null;
                                objPCR004New.Priority = item.Priority;
                                objPCR004New.RequiredForJIGFIX = null;
                                objPCR004New.RequiredQuantity = item.NoOfPieces - item.RequiredQuantity;
                                objPCR004New.Sequence = item.Sequence + 1;
                                objPCR004New.SubLocation = item.SubLocation;
                                objPCR004New.WCDeliver = null;
                                db.PCR004.Add(objPCR004New);
                            }
                        }

                        int PCRLineNo = 10;
                        foreach (var item in objPCR002.PCR003)
                        {
                            var objPCR004 = listPCR004.Where(i => i.ChildItem == item.ChildItem && i.PartNumber == item.PartNumber && i.Project == item.Project).FirstOrDefault();
                            //objPCR004.PCRLineId = item.LineId;
                            objPCR004.PCRLineNo = PCRLineNo;
                            objPCR004.PCRNumber = objPCR002.PCRNumber;
                            objPCR004.EditedBy = objClsLoginInfo.UserName;
                            objPCR004.EditedOn = DateTime.Now;
                            item.PCRLineNo = PCRLineNo;
                            PCRLineNo += 10;

                            (new clsFileUpload()).CopyFolderContentsAsync("PCR004/" + objPCR004.Id, "PCR003/" + item.LineId);
                        }
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = PCRGenerateSuccess;
                }
                else
                {
                    objResponseMsg.Key = objValidate.Key != null ? objValidate.Key : false;
                    objResponseMsg.Value = objValidate.Value != null ? objValidate.Value : "Some Data Invalid Or Already PCR Generated For Generate PCR!";
                    objResponseMsg.IsInformation = objValidate.IsInformation != null ? objValidate.IsInformation : true;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, HttpPost]
        public JsonResult ValidateGeneratePCRBudgetLineSelection(string Ids)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
                var invalidElement = "";
                var alreadyPCRGenerated = "";
                var invalidItemType = "";
                foreach (var item in listAllPCR004)
                {
                    if (item.ItemType != PLTType)
                    {
                        Manager.ConcatString(ref invalidItemType, item.PartNumber.Value.ToString());
                    }
                    else if (!string.IsNullOrEmpty(item.PCRNumber))
                    {
                        Manager.ConcatString(ref alreadyPCRGenerated, item.PartNumber.Value.ToString() + "-" + item.Sequence.Value.ToString());
                    }
                    else if (!IsEnableElementSelection(item.PCRNumber, item.PCRType, item.RequiredQuantity, item.PCRRequirementDate, item.Location, item.WCDeliver, item.CuttingLocation, item.Priority))
                    {
                        Manager.ConcatString(ref invalidElement, item.PartNumber.Value.ToString() + "-" + item.Sequence.Value.ToString());
                    }
                }
                if (invalidElement.Length > 0 || alreadyPCRGenerated.Length > 0 || invalidItemType.Length > 0)
                {
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = (alreadyPCRGenerated.Length > 0 ? "PCR Already Generated For " + alreadyPCRGenerated + "  Budget Line-Sequence." : "") +
                                           (invalidElement.Length > 0 ? "Please Fill All PCR Related Data For " + invalidElement + " Budget Line-Sequence." : "") +
                                           (invalidItemType.Length > 0 ? invalidItemType + " No. Budget Line Is Non-Plate." : "") +
                                           " Please Unselect That All Budget Line-Sequence!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Bulk Update PCR Data
        [SessionExpireFilter, HttpPost]
        public PartialViewResult _UpdateBulkPCRGenerateData(string Ids)
        {
            ViewBag.Ids = Ids;
            return PartialView("_UpdateBulkPCRGenerateData");
        }

        [SessionExpireFilter, HttpPost]
        public JsonResult UpdateBulkPCRGenerateDetails(string Ids, string PCRType, string PCRRequirementDate, string Location,
                                                string WCDeliver, string CuttingLocation, string SubLocation, string PlannerRemark)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(GeneratePCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id) && string.IsNullOrEmpty(i.PCRNumber)).ToList();
                var Validate = ValidateBulkUpdateBudgetLineSelection(Ids);
                var objValidate = ((dynamic)Validate.Data);
                if (objValidate.Key != null && objValidate.Key)
                {
                    if (!string.IsNullOrEmpty(PCRRequirementDate) && Convert.ToDateTime(PCRRequirementDate) <= DateTime.Now)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = PCRRequirementDateInvalid;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    foreach (var item in listAllPCR004)
                    {
                        if (!string.IsNullOrEmpty(PCRType))
                        {
                            if (db.PCR000.Any(i => i.EnumName == "PCR Type" && i.LNDomainName == "ltsfc.pcr.type" && i.Text == PCRType))
                            {
                                item.PCRType = db.PCR000.Where(i => i.EnumName == "PCR Type" && i.LNDomainName == "ltsfc.pcr.type" && i.Text == PCRType).FirstOrDefault().Value.Value.ToString();
                            }
                        }
                        if (!string.IsNullOrEmpty(PCRRequirementDate))
                        {
                            item.PCRRequirementDate = Convert.ToDateTime(PCRRequirementDate);
                        }
                        item.Location = Location;
                        item.WCDeliver = WCDeliver;
                        item.CuttingLocation = CuttingLocation;
                        item.SubLocation = SubLocation;
                        item.PlannerRemark = PlannerRemark;
                        item.EditedBy = objClsLoginInfo.UserName;
                        item.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = BulkUpdatePCRDetails;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objResponseMsg.Key = objValidate.Key != null ? objValidate.Key : false;
                    objResponseMsg.Value = objValidate.Value != null ? objValidate.Value : "Some Data Invalid For Bulk Update PCR Details!";
                    objResponseMsg.IsInformation = objValidate.IsInformation != null ? objValidate.IsInformation : true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, HttpPost]
        public JsonResult ValidateBulkUpdateBudgetLineSelection(string Ids)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
                var invalidItemType = "";
                var alreadyPCRGenerated = "";
                foreach (var item in listAllPCR004)
                {
                    if (item.ItemType != PLTType)
                    {
                        Manager.ConcatString(ref invalidItemType, item.PartNumber.Value.ToString());
                    }
                    else if (!string.IsNullOrEmpty(item.PCRNumber))
                    {
                        Manager.ConcatString(ref alreadyPCRGenerated, item.PartNumber.Value.ToString() + "-" + item.Sequence.Value.ToString());
                    }
                }
                if (alreadyPCRGenerated.Length > 0)
                {
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = (alreadyPCRGenerated.Length > 0 ? "PCR Already Generated For " + alreadyPCRGenerated + " Budget Line-Sequence. You Can Not Update It." : "") +
                                            (invalidItemType.Length > 0 ? invalidItemType + " No. Budget Line Is Non-Plate." : "") +
                                            " Please Unselect That All Budget Line-Sequence!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Release PCR
        [SessionExpireFilter, HttpPost]
        public JsonResult ValidateReleasePCRBudgetLineSelection(string Ids)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
                var pcrNotGenerated = "";
                var pcrNonCreateStatus = "";
                var invalidItemType = "";
                var attachmentNotExist = "";
                var listAllPCR003 = db.PCR003.Where(i => arrayIds.Contains(i.RefId.Value)).ToList();

                foreach (var item in listAllPCR004)
                {
                    if (item.ItemType != PLTType)
                    {
                        Manager.ConcatString(ref invalidItemType, item.PartNumber.Value.ToString());
                    }
                    else if (!listAllPCR003.Any(i => i.RefId == item.Id))
                    {
                        Manager.ConcatString(ref pcrNotGenerated, item.PartNumber.Value.ToString() + "-" + item.Sequence.Value.ToString());
                    }
                    else
                    {
                        var objPCR003 = listAllPCR003.Where(i => i.RefId == item.Id).FirstOrDefault();
                        if (!(new clsFileUpload()).CheckAnyFileUploadInFolder("PCR003/" + objPCR003.LineId))
                        {
                            Manager.ConcatString(ref attachmentNotExist, item.PartNumber.Value.ToString() + "-" + item.Sequence.Value.ToString());
                        }
                    }
                }

                //decimal[] arrayPCRLineIds = listAllPCR004.Select(i => Convert.ToDecimal(i.PCRLineId)).ToArray();              
                foreach (var item in listAllPCR003)
                {
                    if (item.PCRLineStatus != clsImplementationEnum.PCRLineStatus.Created.GetStringValue())
                    {
                        var objPCR004 = listAllPCR004.Where(i => i.Id == item.RefId).FirstOrDefault();
                        Manager.ConcatString(ref pcrNonCreateStatus, item.PartNumber.Value.ToString() + "-" + objPCR004.Sequence.Value.ToString());
                    }
                }
                if (pcrNotGenerated.Length > 0 || pcrNonCreateStatus.Length > 0 || invalidItemType.Length > 0 ||
                    attachmentNotExist.Length > 0)
                {
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = (pcrNotGenerated.Length > 0 ? "PCR Not Generated For " + pcrNotGenerated + " Budget Line-Sequence." : "") +
                                           (invalidItemType.Length > 0 ? invalidItemType + " No. Budget Line Is Non-Plate." : "") +
                                           (attachmentNotExist.Length > 0 ? " Attachment Required For Release " + attachmentNotExist + " Budget Line-Sequence." : "") +
                                           (pcrNonCreateStatus.Length > 0 ? pcrNonCreateStatus + " Budget Line-Sequence Is Already Released." : "") +
                                           " Please Unselect That All Budget Line-Sequence!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ReleasePCR(string Ids)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(GeneratePCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
                var Validate = ValidateReleasePCRBudgetLineSelection(Ids);
                var objValidate = ((dynamic)Validate.Data);
                if (objValidate.Key != null && objValidate.Key)
                {
                    //decimal[] arrayPCRLineIds = listAllPCR004.Select(i => Convert.ToDecimal(i.PCRLineId)).ToArray();
                    var listAllPCR003 = db.PCR003.Where(i => arrayIds.Contains(i.RefId.Value)).ToList();
                    foreach (var item in listAllPCR003)
                    {
                        item.PCRReleaseDate = DateTime.Now;
                        item.PCRLineStatus = clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner.GetStringValue();
                        item.PCRReleasedBy = objClsLoginInfo.UserName;
                        item.PCRRetractBy = null;
                        item.PCRRetractOn = null;
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = PCRReleaseSuccess;
                }
                else
                {
                    objResponseMsg.Key = objValidate.Key != null ? objValidate.Key : false;
                    objResponseMsg.Value = objValidate.Value != null ? objValidate.Value : "Some Data Invalid Or Already PCR Release For Release PCR!";
                    objResponseMsg.IsInformation = objValidate.IsInformation != null ? objValidate.IsInformation : true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Retract PCR
        [SessionExpireFilter, HttpPost]
        public JsonResult ValidateRetractPCRBudgetLineSelection(string Ids)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
                var pcrNotGenerated = "";
                var pcrNonCreateStatus = "";
                var invalidItemType = "";
                var stockAttached = "";
                var listAllPCR003 = db.PCR003.Where(i => arrayIds.Contains(i.RefId.Value)).ToList();
                foreach (var item in listAllPCR004)
                {
                    if (item.ItemType != PLTType)
                    {
                        Manager.ConcatString(ref invalidItemType, item.PartNumber.Value.ToString());
                    }
                    else if (!listAllPCR003.Any(i => i.RefId == item.Id))
                    {
                        Manager.ConcatString(ref pcrNotGenerated, item.PartNumber.Value.ToString() + "-" + item.Sequence.Value.ToString());
                    }
                }

                //decimal[] arrayPCRLineIds = listAllPCR004.Select(i => Convert.ToDecimal(i.PCRLineId)).ToArray();

                foreach (var item in listAllPCR003)
                {
                    var objPCR004 = listAllPCR004.Where(i => i.Id == item.RefId).FirstOrDefault();
                    //if (item.PCRLineStatus != clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner.GetStringValue())
                    //{
                    //    Manager.ConcatString(ref pcrNonCreateStatus, item.PartNumber.Value.ToString() + "-" + objPCR004.Sequence.Value.ToString());
                    //}
                    if (!string.IsNullOrEmpty(item.StockNumber))
                    {
                        Manager.ConcatString(ref stockAttached, item.PartNumber.Value.ToString() + "-" + objPCR004.Sequence.Value.ToString());
                    }
                }
                if (pcrNotGenerated.Length > 0 || pcrNonCreateStatus.Length > 0 || invalidItemType.Length > 0 || stockAttached.Length > 0)
                {
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = (pcrNotGenerated.Length > 0 ? "PCR Not Generated For " + pcrNotGenerated + " Budget Line-Sequence." : "") +
                                           (invalidItemType.Length > 0 ? invalidItemType + " No. Budget Line Is Non-Plate." : "") +
                                           (stockAttached.Length > 0 ? "Material Planner Has Attach Stock To " + stockAttached + " Budget Line-Sequence." : "") +
                                           (pcrNonCreateStatus.Length > 0 ? pcrNonCreateStatus + " Budget Line-Sequence Is Not In Release To Material Planner Status." : "") +
                                           " Please Unselect That All Budget Line-Sequence!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RetractPCR(string Ids)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(GeneratePCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
                var Validate = ValidateRetractPCRBudgetLineSelection(Ids);
                var objValidate = ((dynamic)Validate.Data);
                if (objValidate.Key != null && objValidate.Key)
                {
                    //decimal[] arrayPCRLineIds = listAllPCR004.Select(i => Convert.ToDecimal(i.PCRLineId)).ToArray();
                    var listAllPCR003 = db.PCR003.Where(i => arrayIds.Contains(i.RefId.Value)).ToList();
                    foreach (var item in listAllPCR003)
                    {
                        item.PCRReleaseDate = null;
                        item.PCRLineStatus = clsImplementationEnum.PCRLineStatus.Created.GetStringValue();
                        item.PCRReleasedBy = null;
                        item.PCRRetractBy = objClsLoginInfo.UserName;
                        item.PCRRetractOn = DateTime.Now;
                        item.PCLCreatedBy = null;
                        item.PCLCreationDate = null;
                        item.PCLNumber = null;
                        item.PCLReleaseDate = null;
                        item.PCLReleasedBy = null;
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = PCRRetractSuccess;
                }
                else
                {
                    objResponseMsg.Key = objValidate.Key != null ? objValidate.Key : false;
                    objResponseMsg.Value = objValidate.Value != null ? objValidate.Value : "Some Data Invalid Or PCR Line Status Is Not Release To Material Planner Status For Release PCR!";
                    objResponseMsg.IsInformation = objValidate.IsInformation != null ? objValidate.IsInformation : true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Material Planner PCR

        #region Display & Edit Grid
        [SessionExpireFilter, UserPermissions, LastURLHistory]
        public ActionResult MaterialPlannerPCR()
        {
            ViewBag.PLTType = PLTType;
            ViewBag.Role = "MATL3";
            return View("Index");
        }
        [SessionExpireFilter]
        public PartialViewResult _MaterialPlannerPCR()
        {
            bool IsValidUser = CheckUserIsValid(MaterialPlannerPCRAction);
            ViewBag.MATL3IsValidUser = IsValidUser;
            ViewBag.WorkCenterDeliver = db.SP_GENERAL_GET_CODE_DESCRIPTION_FROM_LN_TABLE("ttirou001", "t_cwoc", "t_dsca").ToList().Select(i => new { CatID = i.Code, CatDesc = i.CodeDescription }).ToList();
            ViewBag.MovementType = clsImplementationEnum.getMovementType().Select(i => i).ToList();
            return PartialView("_MaterialPlannerPCR");
        }

        [SessionExpireFilter]
        public ActionResult LoadMaterialPlannerPCRDataTable(JQueryDataTableParamModel param, decimal? PCLHeaderId)
        {

            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                //string Project = Request["Project"].ToString();
                //whereCondition += " and Project='" + Project + "'";
                if (PCLHeaderId.HasValue)
                {
                    whereCondition += " and p6.PCLHeaderId=" + PCLHeaderId.Value;
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName ={
                                            "p4.Project",
                                            "p4.PartNumber",
                                            "p4.ChildItem",
                                            "p4.Sequence",
                                            "p4.ItemType",
                                            "PCRCreatedByName",
                                            "ISNULL(p4.PCRNumber,'')",
                                            "p4.PCRLineNo",
                                            "ISNULL(p3.PCRLineStatus,'')",
                                            "p3.[Length]",
                                            "p3.Width",
                                            "ISNULL(p3.PCRRequirementDate,p4.PCRRequirementDate)",
                                            "p3.NestedQuantity",
                                            "p3.AreaConsumed",
                                            "WCDeliverName",
                                            "ISNULL(p3.UseAlternateStock,'')",
                                            "ISNULL(p3.StockNumber,'')",
                                            "p3.AlternateStockRemark",
                                            "p3.ShapeType",
                                            "p3.ShapeFile",
                                            "MovementTypeName",
                                            "p3.GrainOrientation",
                                            "p3.MaterialPlannerRemark",
                                            "ItemMaterialGradeName",
                                            "p3.NoofPieces",
                                            "PCRTypeName",
                                            "p3.Thickness",
                                            "p4.RequiredQuantity",
                                            "p3.PCRLineRevNo",
                                            "ISNULL(p3.PCLNumber,'')",
                                            "p6.ScrapQuantityInm2",
                                            "p6.ReturnBalanceInPcs",
                                            "p6.ReturnAreaInm2",
                                            "p6.TotalCuttingLength",
                                            "PlannerName",
                                            "p6.Remark",
                                            "LocationName",
                                            "p4.SubLocation",
                                            "p3.WarehouseCuttingLocation",
                                            "p4.[Priority]",
                                            "p3.PCRPlannerRemark"
                                         };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_PCR_GET_PCR_MATERIAL_PLANNER_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var isDisplay = true;
                var distinctRecords = lstHeader.Select(i => new { i.PartGroupNo }).Distinct().ToList();

                var res = new List<dynamic>();
                foreach (var item in distinctRecords)
                {
                    var hasMultiRecords = false;
                    var list = (from h in lstHeader
                                where h.PartGroupNo == item.PartGroupNo
                                select h).OrderByDescending(i => i.Sequence).ToList();
                    if (list.Count > 1)
                    {
                        hasMultiRecords = true;
                    }
                    foreach (var h in list)
                    {
                        res.Add(new
                        {
                            Id = Convert.ToString(h.Id),
                            Project = Convert.ToString(h.Project),
                            //PartNumber = Convert.ToString(h.PartNumber),
                            PartNumber = (h.SequenceSrNo == 1 ? (hasMultiRecords ? "<img data-gid=\"" + Convert.ToString(h.PartGroupNo.Value) + "\" onclick=\"ExpandCollapsChildMaterialPlannerPCR(this)\" class=\"colexp\" src=\"/Images/details_open.png\" style=\"height:17px;\" data-isexpand=\"false\" />" : "") + Convert.ToString(h.PartNumber) : ""),
                            ChildItem = Convert.ToString(h.ChildItem),
                            Length = Convert.ToString(h.Length),
                            Width = Convert.ToString(h.Width),
                            ARMCode = Convert.ToString(h.ARMCode),
                            NoofPieces = Convert.ToString(h.NoofPieces),
                            PCRType = Convert.ToString(h.PCRType),
                            Thickness = Convert.ToString(h.Thickness),
                            RequiredQuantity = Convert.ToString(h.RequiredQuantity),
                            PCRRequirementDate = GetPCRRequirementDateMPPCRColumnDetails(h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.PCRRequirementDate, isDisplay),
                            NestedQuantity = Convert.ToString(h.NestedQuantity),
                            PCRCreatedBy = Convert.ToString(h.PCRCreatedBy),
                            PCRNumber = Convert.ToString(h.PCRNumber),
                            PCRLineNo = Convert.ToString(h.PCRLineNo),
                            PCRSequenceNo = Convert.ToString(h.PCRSequenceNo),
                            MovementType = GetMovementTypeMPPCRColumnDetails(h.PCRLineStatus, h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.MovementTypeName, h.MovementType, isDisplay),
                            PCLNumber = Convert.ToString(h.PCLNumber),
                            UseAlternateStock = Convert.ToString(h.UseAlternateStock),
                            AreaConsumed = Convert.ToString(h.AreaConsumed),
                            ReturnBalanceInPcs = Convert.ToString(h.ReturnBalanceInPcs),
                            ReturnAreaInm2 = Convert.ToString(h.ReturnAreaInm2),
                            TotalCuttingLength = Convert.ToString(h.TotalCuttingLength),
                            Location = Convert.ToString(h.Location),
                            WCDeliver = h.WCDeliver + (!string.IsNullOrEmpty(h.WCDeliverName) ? "-" + Convert.ToString(h.WCDeliverName) : ""),
                            SubLocation = Convert.ToString(h.SubLocation),
                            WarehouseCuttingLocation = Convert.ToString(h.WarehouseCuttingLocation),
                            Machine = Convert.ToString(h.Machine),
                            Week = Convert.ToString(h.Week),
                            Priority = Convert.ToString(h.Priority),
                            Duration = Convert.ToString(h.Duration),
                            StartDate = Convert.ToString(h.StartDate),
                            EndDate = Convert.ToString(h.EndDate),
                            PCRPlannerRemark = Convert.ToString(h.PCRPlannerRemark),
                            ReturnRemark = Convert.ToString(h.ReturnRemark),
                            AlternateStockRemark = Convert.ToString(h.AlternateStockRemark),
                            LineId = Convert.ToString(h.LineId),
                            PCRTypeName = Convert.ToString(h.PCRTypeName),
                            WCDeliverName = Convert.ToString(h.WCDeliverName),
                            ItemMaterialGradeName = Convert.ToString(h.ItemMaterialGradeName),
                            ItemMaterialGrade = Convert.ToString(h.ItemMaterialGrade),
                            LocationName = Convert.ToString(h.LocationName),
                            PCRLineStatus = Convert.ToString(h.PCRLineStatus),
                            PCRLineRevNo = Convert.ToString(h.PCRLineRevNo),
                            HeaderId = Convert.ToString(h.HeaderId),
                            ItemRevision = Convert.ToString(h.ItemRevision),
                            ItemType = Convert.ToString(h.ItemType),
                            LinkToSCR = Convert.ToString(h.LinkToSCR),
                            MovementTypeName = Convert.ToString(h.MovementTypeName),
                            ShapeFile = Convert.ToString(h.ShapeFile),
                            ShapeType = Convert.ToString(h.ShapeType),
                            ShopFloorWarhouse = Convert.ToString(h.ShopFloorWarhouse),
                            StockNumber = !string.IsNullOrEmpty(h.StockNumber) ? (Convert.ToString(h.StockNumber) + (h.StockRevision.HasValue ? "-R" + Convert.ToString(h.StockRevision) : "")) : "",
                            StockRevision = Convert.ToString(h.StockRevision),
                            Sequence = Convert.ToString(h.Sequence),
                            SequenceSrNo = h.SequenceSrNo,
                            PartGroupNo = h.PartGroupNo.Value,
                            GrainOrientation = Convert.ToString(h.GrainOrientation),
                            MaterialPlannerRemark = Convert.ToString(h.MaterialPlannerRemark),
                            ScrapQuantityInm2 = Convert.ToString(h.ScrapQuantityInm2),
                            Planner = Convert.ToString(h.Planner),
                            Remark = Convert.ToString(h.Remark),
                            PCLHeaderId = Convert.ToString(h.PCLHeaderId),
                        });
                    }
                }

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, HttpPost]
        public JsonResult LoadMaterialPlannerPCREditableData(int id, string lineId, bool isReadOnly = false)
        {
            //var data = new List<string[]>();
            try
            {
                if (id > 0)
                {
                    var isDisplay = isReadOnly;
                    var objPCREditable = new List<SP_PCR_GET_PCR_MATERIAL_PLANNER_DATA_Result>();
                    if (lineId == "0")
                    {
                        objPCREditable = db.SP_PCR_GET_PCR_MATERIAL_PLANNER_DATA(1, 0, "", "p4.Id = " + id).Take(1).ToList();
                    }
                    else
                    {
                        objPCREditable = db.SP_PCR_GET_PCR_MATERIAL_PLANNER_DATA(1, 0, "", "p3.LineId = " + lineId).Take(1).ToList();
                    }
                    if (objPCREditable.FirstOrDefault().ItemType != PLTType)
                    {
                        isDisplay = true;
                    }
                    var data = (from h in objPCREditable
                                select new
                                {
                                    Id = Convert.ToString(h.Id),
                                    Project = Convert.ToString(h.Project),
                                    //PartNumber = Convert.ToString(h.PartNumber),
                                    PartNumber = (h.Sequence > 0 ? "<img data-gid=\"" + Convert.ToString(h.PartGroupNo.Value) + "\" onclick=\"ExpandCollapsChildMaterialPlannerPCR(this)\" class=\"colexp\" src=\"/Images/details_open.png\" style=\"height:17px;\" data-isexpand=\"false\" />" + Convert.ToString(h.PartNumber) : Convert.ToString(h.PartNumber)),
                                    ChildItem = Convert.ToString(h.ChildItem),
                                    Length = GetLengthModifiedMPPCRColumnDetails(h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.Length, h.PCRTypeName, isDisplay),
                                    Width = GetWidthModifiedMPPCRColumnDetails(h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.Width, h.PCRTypeName, isDisplay),
                                    ARMCode = Convert.ToString(h.ARMCode),
                                    NoofPieces = Convert.ToString(h.NoofPieces),
                                    PCRType = Convert.ToString(h.PCRType),
                                    Thickness = Convert.ToString(h.Thickness),
                                    RequiredQuantity = Convert.ToString(h.RequiredQuantity),
                                    PCRRequirementDate = GetPCRRequirementDateMPPCRColumnDetails(h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.PCRRequirementDate, isDisplay),
                                    NestedQuantity = GetNestedQuantityMPPCRColumnDetails(h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.NestedQuantity, isDisplay),//Convert.ToString(h.NestedQuantity),
                                    PCRCreatedBy = Convert.ToString(h.PCRCreatedBy),
                                    PCRNumber = Convert.ToString(h.PCRNumber),
                                    PCRLineNo = Convert.ToString(h.PCRLineNo),
                                    PCRSequenceNo = Convert.ToString(h.PCRSequenceNo),
                                    //MovementType = Convert.ToString(h.MovementType),
                                    PCLNumber = Convert.ToString(h.PCLNumber),
                                    UseAlternateStock = Convert.ToString(h.UseAlternateStock),
                                    AreaConsumed = GetAreaConsumedMPPCRColumnDetails(h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.AreaConsumed, h.PCRTypeName, isDisplay),
                                    ReturnBalanceInPcs = GetReturnBalancePCSMPPCRColumnDetails(h.PCRLineStatus, h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.ReturnBalanceInPcs, isDisplay), //Convert.ToString(h.ReturnBalanceInPcs),
                                    ReturnAreaInm2 = Convert.ToString(h.ReturnAreaInm2),
                                    TotalCuttingLength = GetTotalCuttingLengthMPPCRColumnDetails(h.PCRLineStatus, h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.TotalCuttingLength, isDisplay),//Convert.ToString(h.TotalCuttingLength),
                                    Location = Convert.ToString(h.Location),
                                    WCDeliver = GetWCDeliverMPPCRColumnDetails(h.PCRLineStatus, h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.WCDeliverName, h.WCDeliver, isDisplay),
                                    SubLocation = Convert.ToString(h.SubLocation),
                                    WarehouseCuttingLocation = Convert.ToString(h.WarehouseCuttingLocation),
                                    Machine = Convert.ToString(h.Machine),
                                    Week = Convert.ToString(h.Week),
                                    Priority = Convert.ToString(h.Priority),
                                    Duration = Convert.ToString(h.Duration),
                                    StartDate = Convert.ToString(h.StartDate),
                                    EndDate = Convert.ToString(h.EndDate),
                                    PCRPlannerRemark = Convert.ToString(h.PCRPlannerRemark),
                                    ReturnRemark = Convert.ToString(h.ReturnRemark),
                                    AlternateStockRemark = GetAlternateStockRemarkMPPCRColumnDetails(h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.AlternateStockRemark, h.UseAlternateStock, isDisplay),
                                    LineId = Convert.ToString(h.LineId),
                                    PCRTypeName = Convert.ToString(h.PCRTypeName),
                                    WCDeliverName = Convert.ToString(h.WCDeliverName),
                                    ItemMaterialGradeName = Convert.ToString(h.ItemMaterialGradeName),
                                    ItemMaterialGrade = Convert.ToString(h.ItemMaterialGrade),
                                    LocationName = Convert.ToString(h.LocationName),
                                    PCRLineStatus = Convert.ToString(h.PCRLineStatus),
                                    PCRLineRevNo = Convert.ToString(h.PCRLineRevNo),
                                    HeaderId = Convert.ToString(h.HeaderId),
                                    ItemRevision = Convert.ToString(h.ItemRevision),
                                    ItemType = Convert.ToString(h.ItemType),
                                    LinkToSCR = Convert.ToString(h.LinkToSCR),
                                    MovementType = GetMovementTypeMPPCRColumnDetails(h.PCRLineStatus, h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.MovementTypeName, h.MovementType, isDisplay),
                                    ShapeFile = GetShapeFileMPPCRColumnDetails(h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.ShapeFile, isDisplay),
                                    ShapeType = GetShapeTypeMPPCRColumnDetails(h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.ShapeType, isDisplay),
                                    ShopFloorWarhouse = Convert.ToString(h.ShopFloorWarhouse),
                                    StockNumber = GetStockNumberMPPCRColumnDetails(h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.StockNumber, h.StockRevision, isDisplay),//Convert.ToString(h.StockNumber),
                                    StockRevision = Convert.ToString(h.StockRevision),
                                    Sequence = Convert.ToString(h.Sequence),
                                    SequenceSrNo = h.SequenceSrNo,
                                    PartGroupNo = h.PartGroupNo.Value,
                                    MaterialPlannerRemark = GetMaterialPlannerRemarkMPPCRColumnDetails(h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.MaterialPlannerRemark, isDisplay),
                                    GrainOrientation = GetGrainOrientationMPPCRColumnDetails(h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.GrainOrientation, isDisplay),
                                    ScrapQuantityInm2 = Convert.ToString(h.ScrapQuantityInm2),
                                    Planner = GetPlannerMPPCRColumnDetails(h.PCRLineStatus, h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.Planner, isDisplay),
                                    Remark = GetRemarkMPPCRColumnDetails(h.PCRLineStatus, h.PCRNumber, h.PCLNumber, Convert.ToInt32(h.Id), h.ItemType, h.Remark, isDisplay),
                                    PCLHeaderId = Convert.ToString(h.PCLHeaderId),
                                }).ToList();

                    return Json(new
                    {
                        aaData = data
                    }, JsonRequestBehavior.AllowGet);

                }
                else
                {
                    return Json(new
                    {
                        aaData = new string[] { }
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Generate and Validate PCL
        [SessionExpireFilter, HttpPost]
        public JsonResult SaveMaterialPlannerPCRDetails(PCR003 objPCR003, int Id, int LineId, string MovementTypeName)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(MaterialPlannerPCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var obj = new PCR003();
                if (LineId > 0)
                {
                    obj = db.PCR003.Where(i => i.LineId == LineId).FirstOrDefault();
                }
                else
                {
                    obj = db.PCR003.Where(i => i.RefId == Id).FirstOrDefault();
                }
                if (obj.LineId > 0)
                {
                    var objPCR003Update = db.PCR003.Where(i => i.LineId == obj.LineId).FirstOrDefault();
                    if (objPCR003Update == null)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = MaterialPlannerPCRNotFound;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (objPCR003Update.ItemType == PLTType)
                    {
                        if (objPCR003.PCRRequirementDate != null && objPCR003.PCRRequirementDate.HasValue && objPCR003.PCRRequirementDate.Value < DateTime.Now)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.IsInformation = true;
                            objResponseMsg.Value = PCRRequirementDateInvalid;
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                        else if (!objPCR003.AreaConsumed.HasValue)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.IsInformation = true;
                            objResponseMsg.Value = "Please Enter value for Area Consumed";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                        if (objPCR003Update != null)
                        {
                            var CurrentStockNumber = objPCR003Update.StockNumber;
                            var CurrentStockRevision = objPCR003Update.StockRevision;
                            var CurrentAreaConsume = objPCR003Update.AreaConsumed;

                            var objPCR001Old = db.PCR001.Where(i => i.StockNumber == CurrentStockNumber && i.StockRevision == CurrentStockRevision).FirstOrDefault();
                            var objPCR001New = db.PCR001.Where(i => i.StockNumber == objPCR003.StockNumber && i.StockRevision == objPCR003.StockRevision).FirstOrDefault();
                            if (objPCR001New.Area < objPCR003.AreaConsumed + objPCR001New.AllocatedQuantity - objPCR003Update.AreaConsumed)
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.IsInformation = true;
                                objResponseMsg.Value = "Total Area Consumed for Stock " + objPCR003.StockNumber + " in PCR Lines exceed Total Area available "+ objPCR001New.Area + ".";
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }

                            objPCR003Update.Length = objPCR003.Length;
                            objPCR003Update.Width = objPCR003.Width;
                            objPCR003Update.NestedQuantity = objPCR003.NestedQuantity;
                            objPCR003Update.WCDeliver = objPCR003.WCDeliver;
                            objPCR003Update.StockNumber = objPCR003.StockNumber;
                            objPCR003Update.StockRevision = objPCR003.StockRevision;
                            objPCR003Update.ShapeFile = objPCR003.ShapeFile;
                            objPCR003Update.ShapeType = objPCR003.ShapeType;
                            objPCR003Update.AreaConsumed = objPCR003.AreaConsumed;
                            objPCR003Update.GrainOrientation = objPCR003.GrainOrientation;
                            objPCR003Update.MaterialPlannerRemark = objPCR003.MaterialPlannerRemark;
                            objPCR003Update.UseAlternateStock = objPCR003.UseAlternateStock;
                            objPCR003Update.AlternateStockRemark = objPCR003.AlternateStockRemark;
                            if (!string.IsNullOrEmpty(MovementTypeName))
                            {
                                if (db.PCR000.Any(i => i.EnumName == "Movement Type" && i.LNDomainName == "ltmov.type" && i.Text == MovementTypeName))
                                {
                                    objPCR003Update.MovementType = db.PCR000.Where(i => i.EnumName == "Movement Type" && i.LNDomainName == "ltmov.type" && i.Text == MovementTypeName).FirstOrDefault().Value.Value;
                                }
                            }
                            else
                            {
                                objPCR003Update.MovementType = null;
                            }
                            if (objPCR003.PCRRequirementDate != null && objPCR003.PCRRequirementDate.HasValue)
                            {
                                objPCR003Update.PCRRequirementDate = objPCR003.PCRRequirementDate.Value;
                            }
                            if (objPCR003Update.StockNumber != CurrentStockNumber && objPCR003Update.StockRevision != CurrentStockRevision)
                            {
                                objPCR001Old.AllocatedQuantity = (objPCR001Old.AllocatedQuantity.HasValue ? objPCR001Old.AllocatedQuantity.Value : 0)
                                                                - (CurrentAreaConsume.HasValue ? CurrentAreaConsume.Value : 0);
                                objPCR001Old.FreeQuantity = (objPCR001Old.FreeQuantity.HasValue ? objPCR001Old.FreeQuantity.Value : 0)
                                                                + (CurrentAreaConsume.HasValue ? CurrentAreaConsume.Value : 0);
                            }

                            CurrentAreaConsume = 0;
                            objPCR001New.AllocatedQuantity = (objPCR001New.AllocatedQuantity.HasValue ? objPCR001New.AllocatedQuantity.Value : 0)
                                                            + (objPCR003Update.AreaConsumed.HasValue ? objPCR003Update.AreaConsumed.Value : 0)
                                                            - (CurrentAreaConsume.HasValue ? CurrentAreaConsume.Value : 0);
                            objPCR001New.FreeQuantity = (objPCR001New.Area.HasValue ? objPCR001New.Area.Value : 0)
                                                        - (objPCR001New.AllocatedQuantity.HasValue ? objPCR001New.AllocatedQuantity.Value : 0)
                                                        - (objPCR001New.CutQuantity.HasValue ? objPCR001New.CutQuantity.Value : 0);

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = MaterialPlannerPCRDataUpdated;
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = NonPlateItemRestrict;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ValidateGeneratePCLBudgetLineSelection(PCR004 objPCR004, PCR003 objPCR003)//(string Ids)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                //decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                //var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
                var invalidElement = "";
                var alreadyPCLGenerated = "";
                var invalidItemType = "";
                var PCRNotGenerated = "";
                //foreach (var item in listAllPCR004)
                //{
                if (objPCR004.ItemType != PLTType)
                {
                    Manager.ConcatString(ref invalidItemType, objPCR004.PartNumber.Value.ToString());
                }
                else
                {
                    //var objPCR003 = db.PCR003.Where(i => i.RefId == item.Id).FirstOrDefault();
                    if (objPCR003 == null)
                    {
                        Manager.ConcatString(ref PCRNotGenerated, objPCR004.PartNumber.Value.ToString() + "-" + objPCR004.Sequence.Value.ToString());
                    }
                    else
                    {
                        //var objPCR003 = db.PCR003.Where(i => i.LineId == item.PCRLineId).FirstOrDefault();
                        if (!string.IsNullOrEmpty(objPCR003.PCLNumber))
                        {
                            Manager.ConcatString(ref alreadyPCLGenerated, objPCR003.PartNumber.Value.ToString() + "-" + objPCR004.Sequence.Value.ToString());
                        }
                        else if (!IsEnableMPPCRElementSelection(objPCR003.PCLNumber, objPCR003.PCRRequirementDate, objPCR003.NestedQuantity, objPCR003.StockNumber, objPCR003.UseAlternateStock, objPCR003.AlternateStockRemark))
                        {
                            Manager.ConcatString(ref invalidElement, objPCR004.PartNumber.Value.ToString() + "-" + objPCR004.Sequence.Value.ToString());
                        }
                    }
                }
                //}
                if (invalidElement.Length > 0 || PCRNotGenerated.Length > 0 || invalidItemType.Length > 0 || alreadyPCLGenerated.Length > 0)
                {
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = (PCRNotGenerated.Length > 0 ? "PCR Not Generated For " + PCRNotGenerated + "  Budget Line-Sequence." : "") +
                                           (invalidElement.Length > 0 ? "Please Fill All PCL Related Data For " + invalidElement + " Budget Line-Sequence." : "") +
                                           (alreadyPCLGenerated.Length > 0 ? "PCL Already Generated For " + alreadyPCLGenerated + " Budget Line-Sequence." : "") +
                                           (invalidItemType.Length > 0 ? invalidItemType + " No. Budget Line Is Non-Plate." : "");
                    //+" Please Unselect That All Budget Line-Sequence!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, HttpPost]
        public JsonResult SaveGeneratePCL(CommonParamEnt param)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(MaterialPlannerPCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                //decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                //var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();param
                var objPCR004 = db.PCR004.Where(i => i.Id == param.Id).FirstOrDefault();
                PCR003 objPCR003 = null;
                if (param.LineId.HasValue)
                {
                    objPCR003 = db.PCR003.Where(i => i.LineId == param.LineId).FirstOrDefault();
                }
                var Validate = ValidateGeneratePCLBudgetLineSelection(objPCR004, objPCR003);
                var objValidate = ((StockResponseMsg)Validate.Data);
                if (objValidate.Key)
                {
                    //var listLocation = listAllPCR004.Select(i => i.Location).Distinct().ToList();
                    var objLNDtl = db.SP_GENERAL_GET_CODE_DESCRIPTION_FROM_LN_TABLE("tltlnt000", "t_dimx", "t_ipcl").ToList();
                    //foreach (var Location in listLocation)
                    //{
                    //var listPCR004 = listAllPCR004.Where(i => i.Location == Location && i.ItemType == PLTType).ToList();
                    var PCLSeriesStart = objLNDtl.Where(i => i.Code == objPCR004.Location).FirstOrDefault().Description;
                    //foreach (var item in listPCR004)
                    //{

                    //var objPCR003 = db.PCR003.Where(i => i.RefId == item.Id).FirstOrDefault();

                    var objPCR001 = db.PCR001.Where(i => i.StockNumber == objPCR003.StockNumber && i.StockRevision == objPCR003.StockRevision).FirstOrDefault();
                    objPCR003.PCLNumber = PCLSeriesStart + objPCR003.StockNumber.ToString() + objPCR003.StockRevision.Value.ToString();
                    objPCR003.PCLCreatedBy = objClsLoginInfo.UserName;
                    objPCR003.PCLCreationDate = DateTime.Now;
                    objPCR003.PCRLineStatus = clsImplementationEnum.PCRLineStatus.PCLGenerated.GetStringValue();

                    objPCR001.PCLNo = objPCR003.PCLNumber;
                    objPCR001.StockStatus = clsImplementationEnum.StockStatus.Allocated.GetHashCode();

                    if (!db.PCR006.Any(i => i.PCLNumber == objPCR003.PCLNumber))
                    {
                        PCR006 objPCR006New = new PCR006();
                        objPCR006New.PCLNumber = objPCR003.PCLNumber;
                        objPCR006New.PCLDate = DateTime.Now;
                        objPCR006New.Revision = 0;
                        objPCR006New.Planner = "";
                        objPCR006New.TotalAreaInm2 = objPCR001.AllocatedQuantity;
                        objPCR006New.ReturnAreaInm2 = objPCR001.Area - objPCR001.AllocatedQuantity;
                        objPCR006New.PCLStatus = null;
                        objPCR006New.PlateReceiveOrder = "";
                        objPCR006New.ScrapQuantityInm2 = 0;
                        objPCR006New.ReturnBalanceInPcs = 0;
                        objPCR006New.CreatedBy = objClsLoginInfo.UserName;
                        objPCR006New.CreatedOn = DateTime.Now;
                        objPCR006New.ItemType = objPCR003.ItemType;
                        objPCR006New.MovementType = objPCR003.MovementType;
                        objPCR006New.PCLStatus = clsImplementationEnum.PCRLineStatus.PCLGenerated.GetStringValue();
                        objPCR006New.WorkCenter = objPCR003.WCDeliver;
                        objPCR006New.StockNumber = objPCR003.StockNumber;
                        objPCR006New.StockRevision = objPCR003.StockRevision;
                        objPCR006New.ActualAreaConsumedInm2 = 0;
                        objPCR006New.ActualScrapQuantityInm2 = 0;
                        objPCR006New.ActualScrapQuantityInm2 = 0;
                        objPCR006New.IsOddShape = false;
                        objPCR006New.TotalCuttingLength = 0;
                        AddPCLHistory(objPCR006New, clsImplementationEnum.PCRLineStatus.PCLGenerated.GetStringValue());
                        db.PCR006.Add(objPCR006New);
                    }
                    //}
                    db.SaveChanges();
                    //foreach (var item in listPCR004)
                    //{
                    //    var objPCR003 = db.PCR003.Where(i => i.RefId == item.Id).FirstOrDefault();
                    var objPCR006 = db.PCR006.Where(i => i.PCLNumber == objPCR003.PCLNumber).FirstOrDefault();
                    objPCR003.PCLHeaderId = objPCR006.PCLHeaderId;
                    //}
                    db.SaveChanges();
                    //}
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = PCLGenerateSuccess;
                }
                else
                {
                    objResponseMsg.Key = objValidate.Key;
                    objResponseMsg.Value = objValidate.Value != null ? objValidate.Value : SomeDataInvalidForUpdate;
                    objResponseMsg.IsInformation = objValidate.IsInformation;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Release & Validate PCL
        public JsonResult SaveMaterialPlannerReleasePCLDetails(PCR006 objPCR006, int Id, int LineId, string MovementTypeName)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(MaterialPlannerPCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var obj = new PCR003();
                if (LineId > 0)
                {
                    obj = db.PCR003.Where(i => i.LineId == LineId).FirstOrDefault();
                }
                else
                {
                    obj = db.PCR003.Where(i => i.RefId == Id).FirstOrDefault();
                }
                if (obj != null)
                {
                    var objPCR003Update = db.PCR003.Where(i => i.LineId == obj.LineId).FirstOrDefault();
                    var objPCR006Update = db.PCR006.Where(i => i.PCLHeaderId == objPCR006.PCLHeaderId).FirstOrDefault();
                    if (objPCR003Update == null || objPCR006Update == null)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = MaterialPlannerPCRNotFound;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (objPCR006Update.ItemType == PLTType)
                    {
                        if (objPCR006Update.PCLStatus == clsImplementationEnum.PCRLineStatus.PCLGenerated.GetStringValue())
                        {
                            objPCR006Update.WorkCenter = objPCR006.WorkCenter;
                            objPCR006Update.ReturnBalanceInPcs = objPCR006.ReturnBalanceInPcs;
                            objPCR006Update.TotalCuttingLength = objPCR006.TotalCuttingLength;
                            objPCR006Update.Planner = objPCR006.Planner;
                            objPCR006Update.Remark = objPCR006.Remark;

                            if (!string.IsNullOrEmpty(MovementTypeName))
                            {
                                if (db.PCR000.Any(i => i.EnumName == "Movement Type" && i.LNDomainName == "ltmov.type" && i.Text == MovementTypeName))
                                {
                                    objPCR006Update.MovementType = db.PCR000.Where(i => i.EnumName == "Movement Type" && i.LNDomainName == "ltmov.type" && i.Text == MovementTypeName).FirstOrDefault().Value.Value;
                                }
                            }
                            else
                            {
                                objPCR006Update.MovementType = null;
                            }
                            objPCR006Update.EditedBy = objClsLoginInfo.UserName;
                            objPCR006Update.EditedOn = DateTime.Now;

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = MaterialPlannerPCRForReleaseDataUpdated;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = PCLNotInGeneratedStatus;
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = NonPlateItemRestrict;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, HttpPost]
        public JsonResult SaveReleasePCL(CommonParamEnt param)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(MaterialPlannerPCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                //decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                //var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
                var objPCR004 = db.PCR004.Where(i => i.Id == param.Id).FirstOrDefault();
                PCR003 objPCR003 = null;
                PCR006 objPCR006 = null;
                if (param.LineId.HasValue && param.LineId.Value > 0)
                {
                    objPCR003 = db.PCR003.Where(i => i.LineId == param.LineId).FirstOrDefault();
                }
                if (param.HeaderId.HasValue && param.HeaderId.Value > 0)
                {
                    objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == param.HeaderId).FirstOrDefault();
                }

                var Validate = ValidateReleasePCLBudgetLine(objPCR004, objPCR003, objPCR006);
                var objValidate = ((StockResponseMsg)Validate.Data);
                if (objValidate.Key)
                {
                    //foreach (var item in listAllPCR004)
                    //{
                    //    var objPCR003 = db.PCR003.Where(i => i.RefId == item.Id).FirstOrDefault();
                    //    var objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == objPCR003.PCLHeaderId).FirstOrDefault();
                    var objPCR001 = db.PCR001.Where(i => i.StockNumber == objPCR003.StockNumber && i.StockRevision == objPCR003.StockRevision).FirstOrDefault();

                    UpdateTotalAreaInm2PCL(objPCR006.PCLHeaderId);

                    var CurrentScrapQuantityInm2 = objPCR006.ScrapQuantityInm2;
                    #region Update Current PCR
                    var DeletePCR = clsImplementationEnum.PCRLineStatus.Deleted.GetStringValue();
                    var listPCR003 = db.PCR003.Where(i => i.PCLNumber == objPCR006.PCLNumber && i.PCRLineStatus != DeletePCR).ToList();
                    var res1 = new List<dynamic>();
                    foreach (var objPCR003Update in listPCR003)
                    {
                        var ScrapQuantityInm2 = (objPCR003.AreaConsumed * objPCR006.ScrapQuantityInm2) / objPCR006.TotalAreaInm2;
                        objPCR003Update.ScrapQuantity = Math.Round((ScrapQuantityInm2.HasValue ? ScrapQuantityInm2.Value : 0), 4);
                        objPCR003Update.PCLReleaseDate = DateTime.Now;
                        objPCR003Update.MovementType = objPCR006.MovementType;
                        objPCR003Update.WCDeliver = objPCR006.WorkCenter;                        
                        objPCR003Update.PCRLineStatus = clsImplementationEnum.PCLStatus.PCLReleased.GetStringValue();
                        objPCR003Update.PCLReleasedBy = objClsLoginInfo.UserName;
                        objPCR003Update.EditedBy = objClsLoginInfo.UserName;
                        objPCR003Update.EditedOn = DateTime.Now;
                    }
                    #endregion

                    #region Auto Revise PCR
                    AddRevisedPCR(listPCR003);
                    #endregion

                    #region Update Stock Details
                    objPCR001.FreeQuantity = objPCR001.FreeQuantity - CurrentScrapQuantityInm2;
                    //AddAutoReviseStock(objPCR006);
                    #endregion

                    #region Update PCL Status
                    objPCR006.PCLStatus = clsImplementationEnum.PCLStatus.PCLReleased.GetStringValue();
                    objPCR006.PCLReleaseBy = objClsLoginInfo.UserName;
                    objPCR006.PCLReleaseDate = DateTime.Now;
                    AddPCLHistory(objPCR006, clsImplementationEnum.PCLStatus.PCLReleased.GetStringValue());
                    #endregion

                    db.SaveChanges();
                    //}

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = PCLReleaseSuccess;
                }
                else
                {
                    objResponseMsg.Key = objValidate.Key;
                    objResponseMsg.Value = objValidate.Value != null ? objValidate.Value : SomeDataInvalidForUpdate;
                    objResponseMsg.IsInformation = objValidate.IsInformation;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ValidateReleasePCLBudgetLine(PCR004 objPCR004, PCR003 objPCR003, PCR006 objPCR006)//(string Ids)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                //decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                //var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
                var invalidElement = "";
                var NoInPCLGeneratedStatus = "";
                var PCLAlreadyRelease = "";
                var invalidItemType = "";
                var PCLNotGenerated = "";
                var ReturnBalanceDistribution = "";
                var ReturnBalanceDistributionDataInValid = "";
                var InCompleteDataFill = "";
                var TotalAreaInValid = "";
                var ScrapQtyInValid = "";
                var PlannerRequired = "";
                var MovementTypeRequired = "";
                //foreach (var item in listAllPCR004)
                //{
                //var objPCR003 = db.PCR003.Where(i => i.RefId == item.Id).FirstOrDefault();
                //var objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == objPCR003.PCLHeaderId).FirstOrDefault();
                if (objPCR004.ItemType != PLTType)
                {
                    Manager.ConcatString(ref invalidItemType, objPCR004.PartNumber.Value.ToString());
                }
                else
                {
                    if (objPCR006 == null)
                    {
                        Manager.ConcatString(ref PCLNotGenerated, objPCR004.PartNumber.Value.ToString() + "-" + objPCR004.Sequence.Value.ToString());
                    }
                    else
                    {
                        if (objPCR006.PCLStatus == clsImplementationEnum.PCRLineStatus.PCLReleased.GetStringValue())
                        {
                            Manager.ConcatString(ref PCLAlreadyRelease, objPCR003.PartNumber.Value.ToString() + "-" + objPCR004.Sequence.Value.ToString());
                        }
                        else if (objPCR006.PCLStatus == clsImplementationEnum.PCRLineStatus.PCLGenerated.GetStringValue())
                        {
                            if (objPCR006.ReturnBalanceInPcs.HasValue && objPCR006.ReturnBalanceInPcs.Value > 0)
                            {
                                //if (objPCR006.ReturnBalanceInPcs.Value != db.PCR007.Where(i => i.PCLHeaderId == objPCR006.PCLHeaderId).Count())
                                //{
                                //    Manager.ConcatString(ref ReturnBalanceDistribution, objPCR003.PartNumber.Value.ToString() + "-" + item.Sequence.Value.ToString());
                                //}
                                if (!db.PCR007.Any(i => i.PCLHeaderId == objPCR006.PCLHeaderId))
                                {
                                    Manager.ConcatString(ref ReturnBalanceDistribution, objPCR003.PartNumber.Value.ToString() + "-" + objPCR004.Sequence.Value.ToString());
                                }
                                else
                                {
                                    var response = (StockResponseMsg)ValidateReturnBalanceDistribution(objPCR006, objPCR003).Data;
                                    if (!response.Key)
                                    {
                                        ReturnBalanceDistributionDataInValid += response.Value;
                                    }
                                }
                            }
                            //if (!objPCR006.TotalAreaInm2.HasValue || (objPCR006.TotalAreaInm2.HasValue && objPCR006.TotalAreaInm2.Value <= 0))
                            //{
                            //    Manager.ConcatString(ref TotalAreaInValid, objPCR003.PartNumber.Value.ToString() + "-" + item.Sequence.Value.ToString());
                            //}
                            //if (!objPCR006.ScrapQuantityInm2.HasValue || (objPCR006.ScrapQuantityInm2.HasValue && objPCR006.ScrapQuantityInm2.Value <= 0))
                            //{
                            //    Manager.ConcatString(ref ScrapQtyInValid, objPCR003.PartNumber.Value.ToString() + "-" + item.Sequence.Value.ToString());
                            //}
                            //if (string.IsNullOrEmpty(objPCR006.Planner))
                            //{
                            //    Manager.ConcatString(ref PlannerRequired, objPCR003.PartNumber.Value.ToString() + "-" + item.Sequence.Value.ToString());
                            //}
                            //if (!objPCR006.MovementType.HasValue)
                            //{
                            //    Manager.ConcatString(ref MovementTypeRequired, objPCR003.PartNumber.Value.ToString() + "-" + item.Sequence.Value.ToString());
                            //}
                            if (!objPCR006.TotalCuttingLength.HasValue ||
                                (objPCR006.TotalCuttingLength.HasValue && objPCR006.TotalCuttingLength.Value <= 0) ||
                                string.IsNullOrEmpty(objPCR006.Planner) || string.IsNullOrWhiteSpace(objPCR006.Planner) ||
                                string.IsNullOrEmpty(objPCR006.WorkCenter) || string.IsNullOrWhiteSpace(objPCR006.WorkCenter))
                            {
                                Manager.ConcatString(ref InCompleteDataFill, objPCR003.PartNumber.Value.ToString() + "-" + objPCR004.Sequence.Value.ToString());
                            }
                        }
                        else if (objPCR006.PCLStatus != clsImplementationEnum.PCRLineStatus.PCLGenerated.GetStringValue())
                        {
                            Manager.ConcatString(ref NoInPCLGeneratedStatus, objPCR003.PartNumber.Value.ToString() + "-" + objPCR004.Sequence.Value.ToString());
                        }
                        else if (!IsEnableMPPCLReleaseElementSelection(objPCR006.PCLNumber, objPCR006.TotalCuttingLength, objPCR006.WorkCenter, objPCR006.Planner))
                        {
                            Manager.ConcatString(ref invalidElement, objPCR004.PartNumber.Value.ToString() + "-" + objPCR004.Sequence.Value.ToString());
                        }
                    }
                }
                //}
                if (invalidElement.Length > 0 || PCLNotGenerated.Length > 0 ||
                    invalidItemType.Length > 0 || NoInPCLGeneratedStatus.Length > 0 ||
                    PCLAlreadyRelease.Length > 0 || ReturnBalanceDistribution.Length > 0 ||
                    ReturnBalanceDistributionDataInValid.Length > 0 ||
                    InCompleteDataFill.Length > 0 || TotalAreaInValid.Length > 0 ||
                    ScrapQtyInValid.Length > 0 || PlannerRequired.Length > 0 ||
                    MovementTypeRequired.Length > 0)
                {
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = (PCLNotGenerated.Length > 0 ? "PCL Not Generated For " + PCLNotGenerated + "  Budget Line-Sequence." : "") +
                                           (invalidElement.Length > 0 ? "Please Fill All PCL Related Data For " + invalidElement + " Budget Line-Sequence." : "") +
                                           (NoInPCLGeneratedStatus.Length > 0 ? "PCL Is Not In PCL Generate Status For " + NoInPCLGeneratedStatus + " Budget Line-Sequence." : "") +
                                           (PCLAlreadyRelease.Length > 0 ? "PCL Already Released For " + NoInPCLGeneratedStatus + " Budget Line-Sequence." : "") +
                                           (invalidItemType.Length > 0 ? invalidItemType + " No. Budget Line Is Non-Plate." : "") +
                                           (ReturnBalanceDistribution.Length > 0 ? "Please Maintain Return Balance Distribution For " + ReturnBalanceDistribution + " No. Budget Line" : "") +
                                           (ReturnBalanceDistributionDataInValid.Length > 0 ? ReturnBalanceDistributionDataInValid : "") +
                                           (TotalAreaInValid.Length > 0 ? "Total Consumed Area must be greater than Zero" : "") +
                                           (ScrapQtyInValid.Length > 0 ? "Scrap Quantity must not be less than Zero" : "") +
                                           (PlannerRequired.Length > 0 ? "Please Enter Planner" : "") +
                                           (MovementTypeRequired.Length > 0 ? "Please select Movement Type" : "") +
                                           (InCompleteDataFill.Length > 0 ? "Please Fill All Required Data For " + InCompleteDataFill + " No. Budget Line For Release PCL." : "");
                    //+" Please Unselect That All Budget Line-Sequence!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Return Balance Distribution

        [SessionExpireFilter]
        public PartialViewResult _ReturnBalanceDistribution(CommonParamEnt param)
        {
            var objPCR004 = db.PCR004.Where(i => i.Id == param.Id).FirstOrDefault();
            var objPCR003 = db.PCR003.Where(i => i.LineId == param.LineId).FirstOrDefault();
            var objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == param.HeaderId).FirstOrDefault();
            var objPCR001 = db.PCR001.Where(i => i.StockNumber == objPCR003.StockNumber && i.StockRevision == objPCR003.StockRevision).FirstOrDefault();

            bool IsValidUser = CheckUserIsValid(MaterialPlannerPCRAction);

            ViewBag.PCLHeaderId = objPCR006.PCLHeaderId;
            ViewBag.PCLNumber = objPCR006.PCLNumber;
            ViewBag.OrderNo = objPCR006.PlateIssueOrderProjectManual;
            ViewBag.ReturnBalanceInPcs = objPCR006.ReturnBalanceInPcs;
            ViewBag.Length = objPCR001.Length;
            ViewBag.Width = objPCR001.Width;
            ViewBag.Width = objPCR001.Width;
            ViewBag.MaximunReturnArea = GetMaximunReturnArea(objPCR001, objPCR006);
            ViewBag.StockArea = objPCR001.Area;
            ViewBag.MATL3IsValidUser = IsValidUser;
            if (!string.IsNullOrEmpty(objPCR006.PlateIssueOrderProjectManual))
            {
                //objPCR006.PlateIssueOrderProjectManual = "H01016401";
                var TotalPlateIssueOrderProjectManual = TotalManualProjectQuantity(objPCR006.PlateIssueOrderProjectManual, objPCR001);
                ViewBag.TotalPlateIssueOrderProjectManual = TotalPlateIssueOrderProjectManual;
                ViewBag.StockMinusIssue = objPCR001.Area - TotalPlateIssueOrderProjectManual;

            }
            else
            {
                ViewBag.TotalPlateIssueOrderProjectManual = 0;
                ViewBag.StockMinusIssue = 0;
            }

            return PartialView("_ReturnBalanceDistribution");
        }

        [SessionExpireFilter]
        public ActionResult LoadReturnBalanceDistributionDataTable(JQueryDataTableParamModel param, decimal PCLHeaderId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                //string Project = Request["Project"].ToString();
                whereCondition += " and PCLHeaderId=" + PCLHeaderId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName ={"p7.PCLNumber",
                                          "p7.Sequence",
                                          "p7.LengthInmm",
                                          "p7.WidthInmm",
                                          "p7.AreaInm2",
                                          "p7.IsOddShape",
                                          "p7.StockNumber",
                                          "p7.StockRevision",
                                          "p7.Remark",
                                         };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                //else
                //{
                //    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                //}

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_PCR_GET_RETURN_BALANCE_DISTRIBUTION_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = new List<dynamic>();
                foreach (var h in lstHeader)
                {
                    res.Add(new
                    {
                        PCRReturnBalanceId = Convert.ToString(h.PCRReturnBalanceId),
                        PCLHeaderId = Convert.ToString(h.PCLHeaderId),
                        PCLNumber = Convert.ToString(h.PCLNumber),
                        Revision = Convert.ToString(h.Revision),
                        Sequence = Convert.ToString(h.Sequence),
                        LengthInmm = Convert.ToString(h.LengthInmm),
                        WidthInmm = Convert.ToString(h.WidthInmm),
                        AreaInm2 = Convert.ToString(h.AreaInm2),
                        IsOddShape = GetOddShapeRBDColumnDetails(Convert.ToInt32(h.PCRReturnBalanceId), h.IsOddShape, true),
                        StockNumber = Convert.ToString(h.StockNumber),
                        StockRevision = Convert.ToString(h.StockRevision),
                        Remark = Convert.ToString(h.Remark),
                    });
                }

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadReturnBalanceDistributionEditableData(int id, int PCLHeaderId, bool isReadOnly = false)
        {
            try
            {
                var isDisplay = isReadOnly || !CheckUserIsValid(MaterialPlannerPCRAction);
                var data = new List<dynamic>();
                if (id == 0)
                {
                    var maxSequence = db.PCR007.Where(i => i.PCLHeaderId == PCLHeaderId).OrderByDescending(i => i.Sequence).FirstOrDefault();
                    var IsOddShape = false;
                    data.Add(new
                    {
                        PCRReturnBalanceId = 0,
                        PCLHeaderId = PCLHeaderId,
                        PCLNumber = "",
                        Revision = 0,
                        Sequence = maxSequence != null ? maxSequence.Sequence + 1 : 1,
                        LengthInmm = GetLengthRBDColumnDetails(id, 0, IsOddShape, isDisplay),
                        WidthInmm = GetWidthRBDColumnDetails(id, 0, IsOddShape, isDisplay),
                        AreaInm2 = GetAreaRBDColumnDetails(id, 0, IsOddShape, isDisplay),
                        IsOddShape = GetOddShapeRBDColumnDetails(id, IsOddShape, isDisplay),
                        StockNumber = "",
                        StockRevision = 0,
                        Remark = "",
                    });
                }
                else
                {
                    var objEditable = db.SP_PCR_GET_RETURN_BALANCE_DISTRIBUTION_DATA(1, 0, "", "PCRReturnBalanceId = " + id).Take(1).FirstOrDefault();
                    data.Add(new
                    {
                        PCRReturnBalanceId = objEditable.PCRReturnBalanceId,
                        PCLHeaderId = objEditable.PCLHeaderId,
                        PCLNumber = objEditable.PCLNumber,
                        Revision = objEditable.Revision,
                        Sequence = objEditable.Sequence,
                        LengthInmm = GetLengthRBDColumnDetails(id, objEditable.LengthInmm, objEditable.IsOddShape, isDisplay),
                        WidthInmm = GetWidthRBDColumnDetails(id, objEditable.WidthInmm, objEditable.IsOddShape, isDisplay),
                        AreaInm2 = GetAreaRBDColumnDetails(id, objEditable.AreaInm2, objEditable.IsOddShape, isDisplay),
                        IsOddShape = GetOddShapeRBDColumnDetails(id, objEditable.IsOddShape, isDisplay),
                        StockNumber = objEditable.StockNumber,
                        StockRevision = objEditable.StockRevision,
                        Remark = objEditable.Remark,
                    });
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new List<dynamic>()
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public double TotalManualProjectQuantity(string OrderNo, PCR001 objPCR001)
        {
            double TotalPlateIssueOrderProjectManual = 0;
            if (!string.IsNullOrEmpty(OrderNo))
            {
                var LNLinkedServer = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue()));
                var LNCompanyId = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNCompanyId.GetStringValue()));
                var TotalOrderQuantity = db.Database.SqlQuery<double>("Select SUM(t_qoro) from " + LNLinkedServer + ".dbo.twhinh220" + LNCompanyId + " Where t_oorg = 76 and t_orno = '" + OrderNo + "'").FirstOrDefault<double>();
                var TotalIssue = ((TotalOrderQuantity * 1000) / ((objPCR001.DerivedConversionFactor * objPCR001.Thickness) + (objPCR001.CladThickess1Inmm * objPCR001.TheoConvFactForClad1InKgPerm3) + (objPCR001.CladThickess2Inmm * objPCR001.TheoConvFactForClad2InKgPerm3)));
                TotalPlateIssueOrderProjectManual = Math.Round((TotalIssue.HasValue ? TotalIssue.Value : 0), 4);
            }
            return TotalPlateIssueOrderProjectManual;
        }

        #region Validate & Save

        public JsonResult ValidateReturnBalanceDistributionPartialView(CommonParamEnt param)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            //decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
            //var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
            try
            {
                var objPCR004 = db.PCR004.Where(i => i.Id == param.Id).FirstOrDefault();
                PCR003 objPCR003 = null;
                PCR006 objPCR006 = null;
                if (param.LineId.HasValue && param.LineId.Value > 0)
                {
                    objPCR003 = db.PCR003.Where(i => i.LineId == param.LineId).FirstOrDefault();
                }
                if (param.HeaderId.HasValue && param.HeaderId.Value > 0)
                {
                    objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == param.HeaderId).FirstOrDefault();
                }

                //foreach (var item in listAllPCR004)
                //{
                if (objPCR004.ItemType != PLTType)
                {
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = NonPlateReturnBalanceDistribution;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                //var objPCR003 = db.PCR003.Where(i => i.RefId == item.Id).FirstOrDefault();
                if (objPCR003 != null)
                {
                    if (objPCR003.ItemType != PLTType)
                    {
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = NonPlateReturnBalanceDistribution;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    //var objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == objPCR003.PCLHeaderId).FirstOrDefault();
                    if (objPCR006 == null)
                    {
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = PCLNotGenerated;
                    }
                    else if (objPCR006.PCLStatus != clsImplementationEnum.PCRLineStatus.PCLGenerated.GetStringValue())
                    {
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = PCLNotInGeneratedStatus;
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                    }
                }
                else
                {
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = PCRNotGenerated;
                }
                //}

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SaveReturnBalanceDistribution(PCR007 objPCR007)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(MaterialPlannerPCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == objPCR007.PCLHeaderId).FirstOrDefault();
                var objPCR003 = db.PCR003.Where(i => i.PCLHeaderId == objPCR006.PCLHeaderId).FirstOrDefault();
                var objPCR001 = db.PCR001.Where(i => i.StockNumber == objPCR003.StockNumber && i.StockRevision == objPCR003.StockRevision).FirstOrDefault();

                if (objPCR007.PCRReturnBalanceId == 0)
                {
                    if (objPCR006.ReturnBalanceInPcs.HasValue && objPCR006.ReturnBalanceInPcs.Value == db.PCR007.Where(i => i.PCLHeaderId == objPCR006.PCLHeaderId).Count())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = ReturnBalanceDistributionMaxLinesMaintain;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    PCR007 ojjPCR007New = new PCR007();
                    ojjPCR007New.AreaInm2 = objPCR007.AreaInm2;
                    ojjPCR007New.CreatedBy = objClsLoginInfo.UserName;
                    ojjPCR007New.CreatedOn = DateTime.Now;
                    ojjPCR007New.IsOddShape = objPCR007.IsOddShape;
                    ojjPCR007New.ItemType = objPCR006.ItemType;
                    ojjPCR007New.LengthInmm = objPCR007.LengthInmm;
                    ojjPCR007New.PCLHeaderId = objPCR007.PCLHeaderId;
                    ojjPCR007New.PCLNumber = objPCR006.PCLNumber;
                    ojjPCR007New.Revision = 0;
                    ojjPCR007New.Sequence = objPCR007.Sequence;
                    ojjPCR007New.StockNumber = objPCR003.StockNumber;
                    ojjPCR007New.StockRevision = (int)objPCR003.StockRevision.Value;
                    ojjPCR007New.WidthInmm = objPCR007.WidthInmm;
                    db.PCR007.Add(ojjPCR007New);
                    db.SaveChanges();

                    UpdateReturnAndScrapArea(objPCR006, objPCR001);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = ReturnBalanceDistributionSave;
                }
                else
                {
                    var objPCR007Update = db.PCR007.Where(i => i.PCRReturnBalanceId == objPCR007.PCRReturnBalanceId).FirstOrDefault();
                    objPCR007Update.AreaInm2 = objPCR007.AreaInm2;
                    objPCR007Update.IsOddShape = objPCR007.IsOddShape;
                    objPCR007Update.WidthInmm = objPCR007.WidthInmm;
                    objPCR007Update.LengthInmm = objPCR007.LengthInmm;
                    db.SaveChanges();

                    UpdateReturnAndScrapArea(objPCR006, objPCR001);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = ReturnBalanceDistributionUpdate;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteReturnBalanceDistribution(decimal PCRReturnBalanceId)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(MaterialPlannerPCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var objPCR007 = db.PCR007.Where(i => i.PCRReturnBalanceId == PCRReturnBalanceId).FirstOrDefault();
                if (objPCR007 != null)
                {
                    var objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == objPCR007.PCLHeaderId).FirstOrDefault();
                    var objPCR003 = db.PCR003.Where(i => i.PCLHeaderId == objPCR006.PCLHeaderId).FirstOrDefault();
                    var objPCR001 = db.PCR001.Where(i => i.StockNumber == objPCR003.StockNumber && i.StockRevision == objPCR003.StockRevision).FirstOrDefault();

                    db.PCR007.Remove(objPCR007);
                    db.SaveChanges();

                    UpdateReturnAndScrapArea(objPCR006, objPCR001);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = ReturnBalanceDistributionDelete;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = ReturnBalanceDistributionNotExist;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public void UpdateReturnAndScrapArea(PCR006 objPCR006, PCR001 objPCR001)
        {
            try
            {
                var AreaInm2 = db.PCR007.Where(i => i.PCLHeaderId == objPCR006.PCLHeaderId).Sum(i => i.AreaInm2);                
                var ReturnAreaInm2 = (AreaInm2.HasValue ? AreaInm2.Value : 0);
                //var StockArea = (objPCR001.Area.HasValue ? objPCR001.Area.Value : 0);
                var StockArea = GetMaximunReturnArea(objPCR001, objPCR006).Value;
                var ActualAreaConsumedInm2 = (objPCR006.ActualAreaConsumedInm2.HasValue ? objPCR006.ActualAreaConsumedInm2.Value : 0);
                var ScrapQuantityInm2 = Math.Round(((StockArea) - (ActualAreaConsumedInm2 + ReturnAreaInm2)),4); //Math.Round(((decimal)(Area.Value - ReturnAreaInm2)), 4);
                objPCR006.ReturnAreaInm2 = ReturnAreaInm2;
                objPCR006.ScrapQuantityInm2 = (double)ScrapQuantityInm2;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public JsonResult ValidateReturnBalanceDistribution(PCR006 objPCR006, PCR003 objPCR003)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            double Total = 0;
            try
            {
                var listPCR007 = db.PCR007.Where(i => i.PCLHeaderId == objPCR006.PCLHeaderId).ToList();
                var objPCR001 = db.PCR001.Where(i => i.StockNumber == objPCR003.StockNumber && i.StockRevision == objPCR003.StockRevision).FirstOrDefault();

                foreach (var item in listPCR007)
                {
                    if (!item.IsOddShape.HasValue || (item.IsOddShape.HasValue && item.IsOddShape.Value))
                    {
                        if (item.AreaInm2 <= 0)
                        {
                            objResponseMsg.Value += "Area must be greater than 0 For Sequence " + item.Sequence + ".";
                        }
                    }
                    else
                    {
                        if (item.LengthInmm <= 0)
                        {
                            objResponseMsg.Value += "Length must be greater than 0 For Sequence " + item.Sequence + ".";
                        }
                        else if (item.LengthInmm > objPCR001.Length)
                        {
                            objResponseMsg.Value += "Length of Return Plate cannot be greater than Total Length " + objPCR001.Length + "  For Sequence " + item.Sequence + ".";
                        }
                        if (item.WidthInmm <= 0)
                        {
                            objResponseMsg.Value += "Width must be greater than 0 For Sequence " + item.Sequence + ".";
                        }
                        else if (item.WidthInmm > objPCR001.Width)
                        {
                            objResponseMsg.Value += "Width of Return Plate cannot be greater than Total Width " + objPCR001.Width + "  For Sequence " + item.Sequence + ".";
                        }
                    }
                    Total += (item.AreaInm2.HasValue ? item.AreaInm2.Value : 0);
                }
                if (!string.IsNullOrEmpty(objPCR006.PlateIssueOrderProjectManual))
                {
                    var TotalPlateIssueOrderProjectManual = TotalManualProjectQuantity(objPCR006.PlateIssueOrderProjectManual, objPCR001);
                    var StockMinusIssue = objPCR001.Area - TotalPlateIssueOrderProjectManual;
                    if (StockMinusIssue.Value < Total)
                    {
                        objResponseMsg.Value += "Total Area in Distribution must be equal or less than " + StockMinusIssue + "m2." +
                                                "Reason:" + TotalPlateIssueOrderProjectManual + "m2 is already consumed in Project(Manual) Order " + objPCR006.PlateIssueOrderProjectManual;
                    }
                }

                var MaximunReturnArea = GetMaximunReturnArea(objPCR001, objPCR006);
                //if (Total > MaximunReturnArea)
                //{
                //    objResponseMsg.Value += "Total Area in Distribution must be equal or less than Maximun Return Area " + MaximunReturnArea + " m2.";
                //}
                //else if (Total != MaximunReturnArea)
                //{
                //    objResponseMsg.Value += "Total Area in Distribution must be equal to Total Return Area for PCL";
                //}

                if (!string.IsNullOrEmpty(objResponseMsg.Value) && objResponseMsg.Value.Length > 0)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public double? GetMaximunReturnArea(PCR001 objPCR001, PCR006 objPCR006)
        {
            var MaximunReturnArea = objPCR001.Area - objPCR006.TotalAreaInm2;
            return MaximunReturnArea;
        }
        #endregion

        #endregion

        #region Check Stock
        [SessionExpireFilter]
        public PartialViewResult _CheckStock(int Id)
        {
            var objPCR004 = db.PCR004.Where(i => i.Id == Id).FirstOrDefault();
            ViewBag.ItemName = objPCR004.Description;
            ViewBag.LocationName = db.COM002.Where(i => i.t_dimx == objPCR004.Location).FirstOrDefault().t_desc;
            ViewBag.ChildItem = objPCR004.ChildItem;
            ViewBag.Location = objPCR004.Location;
            return PartialView("_CheckStock");
        }

        [SessionExpireFilter]
        public ActionResult LoadCheckStockDataTable(JQueryDataTableParamModel param, string ChildItem, string Location)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                whereCondition += " and Item='" + ChildItem + "' and Location='" + Location + "' ";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName ={
                                         "StockNumber",
                                         "StockRevision",
                                         "PurchaseOrder",
                                         "POPosition",
                                         "ss.StockStatusName",
                                         "ReceiptNumber",
                                         "ReceiptLine",
                                         "QtyInKg",
                                         };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_PCR_GET_CHECK_STOCK_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = new List<dynamic>();
                foreach (var h in lstHeader)
                {
                    res.Add(new
                    {
                        StockNumber = Convert.ToString(h.StockNumber),
                        StockRevision = Convert.ToString(h.StockRevision),
                        PurchaseOrder = Convert.ToString(h.PurchaseOrder),
                        POPosition = Convert.ToString(h.POPosition),
                        ReceiptNumber = Convert.ToString(h.ReceiptNumber),
                        ReceiptLine = Convert.ToString(h.ReceiptLine),
                        QtyInKg = Convert.ToString(h.QtyInKg),
                        StockStatusName = Convert.ToString(h.StockStatusName),
                    });
                }

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Unbind Stock
        public JsonResult UnbindStock(CommonParamEnt param)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            bool IsPCLDeleted = false;
            double StockArea = 0;
            try
            {
                //var objPCR004 = db.PCR004.Where(i => i.Id == Id).FirstOrDefault();
                //var objPCR003 = db.PCR003.Where(i => i.RefId == objPCR004.Id).FirstOrDefault();
                var objPCR004 = db.PCR004.Where(i => i.Id == param.Id).FirstOrDefault();
                PCR003 objPCR003 = null;
                PCR006 objPCR006 = null;
                if (param.LineId.HasValue && param.LineId.Value > 0)
                {
                    objPCR003 = db.PCR003.Where(i => i.LineId == param.LineId).FirstOrDefault();
                }
                if (param.HeaderId.HasValue && param.HeaderId.Value > 0)
                {
                    objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == param.HeaderId).FirstOrDefault();
                }

                var Validate = ValidateUnbindStock(objPCR004, objPCR003);
                var objValidate = ((StockResponseMsg)Validate.Data);
                if (objValidate.Key)
                {
                    //var objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == objPCR003.PCLHeaderId).FirstOrDefault();
                    var objPCR001 = db.PCR001.Where(i => i.PCLNo == objPCR003.PCLNumber).FirstOrDefault();

                    if (objPCR006 != null && !string.IsNullOrEmpty(objPCR003.PCLNumber))
                    {
                        var listPCR007 = db.PCR007.Where(i => i.PCLHeaderId == objPCR006.PCLHeaderId).ToList();
                        db.PCR007.RemoveRange(listPCR007);

                        var listPCR008 = db.PCR008.Where(i => i.PCLHeaderId == objPCR006.PCLHeaderId).ToList();
                        db.PCR008.RemoveRange(listPCR008);
                        
                        var AreaConsumed = Math.Round((objPCR003.AreaConsumed.HasValue ? objPCR003.AreaConsumed.Value : 0), 4);
                        var TotalAreaInm2 = Math.Round((objPCR006.TotalAreaInm2.HasValue ? objPCR006.TotalAreaInm2.Value : 0), 4);
                        if (AreaConsumed == TotalAreaInm2)
                        {
                            IsPCLDeleted = true;
                            db.PCR006.Remove(objPCR006);
                        }
                        else
                        {
                            StockArea = 0;
                            if (objPCR001 != null)
                            {
                                StockArea = objPCR001.Area.HasValue ? objPCR001.Area.Value : 0;
                            }

                            objPCR006.TotalAreaInm2 = TotalAreaInm2 - AreaConsumed;
                            objPCR006.ReturnAreaInm2 = StockArea - objPCR006.TotalAreaInm2;
                            objPCR006.WorkCenter = string.Empty;
                            objPCR006.ScrapQuantityInm2 = 0;
                            objPCR006.Planner = string.Empty;
                            objPCR006.ReturnBalanceInPcs = 0;
                            objPCR006.TotalCuttingLength = 0;
                            objPCR006.MovementType = null;
                        }
                        (new clsFileUpload()).DeleteFolderFilesAsync("PCR006/"+ objPCR006.PCLHeaderId.ToString());
                    }

                    var objPCR001_1 = db.PCR001.Where(i => i.StockRevision == objPCR003.StockRevision && i.StockNumber == objPCR003.StockNumber && i.Item == objPCR003.ChildItem).FirstOrDefault();
                    if (objPCR001_1 != null)
                    {
                        var AllocatedQuantity = objPCR001_1.AllocatedQuantity;
                        var FreeQuantity = objPCR001_1.FreeQuantity;
                        var CurrentAreaConsume = objPCR003.AreaConsumed;
                        objPCR001_1.AllocatedQuantity = (AllocatedQuantity.HasValue ? AllocatedQuantity.Value : 0)
                                                      - (CurrentAreaConsume.HasValue ? CurrentAreaConsume.Value : 0);
                        objPCR001_1.FreeQuantity = (FreeQuantity.HasValue ? FreeQuantity.Value : 0)
                                                            + (CurrentAreaConsume.HasValue ? CurrentAreaConsume.Value : 0);
                        if (IsPCLDeleted)
                        {
                            objPCR001_1.StockStatus = clsImplementationEnum.StockStatus.Created.GetHashCode();
                            objPCR001_1.PCLNo = string.Empty;
                        }
                    }

                    var objPCR003Update = db.PCR003.Where(i => i.PCLNumber == objPCR003.PCLNumber && i.PCRLineNo == objPCR003.PCRLineNo && i.PCRLineRevNo == objPCR003.PCRLineRevNo).FirstOrDefault();
                    if (objPCR003Update != null)
                    {                        
                        objPCR003Update.StockNumber = string.Empty;
                        objPCR003Update.NestedQuantity = 0;
                        objPCR003Update.AreaConsumed = 0;
                        objPCR003Update.PCRLineStatus = clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner.GetStringValue();
                        objPCR003Update.PCLNumber = string.Empty;
                        objPCR003Update.PCLCreatedBy = null;
                        objPCR003Update.PCLCreationDate = null;
                        objPCR003Update.PCLReleaseDate = null;
                        objPCR003Update.PCLReleasedBy = null;
                        objPCR003Update.PCLHeaderId = null;
                    }

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = StockUnbindSuccess;
                }
                else
                {
                    objResponseMsg.Key = objValidate.Key;
                    objResponseMsg.Value = objValidate.Value != null ? objValidate.Value : SomeDataInvalidForUpdate;
                    objResponseMsg.IsInformation = objValidate.IsInformation;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ValidateUnbindStock(PCR004 objPCR004, PCR003 objPCR003)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                //var objPCR004 = db.PCR004.Where(i => i.Id == Id).FirstOrDefault();
                //var objPCR003 = db.PCR003.Where(i => i.LineId == objPCR004.PCRLineId).FirstOrDefault();
                if (objPCR003 == null)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = PCRNotGenerated;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (objPCR003.ItemType != PLTType)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = NonPlateItemRestrict;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (string.IsNullOrEmpty(objPCR003.StockNumber))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.IsInformation = true;
                        objResponseMsg.Value = StockNotAttached;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    //if (!string.IsNullOrEmpty(objPCR003.StockNumber) && objPCR003.PCRLineStatus != clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner.GetStringValue())
                    //{
                    //    objResponseMsg.Key = false;
                    //    objResponseMsg.IsInformation = true;
                    //    objResponseMsg.Value = StockUnbindInReleaseToMaterialPlannerStatus;
                    //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    //}
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Extra Functions
        public void AddRevisedPCR(List<PCR003> listPCR003)
        {
            foreach (var objPCR003 in listPCR003)
            {
                if (objPCR003.NestedQuantity < objPCR003.NoofPieces)
                {
                    var MaxPCRRevNo = db.PCR003.Where(i => i.HeaderId == objPCR003.HeaderId && i.PCRLineNo == objPCR003.PCRLineNo).OrderByDescending(i => i.PCRLineRevNo).FirstOrDefault().PCRLineRevNo;
                    PCR003 objPCR003New = new PCR003();
                    //objPCR003New.AlternateStockRemark = objPCR003.AlternateStockRemark;
                    objPCR003New.Area = objPCR003.Area;
                    objPCR003New.AreaConsumed = 0;
                    objPCR003New.ChildItem = objPCR003.ChildItem;
                    objPCR003New.CreatedBy = objClsLoginInfo.UserName;
                    objPCR003New.CreatedOn = DateTime.Now;
                    //objPCR003New.EditedBy = objPCR003.EditedBy;
                    //objPCR003New.EditedOn = objPCR003.EditedOn;
                    objPCR003New.Element = objPCR003.Element;
                    objPCR003New.GrainOrientation = objPCR003.GrainOrientation;
                    objPCR003New.HeaderId = objPCR003.HeaderId;
                    objPCR003New.ImmediateParentItem = objPCR003.ImmediateParentItem;
                    objPCR003New.InspectionQty = objPCR003.InspectionQty;
                    objPCR003New.ItemRevision = objPCR003.ItemRevision;
                    objPCR003New.ItemType = objPCR003.ItemType;
                    objPCR003New.Length = objPCR003.Length;
                    //objPCR003New.LineId = objPCR003.LineId;
                    objPCR003New.LinkToSCR = objPCR003.LinkToSCR;
                    objPCR003New.ManufacturingItem = objPCR003.ManufacturingItem;
                    objPCR003New.ManufacturingItemMinusPosition = objPCR003.ManufacturingItemMinusPosition;
                    objPCR003New.MaterialPlannerRemark = objPCR003.MaterialPlannerRemark;
                    objPCR003New.MCRPrepared = objPCR003.MCRPrepared;
                    objPCR003New.MovementType = null;
                    objPCR003New.NestedQuantity = 0;
                    objPCR003New.NoofPieces = objPCR003.NoofPieces - objPCR003.NestedQuantity;
                    objPCR003New.OriginalItem = objPCR003.OriginalItem;
                    objPCR003New.P10 = objPCR003.P10;
                    objPCR003New.P1_OD = objPCR003.P1_OD;
                    objPCR003New.P2_Length= objPCR003.P2_Length;
                    objPCR003New.P3_Width = objPCR003.P3_Width;
                    objPCR003New.P4_OD = objPCR003.P4_OD;
                    objPCR003New.P5_ID = objPCR003.P5_ID;
                    objPCR003New.P6_OD = objPCR003.P6_OD;
                    objPCR003New.P7_ID = objPCR003.P7_ID;
                    objPCR003New.P8_Deg = objPCR003.P8_Deg;
                    objPCR003New.P9 = objPCR003.P9;
                    objPCR003New.PartNumber = objPCR003.PartNumber;
                    //objPCR003New.PCLCreatedBy = objPCR003.PCLCreatedBy;
                    //objPCR003New.PCLCreationDate = objPCR003.PCLCreationDate;
                    objPCR003New.PCLNumber = string.Empty;
                    //objPCR003New.PCLReleaseDate = objPCR003.PCLReleaseDate;
                    //objPCR003New.PCLReleasedBy = objPCR003.PCLReleasedBy;
                    objPCR003New.PCRCreatedBy = objClsLoginInfo.UserName;
                    objPCR003New.PCRCreationDate = DateTime.Now;
                    //objPCR003New.PCRHardCopyNumber = objPCR003.PCRHardCopyNumber;
                    objPCR003New.PCRLineNo = objPCR003.PCRLineNo;
                    objPCR003New.PCRLineRevNo = MaxPCRRevNo + 1;
                    objPCR003New.PCRLineStatus = clsImplementationEnum.PCRLineStatus.ReleasedtoMaterialPlanner.GetStringValue();
                    objPCR003New.PCRPlannerRemark = objPCR003.PCRPlannerRemark;
                    objPCR003New.PCRReleaseDate = DateTime.Now;
                    objPCR003New.PCRReleasedBy = objClsLoginInfo.UserName;
                    //objPCR003New.PCRRequirementDate = objPCR003.PCRRequirementDate;
                    //objPCR003New.PCRRetractBy = objPCR003.PCRRetractBy;
                    //objPCR003New.PCRRetractOn = objPCR003.PCRRetractOn;
                    objPCR003New.PCRType = objPCR003.PCRType;
                    objPCR003New.Project = objPCR003.Project;
                    //objPCR003New.QCApprovalStatus = objPCR003.QCApprovalStatus;
                    //objPCR003New.RejectedQty = objPCR003.RejectedQty;
                    //objPCR003New.ReturnRemark = objPCR003.ReturnRemark;
                    objPCR003New.ScrapQuantity = 0;
                    objPCR003New.SCRNumber = objPCR003.SCRNumber;
                    objPCR003New.ShapeFile = objPCR003.ShapeFile;
                    objPCR003New.ShapeType = objPCR003.ShapeType;
                    objPCR003New.ShopFloorWarhouse = objPCR003.ShopFloorWarhouse;
                    objPCR003New.StockNumber = string.Empty;
                    objPCR003New.StockRevision = null;
                    objPCR003New.Thickness = objPCR003.Thickness;
                    objPCR003New.UseAlternateStock = objPCR003.UseAlternateStock;
                    //objPCR003New.WarehouseCuttingLocation = objPCR003.WarehouseCuttingLocation;
                    //objPCR003New.WCDeliver = objPCR003.WCDeliver;
                    objPCR003New.Width = objPCR003.Width;
                    objPCR003New.RefId = objPCR003.RefId;
                    db.PCR003.Add(objPCR003New);                    
                }
            }
        }

        public void UpdateTotalAreaInm2PCL(decimal PCLHeaderId)
        {
            var objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == PCLHeaderId).FirstOrDefault();
            double TotalAreaInm2 = 0;
            var listPCR003 = db.PCR003.Where(i => i.PCLNumber == objPCR006.PCLNumber).ToList();
            var StockNumber = ""; double? StockRevision = 0;

            foreach (var item in listPCR003)
            {
                if (String.IsNullOrEmpty(StockNumber))
                {
                    StockNumber = item.StockNumber;
                    StockRevision = item.StockRevision;
                }
                var Area = item.Length * item.Width * item.NestedQuantity / 1000000;
                TotalAreaInm2 += (Area.HasValue ? Area.Value : 0);
            }
            objPCR006.TotalAreaInm2 = TotalAreaInm2;

            var objPCR001 = db.PCR001.Where(i => i.StockNumber == StockNumber && i.StockRevision == StockRevision).FirstOrDefault();
            if (!objPCR006.ReturnBalanceInPcs.HasValue || (objPCR006.ReturnBalanceInPcs.HasValue && objPCR006.ReturnBalanceInPcs.Value == 0))
            {
                objPCR006.ReturnAreaInm2 = 0;
                objPCR006.ScrapQuantityInm2 = GetMaximunReturnArea(objPCR001, objPCR006);
            }
            else
            {
                UpdateReturnAndScrapArea(objPCR006, objPCR001);
            }
            db.SaveChanges();
        }

        public void AddAutoReviseStock(PCR006 objPCR006)
        {
            var listPCR007 = db.PCR007.Where(i => i.PCLHeaderId == objPCR006.PCLHeaderId).ToList();
            var objPCR003 = db.PCR003.Where(i => i.PCLHeaderId == objPCR006.PCLHeaderId).FirstOrDefault();
            var objPCR001 = db.PCR001.Where(i => i.StockNumber == objPCR003.StockNumber && i.StockRevision == objPCR003.StockRevision).FirstOrDefault();

            if (listPCR007.Count == 1) //Increase Stock Revision
            {
                var objPCR007 = listPCR007.FirstOrDefault();
                var objPCR001New = new PCR001();
                objPCR001New.Item = objPCR001.Item;
                objPCR001New.StockNumber = objPCR001.StockNumber;
                objPCR001New.StockRevision = objPCR001.StockRevision + 1;
                objPCR001New.Location = objPCR001.Location;
                objPCR001New.Length = objPCR007.LengthInmm;
                objPCR001New.Width = objPCR007.WidthInmm;
                objPCR001New.ARMSet = objPCR001.ARMSet;
                objPCR001New.QtyInKg = objPCR001.QtyInKg;
                objPCR001New.Materials = objPCR001.Materials;
                objPCR001New.Thickness = objPCR001.Thickness;
                objPCR001New.ReservedQty = objPCR001.ReservedQty;
                objPCR001New.AllocatedQuantity = objPCR001.AllocatedQuantity;
                objPCR001New.CutQuantity = objPCR001.CutQuantity;
                objPCR001New.FreeQuantity = objPCR001.FreeQuantity;
                objPCR001New.PurchaseOrder = objPCR001.PurchaseOrder;
                objPCR001New.POPosition = objPCR001.POPosition;
                objPCR001New.ReceiptNumber = objPCR001.ReceiptNumber;
                objPCR001New.ReceiptLine = objPCR001.ReceiptLine;
                objPCR001New.ShapeType = objPCR001.ShapeType;
                objPCR001New.ShapeFile = objPCR001.ShapeFile;
                objPCR001New.SizeCode = objPCR001.SizeCode;
                objPCR001New.Warehouse = objPCR001.Warehouse;
                objPCR001New.WarehouseLocation = objPCR001.WarehouseLocation;
                objPCR001New.DerivedConversionFactor = objPCR001.DerivedConversionFactor;
                objPCR001New.StockStatus = clsImplementationEnum.StockStatus.Created.GetHashCode();
                objPCR001New.HeatNo = objPCR001.HeatNo;
                objPCR001New.TCNo = objPCR001.TCNo;
                objPCR001New.GrainOrientation = objPCR001.GrainOrientation;
                objPCR001New.CuttingMachine = objPCR001.CuttingMachine;
                objPCR001New.Area = objPCR007.AreaInm2;
                objPCR001New.AreaConsumed = objPCR001.AreaConsumed;
                objPCR001New.PCLNo = string.Empty;
                objPCR001New.ARMVersion = objPCR001.ARMVersion;
                objPCR001New.QuantityApproved = objPCR001.QuantityApproved;
                objPCR001New.ContractNo = objPCR001.ContractNo;
                objPCR001New.CladThickess1Inmm = objPCR001.CladThickess1Inmm;
                objPCR001New.CladThickess2Inmm = objPCR001.CladThickess2Inmm;
                objPCR001New.TheoConvFactForBaseMateInKgPerm3 = objPCR001.TheoConvFactForBaseMateInKgPerm3;
                objPCR001New.TheoConvFactForClad1InKgPerm3 = objPCR001.TheoConvFactForClad1InKgPerm3;
                objPCR001New.TheoConvFactForClad2InKgPerm3 = objPCR001.TheoConvFactForClad2InKgPerm3;
                objPCR001New.TheoreticalWeightInKg = objPCR001.TheoreticalWeightInKg;
                objPCR001New.StockPhysicalStatus = clsImplementationEnum.StockPhysicalStatus.Virtual.GetHashCode();
                objPCR001New.ActualLot = objPCR001.ActualLot;
                objPCR001New.ParentStock = objPCR001.StockNumber;
                objPCR001New.ParentStockRevision = objPCR001.StockRevision;
                objPCR001New.OddShape = objPCR007.IsOddShape.HasValue && objPCR007.IsOddShape.Value ? "Yes" : "No";
                objPCR001New.IssueDirectlyOnShopFloor = objPCR001.IssueDirectlyOnShopFloor;
                objPCR001New.Project = objPCR001.Project;
                objPCR001New.Element = objPCR001.Element;
                objPCR001New.Remark = objPCR001.Remark;
                objPCR001New.WorkCenter = objPCR001.WorkCenter;
                objPCR001New.AreaModified = objPCR001.AreaModified;
                objPCR001New.ReturnStockOrderNo = objPCR001.ReturnStockOrderNo;
                objPCR001New.IsReturnRetainedStock = objPCR001.IsReturnRetainedStock;
                objPCR001New.StockRelatedRemark = objPCR001.StockRelatedRemark;
                objPCR001New.TransferOrderMinusSerialize = objPCR001.TransferOrderMinusSerialize;
                objPCR001New.RunNoMinusSeriOutboundAdvice = objPCR001.RunNoMinusSeriOutboundAdvice;
                objPCR001New.IsConsumptionStart = objPCR001.IsConsumptionStart;
                objPCR001New.StockProject = objPCR001.StockProject;
                objPCR001New.SOBOutboundKey = objPCR001.SOBOutboundKey;
                objPCR001New.StockElement = objPCR001.StockElement;
                objPCR001New.StockActivity = objPCR001.StockActivity;
                objPCR001New.SCRNo = objPCR001.SCRNo;
                objPCR001New.ShipmentNo = objPCR001.ShipmentNo;
                objPCR001New.MTRNo = objPCR001.MTRNo;
                objPCR001New.NCRApplicable = objPCR001.NCRApplicable;
                objPCR001New.NCRNumber = objPCR001.NCRNumber;
                objPCR001New.NCRRemark = objPCR001.NCRRemark;
                objPCR001New.ReservedforProject = objPCR001.ReservedforProject;
                objPCR001New.ReservedByWhom = objPCR001.ReservedByWhom;
                objPCR001New.ReservationDate = objPCR001.ReservationDate;
                objPCR001New.ItemType = objPCR001.ItemType;
                objPCR001New.CreatedBy = objClsLoginInfo.UserName;
                objPCR001New.CreatedOn = DateTime.Now;
                objPCR001New.EditedBy = objClsLoginInfo.UserName;
                objPCR001New.EditedOn = DateTime.Now;
                db.PCR001.Add(objPCR001New);
            }
            else//Stock Create with revision 0 and End with Alphabet
            {
                int unicodeofA = 65;
                foreach (var objPCR007 in listPCR007)
                {
                    var objPCR001New = new PCR001();
                    objPCR001New.Item = objPCR001.Item;
                    objPCR001New.StockNumber = objPCR001.StockNumber + ((char)(unicodeofA)).ToString();
                    objPCR001New.StockRevision = 0;
                    objPCR001New.Location = objPCR001.Location;
                    objPCR001New.Length = objPCR007.LengthInmm;
                    objPCR001New.Width = objPCR007.WidthInmm;
                    objPCR001New.ARMSet = objPCR001.ARMSet;
                    objPCR001New.QtyInKg = objPCR001.QtyInKg;
                    objPCR001New.Materials = objPCR001.Materials;
                    objPCR001New.Thickness = objPCR001.Thickness;
                    objPCR001New.ReservedQty = objPCR001.ReservedQty;
                    objPCR001New.AllocatedQuantity = objPCR001.AllocatedQuantity;
                    objPCR001New.CutQuantity = objPCR001.CutQuantity;
                    objPCR001New.FreeQuantity = objPCR001.FreeQuantity;
                    objPCR001New.PurchaseOrder = objPCR001.PurchaseOrder;
                    objPCR001New.POPosition = objPCR001.POPosition;
                    objPCR001New.ReceiptNumber = objPCR001.ReceiptNumber;
                    objPCR001New.ReceiptLine = objPCR001.ReceiptLine;
                    objPCR001New.ShapeType = objPCR001.ShapeType;
                    objPCR001New.ShapeFile = objPCR001.ShapeFile;
                    objPCR001New.SizeCode = objPCR001.SizeCode;
                    objPCR001New.Warehouse = objPCR001.Warehouse;
                    objPCR001New.WarehouseLocation = objPCR001.WarehouseLocation;
                    objPCR001New.DerivedConversionFactor = objPCR001.DerivedConversionFactor;
                    objPCR001New.StockStatus = clsImplementationEnum.StockStatus.Created.GetHashCode();
                    objPCR001New.HeatNo = objPCR001.HeatNo;
                    objPCR001New.TCNo = objPCR001.TCNo;
                    objPCR001New.GrainOrientation = objPCR001.GrainOrientation;
                    objPCR001New.CuttingMachine = objPCR001.CuttingMachine;
                    objPCR001New.Area = objPCR007.AreaInm2;
                    objPCR001New.AreaConsumed = objPCR001.AreaConsumed;
                    objPCR001New.PCLNo = string.Empty;
                    objPCR001New.ARMVersion = objPCR001.ARMVersion;
                    objPCR001New.QuantityApproved = objPCR001.QuantityApproved;
                    objPCR001New.ContractNo = objPCR001.ContractNo;
                    objPCR001New.CladThickess1Inmm = objPCR001.CladThickess1Inmm;
                    objPCR001New.CladThickess2Inmm = objPCR001.CladThickess2Inmm;
                    objPCR001New.TheoConvFactForBaseMateInKgPerm3 = objPCR001.TheoConvFactForBaseMateInKgPerm3;
                    objPCR001New.TheoConvFactForClad1InKgPerm3 = objPCR001.TheoConvFactForClad1InKgPerm3;
                    objPCR001New.TheoConvFactForClad2InKgPerm3 = objPCR001.TheoConvFactForClad2InKgPerm3;
                    objPCR001New.TheoreticalWeightInKg = objPCR001.TheoreticalWeightInKg;
                    objPCR001New.StockPhysicalStatus = clsImplementationEnum.StockPhysicalStatus.Virtual.GetHashCode();
                    objPCR001New.ActualLot = objPCR001.ActualLot;
                    objPCR001New.ParentStock = objPCR001.StockNumber;
                    objPCR001New.ParentStockRevision = objPCR001.StockRevision;
                    objPCR001New.OddShape = objPCR007.IsOddShape.HasValue && objPCR007.IsOddShape.Value ? "Yes" : "No";
                    objPCR001New.IssueDirectlyOnShopFloor = objPCR001.IssueDirectlyOnShopFloor;
                    objPCR001New.Project = objPCR001.Project;
                    objPCR001New.Element = objPCR001.Element;
                    objPCR001New.Remark = objPCR001.Remark;
                    objPCR001New.WorkCenter = objPCR001.WorkCenter;
                    objPCR001New.AreaModified = objPCR001.AreaModified;
                    objPCR001New.ReturnStockOrderNo = objPCR001.ReturnStockOrderNo;
                    objPCR001New.IsReturnRetainedStock = objPCR001.IsReturnRetainedStock;
                    objPCR001New.StockRelatedRemark = objPCR001.StockRelatedRemark;
                    objPCR001New.TransferOrderMinusSerialize = objPCR001.TransferOrderMinusSerialize;
                    objPCR001New.RunNoMinusSeriOutboundAdvice = objPCR001.RunNoMinusSeriOutboundAdvice;
                    objPCR001New.IsConsumptionStart = objPCR001.IsConsumptionStart;
                    objPCR001New.StockProject = objPCR001.StockProject;
                    objPCR001New.SOBOutboundKey = objPCR001.SOBOutboundKey;
                    objPCR001New.StockElement = objPCR001.StockElement;
                    objPCR001New.StockActivity = objPCR001.StockActivity;
                    objPCR001New.SCRNo = objPCR001.SCRNo;
                    objPCR001New.ShipmentNo = objPCR001.ShipmentNo;
                    objPCR001New.MTRNo = objPCR001.MTRNo;
                    objPCR001New.NCRApplicable = objPCR001.NCRApplicable;
                    objPCR001New.NCRNumber = objPCR001.NCRNumber;
                    objPCR001New.NCRRemark = objPCR001.NCRRemark;
                    objPCR001New.ReservedforProject = objPCR001.ReservedforProject;
                    objPCR001New.ReservedByWhom = objPCR001.ReservedByWhom;
                    objPCR001New.ReservationDate = objPCR001.ReservationDate;
                    objPCR001New.ItemType = objPCR001.ItemType;
                    objPCR001New.CreatedBy = objClsLoginInfo.UserName;
                    objPCR001New.CreatedOn = DateTime.Now;
                    objPCR001New.EditedBy = objClsLoginInfo.UserName;
                    objPCR001New.EditedOn = DateTime.Now;
                    db.PCR001.Add(objPCR001New);
                    unicodeofA = unicodeofA + 1;
                }
            }
        }
        #endregion

        #region Return PCR
        public JsonResult ReturnPCR(CommonParamEnt param, string ReturnRemark)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                var objPCR004 = db.PCR004.Where(i => i.Id == param.Id).FirstOrDefault();
                var objPCR003 = db.PCR003.Where(i => i.LineId == param.LineId).FirstOrDefault();
                var objPCR002 = db.PCR002.Where(i => i.HeaderId == objPCR003.HeaderId).FirstOrDefault();
                if (!string.IsNullOrEmpty(objPCR003.StockNumber))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = "Please Unlink Stock First.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (string.IsNullOrEmpty(ReturnRemark))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = "Please Enter Return Remark.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var TotalConsume = db.PCR004.Where(i => i.PCRNumber == objPCR002.PCRNumber && i.Id == objPCR003.RefId).Sum(i => i.RequiredQuantity);
                double? diff = 0;

                var objElement = db.SP_PCR_GET_PROJECT_ELEMENT_FROM_LN(objPCR003.Project).Where(i => i.PartNumber == objPCR003.PartNumber).FirstOrDefault();
                objPCR004.Length = objElement.Length;
                objPCR004.Width = objElement.Width;
                objPCR004.NoOfPieces = objElement.NoOfPieces;
                objPCR004.LengthModified = objElement.LengthModified;
                objPCR004.WidthModified = objElement.WidthModified;
                diff = objPCR004.NoOfPieces - TotalConsume;
                objPCR004.RequiredQuantity = diff > 0 ? diff : 0;
                objPCR004.PCRNumber = string.Empty;
                objPCR004.PCRLineNo = null;
                objPCR004.ReturnBy = objClsLoginInfo.UserName;
                objPCR004.ReturnDate = DateTime.Now;
                objPCR004.ReturnRemark = ReturnRemark;


                var objPCR004Delete = db.PCR004.Where(i => i.Project == objPCR003.Project && i.PartNumber == objPCR003.PartNumber && i.Sequence != objPCR004.Sequence).FirstOrDefault();
                db.PCR004.Remove(objPCR004Delete);

                var objPCR003Delete = db.PCR003.Where(i => i.HeaderId == objPCR003.HeaderId && i.PartNumber == objPCR003.PartNumber && i.PCRLineRevNo == objPCR003.PCRLineRevNo).FirstOrDefault();
                db.PCR003.Remove(objPCR003Delete);

                db.SaveChanges();
                if (!db.PCR003.Any(i => i.HeaderId == objPCR002.HeaderId))
                {
                    db.PCR002.Remove(objPCR002);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = PCRReturnSuccess;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region QC PCR

        #region Display Grid
        [SessionExpireFilter, UserPermissions, LastURLHistory]
        public ActionResult QCPCR()
        {
            ViewBag.PLTType = PLTType;
            ViewBag.Role = "QC";
            return View("Index");
        }

        [SessionExpireFilter]
        public PartialViewResult _QCPCL()
        {
            bool IsValidUser = CheckUserIsValid(QCPCRAction);
            ViewBag.QCIsValidUser = IsValidUser;
            return PartialView("_QCPCL");
        }

        [SessionExpireFilter]
        public ActionResult LoadQCDataTable(JQueryDataTableParamModel param, bool? IsDisplayAll)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                //string Project = Request["Project"].ToString();
                if (!IsDisplayAll.HasValue || (IsDisplayAll.HasValue && !IsDisplayAll.Value))
                {
                    whereCondition += " and PCLStatus='" + clsImplementationEnum.PCLStatus.PCLReleased.GetStringValue() + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName ={
                                            "p4.PartNumber",
                                            "p4.ChildItem",
                                            "p4.Sequence",
                                            "p4.ItemType",
                                            "ISNULL(p6.PCLNumber,'')",
                                            "ISNULL(p6.PCLStatus,'')",
                                            "StockPhysicalStatusName",
                                            "ISNULL(p3.UseAlternateStock,'')",
                                            "ISNULL(p3.StockNumber,'')",
                                            "p3.AreaConsumed",
                                            "LocationName",
                                            "WCDeliverName",
                                            "p4.SubLocation",
                                            "p3.WarehouseCuttingLocation",
                                            "p4.Priority",
                                            "p4.PlannerRemark",
                                            "p3.AlternateStockRemark"
                                         };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_PCR_GET_PCR_QC_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = new List<dynamic>();

                foreach (var h in lstHeader)
                {
                    res.Add(new
                    {
                        Area = Convert.ToString(h.Area),
                        Location = Convert.ToString(h.Location),
                        LocationName = Convert.ToString(h.LocationName),
                        MovementType = Convert.ToString(h.MovementType),
                        MovementTypeName = Convert.ToString(h.MovementTypeName),
                        PCLHeaderId = Convert.ToString(h.PCLHeaderId),
                        PCLNumber = Convert.ToString(h.PCLNumber),
                        PCLStatus = Convert.ToString(h.PCLStatus),
                        ReturnAreaInm2 = Convert.ToString(h.ReturnAreaInm2),
                        ReturnBalanceInPcs = Convert.ToString(h.ReturnBalanceInPcs),
                        StockNumber = Convert.ToString(h.StockNumber),
                        StockPhysicalStatusName = Convert.ToString(h.StockPhysicalStatusName),
                        StockRevision = Convert.ToString(h.StockRevision),
                        WorkCenter = Convert.ToString(h.WorkCenter),
                        WorkCenterName = Convert.ToString(h.WorkCenterName),
                    });
                }

                //var isDisplay = true;
                //var distinctRecords = lstHeader.Select(i => new { i.PartGroupNo }).Distinct().ToList();
                //foreach (var item in distinctRecords)
                //{
                //    var hasMultiRecords = false;
                //    var list = (from h in lstHeader
                //                where h.PartGroupNo == item.PartGroupNo
                //                select h).OrderByDescending(i => i.Sequence).ToList();
                //    if (list.Count > 1)
                //    {
                //        hasMultiRecords = true;
                //    }
                //    foreach (var h in list)
                //    {
                //        res.Add(new
                //        {
                //            Id = Convert.ToString(h.Id),
                //            Project = Convert.ToString(h.Project),
                //            PartNumber = (h.SequenceSrNo == 1 ? (hasMultiRecords ? "<img data-gid=\"" + Convert.ToString(h.PartGroupNo.Value) + "\" onclick=\"ExpandCollapsChildQCPCR(this)\" class=\"colexp\" src=\"/Images/details_open.png\" style=\"height:17px;\" data-isexpand=\"false\" />" : "") + Convert.ToString(h.PartNumber) : ""),
                //            ChildItem = Convert.ToString(h.ChildItem),
                //            Sequence = Convert.ToString(h.Sequence),
                //            SequenceSrNo = h.SequenceSrNo,
                //            PartGroupNo = h.PartGroupNo.Value,
                //            ItemType = Convert.ToString(h.ItemType),
                //            PCLNumber = Convert.ToString(h.PCLNumber),
                //            PCLStatus = Convert.ToString(h.PCLStatus),
                //            StockPhysicalStatusName = Convert.ToString(h.StockPhysicalStatusName),
                //            UseAlternateStock = Convert.ToString(h.UseAlternateStock),
                //            StockNumber = Convert.ToString(h.StockNumber),
                //            AreaConsumed = Convert.ToString(h.AreaConsumed),
                //            Location = Convert.ToString(h.Location),
                //            WCDeliverName = Convert.ToString(h.WCDeliverName),
                //            SubLocation = Convert.ToString(h.SubLocation),
                //            WarehouseCuttingLocation = Convert.ToString(h.WarehouseCuttingLocation),
                //            Priority = Convert.ToString(h.Priority),
                //            PlannerRemark = Convert.ToString(h.PlannerRemark),
                //            AlternateStockRemark = Convert.ToString(h.AlternateStockRemark),
                //            Length = Convert.ToString(h.Length),
                //            Width = Convert.ToString(h.Width),
                //            LengthModified = Convert.ToString(h.LengthModified),
                //            WidthModified = Convert.ToString(h.WidthModified),
                //            Thickness = Convert.ToString(h.Thickness),
                //            LineId = Convert.ToString(h.LineId),
                //            PCLHeaderId = Convert.ToString(h.PCLHeaderId),
                //            LocationName = Convert.ToString(h.LocationName)
                //        });
                //    }
                //}

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Validate & Save
        [SessionExpireFilter]
        public JsonResult ValidateApprovePCL(PCR004 objPCR004, PCR003 objPCR003, PCR006 objPCR006)//(List<PCR004> listAllPCR004)//(string Ids)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                //decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                //var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
                var NonPCLReleaseStatus = "";
                var invalidItemType = "";
                var PCLNotGenerated = "";
                //foreach (var item in listAllPCR004)
                //{
                if (objPCR004 != null && objPCR004.ItemType != PLTType)
                {
                    Manager.ConcatString(ref invalidItemType, objPCR004.PartNumber.Value.ToString());
                }
                else
                {
                    //var objPCR003 = db.PCR003.Where(i => i.RefId == item.Id).FirstOrDefault();
                    //var objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == objPCR003.PCLHeaderId).FirstOrDefault();
                    if (objPCR006 == null)
                    {
                        Manager.ConcatString(ref PCLNotGenerated, objPCR004.PartNumber.Value.ToString());
                    }
                    else
                    {
                        if (objPCR006.PCLStatus != clsImplementationEnum.PCLStatus.PCLReleased.GetStringValue())
                        {
                            Manager.ConcatString(ref NonPCLReleaseStatus, objPCR004.PartNumber.Value.ToString());
                        }
                    }
                }
                //}
                if (invalidItemType.Length > 0 || PCLNotGenerated.Length > 0 || NonPCLReleaseStatus.Length > 0)
                {
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = (invalidItemType.Length > 0 ? invalidItemType + " No. Budget Line Is Non-Plate." : "") +
                                            (PCLNotGenerated.Length > 0 ? "PCL Not Generated For " + PCLNotGenerated + "  Budget Line-Sequence." : "") +
                                            (NonPCLReleaseStatus.Length > 0 ? "PCL Is Not In PCL Released Status." : "") +
                                            " Please Unselect That All Budget Line-Sequence!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult ApprovePCLByQA(CommonParamEnt param)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(QCPCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                PCR004 objPCR004 = null;
                //objPCR004 = db.PCR004.Where(i => i.Id == param.Id).FirstOrDefault();
                PCR003 objPCR003 = null;
                PCR006 objPCR006 = null;
                //if (param.LineId.HasValue && param.LineId.Value > 0)
                //{
                //    objPCR003 = db.PCR003.Where(i => i.LineId == param.LineId).FirstOrDefault();
                //}
                if (param.HeaderId.HasValue && param.HeaderId.Value > 0)
                {
                    objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == param.HeaderId).FirstOrDefault();
                }


                //decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                //var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
                //var Validate = ValidateApprovePCL(listAllPCR004);
                var Validate = ValidateApprovePCL(objPCR004, objPCR003, objPCR006);
                var objValidate = ((StockResponseMsg)Validate.Data);
                if (objValidate.Key)
                {
                    //var listLocation = listAllPCR004.Select(i => i.Location).Distinct().ToList();
                    //foreach (var item in listAllPCR004)
                    //{
                    //    var objPCR003 = db.PCR003.Where(i => i.RefId == item.Id).FirstOrDefault();
                    //    var objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == objPCR003.PCLHeaderId).FirstOrDefault();

                    objPCR006.PCLStatus = clsImplementationEnum.PCLStatus.ApprovedbyQA.GetStringValue();
                    objPCR006.QAApprovalBy = objClsLoginInfo.UserName;
                    objPCR006.QAApprovalDate = DateTime.Now;
                    objPCR006.EditedBy = objClsLoginInfo.UserName;
                    objPCR006.EditedOn = DateTime.Now;
                    AddPCLHistory(objPCR006, clsImplementationEnum.PCLStatus.ApprovedbyQA.GetStringValue());
                    //}

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = PCLApprovedByQASuccess;
                }
                else
                {
                    objResponseMsg.Key = objValidate.Key;
                    objResponseMsg.Value = objValidate.Value != null ? objValidate.Value : SomeDataInvalidForUpdate;
                    objResponseMsg.IsInformation = objValidate.IsInformation;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult ValidateReturnPCL(PCR004 objPCR004, PCR003 objPCR003, PCR006 objPCR006)//(List<PCR004> listAllPCR004)//(string Ids)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                //decimal[] arrayIds = Ids.Split(',').Select(i => Convert.ToDecimal(i)).ToArray();
                //var listAllPCR004 = db.PCR004.Where(i => arrayIds.Contains(i.Id)).ToList();
                var NonPCLQAApprovalStatus = "";
                var invalidItemType = "";
                var PCLNotGenerated = "";
                //foreach (var item in listAllPCR004)
                //{
                if (objPCR004 != null && objPCR004.ItemType != PLTType)
                {
                    Manager.ConcatString(ref invalidItemType, objPCR004.PartNumber.Value.ToString());
                }
                else
                {
                    //var objPCR003 = db.PCR003.Where(i => i.RefId == item.Id).FirstOrDefault();
                    //var objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == objPCR003.PCLHeaderId).FirstOrDefault();
                    if (objPCR006 == null)
                    {
                        Manager.ConcatString(ref PCLNotGenerated, objPCR004.PartNumber.Value.ToString());
                    }
                    else
                    {
                        if (objPCR006.PCLStatus != clsImplementationEnum.PCLStatus.PCLReleased.GetStringValue())
                        {
                            Manager.ConcatString(ref NonPCLQAApprovalStatus, objPCR004.PartNumber.Value.ToString());
                        }
                    }
                }
                //}
                if (invalidItemType.Length > 0 || PCLNotGenerated.Length > 0 || NonPCLQAApprovalStatus.Length > 0)
                {
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = (invalidItemType.Length > 0 ? invalidItemType + " No. Budget Line Is Non-Plate." : "") +
                                            (PCLNotGenerated.Length > 0 ? "PCL Not Generated For " + PCLNotGenerated + "  Budget Line-Sequence." : "") +
                                            (NonPCLQAApprovalStatus.Length > 0 ? "PCL Is Not In Approved by QA Status For " + NonPCLQAApprovalStatus + " Budget Line-Sequence." : "") +
                                            "Please Unselect That All Budget Line-Sequence!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter]
        public JsonResult ReturnPCL(CommonParamEnt param, string Remarks, string Require, string Actual)//(string Ids, string Remarks, string Require, string Actual)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            try
            {
                bool IsValidUser = CheckUserIsValid(QCPCRAction);
                if (!IsValidUser)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.IsInformation = true;
                    objResponseMsg.Value = UnAthorizedForUpdateData;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                PCR004 objPCR004 = null;
                PCR003 objPCR003 = null;
                PCR006 objPCR006 = null;
                if (param.HeaderId.HasValue && param.HeaderId.Value > 0)
                {
                    objPCR006 = db.PCR006.Where(i => i.PCLHeaderId == param.HeaderId).FirstOrDefault();
                }
                var Validate = ValidateReturnPCL(objPCR004, objPCR003, objPCR006);

                var objValidate = ((StockResponseMsg)Validate.Data);
                if (objValidate.Key)
                {

                    ModifyPCL(objPCR006);

                    objPCR006.PCLStatus = clsImplementationEnum.PCLStatus.RejectedbyQA.GetStringValue();
                    objPCR006.QARejectionBy = objClsLoginInfo.UserName;
                    objPCR006.QARejectionDate = DateTime.Now;

                    objPCR006.EditedBy = objClsLoginInfo.UserName;
                    objPCR006.EditedOn = DateTime.Now;
                    objPCR006.ReturnRemarkbyQC = Remarks;
                    objPCR006.Require = Require;
                    objPCR006.Actual = Actual;
                    AddPCLHistory(objPCR006, clsImplementationEnum.PCLStatus.RejectedbyQA.GetStringValue(), Remarks);

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = PCLReleaseSuccess;
                }
                else
                {
                    objResponseMsg.Key = objValidate.Key;
                    objResponseMsg.Value = objValidate.Value != null ? objValidate.Value : SomeDataInvalidForUpdate;
                    objResponseMsg.IsInformation = objValidate.IsInformation;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Excel Generate

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType)
        {
            var objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.GeneratePCR.GetStringValue())
                {
                    var lst = db.SP_PCR_GET_PCR_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var isDisplay = true;
                    var data = (from h in lst
                                select new
                                {
                                    Project = Convert.ToString(h.Project),
                                    PartNumber = h.PartNumber,
                                    Sequence = Convert.ToString(h.Sequence),
                                    FindNo = Convert.ToString(h.PartNumber),
                                    ChildItem = Convert.ToString(h.ChildItem),
                                    ItemRevision = Convert.ToString(h.ItemRevision),
                                    ItemMaterialGrade = h.ItemMaterialGradeName,
                                    ItemType = Convert.ToString(h.ItemType),
                                    Length = Convert.ToString(h.Length),
                                    Width = Convert.ToString(h.Width),
                                    Thickness = Convert.ToString(h.Thickness),
                                    NoOfPieces = Convert.ToString(h.NoOfPieces),
                                    PCRType = GetPCRTypeColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.PCRTypeName, h.PCRType, isDisplay),
                                    LengthModified = GetLengthModifiedColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.LengthModified, h.PCRTypeName, isDisplay),
                                    WidthModified = GetWidthModifiedColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.WidthModified, h.PCRTypeName, isDisplay),
                                    RequiredQuantity = GetRequiredQuantityColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.RequiredQuantity, isDisplay),
                                    PCRRequirementDate = GetPCRRequirementDateColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.PCRRequirementDate, isDisplay),
                                    Location = GetLocationColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.LocationName, h.Location, isDisplay),
                                    WCDeliver = GetWCDeliverColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.WCDeliverName, h.WCDeliver, isDisplay),
                                    SubLocation = GetSubLocationColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.SubLocation, isDisplay),
                                    CuttingLocation = GetCuttingLocationColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.CuttingLocation, isDisplay),
                                    Priority = GetPriorityColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.Priority, isDisplay),//Convert.ToString(h.Priority),
                                    PlannerRemark = GetPlannerRemarkColumnDetails(h.PCRNumber, Convert.ToInt32(h.Id), h.ItemType, h.PlannerRemark, isDisplay),
                                    PCRNumber = Convert.ToString(h.PCRNumber),
                                    PCRLineNo = Convert.ToString(h.PCRLineNo),
                                    RequiredForJIGFIX = h.RequiredForJIGFIX.HasValue ? (h.RequiredForJIGFIX.Value ? "Yes" : "No") : "",
                                }).ToList();

                    strFileName = Helper.GenerateExcel(data, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.GeneratePCL.GetStringValue())
                {
                    var lst = db.SP_PCR_GET_PCR_MATERIAL_PLANNER_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var data = (from h in lst
                                select new
                                {
                                    PartNumber = Convert.ToString(h.PartNumber),
                                    ChildItem = Convert.ToString(h.ChildItem),
                                    Sequence = Convert.ToString(h.Sequence),
                                    ItemType = Convert.ToString(h.ItemType),
                                    PCRCreatedBy = Convert.ToString(h.PCRCreatedBy),
                                    PCRNumber = Convert.ToString(h.PCRNumber),
                                    PCRLineNo = Convert.ToString(h.PCRLineNo),
                                    PCRLineStatus = Convert.ToString(h.PCRLineStatus),
                                    Length = Convert.ToString(h.Length),
                                    Width = Convert.ToString(h.Width),
                                    PCRRequirementDate = Convert.ToString(h.PCRRequirementDate),
                                    NestedQuantity = Convert.ToString(h.NestedQuantity),
                                    WCDeliver = Convert.ToString(h.WCDeliver),
                                    UseAlternateStock = Convert.ToString(h.UseAlternateStock),
                                    StockNumber = Convert.ToString(h.StockNumber),
                                    StockRevision = Convert.ToString(h.StockRevision),
                                    AlternateStockRemark = Convert.ToString(h.AlternateStockRemark),
                                    ShapeType = Convert.ToString(h.ShapeType),
                                    ShapeFile = Convert.ToString(h.ShapeFile),
                                    MovementType = Convert.ToString(h.MovementType),
                                    GrainOrientation = Convert.ToString(h.GrainOrientation),
                                    MaterialPlannerRemark = Convert.ToString(h.MaterialPlannerRemark),
                                    ItemMaterialGradeName = Convert.ToString(h.ItemMaterialGradeName),
                                    ARMCode = Convert.ToString(h.ARMCode),
                                    NoofPieces = Convert.ToString(h.NoofPieces),
                                    PCRTypeName = Convert.ToString(h.PCRTypeName),
                                    Thickness = Convert.ToString(h.Thickness),
                                    RequiredQuantity = Convert.ToString(h.RequiredQuantity),
                                    PCRSequenceNo = Convert.ToString(h.PCRSequenceNo),
                                    PCLNumber = Convert.ToString(h.PCLNumber),
                                    AreaConsumed = Convert.ToString(h.AreaConsumed),
                                    ScrapQuantityInm2 = Convert.ToString(h.ScrapQuantityInm2),
                                    ReturnBalanceInPcs = Convert.ToString(h.ReturnBalanceInPcs),
                                    TotalCuttingLength = Convert.ToString(h.TotalCuttingLength),
                                    Planner = Convert.ToString(h.Planner),
                                    Remark = Convert.ToString(h.Remark),
                                    LocationName = Convert.ToString(h.LocationName),
                                    SubLocation = Convert.ToString(h.SubLocation),
                                    WarehouseCuttingLocation = Convert.ToString(h.WarehouseCuttingLocation),
                                    Machine = Convert.ToString(h.Machine),
                                    Week = Convert.ToString(h.Week),
                                    Priority = Convert.ToString(h.Priority),
                                    Duration = Convert.ToString(h.Duration),
                                    StartDate = Convert.ToString(h.StartDate),
                                    EndDate = Convert.ToString(h.EndDate),
                                    PCRPlannerRemark = Convert.ToString(h.PCRPlannerRemark),
                                }).ToList();

                    strFileName = Helper.GenerateExcel(data, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Column Generate Function

        #region Generate PCR
        public string GetLengthModifiedColumnDetails(string PCRNumber, int Id, string ItemType, double? LengthModified, string PCRTypeName, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCRNumber))
            {
                var isreadonly = true;
                if (PCRTypeName == clsImplementationEnum.PCRType.ModifiedCircle.GetStringValue() ||
                    PCRTypeName == clsImplementationEnum.PCRType.ModifiedRectangle.GetStringValue() ||
                    PCRTypeName == clsImplementationEnum.PCRType.ModifiedRing.GetStringValue() ||
                    PCRTypeName == clsImplementationEnum.PCRType.ModifiedRingSegment.GetStringValue())
                {
                    isreadonly = false;
                }
                return (Helper.GenerateTextbox(Id, "LengthModified", (LengthModified.HasValue ? LengthModified.Value.ToString() : ""), "", isreadonly, "", "18", "form-control numeric"));
            }
            else
            {
                return (LengthModified.HasValue ? LengthModified.Value.ToString() : "");
            }
        }
        public string GetWidthModifiedColumnDetails(string PCRNumber, int Id, string ItemType, double? WidthModified, string PCRTypeName, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCRNumber))
            {
                var isreadonly = true;
                if (PCRTypeName == clsImplementationEnum.PCRType.ModifiedCircle.GetStringValue() ||
                    PCRTypeName == clsImplementationEnum.PCRType.ModifiedRectangle.GetStringValue() ||
                    PCRTypeName == clsImplementationEnum.PCRType.ModifiedRing.GetStringValue() ||
                    PCRTypeName == clsImplementationEnum.PCRType.ModifiedRingSegment.GetStringValue())
                {
                    isreadonly = false;
                }
                return (Helper.GenerateTextbox(Id, "WidthModified", (WidthModified.HasValue ? WidthModified.Value.ToString() : ""), "", isreadonly, "", "18", "form-control numeric"));
            }
            else
            {
                return (WidthModified.HasValue ? WidthModified.Value.ToString() : "");
            }
        }
        public string GetPCRRequirementDateColumnDetails(string PCRNumber, int Id, string ItemType, DateTime? PCRRequirementDate, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCRNumber))
            {
                //var PCRDate = PCRRequirementDate.HasValue ? PCRRequirementDate.Value.ToString("dd/MM/yyyy hh:mm tt") : "";
                //return Helper.GenerateHTMLDateTimeControl(Convert.ToInt32(Id), "PCRRequirementDate", PCRDate, false, true, false, "dd/mm/yyyyTHH:ii:00Z");
                var PCRDate = PCRRequirementDate.HasValue ? PCRRequirementDate.Value.ToString("dd/MM/yyyy") : "";
                return Helper.GenerateHTMLDateControl(Convert.ToInt32(Id), "PCRRequirementDate", PCRDate, false, "datecontrol");
            }
            else
            {
                //return PCRRequirementDate.HasValue ? PCRRequirementDate.Value.ToString("dd/MM/yyyy hh:mm tt") : "";
                return PCRRequirementDate.HasValue ? PCRRequirementDate.Value.ToString("dd/MM/yyyy") : "";
            }
        }
        public string GetRequiredQuantityColumnDetails(string PCRNumber, int Id, string ItemType, double? RequiredQuantity, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCRNumber))
            {
                return (Helper.GenerateTextbox(Convert.ToInt32(Id), "RequiredQuantity",
                                            (RequiredQuantity.HasValue ? RequiredQuantity.Value.ToString() : ""), "", false, "", "18", "form-control requiredquantity"));
            }
            else
            {
                return (RequiredQuantity.HasValue ? RequiredQuantity.Value.ToString() : "");
            }
        }
        public string GetWCDeliverColumnDetails(string PCRNumber, int Id, string ItemType, string WCDeliverName, string WCDeliver, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCRNumber))
            {
                return (Helper.HTMLAutoComplete(Convert.ToInt32(Id), "txtWCDeliver", Convert.ToString(WCDeliverName), "", false, "", "WCDeliver", false, "100", "", "W/C Deliver", "form-control wcdeliver") +
                                          Helper.GenerateHidden(Convert.ToInt32(Id), "WCDeliver", Convert.ToString(WCDeliver)));
            }
            else
            {
                return Convert.ToString(WCDeliverName);
            }
        }
        public string GetPCRTypeColumnDetails(string PCRNumber, int Id, string ItemType, string PCRTypeName, string PCRType, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCRNumber))
            {
                return Helper.HTMLAutoComplete(Convert.ToInt32(Id), "PCRType", Convert.ToString(PCRTypeName), "", false, "", "PCRType", false, "100", "", "PCRType", "form-control pcrtype");
            }
            else
            {
                return PCRTypeName;
            }
        }
        public string GetPlannerRemarkColumnDetails(string PCRNumber, int Id, string ItemType, string PlannerRemark, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCRNumber))
            {
                return Helper.GenerateTextbox(Convert.ToInt32(Id), "PlannerRemark", Convert.ToString(PlannerRemark), "", false, "", "100", "form-control plannerremark");
            }
            else
            {
                return Convert.ToString(PlannerRemark);
            }
        }
        public string GetCuttingLocationColumnDetails(string PCRNumber, int Id, string ItemType, string CuttingLocation, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCRNumber))
            {
                return Helper.HTMLAutoComplete(Convert.ToInt32(Id), "CuttingLocation", Convert.ToString(CuttingLocation), "", false, "", "CuttingLocation", false, "100", "", "Cutting Location", "form-control cuttinglocation");
            }
            else
            {
                return Convert.ToString(CuttingLocation);
            }
        }
        public string GetLocationColumnDetails(string PCRNumber, int Id, string ItemType, string LocationName, string Location, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCRNumber))
            {
                return Helper.HTMLAutoComplete(Convert.ToInt32(Id), "txtLocation", Convert.ToString(LocationName), "", false, "", "Location", false, "100", "", "Location", "form-control location") +
                                         Helper.GenerateHidden(Convert.ToInt32(Id), "Location", Convert.ToString(Location));
            }
            else
            {
                return Convert.ToString(LocationName);
            }
        }
        public string GetSubLocationColumnDetails(string PCRNumber, int Id, string ItemType, string SubLocation, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCRNumber))
            {
                return Helper.GenerateTextbox(Convert.ToInt32(Id), "SubLocation", Convert.ToString(SubLocation), "", false, "", "40", "form-control sublocation");
            }
            else
            {
                return Convert.ToString(SubLocation);
            }
        }
        public string GetPriorityColumnDetails(string PCRNumber, int Id, string ItemType, double? Priority, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCRNumber))
            {
                return Helper.GenerateTextbox(Convert.ToInt32(Id), "Priority", (Priority.HasValue ? Convert.ToString(Priority) : ""), "", false, "", "9", "form-control priority");
            }
            else
            {
                return (Priority.HasValue ? Convert.ToString(Priority) : "");
            }
        }
        //public string GetElementCheckBoxSelectionColumnDetails(int Id, string ItemType,bool IsDisplay = false)
        //{
        //    if (ItemType == PLTType && !IsDisplay)
        //    {
        //        return Helper.GenerateCheckbox(Convert.ToInt32(Id), "chkBudgetLine", false);
        //    }
        //    else
        //    {
        //        return "";
        //    }
        //}
        public bool IsEnableElementSelection(string PCRNumber, string PCRType, double? RequiredQuantity, DateTime? PCRRequirementDate, string Location, string WCDeliverName, string CuttingLocation, double? Priority)
        {
            if (!string.IsNullOrEmpty(PCRType) &&
                RequiredQuantity.HasValue &&
                PCRRequirementDate.HasValue &&
                !string.IsNullOrEmpty(Location) &&
                !string.IsNullOrEmpty(WCDeliverName) &&
                !string.IsNullOrEmpty(CuttingLocation) &&
                Priority.HasValue &&
                string.IsNullOrEmpty(PCRNumber)
              )
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public string GeneratePCRRequirementDate(string PCRNumber, int Id)
        {
            var str = "";
            str = "<div class=\"input-group date form_datetime\" data-date-format=\"dd/mm/yyyy hh:ii\">" +
                 "                   <input type = \"text\" size = \"16\" readonly class=\"form-control\" id=\"txtToDate" + Id + "\" name=\"txtToDate" + Id + "\">" +
                 "           <span class=\"input-group-btn\">" +
                 "               <button id=\"btn" + Id + "\" class=\"btn default date-set\" type=\"button\">" +
                 //"                    <span class=\"glyphicon glyphicon-calendar\"></span>" +
                 "                      <i class=\"fa fa-calendar\"></i>" +
                 "               </button>" +
                 "           </span>" +
                 "       </div>";
            return str;
        }
        #endregion

        #region Material Planner PCR
        public string GetLengthModifiedMPPCRColumnDetails(string PCRNumber, string PCLNumber, int Id, string ItemType, double? Length, string PCRTypeName, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCLNumber))
            {
                var isreadonly = true;
                if (PCRTypeName == clsImplementationEnum.PCRType.ModifiedCircle.GetStringValue() ||
                    PCRTypeName == clsImplementationEnum.PCRType.ModifiedRectangle.GetStringValue() ||
                    PCRTypeName == clsImplementationEnum.PCRType.ModifiedRing.GetStringValue() ||
                    PCRTypeName == clsImplementationEnum.PCRType.ModifiedRingSegment.GetStringValue())
                {
                    isreadonly = false;
                }
                return (Helper.GenerateTextbox(Id, "Length", (Length.HasValue ? Length.Value.ToString() : ""), "", isreadonly, "", "18", "form-control numeric"));
            }
            else
            {
                return (Length.HasValue ? Length.Value.ToString() : "");
            }
        }
        public string GetWidthModifiedMPPCRColumnDetails(string PCRNumber, string PCLNumber, int Id, string ItemType, double? Width, string PCRTypeName, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCLNumber))
            {
                var isreadonly = true;
                if (PCRTypeName == clsImplementationEnum.PCRType.ModifiedCircle.GetStringValue() ||
                    PCRTypeName == clsImplementationEnum.PCRType.ModifiedRectangle.GetStringValue() ||
                    PCRTypeName == clsImplementationEnum.PCRType.ModifiedRing.GetStringValue() ||
                    PCRTypeName == clsImplementationEnum.PCRType.ModifiedRingSegment.GetStringValue())
                {
                    isreadonly = false;
                }
                return (Helper.GenerateTextbox(Id, "Width", (Width.HasValue ? Width.Value.ToString() : ""), "", isreadonly, "", "18", "form-control numeric"));
            }
            else
            {
                return (Width.HasValue ? Width.Value.ToString() : "");
            }
        }
        public string GetPCRRequirementDateMPPCRColumnDetails(string PCRNumber, string PCLNumber, int Id, string ItemType, DateTime? PCRRequirementDate, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCLNumber))
            {
                //var PCRDate = PCRRequirementDate.HasValue ? PCRRequirementDate.Value.ToString("dd/MM/yyyy hh:mm tt") : "";
                //return Helper.GenerateHTMLDateTimeControl(Convert.ToInt32(Id), "PCRRequirementDate", PCRDate, false, true, false, "dd/mm/yyyyTHH:ii:00Z");
                var PCRDate = PCRRequirementDate.HasValue ? PCRRequirementDate.Value.ToString("dd/MM/yyyy") : "";
                return Helper.GenerateHTMLDateControl(Convert.ToInt32(Id), "PCRRequirementDate", PCRDate, false, "datecontrol");
            }
            else
            {
                //return PCRRequirementDate.HasValue ? PCRRequirementDate.Value.ToString("dd/MM/yyyy hh:mm tt") : "";
                return PCRRequirementDate.HasValue ? PCRRequirementDate.Value.ToString("dd/MM/yyyy") : "";
            }
        }
        public string GetStockNumberMPPCRColumnDetails(string PCRNumber, string PCLNumber, int Id, string ItemType, string StockNumber, double? StockRevision, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCLNumber))
            {
                return Helper.HTMLAutoComplete(Convert.ToInt32(Id), "txtStockNumber", Convert.ToString(StockNumber) + "-R" + (StockRevision.HasValue ? StockRevision.Value : 0).ToString(), "", false, "", "txtStockNumber", false, "100", "", "StockNumber", "form-control stocknumber") +
                       Helper.GenerateHidden(Convert.ToInt32(Id), "StockRevision", (StockRevision.HasValue ? StockRevision.Value : 0).ToString()) +
                       Helper.GenerateHidden(Convert.ToInt32(Id), "StockNumber", Convert.ToString(StockNumber));
            }
            else
            {
                return Convert.ToString(StockNumber);
            }
        }
        public string GetWCDeliverMPPCRColumnDetails(string PCLStatus, string PCRNumber, string PCLNumber, int Id, string ItemType, string WCDeliverName, string WCDeliver, bool IsDisplay = false)
        {
            var WCDeliverMPPCRColumnDetails = "";
            if (ItemType == PLTType && !IsDisplay && (PCLStatus != clsImplementationEnum.PCRLineStatus.PCLReleased.GetStringValue()))
            {

                if (!string.IsNullOrEmpty(PCLNumber))
                {
                    WCDeliverMPPCRColumnDetails = Helper.HTMLAutoComplete(Convert.ToInt32(Id), "txtWCDeliver", Convert.ToString(WCDeliverName), "", false, "", "WorkCenter", false, "100", "", "W/C Deliver", "form-control wcdeliver");
                    WCDeliverMPPCRColumnDetails += Helper.GenerateHidden(Convert.ToInt32(Id), "WorkCenter", Convert.ToString(WCDeliver));
                }
                else
                {
                    WCDeliverMPPCRColumnDetails = Helper.HTMLAutoComplete(Convert.ToInt32(Id), "txtWCDeliver", Convert.ToString(WCDeliverName), "", false, "", "WCDeliver", false, "100", "", "W/C Deliver", "form-control wcdeliver");
                    WCDeliverMPPCRColumnDetails += Helper.GenerateHidden(Convert.ToInt32(Id), "WCDeliver", Convert.ToString(WCDeliver));
                }
            }
            else
            {
                WCDeliverMPPCRColumnDetails = Convert.ToString(WCDeliverName);
            }
            return WCDeliverMPPCRColumnDetails;
        }
        public string GetNestedQuantityMPPCRColumnDetails(string PCRNumber, string PCLNumber, int Id, string ItemType, double? NestedQuantity, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCLNumber))
            {
                return Helper.GenerateTextbox(Convert.ToInt32(Id), "NestedQuantity", (NestedQuantity.HasValue ? Convert.ToString(NestedQuantity) : ""), "", false, "", "9", "form-control nestedquantity");
            }
            else
            {
                return (NestedQuantity.HasValue ? Convert.ToString(NestedQuantity) : "");
            }
        }
        public string GetShapeTypeMPPCRColumnDetails(string PCRNumber, string PCLNumber, int Id, string ItemType, string ShapeType, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCLNumber))
            {
                return Helper.GenerateTextbox(Convert.ToInt32(Id), "ShapeType", Convert.ToString(ShapeType), "", false, "", "100", "form-control shapetype");
            }
            else
            {
                return Convert.ToString(ShapeType);
            }
        }
        public string GetShapeFileMPPCRColumnDetails(string PCRNumber, string PCLNumber, int Id, string ItemType, string ShapeFile, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCLNumber))
            {
                return Helper.GenerateTextbox(Convert.ToInt32(Id), "ShapeFile", Convert.ToString(ShapeFile), "", false, "", "100", "form-control shapefile");
            }
            else
            {
                return Convert.ToString(ShapeFile);
            }
        }
        public string GetMaterialPlannerRemarkMPPCRColumnDetails(string PCRNumber, string PCLNumber, int Id, string ItemType, string MaterialPlannerRemark, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCLNumber))
            {
                return Helper.GenerateTextbox(Convert.ToInt32(Id), "MaterialPlannerRemark", Convert.ToString(MaterialPlannerRemark), "", false, "", "100", "form-control matlremark");
            }
            else
            {
                return Convert.ToString(MaterialPlannerRemark);
            }
        }
        public string GetAlternateStockRemarkMPPCRColumnDetails(string PCRNumber, string PCLNumber, int Id, string ItemType, string AlternateStockRemark, string UseAlternateStock, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCLNumber))
            {
                bool isDisabled = !string.IsNullOrEmpty(UseAlternateStock) && Convert.ToString(UseAlternateStock).ToUpper() == "YES" ? false : true;
                return Helper.GenerateTextbox(Convert.ToInt32(Id), "AlternateStockRemark", Convert.ToString(AlternateStockRemark), "", false, "", "100", "form-control stockremark", isDisabled);
            }
            else
            {
                return Convert.ToString(AlternateStockRemark);
            }
        }
        public string GetGrainOrientationMPPCRColumnDetails(string PCRNumber, string PCLNumber, int Id, string ItemType, string GrainOrientation, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCLNumber))
            {
                return Helper.GenerateTextbox(Convert.ToInt32(Id), "GrainOrientation", Convert.ToString(GrainOrientation), "", false, "", "100", "form-control grainorientation");
            }
            else
            {
                return Convert.ToString(GrainOrientation);
            }
        }
        public string GetMovementTypeMPPCRColumnDetails(string PCLStatus, string PCRNumber, string PCLNumber, int Id, string ItemType, string MovementTypeName, int? MovementType, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && (PCLStatus != clsImplementationEnum.PCRLineStatus.PCLReleased.GetStringValue()))
            {
                return Helper.HTMLAutoComplete(Convert.ToInt32(Id), "txtMovementType", Convert.ToString(MovementTypeName), "", false, "", "txtMovementType", false, "100", "", "MovementType", "form-control movementtype");
            }
            else
            {
                return Convert.ToString(MovementTypeName);
            }
        }
        public bool IsEnableMPPCRElementSelection(string PCLNumber, DateTime? PCRRequirementDate, double? NestedQuantity, string StockNumber, string UseAlternateStock, string AlternateStockRemark)
        {
            if (NestedQuantity.HasValue &&
                PCRRequirementDate.HasValue &&
                !string.IsNullOrEmpty(StockNumber) &&
                string.IsNullOrEmpty(PCLNumber)
              )
            {
                if (!string.IsNullOrEmpty(UseAlternateStock) && UseAlternateStock.ToUpper() == "YES")
                {
                    if (!string.IsNullOrEmpty(AlternateStockRemark))
                        return true;
                    else
                        return false;
                }
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool IsEnableMPPCLReleaseElementSelection(string PCLNumber, double? TotalCuttingLength, string WorkCenter, string Planner)
        {
            if (!string.IsNullOrEmpty(PCLNumber)
                && TotalCuttingLength.HasValue
                && !string.IsNullOrEmpty(WorkCenter)
                && !string.IsNullOrEmpty(Planner))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public string GetReturnBalancePCSMPPCRColumnDetails(string PCLStatus, string PCRNumber, string PCLNumber, int Id, string ItemType, double? ReturnBalanceInPcs, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && !string.IsNullOrEmpty(PCLNumber) && (PCLStatus != clsImplementationEnum.PCRLineStatus.PCLReleased.GetStringValue()))
            {
                return (Helper.GenerateTextbox(Convert.ToInt32(Id), "ReturnBalanceInPcs",
                                            (ReturnBalanceInPcs.HasValue ? ReturnBalanceInPcs.Value.ToString() : ""), "", false, "", "18", "form-control numeric returnbalance"));
            }
            else
            {
                return (ReturnBalanceInPcs.HasValue ? ReturnBalanceInPcs.Value.ToString() : "");
            }
        }
        public string GetTotalCuttingLengthMPPCRColumnDetails(string PCLStatus, string PCRNumber, string PCLNumber, int Id, string ItemType, double? TotalCuttingLength, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && !string.IsNullOrEmpty(PCLNumber) && (PCLStatus != clsImplementationEnum.PCRLineStatus.PCLReleased.GetStringValue()))
            {
                return (Helper.GenerateTextbox(Convert.ToInt32(Id), "TotalCuttingLength",
                                            (TotalCuttingLength.HasValue ? TotalCuttingLength.Value.ToString() : ""), "", false, "", "18", "form-control totalcuttinglength"));
            }
            else
            {
                return (TotalCuttingLength.HasValue ? TotalCuttingLength.Value.ToString() : "");
            }
        }
        public string GetPlannerMPPCRColumnDetails(string PCLStatus, string PCRNumber, string PCLNumber, int Id, string ItemType, string Planner, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && !string.IsNullOrEmpty(PCLNumber) && (PCLStatus != clsImplementationEnum.PCRLineStatus.PCLReleased.GetStringValue()))
            {
                return Helper.HTMLAutoComplete(Convert.ToInt32(Id), "txtPlanner", Convert.ToString(Planner), "", false, "", "Planner", false, "100", "", "Planner", "form-control planner") +
                       Helper.GenerateHidden(Convert.ToInt32(Id), "Planner", Planner);
            }
            else
            {
                return Planner;
            }
        }
        public string GetRemarkMPPCRColumnDetails(string PCLStatus, string PCRNumber, string PCLNumber, int Id, string ItemType, string Remark, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && !string.IsNullOrEmpty(PCLNumber) && (PCLStatus != clsImplementationEnum.PCRLineStatus.PCLReleased.GetStringValue()))
            {
                return (Helper.GenerateTextbox(Convert.ToInt32(Id), "Remark", Remark, "", false, "", "30", "form-control remark"));
            }
            else
            {
                return Remark;
            }
        }
        public string GetAreaConsumedMPPCRColumnDetails(string PCRNumber, string PCLNumber, int Id, string ItemType, double? AreaConsumed, string PCRTypeName, bool IsDisplay = false)
        {
            if (ItemType == PLTType && !IsDisplay && string.IsNullOrEmpty(PCLNumber))
            {
                var isreadonly = false;
                return (Helper.GenerateTextbox(Id, "AreaConsumed", (AreaConsumed.HasValue ? AreaConsumed.Value.ToString() : "0"), "", isreadonly, "", "18", "form-control areaconsumed"));
            }
            else
            {
                return (AreaConsumed.HasValue ? AreaConsumed.Value.ToString() : "");
            }
        }
        #endregion

        #region Return Balance Distribution
        public string GetLengthRBDColumnDetails(int Id, double? LengthInmm, bool? IsOddShape, bool IsDisplay = false)
        {
            if (!IsDisplay)
            {
                var IsDisable = !IsOddShape.HasValue || !IsOddShape.Value;
                return (Helper.GenerateTextbox(Id, "LengthInmm", (LengthInmm.HasValue ? LengthInmm.Value.ToString() : ""), "", false, "", "18", "form-control rbddecimal", !IsDisable));
            }
            else
            {
                return (LengthInmm.HasValue ? LengthInmm.Value.ToString() : "");
            }
        }
        public string GetWidthRBDColumnDetails(int Id, double? WidthInmm, bool? IsOddShape, bool IsDisplay = false)
        {
            if (!IsDisplay)
            {
                var IsDisable = !IsOddShape.HasValue || !IsOddShape.Value;
                return (Helper.GenerateTextbox(Id, "WidthInmm", (WidthInmm.HasValue ? WidthInmm.Value.ToString() : ""), "", false, "", "18", "form-control rbddecimal", !IsDisable));
            }
            else
            {
                return (WidthInmm.HasValue ? WidthInmm.Value.ToString() : "");
            }
        }
        public string GetAreaRBDColumnDetails(int Id, double? AreaInm2, bool? IsOddShape, bool IsDisplay = false)
        {
            if (!IsDisplay)
            {
                var IsDisable = !IsOddShape.HasValue || !IsOddShape.Value;
                return (Helper.GenerateTextbox(Id, "AreaInm2", (AreaInm2.HasValue ? AreaInm2.Value.ToString() : ""), "", false, "", "18", "form-control rbddecimal", IsDisable));
            }
            else
            {
                return (AreaInm2.HasValue ? AreaInm2.Value.ToString() : "");
            }
        }
        public string GetOddShapeRBDColumnDetails(int Id, bool? IsOddShape, bool IsDisplay = false)
        {
            if (!IsDisplay)
            {
                return "<label class=\"mt-checkbox mt-checkbox-outline\"><input type=\"checkbox\" onchange=\"chkIsOddShape_Change(this," + Id + ")\" id=\"chkIsOddShape" + Id + "\" name=\"id[]\" class=\"clsCheckbox\" value=\"" + Id + "\"  ><span></span></label>";
            }
            else
            {
                return (IsOddShape.HasValue && IsOddShape.Value ? "Yes" : "No");
            }
        }
        #endregion

        #endregion

        #region Common Functions
        public string GeneratePCRNo(string Location, ref double MaxDocNo)
        {
            string PCRNumber = "";
            var PCRNoLength = 0;
            var PCRSeriesStart = "";
            try
            {
                if (db.PCR002.Any(i => i.Location == Location))
                {
                    var objPCR005 = db.PCR005.Where(i => i.Location == Location).FirstOrDefault();
                    MaxDocNo = db.PCR002.Where(i => i.Location == Location).Max(i => i.PCRDocNo).Value + 1;
                    if (objPCR005 != null)
                    {
                        if (objPCR005.PCRNumberlength.HasValue)
                        {
                            PCRNoLength = objPCR005.PCRNumberlength.Value;
                        }
                        else
                        {
                            PCRNoLength = 9;
                        }
                        PCRSeriesStart = objPCR005.PCRSeriesStart;
                    }
                    PCRNumber = PCRSeriesStart + ((int)MaxDocNo).ToString("D" + (PCRNoLength - PCRSeriesStart.Length));
                }
                else
                {
                    var objPCR005 = db.PCR005.Where(i => i.Location == Location).FirstOrDefault();
                    if (objPCR005 != null)
                    {
                        if (objPCR005.FirstFreeNo.HasValue)
                        {
                            MaxDocNo = objPCR005.FirstFreeNo.Value + 1;
                        }
                        else
                        {
                            MaxDocNo = 1;
                        }
                        if (objPCR005.PCRNumberlength.HasValue)
                        {
                            PCRNoLength = objPCR005.PCRNumberlength.Value;
                        }
                        else
                        {
                            PCRNoLength = 9;
                        }
                        PCRSeriesStart = objPCR005.PCRSeriesStart;
                        PCRNumber = PCRSeriesStart + ((int)MaxDocNo).ToString("D" + (PCRNoLength - PCRSeriesStart.Length));
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return PCRNumber;
        }

        public bool CheckUserIsValid(string ActionName)
        {
            bool IsValid = false;
            if (objClsLoginInfo.listMenuResult != null)
            {
                var lstMenuResult = objClsLoginInfo.listMenuResult;
                if (lstMenuResult.Any(i => (string.IsNullOrEmpty(i.Action) ? "" : i.Action.ToLower()) == ActionName.ToLower()))
                {
                    IsValid = true;
                }
            }
            return IsValid;
        }

        public JsonResult GetStockNumberForMaterialPlannerPCR(string term, string Item, string Location, bool UseAlternateStock, string ProjectNo)
        {
            List<SP_PCR_GET_STOCK_NUMBER_FOR_MATERIAL_PLANNER_PCR_Result> listStockNumber = new List<SP_PCR_GET_STOCK_NUMBER_FOR_MATERIAL_PLANNER_PCR_Result>();
            try
            {
                if (UseAlternateStock)
                {
                    if (db.COM005.Any(i => i.t_sprj == ProjectNo))
                    {
                        var ContractNo = db.COM005.Where(i => i.t_sprj == ProjectNo).FirstOrDefault().t_cono;
                        if (!string.IsNullOrEmpty(ContractNo))
                        {
                            listStockNumber = db.SP_PCR_GET_STOCK_NUMBER_FOR_MATERIAL_PLANNER_PCR("", Location, ContractNo).Where(i => i.ContractNo == ContractNo && i.StockNumber.Contains((string.IsNullOrEmpty(term) ? i.StockNumber : term))).ToList();
                        }
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(Item) && !string.IsNullOrEmpty(Location))
                    {
                        listStockNumber = db.SP_PCR_GET_STOCK_NUMBER_FOR_MATERIAL_PLANNER_PCR(Item, Location, "").Where(i => i.StockNumber.Contains((string.IsNullOrEmpty(term) ? i.StockNumber : term))).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(listStockNumber, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ValidateStockSelection(decimal PCRLineId, string StockNumber, double StockRevision, bool UseAlternateStock)
        {
            StockResponseMsg objResponseMsg = new StockResponseMsg();
            //dynamic objResponseMsg = new ExpandoObject();
            try
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "";
                //objResponseMsg.IsMaterialDifferent = false;

                var DeleteStatus = clsImplementationEnum.PCRLineStatus.Deleted.GetStringValue();
                var objPCR003 = db.PCR003.Where(i => i.LineId == PCRLineId).FirstOrDefault();
                var objPCR002 = db.PCR002.Where(i => i.HeaderId == objPCR003.HeaderId).FirstOrDefault();
                var objPCR001 = db.PCR001.Where(i => i.StockNumber == StockNumber && i.StockRevision == StockRevision).FirstOrDefault();
                var objPreviousPCR = (from p3 in db.PCR003
                                      join p2 in db.PCR002 on p3.HeaderId equals p2.HeaderId
                                      where p3.Project == objPCR003.Project
                                        && p3.ChildItem == objPCR003.ChildItem
                                        && p3.PartNumber == objPCR003.PartNumber
                                        && p3.CreatedOn < objPCR003.CreatedOn
                                        && ((p2.PCRNumber != objPCR002.PCRNumber) || (p3.PCRLineNo != objPCR003.PCRLineNo) || (p3.PCRLineRevNo != objPCR003.PCRLineRevNo))
                                        && string.IsNullOrEmpty(p3.StockNumber) && p3.PCRLineStatus != DeleteStatus
                                      select p3).FirstOrDefault();
                if (objPreviousPCR != null)
                {
                    var objPCR002PreviousPCR = db.PCR002.Where(i => i.HeaderId == objPreviousPCR.HeaderId).FirstOrDefault();
                    //var objPCR004 = db.PCR004.Where(i => i.PCRLineId == objPreviousPCR.LineId).FirstOrDefault();
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Proceed previous PCR No.:" + objPCR002PreviousPCR.PCRNumber + ", PCR Line:" + objPreviousPCR.PCRLineNo + " and Revision:" + objPreviousPCR.PCRLineRevNo;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var ContractNo = db.COM005.Where(i => i.t_sprj == objPCR003.Project).FirstOrDefault().t_cono;
                if (db.PCR001.Any(i => i.ContractNo != ContractNo && i.StockNumber == StockNumber && i.StockRevision == StockRevision))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "PCR Line does not belong to Contract mentioned in Stock.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                if (!string.IsNullOrEmpty(objPCR001.PCLNo))
                {
                    var objPCL = db.PCR006.Where(i => i.PCLNumber == objPCR001.PCLNo).FirstOrDefault();
                    if (objPCL != null && !string.IsNullOrEmpty(objPCL.PCLStatus))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "PCL " + objPCL.PCLNumber + " already Released for Stock Number " + objPCR001.StockNumber + " Revision " + objPCR001.StockRevision;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                if (objPCR001.StockPhysicalStatus.HasValue && (objPCR001.StockPhysicalStatus.Value == clsImplementationEnum.StockPhysicalStatus.Consumed.GetHashCode()))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Stock Number " + objPCR001.StockNumber + " Revision " + objPCR001.StockRevision + " is already Consumed.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                if (objPCR001.ReservedforProject.HasValue && objPCR001.ReservedforProject.Value == 1)
                {
                    var objCOM003 = db.COM003.Where(i => i.t_psno == objPCR001.ReservedByWhom).FirstOrDefault();
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Stock Number is reserved by " + objPCR001.ReservedByWhom + " - " + (objCOM003 != null ? objCOM003.t_name : "");
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetActiveEmployee(string search = "", string param = "")
        {
            var lstEmployee = Manager.GetActiveEmployee();
            List<ddlValue> lst = new List<ddlValue>();
            lst = lstEmployee.Select(x => new ddlValue { id = x.id.ToString(), text = x.text.ToString() }).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public void AddPCLHistory(PCR006 objPCR006, string PCLStatus, string Remarks = "")
        {
            try
            {
                PCR008 objPCR008 = new PCR008();
                objPCR008.CreatedBy = objClsLoginInfo.UserName;
                objPCR008.CreatedOn = DateTime.Now;
                objPCR008.PCLHeaderId = objPCR006.PCLHeaderId;
                objPCR008.PCLNumber = objPCR006.PCLNumber;
                objPCR008.PCLStatus = PCLStatus;
                objPCR008.Revision = objPCR006.Revision;
                objPCR008.Remark = Remarks;
                objPCR006.PCR008.Add(objPCR008);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public PartialViewResult _ViewPCRLines(decimal PCLHeaderId)
        {
            ViewBag.PCLHeaderId = PCLHeaderId;
            return PartialView("_ViewPCRLines");
        }

        public void ModifyPCL(PCR006 objPCR006)
        {
            double? TotalNestedQty = 0;
            double? NoOfPieces = 0;
            var ScrapQty = objPCR006.ScrapQuantityInm2;
            var DeleteStatus = clsImplementationEnum.PCRLineStatus.Deleted.GetStringValue();

            var listPCR003 = db.PCR003.Where(i => i.PCLNumber == objPCR006.PCLNumber).ToList();
            foreach (var objPCR003 in listPCR003)
            {
                var objPCR002 = db.PCR002.Where(i => i.HeaderId == objPCR003.HeaderId).FirstOrDefault();
                var objLastPCR = GetLastPCRSequenceDetails(objPCR002, objPCR003);
                var CaseNo = 0;
                if (string.IsNullOrEmpty(objLastPCR.PCLNumber))
                {
                    CaseNo = 3;
                }
                else
                {
                    if (objLastPCR.StockNumber == objPCR003.StockNumber)
                    {
                        CaseNo = 1;
                    }
                    else
                    {
                        CaseNo = 2;
                    }
                }

                TotalNestedQty = GetTotalNestedQuantity(objPCR002, objPCR003);
                switch (CaseNo)
                {
                    case 1:
                        NoOfPieces = objLastPCR.NoofPieces + TotalNestedQty - objLastPCR.NestedQuantity;
                        UpdatePCRStatus(objPCR002, objPCR003);
                        UpdateStockStatus(objPCR003);
                        GenerateNewPCRRevision(objPCR002, objPCR003, objLastPCR, NoOfPieces);
                        break;
                    case 2:
                        NoOfPieces = TotalNestedQty;
                        UpdatePCRStatus(objPCR002, objPCR003);
                        UpdateStockStatus(objPCR003);
                        GenerateNewPCRRevision(objPCR002, objPCR003, objLastPCR, NoOfPieces);
                        break;
                    case 3:
                        NoOfPieces = objLastPCR.NoofPieces + TotalNestedQty;
                        UpdatePCRStatus(objPCR002, objPCR003);
                        UpdateStockStatus(objPCR003);
                        GenerateNewPCRRevision(objPCR002, objPCR003, objLastPCR, NoOfPieces);
                        UpdateStatusForLastPCRRevision(objLastPCR);
                        break;
                    default:
                        break;
                }
            }

            var listPCR007 = db.PCR007.Where(i => i.PCLHeaderId == objPCR006.PCLHeaderId).ToList();
            db.PCR007.RemoveRange(listPCR007);

            var objLastPCL006 = db.PCR006.Where(i => i.PCLNumber == objPCR006.PCLNumber).OrderByDescending(i => i.Revision).FirstOrDefault();
            objLastPCL006.ReturnAreaInm2 = objLastPCL006.ReturnAreaInm2 + ScrapQty;
            objLastPCL006.ScrapQuantityInm2 = 0;
            objLastPCL006.PCLStatus = string.Empty;
            objLastPCL006.PCLReleaseBy = null;
            objLastPCL006.PCLReleaseDate = null;
            objLastPCL006.PMGConfirmationDate = null;
            objLastPCL006.QAApprovalBy = null;
            objLastPCL006.QAApprovalDate = null;

            var objPCR001 = db.PCR001.Where(i => i.PCLNo == objPCR006.PCLNumber).FirstOrDefault();
            objPCR001.FreeQuantity = objPCR001.FreeQuantity + ScrapQty;

            db.SaveChanges();
        }

        public PCR003 GetLastPCRSequenceDetails(PCR002 objPCR002, PCR003 objPCR003)
        {
            var objLastPCR003 = db.PCR003.Where(i => i.HeaderId == objPCR002.HeaderId && i.PCRLineNo == objPCR003.PCRLineNo).OrderByDescending(i => i.PCRLineRevNo).FirstOrDefault();
            return objLastPCR003;
        }

        public double? GetTotalNestedQuantity(PCR002 objPCR002, PCR003 objPCR003)
        {
            var DeleteStatus = clsImplementationEnum.PCRLineStatus.Deleted.GetStringValue();
            var TotalNestedQuantity = db.PCR003.Where(i => i.HeaderId == objPCR002.HeaderId && i.PCRLineNo == objPCR003.PCRLineNo && i.StockNumber == objPCR003.StockNumber && i.PCRLineRevNo >= objPCR003.PCRLineRevNo && i.PCRLineStatus != DeleteStatus).Sum(i => i.NestedQuantity);
            return TotalNestedQuantity;
        }

        public void UpdatePCRStatus(PCR002 objPCR002, PCR003 objPCR003)
        {
            var DeleteStatus = clsImplementationEnum.PCRLineStatus.Deleted.GetStringValue();
            var listPCR003 = db.PCR003.Where(i => i.HeaderId == objPCR002.HeaderId && i.PCRLineNo == objPCR003.PCRLineNo && i.StockNumber == objPCR003.StockNumber && i.PCRLineRevNo >= objPCR003.PCRLineRevNo && i.PCRLineStatus != DeleteStatus).ToList();
            foreach (var item in listPCR003)
            {
                item.PCRLineStatus = clsImplementationEnum.PCRLineStatus.Deleted.GetStringValue();
            }
            db.SaveChanges();
        }

        public void UpdateStockStatus(PCR003 objPCR003)
        {
            var objPCR001 = db.PCR001.Where(i => i.StockNumber == objPCR003.StockNumber && i.Item == objPCR003.ChildItem && i.StockRevision > objPCR003.StockRevision).ToList();
            foreach (var item in objPCR001)
            {
                item.StockStatus = clsImplementationEnum.StockStatus.Deleted.GetHashCode();
            }
            db.SaveChanges();
        }

        public void GenerateNewPCRRevision(PCR002 objPCR002, PCR003 objPCR003, PCR003 objLastPCR003, double? NoofPieces)
        {
            PCR003 objPCR003New = new PCR003();
            objPCR003New.PCRLineRevNo = objLastPCR003.PCRLineRevNo + 1;
            objPCR003New.StockNumber = objPCR003.StockNumber;
            objPCR003New.StockRevision = objPCR003.StockRevision;
            objPCR003New.NoofPieces = NoofPieces;
            objPCR003New.NestedQuantity = objPCR003.NestedQuantity;
            objPCR003New.AreaConsumed = objPCR003.AreaConsumed;
            objPCR003New.ScrapQuantity = 0;
            objPCR003New.PCRLineStatus = clsImplementationEnum.PCRLineStatus.PCLGenerated.GetStringValue();
            objPCR003New.PCLCreationDate = DateTime.Now;
            objPCR003New.PCLCreatedBy = objClsLoginInfo.UserName;
            objPCR003New.PCLReleaseDate = null;
            objPCR003New.PCLReleasedBy = null;

            objPCR003New.HeaderId = objPCR003.HeaderId;
            objPCR003New.PCRLineNo = objPCR003.PCRLineNo;
            objPCR003New.PartNumber = objPCR003.PartNumber;
            objPCR003New.ManufacturingItem = objPCR003.ManufacturingItem;
            objPCR003New.ChildItem = objPCR003.ChildItem;
            objPCR003New.Length = objPCR003.Length; ;
            objPCR003New.Width = objPCR003.Width;
            objPCR003New.PCRType = objPCR003.PCRType;
            objPCR003New.PCRRequirementDate = objPCR003.PCRRequirementDate;
            objPCR003New.WCDeliver = objPCR003.WCDeliver;
            objPCR003New.ShopFloorWarhouse = objPCR003.ShopFloorWarhouse;
            objPCR003New.WarehouseCuttingLocation = objPCR003.WarehouseCuttingLocation;
            objPCR003New.PCLNumber = objPCR003.PCLNumber;
            objPCR003New.MovementType = objPCR003.MovementType;
            objPCR003New.PCRPlannerRemark = objPCR003.PCRPlannerRemark;
            objPCR003New.MaterialPlannerRemark = objPCR003.MaterialPlannerRemark;
            objPCR003New.PCRCreationDate = objPCR003.PCRCreationDate;
            objPCR003New.PCRReleaseDate = objPCR003.PCRReleaseDate;
            objPCR003New.Project = objPCR003.Project;
            objPCR003New.Element = objPCR003.Element;
            objPCR003New.ManufacturingItemMinusPosition = objPCR003.ManufacturingItemMinusPosition;
            objPCR003New.Thickness = objPCR003.Thickness;
            objPCR003New.GrainOrientation = objPCR003.GrainOrientation;
            objPCR003New.ShapeType = objPCR003.ShapeType;
            objPCR003New.ShapeFile = objPCR003.ShapeFile;
            objPCR003New.Area = objPCR003.Area;
            objPCR003New.ImmediateParentItem = objPCR003.ImmediateParentItem;
            objPCR003New.PCRHardCopyNumber = objPCR003.PCRHardCopyNumber;
            objPCR003New.UseAlternateStock = objPCR003.UseAlternateStock;
            objPCR003New.AlternateStockRemark = objPCR003.AlternateStockRemark;
            objPCR003New.OriginalItem = objPCR003.OriginalItem;
            objPCR003New.QCApprovalStatus = objPCR003.QCApprovalStatus;
            objPCR003New.InspectionQty = objPCR003.InspectionQty;
            objPCR003New.ItemRevision = objPCR003.ItemRevision;
            objPCR003New.LinkToSCR = objPCR003.LinkToSCR;
            objPCR003New.SCRNumber = objPCR003.SCRNumber;
            objPCR003New.RejectedQty = objPCR003.RejectedQty;
            objPCR003New.MCRPrepared = objPCR003.MCRPrepared;
            objPCR003New.ReturnRemark = objPCR003.ReturnRemark;
            objPCR003New.P1_OD = objPCR003.P1_OD;
            objPCR003New.P2_Length = objPCR003.P2_Length;
            objPCR003New.P3_Width = objPCR003.P3_Width;
            objPCR003New.P4_OD = objPCR003.P4_OD;
            objPCR003New.P5_ID = objPCR003.P5_ID;
            objPCR003New.P6_OD = objPCR003.P6_OD;
            objPCR003New.P7_ID = objPCR003.P7_ID;
            objPCR003New.P8_Deg = objPCR003.P8_Deg;
            objPCR003New.P9 = objPCR003.P9; ;
            objPCR003New.P10 = objPCR003.P10;
            objPCR003New.ItemType = objPCR003.ItemType;
            objPCR003New.CreatedBy = objClsLoginInfo.UserName;
            objPCR003New.CreatedOn = DateTime.Now;
            objPCR003New.EditedBy = objPCR003.EditedBy;
            objPCR003New.EditedOn = objPCR003.EditedOn;
            objPCR003New.PCRCreatedBy = objPCR003.PCRCreatedBy;
            objPCR003New.PCRReleasedBy = objPCR003.PCRReleasedBy;
            objPCR003New.PCRRetractBy = objPCR003.PCRRetractBy;
            objPCR003New.PCRRetractOn = objPCR003.PCRRetractOn;
            objPCR003New.PCLHeaderId = objPCR003.PCLHeaderId;
            objPCR003New.RefId = objPCR003.RefId;
            db.PCR003.Add(objPCR003New);
            db.SaveChanges();
        }

        public void UpdateStatusForLastPCRRevision(PCR003 objLastPCR003)
        {
            if (!string.IsNullOrEmpty(objLastPCR003.StockNumber))
            {
                var objPCR001 = db.PCR001.Where(i => i.Item == objLastPCR003.ChildItem && i.StockNumber == objLastPCR003.StockNumber && i.StockRevision == objLastPCR003.StockRevision).FirstOrDefault();
                objPCR001.AllocatedQuantity = objPCR001.AllocatedQuantity - objLastPCR003.AreaConsumed;
                objPCR001.FreeQuantity = objPCR001.FreeQuantity + objLastPCR003.AreaConsumed;
            }
            objLastPCR003.PCRLineStatus = clsImplementationEnum.PCRLineStatus.Deleted.GetStringValue();
            db.SaveChanges();
        }

        #endregion

    }
    public class CommonParamEnt
    {
        public decimal? Id { get; set; }
        public decimal? LineId { get; set; }
        public decimal? HeaderId { get; set; }
    }
}