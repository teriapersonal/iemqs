﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.PCR.Models
{
    public class ActionOutput
    {
        public bool Continue;
        public string Message;
    }
}