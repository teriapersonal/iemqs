﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.PCR.Models
{
    public class ActionInput
    {
        public string ActionName;
        public dynamic Parameters;
    }
}