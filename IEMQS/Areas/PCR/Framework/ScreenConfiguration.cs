﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using IEMQSImplementation.ILN;
using IEMQSImplementation.ILN.Domain.Enums;
using IEMQSImplementation.ILN.Domain.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace IEMQS.Areas.ILN.Framework
{
    public class ScreenConfiguration
    {
        private string _AppName;
        private List<ToolbarItem> _baseToolBarItemsList;
        private List<Table> _tablesList;
        private List<Session> _sessionsList;
        public ScreenConfiguration(string appName)
        {
            _AppName = appName;
            _baseToolBarItemsList = ReadFromSource<ToolbarItem>();
            _tablesList = ReadFromSource<Table>();
            _sessionsList = ReadFromSource<Session>();
        }
        public List<Screen> GetScreens(bool fill = true)
        {
            List<Screen> screens = ReadFromSource<Screen>();
            if (fill)
            {
                foreach (Screen screen in screens)
                {
                    FillReferredScreenObjects(screen);
                }
            }
            return screens;
        }

        public Screen GetScreen(string name, string referenceTableName = null, string referenceColumnName = null)
        {
            Screen screen = null;
            List<Screen> screens = GetScreens(fill: false);
            screen = screens.Find(x => x.Name.ToUpper() == name.ToUpper());
            if (referenceTableName != null && referenceColumnName != null) //Dyamic window
            {
                Table table = _tablesList.Find(x => x.Name == referenceTableName);
                if (table != null)
                {
                    Session referredSession = _sessionsList.Find(x => x.TableId == table.Id);
                    screen.MainSessionId = referredSession.Id;
                    screen.Name = referredSession.Name;
                    screen.FriendlyName = referredSession.FriendlyName;
                }
            }
            FillReferredScreenObjects(screen);
            return screen;
        }

        private void FillReferredScreenObjects(Screen screen)
        {
            List<Session> sessions = ReadFromSource<Session>();
            screen.MainSession = sessions.Where(x => x.Id == screen.MainSessionId).FirstOrDefault();
            if (screen.OccurenceTypeId > 0)
            {
                FillReferredOccurenceTypeObjects(screen);                
            }
            if (screen.MainSession != null)
            {
                FillReferredSessionObjects(screen.MainSession);
            }
            if (screen.SatelliteSessionIds != null && screen.SatelliteSessionIds.Count > 0)
            {
                List<Session> satelliteSessions = new List<Session>();
                foreach (int id in screen.SatelliteSessionIds)
                {
                    Session satelliteSession = sessions.Where(x => x.Id == id).FirstOrDefault();
                    FillReferredSessionObjects(satelliteSession);
                    satelliteSessions.Add(satelliteSession);
                }
                screen.SatelliteSessions = satelliteSessions;
            }
        }

        private void FillReferredOccurenceTypeObjects(Screen screen)
        {
            List<OccurenceType> occurenceTypes = ReadFromSource<OccurenceType>();
            screen.OccurenceType = occurenceTypes.Where(x => x.Id == screen.OccurenceTypeId).FirstOrDefault();
            screen.ToolbarItems = GetToolbarItemObjects(screen.OccurenceType.ToolbarItems);
        }

        private List<List<ToolbarItem>> GetToolbarItemObjects(List<List<ToolbarItem>> groupOfToolbarItems)
        {            
            if (groupOfToolbarItems != null)
            {                
                JsonMergeSettings jsonMergeSettings = new JsonMergeSettings
                {
                    MergeNullValueHandling = MergeNullValueHandling.Ignore,
                    MergeArrayHandling = MergeArrayHandling.Union
                };
                
                List<List<ToolbarItem>> mergedGroupOfToolbarItems = new List<List<ToolbarItem>>();
                foreach (List<ToolbarItem> lst in groupOfToolbarItems)
                {
                    List<ToolbarItem> mergedList = new List<ToolbarItem>();
                    foreach (ToolbarItem toolbarItem in lst)
                    {
                        string toolBarItemJSON = JsonConvert.SerializeObject(toolbarItem);
                        ToolbarItem item = _baseToolBarItemsList.Where(x => x.Id == toolbarItem.Id).FirstOrDefault();
                        string baseToolBarItemJSON= JsonConvert.SerializeObject(item);
                        JObject jObjectToolBarItem = JObject.Parse(toolBarItemJSON);
                        JObject jObjectBaseToolBarItem = JObject.Parse(baseToolBarItemJSON);
                        JObject result = new JObject();
                        result.Merge(jObjectBaseToolBarItem, jsonMergeSettings);
                        result.Merge(jObjectToolBarItem, jsonMergeSettings);
                        string mergedJson = JsonConvert.SerializeObject(result);
                        ToolbarItem mergedToolbarItem = JsonConvert.DeserializeObject<ToolbarItem>(mergedJson);
                        FillChildProperties(mergedToolbarItem);
                        mergedList.Add(mergedToolbarItem);
                    }
                    mergedGroupOfToolbarItems.Add(mergedList);
                }
                return mergedGroupOfToolbarItems;
            }
            return null;
        }

        private void FillChildProperties(ToolbarItem toolbarItem)
        {
            if (toolbarItem.ChildMenuToolbarItems != null)
            {
                List<ToolbarItem> childList = new List<ToolbarItem>();
                foreach (ToolbarItem item in toolbarItem.ChildMenuToolbarItems)
                {
                    ToolbarItem filledItem = _baseToolBarItemsList.Where(x => x.Id == item.Id).FirstOrDefault();
                    childList.Add(filledItem);
                    FillChildProperties(filledItem);
                }
                toolbarItem.ChildMenuToolbarItems = childList;
            }            
        }

        private void FillReferredSessionObjects(Session session)
        {
            session.Table = _tablesList.Find(x => x.Id == session.TableId);
            if (session.Table != null)
            {
                session.TableName = session.Table.Name;
                if(session.Table.Indices!=null && session.Table.Indices.Count > 0)
                {
                    foreach(Index index in session.Table.Indices)
                    {
                        foreach(Column column in index.Columns)
                        {
                            ColumnDefinition dbColumn = session.Table.Columns.Where(x => x.Id == column.Id).FirstOrDefault();
                            Column column1 = SetColumn(dbColumn, column);
                            column.DisplayName = column1.DisplayName;
                            column.DisplayOrder = column1.DisplayOrder;
                            column.FriendlyName = column1.FriendlyName;
                            column.Name = column1.Name;
                            column.Type = column1.Type;
                        }
                    }
                }
                if (session.MultiOccurenceInstance != null)
                {
                    if (session.MultiOccurenceInstance.ViewColumns != null && session.MultiOccurenceInstance.ViewColumns.Count > 0)
                    {
                        foreach (ViewColumn column in session.MultiOccurenceInstance.ViewColumns)
                        {
                            ColumnDefinition dbColumn = session.Table.Columns.Where(x => x.Id == column.Id).FirstOrDefault();
                            ViewColumn column1 = SetColumnDefinition(dbColumn, column);
                            column.Constraints = column1.Constraints;
                            column.DependentOnColumn = column1.DependentOnColumn;
                            column.DescriptionColumn = column1.DescriptionColumn;
                            column.DisplayName = column1.DisplayName;
                            column.DisplayOrder = column1.DisplayOrder;
                            column.FriendlyName = column1.FriendlyName;
                            column.IsMandatory = column1.IsMandatory;
                            column.Name = column1.Name;
                            column.ReferenceColumn = column1.ReferenceColumn;
                            column.ReferenceTable = column1.ReferenceTable;
                            column.Type = column1.Type;
                            

                        }
                    }
                    if (session.MultiOccurenceInstance.GridColumns != null && session.MultiOccurenceInstance.GridColumns.Count > 0)
                    {
                        foreach (GridColumn column in session.MultiOccurenceInstance.GridColumns)
                        {
                            ColumnDefinition dbColumn = session.Table.Columns.Where(x => x.Id == column.Id).FirstOrDefault();
                            GridColumn column1 = SetColumnDefinition(dbColumn, column);
                            column.Constraints = column1.Constraints;
                            column.DependentOnColumn = column1.DependentOnColumn;
                            column.DescriptionColumn = column1.DescriptionColumn;
                            column.DisplayName = column1.DisplayName;
                            column.DisplayOrder = column1.DisplayOrder;
                            column.FriendlyName = column1.FriendlyName;
                            column.IsMandatory = column1.IsMandatory;
                            column.Name = column1.Name;
                            column.ReferenceColumn = column1.ReferenceColumn;
                            column.ReferenceTable = column1.ReferenceTable;
                            column.Type = column1.Type;
                        }
                    }
                }
                //TODO Fill Single Occurence Instance
            }
                      
            
            session.ToolbarItems = GetToolbarItemObjects(session.ToolbarItems);
        }

        private T SetColumnDefinition<T>(ColumnDefinition dbColumn, T column) where T : ColumnDefinition
        {
            JsonMergeSettings jsonMergeSettings = new JsonMergeSettings
            {
                MergeNullValueHandling = MergeNullValueHandling.Ignore,
                MergeArrayHandling = MergeArrayHandling.Union
            };
            string itemJSON = JsonConvert.SerializeObject(column);
            string baseJSON = JsonConvert.SerializeObject(dbColumn);
            JObject jObjectItem = JObject.Parse(itemJSON);
            JObject jObjectBaseItem = JObject.Parse(baseJSON);
            JObject result = new JObject();
            result.Merge(jObjectItem, jsonMergeSettings);
            result.Merge(jObjectBaseItem, jsonMergeSettings);            
            string mergedJson = JsonConvert.SerializeObject(result);
            return JsonConvert.DeserializeObject<T>(mergedJson);
        }

        private T SetColumn<T>(ColumnDefinition dbColumn, T column) where T : Column
        {
            JsonMergeSettings jsonMergeSettings = new JsonMergeSettings
            {
                MergeNullValueHandling = MergeNullValueHandling.Ignore,
                MergeArrayHandling = MergeArrayHandling.Union
            };
            string itemJSON = JsonConvert.SerializeObject(column);
            string baseJSON = JsonConvert.SerializeObject(dbColumn);
            JObject jObjectItem = JObject.Parse(itemJSON);
            JObject jObjectBaseItem = JObject.Parse(baseJSON);
            JObject result = new JObject();
            result.Merge(jObjectItem, jsonMergeSettings);
            result.Merge(jObjectBaseItem, jsonMergeSettings);           
            string mergedJson = JsonConvert.SerializeObject(result);
            return JsonConvert.DeserializeObject<T>(mergedJson);
        }

        private List<T> ReadFromSource<T>()
        {
            return ReadFromJSON<T>();
            //TODO return ReadFromDb<T>;//change here when reading from DB
        }

        private List<T> ReadFromDb<T>()
        {
            //TODO Implement
            return null;
        }

        private List<T> ReadFromJSON<T>()
        {
            string configPath = @"D:\n\iemqs\IEMQS\Resources\ILN\Config\" + _AppName + @"\";
            List<T> obj = null;
            string fileName = string.Empty;
            if (typeof(T) == typeof(Screen))
            {
                fileName = "config.screens.json";
            }
            else if (typeof(T) == typeof(Session))
            {
                fileName = "config.sessions.json";
            }
            else if (typeof(T) == typeof(ToolbarItem))
            {
                fileName = "config.screen.toolbar.item.json";
            }
            else if (typeof(T) == typeof(OccurenceType))
            {
                fileName = "config.screen.occurence.type.json";
            }
            else if (typeof(T) == typeof(Table))
            {
                fileName = "config.tables.json";
            }
            using (StreamReader r = new StreamReader(configPath + fileName))
            {
                string json = r.ReadToEnd();
                obj = JsonConvert.DeserializeObject<List<T>>(json);
            }
            return obj;
        }

    }
}