﻿using IEMQSImplementation.ILN.Persistence;
using IEMQSImplementation.ILN.Persistence.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Threading.Tasks;
using System.Configuration;
using IEMQSImplementation.ILN.Domain.Models;
using Newtonsoft.Json;
using System.Data.SqlClient;
using IEMQS.Areas.PCR.Models;
namespace IEMQS.Areas.ILN.Framework
{
    public class DataAdapter
    {

        /// <summary>
        /// Common method for getting data for the screen which has occurence type - multi occurence
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="procedureName"></param>
        /// <param name="tableName"></param>
        /// <param name="indexName"></param>
        /// <param name="indexColumnNames"></param>
        /// <param name="viewColumnNames"></param>
        /// <param name="viewColumnValues"></param>
        /// <param name="tabularColumnNames"></param>
        /// <returns></returns>
        public async Task<List<List<IDictionary<string, object>>>> GetMultiOccurenceData(
            string userId,
            string procedureName,
            int resultsCount,
            string tableName,
            bool isLNTable,
            int? navigatorIndex,
            string indexName,
            string indexColumnNames,
            string viewColumnNames,
            string viewColumnValues,
            string navigatorType,
            string tabularColumns)
        {
            DataConnectionParams<DynamicParameter> connectionParams;
            DataConnection dataConnection;
            List<List<IDictionary<string, object>>> managedLists = null;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                connectionParams = new DataConnectionParams<DynamicParameter>(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = procedureName,
                    ResultCount = resultsCount,
                    Params = new List<DynamicParameter>()
                        {
                            new DynamicParameter() { DbType = DbType.String , Name = "UserId", Value = userId },
                            new DynamicParameter() { DbType = DbType.String, Name = "TableName", Value = tableName },
                            new DynamicParameter() { DbType = DbType.Boolean, Name = "IsLNTable", Value = isLNTable },
                            new DynamicParameter() { DbType = DbType.String, Name = "IndexName", Value = indexName },                            
                            new DynamicParameter() { DbType = DbType.String, Name = "IndexColumnNames", Value = indexColumnNames },
                            new DynamicParameter() { DbType = DbType.String, Name = "ViewColumnNames", Value = viewColumnNames },
                            new DynamicParameter() { DbType = DbType.String, Name = "ViewColumnValues", Value = viewColumnValues },
                            new DynamicParameter() { DbType = DbType.String, Name = "NavigatorType", Value = navigatorType },
                            new DynamicParameter() { DbType = DbType.Int32, Name = "NavigatorIndex", Value = navigatorIndex },
                            new DynamicParameter() { DbType = DbType.String, Name = "TabularColumnNames", Value = tabularColumns }
                        }
                };
                managedLists = await dataConnection.QueryDynamicMultipleAsync(connectionParams);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return managedLists;
        }
        public async Task<ActionOutput> FetchData(string projectValue, string elementValue, string locationValue, string initValue)
        {
            ActionOutput output = null;
            DataConnection dataConnection;
            try
            {
                dataConnection = new DataConnection(ConfigurationManager.ConnectionStrings["IEMQS"].ConnectionString);
                DataConnectionParams connectionParams = new DataConnectionParams(DataConnectionTypeEnum.StoredProcedure)
                {
                    Query = "[dbo].[SP_PCR_GENERATE_PCR_ACTIONS_FETCH_DATA]",

                    Params = new
                    {
                        Project = projectValue,
                        Element = elementValue,
                        T_Loca = locationValue,
                        T_Init = initValue
                    }
                };
                output = new ActionOutput();
                List<int> queryResult = (await dataConnection.QueryAsync<int>(connectionParams)).ToList();
                output.Continue = queryResult != null && queryResult.Count == 1 && queryResult.First() == 1;
            }
            catch (SqlException ex)
            {
                output.Message = ex.Message;
            }
            catch (Exception ex)
            {
                output.Message = ex.Message;
            }
            return output;
        }
    }
}