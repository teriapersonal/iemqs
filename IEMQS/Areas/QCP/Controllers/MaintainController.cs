﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using System.Data.Entity.Core.Objects;
using IEMQS.Areas.QCP.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;

namespace IEMQS.Areas.QCP.Controllers
{


    public class MaintainController : clsBase
    {
        // GET: QCP/Maintain
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        #region Header
        public ActionResult loadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {

            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qcp1.BU", "qcp1.Location");
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and status in('" + clsImplementationEnum.QCPStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.QCPStatus.Returned.GetStringValue() + "')";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (qcp1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or DocumentNo like '%" + param.sSearch + "%' or QCPRev like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_QCP_GETHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {

                        Convert.ToString(h.HeaderId),
                        h.Project,//db.COM001.Where(i=>i.t_cprj==h.Project).Select(i=>i.t_dsca).FirstOrDefault(),//h.Project,
                        h.BU,//db.COM002.Where(i=>i.t_dtyp==2 && i.t_dimx==h.BU).Select(i=>i.t_desc).FirstOrDefault(),//h.BU,
                        h.DocumentNo,
                        "<span>R</span>"+Convert.ToString(h.QCPRev),
                        h.Status,
                        Convert.ToString(h.CreatedBy),
                        Convert.ToString(h.EditedBy),
                        h.Location//db.COM002.Where(i=>i.t_dtyp==1 && i.t_dimx==h.Location).Select(i=>i.t_desc).FirstOrDefault(),//h.Location,
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult AddHeader(int? headerId)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("QCP001,QCP002,QCP003");
            QCP001 objQCP001 = new QCP001();

            try
            {
                string user = objClsLoginInfo.UserName;

                string BU = string.Join(",", db.ATH001.Where(i => i.Employee == user).Select(i => i.BU).Distinct().ToList());
                //List<Projects> project = new List<Projects>();
                List<Projects> project = Manager.getProjectsByUser(user);

                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.t_actv == 1
                                select b.t_dimx + " - " + b.t_desc).FirstOrDefault();
                ViewBag.TPIAgency = Manager.GetSubCatagories("TPI Agency", BU, location.Split('-')[0]).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).Distinct().ToList();


                if (headerId > 0)
                {
                    NDEModels objNDEModels = new NDEModels();
                    objQCP001 = db.QCP001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    var lstAgency = Manager.GetSubCatagories("TPI Agency", objQCP001.BU, objQCP001.Location).ToList();
                    ViewBag.TP = lstAgency.Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).Distinct().ToList();


                    ViewBag.TP1 = !string.IsNullOrEmpty(objQCP001.TP1) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP1, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
                    ViewBag.TP2 = !string.IsNullOrEmpty(objQCP001.TP2) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP2, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
                    ViewBag.TP3 = !string.IsNullOrEmpty(objQCP001.TP3) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP3, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
                    ViewBag.TP4 = !string.IsNullOrEmpty(objQCP001.TP4) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP4, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
                    ViewBag.TP5 = !string.IsNullOrEmpty(objQCP001.TP5) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP5, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
                    ViewBag.TP6 = !string.IsNullOrEmpty(objQCP001.TP6) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP6, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
                    ViewBag.Project = Manager.GetProjectAndDescription(objQCP001.Project);//new SelectList(project, "projectCode", "projectDescription", objQCP001.Project);
                    ViewBag.Location = location;// new SelectList(lstLocation, "t_dimx", "Location", objQCP001.Location);
                    ViewBag.Action = "Edit";
                }
                else
                {
                    ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
                    objQCP001.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                    objQCP001.QCPRev = 0;
                    ViewBag.Location = location;// new SelectList(lstLocation, "t_dimx", "Location");
                    ViewBag.Action = "AddNew";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            return View(objQCP001);
        }

        public ActionResult GetQCPHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetQCPHeaderGridDataPartial");
        }

        [HttpPost]
        public ActionResult getPurchaseDet(string projectCd, string location)
        {
            PurchaseDetail objQCPCommon = new PurchaseDetail();
            var objQCPHeader = db.QCP001.Where(i => i.Project == projectCd && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).ToList();

            try
            {
                if (!string.IsNullOrEmpty(projectCd))
                {

                    if (objQCPHeader != null && objQCPHeader.Count > 0)// && action.ToLower() != "edit")
                    {
                        objQCPCommon.headerAlreadyExist = true;
                    }
                    else
                    {
                        objQCPCommon = Manager.getProjectWiseBULocation(projectCd);
                        var lstAgency = Manager.GetSubCatagories("TPI Agency", objQCPCommon.QCPBU.Split('-')[0], location.Split('-')[0]).ToList();
                        objQCPCommon.TPIAgency = lstAgency.Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).Distinct().ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
            return Json(objQCPCommon);
        }

        public ActionResult GetProjectPurchaseDetail(int headerID)
        {
            PurchaseDetail objPurchaseDetail = new PurchaseDetail();
            QCP001 objQCP001 = new QCP001();

            if (headerID > 0)
            {
                objQCP001 = db.QCP001.Where(i => i.HeaderId == headerID).FirstOrDefault();
                if (objQCP001 != null)
                {
                    //objPurchaseDetail.QCPBU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx.Equals(objQCP001.BU, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                    objPurchaseDetail = Manager.getProjectWiseBULocation(objQCP001.Project);
                }
            }

            return Json(objPurchaseDetail);
        }

        public bool isProjectAlreadyExist(string projectCd, string location)
        {
            bool flag = false;

            var lstQCP = db.QCP001.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).ToList();
            if (!string.IsNullOrEmpty(projectCd))
            {
                if (lstQCP.Contains(projectCd))
                {
                    flag = true;
                }
            }

            return flag;
        }

        [HttpPost]
        //public ActionResult SaveHeader(FormCollection fc, QCP001 qcp001, bool hasAttachments, Dictionary<string, string> Attach)
        public ActionResult SaveHeader(FormCollection fc, QCP001 qcp001)
        {

            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                if (fc != null)
                {
                    QCP001 objQCP001 = new QCP001();
                    if (qcp001.HeaderId > 0)
                    {
                        #region QCPHeader Update
                        objQCP001 = db.QCP001.Where(i => i.HeaderId == qcp001.HeaderId).FirstOrDefault();
                        objQCP001.DocumentNo = qcp001.DocumentNo;
                        objQCP001.Consultant = qcp001.Consultant;
                        objQCP001.ASME = qcp001.ASME;
                        objQCP001.Specification = qcp001.Specification;
                        objQCP001.Code = qcp001.Code;
                        objQCP001.AbbreviationCode = qcp001.AbbreviationCode;
                        objQCP001.AbbreviationDesc = qcp001.AbbreviationDesc;
                        if (string.Equals(objQCP001.Status, clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))//(objQCP001.Status.ToLower() == "approved")
                        {
                            objQCP001.Status = "Draft";
                            objQCP001.QCPRev = objQCP001.QCPRev + 1;
                        }
                        if (string.Equals(objQCP001.Status, clsImplementationEnum.QCPStatus.Returned.GetStringValue(), StringComparison.OrdinalIgnoreCase))//(objQCP001.Status.ToLower() == "approved")
                        {
                            objQCP001.Status = "Draft";
                        }
                        //objQCP001.Status = "Draft";
                        objQCP001.TP1 = qcp001.TP1;
                        objQCP001.TP2 = qcp001.TP2;
                        objQCP001.TP3 = qcp001.TP3;
                        objQCP001.TP4 = qcp001.TP4;
                        objQCP001.TP5 = qcp001.TP5;
                        objQCP001.TP6 = qcp001.TP6;
                        objQCP001.EditedBy = objClsLoginInfo.UserName;
                        objQCP001.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = objQCP001.HeaderId.ToString();
                        objResponseMsg.Status = objQCP001.Status;
                        objResponseMsg.HeaderId = Convert.ToInt32(objQCP001.HeaderId);
                        objResponseMsg.RevNo = Convert.ToString(objQCP001.QCPRev);
                        #endregion
                    }
                    else
                    {
                        #region Add New QCPHeader                     
                        objQCP001.Project = qcp001.Project;
                        objQCP001.BU = qcp001.BU.Split('-')[0];
                        objQCP001.Location = qcp001.Location.Split('-')[0];
                        objQCP001.QCPRev = 0;
                        objQCP001.DocumentNo = qcp001.DocumentNo;
                        objQCP001.Consultant = qcp001.Consultant;
                        objQCP001.ASME = qcp001.ASME;
                        objQCP001.Specification = qcp001.Specification;
                        objQCP001.Code = qcp001.Code;
                        objQCP001.AbbreviationCode = qcp001.AbbreviationCode;
                        objQCP001.AbbreviationDesc = qcp001.AbbreviationDesc;
                        objQCP001.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                        objQCP001.TP1 = qcp001.TP1;
                        objQCP001.TP2 = qcp001.TP2;
                        objQCP001.TP3 = qcp001.TP3;
                        objQCP001.TP4 = qcp001.TP4;
                        objQCP001.TP5 = qcp001.TP5;
                        objQCP001.TP6 = qcp001.TP6;
                        objQCP001.CreatedBy = objClsLoginInfo.UserName;
                        objQCP001.CreatedOn = DateTime.Now;
                        db.QCP001.Add(objQCP001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = objQCP001.HeaderId.ToString();
                        objResponseMsg.Status = objQCP001.Status;
                        objResponseMsg.HeaderId = Convert.ToInt32(objQCP001.HeaderId);
                        objResponseMsg.RevNo = Convert.ToString(objQCP001.QCPRev);
                        #endregion
                    }
                    // var folderPath = "QCP001/" + objQCP001.HeaderId + "/" + objQCP001.QCPRev;
                    // Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteHeader(int headerId)
        {
            QCP001 objQCP001 = new QCP001();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            try
            {
                if (isHavingLines(headerId))
                {
                    var lstLineIds = db.QCP002.Where(i => i.HeaderId == headerId).ToList();

                    var lineIds = lstLineIds.Select(i => i.LineId);
                    if (lstLineIds != null && lstLineIds.Count > 0)
                    {
                        var lstSubLines = (from a in db.QCP003
                                           where lineIds.Contains(a.LineId)
                                           select a).ToList();
                        if (lstSubLines != null && lstSubLines.Count > 0)
                        {
                            db.QCP003.RemoveRange(lstSubLines);
                            db.SaveChanges();
                        }

                        db.QCP002.RemoveRange(lstLineIds);
                        db.SaveChanges();
                    }
                }

                objQCP001 = db.QCP001.Where(i => i.HeaderId == headerId && i.Status == clsImplementationEnum.QCPStatus.Draft.GetStringValue()).FirstOrDefault();
                db.QCP001.Remove(objQCP001);
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Header Successfully Deleted";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
                //throw;
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult Reviseheader(int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QCP001 objQCP001 = db.QCP001.Where(i => i.HeaderId == headerId).FirstOrDefault();

                if (objQCP001 != null && string.Equals(objQCP001.Status, clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                {
                    objQCP001.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                    objQCP001.QCPRev = objQCP001.QCPRev + 1;
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                objResponseMsg.Status = objQCP001.Status;
                objResponseMsg.HeaderId = objQCP001.HeaderId;
                objResponseMsg.Revision = objQCP001.QCPRev;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg);
        }
        #endregion

        #region Lines
        public ActionResult loadLinesDataTable(JQueryDataTableParamModel param, int refHeaderId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string[] arrStatus = { clsImplementationEnum.QCPStatus.Returned.GetStringValue(), clsImplementationEnum.QCPStatus.Draft.GetStringValue() };
                string condition = string.Empty;
                condition = "HeaderId = " + refHeaderId;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( MainCategory like '%" + param.sSearch + "%' )";
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By ISNULL(" + sortColumnName + ", 1) " + sortDirection + " ";
                }
                if (refHeaderId > 0)
                {
                    var headerStatus = db.QCP001.Where(i => i.HeaderId == refHeaderId).Select(i => i.Status).FirstOrDefault();
                    if (string.Equals(headerStatus, clsImplementationEnum.QCPStatus.Returned.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    {

                        //var lstSubCategory = db.QCP003.Where(i => i.HeaderId == refHeaderId && arrStatus.Contains(i.Status)).Select(i => i.LineId).Distinct().ToList();

                        //condition += " AND LineId IN (" + string.Join(",", lstSubCategory) + ")";
                    }
                }
                var lstLines = db.SP_QCP_GETLINES(StartIndex, EndIndex, strSortOrder, condition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = from c in lstLines
                          select new[] {
                                        Convert.ToString(c.LineSrNo),
                                        Convert.ToString(c.LineId),
                                        Convert.ToString(c.HeaderId),
                                        c.MainCategory,
                                        GetDeleteButton(c.LineId,c.HeaderId).ToString()
                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddLinesToHeader(int headerId, string mainCategory)
        {
            QCP001 objQCP001 = new QCP001();
            QCP002 objQCP002 = new QCP002();
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (headerId > 0)
                    objQCP001 = db.QCP001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                if (objQCP001.Status.ToLower() == "approved")
                {
                    objQCP001.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                    objQCP001.QCPRev = objQCP001.QCPRev + 1;
                }

                objQCP002.HeaderId = objQCP001.HeaderId;
                objQCP002.Project = objQCP001.Project;
                objQCP002.BU = objQCP001.BU;
                objQCP002.Location = objQCP001.Location;
                objQCP002.QCPRev = objQCP001.QCPRev;
                var maxOrderNo = db.QCP002.Where(w => w.HeaderId == objQCP001.HeaderId && w.Project == objQCP001.Project).Max(m => m.LineSrNo);
                objQCP002.LineSrNo = maxOrderNo == null?1: ++maxOrderNo;
                objQCP002.MainCategory = mainCategory;
                objQCP002.CreatedBy = objClsLoginInfo.UserName;
                objQCP002.CreatedOn = DateTime.Now;

                db.QCP002.Add(objQCP002);
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = Convert.ToString(objQCP002.HeaderId);
                objResponseMsg.RevNo = Convert.ToString(objQCP001.QCPRev);
                objResponseMsg.HeaderStatus = objQCP001.Status;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public ActionResult getLineByLineId(int lineId)
        {
            QCP002 objQCP002 = new QCP002();
            if (lineId > 0)
            {
                var objQCP2 = (from a in db.QCP002
                               where a.LineId == lineId
                               select new
                               {
                                   a.HeaderId,
                                   a.LineId,
                                   a.MainCategory,
                               }).FirstOrDefault();
                return Json(objQCP2, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(null);

        }

        [HttpPost]
        public ActionResult DeleteLines_Old(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QCP002 objQCP002 = new QCP002();
            QCP003 objQCP003 = new QCP003();
            try
            {
                if (headerId > 0)
                {
                    string headerStatus = db.QCP001.Where(i => i.HeaderId == headerId).Select(i => i.Status).FirstOrDefault();
                    if (string.Equals(headerStatus, clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Category cannot be removed as  header is already approved.";
                    }
                    else
                    {
                        if (isHavingSublines(lineId))
                        {
                            var lstSublineId = db.QCP003.Where(i => i.LineId == lineId && i.HeaderId == headerId).ToList();//&& i.Status == clsImplementationEnum.QCPStatus.Draft.GetStringValue()).ToList();
                            if (lstSublineId.Any())
                            {
                                db.QCP003.RemoveRange(lstSublineId);
                                db.SaveChanges();
                            }
                        }
                        objQCP002 = db.QCP002.Where(i => i.LineId == lineId && i.HeaderId == headerId).FirstOrDefault();
                        db.QCP002.Remove(objQCP002);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Category Deleted Successfully";
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg);
        }
        public bool GetDeleteButton(int lineId, int headerId)
        {
            bool flag = false;
            string deleted = clsImplementationEnum.QCPStatus.Deleted.GetStringValue();
            if (lineId > 0)
            {
                var lstSublines = db.QCP003.Where(i => i.LineId == lineId).ToList();
                var lstDeletedSublines = db.QCP003.Where(i => i.LineId == lineId && i.Status == deleted).ToList();
                if (lstSublines.Count > 0 && lstDeletedSublines.Count > 0)
                {
                    if (lstSublines.Count == lstDeletedSublines.Count)
                    { flag = true; }
                }
            }
            return flag;
        }

        [HttpPost]
        public ActionResult DeleteLines(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QCP002 objQCP002 = new QCP002();
            QCP003 objQCP003 = new QCP003();
            try
            {
                if (headerId > 0)
                {
                    QCP001 objQCP001 = db.QCP001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    string headerStatus = db.QCP001.Where(i => i.HeaderId == headerId).Select(i => i.Status).FirstOrDefault();
                    bool isDeleteRecord = true;
                    if (string.Equals(headerStatus, clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    {
                        objQCP001.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                        objQCP001.QCPRev = objQCP001.QCPRev + 1;
                    }
                    if (isHavingSublines(lineId))
                    {
                        var lstSublineId = db.QCP003.Where(i => i.LineId == lineId && i.HeaderId == headerId).ToList();//&& i.Status == clsImplementationEnum.QCPStatus.Draft.GetStringValue()).ToList();
                        if (lstSublineId.Any())
                        {
                            if (objQCP001.QCPRev > 0)
                            {
                                lstSublineId.ForEach(x =>
                                {
                                    x.Status = clsImplementationEnum.QCPStatus.Deleted.GetStringValue();
                                    x.EditedBy = objClsLoginInfo.UserName;
                                    x.EditedOn = DateTime.Now;
                                });
                                isDeleteRecord = false;
                            }
                            else { db.QCP003.RemoveRange(lstSublineId); }
                            db.SaveChanges();
                        }
                    }
                    if (isDeleteRecord)
                    {
                        objQCP002 = db.QCP002.Where(i => i.LineId == lineId && i.HeaderId == headerId).FirstOrDefault();
                        db.QCP002.Remove(objQCP002);
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = Convert.ToString(objQCP001.QCPRev);
                    objResponseMsg.Status = objQCP001.Status;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult LoadResequenceLineData(int headerId)
        {
            ViewBag.HeaderId = headerId;
            return PartialView("_LoadResequenceLineData");
        }

        [HttpPost]
        public JsonResult LoadResequenceLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = " HeaderId = " + param.Headerid;
                string[] columnName = { "MainCategory", "LineSrNo" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By ISNULL(" + sortColumnName + ", 1) " + sortDirection + " ";
                }

                var lstResult = db.QCP002.SqlQuery("SELECT * FROM QCP002 WHERE " + strWhere + " " + strSortOrder).ToList();
                var count = lstResult.Count;
                lstResult = lstResult.Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.MainCategory),
                                Convert.ToString(uc.LineSrNo),
                                "<input type='text' name='txtNewSequence' onchange='NewSequenceChange(this)' onkeypress = 'return isNumber(event)' class='form-control input-sm input-small input-inline' value='" + (uc.LineSrNo != 0? uc.LineSrNo+"" : "" ) + "' />",
                                Convert.ToString(uc.LineId),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = count,
                    iTotalDisplayRecords = count,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateLineSequence(int lineId, int newsequence)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (newsequence > 0)
                {
                    var objQCP002 = db.QCP002.FirstOrDefault(w => w.LineId == lineId);
                     
                    objQCP002.LineSrNo= newsequence;
                    objQCP002.EditedBy = objClsLoginInfo.UserName;
                    objQCP002.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Main Category Sequence has been updated for Category " + objQCP002.MainCategory;
                }
                else
                    objResponseMsg.Value = "Order No must be greater then 0";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Sub Line

        [HttpPost]
        public ActionResult AddSubLinePartial(int lineId, int headerId)
        {
            QCP003 objQCP003 = new QCP003();
            objQCP003.LineId = lineId;
            objQCP003.HeaderId = headerId;
            ViewBag.Savebutton = GetDeleteButton(lineId, headerId);
            ViewBag.TPIIntervention = Manager.GetSubCatagories("TPI Intervention", db.QCP001.Where(x => x.HeaderId == headerId).Select(x => x.BU).FirstOrDefault(), objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).Distinct().ToList();
            return PartialView("_QCPSubLinesPartial", objQCP003);
        }

        public ActionResult loadSubLinesDataTable(JQueryDataTableParamModel param, int refHeaderId, int refLineId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string condition = string.Empty;
                condition += "sl.LineId = " + refLineId + " AND sl.HeaderId = " + refHeaderId;
                var headerStatus = param.Status;
                if (refHeaderId > 0)
                {
                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        condition += " and ( l.MainCategory like '%" + param.sSearch + "%' or Activity like '%" + param.sSearch + "%' or ReferenceDoc like '%" + param.sSearch + "%' or Characteristics like '%" + param.sSearch + "%' or AcceptanceCriteria like '%" + param.sSearch + "%'  or VerifyingDoc like '%" + param.sSearch + "%'   or HeaderNotes like '%" + param.sSearch + "%' or FooterNotes like '%" + param.sSearch + "%' or RevisionDesc like '%" + param.sSearch + "%' or Status like '%" + param.sSearch + "%' or sl.CreatedBy like '%" + param.sSearch + "%')";
                    }
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By ISNULL(" + sortColumnName + ", 1) " + sortDirection + " ";
                }
                var lstsubLines = db.SP_QCP_GETSUBLINES(StartIndex, EndIndex, strSortOrder, condition).ToList();
                int? totalRecords = lstsubLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = from sl in lstsubLines
                          select new[] {
                                Convert.ToString(sl.SubLineSrNo),
                                Convert.ToString(sl.HeaderId),
                                Convert.ToString(sl.LineId),
                                Convert.ToString(sl.SubLineId),
                                sl.Activity,
                                sl.ReferenceDoc,
                                sl.Characteristics,
                                sl.AcceptanceCriteria,
                                sl.VerifyingDoc,
                                sl.InitiatorRemark,
                                sl.HeaderNotes,
                                sl.FooterNotes,
                                //sl.RevisionDesc,
                                //Convert.ToString(sl.SubLineRev),
                                //sl.Status,
                                sl.TPIInterventionName,
                                sl.TPIInterventionName2,
                                sl.TPIInterventionName3,
                                sl.TPIInterventionName4,
                                sl.TPIInterventionName5,
                                sl.TPIInterventionName6,
                                Convert.ToString(sl.SubLineRev),
                                sl.Status,
                               Helper.HTMLActionString(sl.LineId,"Edit","Edit Record","fa fa-pencil-square-o","EditSubLine(" + sl.SubLineId +")","",string.Equals(sl.Status, clsImplementationEnum.QCPStatus.Deleted.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(headerStatus, clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase)|| string.Equals(headerStatus, clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                             + Helper.HTMLActionString(sl.LineId,"Delete","Delete Record","fa fa-trash-o","DeleteSubLine(" + sl.SubLineId +")","",string.Equals(sl.Status, clsImplementationEnum.QCPStatus.Deleted.GetStringValue(), StringComparison.OrdinalIgnoreCase)|| string.Equals(sl.Status, clsImplementationEnum.QCPStatus.Deleted.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(headerStatus, clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(headerStatus, clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase))

                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = 0,
                    iTotalRecords = 0,
                    aaData = "",

                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveSublineToLine(FormCollection fm)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QCP001 objQCP001 = new QCP001();
            QCP002 objQCP002 = new QCP002();
            QCP003 objQCP003 = new QCP003();

            try
            {
                int subLineId = Convert.ToInt32(fm["SubLineId"]);
                int headerId = Convert.ToInt32(fm["HeaderId"]);
                int lineId = Convert.ToInt32(fm["LineId"]);
                string modified = clsImplementationEnum.QCPStatus.Modified.GetStringValue();
                string added = clsImplementationEnum.QCPStatus.Added.GetStringValue();
                objQCP001 = db.QCP001.Where(i => i.HeaderId == headerId).FirstOrDefault();

                //if (objQCP001 != null)
                //{
                //    if (string.Equals(objQCP001.Status, clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                //    {
                //        objQCP001.QCPRev = objQCP001.QCPRev + 1;
                //        objQCP001.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                //        db.SaveChanges();
                //    }
                //}

                if (subLineId > 0)
                {
                    #region Sub Line Edit
                    objQCP003 = db.QCP003.Where(i => i.SubLineId == subLineId).FirstOrDefault();
                    if (lineId > 0)
                        objQCP002 = db.QCP002.Where(i => i.LineId == lineId).FirstOrDefault();
                    objQCP003.LineId = lineId;

                    objQCP003.HeaderId = headerId;
                    objQCP003.Project = objQCP002.Project;
                    objQCP003.BU = objQCP002.BU;
                    objQCP003.Location = objQCP002.Location;
                    //objQCP003.QCPRev = objQCP002.QCPRev;
                    objQCP003.LineSrNo = objQCP002.LineSrNo;
                    //objQCP003.SubLineSrNo = Convert.ToInt32(fm["SubLineSrNo"]);
                    //objQCP003.SubLineRev = 0;//Convert.ToInt32(fm["SubLineRev"]);
                    objQCP003.Activity = fm["Activity"];
                    objQCP003.ReferenceDoc = fm["ReferenceDoc"];
                    objQCP003.Characteristics = fm["Characteristics"];
                    objQCP003.AcceptanceCriteria = fm["AcceptanceCriteria"];
                    objQCP003.VerifyingDoc = fm["VerifyingDoc"];
                    objQCP003.HeaderNotes = fm["HeaderNotes"];
                    objQCP003.FooterNotes = fm["FooterNotes"];
                    objQCP003.InitiatorRemark = fm["InitiatorRemark"];
                    objQCP003.TPIIntervention = fm["TPIIntervention"];
                    objQCP003.TPIIntervention2 = fm["TPIIntervention2"];
                    objQCP003.TPIIntervention3 = fm["TPIIntervention3"];
                    objQCP003.TPIIntervention4 = fm["TPIIntervention4"];
                    objQCP003.TPIIntervention5 = fm["TPIIntervention5"];
                    objQCP003.TPIIntervention6 = fm["TPIIntervention6"];


                    if (string.Equals(objQCP001.Status, clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    {
                        objQCP003.SubLineRev = objQCP003.SubLineRev + 1;
                        //objQCP003.Status = modified;// clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                        objQCP003.RevisionDesc = fm["RevisionDesc"];

                        objQCP001.QCPRev = objQCP001.QCPRev + 1;
                        objQCP001.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                    }
                    if (string.Equals(objQCP003.Status, clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    {
                        //  objQCP003.Status = modified;// clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                        objQCP003.SubLineRev = objQCP003.SubLineRev + 1;
                        //objQCP001.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();

                    }
                    objQCP003.Status = modified;//clsImplementationEnum.QCPStatus.DRAFT.GetStringValue();
                    objQCP003.EditedBy = objClsLoginInfo.UserName;
                    objQCP003.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderStatus = objQCP001.Status;
                    objResponseMsg.Value = Convert.ToString(objQCP003.LineId);
                    objResponseMsg.RevNo = Convert.ToString(objQCP001.QCPRev);
                    objResponseMsg.Status = objQCP003.Status;
                    #endregion
                }
                else
                {
                    #region Sub Line Add

                    if (lineId > 0)
                        objQCP002 = db.QCP002.Where(i => i.LineId == lineId).FirstOrDefault();
                    objQCP003.LineId = lineId;

                    objQCP003.HeaderId = headerId;
                    objQCP003.Project = objQCP002.Project;
                    objQCP003.BU = objQCP002.BU;
                    objQCP003.Location = objQCP002.Location;
                    objQCP003.QCPRev = objQCP002.QCPRev;
                    objQCP003.LineSrNo = objQCP002.LineSrNo;
                    //objQCP003.SubLineSrNo = Convert.ToInt32(fm["SubLineSrNo"]);
                    objQCP003.SubLineRev = 0;//Convert.ToInt32(fm["SubLineRev"]);
                    objQCP003.Activity = fm["Activity"];
                    objQCP003.ReferenceDoc = fm["ReferenceDoc"];
                    objQCP003.Characteristics = fm["Characteristics"];
                    objQCP003.AcceptanceCriteria = fm["AcceptanceCriteria"];
                    objQCP003.VerifyingDoc = fm["VerifyingDoc"];
                    objQCP003.HeaderNotes = fm["HeaderNotes"];
                    objQCP003.FooterNotes = fm["FooterNotes"];
                    objQCP003.InitiatorRemark = fm["InitiatorRemark"];
                    objQCP003.TPIIntervention = fm["TPIIntervention"];
                    objQCP003.TPIIntervention2 = fm["TPIIntervention2"];
                    objQCP003.TPIIntervention3 = fm["TPIIntervention3"];
                    objQCP003.TPIIntervention4 = fm["TPIIntervention4"];
                    objQCP003.TPIIntervention5 = fm["TPIIntervention5"];
                    objQCP003.TPIIntervention6 = fm["TPIIntervention6"];
                    var maxOrderNo = db.QCP003.Where(w => w.HeaderId == headerId && w.LineId == lineId).Max(m => m.SubLineSrNo);
                    objQCP003.SubLineSrNo = maxOrderNo == null ? 1 : ++maxOrderNo;


                    if (string.Equals(objQCP001.Status, clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    {
                        objQCP001.QCPRev = objQCP001.QCPRev + 1;
                        objQCP001.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                    }
                    objQCP003.Status = added;// clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                    objQCP003.CreatedBy = objClsLoginInfo.UserName;
                    objQCP003.CreatedOn = DateTime.Now;

                    db.QCP003.Add(objQCP003);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderStatus = objQCP001.Status;
                    objResponseMsg.Value = Convert.ToString(objQCP003.LineId);
                    objResponseMsg.RevNo = Convert.ToString(objQCP001.QCPRev);
                    objResponseMsg.Status = objQCP003.Status;


                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult EditSubLine(int subLineId)
        {
            QCP003 objQCP003 = new QCP003();

            NDEModels objNDEModels = new NDEModels();
            var QCP3 = db.QCP003.Where(i => i.SubLineId == subLineId).FirstOrDefault();
            var TPIIntervention = !string.IsNullOrEmpty(QCP3.TPIIntervention) ? objNDEModels.GetCategory("TPI Intervention", QCP3.TPIIntervention, QCP3.BU, QCP3.Location, true).CategoryDescription : string.Empty;
            var TPIIntervention2 = !string.IsNullOrEmpty(QCP3.TPIIntervention2) ? objNDEModels.GetCategory("TPI Intervention", QCP3.TPIIntervention2, QCP3.BU, QCP3.Location, true).CategoryDescription : string.Empty;
            var TPIIntervention3 = !string.IsNullOrEmpty(QCP3.TPIIntervention3) ? objNDEModels.GetCategory("TPI Intervention", QCP3.TPIIntervention3, QCP3.BU, QCP3.Location, true).CategoryDescription : string.Empty;
            var TPIIntervention4 = !string.IsNullOrEmpty(QCP3.TPIIntervention4) ? objNDEModels.GetCategory("TPI Intervention", QCP3.TPIIntervention4, QCP3.BU, QCP3.Location, true).CategoryDescription : string.Empty;
            var TPIIntervention5 = !string.IsNullOrEmpty(QCP3.TPIIntervention5) ? objNDEModels.GetCategory("TPI Intervention", QCP3.TPIIntervention5, QCP3.BU, QCP3.Location, true).CategoryDescription : string.Empty;
            var TPIIntervention6 = !string.IsNullOrEmpty(QCP3.TPIIntervention6) ? objNDEModels.GetCategory("TPI Intervention", QCP3.TPIIntervention6, QCP3.BU, QCP3.Location, true).CategoryDescription : string.Empty;
            //var TPIIntervention = !string.IsNullOrEmpty(QCP3.TPIIntervention) ? objNDEModels.GetCategory(QCP3.TPIIntervention).CategoryDescription : string.Empty;
            if (subLineId > 0)
            {
                var objQCP3 = (new ModelQCPSubLines
                {
                    SubLineId = QCP3.SubLineId,
                    SubLineSrNo = QCP3.SubLineSrNo,
                    Activity = QCP3.Activity,
                    ReferenceDoc = QCP3.ReferenceDoc,
                    Characteristics = QCP3.Characteristics,
                    AcceptanceCriteria = QCP3.AcceptanceCriteria,
                    VerifyingDoc = QCP3.VerifyingDoc,
                    InitiatorRemark = QCP3.InitiatorRemark,
                    SubLineRev = QCP3.SubLineRev,
                    Status = QCP3.Status,
                    HeaderNotes = QCP3.HeaderNotes,
                    FooterNotes = QCP3.FooterNotes,
                    TPIInterventionDesc = TPIIntervention,
                    TPIInterventionDesc2 = TPIIntervention2,
                    TPIInterventionDesc3 = TPIIntervention3,
                    TPIInterventionDesc4 = TPIIntervention4,
                    TPIInterventionDesc5 = TPIIntervention5,
                    TPIInterventionDesc6 = TPIIntervention6,
                    TPIIntervention = QCP3.TPIIntervention,
                    TPIIntervention2 = QCP3.TPIIntervention2,
                    TPIIntervention3 = QCP3.TPIIntervention3,
                    TPIIntervention4 = QCP3.TPIIntervention4,
                    TPIIntervention5 = QCP3.TPIIntervention5,
                    TPIIntervention6 = QCP3.TPIIntervention6,
                    RevisionDesc = QCP3.RevisionDesc
                });
                //objQCP003 = db.QCP003.Where(i => i.SubLineId == subLineId).seleFirstOrDefault();  
                return Json(objQCP3, JsonRequestBehavior.AllowGet);
            }
            else
                return Json(null, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteSubLines(int sublineId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QCP003 objQCP003 = new QCP003();
            try
            {
                if (sublineId > 0)
                {
                    objQCP003 = db.QCP003.Where(i => i.SubLineId == sublineId).FirstOrDefault();
                    string headerStatus = db.QCP001.Where(i => i.HeaderId == objQCP003.HeaderId).Select(i => i.Status).FirstOrDefault();
                    string draft = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                    if (string.Equals(headerStatus, clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    {
                        objQCP003.QCP001.Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue();
                        objQCP003.QCP001.QCPRev = objQCP003.QCP001.QCPRev + 1;
                    }

                    if (objQCP003.QCP001.QCPRev == 0 && objQCP003.QCP001.Status == draft)
                    {
                        db.QCP003.Remove(objQCP003);
                    }
                    else if (objQCP003.QCP001.QCPRev > 0 && objQCP003.QCP001.Status == draft)
                    {
                        objQCP003.Status = clsImplementationEnum.QCPStatus.Deleted.GetStringValue();
                        objQCP003.EditedBy = objClsLoginInfo.UserName;
                        objQCP003.EditedOn = DateTime.Now;
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult LoadResequenceSubLineData(int headerId, int lineId)
        {
            ViewBag.HeaderId = headerId;
            ViewBag.LineId = lineId;
            return PartialView("_LoadResequenceSubLineData");
        }

        [HttpPost]
        public JsonResult LoadResequenceSubLineGridData(JQueryDataTableParamModel param,string LineId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = " HeaderId = " + param.Headerid + " AND LineId = "+ LineId;
                string[] columnName = { "Activity", "ReferenceDoc", "SubLineSrNo"};
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By ISNULL(" + sortColumnName + ", 1) " + sortDirection + " ";
                }

                var lstResult = db.QCP003.SqlQuery("SELECT * FROM QCP003 WHERE " + strWhere + " " + strSortOrder).ToList();
                var count = lstResult.Count;
                lstResult = lstResult.Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.Activity),
                                Convert.ToString(uc.ReferenceDoc),
                                Convert.ToString(uc.SubLineSrNo),
                                "<input type='text' name='txtNewSequence' onchange='NewSequenceChange(this)' onkeypress = 'return isNumber(event)' class='form-control input-sm input-small input-inline' value='" + (uc.SubLineSrNo != 0? uc.SubLineSrNo+"" : "" ) + "' />",
                                Convert.ToString(uc.SubLineId),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = count,
                    iTotalDisplayRecords = count,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateSubLineSequence(int subLineId, int newsequence)
        {
            //clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (newsequence > 0)
                {
                    var objQCP003 = db.QCP003.FirstOrDefault(w =>  w.SubLineId == subLineId);

                    objQCP003.SubLineSrNo = newsequence;
                    objQCP003.EditedBy = objClsLoginInfo.UserName;
                    objQCP003.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Main Category Sequence has been updated for Category " + objQCP003.Activity;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Order No must be greater then 0";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Copy QCPHeader
        public ActionResult CopyHeaderPartial()
        {
            return PartialView("_CopyToDestinationPartial");
        }

        public ActionResult CopyToDestination(int headerId, string destProject)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QCP001 sourceQCP001 = new QCP001();
            QCP001 destQCP001 = new QCP001();
            SourceQCP objSourceQCP = new SourceQCP();

            try
            {
                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1 && a.t_actv == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();

                if (headerId > 0)
                {
                    sourceQCP001 = db.QCP001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    objSourceQCP = GetSourceQCPDetail(sourceQCP001.HeaderId);

                }

                if (!string.IsNullOrEmpty(destProject))
                {
                    destQCP001 = db.QCP001.Where(i => i.Project.Equals(destProject, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(currentLoc, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                    if (destQCP001 != null)
                    {
                        if (string.Equals(destQCP001.Status, clsImplementationEnum.QCPStatus.Draft.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(destQCP001.Status, clsImplementationEnum.QCPStatus.Returned.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        {

                            objResponseMsg = InsertOrUpdateQCP(objSourceQCP, destQCP001, true);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "You cannot copy to " + destProject + " as it is in " + destQCP001.Status + "Staus";
                        }
                    }
                    else
                    {
                        destQCP001 = new QCP001();
                        destQCP001.Project = destProject;
                        destQCP001.BU = db.COM001.Where(i => i.t_cprj == destProject).FirstOrDefault().t_entu;
                        destQCP001.Location = currentLoc;
                        destQCP001.DocumentNo = destProject + "_QCP";
                        objResponseMsg = InsertOrUpdateQCP(objSourceQCP, destQCP001, false);
                    }
                }
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsg InsertOrUpdateQCP(SourceQCP sourceQCP, QCP001 objQCP001, bool isUpdate)//(SourceQCP sourceQCP,QCP001 objQCP001,bool isUpdate)
        {
            clsHelper.ResponseMsg objResponse = new clsHelper.ResponseMsg();
            var sourceHeader = sourceQCP.SourceHeader;

            try
            {
                //if (isUpdate)
                //{

                //    var lstExistingSubCategory = db.QCP003.Where(i => i.HeaderId == objQCP001.HeaderId).ToList();//sourceQCP.SourceSubCategories;
                //    if (lstExistingSubCategory.Any())
                //    {
                //        db.QCP003.RemoveRange(lstExistingSubCategory);
                //        db.SaveChanges();
                //    }
                //    var lstExistingCategory = db.QCP002.Where(i => i.HeaderId == objQCP001.HeaderId).ToList();// sourceQCP.SourceCategories;
                //    if (lstExistingCategory.Any())
                //    {
                //        db.QCP002.RemoveRange(lstExistingCategory);
                //        db.SaveChanges();
                //    }

                //    #region QCP Header Copy To Existing Header

                //    objQCP001.Consultant = sourceHeader.Consultant;
                //    objQCP001.ASME = sourceHeader.ASME;
                //    objQCP001.Specification = sourceHeader.Specification;
                //    objQCP001.Code = sourceHeader.Code;
                //    objQCP001.AbbreviationCode = sourceHeader.AbbreviationCode;
                //    objQCP001.AbbreviationDesc = sourceHeader.AbbreviationDesc;
                //    //objQCP001.Status = "Draft";
                //    objQCP001.TP1 = sourceHeader.TP1;
                //    objQCP001.TP2 = sourceHeader.TP2;
                //    objQCP001.TP3 = sourceHeader.TP3;
                //    objQCP001.TP4 = sourceHeader.TP4;
                //    objQCP001.TP5 = sourceHeader.TP5;
                //    objQCP001.TP6 = sourceHeader.TP6;

                //    objQCP001.EditedBy = objClsLoginInfo.UserName;
                //    objQCP001.EditedOn = DateTime.Now;

                //    db.SaveChanges();

                //    #endregion

                //}
                if (!isUpdate)
                {
                    #region QCP Header To Create New Header


                    objQCP001.QCPRev = 0;
                    objQCP001.Consultant = sourceHeader.Consultant;
                    objQCP001.ASME = sourceHeader.ASME;
                    objQCP001.Specification = sourceHeader.Specification;
                    objQCP001.Code = sourceHeader.Code;
                    objQCP001.AbbreviationCode = sourceHeader.AbbreviationCode;
                    objQCP001.AbbreviationDesc = sourceHeader.AbbreviationDesc;
                    objQCP001.Status = "Draft";
                    objQCP001.TP1 = sourceHeader.TP1;
                    objQCP001.TP2 = sourceHeader.TP2;
                    objQCP001.TP3 = sourceHeader.TP3;
                    objQCP001.TP4 = sourceHeader.TP4;
                    objQCP001.TP5 = sourceHeader.TP5;
                    objQCP001.TP6 = sourceHeader.TP6;
                    objQCP001.CreatedBy = objClsLoginInfo.UserName;
                    objQCP001.CreatedOn = DateTime.Now;

                    db.QCP001.Add(objQCP001);
                    db.SaveChanges();
                    #endregion
                }

                #region Add Category And SubCategory

                var sourceCategory = sourceQCP.SourceCategories.Where(i => i.HeaderId == sourceHeader.HeaderId).ToList();

                foreach (var item in sourceCategory)
                {
                    QCP002 objQCP002 = new QCP002();
                    objQCP002.HeaderId = objQCP001.HeaderId;
                    objQCP002.Project = objQCP001.Project;
                    objQCP002.BU = objQCP001.BU;
                    objQCP002.Location = objQCP001.Location;
                    objQCP002.QCPRev = objQCP001.QCPRev;
                    objQCP002.MainCategory = item.MainCategory;
                    objQCP002.CreatedBy = objClsLoginInfo.UserName;
                    objQCP002.CreatedOn = DateTime.Now;

                    db.QCP002.Add(objQCP002);
                    db.SaveChanges();

                    var sourceSubCategory = sourceQCP.SourceSubCategories.Where(i => i.HeaderId == item.HeaderId && i.LineId == item.LineId).ToList();


                    db.QCP003.AddRange(sourceSubCategory.Select(x => new QCP003
                    {
                        LineId = objQCP002.LineId,
                        HeaderId = objQCP001.HeaderId,
                        Project = objQCP001.Project,
                        BU = objQCP001.BU,
                        Location = objQCP001.Location,
                        QCPRev = objQCP001.QCPRev,
                        Activity = x.Activity,
                        ReferenceDoc = x.ReferenceDoc,
                        Characteristics = x.Characteristics,
                        AcceptanceCriteria = x.AcceptanceCriteria,
                        VerifyingDoc = x.VerifyingDoc,
                        HeaderNotes = x.HeaderNotes,
                        FooterNotes = x.FooterNotes,
                        RevisionDesc = x.RevisionDesc,
                        InitiatorRemark = x.InitiatorRemark,
                        SubLineRev = 0,
                        Status = clsImplementationEnum.QCPStatus.Draft.GetStringValue(),
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        TPIIntervention = x.TPIIntervention,
                        TPIIntervention2 = x.TPIIntervention,
                        TPIIntervention3 = x.TPIIntervention,
                        TPIIntervention4 = x.TPIIntervention,
                        TPIIntervention5 = x.TPIIntervention,
                        TPIIntervention6 = x.TPIIntervention

                    }));

                    db.SaveChanges();

                }

                #endregion

                objResponse.Key = true;
                objResponse.Value = "QCP Copied Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponse.Key = false;
                objResponse.Value = ex.Message;
            }

            return objResponse;
        }
        #endregion

        #region History
        [HttpPost]
        public ActionResult GetHistoryDetails(int? headerId)
        {
            QCP001 objIQCP001 = db.QCP001.Where(x => x.HeaderId == headerId).FirstOrDefault();
            return PartialView("_GetHistoryDetailsPartial", objIQCP001);
        }

        [HttpPost]
        public JsonResult LoadHistoryDetailData(JQueryDataTableParamModel param, int headerId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qcp1.BU", "qcp1.Location");
                whereCondition += "and qcp1.HeaderId = " + headerId;


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (qcp1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or DocumentNo like '%" + param.sSearch + "%' or QCPRev like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_QCP_GETHEADERHistory(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.DocumentNo),
                           Convert.ToString("R" + uc.QCPRev),
                           Convert.ToString(uc.Status),
                           uc.CreatedBy+"-"+Manager.GetUserNameFromPsNo(uc.CreatedBy),
                           Convert.ToDateTime( uc.CreatedOn).ToString("dd/MM/yyyy"),
                           uc.EditedBy+"-"+Manager.GetUserNameFromPsNo(uc.EditedBy),
                           Convert.ToDateTime( uc.EditedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString(uc.id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult HistoryView(int id)
        {
            ViewBag.id = id;
            QCP001_Log objQCP001 = db.QCP001_Log.FirstOrDefault(x => x.Id == id);
            NDEModels objNDEModels = new NDEModels();
            objQCP001.TP1 = !string.IsNullOrEmpty(objQCP001.TP1) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP1, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
            objQCP001.TP2 = !string.IsNullOrEmpty(objQCP001.TP2) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP2, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
            objQCP001.TP3 = !string.IsNullOrEmpty(objQCP001.TP3) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP3, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
            objQCP001.TP4 = !string.IsNullOrEmpty(objQCP001.TP4) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP4, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
            objQCP001.TP5 = !string.IsNullOrEmpty(objQCP001.TP5) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP5, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;
            objQCP001.TP6 = !string.IsNullOrEmpty(objQCP001.TP6) ? objNDEModels.GetCategory("TPI Agency", objQCP001.TP6, objQCP001.BU, objQCP001.Location, true).CategoryDescription : string.Empty;

            ViewBag.docNo = objQCP001.DocumentNo;
            ViewBag.btnApprAccess = false;
            if (objQCP001.Status == clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue())
            {
                ViewBag.btnApprAccess = true;
            }

            PurchaseDetail objQCPCommon = new PurchaseDetail();

            string BU = db.COM001.Where(i => i.t_cprj == objQCP001.Project).FirstOrDefault().t_entu;

            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == BU
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            var location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx == objQCP001.Location
                            select a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objQCP001.Location = location;
            var contract = db.COM005.Where(i => i.t_sprj == objQCP001.Project).Select(i => i.t_cono).FirstOrDefault();

            var purchaseDet = (from cm004 in db.COM004
                               join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                               where cm005.t_sprj == objQCP001.Project
                               select new { cm004.t_refb, cm004.t_codt, cm004.t_ofbp }).FirstOrDefault();
            objQCP001.Project = db.COM001.Where(i => i.t_cprj == objQCP001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();

            ViewBag.QCPBU = BUDescription.BUDesc;
            ViewBag.purchaseOrderNo = purchaseDet.t_refb;
            ViewBag.purchaseOrderDate = purchaseDet.t_codt.ToString("dd/MM/yyyy hh:mm");

            ViewBag.Customer = db.COM006.Where(i => i.t_bpid.Equals(purchaseDet.t_ofbp, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_nama).FirstOrDefault();//db.COM004.Where(i => i.t_cprj == projectCd).Select(i => i.t_ofbp).FirstOrDefault();

            return View(objQCP001);
        }

        [HttpPost]
        public JsonResult LoadLinesHistoryData(JQueryDataTableParamModel param, int refId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                //string SortingFields = param.iSortingCols;
                //readonly= "readonly"

                Func<QCP003_Log, string> orderingFunction = (tsk =>
                                        sortColumnIndex == 2 && isLocationSortable ? Convert.ToString(tsk.Location) : "");
                var sortDirection = Request["sSortDir_0"]; // asc or desc
                string whereCondition = "1=1";
                whereCondition += " and sl.RefId = " + refId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and ( l.MainCategory like '%" + param.sSearch + "%' or Activity like '%" + param.sSearch + "%' or ReferenceDoc like '%" + param.sSearch + "%' or Characteristics like '%" + param.sSearch + "%' or AcceptanceCriteria like '%" + param.sSearch + "%'  or VerifyingDoc like '%" + param.sSearch + "%'   or HeaderNotes like '%" + param.sSearch + "%' or FooterNotes like '%" + param.sSearch + "%' or RevisionDesc like '%" + param.sSearch + "%' or Status like '%" + param.sSearch + "%' or sl.CreatedBy like '%" + param.sSearch + "%')";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstSublines = db.SP_QCP_GETSUBLINESHistory(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int totalRecord = Convert.ToInt32(lstSublines.Select(x => x.TotalCount).FirstOrDefault());
                NDEModels objNDEModels = new NDEModels();
                var data = (from uc in lstSublines
                            select new[]
                            {
                           Convert.ToString(uc.ROW_NO),
                          "<input type='text' name='txtRemark' readonly= 'readonly'  class='form-control input-sm input-small input-inline' value='" + uc.ReturnRemark + "' />",
                           Convert.ToString(uc.MainCategory),
                           Convert.ToString(uc.Activity),
                            Convert.ToString(uc.ReferenceDoc),
                            Convert.ToString(uc.Characteristics),
                            Convert.ToString(uc.AcceptanceCriteria),
                            Convert.ToString(uc.VerifyingDoc),
                            Convert.ToString(uc.InitiatorRemark),
                           Convert.ToString(uc.HeaderNotes),
                           Convert.ToString(uc.FooterNotes),
                           Convert.ToString(uc.TPIInterventionName),
                           Convert.ToString(!string.IsNullOrEmpty(uc.TPIIntervention2) ? objNDEModels.GetCategory("TPI Intervention",uc.TPIIntervention2, uc.BU, uc.Location, true).CategoryDescription : string.Empty),
                           Convert.ToString(!string.IsNullOrEmpty(uc.TPIIntervention3) ? objNDEModels.GetCategory("TPI Intervention",uc.TPIIntervention3, uc.BU, uc.Location, true).CategoryDescription : string.Empty),
                           Convert.ToString(!string.IsNullOrEmpty(uc.TPIIntervention4) ? objNDEModels.GetCategory("TPI Intervention",uc.TPIIntervention4, uc.BU, uc.Location, true).CategoryDescription : string.Empty),
                           Convert.ToString(!string.IsNullOrEmpty(uc.TPIIntervention5) ? objNDEModels.GetCategory("TPI Intervention",uc.TPIIntervention5, uc.BU, uc.Location, true).CategoryDescription : string.Empty),
                           Convert.ToString(!string.IsNullOrEmpty(uc.TPIIntervention6) ? objNDEModels.GetCategory("TPI Intervention", uc.TPIIntervention6, uc.BU, uc.Location, true).CategoryDescription : string.Empty),
                           Convert.ToString("R"+  uc.SubLineRev),
                           Convert.ToString(uc.Status)
                           //Convert.ToString(uc.RevisionDesc),
                           //Convert.ToString(uc.CreatedBy),
                           //Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy HH:MM"),
                           //Convert.ToString("R" + uc.SubLineRev),
                           //Convert.ToString(uc.Status),
                           //uc.TPIInterventionName
                           }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecord,
                    iTotalDisplayRecords = totalRecord,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region QCP Common Function
        public SourceQCP GetSourceQCPDetail(int headerId)
        {
            SourceQCP objSourceQCP = new SourceQCP();

            objSourceQCP.SourceHeader = db.QCP001.Where(i => i.HeaderId == headerId).FirstOrDefault();
            objSourceQCP.SourceCategories = db.QCP002.Where(i => i.HeaderId == headerId).ToList();
            objSourceQCP.SourceSubCategories = db.QCP003.Where(i => i.HeaderId == headerId).ToList();

            return objSourceQCP;
        }
        [HttpPost]
        public ActionResult SendForApproval(int headerId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QCP001 objQCP001 = new QCP001();
            List<string> lstCategoryName = new List<string>();
            List<QCP003> lstSubcategoryWithReturned = new List<QCP003>();
            List<QCP003> lstSubCategories = new List<QCP003>();
            string[] arrStatus = { clsImplementationEnum.QCPStatus.Draft.GetStringValue(), clsImplementationEnum.QCPStatus.Returned.GetStringValue() };

            try
            {
                if (headerId > 0)
                {
                    objQCP001 = db.QCP001.Where(i => i.HeaderId == headerId).FirstOrDefault();

                    if (!string.Equals(objQCP001.Status, clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase))//&& !string.Equals(objQCP001.Status, clsImplementationEnum.QCPStatus.Returned.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                    //if (objQCP001.Status != clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue())
                    {
                        var lstCategory = db.QCP002.Where(i => i.HeaderId == objQCP001.HeaderId).ToList();
                        //if (lstCategory != null && lstCategory.Count > 0)
                        //{
                            foreach (var item in lstCategory)
                            {
                                var lstSubCategory = db.QCP003.Where(i => i.HeaderId == headerId && i.LineId == item.LineId).ToList();
                                if (lstSubCategory != null && lstSubCategory.Count > 0)
                                {
                                    lstSubCategories.AddRange(lstSubCategory);
                                }
                                else
                                {
                                    lstCategoryName.Add(item.MainCategory);
                                }

                            }

                            if (lstCategoryName != null && lstCategoryName.Count > 0)
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Please Add Sub Category For " + string.Join(",", lstCategoryName);
                            }
                            else
                            {
                                objQCP001.Status = clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue();
                                objQCP001.SubmittedBy = objClsLoginInfo.UserName;
                                objQCP001.SubmittedOn = DateTime.Now;
                                db.SaveChanges();
                                var lstsubcat = lstSubCategories.Where(i => i.Status.Equals(clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.Status.Equals(clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList();
                                lstSubCategories = lstSubCategories.Except(lstsubcat).ToList();//lstSubCategories.Where(i =>  i.Status.Equals(clsImplementationEnum.QCPStatus.SentForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase) || !i.Status.Equals(clsImplementationEnum.QCPStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList();

                                lstSubCategories.ForEach(x =>
                                {
                                    //   x.Status = objQCP001.Status;
                                    x.SubmittedBy = objClsLoginInfo.UserName;
                                    x.SubmittedOn = DateTime.Now;
                                });
                                db.SaveChanges();

                                #region Send Notification
                                string role = clsImplementationEnum.UserRoleName.QC2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QI1.GetStringValue();
                                (new clsManager()).SendNotification(role, objQCP001.Project, objQCP001.BU, objQCP001.Location, "QCP: " + objQCP001.DocumentNo + " has been submitted for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/QCP/Approve/Viewsublines?id=" + objQCP001.HeaderId);
                                #endregion

                                objResponseMsg.Key = true;
                                objResponseMsg.Value = "QCP Header sent successfully  for approval";

                            }

                        //}
                        //else
                        //{
                        //    objResponseMsg.Key = false;
                        //    objResponseMsg.Value = "Please Add Main Category For Header";
                        //}

                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "QCP Header has already been sent for approval";
                    }

                }
                else
                { }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg);

        }

        public bool isHavingLines(int headerId)
        {
            bool isHeaderHavingLines = false;

            if (headerId > 0)
            {
                var lstLines = getSubLinesForHeader(headerId);
                if (lstLines != null && lstLines.Count > 0)
                {
                    isHeaderHavingLines = true;
                }
            }
            return isHeaderHavingLines;
        }

        public bool isHavingSublines(int lineId)
        {
            bool flag = false;
            if (lineId > 0)
            {
                var lstSublines = db.QCP003.Where(i => i.LineId == lineId).ToList();
                if (lstSublines != null && lstSublines.Count > 0)
                    flag = true;
            }
            return flag;
        }

        public List<QCP003> getSubLinesForHeader(int headerId)
        {
            List<QCP003> objQCP003 = new List<QCP003>();
            if (headerId > 0)
            {
                objQCP003 = db.QCP003.Where(i => i.HeaderId == headerId).ToList();
            }

            return objQCP003;
        }

        public JsonResult GetTPIIntervention(int headerId)
        {
            try
            {
                var items = (from li in Manager.GetSubCatagories("TPI Intervention", db.QCP001.Where(x => x.HeaderId == headerId).Select(x => x.BU).FirstOrDefault(), objClsLoginInfo.Location)
                             select new
                             {
                                 id = li.Code,
                                 text = li.Code + "-" + li.Description,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetTPIInterventionValue(int headerId, int lineId, int sublineId)
        {
            try
            {
                string TPIIntervention = (db.QCP003.Where(x => x.HeaderId == headerId && x.LineId == lineId && x.SubLineId == sublineId).Select(x => x.TPIIntervention).FirstOrDefault());
                string[] TPIInterventions = null;

                if (!string.IsNullOrWhiteSpace(TPIIntervention))
                {
                    TPIInterventions = TPIIntervention.Split(',');
                }
                return Json(TPIInterventions, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetTPIInterventionValue2(int headerId, int lineId, int sublineId)
        {
            try
            {
                string TPIIntervention = (db.QCP003.Where(x => x.HeaderId == headerId && x.LineId == lineId && x.SubLineId == sublineId).Select(x => x.TPIIntervention2).FirstOrDefault());
                string[] TPIInterventions = null;

                if (!string.IsNullOrWhiteSpace(TPIIntervention))
                {
                    TPIInterventions = TPIIntervention.Split(',');
                }
                return Json(TPIInterventions, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetTPIInterventionValue3(int headerId, int lineId, int sublineId)
        {
            try
            {
                string TPIIntervention = (db.QCP003.Where(x => x.HeaderId == headerId && x.LineId == lineId && x.SubLineId == sublineId).Select(x => x.TPIIntervention3).FirstOrDefault());
                string[] TPIInterventions = null;

                if (!string.IsNullOrWhiteSpace(TPIIntervention))
                {
                    TPIInterventions = TPIIntervention.Split(',');
                }
                return Json(TPIInterventions, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }


        public JsonResult GetTPIInterventionValue4(int headerId, int lineId, int sublineId)
        {
            try
            {
                string TPIIntervention = (db.QCP003.Where(x => x.HeaderId == headerId && x.LineId == lineId && x.SubLineId == sublineId).Select(x => x.TPIIntervention4).FirstOrDefault());
                string[] TPIInterventions = null;

                if (!string.IsNullOrWhiteSpace(TPIIntervention))
                {
                    TPIInterventions = TPIIntervention.Split(',');
                }
                return Json(TPIInterventions, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetTPIInterventionValue5(int headerId, int lineId, int sublineId)
        {
            try
            {
                string TPIIntervention = (db.QCP003.Where(x => x.HeaderId == headerId && x.LineId == lineId && x.SubLineId == sublineId).Select(x => x.TPIIntervention5).FirstOrDefault());
                string[] TPIInterventions = null;

                if (!string.IsNullOrWhiteSpace(TPIIntervention))
                {
                    TPIInterventions = TPIIntervention.Split(',');
                }
                return Json(TPIInterventions, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetTPIInterventionValue6(int headerId, int lineId, int sublineId)
        {
            try
            {
                string TPIIntervention = (db.QCP003.Where(x => x.HeaderId == headerId && x.LineId == lineId && x.SubLineId == sublineId).Select(x => x.TPIIntervention6).FirstOrDefault());
                string[] TPIInterventions = null;

                if (!string.IsNullOrWhiteSpace(TPIIntervention))
                {
                    TPIInterventions = TPIIntervention.Split(',');
                }
                return Json(TPIInterventions, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int HeaderId, int LineId = 0, int sublineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (sublineId > 0)
            {
                QCP003 objQCP003 = db.QCP003.Where(x => x.SubLineId == sublineId).FirstOrDefault();
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objQCP003.ApprovedBy);
                model.ApprovedOn = objQCP003.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objQCP003.SubmittedBy);
                model.SubmittedOn = objQCP003.SubmittedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objQCP003.CreatedBy);
                model.CreatedOn = objQCP003.CreatedOn;
                model.ReturnedBy = Manager.GetUserNameFromPsNo(objQCP003.ReturnedBy);
                model.ReturnedOn = objQCP003.ReturnedOn;
            }
            else if (LineId > 0)
            {
                QCP002 objQCP002 = db.QCP002.Where(x => x.LineId == LineId).FirstOrDefault();
                model.CreatedBy = Manager.GetUserNameFromPsNo(objQCP002.CreatedBy);
                model.CreatedOn = objQCP002.CreatedOn;
                model.EditedBy = Manager.GetUserNameFromPsNo(objQCP002.EditedBy);
                model.EditedOn = objQCP002.EditedOn;
            }
            else
            {
                QCP001 objQCP001 = db.QCP001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.ApprovedBy = Manager.GetUserNameFromPsNo(objQCP001.ApprovedBy);
                model.ApprovedOn = objQCP001.ApprovedOn;
                model.SubmittedBy = Manager.GetUserNameFromPsNo(objQCP001.SubmittedBy);
                model.SubmittedOn = objQCP001.SubmittedOn;
                model.CreatedBy = Manager.GetUserNameFromPsNo(objQCP001.CreatedBy);
                model.CreatedOn = objQCP001.CreatedOn;
            }
            model.Title = "QCP";
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_QCP_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      DocumentNo = li.DocumentNo,
                                      Status = li.Status,
                                      QCPRev = li.QCPRev,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_QCP_GETLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      MainCategory = li.MainCategory,
                                      QCPRev = li.QCPRev,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.SUB_LINES.GetStringValue())
                {
                    var lst = db.SP_QCP_GETSUBLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      LineSrNo = li.LineSrNo,
                                      QCPRev = li.QCPRev,
                                      SubLineSrNo = li.SubLineSrNo,
                                      Activity = li.Activity,
                                      ReferenceDoc = li.ReferenceDoc,
                                      Characteristics = li.Characteristics,
                                      AcceptanceCriteria = li.AcceptanceCriteria,
                                      VerifyingDoc = li.VerifyingDoc,
                                      HeaderNotes = li.HeaderNotes,
                                      FooterNotes = li.FooterNotes,
                                      RevisionDesc = li.RevisionDesc,
                                      InitiatorRemark = li.InitiatorRemark,
                                      ReturnRemark = li.ReturnRemark,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ReturnedBy = li.ReturnedBy,
                                      ReturnedOn = li.ReturnedOn,
                                      MainCategory = li.MainCategory,
                                      SubLineRev = li.SubLineRev,
                                      TPIIntervention = li.TPIIntervention,
                                      TPIInterventionName = li.TPIInterventionName,
                                      TPIIntervention2 = li.TPIIntervention2,
                                      TPIIntervention3 = li.TPIIntervention3,
                                      TPIIntervention4 = li.TPIIntervention4,
                                      TPIIntervention5 = li.TPIIntervention5,
                                      TPIIntervention6 = li.TPIIntervention6,
                                      TPIInterventionName2 = li.TPIInterventionName2,
                                      TPIInterventionName3 = li.TPIInterventionName3,
                                      TPIInterventionName4 = li.TPIInterventionName4,
                                      TPIInterventionName5 = li.TPIInterventionName5,
                                      TPIInterventionName6 = li.TPIInterventionName6,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_QCP_GETHEADERHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      DocumentNo = Convert.ToString(uc.DocumentNo),
                                      Rev = Convert.ToString("R" + uc.QCPRev),
                                      Status = Convert.ToString(uc.Status),
                                      CreatedBy = uc.CreatedBy + "-" + Manager.GetUserNameFromPsNo(uc.CreatedBy),
                                      CreatedOn = Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                                      EditedBy = uc.EditedBy + "-" + Manager.GetUserNameFromPsNo(uc.EditedBy),
                                      EditedOn = Convert.ToDateTime(uc.EditedOn).ToString("dd/MM/yyyy"),

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {

                    var lst = db.SP_QCP_GETSUBLINESHistory(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      ROW_NO = uc.ROW_NO,
                                      ReturnRemark = uc.ReturnRemark,
                                      MainCategory = uc.MainCategory,
                                      Activity = uc.Activity,
                                      ReferenceDoc = uc.ReferenceDoc,
                                      Characteristics = uc.Characteristics,
                                      AcceptanceCriteria = uc.AcceptanceCriteria,
                                      VerifyingDoc = uc.VerifyingDoc,
                                      InitiatorRemark = uc.InitiatorRemark,
                                      HeaderNotes = uc.HeaderNotes,
                                      FooterNotes = uc.FooterNotes,
                                      TPIIntervention = uc.TPIIntervention,
                                      TPIIntervention2 = uc.TPIIntervention2,
                                      TPIIntervention3 = uc.TPIIntervention3,
                                      TPIIntervention4 = uc.TPIIntervention4,
                                      TPIIntervention5 = uc.TPIIntervention5,
                                      TPIIntervention6 = uc.TPIIntervention6,
                                      SubLineRev = "R" + uc.SubLineRev,
                                      Status = uc.Status
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }


        #endregion
    }
}