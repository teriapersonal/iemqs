﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IEMQSImplementation;

namespace IEMQS.Areas.QCP.Models
{
    public class SourceQCP
    {
        public QCP001 SourceHeader { get; set; }
        public List<QCP002> SourceCategories { get; set; }
        public List<QCP003> SourceSubCategories { get; set; }

    }

    public class ModelQCPSubLines
    {
        public int SubLineId { get; set; }
        public int LineId { get; set; }
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Location { get; set; }
        public int QCPRev { get; set; }
        public Nullable<int> LineSrNo { get; set; }
        public Nullable<int> SubLineSrNo { get; set; }
        public int SubLineRev { get; set; }
        public string Activity { get; set; }
        public string ReferenceDoc { get; set; }
        public string Characteristics { get; set; }
        public string AcceptanceCriteria { get; set; }
        public string VerifyingDoc { get; set; }
        public string HeaderNotes { get; set; }
        public string FooterNotes { get; set; }
        public string RevisionDesc { get; set; }
        public string InitiatorRemark { get; set; }
        public string ReturnRemark { get; set; }
        public string Status { get; set; }
        public string TPIIntervention { get; set; }
        public string TPIInterventionDesc { get; set; }
        public string TPIIntervention2 { get; set; }
        public string TPIInterventionDesc2 { get; set; }
        public string TPIIntervention3 { get; set; }
        public string TPIInterventionDesc3 { get; set; }
        public string TPIIntervention4 { get; set; }
        public string TPIInterventionDesc4 { get; set; }
        public string TPIIntervention5 { get; set; }
        public string TPIInterventionDesc5 { get; set; }
        public string TPIIntervention6 { get; set; }
        public string TPIInterventionDesc6 { get; set; }

    }
}