﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IEMQSImplementation;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Models
{
    public class CustModal
    {
        public int ID { get; set; }
        public int Role { get; set; }
        public int Process { get; set; }
        public bool IsMobileMenu { get; set; }
    }

    public class ModelContractorMaster
    {
        public List<CategoryData> Location { get; set; }
        public List<CategoryData> ContractorCode { get; set; }
        public List<CategoryData> ShopCode { get; set; }


    }

    public class SODModal
    {
        public int Id { get; set; }
        public int Role { get; set; }
        public int ApplicableRole { get; set; }
    }

    public class MenuConfiguration
    {
        public ATH002 ATH002 { get; set; }
        [AllowHtml]
        [UIHint("tinymce_jquery_full")]
        [Required(ErrorMessage = "Required")]
        public string ContentBody { get; set; }
        public string TemplateName { get; set; }
    }
    public enum AuthorizationPopupType
    {
       NewRecord,
       CopyRecord
    }

}