﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class AccessControlController : clsBase
    {
        // GET: Authorization/AccessControl
        public ActionResult Index()
        {
            if (objClsLoginInfo == null)
            {
                RedirectToAction("Index", "Authenticate");
            }
            //var objBU = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).ToList().Select(i => new { BU = i.BU }).Distinct();
            //var objLocation = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).ToList().Select(i => new { Location = i.Location }).Distinct();
            int BUTypeId = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());
            int LocationTypeId = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
            var objBU = (from a1 in db.ATH001
                         join c2 in db.COM002 on a1.BU equals c2.t_dimx
                         where a1.Employee == objClsLoginInfo.UserName && c2.t_dtyp == BUTypeId
                         select new { BU = a1.BU, Name = c2.t_desc }
                   ).ToList().Select(i => new { Code = i.BU, Name = i.Name }).Distinct();

            var objLocation = (from a1 in db.ATH001
                               join c2 in db.COM002 on a1.Location equals c2.t_dimx
                               where a1.Employee == objClsLoginInfo.UserName && c2.t_dtyp == LocationTypeId
                               select new { Location = a1.Location, Name = c2.t_desc }
                  ).ToList().Select(i => new { Code = i.Location, Name = i.Name }).Distinct();

            ViewBag.BU = objBU;
            ViewBag.Location = objLocation;
            return View();
        }

        [HttpPost]
        public ActionResult GetEmployeeByBU(string BUId, string term)
        {
            if (!string.IsNullOrWhiteSpace(BUId))
            {
                var lstRoles = objClsLoginInfo.GetUserRoleList();
                var objRoleGroup = db.ATH004.Where(i => lstRoles.Contains(i.Role)).ToList().Select(i => i.RoleGroup).Distinct();

                List<string> lstBU = BUId.Split(',').ToList();
                if (term == string.Empty)
                {
                    var objEmployee = (from a1 in db.ATH001
                                       join a4 in db.ATH004 on a1.Role equals a4.Id
                                       join c2 in db.COM003 on a1.Employee equals c2.t_psno
                                       where lstBU.Contains(a1.BU) && objRoleGroup.Contains(a4.RoleGroup) && (a1.Employee.Contains(term) || c2.t_name.Contains(term)) && c2.t_actv == 1
                                       select new { Code = a1.Employee, Name = c2.t_psno + "-" + c2.t_name }
                      ).ToList().Select(i => new { Code = i.Code, Name = i.Name }).Distinct().Take(10);
                    return Json(objEmployee, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var objEmployee = (from a1 in db.ATH001
                                       join a4 in db.ATH004 on a1.Role equals a4.Id
                                       join c2 in db.COM003 on a1.Employee equals c2.t_psno
                                       where lstBU.Contains(a1.BU) && objRoleGroup.Contains(a4.RoleGroup) && (a1.Employee.Contains(term) || c2.t_name.Contains(term)) && c2.t_actv == 1
                                       select new { Code = a1.Employee, Name = c2.t_psno + "-" + c2.t_name }
                   ).ToList().Select(i => new { Code = i.Code, Name = i.Name }).Distinct();
                    return Json(objEmployee, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetContractByBU(string BUId, string term)
        {
            if (!string.IsNullOrWhiteSpace(BUId))
            {
                List<string> lstBU = BUId.Split(',').ToList();
                
                //While User not enter any search text for contract
                if (term == string.Empty)
                {
                    //Check if there is any records of all contract for BU and Login person in ATH001 and if any exist then display all contract from COM004 for selected BU
                    if (db.ATH001.Where(i => lstBU.Contains(i.BU) && i.Contract.Equals("ALL", StringComparison.OrdinalIgnoreCase) && i.Employee == objClsLoginInfo.UserName).Any())
                    {
                        var objContract = (from c4 in db.COM004
                                           join c8 in db.COM008 on c4.t_cono equals c8.t_cono
                                           where lstBU.Contains(c8.t_csbu)
                                           select new { Code = c4.t_cono, Name = c4.t_desc }
                          ).ToList().Select(i => new { Code = i.Code, Name = i.Code + "-" + i.Name }).Distinct().Take(10).ToList();
                        objContract.Insert(0, new { Code = "ALL", Name = "ALL - ALL" });
                        return Json(objContract, JsonRequestBehavior.AllowGet);
                    }
                    //If there is not any all records entry then display all contract list from ATH001 which is assign to login person
                    else
                    {
                        var objContract = (from a1 in db.ATH001
                                           join c2 in db.COM004 on a1.Contract equals c2.t_cono into c2a1
                                           from c2 in c2a1.DefaultIfEmpty()
                                           where lstBU.Contains(a1.BU) && a1.Employee == objClsLoginInfo.UserName && (a1.Contract.Contains(term) || c2.t_desc.Contains(term))
                                           select new { Code = a1.Contract, Name = (c2 == null ? (a1.Contract == "ALL" ? "ALL" : "") : c2.t_desc) }
                           ).ToList().Select(i => new { Code = i.Code, Name = i.Code + "-" + i.Name }).Distinct().Take(10);
                        return Json(objContract, JsonRequestBehavior.AllowGet);
                    }
                }
                //When user Enter any specific character for search Contract
                else
                {
                    //Check if there is any records of all contract for BU and Login person in ATH001 and if any exist then display all contract from COM004 for selected BU and contract contain search text
                    if (db.ATH001.Where(i => lstBU.Contains(i.BU) && i.Contract.Equals("ALL",StringComparison.OrdinalIgnoreCase) && i.Employee == objClsLoginInfo.UserName).Any())
                    {
                        var objContract = (from c4 in db.COM004
                                           join c8 in db.COM008 on c4.t_cono equals c8.t_cono
                                           where lstBU.Contains(c8.t_csbu) && (c4.t_desc.Contains(term) || c4.t_cono.Contains(term))
                                           select new { Code = c4.t_cono, Name = c4.t_desc }
                          ).ToList().Select(i => new { Code = i.Code, Name = i.Code + "-" + i.Name }).Distinct().ToList();
                        objContract.Insert(0, new { Code = "ALL", Name = "ALL - ALL" });
                        return Json(objContract, JsonRequestBehavior.AllowGet);
                    }
                    //If there is not any all records entry then display all contract list from ATH001 which is assign to login person and contract contain search text
                    else
                    {
                        var objContract = (from a1 in db.ATH001
                                           join c2 in db.COM004 on a1.Contract equals c2.t_cono into c2a1
                                           from c2 in c2a1.DefaultIfEmpty()
                                           where lstBU.Contains(a1.BU) && a1.Employee == objClsLoginInfo.UserName && (a1.Contract.Contains(term) || c2.t_desc.Contains(term))
                                           select new { Code = a1.Contract, Name = (c2 == null ? (a1.Contract == "ALL" ? "ALL" : "") : c2.t_desc) }
                       ).ToList().Select(i => new { Code = i.Code, Name = i.Code + "-" + i.Name }).Distinct();
                        return Json(objContract, JsonRequestBehavior.AllowGet);
                    }
                }
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetProjectByBU(string BUId, string ContractId, string term)
        {
            if (!string.IsNullOrWhiteSpace(ContractId))
            {
                List<string> lstContract = ContractId.Split(',').ToList();
                List<string> lstBU = BUId.Split(',').ToList();

                //If Contract Is  All Selected Then get All Contract list of Selected BU
                if (ContractId.ToLower() == "all")
                {
                    lstContract = (from c4 in db.COM004
                                   join c8 in db.COM008 on c4.t_cono equals c8.t_cono
                                   where lstBU.Contains(c8.t_csbu)
                                   select new { Code = c4.t_cono, Name = c4.t_desc }
                             ).ToList().Select(i => i.Code).Distinct().ToList();

                }

                //While User not enter any character for search
                if (term == string.Empty)
                {
                    //If User Select All from Contract list then display all projects which is in selected BU Contracts Projects
                    if (ContractId.ToLower() == "all")
                    {
                        var objProjects = (from c1 in db.COM001
                                           join c5 in db.COM005 on c1.t_cprj equals c5.t_sprj
                                           where lstContract.Contains(c5.t_cono)
                                           select new { Code = c1.t_cprj, Name = c1.t_dsca }
                          ).ToList().Select(i => new { Code = i.Code, Name = i.Code + "-" + i.Name }).Distinct().Take(10).ToList();
                        objProjects.Insert(0, new { Code = "ALL", Name = "ALL - ALL" });
                        return Json(objProjects, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //If User Select Specific Conrtact and in ATH001 there is any records for all project for specific contract then display specific contracts all projects
                        if (db.ATH001.Where(i => lstBU.Contains(i.BU) && (i.Contract.Equals(ContractId, StringComparison.OrdinalIgnoreCase) || i.Contract.Equals("ALL", StringComparison.OrdinalIgnoreCase)) && i.Employee == objClsLoginInfo.UserName && i.Project.Equals("ALL", StringComparison.OrdinalIgnoreCase)).Any())
                        {
                            var objProjects = (from c1 in db.COM001
                                               join c5 in db.COM005 on c1.t_cprj equals c5.t_sprj
                                               where lstContract.Contains(c5.t_cono)
                                               select new { Code = c1.t_cprj, Name = c1.t_dsca }
                                            ).ToList().Select(i => new { Code = i.Code, Name = i.Code + "-" + i.Name }).Distinct().Take(10).ToList();
                            objProjects.Insert(0, new { Code = "ALL", Name = "ALL - ALL" });
                            return Json(objProjects, JsonRequestBehavior.AllowGet);
                        }
                        //otherwise Display Only That projects which is allocated in ATH001 for selected Contract
                        else
                        {
                            var objProjects = (from a1 in db.ATH001
                                               join c1 in db.COM001 on a1.Project equals c1.t_cprj into c2a1
                                               from c1 in c2a1.DefaultIfEmpty()
                                               where lstBU.Contains(c1.t_entu) && lstContract.Contains(a1.Contract) && a1.Employee == objClsLoginInfo.UserName && (a1.Project.Contains(term) || c1.t_dsca.Contains(term))
                                               select new { Code = a1.Project, Name = (c1 == null ? (a1.Project == "ALL" ? "ALL" : "") : c1.t_dsca) }
                                          ).ToList().Select(i => new { Code = i.Code, Name = i.Code + "-" + i.Name }).Distinct().Take(10).ToList();
                            objProjects.Insert(0, new { Code = "ALL", Name = "ALL - ALL" });
                            return Json(objProjects, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                //When user Enter any specific character for search project
                else
                {
                    //If User Select All from Contract list then display all projects which is in selected BU Contracts Projects and project contains search text
                    if (ContractId.ToLower() == "all")
                    {
                        var objProjects = (from c1 in db.COM001
                                           join c5 in db.COM005 on c1.t_cprj equals c5.t_sprj
                                           where lstContract.Contains(c5.t_cono) && (c1.t_cprj.Contains(term) || c1.t_dsca.Contains(term))
                                           select new { Code = c1.t_cprj, Name = c1.t_dsca }
                          ).ToList().Select(i => new { Code = i.Code, Name = i.Code + "-" + i.Name }).Distinct().Take(10).ToList();
                        objProjects.Insert(0, new { Code = "ALL", Name = "ALL - ALL" });
                        return Json(objProjects, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        //If User Select Specific Conrtact and in ATH001 there is any records for all project for specific contract then display specific contracts all projects and project contains search text
                        if (db.ATH001.Where(i => lstBU.Contains(i.BU) && (i.Contract.Equals(ContractId, StringComparison.OrdinalIgnoreCase) || i.Contract.Equals("ALL", StringComparison.OrdinalIgnoreCase)) && i.Employee == objClsLoginInfo.UserName && i.Project.Equals("ALL", StringComparison.OrdinalIgnoreCase)).Any())
                        {
                            var objProjects = (from c1 in db.COM001
                                               join c5 in db.COM005 on c1.t_cprj equals c5.t_sprj
                                               where lstContract.Contains(c5.t_cono) && (c1.t_cprj.Contains(term) || c1.t_dsca.Contains(term))
                                               select new { Code = c1.t_cprj, Name = c1.t_dsca }
                                            ).ToList().Select(i => new { Code = i.Code, Name = i.Code + "-" + i.Name }).Distinct().Take(10).ToList();
                            objProjects.Insert(0, new { Code = "ALL", Name = "ALL - ALL" });
                            return Json(objProjects, JsonRequestBehavior.AllowGet);
                        }
                        //otherwise Display Only That projects which is allocated in ATH001 for selected Contract and project contains search text
                        else
                        {
                            var objProjects = (from a1 in db.ATH001
                                               join c1 in db.COM001 on a1.Project equals c1.t_cprj into c2a1
                                               from c1 in c2a1.DefaultIfEmpty()
                                               where lstContract.Contains(a1.Contract) && a1.Employee == objClsLoginInfo.UserName && (a1.Project.Contains(term) || c1.t_dsca.Contains(term))
                                               select new { Code = a1.Project, Name = (c1 == null ? (a1.Project == "ALL" ? "ALL" : "") : c1.t_dsca) }
                                          ).ToList().Select(i => new { Code = i.Code, Name = i.Code + "-" + i.Name }).Distinct().ToList();
                            objProjects.Insert(0, new { Code = "ALL", Name = "ALL - ALL" });
                            return Json(objProjects, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetRoles(string psno)
        {
            if (!string.IsNullOrWhiteSpace(psno))
            {
                var lstRoles = objClsLoginInfo.GetUserRoleList();
                var objRoleGroup = db.ATH004.Where(i => lstRoles.Contains(i.Role)).ToList().Select(i => i.RoleGroup).Distinct();

                var objEmployeeRoles = (from a1 in db.ATH001
                                        join a4 in db.ATH004 on a1.Role equals a4.Id
                                        where a1.Employee == psno && objRoleGroup.Contains(a4.RoleGroup) && a1.Role == a4.Id
                                        select new { Code = a1.Role, Name = a4.Role + " - " + a4.Description }
                                   ).ToList().Select(i => new { Code = i.Code, Name = i.Name }).Distinct();

                //List<string> lstBU = BUId.Split(',').ToList();
                //var objEmployee = (from a1 in db.ATH001
                //                   join c2 in db.COM004 on a1.Contract equals c2.t_cono into c2a1
                //                   from c2 in c2a1.DefaultIfEmpty()
                //                   where lstBU.Contains(a1.BU)
                //                   select new { Code = a1.Contract, Name = (c2 == null ? (a1.Contract == "ALL" ? "ALL" : "") : c2.t_desc) }
                //   ).ToList().Select(i => new { Code = i.Code, Name = i.Name }).Distinct();
                return Json(objEmployeeRoles, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveAuthMatrix(ATH001 ath001)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<ATH001> listATH001 = null;
            try
            {
                var Contract = ath001.Contract != null && ath001.Contract != string.Empty ? ath001.Contract : "None";
                var Project = ath001.Project != null && ath001.Project != string.Empty ? ath001.Project : "None";
                var flag = false;
                List<string> lstBU = ath001.BU.Split(',').ToList();
                var LocIds = db.ATH001.Where(i => i.Employee == ath001.Employee && lstBU.Contains(i.BU)).ToList().Select(i => i.Location).Distinct();
                if (lstBU.Count > 0)
                {
                    listATH001 = new List<ATH001>();
                    foreach (var bu in lstBU)
                    {
                        foreach (var loc in LocIds)
                        {
                            var data = db.ATH001.Where(x => x.BU == bu && x.Location == loc && x.Contract == Contract && x.Employee == ath001.Employee && x.Project == Project && x.Role == ath001.Role).FirstOrDefault();
                            if (data == null)
                            {
                                flag = true;
                                ATH001 objATH001 = new ATH001();
                                objATH001.BU = bu;
                                objATH001.Location = loc;
                                objATH001.Contract = Contract;
                                objATH001.Project = Project;
                                objATH001.Role = ath001.Role;
                                objATH001.Employee = ath001.Employee;
                                objATH001.CreatedBy = objClsLoginInfo.UserName;
                                objATH001.CreatedOn = DateTime.Now;
                                listATH001.Add(objATH001);
                            }
                        }
                    }
                    if (listATH001.Count > 0)
                    {
                        db.ATH001.AddRange(listATH001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                    }
                    if (!flag)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAuthMatrixPartial()
        {
            return PartialView("_AccessControlDataGridPartial");
        }

        [HttpPost]
        public JsonResult LoadAuthMatrixData(JQueryDataTableParamModel param)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (BU like '%" + param.sSearch
                        + "%' or Location like '%" + param.sSearch
                        + "%' or Contract like '%" + param.sSearch
                        + "%' or Project like '%" + param.sSearch
                        + "%' or Role like '%" + param.sSearch
                        + "%' or Employee like '%" + param.sSearch
                        + "%' or CreatedByName like '%" + param.sSearch
                        + "%')";

                }
                else
                {
                    strWhere = "1=1";
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstATH001 = db.SP_ATH_GET_ACCESS_DETAILS
                                (
                                objClsLoginInfo.UserName, StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstATH001
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.Contract),
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.Role),
                           Convert.ToString(uc.Employee),
                           Convert.ToString(uc.CreatedByName),
                           Convert.ToString(uc.CreatedBy)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstATH001.Count > 0 && lstATH001.FirstOrDefault().TotalCount > 0 ? lstATH001.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstATH001.Count > 0 && lstATH001.FirstOrDefault().TotalCount > 0 ? lstATH001.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteAuthMatrix(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ATH001 objATH001 = db.ATH001.Where(x => x.Id == Id).FirstOrDefault();
                if (objATH001 == null)
                {
                    objResponseMsg.Value = "Record not found for delete!";
                }
                else if (objATH001 != null && objATH001.CreatedBy == objClsLoginInfo.UserName)
                {
                    Manager.MaintainATH001Log(Id);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Value = "You are not authorize to delete this record!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

    }
}