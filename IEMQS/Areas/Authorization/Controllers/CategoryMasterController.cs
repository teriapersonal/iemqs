﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class CategoryMasterController : clsBase
    {
        // GET: Authorization/CategoryMaster
        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult Index()
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("GLB001");
            return View();
        }
        [HttpPost]
        public ActionResult getData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                int CategoryId = Convert.ToInt32(param.CategoryId);
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere+= " and (Category like '%" + param.sSearch 
                        + "%' or Description like '%" + param.sSearch
                        + "%' or Module like '%" + param.sSearch
                        + "%' or a.CreatedBy like '%" + param.sSearch
                        + "%' or e.t_name like '%" + param.sSearch
                        + "%' or a.CreatedOn like '%" + param.sSearch
                        + "%')";

                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstResult = db.SP_ATH_GET_Category_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                         
                           Convert.ToString(uc.Category),
                           Convert.ToString(uc.Description),
                           Convert.ToString(uc.Module),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.CreatedOn),
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = lstResult.Count>0 ? lstResult.FirstOrDefault().TotalCount : 0,
                    iTotalDisplayRecords = lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult GetDataHtml(int Id)
        {
            GLB001 objGLB001 = new GLB001();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            var Category = (from a in db.GLB001
                            where a.Id == Id
                            select new { a.Category }).FirstOrDefault();


            var Description = (from a in db.GLB001
                               where a.Id == Id
                               select new { a.Description }).FirstOrDefault();

            var Module = (from a in db.GLB001
                               where a.Id == Id
                               select new { a.Module }).FirstOrDefault();

            objResponseMsg.Value = Category + "|" + Description + "|"+ Module + "|" + Id;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveCategory(GLB001 glb001, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var category = fc["txtCode"].ToString().Trim();
                var isvalid = db.GLB001.Any(x => x.Category == category && x.IsActive == true);
                if (isvalid == false)
                {
                    if (glb001.Id > 0)
                    {
                        GLB001 objGLB001 = db.GLB001.Where(x => x.Id == glb001.Id).FirstOrDefault();
                        objGLB001.Category = fc["txtCode"].ToString().Trim();
                        objGLB001.Description = fc["txtDesc"].ToString().Trim();
                        objGLB001.Module = fc["txtModule"].ToString().Trim();
                        objGLB001.EditedBy = objClsLoginInfo.UserName;
                        objGLB001.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    }
                    else
                    {

                        GLB001 objGLB001 = new GLB001();
                        objGLB001.Category = fc["txtCode"].ToString().Trim();
                        objGLB001.Description = fc["txtDesc"].ToString().Trim();
                        objGLB001.Module = fc["txtModule"].ToString().Trim();
                        objGLB001.IsActive = true;
                        objGLB001.CreatedBy = objClsLoginInfo.UserName;
                        objGLB001.CreatedOn = DateTime.Now;
                        db.GLB001.Add(objGLB001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteGDMatrix(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                GLB001 objGLB001 = db.GLB001.Where(x => x.Id == Id).FirstOrDefault();
                objGLB001.IsActive = false;
                objGLB001.EditedBy = objClsLoginInfo.UserName;
                objGLB001.EditedOn = DateTime.Now;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateGDMatrix(GLB001 glb001, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var id = Convert.ToInt32(fc["hfId"].ToString());
                if (Convert.ToInt32(fc["hfId"].ToString()) > 0)
                {
                    GLB001 objGLB001 = db.GLB001.Where(x => x.Id == id).FirstOrDefault();
                    objGLB001.Category = fc["txtCode"].ToString().Trim();
                    objGLB001.Description = fc["txtDesc"].ToString().Trim();
                    objGLB001.Module = fc["txtModule"].ToString().Trim();
                    objGLB001.EditedBy = objClsLoginInfo.UserName;
                    objGLB001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_ATH_GET_Category_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Category = li.Category,
                                      Description = li.Description,
                                      Module=li.Module,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}