﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class ParentMaterialController : clsBase
    {
        // GET: Authorization/ParentMaterial
        [SessionExpireFilter]
        public ActionResult Index()
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("WPS005");
            return View();
        }

        public ActionResult loadHeaderDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[] { "PNO1", "DESCRIPTION", "MATERIAL" }, param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_ATH_GET_PARENTMATERIAL(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.Id),
                        h.PNO1,//db.COM001.Where(i=>i.t_cprj==h.Project).Select(i=>i.t_dsca).FirstOrDefault(),//h.Project,
                        h.DESCRIPTION,
                        h.MATERIAL,
                        "<center><a class='iconspace' onClick='ParentMateialMasterPartial("+h.Id+")'><i style='margin-left:5px;' class='fa fa-eye' ></i></a></center>",
                        //GetHTMLString(h.HeaderId)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GetParentMaterialPartial(int? id)
        {
            WPS005 objWPS005 = new WPS005();
            if (id > 0)
            {
                objWPS005 = db.WPS005.Where(i => i.Id == id).FirstOrDefault();
            }
            return PartialView("_GetParentMaterialPartial", objWPS005);
        }

        public ActionResult SaveParentMaterial(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WPS005 objWPS005 = new WPS005();
            try
            {
                if (fc != null)
                {
                    int id = Convert.ToInt32(fc["Id"]);
                    if (!isParentMaterialExist(fc["PNO1"], id))
                    {
                        if (id > 0)
                        {
                            objWPS005 = db.WPS005.Where(i => i.Id == id).FirstOrDefault();
                            objWPS005.DESCRIPTION = fc["DESCRIPTION"];
                            objWPS005.MATERIAL = fc["MATERIAL"];
                            objWPS005.EditedBy = objClsLoginInfo.UserName;
                            objWPS005.EditedOn = DateTime.Now;

                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                        }
                        else
                        {
                            objWPS005.PNO1 = fc["PNO1"];
                            objWPS005.DESCRIPTION = fc["DESCRIPTION"];
                            objWPS005.MATERIAL = fc["MATERIAL"];
                            objWPS005.CreatedBy = objClsLoginInfo.UserName;
                            objWPS005.CreatedOn = DateTime.Now;

                            db.WPS005.Add(objWPS005);
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                        } 
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "PNo. Already Exist";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool isParentMaterialExist(string pNo,int id=0)
        {
            bool isParentMaterialExist = false;
            if (db.WPS005.Where(i => i.PNO1.Equals(pNo, StringComparison.OrdinalIgnoreCase) && i.Id != id).Any())
                isParentMaterialExist = true;
            return isParentMaterialExist;
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_ATH_GET_PARENTMATERIAL(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {

                                      PNNo=li.PNO1,
                                      Description=li.DESCRIPTION,
                                      Material=li.MATERIAL,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }



                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}