﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Areas.Authorization.Models;
using IEMQS.Models;
using System.Globalization;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class ContractorMasterController : clsBase
    {
        // GET: Authorization/ContractorMaster
        [SessionExpireFilter]
        public ActionResult Index()
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("WQR007");
            return View();
        }

        public ActionResult loadHeaderDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[] { "WQR7.Location", "WQR7.ContractorCode", "lcom6.t_nama", "WQR7.ShopCode", "lcom2s.t_desc", "WQR7.Status", "WQR7.CreatedBy", "ecom3.t_name", "WQR7.EditedBy", "com3e.t_name" }, param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_ATH_GET_CONTRACTORS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.Id),
                        h.Location,//db.COM001.Where(i=>i.t_cprj==h.Project).Select(i=>i.t_dsca).FirstOrDefault(),//h.Project,
                        h.ContractorCode,
                        Convert.ToDateTime(h.FromDate).ToString("dd/MM/yyyy"),
                        Convert.ToDateTime(h.ToDate).ToString("dd/MM/yyyy"),
                        h.ShopCode,
                        h.Status,
                       "<center><i data-modal='' id='btnUpdate' name='btnUpdate' style='cursor:pointer;' Title='Update Record' class='fa fa-edit' onClick='ContractMasterDetail(" + h.Id + " )' ></i></center>",

                //"<center><a class='btn btn-xs' onClick='ContractMasterDetail(" + h.Id + ")'><i style='margin-left:5px;' class='fa fa-eye' ></i></a></center>",
                        //GetHTMLString(h.HeaderId)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ContractMasterDetailPartial(int? id)
        {
            WQR007 objWQR007 = new WQR007();
            //ModelContractorMaster objModelContractorMaster = GetContractorModel();

            ViewBag.Location = db.WQR008.Select(i => new CategoryData { Value = i.Location, CategoryDescription = i.Location + "-" + i.Description }).ToList();

            if (id > 0)
            {
                objWQR007 = db.WQR007.Where(i => i.Id == id).FirstOrDefault();
                if (objWQR007 != null)
                {
                    ViewBag.LocationSelected = db.WQR008.Where(i =>i.Location.Equals(objWQR007.Location, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location + "-" + i.Description).FirstOrDefault();
                    ViewBag.ContractorCodeSelected = db.COM006.Where(i => i.t_bpid.Equals(objWQR007.ContractorCode, StringComparison.Ordinal)).Select(i => i.t_bpid + "-" + i.t_nama).FirstOrDefault();
                    ViewBag.ShopCode = db.COM002.Where(i => i.t_dtyp == 3 && i.t_dimx.Equals(objWQR007.ShopCode, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_dimx + "-" + i.t_desc).FirstOrDefault();
                }
            }
            else
            {

            }

            return PartialView("_ContractMasterDetailPartial", objWQR007);
        }

        [HttpPost]
        public ActionResult SaveContractor(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WQR007 objWQR007 = new WQR007();
            try
            {
                if (fc != null)
                {
                    int id = Convert.ToInt32(fc["Id"]);
                    var status = fc["Status"];
                    if (!isContractorExist(fc["Location"].Split('-')[0], fc["ContractorCode"], id))
                    {
                        string FromDate = fc["FromDate"] != null ? fc["FromDate"].ToString() : "";
                        string ToDate = fc["ToDate"] != null ? fc["ToDate"].ToString() : "";
                        if (id > 0)
                        {
                            objWQR007 = db.WQR007.Where(i => i.Id == id).FirstOrDefault();
                            //objWQR007.Location = fc["Location"];
                            //objWQR007.ContractorCode = fc["ContractorCode"];
                            
                            objWQR007.FromDate = DateTime.ParseExact(FromDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                            objWQR007.ToDate = DateTime.ParseExact(ToDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                            //objWQR007.FromDate = Convert.ToDateTime(fc["FromDate"]);
                            //objWQR007.ToDate = Convert.ToDateTime(fc["ToDate"]);
                            objWQR007.ShopCode = fc["ShopCode"];
                            objWQR007.Status = Convert.ToBoolean(status) ? "Active" : "Inactive";
                            objWQR007.EditedBy = objClsLoginInfo.UserName;
                            objWQR007.EditedOn = DateTime.Now;

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Contractor Saved Successfully";
                        }
                        else
                        {
                            objWQR007.Location = fc["Location"].Split('-')[0];
                            objWQR007.ContractorCode = fc["ContractorCode"];
                            objWQR007.FromDate = DateTime.ParseExact(FromDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                            objWQR007.ToDate = DateTime.ParseExact(ToDate, @"d/M/yyyy", CultureInfo.InvariantCulture);
                            //objWQR007.FromDate = Convert.ToDateTime(fc["FromDate"]);
                            //objWQR007.ToDate = Convert.ToDateTime(fc["ToDate"]);
                            objWQR007.ShopCode = fc["ShopCode"];
                            objWQR007.Status = Convert.ToBoolean(status) ? "Active" : "Inactive";
                            objWQR007.CreatedBy = objClsLoginInfo.UserName;
                            objWQR007.CreatedOn = DateTime.Now;

                            db.WQR007.Add(objWQR007);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Contractor Saved Successfully";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Contractor Already Exist";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //public ModelContractorMaster GetContractorModel()
        //{
        //    ModelContractorMaster objModelContractorMaster = new ModelContractorMaster();

        //    var lstLocation = db.COM002.Where(i => i.t_dtyp == 2).Select(i => new CategoryData { Value = i.t_dimx, CategoryDescription = i.t_dimx + "-"+i.t_desc }).ToList();
        //    var lstcontractorCode = db.COM006.Select(i => new CategoryData { Value = i.t_bpid, CategoryDescription = i.t_bpid + "-" + i.t_nama }).ToList();
        //    var lstShopCode = db.COM002.Where(i => i.t_dtyp == 3).Select(i => new CategoryData { Value = i.t_dimx, CategoryDescription = i.t_dimx + "-" + i.t_desc }).ToList();
        //    objModelContractorMaster.Location = lstLocation.Distinct().ToList();
        //    objModelContractorMaster.ContractorCode = lstcontractorCode.Distinct().ToList();
        //    objModelContractorMaster.ShopCode = lstShopCode.Distinct().ToList();
        //    return objModelContractorMaster;
        //}

        [HttpPost]
        public ActionResult GetContractorCode(string term)
        {
            List<CategoryData> lstcontractorCode = new List<CategoryData>();
            if (!string.IsNullOrEmpty(term))
            {
                lstcontractorCode = db.COM006.Where(i => i.t_bpid.ToLower().Contains(term.ToLower())).Select(i => new CategoryData { Value = i.t_bpid, CategoryDescription = i.t_bpid + "-" + i.t_nama }).ToList();
            }
            else
            {
                lstcontractorCode = db.COM006.Select(i => new CategoryData { Value = i.t_bpid, CategoryDescription = i.t_bpid + "-" + i.t_nama }).Take(10).ToList();
            }
            return Json(lstcontractorCode, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetShopCode(string term)
        {
            List<CategoryData> lstShopCode = new List<CategoryData>();
            if (!string.IsNullOrEmpty(term))
            {
                lstShopCode = db.COM002.Where(i => i.t_dtyp == 3 && i.t_dimx.Contains(term)).Select(i => new CategoryData { Value = i.t_dimx, CategoryDescription = i.t_dimx + "-" + i.t_desc }).ToList();
            }
            else
            {
                lstShopCode = db.COM002.Where(i => i.t_dtyp == 3).Select(i => new CategoryData { Value = i.t_dimx, CategoryDescription = i.t_dimx + "-" + i.t_desc }).Take(10).ToList();
            }

            return Json(lstShopCode, JsonRequestBehavior.AllowGet);
        }

        public bool isContractorExist(string location, string contractorCode, int id = 0)
        {
            bool isContractorExist = false;
            if (db.WQR007.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.ContractorCode.Equals(contractorCode, StringComparison.OrdinalIgnoreCase) && i.Id != id).Any())
                isContractorExist = true;
            return isContractorExist;
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0, int? latestRv = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_ATH_GET_CONTRACTORS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Location=li.Location,
                                      ContractorCode=li.ContractorCode,
                                      FromDate=li.FromDate,
                                      ToDate=li.ToDate,
                                      Shop=li.ShopCode,
                                      Stutus=li.Status,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }



                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

    }
}