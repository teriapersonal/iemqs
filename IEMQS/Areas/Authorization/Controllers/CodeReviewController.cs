﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class CodeReviewController : clsBase
    {
        #region Utility
        [HttpPost]
        public ActionResult FillSubType(string reviewType)
        {
            List<BULocWiseCategoryModel> lstSubType = new List<BULocWiseCategoryModel>();
            if (reviewType.ToLower() == clsImplementationEnum.CodeReviewType.Application.GetStringValue().ToLower())
            {
                List<string> lstApplicationSubType = clsImplementationEnum.getCodeReviewApplicationSubType().ToList();
                lstSubType = lstApplicationSubType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            }
            else if (reviewType.ToLower() == clsImplementationEnum.CodeReviewType.Database.GetStringValue().ToLower())
            {
                List<string> lstDatabaseSubType = clsImplementationEnum.getCodeReviewDatabaseSubType().ToList();
                lstSubType = lstDatabaseSubType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            }

            return Json(lstSubType, JsonRequestBehavior.AllowGet);
        }
        #endregion
        // GET: Authorization/CodeReview
        #region Project Listing Page

        [SessionExpireFilter]
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadDataGridPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_CodeReviewDataGridPartial");
        }
        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = "1=1";

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                //search Condition 
                string[] columnName = { "Area", "Type", "SubType", "Path", "InvalidCode", "ValidCode", "Status", "Comments", "CreatedBy", "ApprovedBy" };
                //strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_ATH_GET_CODEREVIEW_DETAILS(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.Id),
                            Convert.ToString(uc.Area),
                            Convert.ToString(uc.Type),
                            Convert.ToString(uc.SubType),
                            Convert.ToString(uc.Path),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.Comments),
                            Convert.ToString(uc.CreatedBy),
                            uc.CreatedOn.HasValue ? Convert.ToString(uc.CreatedOn.Value.ToShortDateString()) : "",
                            Convert.ToString(uc.ApprovedBy),
                            uc.ApprovedOn.HasValue ? Convert.ToString(uc.ApprovedOn.Value.ToShortDateString()) : "",
                            Helper.GenerateActionIcon(uc.Id, "View", "View Detail", "fa fa-eye", "",WebsiteURL+"/Authorization/CodeReview/Details/"+uc.Id,false) /*+ Helper.GenerateActionIcon(uc.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteCodeReview("+uc.Id+")","",false)*/
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult DeleteCodeReview(int Id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objATH023 = db.ATH023.Where(x => x.Id == Id).FirstOrDefault();
                if (objATH023 != null)
                {
                    db.ATH023.Remove(objATH023);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Code Review deleted sucessfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Code Review not found, please try again";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                //Main grid(user detail)
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_ATH_GET_CODEREVIEW_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Area = Convert.ToString(uc.Area),
                                      Type = Convert.ToString(uc.Type),
                                      SubType = Convert.ToString(uc.SubType),
                                      Path = Convert.ToString(uc.Path),
                                      InvalidCode = Convert.ToString(uc.InvalidCode),
                                      ValidCode = Convert.ToString(uc.ValidCode),
                                      Status = Convert.ToString(uc.Status),
                                      Comments = Convert.ToString(uc.Comments),
                                      CreatedBy = uc.CreatedBy + "-" + Manager.GetUserNameFromPsNo(uc.CreatedBy),
                                      CreatedDate = uc.CreatedOn == null || uc.CreatedOn.Value == DateTime.MinValue ? "" : uc.CreatedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      ApprovedBy = uc.ApprovedBy != null ? uc.ApprovedBy + "-" + Manager.GetUserNameFromPsNo(uc.ApprovedBy) : string.Empty,
                                      ApprovedDate = uc.ApprovedOn == null || uc.ApprovedOn.Value == DateTime.MinValue ? "" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture)
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Code Review Details Page
        [SessionExpireFilter]
        [AllowAnonymous]
        public ActionResult Details(int? Id)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("ATH023");
            ATH023 objATH023 = null;

            List<string> lstType = clsImplementationEnum.getCodeReviewType().ToList();
            ViewBag.Type = lstType.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();

            
            ViewBag.lstArea = db.ATH002.Where(x => x.ParentId == null && x.IsDisplayInMenu == true).Distinct().AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.Area, CatID = x.Area }).ToList();

            if (Id > 0)
            {
                objATH023 = db.ATH023.FirstOrDefault(c => c.Id == Id);
            }
            else
            {
                objATH023 = new ATH023();
                objATH023.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
            }
            return View(objATH023);
        }

        [HttpPost]
        public ActionResult SaveCodeReviewData(ATH023 model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (model.Id > 0)
                {
                    // this saction is for edit operation
                    var objATH023 = db.ATH023.Where(x => x.Id == model.Id).FirstOrDefault();

                    objATH023.Area = model.Area;
                    objATH023.Type = model.Type;
                    objATH023.SubType = model.SubType;
                    objATH023.Path = model.Path;
                    objATH023.InvalidCode = model.InvalidCode;
                    objATH023.ValidCode = model.ValidCode;
                    objATH023.Status = model.Status;
                    objATH023.Comments = model.Comments;
                    objATH023.EditedBy = objClsLoginInfo.UserName;
                    objATH023.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Value = "Code Review Updated Successfully.";
                    objResponseMsg.HeaderId = objATH023.Id;
                }
                else
                {
                    ATH023 objATH023 = new ATH023
                    {
                        Area = model.Area,
                        Type = model.Type,
                        SubType = model.SubType,
                        Path = model.Path,
                        InvalidCode = model.InvalidCode,
                        ValidCode = model.ValidCode,
                        Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue(),
                        Comments = model.Comments,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    };
                    db.ATH023.Add(objATH023);
                    db.SaveChanges();
                    objResponseMsg.Value = "Code Review Data saved successfully.";
                    objResponseMsg.HeaderId = objATH023.Id;
                }

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ApproveRecord(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Id > 0)
                {
                    ATH023 objATH023 = db.ATH023.Where(x => x.Id == Id).FirstOrDefault();
                    objATH023.Status = clsImplementationEnum.PAMStatus.Approved.GetStringValue();
                    objATH023.ApprovedBy = objClsLoginInfo.UserName;
                    objATH023.ApprovedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Code review successfully approved";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Code review not approved";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}