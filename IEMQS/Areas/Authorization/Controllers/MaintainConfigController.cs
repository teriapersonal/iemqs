﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQS.PLMBOMService;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsHelper;
namespace IEMQS.Areas.Authorization.Controllers
{
    public class MaintainConfigController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index()
        {
            var LoginRoles = objClsLoginInfo.GetUserRoleList();
            if (LoginRoles.Contains(clsImplementationEnum.UserRoleName.ITA1.GetStringValue()) || LoginRoles.Contains(clsImplementationEnum.UserRoleName.ITA2.GetStringValue()) || LoginRoles.Contains(clsImplementationEnum.UserRoleName.ITA3.GetStringValue()))
                return View();
            else
                return new RedirectResult("~/Authentication/Authenticate/AccessDenied");
        }

        public ActionResult loadConfigDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = " 1=1 ";

                string[] columnName = { "[Description]", "Value", "Responsible", "EditedBy", "EditedOn" };
                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var LoginRoles = objClsLoginInfo.GetUserRoleList();
                if(LoginRoles.Contains(clsImplementationEnum.UserRoleName.ITA3.GetStringValue()) || LoginRoles.Contains(clsImplementationEnum.UserRoleName.ITA2.GetStringValue()))
                    whereCondition += " AND Responsible = 'Functional' ";
                //else if (!LoginRoles.Contains(clsImplementationEnum.UserRoleName.ITA1.GetStringValue()))
                //    whereCondition += " AND Responsible is null ";

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_CONFIG_GET_CONFIG(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstPam
                          select new[] {
                               h.Id.ToString(),
                               h.Description,
                               h.Value,
                               h.Responsible,
                               h.EditedBy,
                               h.EditedOn == null || h.EditedOn.Value==DateTime.MinValue? string.Empty :h.EditedOn.Value.ToString("dd/MM/yyyy HH:mm" , CultureInfo.InvariantCulture),
                               Helper.GenerateGridButton( h.Id, "Edit", "Edit Record", "fa fa-edit", "EditRecord("+h.Id+")")
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveConfig(FormCollection fc)
        {
            ResponceMsgTCP objResponseMsg = new ResponceMsgTCP();
            CONFIG objCONFIG = new CONFIG();
            try
            {
                int Id = Convert.ToInt32("0"+fc["Id"]);
                objCONFIG = db.CONFIG.FirstOrDefault(w => w.Id == Id);
                if (objCONFIG != null)
                {
                    objCONFIG.Value = fc["Value" + Id];
                    objCONFIG.EditedBy = objClsLoginInfo.UserName;
                    objCONFIG.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Value = "Value updated successfully";
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Value = "Record not found";
                    //#region Add New Spot Line1
                    //objCONFIG.Value = strValue;
                    //objCONFIG.CreatedBy = objClsLoginInfo.UserName;
                    //objCONFIG.CreatedOn = DateTime.Now;
                    //db.CONFIG.Add(objTCPobjCONFIG002);
                    //objResponseMsg.Value = "Value added successfully";
                    //objResponseMsg.Key = true;
                    //#endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

    }
}