﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class IntWelderStampController : clsBase
    {
        // GET: Authorization/IntWelderStamp
        // GET: Authorization/StageMaster
        //[AllowAnonymous]
        [SessionExpireFilter]
        //[UserPermissions]
        public ActionResult Index()
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("QMS003");
            ViewBag.IsDisplayOnly = false;
            return View();
        }
        [HttpPost]
        public ActionResult getData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Location like '%" + param.sSearch
                        + "%' or WelderPSNo like '%" + param.sSearch
                        + "%' or Stamp like '%" + param.sSearch
                        + "%' or Evaluation like '%" + param.sSearch
                        + "%' or CreatedBy like '%" + param.sSearch
                        + "%' or CreatedOn like '%" + param.sSearch
                        + "%')";

                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_ATH_GET_IntWelderStamp
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.Location),
                           Convert.ToString(getCodeValue(uc.WelderPSNo)),
                           Convert.ToString(uc.Stamp),
                           Convert.ToString(uc.Evaluation),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetIweld(int Id)
        {
            QMS003 objQMS003 = new QMS003();
            if (Id > 0)
            {
                objQMS003 = db.QMS003.Where(x => x.Id == Id).FirstOrDefault();
                ViewBag.Location = db.WQR008.Where(i => i.Location.Equals(objQMS003.Location, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location + "-" + i.Description).FirstOrDefault();
            }
            else
            {
                //var Location = (from a in db.COM002
                //                where a.t_dtyp == 1 && a.t_dimx != ""
                //                select new { a.t_dimx, Desc = a.t_desc }).ToList();
                //ViewBag.Loca = new SelectList(Location, "t_dimx", "Desc");
            }

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            //ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
            ViewBag.lstyesno = clsImplementationEnum.getyesno().ToArray();

            var Psno = (from a in db.COM003
                        where a.t_actv == 1
                        select new { a.t_psno, Desc = a.t_psno }).ToList();
            ViewBag.Psno = new SelectList(Psno, "t_psno", "Desc");
            ViewBag.ShopCode = db.COM002.Where(i => i.t_dimx.Equals(objQMS003.ShopCode, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_dimx + "-" + i.t_desc).FirstOrDefault();
            return PartialView("IntWelderStampHtml", objQMS003);
        }
        [HttpPost]
        public ActionResult SaveIwel(QMS003 qms003, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string WelderPSNo =string.Empty;
                string WelderName = string.Empty;
                if (!string.IsNullOrWhiteSpace(fc["ddlpsno"]))
                {
                    WelderPSNo = fc["ddlpsno"].ToString().Split('-')[0];
                    WelderName = fc["ddlpsno"].ToString().Split('-')[1];
                }
                var psno = fc["ddlpsno"].ToString();
                var stamp = fc["txtStamp"].ToString().ToUpper();
                var isvalid = db.QMS003.Any(x => (x.WelderPSNo == psno && x.Evaluation == true )|| ( x.Stamp == stamp && x.Evaluation==true) );
                if (isvalid == false)
                {
                    bool d1;
                    if (fc["ddlEva"].ToString() == "Yes")
                    {
                        d1 = true;
                    }
                    else
                    {
                        d1 = false;
                    }

                    QMS003 objqms003 = new QMS003();
                    objqms003.Location = fc["ddlLoca"].ToString().Split('-')[0];
                    objqms003.WelderPSNo = !string.IsNullOrWhiteSpace(WelderPSNo) ? WelderPSNo.Trim() : null;
                    objqms003.Name = !string.IsNullOrWhiteSpace(WelderName) ? WelderName.Trim() : null;
                    objqms003.Stamp = fc["txtStamp"].ToString().ToUpper();
                    objqms003.Evaluation = d1;
                    objqms003.ShopCode = fc["ShopCode"];
                    objqms003.CreatedBy = objClsLoginInfo.UserName;
                    objqms003.CreatedOn = DateTime.Now;
                    db.QMS003.Add(objqms003);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteIwel(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS003 objQMS003 = db.QMS003.Where(x => x.Id == Id).FirstOrDefault();
                db.QMS003.Remove(objQMS003);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateIwel(QMS003 qms003, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int id = Convert.ToInt32(fc["hfid"].ToString());
                if (id > 0)
                {
                    bool d1;
                    if (fc["ddlEva"].ToString() == "Yes")
                    {
                        d1 = true;
                    }
                    else
                    {
                        d1 = false;
                    }
                    string WelderPSNo = string.Empty;
                    string WelderName = string.Empty;
                    if (!string.IsNullOrWhiteSpace(fc["ddlpsno"]))
                    {
                        WelderPSNo = fc["ddlpsno"].ToString().Split('-')[0];
                        WelderName = fc["ddlpsno"].ToString().Split('-')[1];
                    }
                    QMS003 objqms003 = db.QMS003.Where(x => x.Id == id).FirstOrDefault();
                    objqms003.Location = fc["ddlLoca"].ToString().Split('-')[0];
                    objqms003.WelderPSNo = !string.IsNullOrWhiteSpace(WelderPSNo) ? WelderPSNo.Trim() : null;
                    objqms003.Name = !string.IsNullOrWhiteSpace(WelderName) ? WelderName.Trim() : null;
                    objqms003.Stamp = fc["txtStamp"].ToString().ToUpper();
                    objqms003.Evaluation = d1;
                    objqms003.ShopCode = fc["ShopCode"];
                    objqms003.EditedBy = objClsLoginInfo.UserName;
                    objqms003.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string getCodeValue(string WelderPSNo)
        {
            var projdesc = db.COM003.Where(i => i.t_psno == WelderPSNo && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            return projdesc;
        }
        [HttpPost]
        public ActionResult getDesc(string WelderPSNo)
        {
            ResponceMsgWithStatus1 objResponseMsg = new ResponceMsgWithStatus1();
            try
            {
                objResponseMsg.projdesc = db.COM003.Where(i => i.t_psno == WelderPSNo && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult verifyStampNo(string stampNo)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                var IntStampExists = db.QMS003.Any(x => x.Stamp == stampNo && x.Evaluation == true);
                var ExtStampExists = db.QMS004.Any(x => x.Stamp == stampNo && x.Evaluation == true);
                if (IntStampExists || ExtStampExists)
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult verifyEmployee(string employee)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                employee = employee.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == employee && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult DisplayIntWeld()
        {
            ViewBag.IsDisplayOnly = true;
            return View("Index");
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_ATH_GET_IntWelderStamp(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Location = Convert.ToString(li.Location),
                                      WelderPSNo = Convert.ToString(getCodeValue(li.WelderPSNo)),
                                      Stamp = Convert.ToString(li.Stamp),
                                      Evaluation = Convert.ToString(li.Evaluation),
                                      CreatedBy = Convert.ToString(li.CreatedBy),
                                      CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
    public class ResponceMsgWithStatus : clsHelper.ResponseMsg
    {
        public string status;
        public string Revision;
        public string projdesc;

    }
}