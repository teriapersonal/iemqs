﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationEnum;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class AppVersionController : clsBase
    {
        // GET: Authorization/AppVersion
        [AllowAnonymous]
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        #region Index Page
        [HttpPost]
        public ActionResult LoadDataGridPartial(string status)
        {
            return PartialView("_AppVersionDataGridPartial");
        }

        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = "where 1=1";

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                //search Condition 
                string[] columnName = { "Version", "Feature", "BugFix", "Enhancement" };
                //strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_ATH_GET_APP_VERSION(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                 Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.Version),
                            Convert.ToString(uc.Feature),
                            Convert.ToString(uc.Enhancement),
                            Convert.ToString(uc.BugFix),
                            Helper.GenerateActionIcon(uc.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL+"/Authorization/AppVersion/Details/"+uc.HeaderId,false)
                             }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        //#region Header Details Page
        [AllowAnonymous]
        [SessionExpireFilter]
        public ActionResult Details(int? Id)
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("ATH021");
            ATH021 objATH021 = new ATH021();
            //If Id pass then get records from database. otherwise new entry in database.
            if (Id.HasValue)
            {
                objATH021 = db.ATH021.Where(c => c.HeaderId == Id).FirstOrDefault();
            }
            else
            {
                objATH021.CreatedBy = objClsLoginInfo.UserName;
                objATH021.CreatedOn = DateTime.Now;
                objATH021.Status = AppVersionStatus.Draft.GetStringValue();
                db.ATH021.Add(objATH021);
                db.SaveChanges();
            }
            List<string> lstAppVersionType = clsImplementationEnum.getAppVersionType().ToList();
            ViewBag.AppVersionTypes = lstAppVersionType.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            var lstArea = (from li in db.ATH002
                           where li.ParentId == null
                           select new SelectItemList
                           {
                               id = li.Id.ToString(),
                               text = li.Description,
                           }).Select(x => new CategoryData { Value = x.text, Code = x.text.ToString(), CategoryDescription = x.text.ToString() }).Distinct().ToList();
            ViewBag.lstArea = lstArea;
            return View(objATH021);
        }
        [HttpPost]
        public ActionResult GetAppVersionHeaderData(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = "where 1=1";
                if (!string.IsNullOrEmpty(headerId))
                {
                    strWhereCondition += " and HeaderId=" + headerId;
                }
                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                //search Condition 
                string[] columnName = { "HeaderId", "Type", "Area", "Remarks", "CreatedBy", "CreatedOn", "EditedBy", "EditedOn" };
                strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.SP_ATH_GET_APP_VERSION_DETAIL(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", ""),
                                    Helper.HTMLAutoComplete(newRecordId,"Type","","",false,"","Type"),
                                    Helper.HTMLAutoComplete(newRecordId, "Area",   "", "", false, "", "50"),
                                    Helper.GenerateTextArea(newRecordId, "Remarks",  "", "", false, "", "500"),
                                    Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveAppVersionHeader(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                              Convert.ToString(uc.LineId),
                               Helper.GenerateHidden(uc.LineId, "HeaderId", Convert.ToString(uc.HeaderId)),
                               Helper.HTMLAutoComplete(uc.LineId, "Type",Convert.ToString(uc.Type),"",true,""),
                               Helper.HTMLAutoComplete(uc.LineId, "Area",Convert.ToString(uc.Area),"",true,""),
                               Helper.GenerateTextArea(uc.LineId, "Remarks",Convert.ToString(uc.Remarks),"",true,"","500"),
                               Helper.GenerateActionIcon(uc.LineId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteData("+uc.LineId+")","",false)+
                               Helper.GenerateActionIcon(uc.LineId, "Edit", "Edit Record", "fa fa-pencil-square-o iconspace editline", "","",false)
                               }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveAppVersionHeader(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            ATH022 objATH022 = new ATH022();
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objATH022 = db.ATH022.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objATH022.Type = !string.IsNullOrEmpty(fc["Type" + refLineId]) ? Convert.ToString(fc["Type" + refLineId]).Trim() : "";
                        objATH022.Area = !string.IsNullOrEmpty(fc["Area" + refLineId]) ? Convert.ToString(fc["Area" + refLineId]).Trim() : "";
                        objATH022.Remarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";
                        //objATH022.LineId = refLineId;
                        objATH022.EditedBy = objClsLoginInfo.UserName;
                        objATH022.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objATH022 = new ATH022();
                        objATH022.HeaderId = refHeaderId;
                        objATH022.Type = !string.IsNullOrEmpty(fc["Type" + newRowIndex]) ? Convert.ToString(fc["Type" + newRowIndex]).Trim() : "";
                        objATH022.Area = !string.IsNullOrEmpty(fc["Area" + newRowIndex]) ? Convert.ToString(fc["Area" + newRowIndex]).Trim() : "";
                        objATH022.Remarks = !string.IsNullOrEmpty(fc["Remarks" + newRowIndex]) ? Convert.ToString(fc["Remarks" + newRowIndex]).Trim() : "";

                        objATH022.CreatedBy = objClsLoginInfo.UserName;
                        objATH022.CreatedOn = DateTime.Now;
                        db.ATH022.Add(objATH022);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objATH022.LineId;

                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteDetails(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ATH022 objATH022 = db.ATH022.Where(x => x.LineId == id).FirstOrDefault();
                if (objATH022 != null)
                {
                    db.ATH022.Remove(objATH022);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        // #endregion

        public ActionResult ReleaseVersion(int id, string DisplayTillDate)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string strReleased = AppVersionStatus.Released.GetStringValue();
            string strFeatures = AppVersionType.Features.GetStringValue();
            string strEnhancements = AppVersionType.Enhancements.GetStringValue();
            string strBugFix = AppVersionType.BugFix.GetStringValue();
            try
            {
                var objATH021 = db.ATH021.Where(i => i.HeaderId == id).FirstOrDefault();
                if (objATH021 != null)
                {
                    if (objATH021.ATH022.Count > 0)
                    {
                        var objReleased = db.ATH021.Where(i => i.Status == strReleased).OrderByDescending(i => i.ReleasedOn).FirstOrDefault();
                        //if there is not any released version then default 1.0.0.
                        if (objReleased == null)
                        {
                            objATH021.Version = "1.0.0";
                            objATH021.Feature = 1;
                            objATH021.Enhancement = 0;
                            objATH021.BugFix = 0;
                        }
                        else
                        {
                            var objATH022 = db.ATH022.Where(i => i.HeaderId == id).ToList();
                            if (objATH022.Exists(i => i.Type == strFeatures))
                            {
                                objATH021.Feature = objReleased.Feature + 1;
                                objATH021.Enhancement = 0;
                                objATH021.BugFix = 0;
                            }
                            else if (objATH022.Exists(i => i.Type == strEnhancements))
                            {
                                objATH021.Feature = objReleased.Feature;
                                objATH021.Enhancement = objReleased.Enhancement + 1;
                                objATH021.BugFix = 0;
                            }
                            else if (objATH022.Exists(i => i.Type == strBugFix))
                            {
                                objATH021.Feature = objReleased.Feature;
                                objATH021.Enhancement = objReleased.Enhancement;
                                objATH021.BugFix = objReleased.BugFix + 1;
                            }
                            objATH021.Version = objATH021.Feature.ToString() + "." + objATH021.Enhancement.ToString() + "." + objATH021.BugFix.ToString();
                        }
                        objATH021.ReleasedBy = objClsLoginInfo.UserName;
                        objATH021.ReleasedOn = DateTime.Now;
                        objATH021.Status = strReleased;
                        objATH021.EditedBy = objClsLoginInfo.UserName;
                        objATH021.EditedOn = DateTime.Now;
                        var dtDisplayTillDate = Manager.getDateTime(DisplayTillDate);
                        if (dtDisplayTillDate == null)
                        {
                            objATH021.DisplayTillDate = DateTime.Now.AddDays(2);
                        }
                        else
                        {
                            objATH021.DisplayTillDate = dtDisplayTillDate;
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.AppVersionReleased.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.AppVersionNoLines.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Delete(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ATH021 objATH021 = db.ATH021.Where(x => x.HeaderId == id).FirstOrDefault();
                if (objATH021 != null)
                {
                    db.ATH022.RemoveRange(objATH021.ATH022.ToList());
                    db.ATH021.Remove(objATH021);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetAppVersionDtl(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<ATH021> listATH021 = null;
            List<ATH022> listATH022 = null;
            string strReleased = AppVersionStatus.Released.GetStringValue();
            string strFeatures = AppVersionType.Features.GetStringValue();
            string strEnhancements = AppVersionType.Enhancements.GetStringValue();
            string strBugFix = AppVersionType.BugFix.GetStringValue();
            List<string> listType = new List<string>();
            try
            {
                if (id == 0)
                {
                    listATH021 = db.ATH021.Where(i => i.Status == strReleased).OrderByDescending(i => i.ReleasedOn).ToList();
                    listATH022 = (from ath21 in db.ATH021
                                  join ath22 in db.ATH022 on ath21.HeaderId equals ath22.HeaderId
                                  where ath21.Status == strReleased
                                  select ath22
                                  ).ToList();
                }
                else
                {
                    listATH021 = db.ATH021.Where(i => i.Status == strReleased && i.HeaderId == id).ToList();
                    listATH022 = (from ath21 in db.ATH021
                                  join ath22 in db.ATH022 on ath21.HeaderId equals ath22.HeaderId
                                  where ath21.Status == strReleased && ath21.HeaderId == id
                                  select ath22
                                  ).ToList();
                }

                string returnhtml = "";

                returnhtml += "<div class=\"dvversiondtl\">";
                foreach (var ath21 in listATH021)
                {
                    listType = new List<string>();
                    returnhtml += "<div class=\"row versionmarginbottom\">";
                    returnhtml += "<div class=\"col-md-6 bold versionheading\"> Version : " + ath21.Version + " </div><div class=\"col-md-6 bold versionheading\"> Released On : " + ath21.ReleasedOn.Value.ToString("dd/MM/yyyy") + " </div>";

                    if (listATH022.Any(i => i.HeaderId == ath21.HeaderId && i.Type == strFeatures))
                    {
                        listType.Add(strFeatures);
                    }
                    if (listATH022.Any(i => i.HeaderId == ath21.HeaderId && i.Type == strEnhancements))
                    {
                        listType.Add(strEnhancements);
                    }
                    if (listATH022.Any(i => i.HeaderId == ath21.HeaderId && i.Type == strBugFix))
                    {
                        listType.Add(strBugFix);
                    }

                    if (listType.Count > 0)
                    {
                        foreach (var type in listType)
                        {
                            returnhtml += "<div class=\"col-md-12\">";

                            var objATH022 = listATH022.Where(i => i.HeaderId == ath21.HeaderId && i.Type == type).ToList();
                            if (objATH022 != null)
                            {
                                var objArea = objATH022.Select(i => new { i.Area }).Distinct().ToList();
                                if (objArea.Count > 0)
                                {
                                    returnhtml += "<div class=\"col-md-12 versiontype\">" + type + "</div>";
                                    returnhtml += "<ul>";
                                    //foreach (var area in objArea)
                                    //{
                                    //    //returnhtml += "<li class=\"bold\">" + area.Area + "</li>";
                                    //    var objRemarks = objATH022.Where(i => i.Area == area.Area && i.Type == type).ToList();
                                    //    if (objRemarks.Count > 0)
                                    //    {
                                    //        returnhtml += "<ol class=\"versionol\">";
                                    //        foreach (var ath22 in objRemarks)
                                    //        {
                                    //            returnhtml += "<li>" + ath22.Area + " - " + ath22.Remarks + "</li>";
                                    //        }
                                    //        returnhtml += "</ol>";
                                    //    }
                                    //}
                                    //returnhtml += "<li class=\"bold\">" + area.Area + "</li>";

                                    //var objRemarks = objATH022.Where(i => i.Type == type).ToList();
                                    if (objATH022.Count > 0)
                                    {
                                        returnhtml += "<ol class=\"versionol\">";
                                        foreach (var ath22 in objATH022)
                                        {
                                            returnhtml += "<li>" + ath22.Area + " - " + ath22.Remarks + "</li>";
                                        }
                                        returnhtml += "</ol>";
                                    }

                                    returnhtml += "</ul>";
                                }
                            }
                            returnhtml += "</div>";
                        }
                    }
                    returnhtml += "</div>";
                }                
                returnhtml += "</div>";
                objResponseMsg.Key = true;
                objResponseMsg.Value = returnhtml;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
    }
}