﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class AccessRequestController : clsBase
    {
        // GET: Authorization/AccessRequest
        #region Access Request Page

        [SessionExpireFilter]
        [AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("ATH020");
            return View();
        }
        [HttpPost]
        public ActionResult LoadDataGridPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_AccessRequestDataGridPartial");
        }

        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (param.Status.ToUpper() == "PENDING")
                {
                    strWhereCondition += " 1=1 and status in('" + clsImplementationEnum.AccessRequest.Requested.GetStringValue() + "')";
                }
                else
                {
                    strWhereCondition += " 1=1";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                #endregion

                //search Condition 
                string[] columnName = { "RequestNo", "PSNo", "BU", "Location", "RoleId", "Status", "Remarks", "RefId", "CreatedOn" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_ATH_GET_ACCESSREQUEST_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.RequestId),
                             Convert.ToString(uc.RequestNo),
                            Convert.ToString(uc.PSNo),
                            Convert.ToString(uc.BU),
                            Convert.ToString(uc.Location),
                            Convert.ToString(uc.RoleId),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.Remarks),
                            Convert.ToString(uc.RefId),
                            uc.CreatedOn == null || uc.CreatedOn.Value==DateTime.MinValue? "" :uc.CreatedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                            Helper.GenerateActionIcon(uc.RequestId, "view", "view Detail", "fa fa-eye","EditDetails("+uc.RequestId+")","",false,false,"color: #337ab7;")

                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
        #region Access Request Page

        [SessionExpireFilter]
        [AllowAnonymous]
        public ActionResult AccessRequestDetailsPartial(int? Id)
        {
            ATH020 objATH020 = new ATH020();

            if (Id > 0)
            {
                objATH020 = db.ATH020.Where(x => x.RequestId == Id).FirstOrDefault();
                ViewBag.UserPS = objATH020.PSNo + " - " + Manager.GetUserNameFromPsNo(objATH020.PSNo);
                if (objATH020.Status == clsImplementationEnum.AccessRequest.Requested.GetStringValue())
                    ViewBag.IsEditApplicable = true;
                else
                    ViewBag.IsEditApplicable = false;

                int BU = Convert.ToInt32(clsImplementationEnum.BLDNumber.BU.GetStringValue());
                ViewBag.BUDesc = Manager.getBUsByID(BU).Where(i => i.t_dimx == objATH020.BU).FirstOrDefault().t_desc;

                int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
                ViewBag.LocationDesc = Manager.getBUsByID(LOC).Where(i => i.t_dimx == objATH020.Location).FirstOrDefault().t_desc;

                ViewBag.RoleDesc = (from roles in db.ATH004
                                    where roles.Id == objATH020.RoleId
                                    select roles.Description).FirstOrDefault().ToString();
            }

            return PartialView("_AccessRequestDetailsPartial", objATH020);

        }
        [HttpPost]
        public ActionResult ApproveRequest(ATH020 model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (model.RequestId > 0)
                {
                    var objATH020 = db.ATH020.Where(x => x.RequestId == model.RequestId).FirstOrDefault();
                    objATH020.BU = model.BU;
                    objATH020.Location = model.Location;
                    objATH020.RoleId = model.RoleId;
                    objATH020.Status = clsImplementationEnum.AccessRequest.Approved.GetStringValue();
                    objATH020.Remarks = model.Remarks;
                    objATH020.EditedBy = objClsLoginInfo.UserName;
                    objATH020.EditedOn = DateTime.Now;


                    ATH001 objATH001 = new ATH001();
                    objATH001.BU = objATH020.BU;
                    objATH001.Location = objATH020.Location;
                    objATH001.Contract = "None";
                    objATH001.Project = "None";
                    objATH001.Role = objATH020.RoleId.Value;
                    objATH001.Employee = objATH020.PSNo;
                    objATH001.CreatedBy = objClsLoginInfo.UserName;
                    objATH001.CreatedOn = DateTime.Now;
                    db.ATH001.Add(objATH001);
                    db.SaveChanges();

                    objATH020.RefId = objATH001.Id;
                    db.SaveChanges();

                    #region Send Notification
                    ATH004 objATH004 = db.ATH004.FirstOrDefault(c => c.Id == objATH020.RoleId.Value);
                    (new clsManager()).SendNotificationByUserPSNumber(objATH004.Role, objATH020.PSNo, "Your request  has been approved", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Request has been approved.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RejectRequest(ATH020 model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (model.RequestId > 0)
                {
                    var objATH020 = db.ATH020.Where(x => x.RequestId == model.RequestId).FirstOrDefault();
                    objATH020.BU = model.BU;
                    objATH020.Location = model.Location;
                    objATH020.RoleId = model.RoleId;
                    objATH020.Status = clsImplementationEnum.AccessRequest.Rejected.GetStringValue();
                    objATH020.Remarks = model.Remarks;
                    objATH020.EditedBy = objClsLoginInfo.UserName;
                    objATH020.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    #region Send Notification
                    ATH004 objATH004 = db.ATH004.FirstOrDefault(c => c.Id == objATH020.RoleId.Value);
                    (new clsManager()).SendNotificationByUserPSNumber(objATH004.Role, objATH020.PSNo, "Your request  has been rejected", clsImplementationEnum.NotificationType.Information.GetStringValue());
                    #endregion

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Request has been rejected.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region -- ISO Check (observation 16532 change 14/09/2018)
        [HttpPost]
        public ActionResult CheckISO(string user, int role = 0)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (user != null && user != "")
                {
                    var objcheck = db.ATH015.Where(x => x.Employee == user && x.Role == role).FirstOrDefault();
                    if (objcheck != null)
                    {
                        if (objcheck.IsActive != false)
                        {
                            objResponseMsg.Key = true;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "User " + user + " Is De-Activated.";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "ISO Designation is Not Maintain for this User";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Employee and Role Not Maintained";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }

}