﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class RoleProcessMappingController : clsBase
    {
        // GET: Authorization/RoleProcessMapping
        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult Index()
        {
            //var list = (from a in db.ATH002
            //            where a.ParentId != null
            //            orderby a.ParentId, a.OrderNo
            //            select new { a.Id, Desc = a.Description }).ToList();
            //ViewBag.Process = new SelectList(list, "Id", "Desc");

            //var loc = (from a in db.ATH004
            //           select new { a.Id, Desc = a.Role }).ToList();
            //ViewBag.Role = new SelectList(loc, "Id", "Desc");

            return View();
        }

        // Observation ID : 13891 : Complete By Ajay CHauhan on 28/11/2017
        [HttpPost]
        public ActionResult GetRoleSearch(string term)
        {
            var lstRoles = (from roles in db.ATH004
                            where (roles.Role.Contains(term) || roles.Description.Contains(term))
                            orderby roles.Role
                            select new { roles.Role, roles.Description, roles.Id }).Distinct().ToList();
            return Json(lstRoles, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult GetProcessSearch(string term)
        {
            var lstProcesses = (from a in db.ATH002
                                where a.ParentId != null && a.Description.Contains(term) && a.IsActive == "1"
                                orderby a.ParentId, a.OrderNo
                                select new { a.Id, Desc = a.Description }).Distinct().ToList();

            return Json(lstProcesses, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult LoadRPMatrixData(JQueryDataTableParamModel param)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += string.Format(" and  (Process like '%{0}%' or c.Role like '%{0}%' or a.CreatedBy + ' - ' + d.t_name like '%{0}%'", param.sSearch);
                    DateTime tempdate;
                    if (DateTime.TryParse(param.sSearch, out tempdate))
                        strWhere += " or CONVERT(date, a.CreatedOn) Like '" + tempdate.ToString("yyyy-MM-dd") + "%'";
                    strWhere += " )";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_ATH_GET_RP_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, totalRecords, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Process),
                           Convert.ToString(uc.Role),
                           ((uc.IsMobileMenu != true) ? GenerateHTMLCheckboxWithEvent(Convert.ToInt32(uc.Id),"check",false, "UpdateisActive(this,"+uc.Id+")", true, "clsCheckbox"): GenerateHTMLCheckboxWithEvent(Convert.ToInt32(uc.Id),"check",true, "UpdateisActive(this,"+uc.Id+")", true, "clsCheckbox")),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.CreatedOn),
                           Convert.ToString(uc.Id),
                            Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecords.Value.ToString() == "" ? 0 : totalRecords.Value,
                    iTotalDisplayRecords = totalRecords.Value.ToString() == "" ? 0 : totalRecords.Value,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveFullkit(int Id, bool IsMobileMenu)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objATH003 = db.ATH003.Where(x => x.Id == Id).FirstOrDefault();
                if (objATH003 != null)
                {
                    objATH003.IsMobileMenu = IsMobileMenu;
                    objATH003.EditedBy = objClsLoginInfo.UserName;
                    objATH003.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public List<ATH003> GetAllRPMatrixdata(string SearchParameter, int StartIndex, int EndIndex)
        {
            List<ATH003> List = db.ATH003.ToList();
            if (EndIndex == -1)
                return List.Skip(StartIndex).ToList();
            else
                return List.Skip(StartIndex).Take(EndIndex).ToList();
        }
        [HttpPost]
        public ActionResult SaveRPMatrix(IEMQS.Areas.Authorization.Models.CustModal ath003, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                var isvalid = db.ATH003.Any(x => x.RoleId == ath003.Role && x.ProcessId == ath003.Process && x.IsActive == true);

                if (isvalid == false)
                {
                    if (ath003.ID > 0)
                    {
                        ATH003 objATH003 = db.ATH003.Where(x => x.Id == ath003.ID).FirstOrDefault();
                        objATH003.RoleId = ath003.Role;
                        objATH003.ProcessId = ath003.Process;
                        objATH003.IsActive = true;
                        objATH003.IsMobileMenu = ath003.IsMobileMenu;
                        objATH003.EditedBy = objClsLoginInfo.UserName;
                        objATH003.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                    }
                    else
                    {
                        ATH003 objATH003 = new ATH003();
                        objATH003.RoleId = ath003.Role;
                        objATH003.ProcessId = ath003.Process;
                        objATH003.IsActive = true;
                        objATH003.IsMobileMenu = ath003.IsMobileMenu;
                        objATH003.CreatedBy = objClsLoginInfo.UserName;
                        objATH003.CreatedOn = DateTime.Now;
                        db.ATH003.Add(objATH003);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public bool isvalidData(int roleid, int processid)
        {
            return true;
        }

        [HttpPost]
        public ActionResult DeleteRPMatrix(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ATH003 objATH003 = db.ATH003.Where(x => x.Id == Id).FirstOrDefault();
                objATH003.IsActive = false;
                objATH003.EditedBy = objClsLoginInfo.UserName;
                objATH003.EditedOn = DateTime.Now;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDescriptionProcess(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objATH003 = db.ATH002.Where(x => x.Id == Id).FirstOrDefault().Description;
                objResponseMsg.Key = true;
                objResponseMsg.Value = objATH003;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult LoadProcessWiseRoleMatrixData(JQueryDataTableParamModel param)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (Process like '%" + param.sSearch
                        + "%' or Role like '%" + param.sSearch
                        + "%' or a.CreatedBy like '%" + param.sSearch
                        + "%' or d.t_name like '%" + param.sSearch
                        + "%' or a.CreatedOn like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }

                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_ATH_GET_RP_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, totalRecords, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Process),
                           Convert.ToString(uc.Role),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.CreatedOn),
                           Convert.ToString(uc.Id),
                            Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecords.Value,
                    iTotalDisplayRecords = totalRecords.Value,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult LoadProcessWiseRolesData(JQueryDataTableParamModel param)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                //var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += string.Format(" and (Process like '%{0}%' or Roles like '%{0}%' or Description like '%{0}%')", param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_ATH_GET_RP_PROCESSWISE
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.ProcessId),
                            Convert.ToString(uc.Process),
                           Convert.ToString(uc.Description),
                           Convert.ToString(uc.Roles),
                           }).ToList();
                var totalCount = lstResult.FirstOrDefault() != null ? lstResult.FirstOrDefault().TotalCount.GetValueOrDefault() : 0;
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalCount,
                    iTotalDisplayRecords = totalCount,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadRolesData(JQueryDataTableParamModel param)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                //var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += string.Format(" and (Role like '%{0}%' or Description like '%{0}%' or RoleGroup like '%{0}%')", param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_ATH_GETROLES
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.Id),
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Role),
                               Convert.ToString(uc.Description),
                               Convert.ToString(uc.RoleGroup),
                           }).ToList();
                var totalCount = lstResult.FirstOrDefault() != null ? lstResult.FirstOrDefault().TotalCount.GetValueOrDefault() : 0;
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalCount,
                    iTotalDisplayRecords = totalCount,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult LoadEmployeeDetails(string Role)
        {
            ViewBag.Role = Role;
            return PartialView("_LoadEmployeeDetails");
        }
        [HttpPost]
        public JsonResult LoadRoleWiseEmployeeData(JQueryDataTableParamModel param, string Role)
        {
            try
            {
                IEnumerable<ATH001> _List = new List<ATH001>();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                //var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (t_psno like '%" + param.sSearch
                        + "%' or t_name like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere = "a4.Role='" + Role + "'";
                }

                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_ATH_GET_RP_ROLEWISE_EMPLOYEE
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.t_psno),
                                Convert.ToString(uc.t_name)
                           }).ToList();

                var totalCount = lstResult.FirstOrDefault() != null ? lstResult.FirstOrDefault().TotalCount.GetValueOrDefault() : 0;
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalCount,
                    iTotalDisplayRecords = totalCount,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                    var lst = db.SP_ATH_GET_RP_DETAILS(1, int.MaxValue, strSortOrder, totalRecords, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Process = li.Process,
                                      Role = li.Role,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.ProcessWiseRoles.GetStringValue())
                {
                    var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                    var lst = db.SP_ATH_GET_RP_PROCESSWISE(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Process = li.Process,
                                      Description = li.Description,
                                      Roles = li.Roles
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.ProcessRoleEmployee.GetStringValue())
                {
                    var lst = db.SP_ATH_GET_RP_ROLEWISE_EMPLOYEE(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      PSNo = li.t_psno,
                                      Name = li.t_name
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.GetAllRole.GetStringValue())
                {
                    var lst = db.SP_ATH_GETROLES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Role = li.Role,
                                      Description = li.Description,
                                      RoleGroup = li.RoleGroup
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion


        [NonAction]
        public static string GenerateHTMLCheckboxWithEvent(int rowId, string columnName, bool columnValue = false, string onClickMethod = "", bool isEnabled = true, string ClassName = "", bool isDisabled = false, string check = "")
        {
            string htmlControl = "";
            string inputID = columnName + "" + rowId.ToString();
            string inputValue = columnValue ? "Checked='Checked'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            string cssClass = !string.IsNullOrEmpty(ClassName) ? "class='" + ClassName + "'" : "";

            if (isDisabled)
            {
                if (check != string.Empty && check != null)
                {
                    htmlControl = "<input type='checkbox' id='" + inputID + "' " + inputValue + " name='" + inputID + "' colname='" + columnName + "' disabled='disabled " + cssClass + " />";
                }
                else
                {
                    htmlControl = "<input type='checkbox' id='" + inputID + "' name='" + inputID + "' colname='" + columnName + "' disabled='disabled " + cssClass + " />";
                }
            }
            else
            {
                if (isEnabled)
                {
                    htmlControl = "<input type='checkbox' id='" + inputID + "' " + inputValue + " name='" + inputID + "' colname='" + columnName + "' " + onClickEvent + " " + (!isEnabled ? "disabled='disabled'" : "") + " " + cssClass + " />";
                }
                else
                {
                    htmlControl = columnValue ? "" : "";
                }
            }
            return htmlControl;
        }

    }
}