﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class WelderLocationController : clsBase
    {
       
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            ViewBag.lstTableColumnDetails = Manager.GetTableColumnDetails("WQR008");
            ViewBag.lstLocation = GetAllLocation().Data;
            return View();
        }

        [HttpPost]
        public ActionResult getData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                int CategoryId = Convert.ToInt32(param.CategoryId);
                string strWhere = " 1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Location like '%" + param.sSearch
                         + "%' or BU like '%" + param.sSearch
                         + "%' or EU like '%" + param.sSearch
                         + "%' or Project like '%" + param.sSearch
                         + "%' or Seam like '%" + param.sSearch
                         + "%' or Description like '%" + param.sSearch
                        + "%' or WQRSeries like '" + param.sSearch+"')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Sorting
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstResult = db.SP_ATH_GET_WELDERLOCATION( StartIndex, EndIndex, strSortOrder, strWhere ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {

                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.EU),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.ParentLocation),
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.Seam),
                           Convert.ToString(uc.Description),
                           Convert.ToString(uc.WQRSeries),
                           Helper.GenerateActionIcon(uc.Id,"btnUpdate","Update Record","fa fa-edit","UpdateRecord(" + uc.Id+ ")")
                           +Helper.GenerateActionIcon(uc.Id,"btnDelete","Delete Record","fa fa-trash","DeleteRecord(" + uc.Id+ ")"),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0,
                    iTotalDisplayRecords = lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetDataHtml(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var wqrdata = db.WQR008.FirstOrDefault(x => x.Id == Id);
                objResponseMsg.Key = true;
                objResponseMsg.Value = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(wqrdata);
            }
            catch (Exception)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error while fetching data";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveWelderLocation(WQR008 wqr008)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (wqr008.Id > 0)
                {
                    var chklist = db.WQR008.FirstOrDefault(x => x.Id == wqr008.Id);
                    if (chklist != null)
                    {
                        chklist.Location = wqr008.Location;
                        chklist.EU = wqr008.EU;
                        chklist.BU = wqr008.BU;
                        chklist.WQRSeries = wqr008.WQRSeries;
                        chklist.Project = wqr008.Project;
                        chklist.Seam = wqr008.Seam;
                        chklist.Description = wqr008.Description;
                        chklist.ParentLocation = wqr008.ParentLocation;
                        chklist.EditedBy = objClsLoginInfo.UserName;
                        chklist.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }  

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {
                    WQR008 objWQR008 = new WQR008();
                    /*var chklist = db.CTQ003.Where(x => x.BU == wqr008.BU && x.Location == wqr008.Location).FirstOrDefault();

                    if (chklist != null)
                    {
                        chklist.BU = wqr008.BU;
                        chklist.Location = wqr008.Location;
                        chklist.EditedBy = objClsLoginInfo.UserName;
                        chklist.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    else
                    {*/
                    objWQR008.Location = wqr008.Location;
                    objWQR008.EU = wqr008.EU;
                    objWQR008.BU = wqr008.BU;
                    objWQR008.WQRSeries = wqr008.WQRSeries;
                    objWQR008.Project = wqr008.Project;
                    objWQR008.Seam = wqr008.Seam;
                    objWQR008.Description = wqr008.Description;
                    objWQR008.ParentLocation = wqr008.ParentLocation;
                    objWQR008.CreatedBy = objClsLoginInfo.UserName;
                    objWQR008.CreatedOn = DateTime.Now;
                   // }
                    
                     db.WQR008.Add(objWQR008);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Delete(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                WQR008 objWQR008 = db.WQR008.FirstOrDefault(x => x.Id == Id);
                db.WQR008.Remove(objWQR008);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetAllLocation()
        {
            int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
            var lstLocations = Manager.getBUsByID(LOC);

            try
            {
                var items = (from li in lstLocations
                             select new
                             {
                                 id = li.t_dimx,
                                 value = li.t_dimx,
                                 label = li.t_dimx+"-"+li.t_desc,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        #region Export Excell
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_ATH_GET_WELDERLOCATION(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      BU=li.BU,
                                      EU=li.EU,
                                      Location= li.Location,
                                      ParentLocation=li.ParentLocation,
                                      Project=li.Project,
                                      Seam=li.Seam,
                                      Description =li.Description,
                                      WQRSeries =li.WQRSeries,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}