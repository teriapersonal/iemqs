﻿using IEMQS.Areas.Authorization.Models;
using IEMQS.Models;
using IEMQSImplementation;
using IEMQSImplementation.Models;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationEnum;

namespace IEMQS.Areas.Authorization.Controllers
{
    public class MenuConfigurationController : clsBase
    {
        // GET: Authorization/MenuConfiguration
        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }
        #region Project Tracker

        [SessionExpireFilter]
        public ActionResult ProjectTracker()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetMenuNodes(string Project)
        {
            List<DiagramNodes> Nodes = new List<DiagramNodes>();
            var lstMenuResult = db.SP_ATH_GET_USER_ACCESS_MENU(objClsLoginInfo.UserName).ToList();
            SP_ATH_GET_USER_ACCESS_MENU_Result Menu;
            var lstMenuFKMS = lstMenuResult.Where(w => w.Area == "FKMS");
            if (lstMenuFKMS.Any())
            {
                Menu = lstMenuFKMS.FirstOrDefault(f => f.ParentId == 0);
                Nodes.Add(new DiagramNodes(Menu));
                var fkm101 = db.FKM101.FirstOrDefault(w => w.Project == Project);
                if(fkm101 != null)
                {
                    Menu = lstMenuFKMS.FirstOrDefault(w => w.Controller == "MaintainFKMS" && w.Action == "Index");
                    if(Menu != null)
                        Nodes.Add(new DiagramNodes(Menu, "/FKMS/MaintainFKMS/FKMSDetails/{0}?urlForm=m", fkm101.HeaderId));
                    Menu = lstMenuFKMS.FirstOrDefault(w => w.Controller == "MaintainFKMS" && w.Action == "ApproverIndex");
                    if (Menu != null)
                        Nodes.Add(new DiagramNodes(Menu, "/FKMS/MaintainFKMS/FKMSDetails/{0}?urlForm=a", fkm101.HeaderId));
                    Menu = lstMenuFKMS.FirstOrDefault(w => w.Controller == "MaintainFKMS" && w.Action == "DisplayFKMS");
                    if (Menu != null)
                        Nodes.Add(new DiagramNodes(Menu, "/FKMS/MaintainFKMS/FKMSDisplayDetails/{0}?urlForm=m", fkm101.HeaderId));
                }

                var fkm111 = db.FKM111.FirstOrDefault(w => w.Project == Project);
                if (fkm111 != null)
                {
                    Menu = lstMenuFKMS.FirstOrDefault(w => w.Controller == "MaintainFR" && w.Action == "Index");
                    if (Menu != null)
                        Nodes.Add(new DiagramNodes(Menu, "/FKMS/MaintainFR/FRDetails/{0}?urlForm=m", fkm111.FKM114.HeaderId));
                    Menu = lstMenuFKMS.FirstOrDefault(w => w.Controller == "MaintainFR" && w.Action == "ApproverIndex");
                    if (Menu != null)
                        Nodes.Add(new DiagramNodes(Menu, "/FKMS/MaintainFR/FRDetails/{0}?urlForm=a", fkm111.FKM114.HeaderId));
                    Menu = lstMenuFKMS.FirstOrDefault(w => w.Controller == "MaintainFR" && w.Action == "ShopIndex");
                    if (Menu != null)
                        Nodes.Add(new DiagramNodes(Menu, "/FKMS/MaintainFR/FRDetails/{0}?urlForm=r", fkm111.FKM114.HeaderId));
                }
            } 

            /*var lstMenuMIP = lstMenuResult.Where(w => w.Area == "MIP");
            if (lstMenuMIP.Any())
            {
                Menu = lstMenuMIP.FirstOrDefault(f => f.ParentId == 0);
                Nodes.Add(new DiagramNodes(Menu.ProcessId, 0, Menu.Process, Menu.Description));
                var mip001 = db.MIP001.FirstOrDefault(w => w.Project == Project);
                if (mip001 != null)
                {
                    Menu = lstMenuMIP.FirstOrDefault(w => w.Controller == "MaintainMIP" && w.Action == "Index");
                    if (Menu != null)
                        Nodes.Add(new DiagramNodes(Menu, "/MIP/MaintainMIP/Details/{0}?urlForm=m", mip001.HeaderId));
                    var Menu = lstMenuMIP.FirstOrDefault(w => w.Controller == "MaintainMIP" && w.Action == "Approve");
                    if (Menu != null)
                        Nodes.Add(new DiagramNodes(Menu, "/MIP/MaintainMIP/Details/{0}?urlForm=a", mip001.HeaderId));
                }

                var mip003 = db.MIP003.FirstOrDefault(w => w.Project == Project);
                if (mip003 != null)
                {
                    if (mip003.Status == MIPHeaderStatus.Returned.GetStringValue() || mip003.Status == MIPHeaderStatus.Draft.GetStringValue())
                        Nodes.Add(new DiagramNodes(i++, MIP, "MIP Maintain", "Maintain MIP", "/MIP/MaintainMIPHeader/Details/{0}?urlForm=m", mip003.HeaderId));
                    else if (mip003.Status == MIPHeaderStatus.SentForApproval.GetStringValue())
                        Nodes.Add(new DiagramNodes(i++, MIP, "MIP Approval", "Approve MIP", "/MIP/MaintainMIPHeader/Details/{0}?urlForm=a", mip003.HeaderId));
                    else if (mip003.Status == MIPHeaderStatus.Approved.GetStringValue())
                        Nodes.Add(new DiagramNodes(i++, MIP, "MIP Master", "Display MIP", "/MIP/MaintainMIPHeader/Details/{0}", mip003.HeaderId));
                }
                var mip004 = db.MIP004.FirstOrDefault(w => w.QualityProject == Project);
                if (mip004 != null)
                {
                    if (mip004.InspectionStatus == MIPInspectionStatus.ReadyToOffer.GetStringValue() || mip004.InspectionStatus == MIPInspectionStatus.Returned.GetStringValue())
                        Nodes.Add(new DiagramNodes(i++, MIP, "MIP Ready To Offer", "Ready To Offer MIP", "/MIP/OfferOperation/OfferOpearationDetails/{0}", mip004.LineId));
                    else if (mip004.InspectionStatus == MIPInspectionStatus.UnderInspection.GetStringValue())
                    {
                        var mip050 = db.MIP050.FirstOrDefault(w => w.RequestNo == mip004.RequestNo && w.RequestNoSequence == mip004.RequestSequenceNo);
                        if (mip050 != null)
                            Nodes.Add(new DiagramNodes(i++, MIP, "MIP Ready To Offer", "Ready To Offer MIP", "/MIP/OfferOperation/OfferOpearationDetails/{0}", mip050.RequestId));
                    }
                    else if (mip004.InspectionStatus == MIPInspectionStatus.Cleared.GetStringValue())
                        Nodes.Add(new DiagramNodes(i++, MIP, "MIP Cleared", "Cleared MIP", "/MIP/OfferOperation/OfferOpearationDetails/{0}", mip004.LineId));

                }
            }*/
            
            

            return Json(Nodes, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult GetMenuNodes2(string Project)
        {
            List<DiagramNodes> Nodes = new List<DiagramNodes>();
            int i = 1;
            int FKMS = i++;
            int MIP = i++;

            var fkm101 = db.FKM101.FirstOrDefault(w => w.Project == Project);
            if (fkm101 != null)
            {
                if (fkm101.Status == PAMStatus.Returned.GetStringValue() || fkm101.Status == PAMStatus.Draft.GetStringValue())
                    Nodes.Add(new DiagramNodes(i++, FKMS, "FKMS Maintain", "Maintain FKMS", "/FKMS/MaintainFKMS/FKMSDetails/{0}?urlForm=m", fkm101.HeaderId));
                else if (fkm101.Status == PAMStatus.SentForApproval.GetStringValue())
                    Nodes.Add(new DiagramNodes(i++, FKMS, "FKMS Approval", "Approve FKMS", "/FKMS/MaintainFKMS/FKMSDetails/{0}?urlForm=a", fkm101.HeaderId));
                Nodes.Add(new DiagramNodes(i++, FKMS, "FKMS Display", "Display FKMS", "/FKMS/MaintainFKMS/FKMSDisplayDetails/{0}?urlForm=m", fkm101.HeaderId));
            }

            var fkm111 = db.FKM111.FirstOrDefault(w => w.Project == Project);
            if (fkm111 != null)
            {
                if (fkm111.Status == FRStatus.Returned.GetStringValue() || fkm111.Status == FRStatus.Draft.GetStringValue())
                    Nodes.Add(new DiagramNodes(i++, FKMS, "FR Maintain", "Maintain Fixture Requirement", "/FKMS/MaintainFR/FRDetails/{0}?urlForm=m", fkm111.FKM114.HeaderId));
                else if (fkm111.Status == FRStatus.SentForApproval.GetStringValue())
                    Nodes.Add(new DiagramNodes(i++, FKMS, "FR Approval", "Approve Fixture Requirement", "/FKMS/MaintainFR/FRDetails/{0}?urlForm=a", fkm111.FKM114.HeaderId));
                else if (fkm111.Status == FRStatus.Approved.GetStringValue())
                    Nodes.Add(new DiagramNodes(i++, FKMS, "FR Shop", "Excute Fixture Requirement", "/FKMS/MaintainFR/FRDetails/{0}?urlForm=r", fkm111.FKM114.HeaderId));
            }
            if(fkm101 != null || fkm111 != null)
                Nodes.Insert(0,new DiagramNodes(FKMS, 0, "FKMS", "Full Kit Management System"));

            var mip001 = db.MIP001.FirstOrDefault(w => w.Project == Project);
            if (mip001 != null)
            {
                if (mip001.Status == MIPHeaderStatus.Returned.GetStringValue() || mip001.Status == MIPHeaderStatus.Draft.GetStringValue())
                    Nodes.Add(new DiagramNodes(i++, MIP, "MIP Master Maintain", "Maintain MIP Master", "/MIP/MaintainMIP/Details/{0}?urlForm=m", mip001.HeaderId));
                else if (mip001.Status == MIPHeaderStatus.SentForApproval.GetStringValue())
                    Nodes.Add(new DiagramNodes(i++, MIP, "MIP Master Approval", "Approve MIP Master", "/MIP/MaintainMIP/Details/{0}?urlForm=a", mip001.HeaderId));
                else if (mip001.Status == MIPHeaderStatus.Approved.GetStringValue())
                    Nodes.Add(new DiagramNodes(i++, MIP, "MIP Master Display", "Display MIP Master", "/MIP/MaintainMIP/Details/{0}", mip001.HeaderId));
            }

            var mip003 = db.MIP003.FirstOrDefault(w => w.Project == Project);
            if (mip003 != null)
            {
                if (mip003.Status == MIPHeaderStatus.Returned.GetStringValue() || mip003.Status == MIPHeaderStatus.Draft.GetStringValue())
                    Nodes.Add(new DiagramNodes(i++, MIP, "MIP Maintain", "Maintain MIP", "/MIP/MaintainMIPHeader/Details/{0}?urlForm=m", mip003.HeaderId));
                else if (mip003.Status == MIPHeaderStatus.SentForApproval.GetStringValue())
                    Nodes.Add(new DiagramNodes(i++, MIP, "MIP Approval", "Approve MIP", "/MIP/MaintainMIPHeader/Details/{0}?urlForm=a", mip003.HeaderId));
                else if (mip003.Status == MIPHeaderStatus.Approved.GetStringValue())
                    Nodes.Add(new DiagramNodes(i++, MIP, "MIP Master", "Display MIP", "/MIP/MaintainMIPHeader/Details/{0}", mip003.HeaderId));
            }
            var mip004 = db.MIP004.FirstOrDefault(w => w.QualityProject == Project);
            if (mip004 != null)
            {
                if (mip004.InspectionStatus == MIPInspectionStatus.ReadyToOffer.GetStringValue() || mip004.InspectionStatus == MIPInspectionStatus.Returned.GetStringValue())
                    Nodes.Add(new DiagramNodes(i++, MIP, "MIP Ready To Offer", "Ready To Offer MIP", "/MIP/OfferOperation/OfferOpearationDetails/{0}", mip004.LineId));
                else if (mip004.InspectionStatus == MIPInspectionStatus.UnderInspection.GetStringValue())
                {
                    var mip050 = db.MIP050.FirstOrDefault(w => w.RequestNo == mip004.RequestNo && w.RequestNoSequence == mip004.RequestSequenceNo);
                    if(mip050 != null)
                        Nodes.Add(new DiagramNodes(i++, MIP, "MIP Ready To Offer", "Ready To Offer MIP", "/MIP/OfferOperation/OfferOpearationDetails/{0}", mip050.RequestId));
                }
                else if (mip004.InspectionStatus == MIPInspectionStatus.Cleared.GetStringValue())
                    Nodes.Add(new DiagramNodes(i++, MIP, "MIP Cleared", "Cleared MIP", "/MIP/OfferOperation/OfferOpearationDetails/{0}", mip004.LineId));

            }
            if (mip001 != null || mip003 != null)
                Nodes.Add(new DiagramNodes(MIP, 0,"MIP", "Manufacturing & Inspaction Plan"));

            return Json(Nodes, JsonRequestBehavior.AllowGet);
        }

        #endregion

        [HttpPost]
        public JsonResult LoadMenuConfigurationData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                //strWhere = "MainMenuOrderNo is not null and ParentId is null and IsDisplayInMenu=1";
                strWhere = " IsDisplayInMenu=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " and (Process like '%" + param.sSearch
                        + "%' or [Description] like '%" + param.sSearch
                        + "%' or MainMenuOrderNo like '%" + param.sSearch
                        + "%')";

                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstATH002 = db.SP_ATH_GET_MENU_CONFIGURATION_DETAILS
                                (
                                StartIndex, 0, strSortOrder, strWhere
                                ).ToList();
                int parentIndex = 0, childIndex = 0, previousParentId = 0;
                var data = (from uc in lstATH002
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           (uc.ParentId == 0 || uc.ParentId==null) ? "<nobr><span class='fa fa-chevron-right' onclick='ExpandCollapsChild("+ uc.Id +", this)'></span></nobr>" : "<span class='"+ uc.ParentId +" child' ></span>",
                           "<span class='reorder editable "+((uc.ParentId == 0 || uc.ParentId==null) ? "":"childmenus")+" ' data-col='process' data-maxlength='100'>"+Convert.ToString(uc.Process) + "</span>",
                           "<span class='editable ' data-col='description' data-maxlength='100'>"+Convert.ToString(uc.Description)+ "</span>",
                            (uc.ParentId == 0 || uc.ParentId==null) ? Convert.ToString(uc.MainMenuOrder):Convert.ToString(uc.SubMenuOrder),
                           uc.ParentId == 0 ? Helper.GenerateActionIcon(uc.Id.Value, "Help", "Click to Update Content", "fa fa-question-circle", "OpenHelpPopup("+ uc.Id.Value +")"): "",
                            uc.ParentId == 0 ? Helper.GenerateActionIcon(uc.Id.Value, "FAQ", "Click to Update Content", "fa fa-commenting", "OpenFAQPopup("+ uc.Id.Value +")"): "",
                        uc.ParentId == 0 ? Helper.GenerateActionIcon(uc.Id.Value, "Location", "Click to Update Content", "icon-pointer ", "OpenLocationPopup("+ uc.Id.Value +")"): "",

                            (uc.ParentId == 0 || uc.ParentId==null) ? "true":"false", //IsParent
                           //(uc.ParentId == 0 || uc.ParentId==null) ?(++parentIndex).ToString():(++childIndex).ToString(),
                           (uc.ParentId == 0 || uc.ParentId==null) ?(++parentIndex).ToString():GetChildIndexNo(ref previousParentId,Convert.ToInt32(uc.ParentId),ref childIndex),
                           uc.ParentId.ToString()
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstATH002.Count > 0 && lstATH002.FirstOrDefault().TotalCount > 0 ? lstATH002.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstATH002.Count > 0 && lstATH002.FirstOrDefault().TotalCount > 0 ? lstATH002.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetChildIndexNo(ref int previousParentId, int currentParentId, ref int currentChildIndex)
        {
            if (previousParentId == currentParentId)
            {
                currentChildIndex = currentChildIndex + 1;
            }
            else
            {
                previousParentId = currentParentId;
                currentChildIndex = 1;
            }
            return currentChildIndex.ToString();
        }

        [HttpPost]
        public ActionResult SaveMainMenuOrder(int sourceid, int sourceorder, int destinationid, int desctinationorder)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                #region Swaping Menu Order
                var objSrcATH002 = db.ATH002.Where(i => i.Id == sourceid).FirstOrDefault();
                var objDescATH002 = db.ATH002.Where(i => i.Id == destinationid).FirstOrDefault();
                if (objSrcATH002 != null)
                {
                    objSrcATH002.MainMenuOrderNo = desctinationorder;
                }
                if (objDescATH002 != null)
                {
                    objDescATH002.MainMenuOrderNo = sourceorder;
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Menu re-order successfully.";
                #endregion

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateMenuName(int Id, string MenuName, string colName)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objATH002 = db.ATH002.Where(i => i.Id == Id).FirstOrDefault();
                if (objATH002 != null)
                {
                    if (colName.ToLower() == "process")
                    {
                        objATH002.Process = MenuName;
                    }
                    else if (colName.ToLower() == "description")
                    {
                        objATH002.Description = MenuName;
                    }
                    objATH002.EditedBy = objClsLoginInfo.UserName;
                    objATH002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Menu update successfully.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Menu update successfully.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult MenuOrderReserial(int sourceorder, int destinationorder, bool isparent)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (isparent)
                {
                    List<ATH002> listATH002 = null;
                    if (destinationorder > sourceorder)
                    {
                        listATH002 = db.ATH002.Where(i => i.MainMenuOrderNo != null && i.MainMenuOrderNo <= destinationorder && i.MainMenuOrderNo >= sourceorder && i.IsDisplayInMenu == true).ToList();
                        foreach (var item in listATH002)
                        {
                            if (item.MainMenuOrderNo == sourceorder)
                            {
                                item.MainMenuOrderNo = destinationorder;
                            }
                            else
                            {
                                item.MainMenuOrderNo = item.MainMenuOrderNo - 1;
                            }
                            item.EditedBy = objClsLoginInfo.UserName;
                            item.EditedOn = DateTime.Now;
                        }
                        db.SaveChanges();
                    }
                    else
                    {
                        listATH002 = db.ATH002.Where(i => i.MainMenuOrderNo != null && i.MainMenuOrderNo >= destinationorder && i.MainMenuOrderNo <= sourceorder && i.IsDisplayInMenu == true).ToList();
                        foreach (var item in listATH002)
                        {
                            if (item.MainMenuOrderNo == sourceorder)
                            {
                                item.MainMenuOrderNo = destinationorder;
                            }
                            else
                            {
                                item.MainMenuOrderNo = item.MainMenuOrderNo + 1;
                            }
                            item.EditedBy = objClsLoginInfo.UserName;
                            item.EditedOn = DateTime.Now;
                        }
                        db.SaveChanges();
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Menu re-order successfully.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SubMenuOrderReserial(int sourceid, int sourceorder, int destinationorder, int sourceparentid, int destinationparentid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (sourceparentid == destinationparentid)
                {
                    List<ATH002> listATH002 = null;
                    if (destinationorder > sourceorder)
                    {
                        listATH002 = db.ATH002.Where(i => i.OrderNo != null && i.OrderNo <= destinationorder && i.OrderNo >= sourceorder && i.IsDisplayInMenu == true && i.ParentId == sourceparentid).ToList();
                        foreach (var item in listATH002)
                        {
                            if (item.OrderNo == sourceorder)
                            {
                                item.OrderNo = destinationorder;
                            }
                            else
                            {
                                item.OrderNo = item.OrderNo - 1;
                            }
                            item.EditedBy = objClsLoginInfo.UserName;
                            item.EditedOn = DateTime.Now;
                        }
                        db.SaveChanges();
                    }
                    else
                    {
                        listATH002 = db.ATH002.Where(i => i.OrderNo != null && i.OrderNo >= destinationorder && i.OrderNo <= sourceorder && i.IsDisplayInMenu == true && i.ParentId == sourceparentid).ToList();
                        foreach (var item in listATH002)
                        {
                            if (item.OrderNo == sourceorder)
                            {
                                item.OrderNo = destinationorder;
                            }
                            else
                            {
                                item.OrderNo = item.OrderNo + 1;
                            }
                            item.EditedBy = objClsLoginInfo.UserName;
                            item.EditedOn = DateTime.Now;
                        }
                        db.SaveChanges();
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Menu re-order successfully.";
                }
                else
                {
                    var objATH002 = db.ATH002.Where(i => i.Id == sourceid).FirstOrDefault();
                    if (objATH002 != null)
                    {
                        objATH002.ParentId = destinationparentid;
                        objATH002.EditedBy = objClsLoginInfo.UserName;
                        objATH002.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Menu re-order successfully.";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region User Wise Menu (observation 17500)
        [HttpPost]
        public ActionResult MenuOrderWiseUser(int sourceorder, int destinationorder)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ATH011[] listATH011 = null;
                if (sourceorder < destinationorder)
                    listATH011 = db.ATH011.Where(x => x.OrderNo >= sourceorder && x.OrderNo <= destinationorder && x.Employee == objClsLoginInfo.UserName).OrderBy(p => p.OrderNo).ToArray();
                else
                    listATH011 = db.ATH011.Where(x => x.OrderNo <= sourceorder && x.OrderNo >= destinationorder && x.Employee == objClsLoginInfo.UserName).OrderByDescending(p => p.OrderNo).ToArray();

                for (int i = listATH011.Length - 1; i > 0; i--)
                {
                    int? temp = listATH011[i].OrderNo;
                    listATH011[i].OrderNo = listATH011[i - 1].OrderNo;
                    listATH011[i - 1].OrderNo = temp;
                    listATH011[i].EditedBy = objClsLoginInfo.UserName;
                    listATH011[i].EditedOn = DateTime.Now;
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Menu re-order successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public JsonResult GetUserAccessMenu()
        {
            try
            {
                var lstATH002 = db.SP_ATH_GET_USER_ACCESS_MENU
                               (
                               objClsLoginInfo.UserName
                               ).ToList();
                return Json(new
                {
                    Key = true,
                    data = lstATH002
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    Key = false,
                    data = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult CurrentStateHelpPartial(string currentState)
        {
            MenuConfiguration obj = new MenuConfiguration();
            int index1 = currentState.IndexOf(WebsiteURL);
            if (index1 != -1)
            {
                string result = currentState.Replace(WebsiteURL + "/", "");
                string area = result.Split('/')[0];
                ATH002 objATH002 = db.ATH002.Where(x => x.Area == area).FirstOrDefault();
                if (objATH002 != null)
                {
                    obj.ContentBody = objATH002.Help;
                }
            }
            return PartialView("~/Views/Shared/_HelpPartial.cshtml", obj);
        }
        [HttpPost]
        public ActionResult CurrentStateFAQPartial(string currentState)
        {
            MenuConfiguration obj = new MenuConfiguration();
            int index1 = currentState.IndexOf(WebsiteURL);
            if (index1 != -1)
            {
                string result = currentState.Replace(WebsiteURL + "/", "");
                string area = result.Split('/')[0];
                ATH002 objATH002 = db.ATH002.Where(x => x.Area == area).FirstOrDefault();
                if (objATH002 != null)
                {
                    obj.ContentBody = objATH002.FAQ;
                }
            }
            return PartialView("~/Views/Shared/_FAQPartial.cshtml", obj);
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult HelpDetailsPartial(int? Id)
        {
            MenuConfiguration obj = new MenuConfiguration();
            int id = Convert.ToInt32(Id);
            obj.ATH002 = db.ATH002.Where(m => m.Id == id).SingleOrDefault();
            if (obj != null)
            {
                obj.ContentBody = obj.ATH002.Help;
            }

            return PartialView("_HelpDetailsPartial", obj);
        }


        //[SessionExpireFilter]
        //[HttpPost]
        //public ActionResult MenuLocationsData(int? id)
        //{
        //    //ATH010 objATH010 = new ATH010();
        //    //return View(objATH010);
        //    return PartialView("_LocationsPartial");
        //}
        public ActionResult FAQDetailsPartial(int? Id)
        {
            MenuConfiguration obj = new MenuConfiguration();

            int id = Convert.ToInt32(Id);
            obj.ATH002 = db.ATH002.Where(m => m.Id == id).SingleOrDefault();
            if (obj != null)
            {
                obj.ContentBody = obj.ATH002.FAQ;
            }
            return PartialView("_FAQDetailsPartial", obj);
        }

        #region login as different user
        public ActionResult LoginWithDifferentUser(int? Id)
        {
            MenuConfiguration obj = new MenuConfiguration();

            return PartialView("_LoginWithDifferentUserPartial", obj);
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        [HttpPost]
        public ActionResult RedirecttoLoginAs(string LoginAs)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                if (!string.IsNullOrWhiteSpace(LoginAs))
                {
                    
                    var Msg = "";
                    Manager.SetLoginSessionDetails(LoginAs, ref Msg, clsImplementationEnum.UserActionType.LoginAs);
                    if (Msg.Length > 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = Msg;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.AuthenticationMessages.UsetNotExist.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        [HttpPost]
        public ActionResult LocationsPartial(int Id)
        {
            string selectedLocations = string.Empty;
            var objATH010 = db.ATH010.Where(x => x.MenuId == Id).ToList();
            if (objATH010.Count > 0)
            {
                selectedLocations = string.Join(",", objATH010.Select(x => x.Location).ToArray());
            }
            ViewBag.lstLoca = selectedLocations;
            return PartialView("_LocationsPartial");
        }

        [HttpPost]
        public ActionResult LoadLocationData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
                var lstResult = Manager.getLocByID(LOC).Where(x => !string.IsNullOrEmpty(x.t_dimx)).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.t_dimx),
                                Convert.ToString(uc.t_desc),
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.Count > 0 ? lstResult.Count : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.Count > 0 ? lstResult.Count : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult SaveHelpData(MenuConfiguration MenuConfiguration)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            ATH002 myobj = db.ATH002.Where(m => m.Id == MenuConfiguration.ATH002.Id).SingleOrDefault();
            try
            {
                if (ModelState.IsValid)
                {
                    if (myobj != null)
                    {
                        myobj.Help = MenuConfiguration.ContentBody;
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.MailMessage.Update;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.MailMessage.Error;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        [ValidateInput(false)]
        public ActionResult SaveFAQData(MenuConfiguration MenuConfiguration)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            ATH002 myobj = db.ATH002.Where(m => m.Id == MenuConfiguration.ATH002.Id).SingleOrDefault();
            try
            {
                if (ModelState.IsValid)
                {
                    if (myobj != null)
                    {
                        myobj.FAQ = MenuConfiguration.ContentBody;
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.MailMessage.Update;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.MailMessage.Error;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveLocationData(string strLocationIds, int ParentId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<ATH010> newObjATH010 = new List<ATH010>();

            int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
            var lstLocations = Manager.getLocByID(LOC).ToList();

            string[] arrayLocationIds = strLocationIds.Split(',').ToArray();
            try
            {
                if (arrayLocationIds.Length >= 0)
                {
                    //remove old data
                    var objATH010 = db.ATH010.Where(x => x.MenuId == ParentId).ToList();
                    if (objATH010.Count > 0)
                    {
                        db.ATH010.RemoveRange(objATH010);
                        db.SaveChanges();
                    }
                    //add new selected data
                    foreach (var item in arrayLocationIds)
                    {
                        newObjATH010.Add(new ATH010
                        {
                            MenuId = ParentId,
                            Location = item,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        });
                    }

                    db.ATH010.AddRange(newObjATH010);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Locations add successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select locations";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.MailMessage.Error;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                List<SP_ATH_GET_MENU_CONFIGURATION_DETAILS_Result> lst = db.SP_ATH_GET_MENU_CONFIGURATION_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  Process = li.Process,
                                  Description = li.Description,
                                  ParentProcess = (li.ParentId != 0) ? lst.Where(i => i.Id == li.ParentId).FirstOrDefault()?.Process : "",
                                  MenuOrder = (li.ParentId == 0 || li.ParentId == null) ? Convert.ToString(li.MainMenuOrder) : Convert.ToString(li.SubMenuOrder),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}