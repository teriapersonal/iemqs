﻿using IEMQSImplementation;
using IEMQSImplementation.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;



namespace IEMQS.Areas.MSW.Models
{
    public class MSW_LNData
    {
      
        public int HeaderId { get; set; }
        public string Project { get; set; }
        public string Document { get; set; }
        public string RevNo { get; set; }
        public string MainTask { get; set; }
        public string ReferenceDrawing { get; set; }
        public string MachineActivity { get; set; }
        public string MachineRequired { get; set; }
        public string Quantity { get; set; }
        public string Location { get; set; }
        public string TaskManager { get; set; }
       public int   TASKUNIQUEID { get; set; }
        public bool isLNData { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

        public int SEQUENCEID { get; set; }
        

    }
}