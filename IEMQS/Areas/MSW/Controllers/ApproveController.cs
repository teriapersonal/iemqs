﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.MSW.Controllers
{
    public class ApproveController : clsBase
    {
        #region Utility
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            //if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            //{
            //    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            //}
            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";

            return htmlControl;
        }

        public static string GenerateTextboxFor(int rowId, string status, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                htmlControl = "<input disabled type='text' id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  />";
            }
            else
            {
                htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";
            }

            return htmlControl;
        }
        #endregion

        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                MSW001 objMSW001 = db.MSW001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objMSW001 != null)
                {
                    objMSW001.RevNo = Convert.ToInt32(objMSW001.RevNo) + 1;
                    objMSW001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objMSW001.ReviseRemark = strRemarks;
                    objMSW001.EditedBy = objClsLoginInfo.UserName;
                    objMSW001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objMSW001.HeaderId, objMSW001.Status, objMSW001.RevNo, objMSW001.Project, objMSW001.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objMSW001.HeaderId;
                    objResponseMsg.Status = objMSW001.Status;
                    objResponseMsg.rev = objMSW001.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [SessionExpireFilter]
        // GET: MSW/Approve
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("MSWApproveGridPartial");
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }

        [HttpPost]
        public ActionResult SaveMSWHeader(MSW001 msw001, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {             

                if (!string.IsNullOrWhiteSpace(fc["hfHeaderId"].ToString()) && Convert.ToInt32(fc["hfHeaderId"].ToString()) > 0)
                {
                    int hid = Convert.ToInt32(fc["hfHeaderId"].ToString());

                    MSW001 objmsw001 = db.MSW001.Where(x => x.HeaderId == hid).FirstOrDefault();
                    string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                    objmsw001.Project = fc["hfProject"].ToString();
                    objmsw001.Document = fc["txtdoc"].ToString();
                    objmsw001.Customer = fc["txtCust"].ToString().Split('-')[0];
                    if (objmsw001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objmsw001.RevNo = Convert.ToInt32(objmsw001.RevNo) + 1;
                        objmsw001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                    }
                    if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                    {
                        objmsw001.CDD = null;
                    }
                    else
                    {
                        objmsw001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                    }
                    objmsw001.Product = fc["ddlCat"].ToString();
                    objmsw001.ProcessLicensor = fc["txtlicence"].ToString();
                    objmsw001.EditedBy = objClsLoginInfo.UserName;
                    objmsw001.EditedOn = DateTime.Now;
                    //objmsw001.ApprovedBy = fc["ddlApprover"].ToString().Split('-')[0].Trim();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                    objResponseMsg.Revision = "R" + objmsw001.RevNo.ToString();
                    objResponseMsg.status = objmsw001.Status;
                    Manager.UpdatePDN002(objmsw001.HeaderId, objmsw001.Status, objmsw001.RevNo, objmsw001.Project, objmsw001.Document);

                }
                else
                {
                    string project = fc["ddlProject"].ToString();
                    project = project.Split('-')[0].Trim();
                    var isvalid = db.MSW001.Any(x => x.Project == project);
                    if (isvalid == false)
                    {
                        string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                        MSW001 objmsw001 = new MSW001();
                        objmsw001.Project = project;
                        objmsw001.Document = fc["txtdoc"].ToString();
                        objmsw001.Customer = fc["txtCust"].ToString().Split('-')[0];
                        objmsw001.RevNo = Convert.ToInt32(rev);
                        objmsw001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                        if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                        {
                            objmsw001.CDD = null;
                        }
                        else
                        {
                            objmsw001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                        }

                        objmsw001.Product = fc["ddlCat"].ToString();
                        objmsw001.ProcessLicensor = fc["txtlicence"].ToString();
                        objmsw001.CreatedBy = objClsLoginInfo.UserName;
                        objmsw001.CreatedOn = DateTime.Now;
                        objmsw001.ApprovedBy = fc["ddlApprover"].ToString().Split('-')[0].Trim();
                        db.MSW001.Add(objmsw001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
                        objResponseMsg.Revision = "R" + objmsw001.RevNo.ToString();
                        objResponseMsg.status = objmsw001.Status;
                        Manager.UpdatePDN002(objmsw001.HeaderId, objmsw001.Status, objmsw001.RevNo, objmsw001.Project, objmsw001.Document);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public JsonResult LoadMSWApproveHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "') and ApprovedBy=" + user+"";
                }
                else
                {
                    strWhere += "1=1";
                }
               /// strWhere += " and ApprovedBy=" + user;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or CDD like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_MSW_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                           Convert.ToString(uc.Document),
                           Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                           Convert.ToString(uc.CDD),
                           Convert.ToString(uc.Product),
                           Convert.ToString(uc.ProcessLicensor),
                           Convert.ToString("R"+uc.RevNo),
                           Convert.ToString(uc.Status),
                         Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult MSWApproveDetail(int Id = 0)
        {
            MSW001 objMSW001 = new MSW001();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
            var project = (from a in db.MSW001
                           where a.HeaderId == Id
                           select a.Project).FirstOrDefault();
            ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv == 1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");
            ViewBag.isPlng3Role = objClsLoginInfo.GetUserRoleList().Any(m => m == clsImplementationEnum.UserRoleName.PLNG3.ToString());
            if (Id > 0)
            {
                objMSW001 = db.MSW001.Where(x => x.HeaderId == Id).FirstOrDefault();
                var PlanningDinID = db.PDN002.Where(x => x.RefId == Id && x.DocumentNo == objMSW001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, Id, clsImplementationEnum.PlanList.Machining_Scope_of_Work.GetStringValue());
                if (objMSW001.ApprovedBy != null)
                {
                    if (objMSW001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
            }
            return View(objMSW001);
        }
        [HttpPost]
        public ActionResult getHeaderId(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int Headerid = (from a in db.MSW001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();
                string status = (from a in db.MSW001
                                 where a.Project == project
                                 select a.Status).FirstOrDefault();
                objResponseMsg.Key = true;
                objResponseMsg.Value = Headerid.ToString() + "|" + status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetHeaderData(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int RevNum;
                project = project.Split('-')[0].ToString().Trim();
                string customer = Manager.GetCustomerCodeAndNameByProject(project);

                var Exists = db.MSW001.Any(x => x.RevNo == null && x.Project == project);
                if (Exists == false)
                {
                    RevNum = 0;
                }
                else
                {
                    string revNo = (from rev in db.MSW001
                                    where rev.Project == project
                                    select rev.Project).FirstOrDefault();
                    RevNum = Convert.ToInt32(revNo) + 1;
                }               
                project = project.Split('-')[0].ToString().Trim();
                var ccdd = Manager.GetContractWiseCdd(project);
                objResponseMsg.Value = customer + "|" + RevNum + "|" + ccdd;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetUpdateView(string project, string document, string RevNo, int lineid, string isln)
        {
            project = project.Split('-')[0].ToString().Trim();
            MSW002 objMSW002 = new MSW002();
            if (lineid > 0)
            {
                objMSW002 = db.MSW002.Where(x => x.LineId == lineid).FirstOrDefault();
            }

            int Headerid = (from a in db.MSW001
                            where a.Project == project
                            select a.HeaderId).FirstOrDefault();

            ViewBag.project = project;
            ViewBag.document = document;
            ViewBag.RevNo = RevNo;
            ViewBag.Headerid = Headerid;
            ViewBag.Lineid = lineid;
            ViewBag.isln = isln;

            return PartialView("MSWApproveLines", objMSW002);
        }

        [HttpPost]
        public ActionResult GetInsertView(string project, string document, string RevNo)
        {
            project = project.Split('-')[0].ToString().Trim();
            int Headerid = (from a in db.MSW001
                            where a.Project == project
                            select a.HeaderId).FirstOrDefault();

            ViewBag.project = project;
            ViewBag.document = document;
            ViewBag.RevNo = RevNo;
            ViewBag.Headerid = Headerid;

            MSW002 objMSW002 = new MSW002();
            return PartialView("MSWApproveLines", objMSW002);
        }
        [HttpPost]
        public ActionResult SaveMSWLines(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                MSW001 objMSW001 = db.MSW001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                MSW002 objMSW002 = new MSW002
                {
                    HeaderId = objMSW001.HeaderId,
                    Project = objMSW001.Project,
                    Document = objMSW001.Document,
                    RevNo = objMSW001.RevNo,
                    MainTask = fc["MainTask" + newRowIndex],
                    ReferenceDrawing = fc["ReferenceDrawing" + newRowIndex],
                    MachineActivity = fc["MachineActivity" + newRowIndex],
                    MachineRequired = fc["MachineRequired" + newRowIndex],
                    Quantity = fc["Quantity" + newRowIndex],
                    Location = fc["Location" + newRowIndex],
                    TaskManager = fc["TaskManager" + newRowIndex],
                    IsLNLine = false,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                };

                if (objMSW001.Status.ToLower() == clsImplementationEnum.CTQStatus.Approved.GetStringValue().ToLower())
                {
                    objMSW001.RevNo = Convert.ToInt32(objMSW001.RevNo) + 1;
                    objMSW001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    objMSW001.EditedBy = objClsLoginInfo.UserName;
                    objMSW001.EditedOn = DateTime.Now;
                }
                db.MSW002.Add(objMSW002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

               
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteMSWLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                MSW002 objMSW002 = db.MSW002.Where(x => x.LineId == Id).FirstOrDefault();
                db.MSW002.Remove(objMSW002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateMSWLines(MSW002 msw002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int lineid = Convert.ToInt32(fc["hfLineid"].ToString());
                if (lineid > 0)
                {
                    MSW002 objMSW002 = db.MSW002.Where(x => x.LineId == lineid).FirstOrDefault();
                    objMSW002.HeaderId = Convert.ToInt32(fc["hfHeaderId"].ToString());
                    objMSW002.Project = fc["hfProject"].ToString();
                    objMSW002.Document = fc["hfdoc"].ToString();
                    objMSW002.RevNo = Convert.ToInt32(Regex.Replace(fc["hfRev"].ToString(), "[^0-9]+", string.Empty));
                    objMSW002.MainTask = fc["txtMainTask"].ToString();
                    objMSW002.ReferenceDrawing = fc["txtRef"].ToString();
                    objMSW002.MachineActivity = fc["txtMact"].ToString();
                    objMSW002.MachineRequired = fc["txtMreq"].ToString();
                    objMSW002.Quantity = fc["txtQty"].ToString();
                    objMSW002.Location = fc["txtLoca"].ToString();
                    objMSW002.TaskManager = fc["txtTask"].ToString();
                    objMSW002.EditedBy = objClsLoginInfo.UserName;
                    objMSW002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult LoadMSWLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"] = Convert.ToInt32(headerid);
                var isEditable = false;
                MSW001 ObjMSW001 = db.MSW001.Where(M => M.HeaderId == headerid).FirstOrDefault();
                if (param.CTQCompileStatus.ToLower() == clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue().ToLower() && ObjMSW001.ApprovedBy==objClsLoginInfo.UserName)
                {
                    isEditable = true;
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = "1=1";
                //string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (ReferenceDrawing like '%" + param.sSearch
                        + "%' or MainTask like '%" + param.sSearch
                        + "%' or MachineActivity like '%" + param.sSearch
                        + "%' or MachineRequired like '%" + param.sSearch
                        + "%' or Quantity like '%" + param.sSearch
                        + "%' or Location like '%" + param.sSearch
                        + "%' or TaskManager like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_MSW_GET_LINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                        "",
                                        Helper.GenerateTextbox(newRecordId, "MainTask"),
                                        Helper.GenerateTextbox(newRecordId, "MachineActivity"),
                                        Helper.GenerateTextbox(newRecordId, "Quantity"),
                                        Helper.GenerateTextbox(newRecordId, "MachineRequired"),
                                        Helper.GenerateTextbox(newRecordId, "Location"),
                                        Helper.GenerateTextbox(newRecordId, "ReferenceDrawing"),
                                        Helper.GenerateTextbox(newRecordId, "TaskManager"),
                                        Helper.GenerateGridButton(newRecordId, "Save", "Add Rocord", "fa fa-plus", "SaveNewRecord();" ),
                                        "",
                                        Helper.GenerateHidden(newRecordId, "HeaderId", param.Headerid),
                                        ""
                                    };

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.ROW_NO),                            
                                Convert.ToString(uc.MainTask),
                               Convert.ToString(uc.MachineActivity) == "" ?    Convert.ToString(uc.MachineActivity) :  "<ul><li>" +  Convert.ToString(uc.MachineActivity).Replace("|", "</li><li>").ToString()+"</li></ul>",

                                isEditable == true ? uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "Quantity", uc.Quantity, "UpdateData(this, "+uc.LineId+");","", true) : GenerateTextboxFor(uc.LineId,"", "Quantity", uc.Quantity, "UpdateData(this, "+uc.LineId+");","", false) : GenerateTextboxFor(uc.LineId,"", "Quantity", uc.Quantity, "UpdateData(this, "+uc.LineId+");","", true),
                               isEditable == true ? uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "MachineRequired", uc.MachineRequired, "UpdateData(this, "+uc.LineId+");","", true) : GenerateTextboxFor(uc.LineId,"", "MachineRequired", uc.MachineRequired, "UpdateData(this, "+uc.LineId+");","", false) : GenerateTextboxFor(uc.LineId,"", "MachineRequired", uc.MachineRequired, "UpdateData(this, "+uc.LineId+");","", true),
                               isEditable == true ? uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "Location", uc.Location, "UpdateData(this, "+uc.LineId+");","", true) : GenerateTextboxFor(uc.LineId,"", "Location", uc.Location, "UpdateData(this, "+uc.LineId+");","", false) : GenerateTextboxFor(uc.LineId,"", "Location", uc.Location, "UpdateData(this, "+uc.LineId+");","", true),
                               isEditable == true ? uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "ReferenceDrawing", uc.ReferenceDrawing.Replace("|", ","), "UpdateData(this, "+uc.LineId+");","", true) : GenerateTextboxFor(uc.LineId,"", "ReferenceDrawing", uc.ReferenceDrawing.Replace("|", ","), "UpdateData(this, "+uc.LineId+");","", false) : GenerateTextboxFor(uc.LineId,"", "ReferenceDrawing", uc.ReferenceDrawing.Replace("|", ","), "UpdateData(this, "+uc.LineId+");","", true),
                               isEditable == true ? uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "TaskManager", uc.TaskManager, "UpdateData(this, "+uc.LineId+");","", true) : GenerateTextboxFor(uc.LineId,"", "TaskManager", uc.TaskManager, "UpdateData(this, "+uc.LineId+");","", false) : GenerateTextboxFor(uc.LineId,"", "TaskManager", uc.TaskManager, "UpdateData(this, "+uc.LineId+");","", true),
                               isEditable == true ? uc.IsLNLine == false ? HTMLActionString(uc.HeaderId,"","Delete","Delete Record","fa fa-trash-o","DeleteLineRecord("+ uc.LineId +");") : null : null,
                               Helper.GenerateHidden(uc.LineId, "LineId", uc.LineId.ToString()),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", uc.HeaderId.ToString()),
                               Convert.ToString(uc.IsLNLine)
                           }).ToList();

                data.Insert(newRecordId, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult ApproveSelectedMSW(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);

                    db.SP_MSW_APPROVE(HeaderId, objClsLoginInfo.UserName);
                    MSW001 objMSW001 = db.MSW001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    int newId = db.MSW001_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objMSW001.HeaderId, objMSW001.Status, objMSW001.RevNo, objMSW001.Project, objMSW001.Document,newId);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objMSW001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objMSW001.Project, clsImplementationEnum.PlanList.Machining_Scope_of_Work.GetStringValue(), objMSW001.RevNo.Value.ToString(), objMSW001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Machining_Scope_of_Work.GetStringValue(), objMSW001.HeaderId.ToString(), false),
                                                        objMSW001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ReturnSelectedMSW(string strHeaderIds , string remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    MSW001 objMSW001 = db.MSW001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    objMSW001.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objMSW001.ApprovedBy = objClsLoginInfo.UserName;
                    objMSW001.ApprovedOn = DateTime.Now;
                    objMSW001.ReturnRemark = remarks;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objMSW001.HeaderId, objMSW001.Status, objMSW001.RevNo, objMSW001.Project, objMSW001.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objMSW001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objMSW001.Project, clsImplementationEnum.PlanList.Machining_Scope_of_Work.GetStringValue(), objMSW001.RevNo.Value.ToString(), objMSW001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Machining_Scope_of_Work.GetStringValue(), objMSW001.HeaderId.ToString(), false),
                                                        objMSW001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Return.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId,string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Machining_Scope_of_Work.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Machining_Scope_of_Work, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult createRevison(int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int? revison = (from a in db.MSW001
                                where a.HeaderId == headerid
                                select a.RevNo).FirstOrDefault();
                revison = revison + 1;
                MSW001 objMSW001 = db.MSW001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                objMSW001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                objMSW001.RevNo = revison;
                objMSW001.EditedBy = objClsLoginInfo.UserName;
                objMSW001.EditedOn = DateTime.Now;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = revison.ToString();
                Manager.UpdatePDN002(objMSW001.HeaderId, objMSW001.Status, objMSW001.RevNo, objMSW001.Project, objMSW001.Document);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getCodeValue(string project, string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {

                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult isLineAvailable(string project)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                project = project.Split('-')[0].Trim();
                var Exists = db.MSW002.Any(x => x.Project == project);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateData(int LineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_MSW_LINE_UPDATE(LineId, columnName, columnValue);
                    db.SP_MSW_LINE_UPDATE(LineId, "EditedBy", objClsLoginInfo.UserName);
                    //ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.LineUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetHistoryView(int headerid)
        {
            MSW001_Log objMSW001 = new MSW001_Log();
            Session["Headerid"] = Convert.ToInt32(headerid);
            return PartialView("getHistoryPartial", objMSW001);
        }

        [HttpPost]
        public JsonResult LoadMSWHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;

                //   strWhere +=  "CreatedBy=" + user + " and ApprovedBy=" + user;
                strWhere += "ApprovedBy=" + user+" "+"and HeaderId="+Convert.ToInt32(Session["Headerid"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }

                var lstResult = db.SP_MSW_GET_HISTORY_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                             //  Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                             //  Convert.ToString(uc.Document),
                             //  Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                             ////  Convert.ToDateTime(uc.CDD).ToString("dd/MM/yyyy"),
                             //  Convert.ToString(uc.Product),
                             //  Convert.ToString(uc.ProcessLicensor),
                             //  Convert.ToString("R"+uc.RevNo),
                             //  Convert.ToString(uc.Status),
                             //  Convert.ToString(uc.HeaderId),
                             //  Convert.ToString(db.MSW001_Log.FirstOrDefault(q => (q.HeaderId == uc.HeaderId && q.RevNo == uc.RevNo)).Id)
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               // Convert.ToString(uc.Customer),
                              Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                             //  Convert.ToDateTime(uc.CDD).ToString("dd/MM/yyyy"),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                                Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                Convert.ToString(uc.ApprovedBy),
                                Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.HeaderId),
                              Convert.ToString(db.MSW001_Log.FirstOrDefault(q => (q.HeaderId == uc.HeaderId && q.RevNo == uc.RevNo)).Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult LoadMSWLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"]= Convert.ToInt32(param.Headerid);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
              
                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (SectionDescription like '%" + param.sSearch
                        + "%' or FurnaceCharge like '%" + param.sSearch
                        + "%' or Height like '%" + param.sSearch
                        + "%' or Width like '%" + param.sSearch
                        + "%' or Length like '%" + param.sSearch
                        + "%' or Weight like '%" + param.sSearch
                        + "%' or Furnace like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_MSW_GET_HISTORY_LINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.ROW_NO),
                           Convert.ToString(uc.MainTask),
                          // Convert.ToString(uc.MachineActivity),
                            Convert.ToString(uc.MachineActivity) == "" ?    Convert.ToString(uc.MachineActivity) :  "<ul><li>" +  Convert.ToString(uc.MachineActivity).Replace("|", "</li><li>").ToString()+"</li></ul>",
                           Convert.ToString(uc.Quantity),
                           Convert.ToString(uc.MachineRequired),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.ReferenceDrawing),
                           Convert.ToString(uc.TaskManager)
                           }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_MSW_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(li.Project)),
                                      CDD = Convert.ToDateTime(li.CDD).ToString("dd/MM/yyyy"),
                                      Product = Convert.ToString(li.Product),
                                      ProcessLicensor = Convert.ToString(li.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + li.RevNo),
                                      Status = Convert.ToString(li.Status),
                                      SubmittedBy = Convert.ToString(li.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy = Convert.ToString(li.ApprovedBy),
                                      ApprovedOn = Convert.ToString(Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy")),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_MSW_GET_LINEDETAILS(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      MainTask = li.MainTask,
                                      MachineActivity = li.MachineActivity,
                                      Quantity = li.Quantity,
                                      MachineRequired = li.MachineRequired,
                                      Location = li.Location,
                                      ReferenceDrawing = li.ReferenceDrawing,
                                      TaskManager = li.TaskManager

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateExcelHistroy(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_MSW_GET_HISTORY_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(li.Project)),
                                      CDD = Convert.ToDateTime(li.CDD).ToString("dd/MM/yyyy"),
                                      Product = Convert.ToString(li.Product),
                                      ProcessLicensor = Convert.ToString(li.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + li.RevNo),
                                      Status = Convert.ToString(li.Status),
                                      SubmittedBy = Convert.ToString(li.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy = Convert.ToString(li.ApprovedBy),
                                      ApprovedOn = Convert.ToString(Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy")),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_MSW_GET_HISTORY_LINEDETAILS(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      MainTask = li.MainTask,
                                      MachineActivity = li.MachineActivity,
                                      Quantity = li.Quantity,
                                      MachineRequired = li.MachineRequired,
                                      Location = li.Location,
                                      ReferenceDrawing = li.ReferenceDrawing,
                                      TaskManager = li.TaskManager

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult getHistoryDetail(int Id = 0)
        {
            MSW001_Log objMSW001 = new MSW001_Log();
            objMSW001 = db.MSW001_Log.Where(x => x.Id == Id).FirstOrDefault();
            var user = objClsLoginInfo.UserName;
            if (objMSW001 != null)
            {
                var lstProjectDesc = (from a in db.COM001
                                      select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
                ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

                List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
                ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                var approver = (from t1 in db.ATH001
                                join t2 in db.COM003 on t1.Employee equals t2.t_psno
                                where t2.t_actv ==1
                                select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
                ViewBag.approver = new SelectList(approver, "Employee", "Desc");
                if (Id > 0)
                {
                    var project = (from a in db.MSW001_Log
                                   where a.Id == Id
                                   select a.Project).FirstOrDefault();
                    ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
                }
            }
            var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.MSW001_Log.Any(q => (q.HeaderId == objMSW001.HeaderId && q.RevNo == (objMSW001.RevNo - 1))) ? urlPrefix + db.MSW001_Log.Where(q => (q.HeaderId == objMSW001.HeaderId && q.RevNo == (objMSW001.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.MSW001_Log.Any(q => (q.HeaderId == objMSW001.HeaderId && q.RevNo == (objMSW001.RevNo + 1))) ? urlPrefix + db.MSW001_Log.Where(q => (q.HeaderId == objMSW001.HeaderId && q.RevNo == (objMSW001.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return View(objMSW001);
        }
    }
}