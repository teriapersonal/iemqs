﻿using IEMQS.Areas.MSW.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Data.SqlClient;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.Utility.Models;

namespace IEMQS.Areas.MSW.Controllers
{
    public class MaintainController : clsBase
    {
        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["LNConcerto"].ToString());
        List<MSW002> listMSW002 = new List<MSW002>();

        #region Utility
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            //if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            //{
            //    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            //}
            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";

            return htmlControl;
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "MSW TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                MSW001_Log objMSW001_Log = db.MSW001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objMSW001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objMSW001_Log.CreatedBy) : null;
                model.CreatedOn = objMSW001_Log.CreatedOn;
                model.EditedBy = objMSW001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objMSW001_Log.EditedBy) : null;
                model.EditedOn = objMSW001_Log.EditedOn;
                model.SubmittedBy = objMSW001_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objMSW001_Log.SubmittedBy) : null;
                model.SubmittedOn = objMSW001_Log.SubmittedOn;

                model.ApprovedBy = objMSW001_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objMSW001_Log.ApprovedBy) : null;
                model.ApprovedOn = objMSW001_Log.ApprovedOn;

            }
            else
            {

                MSW001_Log objMSW001_Log = db.MSW001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objMSW001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objMSW001_Log.CreatedBy) : null;
                model.CreatedOn = objMSW001_Log.CreatedOn;
                model.EditedBy = objMSW001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objMSW001_Log.EditedBy) : null;
                model.EditedOn = objMSW001_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "MSW Timeline";

            if (HeaderId > 0)
            {
                MSW001 objMSW001 = db.MSW001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objMSW001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objMSW001.CreatedBy) : null;
                model.CreatedOn = objMSW001.CreatedOn;
                model.EditedBy = objMSW001.EditedBy != null ? Manager.GetUserNameFromPsNo(objMSW001.EditedBy) : null;
                model.EditedOn = objMSW001.EditedOn;

                // model.SubmittedBy = PMB001_Log.SendToCompiledOn != null ? Manager.GetUserNameFromPsNo(PMB001_Log.SendToCompiledOn) : null;
                //model.SubmittedOn = PMB001_Log.SendToCompiledOn;
                model.SubmittedBy = objMSW001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objMSW001.SubmittedBy) : null;
                model.SubmittedOn = objMSW001.SubmittedOn;
                //model.CompiledBy = PMB001_Log.CompiledBy != null ? Manager.GetUserNameFromPsNo(PMB001_Log.CompiledBy) : null;
                // model.CompiledOn = PMB001_Log.CompiledOn;
                model.ApprovedBy = objMSW001.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objMSW001.ApprovedBy) : null;
                model.ApprovedOn = objMSW001.ApprovedOn;

            }
            else
            {
                MSW001 objMSW001 = db.MSW001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objMSW001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objMSW001.CreatedBy) : null;
                model.CreatedOn = objMSW001.CreatedOn;
                model.EditedBy = objMSW001.EditedBy != null ? Manager.GetUserNameFromPsNo(objMSW001.EditedBy) : null;
                model.EditedOn = objMSW001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [HttpPost]
        public ActionResult ReviseHeader(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                MSW001 objMSW001 = db.MSW001.Where(u => u.HeaderId == strHeaderId).SingleOrDefault();
                if (objMSW001 != null)
                {
                    objMSW001.RevNo = Convert.ToInt32(objMSW001.RevNo) + 1;
                    objMSW001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objMSW001.ReviseRemark = strRemarks;
                    objMSW001.EditedBy = objClsLoginInfo.UserName;
                    objMSW001.EditedOn = DateTime.Now;
                    objMSW001.ReturnRemark = null;
                    objMSW001.ApprovedOn = null;
                    objMSW001.SubmittedBy = null;
                    objMSW001.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objMSW001.HeaderId, objMSW001.Status, objMSW001.RevNo, objMSW001.Project, objMSW001.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objMSW001.HeaderId;
                    objResponseMsg.Status = objMSW001.Status;
                    objResponseMsg.rev = objMSW001.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public static string GenerateTextboxFor(int rowId, string status, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";

            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                htmlControl = "<input disabled type='text' id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  />";
            }
            else
            {
                htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";
            }

            return htmlControl;
        }
        #endregion

        // GET: MSW/Maintain
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();

        }
        [SessionExpireFilter]
        public ActionResult MSWDetail(int Id = 0)
        {
            MSW001 objMSW001 = new MSW001();
            objMSW001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");
            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv == 1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");
            if (Id > 0)
            {
                var project = (from a in db.MSW001
                               where a.HeaderId == Id
                               select a.Project).FirstOrDefault();
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
                objMSW001 = db.MSW001.Where(x => x.HeaderId == Id).FirstOrDefault();
                if (objMSW001.CreatedBy != objClsLoginInfo.UserName)
                {
                    //return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }

                var PlanningDinID = db.PDN002.Where(x => x.RefId == Id && x.DocumentNo == objMSW001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objMSW001.HeaderId, objMSW001.Document);
                ViewBag.isPlng3Role = objClsLoginInfo.GetUserRoleList().Any(m => m == clsImplementationEnum.UserRoleName.PLNG3.ToString());
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
            }
            return View(objMSW001);
        }

        [HttpPost]
        public ActionResult getHeaderId(string project)
        {
            project = project.Split('-')[0].Trim();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int Headerid = (from a in db.MSW001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();
                string status = (from a in db.MSW001
                                 where a.Project == project
                                 select a.Status).FirstOrDefault();


                objResponseMsg.Key = true;
                objResponseMsg.Value = Headerid.ToString() + "|" + status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }

        [HttpPost]
        public ActionResult GetHeaderData(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();
                var ExistsPrject = db.COM001.Any(x => x.t_cprj == project);
                if (ExistsPrject == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    int RevNum;
                    string customer = Manager.GetCustomerCodeAndNameByProject(project);

                    var Exists = db.MSW001.Any(x => x.RevNo == null && x.Project == project);
                    if (Exists == false)
                    {
                        RevNum = 0;
                    }
                    else
                    {
                        string revNo = (from rev in db.MSW001
                                        where rev.Project == project
                                        select rev.Project).FirstOrDefault();
                        RevNum = Convert.ToInt32(revNo) + 1;
                    }
                    //var contract = (from a in db.COM005
                    //                where a.t_sprj == project
                    //                select a.t_cono).FirstOrDefault();
                    //var ccdd = (from a in db.COM008
                    //            where a.t_cono == contract
                    //            select a.t_ccdd).FirstOrDefault();
                    project = project.Split('-')[0].ToString().Trim();
                    var ccdd = Manager.GetContractWiseCdd(project);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = customer + "|" + RevNum + "|" + ccdd;

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveMSWHeader(MSW001 msw001, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {

                //Convert.ToInt32(Request.QueryString["Id"]); //Convert.ToInt32(fc["hfHeaderId"].ToString());

                if (!string.IsNullOrWhiteSpace(fc["hfHeaderId"].ToString()) && Convert.ToInt32(fc["hfHeaderId"].ToString()) > 0)
                {
                    int hid = Convert.ToInt32(fc["hfHeaderId"].ToString());

                    MSW001 objmsw001 = db.MSW001.Where(x => x.HeaderId == hid).FirstOrDefault();
                    string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                    objmsw001.Project = fc["hfProject"].ToString();
                    objmsw001.Document = fc["txtdoc"].ToString();
                    objmsw001.Customer = fc["txtCust"].ToString().Split('-')[0];
                    objmsw001.ReviseRemark = fc["ReviseRemark"].ToString();
                    if (objmsw001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objmsw001.RevNo = Convert.ToInt32(objmsw001.RevNo) + 1;
                        objmsw001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                        objmsw001.ReturnRemark = null;
                        objmsw001.ApprovedOn = null;
                    }
                    if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                    {
                        objmsw001.CDD = null;
                    }
                    else
                    {
                        objmsw001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                    }
                    //objmsw001.Product = fc["ddlCat"].ToString();
                    objmsw001.ProcessLicensor = fc["txtlicence"].ToString();
                    objmsw001.EditedBy = objClsLoginInfo.UserName;
                    objmsw001.EditedOn = DateTime.Now;
                    objmsw001.ApprovedBy = fc["hfApp"].ToString().Split('-')[0].Trim();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                    objResponseMsg.Revision = "R" + objmsw001.RevNo.ToString();
                    objResponseMsg.status = objmsw001.Status;
                    Manager.UpdatePDN002(objmsw001.HeaderId, objmsw001.Status, objmsw001.RevNo, objmsw001.Project, objmsw001.Document);

                }
                else
                {
                    string project = fc["ddlProject"].ToString();
                    project = project.Split('-')[0].Trim();
                    var isvalid = db.MSW001.Any(x => x.Project == project);
                    if (isvalid == false)
                    {
                        string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                        MSW001 objmsw001 = new MSW001();
                        objmsw001.Project = project;
                        objmsw001.Document = fc["txtdoc"].ToString();
                        objmsw001.Customer = fc["txtCust"].ToString().Split('-')[0];
                        objmsw001.RevNo = Convert.ToInt32(rev);
                        objmsw001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                        if (string.IsNullOrWhiteSpace(fc["txtCdd"].ToString()) || fc["txtCdd"].ToString() == "01-01-0001" || fc["txtCdd"].ToString() == "01/01/0001")
                        {
                            objmsw001.CDD = null;
                        }
                        else
                        {
                            objmsw001.CDD = Convert.ToDateTime(fc["txtCdd"].ToString());
                        }

                        objmsw001.Product = fc["ddlCat"].ToString();
                        objmsw001.ProcessLicensor = fc["txtlicence"].ToString();
                        objmsw001.CreatedBy = objClsLoginInfo.UserName;
                        objmsw001.CreatedOn = DateTime.Now;
                        objmsw001.ApprovedBy = fc["ddlApprover"].ToString().Split('-')[0].Trim();
                        objmsw001.ReviseRemark = fc["ReviseRemark"].ToString();
                        db.MSW001.Add(objmsw001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
                        objResponseMsg.Revision = "R" + objmsw001.RevNo.ToString();
                        objResponseMsg.status = objmsw001.Status;
                        Manager.UpdatePDN002(objmsw001.HeaderId, objmsw001.Status, objmsw001.RevNo, objmsw001.Project, objmsw001.Document);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult getData(string project, string document, string RevNo, string result)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            project = project.Split('-')[0].ToString().Trim();


            int Headerid = (from a in db.MSW001
                            where a.Project == project
                            select a.HeaderId).FirstOrDefault();


            int rev = Convert.ToInt32(Regex.Replace(RevNo.ToString(), "[^0-9]+", string.Empty));
            var isvalid = db.MSW002.Any(x => x.Project == project && x.IsLNLine == true && x.Document == document && x.RevNo == rev);

            List<MSW_LNData> lstMSW_LNData = getDataLN(project, document, RevNo);
            if (result.ToLower() == "true")
            {
                var deletelndata = db.MSW002.Where(m => m.HeaderId == Headerid && m.IsLNLine == true).ToList();
                db.MSW002.RemoveRange(deletelndata);
                db.SaveChanges();
              
            }
            foreach (var listln in lstMSW_LNData.ToList())
            {
                // var qry = db.SCP002.Where(m => m.ReferenceDrawing == listln.ReferenceDrawing && m.ScopeOfWork == listln.ScopeOfWork && m.TaskManager == listln.TaskManager && m.TaskDescription==listln.TaskDecription).FirstOrDefault();
                var qry = db.MSW002.Where(m => m.TASKUNIQUEID == listln.TASKUNIQUEID && m.Project == listln.Project).FirstOrDefault();
                if (qry == null)
                {
                    MSW002 msw = new MSW002();
                    msw.HeaderId = listln.HeaderId;
                    msw.Project = listln.Project;
                    msw.Document = listln.Document;
                    msw.MainTask = listln.MainTask;
                    msw.RevNo = Convert.ToInt32(Regex.Replace(listln.RevNo.ToString(), "[^0-9]+", string.Empty));
                    msw.ReferenceDrawing = listln.ReferenceDrawing;
                    msw.MachineActivity = listln.MachineActivity;
                    msw.MachineRequired = listln.MachineRequired;
                    msw.Quantity = listln.Quantity;
                    msw.Location = listln.Location;
                    msw.TaskManager = listln.TaskManager;
                    msw.IsLNLine = listln.isLNData;
                    msw.TASKUNIQUEID = Convert.ToInt32(listln.TASKUNIQUEID);

                    msw.CreatedBy = listln.CreatedBy;
                    msw.CreatedOn = Convert.ToDateTime(listln.CreatedOn);
                    db.MSW002.Add(msw);
                    db.SaveChanges();

                }
                else
                {
                    if (result.ToLower().ToString() == "true")
                    {

                        MSW002 msw = new MSW002();
                        msw = db.MSW002.Where(m => m.TASKUNIQUEID == listln.TASKUNIQUEID && m.Project == listln.Project).FirstOrDefault();
                        msw.HeaderId = listln.HeaderId;
                        msw.Project = listln.Project;
                        msw.Document = listln.Document;
                        msw.MainTask = listln.MainTask;
                        msw.RevNo = Convert.ToInt32(Regex.Replace(listln.RevNo.ToString(), "[^0-9]+", string.Empty));
                        msw.ReferenceDrawing = listln.ReferenceDrawing;
                        msw.MachineActivity = listln.MachineActivity;
                        msw.MachineRequired = listln.MachineRequired;
                        msw.Quantity = listln.Quantity;
                        msw.Location = listln.Location;
                        msw.TaskManager = listln.TaskManager;
                        msw.IsLNLine = listln.isLNData;
                        msw.CreatedBy = listln.CreatedBy;
                        msw.CreatedOn = Convert.ToDateTime(listln.CreatedOn);
                        db.SaveChanges();
                    }
                }
            }




            return Json(lstMSW_LNData, JsonRequestBehavior.AllowGet);
        }
        public List<MSW_LNData> getDataLN(string project, string document, string RevNo)
        {
            List<MSW_LNData> data = new List<MSW_LNData>();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();
                connection.Open();
                int Headerid = (from a in db.MSW001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();

                var location = objClsLoginInfo.Location;

                List<string> list = new List<string>();
                list = (from a in db.MSW003
                            //where a.Location == location
                        select a.TaskManager).ToList();


                //string query = "select  DISTINCT  a.CLITEMTEXT as 'Machining Activity' , a.TEXT_3 as 'Machine Required' , " +
                //                " a.TEXT2 as 'Ref Drawing' , B.CONTACT as 'Task Manager' , B.TASKUNIQUEID as 'TASKUNIQUEID' , A.NAME AS 'MAIN TASK' , a.SEQUENCEID  " +
                //                " from [dbo].[VR_PROJECT_TASK_CHECKLIST]  a " +
                //                " LEFT  join[dbo].[PROJ_TASK] b on a.PROJECTID = b.PROJECTID and a.TASKUNIQUEID = b.TASKUNIQUEID " +
                //                " where a.PROJECTNAME = @proj " +
                //                " and CONTACT in ({0}) AND a.CLITEMTEXT IS NOT NULL " +
                //                " ORDER BY a.NAME";

                string query = " SELECT " +
                "        c.TASKUNIQUEID as 'TASKUNIQUEID' " +

            "        ,Isnull( " +
            "        Stuff((SELECT '|' + cc.CLITEMTEXT " +
            "        FROM dbo.VR_PROJECT_TASK_CHECKLIST  cc " +
            "        Join " +
            "        (SELECT DISTINCT TASKUNIQUEID, CONTACT, EXPECTED_START_DATE, EXPECTED_END_DATE FROM " +
            "         dbo.PROJ_TASK WHERE  PROJECTNAME = @proj AND CONTACT in ({0}) " +
            "        ) pp on pp.TASKUNIQUEID = cc.TASKUNIQUEID " +
            "        WHERE cc.TASKUNIQUEID = c.TASKUNIQUEID " +
            "        and cc.PROJECTNAME = @proj " +
            "        FOR XML PATH('')), 1, 1, ''),'') as 'Machining Activity' " +

            "        ,Isnull( " +
            "        Stuff((SELECT Distinct '|' + cc.TEXT2 " +
            "        FROM dbo.VR_PROJECT_TASK_CHECKLIST  cc " +
            "        Join " +
            "        (SELECT DISTINCT TASKUNIQUEID, CONTACT, EXPECTED_START_DATE, EXPECTED_END_DATE FROM " +
            "         dbo.PROJ_TASK WHERE  PROJECTNAME = @proj AND CONTACT in ({0}) " +
            "        ) pp on pp.TASKUNIQUEID = cc.TASKUNIQUEID " +
            "        WHERE cc.TASKUNIQUEID = c.TASKUNIQUEID " +
            "        and cc.PROJECTNAME = @proj " +
            "        FOR XML PATH('')), 1, 1, ''),'') as  'Ref Drawing' " +

            "        ,p.CONTACT as 'Task Manager' " +
            "        ,c.NAME as 'MAIN TASK' " +
            " 	   ,c.TEXT_3 as 'Machine Required' " +


            " FROM dbo.VR_PROJECT_TASK_CHECKLIST c " +
            " Join " +
            " (SELECT DISTINCT TASKUNIQUEID, CONTACT, EXPECTED_START_DATE, EXPECTED_END_DATE FROM " +
            "  dbo.PROJ_TASK WHERE  PROJECTNAME = @proj AND CONTACT in ({0}) " +
            " ) p on p.TASKUNIQUEID = c.TASKUNIQUEID " +
            " where c.PROJECTNAME = @proj " +
            "  AND p.CONTACT in ({0})  " +
            " GROUP BY c.TASKUNIQUEID ,p.CONTACT ,c.NAME ,c.TEXT_3 ";


                string[] paramNames = list.Select(
                        (s, i) => "@taskmanager" + i.ToString()
                    ).ToArray();
                string inClause = string.Join(",", paramNames);
                using (var command = new SqlCommand(string.Format(query, inClause), connection))
                {
                    command.Parameters.AddWithValue("@proj", project);
                    for (int i = 0; i < paramNames.Length; i++)
                    {
                        command.Parameters.AddWithValue(paramNames[i], list[i]);
                    }
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        MSW_LNData f = new MSW_LNData();
                        f.HeaderId = Headerid;
                        f.Project = project;
                        f.Document = document;
                        f.RevNo = RevNo;
                        f.TASKUNIQUEID = Convert.ToInt32(reader["TASKUNIQUEID"]);
                        //f.SEQUENCEID = Convert.ToInt32(reader["SEQUENCEID"]);
                        f.MainTask = Convert.ToString(reader["Main Task"]);
                        f.ReferenceDrawing = Convert.ToString(reader["Ref Drawing"]);
                        f.MachineActivity = Convert.ToString(reader["Machining Activity"]);
                        f.MachineRequired = Convert.ToString(reader["Machine Required"]);
                        f.Quantity = "";
                        f.Location = "";
                        f.TaskManager = Convert.ToString(reader["Task Manager"]);
                        f.isLNData = true;
                        f.CreatedBy = objClsLoginInfo.UserName;
                        f.CreatedOn = DateTime.Now;

                        data.Add(f);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            finally
            {
                connection.Close();
            }
            return data;
        }
        public List<MSW_LNData> getDataMSW002(string project, string document, string RevNo, int Headerid)
        {
            List<MSW_LNData> data1 = new List<MSW_LNData>();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();



                var location = objClsLoginInfo.Location;

                List<string> list = new List<string>();
                list = (from a in db.MSW003
                        //Observation 15975
                        //where a.Location == location
                        select a.TaskManager).ToList();



                var qry = db.MSW002.Where(m => m.HeaderId == Headerid).ToList();

                foreach (var itm in qry.ToList())
                {
                    MSW_LNData ln_data = new MSW_LNData();
                    ln_data.HeaderId = Headerid;
                    ln_data.Project = project;
                    ln_data.Document = document;
                    ln_data.RevNo = RevNo;
                    ln_data.MainTask = itm.MainTask;
                    ln_data.ReferenceDrawing = itm.ReferenceDrawing;
                    ln_data.MachineActivity = itm.MachineActivity;
                    ln_data.MachineRequired = itm.MachineRequired;
                    ln_data.TASKUNIQUEID = Convert.ToInt32(itm.TASKUNIQUEID);
                    ln_data.Quantity = "";
                    ln_data.Location = "";
                    ln_data.TaskManager = itm.TaskManager;
                    ln_data.isLNData = true;
                    ln_data.CreatedBy = objClsLoginInfo.UserName;
                    ln_data.CreatedOn = DateTime.Now;
                    data1.Add(ln_data);

                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            finally
            {
                connection.Close();
            }
            return data1;
        }
        [HttpPost]
        public ActionResult LoadMSWLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"] = Convert.ToInt32(headerid);
                var status = db.MSW001.Where(i => i.HeaderId == headerid).Select(i => i.Status).FirstOrDefault();
                var isEditable = true;
                if (param.CTQCompileStatus.ToLower() == clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue().ToLower() || param.CTQCompileStatus.ToLower() == clsImplementationEnum.CTQStatus.Approved.GetStringValue().ToLower())
                {
                    isEditable = false;
                }
                string strWhere = "1=1";
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (ReferenceDrawing like '%" + param.sSearch
                        + "%' or MainTask like '%" + param.sSearch
                        + "%' or MachineActivity like '%" + param.sSearch
                        + "%' or MachineRequired like '%" + param.sSearch
                        + "%' or Quantity like '%" + param.sSearch
                        + "%' or Location like '%" + param.sSearch
                        + "%' or TaskManager like '%" + param.sSearch
                        + "%')";
                }
                else
                {                    
                  strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);                    
                }
                var lstResult = db.SP_MSW_GET_LINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                        "",
                                        Helper.GenerateTextbox(newRecordId, "MainTask"),
                                        Helper.GenerateTextbox(newRecordId, "MachineActivity"),
                                        Helper.GenerateTextbox(newRecordId, "Quantity"),
                                        Helper.GenerateTextbox(newRecordId, "MachineRequired"),
                                        Helper.GenerateTextbox(newRecordId, "Location"),
                                        Helper.GenerateTextbox(newRecordId, "ReferenceDrawing"),
                                        Helper.GenerateTextbox(newRecordId, "TaskManager"),
                                        Helper.GenerateGridButton(newRecordId, "Save", "Add Rocord", "fa fa-plus", "SaveNewRecord();" ),
                                        "",
                                        Helper.GenerateHidden(newRecordId, "HeaderId", param.Headerid),
                                    };

                var data = (from uc in lstResult
                            select new[]
                            {
                               Convert.ToString(uc.ROW_NO),
                               Convert.ToString(uc.MainTask),
                               Convert.ToString(uc.MachineActivity) == "" ?    Convert.ToString(uc.MachineActivity) :  "<ul><li>" +  Convert.ToString(uc.MachineActivity).Replace("|", "</li><li>").ToString()+"</li></ul>",
                               isEditable == false ? GenerateTextboxFor(uc.LineId,"", "Quantity", uc.Quantity, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "Quantity", uc.Quantity, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "Quantity", uc.Quantity, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                               isEditable == false ? GenerateTextboxFor(uc.LineId,"", "MachineRequired", uc.MachineRequired, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "MachineRequired", uc.MachineRequired, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "MachineRequired", uc.MachineRequired, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                               isEditable == false ? GenerateTextboxFor(uc.LineId,"", "Location", uc.Location, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "Location", uc.Location, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "Location", uc.Location, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                               isEditable == false ? GenerateTextboxFor(uc.LineId,"", "ReferenceDrawing", uc.ReferenceDrawing.Replace("|", ","), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "ReferenceDrawing", uc.ReferenceDrawing.Replace("|", ","), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "ReferenceDrawing", uc.ReferenceDrawing.Replace("|", ","), "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                                //isEditable == false ? GenerateTextboxFor(uc.LineId,"", "TaskManager", uc.TaskManager, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true ? GenerateTextboxFor(uc.LineId,"", "TaskManager", uc.TaskManager, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "TaskManager", uc.TaskManager, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                               isEditable == false ?  GenerateTextboxFor(uc.LineId,"", "TaskManager", uc.TaskManager, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", true) : uc.IsLNLine == true && status!=clsImplementationEnum.QualityIdHeaderStatus.Draft.GetStringValue() ? GenerateTextboxFor(uc.LineId,"", "TaskManager", uc.TaskManager, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false) : GenerateTextboxFor(uc.LineId,"", "TaskManager", uc.TaskManager, "UpdateData(this, "+uc.LineId+");","checkStatus("+ uc.HeaderId +");", false),
                               isEditable == false ? null : uc.IsLNLine == false ? HTMLActionString(uc.HeaderId,"","Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");") : null,
                               Helper.GenerateHidden(uc.LineId, "LineId", uc.LineId.ToString()),
                               Helper.GenerateHidden(uc.HeaderId, "HeaderId", uc.HeaderId.ToString()),
                               Convert.ToString(uc.IsLNLine)
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = "",
                  
                }, JsonRequestBehavior.AllowGet);
            }

        }


        [HttpPost]
        public ActionResult GetUpdateView(string project, string document, string RevNo, int lineid, string isln)
        {
            MSW002 objMSW002 = new MSW002();
            var isvalid = db.MSW002.Any(x => x.LineId == lineid);
            if (isvalid == true)
            {
                project = project.Split('-')[0].Trim();
                objMSW002 = db.MSW002.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objMSW002 != null)
                {
                    int Headerid = (from a in db.MSW001
                                    where a.Project == project
                                    select a.HeaderId).FirstOrDefault();
                    ViewBag.project = project;
                    ViewBag.document = document;
                    ViewBag.RevNo = RevNo;
                    ViewBag.Headerid = Headerid;
                    ViewBag.Lineid = lineid;
                    ViewBag.isln = isln;
                }
            }
            return PartialView("MSWLines", objMSW002);
        }

        [HttpPost]
        public ActionResult GetInsertView(string project, string document, string RevNo)
        {
            project = project.Split('-')[0].Trim();
            int Headerid = (from a in db.MSW001
                            where a.Project == project
                            select a.HeaderId).FirstOrDefault();

            ViewBag.project = project;
            ViewBag.document = document;
            ViewBag.RevNo = RevNo;
            ViewBag.Headerid = Headerid;

            MSW002 objMSW002 = new MSW002();
            return PartialView("MSWLines", objMSW002);
        }
        [HttpPost]
        public ActionResult SaveMSWLines(FormCollection fc)
        {
            int newRowIndex = 0;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                MSW001 objMSW001 = db.MSW001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                MSW002 objMSW002 = new MSW002
                {
                    HeaderId = objMSW001.HeaderId,
                    Project = objMSW001.Project,
                    Document = objMSW001.Document,
                    RevNo = objMSW001.RevNo,
                    MainTask = fc["MainTask" + newRowIndex],
                    ReferenceDrawing = fc["ReferenceDrawing" + newRowIndex],
                    MachineActivity = fc["MachineActivity" + newRowIndex],
                    MachineRequired = fc["MachineRequired" + newRowIndex],
                    Quantity = fc["Quantity" + newRowIndex],
                    Location = fc["Location" + newRowIndex],
                    TaskManager = fc["TaskManager" + newRowIndex],
                    IsLNLine = false,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                };

                if (objMSW001.Status.ToLower() == clsImplementationEnum.CTQStatus.Approved.GetStringValue().ToLower())
                {
                    objMSW001.RevNo = Convert.ToInt32(objMSW001.RevNo) + 1;
                    objMSW001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    objMSW001.EditedBy = objClsLoginInfo.UserName;
                    objMSW001.EditedOn = DateTime.Now;
                    objMSW001.ReturnRemark = null;
                    objMSW001.ApprovedOn = null;

                }
                db.MSW002.Add(objMSW002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                //MSW002 objMSW002 = new MSW002();
                //objMSW002.HeaderId = Convert.ToInt32(fc["hfHeaderId"].ToString());
                //objMSW002.Project = fc["hfProject"].ToString();
                //objMSW002.Document = fc["hfdoc"].ToString();
                //objMSW002.RevNo = Convert.ToInt32(Regex.Replace(fc["hfRev"].ToString(), "[^0-9]+", string.Empty));
                //objMSW002.MainTask = fc["txtMainTask"].ToString();
                //objMSW002.ReferenceDrawing = fc["txtRef"].ToString();
                //objMSW002.MachineActivity = fc["txtMact"].ToString();
                //objMSW002.MachineRequired = fc["txtMreq"].ToString();
                //objMSW002.Quantity = fc["txtQty"].ToString();
                //objMSW002.Location = fc["txtLoca"].ToString();
                //objMSW002.TaskManager = fc["txtTask"].ToString();
                //objMSW002.IsLNLine = false;
                //objMSW002.CreatedBy = objClsLoginInfo.UserName;
                //objMSW002.CreatedOn = DateTime.Now;
                //db.MSW002.Add(objMSW002);
                //db.SaveChanges();
                //objResponseMsg.Key = true;
                //objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteMSWLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                MSW002 objMSW002 = db.MSW002.Where(x => x.LineId == Id).FirstOrDefault();
                db.MSW002.Remove(objMSW002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateMSWLines(MSW002 msw002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int lineid = Convert.ToInt32(fc["hfLineid"].ToString());
                if (lineid > 0)
                {
                    MSW002 objMSW002 = db.MSW002.Where(x => x.LineId == lineid).FirstOrDefault();
                    objMSW002.HeaderId = Convert.ToInt32(fc["hfHeaderId"].ToString());
                    objMSW002.Project = fc["hfProject"].ToString();
                    objMSW002.Document = fc["hfdoc"].ToString();
                    objMSW002.RevNo = Convert.ToInt32(Regex.Replace(fc["hfRev"].ToString(), "[^0-9]+", string.Empty));
                    objMSW002.ReferenceDrawing = fc["txtRef"].ToString();
                    objMSW002.MainTask = fc["txtMainTask"].ToString();
                    objMSW002.MachineActivity = fc["txtMact"].ToString();
                    objMSW002.MachineRequired = fc["txtMreq"].ToString();
                    objMSW002.Quantity = fc["txtQty"].ToString();
                    objMSW002.Location = fc["txtLoca"].ToString();
                    objMSW002.TaskManager = fc["txtTask"].ToString();
                    objMSW002.EditedBy = objClsLoginInfo.UserName;
                    objMSW002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("GetMSWGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadMSWHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                //strWhere += " and CreatedBy=" + user;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or CDD like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_MSW_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                               Convert.ToDateTime(uc.CDD).ToString("dd/MM/yyyy"),
                               //Convert.ToString(uc.CDD),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.HeaderId),
                               "<nobr><center>"+"<a title='View' href='"+WebsiteURL+"/MSW/Maintain/MSWDetail?Id="+Convert.ToInt32(uc.HeaderId)+"'><i style='' class='iconspace fa fa-eye'></i></a>"+
                               Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/MSW/Maintain/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblTPHeader')","",  (( uc.RevNo >0 && uc.Status != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue()) || ( uc.RevNo == 0 && uc.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ) ? false:true) +
                               (uc.RevNo>0 ?"<a title=\"History\" onclick=\"history('"+uc.HeaderId+"')\"><i style='' class='iconspace fa fa-history'></i></a>":"<a title=\"History\"><i style='margin-left:5px;' class='disabledicon fa fa-history'></i></a>")+"<a title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/MSW/Maintain/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')><i style='' class='iconspace fa fa-clock-o'></i></a>"
                               + "</center></nobr>",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Machining_Scope_of_Work);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult sentForApproval(string Approver, int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                if (headerid > 0)
                {
                    Approver = Approver.Split('-')[0].Trim();
                    if (Approver.Split('-')[0].ToString().Trim() == objClsLoginInfo.UserName)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.SMessage.ToString();
                    }
                    else
                    {
                        MSW001 objMSW001 = db.MSW001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                        objMSW001.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                        objMSW001.ApprovedBy = Approver.Split('-')[0].ToString().Trim();
                        objMSW001.SubmittedBy = objClsLoginInfo.UserName;
                        objMSW001.SubmittedOn = DateTime.Now;

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.AMessage.ToString();
                        Manager.UpdatePDN002(objMSW001.HeaderId, objMSW001.Status, objMSW001.RevNo, objMSW001.Project, objMSW001.Document);

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(),
                                                            objMSW001.Project,
                                                            "",
                                                            "",
                                                            Manager.GetPDINDocumentNotificationMsg(objMSW001.Project, clsImplementationEnum.PlanList.Machining_Scope_of_Work.GetStringValue(), objMSW001.RevNo.Value.ToString(), objMSW001.Status),
                                                            clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                            Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Machining_Scope_of_Work.GetStringValue(), objMSW001.HeaderId.ToString(), true),
                                                            objMSW001.ApprovedBy);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult createRevison(int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int? revison = (from a in db.MSW001
                                where a.HeaderId == headerid
                                select a.RevNo).FirstOrDefault();
                revison = revison + 1;
                MSW001 objMSW001 = db.MSW001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                objMSW001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                objMSW001.RevNo = revison;
                objMSW001.EditedBy = objClsLoginInfo.UserName;
                objMSW001.EditedOn = DateTime.Now;
                objMSW001.ReturnRemark = null;
                objMSW001.ApprovedOn = null;

                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = revison.ToString();
                Manager.UpdatePDN002(objMSW001.HeaderId, objMSW001.Status, objMSW001.RevNo, objMSW001.Project, objMSW001.Document);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CopyHeaderPartial(int Id)
        {
            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            return PartialView("CopyToDestinationPartial");
        }
        [HttpPost]
        public ActionResult CopyToDestination(int headerId, string destProject)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            MSW001 sourceMSW001 = new MSW001();
            MSW001 destMSW001 = new MSW001();
            SourceMSW objSourceMSW = new SourceMSW();

            try
            {
                string currentUser = objClsLoginInfo.UserName;
                if (headerId > 0)
                {
                    sourceMSW001 = db.MSW001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    objSourceMSW = GetSourceMSWDetail(sourceMSW001.HeaderId);
                }
                if (!string.IsNullOrEmpty(destProject))
                {
                    destMSW001 = db.MSW001.Where(i => i.Project.Equals(destProject, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                    if (destMSW001 != null)
                    {
                        if (string.Equals(destMSW001.Status, clsImplementationEnum.QCPStatus.Draft.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        {
                            objResponseMsg = InsertOrUpdateMSW(objSourceMSW, destMSW001, true);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "You cannot copy to " + destProject + " as it is in " + destMSW001.Status + "Staus";
                        }
                    }
                    else
                    {
                        destMSW001 = new MSW001();
                        destMSW001.Project = destProject;
                        objResponseMsg = InsertOrUpdateMSW(objSourceMSW, destMSW001, false);
                    }
                }
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public SourceMSW GetSourceMSWDetail(int headerId)
        {
            SourceMSW objSourceMSW = new SourceMSW();
            objSourceMSW.SourceHeader = db.MSW001.Where(i => i.HeaderId == headerId).FirstOrDefault();
            objSourceMSW.SourceCategories = db.MSW002.Where(i => i.HeaderId == headerId).ToList();
            return objSourceMSW;
        }

        public clsHelper.ResponseMsg InsertOrUpdateMSW(SourceMSW sourceMSW, MSW001 objMSW001, bool isUpdate)//(SourceQCP sourceQCP,QCP001 objQCP001,bool isUpdate)
        {
            clsHelper.ResponseMsg objResponse = new clsHelper.ResponseMsg();
            var sourceHeader = sourceMSW.SourceHeader;

            try
            {
                if (isUpdate)
                {
                    var lstExistingCategory = db.MSW002.Where(i => i.HeaderId == objMSW001.HeaderId).ToList();// sourceQCP.SourceCategories;
                    if (lstExistingCategory.Any())
                    {
                        db.MSW002.RemoveRange(lstExistingCategory);
                        db.SaveChanges();
                    }

                    #region MSW Header Copy To Existing Header

                    objMSW001.Document = sourceHeader.Document;
                    objMSW001.Customer = sourceHeader.Customer;
                    objMSW001.RevNo = sourceHeader.RevNo;
                    objMSW001.CDD = sourceHeader.CDD;
                    objMSW001.Product = sourceHeader.Product;
                    objMSW001.ProcessLicensor = sourceHeader.ProcessLicensor;
                    objMSW001.EditedBy = objClsLoginInfo.UserName;
                    objMSW001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objMSW001.HeaderId, objMSW001.Status, objMSW001.RevNo, objMSW001.Project, objMSW001.Document);
                    #endregion
                }
                else
                {
                    #region QCP Header To Create New Header

                    objMSW001.RevNo = 0;
                    objMSW001.Document = sourceHeader.Document;
                    objMSW001.Customer = sourceHeader.Customer;
                    objMSW001.RevNo = sourceHeader.RevNo;
                    objMSW001.CDD = sourceHeader.CDD;
                    objMSW001.Product = sourceHeader.Product;
                    objMSW001.ProcessLicensor = sourceHeader.ProcessLicensor;
                    objMSW001.Status = "Draft";
                    objMSW001.CreatedBy = objClsLoginInfo.UserName;
                    objMSW001.CreatedOn = DateTime.Now;
                    db.MSW001.Add(objMSW001);
                    db.SaveChanges();
                    Manager.UpdatePDN002(objMSW001.HeaderId, objMSW001.Status, objMSW001.RevNo, objMSW001.Project, objMSW001.Document);
                    #endregion
                }

                #region Add lines

                var sourceCategory = sourceMSW.SourceCategories.Where(i => i.HeaderId == sourceHeader.HeaderId).ToList();

                foreach (var item in sourceCategory)
                {
                    MSW002 objMSW002 = new MSW002();
                    objMSW002.HeaderId = objMSW001.HeaderId;
                    objMSW002.Project = objMSW001.Project;
                    objMSW002.Document = objMSW001.Document;
                    objMSW002.RevNo = objMSW001.RevNo;
                    objMSW002.ReferenceDrawing = item.ReferenceDrawing;
                    objMSW002.MainTask = item.MainTask;
                    objMSW002.MachineActivity = item.MachineActivity;
                    objMSW002.MachineRequired = item.MachineRequired;
                    objMSW002.Quantity = item.Quantity;
                    objMSW002.Location = item.Location;
                    objMSW002.TaskManager = item.TaskManager;
                    objMSW002.CreatedBy = objClsLoginInfo.UserName;
                    objMSW002.CreatedOn = DateTime.Now;

                    db.MSW002.Add(objMSW002);
                    db.SaveChanges();
                }

                #endregion

                objResponse.Key = true;
                objResponse.Value = "MSW Copied Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponse.Key = false;
                objResponse.Value = ex.Message;
            }

            return objResponse;
        }
        [HttpPost]
        public ActionResult GetHistoryView(int headerid = 0)
        {
            MSW001_Log objMSW001 = new MSW001_Log();
            Session["Headerid"] = Convert.ToInt32(headerid);
            return PartialView("getHistoryPartial", objMSW001);
        }

        [HttpPost]
        public JsonResult LoadMSWHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                //var user = objClsLoginInfo.UserName;

                string strWhere = string.Empty;
                // string strWhere = string.Empty;

                //   strWhere +=  "CreatedBy=" + user + " and ApprovedBy=" + user;
                strWhere += "1=1 and HeaderId=" + Convert.ToInt32(Session["Headerid"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                         + "%' or Document like '%" + param.sSearch
                         + "%' or Customer like '%" + param.sSearch
                         + "%' or RevNo like '%" + param.sSearch
                         + "%' or Product like '%" + param.sSearch
                         + "%' or ProcessLicensor like '%" + param.sSearch
                         + "%' or Status like '%" + param.sSearch + "%')";
                }

                var lstResult = db.SP_MSW_GET_HISTORY_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               // Convert.ToString(uc.Customer),
                              Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                             //  Convert.ToDateTime(uc.CDD).ToString("dd/MM/yyyy"),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                                Convert.ToString(uc.SubmittedBy),
                                Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                Convert.ToString(uc.ApprovedBy),
                                Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.HeaderId),
                              Convert.ToString(db.MSW001_Log.FirstOrDefault(q => (q.HeaderId == uc.HeaderId && q.RevNo == uc.RevNo)).Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult LoadMSWLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                Session["Headerid"] = Convert.ToInt32(headerid);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (SectionDescription like '%" + param.sSearch
                        + "%' or FurnaceCharge like '%" + param.sSearch
                        + "%' or Height like '%" + param.sSearch
                        + "%' or Width like '%" + param.sSearch
                        + "%' or Length like '%" + param.sSearch
                        + "%' or Weight like '%" + param.sSearch
                        + "%' or Furnace like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_MSW_GET_HISTORY_LINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.ROW_NO),
                           Convert.ToString(uc.MainTask),
                           Convert.ToString(uc.MachineActivity) == "" ?    Convert.ToString(uc.MachineActivity) :  "<ul><li>" +  Convert.ToString(uc.MachineActivity).Replace("|", "</li><li>").ToString()+"</li></ul>",
                           Convert.ToString(uc.Quantity),
                           Convert.ToString(uc.MachineRequired),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.ReferenceDrawing),
                           Convert.ToString(uc.TaskManager)
                           }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [SessionExpireFilter]
        public ActionResult getHistoryDetail(int Id = 0)
        {
            MSW001_Log objMSW001 = new MSW001_Log();
            objMSW001 = db.MSW001_Log.Where(x => x.Id == Id).FirstOrDefault();
            var user = objClsLoginInfo.UserName;
            if (objMSW001 != null)
            {
                var lstProjectDesc = (from a in db.COM001
                                      select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
                ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

                List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
                ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                var approver = (from t1 in db.ATH001
                                join t2 in db.COM003 on t1.Employee equals t2.t_psno
                                where t2.t_actv == 1
                                select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
                ViewBag.approver = new SelectList(approver, "Employee", "Desc");
                if (Id > 0)
                {
                    var project = (from a in db.MSW001_Log
                                   where a.Id == Id
                                   select a.Project).FirstOrDefault();
                    ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
                }
            }
            var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.MSW001_Log.Any(q => (q.HeaderId == objMSW001.HeaderId && q.RevNo == (objMSW001.RevNo - 1))) ? urlPrefix + db.MSW001_Log.Where(q => (q.HeaderId == objMSW001.HeaderId && q.RevNo == (objMSW001.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.MSW001_Log.Any(q => (q.HeaderId == objMSW001.HeaderId && q.RevNo == (objMSW001.RevNo + 1))) ? urlPrefix + db.MSW001_Log.Where(q => (q.HeaderId == objMSW001.HeaderId && q.RevNo == (objMSW001.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return View(objMSW001);
        }

        [HttpPost]
        public ActionResult getCodeValue(string project, string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {

                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult verifyApprover(string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                approver = approver.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == approver && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult isLineAvailable(string project)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                project = project.Split('-')[0].Trim();
                var Exists = db.MSW002.Any(x => x.Project == project);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult retrack(string headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int hd = Convert.ToInt32(headerid);
                if (hd > 0)
                {
                    MSW001 objMSW001 = db.MSW001.Where(x => x.HeaderId == hd).FirstOrDefault();
                    objMSW001.SubmittedOn = null;
                    objMSW001.SubmittedBy = null;

                    objMSW001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    Manager.UpdatePDN002(objMSW001.HeaderId, objMSW001.Status, objMSW001.RevNo, objMSW001.Project, objMSW001.Document);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateData(int LineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!string.IsNullOrEmpty(columnName))
                {
                    db.SP_MSW_LINE_UPDATE(LineId, columnName, columnValue);
                    db.SP_MSW_LINE_UPDATE(LineId, "EditedBy", objClsLoginInfo.UserName);
                    //ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.LineUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReviseData(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                MSW001 objMSW001 = db.MSW001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objMSW001.Status.ToLower() == clsImplementationEnum.CTQStatus.Approved.GetStringValue().ToLower())
                {
                    objMSW001.RevNo += 1;
                    objMSW001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    objMSW001.EditedBy = objClsLoginInfo.UserName;
                    objMSW001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                objResponseMsg.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                objResponseMsg.RevNo = "R" + objMSW001.RevNo;
                objResponseMsg.HeaderId = objMSW001.HeaderId;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_MSW_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer= Convert.ToString(Manager.GetCustomerCodeAndNameByProject(li.Project)),
                                      CDD = Convert.ToDateTime(li.CDD).ToString("dd/MM/yyyy"),
                                      Product=Convert.ToString(li.Product),
                                      ProcessLicensor=Convert.ToString(li.ProcessLicensor),
                                      RevNo=Convert.ToString("R" + li.RevNo),
                                      Status=Convert.ToString(li.Status),
                                      SubmittedBy=Convert.ToString(li.SubmittedBy),
                                      SubmittedOn=Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy=Convert.ToString(li.ApprovedBy),
                                      ApprovedOn= Convert.ToString(Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy")),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_MSW_GET_LINEDETAILS(1, int.MaxValue, strSortOrder,headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      MainTask =li.MainTask,
                                      MachineActivity=li.MachineActivity,
                                      Quantity=li.Quantity,
                                      MachineRequired=li.MachineRequired,
                                      Location=li.Location,
                                      ReferenceDrawing=li.ReferenceDrawing,
                                      TaskManager=li.TaskManager                                   

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GenerateExcelHistroy(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_MSW_GET_HISTORY_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(li.Project)),
                                      CDD = Convert.ToDateTime(li.CDD).ToString("dd/MM/yyyy"),
                                      Product = Convert.ToString(li.Product),
                                      ProcessLicensor = Convert.ToString(li.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + li.RevNo),
                                      Status = Convert.ToString(li.Status),
                                      SubmittedBy = Convert.ToString(li.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy = Convert.ToString(li.ApprovedBy),
                                      ApprovedOn = Convert.ToString(Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy")),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else
                {
                    int headerid = Convert.ToInt32(Session["Headerid"]);
                    var lst = db.SP_MSW_GET_HISTORY_LINEDETAILS(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      MainTask = li.MainTask,
                                      MachineActivity = li.MachineActivity,
                                      Quantity = li.Quantity,
                                      MachineRequired = li.MachineRequired,
                                      Location = li.Location,
                                      ReferenceDrawing = li.ReferenceDrawing,
                                      TaskManager = li.TaskManager

                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }

    public class ResponceMsgWithStatus : clsHelper.ResponseMsg
    {
        public string status;
        public string Revision;
        public string projdesc;
        public string appdesc;
        public int HeaderId;
        public int? RevNo;
        public string contractDesc;
    }
    public class CustomResponceMsg : clsHelper.ResponseMsg
    {
        public string Project;
        public string Customer;
        public int HeaderID;
        public int LineID;
        public string Status;
        public string CDD;
        public int? rev;
    }

}