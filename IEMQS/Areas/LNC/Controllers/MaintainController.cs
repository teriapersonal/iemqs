﻿using IEMQS.Areas.LNC.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Entity;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.LNC.Controllers
{
    /// <summary>
    /// Area Cloned from "IMB" and Modified by M. Ozair Khatri (90385894)
    /// 
    /// First Commit - 28-07-2017
    /// Commit - 90385894 - 01-08-2017 - Added autocomplete, changed primary key to HeaderId and LineId.
    /// Commit - 90385894 - 08-08-2017 - Added code to delete uploaded files if no attachments exist, Documents moved to clsUploadKM.
    /// </summary>
    public class MaintainController : clsBase
    {
        #region ViewMethods

        [SessionExpireFilter, AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        public ActionResult AddHeader(string Project)
        {
            LNC001 objLNC = new LNC001();
            try
            {
                string loginUser = objClsLoginInfo.UserName;
                List<COM001> project = (from li in db.COM001
                                        select li).ToList();

                if (!string.IsNullOrWhiteSpace(Project))
                {
                    objLNC = db.LNC001.FirstOrDefault(x => (x.Project.Equals(Project)));
                    ViewBag.Action = "edit";
                }
                else
                {
                    objLNC.Project = "";
                    objLNC.DocumentNo = "";
                    ViewBag.Project = new SelectList(project, "t_cprj", "t_dsca");
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return View(objLNC);
        }

        [SessionExpireFilter]
        public async Task<ActionResult> EditHeader(string HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (string.IsNullOrWhiteSpace(HeaderId))
                    throw new Exception("Invalid Request");
                int headerId = Int32.Parse(HeaderId);
                LNC001 objLNC001 = await db.LNC001.FirstOrDefaultAsync(x => x.HeaderId == headerId);
                if (objLNC001 != null)
                {
                    /*if (objLNC001.CreatedBy != objClsLoginInfo.UserName)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Document is created by someone else";
                    }
                    else
                    {*/
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Header already Exists.";
                    ViewBag.Action = "edit";
                    ViewBag.ProjectSelect = (from li in db.COM001
                                             where li.t_cprj.Equals(objLNC001.Project)
                                             select li.t_cprj + "-" + li.t_dsca).FirstOrDefault().ToString();
                    objLNC001.Customer = Manager.GetCustomerCodeAndNameByProject(objLNC001.Project);

                    var PlanningDinID = db.PDN002.Where(x => x.RefId == headerId && x.DocumentNo == objLNC001.DocumentNo).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                    ViewBag.PlanningDinID = PlanningDinID;

                    return View("AddHeader", objLNC001);
                    // }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document Not Found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        [SessionExpireFilter]
        public async Task<ActionResult> ViewHeader(string HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (string.IsNullOrWhiteSpace(HeaderId))
                    throw new Exception("Invalid Request");
                int headerId = Int32.Parse(HeaderId);
                LNC001 objLNC001 = await db.LNC001.FirstOrDefaultAsync(x => x.HeaderId == headerId);
                if (objLNC001 != null)
                {
                    /*if (objLNC001.CreatedBy != objClsLoginInfo.UserName)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Document is created by someone else";
                    }
                    else
                    {*/
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Header already Exists.";
                    ViewBag.Action = "view";
                    ViewBag.ProjectSelect = (from li in db.COM001
                                             where li.t_cprj.Equals(objLNC001.Project)
                                             select li.t_cprj + "-" + li.t_dsca).FirstOrDefault().ToString();
                    objLNC001.Customer = Manager.GetCustomerCodeAndNameByProject(objLNC001.Project);
                    return View("AddHeader", objLNC001);
                    //}
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Document Not Found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        public ActionResult GetAllLinesGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetAllLinesGridDataPartial");
        }

        #endregion

        #region DataHandlers

        public JsonResult GetProjects(string term)
        {
            try
            {
                var items = from li in db.COM001
                            where term == null || li.t_cprj.Contains(term) || li.t_dsca.Contains(term)
                            select new
                            {
                                id = li.t_cprj,
                                projectDescription = li.t_cprj + " - " + li.t_dsca
                            };
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetProductTypesAutoComplete(string term)
        {
            var vals = clsImplementationEnum.getProductCategory();
            var filtered = vals.Where(q => term == null || q.ToLower().Contains(term.ToLower())).ToList();
            return Json(filtered, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetEquipmentCategoryAutoComplete(string term, string Product)
        {
            var vals = clsImplementationEnum.getEquipmentCategory(Product);
            var filtered = vals.Where(q => term == null || q.ToLower().Contains(term.ToLower())).ToList();
            return Json(filtered, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetProjectsAutoComplete(string term)
        {
            var vals = clsImplementationEnum.getProductCategory();
            var filtered = vals.Where(q => term == null || q.ToLower().Contains(term.ToLower())).ToList();
            return Json(filtered, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetProductTypes(string search)
        {
            try
            {
                var vals = clsImplementationEnum.getProductCategory().AsEnumerable();//LNCDataHelper.ProductType.AsEnumerable();
                if (!string.IsNullOrWhiteSpace(search))
                    vals = vals.Where(q => q.ToLower().Contains(search.ToLower()));
                var items = vals.Select(q => new
                {
                    id = q,
                    text = q
                }).AsQueryable();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult loadHeaderDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;


                //var list = db.LNC001.ToList();
                string whereCondition = "where 1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition = whereCondition +
                        " and (lnc1.Project+' - '+com1.t_dsca) like '%" + param.sSearch + "%' or " +
                        "lnc1.DocumentNo like '%" + param.sSearch + "%' or " +
                        "(Customer+' - '+com6.t_nama) like '%" + param.sSearch + "%' or " +
                        "Product like '%" + param.sSearch + "%' or " +
                        "ProcessLicensor like '%" + param.sSearch + "%' " +
                        //"CreatedBy like '%" + param.sSearch + "%' or " +
                        //"Activity like '%" + param.sSearch + "%' or " +
                        //"Learning like '%" + param.sSearch + "%' or " +
                        //"JobNo like '%" + param.sSearch + "%' or " +
                        //"CorrectiveAction like '%" + param.sSearch + "%' or " +
                        //"RootCause like '%" + param.sSearch + "%' or " +
                        //"PreventiveAction like '%" + param.sSearch + "%'" +
                        " ";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var data = db.SP_LNC_GETHEADER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = data.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in data
                          select new[] {

                           Convert.ToString(h.Project),
                           Convert.ToString(h.DocumentNo),
                           Convert.ToString(h.Customer),
                           h.CDD == DateTime.MinValue ? " ": h.CDD.ToString("dd-MMM-yyyy"),
                           Convert.ToString(h.Product),
                           Convert.ToString(h.ProcessLicensor),
                           Convert.ToString(true),
                            Convert.ToString(h.HeaderId),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult loadAllLinesDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;


                //var list = db.LNC001.ToList();
                string whereCondition = "where 1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition =
                        whereCondition + " and (lnc1.Project+' - '+com1.t_dsca) like '%" + param.sSearch + "%' or " +
                        "lnc1.DocumentNo like '%" + param.sSearch + "%' or " +
                        "(Customer+' - '+com6.t_nama) like '%" + param.sSearch + "%' or " +
                        "Product like '%" + param.sSearch + "%' or " +
                        "ProcessLicensor like '%" + param.sSearch + "%' or " +
                        //"CreatedBy like '%" + param.sSearch + "%' or " +
                        "Activity like '%" + param.sSearch + "%' or " +
                        "Learning like '%" + param.sSearch + "%' or " +
                        "JobNo like '%" + param.sSearch + "%' or " +
                        "CorrectiveAction like '%" + param.sSearch + "%' or " +
                        "RootCause like '%" + param.sSearch + "%' or " +
                        "PreventiveAction like '%" + param.sSearch + "%'" +
                        " ";
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var data = db.SP_LNC_GETALLLINES(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = data.Select(i => i.TotalCount).FirstOrDefault();
                FileUploadController _objFUC = new FileUploadController();
                var res = from h in data
                          select new[] {
                              Convert.ToString(h.Project),
                           Convert.ToString(h.LineId),
                           Convert.ToString(h.HeaderId),
                           //Convert.ToString(h.DocumentNo),
                           //Convert.ToString(Manager.GetCustomerCodeAndNameByProject(h.Project.Split('-')[0].Trim())),
                           //Convert.ToString(h.JobNo),
                           Convert.ToString( h.Activity),
                           Convert.ToString( h.Learning),
                           Convert.ToString( h.PreventiveAction),
                           Convert.ToString( h.RootCause),
                           Convert.ToString( h.CorrectiveAction),
                           //Convert.ToString(true),
                           //getDocumentsKM("LNC002/"+h.LineId.ToString()),
                           "<center>"+
                                //"<a onclick=\"EditHeader('"+(h.HeaderId.ToString())+"')\" title=\"View\" class=\"fa fa-eye\" title=\"View Header\" style=\"cursor:pointer;color:black\" name=\"btnAction\" id=\"btnView\"></a>"+                                
                                "<i id=\"btnDelete\" name=\"btnAction\" Title=\"Delete Header\" class=\"iconspace fa fa-trash-o iconspace\" style=\"cursor:pointer;color:black\" onclick=\"DeleteHeader('"+h.HeaderId+"')\"></i>" +
                               GenerateGridButton(h.LineId, "", "View" + ((h.CreatedBy.Trim() == objClsLoginInfo.UserName.Trim()) ? "/Edit" : "") + " Attachments", "fa fa-paperclip " + (_objFUC.CheckAnyDocumentsExits("LNC002",h.LineId) ? "hasattachment" : ""), "showAttachments(this, " + h.LineId + "," + (h.CreatedBy.Trim() != objClsLoginInfo.UserName.Trim()).ToString().ToLower() + ")") +
                            "</center>"
                    };

                //GenerateGridButton(h.LineId, "", "View"+((h.CreatedBy.Trim() == objClsLoginInfo.UserName.Trim()) ? "/Edit":"")+" Attachments", "fa fa-paperclip "+((new clsFileUpload()).CheckAnyFileUploadInFolder("LNC002/"+h.LineId.ToString())?"hasattachment":""), "showAttachments(this, "+ h.LineId + ","+(h.CreatedBy.Trim() != objClsLoginInfo.UserName.Trim()).ToString().ToLower()+")")+
                
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='iconspace ' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public ActionResult loadLinesDataTable(JQueryDataTableParamModel param, string HeaderId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;


                string whereCondition = "where HeaderId=" + HeaderId + " ";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition = "and SrNo like '%" + param.sSearch + "%' or Activity like '%" + param.sSearch + "%' or JobNo like '%" + param.sSearch + "%' or Learning like '%" + param.sSearch + "%' or CorrectiveAction like '%" + param.sSearch + "%' or RootCause like '%" + param.sSearch + "%' or PreventiveAction like '%" + param.sSearch + "%'";
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var data = db.SP_LNC_GETLINES(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = data.Select(i => i.TotalCount).FirstOrDefault();

                
                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateHidden(newRecordId, "HeaderId",HeaderId),
                                    Helper.GenerateHidden(newRecordId, "LineId", "0"),
                                    "",
                                    Helper.GenerateTextArea(newRecordId, "JobNo","","",false,"","200"),
                                    Helper.GenerateTextArea(newRecordId, "Activity","","",false,"","200"),
                                    Helper.GenerateTextArea(newRecordId, "Learning","","",false,"","1000"),
                                    Helper.GenerateTextArea(newRecordId, "PreventiveAction","","",false,"","1000"),
                                    Helper.GenerateTextArea(newRecordId, "RootCause","","",false,"","1000"),
                                    Helper.GenerateTextArea(newRecordId, "CorrectiveAction","","",false,"","1000"),
                                    "",
                                    "true",
                                    Helper.GenerateGridButton(newRecordId, "Add", "Add Record", "fa fa-plus", "SaveNewRecord();" )
                                };

                FileUploadController _objFUC = new FileUploadController();
                var res = (from c in data
                           orderby c.SrNo
                           select new[] {
                                 Convert.ToString(c.HeaderId),
                              Convert.ToString(c.LineId),
                                Convert.ToString(c.SrNo),
                                ((c.CreatedBy == objClsLoginInfo.UserName) ?Helper.GenerateTextArea(c.LineId, "JobNo", c.JobNo,"updateData(this, \"JobNo\","+c.LineId.ToString()+")",false,"","200") : Convert.ToString(c.JobNo)),
                                ((c.CreatedBy == objClsLoginInfo.UserName) ?Helper.GenerateTextArea(c.LineId, "Activity",c.Activity,"updateData(this, \"Activity\","+c.LineId.ToString()+")",false,"","200") : Convert.ToString(c.Activity)),
                                ((c.CreatedBy == objClsLoginInfo.UserName) ?Helper.GenerateTextArea(c.LineId, "Learning", c.Learning,"updateData(this, \"Learning\","+c.LineId.ToString()+")",false,"","1000") : Convert.ToString(c.Learning)),
                                ((c.CreatedBy == objClsLoginInfo.UserName) ?Helper.GenerateTextArea(c.LineId, "PreventiveAction", c.PreventiveAction,"updateData(this, \"PreventiveAction\","+c.LineId.ToString()+")",false,"","1000") : Convert.ToString(c.PreventiveAction)),
                                ((c.CreatedBy == objClsLoginInfo.UserName) ?Helper.GenerateTextArea(c.LineId, "RootCause", c.RootCause,"updateData(this, \"RootCause\","+c.LineId.ToString()+")",false,"","1000") : Convert.ToString(c.RootCause)),
                                ((c.CreatedBy == objClsLoginInfo.UserName) ?Helper.GenerateTextArea(c.LineId, "CorrectiveAction", c.CorrectiveAction,"updateData(this, \"CorrectiveAction\","+c.LineId.ToString()+")",false,"","1000") : Convert.ToString(c.CorrectiveAction)),
                                //getDocumentsKM("LNC002/"+c.LineId.ToString()),
                                "",
                                Convert.ToString(c.CreatedBy == objClsLoginInfo.UserName),
                                ((c.CreatedBy == objClsLoginInfo.UserName) ? Helper.HTMLActionString(c.LineId, "", "Delete Record", "fa fa-trash-o", "DeleteLine("+ c.LineId +");"):"")+""+
                                Helper.HTMLActionString(c.LineId, "Attachment", "View" + ((c.CreatedBy == objClsLoginInfo.UserName) ? "/Edit" : "") + " Attachments", "fa fa-paperclip " + (_objFUC.CheckAnyDocumentsExits("LNC002", c.LineId) ? "hasattachment" : ""), "showAttachments(this, " + c.LineId + "," + (c.CreatedBy != objClsLoginInfo.UserName).ToString().ToLower() + ")")
                  }).ToList();

                //Helper.HTMLActionString(c.LineId, "Attachment", "View"+((c.CreatedBy == objClsLoginInfo.UserName) ? "/Edit":"")+" Attachments", "fa fa-paperclip "+((new clsFileUpload()).CheckAnyFileUploadInFolder("LNC002/"+c.LineId.ToString())?"hasattachment":""), "showAttachments(this, "+ c.LineId + ","+(c.CreatedBy != objClsLoginInfo.UserName).ToString().ToLower()+")")
                

                res.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult UpdateData(string Column, string Value, int LineId = 0)
        {
            clsHelper.ResponseMsg objResponse = new clsHelper.ResponseMsg();
            try
            {
                var objLNC002 = db.LNC002.Where(q => q.LineId == LineId).FirstOrDefault();
                if (objLNC002 != null)
                {
                    if (objLNC002.CreatedBy.Equals(objClsLoginInfo.UserName))
                    {
                        if (typeof(LNC002).GetProperty(Column) != null)
                        {
                            if (!string.IsNullOrWhiteSpace(Value))
                            {
                                typeof(LNC002).GetProperty(Column).SetValue(objLNC002, Value);
                                db.Entry(objLNC002).State = EntityState.Modified;
                                db.SaveChanges();
                                objResponse.Key = true;
                                objResponse.Value = "Value Updated";
                            }
                            else
                            {
                                objResponse.Key = false;
                                objResponse.Value = "Field is mandatory, Value not updated!";
                            }
                        }
                        else
                        {
                            objResponse.Key = false;
                            objResponse.Value = "Some error occurred, Value not updated!";
                        }
                    }
                    else
                    {
                        objResponse.Key = false;
                        objResponse.Value = "You did not create this entry, Value not updated!";
                    }
                }
                else
                {
                    objResponse.Key = false;
                    objResponse.Value = "Some error occurred, Value not updated!";
                }
            }
            catch (Exception)
            {
                objResponse.Key = false;
                objResponse.Value = "Some error occurred, Value not updated!";
            }
            return Json(objResponse, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteHeader(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                /*if (string.IsNullOrWhiteSpace(Project))
                    throw new Exception("Invalid Request");*/
                LNC001 objLNC001 = await db.LNC001.FirstOrDefaultAsync(x => x.HeaderId == HeaderId);
                if (objLNC001 != null)
                {
                    if (objLNC001.CreatedBy.Equals(objClsLoginInfo.UserName))
                    {
                        /*var lines = db.LNC002.Where(q => q.HeaderId == HeaderId).Select(q => q.LineId);
                        foreach(var line in lines)
                        {
                            var folderPath = "LNC002";///" + line.ToString();
                            var toDelete = clsUploadKM.getDocs(folderPath);
                            foreach (var item in toDelete)
                                clsUploadKM.DeleteFile(folderPath, item.Key);
                        }
                        Uncomment to delete files (Folders not deleted)
                         */
                        db.LNC001.Remove(objLNC001);
                        await db.SaveChangesAsync();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Entry Deleted Successfully";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "You are not allowed to Delete";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Entry not found!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public async Task<ActionResult> DeleteLine(int LineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                /*if (string.IsNullOrWhiteSpace(Project))
                    throw new Exception("Invalid Request");*/
                LNC002 objLNC002 = await db.LNC002.FirstOrDefaultAsync(x => x.LineId == LineId);
                if (objLNC002 != null)
                {
                    if (objLNC002.CreatedBy.Equals(objClsLoginInfo.UserName))
                    {
                        /*var folderPath = "LNC002/" + LineId.ToString();
                        var toDelete = clsUploadKM.getDocs(folderPath);
                        foreach (var item in toDelete)
                            clsUploadKM.DeleteFile(folderPath, item.Key);

                        Uncomment to delete files (Folders not deleted)
                         */
                        db.LNC002.Remove(objLNC002);
                        await db.SaveChangesAsync();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Entry Deleted Successfully";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Unauthorized Request";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Entry not found!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }
        [HttpPost]
        public ActionResult UpdateEquipement(int headerid, string equipmentcategory)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var obj = db.LNC001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                if (obj != null)
                {
                    obj.EquipmentCategory = equipmentcategory;
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetProjectDetail(string Project)
        {
            LNCProjectDetail objProjectDetail = new LNCProjectDetail();
            LNC001 objLNC = new LNC001();
            if (!string.IsNullOrWhiteSpace(Project))
            {
                if (!db.COM001.Any(q => q.t_cprj.Equals(Project)))
                {
                    objProjectDetail.Condition = "invalid";
                    objProjectDetail.objLNC = null;
                }
                else
                {
                    LNC001 objHeader = db.LNC001.Where(x => (x.Project.Equals(Project))).FirstOrDefault();
                    var count = db.LNC001.Where(x => (x.Project.Equals(Project))).Count();
                    try
                    {
                        if (objHeader != null)
                            objHeader.LNC002 = new List<LNC002>();
                        objProjectDetail.objLNC = objHeader;
                        if (count > 0)
                        {
                            if (true)//(objHeader.CreatedBy.Equals(objClsLoginInfo.UserName))
                            {
                                objProjectDetail.Condition = "edit";
                                objProjectDetail.objLNC = objHeader;
                            }
                            //else
                            //    objProjectDetail.Condition = "otheruser";

                        }
                        else
                        {
                            objProjectDetail.objLNC = new LNC001();
                            objProjectDetail.Condition = "okay";
                        }
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        throw;
                    }
                    if (objProjectDetail.Condition.Equals("okay"))
                    {
                        objProjectDetail.objLNC.Project = Project;
                        objProjectDetail.objLNC.DocumentNo = objClsLoginInfo.Location.Substring(0, 1) + "02_" + Project.Trim().Substring(Project.Trim().Length - 4) + "_PL96";
                        objProjectDetail.objLNC.Customer = Manager.GetCustomerCodeAndNameByProject(Project); //Changed to manager class as suggested by Pooja Desai
                        objProjectDetail.objLNC.CDD = DateTime.Parse(Manager.GetContractWiseCdd(Project));

                    }
                }
            }
            return Json(objProjectDetail, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAllProjectDetails(string Project)
        {
            ResponceMsgProject objResponseMsg = new ResponceMsgProject();
            LNC001 objLNC = new LNC001();
            if (!string.IsNullOrWhiteSpace(Project))
            {
                if (!db.COM001.Any(q => q.t_cprj.Equals(Project)))
                {
                    objResponseMsg.Condition = "invalid";
                }
                else
                {

                    objResponseMsg.IsExist = IsProjectExists(Project);
                    if (objResponseMsg.IsExist)
                    {
                        LNC001 objHeader = db.LNC001.Where(x => (x.Project.Equals(Project))).FirstOrDefault();
                        objResponseMsg.HeaderId = objHeader.HeaderId;
                    }
                    if (!objResponseMsg.IsExist)
                    {
                        objResponseMsg.Project = Project;
                        objResponseMsg.DocumentNo = objClsLoginInfo.Location.Substring(0, 1) + "02_" + Project.Trim().Substring(Project.Trim().Length - 4) + "_PL96";
                        objResponseMsg.Customer = Manager.GetCustomerCodeAndNameByProject(Project); //Changed to manager class as suggested by Pooja Desai
                        objResponseMsg.CDD = Manager.GetContractWiseCdd(Project);
                    }
                }
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public class ResponceMsgProject : clsHelper.ResponseMsg
        {
            public string BU;
            public string Project;
            public string DocumentNo;
            public string Customer;
            public string CDD;
            public string Condition;
            public int HeaderId;
            public bool IsExist;
        }


        [HttpPost]
        public bool IsProjectExists(string project)
        {
            return db.LNC001.Any(x => x.Project == project);
        }
        [HttpPost]
        public ActionResult AddLNCHeader(LNC001 objLNC001)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var existingLNC001 = db.LNC001.Where(q => q.Project.Equals(objLNC001.Project) && q.DocumentNo.Equals(objLNC001.DocumentNo)).FirstOrDefault();
                if (existingLNC001 != null)
                {
                    existingLNC001.Customer = objLNC001.Customer.Split('-')[0].Trim();
                    if (objLNC001.CDD.Year <= 1754)
                        objLNC001.CDD = new DateTime().AddYears(2000);
                    existingLNC001.CDD = objLNC001.CDD;
                    existingLNC001.Product = objLNC001.Product;
                    existingLNC001.EquipmentCategory = objLNC001.EquipmentCategory;
                    existingLNC001.ProcessLicensor = objLNC001.ProcessLicensor;
                    db.Entry(existingLNC001).State = EntityState.Modified;
                    var lines = db.LNC002.Where(q => q.Project.Equals(objLNC001.Project) && q.DocumentNo.Equals(objLNC001.DocumentNo)).ToList();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = db.LNC001.Where(q => q.Project.Equals(objLNC001.Project)).FirstOrDefault().HeaderId.ToString();
                    Manager.AddPDN002(objLNC001.HeaderId, null, null, objLNC001.Project, objLNC001.DocumentNo, "Learning from Project", clsImplementationEnum.PlanList.Learning_Capture.GetStringValue());
                    Manager.UpdatePDN002(objLNC001.HeaderId, "", 0, objLNC001.Project, objLNC001.DocumentNo);
                }
                else
                {
                    try
                    {
                        if (objLNC001.CDD.Year <= 1754)
                            objLNC001.CDD = new DateTime().AddYears(2000);
                        objLNC001.CreatedBy = objClsLoginInfo.UserName;
                        objLNC001.CreatedDate = DateTime.Now;
                        db.LNC001.Add(objLNC001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = db.LNC001.Where(q => q.Project.Equals(objLNC001.Project)).FirstOrDefault().HeaderId.ToString();
                        Manager.AddPDN002(objLNC001.HeaderId, null, null, objLNC001.Project, objLNC001.DocumentNo, "Learning from Project", clsImplementationEnum.PlanList.Learning_Capture.GetStringValue());
                    }
                    catch (Exception ex)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Failed to add to database";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddLNCLine(LNC002 objLNC002, bool hasAttachments, Dictionary<string, string> Attach)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var existingLNC001 = db.LNC001.Where(q => q.Project.Equals(objLNC002.Project)).FirstOrDefault();
                var existingLNC002 = db.LNC002.Where(q => q.LineId == objLNC002.LineId).FirstOrDefault();
                if (existingLNC001 == null)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Header Not found";
                }
                else
                {
                    if (existingLNC002 != null)
                    {
                        existingLNC002.JobNo = objLNC002.JobNo;
                        existingLNC002.Activity = objLNC002.Activity;
                        existingLNC002.Learning = objLNC002.Learning;
                        existingLNC002.CorrectiveAction = objLNC002.CorrectiveAction;
                        existingLNC002.RootCause = objLNC002.RootCause;
                        existingLNC002.PreventiveAction = objLNC002.PreventiveAction;
                        db.Entry(existingLNC002).State = EntityState.Modified;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Entry Updated";
                        var folderPath = "LNC002/" + existingLNC002.LineId.ToString();
                        ManageDocumentsKM(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        try
                        {
                            objLNC002.DocumentNo = "H02_" + objLNC002.Project.Substring(objLNC002.Project.Length - 4) + "_PL56";
                            objLNC002.HeaderId = existingLNC001.HeaderId;
                            objLNC002.CreatedBy = objClsLoginInfo.UserName;
                            objLNC002.CreatedOn = DateTime.Now;
                            db.LNC002.Add(objLNC002);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Entry Added";
                            var folderPath = "LNC002/" + getLineFromLineObject(objLNC002);
                            ManageDocumentsKM(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                        }
                        catch (Exception e)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Failed to add to database";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveAttachments(bool hasAttachments, Dictionary<string, string> Attach, int LineId = 0)
        {
            var objResult = new clsHelper.ResponseMsg();
            try
            {
                if (db.LNC002.Any(q => q.LineId == LineId))
                {
                    var objLNC002 = db.LNC002.Where(q => q.LineId == LineId).FirstOrDefault();
                    if (objLNC002.CreatedBy == objClsLoginInfo.UserName)
                    {
                        var folderPath = "LNC002/" + objLNC002.LineId.ToString();
                        ManageDocumentsKM(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                        objResult.Key = true;
                        objResult.Value = "Saved Successfully.";
                    }
                    else
                    {
                        objResult.Key = false;
                        objResult.Value = "You cannot modify attachments on this line.";
                    }
                }
                else
                {
                    objResult.Key = false;
                    objResult.Value = "Line not found.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResult.Key = false;
                objResult.Value = "Error while saving attachments";
            }
            return Json(objResult, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AddLines(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int rowindex = 0;
                LNC002 objLNC002 = new LNC002();
                int HeaderId = Convert.ToInt32(fc["HeaderId" + rowindex]);
                var objLNC001 = db.LNC001.Where(q => q.HeaderId.Equals(HeaderId)).FirstOrDefault();
                if (objLNC001 == null)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Header Not found";
                }
                else
                {
                    try
                    {

                        #region Lines
                        //Activity Learning    PreventiveAction RootCause   CorrectiveAction
                        objLNC002.HeaderId = objLNC001.HeaderId;
                        objLNC002.Project = objLNC001.Project;
                        objLNC002.DocumentNo = objLNC001.DocumentNo;
                        var maxnum = db.LNC002.Where(x => x.HeaderId == objLNC001.HeaderId).ToList();
                        objLNC002.SrNo = (maxnum.Count > 0 ? maxnum.Max(x => x.SrNo) : 0) + 1;
                        objLNC002.JobNo = fc["JobNo" + rowindex];
                        objLNC002.Activity = fc["Activity" + rowindex];
                        objLNC002.Learning = fc["Learning" + rowindex];
                        objLNC002.PreventiveAction = fc["PreventiveAction" + rowindex];
                        objLNC002.CorrectiveAction = fc["CorrectiveAction" + rowindex];
                        objLNC002.RootCause = fc["RootCause" + rowindex];
                        objLNC002.CreatedBy = objClsLoginInfo.UserName;
                        objLNC002.CreatedOn = DateTime.Now;
                        db.LNC002.Add(objLNC002);
                        db.SaveChanges();
                        #endregion
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                    }
                    catch (Exception e)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(e);
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region HelperMethods

        [NonAction]
        public int getLineFromLineObject(LNC002 obj)
        {
            return db.LNC002.Where(q => q.Project.Equals(obj.Project) && q.SrNo == obj.SrNo).FirstOrDefault().LineId;
        }

        [NonAction]
        public DateTime GetCDDFromProject(string Project)
        {
            try
            {
                var contract = db.COM005.Where(q => q.t_sprj.Equals(Project)).FirstOrDefault().t_cono;
                return db.COM008.Where(q => q.t_cono.Equals(contract)).FirstOrDefault().t_ccdd;
            }
            catch (Exception) { return new DateTime(); }
        }

        [NonAction]
        public static string ToBase64(string plainText)
        {
            var plainTextBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
            return System.Convert.ToBase64String(plainTextBytes);
        }

        [NonAction]
        public static byte[] FromBase64(string base64EncodedData)
        {
            base64EncodedData = base64EncodedData.Substring(base64EncodedData.IndexOf("base64,") + 7);
            var base64EncodedBytes = System.Convert.FromBase64String(base64EncodedData);
            return base64EncodedBytes;
        }

        [NonAction]
        public string GetCustomerCodeAndNameByProject(string project)
        {
            var result = (from cm004 in db.COM004
                          join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                          join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                          where cm005.t_sprj == project
                          select cm004.t_ofbp + "-" + cm006.t_nama).FirstOrDefault();
            return (result == null) ? " " : result;
        }

        public string GetCustomerCode(string project)
        {
            var result = (from cm004 in db.COM004
                          join cm005 in db.COM005 on cm004.t_cono equals cm005.t_cono
                          join cm006 in db.COM006 on cm004.t_ofbp equals cm006.t_bpid
                          where cm005.t_sprj == project
                          select cm004.t_ofbp).FirstOrDefault();
            return (result == null) ? " " : result;
        }

        [NonAction]
        public List<T> FilterList<T>(List<T> inputList, string Filter)
        {
            var outputList = inputList;
            var props = typeof(T).GetProperties();
            foreach (var property in props)
            {
                outputList = outputList.Where(q => property.GetValue(q).ToString().ToLower().Contains(Filter.ToLower())).ToList();
            }
            return outputList;
        }

        [NonAction]
        public string getProjectDescription(string Project)
        {
            return db.COM001.Where(q => q.t_cprj.Equals(Project)).Select(q => q.t_cprj + " - " + q.t_dsca).FirstOrDefault();
        }

        //[NonAction]
        //public string getDocuments(int LineId)
        //{
        //    string Folder = "LNC002/" + LineId.ToString();
        //    string result = "";
        //    var Files = clsUploadKM.getDocs(Folder);
        //    foreach (var file in Files)
        //    {
        //        result += "<input type=\"hidden\" id=\"" + file.Key + "\" value=\"URL:" + file.Value + "\" class=\"attach\" />";
        //    }
        //    return result;
        //}

        //[NonAction]
        //public static string getDocumentsKM(string folderPath)
        //{
        //    string result = "";
        //    var Files = clsUploadKM.getDocs(folderPath);
        //    foreach (var file in Files)
        //    {
        //        result += "<input type=\"hidden\" id=\"" + file.Name + "\" value=\"URL:" + file.URL + "\" class=\"attach\" />";
        //        if (!string.IsNullOrWhiteSpace(file.Comments))
        //            result += "<input type=\"text\" disabled data-toggle=\"tooltip\" placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"" + file.Comments + "\" title=\"" + file.Comments + "\" />";
        //        else
        //            result += "<input type=\"text\" disabled  placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"\" />";
        //    }
        //    return result;
        //}

        [HttpPost]
        public string getDocumentsArrayKM(string folderPath)
        {
            string result = "";
            var Files = clsUploadKM.getDocs(folderPath);
            foreach (var file in Files)
            {
                result += "<input type=\"hidden\" id=\"" + file.Name + "\" value=\"URL:" + file.URL + "\" class=\"attach\" />";
                if (!string.IsNullOrWhiteSpace(file.Comments))
                    result += "<input type=\"text\" disabled data-toggle=\"tooltip\" placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"" + file.Comments + "\" title=\"" + file.Comments + "\" />";
                else
                    result += "<input type=\"text\" disabled  placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"\" />";

            }
            return result;
        }

        [NonAction]
        public static void ManageDocumentsKM(string folderPath, bool hasAttachments, Dictionary<string, string> Attach, string Uploader)
        {
            if (hasAttachments)
            {
                var existing = clsUploadKM.getDocs(folderPath);
                var toDelete = new Dictionary<string, string>();
                foreach (var item in existing)
                {
                    if (Attach.Where(q => q.Key.Equals(item.Name)).Count() <= 0)
                        toDelete.Add(item.Name, item.URL);
                    else
                    {
                        if (Attach.Where(q => q.Key.Equals(item.Name)).FirstOrDefault().Value.Substring(0, 3) != "URL")
                            toDelete.Add(item.Name, item.URL);
                    }
                }
                foreach (var item in toDelete)
                    clsUploadKM.DeleteFile(folderPath, item.Key);
                var toUpload = Attach.Where(q => !string.IsNullOrWhiteSpace(q.Value)).Where(q => (q.Value.Length <= 3) || (q.Value.Substring(0, 3) != "URL")).ToList();

                foreach (var attch in toUpload)
                {
                    try
                    {
                        var base64Data = attch.Value;
                        var dataBytes = FromBase64(attch.Value);
                        var cmnt = Attach.Any(q => q.Key.Equals("_" + attch.Key));
                        var x = Attach.Where(q => q.Key.Equals("_" + attch.Key)).FirstOrDefault();
                        string comment = (!cmnt) ? null : Attach.Where(q => q.Key.Equals("_" + attch.Key)).FirstOrDefault().Value;
                        clsUploadKM.Upload(attch.Key, dataBytes, folderPath, Uploader, comment);
                    }
                    catch (Exception e) { Elmah.ErrorSignal.FromCurrentContext().Raise(e); }
                }
            }
            else
            {
                var existing = clsUploadKM.getDocs(folderPath);
                foreach (var item in existing)
                    clsUploadKM.DeleteFile(folderPath, item.Name);
            }
        }


        #endregion

        #region Export Excel
        //Excel funtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                { //header grid data
                    var lst = db.SP_LNC_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      DocumentNo = uc.DocumentNo,
                                      Customer = uc.Customer,
                                      CDD = uc.CDD == DateTime.MinValue ? "NA" : uc.CDD.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      Product = uc.Product,
                                      EquipmentCategory = uc.EquipmentCategory,
                                      ProcessLicensor = uc.ProcessLicensor,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                { //GET aLL LINES OR LOAD ALL LINES
                    var lst = db.SP_LNC_GETALLLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = uc.Project,
                                      Activity = uc.Activity,
                                      Learning = uc.Learning,
                                      Correction = uc.PreventiveAction,
                                      RootCause = uc.RootCause,
                                      CorrectiveAction = uc.CorrectiveAction,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.SUB_LINES.GetStringValue())
                {  //line grid data
                    var lst = db.SP_LNC_GETLINES(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from c in lst
                                  select new
                                  {
                                      SrNo = c.SrNo,
                                      JobNo = c.JobNo,
                                      Activity = c.Activity,
                                      Learning = c.Learning,
                                      PreventiveAction = c.PreventiveAction,
                                      RootCause = c.RootCause,
                                      CorrectiveAction = c.CorrectiveAction,
                                      //getDocumentsKM("LNC002//getDocumentsKM("LNC002/" + c.LineId.ToString()),
                                      CreatedBy = c.CreatedBy,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {  //line grid data
                    var lst = db.SP_PLN_LOCKINGCLEAT_LC_LINES_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();


                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No data available";
                    }

                    var newlst = (from uc in lst
                                  select new
                                  {
                                      LCDocument = uc.LCDocument,
                                      JointType = uc.JointType,
                                      LineStatus = uc.LineStatus,
                                      ReturnRemark = uc.ReturnRemark,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        #region ExcelHelper

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
                {
                    ExcelPackage package = new ExcelPackage(upload.InputStream);
                    bool isError;
                    DataSet ds = ToAddLNC(package, out isError);

                    if (!isError)
                    {
                        try
                        {
                            string lncproject = string.Empty;
                            string docno = string.Empty;
                            int headerId = 0;
                            //header data
                            List<int> lstHeaderId = new List<int>();
                            List<string> lstDocumentNo = new List<string>();
                            List<LNC002> lstAddLines = new List<LNC002>();
                            int sr = 0;
                            string wppNumber = string.Empty;
                            foreach (DataRow item in ds.Tables[0].Rows)
                            {
                                #region string Header variables
                                string Project = item.Field<string>("Project"); //1
                                string Product = item.Field<string>("Product Category");
                                string EquipmentCategory = item.Field<string>("Equipment Category");
                                string ProcessLicensor = item.Field<string>("Process Licensor");
                                #endregion

                                #region Line Data
                                string Job = item.Field<string>("Job No");
                                string Component = item.Field<string>("Component/Activity");
                                string ProblemDescription = item.Field<string>("Problem Description");
                                string Correction = item.Field<string>("Correction");
                                string RootCause = item.Field<string>("Root Cause");
                                string CorrectiveAction = item.Field<string>("Corrective Action");
                                #endregion
                                LNC001 objLNC001 = new LNC001();
                                if (!string.IsNullOrWhiteSpace(Project) || !string.IsNullOrWhiteSpace(Product) && !string.IsNullOrWhiteSpace(ProcessLicensor))
                                {
                                    #region Header 

                                    clsHelper.ResponseMsgWithStatus objModelWPP = new clsHelper.ResponseMsgWithStatus();
                                    objLNC001 = db.LNC001.Where(x => x.Project == Project).FirstOrDefault();
                                    if (objLNC001 == null)
                                    {
                                        objLNC001 = db.LNC001.Add(new LNC001
                                        {
                                            Project = Project,
                                            DocumentNo = objClsLoginInfo.Location.Substring(0, 1) + "02_" + Project.Trim().Substring(Project.Trim().Length - 4) + "_PL96",
                                            Customer = GetCustomerCode(Project),
                                            CDD = Convert.ToDateTime(Helper.ToNullIfTooEarlyForDb(GetCDDFromProject(Project))),
                                            Product = Product,
                                            EquipmentCategory = EquipmentCategory,
                                            ProcessLicensor = ProcessLicensor,
                                            CreatedBy = objClsLoginInfo.UserName,
                                            CreatedDate = DateTime.Now
                                        });
                                    }
                                    else
                                    {
                                        objLNC001.Product = Product;
                                        objLNC001.EquipmentCategory = EquipmentCategory;
                                        objLNC001.ProcessLicensor = ProcessLicensor;
                                    }
                                    db.SaveChanges();
                                    var maxnum = db.LNC002.Where(x => x.HeaderId == objLNC001.HeaderId).ToList();
                                    sr = (maxnum.Count > 0 ? maxnum.Max(x => x.SrNo) : 0) + 1;
                                    headerId = objLNC001.HeaderId;
                                    lncproject = objLNC001.Project;
                                    docno = objLNC001.DocumentNo;
                                    lstDocumentNo.Add(objLNC001.DocumentNo);
                                    #endregion
                                }

                                if (!string.IsNullOrWhiteSpace(Job) && !string.IsNullOrWhiteSpace(Component))
                                {
                                    #region Lines
                                    LNC002 objLNC002 = new LNC002();
                                    //Activity Learning    PreventiveAction RootCause   CorrectiveAction
                                    objLNC002.HeaderId = headerId;
                                    objLNC002.Project = lncproject;
                                    objLNC002.DocumentNo = docno;
                                    objLNC002.SrNo = sr;
                                    objLNC002.JobNo = Job;
                                    objLNC002.Activity = Component;
                                    objLNC002.Learning = ProblemDescription;
                                    objLNC002.PreventiveAction = Correction;
                                    objLNC002.CorrectiveAction = CorrectiveAction;
                                    objLNC002.RootCause = RootCause;
                                    objLNC002.CreatedBy = objClsLoginInfo.UserName;
                                    objLNC002.CreatedOn = DateTime.Now;
                                    lstAddLines.Add(objLNC002);
                                    sr++;
                                    #endregion
                                }
                            }
                            db.LNC002.AddRange(lstAddLines);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            if (lstDocumentNo.Count() > 0)
                            {
                                objResponseMsg.Value = "Learning Capture Documents:" + string.Join(",", lstDocumentNo) + " imported successfully";
                            }
                            else
                            {
                                objResponseMsg.Value = "Learning Capture imported successfully";
                            }
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                    else
                    {
                        objResponseMsg = ExportToErrorExcel(ds);
                    }
                    if (ds.Tables.Count == 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please check number of column in excel file";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excel file is not valid";
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        public clsHelper.ResponceMsgWithFileName ExportToErrorExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/LNC/LearningCapture-Error Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();

                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            #region Generate for HEader
                            string Project = item.Field<string>("Project"); //1
                            string Product = item.Field<string>("Product Category");
                            string EquipmentCategory = item.Field<string>("Equipment Category");
                            string ProcessLicensor = item.Field<string>("Process Licensor");
                            string Job = item.Field<string>("Job No");
                            string Component = item.Field<string>("Component/Activity");

                            if ((!string.IsNullOrWhiteSpace(Project) || !string.IsNullOrWhiteSpace(Product) || !string.IsNullOrWhiteSpace(ProcessLicensor)) && !string.IsNullOrWhiteSpace(Job) && !string.IsNullOrWhiteSpace(Component))
                            {
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(10))) { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(10).ToString() + " )"; excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(11))) { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(11).ToString() + " )"; excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(12))) { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>(12).ToString() + " )"; excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }
                                if (!string.IsNullOrWhiteSpace(item.Field<string>(13))) { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>(13).ToString() + " )"; excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }
                            }
                            #endregion

                            #region Common Columns for Lines
                            ///lines
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(14))) { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>(14).ToString() + " )"; excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(15))) { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>(15).ToString() + " )"; excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(16))) { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>(16).ToString() + " )"; excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(17))) { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7) + " (Note: " + item.Field<string>(17).ToString() + " )"; excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(18))) { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8) + " (Note: " + item.Field<string>(18).ToString() + " )"; excelWorksheet.Cells[i, 9].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(19))) { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9) + " (Note: " + item.Field<string>(19).ToString() + " )"; excelWorksheet.Cells[i, 10].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9); }
                            #endregion
                            i++;
                        }

                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public DataSet ToAddLNC(ExcelPackage package, out bool isError)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtLinesExcel = new DataTable();
            isError = false;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtLinesExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtLinesExcel.NewRow();

                    foreach (var cell in row)
                    {
                        newRow[cell.Start.Column - 1] = cell.Text;
                    }

                    dtLinesExcel.Rows.Add(newRow);
                }
                #endregion

                #region Error columns for Header Data
                dtLinesExcel.Columns.Add("ErrorProject", typeof(string)); //1
                dtLinesExcel.Columns.Add("ErrorProductCategory", typeof(string));
                dtLinesExcel.Columns.Add("ErrorEquipmentCategory", typeof(string));
                dtLinesExcel.Columns.Add("ErrorProcessLicensor", typeof(string));
                #endregion

                #region Error columns for Common Line Data
                dtLinesExcel.Columns.Add("ErrorJobNo", typeof(string));
                dtLinesExcel.Columns.Add("ErrorComponentActivity", typeof(string));
                dtLinesExcel.Columns.Add("ErrorProblemDescription", typeof(string));
                dtLinesExcel.Columns.Add("ErrorCorrection", typeof(string));
                dtLinesExcel.Columns.Add("ErrorRootCause", typeof(string));
                dtLinesExcel.Columns.Add("ErrorCorrectiveAction", typeof(string));
                #endregion

                #region Validate 

                foreach (DataRow item in dtLinesExcel.Rows)
                {
                    #region string Header variables
                    string Project = item.Field<string>("Project"); //1
                    string Product = item.Field<string>("Product Category");
                    string EquipmentCategory = item.Field<string>("Equipment Category");
                    string ProcessLicensor = item.Field<string>("Process Licensor");
                    #endregion

                    #region Line Data
                    string Job = item.Field<string>("Job No");
                    string Component = item.Field<string>("Component/Activity");
                    string ProblemDescription = item.Field<string>("Problem Description");
                    string Correction = item.Field<string>("Correction");
                    string RootCause = item.Field<string>("Root Cause");
                    string CorrectiveAction = item.Field<string>("Corrective Action");
                    #endregion

                    var objAccessProjects = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, "").ToList();
                    var lstProjects = (from c1 in db.COM001
                                       where objAccessProjects.Contains(c1.t_cprj)
                                       select c1.t_cprj.ToLower().Trim()).Distinct().ToList();

                    var vals = clsImplementationEnum.getProductCategory();
                    var lstProduct = vals.Select(q => q.ToLower()).ToList();
                    if ((!string.IsNullOrWhiteSpace(Project) || !string.IsNullOrWhiteSpace(Product) || !string.IsNullOrWhiteSpace(ProcessLicensor)) && !string.IsNullOrWhiteSpace(Job) && !string.IsNullOrWhiteSpace(Component))
                    {
                        #region Validate Common Columns in Header
                        if (!lstProjects.Contains(Project.ToLower().Trim()))
                        {
                            item["ErrorProject"] = "Project is not valid"; isError = true;
                        }
                        if (!lstProduct.Contains(Product.ToLower().Trim()))
                        {
                            item["ErrorProductCategory"] = "Product Category is not valid"; isError = true;
                        }
                        if (EquipmentCategory.Length > 100)
                        {
                            item["ErrorEquipmentCategory"] = "Equipment Category have maximum limit of 100 characters"; isError = true;
                        }
                        if (ProcessLicensor.Length > 50)
                        {
                            item["ErrorProcessLicensor"] = "Process Licensor have maximum limit of 20 characters"; isError = true;
                        }
                        #endregion
                    }

                    if (!string.IsNullOrWhiteSpace(Job) && !string.IsNullOrWhiteSpace(Component))
                    {
                        #region Validate Common Columns in Lines

                        if (Job.Length > 200)
                        {
                            item["ErrorJobNo"] = "Job No have maximum limit of 200 characters"; isError = true;
                        }
                        if (!string.IsNullOrWhiteSpace(Component))
                        {
                            if (Component.Length > 200)
                            {
                                item["ErrorComponentActivity"] = "Component/Activity have maximum limit of 200 characters"; isError = true;
                            }
                        }
                        if (!string.IsNullOrWhiteSpace(ProblemDescription))
                        {
                            if (ProblemDescription.Length > 1000)
                            {
                                item["ErrorProblemDescription"] = "Problem Description have maximum limit of 1000 characters"; isError = true;
                            }
                        }
                        else { item["ErrorProblemDescription"] = "Problem Description is Required"; isError = true; }

                        if (!string.IsNullOrWhiteSpace(Correction))
                        {
                            if (Correction.Length > 1000)
                            {
                                item["ErrorCorrection"] = "Correction have maximum limit of 1000 characters"; isError = true;
                            }
                        }
                        else { item["ErrorCorrection"] = "Correction is Required"; isError = true; }

                        if (!string.IsNullOrWhiteSpace(RootCause))
                        {
                            if (RootCause.Length > 1000)
                            {
                                item["ErrorRootCause"] = "Root Cause have maximum limit of 1000 characters"; isError = true;
                            }
                        }
                        else { item["ErrorRootCause"] = "Root Cause is Required"; isError = true; }

                        if (!string.IsNullOrWhiteSpace(CorrectiveAction))
                        {
                            if (CorrectiveAction.Length > 1000)
                            {
                                item["ErrorCorrectiveAction"] = "Corrective Action have maximum limit of 1000 characters"; isError = true;
                            }
                        }
                        else { item["ErrorCorrectiveAction"] = "Corrective Action is Required"; isError = true; }

                        #endregion
                    }
                    else
                    {
                        if (!string.IsNullOrWhiteSpace(Job))
                        {
                            item["ErrorJobNo"] = "Job No is Required"; isError = true;
                        }
                        if (!string.IsNullOrWhiteSpace(Component))
                        {
                            item["ErrorComponentActivity"] = "Component/Activity is Required"; isError = true;
                        }
                    }

                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                return new DataSet();
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtLinesExcel);

            if (ds.Tables.Count == 0)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
            }
            return ds;

        }

        #endregion
    }
}