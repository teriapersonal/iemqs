﻿using System.Web.Mvc;

namespace IEMQS.Areas.LNC
{
    public class LNCAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "LNC";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "LNC_default",
                "LNC/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}