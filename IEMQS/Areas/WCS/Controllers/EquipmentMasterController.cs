﻿using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.WCS.Controllers
{
    public class EquipmentMasterController : clsBase
    {
        clsManager objClsManager = new clsManager();

        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult AddHeader(int id = 0)
        {
            WCS003 model = new WCS003();
            List<AutoCompleteModel> list = objClsManager.GetWCSBUList();
            ViewBag.BUList = (from u in list select new { CatID = u.Value, CatDesc = u.Text }).OrderBy(u => u.CatID).ToList();
            model.Location = objClsLoginInfo.Location;
            if (id > 0)
            {
                model = db.WCS003.Where(u => u.HeaderId == id).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(u => u.t_dtyp == 2 && u.t_dimx == model.BU).Select(u => u.t_dimx + " - " + u.t_desc).FirstOrDefault();
            }
            ViewBag.LocationDesc = db.COM002.Where(u => u.t_dtyp == 1 && u.t_dimx == model.Location).Select(u => u.t_dimx + " - " + u.t_desc).FirstOrDefault();
            return View(model);
        }

        public ActionResult GetEquipment(string term, string BU, string Location)
        {
            var catList = Manager.GetSubCatagories("Equipment", BU, Location);

            if (!string.IsNullOrEmpty(term))
            {
                catList = (from u in catList where u.Code.Trim().ToLower().Contains(term.Trim().ToLower()) select u).ToList();
            }
            var list1 = (from u in catList select new { Value = u.Code, Text = u.Code }).OrderBy(u => u.Value).ToList();
            return Json(list1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProcessLicensor(string term, string BU, string Location)
        {
            var catList = Manager.GetSubCatagories("Process Licensor", BU, Location);

            if (!string.IsNullOrEmpty(term))
            {
                catList = (from u in catList where u.Code.Trim().ToLower().Contains(term.Trim().ToLower()) select u).ToList();
            }
            var list1 = (from u in catList select new { Value = u.Code, Text = u.Code }).OrderBy(u => u.Value).ToList();
            return Json(list1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                string BU = "";
                List<AutoCompleteModel> BUList = objClsManager.GetWCSBUList();
                foreach (var bus in BUList)
                {
                    BU += "'" + bus.Value + "',";
                }
                BU = BU.TrimEnd(',');

                string Location = objClsLoginInfo.Location;

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "BU", "Location", "Equipment", "EquipmentSubCategory", "ProcessLicensor", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }


                var lstResult = db.SP_WCS_GET_EQUIPMENT_MASTER_HEADER(StartIndex, EndIndex, strSortOrder, strWhere, BU, Location).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.Equipment),
                           Convert.ToString(uc.EquipmentSubCategory),
                           Convert.ToString(uc.ProcessLicensor),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #region CRUD Header

        [HttpPost]
        public ActionResult SaveHeader(FormCollection fc)
        {
            string BU = fc["BU"].ToString();
            string Location = fc["Location"].ToString();
            string Equipment = fc["Equipment"].ToString();
            string EquipmentSubCategory = fc["EquipmentSubCategory"].ToString();
            string ProcessLicensor = fc["ProcessLicensor"].ToString();
            int HeaderId = fc["HeaderId"] != "" ? Convert.ToInt32(fc["HeaderId"].ToString()) : 0;

            clsHelper.ResponseMsgWCS objResponseMsg = new clsHelper.ResponseMsgWCS();
            try
            {
                if (HeaderId > 0)
                {
                    if (!db.WCS003.Any(u => u.BU == BU && u.Location == Location && u.Equipment == Equipment && u.HeaderId != HeaderId))
                    {
                        var item = db.WCS003.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                        if (item != null)
                        {
                            item.BU = BU;
                            item.Location = Location;
                            item.Equipment = Equipment;
                            item.EquipmentSubCategory = EquipmentSubCategory;
                            item.ProcessLicensor = ProcessLicensor;
                            item.EditedBy = objClsLoginInfo.UserName;
                            item.EditedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                        objResponseMsg.HeaderId = HeaderId;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
                else
                {
                    if (!db.WCS003.Any(u => u.BU == BU && u.Location == Location && u.Equipment == Equipment))
                    {
                        WCS003 objWCS003 = new WCS003();
                        objWCS003.BU = BU;
                        objWCS003.Location = Location;
                        objWCS003.Equipment = Equipment;
                        objWCS003.EquipmentSubCategory = EquipmentSubCategory;
                        objWCS003.ProcessLicensor = ProcessLicensor;
                        objWCS003.CreatedBy = objClsLoginInfo.UserName;
                        objWCS003.CreatedOn = DateTime.Now;
                        db.WCS003.Add(objWCS003);
                        db.SaveChanges();
                        objResponseMsg.HeaderId = objWCS003.HeaderId;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region CRUD Detail

        [HttpPost]
        public ActionResult LoadLineDataGridPartial()
        {
            WCS004 model = new WCS004();
            return PartialView("_GetLineGridDataPartial", model);
        }

        public ActionResult LoadLineDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int HeaderId = param.Headerid != "" ? Convert.ToInt32(param.Headerid) : 0;
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = " HeaderId=" + param.Headerid;

                #region Datatable Sorting 

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string _urlform = string.Empty;
                string indextype = param.Department;

                #endregion

                string[] columnName = { "PartDescription" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstMCR = db.SP_WCS_GET_EQUIPMENT_MASTER_DETAIL(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstMCR.Select(i => i.TotalCount).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "0",
                                    "0",
                                    Helper.GenerateHTMLTextbox(newRecordId,"PartDescription","","",false,"",false,"200"),
                                    GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveRecord(0);" ),
                                };

                var res = (from h in lstMCR
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Convert.ToString(h.ROW_NO),
                                    Helper.GenerateHTMLTextbox(h.LineId,"PartDescription",h.PartDescription,"UpdateData(this, "+ h.LineId+",true);",false,"",false,"200"),
                                    GenerateGridButton(h.LineId, "Delete", "Delete Record", "fa fa-trash", "DeleteRecord("+h.LineId+");" ),
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isDisabled = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' title='" + buttonTooltip + "' " + (isDisabled ? "disabled" : "") + "  class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> </a>";

            return htmlControl;
        }

        [HttpPost]
        public ActionResult SaveLine(FormCollection fc)
        {
            string PartDescription = fc["PartDescription0"].ToString();
            int HeaderId = fc["HeaderId"] != "" ? Convert.ToInt32(fc["HeaderId"].ToString()) : 0;

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!db.WCS004.Any(u => u.HeaderId == HeaderId && u.PartDescription == PartDescription))
                {
                    WCS004 objWCS004 = new WCS004();
                    objWCS004.HeaderId = HeaderId;
                    objWCS004.PartDescription = PartDescription;
                    objWCS004.CreatedBy = objClsLoginInfo.UserName;
                    objWCS004.CreatedOn = DateTime.Now;
                    db.WCS004.Add(objWCS004);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateLineData(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int LineId = Convert.ToInt32(rowId);
                var objWCS004 = db.WCS004.Where(u => u.LineId == LineId).FirstOrDefault();

                columnValue = columnValue == null ? null : columnValue.Replace("'", "''");

                if (columnName == "PartDescription")
                {
                    if (!db.WCS004.Any(u => u.HeaderId == objWCS004.HeaderId && u.PartDescription == columnValue && u.LineId != LineId))
                    {
                        db.SP_COMMON_TABLE_UPDATE("WCS004", LineId, "LineId", columnName, columnValue, objClsLoginInfo.UserName);
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
                else
                {
                    db.SP_COMMON_TABLE_UPDATE("WCS004", LineId, "LineId", columnName, columnValue, objClsLoginInfo.UserName);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                WCS004 objWCS004 = db.WCS004.Where(x => x.LineId == Id).FirstOrDefault();
                db.WCS004.Remove(objWCS004);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Copy

        [HttpPost]
        public ActionResult LoadCopyDataPartial()
        {
            List<AutoCompleteModel> list = objClsManager.GetWCSBUList();
            ViewBag.BUList = (from u in list select new { CatID = u.Value, CatDesc = u.Text }).OrderBy(u => u.CatID).ToList();
            ViewBag.Location = objClsLoginInfo.Location;
            ViewBag.LocationDesc = db.COM002.Where(u => u.t_dtyp == 1 && u.t_dimx == objClsLoginInfo.Location).Select(u => u.t_dimx + " - " + u.t_desc).FirstOrDefault();

            return PartialView("_CopyDataPartial");
        }

        [HttpPost]
        public ActionResult CopyData(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int HeaderId = fc["HeaderId"] != "" ? Convert.ToInt32(fc["HeaderId"].ToString()) : 0;
                string BU = fc["BU_Copy"].ToString().Trim();
                string Location = fc["Location_Copy"].ToString().Trim();
                string Equipment = fc["Equipment_Copy"].ToString().Trim();

                //copy logic here
                if (!db.WCS003.Any(u => u.BU == BU && u.Location == Location && u.Equipment == Equipment))
                {
                    WCS003 objOldWCS003 = db.WCS003.Where(u => u.HeaderId == HeaderId).FirstOrDefault();

                    //copy header                     
                    WCS003 objWCS003 = new WCS003();
                    objWCS003.BU = BU;
                    objWCS003.Location = Location;
                    objWCS003.Equipment = Equipment;
                    objWCS003.EquipmentSubCategory = objOldWCS003.EquipmentSubCategory;
                    objWCS003.ProcessLicensor = objOldWCS003.ProcessLicensor;
                    objWCS003.CreatedBy = objClsLoginInfo.UserName;
                    objWCS003.CreatedOn = DateTime.Now;
                    db.WCS003.Add(objWCS003);
                    db.SaveChanges();
                    int NewHeaderId = objWCS003.HeaderId;

                    //copy lines
                    List<WCS004> objWCS004List = new List<WCS004>();
                    var list = db.WCS004.Where(u => u.HeaderId == HeaderId).ToList();
                    list.ForEach(u =>
                    {
                        objWCS004List.Add(new WCS004
                        {
                            HeaderId = NewHeaderId,
                            PartDescription = u.PartDescription,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        });
                    });
                    db.WCS004.AddRange(objWCS004List);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.CopyRecord.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Import Excel

        [HttpPost]
        public ActionResult LoadImportDataPartial()
        {
            return PartialView("_UploadExcelPartial");
        }

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                string Location = objClsLoginInfo.Location;

                bool isError = false;
                DataSet ds = GetDataFromExcel(package, out isError);
                if (!isError)
                {
                    try
                    {
                        //header data
                        List<int> lstHeaderId = new List<int>();
                        List<WCS004> lstWCS004 = new List<WCS004>();
                        int NewHeaderid = 0;

                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("BU")) && !string.IsNullOrWhiteSpace(item.Field<string>("Location")) && !string.IsNullOrWhiteSpace(item.Field<string>("Equipment")))
                            {
                                #region Header 

                                WCS003 objWCS003 = new WCS003();
                                objWCS003.BU = item.Field<string>("BU");
                                objWCS003.Location = item.Field<string>("Location");
                                objWCS003.Equipment = item.Field<string>("Equipment");
                                objWCS003.EquipmentSubCategory = item.Field<string>("Equipment Sub Category");
                                objWCS003.ProcessLicensor = item.Field<string>("Process Licensor");
                                objWCS003.CreatedBy = objClsLoginInfo.UserName;
                                objWCS003.CreatedOn = DateTime.Now;
                                db.WCS003.Add(objWCS003);
                                db.SaveChanges();
                                NewHeaderid = objWCS003.HeaderId;

                                #endregion
                            }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>("Part Description")))
                            {
                                #region Lines

                                WCS004 objWCS004 = new WCS004();
                                objWCS004.HeaderId = NewHeaderid;
                                objWCS004.PartDescription = item.Field<string>("Part Description");
                                objWCS004.CreatedBy = objClsLoginInfo.UserName;
                                objWCS004.CreatedOn = DateTime.Now;
                                lstWCS004.Add(objWCS004);

                                #endregion
                            }
                        }

                        if (lstWCS004.Count > 0)
                        {
                            db.WCS004.AddRange(lstWCS004);
                            db.SaveChanges();
                        }
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Equipment wise Standard Bill of Material imported successfully";
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                else
                {
                    objResponseMsg = ErrorExportToExcel(ds);
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Excel file is not valid";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public DataSet GetDataFromExcel(ExcelPackage package, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtLinesExcel = new DataTable();
            isError = false;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtLinesExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtLinesExcel.NewRow();

                    foreach (var cell in row)
                    {
                        newRow[cell.Start.Column - 1] = cell.Text;
                    }

                    dtLinesExcel.Rows.Add(newRow);
                }
                #endregion

                #region Error columns for Header Data

                dtLinesExcel.Columns.Add("BUErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("LocationErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("EquipmentErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("EquipmentSubCategoryErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("ProcessLicensorErrorMsg", typeof(string));

                #endregion

                #region Error columns for Line Data

                dtLinesExcel.Columns.Add("PartDescriptionErrorMsg", typeof(string));

                #endregion

                #region Validation 

                List<AutoCompleteModel> lstBUList = objClsManager.GetWCSBUList();
                List<GLB002> lstEquipmentList = objClsManager.GetSubCatagories("Equipment");
                List<GLB002> lstProcessLicensorList = objClsManager.GetSubCatagories("Process Licensor");

                List<WCS003> lstExistingWCS003 = db.WCS003.ToList();

                List<WCS003> lstSheetWCS003 = new List<WCS003>();
                List<WCS004> lstSheetWCS004 = new List<WCS004>();

                foreach (DataRow item in dtLinesExcel.Rows)
                {
                    #region string variables

                    string errorMessage = string.Empty;

                    string BU = item.Field<string>("BU");
                    string Location = item.Field<string>("Location");
                    string Equipment = item.Field<string>("Equipment");
                    string EquipmentSubCategory = item.Field<string>("Equipment Sub Category");
                    string ProcessLicensor = item.Field<string>("Process Licensor");

                    string PartDescription = item.Field<string>("Part Description");

                    #endregion

                    #region Validate Header

                    if (!string.IsNullOrWhiteSpace(BU) || !string.IsNullOrWhiteSpace(Location) || !string.IsNullOrWhiteSpace(Equipment))
                    {
                        if (string.IsNullOrWhiteSpace(BU))
                        {
                            item["BUErrorMsg"] = "BU is Required"; isError = true;
                        }
                        else
                        {
                            if (!lstBUList.Any(u => u.Value.Trim().ToLower() == BU.Trim().ToLower()))
                            {
                                item["BUErrorMsg"] = "BU is not belongs to User"; isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(Location))
                        {
                            item["LocationErrorMsg"] = "Location is Required"; isError = true;
                        }
                        else
                        {
                            if (objClsLoginInfo.Location.Trim().ToLower() != Location.Trim().ToLower())
                            {
                                item["LocationErrorMsg"] = "Location is not belongs to User"; isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(Equipment))
                        {
                            item["EquipmentErrorMsg"] = "Equipment is Required"; isError = true;
                        }
                        else
                        {
                            if (!lstEquipmentList.Any(u => u.BU.Trim().ToLower() == BU.Trim().ToLower() && u.Location.Trim().ToLower() == Location.Trim().ToLower() &&
                            u.Code.Trim().ToLower() == Equipment.Trim().ToLower()))
                            {
                                item["EquipmentErrorMsg"] = "Equipment is not belongs to BU and Location"; isError = true;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(EquipmentSubCategory))
                        {
                            if (EquipmentSubCategory.Length > 100)
                            {
                                item["EquipmentSubCategoryErrorMsg"] = "Equipment Sub Category have maximum limit of 100 characters"; isError = true;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(ProcessLicensor))
                        {
                            if (!lstProcessLicensorList.Any(u => u.BU.Trim().ToLower() == BU.Trim().ToLower() && u.Location.Trim().ToLower() == Location.Trim().ToLower() &&
                           u.Code.Trim().ToLower() == ProcessLicensor.Trim().ToLower()))
                            {
                                item["ProcessLicensorErrorMsg"] = "Process Licensor is not belongs to BU and Location"; isError = true;
                            }
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(BU) && !string.IsNullOrWhiteSpace(Location) && !string.IsNullOrWhiteSpace(Equipment))
                    {
                        if (lstExistingWCS003.Any(u => u.BU.Trim().ToLower() == BU.Trim().ToLower() && u.Location.Trim().ToLower() == Location.Trim().ToLower() && u.Equipment.Trim().ToLower() == Equipment.Trim().ToLower()))
                        {
                            item["EquipmentErrorMsg"] = "Equipment is Already Exist"; isError = true;
                        }
                        else if (lstSheetWCS003.Any(u => u.BU.Trim().ToLower() == BU.Trim().ToLower() && u.Location.Trim().ToLower() == Location.Trim().ToLower() && u.Equipment.Trim().ToLower() == Equipment.Trim().ToLower()))
                        {
                            item["EquipmentErrorMsg"] = "Equipment is Already Exist in this sheet"; isError = true;
                        }
                        lstSheetWCS003.Add(new WCS003()
                        {
                            BU = BU,
                            Location = Location,
                            Equipment = Equipment
                        });

                        lstSheetWCS004 = new List<WCS004>();
                    }

                    #endregion

                    #region Validate Lines

                    if (string.IsNullOrWhiteSpace(PartDescription))
                    {
                        item["PartDescriptionErrorMsg"] = "Part Description is Required"; isError = true;
                    }
                    else
                    {
                        if (PartDescription.Length > 200)
                        {
                            item["PartDescriptionErrorMsg"] = "Part Description have maximum limit of 200 characters"; isError = true;
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(PartDescription))
                    {
                        if (lstSheetWCS004.Any(u => u.PartDescription.Trim().ToLower() == PartDescription.Trim().ToLower()))
                        {
                            item["PartDescriptionErrorMsg"] = "Part Description is Already Exist in this sheet"; isError = true;
                        }

                        lstSheetWCS004.Add(new WCS004()
                        {
                            PartDescription = PartDescription
                        });
                    }

                    #endregion
                }

                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtLinesExcel);
            return ds;
        }

        public clsHelper.ResponceMsgWithFileName ErrorExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/WCS/Maintain Equipment wise BOM - Error Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();

                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            #region Generate for Header

                            ///BU

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(6)))
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(6).ToString() + " )";
                                excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }

                            ///Location
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(7)))
                            {
                                excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(7).ToString() + " )";
                                excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }

                            ///Equipment
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(8)))
                            {
                                excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>(8).ToString() + " )";
                                excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(9)))
                            {
                                excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>(9).ToString() + " )";
                                excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(10)))
                            {
                                excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>(10).ToString() + " )";
                                excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }

                            #endregion

                            #region Common Columns for Lines
                            ///lines

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(11)))
                            {
                                excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>(11).ToString() + " )";
                                excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }

                            #endregion

                            i++;
                        }

                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        #endregion

        #region Export Excel

        public ActionResult GenerateExcelHeader(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string BU = "";
                List<AutoCompleteModel> BUList = objClsManager.GetWCSBUList();
                foreach (var bus in BUList)
                {
                    BU += "'" + bus.Value + "',";
                }
                BU = BU.TrimEnd(',');

                string Location = objClsLoginInfo.Location;

                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_WCS_GET_EQUIPMENT_MASTER_HEADER(1, int.MaxValue, strSortOrder, whereCondition, BU, Location).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      BU = Convert.ToString(li.BU),
                                      Location = Convert.ToString(li.Location),
                                      Equipment = Convert.ToString(li.Equipment),
                                      EquipmentSubCategory = Convert.ToString(li.EquipmentSubCategory),
                                      ProcessLicensor = Convert.ToString(li.ProcessLicensor),
                                      CreatedBy = Convert.ToString(li.CreatedBy),
                                      CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateExcelLines(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_WCS_GET_EQUIPMENT_MASTER_DETAIL(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var newlst = (from li in lst
                              select new
                              {
                                  SrNo = Convert.ToString(li.ROW_NO),
                                  PartDescription = Convert.ToString(li.PartDescription),
                                  //CreatedBy = Convert.ToString(li.CreatedBy),
                                  //CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}