﻿using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.WCS.Controllers
{
    public class MaterialMasterController : clsBase
    {
        clsManager objClsManager = new clsManager();

        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult AddHeader(int id = 0)
        {
            WCS001 model = new WCS001();
            List<AutoCompleteModel> list = objClsManager.GetWCSBUList();
            ViewBag.BUList = (from u in list select new { CatID = u.Value, CatDesc = u.Text }).OrderBy(u => u.CatID).ToList();
            model.Location = objClsLoginInfo.Location;
            if (id > 0)
            {
                model = db.WCS001.Where(u => u.HeaderId == id).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(u => u.t_dtyp == 2 && u.t_dimx == model.BU).Select(u => u.t_dimx + " - " + u.t_desc).FirstOrDefault();
            }
            ViewBag.LocationDesc = db.COM002.Where(u => u.t_dtyp == 1 && u.t_dimx == model.Location).Select(u => u.t_dimx + " - " + u.t_desc).FirstOrDefault();
            return View(model);
        }

        public ActionResult GetManufacturingCode(string term, string BU, string Location)
        {
            List<GLB002> catList = Manager.GetSubCatagories("Manufacturing Code", BU, Location);

            if (!string.IsNullOrEmpty(term))
            {
                catList = (from u in catList where u.Code.Trim().ToLower().Contains(term.Trim().ToLower()) select u).ToList();
            }
            var list1 = (from u in catList select new { Value = u.Code, Text = u.Code }).OrderBy(u => u.Value).ToList();
            return Json(list1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                string BU = "";
                List<AutoCompleteModel> BUList = objClsManager.GetWCSBUList();
                foreach (var bus in BUList)
                {
                    BU += "'" + bus.Value + "',";
                }
                BU = BU.TrimEnd(',');

                string Location = objClsLoginInfo.Location;

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "BU", "Location", "ManufacturingCode", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }


                var lstResult = db.SP_WCS_GET_MATERIAL_MASTER_HEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere, BU, Location
                                ).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Location),
                           Convert.ToString(uc.ManufacturingCode),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #region CRUD Header

        [HttpPost]
        public ActionResult SaveHeader(FormCollection fc)
        {
            string BU = fc["BU"].ToString();
            string Location = fc["Location"].ToString();
            string ManufacturingCode = fc["ManufacturingCode"].ToString();
            int HeaderId = fc["HeaderId"] != "" ? Convert.ToInt32(fc["HeaderId"].ToString()) : 0;

            clsHelper.ResponseMsgWCS objResponseMsg = new clsHelper.ResponseMsgWCS();
            try
            {
                if (HeaderId > 0)
                {
                    if (!db.WCS001.Any(u => u.BU == BU && u.Location == Location && u.ManufacturingCode == ManufacturingCode && u.HeaderId != HeaderId))
                    {
                        var item = db.WCS001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                        if (item != null)
                        {
                            item.BU = BU;
                            item.Location = Location;
                            item.ManufacturingCode = ManufacturingCode;
                            item.EditedBy = objClsLoginInfo.UserName;
                            item.EditedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                        objResponseMsg.HeaderId = HeaderId;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
                else
                {
                    if (!db.WCS001.Any(u => u.BU == BU && u.Location == Location && u.ManufacturingCode == ManufacturingCode))
                    {
                        WCS001 objWCS001 = new WCS001();
                        objWCS001.BU = BU;
                        objWCS001.Location = Location;
                        objWCS001.ManufacturingCode = ManufacturingCode;
                        objWCS001.CreatedBy = objClsLoginInfo.UserName;
                        objWCS001.CreatedOn = DateTime.Now;
                        db.WCS001.Add(objWCS001);
                        db.SaveChanges();
                        objResponseMsg.HeaderId = objWCS001.HeaderId;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region CRUD Detail

        [HttpPost]
        public ActionResult LoadLineDataGridPartial()
        {
            WCS002 model = new WCS002();
            return PartialView("_GetLineGridDataPartial", model);
        }

        public ActionResult LoadLineDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int HeaderId = param.Headerid != "" ? Convert.ToInt32(param.Headerid) : 0;
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = " HeaderId=" + param.Headerid;

                #region Datatable Sorting 

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                string _urlform = string.Empty;
                string indextype = param.Department;

                #endregion

                string[] columnName = { "Material", "ProductForm", "MaterialSpecificationAndGrade", "UnitofMeasurement", "DensityofMaterial" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lst = db.SP_WCS_GET_MATERIAL_MASTER_DETAIL(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lst.Select(i => i.TotalCount).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "0",
                                    Helper.GenerateHTMLTextbox(newRecordId,"Material","","",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"ProductForm","","",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"MaterialSpecificationAndGrade","","",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(newRecordId,"UnitofMeasurement","","",false,"",false,"50"),
                                    Helper.GenerateNumericTextbox(newRecordId,"DensityofMaterial","","",false,"",false),
                                    GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveRecord(0);" ),
                                };

                var res = (from h in lst
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHTMLTextbox(h.LineId,"Material",h.Material,"UpdateData(this, "+ h.LineId+",true);",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(h.LineId,"ProductForm",h.ProductForm,"UpdateData(this, "+ h.LineId+",true);",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(h.LineId,"MaterialSpecificationAndGrade",h.MaterialSpecificationAndGrade,"UpdateData(this, "+ h.LineId+",true);",false,"",false,"50"),
                                    Helper.GenerateHTMLTextbox(h.LineId,"UnitofMeasurement",h.UnitofMeasurement,"UpdateData(this, "+ h.LineId+",true);",false,"",false,"50"),
                                    Helper.GenerateNumericTextbox(h.LineId, "DensityofMaterial", Convert.ToString(h.DensityofMaterial), "UpdateData(this, "+ h.LineId+",true);",false,"",false , "",""),
                                    GenerateGridButton(h.LineId, "Delete", "Delete Record", "fa fa-trash", "DeleteRecord("+h.LineId+");" ),
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isDisabled = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' title='" + buttonTooltip + "' " + (isDisabled ? "disabled" : "") + "  class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> </a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        [HttpPost]
        public ActionResult SaveLine(FormCollection fc)
        {
            string Material = fc["Material0"].ToString();
            string ProductForm = fc["ProductForm0"].ToString();
            string MaterialSpecificationAndGrade = fc["MaterialSpecificationAndGrade0"].ToString();
            string UnitofMeasurement = fc["UnitofMeasurement0"].ToString();
            string DensityofMaterial = fc["DensityofMaterial0"].ToString();
            int HeaderId = fc["HeaderId"] != "" ? Convert.ToInt32(fc["HeaderId"].ToString()) : 0;

            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //index change as per observationid : 16184
                if (!db.WCS002.Any(u => u.HeaderId == HeaderId && u.Material == Material && u.ProductForm == ProductForm && u.MaterialSpecificationAndGrade == MaterialSpecificationAndGrade
                   && u.UnitofMeasurement == UnitofMeasurement && u.DensityofMaterial.ToString() == DensityofMaterial))
                {
                    WCS002 objWCS002 = new WCS002();
                    objWCS002.HeaderId = HeaderId;
                    objWCS002.Material = Material;
                    objWCS002.ProductForm = ProductForm;
                    objWCS002.MaterialSpecificationAndGrade = MaterialSpecificationAndGrade;
                    objWCS002.UnitofMeasurement = UnitofMeasurement;
                    objWCS002.DensityofMaterial = Convert.ToDecimal(DensityofMaterial);
                    objWCS002.CreatedBy = objClsLoginInfo.UserName;
                    objWCS002.CreatedOn = DateTime.Now;
                    db.WCS002.Add(objWCS002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetLineReferecnceInCalculation(int LineId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (db.WCS008.Any(u => u.ProductForm == LineId))
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = GetHtmlString(LineId);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No reference found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public string GetHtmlString(int LineId)
        {
            StringBuilder sb = new StringBuilder();

            var list = (from a in db.WCS007
                        join b in db.WCS008 on a.HeaderId equals b.HeaderId
                        where b.ProductForm == LineId
                        select a).Distinct().ToList();

            sb.Append("<h4>Weight and CG calculation will be updated for below records. Are you sure want to update? </h4>");
            sb.Append("<br/>");
            sb.Append("<div style=\"max-height:500px;overflow:auto\">");
            sb.Append("<table id=\"tblDetails\" class=\"table table-bordered\">");
            sb.Append("<thead style='background-color:#b1eaef'><tr>");

            sb.Append("<th class=\"min-phone-l\">Sr No</th>" +
                "<th class=\"min-phone-l\">BU</th>" +
            "<th class=\"min-phone-l\">Location</th>" +
            "<th class=\"min-phone-l\">Job No</th>" +
            "<th class=\"min-phone-l\">Document No</th>" +
            "<th class=\"min-phone-l\">Equipment</th>" +
            "<th class=\"min-phone-l\">Equipment No.</th>" +
            "<th class=\"min-phone-l\">Customer</th>");

            sb.Append("</tr></thead>");

            sb.Append("<tbody>");

            int i = 1;
            foreach (var item in list)
            {
                sb.Append("<tr>");

                sb.Append("<td>" + i.ToString() + "</td>");
                sb.Append("<td>" + item.BU + "</td>");
                sb.Append("<td>" + item.Location + "</td>");
                sb.Append("<td>" + item.JobNo + "</td>");
                sb.Append("<td>" + item.DocumentNo + "</td>");
                sb.Append("<td>" + item.Equipment + "</td>");
                sb.Append("<td>" + item.EquipmentNo + "</td>");
                sb.Append("<td>" + item.Customer + "</td>");

                sb.Append("</tr>");

                i++;
            }

            sb.Append("</tbody>");

            sb.Append("</table>");
            sb.Append("</div>");
            return sb.ToString();
        }

        [HttpPost]
        public ActionResult UpdateLineData(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int LineId = Convert.ToInt32(rowId);
                var objWCS002 = db.WCS002.Where(u => u.LineId == LineId).FirstOrDefault();

                columnValue = columnValue == null ? null : columnValue.Replace("'", "''");

                if (columnName == "Material")
                {
                    if (!db.WCS002.Any(u => u.HeaderId == objWCS002.HeaderId && u.Material == columnValue && u.ProductForm == objWCS002.ProductForm
                       && u.MaterialSpecificationAndGrade == objWCS002.MaterialSpecificationAndGrade && u.DensityofMaterial == objWCS002.DensityofMaterial
                       && u.UnitofMeasurement == objWCS002.UnitofMeasurement && u.LineId != LineId))
                    {
                        db.SP_COMMON_TABLE_UPDATE("WCS002", LineId, "LineId", columnName, columnValue, objClsLoginInfo.UserName);
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
                else if (columnName == "ProductForm")
                {
                    if (!db.WCS002.Any(u => u.HeaderId == objWCS002.HeaderId && u.Material == objWCS002.Material && u.ProductForm == columnValue
                       && u.MaterialSpecificationAndGrade == objWCS002.MaterialSpecificationAndGrade && u.DensityofMaterial == objWCS002.DensityofMaterial
                       && u.UnitofMeasurement == objWCS002.UnitofMeasurement && u.LineId != LineId))
                    {
                        db.SP_COMMON_TABLE_UPDATE("WCS002", LineId, "LineId", columnName, columnValue, objClsLoginInfo.UserName);
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
                else if (columnName == "MaterialSpecificationAndGrade")
                {
                    if (!db.WCS002.Any(u => u.HeaderId == objWCS002.HeaderId && u.Material == objWCS002.Material && u.ProductForm == objWCS002.ProductForm
                       && u.MaterialSpecificationAndGrade == columnValue && u.DensityofMaterial == objWCS002.DensityofMaterial
                       && u.UnitofMeasurement == objWCS002.UnitofMeasurement && u.LineId != LineId))
                    {
                        db.SP_COMMON_TABLE_UPDATE("WCS002", LineId, "LineId", columnName, columnValue, objClsLoginInfo.UserName);
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
                else if (columnName == "UnitofMeasurement")
                {
                    if (!db.WCS002.Any(u => u.HeaderId == objWCS002.HeaderId && u.Material == objWCS002.Material && u.ProductForm == objWCS002.ProductForm
                       && u.MaterialSpecificationAndGrade == objWCS002.MaterialSpecificationAndGrade && u.DensityofMaterial == objWCS002.DensityofMaterial
                       && u.UnitofMeasurement == columnValue && u.LineId != LineId))
                    {
                        db.SP_COMMON_TABLE_UPDATE("WCS002", LineId, "LineId", columnName, columnValue, objClsLoginInfo.UserName);
                        objResponseMsg.Key = true;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
                else if (columnName == "DensityofMaterial")
                {
                    if (!db.WCS002.Any(u => u.HeaderId == objWCS002.HeaderId && u.Material == objWCS002.Material && u.ProductForm == objWCS002.ProductForm
                      && u.MaterialSpecificationAndGrade == objWCS002.MaterialSpecificationAndGrade && u.DensityofMaterial.ToString() == columnValue
                      && u.UnitofMeasurement == objWCS002.UnitofMeasurement && u.LineId != LineId))
                    {
                        db.SP_COMMON_TABLE_UPDATE("WCS002", LineId, "LineId", columnName, columnValue, objClsLoginInfo.UserName);

                        var list = (from a in db.WCS007
                                    join b in db.WCS008 on a.HeaderId equals b.HeaderId
                                    where b.ProductForm == LineId
                                    select b).ToList();

                        foreach (var item in list)
                        {
                            decimal VolumeCalculate = item.VolumeCalculate != null ? Convert.ToDecimal(item.VolumeCalculate) : 0;
                            decimal LocationL = item.LocationL != null ? Convert.ToDecimal(item.LocationL) : 0;
                            decimal LocationX = item.LocationX != null ? Convert.ToDecimal(item.LocationX) : 0;

                            decimal UnitWeightofPart = 0;
                            decimal TotalWeightofPart = 0;
                            decimal W_X_L = 0;

                            decimal Qty = item.Qty != null ? Convert.ToDecimal(item.Qty) : 0;
                            decimal DensityofMaterial = columnValue != "" ? Convert.ToDecimal(columnValue) : 0;

                            UnitWeightofPart = Math.Round(DensityofMaterial * VolumeCalculate, 2);
                            TotalWeightofPart = Math.Round(UnitWeightofPart * Qty, 2);
                            W_X_L = Math.Round(TotalWeightofPart * LocationX, 2);

                            item.UnitWeightofPart = UnitWeightofPart;
                            item.TotalWeightofPartW = TotalWeightofPart;
                            item.WXL = W_X_L;
                        }

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                    }
                }
                else
                {
                    db.SP_COMMON_TABLE_UPDATE("WCS002", LineId, "LineId", columnName, columnValue, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                WCS002 objWCS002 = db.WCS002.Where(x => x.LineId == Id).FirstOrDefault();
                if (db.WCS008.Any(u => u.ProductForm == Id))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Line detail used in Weight & CG Calculation. You can't delete it.";
                }
                else if (db.WCS006.Any(u => u.ProductForm == Id))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Line detail used in Formula master. You can't delete it.";
                }
                else
                {
                    db.WCS002.Remove(objWCS002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Copy

        [HttpPost]
        public ActionResult LoadCopyDataPartial()
        {
            List<AutoCompleteModel> list = objClsManager.GetWCSBUList();
            ViewBag.BUList = (from u in list select new { CatID = u.Value, CatDesc = u.Text }).OrderBy(u => u.CatID).ToList();
            ViewBag.Location = objClsLoginInfo.Location;
            ViewBag.LocationDesc = db.COM002.Where(u => u.t_dtyp == 1 && u.t_dimx == objClsLoginInfo.Location).Select(u => u.t_dimx + " - " + u.t_desc).FirstOrDefault();

            return PartialView("_CopyDataPartial");
        }

        [HttpPost]
        public ActionResult CopyData(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int HeaderId = fc["HeaderId"] != "" ? Convert.ToInt32(fc["HeaderId"].ToString()) : 0;
                string BU = fc["BU_Copy"].ToString().Trim();
                string Location = fc["Location_Copy"].ToString().Trim();
                string ManufacturingCode = fc["ManufacturingCode_Copy"].ToString().Trim();

                //copy logic here
                if (!db.WCS001.Any(u => u.BU == BU && u.Location == Location && u.ManufacturingCode == ManufacturingCode))
                {
                    //copy header                     
                    WCS001 objWCS001 = new WCS001();
                    objWCS001.BU = BU;
                    objWCS001.Location = Location;
                    objWCS001.ManufacturingCode = ManufacturingCode;
                    objWCS001.CreatedBy = objClsLoginInfo.UserName;
                    objWCS001.CreatedOn = DateTime.Now;
                    db.WCS001.Add(objWCS001);
                    db.SaveChanges();
                    int NewHeaderId = objWCS001.HeaderId;

                    //copy lines
                    List<WCS002> objWCS002List = new List<WCS002>();
                    var list = db.WCS002.Where(u => u.HeaderId == HeaderId).ToList();
                    list.ForEach(u =>
                    {
                        objWCS002List.Add(new WCS002
                        {
                            HeaderId = NewHeaderId,
                            Material = u.Material,
                            ProductForm = u.ProductForm,
                            MaterialSpecificationAndGrade = u.MaterialSpecificationAndGrade,
                            UnitofMeasurement = u.UnitofMeasurement,
                            DensityofMaterial = u.DensityofMaterial,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        });
                    });
                    db.WCS002.AddRange(objWCS002List);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.CopyRecord.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Import Excel

        [HttpPost]
        public ActionResult LoadImportDataPartial()
        {
            return PartialView("_UploadExcelPartial");
        }

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                string Location = objClsLoginInfo.Location;

                bool isError = false;
                DataSet ds = GetDataFromExcel(package, out isError);
                if (!isError)
                {
                    try
                    {
                        //header data
                        List<int> lstHeaderId = new List<int>();
                        List<WCS002> lstWCS002 = new List<WCS002>();
                        int NewHeaderid = 0;

                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("BU")) && !string.IsNullOrWhiteSpace(item.Field<string>("Location")) && !string.IsNullOrWhiteSpace(item.Field<string>("Manufacturing Code")))
                            {
                                #region Header 

                                WCS001 objWCS001 = new WCS001();
                                objWCS001.BU = item.Field<string>("BU");
                                objWCS001.Location = item.Field<string>("Location");
                                objWCS001.ManufacturingCode = item.Field<string>("Manufacturing Code");
                                objWCS001.CreatedBy = objClsLoginInfo.UserName;
                                objWCS001.CreatedOn = DateTime.Now;
                                db.WCS001.Add(objWCS001);
                                db.SaveChanges();
                                NewHeaderid = objWCS001.HeaderId;

                                #endregion
                            }


                            if (!string.IsNullOrWhiteSpace(item.Field<string>("Material")) && !string.IsNullOrWhiteSpace(item.Field<string>("Product Form")) &&
                                !string.IsNullOrWhiteSpace(item.Field<string>("Material Specification And Grade")) && !string.IsNullOrWhiteSpace(item.Field<string>("Unit of Measurement")) &&
                                !string.IsNullOrWhiteSpace(item.Field<string>("Density of Material")))
                            {
                                #region Lines

                                WCS002 objWCS002 = new WCS002();
                                objWCS002.HeaderId = NewHeaderid;
                                objWCS002.Material = item.Field<string>("Material");
                                objWCS002.ProductForm = item.Field<string>("Product Form"); ;
                                objWCS002.MaterialSpecificationAndGrade = item.Field<string>("Material Specification And Grade"); ;
                                objWCS002.UnitofMeasurement = item.Field<string>("Unit of Measurement");
                                objWCS002.DensityofMaterial = Math.Round(Convert.ToDecimal(item.Field<string>("Density of Material")), 2);
                                objWCS002.CreatedBy = objClsLoginInfo.UserName;
                                objWCS002.CreatedOn = DateTime.Now;
                                lstWCS002.Add(objWCS002);

                                #endregion
                            }
                        }

                        if (lstWCS002.Count > 0)
                        {
                            db.WCS002.AddRange(lstWCS002);
                            db.SaveChanges();
                        }

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Material Master imported successfully";
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                else
                {
                    objResponseMsg = ErrorExportToExcel(ds);
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Excel file is not valid";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public DataSet GetDataFromExcel(ExcelPackage package, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtLinesExcel = new DataTable();
            isError = false;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtLinesExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtLinesExcel.NewRow();

                    foreach (var cell in row)
                    {
                        newRow[cell.Start.Column - 1] = cell.Text;
                    }

                    dtLinesExcel.Rows.Add(newRow);
                }
                #endregion

                #region Error columns for Header Data

                dtLinesExcel.Columns.Add("BUErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("LocationErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("ManufacturingCodeErrorMsg", typeof(string));

                #endregion

                #region Error columns for Line Data

                dtLinesExcel.Columns.Add("MaterialErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("ProductFormErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("MaterialSpecificationAndGradeErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("UnitofMeasurementErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("DensityofMaterialErrorMsg", typeof(string));

                #endregion

                #region Validation 

                List<AutoCompleteModel> lstBUList = objClsManager.GetWCSBUList();
                List<GLB002> lstManufacturingCodeList = objClsManager.GetSubCatagories("Manufacturing Code");

                List<WCS001> lstExistingWCS001 = db.WCS001.ToList();

                List<WCS001> lstSheetWCS001 = new List<WCS001>();
                List<WCS002> lstSheetWCS002 = new List<WCS002>();

                foreach (DataRow item in dtLinesExcel.Rows)
                {
                    #region string variables

                    string errorMessage = string.Empty;

                    string BU = item.Field<string>("BU");
                    string Location = item.Field<string>("Location");
                    string ManufacturingCode = item.Field<string>("Manufacturing Code");

                    string Material = item.Field<string>("Material");
                    string ProductForm = item.Field<string>("Product Form");
                    string MaterialSpecificationAndGrade = item.Field<string>("Material Specification And Grade");
                    string UnitofMeasurement = item.Field<string>("Unit of Measurement");
                    string DensityofMaterial = item.Field<string>("Density of Material");

                    #endregion

                    #region Validate Header

                    if (!string.IsNullOrWhiteSpace(BU) || !string.IsNullOrWhiteSpace(Location) || !string.IsNullOrWhiteSpace(ManufacturingCode))
                    {
                        if (string.IsNullOrWhiteSpace(BU))
                        {
                            item["BUErrorMsg"] = "BU is Required"; isError = true;
                        }
                        else
                        {
                            if (!lstBUList.Any(u => u.Value.Trim().ToLower() == BU.Trim().ToLower()))
                            {
                                item["BUErrorMsg"] = "BU is not belongs to User"; isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(Location))
                        {
                            item["LocationErrorMsg"] = "Location is Required"; isError = true;
                        }
                        else
                        {
                            if (objClsLoginInfo.Location.Trim().ToLower() != Location.Trim().ToLower())
                            {
                                item["LocationErrorMsg"] = "Location is not belongs to User"; isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(ManufacturingCode))
                        {
                            item["ManufacturingCodeErrorMsg"] = "Manufacturing Code is Required"; isError = true;
                        }
                        else
                        {
                            if (!lstManufacturingCodeList.Any(u => u.BU.Trim().ToLower() == BU.Trim().ToLower() &&
                            u.Location.Trim().ToLower() == Location.Trim().ToLower() &&
                            u.Code.Trim().ToLower() == ManufacturingCode.Trim().ToLower()))
                            {
                                item["ManufacturingCodeErrorMsg"] = "Manufacturing Code is not belongs to BU and Location"; isError = true;
                            }
                        }
                    }

                    if (!string.IsNullOrWhiteSpace(BU) && !string.IsNullOrWhiteSpace(Location) && !string.IsNullOrWhiteSpace(ManufacturingCode))
                    {
                        if (lstExistingWCS001.Any(u => u.BU.Trim().ToLower() == BU.Trim().ToLower() && u.Location.Trim().ToLower() == Location.Trim().ToLower() && u.ManufacturingCode.Trim().ToLower() == ManufacturingCode.Trim().ToLower()))
                        {
                            item["ManufacturingCodeErrorMsg"] = "Manufacturing Code is Already Exist"; isError = true;
                        }
                        else if (lstSheetWCS001.Any(u => u.BU.Trim().ToLower() == BU.Trim().ToLower() && u.Location.Trim().ToLower() == Location.Trim().ToLower() && u.ManufacturingCode.Trim().ToLower() == ManufacturingCode.Trim().ToLower()))
                        {
                            item["ManufacturingCodeErrorMsg"] = "Manufacturing Code is Already Exist in this sheet"; isError = true;
                        }
                        lstSheetWCS001.Add(new WCS001()
                        {
                            BU = BU,
                            Location = Location,
                            ManufacturingCode = ManufacturingCode
                        });

                        lstSheetWCS002 = new List<WCS002>();
                    }

                    #endregion

                    #region Validate Lines

                    if (!string.IsNullOrWhiteSpace(Material) || !string.IsNullOrWhiteSpace(ProductForm) || !string.IsNullOrWhiteSpace(MaterialSpecificationAndGrade) ||
                          !string.IsNullOrWhiteSpace(UnitofMeasurement) || !string.IsNullOrWhiteSpace(DensityofMaterial))
                    {
                        if (string.IsNullOrWhiteSpace(Material))
                        {
                            item["MaterialErrorMsg"] = "Material is Required"; isError = true;
                        }
                        else
                        {
                            if (Material.Length > 50)
                            {
                                item["MaterialErrorMsg"] = "Material have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(ProductForm))
                        {
                            item["ProductFormErrorMsg"] = "Product Form is Required"; isError = true;
                        }
                        else
                        {
                            if (ProductForm.Length > 50)
                            {
                                item["ProductFormErrorMsg"] = "Product Form have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(MaterialSpecificationAndGrade))
                        {
                            item["MaterialSpecificationAndGradeErrorMsg"] = "Material Specification And Grade is Required"; isError = true;
                        }
                        else
                        {
                            if (MaterialSpecificationAndGrade.Length > 50)
                            {
                                item["MaterialSpecificationAndGradeErrorMsg"] = "Material Specification And Grade have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(UnitofMeasurement))
                        {
                            item["UnitofMeasurementErrorMsg"] = "Unit of Measurement is Required"; isError = true;
                        }
                        else
                        {
                            if (UnitofMeasurement.Length > 50)
                            {
                                item["UnitofMeasurementErrorMsg"] = "Unit of Measurement have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(DensityofMaterial))
                        {
                            item["DensityofMaterialErrorMsg"] = "Density of Material is Required"; isError = true;
                        }
                        else
                        {
                            Double densityofmaterial;
                            if (!Double.TryParse(DensityofMaterial, out densityofmaterial))
                            {
                                item["DensityofMaterialErrorMsg"] = "'" + DensityofMaterial + "' is Not Valid, Please Enter Only Numeric Value"; isError = true;
                            }
                            else if (DensityofMaterial.Length > 10)
                            {
                                item["DensityofMaterialErrorMsg"] = "Density of Material have maximum limit of 10 digits"; isError = true;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(Material) && !string.IsNullOrWhiteSpace(UnitofMeasurement))
                        {
                            if (lstSheetWCS002.Any(u => u.Material.Trim().ToLower() == Material.Trim().ToLower() && u.ProductForm.Trim().ToLower() == ProductForm.Trim().ToLower()
                            && u.MaterialSpecificationAndGrade.Trim().ToLower() == MaterialSpecificationAndGrade.Trim().ToLower() && u.DensityofMaterial.ToString() == DensityofMaterial
                            && u.UnitofMeasurement.Trim().ToLower() == UnitofMeasurement.Trim().ToLower()))
                            {
                                item["MaterialErrorMsg"] = "Material is Already Exist in this sheet"; isError = true;
                            }

                            lstSheetWCS002.Add(new WCS002()
                            {
                                Material = Material,
                                UnitofMeasurement = UnitofMeasurement
                            });
                        }
                    }

                    #endregion
                }

                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtLinesExcel);
            return ds;
        }

        public clsHelper.ResponceMsgWithFileName ErrorExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/WCS/Maintain Material Master - Error Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();

                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            #region Generate for Header

                            ///BU Error Message

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(8)))
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(8).ToString() + " )";
                                excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }

                            ///Location
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(9)))
                            {
                                excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(9).ToString() + " )";
                                excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }

                            ///Manufacturing Code
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(10)))
                            {
                                excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>(10).ToString() + " )";
                                excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }

                            #endregion

                            #region Common Columns for Lines
                            ///lines

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(11)))
                            {
                                excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>(11).ToString() + " )";
                                excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(12)))
                            {
                                excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>(12).ToString() + " )";
                                excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(13)))
                            {
                                excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>(13).ToString() + " )";
                                excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(14)))
                            {
                                excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>(14).ToString() + " )";
                                excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(15)))
                            {
                                excelWorksheet.Cells[i, 8].Value = item.Field<string>(7) + " (Note: " + item.Field<string>(15).ToString() + " )";
                                excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }

                            #endregion 
                            i++;
                        }

                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        #endregion

        #region Export Excel

        public ActionResult GenerateExcelHeader(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string BU = "";
                List<AutoCompleteModel> BUList = objClsManager.GetWCSBUList();
                foreach (var bus in BUList)
                {
                    BU += "'" + bus.Value + "',";
                }
                BU = BU.TrimEnd(',');

                string Location = objClsLoginInfo.Location;

                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_WCS_GET_MATERIAL_MASTER_HEADER(1, int.MaxValue, strSortOrder, whereCondition, BU, Location).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      BU = Convert.ToString(li.BU),
                                      Location = Convert.ToString(li.Location),
                                      ManufacturingCode = Convert.ToString(li.ManufacturingCode),
                                      CreatedBy = Convert.ToString(li.CreatedBy),
                                      CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateExcelLines(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_WCS_GET_MATERIAL_MASTER_DETAIL(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var newlst = (from li in lst
                              select new
                              {
                                  Material = Convert.ToString(li.Material),
                                  ProductForm = Convert.ToString(li.ProductForm),
                                  MaterialSpecificationAndGrade = Convert.ToString(li.MaterialSpecificationAndGrade),
                                  UnitofMeasurement = Convert.ToString(li.UnitofMeasurement),
                                  DensityofMaterial = Convert.ToString(li.DensityofMaterial),
                                  //CreatedBy = Convert.ToString(li.CreatedBy),
                                  //CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}