﻿using IEMQS.Areas.WCS.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.WCS.Controllers
{
    public class MaintainWCSController : clsBase
    {
        clsManager objClsManager = new clsManager();

        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult AddHeader(int id = 0)
        {
            WCS007 model = new WCS007();
            List<AutoCompleteModel> list = objClsManager.GetWCSBUList();
            ViewBag.BUList = (from u in list select new { CatID = u.Value, CatDesc = u.Text }).OrderBy(u => u.CatID).ToList();
            model.Location = objClsLoginInfo.Location;

            ViewBag.LineCount = 0;
            if (id > 0)
            {
                model = db.WCS007.Where(u => u.HeaderId == id).FirstOrDefault();
                ViewBag.BU = db.COM002.Where(u => u.t_dtyp == 2 && u.t_dimx == model.BU).Select(u => u.t_dimx + " - " + u.t_desc).FirstOrDefault();
                ViewBag.JobNo = db.COM001.Where(u => u.t_cprj == model.JobNo).Select(u => u.t_cprj + " - " + u.t_dsca).FirstOrDefault();

                ViewBag.LineCount = db.WCS008.Where(u => u.HeaderId == id).Count();
            }
            ViewBag.LocationDesc = db.COM002.Where(u => u.t_dtyp == 1 && u.t_dimx == model.Location).Select(u => u.t_dimx + " - " + u.t_desc).FirstOrDefault();
            return View(model);
        }

        public ActionResult GetEquipment(string term, string BU, string Location)
        {
            var catList = Manager.GetSubCatagories("Equipment", BU, Location);

            if (!string.IsNullOrEmpty(term))
            {
                catList = (from u in catList where u.Code.Trim().ToLower().Contains(term.Trim().ToLower()) select u).ToList();
            }
            var list1 = (from u in catList select new { Value = u.Code, Text = u.Code }).OrderBy(u => u.Value).ToList();
            return Json(list1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProcessLicenser(string term, string BU, string Location, string Equipment)
        {
            var catList = db.WCS003.Where(u => u.BU == BU && u.Location == Location && u.Equipment == Equipment && u.ProcessLicensor != null && u.ProcessLicensor != "").ToList();

            if (!string.IsNullOrEmpty(term))
            {
                catList = (from u in catList where u.ProcessLicensor.Trim().ToLower().Contains(term.Trim().ToLower()) select u).ToList();
            }
            var list1 = (from u in catList select new { Value = u.ProcessLicensor, Text = u.ProcessLicensor }).OrderBy(u => u.Value).ToList();
            return Json(list1, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult getHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                string BU = "";
                List<AutoCompleteModel> BUList = objClsManager.GetWCSBUList();
                foreach (var bus in BUList)
                {
                    BU += "'" + bus.Value + "',";
                }
                BU = BU.TrimEnd(',');

                string Location = objClsLoginInfo.Location;

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "BU", "Location", "CONVERT(nvarchar(20),Date,103)", "DocumentNo", "JobNo", "Equipment", "EquipmentNo", "Customer", "PONo",
                        "ProcessLicenser", "Project", "ProjectLocation", "ValueofPie","WeightofWelding","WeightofFIM","WeightofInstrumentationandCovers","WeightofCatalyst","TransportationLooseWeight", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };

                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_WCS_GET_WEIGHT_CALCULATION_HEADER(StartIndex, EndIndex, strSortOrder, strWhere, BU, Location).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.BU),
                           Convert.ToString(uc.Location),
                           Convert.ToDateTime(uc.Date).ToString("dd/MM/yyyy"),
                           Convert.ToString(uc.DocumentNo),
                           Convert.ToString(uc.JobNo),
                           Convert.ToString(uc.Equipment),
                           Convert.ToString(uc.EquipmentNo),
                           Convert.ToString(uc.Customer),
                           Convert.ToString(uc.PONo),
                           Convert.ToString(uc.ProcessLicenser),
                           Convert.ToString(uc.Project),
                           Convert.ToString(uc.ProjectLocation),
                           Convert.ToString(uc.ValueofPie),
                           Convert.ToString(uc.WeightofWelding),
                           Convert.ToString(uc.WeightofFIM),
                           Convert.ToString(uc.WeightofInstrumentationandCovers),
                           Convert.ToString(uc.WeightofCatalyst),
                           Convert.ToString(uc.TransportationLooseWeight),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #region CRUD Header

        [HttpPost]
        public ActionResult SaveHeader(FormCollection fc, bool IsValueOfPieUpdated)
        {
            string BU = fc["BU"].ToString();
            string Location = fc["Location"].ToString();
            string Date = fc["Date"].ToString();
            string JobNo = fc["JobNo"].ToString();
            string Equipment = fc["Equipment"].ToString();
            string EquipmentNo = fc["EquipmentNo"].ToString();
            string Customer = fc["Customer"].ToString();
            string PONo = fc["PONo"].ToString();
            string ProcessLicenser = fc["ProcessLicenser"].ToString();
            string Project = fc["Project"].ToString();
            string ProjectLocation = fc["ProjectLocation"].ToString();
            decimal ValueofPie = fc["ValueofPie"] != "" ? Convert.ToDecimal(fc["ValueofPie"].ToString()) : 0;
            int HeaderId = fc["HeaderId"] != "" ? Convert.ToInt32(fc["HeaderId"].ToString()) : 0;

            decimal? WeightofWelding = null;
            if (fc["WeightofWelding"] != null && fc["WeightofWelding"] != "") { WeightofWelding = Convert.ToDecimal(fc["WeightofWelding"].ToString()); }

            decimal? WeightofFIM = null;
            if (fc["WeightofFIM"] != null && fc["WeightofFIM"] != "") { WeightofFIM = Convert.ToDecimal(fc["WeightofFIM"].ToString()); }

            decimal? WeightofInstrumentationandCovers = null;
            if (fc["WeightofInstrumentationandCovers"] != null && fc["WeightofInstrumentationandCovers"] != "") { WeightofInstrumentationandCovers = Convert.ToDecimal(fc["WeightofInstrumentationandCovers"].ToString()); }

            decimal? WeightofCatalyst = null;
            if (fc["WeightofCatalyst"] != null && fc["WeightofCatalyst"] != "") { WeightofCatalyst = Convert.ToDecimal(fc["WeightofCatalyst"].ToString()); }

            decimal? TransportationLooseWeight = null;
            if (fc["TransportationLooseWeight"] != null && fc["TransportationLooseWeight"] != "") { TransportationLooseWeight = Convert.ToDecimal(fc["TransportationLooseWeight"].ToString()); }

            DateTime? dtDate = null;
            if (Date != "")
                dtDate = Convert.ToDateTime(Date);

            clsFormulaCalculator objFormulaCalc = new clsFormulaCalculator();
            clsHelper.ResponseMsgWCS objResponseMsg = new clsHelper.ResponseMsgWCS();

            try
            {
                if (HeaderId > 0)
                {
                    var model = db.WCS007.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (model != null)
                    {
                        model.Date = dtDate;
                        model.JobNo = JobNo;
                        model.DocumentNo = JobNo + model.DocNo.ToString().PadLeft(3, '0');
                        model.Equipment = Equipment;
                        model.EquipmentNo = EquipmentNo;
                        model.Customer = Customer;
                        model.PONo = PONo;
                        model.ProcessLicenser = ProcessLicenser;
                        model.Project = Project;
                        model.ProjectLocation = ProjectLocation;
                        model.ValueofPie = ValueofPie;
                        model.EditedBy = objClsLoginInfo.UserName;
                        model.EditedOn = DateTime.Now;
                        model.WeightofWelding = WeightofWelding;
                        model.WeightofFIM = WeightofFIM;
                        model.WeightofInstrumentationandCovers = WeightofInstrumentationandCovers;
                        model.WeightofCatalyst = WeightofCatalyst;
                        model.TransportationLooseWeight = TransportationLooseWeight;

                        bool IsCalculationError = false;
                        string CalculationError = string.Empty;
                        if (IsValueOfPieUpdated)
                        {
                            #region Auto Update of Calculation

                            //update calculation
                            var list = (from a in db.WCS007
                                        join b in db.WCS008 on a.HeaderId equals b.HeaderId
                                        join c in db.WCS006 on b.ProductForm equals c.LineId
                                        join d in db.WCS005 on new { c.HeaderId, a.BU, a.Location } equals new { d.HeaderId, d.BU, d.Location }
                                        where a.HeaderId == HeaderId && (c.FormulaforVolumeCalculation.Trim().ToLower().Contains("pi()") || c.FormulaforVolumeCalculationInternalVolume.Trim().ToLower().Contains("pi()")
                                        || c.FormulaforCGCalculation.Trim().ToLower().Contains("pi()"))
                                        select b).Distinct().ToList();

                            string result = "success";
                            foreach (var item in list)
                            {
                                string VolumeCalculationFormula = "";
                                string CGVolumeCalculationFormula = "";
                                string InnerVolumeCalculationFormula = "";

                                decimal VolumeCalculate = 0;
                                decimal InnerVolumeofEquipment = 0;
                                decimal LocationL = 0;
                                decimal LocationX = item.LocationX != null ? Convert.ToDecimal(item.LocationX) : 0;

                                decimal UnitWeightofPart = 0;
                                decimal TotalWeightofPartW = 0;
                                decimal W_X_L = 0;
                                decimal HydroWeight = 0;
                                decimal OperatingWeig = 0;

                                decimal Qty = item.Qty != null ? Convert.ToDecimal(item.Qty) : 0;

                                decimal OperatingFluidDensity = item.OperatingFluidDensity != null ? Convert.ToDecimal(item.OperatingFluidDensity) : 0;
                                decimal DensityofWater = item.DensityofWater != null ? Convert.ToDecimal(item.DensityofWater) : 0;

                                decimal DensityofMaterial = item.DensityofMaterial != null ? Convert.ToDecimal(item.DensityofMaterial) : 0;

                                //get formula from WCS006                         
                                var objWCS006 = (from a in db.WCS006
                                                 where a.LineId == item.ProductForm
                                                 select a).FirstOrDefault();
                                if (objWCS006 != null)
                                {
                                    VolumeCalculationFormula = objWCS006.FormulaforVolumeCalculation;
                                    CGVolumeCalculationFormula = objWCS006.FormulaforCGCalculation;
                                    InnerVolumeCalculationFormula = objWCS006.FormulaforVolumeCalculationInternalVolume;
                                }

                                string VolumeCalculateFormula = string.Empty;
                                string InnerVolumeofEquipmentFormula = string.Empty;
                                string LocationLFormula = string.Empty;

                                result = objFormulaCalc.CalculateWCSFormulas(VolumeCalculationFormula, InnerVolumeCalculationFormula, CGVolumeCalculationFormula
                                    , item.Input1, item.Input1Value, item.Input2, item.Input2Value, item.Input3, item.Input3Value, item.Input4, item.Input4Value, item.Input5, item.Input5Value
                                    , item.Input6, item.Input6Value, item.Input7, item.Input7Value, item.Input8, item.Input8Value, item.Input9, item.Input9Value, item.Input10, item.Input10Value
                                    , ValueofPie, ref VolumeCalculate, ref InnerVolumeofEquipment, ref LocationL
                                    , ref VolumeCalculateFormula, ref InnerVolumeofEquipmentFormula, ref LocationLFormula);

                                if (result.ToLower() == "success")
                                {
                                    //calculation
                                    UnitWeightofPart = Math.Round(DensityofMaterial * VolumeCalculate, 2);
                                    TotalWeightofPartW = Math.Round(UnitWeightofPart * Qty, 2);

                                    W_X_L = Math.Round(TotalWeightofPartW * LocationX, 2);
                                    HydroWeight = Math.Round(InnerVolumeofEquipment * DensityofWater, 2);
                                    OperatingWeig = Math.Round(InnerVolumeofEquipment * OperatingFluidDensity, 2);

                                    item.VolumeCalculate = VolumeCalculate;
                                    item.UnitWeightofPart = UnitWeightofPart;
                                    item.TotalWeightofPartW = TotalWeightofPartW;
                                    item.LocationL = LocationL;
                                    item.WXL = W_X_L;
                                    item.InnerVolumeofEquipment = InnerVolumeofEquipment;
                                    item.HydroTestWeight = HydroWeight;
                                    item.OperatingWeight = OperatingWeig;
                                }
                                else
                                {
                                    IsCalculationError = true;
                                    CalculationError = result;
                                    break;
                                }
                            }

                            #endregion
                        }

                        if (IsCalculationError)
                        {
                            objResponseMsg.HeaderId = HeaderId;
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = CalculationError;
                        }
                        else
                        {
                            db.SaveChanges();
                            objResponseMsg.HeaderId = HeaderId;
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                        }
                    }
                }
                else
                {
                    int DocNo = 1;
                    var item = db.WCS007.OrderByDescending(u => u.DocNo).FirstOrDefault();
                    if (item != null && item.DocNo != null && item.DocNo.ToString() != "")
                        DocNo = Convert.ToInt32(item.DocNo) + 1;

                    WCS007 objWCS007 = new WCS007();
                    objWCS007.BU = BU;
                    objWCS007.Location = Location;
                    objWCS007.Date = dtDate;
                    objWCS007.JobNo = JobNo;
                    objWCS007.DocNo = DocNo;
                    objWCS007.DocumentNo = JobNo + DocNo.ToString().PadLeft(3, '0');
                    objWCS007.Equipment = Equipment;
                    objWCS007.EquipmentNo = EquipmentNo;
                    objWCS007.Customer = Customer;
                    objWCS007.PONo = PONo;
                    objWCS007.ProcessLicenser = ProcessLicenser;
                    objWCS007.Project = Project;
                    objWCS007.ProjectLocation = ProjectLocation;
                    objWCS007.ValueofPie = ValueofPie;
                    objWCS007.CreatedBy = objClsLoginInfo.UserName;
                    objWCS007.CreatedOn = DateTime.Now;
                    objWCS007.WeightofWelding = WeightofWelding;
                    objWCS007.WeightofFIM = WeightofFIM;
                    objWCS007.WeightofInstrumentationandCovers = WeightofInstrumentationandCovers;
                    objWCS007.WeightofCatalyst = WeightofCatalyst;
                    objWCS007.TransportationLooseWeight = TransportationLooseWeight;

                    db.WCS007.Add(objWCS007);
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objWCS007.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetPIReferecnceInCalculation(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var list = (from a in db.WCS007
                            join b in db.WCS008 on a.HeaderId equals b.HeaderId
                            join c in db.WCS006 on b.ProductForm equals c.LineId
                            join d in db.WCS005 on new { c.HeaderId, a.BU, a.Location } equals new { d.HeaderId, d.BU, d.Location }
                            where a.HeaderId == HeaderId && (c.FormulaforVolumeCalculation.Trim().ToLower().Contains("pi()") || c.FormulaforVolumeCalculationInternalVolume.Trim().ToLower().Contains("pi()")
                            || c.FormulaforCGCalculation.Trim().ToLower().Contains("pi()"))
                            select a).Distinct().ToList();

                if (list.Count() > 0)
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No reference found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region CRUD Line

        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult AddLine(int? id, int HeaderId = 0)
        {
            if (HeaderId <= 0)
            {
                return View("Index");
            }

            ViewBag.ProductForm = "";
            WCS008 model = new WCS008();
            model.HeaderId = HeaderId;
            if (id > 0)
            {
                model = db.WCS008.Where(u => u.LineId == id).FirstOrDefault();
                if (model != null)
                {
                    var obj = db.WCS006.Where(x => x.LineId == model.ProductForm).FirstOrDefault();
                    if (obj != null)
                        ViewBag.ProductForm = obj.ProductFormDesc;
                }
            }

            var objWCS007 = db.WCS007.Where(u => u.HeaderId == model.HeaderId).FirstOrDefault();
            ViewBag.BU = objWCS007.BU;
            ViewBag.Location = objWCS007.Location;
            ViewBag.Equipment = objWCS007.Equipment;

            ViewBag.WeightofCatalyst = "";
            if (objWCS007.WeightofCatalyst != null && objWCS007.WeightofCatalyst.ToString() != "")
                ViewBag.WeightofCatalyst = objWCS007.WeightofCatalyst.ToString();

            return View(model);
        }

        public ActionResult GetWeightCalculation(int HeaderId)
        {
            decimal TotalWeightofEquipment = 0;
            decimal WXL = 0;
            decimal CenterofGravity = 0;
            decimal OverallOperatingWeight = 0;

            try
            {
                var objWCS007 = db.WCS007.Where(u => u.HeaderId == HeaderId).FirstOrDefault();

                decimal TotalWeightofPartW = 0;
                var temp1 = db.WCS008.Where(u => u.HeaderId == HeaderId).Sum(u => u.TotalWeightofPartW);
                if (temp1 != null)
                    TotalWeightofPartW = Convert.ToDecimal(temp1);

                decimal WeightofWelding = 0;
                if (objWCS007.WeightofWelding != null && objWCS007.WeightofWelding.ToString() != "")
                    WeightofWelding = Convert.ToDecimal(objWCS007.WeightofWelding);

                decimal WeightofFIM = 0;
                if (objWCS007.WeightofFIM != null && objWCS007.WeightofFIM.ToString() != "")
                    WeightofFIM = Convert.ToDecimal(objWCS007.WeightofFIM);

                decimal WeightofInstrumentationandCovers = 0;
                if (objWCS007.WeightofInstrumentationandCovers != null && objWCS007.WeightofInstrumentationandCovers.ToString() != "")
                    WeightofInstrumentationandCovers = Convert.ToDecimal(objWCS007.WeightofInstrumentationandCovers);

                decimal WeightofCatalyst = 0;
                if (objWCS007.WeightofCatalyst != null && objWCS007.WeightofCatalyst.ToString() != "")
                    WeightofCatalyst = Convert.ToDecimal(objWCS007.WeightofCatalyst);

                decimal TransportationLooseWeight = 0;
                if (objWCS007.TransportationLooseWeight != null && objWCS007.TransportationLooseWeight.ToString() != "")
                    TransportationLooseWeight = Convert.ToDecimal(objWCS007.TransportationLooseWeight);

                TotalWeightofEquipment = TotalWeightofPartW + WeightofWelding + WeightofFIM + WeightofInstrumentationandCovers + TransportationLooseWeight;

                var temp2 = db.WCS008.Where(u => u.HeaderId == HeaderId).Sum(u => u.WXL);
                if (temp2 != null)
                    WXL = Convert.ToDecimal(temp2);

                decimal TotalOperatingWeight = 0;
                var temp3 = db.WCS008.Where(u => u.HeaderId == HeaderId).Sum(u => u.OperatingWeight);
                if (temp3 != null)
                    TotalOperatingWeight = Convert.ToDecimal(temp3);

                if (TotalWeightofEquipment > 0)
                    CenterofGravity = WXL / TotalWeightofEquipment;

                TotalWeightofEquipment = Math.Round(TotalWeightofEquipment, 2);
                WXL = Math.Round(WXL, 2);
                CenterofGravity = Math.Round(CenterofGravity, 2);
                OverallOperatingWeight = Math.Round(TotalOperatingWeight + WeightofCatalyst, 2);

                //decimal TotalWeightWelding = Convert.ToDecimal(db.WCS008.Where(u => u.HeaderId == HeaderId).Sum(u => u.WeightofWelding));
                //decimal TotalWeightFIM = Convert.ToDecimal(db.WCS008.Where(u => u.HeaderId == HeaderId).Sum(u => u.WeightofFIM));
                //decimal TotalWeightInstrumentation = Convert.ToDecimal(db.WCS008.Where(u => u.HeaderId == HeaderId).Sum(u => u.WeightofInstrumentationandCovers));
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return Json(new
            {
                TotalWeightofEquipment = TotalWeightofEquipment.ToString(),
                WXL = WXL.ToString(),
                CenterofGravity = CenterofGravity.ToString(),
                OverallOperatingWeight = OverallOperatingWeight.ToString()
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetProductForm(string term, string BU, string Location)
        {
            var objWCS006List = (from a in db.WCS005
                                 join b in db.WCS006 on a.HeaderId equals b.HeaderId
                                 where a.BU == BU && a.Location == Location
                                 select new
                                 {
                                     b.LineId,
                                     b.ProductFormDesc
                                 }).ToList();

            if (!string.IsNullOrEmpty(term))
            {
                objWCS006List = (from u in objWCS006List where u.ProductFormDesc.Trim().ToLower().Contains(term.Trim().ToLower()) select u).ToList();
            }

            var list1 = (from u in objWCS006List select new { Value = u.LineId, Text = u.ProductFormDesc }).OrderBy(u => u.Text).ToList();

            return Json(list1, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetPartDescription(string term, string BU, string Location, string Equipment)
        {
            List<WCS004> objWCS004List = (from a in db.WCS004
                                          join b in db.WCS003 on a.HeaderId equals b.HeaderId
                                          where b.BU == BU && b.Location == Location && b.Equipment == Equipment
                                          select a).ToList();

            if (!string.IsNullOrEmpty(term))
            {
                objWCS004List = (from u in objWCS004List where u.PartDescription.Trim().ToLower().Contains(term.Trim().ToLower()) select u).ToList();
            }

            var list1 = (from u in objWCS004List select new { Value = u.PartDescription, Text = u.PartDescription }).OrderBy(u => u.Value).ToList();

            return Json(list1, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDataProductForm(int ProductForm, string BU, string Location)
        {
            var objWCS006 = (from a in db.WCS006
                             where a.LineId == ProductForm
                             select new
                             {
                                 Input1Description = a.Input1Description,
                                 Input2Description = a.Input2Description,
                                 Input3Description = a.Input3Description,
                                 Input4Description = a.Input4Description,
                                 Input5Description = a.Input5Description,
                                 Input6Description = a.Input6Description,
                                 Input7Description = a.Input7Description,
                                 Input8Description = a.Input8Description,
                                 Input9Description = a.Input9Description,
                                 Input10Description = a.Input10Description,
                                 FormulaforVolumeCalculation = a.FormulaforVolumeCalculation,
                                 FormulaforInternalVolumeCalculation = a.FormulaforVolumeCalculationInternalVolume,
                                 FormulaforCGCalculation = a.FormulaforCGCalculation
                             }).FirstOrDefault();

            return Json(new
            {
                data2 = objWCS006,
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadLineDataGridPartial()
        {
            WCS008 model = new WCS008();
            return PartialView("_GetLineGridDataPartial", model);
        }

        [HttpPost]
        public ActionResult LoadLineDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int HeaderId = param.Headerid != "" ? Convert.ToInt32(param.Headerid) : 0;

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = " HeaderId=" + HeaderId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "ProductFormDesc", "PartDescription","MaterialSpecificationAndGrade","DensityofMaterial", "Qty", "VolumeCalculate", "UnitWeightofPart", "TotalWeightofPartW", "LocationL", "LocationX", "WXL",
                        "InnerVolumeofEquipment", "DensityofWater", "HydroTestWeight", "OperatingFluidDensity", "OperatingWeight","CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)" };
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstResult = db.SP_WCS_GET_WEIGHT_CALCULATION_DETAIL(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = (from uc in lstResult
                            select new[]
                            {
                           uc.ProductFormDesc,
                           uc.PartDescription,
                           uc.MaterialSpecificationAndGrade,
                           uc.DensityofMaterial!=null ? Convert.ToString(uc.DensityofMaterial.Value):"",
                           Convert.ToString(uc.VolumeCalculate),
                           Convert.ToString(uc.UnitWeightofPart),
                           uc.Qty!=null ? Convert.ToString(uc.Qty.Value):"",
                           Convert.ToString(uc.TotalWeightofPartW),
                           //Convert.ToString(uc.WeightofWelding),
                           //Convert.ToString(uc.WeightofFIM),
                           //Convert.ToString(uc.WeightofInstrumentationandCovers),
                           Convert.ToString(uc.LocationL),
                           Convert.ToString(uc.LocationX),
                           Convert.ToString(uc.WXL),
                           Convert.ToString(uc.InnerVolumeofEquipment),
                           Convert.ToString(uc.DensityofWater),
                           Convert.ToString(uc.HydroTestWeight),
                           //Convert.ToString(uc.WeightofCatalyst),
                           Convert.ToString(uc.OperatingFluidDensity),
                           Convert.ToString(uc.OperatingWeight),
                           uc.CreatedBy,
                           Convert.ToDateTime(uc.CreatedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString(uc.LineId),
                           Convert.ToString(uc.LineId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveLine(FormCollection fc)
        {
            int ProductForm = fc["ProductForm"] != "" ? Convert.ToInt32(fc["ProductForm"].ToString()) : 0;
            string PartDescription = Convert.ToString(fc["PartDescription"]);
            string MaterialSpecificationAndGrade = Convert.ToString(fc["MaterialSpecificationAndGrade"]);

            decimal? DensityofMaterial = null;
            if (fc["DensityofMaterial"] != "") { DensityofMaterial = Convert.ToDecimal(fc["DensityofMaterial"].ToString()); }

            decimal Qty = fc["Qty"] != "" ? Convert.ToDecimal(fc["Qty"].ToString()) : 0;

            string Input1 = fc["Input1"].ToString();

            decimal? Input1Value = null;
            if (fc["Input1Value"] != "") { Input1Value = Convert.ToDecimal(fc["Input1Value"].ToString()); }

            string Input2 = fc["Input2"].ToString();
            decimal? Input2Value = null;
            if (fc["Input2Value"] != "") { Input2Value = Convert.ToDecimal(fc["Input2Value"].ToString()); }

            string Input3 = fc["Input3"].ToString();
            decimal? Input3Value = null;
            if (fc["Input3Value"] != "") { Input3Value = Convert.ToDecimal(fc["Input3Value"].ToString()); }

            string Input4 = fc["Input4"].ToString();
            decimal? Input4Value = null;
            if (fc["Input4Value"] != "") { Input4Value = Convert.ToDecimal(fc["Input4Value"].ToString()); }

            string Input5 = fc["Input5"].ToString();
            decimal? Input5Value = null;
            if (fc["Input5Value"] != "") { Input5Value = Convert.ToDecimal(fc["Input5Value"].ToString()); }

            string Input6 = fc["Input6"].ToString();
            decimal? Input6Value = null;
            if (fc["Input6Value"] != "") { Input6Value = Convert.ToDecimal(fc["Input6Value"].ToString()); }

            string Input7 = fc["Input7"].ToString();
            decimal? Input7Value = null;
            if (fc["Input7Value"] != "") { Input7Value = Convert.ToDecimal(fc["Input7Value"].ToString()); }

            string Input8 = fc["Input8"].ToString();
            decimal? Input8Value = null;
            if (fc["Input8Value"] != "") { Input8Value = Convert.ToDecimal(fc["Input8Value"].ToString()); }

            string Input9 = fc["Input9"].ToString();
            decimal? Input9Value = null;
            if (fc["Input9Value"] != "") { Input9Value = Convert.ToDecimal(fc["Input9Value"].ToString()); }

            string Input10 = fc["Input10"].ToString();
            decimal? Input10Value = null;
            if (fc["Input10Value"] != "") { Input10Value = Convert.ToDecimal(fc["Input10Value"].ToString()); }

            decimal? VolumeCalculate = null;
            if (fc["VolumeCalculate"] != "") { VolumeCalculate = Convert.ToDecimal(fc["VolumeCalculate"].ToString()); }

            decimal? UnitWeightofPart = null;
            if (fc["UnitWeightofPart"] != "") { UnitWeightofPart = Convert.ToDecimal(fc["UnitWeightofPart"].ToString()); }

            //decimal? WeightofWelding = null;
            //if (fc["WeightofWelding"] != null && fc["WeightofWelding"] != "") { WeightofWelding = Convert.ToDecimal(fc["WeightofWelding"].ToString()); }

            //decimal? WeightofFIM = null;
            //if (fc["WeightofFIM"] != null && fc["WeightofFIM"] != "") { WeightofFIM = Convert.ToDecimal(fc["WeightofFIM"].ToString()); }

            //decimal? WeightofInstrumentationandCovers = null;
            //if (fc["WeightofInstrumentationandCovers"] != null && fc["WeightofInstrumentationandCovers"] != "") { WeightofInstrumentationandCovers = Convert.ToDecimal(fc["WeightofInstrumentationandCovers"].ToString()); }

            decimal? TotalWeightofPartW = null;
            if (fc["TotalWeightofPartW"] != "") { TotalWeightofPartW = Convert.ToDecimal(fc["TotalWeightofPartW"].ToString()); }

            decimal? LocationL = null;
            if (fc["LocationL"] != "") { LocationL = Convert.ToDecimal(fc["LocationL"].ToString()); }

            decimal? LocationX = null;
            if (fc["LocationX"] != "") { LocationX = Convert.ToDecimal(fc["LocationX"].ToString()); }

            decimal? WXL = null;
            if (fc["WXL"] != "") { WXL = Convert.ToDecimal(fc["WXL"].ToString()); }

            decimal? InnerVolumeofEquipment = null;
            if (fc["InnerVolumeofEquipment"] != "") { InnerVolumeofEquipment = Convert.ToDecimal(fc["InnerVolumeofEquipment"].ToString()); }

            decimal? DensityofWater = null;
            if (fc["DensityofWater"] != "") { DensityofWater = Convert.ToDecimal(fc["DensityofWater"].ToString()); }

            decimal? HydroTestWeight = null;
            if (fc["HydroTestWeight"] != "") { HydroTestWeight = Convert.ToDecimal(fc["HydroTestWeight"].ToString()); }

            //decimal? WeightofCatalyst = null;
            //if (fc["WeightofCatalyst"] != null && fc["WeightofCatalyst"] != "") { WeightofCatalyst = Convert.ToDecimal(fc["WeightofCatalyst"].ToString()); }

            decimal? OperatingFluidDensity = null;
            if (fc["OperatingFluidDensity"] != "") { OperatingFluidDensity = Convert.ToDecimal(fc["OperatingFluidDensity"].ToString()); }

            decimal? OperatingWeight = null;
            if (fc["OperatingWeight"] != "") { OperatingWeight = Convert.ToDecimal(fc["OperatingWeight"].ToString()); }

            int HeaderId = fc["HeaderId"] != "" ? Convert.ToInt32(fc["HeaderId"].ToString()) : 0;
            int LineId = fc["LineId"] != "" ? Convert.ToInt32(fc["LineId"].ToString()) : 0;

            clsHelper.ResponseMsgWCS objResponseMsg = new clsHelper.ResponseMsgWCS();
            try
            {
                if (LineId > 0)
                {
                    var item = db.WCS008.Where(x => x.LineId == LineId).FirstOrDefault();
                    if (item != null)
                    {
                        item.HeaderId = HeaderId;
                        item.ProductForm = ProductForm;
                        item.PartDescription = PartDescription;
                        item.MaterialSpecificationAndGrade = MaterialSpecificationAndGrade;
                        item.DensityofMaterial = DensityofMaterial;
                        item.Qty = Qty;
                        item.Input1 = Input1;
                        item.Input1Value = Input1Value;
                        item.Input2 = Input2;
                        item.Input2Value = Input2Value;
                        item.Input3 = Input3;
                        item.Input3Value = Input3Value;
                        item.Input4 = Input4;
                        item.Input4Value = Input4Value;
                        item.Input5 = Input5;
                        item.Input5Value = Input5Value;
                        item.Input6 = Input6;
                        item.Input6Value = Input6Value;
                        item.Input7 = Input7;
                        item.Input7Value = Input7Value;
                        item.Input8 = Input8;
                        item.Input8Value = Input8Value;
                        item.Input9 = Input9;
                        item.Input9Value = Input9Value;
                        item.Input10 = Input10;
                        item.Input10Value = Input10Value;
                        item.VolumeCalculate = VolumeCalculate;
                        item.UnitWeightofPart = UnitWeightofPart;
                        //item.WeightofWelding = WeightofWelding;
                        //item.WeightofFIM = WeightofFIM;
                        //item.WeightofInstrumentationandCovers = WeightofInstrumentationandCovers;
                        item.TotalWeightofPartW = TotalWeightofPartW;
                        item.LocationL = LocationL;
                        item.LocationX = LocationX;
                        item.WXL = WXL;
                        item.InnerVolumeofEquipment = InnerVolumeofEquipment;
                        item.DensityofWater = DensityofWater;
                        item.HydroTestWeight = HydroTestWeight;
                        //item.WeightofCatalyst = WeightofCatalyst;
                        item.OperatingFluidDensity = OperatingFluidDensity;
                        item.OperatingWeight = OperatingWeight;
                        item.EditedBy = objClsLoginInfo.UserName;
                        item.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    objResponseMsg.LineId = LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
                else
                {
                    WCS008 objWCS008 = new WCS008();
                    objWCS008.HeaderId = HeaderId;
                    objWCS008.ProductForm = ProductForm;
                    objWCS008.PartDescription = PartDescription;
                    objWCS008.MaterialSpecificationAndGrade = MaterialSpecificationAndGrade;
                    objWCS008.DensityofMaterial = DensityofMaterial;
                    objWCS008.Qty = Qty;
                    objWCS008.Input1 = Input1;
                    objWCS008.Input1Value = Input1Value;
                    objWCS008.Input2 = Input2;
                    objWCS008.Input2Value = Input2Value;
                    objWCS008.Input3 = Input3;
                    objWCS008.Input3Value = Input3Value;
                    objWCS008.Input4 = Input4;
                    objWCS008.Input4Value = Input4Value;
                    objWCS008.Input5 = Input5;
                    objWCS008.Input5Value = Input5Value;
                    objWCS008.Input6 = Input6;
                    objWCS008.Input6Value = Input6Value;
                    objWCS008.Input7 = Input7;
                    objWCS008.Input7Value = Input7Value;
                    objWCS008.Input8 = Input8;
                    objWCS008.Input8Value = Input8Value;
                    objWCS008.Input9 = Input9;
                    objWCS008.Input9Value = Input9Value;
                    objWCS008.Input10 = Input10;
                    objWCS008.Input10Value = Input10Value;
                    objWCS008.VolumeCalculate = VolumeCalculate;
                    objWCS008.UnitWeightofPart = UnitWeightofPart;
                    //objWCS008.WeightofWelding = WeightofWelding;
                    //objWCS008.WeightofFIM = WeightofFIM;
                    //objWCS008.WeightofInstrumentationandCovers = WeightofInstrumentationandCovers;
                    objWCS008.TotalWeightofPartW = TotalWeightofPartW;
                    objWCS008.LocationL = LocationL;
                    objWCS008.LocationX = LocationX;
                    objWCS008.WXL = WXL;
                    objWCS008.InnerVolumeofEquipment = InnerVolumeofEquipment;
                    objWCS008.DensityofWater = DensityofWater;
                    objWCS008.HydroTestWeight = HydroTestWeight;
                    //objWCS008.WeightofCatalyst = WeightofCatalyst;
                    objWCS008.OperatingFluidDensity = OperatingFluidDensity;
                    objWCS008.OperatingWeight = OperatingWeight;
                    objWCS008.CreatedBy = objClsLoginInfo.UserName;
                    objWCS008.CreatedOn = DateTime.Now;
                    db.WCS008.Add(objWCS008);
                    db.SaveChanges();

                    objResponseMsg.LineId = objWCS008.LineId;

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                WCS008 objWCS008 = db.WCS008.Where(x => x.LineId == Id).FirstOrDefault();
                db.WCS008.Remove(objWCS008);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CalculateVolumeFromFormula(FormCollection fc)
        {
            clsHelper.ResponseMsgWCS objResponseMsg = new clsHelper.ResponseMsgWCS();

            decimal VolumeCalculate = 0;
            decimal InnerVolumeofEquipment = 0;
            decimal LocationL = 0;

            int HeaderId = Convert.ToInt32(fc["HeaderId"].ToString());
            int ProductForm = Convert.ToInt32(fc["ProductForm"].ToString());

            string Input1 = fc["Input1"].ToString().ToLower();
            decimal Input1Value = (fc["Input1Value"] != null && fc["Input1Value"] != "") ? Convert.ToDecimal(fc["Input1Value"]) : 0;

            string Input2 = fc["Input2"].ToString().ToLower();
            decimal Input2Value = (fc["Input2Value"] != null && fc["Input2Value"] != "") ? Convert.ToDecimal(fc["Input2Value"]) : 0;

            string Input3 = fc["Input3"].ToString().ToLower();
            decimal Input3Value = (fc["Input3Value"] != null && fc["Input3Value"] != "") ? Convert.ToDecimal(fc["Input3Value"]) : 0;

            string Input4 = fc["Input4"].ToString().ToLower();
            decimal Input4Value = (fc["Input4Value"] != null && fc["Input4Value"] != "") ? Convert.ToDecimal(fc["Input4Value"]) : 0;

            string Input5 = fc["Input5"].ToString().ToLower();
            decimal Input5Value = (fc["Input5Value"] != null && fc["Input5Value"] != "") ? Convert.ToDecimal(fc["Input5Value"]) : 0;

            string Input6 = fc["Input6"].ToString().ToLower();
            decimal Input6Value = (fc["Input6Value"] != null && fc["Input6Value"] != "") ? Convert.ToDecimal(fc["Input6Value"]) : 0;

            string Input7 = fc["Input7"].ToString().ToLower();
            decimal Input7Value = (fc["Input7Value"] != null && fc["Input7Value"] != "") ? Convert.ToDecimal(fc["Input7Value"]) : 0;

            string Input8 = fc["Input8"].ToString().ToLower();
            decimal Input8Value = (fc["Input8Value"] != null && fc["Input8Value"] != "") ? Convert.ToDecimal(fc["Input8Value"]) : 0;

            string Input9 = fc["Input9"].ToString().ToLower();
            decimal Input9Value = (fc["Input9Value"] != null && fc["Input9Value"] != "") ? Convert.ToDecimal(fc["Input9Value"]) : 0;

            string Input10 = fc["Input10"].ToString().ToLower();
            decimal Input10Value = (fc["Input10Value"] != null && fc["Input10Value"] != "") ? Convert.ToDecimal(fc["Input10Value"]) : 0;

            string VolumeCalculationFormula = string.Empty;
            string CGVolumeCalculationFormula = string.Empty;
            string InnerVolumeCalculationFormula = string.Empty;

            decimal PI = 0;
            var objWCS007 = db.WCS007.Where(u => u.HeaderId == HeaderId).FirstOrDefault();
            if (objWCS007.ValueofPie != null)
                PI = Convert.ToDecimal(objWCS007.ValueofPie);

            var objWCS006 = (from a in db.WCS006
                             where a.LineId == ProductForm
                             select a).FirstOrDefault();
            if (objWCS006 != null)
            {
                VolumeCalculationFormula = objWCS006.FormulaforVolumeCalculation;
                CGVolumeCalculationFormula = objWCS006.FormulaforCGCalculation;
                InnerVolumeCalculationFormula = objWCS006.FormulaforVolumeCalculationInternalVolume;
            }

            clsFormulaCalculator objFormulaCalc = new clsFormulaCalculator();

            string VolumeCalculateFormula = string.Empty;
            string InnerVolumeofEquipmentFormula = string.Empty;
            string LocationLFormula = string.Empty;

            try
            {
                string result = objFormulaCalc.CalculateWCSFormulas(VolumeCalculationFormula, InnerVolumeCalculationFormula, CGVolumeCalculationFormula, Input1, Input1Value,
                                Input2, Input2Value, Input3, Input3Value, Input4, Input4Value, Input5, Input5Value, Input6, Input6Value, Input7, Input7Value, Input8, Input8Value,
                                Input9, Input9Value, Input10, Input10Value, PI,
                                ref VolumeCalculate, ref InnerVolumeofEquipment, ref LocationL, ref VolumeCalculateFormula, ref InnerVolumeofEquipmentFormula, ref LocationLFormula);

                if (result.ToLower() == "success")
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = result;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            finally
            {
                objResponseMsg.VolumeCalculate = VolumeCalculate.ToString();
                objResponseMsg.InnerVolumeofEquipment = InnerVolumeofEquipment.ToString();
                objResponseMsg.LocationL = LocationL.ToString();

                objResponseMsg.VolumeCalculateFormula = VolumeCalculateFormula;
                objResponseMsg.InnerVolumeofEquipmentFormula = InnerVolumeofEquipmentFormula;
                objResponseMsg.LocationLFormula = LocationLFormula;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Export Excel

        public ActionResult GenerateExcelHeader(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string BU = "";
                List<AutoCompleteModel> BUList = objClsManager.GetWCSBUList();
                foreach (var bus in BUList)
                {
                    BU += "'" + bus.Value + "',";
                }
                BU = BU.TrimEnd(',');

                string Location = objClsLoginInfo.Location;

                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_WCS_GET_WEIGHT_CALCULATION_HEADER(1, int.MaxValue, strSortOrder, whereCondition, BU, Location).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    var newlst = (from li in lst
                                  select new
                                  {
                                      BU = Convert.ToString(li.BU),
                                      Location = Convert.ToString(li.Location),
                                      Date = Convert.ToDateTime(li.Date).ToString("dd/MM/yyyy"),
                                      DocumentNo = Convert.ToString(li.DocumentNo),
                                      JobNo = Convert.ToString(li.JobNo),
                                      Equipment = Convert.ToString(li.Equipment),
                                      EquipmentNo = Convert.ToString(li.EquipmentNo),
                                      Customer = Convert.ToString(li.Customer),
                                      PONo = Convert.ToString(li.PONo),
                                      ProcessLicenser = Convert.ToString(li.ProcessLicenser),
                                      Project = Convert.ToString(li.Project),
                                      ProjectLocation = Convert.ToString(li.ProjectLocation),
                                      ValueofPie = Convert.ToString(li.ValueofPie),
                                      WeightofWelding = Convert.ToString(li.WeightofWelding),
                                      WeightofFIM = Convert.ToString(li.WeightofFIM),
                                      WeightofInstrumentationandCovers = Convert.ToString(li.WeightofInstrumentationandCovers),
                                      WeightofCatalyst = Convert.ToString(li.WeightofCatalyst),
                                      TransportationLooseWeight = Convert.ToString(li.TransportationLooseWeight),
                                      CreatedBy = Convert.ToString(li.CreatedBy),
                                      CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateExcelLines(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_WCS_GET_WEIGHT_CALCULATION_DETAIL(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var newlst = (from li in lst
                              select new
                              {
                                  ProductForm = Convert.ToString(li.ProductFormDesc),
                                  PartDescription = Convert.ToString(li.PartDescription),
                                  MaterialSpecificationAndGrade = li.MaterialSpecificationAndGrade,
                                  DensityofMaterial = Convert.ToString(li.DensityofMaterial),
                                  VolumeCalculate = Convert.ToString(li.VolumeCalculate),
                                  UnitWeightofPart = Convert.ToString(li.UnitWeightofPart),
                                  Qty = Convert.ToString(li.Qty != null ? Convert.ToDouble(li.Qty.Value) : 0),
                                  //WeightofWelding = Convert.ToString(li.WeightofWelding),
                                  //WeightofFIM = Convert.ToString(li.WeightofFIM),
                                  //WeightofInstrumentationandCovers = Convert.ToString(li.WeightofInstrumentationandCovers),
                                  TotalWeightofPart = Convert.ToString(li.TotalWeightofPartW),
                                  LocationL = Convert.ToString(li.LocationL),
                                  LocationX = Convert.ToString(li.LocationX),
                                  WXL = Convert.ToString(li.WXL),
                                  InnerVolumeofEquipment = Convert.ToString(li.InnerVolumeofEquipment),
                                  DensityofWater = Convert.ToString(li.DensityofWater),
                                  HydroTestWeight = Convert.ToString(li.HydroTestWeight),
                                  //WeightofCatalyst = Convert.ToString(li.WeightofCatalyst),
                                  OperatingFluidDensity = Convert.ToString(li.OperatingFluidDensity),
                                  OperatingWeight = Convert.ToString(li.OperatingWeight),
                                  CreatedBy = Convert.ToString(li.CreatedBy),
                                  CreatedOn = Convert.ToDateTime(li.CreatedOn).ToString("dd/MM/yyyy"),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region Report 

        [AllowAnonymous]
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult PrintReportWCS()
        {
            List<AutoCompleteModel> list = objClsManager.GetWCSBUList();
            ViewBag.BUList = (from u in list select new { CatID = u.Value, CatDesc = u.Text }).OrderBy(u => u.CatID).ToList();
            ViewBag.Location = objClsLoginInfo.Location;
            ViewBag.LocationDesc = db.COM002.Where(u => u.t_dtyp == 1 && u.t_dimx == objClsLoginInfo.Location).Select(u => u.t_dimx + " - " + u.t_desc).FirstOrDefault();
            return View();
        }

        public JsonResult GetDocumentNo(string search, string BU, string Location, string JobNo, string Equipment)
        {
            try
            {
                var objList = (from u in db.WCS007
                               where u.BU == BU && u.Location == Location && u.JobNo == JobNo && u.Equipment == Equipment
                               select u).ToList();
                if (!string.IsNullOrEmpty(search))
                {
                    objList = (from u in objList where u.DocumentNo.Trim().ToLower().Contains(search.Trim().ToLower()) select u).ToList();
                }
                var list1 = (from u in objList select new { id = u.DocumentNo.Trim(), text = u.DocumentNo.Trim() }).OrderBy(u => u.text).ToList();
                return Json(list1, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult GetHeadeId(string BU, string Location, string JobNo, string Equipment)//string DocumentNos
        {
            try
            {
                //var list = DocumentNos.Split(',').ToList();
                var objList = (from u in db.WCS007
                               where u.BU == BU && u.Location == Location && u.JobNo == JobNo && u.Equipment == Equipment
                               select u.HeaderId).ToList();
                string HeaderIds = "";
                objList.ForEach(u =>
                {
                    HeaderIds += u.ToString() + ",";
                });
                HeaderIds = HeaderIds.TrimEnd(',');
                return Json(HeaderIds, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
    }
}