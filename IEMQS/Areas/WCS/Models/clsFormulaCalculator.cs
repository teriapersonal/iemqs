﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using IEMQS.ExcelCalcService;
using System.Text.RegularExpressions;
using System.Globalization;

namespace IEMQS.Areas.WCS.Models
{
    public class clsFormulaCalculator
    {
        public string CalculateWCSFormulas(string VolumeFormula, string InternalVolumeFormula, string CGFormula, string Input1, decimal? Input1Value
            , string Input2, decimal? Input2Value, string Input3, decimal? Input3Value, string Input4, decimal? Input4Value, string Input5, decimal? Input5Value
            , string Input6, decimal? Input6Value, string Input7, decimal? Input7Value
            , string Input8, decimal? Input8Value, string Input9, decimal? Input9Value, string Input10, decimal? Input10Value
            , decimal valueOfPie, ref decimal Volume, ref decimal InternalVolume, ref decimal CenterOfGravity
            , ref string VolumeFormulaFinal, ref string InternalVolumeFormulaFinal, ref string CGFormulaFinal)
        {
            string result = "success";

            ExcelCalc objService = new ExcelCalc();
            ResponseModel objResponse = new ResponseModel();

            try
            {
                Input1 = !string.IsNullOrWhiteSpace(Input1) ? Input1.ToLower() : "";
                Input2 = !string.IsNullOrWhiteSpace(Input2) ? Input2.ToLower() : "";
                Input3 = !string.IsNullOrWhiteSpace(Input3) ? Input3.ToLower() : "";
                Input4 = !string.IsNullOrWhiteSpace(Input4) ? Input4.ToLower() : "";
                Input5 = !string.IsNullOrWhiteSpace(Input5) ? Input5.ToLower() : "";
                Input6 = !string.IsNullOrWhiteSpace(Input6) ? Input6.ToLower() : "";
                Input7 = !string.IsNullOrWhiteSpace(Input7) ? Input7.ToLower() : "";
                Input8 = !string.IsNullOrWhiteSpace(Input8) ? Input8.ToLower() : "";
                Input9 = !string.IsNullOrWhiteSpace(Input9) ? Input9.ToLower() : "";
                Input10 = !string.IsNullOrWhiteSpace(Input10) ? Input10.ToLower() : "";

                Hashtable ht = new Hashtable();
                ht["pi()"] = valueOfPie;

                List<string> parameterList = new List<string>();
                if (!string.IsNullOrWhiteSpace(Input1)) { parameterList.Add(Input1); ht[Input1] = Input1Value; }
                if (!string.IsNullOrWhiteSpace(Input2)) { parameterList.Add(Input2); ht[Input2] = Input2Value; }
                if (!string.IsNullOrWhiteSpace(Input3)) { parameterList.Add(Input3); ht[Input3] = Input3Value; }
                if (!string.IsNullOrWhiteSpace(Input4)) { parameterList.Add(Input4); ht[Input4] = Input4Value; }
                if (!string.IsNullOrWhiteSpace(Input5)) { parameterList.Add(Input5); ht[Input5] = Input5Value; }
                if (!string.IsNullOrWhiteSpace(Input6)) { parameterList.Add(Input6); ht[Input6] = Input6Value; }
                if (!string.IsNullOrWhiteSpace(Input7)) { parameterList.Add(Input7); ht[Input7] = Input7Value; }
                if (!string.IsNullOrWhiteSpace(Input8)) { parameterList.Add(Input8); ht[Input8] = Input8Value; }
                if (!string.IsNullOrWhiteSpace(Input9)) { parameterList.Add(Input9); ht[Input9] = Input9Value; }
                if (!string.IsNullOrWhiteSpace(Input10)) { parameterList.Add(Input10); ht[Input10] = Input10Value; }
                parameterList = SortByLength(parameterList);

                parameterList.Add("pi()");

                #region Volume Calculation

                {
                    if (!string.IsNullOrEmpty(VolumeFormula))
                    {
                        string form = "=" + VolumeFormula.ToLower();
                        string _strFormula = form.Replace("'", "\"");
                        if (parameterList != null)
                        {
                            foreach (string key in parameterList)
                            {
                                if (form.Contains(key))
                                {
                                    if (ht[key] != null)
                                    {
                                        _strFormula = _strFormula.SafeReplace(key.ToString(), ht[key].ToString(), true);
                                    }
                                }
                            }
                        }

                        VolumeFormulaFinal = _strFormula.ToString();

                        objResponse = objService.CalculateFormula(VolumeFormulaFinal);
                        if (objResponse.key)
                        {
                            string strVolume = objResponse.value;

                            if (strVolume != "")
                                Volume = Math.Round(Convert.ToDecimal(strVolume), 2);
                        }
                    }
                }

                #endregion

                #region CG Calculation

                {
                    if (!string.IsNullOrEmpty(CGFormula))
                    {
                        string form = "=" + CGFormula.ToLower();
                        string _strFormula = form.Replace("'", "\"");
                        if (parameterList != null)
                        {
                            foreach (string key in parameterList)
                            {
                                if (form.Contains(key))
                                {
                                    if (ht[key] != null)
                                    {
                                        _strFormula = _strFormula.SafeReplace(key.ToString(), ht[key].ToString(), true);
                                    }
                                }
                            }
                        }

                        CGFormulaFinal = _strFormula.ToString();

                        objResponse = objService.CalculateFormula(CGFormulaFinal);
                        if (objResponse.key)
                        {
                            string strCenterOfGravity = objResponse.value;

                            if (strCenterOfGravity != "")
                                CenterOfGravity = Math.Round(Convert.ToDecimal(strCenterOfGravity), 2);
                        }
                    }
                }

                #endregion

                #region Inner Volume Calculation

                {
                    if (!string.IsNullOrEmpty(InternalVolumeFormula))
                    {
                        string form = "=" + InternalVolumeFormula.ToLower();
                        string _strFormula = form.Replace("'", "\"");
                        if (parameterList != null)
                        {
                            foreach (string key in parameterList)
                            {
                                if (form.Contains(key))
                                {
                                    if (ht[key] != null)
                                    {
                                        _strFormula = _strFormula.SafeReplace(key.ToString(), ht[key].ToString(), true);
                                    }
                                }
                            }
                        }

                        InternalVolumeFormulaFinal = _strFormula.ToString();

                        objResponse = objService.CalculateFormula(InternalVolumeFormulaFinal);
                        if (objResponse.key)
                        {
                            string strInternalVolume = objResponse.value;

                            if (strInternalVolume != "")
                                InternalVolume = Math.Round(Convert.ToDecimal(strInternalVolume), 2);
                        }
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                result = ex.Message;
            }
            finally
            {
                objService = null;

                //xlWorkBook.Close(false);
                //xlApp.Quit();

                //ReleaseComObject(xlWorkSheet);
                //ReleaseComObject(xlWorkBook);
                //ReleaseComObject(xlApp);
            }
            return result;
        }

        public List<string> SortByLength(IEnumerable<string> e)
        {
            // Use LINQ to sort the array received and return a copy.
            var sorted = (from s in e
                          orderby s.Length descending
                          select s).ToList();
            return sorted;
        }
    }

    public static class StringExtensions
    {
        public static string SafeReplace(this string input, string find, string replace, bool matchWholeWord)
        {
            string textToFind = matchWholeWord ? string.Format(@"\b{0}\b", find) : find;
            string result = "";
            if (find.Trim().ToLower() == "pi()")
            {
                result = input.Replace(find, replace);
            }
            else
            {
                result = Regex.Replace(input, textToFind, replace);
            }
            return result;
        }

        public static string RoundedToZeroDecimals(this string input)
        {
            string result = input;
            if (!string.IsNullOrWhiteSpace(input))
            {
                decimal res = 0;
                if (Decimal.TryParse(input, out res))
                {
                    result = Math.Round(res, 0).ToString();
                }
            }
            return result;
        }
    }
}