﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.SSS.Controllers
{
    public class InputForWEController : clsBase
    {
        // GET: SSS/InputForWE
        public ActionResult Index(int roleId)
        {
            ViewBag.RoleId = roleId;
            return View();
        }

        public ActionResult IndexDE()
        {
            return RedirectToAction("Index", "InputForWE", new { roleId = Convert.ToInt32(RoleForSSS.DE) });
        }
        public ActionResult IndexWE()
        {
            return RedirectToAction("Index", "InputForWE", new { roleId = Convert.ToInt32(RoleForSSS.WE) });
        }
        public ActionResult GETWeldingInputPartial(string status, string RoleId)
        {
            ViewBag.Status = status;
            ViewBag.RoleId = RoleId;
            return PartialView("_GetInputWEDataGridPartial");
        }

        public ActionResult loadWeldingInputDataTable(JQueryDataTableParamModel param, string status, int RoleId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";

                if (status.ToUpper() == "PENDING")
                {
                    if (RoleId == Convert.ToInt32(RoleForSSS.DE))
                    {
                        whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "')";
                    }
                    else if (RoleId == Convert.ToInt32(RoleForSSS.WE))
                    {
                        whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "') and ISNULL(s4.WEId,'0')>0 and s4.ToPerson='" + objClsLoginInfo.UserName + "'";
                    }
                }
                else
                {
                    whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "')";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "s1.SSSNo", "s1.Contract", "s1.Customer", "s1.CSRMeetingOn", "s4.TargetDate", "s4.DesignCode", "s4.Status" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_SSS_GETWeldingInput(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstPam
                          select new[] {
                              h.SSSNo.ToString(),
                              h.Contract.ToString(),
                              h.Customer.ToString(),
                              Convert.ToString(Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy")),
                              Convert.ToString(Convert.ToDateTime(h.TargetDate).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.TargetDate).ToString("dd/MM/yyyy")),
                              h.DesignCode!=null ?h.DesignCode.ToString():"",
                              h.Status,                              
                              "", //Action
                              h.SSSId.ToString(),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [SessionExpireFilter]
        public ActionResult WeldingInput(int SSSId = 0, int RoleId = 0)
        {
            ViewBag.SSSId = SSSId;
            var objSSS004 = db.SSS004.Where(i => i.SSSId == SSSId).FirstOrDefault();
            if (objSSS004 == null)
            {
                objSSS004 = new SSS004();
                objSSS004.SSSId = SSSId;
                objSSS004.Status = SSSStatus.Draft.GetStringValue();
            }
            var listEmployee = Manager.GetApprover(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), objClsLoginInfo.Location, true, objClsLoginInfo.UserName, "");
            ViewBag.lstToEmployee = listEmployee.Select(i => new BULocWiseCategoryModel { CatID = i.id, CatDesc = i.text }).ToList();
            if (objSSS004.ToPerson != null && objSSS004.ToPerson != string.Empty)
            {
                ViewBag.ToUserName = objSSS004.ToPerson + "-" + (db.COM003.Where(i=> i.t_actv == 1).FirstOrDefault(i => i.t_psno == objSSS004.ToPerson).t_name);
            }
            if (objSSS004.SubmittedBy != null && objSSS004.SubmittedBy != string.Empty)
            {
                ViewBag.FromUserName = objSSS004.SubmittedBy + "-" + (db.COM003.Where(i => i.t_actv == 1).FirstOrDefault(i => i.t_psno == objSSS004.SubmittedBy).t_name);
            }
            ViewBag.RoleId = RoleId;
            return View("InputWEMain", objSSS004);
        }
        [HttpPost]
        public ActionResult SaveWEInput(SSS004 objSSS004)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (Convert.ToInt32(objSSS004.WEId) > 0)
                {
                    var objSSS004Update = db.SSS004.Where(i => i.WEId == objSSS004.WEId).FirstOrDefault();
                    if (objSSS004Update.Status.ToLower() == SSSStatus.Draft.GetStringValue().ToLower())
                    {
                        objSSS004Update.ToPerson = objSSS004.ToPerson;
                        objSSS004Update.TargetDate = objSSS004.TargetDate.HasValue ? Manager.getDateTime(objSSS004.TargetDate.Value.ToShortDateString()) : objSSS004.TargetDate;
                        objSSS004Update.DesignCode = objSSS004.DesignCode;
                        objSSS004Update.OtherRequirement = objSSS004.OtherRequirement;
                        objSSS004Update.DesignPressureShell = objSSS004.DesignPressureShell;
                        objSSS004Update.DesignPressureTube = objSSS004.DesignPressureTube;
                        objSSS004Update.DesignTemperatureShell = objSSS004.DesignTemperatureShell;
                        objSSS004Update.DesignTemperatureTube = objSSS004.DesignTemperatureTube;
                        objSSS004Update.ServiceRequirementShell = objSSS004.ServiceRequirementShell;
                        objSSS004Update.ServiceRequirementTube = objSSS004.ServiceRequirementTube;
                        objSSS004Update.FluidCirculatedShell = objSSS004.FluidCirculatedShell;
                        objSSS004Update.FluidCirculatedTube = objSSS004.FluidCirculatedTube;
                        objSSS004Update.MDMTShell = objSSS004.MDMTShell;
                        objSSS004Update.MDMTTube = objSSS004.MDMTTube;
                        objSSS004Update.EditedBy = objClsLoginInfo.UserName;
                        objSSS004Update.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }

                    objResponseMsg.HeaderId = objSSS004Update.WEId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Welding Input Updated Successfully";

                    //var folderPath = "SSS004/" + objSSS004Update.WEId;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }
                else
                {
                    SSS001 objSSS001 = db.SSS001.Where(i => i.SSSId == objSSS004.SSSId).First();

                    SSS004 objSSS004New = new SSS004();
                    objSSS004New.SSSId = objSSS004.SSSId;
                    objSSS004New.Contract = objSSS001.Contract;
                    objSSS004New.Customer = objSSS001.Customer;
                    objSSS004New.ToPerson = objSSS004.ToPerson;
                    objSSS004New.TargetDate = objSSS004.TargetDate.HasValue ? Manager.getDateTime(objSSS004.TargetDate.Value.ToShortDateString()) : objSSS004.TargetDate;
                    objSSS004New.DesignCode = objSSS004.DesignCode;
                    objSSS004New.OtherRequirement = objSSS004.OtherRequirement;
                    objSSS004New.DesignPressureShell = objSSS004.DesignPressureShell;
                    objSSS004New.DesignPressureTube = objSSS004.DesignPressureTube;
                    objSSS004New.DesignTemperatureShell = objSSS004.DesignTemperatureShell;
                    objSSS004New.DesignTemperatureTube = objSSS004.DesignTemperatureTube;
                    objSSS004New.ServiceRequirementShell = objSSS004.ServiceRequirementShell;
                    objSSS004New.ServiceRequirementTube = objSSS004.ServiceRequirementTube;
                    objSSS004New.FluidCirculatedShell = objSSS004.FluidCirculatedShell;
                    objSSS004New.FluidCirculatedTube = objSSS004.FluidCirculatedTube;
                    objSSS004New.MDMTShell = objSSS004.MDMTShell;
                    objSSS004New.MDMTTube = objSSS004.MDMTTube;
                    objSSS004New.CreatedBy = objClsLoginInfo.UserName;
                    objSSS004New.CreatedOn = DateTime.Now;
                    objSSS004New.Status = SSSStatus.Draft.GetStringValue();
                    db.SSS004.Add(objSSS004New);
                    db.SaveChanges();

                    objResponseMsg.HeaderId = objSSS004New.WEId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Welding Input Save Successfully";

                    //var folderPath = "SSS004/" + objSSS004New.WEId;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet); ;
        }
        public ActionResult LoadTabWisePartial(int? id, string partialName)
        {
            string filepath = string.Empty;
            List<string> lstTypesOfRolling = getTypesOfRolling();
            ViewBag.TypeOfRolling = lstTypesOfRolling.Select(i => new BULocWiseCategoryModel { CatID = i, CatDesc = i }).ToList();
            List<string> lstDishedEndType = getDishedEndType();
            List<string> lstTypesOfForming = getTypesOfForming();
            ViewBag.DishedEndType = lstDishedEndType.Select(i => new BULocWiseCategoryModel { CatID = i, CatDesc = i }).ToList();
            ViewBag.TypeOfForming = lstTypesOfForming.Select(i => new BULocWiseCategoryModel { CatID = i, CatDesc = i }).ToList();
            List<string> lstTubeTypes = getTubeTypes();
            ViewBag.TubeTypes = lstTubeTypes.Select(i => new BULocWiseCategoryModel { CatID = i, CatDesc = i }).ToList();
            List<string> lstTypeOfNozzle = getTypeOfNozzle();
            ViewBag.TypeOfNozzle = lstTypeOfNozzle.Select(i => new BULocWiseCategoryModel { CatID = i, CatDesc = i }).ToList();

            switch (partialName)
            {
                case "tab1":
                    filepath = "_tab1";
                    break;
                case "tab2":
                    filepath = "_tab2";

                    break;
                case "tab3":
                    filepath = "_tab3";

                    break;
                case "tab4":
                    filepath = "_tab4";
                    break;
                case "tab5":
                    filepath = "_tab5";
                    break;
                case "tab6":
                    filepath = "_tab6";
                    break;
                case "tab7":
                    filepath = "_tab7";
                    break;
                case "tab8":
                    filepath = "_tab8";
                    break;
                default:
                    filepath = "_tab1";
                    break;
            }
            ViewBag.WEId = id;

            return PartialView(filepath);
        }

        [HttpPost]
        public ActionResult SubmitWE(int WEId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS004 objSSS004 = db.SSS004.Where(x => x.WEId == WEId).FirstOrDefault();

                if (objSSS004 != null)
                {
                    objSSS004.SubmittedBy = objClsLoginInfo.UserName;
                    objSSS004.SubmittedOn = DateTime.Now;
                    objSSS004.Status = SSSStatus.Submitted.GetStringValue();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Weldding Input Submitted Successfully.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region CylindricalShell
        public ActionResult loadCylindricalShellDataTable(JQueryDataTableParamModel param, int WEId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";
                whereCondition += " and WEId= " + WEId.ToString();

                string[] columnName = { "ShellDescription", "ShellId", "Thick", "Material", "TypeOfRolling", "TempOfRolling", "Remarks" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_SSS_WE_GETCylindricalShell(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                int SrNo = 0;
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "WEId", Convert.ToString(WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(newRecordId, "ShellDescription","","", "", false,"","ShellDescription"),
                                    GenerateAutoComplete(newRecordId, "ShellId","","", "", false,"","ShellId",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Thick","","", "", false,"","Thick",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Material","","", "", false,"","Material"),
                                    GenerateAutoComplete(newRecordId, "TypeOfRolling","","", "", false,"","TypeOfRolling",false,"autocomplete"),
                                    GenerateAutoComplete(newRecordId, "TempOfRolling","","", "", false,"","TempOfRolling",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Remarks","","", "", false,"","Remarks"),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "WEId", Convert.ToString(h.WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(h.LineId, "ShellDescription",h.ShellDescription,"", "", true,"","ShellDescription",false),
                                    GenerateAutoComplete(h.LineId, "ShellId",h.ShellId.ToString(),"", "", true,"","ShellId",false,"numeric"),
                                    GenerateAutoComplete(h.LineId, "Thick",h.Thick.ToString(),"", "", true,"","Thick",false,"numeric"),
                                    GenerateAutoComplete(h.LineId, "Material",h.Material.ToString(),"", "", true,"","Material",false),
                                    GenerateAutoComplete(h.LineId, "TypeOfRolling",h.TypeOfRolling.ToString(),"", "", true,"","TypeOfRolling",false,"autocomplete"),
                                    GenerateAutoComplete(h.LineId, "TempOfRolling",h.TempOfRolling.ToString(),"", "", true,"","TempOfRolling",false,"numeric"),
                                    GenerateAutoComplete(h.LineId, "Remarks",h.Remarks.ToString(),"", "", true,"","Remarks",false),
                                    HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteLine("+ h.WEId+ "," + h.LineId +"); ")
                                    + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveWECylindricalShell(FormCollection fc, int Id = 0, int WEId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS005 objSSS005 = null;

                if (Id > 0)
                {
                    objSSS005 = db.SSS005.Where(x => x.LineId == Id).FirstOrDefault();
                    objSSS005.ShellDescription = fc["ShellDescription" + Id];
                    if (fc["ShellId" + Id] == string.Empty)
                    {
                        objSSS005.ShellId = null;
                    }
                    else
                    {
                        objSSS005.ShellId = Convert.ToInt32(fc["ShellId" + Id]);
                    }
                    if (fc["Thick" + Id] == string.Empty)
                    {
                        objSSS005.Thick = null;
                    }
                    else
                    {
                        objSSS005.Thick = Convert.ToInt32(fc["Thick" + Id]);
                    }
                    objSSS005.Material = fc["Material" + Id];
                    objSSS005.TypeOfRolling = fc["TypeOfRolling" + Id];
                    if (fc["TempOfRolling" + Id] == string.Empty)
                    {
                        objSSS005.TempOfRolling = null;
                    }
                    else
                    {
                        objSSS005.TempOfRolling = Convert.ToInt32(fc["TempOfRolling" + Id]);
                    }

                    objSSS005.Remarks = fc["Remarks" + Id];

                    objSSS005.EditedBy = objClsLoginInfo.UserName;
                    objSSS005.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS005 = new SSS005();
                    objSSS005.WEId = WEId;
                    objSSS005.ShellDescription = fc["ShellDescription" + Id];
                    if (fc["ShellId" + Id] == string.Empty)
                    {
                        objSSS005.ShellId = null;
                    }
                    else
                    {
                        objSSS005.ShellId = Convert.ToInt32(fc["ShellId" + Id]);
                    }
                    if (fc["Thick" + Id] == string.Empty)
                    {
                        objSSS005.Thick = null;
                    }
                    else
                    {
                        objSSS005.Thick = Convert.ToInt32(fc["Thick" + Id]);
                    }
                    objSSS005.Material = fc["Material" + Id];
                    objSSS005.TypeOfRolling = fc["TypeOfRolling" + Id];
                    if (fc["TempOfRolling" + Id] == string.Empty)
                    {
                        objSSS005.TempOfRolling = null;
                    }
                    else
                    {
                        objSSS005.TempOfRolling = Convert.ToInt32(fc["TempOfRolling" + Id]);
                    }

                    objSSS005.Remarks = fc["Remarks" + Id];
                    objSSS005.CreatedBy = objClsLoginInfo.UserName;
                    objSSS005.CreatedOn = DateTime.Now;
                    db.SSS005.Add(objSSS005);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteWECylindricalShell(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS005 objSSS005 = db.SSS005.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS005 != null)
                {
                    db.SSS005.Remove(objSSS005);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Cylindrical Shell successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Cylindrical Shell not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Cone
        public ActionResult loadConeDataTable(JQueryDataTableParamModel param, int WEId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";
                whereCondition += " and WEId= " + WEId.ToString();

                string[] columnName = { "Description", "Type", "LargeConeId", "SmallConeId", "Thickness", "Material", "TypeOfForming", "TempOfForming", "Remarks" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_SSS_WE_GETCone(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                int SrNo = 0;
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "WEId", Convert.ToString(WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(newRecordId, "Description","","", "", false,"","Description"),
                                    GenerateAutoComplete(newRecordId, "Type","","", "", false,"","Type"),
                                    GenerateAutoComplete(newRecordId, "LargeConeId","","", "", false,"","LargeConeId",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "SmallConeId","","", "", false,"","SmallConeId",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Thickness","","", "", false,"","Thickness",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Material","","", "", false,"","Material"),
                                    GenerateAutoComplete(newRecordId, "TypeOfForming","","", "", false,"","TypeOfForming",false,"autocomplete"),
                                    GenerateAutoComplete(newRecordId, "TempOfForming","","", "", false,"","TempOfForming",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Remarks","","", "", false,"","Remarks"),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "WEId", Convert.ToString(h.WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(h.LineId, "Description",h.Description,"", "", true,"","Description",false,"",h.Description),
                                    GenerateAutoComplete(h.LineId, "Type",h.Type,"", "", true,"","Type",false,"",h.Type),
                                    GenerateAutoComplete(h.LineId, "LargeConeId",h.LargeConeId.ToString(),"", "", true,"","LargeConeId",false,"numeric",h.LargeConeId.ToString()),
                                    GenerateAutoComplete(h.LineId, "SmallConeId",h.SmallConeId.ToString(),"", "", true,"","SmallConeId",false,"numeric",h.SmallConeId.ToString()),
                                    GenerateAutoComplete(h.LineId, "Thickness",h.Thickness.ToString(),"", "", true,"","Thickness",false,"numeric",h.Thickness.ToString()),
                                    GenerateAutoComplete(h.LineId, "Material",h.Material,"", "", true,"","Material",false,"",h.Material),
                                    GenerateAutoComplete(h.LineId, "TypeOfForming",h.TypeOfForming,"", "", true,"","TypeOfForming",false,"autocomplete",h.TypeOfForming),
                                    GenerateAutoComplete(h.LineId, "TempOfForming",h.TempOfForming.ToString(),"", "", true,"","TempOfForming",false,"numeric",h.TempOfForming.ToString()),
                                    GenerateAutoComplete(h.LineId, "Remarks",h.Remarks,"", "", true,"","Remarks",false,"",h.Remarks),                                    
                                    HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteLine("+ h.WEId+ "," + h.LineId +"); ")
                                    + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveCone(FormCollection fc, int Id = 0, int WEId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS006 objSSS006 = null;

                if (Id > 0)
                {
                    objSSS006 = db.SSS006.Where(x => x.LineId == Id).FirstOrDefault();
                    objSSS006.Description = fc["Description" + Id];
                    objSSS006.Type = fc["Type" + Id];
                    if (fc["LargeConeId" + Id] == string.Empty)
                    {
                        objSSS006.LargeConeId = null;
                    }
                    else
                    {
                        objSSS006.LargeConeId = Convert.ToInt32(fc["LargeConeId" + Id]);
                    }
                    if (fc["SmallConeId" + Id] == string.Empty)
                    {
                        objSSS006.SmallConeId = null;
                    }
                    else
                    {
                        objSSS006.SmallConeId = Convert.ToInt32(fc["SmallConeId" + Id]);
                    }
                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS006.Thickness = null;
                    }
                    else
                    {
                        objSSS006.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }
                    objSSS006.Material = fc["Material" + Id];
                    objSSS006.TypeOfForming = fc["TypeOfForming" + Id];
                    if (fc["TempOfForming" + Id] == string.Empty)
                    {
                        objSSS006.TempOfForming = null;
                    }
                    else
                    {
                        objSSS006.TempOfForming = Convert.ToInt32(fc["TempOfForming" + Id]);
                    }

                    objSSS006.Remarks = fc["Remarks" + Id];

                    objSSS006.EditedBy = objClsLoginInfo.UserName;
                    objSSS006.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS006 = new SSS006();
                    objSSS006.WEId = WEId;
                    objSSS006.Description = fc["Description" + Id];
                    objSSS006.Type = fc["Type" + Id];
                    if (fc["LargeConeId" + Id] == string.Empty)
                    {
                        objSSS006.LargeConeId = null;
                    }
                    else
                    {
                        objSSS006.LargeConeId = Convert.ToInt32(fc["LargeConeId" + Id]);
                    }
                    if (fc["SmallConeId" + Id] == string.Empty)
                    {
                        objSSS006.SmallConeId = null;
                    }
                    else
                    {
                        objSSS006.SmallConeId = Convert.ToInt32(fc["SmallConeId" + Id]);
                    }
                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS006.Thickness = null;
                    }
                    else
                    {
                        objSSS006.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }
                    objSSS006.Material = fc["Material" + Id];
                    objSSS006.TypeOfForming = fc["TypeOfForming" + Id];
                    if (fc["TempOfForming" + Id] == string.Empty)
                    {
                        objSSS006.TempOfForming = null;
                    }
                    else
                    {
                        objSSS006.TempOfForming = Convert.ToInt32(fc["TempOfForming" + Id]);
                    }

                    objSSS006.Remarks = fc["Remarks" + Id];

                    objSSS006.CreatedBy = objClsLoginInfo.UserName;
                    objSSS006.CreatedOn = DateTime.Now;
                    db.SSS006.Add(objSSS006);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteCone(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS006 objSSS006 = db.SSS006.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS006 != null)
                {
                    db.SSS006.Remove(objSSS006);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Cone successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Cone not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Dished End
        public ActionResult loadDishedEndDataTable(JQueryDataTableParamModel param, int WEId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";
                whereCondition += " and WEId= " + WEId.ToString();

                string[] columnName = { "Description", "Type", "Radius", "Thickness", "Material", "TypeOfForming", "TempOfForming", "Remarks" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_SSS_WE_DishedEnd(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                int SrNo = 0;
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "WEId", Convert.ToString(WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(newRecordId, "Description","","", "", false,"","Description"),
                                    GenerateAutoComplete(newRecordId, "Type","","", "", false,"","Type",false,"autocomplete"),
                                    GenerateAutoComplete(newRecordId, "Radius","","", "", false,"","Radius",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Radius2","","", "", false,"","Radius",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Thickness","","", "", false,"","Thickness",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Material","","", "", false,"","Material"),
                                    GenerateAutoComplete(newRecordId, "TypeOfForming","","", "", false,"","TypeOfForming",false,"autocomplete"),
                                    GenerateAutoComplete(newRecordId, "TempOfForming","","", "", false,"","TempOfForming",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Remarks","","", "", false,"","Remarks"),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "WEId", Convert.ToString(h.WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(h.LineId, "Description",h.Description,"", "", true,"","Description",false,"",h.Description),
                                    GenerateAutoComplete(h.LineId, "Type",h.Type,"", "", true,"","Type",false,"autocomplete",h.Type),
                                    GenerateAutoComplete(h.LineId, "Radius",h.Radius.ToString(),"", "", true,"","Radius",false,"numeric",h.Radius.ToString()),
                                    GenerateAutoComplete(h.LineId, "Radius2",h.Radius2.ToString(),"", "", true,"","Radius2",false,"numeric",h.Radius2.ToString()),
                                    GenerateAutoComplete(h.LineId, "Thickness",h.Thickness.ToString(),"", "", true,"","Thickness",false,"numeric",h.Thickness.ToString()),
                                    GenerateAutoComplete(h.LineId, "Material",h.Material,"", "", true,"","Material",false,"",h.Material),
                                    GenerateAutoComplete(h.LineId, "TypeOfForming",h.TypeOfForming,"", "", true,"","TypeOfForming",false,"autocomplete",h.TypeOfForming),
                                    GenerateAutoComplete(h.LineId, "TempOfForming",h.TempOfForming.ToString(),"", "", true,"","TempOfForming",false,"numeric",h.TempOfForming.ToString()),
                                    GenerateAutoComplete(h.LineId, "Remarks",h.Remarks,"", "", true,"","Remarks",false,"",h.Remarks),                                    
                                    HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteLine("+ h.WEId+ "," + h.LineId +"); ")
                                    + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveDishedEnd(FormCollection fc, int Id = 0, int WEId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS007 objSSS007 = null;

                if (Id > 0)
                {
                    objSSS007 = db.SSS007.Where(x => x.LineId == Id).FirstOrDefault();
                    objSSS007.Description = fc["Description" + Id];
                    objSSS007.Type = fc["Type" + Id];
                    if (fc["Radius" + Id] == string.Empty)
                    {
                        objSSS007.Radius = null;
                    }
                    else
                    {
                        objSSS007.Radius = Convert.ToInt32(fc["Radius" + Id]);
                    }
                    if (fc["Radius2" + Id] == string.Empty)
                    {
                        objSSS007.Radius2 = null;
                    }
                    else
                    {
                        objSSS007.Radius2 = Convert.ToInt32(fc["Radius2" + Id]);
                    }
                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS007.Thickness = null;
                    }
                    else
                    {
                        objSSS007.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }

                    objSSS007.Material = fc["Material" + Id];
                    objSSS007.TypeOfForming = fc["TypeOfForming" + Id];
                    if (fc["TempOfForming" + Id] == string.Empty)
                    {
                        objSSS007.TempOfForming = null;
                    }
                    else
                    {
                        objSSS007.TempOfForming = Convert.ToInt32(fc["TempOfForming" + Id]);
                    }

                    objSSS007.Remarks = fc["Remarks" + Id];

                    objSSS007.EditedBy = objClsLoginInfo.UserName;
                    objSSS007.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS007 = new SSS007();
                    objSSS007.WEId = WEId;
                    objSSS007.Description = fc["Description" + Id];
                    objSSS007.Type = fc["Type" + Id];
                    if (fc["Radius" + Id] == string.Empty)
                    {
                        objSSS007.Radius = null;
                    }
                    else
                    {
                        objSSS007.Radius = Convert.ToInt32(fc["Radius" + Id]);
                    }
                    if (fc["Radius2" + Id] == string.Empty)
                    {
                        objSSS007.Radius2 = null;
                    }
                    else
                    {
                        objSSS007.Radius2 = Convert.ToInt32(fc["Radius2" + Id]);
                    }
                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS007.Thickness = null;
                    }
                    else
                    {
                        objSSS007.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }

                    objSSS007.Material = fc["Material" + Id];
                    objSSS007.TypeOfForming = fc["TypeOfForming" + Id];
                    if (fc["TempOfForming" + Id] == string.Empty)
                    {
                        objSSS007.TempOfForming = null;
                    }
                    else
                    {
                        objSSS007.TempOfForming = Convert.ToInt32(fc["TempOfForming" + Id]);
                    }

                    objSSS007.Remarks = fc["Remarks" + Id];

                    objSSS007.CreatedBy = objClsLoginInfo.UserName;
                    objSSS007.CreatedOn = DateTime.Now;
                    db.SSS007.Add(objSSS007);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteDishedEnd(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS007 objSSS007 = db.SSS007.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS007 != null)
                {
                    db.SSS007.Remove(objSSS007);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Dished End successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Dished End not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Nozzles
        public ActionResult loadNozzlesDataTable(JQueryDataTableParamModel param, int WEId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";
                whereCondition += " and WEId= " + WEId.ToString();

                string[] columnName = { "Description", "Type", "NozzleId", "Thickness", "Material", "TempOfRolling", "TempOfRolling", "TypeOfForming", "TempOfForming", "Remarks" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_SSS_WE_Nozzles(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                int SrNo = 0;
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "WEId", Convert.ToString(WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(newRecordId, "Description","","", "", false,"","Description"),
                                    GenerateAutoComplete(newRecordId, "Type","","", "", false,"","Type",false,"autocomplete"),
                                    GenerateAutoComplete(newRecordId, "NozzleId","","", "", false,"","NozzleId",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Thickness","","", "", false,"","Thickness",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Material","","", "", false,"","Material"),
                                    GenerateAutoComplete(newRecordId, "TypeOfRolling","","", "", false,"","TypeOfRolling",false,"autocomplete"),
                                    GenerateAutoComplete(newRecordId, "TempOfRolling","","", "", false,"","TempOfRolling",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "TypeOfForming","","", "", false,"","TypeOfForming",false,"autocomplete"),
                                    GenerateAutoComplete(newRecordId, "TempOfForming","","", "", false,"","TempOfForming",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Remarks","","", "", false,"","Remarks"),                                    
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "WEId", Convert.ToString(h.WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(h.LineId, "Description",h.Description,"", "", true,"","Description",false,"",h.Description),
                                    GenerateAutoComplete(h.LineId, "Type",h.Type,"", "", true,"","Type",false,"autocomplete",h.Type),
                                    GenerateAutoComplete(h.LineId, "NozzleId",h.NozzleId.ToString(),"", "", true,"","NozzleId",false,"numeric",h.NozzleId.ToString()),
                                    GenerateAutoComplete(h.LineId, "Thickness",h.Thickness.ToString(),"", "", true,"","Thickness",false,"numeric",h.Thickness.ToString()),
                                    GenerateAutoComplete(h.LineId, "Material",h.Material,"", "", true,"","Material",false,"",h.Material),
                                    GenerateAutoComplete(h.LineId, "TypeOfRolling",h.TypeOfRolling,"", "", true,"","TypeOfRolling",false,"autocomplete",h.TypeOfRolling),
                                    GenerateAutoComplete(h.LineId, "TempOfRolling",h.TempOfRolling.ToString(),"", "", true,"","TempOfRolling",false,"numeric",h.TempOfRolling.ToString()),
                                    GenerateAutoComplete(h.LineId, "TypeOfForming",h.TypeOfForming,"", "", true,"","TypeOfForming",false,"autocomplete",h.TypeOfForming),
                                    GenerateAutoComplete(h.LineId, "TempOfForming",h.TempOfForming.ToString(),"", "", true,"","TempOfForming",false,"numeric",h.TempOfForming.ToString()),
                                    GenerateAutoComplete(h.LineId, "Remarks",h.Remarks,"", "", true,"","Remarks",false,"",h.Remarks),                                    
                                    HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteLine("+ h.WEId+ "," + h.LineId +"); ")
                                    + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveNozzles(FormCollection fc, int Id = 0, int WEId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS008 objSSS008 = null;

                if (Id > 0)
                {
                    objSSS008 = db.SSS008.Where(x => x.LineId == Id).FirstOrDefault();
                    objSSS008.Description = fc["Description" + Id];
                    objSSS008.Type = fc["Type" + Id];
                    if (fc["NozzleId" + Id] == string.Empty)
                    {
                        objSSS008.NozzleId = null;
                    }
                    else
                    {
                        objSSS008.NozzleId = Convert.ToInt32(fc["NozzleId" + Id]);
                    }
                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS008.Thickness = null;
                    }
                    else
                    {
                        objSSS008.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }

                    objSSS008.Material = fc["Material" + Id];
                    objSSS008.TypeOfRolling = fc["TypeOfRolling" + Id];
                    if (fc["TempOfRolling" + Id] == string.Empty)
                    {
                        objSSS008.TempOfRolling = null;
                    }
                    else
                    {
                        objSSS008.TempOfRolling = Convert.ToInt32(fc["TempOfRolling" + Id]);
                    }

                    objSSS008.TypeOfForming = fc["TypeOfForming" + Id];
                    if (fc["TempOfForming" + Id] == string.Empty)
                    {
                        objSSS008.TempOfForming = null;
                    }
                    else
                    {
                        objSSS008.TempOfForming = Convert.ToInt32(fc["TempOfForming" + Id]);
                    }

                    objSSS008.Remarks = fc["Remarks" + Id];

                    objSSS008.EditedBy = objClsLoginInfo.UserName;
                    objSSS008.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS008 = new SSS008();
                    objSSS008.WEId = WEId;
                    objSSS008.Description = fc["Description" + Id];
                    objSSS008.Type = fc["Type" + Id];
                    if (fc["NozzleId" + Id] == string.Empty)
                    {
                        objSSS008.NozzleId = null;
                    }
                    else
                    {
                        objSSS008.NozzleId = Convert.ToInt32(fc["NozzleId" + Id]);
                    }
                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS008.Thickness = null;
                    }
                    else
                    {
                        objSSS008.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }

                    objSSS008.Material = fc["Material" + Id];
                    objSSS008.TypeOfRolling = fc["TypeOfRolling" + Id];
                    if (fc["TempOfRolling" + Id] == string.Empty)
                    {
                        objSSS008.TempOfRolling = null;
                    }
                    else
                    {
                        objSSS008.TempOfRolling = Convert.ToInt32(fc["TempOfRolling" + Id]);
                    }

                    objSSS008.TypeOfForming = fc["TypeOfForming" + Id];
                    if (fc["TempOfForming" + Id] == string.Empty)
                    {
                        objSSS008.TempOfForming = null;
                    }
                    else
                    {
                        objSSS008.TempOfForming = Convert.ToInt32(fc["TempOfForming" + Id]);
                    }

                    objSSS008.Remarks = fc["Remarks" + Id];

                    objSSS008.CreatedBy = objClsLoginInfo.UserName;
                    objSSS008.CreatedOn = DateTime.Now;
                    db.SSS008.Add(objSSS008);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteNozzles(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS008 objSSS008 = db.SSS008.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS008 != null)
                {
                    db.SSS008.Remove(objSSS008);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Nozzles successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Nozzles not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Tubesheet
        public ActionResult loadTubesheetDataTable(JQueryDataTableParamModel param, int WEId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";
                whereCondition += " and WEId= " + WEId.ToString();

                string[] columnName = { "Description", "Type", "Thickness", "Lip1Thickness", "Lip2Thickness", "TypeOfForming", "Material", "Remarks" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_SSS_WE_TubeSheet(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                int SrNo = 0;
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "WEId", Convert.ToString(WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(newRecordId, "Description","","", "", false,"","Description"),
                                    GenerateAutoComplete(newRecordId, "Type","","", "", false,"","Type"),
                                    GenerateAutoComplete(newRecordId, "Thickness","","", "", false,"","Thickness",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Lip1Thickness","","", "", false,"","Lip1Thickness",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Lip2Thickness","","", "", false,"","Lip2Thickness",false,"numeric"),
                                    //GenerateAutoComplete(newRecordId, "TypeOfForming","","", "", false,"","TypeOfForming",false,"autocomplete"),
                                    GenerateAutoComplete(newRecordId, "Material","","", "", false,"","Material"),
                                    GenerateAutoComplete(newRecordId, "Remarks","","", "", false,"","Remarks"),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "WEId", Convert.ToString(h.WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(h.LineId, "Description",h.Description,"", "", false,"","Description",true,"",h.Description),
                                    GenerateAutoComplete(h.LineId, "Type",h.Type,"", "", false,"","Type",true,"",h.Type),
                                    GenerateAutoComplete(h.LineId, "Thickness",h.Thickness.ToString(),"", "", false,"","Thickness",true,"numeric",h.Thickness.ToString()),
                                    GenerateAutoComplete(h.LineId, "Lip1Thickness",h.Lip1Thickness.ToString(),"", "", false,"","Lip1Thickness",true,"numeric",h.Lip1Thickness.ToString()),
                                    GenerateAutoComplete(h.LineId, "Lip2Thickness",h.Lip2Thickness.ToString(),"", "", false,"","Lip2Thickness",true,"numeric",h.Lip2Thickness.ToString()),
                                    //GenerateAutoComplete(h.LineId, "TypeOfForming",h.TypeOfForming,"", "", false,"","TypeOfForming",true,"",h.TypeOfForming),
                                    GenerateAutoComplete(h.LineId, "Material",h.Material,"", "", false,"","Material",true,"",h.Material),
                                    GenerateAutoComplete(h.LineId, "Remarks",h.Remarks,"", "", false,"","Remarks",true,"",h.Remarks),
                                    HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteLine("+ h.WEId+ "," + h.LineId +"); ")
                                    + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveTubesheet(FormCollection fc, int Id = 0, int WEId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS009 objSSS009 = null;

                if (Id > 0)
                {
                    objSSS009 = db.SSS009.Where(x => x.LineId == Id).FirstOrDefault();
                    objSSS009.Description = fc["Description" + Id];
                    objSSS009.Type = fc["Type" + Id];
                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS009.Thickness = null;
                    }
                    else
                    {
                        objSSS009.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }
                    if (fc["Lip1Thickness" + Id] == string.Empty)
                    {
                        objSSS009.Lip1Thickness = null;
                    }
                    else
                    {
                        objSSS009.Lip1Thickness = Convert.ToInt32(fc["Lip1Thickness" + Id]);
                    }
                    if (fc["Lip2Thickness" + Id] == string.Empty)
                    {
                        objSSS009.Lip2Thickness = null;
                    }
                    else
                    {
                        objSSS009.Lip2Thickness = Convert.ToInt32(fc["Lip2Thickness" + Id]);
                    }

                    objSSS009.Material = fc["Material" + Id];
                    objSSS009.TypeOfForming = fc["TypeOfForming" + Id];
                    objSSS009.Remarks = fc["Remarks" + Id];

                    objSSS009.EditedBy = objClsLoginInfo.UserName;
                    objSSS009.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS009 = new SSS009();
                    objSSS009.WEId = WEId;
                    objSSS009.Description = fc["Description" + Id];
                    objSSS009.Type = fc["Type" + Id];
                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS009.Thickness = null;
                    }
                    else
                    {
                        objSSS009.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }
                    if (fc["Lip1Thickness" + Id] == string.Empty)
                    {
                        objSSS009.Lip1Thickness = null;
                    }
                    else
                    {
                        objSSS009.Lip1Thickness = Convert.ToInt32(fc["Lip1Thickness" + Id]);
                    }
                    if (fc["Lip2Thickness" + Id] == string.Empty)
                    {
                        objSSS009.Lip2Thickness = null;
                    }
                    else
                    {
                        objSSS009.Lip2Thickness = Convert.ToInt32(fc["Lip2Thickness" + Id]);
                    }

                    objSSS009.Material = fc["Material" + Id];
                    objSSS009.TypeOfForming = fc["TypeOfForming" + Id];
                    objSSS009.Remarks = fc["Remarks" + Id];

                    objSSS009.CreatedBy = objClsLoginInfo.UserName;
                    objSSS009.CreatedOn = DateTime.Now;
                    db.SSS009.Add(objSSS009);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteTubesheet(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS009 objSSS009 = db.SSS009.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS009 != null)
                {
                    db.SSS009.Remove(objSSS009);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Tubesheet successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Tubesheet not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Girth Flanges
        public ActionResult loadGirthFlangesDataTable(JQueryDataTableParamModel param, int WEId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";
                whereCondition += " and WEId= " + WEId.ToString();

                string[] columnName = { "Description", "Type", "Thickness", "Lip1Thickness", "Lip2Thickness", "TypeOfForming", "Material", "Remarks" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstPam = db.SP_SSS_WE_GirthFlanges(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                int SrNo = 0;
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "WEId", Convert.ToString(WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(newRecordId, "Description","","", "", false,"","Description"),
                                    GenerateAutoComplete(newRecordId, "Thickness","","", "", false,"","Thickness",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Material","","", "", false,"","Material"),
                                    GenerateAutoComplete(newRecordId, "Remarks","","", "", false,"","Remarks"),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "WEId", Convert.ToString(h.WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(h.LineId, "Description",h.Description,"", "", true,"","Description",false,"",h.Description),
                                    GenerateAutoComplete(h.LineId, "Thickness",h.Thickness.ToString(),"", "", true,"","Thickness",false,"numeric",h.Thickness.ToString()),
                                    GenerateAutoComplete(h.LineId, "Material",h.Material,"", "", true,"","Material",false,"",h.Material),
                                    GenerateAutoComplete(h.LineId, "Remarks",h.Remarks,"", "", true,"","Remarks",false,"",h.Remarks),                                    
                                    HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteLine("+ h.WEId+ "," + h.LineId +"); ")
                                    + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveGirthFlanges(FormCollection fc, int Id = 0, int WEId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS010 objSSS010 = null;

                if (Id > 0)
                {
                    objSSS010 = db.SSS010.Where(x => x.LineId == Id).FirstOrDefault();
                    objSSS010.Description = fc["Description" + Id];

                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS010.Thickness = null;
                    }
                    else
                    {
                        objSSS010.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }

                    objSSS010.Material = fc["Material" + Id];
                    objSSS010.Remarks = fc["Remarks" + Id];

                    objSSS010.EditedBy = objClsLoginInfo.UserName;
                    objSSS010.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS010 = new SSS010();
                    objSSS010.WEId = WEId;
                    objSSS010.Description = fc["Description" + Id];

                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS010.Thickness = null;
                    }
                    else
                    {
                        objSSS010.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }

                    objSSS010.Material = fc["Material" + Id];
                    objSSS010.Remarks = fc["Remarks" + Id];

                    objSSS010.CreatedBy = objClsLoginInfo.UserName;
                    objSSS010.CreatedOn = DateTime.Now;
                    db.SSS010.Add(objSSS010);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteGirthFlanges(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS010 objSSS010 = db.SSS010.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS010 != null)
                {
                    db.SSS010.Remove(objSSS010);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Girth Flanges successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Girth Flanges not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Tubes
        public ActionResult loadTubesDataTable(JQueryDataTableParamModel param, int WEId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";
                whereCondition += " and WEId= " + WEId.ToString();

                string[] columnName = { "Description", "Type", "Thickness", "Lip1Thickness", "Lip2Thickness", "TypeOfForming", "Material", "Remarks" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstPam = db.SP_SSS_WE_Tubes(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                int SrNo = 0;
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "WEId", Convert.ToString(WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(newRecordId, "Description","","", "", false,"","Description"),
                                    GenerateAutoComplete(newRecordId, "Type","","", "", false,"","Type",false,"autocomplete"),
                                    GenerateAutoComplete(newRecordId, "OD","","", "", false,"","OD",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Thickness","","", "", false,"","Thickness",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Material","","", "", false,"","Material"),
                                    GenerateAutoComplete(newRecordId, "UBendRadius","","", "", false,"","UBendRadius",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Remarks","","", "", false,"","Remarks"),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "WEId", Convert.ToString(h.WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(h.LineId, "Description",h.Description,"", "", true,"","Description",false,"",h.Description),
                                    GenerateAutoComplete(h.LineId, "Type",h.Type,"", "", true,"","Type",false,"autocomplete",h.Type),
                                    GenerateAutoComplete(h.LineId, "OD",h.OD.ToString(),"", "", true,"","OD",false,"numeric",h.OD.ToString()),
                                    GenerateAutoComplete(h.LineId, "Thickness",h.Thickness.ToString(),"", "", true,"","Thickness",false,"numeric",h.Thickness.ToString()),
                                    GenerateAutoComplete(h.LineId, "Material",h.Material,"", "", true,"","Material",false,"",h.Material),
                                    GenerateAutoComplete(h.LineId, "UBendRadius",h.UBendRadius.ToString(),"", "", true,"","UBendRadius",false,"numeric",h.UBendRadius.ToString()),
                                    GenerateAutoComplete(h.LineId, "Remarks",h.Remarks,"", "", true,"","Remarks",false,"",h.Remarks),                                    
                                    HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteLine("+ h.WEId+ "," + h.LineId +"); ")
                                    + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveTubes(FormCollection fc, int Id = 0, int WEId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS011 objSSS011 = null;

                if (Id > 0)
                {
                    objSSS011 = db.SSS011.Where(x => x.LineId == Id).FirstOrDefault();
                    objSSS011.Description = fc["Description" + Id];
                    objSSS011.Type = fc["Type" + Id];

                    if (fc["OD" + Id] == string.Empty)
                    {
                        objSSS011.OD = null;
                    }
                    else
                    {
                        objSSS011.OD = Convert.ToInt32(fc["OD" + Id]);
                    }

                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS011.Thickness = null;
                    }
                    else
                    {
                        objSSS011.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }

                    objSSS011.Material = fc["Material" + Id];

                    if (fc["UBendRadius" + Id] == string.Empty)
                    {
                        objSSS011.UBendRadius = null;
                    }
                    else
                    {
                        objSSS011.UBendRadius = Convert.ToInt32(fc["UBendRadius" + Id]);
                    }

                    objSSS011.Remarks = fc["Remarks" + Id];

                    objSSS011.EditedBy = objClsLoginInfo.UserName;
                    objSSS011.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS011 = new SSS011();
                    objSSS011.WEId = WEId;
                    objSSS011.Description = fc["Description" + Id];
                    objSSS011.Type = fc["Type" + Id];

                    if (fc["OD" + Id] == string.Empty)
                    {
                        objSSS011.OD = null;
                    }
                    else
                    {
                        objSSS011.OD = Convert.ToInt32(fc["OD" + Id]);
                    }

                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS011.Thickness = null;
                    }
                    else
                    {
                        objSSS011.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }

                    objSSS011.Material = fc["Material" + Id];

                    if (fc["UBendRadius" + Id] == string.Empty)
                    {
                        objSSS011.UBendRadius = null;
                    }
                    else
                    {
                        objSSS011.UBendRadius = Convert.ToInt32(fc["UBendRadius" + Id]);
                    }

                    objSSS011.Remarks = fc["Remarks" + Id];

                    objSSS011.CreatedBy = objClsLoginInfo.UserName;
                    objSSS011.CreatedOn = DateTime.Now;
                    db.SSS011.Add(objSSS011);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteTubes(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS011 objSSS011 = db.SSS011.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS011 != null)
                {
                    db.SSS011.Remove(objSSS011);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Tubes successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Tubes not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Other Components
        public ActionResult loadOtherComponentsTable(JQueryDataTableParamModel param, int WEId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";
                whereCondition += " and WEId= " + WEId.ToString();

                string[] columnName = { "Description", "Type", "Thickness", "Lip1Thickness", "Lip2Thickness", "TypeOfForming", "Material", "Remarks" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstPam = db.SP_SSS_WE_OtherComponents(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                int SrNo = 0;
                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "WEId", Convert.ToString(WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(newRecordId, "Description","","", "", false,"","Description"),
                                    GenerateAutoComplete(newRecordId, "Thickness","","", "", false,"","Thickness",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Material","","", "", false,"","Material"),
                                    GenerateAutoComplete(newRecordId, "Remarks","","", "", false,"","Remarks"),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "WEId", Convert.ToString(h.WEId)),
                                    (SrNo++).ToString(),
                                    GenerateAutoComplete(h.LineId, "Description",h.Description,"", "", true,"","Description",false,"",h.Description),
                                    GenerateAutoComplete(h.LineId, "Thickness",h.Thickness.ToString(),"", "", true,"","Thickness",false,"numeric",h.Thickness.ToString()),
                                    GenerateAutoComplete(h.LineId, "Material",h.Material,"", "", true,"","Material",false,"",h.Material),
                                    GenerateAutoComplete(h.LineId, "Remarks",h.Remarks,"", "", true,"","Remarks",false,"",h.Remarks),                                    
                                    HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteLine("+ h.WEId+ "," + h.LineId +"); ")
                                    + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveOtherComponents(FormCollection fc, int Id = 0, int WEId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS012 objSSS012 = null;

                if (Id > 0)
                {
                    objSSS012 = db.SSS012.Where(x => x.LineId == Id).FirstOrDefault();
                    objSSS012.Description = fc["Description" + Id];

                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS012.Thickness = null;
                    }
                    else
                    {
                        objSSS012.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }

                    objSSS012.Material = fc["Material" + Id];
                    objSSS012.Remarks = fc["Remarks" + Id];

                    objSSS012.EditedBy = objClsLoginInfo.UserName;
                    objSSS012.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS012 = new SSS012();
                    objSSS012.WEId = WEId;
                    objSSS012.Description = fc["Description" + Id];

                    if (fc["Thickness" + Id] == string.Empty)
                    {
                        objSSS012.Thickness = null;
                    }
                    else
                    {
                        objSSS012.Thickness = Convert.ToInt32(fc["Thickness" + Id]);
                    }

                    objSSS012.Material = fc["Material" + Id];
                    objSSS012.Remarks = fc["Remarks" + Id];

                    objSSS012.CreatedBy = objClsLoginInfo.UserName;
                    objSSS012.CreatedOn = DateTime.Now;
                    db.SSS012.Add(objSSS012);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteOtherComponents(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS012 objSSS012 = db.SSS012.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS012 != null)
                {
                    db.SSS012.Remove(objSSS012);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Other Components successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Other Components not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Excell
        //Code By : Ajay Chauhan on 13/11/2017, Export Funxtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_SSS_GETWeldingInput(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      SSSNo = li.SSSNo,
                                      Contract = li.Contract,
                                      Customer = li.Customer,
                                      CSRMeetingOn = li.CSRMeetingOn,
                                      TargetDate = li.TargetDate != null ? Convert.ToString(li.TargetDate) : "",
                                      DesignCode = li.DesignCode != null ? Convert.ToString(li.DesignCode) : "",
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.WI_CYLINDRICALSHEET.GetStringValue())
                {
                    var lst = db.SP_SSS_WE_GETCylindricalShell(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      ShellDescription = li.ShellDescription,
                                      ShellId = li.ShellId,
                                      Thick = li.Thick,
                                      Material = li.Material,
                                      TypeOfRolling = li.TypeOfRolling,
                                      TempOfRolling = li.TempOfRolling,
                                      Remarks = li.Remarks
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.WI_CONE.GetStringValue())
                {
                    var lst = db.SP_SSS_WE_GETCone(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Description = li.Description,
                                      Type = li.Type,
                                      LargeConeId = li.LargeConeId,
                                      SmallConeId = li.SmallConeId,
                                      Thickness = li.Thickness,
                                      Material = li.Material,
                                      TypeOfForming = li.TypeOfForming,
                                      TempOfForming = li.TempOfForming,
                                      Remarks = li.Remarks,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.WI_DISHED_END.GetStringValue())
                {
                    var lst = db.SP_SSS_WE_DishedEnd(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Description = li.Description,
                                      Type = li.Type,
                                      Radius = li.Radius,
                                      Radius2 = li.Radius2,
                                      Thickness = li.Thickness,
                                      Material = li.Material,
                                      TypeOfForming = li.TypeOfForming,
                                      TempOfForming = li.TempOfForming,
                                      Remarks = li.Remarks,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.WI_NOZZLES.GetStringValue())
                {
                    var lst = db.SP_SSS_WE_Nozzles(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Description = li.Description,
                                      Type = li.Type,
                                      NozzleId = li.NozzleId,
                                      Thickness = li.Thickness,
                                      Material = li.Material,
                                      TypeOfRolling = li.TypeOfRolling,
                                      TempOfRolling = li.TempOfRolling,
                                      TypeOfForming = li.TypeOfForming,
                                      TempOfForming = li.TempOfForming,
                                      Remarks = li.Remarks,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.WI_TUBESHEET.GetStringValue())
                {
                    var lst = db.SP_SSS_WE_TubeSheet(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Description = li.Description,
                                      Type = li.Type,
                                      Thickness = li.Thickness,
                                      Lip1Thickness = li.Lip1Thickness,
                                      Lip2Thickness = li.Lip2Thickness,
                                      Material = li.Material,
                                      Remarks = li.Remarks,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.WI_GIRTH_FLANGES.GetStringValue())
                {
                    var lst = db.SP_SSS_WE_GirthFlanges(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Description = li.Description,
                                      Thickness = li.Thickness,
                                      Material = li.Material,
                                      Remarks = li.Remarks,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.WI_TUBES.GetStringValue())
                {
                    var lst = db.SP_SSS_WE_Tubes(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Description = li.Description,
                                      Type = li.Type,
                                      OD = li.OD,
                                      Thickness = li.Thickness,
                                      Material = li.Material,
                                      UBendRadius = li.UBendRadius,                                      
                                      Remarks = li.Remarks,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.WI_OTHER_COMPONENTS.GetStringValue())
                {
                    var lst = db.SP_SSS_WE_OtherComponents(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Description = li.Description,
                                      Thickness = li.Thickness,
                                      Material = li.Material,
                                      Remarks = li.Remarks,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string cssClass = "", string title = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + (cssClass != string.Empty ? cssClass : "");
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " title='" + title + "' />";

            return strAutoComplete;
        }
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }

        public static List<string> getTypesOfRolling()
        {
            List<string> items = GetEnumDescription<TypesOfRolling>();
            return items;
        }
        public static List<string> getTypesOfForming()
        {
            List<string> items = GetEnumDescription<TypesOfForming>();
            return items;
        }
        public static List<string> getDishedEndType()
        {
            List<string> items = GetEnumDescription<DishedEndType>();
            return items;
        }
        public static List<string> getTubeTypes()
        {
            List<string> items = GetEnumDescription<TubeTypes>();
            return items;
        }
        public static List<string> getTypeOfNozzle()
        {
            List<string> items = GetEnumDescription<TypeOfNozzle>();
            return items;
        }
        public static List<string> GetEnumDescription<T>() where T : struct
        {
            Type t = typeof(T);
            return !t.IsEnum ? null : Enum.GetValues(t).Cast<Enum>().Select(x => x.GetStringValue()).ToList();
        }

    }
}