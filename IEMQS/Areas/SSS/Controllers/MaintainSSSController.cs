﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.PAM.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.SSS.Controllers
{
    public class MaintainSSSController : clsBase
    {
        // GET: SSS/MaintainSSS
        public ActionResult Index(int roleId)
        {
            //if (roleId == Convert.ToInt32(RoleForSSS.DE))
            //{

            //}
            //else if (roleId == Convert.ToInt32(RoleForSSS.DE))
            //{

            //}
            ViewBag.RoleId = roleId;
            return View();
        }

        public ActionResult IndexDE()
        {
            return RedirectToAction("Index", "MaintainSSS", new { roleId = Convert.ToInt32(RoleForSSS.DE) });
        }
        public ActionResult IndexPMG()
        {
            return RedirectToAction("Index", "MaintainSSS", new { roleId = Convert.ToInt32(RoleForSSS.PMG) });
        }
        public ActionResult GETMaintainSSSHeaderPartial(string status, string RoleId)
        {
            ViewBag.Status = status;
            ViewBag.RoleId = RoleId;
            return PartialView("_GetSSSDataGridPartial");
        }

        public ActionResult loadSSSHeaderDataTable(JQueryDataTableParamModel param, string status, int RoleId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";

                if (status.ToUpper() == "PENDING")
                {
                    if (RoleId == Convert.ToInt32(RoleForSSS.PMG))
                    {
                        whereCondition += " and s1.status in ('" + SSSStatus.Draft.GetStringValue() + "')";
                    }
                    else if (RoleId == Convert.ToInt32(RoleForSSS.DE))
                    {
                        whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and s1.status in ('')";
                    }
                }
                else
                {
                    whereCondition += " and s1.status in ('" + SSSStatus.Draft.GetStringValue() + "','" + SSSStatus.Submitted.GetStringValue() + "')";
                }

                string[] columnName = { "SSSId", "SSSNo", "SINNo", "Issue", "s1.Status", "Contract", "Customer", "DSSNo", "CSRMeetingOn", "c3.t_psno+'-'+c3.t_name" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_SSS_GETMaintainSSSHeader(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstPam
                          select new[] {
                               h.SSSNo.ToString(),
                               h.Contract.ToString(),
                               h.Customer.ToString(),
                               //h.DSSNo.ToString(),
                               h.SINNo!=null?( h.SINNo.ToString()):"",
                               Convert.ToString(Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy")),
                               h.CreatedBy.ToString(),
                               h.Status.ToString(),
                               "", //Action
                               h.SSSId.ToString()
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult AddSSS(int id = 0, int RoleId = 0)
        {
            var objSSS001 = db.SSS001.Where(x => x.SSSId == id).FirstOrDefault();
            if (objSSS001 == null)
            {
                objSSS001 = new SSS001();
                objSSS001.SSSId = 0;
                objSSS001.DSSId = 0;
                objSSS001.Status = SSSStatus.Draft.GetStringValue();
            }
            else
            {
                var lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_dimx + "-" + x.t_desc), CatID = x.t_dimx }).ToList();
                ViewBag.lstDepartment = lstDepartment;

            }
            ViewBag.RoleId = RoleId;
            return View("AddUpdateSSS", objSSS001);
        }

        public ActionResult loadProjectDataTable(JQueryDataTableParamModel param, int DSSId)
        {
            try
            {
                MKTController mktController = new MKTController();
                var objPAM006 = db.PAM006.FirstOrDefault(x => x.DSSId == DSSId);
                if (objPAM006 != null)
                    return mktController.loadProjectDataTable(param, objPAM006.HeaderId);
                else
                    return View();
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ActionResult GetSSSHeaderDetails(int SSSId, bool DisplayDSS)
        {
            var objSSS001 = db.SSS001.Where(x => x.SSSId == SSSId).FirstOrDefault();
            if (objSSS001 == null)
            {
                objSSS001 = new SSS001();
                objSSS001.SSSId = 0;
                objSSS001.DSSId = 0;
                objSSS001.Status = SSSStatus.Draft.GetStringValue();
                ViewBag.PAMHeaderId = 0;
            }
            else
            {
                ViewBag.Contract = db.Database.SqlQuery<string>(@"SELECT t_cono +'-'+ t_desc FROM    " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objSSS001.Contract + "'").FirstOrDefault();
                ViewBag.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175 where t_bpid = '" + objSSS001.Customer + "'").FirstOrDefault();

                var PAMHeaderId = (from p1 in db.PAM001
                                   join p6 in db.PAM006 on p1.HeaderId equals p6.HeaderId
                                   join s1 in db.SSS001 on p6.DSSId equals s1.DSSId
                                   where s1.SSSId == SSSId
                                   select p1.HeaderId
                                 ).FirstOrDefault();
                ViewBag.PAMHeaderId = PAMHeaderId;
                //var lstDSSNo = db.SP_SSS_GetIssueSIN("", objSSS001.DSSId).FirstOrDefault();
                //if (lstDSSNo != null)
                //{
                //    ViewBag.SINNo = lstDSSNo.SINNo;
                //}
            }
            ViewBag.DisplayDSS = DisplayDSS;
            return PartialView("_SSSHeaderDetails", objSSS001);
        }

        public ActionResult GetContractDetailsByDSSId(int DSSId)
        {
            string Contract = string.Empty;
            string Customer = string.Empty;
            string ContractNo = string.Empty;
            string CustomerNo = string.Empty;
            string SSSNo = string.Empty;
            try
            {
                var objPAM006 = db.PAM006.Where(i => i.DSSId == DSSId).FirstOrDefault();

                PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == objPAM006.HeaderId);

                Contract = db.Database.SqlQuery<string>(@"SELECT t_cono +'-'+ t_desc FROM    " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objPAM001.ContractNo + "'").FirstOrDefault();
                Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPAM001.Customer + "'").FirstOrDefault();
                ContractNo = objPAM001.ContractNo;
                CustomerNo = objPAM001.Customer;
                SSSNo = MakeSSSNo(objPAM001.ContractNo);
            }
            catch (Exception)
            {
                throw;
            }
            return Json(new
            {
                Contract = Contract,
                Customer = Customer,
                ContractNo = ContractNo,
                CustomerNo = CustomerNo,
                SSSNo = SSSNo
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDSSNo(string DSSNo)
        {
            //var lstDSSNo = (from p4 in db.PAM006
            //                join s1 in db.SSS001 on p4.DSSNo equals s1.DSSNo into w410
            //                from p in w410.DefaultIfEmpty()
            //                where p4.DSSNo.Contains(DSSNo)
            //                select new { DSSNo = p4.DSSNo, DSSId = p4.DSSId, SSSId = ((p4.DSSNo == p.DSSNo) ? p.SSSId : 0) }
            //             );

            //var finalDSSNo = lstDSSNo.Where(i => i.SSSId == 0).Select(i => new CategoryData { Value = i.DSSId.ToString(), CategoryDescription = i.DSSNo }).ToList();
            List<CategoryData> finalDSSNo = new List<CategoryData>();
            var lstDSSNo = db.SP_SSS_GetIssueSIN(DSSNo, 0);
            if (lstDSSNo != null)
            {
                finalDSSNo = lstDSSNo.Where(i => i.SSSId == 0).Select(i => new CategoryData { Value = i.DSSId.ToString(), CategoryDescription = i.SINNo }).ToList();
            }
            return Json(finalDSSNo, JsonRequestBehavior.AllowGet);
        }

        public string MakeSSSNo(string ContractNo)
        {
            var SSSNo = string.Empty;
            var mtNumber = "1";
            var objSSS001 = db.SSS001.Where(i => i.SSSNo.Contains(ContractNo + "_SSS_")).ToList();
            if (objSSS001.Count > 0)
            {
                int MaxNo = 0;
                foreach (var item in objSSS001)
                {
                    string SplitId = item.SSSNo.Split('_').Last();
                    if (MaxNo < Convert.ToInt32(SplitId))
                    {
                        MaxNo = Convert.ToInt32(SplitId);
                    }
                }
                mtNumber = (MaxNo + 1).ToString();
            }
            SSSNo = ContractNo + "_SSS_" + mtNumber.PadLeft(3, '0');
            //HandoverReport = ContractNo + "/PDH/" + mtNumber.PadLeft(1, '0');
            return SSSNo;
        }
        [HttpPost]
        public ActionResult SaveSSS(SSS001 objSSS001)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (Convert.ToInt32(objSSS001.SSSId) > 0)
                {
                    var objSSS001Update = db.SSS001.Where(i => i.SSSId == objSSS001.SSSId).FirstOrDefault();
                    if (objSSS001.CSRMeetingOn.HasValue)
                    {
                        objSSS001Update.CSRMeetingOn = objSSS001.CSRMeetingOn.HasValue ? Manager.getDateTime(objSSS001.CSRMeetingOn.Value.ToShortDateString()) : objSSS001.CSRMeetingOn;
                    }
                    else
                    {
                        objSSS001Update.CSRMeetingOn = null;
                    }
                    objSSS001Update.EditedBy = objClsLoginInfo.UserName;
                    objSSS001Update.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objSSS001Update.SSSId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "SSS Updated Successfully";
                }
                else
                {
                    var DSSNo = db.PAM006.Where(i => i.DSSId == objSSS001.DSSId).FirstOrDefault().DSSNo;
                    SSS001 objSSS001New = new SSS001();
                    objSSS001New.Contract = objSSS001.Contract;
                    if (objSSS001.CSRMeetingOn.HasValue)
                    {
                        objSSS001New.CSRMeetingOn = objSSS001.CSRMeetingOn.HasValue ? Manager.getDateTime(objSSS001.CSRMeetingOn.Value.ToShortDateString()) : objSSS001.CSRMeetingOn;
                    }
                    else
                    {
                        objSSS001New.CSRMeetingOn = null;
                    }
                    objSSS001New.Customer = objSSS001.Customer;
                    objSSS001New.DSSId = objSSS001.DSSId;
                    objSSS001New.DSSNo = DSSNo;
                    objSSS001New.Issue = objSSS001.Issue;
                    objSSS001New.SSSNo = objSSS001.SSSNo;
                    objSSS001New.SINNo = objSSS001.SINNo;
                    objSSS001New.Status = SSSStatus.Draft.GetStringValue();
                    objSSS001New.CreatedBy = objClsLoginInfo.UserName;
                    objSSS001New.CreatedOn = DateTime.Now;
                    db.SSS001.Add(objSSS001New);
                    db.SaveChanges();

                    objResponseMsg.HeaderId = objSSS001New.SSSId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "SSS Save Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet); ;
        }

        [HttpPost]
        public ActionResult SubmitSSS(int SSSId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS001 objSSS001 = db.SSS001.Where(x => x.SSSId == SSSId).FirstOrDefault();

                if (objSSS001 != null)
                {
                    if (db.SSS002.Where(i => i.SSSId == SSSId).Any())
                    {
                        objSSS001.Status = SSSStatus.Submitted.GetStringValue();
                        objSSS001.SubmittedBy = objClsLoginInfo.UserName;
                        objSSS001.SubmittedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "SSS Submitted successfully.";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Add Atleast One Department For Submit SSS.";
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "SSS not available for submit.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region SSS Department
        public ActionResult GetSSSDepartment(int SSSId)
        {
            ViewBag.SSSId = SSSId.ToString();
            ViewBag.Status = db.SSS001.Where(i => i.SSSId == SSSId).FirstOrDefault().Status.ToLower();
            ViewBag.lstDepartment = db.COM002.Where(x => x.t_dtyp == 3).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_dimx + "-" + x.t_desc), CatID = x.t_dimx }).ToList();

            return PartialView("~/Areas/SSS/Views/Shared/_SSSDepartmentDetails.cshtml");
        }

        public ActionResult loadSSSDeptDataTable(JQueryDataTableParamModel param, int SSSId, string ForApprove)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";
                whereCondition += " and SSSId= " + SSSId.ToString();

                string[] columnName = { "Department", "RecievedBy" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstPam = db.SP_SSS_GETSSSDEPARMENTDETAILS(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var Status = db.SSS001.FirstOrDefault(i => i.SSSId == SSSId).Status;

                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "SSSId", Convert.ToString(SSSId)),
                                    GenerateAutoComplete(newRecordId, "Department","","", "", false,"","Department")+""+Helper.GenerateHidden(newRecordId, "ddlDepartment", Convert.ToString(newRecordId)),
                                    GenerateAutoComplete(newRecordId, "RecievedBy","","", "", false,"","RecievedBy")+""+Helper.GenerateHidden(newRecordId, "ddlRecievedBy", Convert.ToString(newRecordId)),
                                    GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveDeptRecord(0);" ),
                                    "",
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "SSSId", Convert.ToString(h.SSSId)),
                                    GenerateAutoComplete(h.LineId, "Department",h.Department,"", "", true,"","Department")+""+Helper.GenerateHidden(h.LineId, "ddlDepartment",h.Department),
                                    GenerateAutoComplete(h.LineId, "RecievedBy",h.RecievedBy,"", "", true,"","RecievedBy")+""+Helper.GenerateHidden(h.LineId, "ddlRecievedBy", Convert.ToString(h.RecievedBy)),
                                    Status.ToLower()==SSSStatus.Draft.GetStringValue().ToLower()? HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteDepartmentLine("+ h.SSSId+ "," + h.LineId +"); "):"",
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = columnName == "SeamDescription" ? "autocomplete form-control clsseamdesc" : "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        public JsonResult GetPersonDeptWise(string department)
        {
            var employeslist = db.COM003.Where(x => x.t_depc == department && x.t_actv == 1).ToList();
            if (employeslist.Count > 0)
            {
                var lstEmployee = db.COM003.Where(x => x.t_depc == department && x.t_actv == 1).Select(x => new BULocWiseCategoryModel { CatDesc = (x.t_psno + "-" + x.t_name), CatID = x.t_psno }).ToList();
                return Json(new { lstPerson = lstEmployee }, JsonRequestBehavior.AllowGet);
            }
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveSSSDepartment(FormCollection fc, int Id = 0, int sssid = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS002 objSSS002 = null;
                if (db.SSS001.Where(i => i.SSSId == sssid).FirstOrDefault().Status.ToLower() == SSSStatus.Draft.GetStringValue().ToLower())
                {
                    if (Id > 0)
                    {
                        objSSS002 = db.SSS002.Where(x => x.LineId == Id).FirstOrDefault();
                        objSSS002.Department = fc["ddlDepartment" + Id];
                        objSSS002.RecievedBy = fc["ddlRecievedBy" + Id];
                        objSSS002.EditedBy = objClsLoginInfo.UserName;
                        objSSS002.EditedOn = DateTime.Now;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                        db.SaveChanges();
                    }
                    else
                    {
                        string Department = fc["ddlDepartment" + Id];
                        if (!db.SSS002.Where(i => i.SSSId == sssid && i.Department == Department).Any())
                        {
                            objSSS002 = new SSS002();
                            objSSS002.SSSId = sssid;
                            objSSS002.Department = fc["ddlDepartment" + Id];
                            objSSS002.RecievedBy = fc["ddlRecievedBy" + Id];
                            objSSS002.CreatedBy = objClsLoginInfo.UserName;
                            objSSS002.CreatedOn = DateTime.Now;
                            objSSS002.DSSId = db.SSS001.FirstOrDefault(i => i.SSSId == sssid).DSSId;
                            db.SSS002.Add(objSSS002);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Department Already Exist";
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "You Can Not Add Department In Submitted Status";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateDept(int LineId, bool Acknowledged)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS002 objSSS002 = db.SSS002.Where(x => x.LineId == LineId).FirstOrDefault();
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteDepartmentLines(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS002 objSSS002 = db.SSS002.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS002 != null)
                {
                    if (db.SSS001.Where(i => i.SSSId == objSSS002.SSSId).FirstOrDefault().Status.ToLower() == SSSStatus.Draft.GetStringValue().ToLower())
                    {

                        db.SSS002.Remove(objSSS002);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Department successfully deleted.";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "You Can Not Delete Department In Submitted Status";
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Department not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Excell
        //Code By : Ajay Chauhan on 09/11/2017, Export Funxtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_SSS_GETMaintainSSSHeader(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      SSSId = li.SSSId,
                                      SSSNo = li.SSSNo,
                                      Contract = li.Contract,
                                      Customer = li.Customer,
                                      DSSNo = li.DSSNo,
                                      CSRMeetingOn = li.CSRMeetingOn,
                                      CreatedBy = li.CreatedBy,
                                      Status = li.Status,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }

    public enum SSSStatus
    {
        [StringValue("Draft")]
        Draft,
        [StringValue("Submitted")]
        Submitted
    }
    public enum PCCInputStatus
    {
        [StringValue("Draft")]
        Draft,
        [StringValue("Submitted")]
        Submitted,
        [StringValue("Completed")]
        Completed
    }
    public enum RoleForSSS
    {
        [StringValue("PMG")]
        PMG = 1,
        [StringValue("DE")]
        DE = 2,
        [StringValue("WE")]
        WE = 3,
        [StringValue("PCC")]
        PCC = 4,
        [StringValue("NDT")]
        NDT = 5,
        [StringValue("SUB")]
        SUB = 6,
        [StringValue("SHOP")]
        SHOP = 7,
        [StringValue("MOM")]
        MOM = 8,
    }
    public enum TypesOfRolling
    {
        [StringValue("Hot Rolling")]
        HotRolling,
        [StringValue("Warm Rolling")]
        WarmRolling,
        [StringValue("Cold Rolling")]
        ColdRolling,
    }
    public enum TypesOfForming
    {
        [StringValue("Hot Forming")]
        HotForming,
        [StringValue("Warm Forming")]
        WarmForming,
        [StringValue("Cold Forming")]
        ColdForming,
    }
    public enum DishedEndType
    {
        [StringValue("Hemi-Head")]
        HemiHead,
        [StringValue("Ellipsoidal Head")]
        EllipsoidalHead,
        [StringValue("Tori spherical Head")]
        TorisphericalHead,
    }
    public enum TubeTypes
    {
        [StringValue("U-Tube")]
        UTube,
        [StringValue("Bend Tube")]
        BendTube,
        [StringValue("Straight Tube")]
        StraightTube,
    }
    public enum TypeOfNozzle
    {
        [StringValue("Self Reinforced")]
        SelfReinforced,
        [StringValue("Nozzle with Reinforcing pad")]
        NozzleWithReinforcingPad,

    }
}