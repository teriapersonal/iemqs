﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.SSS.Controllers
{
    public class ExpansionMockUpController : clsBase
    {
        // GET: SSS/ExpansionMockUp
        public ActionResult Index(int roleId)
        {
            ViewBag.RoleId = roleId;
            return View();
        }

        public ActionResult IndexDE()
        {
            return RedirectToAction("Index", "ExpansionMockUp", new { roleId = Convert.ToInt32(RoleForSSS.DE) });
        }
        public ActionResult IndexPCC()
        {
            return RedirectToAction("Index", "ExpansionMockUp", new { roleId = Convert.ToInt32(RoleForSSS.PCC) });
        }
        public ActionResult GETExpansionMockUpPartial(string status, string RoleId)
        {
            ViewBag.Status = status;
            ViewBag.RoleId = RoleId;
            return PartialView("_GetExpansionMockUpDataGridPartial");
        }
        public ActionResult GetExpansionMockUpLinePartial(int ExpansionMockUpId = 0)
        {
            var PAMHeaderId = (from s14 in db.SSS014
                               join s1 in db.SSS001 on s14.SSSId equals s1.SSSId
                               join p6 in db.PAM006 on s1.DSSId equals p6.DSSId
                               select p6.HeaderId
                             ).FirstOrDefault();
            ViewBag.Projects = db.SP_PAM_GETPROJECTLIST(PAMHeaderId).Select(i => new BULocWiseCategoryModel { CatID = i.id, CatDesc = i.text }).ToList();
            return PartialView("_GetExpansionMockUpLinePartial");
        }
        public ActionResult loadExpansionMockUpDataTable(JQueryDataTableParamModel param, string status, int RoleId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";

                if (status.ToUpper() == "PENDING")
                {
                    if (RoleId == Convert.ToInt32(RoleForSSS.DE))
                    {
                        whereCondition += " and s1.status in ('" + PCCInputStatus.Submitted.GetStringValue() + "')";
                    }
                    else if (RoleId == Convert.ToInt32(RoleForSSS.PCC))
                    {
                        whereCondition += " and s1.status in ('" + PCCInputStatus.Submitted.GetStringValue() + "') and ISNULL(s14.ExpansionMockUpId,'0')>0 and s14.ToPerson='" + objClsLoginInfo.UserName + "' and s14.status in ('" + PCCInputStatus.Submitted.GetStringValue() + "')";
                    }
                }
                else
                {
                    if (RoleId == Convert.ToInt32(RoleForSSS.DE))
                    {
                        whereCondition += " and s1.status in ('" + PCCInputStatus.Submitted.GetStringValue() + "')";
                    }
                    else if (RoleId == Convert.ToInt32(RoleForSSS.PCC))
                    {
                        whereCondition += " and s14.ToPerson='" + objClsLoginInfo.UserName + "'";
                    }
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "s1.SSSId", "s1.SSSNo", "s1.Contract", "s1.Customer", "CSRMeetingOn", "TargetDate", "s14.Status" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }


                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                //var lstPam = db.SP_SSS_GETPCCInput(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var lstPam = db.SP_SSS_GETExpansionMockUp(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstPam
                          select new[] {
                              h.SSSNo.ToString(),
                              h.Contract.ToString(),
                              h.Customer.ToString(),
                              Convert.ToString(Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy")),
                              Convert.ToString(Convert.ToDateTime(h.TargetDate).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.TargetDate).ToString("dd/MM/yyyy")),
                              h.Status!=null ?h.Status.ToString():"",//h.Status!=null ?h.Status.ToString():"",
                              "", //Action
                              h.SSSId.ToString(),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult ExpansionMockUpForm(int SSSId = 0, int RoleId = 0)
        {
            ViewBag.SSSId = SSSId;
            var objSSS014 = db.SSS014.Where(i => i.SSSId == SSSId).FirstOrDefault();
            if (objSSS014 == null)
            {
                objSSS014 = new SSS014();
                objSSS014.SSSId = SSSId;
                objSSS014.Status = PCCInputStatus.Draft.GetStringValue();
            }
            var listEmployee = Manager.GetApprover(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objClsLoginInfo.Location, true, objClsLoginInfo.UserName, "");
            ViewBag.lstToEmployee = listEmployee.Select(i => new BULocWiseCategoryModel { CatID = i.id, CatDesc = i.text }).ToList();
            if (objSSS014.ToPerson != null && objSSS014.ToPerson != string.Empty)
            {
                ViewBag.ToUserName = objSSS014.ToPerson + "-" + (db.COM003.Where(i => i.t_actv == 1).FirstOrDefault(i => i.t_psno == objSSS014.ToPerson).t_name);
            }
            if (objSSS014.SubmittedBy != null && objSSS014.SubmittedBy != string.Empty)
            {
                ViewBag.FromUserName = objSSS014.SubmittedBy + "-" + (db.COM003.Where(i => i.t_actv == 1).FirstOrDefault(i => i.t_psno == objSSS014.SubmittedBy).t_name);

            }
            ViewBag.RoleId = RoleId;
            return View(objSSS014);
        }
        [HttpPost]
        public ActionResult SaveExpansionMockUp(SSS014 objSSS014)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (Convert.ToInt32(objSSS014.ExpansionMockUpId) > 0)
                {
                    var objSSS014Update = db.SSS014.Where(i => i.ExpansionMockUpId == objSSS014.ExpansionMockUpId).FirstOrDefault();
                    objSSS014Update.ToPerson = objSSS014.ToPerson;

                    objSSS014Update.Notes = objSSS014.Notes;
                    objSSS014Update.EditedBy = objClsLoginInfo.UserName;
                    objSSS014Update.EditedOn = DateTime.Now;
                    if (objSSS014Update.Status.ToLower() == SSSStatus.Draft.GetStringValue().ToLower())
                    {
                        objSSS014Update.TargetDate = objSSS014.TargetDate.HasValue ? Manager.getDateTime(objSSS014.TargetDate.Value.ToShortDateString()) : objSSS014.TargetDate;
                    }
                    db.SaveChanges();

                    //if (objSSS014Update.Status.ToLower() == SSSStatus.Draft.GetStringValue().ToLower())
                    //{
                    //    var folderPath = "SSS014/" + objSSS014.ExpansionMockUpId;
                    //    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    //}

                    objResponseMsg.HeaderId = objSSS014Update.ExpansionMockUpId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Expansion Mock Up Updated Successfully";
                }
                else
                {
                    SSS001 objSSS001 = db.SSS001.Where(i => i.SSSId == objSSS014.SSSId).First();

                    SSS014 objSSS004New = new SSS014();
                    objSSS004New.SSSId = objSSS014.SSSId;
                    objSSS004New.ContractNo = objSSS001.Contract;
                    objSSS004New.Customer = objSSS001.Customer;
                    objSSS004New.ToPerson = objSSS014.ToPerson;
                    objSSS004New.TargetDate = objSSS014.TargetDate.HasValue ? Manager.getDateTime(objSSS014.TargetDate.Value.ToShortDateString()) : objSSS014.TargetDate;
                    objSSS004New.Notes = objSSS014.Notes;
                    objSSS004New.Status = SSSStatus.Draft.GetStringValue();
                    objSSS004New.CreatedBy = objClsLoginInfo.UserName;
                    objSSS004New.CreatedOn = DateTime.Now;
                    objSSS004New.Status = SSSStatus.Draft.GetStringValue();
                    db.SSS014.Add(objSSS004New);
                    db.SaveChanges();

                    //var folderPath = "SSS014/" + objSSS004New.ExpansionMockUpId;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);

                    objResponseMsg.HeaderId = objSSS004New.ExpansionMockUpId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Expansion Mock Up Save Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet); ;
        }

        [HttpPost]
        public ActionResult SubmitExpansionMockUp(int ExpansionMockUpId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS014 objSSS014 = db.SSS014.Where(x => x.ExpansionMockUpId == ExpansionMockUpId).FirstOrDefault();

                if (objSSS014 != null)
                {
                    if (objSSS014.SSS015.Count > 0)
                    {

                        objSSS014.SubmittedBy = objClsLoginInfo.UserName;
                        objSSS014.SubmittedOn = DateTime.Now;
                        objSSS014.Status = PCCInputStatus.Submitted.GetStringValue();
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Expansion Mock Up Submitted Successfully.";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please Add Atleast One Details";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #region Lines
        public ActionResult loadExpansionMockUpLineDataTable(JQueryDataTableParamModel param, int ExpansionMockUpId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";
                whereCondition += " and ExpansionMockUpId= " + ExpansionMockUpId.ToString();

                string[] columnName = { "ShellDescription", "ShellId", "Thick", "Material", "TypeOfRolling", "TempOfRolling", "Remarks" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_SSS_GETExpansionMockUpLines(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();


                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "ExpansionMockUpId", Convert.ToString(ExpansionMockUpId)),
                                    GenerateAutoComplete(newRecordId, "Project","","", "", false,"","Project"+newRecordId,false,"autocomplete")+""+Helper.GenerateHidden(newRecordId, "ddlProject", Convert.ToString(newRecordId)),
                                    GenerateAutoComplete(newRecordId, "TubeSheetThickness","","", "", false,"","TubeSheetThickness",false,"numeric4"),
                                    GenerateAutoComplete(newRecordId, "TubeSheetMaterial","","", "", false,"","TubeSheetMaterial",false),
                                    GenerateAutoComplete(newRecordId, "OD","","", "", false,"","OD",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "TubeThickness","","", "", false,"","TubeThickness",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "TubeMaterial","","", "", false,"","TubeMaterial",false,""),
                                    GenerateAutoComplete(newRecordId, "QuantityOfTubes","","", "", false,"","QuantityOfTubes",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "DimOfBlcok","","", "", false,"","DimOfBlcok",false,"numeric18"),
                                    GenerateAutoComplete(newRecordId, "QuantityOfBlock","","", "", false,"","QuantityOfBlock",false,"numeric"),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "ExpansionMockUpId", Convert.ToString(h.ExpansionMockUpId)),
                                    GenerateAutoComplete(h.LineId, "Project",h.Project,"", "", true,"","Project"+h.LineId,false,"autocomplete")+""+Helper.GenerateHidden(h.LineId, "ddlProject", Convert.ToString(h.LineId)),
                                    GenerateAutoComplete(h.LineId, "TubeSheetThickness",h?.TubeSheetThickness.ToString(),"", "", true,"","TubeSheetThickness",false,"numeric4"),
                                    GenerateAutoComplete(h.LineId, "TubeSheetMaterial",h.TubeSheetMaterial,"", "", true,"","TubeSheetMaterial",false),
                                    GenerateAutoComplete(h.LineId, "OD",h?.OD.ToString(),"", "", true,"","OD",true,"numeric"),
                                    GenerateAutoComplete(h.LineId, "TubeThickness",h?.TubeThickness.ToString(),"", "", true,"","TubeThickness",false,"numeric"),
                                    GenerateAutoComplete(h.LineId, "TubeMaterial",h?.TubeMaterial,"", "", true,"","TubeMaterial",false,""),
                                    GenerateAutoComplete(h.LineId, "QuantityOfTubes",h?.QuantityOfTubes.ToString(),"", "", true,"","QuantityOfTubes",false,"numeric"),
                                    GenerateAutoComplete(h.LineId, "DimOfBlcok",(h?.DimOfBlcok!=null? h?.DimOfBlcok.ToString():""),"", "", true,"","DimOfBlcok",false,"numeric18"),
                                    GenerateAutoComplete(h.LineId, "QuantityOfBlock",h?.QuantityOfBlock.ToString(),"", "", true,"","QuantityOfBlock",false,"numeric"),
                                    HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteLine("+ h.ExpansionMockUpId+ "," + h.LineId +"); ")
                                    + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveExpansionMockUpLine(FormCollection fc, int Id = 0, int ExpansionMockUpId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS015 objSSS015 = null;

                if (Id > 0)
                {
                    objSSS015 = db.SSS015.Where(x => x.LineId == Id).FirstOrDefault();

                    objSSS015.Project = fc["ddlProject" + Id];
                    if (fc["TubeSheetThickness" + Id] == string.Empty)
                    {
                        objSSS015.TubeSheetThickness = null;
                    }
                    else
                    {
                        objSSS015.TubeSheetThickness = Convert.ToInt32(fc["TubeSheetThickness" + Id]);
                    }
                    if (fc["OD" + Id] == string.Empty)
                    {
                        objSSS015.OD = null;
                    }
                    else
                    {
                        objSSS015.OD = Convert.ToInt32(fc["OD" + Id]);
                    }
                    objSSS015.TubeSheetMaterial = fc["TubeSheetMaterial" + Id];
                    if (fc["TubeThickness" + Id] == string.Empty)
                    {
                        objSSS015.TubeThickness = null;
                    }
                    else
                    {
                        objSSS015.TubeThickness = Convert.ToInt32(fc["TubeThickness" + Id]);
                    }
                    objSSS015.TubeMaterial = fc["TubeMaterial" + Id];
                    if (fc["QuantityOfTubes" + Id] == string.Empty)
                    {
                        objSSS015.QuantityOfTubes = null;
                    }
                    else
                    {
                        objSSS015.QuantityOfTubes = Convert.ToInt32(fc["QuantityOfTubes" + Id]);
                    }

                    if (fc["DimOfBlcok" + Id] != null && fc["DimOfBlcok" + Id].ToString() != string.Empty)
                    {
                        objSSS015.DimOfBlcok = Convert.ToDecimal(fc["DimOfBlcok" + Id].ToString());
                    }
                    else
                    {
                        objSSS015.DimOfBlcok = null;
                    }

                    if (fc["QuantityOfBlock" + Id] == string.Empty)
                    {
                        objSSS015.QuantityOfBlock = null;
                    }
                    else
                    {
                        objSSS015.QuantityOfBlock = Convert.ToInt32(fc["QuantityOfBlock" + Id]);
                    }

                    objSSS015.EditedBy = objClsLoginInfo.UserName;
                    objSSS015.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS015 = new SSS015();
                    objSSS015.ExpansionMockUpId = ExpansionMockUpId;
                    objSSS015.SSSId = db.SSS014.FirstOrDefault(i => i.ExpansionMockUpId == ExpansionMockUpId).SSSId;
                    objSSS015.Project = fc["ddlProject" + Id];
                    if (fc["TubeSheetThickness" + Id] == string.Empty)
                    {
                        objSSS015.TubeSheetThickness = null;
                    }
                    else
                    {
                        objSSS015.TubeSheetThickness = Convert.ToInt32(fc["TubeSheetThickness" + Id]);
                    }
                    if (fc["OD" + Id] == string.Empty)
                    {
                        objSSS015.OD = null;
                    }
                    else
                    {
                        objSSS015.OD = Convert.ToInt32(fc["OD" + Id]);
                    }
                    objSSS015.TubeSheetMaterial = fc["TubeSheetMaterial" + Id];
                    if (fc["TubeThickness" + Id] == string.Empty)
                    {
                        objSSS015.TubeThickness = null;
                    }
                    else
                    {
                        objSSS015.TubeThickness = Convert.ToInt32(fc["TubeThickness" + Id]);
                    }
                    objSSS015.TubeMaterial = fc["TubeMaterial" + Id];
                    if (fc["QuantityOfTubes" + Id] == string.Empty)
                    {
                        objSSS015.QuantityOfTubes = null;
                    }
                    else
                    {
                        objSSS015.QuantityOfTubes = Convert.ToInt32(fc["QuantityOfTubes" + Id]);
                    }

                    if (fc["DimOfBlcok" + Id] != null && fc["DimOfBlcok" + Id].ToString() != string.Empty)
                    {
                        objSSS015.DimOfBlcok = Convert.ToDecimal(fc["DimOfBlcok" + Id].ToString());
                    }
                    else
                    {
                        objSSS015.DimOfBlcok = null;
                    }

                    if (fc["QuantityOfBlock" + Id] == string.Empty)
                    {
                        objSSS015.QuantityOfBlock = null;
                    }
                    else
                    {
                        objSSS015.QuantityOfBlock = Convert.ToInt32(fc["QuantityOfBlock" + Id]);
                    }

                    objSSS015.CreatedBy = objClsLoginInfo.UserName;
                    objSSS015.CreatedOn = DateTime.Now;
                    db.SSS015.Add(objSSS015);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteExpansionMockUpLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS015 objSSS015 = db.SSS015.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS015 != null)
                {
                    db.SSS015.Remove(objSSS015);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Excell
        //Code By : Ajay Chauhan on 13/11/2017, Export Funxtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_SSS_GETExpansionMockUp(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      SSSNo = li.SSSNo,
                                      Contract = li.Contract,
                                      Customer = li.Customer,
                                      CSRMeetingOn = li.CSRMeetingOn,
                                      TargetDate = li.TargetDate,
                                      Status = li.Status,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_SSS_GETExpansionMockUpLines(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      TubeSheetThickness = li.TubeSheetThickness,
                                      TubeSheetMaterial = li.TubeSheetMaterial,
                                      OD = li.OD,
                                      TubeThickness = li.TubeThickness,
                                      TubeMaterial = li.TubeMaterial,
                                      QuantityOfTubes = li.QuantityOfTubes,
                                      DimOfBlcok = li.DimOfBlcok,
                                      QuantityOfBlock = li.QuantityOfBlock,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string cssClass = "", string title = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + (cssClass != string.Empty ? cssClass : "");
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " title='" + title + "' />";

            return strAutoComplete;
        }
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }

    }
}