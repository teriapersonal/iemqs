﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.SSS.Controllers
{
    public class RollingRequestFormPCCController : clsBase
    {
        // GET: SSS/RollingRequestFormPCC
        public ActionResult Index(int roleId)
        {
            ViewBag.RoleId = roleId;
            return View();
        }

        public ActionResult IndexDE()
        {
            return RedirectToAction("Index", "RollingRequestFormPCC", new { roleId = Convert.ToInt32(RoleForSSS.DE) });
        }
        public ActionResult IndexPCC()
        {
            return RedirectToAction("Index", "RollingRequestFormPCC", new { roleId = Convert.ToInt32(RoleForSSS.PCC) });
        }
        public ActionResult GETPCCInputPartial(string status, string RoleId)
        {
            ViewBag.Status = status;
            ViewBag.RoleId = RoleId;
            return PartialView("_GetRollingRequestDataGridPartial");
        }

        public ActionResult loadPCCInputDataTable(JQueryDataTableParamModel param, string status, int RoleId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";

                if (status.ToUpper() == "PENDING")
                {
                    if (RoleId == Convert.ToInt32(RoleForSSS.DE))
                    {
                        whereCondition += " and s1.status in ('" + PCCInputStatus.Submitted.GetStringValue() + "')";
                    }
                    else if (RoleId == Convert.ToInt32(RoleForSSS.PCC))
                    {
                        whereCondition += " and s1.status in ('" + PCCInputStatus.Submitted.GetStringValue() + "') and ISNULL(s13.LineId,'0')>0 and s13.ToPerson='" + objClsLoginInfo.UserName + "' and s13.status in ('" + PCCInputStatus.Submitted.GetStringValue() + "','" + PCCInputStatus.Completed.GetStringValue() + "')";
                    }
                }
                else
                {
                    whereCondition += " and s1.status in ('')";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "s1.SSSNo", "s1.Contract", "s1.Customer", "s1.CSRMeetingOn", "s13.TargetDate", "s13.Status" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_SSS_GETPCCInput(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstPam
                          select new[] {
                              h.SSSNo.ToString(),
                              h.Contract.ToString(),
                              h.Customer.ToString(),
                              Convert.ToString(Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy")),
                              Convert.ToString(Convert.ToDateTime(h.TargetDate).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.TargetDate).ToString("dd/MM/yyyy")),
                              h.Status!=null ?h.Status.ToString():"",//h.Status!=null ?h.Status.ToString():"",
                              "", //Action
                              h.SSSId.ToString(),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult RollingRequestForm(int SSSId = 0, int RoleId = 0)
        {
            ViewBag.SSSId = SSSId;
            var objSSS013 = db.SSS013.Where(i => i.SSSId == SSSId).FirstOrDefault();
            if (objSSS013 == null)
            {
                objSSS013 = new SSS013();
                objSSS013.SSSId = SSSId;
                objSSS013.Status = PCCInputStatus.Draft.GetStringValue();
            }
            var listEmployee = Manager.GetApprover(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objClsLoginInfo.Location, true, objClsLoginInfo.UserName, "");
            ViewBag.lstToEmployee = listEmployee.Select(i => new BULocWiseCategoryModel { CatID = i.id, CatDesc = i.text }).ToList();
            if (objSSS013.ToPerson != null && objSSS013.ToPerson != string.Empty)
            {
                ViewBag.ToUserName = objSSS013.ToPerson + "-" + (db.COM003.Where(i => i.t_actv == 1).FirstOrDefault(i => i.t_psno == objSSS013.ToPerson).t_name);
            }
            if (objSSS013.SubmittedBy != null && objSSS013.SubmittedBy != string.Empty)
            {
                ViewBag.FromUserName = objSSS013.SubmittedBy + "-" + (db.COM003.Where(i => i.t_actv == 1).FirstOrDefault(i => i.t_psno == objSSS013.SubmittedBy).t_name);
            }
            List<string> lstTypesOfRolling = getTypesOfRolling();
            ViewBag.TypeOfRolling = lstTypesOfRolling.Select(i => new BULocWiseCategoryModel { CatID = i, CatDesc = i }).ToList();

            ViewBag.RoleId = RoleId;
            return View(objSSS013);
        }

        [HttpPost]
        public ActionResult SavePCCInput(SSS013 objSSS013)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (Convert.ToInt32(objSSS013.LineId) > 0)
                {
                    var objSSS013Update = db.SSS013.Where(i => i.LineId == objSSS013.LineId).FirstOrDefault();
                    if (objSSS013Update.Status.ToLower() == PCCInputStatus.Draft.GetStringValue().ToLower())
                    {
                        objSSS013Update.ToPerson = objSSS013.ToPerson;
                        objSSS013Update.TargetDate = objSSS013.TargetDate.HasValue ? Manager.getDateTime(objSSS013.TargetDate.Value.ToShortDateString()) : objSSS013.TargetDate;
                        objSSS013Update.Material = objSSS013.Material;
                        objSSS013Update.UTSTemperatureAmbient = objSSS013.UTSTemperatureAmbient;
                        objSSS013Update.YieldStressAmbient = objSSS013.YieldStressAmbient;
                        objSSS013Update.UserTemperature = objSSS013.UserTemperature;
                        objSSS013Update.YieldStressUser = objSSS013.YieldStressUser;
                        objSSS013Update.ShellId = objSSS013.ShellId;
                        objSSS013Update.NomicalThickness = objSSS013.NomicalThickness;
                        objSSS013Update.TTLineDistance = objSSS013.TTLineDistance;
                        objSSS013Update.EditedBy = objClsLoginInfo.UserName;
                        objSSS013Update.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    else if (objSSS013Update.Status.ToLower() == PCCInputStatus.Submitted.GetStringValue().ToLower())
                    {
                        objSSS013Update.ToPerson = objSSS013.ToPerson;
                        objSSS013Update.EditedBy = objClsLoginInfo.UserName;
                        objSSS013Update.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }

                    objResponseMsg.HeaderId = objSSS013Update.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PCC Input Updated Successfully";
                }
                else
                {
                    SSS001 objSSS001 = db.SSS001.Where(i => i.SSSId == objSSS013.SSSId).First();

                    SSS013 objSSS013New = new SSS013();
                    objSSS013New.SSSId = objSSS013.SSSId;
                    objSSS013New.ContractNo = objSSS001.Contract;
                    objSSS013New.Customer = objSSS001.Customer;
                    objSSS013New.ToPerson = objSSS013.ToPerson;
                    objSSS013New.TargetDate = objSSS013.TargetDate.HasValue ? Manager.getDateTime(objSSS013.TargetDate.Value.ToShortDateString()) : objSSS013.TargetDate;
                    objSSS013New.Status = PCCInputStatus.Draft.GetStringValue();
                    objSSS013New.Material = objSSS013.Material;
                    objSSS013New.UTSTemperatureAmbient = objSSS013.UTSTemperatureAmbient;
                    objSSS013New.YieldStressAmbient = objSSS013.YieldStressAmbient;
                    objSSS013New.UserTemperature = objSSS013.UserTemperature;
                    objSSS013New.YieldStressUser = objSSS013.YieldStressUser;
                    objSSS013New.ShellId = objSSS013.ShellId;
                    objSSS013New.NomicalThickness = objSSS013.NomicalThickness;
                    objSSS013New.TTLineDistance = objSSS013.TTLineDistance;
                    objSSS013New.CreatedBy = objClsLoginInfo.UserName;
                    objSSS013New.CreatedOn = DateTime.Now;

                    db.SSS013.Add(objSSS013New);
                    db.SaveChanges();

                    objResponseMsg.HeaderId = objSSS013New.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PCC Input Save Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet); ;
        }

        [HttpPost]
        public ActionResult SubmitPCCInput(int LineId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS013 objSSS013 = db.SSS013.Where(x => x.LineId == LineId).FirstOrDefault();

                if (objSSS013 != null)
                {
                    objSSS013.SubmittedBy = objClsLoginInfo.UserName;
                    objSSS013.SubmittedOn = DateTime.Now;
                    objSSS013.Status = PCCInputStatus.Submitted.GetStringValue();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PCC Input Submitted Successfully.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult SavePCCInputReplay(SSS013 objSSS013, int Id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (Id > 0)
                {
                    var objSSS013Update = db.SSS013.Where(i => i.LineId == Id).FirstOrDefault();

                    objSSS013Update.RollingMachine = objSSS013.RollingMachine;
                    objSSS013Update.RollingDia = objSSS013.RollingDia;
                    objSSS013Update.MaxPossibleWidth = objSSS013.MaxPossibleWidth;
                    objSSS013Update.PlateThickness = objSSS013.PlateThickness;
                    objSSS013Update.EdgeBreakingAllowance = objSSS013.EdgeBreakingAllowance;
                    objSSS013Update.TypeOfEdgeBreaking = objSSS013.TypeOfEdgeBreaking;
                    objSSS013Update.TemperatureOfBreaking = objSSS013.TemperatureOfBreaking;
                    objSSS013Update.PlatesPerChargeBreaking = objSSS013.PlatesPerChargeBreaking;
                    objSSS013Update.SoakingTimeBreakingHour = objSSS013.SoakingTimeBreakingHour;
                    objSSS013Update.SoakingTimeBreakingMinute = objSSS013.SoakingTimeBreakingMinute;
                    objSSS013Update.TypeOfRolling = objSSS013.TypeOfRolling;
                    objSSS013Update.TempOfRolling = objSSS013.TempOfRolling;
                    objSSS013Update.PlatePerChargeRolling = objSSS013.PlatePerChargeRolling;
                    objSSS013Update.SoakingTimeRollingHour = objSSS013.SoakingTimeRollingHour;
                    objSSS013Update.SoakingTimeRollingMinute = objSSS013.SoakingTimeRollingMinute;
                    objSSS013Update.TypeOfReRolling = objSSS013.TypeOfReRolling;
                    objSSS013Update.TempOfReRolling = objSSS013.TempOfReRolling;
                    objSSS013Update.PlatePerChargeReRolling = objSSS013.PlatePerChargeReRolling;
                    objSSS013Update.SoakingTimeReRollingHour = objSSS013.SoakingTimeReRollingHour;
                    objSSS013Update.SoakingTimeReRollingMinute = objSSS013.SoakingTimeReRollingMinute;
                    objSSS013Update.EditedBy = objClsLoginInfo.UserName;
                    objSSS013Update.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    objResponseMsg.HeaderId = objSSS013Update.LineId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PCC Replay Successfully";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet); ;
        }

        [HttpPost]
        public ActionResult SubmitPCCReplayInput(int LineId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS013 objSSS013 = db.SSS013.Where(x => x.LineId == LineId).FirstOrDefault();

                if (objSSS013 != null)
                {
                    objSSS013.PCCBy = objClsLoginInfo.UserName;
                    objSSS013.PCCOn = DateTime.Now;
                    objSSS013.Status = PCCInputStatus.Completed.GetStringValue();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "PCC Replay Submitted Successfully.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public static List<string> getTypesOfRolling()
        {
            List<string> items = GetEnumDescription<TypesOfRolling>();
            return items;
        }
        public static List<string> GetEnumDescription<T>() where T : struct
        {
            Type t = typeof(T);
            return !t.IsEnum ? null : Enum.GetValues(t).Cast<Enum>().Select(x => x.GetStringValue()).ToList();
        }

        #region Export Excell
        //Code By : Ajay Chauhan on 09/11/2017, Export Funxtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_SSS_GETPCCInput(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      SSSNo = li.SSSNo,
                                      Contract = li.Contract,
                                      Customer = li.Customer,
                                      CSRMeetingOn = li.CSRMeetingOn,
                                      TargetDate = li.TargetDate,
                                      Status = li.Status,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}