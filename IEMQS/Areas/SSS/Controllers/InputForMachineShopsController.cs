﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace IEMQS.Areas.SSS.Controllers
{
    public class InputForMachineShopsController : clsBase
    {
        // GET: SSS/InputForMachineShops
        public ActionResult Index(int roleId)
        {
            ViewBag.RoleId = roleId;
            return View();
        }
        public ActionResult IndexDE()
        {
            return RedirectToAction("Index", "InputForMachineShops", new { roleId = Convert.ToInt32(RoleForSSS.DE) });
        }
        public ActionResult IndexSHOP()
        {
            return RedirectToAction("Index", "InputForMachineShops", new { roleId = Convert.ToInt32(RoleForSSS.SHOP) });
        }
        public ActionResult InputForMachinePartial(string status, string RoleId)
        {
            ViewBag.Status = status;
            ViewBag.RoleId = RoleId;
            return PartialView("_InputForMachineDataGrid");
        }
        [SessionExpireFilter]
        public ActionResult MachineShopForm(int SSSId = 0, int RoleId = 0)
        {
            ViewBag.SSSId = SSSId;
            var objSSS020 = db.SSS020.Where(i => i.SSSId == SSSId).FirstOrDefault();
            if (objSSS020 == null)
            {
                objSSS020 = new SSS020();
                objSSS020.SSSId = SSSId;
                objSSS020.Status = PCCInputStatus.Draft.GetStringValue();
            }
            var listEmployee = Manager.GetApprover(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(), objClsLoginInfo.Location, true, objClsLoginInfo.UserName, "");
            ViewBag.lstToEmployee = listEmployee.Select(i => new BULocWiseCategoryModel { CatID = i.id, CatDesc = i.text }).ToList();
            if (objSSS020.ToPerson != null && objSSS020.ToPerson != string.Empty)
            {
                ViewBag.ToUserName = objSSS020.ToPerson + "-" + (db.COM003.Where(i => i.t_actv == 1).FirstOrDefault(i => i.t_psno == objSSS020.ToPerson).t_name);
            }
            if (objSSS020.SubmittedBy != null && objSSS020.SubmittedBy != string.Empty)
            {
                ViewBag.FromUserName = objSSS020.SubmittedBy + "-" + (db.COM003.Where(i => i.t_actv == 1).FirstOrDefault(i => i.t_psno == objSSS020.SubmittedBy).t_name);

            }
            ViewBag.RoleId = RoleId;
            return View(objSSS020);
        }

        public ActionResult GetDrillingMockUpLinePartial(int DrillMockUpBlockId = 0)
        {
            var PAMHeaderId = (from s21 in db.SSS020
                               join s1 in db.SSS001 on s21.SSSId equals s1.SSSId
                               join p6 in db.PAM006 on s1.DSSId equals p6.DSSId
                               select p6.HeaderId
                             ).FirstOrDefault();
            ViewBag.Projects = db.SP_PAM_GETPROJECTLIST(PAMHeaderId).Select(i => new BULocWiseCategoryModel { CatID = i.id, CatDesc = i.text }).ToList();
            return PartialView("_GetMachineLinePartial");
        }
        [HttpPost]
        public ActionResult SaveDrillingMockUp(SSS020 objSSS020)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (Convert.ToInt32(objSSS020.DrillMockUpBlockId) > 0)
                {
                    var objSSS020Update = db.SSS020.Where(i => i.DrillMockUpBlockId == objSSS020.DrillMockUpBlockId).FirstOrDefault();
                    objSSS020Update.ToPerson = objSSS020.ToPerson;

                    objSSS020Update.Notes = objSSS020.Notes;
                    objSSS020Update.EditedBy = objClsLoginInfo.UserName;
                    objSSS020Update.EditedOn = DateTime.Now;
                    if (objSSS020Update.Status.ToLower() == SSSStatus.Draft.GetStringValue().ToLower())
                    {
                        //objSSS020Update.TargetDate = objSSS020.TargetDate;
                    }
                    db.SaveChanges();

                    //if (objSSS020Update.Status.ToLower() == SSSStatus.Draft.GetStringValue().ToLower())
                    //{
                    //    var folderPath = "SSS020/" + objSSS020.DrillMockUpBlockId;
                    //    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    //}

                    objResponseMsg.HeaderId = objSSS020Update.DrillMockUpBlockId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Drilling Mock Up Updated Successfully";
                }
                else
                {
                    SSS001 objSSS001 = db.SSS001.Where(i => i.SSSId == objSSS020.SSSId).First();

                    SSS020 objSSS020New = new SSS020();
                    objSSS020New.SSSId = objSSS020.SSSId;
                    objSSS020New.ContractNo = objSSS001.Contract;
                    objSSS020New.Customer = objSSS001.Customer;
                    objSSS020New.ToPerson = objSSS020.ToPerson;
                    //objSSS020New.TargetDate = objSSS020.TargetDate;
                    objSSS020New.Notes = objSSS020.Notes;
                    objSSS020New.Status = SSSStatus.Draft.GetStringValue();
                    objSSS020New.CreatedBy = objClsLoginInfo.UserName;
                    objSSS020New.CreatedOn = DateTime.Now;
                    objSSS020New.Status = SSSStatus.Draft.GetStringValue();
                    db.SSS020.Add(objSSS020New);
                    db.SaveChanges();

                    //var folderPath = "SSS020/" + objSSS020New.DrillMockUpBlockId;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);

                    objResponseMsg.HeaderId = objSSS020New.DrillMockUpBlockId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Drilling Mock Up Save Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet); ;
        }

        [HttpPost]
        public ActionResult SubmitDrillingMockUp(int DrillMockUpBlockId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS020 objSSS020 = db.SSS020.Where(x => x.DrillMockUpBlockId == DrillMockUpBlockId).FirstOrDefault();

                if (objSSS020 != null)
                {
                    if (objSSS020.SSS021.Count > 0)
                    {

                        objSSS020.SubmittedBy = objClsLoginInfo.UserName;
                        objSSS020.SubmittedOn = DateTime.Now;
                        objSSS020.Status = PCCInputStatus.Submitted.GetStringValue();
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Drilling Mock Up Submitted Successfully.";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please Add Atleast One Details";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult loadDrillingMockUpLineDataTable(JQueryDataTableParamModel param, int DrillMockUpBlockId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";
                whereCondition += " and DrillMockUpBlockId= " + DrillMockUpBlockId.ToString();

                string[] columnName = { "Project", "TubeSheetThickness", "Material", "TubeOD", "UserTemp", "LD", "DimMockUpBlock", "QuantityMockUpBlock", "CreatedBy", "EditedBy" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                var lstPam = db.SP_SSS_GETDrillingMockUpLines(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();


                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "DrillMockUpBlockId", Convert.ToString(DrillMockUpBlockId)),
                                    GenerateAutoComplete(newRecordId, "Project","","", "", false,"","Project"+newRecordId,false,"autocomplete")+""+Helper.GenerateHidden(newRecordId, "ddlProject", Convert.ToString(newRecordId)),
                                    GenerateAutoComplete(newRecordId, "TubeSheetThickness","","", "", false,"","TubeSheetThickness",false,"numeric","","200"),
                                    GenerateAutoComplete(newRecordId, "Material","","", "", false,"","Material",false,"",""),
                                    GenerateAutoComplete(newRecordId, "TubeOD","","", "", false,"","TubeOD",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "UserTemp","","", "", false,"","UserTemp",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "LD","","", "", false,"","LD",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "DimMockUpBlock","","", "", false,"","DimMockUpBlock",false,"","","20"),
                                    GenerateAutoComplete(newRecordId, "QuantityMockUpBlock","","", "", false,"","QuantityMockUpBlock",false,"numeric"),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "DrillMockUpBlockId", Convert.ToString(h.DrillMockUpBlockId)),
                                    GenerateAutoComplete(h.LineId, "Project",h.Project,"", "", true,"","Project"+h.LineId,false,"autocomplete")+""+Helper.GenerateHidden(h.LineId, "ddlProject", Convert.ToString(h.Project)),
                                    GenerateAutoComplete(h.LineId, "TubeSheetThickness",h?.TubeSheetThickness.Value.ToString(),"", "", true,"","TubeSheetThickness",false,"numeric","","200"),
                                    GenerateAutoComplete(h.LineId, "Material",h.Material,"", "", true,"","Material",false,"",""),
                                    GenerateAutoComplete(h.LineId, "TubeOD",h?.TubeOD.ToString(),"", "", true,"","TubeOD",true,"numeric"),
                                    GenerateAutoComplete(h.LineId, "UserTemp",h?.UserTemp.ToString(),"", "", true,"","UserTemp",false,"numeric"),
                                    GenerateAutoComplete(h.LineId, "LD",h?.LD.ToString(),"", "", true,"","LD",false,"numeric"),
                                    //GenerateAutoComplete(h.LineId, "DimMockUpBlock",h.DimMockUpBlock,"", "", true,"","DimMockUpBlock",false),
                                    GenerateAutoComplete(h.LineId, "DimMockUpBlock",h.DimMockUpBlock!=null? h.DimMockUpBlock.ToString():"","","", true,"","DimMockUpBlock",false,"","","20"),
                                    GenerateAutoComplete(h.LineId, "QuantityMockUpBlock",h.QuantityMockUpBlock,"", "", true,"","QuantityMockUpBlock",false,"numeric"),
                                    HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteLine("+ h.DrillMockUpBlockId+ "," + h.LineId +"); ")
                                    + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = "",//strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult DeleteDrillingMockUpLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS021 objSSS021 = db.SSS021.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS021 != null)
                {
                    db.SSS021.Remove(objSSS021);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SaveDrillingMockUpLine(FormCollection fc, int Id = 0, int DrillMockUpBlockId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS021 objSSS021 = null;

                if (Id > 0)
                {
                    objSSS021 = db.SSS021.Where(x => x.LineId == Id).FirstOrDefault();

                    objSSS021.Project = fc["ddlProject" + Id];
                    if (fc["TubeSheetThickness" + Id] == string.Empty)
                    {
                        objSSS021.TubeSheetThickness = null;
                    }
                    else
                    {
                        objSSS021.TubeSheetThickness = Convert.ToInt32(fc["TubeSheetThickness" + Id]);
                    }

                    objSSS021.Material = fc["Material" + Id];

                    if (fc["TubeOD" + Id] == string.Empty)
                    {
                        objSSS021.TubeOD = null;
                    }
                    else
                    {
                        objSSS021.TubeOD = Convert.ToInt32(fc["TubeOD" + Id]);
                    }

                    objSSS021.UserTemp = Convert.ToInt32(fc["UserTemp" + Id]);

                    if (fc["LD" + Id] == string.Empty)
                    {
                        objSSS021.LD = null;
                    }
                    else
                    {
                        objSSS021.LD = Convert.ToInt32(fc["LD" + Id]);
                    }
                    if (fc["DimMockUpBlock" + Id] == string.Empty)
                    {
                        objSSS021.DimMockUpBlock = null;
                    }
                    else
                    {
                        objSSS021.DimMockUpBlock = fc["DimMockUpBlock" + Id];
                    }

                    if (fc["QuantityMockUpBlock" + Id] == string.Empty)
                    {
                        objSSS021.QuantityMockUpBlock = null;
                    }
                    else
                    {
                        objSSS021.QuantityMockUpBlock = fc["QuantityMockUpBlock" + Id];
                    }
                    objSSS021.QuantityMockUpBlock = fc["QuantityMockUpBlock" + Id];
                    objSSS021.EditedBy = objClsLoginInfo.UserName;
                    objSSS021.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS021 = new SSS021();
                    objSSS021.DrillMockUpBlockId = DrillMockUpBlockId;
                    objSSS021.SSSId = db.SSS020.FirstOrDefault(i => i.DrillMockUpBlockId == DrillMockUpBlockId).SSSId;
                    objSSS021.Project = fc["ddlProject" + Id];
                    if (fc["TubeSheetThickness" + Id] == string.Empty)
                    {
                        objSSS021.TubeSheetThickness = null;
                    }
                    else
                    {
                        objSSS021.TubeSheetThickness = Convert.ToInt32(fc["TubeSheetThickness" + Id]);
                    }

                    objSSS021.Material = fc["Material" + Id];

                    if (fc["TubeOD" + Id] == string.Empty)
                    {
                        objSSS021.TubeOD = null;
                    }
                    else
                    {
                        objSSS021.TubeOD = Convert.ToInt32(fc["TubeOD" + Id]);
                    }

                    objSSS021.UserTemp = Convert.ToInt32(fc["UserTemp" + Id]);

                    if (fc["LD" + Id] == string.Empty)
                    {
                        objSSS021.LD = null;
                    }
                    else
                    {
                        objSSS021.LD = Convert.ToInt32(fc["LD" + Id]);
                    }
                    if (fc["DimMockUpBlock" + Id] == string.Empty)
                    {
                        objSSS021.DimMockUpBlock = null;
                    }
                    else
                    {
                        objSSS021.DimMockUpBlock = fc["DimMockUpBlock" + Id];
                    }

                    if (fc["QuantityMockUpBlock" + Id] == string.Empty)
                    {
                        objSSS021.QuantityMockUpBlock = null;
                    }
                    else
                    {
                        objSSS021.QuantityMockUpBlock = fc["QuantityMockUpBlock" + Id];
                    }

                    objSSS021.CreatedBy = objClsLoginInfo.UserName;
                    objSSS021.CreatedOn = DateTime.Now;
                    db.SSS021.Add(objSSS021);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #region Export Excell
        //Code By : Anil Hadiyal on 28/12/2017, Export Functionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_SSS_GETMachineShopTable(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      ToPerson = li.ToPerson,
                                      Customer = li.Customer,
                                      ContractNo = li.ContractNo,
                                      Status = li.Status,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_SSS_GETDrillingMockUpLines(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      TubeSheetThickness = li.TubeSheetThickness,
                                      Material = li.Material,
                                      TubeOD = li?.TubeOD,
                                      UserTemp = li?.UserTemp,
                                      LD = li?.LD,
                                      DimMockUpBlock = li.DimMockUpBlock,
                                      QuantityMockUpBlock = li.QuantityMockUpBlock,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string cssClass = "", string title = "", string maxLength = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + (cssClass != string.Empty ? cssClass : "");
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " title='" + title + "'" + (maxLength.Length > 0 ? "maxlength='" + maxLength + "'" : "") + " />";

            return strAutoComplete;
        }
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }
        public ActionResult loadMachineShopDataTable(JQueryDataTableParamModel param, string status, int RoleId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";

                if (status.ToUpper() == "PENDING")
                {
                    if (RoleId == Convert.ToInt32(RoleForSSS.DE))
                    {
                        whereCondition += " and s1.status in ('" + PCCInputStatus.Submitted.GetStringValue() + "')";
                    }
                    else if (RoleId == Convert.ToInt32(RoleForSSS.SHOP))
                    {
                        whereCondition += " and s1.status in ('" + PCCInputStatus.Submitted.GetStringValue() + "') and ISNULL(s1.DrillMockUpBlockId,'0')>0 and s1.ToPerson='" + objClsLoginInfo.UserName + "' and s1.status in ('" + PCCInputStatus.Submitted.GetStringValue() + "')";
                    }
                }
                else
                {
                    whereCondition += " and s1.status in ('')";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "s2.Project", "s1.ToPerson", "s1.ContractNo", "s1.Customer", "s1.DrillMockUpBlockId", "s1.Status" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion


                var lstPam = db.SP_SSS_GETMachineShopTable(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstPam
                          select new[] {
                              h.Project.ToString(),
                              h.ToPerson.ToString(),
                              h.Customer.ToString(),
                              h.ContractNo.ToString(),
                             //h.CreatedBy.ToString(),
                              h.DrillMockUpBlockId.ToString(),
                              h.Status!=null ?h.Status.ToString():"",//h.Status!=null ?h.Status.ToString():"",
                              "", //Action
                              h.SSSId.ToString(),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}