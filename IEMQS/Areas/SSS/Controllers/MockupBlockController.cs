﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.SSS.Controllers
{
    public class MockupBlockController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index(int roleId = 0)
        {
            ViewBag.RoleId = roleId;
            return View();
        }

        [SessionExpireFilter]
        public ActionResult IndexDE()
        {
            return RedirectToAction("Index", "MockupBlock", new { roleId = Convert.ToInt32(RoleForSSS.DE) });
        }

        [SessionExpireFilter]
        public ActionResult IndexNDT()
        {
            return RedirectToAction("Index", "MockupBlock", new { roleId = Convert.ToInt32(RoleForSSS.NDT) });
        }

        public ActionResult GetMockupBlockPartial(string status, string RoleId)
        {
            ViewBag.Status = status;
            ViewBag.RoleId = RoleId;
            return PartialView("_GetMockupBlockDataGridPartial");
        }

        public ActionResult GetMockupBlockLinePartial(int MockUpBlockId = 0)
        {
            var PAMHeaderId = (from s16 in db.SSS016
                               join s1 in db.SSS001 on s16.SSSId equals s1.SSSId
                               join p6 in db.PAM006 on s1.DSSId equals p6.DSSId
                               select p6.HeaderId
                             ).FirstOrDefault();
            ViewBag.Projects = db.SP_PAM_GETPROJECTLIST(PAMHeaderId).Select(i => new BULocWiseCategoryModel { CatID = i.id, CatDesc = i.text }).ToList();
            return PartialView("_GetMockupBlockLinePartial");
        }

        public ActionResult loadMockupBlockDataTable(JQueryDataTableParamModel param, string status, int RoleId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                if (status.ToUpper() == "PENDING")
                {
                    if (RoleId == Convert.ToInt32(RoleForSSS.DE))
                    {
                        whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "')";
                    }
                    else if (RoleId == Convert.ToInt32(RoleForSSS.NDT))
                    {
                        whereCondition += " and s1.status in ('" + SSSStatus.Submitted.GetStringValue() + "') and ISNULL(s16.MockUpBlockId,'0')>0 and s16.ToPerson='" + objClsLoginInfo.UserName + "' and s16.status in ('" + SSSStatus.Submitted.GetStringValue() + "')";
                    }
                }
                else
                {
                    whereCondition += " and s1.status in ('')";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "SSSNo", "Contract", "s1.Customer", "s1.CSRMeetingOn", "s16.Status" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_SSS_GETMockupBlock(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstPam
                          select new[] {
                              h.SSSNo.ToString(),
                              h.Contract.ToString(),
                              h.Customer.ToString(),
                              Convert.ToString(Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(h.CSRMeetingOn).ToString("dd/MM/yyyy")),
                              h.Status!=null ?h.Status.ToString():"",//h.Status!=null ?h.Status.ToString():"",
                              "", //Action
                              h.SSSId.ToString(),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        public ActionResult MockupBlockForm(int SSSId = 0, int RoleId = 0)
        {
            ViewBag.SSSId = SSSId;
            var objSSS016 = db.SSS016.Where(i => i.SSSId == SSSId).FirstOrDefault();
            if (objSSS016 == null)
            {
                objSSS016 = new SSS016();
                objSSS016.SSSId = SSSId;
                objSSS016.Status = SSSStatus.Draft.GetStringValue();
            }
            var listEmployee = Manager.GetApprover(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objClsLoginInfo.Location, true, objClsLoginInfo.UserName, "");
            ViewBag.lstToEmployee = listEmployee.Select(i => new BULocWiseCategoryModel { CatID = i.id, CatDesc = i.text }).ToList();
            if (objSSS016.ToPerson != null && objSSS016.ToPerson != string.Empty)
            {
                ViewBag.ToUserName = objSSS016.ToPerson + "-" + (db.COM003.Where(i=> i.t_actv == 1).FirstOrDefault(i => i.t_psno == objSSS016.ToPerson).t_name);
            }
            if (objSSS016.SubmittedBy != null && objSSS016.SubmittedBy != string.Empty)
            {
                ViewBag.FromUserName = objSSS016.SubmittedBy + "-" + (db.COM003.Where(i => i.t_actv == 1).FirstOrDefault(i => i.t_psno == objSSS016.SubmittedBy).t_name);

            }
            ViewBag.RoleId = RoleId;
            return View(objSSS016);
        }

        [HttpPost]
        public ActionResult SaveMockupBlock(SSS016 objSSS016)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (Convert.ToInt32(objSSS016.MockUpBlockId) > 0)
                {
                    var objSSS016Update = db.SSS016.Where(i => i.MockUpBlockId == objSSS016.MockUpBlockId).FirstOrDefault();
                    if (objSSS016Update.Status.ToLower() == SSSStatus.Draft.GetStringValue().ToLower())
                    {
                        objSSS016Update.ToPerson = objSSS016.ToPerson;
                        //objSSS014Update.TargetDate = objSSS016.TargetDate;
                        objSSS016Update.Notes = objSSS016.Notes;
                        objSSS016Update.EditedBy = objClsLoginInfo.UserName;
                        objSSS016Update.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }

                    //if (objSSS016Update.Status.ToLower() == SSSStatus.Draft.GetStringValue().ToLower())
                    //{
                    //    var folderPath = "SSS016/" + objSSS016Update.MockUpBlockId;
                    //    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    //}

                    objResponseMsg.HeaderId = objSSS016Update.MockUpBlockId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Expansion Mock Up Updated Successfully";
                }
                else
                {
                    SSS001 objSSS001 = db.SSS001.Where(i => i.SSSId == objSSS016.SSSId).First();

                    SSS016 objSSS016New = new SSS016();
                    objSSS016New.SSSId = objSSS016.SSSId;
                    objSSS016New.ContractNo = objSSS001.Contract;
                    objSSS016New.Customer = objSSS001.Customer;
                    objSSS016New.ToPerson = objSSS016.ToPerson;
                    objSSS016New.Notes = objSSS016.Notes;
                    objSSS016New.Status = SSSStatus.Draft.GetStringValue();
                    objSSS016New.CreatedBy = objClsLoginInfo.UserName;
                    objSSS016New.CreatedOn = DateTime.Now;
                    objSSS016New.Status = SSSStatus.Draft.GetStringValue();
                    db.SSS016.Add(objSSS016New);
                    db.SaveChanges();

                    //var folderPath = "SSS016/" + objSSS016New.MockUpBlockId;
                    //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);

                    objResponseMsg.HeaderId = objSSS016New.MockUpBlockId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Expansion Mock Up Save Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet); ;
        }

        [HttpPost]
        public ActionResult SubmitMockupBlock(int MockUpBlockId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS016 objSSS016 = db.SSS016.Where(x => x.MockUpBlockId == MockUpBlockId).FirstOrDefault();

                if (objSSS016 != null)
                {
                    if (objSSS016.SSS017.Count > 0)
                    {
                        objSSS016.SubmittedBy = objClsLoginInfo.UserName;
                        objSSS016.SubmittedOn = DateTime.Now;
                        objSSS016.Status = PCCInputStatus.Submitted.GetStringValue();
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "PCC Input Submitted Successfully.";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please Add Atleast One Details";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region Lines
        public ActionResult loadMockupBlockLineDataTable(JQueryDataTableParamModel param, int MockUpBlockId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                //var lstdept = db.COM002.Where(x => x.t_dtyp == 3).ToList();
                //var lstEmp = db.COM003.ToList();

                string whereCondition = "1=1";
                whereCondition += " and MockUpBlockId= " + MockUpBlockId.ToString();

                string[] columnName = { "ShellThickness", "Material", "HeadThickness", "DimOfBlcok", "QuantityOfBlock" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstPam = db.SP_SSS_GetMockupBlockLines(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();


                int newRecordId = 0;
                var newRecord = new[] {
                                    "",
                                    Helper.GenerateHidden(newRecordId, "MockUpBlockId", Convert.ToString(MockUpBlockId)),
                                    GenerateAutoComplete(newRecordId, "Project","","", "", false,"","Project",false,"autocomplete")+""+Helper.GenerateHidden(newRecordId, "ddlProject", Convert.ToString(newRecordId)),
                                    GenerateAutoComplete(newRecordId, "ShellThickness","","", "", false,"","ShellThickness",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "HeadThickness","","", "", false,"","HeadThickness",false,"numeric"),
                                    GenerateAutoComplete(newRecordId, "Material","","", "", false,"","Material",false,""),
                                    GenerateAutoComplete(newRecordId, "DimOfBlcok","","", "", false,"","DimOfBlcok",false,""),
                                    GenerateAutoComplete(newRecordId, "QuantityOfBlock","","", "", false,"","QuantityOfBlock",false,"numeric"),
                                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveLine(0);" )
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "MockUpBlockId", Convert.ToString(h.MockUpBlockId)),
                                    GenerateAutoComplete(h.LineId, "Project",h.Project,"", "", true,"","Project",false,"autocomplete")+""+Helper.GenerateHidden(h.LineId, "ddlProject", Convert.ToString(h.LineId)),
                                    GenerateAutoComplete(h.LineId, "ShellThickness",h?.ShellThickness.ToString(),"", "", true,"","ShellThickness",false,"numeric"),
                                    GenerateAutoComplete(h.LineId, "HeadThickness",h?.HeadThickness.ToString(),"", "", true,"","HeadThickness",false,"numeric"),
                                    GenerateAutoComplete(h.LineId, "Material",h.Material,"", "", true,"","Material",true,""),
                                    GenerateAutoComplete(h.LineId, "DimOfBlcok",h.DimOfBlcok,"", "", true,"","DimOfBlcok",false,""),
                                    GenerateAutoComplete(h.LineId, "QuantityOfBlock",h?.QuantityOfBlock.ToString(),"", "", true,"","QuantityOfBlock",false,"numeric"),                                    
                                    HTMLActionString(h.LineId,"","Delete","Delete Record","fa fa-trash-o iconspace deleteline","DeleteLine("+ h.MockUpBlockId+ "," + h.LineId +"); ")
                                    + HTMLActionString(h.LineId,"","Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                    }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveMockupBlockLine(FormCollection fc, int Id = 0, int MockUpBlockId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                SSS017 objSSS017 = null;

                if (Id > 0)
                {
                    objSSS017 = db.SSS017.Where(x => x.LineId == Id).FirstOrDefault();

                    objSSS017.Project = fc["ddlProject" + Id];
                    if (fc["ShellThickness" + Id] == string.Empty)
                    {
                        objSSS017.ShellThickness = null;
                    }
                    else
                    {
                        objSSS017.ShellThickness = Convert.ToInt32(fc["ShellThickness" + Id]);
                    }
                    if (fc["HeadThickness" + Id] == string.Empty)
                    {
                        objSSS017.HeadThickness = null;
                    }
                    else
                    {
                        objSSS017.HeadThickness = Convert.ToInt32(fc["HeadThickness" + Id]);
                    }
                    objSSS017.Material = fc["Material" + Id];

                    objSSS017.DimOfBlcok = fc["DimOfBlcok" + Id];
                    if (fc["QuantityOfBlock" + Id] == string.Empty)
                    {
                        objSSS017.QuantityOfBlock = null;
                    }
                    else
                    {
                        objSSS017.QuantityOfBlock = Convert.ToInt32(fc["QuantityOfBlock" + Id]);
                    }

                    objSSS017.EditedBy = objClsLoginInfo.UserName;
                    objSSS017.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                    db.SaveChanges();
                }
                else
                {
                    objSSS017 = new SSS017();
                    objSSS017.MockUpBlockId = MockUpBlockId;
                    objSSS017.SSSId = db.SSS016.FirstOrDefault(i => i.MockUpBlockId == MockUpBlockId).SSSId;
                    objSSS017.Project = fc["ddlProject" + Id];
                    if (fc["ShellThickness" + Id] == string.Empty)
                    {
                        objSSS017.ShellThickness = null;
                    }
                    else
                    {
                        objSSS017.ShellThickness = Convert.ToInt32(fc["ShellThickness" + Id]);
                    }
                    if (fc["HeadThickness" + Id] == string.Empty)
                    {
                        objSSS017.HeadThickness = null;
                    }
                    else
                    {
                        objSSS017.HeadThickness = Convert.ToInt32(fc["HeadThickness" + Id]);
                    }
                    objSSS017.Material = fc["Material" + Id];

                    objSSS017.DimOfBlcok = fc["DimOfBlcok" + Id];
                    if (fc["QuantityOfBlock" + Id] == string.Empty)
                    {
                        objSSS017.QuantityOfBlock = null;
                    }
                    else
                    {
                        objSSS017.QuantityOfBlock = Convert.ToInt32(fc["QuantityOfBlock" + Id]);
                    }

                    objSSS017.CreatedBy = objClsLoginInfo.UserName;
                    objSSS017.CreatedOn = DateTime.Now;
                    db.SSS017.Add(objSSS017);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteMockupDataLine(int LineID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                SSS017 objSSS017 = db.SSS017.Where(x => x.LineId == LineID).FirstOrDefault();

                if (objSSS017 != null)
                {
                    db.SSS017.Remove(objSSS017);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Line not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Excell
        //Code By : Ajay Chauhan on 13/11/2017, Export Funxtionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_SSS_GETMockupBlock(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      SSSNo = li.SSSNo,
                                      Contract = li.Contract,
                                      Customer = li.Customer,
                                      CSRMeetingOn = li.CSRMeetingOn,
                                      Status = li.Status,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_SSS_GetMockupBlockLines(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      ShellThickness = li.ShellThickness,
                                      HeadThickness = li.HeadThickness,
                                      Material = li.Material,
                                      DimOfBlcok = li.DimOfBlcok,
                                      QuantityOfBlock = li.QuantityOfBlock,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string cssClass = "", string title = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + (cssClass != string.Empty ? cssClass : "");
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + (disabled ? "disabled" : "") + " title='" + title + "' />";

            return strAutoComplete;
        }

        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }

    }
}