﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.KeyUser.Controllers
{
    public class GlobalDataController : clsBase
    {
        // GET: KeyUser/GlobalData
        [SessionExpireFilter]
        public ActionResult Index()
        {
            var Category = (from a in db.GLB001
                            where a.IsActive == true
                            select new { a.Id, Desc = a.Category }).ToList();
            ViewBag.Category = new SelectList(Category, "Id", "Desc");

            return View();
        }

        [HttpPost]
        public ActionResult getData(JQueryDataTableParamModel param)
        {
            try
            {
                // IEnumerable<ATH001> _List = new List<ATH001>();

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                //var totalRecords = new ObjectParameter("totalRecords", typeof(int));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                int CategoryId = Convert.ToInt32(param.CategoryId);

                //string SortingFields = //param.iSortCols;

                var lstResult = db.SP_ATH_GET_GlobalData_DETAILS
                                (
                                StartIndex, EndIndex, "", CategoryId , ""
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.BUDesc),
                           Convert.ToString(uc.LocaDesc),
                           Convert.ToString(uc.CatDesc),
                           Convert.ToString(uc.Code),
                           Convert.ToString(uc.Description),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.CreatedOn),
                           Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = lstResult.FirstOrDefault().TotalCount,
                    iTotalDisplayRecords = lstResult.FirstOrDefault().TotalCount,
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetDataHtml(int Id , string Passval)
        {
            GLB002 objGLB002 = new GLB002();

            var BU = (  from a in db.COM002
                        where a.t_dtyp == 2 && a.t_dimx !=  null
                        select new { a.t_dimx, Desc = a.t_desc }).ToList();
            ViewBag.BU = new SelectList(BU, "t_dimx", "Desc");

            var Location = (from a in db.COM002
                        where a.t_dtyp == 1 && a.t_dimx != null
                        select new { a.t_dimx, Desc = a.t_desc }).ToList();

            ViewBag.Loca = new SelectList(Location, "t_dimx", "Desc");
            ViewBag.Categoryid = Passval.Split('-')[0].ToString();
            ViewBag.Category  = Passval.Split('-')[1].ToString();

            return PartialView("GetGlobalDataHtml", objGLB002);
        }

        [HttpPost]
        public ActionResult GetDataHtmlUpdate(int Id, string Passval)
        {
            GLB002 objGLB002 = new GLB002();

            if (Id > 0)
            {
                objGLB002 = db.GLB002.Where(x => x.Id == Id).FirstOrDefault();
            }

            var BU = (from a in db.COM002
                      where a.t_dtyp == 2 && a.t_dimx != null
                      select new { a.t_dimx, Desc = a.t_desc }).ToList();
            ViewBag.BU = new SelectList(BU, "t_dimx", "Desc");

            var Location = (from a in db.COM002
                            where a.t_dtyp == 1 && a.t_dimx != null
                            select new { a.t_dimx, Desc = a.t_desc }).ToList();

            ViewBag.Loca = new SelectList(Location, "t_dimx", "Desc");
            ViewBag.Category = Passval;

            return PartialView("GetGlobalDataHtml", objGLB002);
        }
        [HttpPost]
        public ActionResult SaveGDMatrix(GLB002 glb002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (glb002.Id > 0)
                {
                    GLB002 objGLB002 = db.GLB002.Where(x => x.Id == glb002.Id).FirstOrDefault();
                    objGLB002.BU = fc["ddlBU"].ToString();
                    objGLB002.Location = fc["ddlLoca"].ToString();
                    objGLB002.Category =Convert.ToInt32(fc["hfcatId"].ToString());
                    objGLB002.Code = fc["txtCode"].ToString();
                    objGLB002.Description = glb002.Description;
                    objGLB002.EditedBy = objClsLoginInfo.UserName;
                    objGLB002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {
                  
                    GLB002 objGLB002 = new GLB002();
                    objGLB002.BU = fc["ddlBU"].ToString();
                    objGLB002.Location = fc["ddlLoca"].ToString();
                    objGLB002.Category = Convert.ToInt32(fc["hfcatId"].ToString());
                    objGLB002.Code = fc["txtCode"].ToString();
                    objGLB002.Description = glb002.Description;
                    objGLB002.IsActive = true;
                    objGLB002.CreatedBy = objClsLoginInfo.UserName;
                    objGLB002.CreatedOn = DateTime.Now;
                    db.GLB002.Add(objGLB002);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteGDMatrix(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                GLB002 objGLB002 = db.GLB002.Where(x => x.Id == Id).FirstOrDefault();
                objGLB002.IsActive = false;
                objGLB002.EditedBy = objClsLoginInfo.UserName;
                objGLB002.EditedOn = DateTime.Now;
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateGDMatrix(GLB002 glb002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (glb002.Id > 0)
                {
                    GLB002 objGLB002 = db.GLB002.Where(x => x.Id == glb002.Id).FirstOrDefault();
                    //objGLB002.Code = fc["txtCode"].ToString();
                    objGLB002.Description = glb002.Description;
                    objGLB002.EditedBy = objClsLoginInfo.UserName;
                    objGLB002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

    }
}