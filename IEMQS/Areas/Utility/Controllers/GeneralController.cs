﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.WQ.Models;
using IEMQS.DESCore.Data;
using IEMQSImplementation;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Xml;
using static IEMQSImplementation.clsImplementationEnum;

namespace IEMQS.Areas.Utility.Controllers
{
    public class GeneralController : clsBase
    {
        public ActionResult Index()
        {
            return View();
        }

        //[HttpPost]
        //public ActionResult GetProjectsByUser()
        //{
        //    List<Projects> lstProjects = Manager.getProjectsByUser(objClsLoginInfo.UserName);
        //    //var lstProjects = new SelectList(project, "projectCode", "projectDescription");
        //    return Json(lstProjects, JsonRequestBehavior.AllowGet);
        //}
        [HttpPost]
        public ActionResult GetProjectResult(string term, string location = "")

        {
            List<Projects> lstProjects = new List<Projects>();
            var objAccessProjects = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, location).ToList();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProjects = (from c1 in db.COM001
                               where objAccessProjects.Contains(c1.t_cprj) && (c1.t_dsca.Contains(term) || c1.t_cprj.Contains(term))
                               select new Projects
                               {
                                   Value = c1.t_cprj,
                                   projectCode = c1.t_cprj,
                                   projectDescription = c1.t_cprj + " - " + c1.t_dsca,
                                   Text = c1.t_cprj + " - " + c1.t_dsca
                               }
                              ).Distinct().ToList();
                //lstProjects = Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, term).ToList();
            }
            else
            {
                lstProjects = (from c1 in db.COM001
                               where objAccessProjects.Contains(c1.t_cprj)
                               select new Projects
                               {
                                   Value = c1.t_cprj,
                                   projectCode = c1.t_cprj,
                                   projectDescription = c1.t_cprj + " - " + c1.t_dsca,
                                   Text = c1.t_cprj + " - " + c1.t_dsca
                               }
                              ).Distinct().Take(10).ToList();
                //lstProjects = Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, term).Take(10).ToList();
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Contracts(string term)
        {
            if (!string.IsNullOrWhiteSpace(term))
            {
                return Json(new clsManager()
                    .GetContractsByLoggedInUser(objClsLoginInfo.UserName, objClsLoginInfo.Location)
                    .Where(x => x.ContractNo.Contains(term) || x.ContractName.Contains(term))
                    .ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new clsManager()
                    .GetContractsByLoggedInUser(objClsLoginInfo.UserName, objClsLoginInfo.Location)
                    .ToList(), JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ProjectsByContract(string term, string contract)
        {
            if (!string.IsNullOrWhiteSpace(term))
            {
                return Json(new clsManager()
                       .GetProjectsByContract(contract, objClsLoginInfo.UserName, objClsLoginInfo.Location)
                       .ToList(), JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new clsManager()
                        .GetProjectsByContract(contract, objClsLoginInfo.UserName, objClsLoginInfo.Location)
                        .Where(x => x.ProjectNo.Contains(term) || x.ProjectName.Contains(term))
                        .ToList(), JsonRequestBehavior.AllowGet);
            }

        }

        // All active projects 
        [HttpPost]
        public ActionResult GetAllProjectResult(string term)
        {
            List<Projects> lstProjects = new List<Projects>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProjects = Manager.getAllActiveProject(term).ToList();
            }
            else
            {
                lstProjects = Manager.getAllActiveProject(term).Take(10).ToList();
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public virtual ActionResult Download(string file)
        {
            string fullPath = Path.Combine(Server.MapPath("~/Resources/Download/"), file);
            return File(fullPath, "application/vnd.ms-excel", file);
        }


        // All active & Inactive projects 
        [HttpPost]
        public ActionResult GetAllProject(string term)
        {
            List<Projects> lstProjects = new List<Projects>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProjects = Manager.getAllActiveInActiveProject(term).ToList();
            }
            else
            {
                lstProjects = Manager.getAllActiveInActiveProject(term).Take(10).ToList();
            }
            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }



        //All HtrNumbers    // Code Change by Rahul this function is filtering ProjectCode , Location, Draft status
        [HttpPost]
        public ActionResult GetAllHtr(string projectCode)
        {
            List<HTRNumbers> lstHtrs = new List<HTRNumbers>();
            var currentLoc = objClsLoginInfo.Location;
            var status = clsImplementationEnum.HTRStatus.Draft.GetStringValue();

            if (!string.IsNullOrWhiteSpace(projectCode))
            {
                lstHtrs = (from a in db.HTR001
                           where (a.Project.Trim() == projectCode.Trim() && a.Location.Trim() == currentLoc.Trim() && a.Status.Trim() == status.Trim())
                           select new HTRNumbers { projectCode = a.Project, HTRNos = a.HTRNo }).ToList();
            }
            else
            {
                //lstProjects = Manager.getAllActiveInActiveProject(term).Take(10).ToList();
            }

            return Json(lstHtrs, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetApproverResult(string term)
        {
            string rolePLNG2 = clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue();
            List<ApproverModel> lstApprovers = Manager.GetApproverList(rolePLNG2, objClsLoginInfo.Location, objClsLoginInfo.UserName, term).ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetApproverResultForPMG2(string term)
        {
            string rolePMG2 = clsImplementationEnum.UserRoleName.PMG2.GetStringValue();
            List<ApproverModel> lstApprovers = Manager.GetApproverList(rolePMG2, objClsLoginInfo.Location, objClsLoginInfo.UserName, term).ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetPMGUSER(string term)
        {
            List<ApproverModel> lstApprovers = Manager.GetManagerList(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term).ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetPMGManagerWithContract(string term, string ContractNo)
        {
            List<ApproverModel> lstApprovers = Manager.GetManagerList(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term).ToList();//Manager.GetManagerListWithContractno(clsImplementationEnum.UserRoleName.PMG2.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term, ContractNo).ToList();
            var psno = db.Database.SqlQuery<string>(@"SELECT t_crep  FROM " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + ContractNo + "'").FirstOrDefault();
            if (!string.IsNullOrEmpty(psno))
            {
                lstApprovers = lstApprovers.Where(i => i.Code == psno).ToList();
            }
            else
            {
                lstApprovers = new List<ApproverModel>();
            }

            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetApproverByRole(string term, string role)
        {
            List<ApproverModel> lstApprovers = Manager.GetApproverList(role, objClsLoginInfo.Location, objClsLoginInfo.UserName, term).ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }

        ////Created For AutoComplete 
        [HttpPost]
        public ActionResult GetApproverResultAutoComplete(string term)
        {
            List<ApproverModel> lstApprovers = new List<ApproverModel>();
            if (!string.IsNullOrEmpty(term))
            {
                lstApprovers = Manager.GetApproverList(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term).Distinct().ToList();
            }
            else
            {
                lstApprovers = Manager.GetApproverList(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term).Distinct().Take(10).ToList();
            }
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }
        ////created by Jaydeep Sakariya (21/08/2017)
        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }
        ////created by Jaydeep Sakariya (21/08/2017)
        [HttpPost]
        public ActionResult GetContractProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetContractProjectWise(projectCode);
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }

        ////created by Nikita (24/07/2017)
        ////description :for NDE Quality project autocomplete
        [HttpPost]
        public ActionResult GetQualityProjectResult(string term)
        {
            var lstQualityProject = (from a in db.QMS010
                                     where a.QualityProject.Contains(term)
                                     select new { qualityCode = a.QualityProject, qualityDescription = a.QualityProject }).Distinct().ToList();
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityProjectResultAutoComplete(string term, string location)
        {
            List<CategoryData> lstQualityProject = new List<CategoryData>();
            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = (from a in db.QMS010
                                     where a.QualityProject.Contains(term) && a.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                                     select new CategoryData { qualityCode = a.QualityProject, qualityDescription = a.QualityProject }).Distinct().ToList();
            }
            else
            {
                lstQualityProject = (from a in db.QMS010
                                     where a.QualityProject.Contains(term) && a.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                                     select new CategoryData { qualityCode = a.QualityProject, qualityDescription = a.QualityProject }).Distinct().Take(10).ToList();
            }
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        ////created by Trishul Tandel (24/08/2017)
        ////description :for Quality project autocomplete
        [HttpPost]
        public ActionResult GetQualityProjectByProject(string term, string project)
        {
            var lstQualityProject = (from a in db.QMS010
                                     where a.QualityProject.Contains(term) && a.Project == project
                                     select new { qualityCode = a.QualityProject, qualityDescription = a.QualityProject }).ToList();
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        ////created by Trishul Tandel (24/08/2017)
        ////description :for Quality IDs autocomplete
        [HttpPost]
        public ActionResult GetQualityIDsByQualityProject(string qproject, string type)
        {
            string type1 = type;
            if (type == "Part")
                type1 = "Assembly";

            var lstQualityIds1 = (from a in db.QMS015_Log
                                  where a.QualityProject == qproject && (a.QualityIdApplicableFor == type || a.QualityIdApplicableFor == type1)
                                  select new { QualityId = a.QualityId, qualityIdDescription = a.QualityIdDesc }).Distinct().ToList();

            //var lstQualityIds2 = (from a in db.QMS017_Log
            //                      where a.IQualityProject == qproject
            //                         select new { QualityId = a.QualityId, qualityIdDescription = a.QualityId }).Distinct().ToList();

            var lstQualityIds = lstQualityIds1;//.Union(lstQualityIds2);
            var list = lstQualityIds.Select(x => new BULocWiseCategoryModel { CatDesc = x.QualityId, CatID = x.QualityId }).ToList();
            return Json(list, JsonRequestBehavior.AllowGet);
        }

        ////created by Nikita (26/07/2017)
        ////description :for Project autocomplete in PCL
        [HttpPost]
        public ActionResult GetProject(string term)
        {
            var lstProjectDesc = (from a in db.COM001
                                  where a.t_psts == 1 && (a.t_cprj.Contains(term))
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            return Json(lstProjectDesc, JsonRequestBehavior.AllowGet);
        }

        ////created by Deepak (17/08/2017)
        ////description :for Project autocomplete in PCL
        [HttpPost]
        public ActionResult GetProjectListForTPI(string term)
        {
            var lstQualityProject = (from a in db.QMS010
                                     where a.QualityProject.Contains(term)
                                     select new { qualityCode = a.QualityProject, qualityDescription = a.QualityProject }).ToList();
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }
        ////created by Deepak (18/08/2017)
        ////description :for Tpi Agency autocomplete in TPI
        [HttpPost]
        public ActionResult GeTPIAgencyListForTPI(string term)
        {
            var StageType = (from a in db.GLB002
                             where a.Category == 72 && a.IsActive == true && a.Description.Contains(term)
                             select new { a.Code, Desc = a.Code + " - " + a.Description }).ToList();
            return Json(StageType, JsonRequestBehavior.AllowGet);
        }

        ////created by Deepak (18/08/2017)
        ////description :for Tpi Agency autocomplete in TPI
        [HttpPost]
        public ActionResult GeTPIInterventionList(string term)
        {
            var StageType = (from a in db.GLB002
                             where a.Category == 65 && a.IsActive == true && a.Description.Contains(term)
                             select new { a.Code, Desc = a.Code + " - " + a.Description }).ToList();
            return Json(StageType, JsonRequestBehavior.AllowGet);
        }


        ////created by Pooja (28/07/2017)
        ////description :for Category in NDE
        [HttpPost]
        public ActionResult GetSubCatagory(string Key, string strBU = "", string strLoc = "")
        {
            List<CategoryData> lstGLB002 = (from glb002 in db.GLB002
                                            join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                            where glb001.Category == Key && glb002.IsActive == true && strBU.Equals(glb002.BU) && strLoc.Equals(glb002.Location)
                                            select new CategoryData { Id = glb002.Id, CategoryDescription = glb002.Code + " - " + glb002.Description }).ToList();

            return Json(lstGLB002, JsonRequestBehavior.AllowGet);
        }

        ////created by Ravi (02/08/2018)
        [HttpPost]
        public ActionResult GetSubCatagoryForAutoComplete(string Key, string BU = "", string Loc = "", string term = "")
        {
            var lstData = (from glb002 in db.GLB002
                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                           where glb001.Category == Key && glb002.IsActive == true && BU.Equals(glb002.BU) && Loc.Equals(glb002.Location)
                           select new AutoCompleteList { id = glb002.Code, value = glb002.Description, label = glb002.Code + " - " + glb002.Description }).ToList();
            if (!String.IsNullOrWhiteSpace(term))
                lstData = lstData.Where(w => w.label.Contains(term)).ToList();

            return Json(lstData, JsonRequestBehavior.AllowGet);
        }

        ////created by Pooja Desai (01/08/2017)
        //// employee -Name autocomplete
        [HttpPost]
        public ActionResult GetEmployeeResult(string term)
        {
            List<Employee> lstEmployee = Manager.getEmployeeAutocomplete(term).ToList();
            return Json(lstEmployee, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetFurnaceSupervisorResult(string term)
        {
            List<ApproverModel> lstApprovers = Manager.GetApproverList(clsImplementationEnum.UserRoleName.FS3.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term).Distinct().ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMETResult(string term)
        {
            List<ApproverModel> lstApprovers = Manager.GetApproverList(clsImplementationEnum.UserRoleName.MET3.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term).Distinct().ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetMETWEResult(string term)
        {
            List<ApproverModel> lstApprovers = Manager.GetMEWRoleList(objClsLoginInfo.Location, objClsLoginInfo.UserName, term, "").Distinct().ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetInspectorResult(string term)
        {
            List<ApproverModel> lstApprovers = Manager.GetInspectorList(objClsLoginInfo.Location, objClsLoginInfo.UserName, term).Distinct().ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }

        //Added by M. Ozair Khatri (90385894) on 24-08-2017 to fetch SSRS Report
        public ActionResult OpenSSRSReport(string serverRelativeUrlPath, List<SSRSParam> ReportParameters, bool ShowAttachment = false)
        {
            //var credentials = new WindowsImpersonationCredentials();

            string url = Request.Url.AbsoluteUri;           // http://localhost:1302/TESTERS/Default6.aspx
            string path = Request.Url.AbsolutePath;         // /TESTERS/Default6.aspx
            //string host = Request.Url.Scheme + System.Uri.SchemeDelimiter + Request.Url.Host;                 // localhost
            var host = System.Configuration.ConfigurationManager.AppSettings["Applicaiton_URL"];
            ViewBag.ReportUrl = serverRelativeUrlPath;
            var ReportParams = new List<ReportParameter>();
            var param = new Dictionary<string, string[]>();
            foreach (var rp in ReportParameters)
            {
                if (param.Any(q => q.Key.Equals(rp.Param)))
                {
                    var oldValue = param.FirstOrDefault(q => q.Key.Equals(rp.Param)).Value.ToList();
                    param.Remove(rp.Param);
                    oldValue.Add(rp.Value);
                    param.Add(rp.Param, oldValue.ToArray());
                }
                else
                    param.Add(rp.Param, new string[] { rp.Value });
            }
            foreach (var p in param)
                ReportParams.Add(new ReportParameter(p.Key, p.Value));

            if (ShowAttachment)
            {
                ReportParams.Add(new ReportParameter("Host", host));
            }
            ViewBag.Params = ReportParams;
            return View("Report");//return clsReport.GetReportData(serverRelativeUrlPath, ReportParameters, CustomServerPath);
        }



        ////created by Nikita (3/10/2017)
        ////description :for Project autocomplete in PCL

        [HttpPost]
        public ActionResult GetIntExtWelderDetail(string term, string location)
        {
            List<ApproverModel> lstApproverModel = new List<ApproverModel>();
            List<ModelWelderDetail> lstModelWelderDetail = new List<ModelWelderDetail>();
            if (!string.IsNullOrEmpty(term))
            {
                var listWelders = (from a in db.QMS003
                                   where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                                   select new ModelWelderDetail { WelderPSNo = a.WelderPSNo, WelderName = string.IsNullOrEmpty(a.Name) ? "" : a.Name }
                                      )
                                 .Union(from b in db.QMS004
                                        where b.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany == true //&& (b.Name.ToLower().Contains(term.ToLower()) || b.WelderPSNo.Contains(term))
                                        select new ModelWelderDetail { WelderPSNo = b.WelderPSNo, WelderName = (string.IsNullOrEmpty(b.Name) ? "" : b.Name) + " " + (string.IsNullOrEmpty(b.FatherName) ? "" : b.FatherName) + " " + (string.IsNullOrEmpty(b.Surname) ? "" : b.Surname) }).Select(i => new { WelderPSNo = i.WelderPSNo, WelderName = i.WelderName }).Distinct().ToList();
                lstModelWelderDetail = listWelders.Where(a => (a.WelderName.ToLower().Contains(term.ToLower()) || a.WelderPSNo.Contains(term))).Select(i => new ModelWelderDetail { WelderPSNo = i.WelderPSNo, WelderName = i.WelderName }).ToList();
            }
            else
            {
                lstModelWelderDetail = (from a in db.QMS003
                                        where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                                        select new ModelWelderDetail { WelderPSNo = a.WelderPSNo, WelderName = string.IsNullOrEmpty(a.Name) ? "" : a.Name }
                      )
                 .Union(from b in db.QMS004
                        where b.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany == true
                        select new ModelWelderDetail { WelderPSNo = b.WelderPSNo, WelderName = (string.IsNullOrEmpty(b.Name) ? "" : b.Name) + " " + (string.IsNullOrEmpty(b.FatherName) ? "" : b.FatherName) + " " + (string.IsNullOrEmpty(b.Surname) ? "" : b.Surname) }).ToList();
            }
            lstApproverModel = lstModelWelderDetail.Select(i => new ApproverModel { Code = i.WelderPSNo, Name = i.WelderPSNo + "-" + i.WelderName, WelderName = i.WelderName }).Take(10).ToList();

            return Json(lstApproverModel, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetSupplier(string term)
        {
            var lstSupplier = (from a in db.COM006
                               where (a.t_bpid.Contains(term))
                               select new DropdownModel { Code = a.t_bpid, Description = a.t_bpid + " - " + a.t_nama }).ToList();
            return Json(lstSupplier, JsonRequestBehavior.AllowGet);
        }

        //Added as  New Method for attaching < by M. Ozair Khatri on 10-08-2017>
        #region SharepointAttachments
        /// <summary>
        /// Gets an HTML string for documents at the path specified by <paramref name="folderPath"/> 
        /// -- Added by M. Ozair Khatri on 10-08-2017
        /// </summary>
        /// <param name="folderPath">Sharepoint Path to fetch files from</param>
        /// <returns>HTML string of Documents</returns>
        [HttpPost]
        public string getDocuments(string folderPath, bool includeFromSubfolders = false)
        {
            if (string.IsNullOrWhiteSpace(folderPath))
                return "";
            string result = "";
            var Files = clsUpload.getDocsCopy(folderPath, includeFromSubfolders);
            foreach (var file in Files)
            {
                result += "<input type=\"hidden\" id=\"" + file.Name + "\" value=\"URL:" + file.URL + "\" class=\"attach\" />";
                if (!string.IsNullOrWhiteSpace(file.Comments))
                    result += "<input type=\"text\" disabled data-toggle=\"tooltip\" placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"" + file.Comments + "\" title=\"" + file.Comments + "\" />";
                else
                    result += "<input type=\"text\" disabled  placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"\" />";

            }
            return result;
        }

        ///// <summary>
        ///// Gets an HTML string for documents at the path specified by <paramref name="folderPath"/> from KM Library
        ///// -- Added by M. Ozair Khatri on 10-08-2017
        ///// </summary>
        ///// <param name="folderPath">Sharepoint Path to fetch files from</param>
        ///// <returns>HTML string of Documents</returns>
        //[HttpPost]
        //public string getDocumentsKM(string folderPath)
        //{
        //    if (string.IsNullOrWhiteSpace(folderPath))
        //        return "";
        //    string result = "";
        //    var Files = clsUploadKM.getDocs(folderPath);
        //    foreach (var file in Files)
        //    {
        //        result += "<input type=\"hidden\" id=\"" + file.Name + "\" value=\"URL:" + file.URL + "\" class=\"attach\" />";
        //        if (!string.IsNullOrWhiteSpace(file.Comments))
        //            result += "<input type=\"text\" disabled data-toggle=\"tooltip\" placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"" + file.Comments + "\" title=\"" + file.Comments + "\" />";
        //        else
        //            result += "<input type=\"text\" disabled  placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"\" />";

        //    }
        //    return result;
        //}
        #endregion

        #region NCR
        /// <summary>
        /// Created by Zaid Khandwani (21/08/2017)
        /// Autoextender for Role base employee detail for specific Project
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ProjectRoleWiseEmpDetail(string term, string role, string project, string BU)
        {
            if (string.IsNullOrEmpty(BU))
                return Json(null, JsonRequestBehavior.AllowGet);
            string bu = BU.Split('-')[0].Trim();
            List<Employee> lstEmployee = Manager.getProjectEmployeeAutocomplete(role, project, term, bu).Distinct().ToList();
            return Json(lstEmployee, JsonRequestBehavior.AllowGet);
        }

        public ActionResult supplier(string term, string project)
        {
            List<Supplier> lstSupplier = Manager.getSupplierAutocomplete(term).Distinct().ToList();
            return Json(lstSupplier, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Created by Zaid Khandwani (21/08/2017)
        /// Autoextender for Department
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeptDetail(string term)
        {
            List<Department> lstDepartment = Manager.getDepartmentAutocomplete(term).ToList();
            return Json(lstDepartment, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ResponsibleDeptManager(string term, string project)
        {
            List<Employee> lstEmployee = Manager.getNCRResponsibleDeptManagerAutocomplete(term, project).ToList();
            return Json(lstEmployee, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetShopCode(string term)
        {
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            if (!string.IsNullOrEmpty(term))
            {
                lstCategoryData = db.COM002.Where(i => i.t_dtyp == 3 && i.t_dimx != "" && (i.t_dimx.ToLower().Contains(term.ToLower()) || i.t_desc.ToLower().Contains(term.ToLower())))
                                .Select(i => new CategoryData { Code = i.t_dimx, CategoryDescription = i.t_dimx + "-" + i.t_desc }).ToList();
            }
            else
            {
                lstCategoryData = db.COM002.Where(i => i.t_dtyp == 3 && i.t_dimx != "")
                                .Select(i => new CategoryData { Code = i.t_dimx, CategoryDescription = i.t_dimx + "-" + i.t_desc }).Take(10).ToList();
            }
            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQCUser(string term)
        {
            List<ApproverModel> lstApprovers = Manager.GetManagerList(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term).ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDesignerUser(string term)
        {
            List<ApproverModel> lstApprovers = Manager.GetManagerList(clsImplementationEnum.UserRoleName.ENGG3.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term).ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDesignerLead(string term)
        {
            List<ApproverModel> lstApprovers = Manager.GetManagerList(clsImplementationEnum.UserRoleName.ENGG2.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term).ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region BigAttachments
        [HttpPost]
        public string GetBigDocuments(string folderPath, bool includeFromSubfolders = false, string Type = "Normal")
        {
            if (string.IsNullOrWhiteSpace(folderPath))
                return "";
            string result = "";
            Type = (Type == "Normal" ? "" : Type);
            //var Files = clsUploadCopy.GetDocuments(folderPath, Manager.GetATHObject(Type), includeFromSubfolders);
            var Files = (new clsFileUpload(Type)).GetDocuments(folderPath, includeFromSubfolders);
            foreach (var file in Files)
            {
                result += "<input type=\"hidden\" id=\"" + file.Name + "\" value=\"URL:" + file.URL + "\" class=\"attach\" />";
                if (!string.IsNullOrWhiteSpace(file.Comments))
                    result += "<input type=\"text\" disabled data-toggle=\"tooltip\" placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"" + file.Comments + "\" title=\"" + file.Comments + "\" />";
                else
                    result += "<input type=\"text\" disabled  placeholder=\"Comments\" id=\"_" + file.Name + "\" class=\"form-control comment\" onchange=\"$(this).attr('value', $(this).val())\" maxlength=\"100\" value=\"\" />";
            }
            return result;
        }

        [AllowAnonymous]
        public string GetBigDocumentsXML(string folderPath, bool includeFromSubfolders = false, string Type = "Normal")
        {
            if (string.IsNullOrWhiteSpace(folderPath))
                return "";
            Type = Type == "Normal" ? "" : Type;
            var Files = (new clsFileUpload(Type)).GetDocuments(folderPath, includeFromSubfolders);
            XmlDocument doc = new XmlDocument();
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);
            XmlElement element1 = doc.CreateElement(string.Empty, "body", string.Empty);
            doc.AppendChild(element1);
            foreach (var file in Files)
            {
                XmlElement element2 = doc.CreateElement(string.Empty, "Attachment", string.Empty);
                element2.SetAttribute("Name", file.Name);
                element2.SetAttribute("Comments", file.Comments);
                element2.SetAttribute("URL", file.URL);
                element2.SetAttribute("FolderPath", folderPath);
                element1.AppendChild(element2);
            }
            return doc.OuterXml;
        }

        [HttpPost]
        public bool ManageBigDocuments(string folderPath, string[] toDelete, string Type = "Normal")
        {
            try
            {
                //var existing = clsUploadCopy.GetDocuments(folderPath, Manager.GetATHObject(Type));                
                var newFiles = Request.Files.AllKeys;
                foreach (var item in toDelete)
                {
                    foreach (var fileName in item.Split(','))
                        clsUploadCopy.DeleteFile(folderPath, fileName, Manager.GetATHObject(Type));
                }
                var result = true;
                foreach (var fileName in newFiles)
                {
                    try
                    {
                        var stream = Request.Files.Get(fileName).InputStream;
                        var CommentName = "_" + fileName;
                        var comment = (string)Request.Form.Get(CommentName);
                        //using (var file = System.IO.File.Create(Server.MapPath("~/Temp/" + Session.SessionID + "/" + fileName)))
                        //{
                        //stream.CopyTo(file);
                        var uploaded = clsUploadCopy.Upload(fileName, folderPath, Manager.GetATHObject(Type), stream/*new FileStream(Server.MapPath("~/Temp/" + Session.SessionID + "/" + fileName),FileMode.Open)*/, objClsLoginInfo.UserName, comment);
                        result = !uploaded ? uploaded : result;
                        //}
                    }
                    catch (Exception exc) { Elmah.ErrorSignal.FromCurrentContext().Raise(exc); result = false; }
                }
                return result;
            }
            catch (Exception exc) { Elmah.ErrorSignal.FromCurrentContext().Raise(exc); }
            return false;
        }

        [HttpPost]
        public ActionResult GetAttachmentParams(string Type = "Normal")
        {
            var objATH008 = Manager.GetATHObject(Type);
            return Json(new
            {
                FileSize = objATH008.MaxFileSize,
                FileExtn = objATH008.AllowedExtensions.Split(',')
            });
        }

        [HttpPost]
        public ActionResult DownloadSharePointFile(string fileName, string folderPath, string Type = "Normal")
        {
            if (string.IsNullOrWhiteSpace(fileName) || string.IsNullOrWhiteSpace(folderPath))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            Type = Type == "Normal" ? "" : Type;
            var stream = (new clsFileUpload(Type)).GetFileStream(folderPath, fileName);
            FileStreamResult fsr = new FileStreamResult(stream, "application/octet-stream");
            fsr.FileDownloadName = fileName;
            return fsr;
        }

        #endregion

        #region Notification
        [HttpPost]
        public ActionResult ViewNotification(string username, string Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<string> lstNotificationType = new List<string> { clsImplementationEnum.NotificationType.Alert.GetStringValue(), clsImplementationEnum.NotificationType.Announcement.GetStringValue(), clsImplementationEnum.NotificationType.Information.GetStringValue(), clsImplementationEnum.NotificationType.NewlyAdded.GetStringValue() };

                List<GLB010> lstGLB010 = new List<GLB010>();

                if (string.IsNullOrEmpty(Id))
                    lstGLB010 = db.GLB010.Where(c => c.PSNo == username && lstNotificationType.Contains(c.NotificationType) && c.IsRead == false).ToList();
                else
                {
                    int nID = Convert.ToInt32(Id);
                    lstGLB010 = db.GLB010.Where(c => c.PSNo == username && c.Id == nID && c.IsRead == false).ToList();
                }
                lstGLB010.ForEach(x =>
                {
                    x.IsRead = true;
                });

                db.SaveChanges();

                objResponseMsg.Key = true;
                int totalcount = 0;
                //int temp = db.GLB010.Where(c => c.PSNo == username && c.NotificationType != "Alert" && c.IsRead == false).ToList().Count();
                totalcount = db.Database.SqlQuery<int>(@"SELECT Count(*) From GLB010 Where PSNo = '" + username + "' and IsRead = 0 and NotificationType <> 'Alert'").FirstOrDefault();
                objResponseMsg.Value = Convert.ToString(totalcount);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult MarkAsReadAllNotification(string username)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<GLB010> lstGLB010 = new List<GLB010>();
                if (!string.IsNullOrEmpty(username))
                {
                    lstGLB010 = db.GLB010.Where(c => c.PSNo == username && c.IsRead == false).ToList();
                }

                lstGLB010.ForEach(x => { x.IsRead = true; });
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult HeaderNotificationPopupPartial()
        {
            return PartialView("_HeaderNotificationPopupPartial");
        }

        [HttpPost]
        public ActionResult HeaderNotificationGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_HeaderNotificationGridDataPartial");
        }

        public ActionResult LoadHeaderNotificationGridData(IEMQS.Models.JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;
                string whereCondition = "1=1";

                string indextype = param.Department;
                if (string.IsNullOrWhiteSpace(indextype))
                {
                    indextype = clsImplementationEnum.WPPIndexType.maintain.GetStringValue();
                }

                if (objClsLoginInfo != null)
                {
                    whereCondition += " AND G.PSNo='" + objClsLoginInfo.UserName + "' AND G.NotificationType <> 'Alert'";
                }
                string strSortOrder = string.Empty;

                if (status.ToLower() == "pending")
                {
                    whereCondition += " AND G.IsRead = 0";
                    strSortOrder = "ORDER BY tm.CreatedOn DESC";
                }
                else
                {
                    strSortOrder = "ORDER BY tm.IsRead, tm.CreatedOn DESC";
                }

                string[] columnName = { "NotificationMsg" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstNotification = db.SP_GLB_GET_USER_NOTIFICATION(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstNotification.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstNotification
                           select new[] {
                               Convert.ToString(h.Id),
                               "<a href="+(string.IsNullOrEmpty(Convert.ToString(h.RedirectionPath)) ? "javascript:void(0);" : clsBase.WebsiteURL + h.RedirectionPath )+" onclick=\"ViewNotification('"+objClsLoginInfo.UserName.Trim()+"',"+h.Id+"); return true; \"><span class=\"details " + (Convert.ToBoolean(h.IsRead) ? "" : " unread-notification")+"\"><span style=\"padding: 4px 10px 4px 10px;\" class=\"label label-sm label-icon label-" + h.IconColor+"\"><i class=\"fa fa-"+h.Icon+"\"></i></span> "+h.NotificationMsg+"</span></a>"

                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region SidebarMenu
        [HttpPost]
        public ActionResult LoadSidebarPartial()
        {
            return PartialView("_SidebarPartial");
        }
        #endregion

        public ActionResult GetWeldderPSNo(string term)
        {
            var lstWelder = (from a in db.QMS003
                             where a.WelderPSNo.Contains(term) || a.Name.Contains(term)
                             select new ModelWelderDetail { WelderPSNo = a.WelderPSNo, ShopCode = a.ShopCode, Stamp = a.Stamp, WelderName = a.WelderPSNo + " - " + a.Name }
                                 )
                                 .Union(from b in db.QMS004
                                        where b.WelderPSNo.Contains(term) || b.Name.Contains(term)
                                        select new ModelWelderDetail { WelderPSNo = b.WelderPSNo, ShopCode = b.ShopCode, Stamp = b.Stamp, WelderName = b.WelderPSNo + " - " + b.Name }).Distinct().ToList();

            return Json(lstWelder, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetWMLWeldderPSNo(string term, string jointtype, string weldingprocess, string location)
        {
            List<ModelWelderDetail> lstWelder = new List<ModelWelderDetail>();
            //switch (jointtype.Trim())
            //{
            //    case "Groove/Fillet":
            //        lstWelder = (from a in db.WQR004
            //                     join com in db.COM003 on a.Welder equals com.t_psno                                 
            //                     where a.ValueUpto >= DateTime.Now && (a.Welder.Contains(term) || com.t_name.Contains(term))
            //                     select new ModelWelderDetail { WelderPSNo = a.Welder, WelderName = a.Welder + "-" + com.t_name }
            //                    ).Distinct().ToList();
            //        break;
            //    case "Overlay":
            //        lstWelder = (from a in db.WQR005
            //                     join com in db.COM003 on a.WelderName equals com.t_psno
            //                     where a.ValueUpto >= DateTime.Now && (a.WelderName.Contains(term) || com.t_name.Contains(term))
            //                     select new ModelWelderDetail { WelderPSNo = a.WelderName, WelderName = a.WelderName + "-" + com.t_name }
            //                    ).Distinct().ToList();
            //        break;
            //    case "T#TS":
            //        lstWelder = (from a in db.WQR006
            //                     join com in db.COM003 on a.WelderName equals com.t_psno
            //                     where a.ValueUpto >= DateTime.Now && a.WelderName.Contains(term)
            //                     select new ModelWelderDetail { WelderPSNo = a.WelderName, WelderName = a.WelderName + "-" + com.t_name }
            //                    ).Distinct().ToList();
            //        break;
            //}

            if (!string.IsNullOrEmpty(jointtype))
            {
                var objWelderDtl = db.SP_GEN_GET_WELDER_DETAILS_BY_JOINTTYPE(jointtype, term, weldingprocess, location);
                if (objWelderDtl != null)
                {
                    lstWelder = objWelderDtl.Select(a => new ModelWelderDetail { WelderPSNo = a.WelderPSNo, WelderName = a.WelderName, Stamp = a.Stamp }).ToList();
                }
            }
            if (term == string.Empty)
            {
                lstWelder = lstWelder.Take(10).ToList();
            }
            return Json(lstWelder, JsonRequestBehavior.AllowGet);
        }
        #region Report Filter(IPI/NDE)
        //fetch all BU 
        [HttpPost]
        public JsonResult GetAllBU()
        {
            try
            {
                var lstBu = (from ath1 in db.ATH001
                             join com2 in db.COM002 on ath1.BU equals com2.t_dimx
                             where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim() && ath1.BU != ""
                             select new { id = ath1.BU, text = com2.t_desc }).Distinct().ToList();
                return Json(lstBu, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        //fetch Location based on selected BU
        [HttpPost]
        public JsonResult GetAllLocation(string selectedBU = "")
        {
            try
            {
                var lstBusLoc = (from ath1 in db.ATH001
                                 join com2 in db.COM002 on ath1.Location equals com2.t_dimx
                                 where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim() && (selectedBU != "" ? selectedBU.Contains(ath1.BU) : true)
                                 select new { id = ath1.Location, text = com2.t_desc }).Distinct().ToList();
                return Json(lstBusLoc, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        //fetch Location based on selected BU
        [HttpPost]
        public ActionResult GetProcessCluster(string term, string selectedBU, string selectedLoc)
        {
            try
            {
                List<CategoryData> lstProcessCluster = new List<CategoryData>();
                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstProcessCluster = (from a in db.QMS010
                                         where a.InspectionCentre.Contains(term) && selectedBU.ToLower().Contains(a.BU.Trim().ToLower()) && selectedLoc.ToLower().Contains(a.Location.Trim().ToLower()) && a.InspectionCentre != null
                                         select new CategoryData { id = a.InspectionCentre, text = a.InspectionCentre }
                                          ).Distinct().ToList();
                }
                else
                {
                    lstProcessCluster = (from a in db.QMS010
                                         where selectedBU.ToLower().Contains(a.BU.Trim().ToLower()) && selectedLoc.ToLower().Contains(a.Location.Trim().ToLower()) && a.InspectionCentre != null
                                         select new CategoryData { id = a.InspectionCentre, text = a.InspectionCentre }
                                      ).Distinct().ToList();
                }
                return Json(lstProcessCluster, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        //fetch Quality Project based on selected BU and Location
        [HttpPost]
        public JsonResult GetAllQualityProject(string term, string selectedBU, string selectedLoc)
        {
            try
            {
                List<CategoryData> lstQualityProject = new List<CategoryData>();
                var lstProjectByUser = Manager.getProjectsByUser(objClsLoginInfo.UserName.Trim()).Select(x => x.projectCode).Distinct().ToList();
                List<string> arrBU = new List<string>();
                List<string> arrLoc = new List<string>();
                if (!string.IsNullOrWhiteSpace(selectedBU))
                {
                    arrBU = selectedBU.Split(',').ToList();
                }
                if (!string.IsNullOrWhiteSpace(selectedLoc))
                {
                    arrLoc = selectedLoc.ToLower().Split(',').ToList();
                }
                if (!string.IsNullOrWhiteSpace(term))
                {
                    lstQualityProject = (from lst in lstProjectByUser
                                         join qms10 in db.QMS010 on lst equals qms10.Project
                                         where qms10.QualityProject.ToLower().Trim().Contains(term.ToLower().Trim()) && arrBU.Contains(qms10.BU.ToLower().Trim()) && arrLoc.Contains(qms10.Location.ToLower().Trim())
                                         orderby qms10.QualityProject
                                         select new CategoryData { id = qms10.QualityProject, text = qms10.QualityProject }).Distinct().ToList();
                }
                else
                {
                    lstQualityProject = (from lst in lstProjectByUser
                                         join qms10 in db.QMS010 on lst equals qms10.Project
                                         where selectedBU.ToLower().Contains(qms10.BU.Trim().ToLower()) && selectedLoc.ToLower().Contains(qms10.Location.Trim().ToLower())
                                         orderby qms10.QualityProject
                                         select new CategoryData { id = qms10.QualityProject, text = qms10.QualityProject }).Distinct().ToList();
                }
                lstQualityProject = lstQualityProject.Distinct().ToList();
                return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetSelectedLocWiseQualityProject(string term, string location)
        {
            try
            {
                List<CategoryData> lstQualityProject = new List<CategoryData>();
                var lstProjectByUser = Manager.getProjectsByUser(objClsLoginInfo.UserName.Trim());
                var item = (from lst in lstProjectByUser
                            join qms10 in db.QMS010 on lst.projectCode equals qms10.Project
                            where location.Trim().ToLower() == qms10.Location.Trim().ToLower()
                            select new CategoryData { id = qms10.QualityProject, text = qms10.QualityProject }).ToList().Select(i => new { id = i.id, text = i.text }).Distinct().ToList();

                if (!string.IsNullOrWhiteSpace(term))
                {

                    lstQualityProject = (from lst in item
                                         where lst.id.Trim().Contains(term.Trim())
                                         select new CategoryData { id = lst.id, text = lst.text }).Distinct().ToList();
                }
                else
                {
                    lstQualityProject = (from lst in item
                                         select new CategoryData { id = lst.id, text = lst.text }).Distinct().ToList();
                }
                return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        //stage type
        [HttpPost]
        public ActionResult GetAllStagesType(string selectedBU, string selectedLoc)
        {
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            lstCategoryData = db.QMS002.Where(x => x.StageType != "" && selectedLoc.Contains(x.Location) && selectedBU.Contains(x.BU)).
                                Select(i =>
                                new CategoryData
                                { id = i.StageType, text = i.StageType }
                                ).Distinct().ToList();
            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }

        //stage code
        [HttpPost]
        public ActionResult GetAllStages(string selectedBU, string selectedLoc, string selectedType)
        {
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            lstCategoryData = db.QMS002.Where(x => x.StageCode != "" && selectedType.Contains(x.StageType) && selectedLoc.Contains(x.Location) && selectedBU.Contains(x.BU)).
                                Select(i =>
                                new CategoryData
                                { id = i.StageCode, text = i.StageCode + "-" + i.StageDesc }
                                ).Distinct().ToList();
            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }

        //Shops
        [HttpPost]
        public ActionResult GetAllShops(string selectedBU, string selectedLoc)
        {
            try
            {
                IQueryable<CategoryData> lstCategoryData = null;
                lstCategoryData = (from glb002 in db.GLB002
                                   join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                   where glb001.Category.Equals("Shop", StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && selectedLoc.Contains(glb002.Location) && selectedBU.Contains(glb002.BU)
                                   select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { id = i.Code, text = i.Code + " - " + i.Description }).Distinct();

                return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Protocol
        [HttpPost]
        public bool ViewProtocolByPROD(string ProtocolType, int ProtocolId)
        {
            try
            {
                string PRLTableName = Manager.GetLinkedProtocolTableName(ProtocolType);
                db.Database.ExecuteSqlCommand("UPDATE " + PRLTableName + " SET EditedBy='" + objClsLoginInfo.UserName + "', EditedOn='" + DateTime.Now + "' WHERE HeaderId = " + ProtocolId);

                return true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return false;
            }
        }

        //stage code
        [HttpPost]
        public ActionResult GetAllProtocolbyStages(string term, string selectedBU, string selectedLoc, string stage)
        {
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            var lst = Manager.GetSubCatagories("Protocol Type", selectedBU, selectedLoc).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstProtocolType = db.PRL000.Where(x => x.StageCode == stage && x.Location == selectedLoc && x.BU == selectedBU && x.DeletedBy == null).Select(i => i.ProtocolType).ToArray();

            lstCategoryData = (from a in lst
                               where lstProtocolType.Contains(a.Value) && (term == "" || a.CategoryDescription.Contains(term))
                               select new CategoryData { Value = a.Code, Code = a.Code, CategoryDescription = a.CategoryDescription }).Distinct().ToList();

            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }

        //stage code
        [HttpPost]
        public ActionResult GetAllProtocolbyStageswithOtherFiles(string term, string selectedBU, string selectedLoc, string stage)
        {
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            var lst = Manager.GetSubCatagories("Protocol Type", selectedBU, selectedLoc).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            var lstProtocolType = db.PRL000.Where(x => x.StageCode == stage && x.Location == selectedLoc && x.BU == selectedBU && x.DeletedBy == null).Select(i => i.ProtocolType).ToArray();
            var otherfiles = clsImplementationEnum.ProtocolType.Other_Files.GetStringValue();
            lstCategoryData = (from a in lst
                               where lstProtocolType.Contains(a.Value) && (term == "" || a.CategoryDescription.Contains(term))
                               select new CategoryData { Value = a.Code, Code = a.Code, CategoryDescription = a.CategoryDescription }).Distinct().ToList();
            lstCategoryData.Insert(0, lst.Where(o => o.Code == otherfiles).Select(a => new CategoryData { Value = a.Code, Code = a.Code, CategoryDescription = a.CategoryDescription }).FirstOrDefault());
            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Traveler Methods
        [HttpPost]
        public ActionResult GetTravelerProjects(string term)
        {
            List<AutoCompleteModel> lstProject = new List<AutoCompleteModel>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProject = db.TVL001.Where(i => i.ProjectNo.ToUpper().Contains(term.ToUpper())).Select(s => new AutoCompleteModel { Text = s.ProjectNo, Value = s.ProjectNo }).ToList();
            }
            else
            {
                lstProject = db.TVL001.Select(s => new AutoCompleteModel { Text = s.ProjectNo, Value = s.ProjectNo }).Take(10).ToList();
            }
            return Json(lstProject, JsonRequestBehavior.AllowGet); ;
        }

        #endregion
        #region Segregnation Of Duties
        [HttpPost]
        public ActionResult GetAllRoles(string term)
        {
            List<AutoCompleteModelForRole> lstRole = new List<AutoCompleteModelForRole>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstRole = db.ATH004.Where(i => i.Role.ToUpper().Contains(term.ToUpper())).Select(s => new AutoCompleteModelForRole { Text = s.Role, Value = s.Id }).OrderBy(x => x.Text).ToList();
            }
            else
            {
                lstRole = db.ATH004.Select(s => new AutoCompleteModelForRole { Text = s.Role, Value = s.Id }).OrderBy(x => x.Text).ToList();
            }
            return Json(lstRole, JsonRequestBehavior.AllowGet); ;
        }

        #endregion

        #region MIP partial view and methods for grid actions 
        [HttpPost]
        public ActionResult MIPLoadOfferFilteredDataPartial(FilterReqMIPModel filterparamodel)
        {
            filterparamodel.objMIP004 = db.MIP004.FirstOrDefault(f => f.LineId == filterparamodel.LineId); //&& f.RequestSequenceNo == filterparamodel.RequestSequenceNo
            filterparamodel.objMIP003 = filterparamodel.objMIP004.MIP003;
            filterparamodel.HeaderId = filterparamodel.objMIP004.HeaderId;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(filterparamodel.objMIP003.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == filterparamodel.objMIP003.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == filterparamodel.objMIP003.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.DocNoDesc = Manager.GetCategoryOnlyDescription(filterparamodel.objMIP003.DocNo, filterparamodel.objMIP003.BU, filterparamodel.objMIP003.Location, "MIP_DocNo");
            ViewBag.MIPTypeDesc = Manager.GetCategoryOnlyDescription(filterparamodel.objMIP003.MIPType, filterparamodel.objMIP003.BU, filterparamodel.objMIP003.Location, "MIP_Type");

            return PartialView("~/Views/Shared/MIPPartials/_LoadFilteredDataPartial.cshtml", filterparamodel);
        }
        [HttpPost]
        public ActionResult MIPLoadRequestFilteredDataPartial(FilterReqMIPModel requestfilterparamodel)
        {
            requestfilterparamodel.objMIP004 = db.MIP004.FirstOrDefault(f => f.LineId == requestfilterparamodel.LineId);
            requestfilterparamodel.objMIP003 = requestfilterparamodel.objMIP004.MIP003;
            requestfilterparamodel.HeaderId = requestfilterparamodel.objMIP004.HeaderId;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(requestfilterparamodel.objMIP003.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == requestfilterparamodel.objMIP003.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == requestfilterparamodel.objMIP003.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.DocNoDesc = Manager.GetCategoryOnlyDescription(requestfilterparamodel.objMIP003.DocNo, requestfilterparamodel.objMIP003.BU, requestfilterparamodel.objMIP003.Location, "MIP_DocNo");
            ViewBag.MIPTypeDesc = Manager.GetCategoryOnlyDescription(requestfilterparamodel.objMIP003.MIPType, requestfilterparamodel.objMIP003.BU, requestfilterparamodel.objMIP003.Location, "MIP_Type");

            return PartialView("~/Views/Shared/MIPPartials/_LoadRequestFilteredDataPartial.cshtml", requestfilterparamodel);
        }
        [HttpPost]
        public JsonResult MIPLoadFilteredOfferData(IEMQS.Models.JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "m3.BU", "m3.Location");
                whereCondition += "and m3.headerId =" + param.Headerid;
                //search Condition 
                string[] columnName = { "QualityProject", "(m3.Project+'-'+com1.t_dsca)", "(LTRIM(RTRIM(m3.Location))+'-'+LTRIM(RTRIM(lcom2.t_desc)))", "(LTRIM(RTRIM(m3.BU))+'-'+LTRIM(RTRIM(bcom2.t_desc)))" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_MIP_HEADER_GET_LINES_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstResult.Select(i => i.TotalCount).FirstOrDefault();

                var data = from uc in lstResult
                           select new[] {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Convert.ToString(uc.OperationNo),
                               uc.ActivityDescription,
                               uc.RefDocument,
                               uc.RefDocRevNo,
                               Manager.GetCategoryOnlyDescription(uc.PIA,uc.BU.Split('-')[0],uc.Location.Split('-')[0] ,"PIA" ),
                               Manager.GetCategoryOnlyDescription(uc.IAgencyLNT,uc.BU.Split('-')[0], uc.Location.Split('-')[0],"MIP Inspection Agency L&T" ),
                               Manager.GetCategoryOnlyDescription(uc.IAgencyVVPT,uc.BU.Split('-')[0],uc.Location.Split('-')[0],"MIP Inspection Agency VVPT"),
                               Manager.GetCategoryOnlyDescription(uc.IAgencySRO,uc.BU.Split('-')[0], uc.Location.Split('-')[0]  ,"MIP Inspection Agency SRO"),
                               uc.RecordsDocuments,
                               uc.Observations,
                               "R"+uc.LineRevNo,
                               "R"+uc.MIPRevNo,
                               uc.InspectionStatus
                    };

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult MIPLoadRequestFilteredOfferData(IEMQS.Models.JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                strWhereCondition += " 1=1 and RequestNo=" + Convert.ToInt32(param.Headerid);
                //search Condition 
                string[] columnName = { "RequestId",
                                        "QualityProject",
                                        "Project",
                                        "BU",
                                        "Location",
                                        "DocNo",
                                        "MIPType",
                                        "RevNo",
                                        "ComponentNo",
                                        "DrawingNo",
                                        "RawMaterialGrade",
                                        "PlateNo",
                                        "IterationNo",
                                        "RequestNo",
                                        "RequestNoSequence",
                                        "OperationNo",
                                        "LineRevNo",
                                        "MIPRevNo",
                                        "ActivityDescription",
                                        "RefDocument",
                                        "RefDocRevNo",
                                        "PIA",
                                        "IAgencyLNT",
                                        "IAgencyVVPT",
                                        "IAgencySRO",
                                        "RecordsDocuments",
                                        "Observations",
                                        "CreatedBy",
                                        "CreatedOn",
                                        "EditedBy",
                                        "EditedOn",
                                        "TestResult",
                                        "InspectedBy",
                                        "InspectedOn",
                                        "QCRemarks",
                                        "OfferedBy",
                                        "OfferedOn",
                                        };
                strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstResult = db.MIP050.SqlQuery("SELECT * FROM MIP050 WHERE " + strWhereCondition + " " + strSortOrder).ToList();// SP_MIP_GET_ATTEND_INSPECTION_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                var TotalCount = lstResult.Count;
                lstResult = lstResult.Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                                uc.RequestId+"",
                                uc.RequestNoSequence+"",
                                uc.QualityProject,
                                uc.Project,
                                uc.BU,
                                uc.Location,
                                Manager.GetCategoryOnlyDescription(uc.DocNo,uc.BU,uc.Location  ,"MIP_DocNo" ),
                                Manager.GetCategoryOnlyDescription(uc.MIPType,uc.BU,uc.Location  ,"MIP_Type" ),
                                uc.RevNo+"",
                                uc.ComponentNo,
                                uc.DrawingNo,
                                uc.RawMaterialGrade,
                                uc.PlateNo,
                                uc.IterationNo +"",
                                uc.OperationNo+"",
                                uc.LineRevNo+"",
                                uc.MIPRevNo+"",
                                uc.ActivityDescription,
                                uc.RefDocument,
                                uc.RefDocRevNo,
                                uc.PIA,
                                uc.IAgencyLNT,
                                uc.IAgencyVVPT,
                                uc.IAgencySRO,
                                uc.RecordsDocuments,
                                uc.Observations,
                                uc.CreatedBy,
                                Convert.ToString(uc.CreatedOn),
                                uc.EditedBy,
                                Convert.ToString(uc.EditedOn),
                                uc.TestResult,
                                uc.OfferedBy,
                                Convert.ToString(uc.OfferedOn),
                                uc.QCRemarks,
                                uc.InspectedBy,
                                Convert.ToString(uc.InspectedOn)
                              }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = TotalCount,
                    iTotalDisplayRecords = TotalCount,
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public class FilterReqMIPModel
        {
            public int? LineId { get; set; }
            public int? HeaderId { get; set; }
            public int tableID { get; set; }
            public MIP004 objMIP004 { get; set; }
            public MIP003 objMIP003 { get; set; }

        }
        #endregion
        #region
        [HttpPost]
        public ActionResult GetAuthorizedUsers(string term)
        {
            string[] lstNotAllowRoles = { clsImplementationEnum.UserRoleName.ITA1.GetStringValue(), clsImplementationEnum.UserRoleName.ITA2.GetStringValue(), clsImplementationEnum.UserRoleName.ITA3.GetStringValue() };
            List<CategoryData> lstUsers = new List<CategoryData>();

            List<string> lstNotAllowUsers = (from ath1 in db.ATH001
                                             join ath4 in db.ATH004 on ath1.Role equals ath4.Id
                                             where lstNotAllowRoles.Contains(ath4.Role)
                                             select ath1.Employee).Distinct().ToList();

            if (!string.IsNullOrWhiteSpace(term))
            {

                lstUsers = (from ath1 in db.ATH001
                            join com3 in db.COM003 on ath1.Employee equals com3.t_psno
                            where com3.t_actv == 1
                                  && !lstNotAllowUsers.Contains(ath1.Employee)
                                  && ((ath1.Employee + " - " + com3.t_name).Contains(term)
                                  || ath1.Employee.Contains(term)
                                  || com3.t_name.Contains(term))
                            select
                             new CategoryData
                             {
                                 id = com3.t_psno,
                                 text = com3.t_psno + " - " + com3.t_name
                             }).Distinct().ToList();
            }
            else
            {
                lstUsers = (from ath1 in db.ATH001
                            join com3 in db.COM003 on ath1.Employee equals com3.t_psno
                            where com3.t_actv == 1 && !lstNotAllowUsers.Contains(ath1.Employee)
                            select
                             new CategoryData
                             {
                                 id = com3.t_psno,
                                 text = com3.t_psno + " - " + com3.t_name
                             }).Distinct().Take(10).ToList();
            }
            return Json(lstUsers, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region CFAR
        [HttpPost]
        public ActionResult GetBUwiseApproverResult(string term, string role, string selectedbu)
        {
            string[] roles = role.Split(',').ToArray();
            List<ApproverModel> lstApprovers = GetApproverListByUser(objClsLoginInfo.UserName, term, roles, selectedbu).Distinct().ToList();

            var lstEmployee = (from lst in lstApprovers
                               select new CategoryData { id = lst.Code, text = lst.Name }).ToList();
            return Json(lstEmployee, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetLocationwiseApproverResult(string term, string role, string location)
        {
            string[] roles = role.Split(',').ToArray();
            List<CategoryData> lstApproverList = new List<CategoryData>();

            int[] RoleId = db.ATH004.Where(x => role.Contains(x.Role)).Select(x => x.Id).ToArray();
            lstApproverList = db.ATH001.
                  Join(db.COM003, x => x.Employee, y => y.t_psno,
                  (x, y) => new { x, y })
                  .Where(z => z.y.t_actv == 1 && z.x.Location.Contains(location) && RoleId.Contains(z.x.Role) && (z.x.Employee.Contains(term) || z.y.t_name.Contains(term) || (z.x.Employee + " - " + z.y.t_name).Contains(term)))
                  .Select(z => new CategoryData
                  {
                      id = z.x.Employee,
                      text = z.x.Employee + " - " + z.y.t_name
                  }).Distinct().ToList();

            if (lstApproverList.Count() > 0)
            {
                lstApproverList = lstApproverList.Select(x => new CategoryData { id = x.id, text = x.text }).Distinct().ToList();
            }

            return Json(lstApproverList, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetRoleApproverResult(string term, string role)
        {
            string[] roles = role.Split(',').ToArray();
            List<ApproverModel> lstApprovers = GetApproverListByUser(objClsLoginInfo.UserName, term, roles).Distinct().ToList();
            var lstEmployee = (from lst in lstApprovers
                               select new CategoryData { id = lst.Code, text = lst.Name }).ToList();
            return Json(lstEmployee, JsonRequestBehavior.AllowGet);
        }

        public List<ApproverModel> GetApproverListByUser(string currentEmployee, string terms, string[] roles, string selectedbu = "")
        {
            List<ApproverModel> lstApproverModel = new List<ApproverModel>();
            string[] role = roles;

            int[] RoleId = db.ATH004.Where(x => role.Contains(x.Role)).Select(x => x.Id).ToArray();
            if (!string.IsNullOrWhiteSpace(selectedbu))
            {
                lstApproverModel = db.ATH001.
                    Join(db.COM003, x => x.Employee, y => y.t_psno,
                    (x, y) => new { x, y })
                    .Where(z => z.y.t_actv == 1 && z.x.BU.Contains(selectedbu) && RoleId.Contains(z.x.Role) && (z.x.Employee.Contains(terms) || z.y.t_name.Contains(terms) || (z.x.Employee + " - " + z.y.t_name).Contains(terms)))
                    .Select(z => new ApproverModel
                    {
                        Code = z.x.Employee,
                        Name = z.x.Employee + " - " + z.y.t_name
                    }).Distinct().ToList();
            }
            else
            {
                lstApproverModel = db.ATH001.
                        Join(db.COM003, x => x.Employee, y => y.t_psno,
                        (x, y) => new { x, y })
                        .Where(z => z.y.t_actv == 1 && RoleId.Contains(z.x.Role) && (z.x.Employee.Contains(terms) || z.y.t_name.Contains(terms) || (z.x.Employee + " - " + z.y.t_name).Contains(terms)))
                        .Select(z => new ApproverModel
                        {
                            Code = z.x.Employee,
                            Name = z.x.Employee + " - " + z.y.t_name
                        }).Distinct().ToList();
            }
            if (lstApproverModel.Count() > 0)
            {
                lstApproverModel = lstApproverModel.Select(x => new ApproverModel { Code = x.Code, Name = x.Name }).Distinct().ToList();
            }

            return lstApproverModel;
        }

        [HttpPost]
        public ActionResult GetAllAuthorisedQualityProject(string term)
        {
            try
            {
                var lstProjectByUser = Manager.getProjectsByUser(objClsLoginInfo.UserName.Trim());
                var item = (from lst in lstProjectByUser
                            join qms10 in db.QMS010 on lst.projectCode equals qms10.Project
                            where (term == "" || qms10.QualityProject.ToLower().Trim().Contains(term.ToLower().Trim()))
                            orderby qms10.QualityProject
                            select new { Value = qms10.QualityProject, Text = qms10.QualityProject }).Distinct().Take(10).ToList();

                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Fixture/FKMS MaterialCalculations
        [HttpGet]
        public ActionResult GetMaterialFromCategory(int Id)
        {
            List<BULocWiseCategoryModel> lstMaterial = new List<BULocWiseCategoryModel>();
            if (Id == 1)
            {
                var data = from q in db.FXR006.ToList()
                           select new { name = q.Type + "-" + q.Size };

                lstMaterial = data.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.name, CatID = x.name }).ToList();
            }
            else if (Id == 2)
            {
                var data = from q in db.FXR005.ToList()
                           select new { name = q.Bore + "-" + q.Schedule };

                lstMaterial = data.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.name, CatID = x.name }).ToList();
            }
            return Json(lstMaterial, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetWeightValue(string material, string type)
        {
            var mType = material.Split('-')[0];
            var mSize = material.Split('-')[1];
            decimal weight = 0;
            if (type.ToLower() == "structural")
            {
                weight = Convert.ToDecimal(db.FXR006.Where(x => x.Type == mType && x.Size == mSize).FirstOrDefault().Weight);
            }
            else
            {
                weight = Convert.ToDecimal(db.FXR005.Where(x => x.Bore == mType && x.Schedule == mSize).FirstOrDefault().Weight);
            }

            return Json(weight, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDensityValue(string name)
        {
            var density = db.FXR003.Where(x => x.MaterialType == name).FirstOrDefault().Density;
            return Json(density, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string getFXRData(string headerID, string lineID, string processType)
        {
            string val = "";
            int LineID = Int32.Parse(lineID);
            int HeaderID = Int32.Parse(headerID);
            if (processType.ToLower() == "fixture")
            {
                FXR002 objFXR002 = db.FXR002.Where(x => x.LineId == LineID && x.HeaderId == HeaderID).FirstOrDefault();
                if (objFXR002 != null)
                {
                    string fixtureName = objFXR002.FixtureName;
                    string project = objFXR002.Project;
                    FXR002 obj1FXR002 = db.FXR002.Where(x => x.FixtureName == fixtureName && x.Project == project && x.QtyofFixture != null).FirstOrDefault();
                    if (obj1FXR002 != null)
                    {
                        val = obj1FXR002.QtyofFixture.ToString();
                    }
                    else
                    {
                        val = "";
                    }
                }
                else
                {
                    val = "";
                }
            }
            else
            {
                //Code Comment because of remove FKM111 reference from FKM106
                //FKM111 objFKM111 = db.FKM111.Where(x => x.RefId == HeaderID && x.QtyofFixture != null).FirstOrDefault();
                //if (objFKM111 != null)
                //{
                //    val = objFKM111.QtyofFixture.ToString();
                //}
                //else
                //{
                //    val = "";
                //}
            }
            return val;
        }

        [HttpPost]
        public ActionResult GetAllocatedContractorName(string term, int take = 0)
        {
            var lstBPName = db.SP_GETBPNAME(term).Select(s => new { Value = s.BPID, Text = s.BPID + " - " + s.NAME });
            if (take > 0)
                lstBPName = lstBPName.Take(take);
            return Json(lstBPName, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Generate PDF from SSRS report

        [HttpPost]
        public string GeneratePDFfromSSRSreport(string fullPath, string ReportPath, List<SSRSParam> reportParams, DownloadReportFormat format = DownloadReportFormat.pdf)
        {
            string reportFilename = string.Empty;
            try
            {
                Warning[] warnings = null;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = string.Empty;

                var ReportParams = new List<ReportParameter>();
                var param = new Dictionary<string, string[]>();
                foreach (var rp in reportParams)
                {
                    if (param.Any(q => q.Key.Equals(rp.Param)))
                    {
                        var oldValue = param.FirstOrDefault(q => q.Key.Equals(rp.Param)).Value.ToList();
                        param.Remove(rp.Param);
                        oldValue.Add(rp.Value);
                        param.Add(rp.Param, oldValue.ToArray());
                    }
                    else
                        param.Add(rp.Param, new string[] { rp.Value });
                }
                foreach (var p in param)
                    ReportParams.Add(new ReportParameter(p.Key, p.Value));

                // Setup the report viewer object and get the array of bytes
                var viewer = new ReportViewer();
                viewer = GetReportServerCreadential(viewer, ReportPath, ReportParams);
                string fileformat = (format == DownloadReportFormat.pdf ? "PDF" : "EXCELOPENXML");
                byte[] bytes = viewer.ServerReport.Render(fileformat, null, out mimeType, out encoding, out extension,
                              out streamIds, out warnings);

                //create file
                if (Directory.Exists(Path.GetDirectoryName(fullPath)))
                {
                    System.IO.File.Delete(fullPath);
                }
                using (FileStream fs = new FileStream(fullPath, FileMode.Create))
                { fs.Write(bytes, 0, bytes.Length); }

                reportFilename = fullPath;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return reportFilename;
        }

        [HttpPost]
        public byte[] GetPDFBytesfromSSRSreport(string ReportPath, List<SSRSParam> reportParams, DownloadReportFormat format = DownloadReportFormat.pdf)
        {
            try
            {
                Warning[] warnings = null;
                string[] streamIds;
                string mimeType = string.Empty;
                string encoding = string.Empty;
                string extension = string.Empty;

                var ReportParams = new List<ReportParameter>();
                var param = new Dictionary<string, string[]>();
                foreach (var rp in reportParams)
                {
                    if (param.Any(q => q.Key.Equals(rp.Param)))
                    {
                        var oldValue = param.FirstOrDefault(q => q.Key.Equals(rp.Param)).Value.ToList();
                        param.Remove(rp.Param);
                        oldValue.Add(rp.Value);
                        param.Add(rp.Param, oldValue.ToArray());
                    }
                    else
                        param.Add(rp.Param, new string[] { rp.Value });
                }
                foreach (var p in param)
                    ReportParams.Add(new ReportParameter(p.Key, p.Value));

                // Setup the report viewer object and get the array of bytes
                var viewer = new ReportViewer();
                viewer = GetReportServerCreadential(viewer, ReportPath, ReportParams);
                string fileformat = (format == DownloadReportFormat.pdf ? "PDF" : "EXCELOPENXML");
                byte[] bytes = viewer.ServerReport.Render(fileformat, null, out mimeType, out encoding, out extension,
                              out streamIds, out warnings);

                return bytes;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return null; ;
        }




        public ActionResult ViewReportAndUploadToNode(string serverRelativeUrlPath, List<SSRSParam> ReportParameters, bool ShowAttachment = false, DownloadReportFormat format = DownloadReportFormat.pdf, string NewFileName = "", string PathCreate = "")
        {
            var host = System.Configuration.ConfigurationManager.AppSettings["Applicaiton_URL"];
            try
            {
                if (ReportParameters == null)
                {
                    ReportParameters = new List<SSRSParam>();
                }
                var fileName = DateTime.Now.ToString("yyyyMMddHHmmsss")  + NewFileName + (format == DownloadReportFormat.pdf ? ".pdf" : ".xlsx");

                if (!Directory.Exists(PathCreate))
                {
                    Directory.CreateDirectory(PathCreate);
                }
                string fullPath = Path.Combine(PathCreate, fileName);
                if (ShowAttachment)
                {
                    ReportParameters.Add(new SSRSParam("Host", host));
                }

                GeneratePDFfromSSRSreport(fullPath, serverRelativeUrlPath, ReportParameters, format);

                return Json(new { fileName = fileName, fullPath = fullPath, format = format, errorMessage = "" });
            }
            catch (Exception ex)
            {
                return Json(new { fileName = "", format = format, errorMessage = ex.Message });
            }

        }




        [HttpPost]
        public string GenerateImagefromSSRSreport(string fullPath, string ReportPath, List<SSRSParam> reportParams, DownloadReportFormat format = DownloadReportFormat.pdf)
        {
            string reportFilename = string.Empty;
            try
            {
                Warning[] warnings = null;
                string[] streamIds;
                string mimeType = "image/jpeg";
                string encoding = string.Empty;
                string extension = string.Empty;

                var ReportParams = new List<ReportParameter>();
                var param = new Dictionary<string, string[]>();
                foreach (var rp in reportParams)
                {
                    if (param.Any(q => q.Key.Equals(rp.Param)))
                    {
                        var oldValue = param.FirstOrDefault(q => q.Key.Equals(rp.Param)).Value.ToList();
                        param.Remove(rp.Param);
                        oldValue.Add(rp.Value);
                        param.Add(rp.Param, oldValue.ToArray());
                    }
                    else
                        param.Add(rp.Param, new string[] { rp.Value });
                }
                foreach (var p in param)
                    ReportParams.Add(new ReportParameter(p.Key, p.Value));

                // Setup the report viewer object and get the array of bytes
                var viewer = new ReportViewer();
                viewer = GetReportServerCreadential(viewer, ReportPath, ReportParams);
                byte[] bytes = viewer.ServerReport.Render("IMAGE", null, out mimeType, out encoding, out extension,
                              out streamIds, out warnings);

                //create file
                if (Directory.Exists(Path.GetDirectoryName(fullPath)))
                {
                    System.IO.File.Delete(fullPath);
                }
                if (!Directory.Exists(fullPath))
                {
                    Directory.CreateDirectory(fullPath);
                }
                else
                {
                    DirectoryInfo directoryInfo = new DirectoryInfo(fullPath);
                    foreach (FileInfo file in directoryInfo.GetFiles())
                    {
                        file.Delete();
                    }
                    foreach (DirectoryInfo dir in directoryInfo.GetDirectories())
                    {
                        dir.Delete(true);
                    }
                }

                using (FileStream fs = new FileStream(fullPath, FileMode.Create))
                { fs.Write(bytes, 0, bytes.Length); }

                reportFilename = fullPath;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return reportFilename;
        }

        [HttpPost]
        public string GenerateUploadImagefromSSRSreport(string localpath, string ReportPath, List<SSRSParam> reportParams)
        {
            try
            {
                Warning[] warnings = null;
                string[] streamIds;
                string mimeType = "image/jpeg";
                string encoding = string.Empty;
                string extension = string.Empty;

                var ReportParams = new List<ReportParameter>();
                var param = new Dictionary<string, string[]>();
                foreach (var rp in reportParams)
                {
                    if (param.Any(q => q.Key.Equals(rp.Param)))
                    {
                        var oldValue = param.FirstOrDefault(q => q.Key.Equals(rp.Param)).Value.ToList();
                        param.Remove(rp.Param);
                        oldValue.Add(rp.Value);
                        param.Add(rp.Param, oldValue.ToArray());
                    }
                    else
                        param.Add(rp.Param, new string[] { rp.Value });
                }
                foreach (var p in param)
                    ReportParams.Add(new ReportParameter(p.Key, p.Value));

                // Setup the report viewer object and get the array of bytes
                var viewer = new ReportViewer();
                viewer = GetReportServerCreadential(viewer, ReportPath, ReportParams);
                byte[] bytes = viewer.ServerReport.Render("IMAGE", null, out mimeType, out encoding, out extension,
                              out streamIds, out warnings);


                if (Directory.Exists(Path.GetDirectoryName(localpath)))
                {
                    System.IO.File.Delete(localpath);
                }
                using (FileStream fs = new FileStream(localpath, FileMode.Create))
                { fs.Write(bytes, 0, bytes.Length); }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return localpath;
        }

        public ReportViewer GetReportServerCreadential(ReportViewer viewer, string ReportPath, List<ReportParameter> ReportParams)
        {
            var credentials = new IEMQS.Areas.Utility.Controllers.WindowsImpersonationCredentials(System.Configuration.ConfigurationManager.AppSettings["File_Upload_UserID"], System.Configuration.ConfigurationManager.AppSettings["File_Upload_Password"], System.Configuration.ConfigurationManager.AppSettings["NetworkDomain"]);

            viewer.ProcessingMode = ProcessingMode.Remote;
            viewer.ServerReport.ReportServerCredentials = credentials;
            viewer.ServerReport.ReportPath = System.Configuration.ConfigurationManager.AppSettings["SSRS_Report_Folder"] + ReportPath;
            viewer.ServerReport.ReportServerUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["SSRS_Server_URL"]);
            viewer.ServerReport.SetParameters((List<ReportParameter>)ReportParams);

            return viewer;
        }
        #endregion

        #region --Post Validations

        [HttpPost]
        public ActionResult CheckSubmit(int headerId, string table, string action, bool isapprover)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.StatusChanged;
                var objtable = db.Database.SqlQuery<TableViewModel>("SELECT HeaderId,Status FROM " + table + " where HeaderId = " + headerId + "").FirstOrDefault();

                if (objtable != null)
                {
                    if (action.ToLower() == clsImplementationEnum.ButtonStatus.SendForApproval.GetStringValue().ToLower() || action.ToLower() == clsImplementationEnum.ButtonStatus.Save.GetStringValue().ToLower() || action.ToLower() == clsImplementationEnum.ButtonStatus.Submit.GetStringValue().ToLower() || action.ToLower() == clsImplementationEnum.ButtonStatus.Fetch.GetStringValue().ToLower() || action.ToLower() == clsImplementationEnum.ButtonStatus.CRUD.GetStringValue().ToLower())
                    {
                        if (isapprover)
                        {
                            if (objtable.Status.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower())
                            {
                                objResponseMsg.Key = true;
                            }
                        }
                        else
                        {
                            if (objtable.Status.ToLower() == clsImplementationEnum.PlanStatus.DRAFT.GetStringValue().ToLower() || objtable.Status.ToLower() == clsImplementationEnum.PlanStatus.Returned.GetStringValue().ToLower())
                            {
                                objResponseMsg.Key = true;
                            }
                        }
                    }
                    else if (action.ToLower() == clsImplementationEnum.ButtonStatus.Retract.GetStringValue().ToLower() || action.ToLower() == clsImplementationEnum.ButtonStatus.Approve.GetStringValue().ToLower() || action.ToLower() == clsImplementationEnum.ButtonStatus.Return.GetStringValue().ToLower())
                    {
                        if (objtable.Status.ToLower() == clsImplementationEnum.PlanStatus.SendForApproval.GetStringValue().ToLower())
                        {
                            objResponseMsg.Key = true;
                        }
                    }
                    else if (action.ToLower() == clsImplementationEnum.ButtonStatus.Revoke.GetStringValue().ToLower() || action.ToLower() == clsImplementationEnum.ButtonStatus.Revise.GetStringValue().ToLower())
                    {
                        if (objtable.Status.ToLower() == clsImplementationEnum.PlanStatus.Approved.GetStringValue().ToLower())
                        {
                            objResponseMsg.Key = true;
                        }
                    }
                }
                else
                {
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.InvalidHeaderId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public class TableViewModel
        {
            public int HeaderId { get; set; }
            public string Status { get; set; }
        }

        #endregion

        #region Scroll Status

        [HttpPost]
        public ActionResult GetStatus()
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            string specialStage = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.Announcement.GetStringValue()));

            if (String.IsNullOrEmpty(specialStage))
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "";
            }
            else
            {
                objResponseMsg.Key = true;
                objResponseMsg.Value = specialStage;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion 

        [HttpPost]
        public ActionResult UpdateAnyTable(string TableName, string PrimaryId, string PrimaryColumnName, string UpdateColumnName, string UpdateColumnValue, bool SetNullForBlank = true)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //if(SetNullForBlank && UpdateColumnValue == "")
                //{
                //    UpdateColumnValue = null;
                //}
                int i = db.SP_COMMON_TABLE_UPDATE(TableName, Convert.ToInt32(PrimaryId), PrimaryColumnName, UpdateColumnName, UpdateColumnValue, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occured, Please try again";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet); ;
        }

        [HttpPost]
        public ActionResult GetAllProjectBUWise(string term, string BU)
        {
            List<AutoCompleteModel> lstProject = new List<AutoCompleteModel>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProject = (from cm001 in db.COM001
                              where cm001.t_entu == BU && (cm001.t_cprj.Contains(term) || cm001.t_dsca.Contains(term))
                              select new AutoCompleteModel { Value = cm001.t_cprj, Text = cm001.t_cprj + "-" + cm001.t_dsca }).ToList();
            }
            else
            {
                lstProject = (from cm001 in db.COM001
                              where cm001.t_entu == BU
                              select new AutoCompleteModel { Value = cm001.t_cprj, Text = cm001.t_cprj + "-" + cm001.t_dsca }).Take(10).ToList();
            }
            return Json(lstProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult addFavoritesProcess(string strpath)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            string[] strArr = null;
            //char[] splitchar = { '/' };
            strArr = strpath.Split('/');
            string area = string.Empty;
            string controller = string.Empty;
            string action = string.Empty;
            try

            {
                if (strArr.Length > 0)
                {
                    area = strArr[0].Trim();
                }
                if (strArr.Length > 1)
                {
                    controller = strArr[1].Trim();
                }
                if (strArr.Length > 2)
                {
                    action = strArr[2].Trim();
                }
                if (action == string.Empty) { action = "Index"; }

                ATH025 objATH025 = new ATH025();
                var urlList = (from ATH002 in db.ATH002
                               where ATH002.Area.ToLower() == area.ToLower() && ATH002.Controller.ToLower() == controller.ToLower() && ATH002.Action.ToLower() == action.ToLower()
                               select new { id = ATH002.Id }
                               ).FirstOrDefault();

                if (urlList != null)
                {
                    var processIdExist = db.ATH025.Any(x => x.ProcessId == urlList.id && x.Employee.Trim() == objClsLoginInfo.UserName.Trim());

                    int? newOrder = Convert.ToInt32(maxOrderNo(objClsLoginInfo.UserName.Trim()));
                    if (!processIdExist)
                    {
                        if (newOrder <= 16)
                        {
                            objATH025.Employee = objClsLoginInfo.UserName;
                            objATH025.ProcessId = urlList.id;
                            objATH025.Order = newOrder;
                            objATH025.CreatedBy = objClsLoginInfo.UserName;
                            objATH025.CreatedOn = DateTime.Now;
                            db.ATH025.Add(objATH025);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.HeaderId = objATH025.Id;
                        }
                        else
                        {
                            objATH025 = db.ATH025.Where(x => x.Employee.Trim() == objClsLoginInfo.UserName.Trim()).OrderBy(x => x.Order).FirstOrDefault();
                            int? deletedorderno = objATH025.Order;
                            var lstNextOrder = db.ATH025.Where(x => x.Employee.Trim() == objClsLoginInfo.UserName.Trim() && x.Order > deletedorderno).ToList();
                            db.ATH025.Remove(objATH025);
                            db.SaveChanges();
                            foreach (var item in lstNextOrder)
                            {
                                ATH025 updateATH025 = db.ATH025.Where(x => x.Id == item.Id).FirstOrDefault();
                                updateATH025.Order = updateATH025.Order - 1;
                                db.SaveChanges();
                            }
                            objATH025.Employee = objClsLoginInfo.UserName;
                            objATH025.ProcessId = urlList.id;
                            objATH025.Order = Convert.ToInt32(maxOrderNo(objClsLoginInfo.UserName.Trim()));
                            objATH025.CreatedBy = objClsLoginInfo.UserName;
                            objATH025.CreatedOn = DateTime.Now;
                            db.ATH025.Add(objATH025);
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.HeaderId = objATH025.Id;

                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string maxOrderNo(string employee)
        {
            int? i = 1;
            var order = db.ATH025.Where(x => x.Employee.Trim() == objClsLoginInfo.UserName.Trim()).OrderByDescending(x => x.Order).FirstOrDefault();

            if (order != null)
            {
                i = order.Order + 1;
            }
            string orderNo = i.ToString();
            return orderNo;
        }
        [HttpPost]
        public ActionResult Get_DPS_DrawingDocumentNo(string term, string Project)
        {
            SqlConnection objConn = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["DPS"]));
            List<AutoCompleteModel> lstDrawingNo = new List<AutoCompleteModel>();
            if (!string.IsNullOrEmpty(Project))
            {
                try
                {

                    objConn.Open();
                    SqlCommand objCmd = new SqlCommand();
                    objCmd.Connection = objConn;
                    objCmd.CommandType = CommandType.Text;
                    objCmd.Parameters.AddWithValue("@projNo", Project.Trim());
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    DataTable dataTable = new DataTable();

                    if (string.IsNullOrEmpty(term))
                    {
                        objCmd.CommandText = "SELECT distinct TOP 10 documentno FROM DPSMaster WHERE projectno = @projNo";
                        dataAdapt.SelectCommand = objCmd;
                        dataAdapt.Fill(dataTable);

                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            lstDrawingNo = (from DataRow row in dataTable.Rows
                                            select new AutoCompleteModel
                                            {
                                                Text = Convert.ToString(row["documentno"]),
                                                Value = Convert.ToString(row["documentno"])
                                            }).ToList();
                        }
                    }
                    else
                    {
                        objCmd.Parameters.AddWithValue("@term", term);
                        objCmd.CommandText = "SELECT distinct documentno FROM DPSMaster WHERE projectno = @projNo AND documentno like '%' + @term + '%' ";
                        dataAdapt.SelectCommand = objCmd;
                        dataAdapt.Fill(dataTable);

                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            lstDrawingNo = (from DataRow row in dataTable.Rows
                                            select new AutoCompleteModel
                                            {
                                                Text = Convert.ToString(row["documentno"]),
                                                Value = Convert.ToString(row["documentno"])
                                            }).ToList();
                        }
                    }
                    #region Fetch Doc No from PAM -DSS
                    List<AutoCompleteModel> lst_DIN_DSS_DocNo = new List<AutoCompleteModel>();
                    var objWelderDtl = db.SP_TCP_FETCH_DRAWINGNO(Project).Where(i => (term == "" || i.Contains(term)));
                    if (objWelderDtl != null)
                    {
                        lst_DIN_DSS_DocNo = objWelderDtl.Select(a => new AutoCompleteModel { Text = a.ToString(), Value = a.ToString() }).ToList();
                        if (lst_DIN_DSS_DocNo.Count > 0)
                        {
                            lstDrawingNo.AddRange(lst_DIN_DSS_DocNo);
                        }
                    }
                    #endregion
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                finally
                {
                    if (objConn.State == ConnectionState.Open)
                        objConn.Close();
                }

            }
            return Json(lstDrawingNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_DPS_DrawingDocumentNo_By_Project(string term, string Project)
        {
            SqlConnection objConn = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["DPS"]));
            List<AutoCompleteModel> lstDrawingNo = new List<AutoCompleteModel>();
            if (!string.IsNullOrEmpty(Project))
            {
                try
                {

                    objConn.Open();
                    SqlCommand objCmd = new SqlCommand();
                    objCmd.Connection = objConn;
                    objCmd.CommandType = CommandType.Text;
                    objCmd.Parameters.AddWithValue("@projNo", Project.Trim());
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    DataTable dataTable = new DataTable();

                    if (string.IsNullOrEmpty(term))
                    {
                        objCmd.CommandText = "SELECT distinct TOP 10 documentno FROM DPSMaster WHERE projectno = @projNo";
                        dataAdapt.SelectCommand = objCmd;
                        dataAdapt.Fill(dataTable);

                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            lstDrawingNo = (from DataRow row in dataTable.Rows
                                            select new AutoCompleteModel
                                            {
                                                Text = Convert.ToString(row["documentno"]),
                                                Value = Convert.ToString(row["documentno"])
                                            }).ToList();
                        }
                    }
                    else
                    {
                        objCmd.Parameters.AddWithValue("@term", term);
                        objCmd.CommandText = "SELECT distinct documentno FROM DPSMaster WHERE projectno = @projNo AND documentno like '%' + @term + '%' ";
                        dataAdapt.SelectCommand = objCmd;
                        dataAdapt.Fill(dataTable);

                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            lstDrawingNo = (from DataRow row in dataTable.Rows
                                            select new AutoCompleteModel
                                            {
                                                Text = Convert.ToString(row["documentno"]),
                                                Value = Convert.ToString(row["documentno"])
                                            }).ToList();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                finally
                {
                    if (objConn.State == ConnectionState.Open)
                        objConn.Close();
                }

            }
            return Json(lstDrawingNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Get_Task_Unique_Id(string term, string Project, string DeleverTo)
        {
            SqlConnection objConn = new SqlConnection(Convert.ToString(ConfigurationManager.ConnectionStrings["LNConcerto"]));
            List<TaskAutoCompleteModel> lstDrawingNo = new List<TaskAutoCompleteModel>();
            if (!string.IsNullOrEmpty(Project))
            {
                try
                {

                    objConn.Open();
                    SqlCommand objCmd = new SqlCommand();
                    objCmd.Connection = objConn;
                    objCmd.CommandType = CommandType.Text;
                    //objCmd.Parameters.AddWithValue("@projNo", Project.Trim());
                    SqlDataAdapter dataAdapt = new SqlDataAdapter();
                    DataTable dataTable = new DataTable();
                    var objFKM113 = db.FKM113.FirstOrDefault(f => f.DeleverTo == DeleverTo);

                    if (objFKM113 != null)
                    {
                        if (string.IsNullOrEmpty(term))
                        {
                            objCmd.CommandText = "select a.TASKUNIQUEID, a.NAME [TaskName] from PROJ_TASK a with(nolock) " +
                                "join PROJECT b with(nolock) on a.PROJECTNAME = b.PROJECTNAME where a.PROJECTNAME =  '" + Project + "' " +
                                " AND a.Contact in ('" + objFKM113.TaskManager.Replace(",", "','") + "')" +
                                " AND a.SUMMARY <> 1 AND a.TASK_TYPE = 0 order by a.TASKID";
                        }
                        else
                        {
                            objCmd.Parameters.AddWithValue("@term", term);
                            objCmd.CommandText = "select a.TASKUNIQUEID, a.NAME [TaskName] from PROJ_TASK a with(nolock) " +
                                "join PROJECT b with(nolock) on a.PROJECTNAME = b.PROJECTNAME where a.PROJECTNAME = @projNo AND a.NAME like '%' + @term + '%'" +
                                " AND a.Contact in ('" + objFKM113.TaskManager.Replace(",", "','") + "')" +
                                " AND a.SUMMARY <> 1 AND a.TASK_TYPE = 0 order by a.TASKID";
                        }
                        dataAdapt.SelectCommand = objCmd;
                        dataAdapt.Fill(dataTable);
                        if (dataTable != null && dataTable.Rows.Count > 0)
                        {
                            lstDrawingNo = (from DataRow row in dataTable.Rows
                                            select new TaskAutoCompleteModel
                                            {
                                                text = Convert.ToString(row["TaskName"]),
                                                id = Convert.ToString(row["TASKUNIQUEID"])
                                            }).ToList();
                        }
                    }
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
                finally
                {
                    if (objConn.State == ConnectionState.Open)
                        objConn.Close();
                }

            }
            return Json(lstDrawingNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetActiveEmployee(string search = "", string param = "")
        {
            var lstEmployee = Manager.GetActiveEmployee();
            List<ddlValue> lst = new List<ddlValue>();
            if (string.IsNullOrWhiteSpace(search))
            {
                lst = lstEmployee.Select(x => new ddlValue { id = x.id.ToString(), text = x.text.ToString() }).Distinct().Take(10).ToList();
            }
            else
            {
                search = search.ToLower();
                lst = lstEmployee.Where(x => x.id.Contains(search) || x.text.ToLower().Contains(search)).Select(x => new ddlValue { id = x.id.ToString(), text = x.text.ToString() }).Distinct().ToList();
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetActiveEmployeName(string search = "", string param = "")
        {
            var lstEmployee = Manager.GetActiveEmployeName();
            List<ddlValue> lst = new List<ddlValue>();
            if (string.IsNullOrWhiteSpace(search))
            {
                lst = lstEmployee.Select(x => new ddlValue { id = x.id.ToString(), text = x.text.ToString() }).Distinct().Take(10).ToList();
            }
            else
            {
                search = search.ToLower();
                lst = lstEmployee.Where(x => x.id.Contains(search) || x.text.ToLower().Contains(search)).Select(x => new ddlValue { id = x.id.ToString(), text = x.text.ToString() }).Distinct().ToList();
            }
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public bool IsPAMApproved(string project)
        {
            try
            {
                return Manager.IsPAMApprovedForProject(project);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return false;
            }
        }

        public ActionResult ViewReport(string serverRelativeUrlPath, List<SSRSParam> ReportParameters, bool ShowAttachment = false, DownloadReportFormat format = DownloadReportFormat.pdf)
        {
            var host = System.Configuration.ConfigurationManager.AppSettings["Applicaiton_URL"];
            try
            {
                if (ReportParameters == null)
                {
                    ReportParameters = new List<SSRSParam>();
                }
                var fileName = DateTime.Now.ToString("yyyyMMddHHmmsss") + (format == DownloadReportFormat.pdf ? ".pdf" : ".xlsx");
                if (!Directory.Exists(Server.MapPath("~/temp")))
                {
                    Directory.CreateDirectory(Server.MapPath("~/temp"));
                }
                string fullPath = Path.Combine(Server.MapPath("~/temp"), fileName);

                if (ShowAttachment)
                {
                    ReportParameters.Add(new SSRSParam("Host", host));
                }

                GeneratePDFfromSSRSreport(fullPath, serverRelativeUrlPath, ReportParameters, format);

                return Json(new { fileName = fileName, format = format, errorMessage = "" });
            }
            catch (Exception ex)
            {
                return Json(new { fileName = "", format = format, errorMessage = ex.Message });
            }

        }

        [HttpGet]
        [DeleteFileAttribute] //Action Filter, it will auto delete the file after download
        public ActionResult DownloadReport(string file, DownloadReportFormat format = DownloadReportFormat.pdf, string downloadName = null)
        {
            //get the temp folder and file path in server
            string fullPath = Path.Combine(Server.MapPath("~/temp"), file);

            if (format == DownloadReportFormat.excel)
            {
                //return the file for download, this is an excel 
                //so I set the file content type to "application/vnd.ms-excel"
                if (!string.IsNullOrEmpty(downloadName))
                    file = downloadName + ".xlsx";
                return File(fullPath, "application/vnd.ms-excel", file);
            }
            else //if (format == DownloadReportFormat.excel)
            {
                //return the file for download, this is an pdf 
                //so I set the file content type to "application/pfd"
                if (!string.IsNullOrEmpty(downloadName))
                    file = downloadName + ".pdf";
                return File(fullPath, "application/pfd", file);
            }
        }

        /// <summary>
        /// This method is to get list of Quality Project based on user and location.
        /// Created By Darshan Dave 10/12/2018
        /// </summary>
        /// <param name="term"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetQualityProjectDetailsAutoComplete(string term)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<Projects> lstQualityProject = new List<Projects>();
            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(term.ToLower())
                                                                 && lstCOMLocation.Contains(i.Location)
                                                                 && lstCOMBU.Contains(i.BU)
                                                               ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                        && lstCOMBU.Contains(i.BU)
                                                        ).Select(i => new Projects { Value = i.QualityProject, projectCode = i.QualityProject }).Take(10).Distinct().ToList();
            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAllAthorizedQualityProjectsByProject(string term, string Project)
        {
            try
            {
                if (Project != null && Project.Trim().Length > 0)
                {
                    var lstProjectByUser = Manager.getProjectsByUser(objClsLoginInfo.UserName.Trim());
                    var item = (from lst in lstProjectByUser
                                join qms10 in db.QMS010 on lst.projectCode equals qms10.Project
                                where (term == "" || qms10.QualityProject.ToLower().Trim().Contains(term.ToLower().Trim()))
                                    && qms10.Project == Project
                                orderby qms10.QualityProject
                                select new { Value = qms10.QualityProject, Text = qms10.QualityProject }).Distinct().Take(10).ToList();

                    return Json(item, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        // Method added for protocol (Seam Validation)
        [HttpPost]
        public bool CheckSeamInSeamList(string seamno = "", string qualityproject = "", string location = "")
        {
            bool flag = db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject);
            return flag;
        }

        public List<string> GetPLMDrawingNumber(string Project)
        {
            List<string> lst = new List<string>();
            bool IsPLMProject = Manager.IsPLMProject(Project);
            if (IsPLMProject)
            {
                #region PLM drawing number code
                var SkipPLM = Convert.ToBoolean(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.SkipPLM.GetStringValue()));

                if (!SkipPLM)
                {
                    IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();
                    try
                    {
                        List<string> lstdrawing = drawref.getDocumentMasterList(Project).ToList();
                        //lstdrawing.ForEach(x =>
                        //{
                        //    x = !string.IsNullOrWhiteSpace(x) ? x.Split('|')[1].ToString() : "";
                        //});

                        //lst = lstdrawing.AsEnumerable().Select(x => x.ToString()).ToList();
                        foreach (string item in lstdrawing)
                        {
                            var data = (!string.IsNullOrWhiteSpace(item) ? item.Split('|')[1].ToString() : "");
                            lst.Add(data);
                        }
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                else
                {
                    lst.Add("Dummy Drawing Number");
                }
                #endregion
            }
            else
            {
                #region fetch from DES
                var contract = db.COM005.Where(i => i.t_sprj == Project).Select(i => i.t_cono).FirstOrDefault();
                using (IEMQSDESEntities db = new IEMQSDESEntities())
                {
                    try
                    {
                        List<string> lstProject = new List<string>();
                        if (string.IsNullOrWhiteSpace(contract))
                        {
                            lstProject.Add(Project);
                        }
                        else
                        {
                            lstProject=db.DES051.Where(x => x.Contract == contract).Select(x => x.Project).ToList();
                        }
                        lst = db.DES059.Where(x => x.DocumentTypeId == 1 && lstProject.Contains(x.Project) && x.DocumentNo != null && x.Status == "Completed" && x.IsLatestRevision == true).Select(x => x.DocumentNo).Distinct().ToList();
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                #endregion
            }
            return lst;
        }

        public string getDepartmentNamebypsno(string dept = "", string psno = "")
        {
            string deptname = string.Empty;
            if (!string.IsNullOrWhiteSpace(psno))
            {
                deptname = Manager.GetDepartmentByPsno(psno);
            }
            if (!string.IsNullOrWhiteSpace(dept))
            {
                deptname = Manager.getDepartmentDescription(dept);
            }
            return deptname;
        }

        public clsHelper.ResponseMsgWithStatus RCAStatus(string action, string module, int? id, string Location = "", string bu = "", string Project = "", string DateOfOccurrence = "", string description = "", string Resp_Person = "", string Resp_Dept = "", string SubToInitBy = "")
        {
            clsHelper.ResponseMsgWithStatus objResponse = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string rcaNumber = string.Empty;
                string rcaStatus = string.Empty;
                string url = string.Empty;
                if (action == "I")
                {
                    string result = "";
                    string RCAConnectionstring = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.RCAConnectionstring.GetStringValue()));
                    SqlConnection connection = new SqlConnection(RCAConnectionstring);
                    connection.Open();
                    using (var Cmd = new SqlCommand("RCA.RCA_SP_AddRCATransaction", connection))
                    {
                        Cmd.CommandType = CommandType.StoredProcedure;
                        Cmd.Parameters.AddWithValue("@Location", Location);
                        Cmd.Parameters.AddWithValue("@BU", bu);
                        Cmd.Parameters.AddWithValue("@Project", Project);
                        Cmd.Parameters.AddWithValue("@DateOfOccurrence", DateOfOccurrence);
                        Cmd.Parameters.AddWithValue("@IncidentType", module);
                        Cmd.Parameters.AddWithValue("@ProblemStatement ", description);
                        Cmd.Parameters.AddWithValue("@ResponsiblePerson ", Resp_Person);
                        Cmd.Parameters.AddWithValue("@ResponsibleDepartment", Resp_Dept);
                        Cmd.Parameters.AddWithValue("@ReferenceNo", id);
                        Cmd.Parameters.AddWithValue("@CreatedBy", objClsLoginInfo.UserName);
                        Cmd.Parameters.AddWithValue("@SubToInitBy", SubToInitBy);
                        Cmd.Parameters.AddWithValue("@FormType", "");
                        System.Data.SqlClient.SqlDataReader sdr = Cmd.ExecuteReader();
                        if (sdr.HasRows)
                        {
                            while (sdr.Read())
                            {
                                result = sdr["Result"].ToString();
                            }
                        }
                        connection.Close();

                        string responseFromServer = result;
                        if (!string.IsNullOrWhiteSpace(responseFromServer))
                        {
                            string[] lst = action == "I" ? responseFromServer.Split('|').ToArray() : responseFromServer.Split('-').ToArray();
                            if (lst.Count() > 0)
                            {
                                rcaNumber = lst[0];
                                rcaStatus = lst[1];
                            }
                        }
                    }

                    var host = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.RCA_Insert_URL.GetStringValue()));
                    url = host + "?Location=" + Location
                                 + "&BU=" + bu
                                 + "&Project=" + Project
                                 + "&DateOfOccurrence=" + DateOfOccurrence
                                 + "&IncidentType=" + module
                                 + "&Problem_Statement=" + description
                                 + "&Resp_Person=" + Resp_Person
                                 + "&Resp_Dept=" + Resp_Dept
                                 + "&Ref_No=" + id
                                 + "&CreatedBy=" + objClsLoginInfo.UserName
                                 + "&SubToInitBy=" + SubToInitBy;
                }
                if (action == "V")
                {
                    var host = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.RCA_ViewStatus_URL.GetStringValue()));
                    url = host + "?Ref_No=" + id
                                 + "&IncidentType=" + module;

                    WebRequest request = WebRequest.Create(url);
                    // If required by the server, set the credentials.
                    request.Credentials = CredentialCache.DefaultCredentials;
                    // Get the response.
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();
                    // Get the stream containing content returned by the server.
                    Stream dataStream = response.GetResponseStream();
                    // Open the stream using a StreamReader for easy access.
                    StreamReader reader = new StreamReader(dataStream);
                    // Read the content.

                    string responseFromServer = reader.ReadToEnd();
                    if (!string.IsNullOrWhiteSpace(responseFromServer))
                    {
                        string[] lst = action == "I" ? responseFromServer.Split('|').ToArray() : responseFromServer.Split('-').ToArray();
                        if (lst.Count() > 0)
                        {
                            rcaNumber = lst[0];
                            rcaStatus = lst[1];
                        }
                    }
                    // Cleanup the streams and the response.
                    reader.Close();
                    dataStream.Close();
                    response.Close();
                }
                objResponse.Key = true;
                objResponse.Value = rcaNumber;
                objResponse.dataValue = rcaStatus;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponse.Key = false;
            }
            return objResponse;
        }
        public List<CategoryData> GetMTRLocation()
        {
            List<CategoryData> listLocation = new List<CategoryData>();
            try
            {
                string rcaStatus = string.Empty;
                string MTRConnectionString = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.MTRConnectionString.GetStringValue()));
                string MTRLocationQuery = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.MTRLocationQuery.GetStringValue()));
                //string LNCompanyId = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNCompanyId.GetStringValue()));
                //string LNLinkedServer = Convert.ToString(Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue()));
                //MTRLocationQuery = MTRLocationQuery.Replace("[LNLinkedServer]", LNLinkedServer);
                //MTRLocationQuery = MTRLocationQuery.Replace("[LNCompanyId]", LNCompanyId);
                SqlConnection connection = new SqlConnection(MTRConnectionString);
                connection.Open();
                using (var Cmd = new SqlCommand(MTRLocationQuery, connection))
                {
                    Cmd.CommandType = CommandType.Text;
                    System.Data.SqlClient.SqlDataReader sdr = Cmd.ExecuteReader();
                    if (sdr.HasRows)
                    {
                        while (sdr.Read())
                        {
                            listLocation.Add(new CategoryData { Code = sdr["Dept"].ToString(), Description = sdr["DeptDesc"].ToString() });
                        }
                    }
                    connection.Close();
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return listLocation;
        }

        [HttpPost]
        public ActionResult GetActiveEmployeforFilter(string search = "")
        {
            var lstEmployee = Manager.GetActiveEmployeName();
            List<ddlValue> lst = new List<ddlValue>();
            if (string.IsNullOrWhiteSpace(search))
            {
                lst = lstEmployee.Select(x => new ddlValue { id = x.id.ToString(), text = x.text.ToString() }).Distinct().ToList();
            }
            else
            {
                search = search.ToLower();
                lst = lstEmployee.Where(x => x.id.Contains(search) || x.text.ToLower().Contains(search)).Select(x => new ddlValue { id = x.id.ToString(), text = x.text.ToString() }).Distinct().ToList();
            }
            lst.ForEach(x =>
            {
                x.text = string.IsNullOrWhiteSpace(x.text) ? x.id : x.text;
            });
            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public List<ddlValue> GetRoleWiseEmployee(string param = "")
        {
            string[] arrayRoles = param.Split(',');
            List<string> RoleId = db.ATH004.Where(x => arrayRoles.Contains(x.Role)).Select(x => x.Id.ToString()).ToList();
            var lstApproverList = db.ATH001.
                    Join(db.COM003, x => x.Employee, y => y.t_psno,
                    (x, y) => new { x, y })
                    .Where(z => z.y.t_actv == 1 && RoleId.Contains(z.x.Role.ToString()))
                    .Distinct()
                    .Select(z => new ddlValue
                    {
                        id = z.x.Employee,
                        text = z.x.Employee + " - " + z.y.t_name
                    }).Distinct().ToList();

            return lstApproverList;
        }

        public List<ddlValue> GetICSEmployee()
        {
            //string[] arrayRoles = param.Split(',');
            List<string> RoleId = db.ATH004.Select(x => x.Id.ToString()).ToList();
            var lstApproverList = db.ATH001.
                    Join(db.COM003, x => x.Employee, y => y.t_psno,
                    (x, y) => new { x, y })
                    .Where(z => z.y.t_actv == 1 && RoleId.Contains(z.x.Role.ToString()))
                    .Distinct()
                    .Select(z => new ddlValue
                    {
                        id = z.x.Employee,
                        text = z.x.Employee + " - " + z.y.t_name
                    }).Distinct().ToList();

            return lstApproverList;
        }

        [HttpGet]
        public ActionResult GetMobileAppConfiguration()
        {
            try
            {
                string MobileAppVersion = Manager.GetConfigValue("MobileAppVersion");
                Boolean MobileForceUpdate = Convert.ToBoolean(Manager.GetConfigValue("MobileForceUpdate"));
                string APK_Download_URL = Manager.GetConfigValue("APK_Download_URL");
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    MobileAppVersion,
                    MobileForceUpdate,
                    APK_Download_URL
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new
                {
                    res = "0",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public JsonResult GetTestInstrument(string term,string location)
        {
            try
            {
                //List<ddlValue> lstBusLoc = new List<ddlValue>();
                //if (string.IsNullOrEmpty(term))
                //{
                   var lstBusLoc = (from data in db.NDE019
                                     where data.Location == location && data.Location.Contains(term)
                                    select new { id = data.TestInstrument, text = data.TestInstrument }).Distinct().ToList();

                //}
                //else
                //{
                //    lstBusLoc = (from data in db.NDE019
                //                 where data.Location == location && data.Location.Contains(term)
                //                 select new { id = data.TestInstrument, text = data.TestInstrument }).Distinct().ToList();
                //}
                return Json(lstBusLoc, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }


    }
    public class DeleteFileAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Flush();

            //convert the current filter context to file and get the file path
            string filePath = (filterContext.Result as FilePathResult).FileName;

            //delete the file after download
            System.IO.File.Delete(filePath);
        }
    }

    public class UserActivity : clsBase
    {
        public bool Add(string ActionType, string Action)
        {
            try
            {
                ATH024 objATH024 = new ATH024();
                objATH024.ActionType = ActionType;
                objATH024.Action = Action;
                objATH024.ActionDate = DateTime.Now;
                objATH024.ActionBy = objClsLoginInfo.UserName;
                objATH024.Browser = System.Web.HttpContext.Current.Request.Browser.Browser;
                objATH024.IPAddress = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];

                db.ATH024.Add(objATH024);
                db.SaveChanges();
            }
            catch
            {
                return false;
            }
            return true;
        }
    }

    [Serializable]
    public class WindowsImpersonationCredentials : IReportServerCredentials
    {
        // local variable for network credential.
        private string _UserName;
        private string _PassWord;
        private string _DomainName;
        public WindowsImpersonationCredentials(string UserName, string PassWord, string DomainName)
        {
            _UserName = UserName;
            _PassWord = PassWord;
            _DomainName = DomainName;
        }
        public bool GetFormsCredentials(out Cookie authCookie, out string userName, out string password, out string authority)
        {
            authCookie = null;
            userName = password = authority = null;
            return false;
        }

        public WindowsIdentity ImpersonationUser
        {
            get { return WindowsIdentity.GetCurrent(); }
        }

        public ICredentials NetworkCredentials
        {
            get { return new NetworkCredential(_UserName, _PassWord, _DomainName); }
        }

        public override string ToString()
        {
            return String.Format("WindowsIdentity: {0} ({1})", this.ImpersonationUser.Name, this.ImpersonationUser.User.Value);
        }

        
    }




}