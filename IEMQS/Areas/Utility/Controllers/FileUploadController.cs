﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Microsoft.SharePoint.Client;
using Shp = Microsoft.SharePoint.Client;
using System.Text;
using Ionic.Zip;
using System.IO.Compression;
using IEMQS.DESCore;
using IEMQS.DESServices;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace IEMQS.Areas.Utility.Controllers
{
    public class FileUploadController : clsBase
    {
        // GET: Utility/FileUpload
        //public ActionResult Index()
        //{
        //    return View();
        //}

        //[SessionExpireFilter]
        //public ActionResult tempupload()
        //{
        //    return View();
        //}

        [HttpPost]
        public ActionResult GetAttachmentDetails(string folderPath, bool includeFromSubfolders = false, string Type = "", string orderby = "")
        {
            if (string.IsNullOrWhiteSpace(folderPath))
                return Json("", JsonRequestBehavior.AllowGet);
            var Files = (new clsFileUpload(Type)).GetDocuments(folderPath, includeFromSubfolders, orderby);
            return Json(Files, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveDeletedFile(string folderPath, string[] toDelete, string Type = "", string removeonlycreatedbyme = "false")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string UnAuthorizeFileName = "";
            string RemoveFileName = "";
            try
            {
                foreach (var item in toDelete)
                {
                    if (removeonlycreatedbyme == "true")
                    {
                        if (objClsLoginInfo.UserName.Trim() != (new clsFileUpload(Type)).GetFileUploaderName(folderPath, item))
                        {
                            Manager.ConcatString(ref UnAuthorizeFileName, item);
                            continue;
                        }
                    }
                    (new clsFileUpload(Type)).DeleteFile(folderPath, item);
                    Manager.ConcatString(ref RemoveFileName, item);
                }
                if (UnAuthorizeFileName.Length > 0)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = UnAuthorizeFileName + " files can not be delete by you!";
                }
                if (RemoveFileName.Length > 0)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = RemoveFileName + " file removed successfully!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public HttpResponseMessage UploadFile()
        {
            try
            {
                foreach (string file in Request.Files)
                {
                    var FileDataContent = Request.Files[file];
                    var FolderPath = Request.Form["folderpath"];
                    var comment = Request.Form["comments"];
                    bool onFTP = Request.Form["onFTP"] != null ? Convert.ToBoolean(Request.Form["onFTP"]) : false;
                    var Type = Request.Form["Type"] != null ? Convert.ToString(Request.Form["Type"]) : "";
                    if (FileDataContent != null && FileDataContent.ContentLength > 0)
                    {
                        // take the input stream, and save it to a temp folder using the original file.part name posted
                        var stream = FileDataContent.InputStream;
                        var fileName = Path.GetFileName(FileDataContent.FileName);
                        var FileLibraryPath = System.Configuration.ConfigurationManager.AppSettings["IEMQSDOC_Path" + Type];
                        string mergePath = string.Empty;
                        if (FileLibraryPath != null)
                        {
                            mergePath = FileLibraryPath + FolderPath + "/";
                        }

                        var UploadPath = Server.MapPath("~/App_Data/" + FolderPath);

                        Directory.CreateDirectory(UploadPath);
                        string path = Path.Combine(UploadPath, fileName);
                        try
                        {
                            if (System.IO.File.Exists(path))
                                System.IO.File.Delete(path);
                            using (var fileStream = System.IO.File.Create(path))
                            {
                                try
                                {
                                    stream.CopyTo(fileStream);
                                }
                                catch (Exception ex)
                                {
                                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                    throw ex;
                                }
                            }
                            // Once the file part is saved, see if we have enough to merge it
                            ChunkMergeUtility cmUtility = new ChunkMergeUtility();
                            try
                            {
                                var IsFileMerge = cmUtility.MergeFile(path, mergePath, FolderPath, Type, objClsLoginInfo.UserName, onFTP, comment);
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                throw ex;
                            }
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }

            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent("File uploaded.")
            };
        }

        [HttpPost]
        public bool CheckFileExistInFolder(string folderPath, string Type = "", bool includeFromSubfolders = false)
        {
            if (string.IsNullOrWhiteSpace(folderPath))
                return false;

            var isexist = (new clsFileUpload(Type)).CheckAttachmentUpload(folderPath, includeFromSubfolders);
            return isexist;
        }
        public ActionResult LoadFileUploadPartial(string controlid, string folderpath, int? maxfileuploadsizeinmb, int? maxchunksizeinmb, int? chunkintervalinsec,
                                                bool? showdeleteaction, bool? showattachmentbtn, bool? isdisabled,
                                                bool? reloadafterupload, bool? isrequired, bool? showdownloadbtn, bool? ismultipleuploaded, bool? isfilesdisplayfromdefaulturl,
                                                bool? removeonlycreatedbyme, bool? hideattachbtnforsignleupload,
                                                string actionurl = "", string type = "", string allowfileextension = "", string successcallbackfunction = "", string orderby = "",
                                                string filesdisplayfromurl = "", string deletesuccesscallbackfunction = "", string otherfolderpath = "", string documentloadcallbackfunction = "")
        {
            FileUploadEnt ent = new FileUploadEnt(controlid, folderpath);
            if (maxchunksizeinmb.HasValue) { ent.MaxChunkSizeInMB = Convert.ToInt32(maxchunksizeinmb); }
            if (chunkintervalinsec.HasValue) { ent.ChunkIntervalInSec = Convert.ToInt32(chunkintervalinsec); }
            if (maxfileuploadsizeinmb.HasValue) { ent.MaxFileUploadSizeInMB = Convert.ToInt32(maxfileuploadsizeinmb); }
            if (showdeleteaction.HasValue) { ent.ShowDeleteAction = Convert.ToBoolean(showdeleteaction); }
            if (showattachmentbtn.HasValue) { ent.ShowAttachmentBtn = Convert.ToBoolean(showattachmentbtn); }
            if (isdisabled.HasValue) { ent.IsDisabled = Convert.ToBoolean(isdisabled); }
            if (reloadafterupload.HasValue) { ent.ReloadAfterUpload = Convert.ToBoolean(reloadafterupload); }
            if (isrequired.HasValue) { ent.IsRequired = Convert.ToBoolean(isrequired); }
            if (actionurl != string.Empty) { ent.ActionURL = actionurl; }
            if (type != string.Empty) { ent.Type = type; }
            if (allowfileextension != string.Empty) { ent.AllowFileExtension = allowfileextension; }
            if (successcallbackfunction != string.Empty) { ent.SuccessCallbackFunction = successcallbackfunction; }
            if (documentloadcallbackfunction != string.Empty) { ent.DocumentLoadCallbackFunction = documentloadcallbackfunction; }
            if (deletesuccesscallbackfunction != string.Empty) { ent.DeleteSuccessCallbackFunction = deletesuccesscallbackfunction; }
            if (showdownloadbtn.HasValue) { ent.ShowDownloadBtn = Convert.ToBoolean(showdownloadbtn); }
            if (ismultipleuploaded.HasValue) { ent.IsMultipleUploaded = Convert.ToBoolean(ismultipleuploaded); }
            if (removeonlycreatedbyme.HasValue) { ent.RemoveOnlyCreatedByMe = Convert.ToBoolean(removeonlycreatedbyme); }
            if (isfilesdisplayfromdefaulturl.HasValue) { ent.IsFilesDisplayFromDefaultURL = Convert.ToBoolean(isfilesdisplayfromdefaulturl); }
            if (orderby != string.Empty) { ent.OrderBy = orderby; }
            if (filesdisplayfromurl != string.Empty) { ent.FilesDisplayFromURL = filesdisplayfromurl; }
            if (otherfolderpath != string.Empty) { ent.OtherFolderPath = otherfolderpath; }
            if (hideattachbtnforsignleupload.HasValue) { ent.HideAttachbtnForSignleUpload = Convert.ToBoolean(hideattachbtnforsignleupload); }
            ent.Uploader = objClsLoginInfo.UserName.Trim();

            return PartialView("_FileUploadPartial", ent);
        }

        [HttpPost]
        public ActionResult DownloadSharePointFile(string fileName, string folderPath, string Type = "")
        {
            if (string.IsNullOrWhiteSpace(fileName) || string.IsNullOrWhiteSpace(folderPath))
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            var stream = (new clsFileUpload(Type)).GetFileStream(folderPath, fileName);
            FileStreamResult fsr = new FileStreamResult(stream, "application/octet-stream");
            fsr.FileDownloadName = fileName;
            return fsr;
        }

        //public ActionResult DownloadZipFile(string Type = "")
        //{
        //    //MemoryStream ms = new MemoryStream();
        //    //ms.Seek(0, SeekOrigin.Begin);
        //    //CreateZipForFolder("FDS001/1095/1153", "TwoHundredMBCopy.doc", Server.MapPath("~/App_Data/FDS001/1095/1153/TwoHundredMBCopy.doc"));
        //    ZipFile zip = new ZipFile();
        //    zip.CompressionLevel = Ionic.Zlib.CompressionLevel.BestSpeed;
        //    zip.BufferSize = 65536 * 16;
        //    zip.UseZip64WhenSaving = Zip64Option.Always;
        //    zip.AddDirectory(Server.MapPath("~/App_Data/FDS001"));
        //    zip.Save(Server.MapPath("~/App_Data/File.zip"));

        //    string fullPath = Path.Combine(Server.MapPath("~/App_Data/"), "File.zip");
        //    return File(fullPath, "application/octet-stream", "File.zip");

        //    //return File(ms, "application/octet-stream", "FDMS-11.zip");
        //}
        //public ActionResult DownloadFile(string Type = "")
        //{
        //    string fullPath = Path.Combine(Server.MapPath("~/App_Data/"), "File.zip");
        //    return File(fullPath, "application/octet-stream", "File.zip");

        //}
        //public void CreateZipForFolder(string folderPath, string FileName, string Path)
        //{
        //    List<Stream> streams = new List<Stream>();
        //    ZipFile zf = new ZipFile();
        //    Stream responseStream = (new clsFileUpload()).GetFTPFileStream(folderPath, FileName, Path);
        //    if (responseStream != null)
        //    {
        //        //zf.AddEntry(FileName, responseStream);
        //        streams.Add(responseStream);
        //    }
        //    //zf.Save(Server.MapPath("~/App_Data/File.zip"));
        //    foreach (var stream in streams)
        //        stream.Close();
        //}
        //public ActionResult DownloadZipFile()
        //{
        //    DirectoryInfo directorySelected = new DirectoryInfo(Server.MapPath("~/App_Data/FDS001/1095/1153"));
        //    foreach (FileInfo fileToCompress in directorySelected.GetFiles())
        //    {
        //        using (FileStream originalFileStream = fileToCompress.OpenRead())
        //        {
        //            if ((System.IO.File.GetAttributes(fileToCompress.FullName) &
        //               FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompress.Extension != ".gz")
        //            {
        //                using (FileStream compressedFileStream = System.IO.File.Create(fileToCompress.FullName + ".gz"))
        //                {
        //                    using (GZipStream compressionStream = new GZipStream(compressedFileStream,
        //                       CompressionMode.Compress))
        //                    {
        //                        originalFileStream.CopyTo(compressionStream);

        //                    }
        //                }                       
        //            }

        //        }
        //    }
        //    return File("", "application/octet-stream", "File.zip");
        //}

        public ActionResult DownloadZipFile(string folderPath, bool includeFromSubfolders = false, string Type = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (string.IsNullOrWhiteSpace(folderPath))
                return Json("", JsonRequestBehavior.AllowGet);
            List<clsFileUpload.ZipFilesEnt> listZipFilesEnt = new List<clsFileUpload.ZipFilesEnt>();
            listZipFilesEnt = (new clsFileUpload(Type)).CreateZipFilelist(folderPath, includeFromSubfolders);
            if (listZipFilesEnt.Count > 0)
            {
                try
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                        //zip.AddDirectoryByName("Files");   //for create folder
                        foreach (clsFileUpload.ZipFilesEnt file in listZipFilesEnt)
                        {
                            //zip.AddDirectoryByName("Files1");
                            zip.AddEntry(file.FileName, file.Bytes);
                        }

                        Response.Clear();
                        Response.BufferOutput = false;
                        string zipName = String.Format("Zip_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                        Response.ContentType = "application/zip";
                        Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                        zip.Save(Response.OutputStream);
                        Response.End();
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "File Zip Successfully!";
                }
                catch (Exception ex)
                {
                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "No Files Found For Download Zip!";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult WriteContentOnPDF(string filenames, string UploadFolderPath, string content, int posX, int posY, int rotation = 0, int align = 0, string position = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                FileUpload_WriteContentOnPDF(filenames, UploadFolderPath, content, posX, posY, rotation, align, position);
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public static void FileUpload_WriteContentOnPDF(string filenames, string UploadFolderPath, string content, int posX, int posY, int rotation = 0, int align = 0, string position = "")
        {
            // ----- Read and Add HTR Number on new PDF ----- //
            if (String.IsNullOrWhiteSpace(filenames))
                return;
            var FileNames = filenames.Split(',');
            var HTRDocuments = (new clsFileUpload()).GetDocuments(UploadFolderPath);

            foreach (var item in HTRDocuments.Where(w => w.Name.ToLower().Contains(".pdf") && FileNames.Contains(w.Name)))
            {
                using (var ms = new MemoryStream())
                {

                    //var templateReader = new iTextSharp.text.pdf.PdfReader(item.URL);
                    using (var rdr = new iTextSharp.text.pdf.PdfReader(item.URL))
                    {
                        var stamper = new iTextSharp.text.pdf.PdfStamper(rdr, ms);
                        for (int i = 1; i <= rdr.NumberOfPages; i++)
                        {
                            SetPosXYByPosition(position, rdr.GetPageSize(i).Height, rdr.GetPageSize(i).Width, ref posX, ref posY, ref align);

                            if (new int[] { 0, 90, 180, 360 }.Contains(rotation))
                            {
                                var image = iTextSharp.text.Image.GetInstance(rotation == 0 || rotation == 180 ? new System.Drawing.Bitmap(280, 30) : new System.Drawing.Bitmap(30, 280), iTextSharp.text.BaseColor.WHITE);
                                if (rotation == 0 || rotation == 180)
                                    image.SetAbsolutePosition(posX - 250, posY - 10);
                                else
                                    image.SetAbsolutePosition(posX - 20, posY - 50);
                                stamper.GetOverContent(i).AddImage(image, true);
                            }
                            var bf = iTextSharp.text.pdf.BaseFont.CreateFont();
                            stamper.GetOverContent(i).BeginText();
                            stamper.GetOverContent(i).SetFontAndSize(bf, 18f);
                            stamper.GetOverContent(i).ShowTextAligned(align, content, posX, posY, rotation);
                            stamper.GetOverContent(i).EndText();
                        }
                        stamper.Close();
                    }

                    byte[] bytes = ms.ToArray();
                    ms.Close();
                    System.IO.Stream stream = new System.IO.MemoryStream(bytes);
                    (new clsFileUpload()).FileUpload(item.Name, UploadFolderPath, stream, item.Uploader, item.Comments, item.onFTP); // Override file with added label on PDF
                }
            }
            // ------------------------------------------ //
        }

        private static void SetPosXYByPosition(string position, float height, float width, ref int posX, ref int posY, ref int align)
        {
            if (String.IsNullOrWhiteSpace(position))
                return;
            position = position.ToLower();
            if (position.Contains("top"))
                posY = Convert.ToInt32(height - 20);
            else if (position.Contains("bottom"))
                posY = 20;

            if (position.Contains("left"))
            {
                posX = 20;
                align = iTextSharp.text.pdf.PdfContentByte.ALIGN_LEFT;
            }
            else if (position.Contains("right"))
            {
                posX = Convert.ToInt32(width - 10);
                align = iTextSharp.text.pdf.PdfContentByte.ALIGN_RIGHT;
            }
            else
            {
                posX = Convert.ToInt32(width / 2);
                align = iTextSharp.text.pdf.PdfContentByte.ALIGN_CENTER;
            }
        }

        public ActionResult ExtractZipFolder(string uploader, string folderPath = "PAM007/ContractWiseDocuments/M05131590", string FileName = "Test.zip", bool includeFromSubfolders = false, string Type = "", bool RemoveAfterExtract = true)
        {
            var dateOne = DateTime.Now;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (string.IsNullOrWhiteSpace(folderPath))
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
            var ExtractFolderName = DateTime.Now.ToFileTime();
            var stream = (new clsFileUpload(Type)).GetFileStream(folderPath, FileName);
            var UploadPath = Server.MapPath("~/App_Data/Extract/" + ExtractFolderName);
            Directory.CreateDirectory(UploadPath);
            using (Stream file = System.IO.File.Create(UploadPath + "/" + FileName))
            {
                CopyStream(stream, file);
            }

            var existingZipFile = UploadPath + "/" + FileName;
            var targetDirectory = UploadPath;

            using (ZipFile zip = ZipFile.Read(existingZipFile))
            {
                foreach (ZipEntry e in zip)
                {
                    e.Extract(targetDirectory);
                }
            }
            if (RemoveAfterExtract)
            {
                System.IO.File.Delete(existingZipFile);
            }

            DirectoryInfo d = new DirectoryInfo(targetDirectory);//Assuming Test is your Folder
            FileInfo[] Files = d.GetFiles(); //Getting Text files
            string str = "";
            foreach (FileInfo file in Files)
            {
                str = file.Name;
                clsManager Manager = new clsManager();
                try
                {
                    using (Stream data = new MemoryStream())
                    {
                        var OnFTP = false;
                        using (FileStream fileChunk = new FileStream(UploadPath + "/" + file.Name, FileMode.Open))
                        {
                            fileChunk.CopyTo(data);
                        }
                        long fileSize = file.Length;
                        if (fileSize > 100000000) { OnFTP = true; }
                        (new clsFileUpload(Type)).FileUpload(file.Name, folderPath, data, uploader, "", OnFTP);
                    }
                }
                catch (Exception)
                {
                }
            }
            var dateTwo = DateTime.Now;
            var diff = dateTwo.Subtract(dateOne);
            var res = String.Format("{0}:{1}:{2}", diff.Hours, diff.Minutes, diff.Seconds);
            objResponseMsg.Value = res;
            objResponseMsg.Key = true;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public static void CopyStream(Stream input, Stream output)
        {
            byte[] buffer = new byte[8 * 1024];
            int len;
            while ((len = input.Read(buffer, 0, buffer.Length)) > 0)
            {
                output.Write(buffer, 0, len);
            }
        }

        public ActionResult DownloadMergedFileWithMultipleFolders(string folderPaths, string fileName = "Merged", bool includeFromSubfolders = false, string filetype = "pdf", string Type = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (string.IsNullOrWhiteSpace(folderPaths))
                return Json("", JsonRequestBehavior.AllowGet);
            bool isFiledAdded = false;
            fileName = string.Join("�", fileName.Split(Path.GetInvalidFileNameChars()));
            string pdfName = String.Format("{0}_{1}.{2}", fileName, DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"), filetype);
            string OutFile = Server.MapPath("~/Resources/Download/") + pdfName;
            if (filetype.ToLower() == "pdf")
            {
                try
                {
                    using (var doc = new iTextSharp.text.Document())
                    using (FileStream stream = new FileStream(OutFile, FileMode.Create))
                    using (var pdf = new iTextSharp.text.pdf.PdfCopy(doc, stream))
                    {
                        doc.Open();
                        foreach (var folderPath in folderPaths.Split(','))
                        {
                            if (string.IsNullOrWhiteSpace(folderPaths))
                                continue;
                            var listZipFilesEnt = (new clsFileUpload(Type)).CreateZipFilelist(folderPath, includeFromSubfolders);
                            if (listZipFilesEnt.Count > 0)
                            {
                                try
                                {
                                    foreach (clsFileUpload.ZipFilesEnt file in listZipFilesEnt.Where(w => w.FileName.Contains(".pdf")))
                                    {
                                        var reader = new iTextSharp.text.pdf.PdfReader(file.Bytes);
                                        iTextSharp.text.pdf.PdfImportedPage page = null;

                                        for (int i = 0; i < reader.NumberOfPages; i++)
                                        {
                                            page = pdf.GetImportedPage(reader, i + 1);
                                            pdf.AddPage(page);
                                        }
                                        pdf.FreeReader(reader);
                                        reader.Close();
                                        isFiledAdded = true;
                                    }
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = "File PDF Successfully!";
                                }
                                catch (Exception ex)
                                {
                                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                }
                            }
                        }
                        if (isFiledAdded)
                        {
                            pdf.Close();
                            Response.Clear();
                            Response.BufferOutput = false;
                            Response.ContentType = "application/pdf";
                            Response.AddHeader("content-disposition", "attachment; filename=" + pdfName);
                            stream.Close();
                            var streamread = System.IO.File.OpenRead(OutFile);
                            streamread.CopyTo(Response.OutputStream);
                            streamread.Close();
                            Response.End();
                            System.IO.File.Delete(OutFile);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "No Files Found For Download Zip!";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                catch
                {
                    System.IO.File.Delete(OutFile);
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "File Type not supported for merging";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public string MergedPDFFileWithMultipleFileBytes(List<byte[]> lstFileBytes, string fileName, string filetype = "pdf")
        {

            try
            {
                using (var doc = new iTextSharp.text.Document())
                using (FileStream stream = new FileStream(fileName, FileMode.Create))
                using (var pdf = new iTextSharp.text.pdf.PdfCopy(doc, stream))
                {
                    doc.Open();
                    foreach (var fileBytes in lstFileBytes)
                    {
                        try
                        {
                            var reader = new iTextSharp.text.pdf.PdfReader(fileBytes);
                            iTextSharp.text.pdf.PdfImportedPage page = null;

                            for (int i = 0; i < reader.NumberOfPages; i++)
                            {
                                page = pdf.GetImportedPage(reader, i + 1);
                                pdf.AddPage(page);
                            }
                            pdf.FreeReader(reader);
                            reader.Close();
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        }
                    }
                    pdf.Close();
                    stream.Close();
                }
            }
            catch (Exception ex)
            {
                System.IO.File.Delete(fileName);
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }

            return fileName;
        }

        [HttpPost]
        public ActionResult CopyFilesToFolders(string FromFolderPath, string ToFolderPaths, string FileNames)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (string.IsNullOrWhiteSpace(ToFolderPaths))
                {
                    objResponseMsg.Value = "FolderPaths is required";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (string.IsNullOrWhiteSpace(FileNames))
                {
                    objResponseMsg.Value = "FileNames is required";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var objFileUpload = new clsFileUpload();
                foreach (var folderpath in ToFolderPaths.Split(','))
                {
                    foreach (var filename in FileNames.Split(','))
                    {
                        objFileUpload.CopyFolderContentsAsync(FromFolderPath, folderpath, filename);
                    }
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "File Copied Successfuly";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CheckAllFileUploadSuccessfully(string FolderPath, string[] FileNames, string Type = "", string OtherFolderPath = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (string.IsNullOrWhiteSpace(FolderPath))
                return Json("", JsonRequestBehavior.AllowGet);

            //CheckFiles:
            var Files = (new clsFileUpload(Type)).GetDocuments(FolderPath, false);
            foreach (var file in FileNames)
            {
                if (!Files.Any(i => i.Name == file))
                {
                    //System.Threading.Thread.Sleep(10000);
                    //goto CheckFiles;
                    objResponseMsg.Key = false;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
            }
            if (!string.IsNullOrEmpty(OtherFolderPath))
            {
                foreach (var destinationFolder in OtherFolderPath.Split(','))
                {
                    (new clsFileUpload(Type)).CopyFolderContentsAsync(FolderPath, destinationFolder);
                }
            }
            objResponseMsg.Key = true;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #region FCS CODE
        public PartialViewResult LoadpartialViewDocumentUpload(
            string TableName, string TableId, string Path, string ControlID, bool? IsMultipleUploaded, bool? IsDisabled,
            string folderpath, int? MaxFileUploadSizeInMB, int? MaxChunkSizeInMB, int? ChunkIntervalInSec,
            bool? ShowDeleteAction, bool? ShowAttachmentBtn, bool? isdisabled,
            bool? ReloadAfterUpload, bool? IsRequired, bool? ShowDownloadBtn, bool? ismultipleuploaded,
            bool? IsFilesDisplayFromDefaultURL, bool? RemoveOnlyCreatedByMe, bool? HideAttachbtnForSignleUpload,
            string ActionURL = "", string Type = "", string AllowFileExtension = "", string SuccessCallbackFunction = "",
            string OrderBy = "", string FilesDisplayFromURL = "", string DeleteSuccessCallbackFunction = "",
            string OtherFolderPath = "", string DocumentLoadCallbackFunction = "", int? ViewerId = 1)
        {

            FCS_FileUploadEnt _obj = new FCS_FileUploadEnt(ControlID, Path);

            IpLocation ipLocation = CommonService.GetUseIPConfig;
            _obj.CurrentLocationIp = ipLocation.Ip;
            _obj.FilePath = ipLocation.File_Path;
            _obj.FileLocation = ipLocation.Location;
            _obj.DocumentPath = Path;
            _obj.TableName = TableName;
            _obj.TableId = TableId;

            if (!string.IsNullOrEmpty(TableId) && TableId != "0") _obj.AutoProcessQueue = true;
            if (ViewerId.HasValue) { _obj.ViewerId = Convert.ToInt32(ViewerId); }
            if (IsMultipleUploaded.HasValue) { _obj.IsMultipleUploaded = Convert.ToBoolean(IsMultipleUploaded); }
            if (IsDisabled.HasValue) { _obj.IsDisabled = Convert.ToBoolean(IsDisabled); }
            if (MaxChunkSizeInMB.HasValue) { _obj.MaxChunkSizeInMB = Convert.ToInt32(MaxChunkSizeInMB); }
            if (ChunkIntervalInSec.HasValue) { _obj.ChunkIntervalInSec = Convert.ToInt32(ChunkIntervalInSec); }
            if (MaxFileUploadSizeInMB.HasValue) { _obj.MaxFileUploadSizeInMB = Convert.ToInt32(MaxFileUploadSizeInMB); }
            if (ShowDeleteAction.HasValue) { _obj.ShowDeleteAction = Convert.ToBoolean(ShowDeleteAction); }
            if (ShowAttachmentBtn.HasValue) { _obj.ShowAttachmentBtn = Convert.ToBoolean(ShowAttachmentBtn); }
            if (ReloadAfterUpload.HasValue) { _obj.ReloadAfterUpload = Convert.ToBoolean(ReloadAfterUpload); }
            if (IsRequired.HasValue) { _obj.IsRequired = Convert.ToBoolean(IsRequired); }
            if (ActionURL != string.Empty) { _obj.ActionURL = ActionURL; }
            if (Type != string.Empty) { _obj.Type = Type; }
            if (AllowFileExtension != string.Empty) { _obj.AllowFileExtension = AllowFileExtension; }
            if (SuccessCallbackFunction != string.Empty) { _obj.SuccessCallbackFunction = SuccessCallbackFunction; }
            if (DocumentLoadCallbackFunction != string.Empty) { _obj.DocumentLoadCallbackFunction = DocumentLoadCallbackFunction; }
            if (DeleteSuccessCallbackFunction != string.Empty) { _obj.DeleteSuccessCallbackFunction = DeleteSuccessCallbackFunction; }
            if (ShowDownloadBtn.HasValue) { _obj.ShowDownloadBtn = Convert.ToBoolean(ShowDownloadBtn); }
            if (RemoveOnlyCreatedByMe.HasValue) { _obj.RemoveOnlyCreatedByMe = Convert.ToBoolean(RemoveOnlyCreatedByMe); }
            if (IsFilesDisplayFromDefaultURL.HasValue) { _obj.IsFilesDisplayFromDefaultURL = Convert.ToBoolean(IsFilesDisplayFromDefaultURL); }
            if (OrderBy != string.Empty) { _obj.OrderBy = OrderBy; }
            if (FilesDisplayFromURL != string.Empty) { _obj.FilesDisplayFromURL = FilesDisplayFromURL; }
            if (OtherFolderPath != string.Empty) { _obj.OtherFolderPath = OtherFolderPath; }
            if (HideAttachbtnForSignleUpload.HasValue) { _obj.HideAttachbtnForSignleUpload = Convert.ToBoolean(HideAttachbtnForSignleUpload); }
            _obj.Uploader = objClsLoginInfo.Name.Trim();

            return PartialView("_FileUploadPartialFCS", _obj);
        }

        public ActionResult SaveDocumentDetails(cls_Document_Details _objData)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                List<FCS001> _lst = new List<FCS001>();

                foreach (var item in _objData.FileFormatData)
                {
                    FCS001 _obj = new FCS001();
                    _obj.TableName = _objData.TableName;
                    _obj.TableId = _objData.TableId;
                    _obj.MainDocumentPath = item.destination + "/" + item.filename;
                    _obj.Document_name = item.originalname;
                    _obj.FileFormate = Path.GetExtension(item.originalname);
                    _obj.FileLocation = _objData.FileLocation;
                    _obj.ViewerId = _objData.ViewerId;

                    _obj.IsMainDocument = false;
                    _obj.Status = "";
                    _obj.Remark = "";
                    _obj.FileVersion = 1;
                    _obj.VersionGUID = Guid.NewGuid().ToString();
                    _obj.IsLatest = true;

                    if (_obj.FileLocation == (int)LocationType.Hazira)
                        _obj.Central = true; //By Default Centrall will be false.
                    else
                        _obj.Central = false; //By Default Centrall will be false.

                    _obj.CreatedBy = CommonService.objClsLoginInfo.UserName;
                    _obj.CreatedOn = DateTime.Now;

                    _lst.Add(_obj);
                }

                db.FCS001.AddRange(_lst);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderInsert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDocumentsByTableNameAndTableId(DataTableParamModel parm, Int64? TableId, string TableName, int? ViewerId = 1)
        {
            try
            {
                ViewerId = ViewerId >= 1 ? ViewerId : 1;
                var _lst_document = db.SP_COMMON_GET_DOCUMENT_DETAILS(parm.iDisplayStart, parm.iDisplayLength, parm.sSearch, parm.sSortDir_0, parm.iSortCol_0, TableName, TableId, ViewerId).ToList();
                int recordsTotal = _lst_document.Select(x => x.TableCount).FirstOrDefault() ?? 0;

                return Json(new
                {
                    Echo = parm.sEcho,
                    iTotalRecord = recordsTotal,
                    iTotalDisplayRecords = recordsTotal,
                    data = _lst_document
                });
            }
            catch (Exception ex)
            {
                clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult Delete_Document_File(Int64 Id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                FCS001 _obj = db.FCS001.Find(Id);
                db.FCS001.Remove(_obj);
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderInsert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult UpdateDocumentRemark(cls_Document_Details model)
        {
            try
            {
                var data = db.FCS001.Where(yy => yy.DocumentMappingId == model.DocumentMappingId).FirstOrDefault();
                data.Remark = model.Remark;
                db.Entry(data).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();

                return Json(new { Status = true, Msg = "Remark has been updated successfully" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult CheckAnyRemarksEmptyInTable(Int64? TableId, string TableName)
        {
            try
            {
                var data = db.FCS001.Count(w => (w.Remark == "" || w.Remark == null) && w.TableId == TableId && w.TableName == TableName);
                if (data > 0)
                    return Json(new { Status = false, Msg = "Please enter remarks for all document" }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = true, Msg = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetAllDocumentFileName(Int64? TableId, string TableName, int ViewerId)
        {
            try
            {
                var vCount = db.FCS001.Where(w => w.TableId == TableId && w.TableName == TableName && w.ViewerId == ViewerId).Select(s => s.Document_name);
                if (vCount.Count() > 0)
                    return Json(new { Status = true, Msg = vCount.ToList() }, JsonRequestBehavior.AllowGet);
                else
                    return Json(new { Status = false, Msg = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { Status = false, Msg = "something went wrong try again later" }, JsonRequestBehavior.AllowGet);
                throw;
            }
        }

        [HttpPost]
        public JsonResult SaveCopyDocumentDetails(cls_Document_Details _objData)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                List<FCS001> _lstFromData = db.FCS001.Where(w => w.TableId == _objData.TableId && w.TableName == _objData.TableName).ToList();
                if (_lstFromData.Count > 0)
                {
                    List<FCS001> _lst = new List<FCS001>();

                    foreach (var item in _lstFromData)
                    {
                        FCS001 _obj = new FCS001();
                        _obj.TableName = item.TableName;
                        _obj.TableId = item.TableId;
                        _obj.MainDocumentPath = item.MainDocumentPath;
                        _obj.Document_name = item.Document_name;
                        _obj.FileFormate = item.FileFormate;
                        _obj.FileLocation = item.FileLocation;
                        _obj.ViewerId = item.ViewerId;

                        _obj.IsMainDocument = false;
                        _obj.Status = "";
                        _obj.Remark = "";
                        _obj.FileVersion = 1;
                        _obj.VersionGUID = Guid.NewGuid().ToString();
                        _obj.IsLatest = true;

                        if (_obj.FileLocation == (int)LocationType.Hazira)
                            _obj.Central = true; //By Default Centrall will be false.
                        else
                            _obj.Central = false; //By Default Centrall will be false.

                        _obj.CreatedBy = CommonService.objClsLoginInfo.UserName;
                        _obj.CreatedOn = DateTime.Now;

                        _lst.Add(_obj);
                    }

                    db.FCS001.AddRange(_lst);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderInsert.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public bool CheckAnyDocumentsExits(string TableName, Int64? TableId)
        {
            bool isExits = false;
            try
            {
                if (db.FCS001.Where(c => c.TableId == TableId && c.TableName == TableName).Count() > 0)
                    isExits = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return isExits;
        }

        [HttpPost]
        public List<FCS001> GetAllDocumentsByTableNameTableId(string TableName, Int64? TableId)
        {
            List<FCS001> _lst = new List<FCS001>();
            try
            {
                _lst = db.FCS001.Where(c => c.TableId == TableId && c.TableName == TableName).ToList();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return _lst;
        }

        public async Task CopyDataOnFCSServerAsync(string FromPath, string ToPath, string Old_TableName, Int64 Old_TableId, string New_TableName, Int64 New_TableId, IpLocation ipLocation, string LoginUserName, Int64? Id, bool IsAddTableId = false)
        {
            await Task.Run(() => CopyDataOnFCSServer(FromPath, ToPath, Old_TableName, Old_TableId, New_TableName, New_TableId, ipLocation, LoginUserName, Id, IsAddTableId));
        }

        public bool CopyDataOnFCSServer(string FromPath, string ToPath, string Old_TableName, Int64 Old_TableId, string New_TableName, Int64 New_TableId, IpLocation ipLocation, string LoginUserName, Int64? Id, bool IsAddTableId = false)
        {
            try
            {
                #region COPY FOLDER WITH DATA ON FCS SERVER
                //CURRENTLY WE FIX THE CONDITION FOR HZW | WE HAVE TO PUT CONDITION FOR LOCATION
                var UserName = Manager.GetConfigValue("FCS_HZW_UserName").ToString();
                var Password = Manager.GetConfigValue("FCS_HZW_Password").ToString();
                var Domain = Manager.GetConfigValue("FCS_HZW_Domain").ToString();
                var NetCre = new NetworkCredential(UserName, Password, Domain);//vhzqaplmfcs
                //IpLocation ipLocation = CommonService.GetUseIPConfig;
                //CommonService.GetUseIPConfig
                string[] ipArray = ipLocation.File_Path.Split('/');
                string NetworkDomain = ipArray[0];

                using (var client = new WebClient())
                {
                    cls_copy_params objtb = new cls_copy_params();

                    if (Id > 0)
                    {
                        FCS001 _objF = db.FCS001.Where(w => w.DocumentMappingId == Id).FirstOrDefault();
                        objtb.frompath = "C://FCSFIles//" + FromPath + "//1//"; // "c://FCSFiles////ICS001//19//1/";
                        objtb.topath = "C://FCSFIles//" + ToPath + "//1//";  // "c://FCSFiles////ICS001//19//1000/";

                        string[] PathArr = _objF.MainDocumentPath.Split('/');
                        if (PathArr.Length > 0)
                            objtb.filename = PathArr[PathArr.Length - 1].ToString();
                        else
                            objtb.filename = _objF.Document_name;
                    }
                    else
                    {
                        objtb.frompath = "C://FCSFIles//" + FromPath;
                        objtb.topath = "C://FCSFIles//" + ToPath;
                        objtb.filename = "";
                    }
                    client.Headers.Add("Content-Type", "application/json");
                    client.Credentials = NetCre;
                    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    string sAddress = ipLocation.Ip + "/api/CopyFolderDetails"; //"https://10.7.66.185/api/CopyFolderDetails"
                    var result = client.UploadString(sAddress, "POST", JsonConvert.SerializeObject(objtb));
                    Console.WriteLine(result);
                }
                #endregion

                #region FCS001 | Data copy from Old to new tablename and tableid
                List<FCS001> _lstFromData = db.FCS001.Where(w => w.TableId == Old_TableId && w.TableName == Old_TableName).ToList();

                if (Id > 0)
                {
                    _lstFromData = _lstFromData.Where(w => w.DocumentMappingId == Id).ToList();
                }

                if (_lstFromData.Count > 0)
                {
                    //List<FCS001> _lst = new List<FCS001>();

                    foreach (var item in _lstFromData)
                    {
                        FCS001 _obj = new FCS001();
                        _obj.TableName = New_TableName;
                        _obj.TableId = New_TableId;

                        string[] PathArr = item.MainDocumentPath.Split('/');
                        string MainPath = "";
                        if (PathArr.Length > 0)
                        {
                            if (IsAddTableId)
                                MainPath = ipLocation.File_Path + "/" + New_TableName + "//" + New_TableId + "//1//" + PathArr[PathArr.Length - 1].ToString();
                            else
                                MainPath = ipLocation.File_Path + "/" + New_TableName + "//1//" + PathArr[PathArr.Length - 1].ToString();
                        }
                        else
                            MainPath = item.MainDocumentPath;

                        _obj.MainDocumentPath = MainPath;// item.MainDocumentPath.Replace("/" + Old_TableName + "/", "/" + New_TableName + "/").Replace("/" + Old_TableId + "/", "/" + New_TableId + "/");
                        _obj.Document_name = item.Document_name;
                        _obj.FileFormate = item.FileFormate;
                        _obj.FileLocation = item.FileLocation;
                        _obj.ViewerId = item.ViewerId;

                        _obj.IsMainDocument = false;
                        _obj.Status = "";
                        _obj.Remark = "";
                        _obj.FileVersion = 1;
                        _obj.VersionGUID = Guid.NewGuid().ToString();
                        _obj.IsLatest = true;

                        if (_obj.FileLocation == (int)LocationType.Hazira)
                            _obj.Central = true; //By Default Centrall will be false.
                        else
                            _obj.Central = false; //By Default Centrall will be false.

                        _obj.CreatedBy = LoginUserName;
                        _obj.CreatedOn = DateTime.Now;

                        //_lst.Add(_obj);
                        db.FCS001.Add(_obj);
                        db.SaveChanges();
                    }

                    //if (_lst.Count > 0)
                    //{
                    //    db.FCS001.AddRange(_lst);
                    //    db.SaveChanges();
                    //}
                }
                #endregion

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task DeleteFileOnFCSServerAsync(Int64? id, IpLocation ipLocation)
        {
            try
            {
                await Task.Run(() => DeteleFileAndData(id, ipLocation));
            }
            catch (Exception ex)
            {
            }
        }

        public void DeteleFileAndData(Int64? id, IpLocation ipLocation)
        {
            try
            {
                FCS001 _obj = db.FCS001.Where(w => w.DocumentMappingId == id).FirstOrDefault();

                #region DELETE FILE ON FCS SERVER
                //CURRENTLY WE FIX THE CONDITION FOR HZW | WE HAVE TO PUT CONDITION FOR LOCATION
                var UserName = Manager.GetConfigValue("FCS_HZW_UserName").ToString();
                var Password = Manager.GetConfigValue("FCS_HZW_Password").ToString();
                var Domain = Manager.GetConfigValue("FCS_HZW_Domain").ToString();
                var NetCre = new NetworkCredential(UserName, Password, Domain);//vhzqaplmfcs
                //IpLocation ipLocation = CommonService.GetUseIPConfig;
                string[] ipArray = ipLocation.File_Path.Split('/');
                string NetworkDomain = ipArray[0];

                using (var client = new WebClient())
                {
                    cls_copy_params objtb = new cls_copy_params();
                    objtb.deletePath = _obj.MainDocumentPath;
                    client.Headers.Add("Content-Type", "application/json");
                    client.Credentials = NetCre;
                    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    string sAddress = ipLocation.Ip + "/api/DeleteDoc"; //"https://10.7.66.185/api/DeleteDoc"
                    var result = client.UploadString(sAddress, "POST", JsonConvert.SerializeObject(objtb));
                    Console.WriteLine(result);
                }
                #endregion

                #region DELETE DATA IN TABLE
                db.FCS001.Remove(_obj);
                db.SaveChanges();
                #endregion
            }
            catch (Exception ex)
            {
            }
        }

        public async Task DeleteAllFileOnFCSServerAsync(Int64? Tableid, string TableName, string DeletePath, IpLocation ipLocation)
        {
            try
            {
                await Task.Run(() => DeteleAllFileAndData(Tableid, TableName, DeletePath, ipLocation));
            }
            catch { }
        }

        public void DeteleAllFileAndData(Int64? Tableid, string TableName, string DeletePath, IpLocation ipLocation)
        {
            try
            {
                #region DELETE FILE ON FCS SERVER
                //CURRENTLY WE FIX THE CONDITION FOR HZW | WE HAVE TO PUT CONDITION FOR LOCATION
                var UserName = Manager.GetConfigValue("FCS_HZW_UserName").ToString();
                var Password = Manager.GetConfigValue("FCS_HZW_Password").ToString();
                var Domain = Manager.GetConfigValue("FCS_HZW_Domain").ToString();
                var NetCre = new NetworkCredential(UserName, Password, Domain);//vhzqaplmfcs
                //IpLocation ipLocation = CommonService.GetUseIPConfig;
                string[] ipArray = ipLocation.File_Path.Split('/');
                string NetworkDomain = ipArray[0];

                using (var client = new WebClient())
                {
                    cls_copy_params objtb = new cls_copy_params();
                    objtb.deletePath = "C://FCSFIles//" + DeletePath;
                    client.Headers.Add("Content-Type", "application/json");
                    client.Credentials = NetCre;
                    ServicePointManager.ServerCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                    string sAddress = ipLocation.Ip + "/api/DeleteFolderContentDetails"; //"https://10.7.66.185/api/DeleteDoc"
                    var result = client.UploadString(sAddress, "POST", JsonConvert.SerializeObject(objtb));
                    //Console.WriteLine(result);
                }
                #endregion

                #region DELETE DATA IN TABLE
                List<FCS001> _lst = db.FCS001.Where(w => w.TableId == Tableid && w.TableName == TableName).ToList();
                db.FCS001.RemoveRange(_lst);
                db.SaveChanges();
                #endregion
            }
            catch (Exception ex)
            {
            }
        }

        #endregion

    }

    public class FileUploadEnt
    {
        int chunkSize = 2;
        int chunkInterval = 0;
        string allowFileExtension = "";
        int maxFileUploadSizeInMB = 0;
        public FileUploadEnt(string controlID, string folderPath)
        {
            FolderPath = folderPath;
            ControlID = controlID;
            Type = "";
            ShowDeleteAction = false;
            MaxChunkSizeInMB = 2;
            ShowAttachmentBtn = true;
            IsDisabled = false;
            ReloadAfterUpload = true;
            IsRequired = false;
            AllowFileExtension = (new clsFileUpload(Type)).GetSharePointConfiguration().AllowExtensions;
            //this.AllowFileExtension = "*";
            ActionURL = "/Utility/FileUpload/UploadFile";
            MaxFileUploadSizeInMB = 0;
            SuccessCallbackFunction = "";
            DocumentLoadCallbackFunction = "";
            DeleteSuccessCallbackFunction = "";
            ShowDownloadBtn = true;
            IsMultipleUploaded = true;
            OrderBy = "";
            IsFilesDisplayFromDefaultURL = true;
            FilesDisplayFromURL = "/Utility/FileUpload/GetAttachmentDetails";
            OtherFolderPath = "";
            RemoveOnlyCreatedByMe = false;
            Uploader = "";
            HideAttachbtnForSignleUpload = true;
        }

        public string ControlID { get; set; }
        public string ActionURL { get; set; }
        public string FolderPath { get; set; }
        public string Type { get; set; }
        public bool ShowDeleteAction { get; set; }
        public int MaxChunkSizeInMB
        {
            get { return chunkSize; }
            set { this.chunkSize = value; }
        }
        public bool ShowAttachmentBtn { get; set; }
        public int ChunkIntervalInSec
        {
            get
            {
                if (this.chunkInterval == 0)
                {
                    return 1;
                }
                else
                {
                    return chunkInterval;
                }
            }
            set
            {
                this.chunkInterval = value;
            }
        }
        public bool IsDisabled { get; set; }
        public bool ReloadAfterUpload { get; set; }
        public bool IsRequired { get; set; }
        public string AllowFileExtension
        {
            get
            {
                return allowFileExtension;
            }
            set
            {
                //var validextensionArray = (new clsFileUpload(Type)).GetSharePointConfiguration().AllowExtensions.Split(',');
                //var extensionArray = value.Split(',');
                //var finalextension = validextensionArray.Where(x => extensionArray.Contains(x));
                //allowFileExtension = string.Join(allowFileExtension + ",", finalextension);
                allowFileExtension = value;
            }
        }
        public int MaxFileUploadSizeInMB
        {
            get { return maxFileUploadSizeInMB; }
            set { maxFileUploadSizeInMB = value; }
        }
        public string SuccessCallbackFunction { get; set; }
        public string DocumentLoadCallbackFunction { get; set; }
        public bool ShowDownloadBtn { get; set; }
        public bool IsMultipleUploaded { get; set; }
        public string OrderBy { get; set; }
        public bool IsFilesDisplayFromDefaultURL { get; set; }
        public string FilesDisplayFromURL { get; set; }
        public string DeleteSuccessCallbackFunction { get; set; }
        public string OtherFolderPath { get; set; }
        public bool RemoveOnlyCreatedByMe { get; set; }
        public string Uploader { get; set; }
        public bool HideAttachbtnForSignleUpload { get; set; }
    }

    //class ChunkMergeUtility
    //{
    //    public string FileName { get; set; }
    //    public string TempFolder { get; set; }
    //    public int MaxFileSizeMB { get; set; }
    //    public List<String> FileParts { get; set; }
    //    public ChunkMergeUtility()
    //    {
    //        FileParts = new List<string>();
    //    }

    //    /// <summary>
    //    /// original name + ".part_N.X" (N = file part number, X = total files)
    //    /// Objective = enumerate files in folder, look for all matching parts of split file. If found, merge and return true.
    //    /// </summary>
    //    /// <param name="FileName"></param>
    //    /// <returns></returns>
    //    public bool MergeFile(string FileName, string mergePath, string FolderPath, string Type, string uploader, bool onFTP, string comment = "")
    //    {
    //        bool rslt = false;
    //        try
    //        {
    //            // parse out the different tokens from the filename according to the convention
    //            string partToken = ".part_";
    //            string baseFileName = FileName.Substring(0, FileName.IndexOf(partToken));
    //            string trailingTokens = FileName.Substring(FileName.IndexOf(partToken) + partToken.Length);
    //            int FileIndex = 0;
    //            int FileCount = 0;
    //            int.TryParse(trailingTokens.Substring(0, trailingTokens.IndexOf(".")), out FileIndex);
    //            int.TryParse(trailingTokens.Substring(trailingTokens.IndexOf(".") + 1), out FileCount);
    //            // get a list of all file parts in the temp folder
    //            string Searchpattern = Path.GetFileName(baseFileName) + partToken + "*";
    //            string[] FilesList = Directory.GetFiles(Path.GetDirectoryName(FileName), Searchpattern);
    //            //  merge .. improvement would be to confirm individual parts are there / correctly in sequence, a security check would also be important
    //            // only proceed if we have received all the file chunks

    //            if (FilesList.Count() == FileCount)
    //            {
    //                // use a singleton to stop overlapping processes
    //                if (!MergeFileManager.Instance.InUse(baseFileName))
    //                {
    //                    MergeFileManager.Instance.AddFile(baseFileName);
    //                    var MergeFileName = mergePath + baseFileName.Split('\\').Last();

    //                    try
    //                    {
    //                        if (System.IO.File.Exists(MergeFileName))
    //                            System.IO.File.Delete(MergeFileName);
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
    //                        throw ex;
    //                    }

    //                    try
    //                    {
    //                        // add each file located to a list so we can get them into 
    //                        // the correct order for rebuilding the file
    //                        List<SortedFile> MergeList = new List<SortedFile>();
    //                        try
    //                        {
    //                            foreach (string File in FilesList)
    //                            {
    //                                SortedFile sFile = new SortedFile();
    //                                sFile.FileName = File;
    //                                baseFileName = File.Substring(0, File.IndexOf(partToken));
    //                                trailingTokens = File.Substring(File.IndexOf(partToken) + partToken.Length);
    //                                int.TryParse(trailingTokens.Substring(0, trailingTokens.IndexOf(".")), out FileIndex);
    //                                sFile.FileOrder = FileIndex;
    //                                MergeList.Add(sFile);
    //                            }
    //                        }
    //                        catch (Exception ex)
    //                        {
    //                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
    //                            throw ex;
    //                        }


    //                        // sort by the file-part number to ensure we merge back in the correct order
    //                        var MergeOrder = MergeList.OrderBy(s => s.FileOrder).ToList();

    //                        #region Merge chunk file in Stream and save using Sharepoint Library
    //                        using (Stream stream = new MemoryStream())
    //                        {
    //                            try
    //                            {
    //                                if (onFTP)
    //                                    FtpCreateFolder(FolderPath, baseFileName.Split('\\').Last(), Type);

    //                                foreach (var chunk in MergeOrder)
    //                                {
    //                                    if (onFTP)
    //                                    {
    //                                        try
    //                                        {
    //                                            UploadFileToFtp(FolderPath, baseFileName.Split('\\').Last(), chunk.FileName, Type);
    //                                            byte[] buffer = Encoding.ASCII.GetBytes(baseFileName.Split('\\').Last());
    //                                            stream.Write(buffer, 0, buffer.Length);
    //                                        }
    //                                        catch (Exception ex)
    //                                        {
    //                                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
    //                                            throw ex;
    //                                        }
    //                                    }
    //                                    else
    //                                    {
    //                                        try
    //                                        {
    //                                            using (FileStream fileChunk = new FileStream(chunk.FileName, FileMode.Open))
    //                                            {
    //                                                fileChunk.CopyTo(stream);
    //                                            }
    //                                        }
    //                                        catch (IOException ex)
    //                                        {
    //                                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
    //                                            throw ex;
    //                                        }
    //                                    }
    //                                }
    //                                var UploadFileName = baseFileName.Split('\\').Last();
    //                                clsManager Manager = new clsManager();
    //                                try
    //                                {
    //                                    (new clsFileUpload(Type)).FileUpload(UploadFileName, FolderPath, stream, uploader, comment, onFTP);
    //                                }
    //                                catch (Exception ex)
    //                                {
    //                                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
    //                                    throw ex;
    //                                }
    //                            }
    //                            catch (Exception ex)
    //                            {
    //                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
    //                                throw ex;
    //                            }
    //                            finally
    //                            {
    //                                foreach (var chunk in MergeOrder)
    //                                {
    //                                    if (System.IO.File.Exists(chunk.FileName))
    //                                    {
    //                                        System.IO.File.Delete(chunk.FileName);
    //                                    }
    //                                }
    //                            }
    //                        }
    //                        #endregion
    //                    }
    //                    catch (Exception ex)
    //                    {
    //                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
    //                        MergeFileManager.Instance.RemoveFile(baseFileName);
    //                        throw ex;
    //                    }
    //                    rslt = true;
    //                    // unlock the file from singleton                       
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
    //            throw ex;
    //        }
    //        return rslt;
    //    }
    //    private void UploadFileToFtp(string folderPath, string fileName, string localFile, string Type = "")
    //    {
    //        //string host = "ftp://localhost";
    //        //string user = @"LTHED\20036413";
    //        //string pass = "Abhi@2018";
    //        FtpWebRequest ftpRequest = null;
    //        Stream ftpStream = null;
    //        try
    //        {
    //            var FTP = (new clsFileUpload(Type)).GetSharePointConfiguration().FTP;

    //            /* Create an FTP Request */
    //            ftpRequest = (FtpWebRequest)FtpWebRequest.Create(FTP.Host + "/" + folderPath + "/" + fileName);
    //            /* Log in to the FTP Server with the User Name and Password Provided */
    //            ftpRequest.Credentials = new NetworkCredential(FTP.UserName, FTP.Password);

    //            /* When in doubt, use these options */
    //            ftpRequest.UseBinary = true;
    //            ftpRequest.UsePassive = true;
    //            ftpRequest.KeepAlive = true;
    //            /* Specify the Type of FTP Request */

    //            ftpRequest.Method = WebRequestMethods.Ftp.AppendFile;
    //            /* Establish Return Communication with the FTP Server */
    //            ftpStream = ftpRequest.GetRequestStream();
    //            /* Open a File Stream to Read the File for Upload */
    //            FileStream localFileStream = new FileStream(localFile, FileMode.Open);
    //            /* Buffer for the Downloaded Data */
    //            byte[] byteBuffer = new byte[localFileStream.Length];
    //            int bytesSent = localFileStream.Read(byteBuffer, 0, byteBuffer.Length);
    //            /* Upload the File by Sending the Buffered Data Until the Transfer is Complete */
    //            try
    //            {
    //                //while (bytesSent != 0)
    //                //{
    //                ftpStream.Write(byteBuffer, 0, byteBuffer.Length);
    //                //bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
    //                //}
    //            }
    //            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
    //            /* Resource Cleanup */
    //            localFileStream.Close();
    //            ftpStream.Close();
    //            ftpRequest = null;
    //        }
    //        catch (Exception ex) { Console.WriteLine(ex.ToString()); }
    //        return;

    //    }
    //    private void FtpCreateFolder(string folderPath, string fileName, string Type = "")
    //    {
    //        var ftpEnt = (new clsFileUpload(Type)).GetFTPDetails();

    //        string Path = string.Empty;
    //        foreach (var item in folderPath.Split('/'))
    //        {
    //            Path += "/" + item;
    //            try
    //            {
    //                FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpEnt.Host + Path);
    //                request.Method = WebRequestMethods.Ftp.ListDirectory;
    //                request.Credentials = new NetworkCredential(ftpEnt.UserName, ftpEnt.Password);

    //                using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
    //                {
    //                    if (item == fileName)
    //                    {
    //                        FtpWebRequest requestDelete = (FtpWebRequest)WebRequest.Create(ftpEnt.Host + Path);
    //                        request.Method = WebRequestMethods.Ftp.DeleteFile;
    //                        request.Credentials = new NetworkCredential(ftpEnt.UserName, ftpEnt.Password);
    //                        using (FtpWebResponse responseDelete = (FtpWebResponse)request.GetResponse())
    //                        {
    //                        }
    //                    }
    //                    continue;
    //                }
    //            }
    //            catch (WebException ex)
    //            {
    //                if (ex.Response != null)
    //                {
    //                    FtpWebResponse response = (FtpWebResponse)ex.Response;
    //                    if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
    //                    {
    //                        WebRequest request = WebRequest.Create(ftpEnt.Host + Path);
    //                        request.Method = WebRequestMethods.Ftp.MakeDirectory;
    //                        request.Credentials = new NetworkCredential(ftpEnt.UserName, ftpEnt.Password);
    //                        using (var resp = (FtpWebResponse)request.GetResponse())
    //                        {
    //                            continue;
    //                        }
    //                    }
    //                }
    //            }
    //        }

    //    }
    //}

    #region FCS    
    public class FCS_FileUploadEnt
    {
        int chunkSize = 2;
        int chunkInterval = 0;
        string allowFileExtension = "";
        int maxFileUploadSizeInMB = 0;
        public FCS_FileUploadEnt(string controlID, string folderPath)
        {
            FolderPath = folderPath;
            ControlID = controlID;
            Type = "";
            ShowDeleteAction = false;
            MaxChunkSizeInMB = 2;
            ShowAttachmentBtn = true;
            IsDisabled = false;
            ReloadAfterUpload = true;
            IsRequired = false;
            AllowFileExtension = ".jpg,.jpeg,.pdf,.png,.doc,.docx,.xls,.xlsx,.ppt,.pptx,.zip,.iso,.xlsm,.dwg,.mp4,.stp,.3dxml,.sldasm,.dft,.par,.stl";
            //this.AllowFileExtension = "*";
            ActionURL = "/Utility/FileUpload/UploadFile";
            MaxFileUploadSizeInMB = 200000;
            SuccessCallbackFunction = "";
            DocumentLoadCallbackFunction = "";
            DeleteSuccessCallbackFunction = "";
            ShowDownloadBtn = true;
            IsMultipleUploaded = true;
            OrderBy = "";
            IsFilesDisplayFromDefaultURL = true;
            FilesDisplayFromURL = "/Utility/FileUpload/GetDocumentsByTableNameAndTableId";
            OtherFolderPath = "";
            RemoveOnlyCreatedByMe = false;
            Uploader = "";
            HideAttachbtnForSignleUpload = true;
            ViewerId = 1;
            AutoProcessQueue = false;
            FileLocation = 0;
        }

        public string ControlID { get; set; }
        public string ActionURL { get; set; }
        public string FolderPath { get; set; }
        public string Type { get; set; }
        public bool ShowDeleteAction { get; set; }
        public int MaxChunkSizeInMB
        {
            get { return chunkSize; }
            set { this.chunkSize = value; }
        }
        public bool ShowAttachmentBtn { get; set; }
        public int ChunkIntervalInSec
        {
            get
            {
                if (this.chunkInterval == 0)
                {
                    return 1;
                }
                else
                {
                    return chunkInterval;
                }
            }
            set
            {
                this.chunkInterval = value;
            }
        }
        public bool IsDisabled { get; set; }
        public bool ReloadAfterUpload { get; set; }
        public bool IsRequired { get; set; }
        public string AllowFileExtension
        {
            get
            {
                return allowFileExtension;
            }
            set
            {
                //var validextensionArray = (new clsFileUpload(Type)).GetSharePointConfiguration().AllowExtensions.Split(',');
                //var extensionArray = value.Split(',');
                //var finalextension = validextensionArray.Where(x => extensionArray.Contains(x));
                //allowFileExtension = string.Join(allowFileExtension + ",", finalextension);
                allowFileExtension = value;
            }
        }
        public int MaxFileUploadSizeInMB
        {
            get { return maxFileUploadSizeInMB; }
            set { maxFileUploadSizeInMB = value; }
        }
        public string SuccessCallbackFunction { get; set; }
        public string DocumentLoadCallbackFunction { get; set; }
        public bool ShowDownloadBtn { get; set; }
        public bool IsMultipleUploaded { get; set; }
        public string OrderBy { get; set; }
        public bool IsFilesDisplayFromDefaultURL { get; set; }
        public string FilesDisplayFromURL { get; set; }
        public string DeleteSuccessCallbackFunction { get; set; }
        public string OtherFolderPath { get; set; }
        public bool RemoveOnlyCreatedByMe { get; set; }
        public string Uploader { get; set; }
        public bool HideAttachbtnForSignleUpload { get; set; }

        //FCS ADDED
        public string CurrentLocationIp { get; set; }
        public string FilePath { get; set; }
        public string DocumentPath { get; set; }
        public string TableName { get; set; }
        public string TableId { get; set; }
        public int ViewerId { get; set; }
        public bool AutoProcessQueue { get; set; }
        public int FileLocation { get; set; }
    }

    public class cls_Document_Details
    {
        public Int64? DocumentMappingId { get; set; }
        public string TableName { get; set; }
        public Int64? TableId { get; set; }
        public bool IsMainDocument { get; set; }
        public string MainDocumentPath { get; set; }
        public string Status { get; set; }
        public string DocumentName { get; set; }
        public string FileFormate { get; set; }
        public bool IsRevisionForDocMapping { get; set; }
        public string Remark { get; set; }
        public int? FileVersion { get; set; }
        public bool IsLatest { get; set; }
        public string PreFixVesion { get; set; }
        public bool IsFileFromAnotherDropzone { get; set; }
        public string VersionGUID { get; set; }
        public int FileLocation { get; set; }
        public string CurrentLocationIp { get; set; }
        public Int64? OldDocIDForRevision { get; set; }
        public List<string> newPathAfterRevise { get; set; }
        public List<cls_Document_Detail_Data> FileFormatData { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public string EditedBy { get; set; }
        public DateTime? EditedOn { get; set; }
        public int ViewerId { get; set; }
    }

    public class cls_Document_Detail_Data
    {
        public string filename { get; set; }
        public string originalname { get; set; }
        public string destination { get; set; }
    }

    public class cls_copy_params
    {
        public string frompath { get; set; }
        public string topath { get; set; }
        public string deletePath { get; set; }
        public string filename { get; set; }
    }
    #endregion
}