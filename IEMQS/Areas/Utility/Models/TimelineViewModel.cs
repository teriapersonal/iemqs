﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.Utility.Models
{
    public class TimelineViewModel
    {
        public string TimelineTitle { get; set; }
        public string Title { get; set; }
        public string CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public string SubmittedBy { get; set; }
        public Nullable<System.DateTime> SubmittedOn { get; set; }
        public string ReturnedBy { get; set; }
        public Nullable<System.DateTime> ReturnedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string EditedBy { get; set; }
        public Nullable<System.DateTime> EditedOn { get; set; }
        public string ReviewedBy { get; set; }
        public Nullable<System.DateTime> ReviewedOn { get; set; }
        public string FurnaceBy { get; set; }
        public Nullable<System.DateTime> FurnaceOn { get; set; }
        public string InspectBy { get; set; }
        public Nullable<System.DateTime> InspectOn { get; set; }
        public string FDRBy { get; set; }
        public Nullable<System.DateTime> FDROn { get; set; }
        public string DOCAssBy { get; set; }
        public Nullable<System.DateTime> DOCAssOn { get; set; }
        public string SCNBy { get; set; }
        public Nullable<System.DateTime> SCNOn { get; set; }
        public string DocVeriBy { get; set; }
        public Nullable<System.DateTime> DocVeriOn { get; set; }
        public string QAVeriBy { get; set; }
        public Nullable<System.DateTime> QAVeriOn { get; set; }
        public string PMGAccepBy { get; set; }
        public Nullable<System.DateTime> PMGAccepOn { get; set; }
        public string CompletedBy { get; set; }
        public string CompiledBy { get; set; }

        public string HTRType { get; set; }
        public string HTRpqrStatus { get; set; }
        public Nullable<System.DateTime> CompletedOn { get; set; }

        public string LNSubmittedBy { get; set; }
        public Nullable<System.DateTime> LNSubmittedOn { get; set; }

        #region CRS
        public string ReturnedByReviewer { get; set; }
        public Nullable<System.DateTime> ReturnedOnReviewer { get; set; }
        public string ReturnedByApprover { get; set; }
        public Nullable<System.DateTime> ReturnedOnApprover { get; set; }
        #endregion

        #region NCR
        public string QCEngineer { get; set; }
        public string RespDeptMgr { get; set; }
        public string QAEngineer { get; set; }
        public string WEEngineer { get; set; }
        public string DEEngineer { get; set; }
        public string SDE { get; set; }
        public string QCManager { get; set; }
        public string ExternalApprover { get; set; }
        public string ExternClosureAgent { get; set; }
        public string ExtrApprovalReq { get; set; }
        public string ECRRequired { get; set; }
        public string RectificationRequired { get; set; }
        public string ExternClosureReq { get; set; }
        public string NCRClass { get; set; }
        public string Status { get; set; }
        public string ClosedBy { get; set; }

        public Nullable<System.DateTime> ValidateOn { get; set; }
        public Nullable<System.DateTime> RespDeptOn { get; set; }
        public Nullable<System.DateTime> QAApproveOn { get; set; }
        public Nullable<System.DateTime> WEApproveOn { get; set; }
        public Nullable<System.DateTime> DEApproveOn { get; set; }
        public Nullable<System.DateTime> ReviewOn { get; set; }
        public Nullable<System.DateTime> ApproveOn { get; set; }
        public Nullable<System.DateTime> ExtApproveOn { get; set; }
        public Nullable<System.DateTime> PlanImptOn { get; set; }
        public Nullable<System.DateTime> ImplementOn { get; set; }
        public Nullable<System.DateTime> QCClosureOn { get; set; }
        public Nullable<System.DateTime> ExtClosureOn { get; set; }
        public Nullable<System.DateTime> QAClosureOn { get; set; }
        public Nullable<System.DateTime> MarkedForCancelOn { get; set; }
        public Nullable<System.DateTime> CompiledOn { get; set; }
        public Nullable<System.DateTime> ClosedOn { get; set; }
        #endregion


        #region IPI
        public string OfferedBy { get; set; }
        public Nullable<System.DateTime> OfferedOn { get; set; }
        public string PrintedBy { get; set; }
        public Nullable<System.DateTime> PrintedOn { get; set; }
        #region NON-NDE
        public string InspectedBy { get; set; }
        public Nullable<System.DateTime> InspectedOn { get; set; }
        public string OfferedtoCustomerBy { get; set; }
        public Nullable<System.DateTime> OfferedtoCustomerOn { get; set; }
        public string TPIResultBy { get; set; }
        public Nullable<System.DateTime> TPIResultOn { get; set; }
        #endregion

        #region NDE       
        public string RequestGeneratedBy { get; set; }
        public Nullable<System.DateTime> RequestGeneratedOn { get; set; }
        public string ConfirmedBy { get; set; }
        public Nullable<System.DateTime> ConfirmedOn { get; set; }
        #endregion

        #endregion

        #region IMS       
        public string IssuedBy { get; set; }
        public Nullable<System.DateTime> IssuedOn { get; set; }
        public string ReceivedBy { get; set; }
        public Nullable<System.DateTime> ReceivedOn { get; set; }
        #endregion

        #region ASME
        public string ShopInCharge { get; set; }
        public DateTime? ShopInChargeOn { get; set; }
        public string WE { get; set; }
        public DateTime? WEOn { get; set; }
        public string WQTC { get; set; }
        public DateTime? WQTCOn { get; set; }
        #endregion

        #region LTFPS

        public string WEApprovedBy { get; set; }
        public Nullable<System.DateTime> WEApprovedOn { get; set; }

        public string QCApprovedBy { get; set; }
        public Nullable<System.DateTime> QCApprovedOn { get; set; }

        public string CustomerApprovedBy { get; set; }
        public Nullable<System.DateTime> CustomerApprovedOn { get; set; }

        public string PMGReleaseBy { get; set; }
        public Nullable<System.DateTime> PMGReleaseOn { get; set; }

        #endregion


        #region Traveler

        public string QIApprovedBy { get; set; }
        public Nullable<System.DateTime> QIApprovedOn { get; set; }


        public string PlanningApprovedBy { get; set; }
        public Nullable<System.DateTime> PlanningApprovedOn { get; set; }

        public string PlanningReleasedBy { get; set; }
        public Nullable<System.DateTime> PlanningReleasedOn { get; set; }
        public Nullable<System.DateTime> Completedon { get; set; }
        #endregion


        public string TPApprovedBy { get; set; }
        public Nullable<System.DateTime> TPApprovedOn { get; set; }
        public string TPSubmittedBy { get; set; }
        public Nullable<System.DateTime> TPSubmittedOn { get; set; }
        public string TPCreatedBy { get; set; }
        public Nullable<System.DateTime> TPCreatedOn { get; set; }
        public string TPEditedBy { get; set; }
        public Nullable<System.DateTime> TPEditedOn { get; set; }
        public string TPReturnedBy { get; set; }
        public Nullable<System.DateTime> TPReturnedOn { get; set; }
    }
}