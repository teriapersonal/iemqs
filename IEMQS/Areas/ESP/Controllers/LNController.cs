﻿using IEMQS.Areas.Utility.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;
using IEMQSImplementation.ESP;

namespace IEMQS.Areas.ESP.Controllers
{
    [Models.CustomHandleError]
    public class LNController : ApiController
    {
        ESPEntitiesContext dbESP = new ESPEntitiesContext();

        // GET: api/ESP/
        [Route("api/ESP/Test")]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        [Route("api/ESP/TestPost")]
        [HttpPost]
        public IEnumerable<string> Post()
        {
            return new string[] { "Postvalue1", "Postvalue2" };
        }

        [Route("api/ESP/InsertToInventory")]
        [HttpPost]
        public ResultClass InsertToInventory([FromBody]RequestInventory model)
        {
            if (ModelState.IsValid)
            {
                var objResponseData = new ResultClass();
                try
                {
                    var ReqFields = new List<string>();
                    if (!clsImplementationEnum.getMaterialOwners().Contains(model.MaterialOwner))
                    {
                        ReqFields.Add("MaterialOwner");
                    }
                    if (!clsImplementationEnum.getMaterialTypes().Contains(model.MaterialType))
                    {
                        ReqFields.Add("MaterialType");
                    }
                    if (String.IsNullOrWhiteSpace(model.MaterialLocation) && model.TransactionType == clsImplementationEnum.TransactionType.Received.GetStringValue())
                    {
                        ReqFields.Add("MaterialLocation");
                    }
                    if (!clsImplementationEnum.getStageTypes().Contains(model.Stage))
                    {
                        ReqFields.Add("Stage");
                    }
                    if (!clsImplementationEnum.getTransactionTypes().Contains(model.TransactionType))
                    {
                        ReqFields.Add("TransactionType");
                    }
                    if (model.Qty < 0)
                    {
                        ReqFields.Add("Qty");
                    }
                    if (ReqFields.Any())
                    {
                        objResponseData.Status = false;
                        objResponseData.Message = "[" + string.Join(",", ReqFields) + "] value is invalid!";
                        return objResponseData;
                    }
                    using (var db = new ESPEntitiesContext())
                    {
                        if (model.MaterialType == clsImplementationEnum.MaterialType.FXR.GetStringValue() || model.MaterialType == clsImplementationEnum.MaterialType.KIT.GetStringValue())
                        {
                            var objESP130 = new ESP130();
                            objESP130.Project = model.Project;
                            objESP130.MaterialType = model.MaterialType;
                            objESP130.MaterialOwner = model.MaterialOwner;
                            objESP130.Department = model.Department;
                            objESP130.PosNo_FixNo = string.IsNullOrWhiteSpace(model.FindNo) ? model.PosNo_FixNo : model.FindNo;
                            objESP130.ItemId = model.ItemId;
                            objESP130.PCLNo = model.PCLNo;
                            objESP130.Stage = model.Stage;
                            objESP130.MaterialLocation = model.MaterialLocation;
                            objESP130.MaterialSubLocation = model.MaterialSubLocation;
                            objESP130.Qty = model.Qty;
                            objESP130.TransactionType = model.TransactionType;
                            objESP130.ReceivedfromInKit = model.ReceivedfromInKit;
                            objESP130.KitNo = model.KitNo;
                            objESP130.CreatedBy = model.UserPSNo;
                            objESP130.CreatedOn = DateTime.Now;
                            db.ESP130.Add(objESP130);
                            db.SaveChanges();
                            objResponseData.Status = true;
                            objResponseData.Message = "success";
                        }
                        else
                        {
                            var objPCLData = dbESP.SP_ESP_GET_PCR_PCL_DETAILS(model.Location, 0, 1, "", " Project = '" + model.Project + "' AND PCLNumber = '" + model.PCLNumber + "' AND PCLRevision = '" + model.PCLRevision + "' AND t_prtn = '" + model.FindNo + "' ").FirstOrDefault();
                            if (objPCLData != null)
                            {
                                var objESP130 = new ESP130();
                                objESP130.Project = objPCLData.Project.Trim();
                                objESP130.MaterialType = model.MaterialType;
                                objESP130.MaterialOwner = model.MaterialOwner;
                                objESP130.Department = objPCLData.SFCDept.Trim();
                                objESP130.PosNo_FixNo = objPCLData.FindNo;
                                objESP130.ItemId = objPCLData.Item.Trim();
                                objESP130.PCLNo = objPCLData.PCLNumber.Trim();
                                objESP130.Stage = model.Stage;
                                objESP130.MaterialLocation = model.MaterialLocation;
                                objESP130.MaterialSubLocation = model.MaterialSubLocation;
                                objESP130.Qty = model.Qty;
                                objESP130.TransactionType = model.TransactionType;
                                objESP130.ReceivedfromInKit = model.ReceivedfromInKit;
                                objESP130.KitNo = model.KitNo;
                                objESP130.CreatedBy = model.UserPSNo;
                                objESP130.CreatedOn = DateTime.Now;
                                db.ESP130.Add(objESP130);
                                db.SaveChanges();
                                objResponseData.Status = true;
                                objResponseData.Message = "success";
                            }
                            else
                            {
                                objResponseData.Status = false;
                                objResponseData.Message = "Record not found!!";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    objResponseData.Status = false;
                    objResponseData.Message = ex.Message.ToString();
                    return objResponseData;
                }
                return objResponseData;
            }
            else
            {
                var errors = new List<string>();
                if (ModelState != null)
                {
                    foreach (var state in ModelState)
                    {
                        foreach (var error in state.Value.Errors)
                        {
                            errors.Add(error.ErrorMessage);
                        }
                    }
                }
                return new ResultClass { Status = false, Message = string.Join(Environment.NewLine, errors) };
            }
        }

        [Route("api/ESP/ReleasedOutbound")]
        [HttpPost]
        public ResultClass ReleasedOutbound([FromBody]RequestIssueNonPlate model)
        {
            if (ModelState.IsValid)
            {
                var objResponseData = new ResultClass();
                try
                {
                    using (var db = new ESPEntitiesContext())
                    {
                        var objESP112 = db.ESP112.FirstOrDefault(w => w.Project == model.Project && w.NodeKey == model.ItemId && w.SOBKey == model.SOBKey && w.FullKitNo == model.FullKitNo);
                        if (objESP112 != null)
                        {
                            objESP112.Status = "Issued by Store";
                            objESP112.EditedBy = model.UserPSNo;
                            objESP112.EditedOn = DateTime.Now;
                            db.SaveChanges();
                            objResponseData.Status = true;
                            objResponseData.Message = "success";
                        }
                        else
                        {
                            ESP112 ESP112 = new ESP112();
                            string[] str = model.FullKitNo.Split('-');
                            if (str.Length == 3)
                            {
                                int nodeid = Convert.ToInt32(str[2]);
                                var GetFindNo = db.ESP102.Where(x => x.NodeId == nodeid).FirstOrDefault().FindNo;

                                ESP112.Project = model.Project;
                                ESP112.NodeId = nodeid;
                                ESP112.FindNo = GetFindNo;
                                ESP112.NodeKey = model.ItemId;
                                ESP112.FullKitNo = model.FullKitNo;
                                ESP112.SOBKey = model.SOBKey;
                                ESP112.Status = "Issued by Store";
                                ESP112.CreatedBy = model.UserPSNo;
                                ESP112.CreatedOn = DateTime.Now;

                                db.ESP112.Add(ESP112);
                                db.SaveChanges();

                                objResponseData.Status = true;
                                objResponseData.Message = "success";
                            }
                            else
                            {
                                objResponseData.Status = false;
                                objResponseData.Message = "Record not found!!";
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    objResponseData.Status = false;
                    objResponseData.Message = ex.Message.ToString();
                    return objResponseData;
                }
                return objResponseData;
            }
            else
            {
                var errors = new List<string>();
                if (ModelState != null)
                {
                    foreach (var state in ModelState)
                    {
                        foreach (var error in state.Value.Errors)
                        {
                            errors.Add(error.ErrorMessage);
                        }
                    }
                }
                return new ResultClass { Status = false, Message = string.Join(Environment.NewLine, errors) };
            }
        }

        public class ResultClass
        {
            public bool Status { get; set; }
            public string Message { get; set; }
        }
          
        public class RequestInventory
        {
            public int Id { get; set; }
            [Required(ErrorMessage = "Project is required")]
            public string Project { get; set; }
            [Required(ErrorMessage = "MaterialType is required")]
            public string MaterialType { get; set; }
            [Required(ErrorMessage = "MaterialOwner is required")]
            public string MaterialOwner { get; set; }
            public string Department { get; set; }
            public string PosNo_FixNo { get; set; }
            public string ItemId { get; set; }
            public string PCLNo { get; set; }
            [Required(ErrorMessage = "Stage is required")]
            public string Stage { get; set; }
            public string MaterialLocation { get; set; }
            public string MaterialSubLocation { get; set; }
            [Required(ErrorMessage = "Qty is required")]
            public int Qty { get; set; }
            [Required(ErrorMessage = "TransactionType is required")]
            public string TransactionType { get; set; }
            public string ReceivedfromInKit { get; set; }
            public string CreatedBy { get; set; }
            public DateTime CreatedOn { get; set; }
            public string KitNo { get; set; }
            public bool IsAcknowledged { get; set; }
            [Required(ErrorMessage = "UserPSNo is required")]
            public string UserPSNo { get; set; }
            [Required(ErrorMessage = "Location is required")]
            public string Location { get; set; }
            [Required(ErrorMessage = "FindNo is required")]
            public string FindNo { get; set; }
            [Required(ErrorMessage = "PCLNumber is required")]
            public string PCLNumber { get; set; }
            [Required(ErrorMessage = "PCLRevision is required")]
            public string PCLRevision { get; set; }
        }
     
        public class RequestIssueNonPlate
        {
            [Required(ErrorMessage = "Project is required")]
            public string Project { get; set; }
            [Required(ErrorMessage = "FullKitNo is required")]
            public string FullKitNo { get; set; }
            [Required(ErrorMessage = "ItemId is required")]
            public string ItemId { get; set; }
            [Required(ErrorMessage = "SOBKey is required")]
            public string SOBKey { get; set; }
            [Required(ErrorMessage = "UserPSNo is required")]
            public string UserPSNo { get; set; }
        }
    }
  
}
