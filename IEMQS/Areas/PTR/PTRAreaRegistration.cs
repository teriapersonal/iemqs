﻿using System.Web.Mvc;

namespace IEMQS.Areas.PTR
{
    public class PTRAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PTR";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PTR_default",
                "PTR/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}