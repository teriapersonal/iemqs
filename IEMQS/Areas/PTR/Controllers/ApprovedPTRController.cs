﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PTR.Controllers
{
    public class ApprovedPTRController : clsBase
    {
        // GET: PTR/ApprovedPTR
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }
        [HttpPost]
        public ActionResult GetHTDetailGridDataPartial(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            return PartialView("_GetHTDetailGridDataPartial");
        }
        [clsBase.SessionExpireFilter]
        [AllowAnonymous]
        //[clsBase.UserPermissions]
        public ActionResult PTRLineDetails(string HId)
        { 
            int HeaderId = 0;
            if (!string.IsNullOrWhiteSpace(HId))
            {
                HeaderId = Convert.ToInt32(HId);
            }
            PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objPTR001 != null)
            {
                ViewBag.HeaderID = objPTR001.HeaderId;
                ViewBag.Status = objPTR001.Status;
                ViewBag.PTRNo = objPTR001.PTRNo;
                objPTR001.Project = Convert.ToString(db.COM001.Where(x => x.t_cprj == objPTR001.Project).Select(x => x.t_cprj+"-"+ x.t_dsca).FirstOrDefault());
                objPTR001.BU = Convert.ToString(db.COM002.Where(x => x.t_dimx == objPTR001.BU).Select(x => x.t_dimx+"-"+x.t_desc).FirstOrDefault());
                objPTR001.Location = Convert.ToString(db.COM002.Where(x => x.t_dimx == objPTR001.Location).Select(x => x.t_dimx+"-"+ x.t_desc).FirstOrDefault());
            }
            return View(objPTR001);
            //return View();
        }

        [clsBase.SessionExpireFilter]
        [AllowAnonymous]
        //[clsBase.UserPermissions]
        public ActionResult PTRHTLineDetails(string HeaderId,string LineId)
        {
            PTR002 objPTR002 = new PTR002();
   
            objPTR002.HeaderId = Convert.ToInt32(HeaderId);
            
            
            if (LineId == null)
                {
                    PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == objPTR002.HeaderId).FirstOrDefault();
                    objPTR002.PTRNo = objPTR001.PTRNo;
                    objPTR002.RevNo = 0;
                    objPTR002.Project = Convert.ToString(db.COM001.Where(x => x.t_cprj == objPTR001.Project).Select(x => x.t_cprj + "-" + x.t_dsca).FirstOrDefault());
                    objPTR002.BU = Convert.ToString(db.COM002.Where(x => x.t_dimx == objPTR001.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault());
                    objPTR002.Location = Convert.ToString(db.COM002.Where(x => x.t_dimx == objPTR001.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault());
                }
            else
            {
                objPTR002.LineId = Convert.ToInt32(LineId);
                objPTR002 = db.PTR002.Where(x => x.LineId == objPTR002.LineId).FirstOrDefault();
                objPTR002.Project = Convert.ToString(db.COM001.Where(x => x.t_cprj == objPTR002.Project).Select(x => x.t_cprj + "-" + x.t_dsca).FirstOrDefault());
                objPTR002.BU = Convert.ToString(db.COM002.Where(x => x.t_dimx == objPTR002.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault());
                objPTR002.Location = Convert.ToString(db.COM002.Where(x => x.t_dimx == objPTR002.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault());
            }


            return PartialView("PTRHTLineDetails",objPTR002);
        }
        public ActionResult MechDetails(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            return View();
        }
        public ActionResult ChemDetails(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            return View();
        }

        public ActionResult LoadChemDetailData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                var HeaderId = Convert.ToInt32(param.Headerid);
                string condition = string.Empty;
                condition += "where HeaderId=" + HeaderId + " ";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( CouponID like '%" + param.sSearch + "%'  or SampleLocation like '%" + param.sSearch + "%' or Elements like '%" + param.sSearch + "%' or AcceptanceCriteria like '%" + param.sSearch + "%'  or TestingSpecification like '%" + param.sSearch + "%' or Remarks like '%" + param.sSearch + "%' )";
                }

                var lstResult = db.SP_PTR_GET_CHEM_LINE_DETAILS
                                (
                                StartIndex, EndIndex, "", condition
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                                            Convert.ToString(uc.LineId),
                                            Convert.ToString(uc.CouponID),
                                            Convert.ToString(uc.SampleLocation),
                                            Convert.ToString(uc.Elements),
                                            Convert.ToString(uc.AcceptanceCriteria),
                                            Convert.ToString(uc.TestingSpecification),
                                            Convert.ToString(uc.Remarks),
                                        Convert.ToString(uc.LineId),
                                        Convert.ToString(uc.HeaderId)
                                 }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult LoadMechDetailData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var HeaderId = Convert.ToInt32(param.Headerid);
                string condition = string.Empty;
                condition += "where HeaderId=" + HeaderId + " ";


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( CouponID like '%" + param.sSearch + "%'  or TestTypeOrientation like '%" + param.sSearch + "%' or SimpleLocation like '%" + param.sSearch + "%' or TestTemperature like '%" + param.sSearch + "%'  )";
                }

                var lstResult = db.SP_PTR_GET_MECH_LINE_DETAILS
                                (
                                StartIndex, EndIndex, "", condition
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                                     Convert.ToString(uc.LineId),
                                     Convert.ToString(uc.CouponID),
                                     Convert.ToString(uc.TestTypeOrientation),
                                     Convert.ToString(uc.SimpleLocation),
                                     Convert.ToString(uc.TestTemperature),
                                     Convert.ToString(uc.NoofSamples),
                                    Convert.ToString(uc.LineId),
                                    Convert.ToString(uc.HeaderId)
                         }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //public ActionResult SortLoadMechDetailData(JQueryDataTableParamModel param)
        //{
        //    try
        //    {
        //        var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
        //        var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
        //        int StartIndex = param.iDisplayStart + 1;
        //        int EndIndex = param.iDisplayStart + param.iDisplayLength;
                
        //        var HeaderId = Convert.ToInt32(param.Headerid);
        //        string condition = string.Empty;
        //        condition += "where HeaderId=" + HeaderId + " ";

        //        var lstResult = db.SP_PTR_GET_MECH_LINE_DETAILS
        //                        (
        //                        StartIndex, EndIndex, " ORDER BY LineId ASC ", condition
        //                        ).ToList();

        //        var data = (from uc in lstResult
        //                    select new[]
        //                    {
        //                         Convert.ToString(uc.LineId),
        //                         Convert.ToString(uc.CouponID),
        //                         Convert.ToString(uc.TestTypeOrientation),
        //                         Convert.ToString(uc.SimpleLocation),
        //                         Convert.ToString(uc.TestTemperature),
        //                         Convert.ToString(uc.NoofSamples),
        //                            Convert.ToString(uc.LineId),
        //                            Convert.ToString(uc.HeaderId)
        //                 }).ToList();

        //        return Json(new
        //        {
        //            sEcho = Convert.ToInt32(param.sEcho),
        //            iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
        //            aaData = data
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalRecords = "0",
        //            iTotalDisplayRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        public ActionResult MechMetDetails(int? Id, int? HId)
        {
            PTR003 objPTR003 = new PTR003();
            PTR001 objPTR001 = new PTR001();
            ViewBag.HeaderId = HId;
            if (Id == null)
            {
                objPTR001 = db.PTR001.Where(x => x.HeaderId == HId).FirstOrDefault();
                objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR003.HeaderId = objPTR001.HeaderId;
                objPTR003.Project = objPTR001.Project;
                objPTR003.Location = objPTR001.Location;
                objPTR003.BU = objPTR001.BU;
                objPTR003.RevNo = objPTR001.RevNo;
                objPTR003.PTRNo = objPTR001.PTRNo;
                return PartialView("MechMetDetails", objPTR003);
            }
            else
            {
                objPTR003 = db.PTR003.Where(x => x.LineId == Id).FirstOrDefault();
                objPTR003.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR003.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR003.Project = db.COM001.Where(i => i.t_cprj == objPTR003.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR003.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR003.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                return PartialView("MechMetDetails", objPTR003);
            }

        }
        public ActionResult ChemMetDetails(int? Id, int? HId)
        {
            PTR004 objPTR004 = new PTR004();
            PTR001 objPTR001 = new PTR001();
            ViewBag.HeaderId = HId;
            if (Id == null)
            {
                objPTR001 = db.PTR001.Where(x => x.HeaderId == HId).FirstOrDefault();
                objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR004.HeaderId = objPTR001.HeaderId;
                objPTR004.Project = objPTR001.Project;
                objPTR004.Location = objPTR001.Location;
                objPTR004.BU = objPTR001.BU;
                objPTR004.RevNo = objPTR001.RevNo;
                objPTR004.PTRNo = objPTR001.PTRNo;
                return PartialView("ChemMetDetails", objPTR004);
            }
            else
            {
                objPTR004 = db.PTR004.Where(x => x.LineId == Id).FirstOrDefault();
                objPTR004.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR004.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR004.Project = db.COM001.Where(i => i.t_cprj == objPTR004.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR004.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR004.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                return PartialView("ChemMetDetails", objPTR004);
            }
        }
        public ActionResult GetChemDetailsPartial(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            PTR001 objPTR001 = new PTR001();
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            return PartialView("GetChemDetailsPartial", objPTR001);
        }
        public ActionResult GetMechDetailsPartial(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            PTR001 objPTR001 = new PTR001();
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            return PartialView("GetMechDetailsPartial", objPTR001);
        }
        public ActionResult SendtoCompiler(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string compileStatus = clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue();

            try
            {
                PTR001 objPTR001 = db.PTR001.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                objPTR001.Status = compileStatus;
                objPTR001.WE2ApprovalBy = objClsLoginInfo.UserName;
                objPTR001.WE2ApprovalOn = DateTime.Now;
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.CompilerAction.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult LoadPTRHTDetails(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string Condition = string.Empty;
                Condition += "where HeaderId="+ param.Headerid + " ";
                
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    Condition += " and ( CouponID like '%" + param.sSearch + "%'  or HTTime1 like '%" + param.sSearch + "%'  or CouponNo like '%" + param.sSearch + "%'  or HTType1 like '%" + param.sSearch + "%' or HTTemp1 like '%" + param.sSearch + "%'  )"; 
                }     
                var lstResult = db.SP_PTR_GET_HT_LINE_DETAILS
                                (
                                StartIndex, EndIndex, "", Condition
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                                     Convert.ToString(uc.CouponNo),
                                     Convert.ToString(uc.CouponID),
                                     Convert.ToString(uc.HTType1),
                                     Convert.ToString(uc.HTTemp1),
                                     Convert.ToString(uc.HTTime1),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId)
                         }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = 1,
                    iTotalDisplayRecords = 1,
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SavePTRLines(FormCollection fc, PTR002 ptr002)
        {
            PTR002 objPTR002 = new PTR002();
            PTR001 objPTR001 = new PTR001();
            //status from enum
            //string draftStatus = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            try
            {
                int headerID = ptr002.HeaderId;
                objPTR001 = db.PTR001.Where(x => x.HeaderId == headerID).FirstOrDefault();
                if (ptr002.LineId > 0)
                {   
                    objPTR002 = db.PTR002.Where(x => x.LineId == ptr002.LineId).FirstOrDefault();
                    objPTR002.CouponID = ptr002.CouponID;
                    objPTR002.CouponNo = ptr002.CouponNo;
                    objPTR002.HTTemp1 = ptr002.HTTemp1;
                    objPTR002.HTTemp2 = ptr002.HTTemp2;
                    objPTR002.HTTemp3 = ptr002.HTTemp3;
                    objPTR002.HTTemp4 = ptr002.HTTemp4;
                    objPTR002.HTTemp5 = ptr002.HTTemp5;
                    objPTR002.HTTemp6 = ptr002.HTTemp6;
                    objPTR002.HTTime1 = ptr002.HTTime1;
                    objPTR002.HTTime2 = ptr002.HTTime2;
                    objPTR002.HTTime3 = ptr002.HTTime3;
                    objPTR002.HTTime4 = ptr002.HTTime4;
                    objPTR002.HTTime5 = ptr002.HTTime5;
                    objPTR002.HTTime6 = ptr002.HTTime6;
                    objPTR002.HTType1 = ptr002.HTType1;
                    objPTR002.HTType2 = ptr002.HTType2;
                    objPTR002.HTType3 = ptr002.HTType3;
                    objPTR002.HTType4 = ptr002.HTType4;
                    objPTR002.HTType5 = ptr002.HTType5;
                    objPTR002.HTType6 = ptr002.HTType6;
                    objPTR002.EditedBy = objClsLoginInfo.UserName;
                    objPTR002.EditedOn = DateTime.Now;
                    objPTR001.EditedBy = objClsLoginInfo.UserName;
                    objPTR001.EditedOn = DateTime.Now;     
                }
                else
                {
                    objPTR002.HeaderId = headerID;
                    objPTR002.Project = fc["Project"].Split('-')[0];
                    objPTR002.BU = fc["BU"].Split('-')[0];
                    objPTR002.Location = fc["Location"].Split('-')[0];
                    objPTR002.PTRNo = ptr002.PTRNo;
                    objPTR002.RevNo = ptr002.RevNo;
                    objPTR002.CouponID = ptr002.CouponID;
                    objPTR002.CouponNo = ptr002.CouponNo;
                    objPTR002.HTTemp1 = ptr002.HTTemp1;
                    objPTR002.HTTemp2 = ptr002.HTTemp2;
                    objPTR002.HTTemp3 = ptr002.HTTemp3;
                    objPTR002.HTTemp4 = ptr002.HTTemp4;
                    objPTR002.HTTemp5 = ptr002.HTTemp5;
                    objPTR002.HTTemp6 = ptr002.HTTemp6;
                    objPTR002.HTTime1 = ptr002.HTTime1;
                    objPTR002.HTTime2 = ptr002.HTTime2;
                    objPTR002.HTTime3 = ptr002.HTTime3;
                    objPTR002.HTTime4 = ptr002.HTTime4;
                    objPTR002.HTTime5 = ptr002.HTTime5;
                    objPTR002.HTTime6 = ptr002.HTTime6;
                    objPTR002.HTType1 = ptr002.HTType1;
                    objPTR002.HTType2 = ptr002.HTType2;
                    objPTR002.HTType3 = ptr002.HTType3;
                    objPTR002.HTType4 = ptr002.HTType4;
                    objPTR002.HTType5 = ptr002.HTType5;
                    objPTR002.HTType6 = ptr002.HTType6;              
                    objPTR002.RevNo = 0;
                    objPTR002.CreatedBy = objClsLoginInfo.UserName;
                    objPTR002.CreatedOn = DateTime.Now;
                    db.PTR002.Add(objPTR002);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                if (ptr002.LineId > 0)
                {
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Update;
                }
                else
                {
                    objResponseMsg.Value = clsImplementationMessage.PTMTCTQMessages.Insert;
                }
                //objResponseMsg.Status = objCTQ001.Status;
                //objResponseMsg.CTQRev = Convert.ToInt32(objCTQ001.CTQRev);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult PTRHTDetails(int HeaderId)
        {
            PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            ViewBag.HeaderId = HeaderId;
            objPTR001.Project = Convert.ToString(db.COM001.Where(x => x.t_cprj == objPTR001.Project).Select(x => x.t_cprj + "-" + x.t_dsca).FirstOrDefault());
            objPTR001.BU = Convert.ToString(db.COM002.Where(x => x.t_dimx == objPTR001.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault());
            objPTR001.Location = Convert.ToString(db.COM002.Where(x => x.t_dimx == objPTR001.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault());
            return View(objPTR001);
        }
        public class ResponceMsgWithStatus : clsHelper.ResponseMsg
        {
            public int CTQRev;
            public string Status;
        }
        public class ResponceMsgWithHeaderId : clsHelper.ResponseMsg
        {
            public int HeaderId;
        }
        public class ResponceMsgProjectBU : clsHelper.ResponseMsg
        {
            public string BU;
            public string Project;
        }
        public JsonResult LoadPTRHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
           
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.RetQC3.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.RetQA3.GetStringValue() + "')";
                }
                else
                {
                     strWhere += "1=1";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (ptr1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or PTRNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "ptr1.BU", "ptr1.Location");
                var lstResult = db.SP_PTR_GETHEADER
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                                    Convert.ToString(""),
                                    Convert.ToString(uc.PTRNo),
                                    Convert.ToString(uc.Project),
                                    Convert.ToString(uc.Location),
                                    Convert.ToString(uc.Status),
                                    Convert.ToString(uc.CouponCategory),
                                    Convert.ToString(uc.CouponNo),
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.HeaderId)
                         }).ToList();
             
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = 1,
                    iTotalDisplayRecords = 1,
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

    }
}