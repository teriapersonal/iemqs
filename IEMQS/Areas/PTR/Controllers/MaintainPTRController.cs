﻿using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PTR.Controllers
{
    public class MaintainPTRController : clsBase
    {
        clsManager objClsManager = new clsManager();

        public const int PTRNoUptoDigit = 4;
        // GET: PTR/MaintainPTR
        #region Index
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            ViewBag.User = objClsLoginInfo.UserName;
            ViewBag.location = objClsLoginInfo.Location;
            return View();
        }
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("GetHeaderGridDataPartial");
        }

        [HttpPost]
        public JsonResult NextPTR(int? HId, string status)
        {
            if (HId != 0)
            {
                ViewBag.Status = status;
                var user = objClsLoginInfo.UserName;
                string strWhere = string.Empty;
                string cond = string.Empty;
                //System.Collections.Generic.IEnumerable<IEMQSImplementation.PTR001> lst;
                List<UserRoleDetails> role = GetRoleByLocation(user, objClsLoginInfo.Location);
                if (status == "Pending")
                {
                    int cnt = 0;

                    if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))   //WE3
                    {
                        //Draft, Submitted to QC / LAB / WE2 (Until Lab ID is not Generated, Return at any stage)
                        cond += "(status in ('" + clsImplementationEnum.PTRStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.RetWE2.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.RetQC3.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.RetQA3.GetStringValue() + @"') and (LABId = '' or LABId IS null))";
                        //and CouponCategory in('" + clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue() + "','" + clsImplementationEnum.CouponCat.ConsQualf.GetStringValue() + "','" + clsImplementationEnum.CouponCat.MockUp.GetStringValue() + "','" + clsImplementationEnum.CouponCat.PerfQualf.GetStringValue() + "','" + clsImplementationEnum.CouponCat.ProcQualf.GetStringValue() + "') ";
                        cnt++;
                    }
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), StringComparison.OrdinalIgnoreCase)))   //WE2
                    {
                        //if (cnt > 0) cond += " or ";
                        cond += "(status in ('" + clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue() + "'))";
                        cnt++;
                    }
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QA3.GetStringValue(), StringComparison.OrdinalIgnoreCase))) //QA3 
                                                                                                                                                                //|| x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WTC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))  //QA3 nad WTC3
                    {
                        //if (cnt > 0) cond += " or ";
                        cond += "(status = '" + clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue() + "' AND (LABId = '' or LABId IS null))";
                        cnt++;
                    }
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))  //QC3
                    {
                        //if (cnt > 0) cond += " or ";
                        cond += "(status in ('" + clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue() + "'))";// and CouponCategory in('" + clsImplementationEnum.CouponCat.WaterAnls.GetStringValue() + "'))";
                        cnt++;
                    }
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                         || x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.MCC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                         || x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.SCC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))  //PMG3 and MCC3 and SCC3
                    {
                        //if (cnt > 0) cond += " or ";
                        cond += "(status in ('" + clsImplementationEnum.PTRStatus.Draft.GetStringValue() + "', '" + clsImplementationEnum.PTRStatus.RetQC3.GetStringValue() + "', '" + clsImplementationEnum.PTRStatus.RetQA3.GetStringValue() + "') and CouponCategory in('" + clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue() + "')) ";
                        cnt++;
                    }
                    if (cond == "")
                        strWhere += "1=2";
                    else
                        strWhere += "1=1 and (" + cond + ")";
                }
                if (status == "Acknowledge")
                {

                    if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))   //WE3
                        strWhere += "status != '" + clsImplementationEnum.PTRStatus.Closed.GetStringValue() + "' AND ";
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QA3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))   //WE3
                        strWhere += "status = '" + clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue() + "' AND ";
                    strWhere += "(LABId != '' or LABId is not null)";
                }
                strWhere += " and Location='" + objClsLoginInfo.Location + "'";
                var lstResult = db.SP_PTR_GETHEADER(0, 0, "", strWhere).ToList();
                var q = lstResult.Where(i => i.HeaderId > HId).OrderBy(i => i.HeaderId).FirstOrDefault();
                return Json(q, JsonRequestBehavior.AllowGet);
            }
            return Json(null);

        }

        public JsonResult PrevPTR(int? HId, string status)
        {
            if (HId != 0)
            {
                ViewBag.Status = status;
                var user = objClsLoginInfo.UserName;
                string strWhere = string.Empty;
                string cond = string.Empty;
                //System.Collections.Generic.IEnumerable<IEMQSImplementation.PTR001> lst;
                List<UserRoleDetails> role = GetRoleByLocation(user, objClsLoginInfo.Location);
                if (status == "Pending")
                {
                    int cnt = 0;

                    if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))   //WE3
                    {
                        //Draft, Submitted to QC / LAB / WE2 (Until Lab ID is not Generated, Return at any stage)
                        cond += "(status in ('" + clsImplementationEnum.PTRStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.RetWE2.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.RetQC3.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.RetQA3.GetStringValue() + @"') and (LABId = '' or LABId IS null))";
                        //and CouponCategory in('" + clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue() + "','" + clsImplementationEnum.CouponCat.ConsQualf.GetStringValue() + "','" + clsImplementationEnum.CouponCat.MockUp.GetStringValue() + "','" + clsImplementationEnum.CouponCat.PerfQualf.GetStringValue() + "','" + clsImplementationEnum.CouponCat.ProcQualf.GetStringValue() + "') ";
                        cnt++;
                    }
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), StringComparison.OrdinalIgnoreCase)))   //WE2
                    {
                        //if (cnt > 0) cond += " or ";
                        cond += "(status in ('" + clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue() + "'))";
                        cnt++;
                    }
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QA3.GetStringValue(), StringComparison.OrdinalIgnoreCase))) //QA3 
                                                                                                                                                                //|| x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WTC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))  //QA3 nad WTC3
                    {
                        //if (cnt > 0) cond += " or ";
                        cond += "(status = '" + clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue() + "' AND (LABId = '' or LABId IS null))";
                        cnt++;
                    }
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))  //QC3
                    {
                        //if (cnt > 0) cond += " or ";
                        cond += "(status in ('" + clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue() + "'))";// and CouponCategory in('" + clsImplementationEnum.CouponCat.WaterAnls.GetStringValue() + "'))";
                        cnt++;
                    }
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                         || x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.MCC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                         || x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.SCC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))  //PMG3 and MCC3 and SCC3
                    {
                        //if (cnt > 0) cond += " or ";
                        cond += "(status in ('" + clsImplementationEnum.PTRStatus.Draft.GetStringValue() + "', '" + clsImplementationEnum.PTRStatus.RetQC3.GetStringValue() + "', '" + clsImplementationEnum.PTRStatus.RetQA3.GetStringValue() + "') and CouponCategory in('" + clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue() + "')) ";
                        cnt++;
                    }
                    if (cond == "")
                        strWhere += "1=2";
                    else
                        strWhere += "1=1 and (" + cond + ")";
                }
                if (status == "Acknowledge")
                {

                    if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))   //WE3
                        strWhere += "status != '" + clsImplementationEnum.PTRStatus.Closed.GetStringValue() + "' AND ";
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QA3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))   //WE3
                        strWhere += "status = '" + clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue() + "' AND ";
                    strWhere += "(LABId != '' or LABId is not null)";
                }
                strWhere += " and Location='" + objClsLoginInfo.Location + "'";
                var lstResult = db.SP_PTR_GETHEADER(0, 0, "", strWhere).ToList();
                var q = lstResult.Where(i => i.HeaderId > HId).OrderBy(i => i.HeaderId).FirstOrDefault();
                return Json(q, JsonRequestBehavior.AllowGet);
            }
            return Json(null);

        }

        public JsonResult LoadPTRHeaderData(JQueryDataTableParamModel param, string CTQCompileStatus)
        {

            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1
                                && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx).FirstOrDefault();
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                string strWhere = string.Empty;
                string cond = string.Empty;
                //System.Collections.Generic.IEnumerable<IEMQSImplementation.PTR001> lst;
                List<UserRoleDetails> role = GetRoleByLocation(user, objClsLoginInfo.Location);
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    //strWhere += "1=1 and status in('" + clsImplementationEnum.PTRStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.RetWE2.GetStringValue() + "')";
                    int cnt = 0;

                    if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))   //WE3
                    {
                        //Draft, Submitted to QC / LAB / WE2 (Until Lab ID is not Generated, Return at any stage)
                        cond += "(status in ('" + clsImplementationEnum.PTRStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.RetWE2.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.RetQC3.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.RetQA3.GetStringValue() + @"') and (LABId = '' or LABId IS null))";
                        //and CouponCategory in('" + clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue() + "','" + clsImplementationEnum.CouponCat.ConsQualf.GetStringValue() + "','" + clsImplementationEnum.CouponCat.MockUp.GetStringValue() + "','" + clsImplementationEnum.CouponCat.PerfQualf.GetStringValue() + "','" + clsImplementationEnum.CouponCat.ProcQualf.GetStringValue() + "') ";
                        cnt++;
                    }
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), StringComparison.OrdinalIgnoreCase)))   //WE2
                    {
                        //if (cnt > 0) cond += " or ";
                        cond += "(status in ('" + clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue() + "'))";
                        cnt++;
                    }
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QA3.GetStringValue(), StringComparison.OrdinalIgnoreCase))) //QA3 
                                                                                                                                                                //|| x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WTC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))  //QA3 nad WTC3
                    {
                        //if (cnt > 0) cond += " or ";
                        cond += "(status = '" + clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue() + "' AND (LABId = '' or LABId IS null))";
                        cnt++;
                    }
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))  //QC3
                    {
                        //if (cnt > 0) cond += " or ";
                        cond += "(status in ('" + clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue() + "'))";// and CouponCategory in('" + clsImplementationEnum.CouponCat.WaterAnls.GetStringValue() + "'))";
                        cnt++;
                    }
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                         || x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.MCC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                         || x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.SCC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))  //PMG3 and MCC3 and SCC3
                    {
                        //if (cnt > 0) cond += " or ";
                        cond += "(status in ('" + clsImplementationEnum.PTRStatus.Draft.GetStringValue() + "', '" + clsImplementationEnum.PTRStatus.RetQC3.GetStringValue() + "', '" + clsImplementationEnum.PTRStatus.RetQA3.GetStringValue() + "') and CouponCategory in('" + clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue() + "')) ";
                        cnt++;
                    }
                    if (cond == "")
                        strWhere += "1=2";
                    else
                        strWhere += "1=1 and (" + cond + ")";
                    //strWhere += "1=1 and status in('" + clsImplementationEnum.PTRStatus.Draft.GetStringValue() + "','"  + clsImplementationEnum.PTRStatus.RetWE2.GetStringValue() + "')";
                    //lst = (from objPTR001 in db.PTR001
                    //           where objPTR001.Status == "Draft"
                    //          select objPTR001).ToList();
                }
                else if (param.CTQCompileStatus.ToUpper() == "ACKNOWLEDGE")
                {
                    if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))   //WE3
                        strWhere += "status != '" + clsImplementationEnum.PTRStatus.Closed.GetStringValue() + "' AND ";
                    else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QA3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))   //WE3
                        strWhere += "status = '" + clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue() + "' AND ";
                    strWhere += "(LABId != '' or LABId is not null)";
                }
                else
                {
                    strWhere += "1=1 ";
                    //if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), StringComparison.OrdinalIgnoreCase) || x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))
                    //{
                    //    strWhere += " AND status != '" + clsImplementationEnum.PTRStatus.Draft.GetStringValue() + "'";
                    //}
                    //else if (role.Any(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QA3.GetStringValue(), StringComparison.OrdinalIgnoreCase)))
                    //{
                    //    strWhere += " AND status in('" + clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue() + "','" + clsImplementationEnum.PTRStatus.Closed.GetStringValue() + "')";
                    //}


                    //lst = (from objPTR001 in db.PTR001
                    //        select objPTR001).ToList();
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //strWhere += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (ptr1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or PTRNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or QualityProject like '%" + param.sSearch + "%' or CouponCategory like '%" + param.sSearch + "%' or CouponNo like '%" + param.sSearch + "%')";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "bcom2.t_desc",
                                        "lcom2.t_desc",
                                        "ptr1.Project+'-'+com1.t_dsca",
                                        "PTRNo",
                                        "RevNo",
                                        "Status",
                                        "QualityProject",
                                        "CouponCategory",
                                        "LABId",
                                        "LabIdCreatedBy+'-'+com3.t_name",
                                        "LABIdCreatedOn",
                                        "CouponNo"
                                        };
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                    strWhere += Manager.MakeDefaultWhere(user, "ptr1.BU", "ptr1.Location");
                }
                else if (param.SearchFilter != null)
                {
                    List<SearchFilter> searchflt = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<SearchFilter>>(param.SearchFilter);
                    var fltTypeOfTest = searchflt.FirstOrDefault(f => f.ColumnName == "TypeOfTest");
                    if (fltTypeOfTest != null && !string.IsNullOrWhiteSpace(fltTypeOfTest.Value))
                    {
                        switch (fltTypeOfTest.Value.ToLower())
                        {
                            case "both":
                                strWhere += " AND MechTest = 'true' AND ChemicalTest = 'true' ";
                                break;
                            case "mechanical":
                                strWhere += " AND MechTest = 'true' AND ChemicalTest = 'false' ";
                                break;
                            case "chemical":
                                strWhere += " AND MechTest = 'false' AND ChemicalTest = 'true' ";
                                break;
                            default:
                                strWhere += " AND 1 = 0 ";
                                break;
                        }
                    }

                    if (searchflt != null)
                    {
                        foreach (var item in searchflt)
                        {
                            item.ColumnName = item.ColumnName.Replace("PTRInitiator", "com33.t_name");
                        }
                    }
                    var serchFilter = new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(searchflt.Where(w => w.ColumnName != "TypeOfTest"));
                    strWhere += Manager.MakeDatatableForSearch(serchFilter);
                }
                strWhere += " and Location='" + objClsLoginInfo.Location + "'";
                var lstResult = db.SP_PTR_GETHEADER
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();
                //PTR001 objPTR001 = new PTR001();
                //var lst = (from objPTR001 in db.PTR001
                //           where objPTR001.Status == "Draft"
                //           select objPTR001).ToList();
                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.PTRNo),
                                "R" +Convert.ToString(uc.RevNo),
                                uc.LABId,
                                uc.LabIdCreatedOn.HasValue? uc.LabIdCreatedOn.Value.ToString("dd/MM/yyyy"):"",
                                Convert.ToString(uc.Project),
                                //Convert.ToString(uc.QualityProject),
                                //Convert.ToString(clsImplementationEnum.GetStringValue((clsImplementationEnum.CouponCat)Enum.Parse(typeof(clsImplementationEnum.CouponCat),uc.CouponCategory))),
                                Convert.ToString(uc.CouponCategory),
                                Convert.ToString(uc.CouponNo),
                                GetTestType(uc.MechTest,uc.ChemicalTest),
                                Convert.ToString(uc.PTRInitiator),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.Location),
                           //Convert.ToString(uc.HeaderId),
                           "<center><a class=''   href='"+WebsiteURL + "/PTR/MaintainPTR/PTRDetails?HId="+Convert.ToInt32(uc.HeaderId)+"&Status="+CTQCompileStatus+"'><i  class='fa fa-eye'></i></a><a class='' href='javascript: void(0)' onclick=ShowTimeline('/PTR/MaintainPTR/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"');><i style='margin-left:5px;cursor:pointer;' title='Timeline' class='fa fa-clock-o '></i></a>"
                           +getHistoryBtn(uc.HeaderId,uc.RevNo,uc.Status)
                           +""+getCopyBtn(uc.HeaderId,uc.Status,uc.CouponCategory,role)
                           +""+"<i  data-modal='' id='btnPS' name='btnPS' style='cursor:pointer;margin-left:5px;' Title='Print Report' class='fa fa-print' onClick=ParameterSlip(" + uc.HeaderId + ")></i>"+"</center>",
                           Convert.ToString(uc.HeaderId)
                         }).ToList();
                //var data = (from uc in lstResult
                //            select new[]
                //           {
                //           Convert.ToString(uc.Status),
                //           Convert.ToString(uc.BU),
                //           Convert.ToString(uc.Location),
                //           Convert.ToString(uc.CTQNo),
                //           Convert.ToString("R"+uc.CTQRev),
                //            Convert.ToString(uc.Status),
                //           Convert.ToString(uc.HeaderId),
                //           Convert.ToString(uc.HeaderId)
                //           }).ToList();

                //        var data = (from uc in lstResult
                //                    select new[]
                //                   {
                //var data = (new[] {
                //            Convert.ToString("prj1"),
                //           Convert.ToString("bu"),
                //           Convert.ToString("paw"),
                //           Convert.ToString("123"),
                //           Convert.ToString("R1"),
                //            Convert.ToString("PENDING"),
                //           Convert.ToString("as"),
                //           Convert.ToString("as")
                //           }.ToList());
                //List < List < string >> data = new List<List<string>>();
                //data.Add(new List<string> { "a", "b" ,"fad","ffa","sa","gdf","dsaf","sda"});
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    //iTotalRecords = (lst.Count() > 0  ? lst.Count() : 0),
                    //iTotalDisplayRecords = (lst.Count() > 0  ? lst.Count() : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Print
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult PrintReport1()
        {
            return View();
        }
        //[SessionExpireFilter, AllowAnonymous, UserPermissions]
        //public ActionResult PrintReport2()
        //{
        //    return View();
        //}
        [HttpPost]
        public JsonResult getLocation()
        {
            int LOC = Convert.ToInt32(clsImplementationEnum.BLDNumber.LOC.GetStringValue());
            var lstLocations = db.COM002.Where(x => x.t_dtyp == LOC).Distinct().ToList();
            var items = (from li in lstLocations
                         select new
                         {
                             id = li.t_dimx,
                             text = li.t_desc,
                         }).ToList();

            return Json(items, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetQualityProjectResult()
        {
            var lstQualityProject = (from a in db.QMS010
                                     orderby a.QualityProject
                                     select new { qualityCode = a.QualityProject, qualityDescription = a.QualityProject }).Distinct().ToList();
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetProjectForDropdownResult()
        {
            var lstQualityProject = (from a in db.COM001
                                     orderby a.t_cprj
                                     select new { id = a.t_cprj, text = a.t_cprj + " - " + a.t_dsca }).Distinct().ToList();
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult GetRangeofQualityProject(string QprojectF, string QprojectT)
        {
            List<String> lstQualityProject = (from a in db.QMS010
                                              where (a.QualityProject.CompareTo(QprojectF) >= 0 && a.QualityProject.CompareTo(QprojectT) <= 0)
                                              orderby a.QualityProject
                                              select a.QualityProject).Distinct().ToList();
            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Role and Authorization
        public List<UserRoleDetails> GetRoleByLocation(string User, string location)
        {
            //List<int> lstRole = (from a in db.ATH001
            //                     where a.Employee == User && a.Location == location
            //                     select a.Role).ToList();
            List<UserRoleDetails> role = (from a in db.ATH001
                                          join b in db.ATH004 on a.Role equals b.Id
                                          where a.Employee.Equals(User, StringComparison.OrdinalIgnoreCase) && a.Location.Equals(location)
                                          select new UserRoleDetails() { UserRoleId = a.Role, UserRoleDesc = b.Role }).Distinct().ToList();
            return role;
        }
        public ActionResult GetUserRoleByLocation(string User, string location)
        {
            var lstRole = GetRoleByLocation(User, location);
            return Json(lstRole, JsonRequestBehavior.AllowGet);
        }
        public Boolean chkAuth(string cc, string status)
        {
            List<UserRoleDetails> role = GetRoleByLocation(objClsLoginInfo.UserName, objClsLoginInfo.Location);
            List<String> st3 = new List<String> { clsImplementationEnum.PTRStatus.Draft.GetStringValue(), clsImplementationEnum.PTRStatus.RetWE2.GetStringValue(), clsImplementationEnum.PTRStatus.RetQA3.GetStringValue(), clsImplementationEnum.PTRStatus.Closed.GetStringValue() };
            List<String> cc3 = new List<string> { clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue(), clsImplementationEnum.CouponCat.ConsQualf.GetStringValue(), clsImplementationEnum.CouponCat.MockUp.GetStringValue(), clsImplementationEnum.CouponCat.PerfQualf.GetStringValue(), clsImplementationEnum.CouponCat.ProcQualf.GetStringValue() };
            List<String> st31 = new List<string> { clsImplementationEnum.PTRStatus.RetQC3.GetStringValue() };
            List<String> cc31 = new List<string> { clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue() };
            if (role.Where(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())   //WE3
            {
                if ((st3.Contains(status) && cc3.Contains(cc)) || (st31.Contains(status) && cc31.Contains(cc)))
                    return true;
            }
            if (role.Where(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())   //WE2
            {
                if ((new[] { clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue() }.Contains(status))) // || (new[] { clsImplementationEnum.PTRStatus.RetQC3.GetStringValue() }.Contains(status) && new[] { clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue() }.Contains(cc)) || (new[] { clsImplementationEnum.PTRStatus.RetQA3.GetStringValue() }.Contains(status) && new[] { clsImplementationEnum.CouponCat.ConsQualf.GetStringValue(), clsImplementationEnum.CouponCat.MockUp.GetStringValue(), clsImplementationEnum.CouponCat.ProcQualf.GetStringValue() }.Contains(cc)))
                    return true;
            }
            if (role.Where(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QA3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                        || x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WTC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())  //QA3 and WTC3
            {
                if (new[] { clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue() }.Contains(status))
                    return true;
            }
            if (role.Where(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())  //QC3
            {
                if ((new[] { clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue() }.Contains(status)) || (new[] { clsImplementationEnum.PTRStatus.Draft.GetStringValue(), clsImplementationEnum.PTRStatus.RetQA3.GetStringValue(), clsImplementationEnum.PTRStatus.Closed.GetStringValue() }.Contains(status) && new[] { clsImplementationEnum.CouponCat.WaterAnls.GetStringValue() }.Contains(cc))) //|| (st31.Contains(status) && new[] { clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue(), clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue() }.Contains(cc)))
                    return true;
            }
            if (role.Where(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                        || x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.MCC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                        || x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.SCC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())  //PMG3 and MCC3 and SCC3
            {
                if (new[] { clsImplementationEnum.PTRStatus.Draft.GetStringValue(), clsImplementationEnum.PTRStatus.RetQC3.GetStringValue(), clsImplementationEnum.PTRStatus.RetQA3.GetStringValue(), clsImplementationEnum.PTRStatus.Closed.GetStringValue() }.Contains(status) && new[] { clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue() }.Contains(cc))
                    return true;
            }
            return false;
        }
        #endregion

        public ActionResult PTRDetail(int? PDHId)
        {
            PAM009 objPAM009 = new PAM009();
            if (PDHId != null)
            {
                objPAM009 = db.PAM009.Where(x => x.PDHId == PDHId).FirstOrDefault();
            }

            return View(objPAM009);
        }
        public ActionResult SaveDetails(FormCollection fc, PAM009 pam009)
        {
            PAM009 objPAM009 = new PAM009();
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            //string Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
            try
            {
                if (pam009.PDHId > 0)
                {
                    objPAM009 = db.PAM009.Where(x => x.PDHId == pam009.PDHId).FirstOrDefault();
                }
                objPAM009.HeaderId = pam009.HeaderId;
                objPAM009.ProposalDesignRev = pam009.ProposalDesignRev;
                objPAM009.Status = pam009.Status;
                objPAM009.Licensor = pam009.Licensor;
                objPAM009.HPC1 = pam009.HPC1;
                objPAM009.HPC3 = pam009.HPC3;
                objPAM009.Combination = pam009.Combination;
                objPAM009.ReferPlanningInput = pam009.ReferPlanningInput;
                objPAM009.ForgedShell = pam009.ForgedShell;
                objPAM009.ASMEYes = pam009.ASMEYes;
                //objPAM009.ASMENo = pam009.ASMENo;
                objPAM009.IBR = pam009.IBR;
                objPAM009.PEDATEX = pam009.PEDATEX;
                objPAM009.CRN = pam009.CRN;
                objPAM009.DOSH = pam009.DOSH;
                objPAM009.SELO = pam009.SELO;
                objPAM009.GOST = pam009.GOST;
                objPAM009.LocalRegulationOthers = pam009.LocalRegulationOthers;
                objPAM009.Warm = pam009.Warm;
                objPAM009.Cold = pam009.Cold;
                objPAM009.Hot = pam009.Hot;
                objPAM009.ShellRollingNotApplicable = pam009.ShellRollingNotApplicable;
                objPAM009.ForgedShellWithProjection = pam009.ForgedShellWithProjection;
                objPAM009.ForgedShellWithIntegralNub = pam009.ForgedShellWithIntegralNub;
                objPAM009.ThkOnPlate = pam009.ThkOnPlate;
                objPAM009.NoInsideProjection = pam009.NoInsideProjection;
                objPAM009.NubShellNotApplicable = pam009.NubShellNotApplicable;
                objPAM009.SinglePiece = pam009.SinglePiece;
                objPAM009.Piece2 = pam009.Piece2;
                objPAM009.Piece3 = pam009.Piece3;
                objPAM009.TubeSheetNotApplicable = pam009.TubeSheetNotApplicable;
                objPAM009.TubeSheetOthers = pam009.TubeSheetOthers;
                objPAM009.SingleLayer = pam009.SingleLayer;
                objPAM009.DoubleLayer = pam009.DoubleLayer;
                objPAM009.Clad = pam009.Clad;
                objPAM009.RefractoryAtShop = pam009.RefractoryAtShop;
                objPAM009.RefractoryAtSite = pam009.RefractoryAtSite;
                objPAM009.OverlayNotApplicable = pam009.OverlayNotApplicable;
                objPAM009.Part4 = pam009.Part4;
                objPAM009.Part5 = pam009.Part5;
                objPAM009.CatalystNotApplicable = pam009.CatalystNotApplicable;
                objPAM009.SinglePiece2 = pam009.SinglePiece2;
                objPAM009.Half2 = pam009.Half2;
                objPAM009.Petals4 = pam009.Petals4;
                objPAM009.Petals6 = pam009.Petals6;
                objPAM009.HeadConstructionOthers = pam009.HeadConstructionOthers;
                objPAM009.WithCrown = pam009.WithCrown;
                objPAM009.HeadConstructionCold = pam009.HeadConstructionCold;
                objPAM009.HeadConstructionWarm = pam009.HeadConstructionWarm;
                objPAM009.HeadConstructionHot = pam009.HeadConstructionHot;
                objPAM009.FormingInIndia = pam009.FormingInIndia;
                objPAM009.ReadyHeadImported = pam009.ReadyHeadImported;
                objPAM009.LSRYes = pam009.LSRYes;
                //objPAM009.LSRNo = pam009.LSRNo;
                objPAM009.InternalYes = pam009.InternalYes;
                objPAM009.InternalNo = pam009.InternalNo;
                objPAM009.SiteInstallationScope = pam009.SiteInstallationScope;
                objPAM009.OtherPoints = pam009.OtherPoints;
                if (pam009.PDHId > 0)
                {
                    objPAM009.EditedBy = objClsLoginInfo.UserName;
                    objPAM009.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Value = "Record Updated Succesfully";
                }
                else
                {
                    objPAM009.CreatedBy = objClsLoginInfo.UserName;
                    objPAM009.CreatedOn = DateTime.Now;
                    db.PAM009.Add(objPAM009);
                    db.SaveChanges();
                    objResponseMsg.Value = "Record Added Succesfully";
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetMaintainProjectNo(string term)
        {
            db.Configuration.ProxyCreationEnabled = false;
            //var res = db.PTR001.Select(c => c.QualityProject).Distinct().ToList();

            var res = db.PTR001.Where(c => c.QualityProject.Contains(term)).Select(x => new { x.QualityProject, x.HeaderId, x.PTRNo }).Distinct().ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult FetchMaintainProjectNo(string maintainqualityproject)
        {
            db.Configuration.ProxyCreationEnabled = false;
            var res = db.PTR001.Where(i => i.QualityProject.Equals(maintainqualityproject, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        #region PTR Header
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult PTRDetails(int? Hid, string Status)
        {
            var user = objClsLoginInfo.UserName;
            ViewBag.User = objClsLoginInfo.UserName;
            ViewBag.location = objClsLoginInfo.Location;
            ViewBag.Status = Status;
            PTR001 objPTR001 = new PTR001();

            var project = Manager.getProjectsByUser(user);
            ViewBag.Project = new SelectList(project, "projectCode", "projectDescription");
            List<UserRoleDetails> role = GetRoleByLocation(user, objClsLoginInfo.Location);
            //List<String> CouponCat = new List<String>();
            ViewBag.IsValid = "false";
            ViewBag.GenerateLabId = "false";
            var CouponCategory = new List<CouponCat>();
            if (role.Where(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())//.Contains(3))
            {
                ViewBag.IsValid = "true";
                CouponCategory.Add(new CouponCat() { Value = clsImplementationEnum.CouponCat.ConsQualf.GetStringValue(), Text = clsImplementationEnum.CouponCat.ConsQualf.GetStringValue() });
                CouponCategory.Add(new CouponCat() { Value = clsImplementationEnum.CouponCat.MockUp.GetStringValue(), Text = clsImplementationEnum.CouponCat.MockUp.GetStringValue() });
                CouponCategory.Add(new CouponCat() { Value = clsImplementationEnum.CouponCat.PerfQualf.GetStringValue(), Text = clsImplementationEnum.CouponCat.PerfQualf.GetStringValue() });
                CouponCategory.Add(new CouponCat() { Value = clsImplementationEnum.CouponCat.ProcQualf.GetStringValue(), Text = clsImplementationEnum.CouponCat.ProcQualf.GetStringValue() });
                CouponCategory.Add(new CouponCat() { Value = clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue(), Text = clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue() });
            }
            if (role.Where(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())//role.Contains(15))
            {
                CouponCategory.Add(new CouponCat() { Value = clsImplementationEnum.CouponCat.WaterAnls.GetStringValue(), Text = clsImplementationEnum.CouponCat.WaterAnls.GetStringValue() });
            }
            if (role.Where(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.PMG3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                        || x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.MCC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                        || x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.SCC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())//role.Contains(24) || role.Contains(65) || role.Contains(66))
            {
                CouponCategory.Add(new CouponCat() { Value = clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue(), Text = clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue() });
            }
            if (role.Where(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.QA3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any() || role.Where(x => x.UserRoleDesc.Equals(clsImplementationEnum.UserRoleName.WTC3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
            {
                ViewBag.GenerateLabId = "true";
            }
            //CouponCategory.OrderBy(x=>x.Value);
            ViewBag.CouCat = CouponCategory.OrderBy(x => x.Value);
            ////header id for view mode
            if (Hid != null)
            {
                var objHeader = Convert.ToInt32(Hid);
                string draftStatus = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                //string returnedStatus = clsImplementationEnum.CTQStatus.Returned.GetStringValue();

                objPTR001 = db.PTR001.Where(x => x.HeaderId == objHeader).FirstOrDefault();
                var PTRLocation = objPTR001.Location;
                //ViewBag.InspectionAgency1 = db.GLB002.Where(a => a.BU == objPTR001.BU && a.Location == objPTR001.Location && a.Code == objPTR001.InspectionAgency1).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                //ViewBag.InspectionAgency2 = db.GLB002.Where(a => a.BU == objPTR001.BU && a.Location == objPTR001.Location && a.Code == objPTR001.InspectionAgency2).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                //ViewBag.InspectionAgency3 = db.GLB002.Where(a => a.BU == objPTR001.BU && a.Location == objPTR001.Location && a.Code == objPTR001.InspectionAgency3).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                //ViewBag.InspectionStage = db.QMS002.Where(a => a.BU == objPTR001.BU && a.Location == objPTR001.Location && a.StageCode == objPTR001.InspectionStage).Select(a => a.StageCode + " - " + a.StageDesc).FirstOrDefault();
                ViewBag.InspectionStage = getCategoryDesc("TPI Intervention", objPTR001.BU, objPTR001.Location, objPTR001.InspectionStage);
                ViewBag.InspectionStage2 = getCategoryDesc("TPI Intervention", objPTR001.BU, objPTR001.Location, objPTR001.InspectionStage2);
                ViewBag.InspectionStage3 = getCategoryDesc("TPI Intervention", objPTR001.BU, objPTR001.Location, objPTR001.InspectionStage3);
                ViewBag.InspectionAgency1 = getCategoryDesc2("Inspection Agency", objPTR001.BU, objPTR001.Location, objPTR001.InspectionAgency1);
                ViewBag.InspectionAgency2 = getCategoryDesc2("Inspection Agency", objPTR001.BU, objPTR001.Location, objPTR001.InspectionAgency2);
                ViewBag.InspectionAgency3 = getCategoryDesc2("Inspection Agency", objPTR001.BU, objPTR001.Location, objPTR001.InspectionAgency3);

                objPTR001.WE2ID = db.COM003.Where(a => a.t_psno == objPTR001.WE2ID).Select(a => a.t_psno + " - " + a.t_name).FirstOrDefault();
                objPTR001.QC3ID = db.COM003.Where(a => a.t_psno == objPTR001.QC3ID).Select(a => a.t_psno + " - " + a.t_name).FirstOrDefault();
                objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                if (chkAuth(objPTR001.CouponCategory, objPTR001.Status))
                {
                    ViewBag.auth = "yes";
                }
                else
                    ViewBag.auth = "no";
                if ((!objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Draft.GetStringValue())) || ViewBag.auth == "no")
                {
                    //ViewBag.CouCat = new[] { objPTR001.CouponCategory };
                    var cc = new List<CouponCat>();
                    cc.Add(new CouponCat() { Value = objPTR001.CouponCategory, Text = objPTR001.CouponCategory });
                    ViewBag.CouCat = cc;
                }
                ViewBag.Project = objPTR001.QualityProject;
                ViewBag.Action = "edit";
                ViewBag.IsAllowToCopy = CheckPTRCopyAllow(objPTR001.CouponCategory, objPTR001.Status, role);
                ViewBag.IsDifferentLocationUser = (PTRLocation.Trim().ToUpper() != objClsLoginInfo.Location.Trim().ToUpper());
            }
            else
            {
                objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                objPTR001.RevNo = 0;
                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1
                                && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx + "-" + b.t_desc).FirstOrDefault();
                objPTR001.Location = location;
                ViewBag.IsAllowToCopy = false;
                ViewBag.IsDifferentLocationUser = false;
            }
            var t_dimx = objPTR001.Location.Split('-')[0].Trim();
            ViewBag.lstAWSClass = db.WPS002.Where(w => w.Location.Equals(t_dimx, StringComparison.OrdinalIgnoreCase) && !w.AWSClass.StartsWith("-")).Select(s => s.AWSClass).Distinct().ToList();
            return View(objPTR001);
            //return View();
        }
        [HttpPost]
        public ActionResult SavePTRHeader(FormCollection fc, PTR001 ptr001)
        {
            PTR001 objPTR001 = new PTR001();
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            //string Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
            try
            {
                var axy = fc["HTDetail"];
                string location = fc["Location"].Split('-')[0].Trim();
                if (ptr001.HeaderId > 0)
                {
                    objPTR001 = db.PTR001.Where(m => m.HeaderId == ptr001.HeaderId).FirstOrDefault();
                }
                objPTR001.QualityProject = ptr001.QualityProject;
                objPTR001.Project = ptr001.Project.Split('-')[0].Trim();
                objPTR001.BU = fc["BU"].Split('-')[0].Trim();
                objPTR001.Location = location;
                //objPTR001.CodeSpecification = ptr001.CodeSpecification;

                objPTR001.Status = ptr001.Status;
                if (ptr001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                {
                    objPTR001.RevNo = Convert.ToInt32(objPTR001.RevNo) + 1;
                    objPTR001.InitiatorRemarks = null;
                    objPTR001.WE2Remarks = null;
                    objPTR001.QC3Remarks = null;
                    objPTR001.QA3WTC3Remarks = null;
                    objPTR001.SubmittedBy = null;
                    objPTR001.SubmittedOn = null;
                    objPTR001.WE2ApprovalBy = null;
                    objPTR001.WE2ApprovalOn = null;
                    objPTR001.WE2ReturnBy = null;
                    objPTR001.WE2ReturnOn = null;
                    objPTR001.QC3ApprovalBy = null;
                    objPTR001.QC3ApprovalOn = null;
                    objPTR001.QC3ReturnBy = null;
                    objPTR001.QC3ReturnOn = null;
                    objPTR001.QA3WTC3ApprovalBy = null;
                    objPTR001.QA3WTC3ApprovalOn = null;
                    objPTR001.QA3WTC3ReturnBy = null;
                    objPTR001.QA3WTC3ReturnOn = null;
                    objPTR001.RetractedBy = null;
                    objPTR001.RetractedOn = null;
                    foreach (var i in objPTR001.PTR002)
                    {
                        i.RevNo = objPTR001.RevNo;
                    }
                    foreach (var i in objPTR001.PTR003)
                    {
                        i.RevNo = objPTR001.RevNo;
                    }
                    foreach (var i in objPTR001.PTR004)
                    {
                        i.RevNo = objPTR001.RevNo;
                    }
                    foreach (var i in objPTR001.PTR005)
                    {
                        i.RevNo = objPTR001.RevNo;
                    }
                    foreach (var i in objPTR001.PTR006)
                    {
                        i.RevNo = objPTR001.RevNo;
                    }
                    objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                }

                objPTR001.CouponCategory = ptr001.CouponCategory;
                objPTR001.CouponNo = ptr001.CouponNo;
                objPTR001.SeamNo = ptr001.SeamNo;
                objPTR001.MaterialSpec = ptr001.MaterialSpec;
                objPTR001.Size = ptr001.Size;
                objPTR001.LTFPSNo = ptr001.LTFPSNo;
                objPTR001.WPSNo = ptr001.WPSNo;
                objPTR001.WeldingProcess = ptr001.WeldingProcess;
                objPTR001.FillerWire = ptr001.FillerWire;
                objPTR001.Position = ptr001.Position;
                objPTR001.BrandName = ptr001.BrandName;
                objPTR001.BatchNo = ptr001.BatchNo;
                objPTR001.WeldThickness = ptr001.WeldThickness;
                objPTR001.ApplicableCodeSpec = ptr001.ApplicableCodeSpec;
                objPTR001.WQTno = ptr001.WQTno;
                objPTR001.PQRnO = ptr001.PQRnO;
                objPTR001.WelderID = ptr001.WelderID;
                //objPTR001.InspectionAgency1 = ("" + ptr001.InspectionAgency1).Split('-')[0].Trim();
                //objPTR001.InspectionAgency2 = ("" + ptr001.InspectionAgency2).Split('-')[0].Trim();
                //objPTR001.InspectionAgency3 = ("" + ptr001.InspectionAgency3).Split('-')[0].Trim();
                //objPTR001.InspectionStage = ("" + ptr001.InspectionStage).Split('-')[0].Trim();
                objPTR001.InspectionAgency1 = ptr001.InspectionAgency1;
                objPTR001.InspectionAgency2 = ptr001.InspectionAgency2;
                objPTR001.InspectionAgency3 = ptr001.InspectionAgency3;
                objPTR001.InspectionStage = ptr001.InspectionStage;
                objPTR001.InspectionStage2 = ptr001.InspectionStage2;
                objPTR001.InspectionStage3 = ptr001.InspectionStage3;
                objPTR001.NoofCoupons = ptr001.NoofCoupons;
                objPTR001.MechTest = ptr001.MechTest;
                objPTR001.ChemicalTest = ptr001.ChemicalTest;
                objPTR001.HTDetail = ptr001.HTDetail;
                objPTR001.StampedBy = ptr001.StampedBy;
                objPTR001.CGDP = ptr001.CGDP;
                objPTR001.WE2ID = ("" + ptr001.WE2ID).Split('-')[0].Trim();
                objPTR001.QC3ID = ("" + ptr001.QC3ID).Split('-')[0].Trim();
                objPTR001.InitiatorRemarks = ptr001.InitiatorRemarks;
                objPTR001.WE2Remarks = ptr001.WE2Remarks;
                objPTR001.QC3Remarks = ptr001.QC3Remarks;
                objPTR001.QA3WTC3Remarks = ptr001.QA3WTC3Remarks;
                if (chkAuth(objPTR001.CouponCategory, objPTR001.Status))
                {
                    if (ptr001.HeaderId > 0)
                    {
                        //var a = fc["DocNo"];
                        //objPTR001.DocNo = !string.IsNullOrEmpty(fc["DocNo"]) ? Convert.ToInt32(fc["DocNo"]) : 0;
                        //objPTR001.PTRNo = ptr001.PTRNo;
                        objPTR001.EditedBy = objClsLoginInfo.UserName;
                        objPTR001.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = objPTR001.PTRNo + " Updated Succesfully";
                        objResponseMsg.HeaderId = objPTR001.HeaderId;
                        objResponseMsg.RevNo = objPTR001.RevNo;
                        objResponseMsg.PTRNo = objPTR001.PTRNo;
                    }
                    else
                    {
                        ResponceMsgProjectBU obj = GetMaxPRTNo();
                        objPTR001.DocNo = obj.DocNo;
                        objPTR001.PTRNo = obj.PTRNo;

                        if (db.PTR001.Any(x => x.PTRNo.ToLower() == objPTR001.PTRNo.ToLower()))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = objPTR001.PTRNo + " this PTR No already exists, Please refresh the page.";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }

                        objPTR001.RevNo = 0;
                        objPTR001.CreatedOn = DateTime.Now;
                        objPTR001.CreatedBy = objClsLoginInfo.UserName;
                        db.PTR001.Add(objPTR001);
                        db.SaveChanges();
                        objResponseMsg.Value = objPTR001.PTRNo + " Added Succesfully";
                        objResponseMsg.HeaderId = objPTR001.HeaderId;
                        objResponseMsg.RevNo = objPTR001.RevNo;
                        objResponseMsg.PTRNo = objPTR001.PTRNo;
                    }



                    //ATH008 objATH008 = db.ATH008.Where(x => x.Id == 2).FirstOrDefault();
                    //clsUploadCopy.CopyFolderContents(folderPath, "PTR001/" + objPTR001.HeaderId + "/R" + (objPTR001.RevNo + 1), objATH008);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "You don't have permission to save this record";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                objResponseMsg.Key = true;
                objResponseMsg.HeaderId = objPTR001.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
                // objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateInitiatorRemarks(int HeaderId, string text)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objPTR001 != null)
                {
                    objPTR001.InitiatorRemarks = text;
                    objPTR001.EditedBy = objClsLoginInfo.UserName;
                    objPTR001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Initiator Remarks successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Header not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateWE2Remarks(int HeaderId, string text)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objPTR001 != null)
                {
                    objPTR001.WE2Remarks = text;
                    objPTR001.EditedBy = objClsLoginInfo.UserName;
                    objPTR001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "WE2 Remarks successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Header not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateQC3Remarks(int HeaderId, string text)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objPTR001 != null)
                {
                    objPTR001.QC3Remarks = text;
                    objPTR001.EditedBy = objClsLoginInfo.UserName;
                    objPTR001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "QC3 Remarks successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Header not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateQA3WTC3Remarks(int HeaderId, string text)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objPTR001 != null)
                {
                    objPTR001.QA3WTC3Remarks = text;
                    objPTR001.EditedBy = objClsLoginInfo.UserName;
                    objPTR001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "QA3 Remarks successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Header not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RetractStatus(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PTR001 objPTR001 = db.PTR001.Where(u => u.HeaderId == HeaderId).FirstOrDefault();
                if (objPTR001 != null)
                {
                    objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                    objPTR001.RetractedBy = objClsLoginInfo.UserName;
                    objPTR001.RetractedOn = DateTime.Now;
                    objPTR001.EditedBy = objClsLoginInfo.UserName;
                    objPTR001.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Retract.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GenerateLABID(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PTR001 objPTR001 = db.PTR001.Where(u => u.HeaderId == HeaderId).FirstOrDefault();
                if (objPTR001 != null)
                {
                    #region Generate New Lab Id
                    string NewLabId = string.Empty;
                    if (objPTR001.LabId == null)
                    {
                        NewLabId = db.Database.SqlQuery<string>("SELECT dbo.FN_GENERATE_PTR_LAD_ID('" + objPTR001.HeaderId + "')").FirstOrDefault();
                        objPTR001.LabIdCreatedBy = objClsLoginInfo.UserName;
                        objPTR001.LabIdCreatedOn = DateTime.Now;
                        objPTR001.LabId = NewLabId;
                        db.SaveChanges();
                    }
                    #endregion

                    if (!string.IsNullOrEmpty(NewLabId))
                    {
                        #region Send Mail
                        string emailTo = objPTR001.CreatedBy;
                        string cc = objClsLoginInfo.UserName;

                        if (!string.IsNullOrWhiteSpace(emailTo))
                        {
                            Hashtable _ht = new Hashtable();
                            EmailSend _objEmail = new EmailSend();
                            _objEmail.MailCc = Manager.GetMailIdFromPsNo(cc);
                            _ht["[Initiator]"] = Manager.GetUserNameFromPsNo(objPTR001.CreatedBy);
                            _ht["[PTRNo]"] = objPTR001.PTRNo;
                            _ht["[LabID]"] = NewLabId;
                            _ht["[Regards]"] = Manager.GetUserNameFromPsNo(objClsLoginInfo.UserName);
                            MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.PTR.LabId).SingleOrDefault();
                            _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.CreatedBy); //initiator
                            _ht["[Subject]"] = "PTR" + objPTR001.PTRNo;
                            _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                        }
                        #endregion

                        objResponseMsg.item = NewLabId;
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.LABID.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult UpdateMechenicalAndChemicalRemarks(int HeaderId, string Remarks, string Type)
        {
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            try
            {
                var objPTR001 = db.PTR001.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                if (objPTR001 != null)
                {
                    if (Type == "mech")
                    {
                        objPTR001.MechenicalRemarks = Remarks;
                    }
                    else if (Type == "chem")
                    {
                        objPTR001.ChemicalRemarks = Remarks;
                    }
                    objResponseMsg.Key = true;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.PLCMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region old code
        public ActionResult MechMetDetails(int? Id, int? HId)
        {
            PTR003 objPTR003 = new PTR003();
            PTR001 objPTR001 = new PTR001();
            ViewBag.HeaderId = HId;
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HId).FirstOrDefault();
            ViewBag.status = objPTR001.Status;
            ViewBag.cc = objPTR001.CouponCategory;
            if (chkAuth(objPTR001.CouponCategory, objPTR001.Status))
                ViewBag.auth = "yes";
            else
                ViewBag.auth = "no";
            if (Id > 0)
            {
                objPTR003 = db.PTR003.Where(x => x.LineId == Id).FirstOrDefault();
                objPTR003.TestTypeOrientation = db.GLB002.Where(a => a.BU == objPTR003.BU && a.Location == objPTR003.Location && a.Code == objPTR003.TestTypeOrientation).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                //objPTR003.SimpleLocation = db.GLB002.Where(a => a.BU == objPTR003.BU && a.Location == objPTR003.Location && a.Code == objPTR003.SimpleLocation).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                //objPTR003.TestTemperature = db.GLB002.Where(a => a.BU == objPTR003.BU && a.Location == objPTR003.Location && a.Code == objPTR003.TestTemperature).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                objPTR003.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR003.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR003.Project = db.COM001.Where(i => i.t_cprj == objPTR003.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR003.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR003.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                return PartialView("MechMetDetails", objPTR003);
            }
            else
            {

                objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR003.HeaderId = objPTR001.HeaderId;
                objPTR003.Project = objPTR001.Project;
                objPTR003.Location = objPTR001.Location;
                objPTR003.BU = objPTR001.BU;
                objPTR003.RevNo = objPTR001.RevNo;
                objPTR003.PTRNo = objPTR001.PTRNo;
                return PartialView("MechMetDetails", objPTR003);
            }
        }
        public ActionResult HTLineDetails(int? Id, int? HId)
        {
            PTR002 objPTR002 = new PTR002();
            PTR001 objPTR001 = new PTR001();
            ViewBag.HeaderId = HId;
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HId).FirstOrDefault();
            ViewBag.status = objPTR001.Status;
            ViewBag.cc = objPTR001.CouponCategory;
            if (chkAuth(objPTR001.CouponCategory, objPTR001.Status))
                ViewBag.auth = "yes";
            else
                ViewBag.auth = "no";
            if (Id > 0)
            {
                objPTR002 = db.PTR002.Where(x => x.LineId == Id).FirstOrDefault();
                objPTR002.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR002.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR002.Project = db.COM001.Where(i => i.t_cprj == objPTR002.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR002.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR002.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                return PartialView("_HTLineDetails", objPTR002);
            }
            else
            {
                objPTR002.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR002.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR002.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR002.HeaderId = objPTR001.HeaderId;
                objPTR002.RevNo = objPTR001.RevNo;
                objPTR002.PTRNo = objPTR001.PTRNo;
                int Headerid = Convert.ToInt32(HId);
                var couNo = (from a in db.PTR002
                             where a.HeaderId == Headerid
                             select a).Max(a => a.CouponNo);
                if (couNo != null)
                    objPTR002.CouponNo = couNo + 1;
                else
                    objPTR002.CouponNo = 1;
                return PartialView("_HTLineDetails", objPTR002);
            }
        }
        public ActionResult ChemLineDetails(int? Id, int? HId)
        {
            PTR004 objPTR004 = new PTR004();
            PTR001 objPTR001 = new PTR001();
            ViewBag.HeaderId = HId;
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HId).FirstOrDefault();
            ViewBag.status = objPTR001.Status;
            ViewBag.cc = objPTR001.CouponCategory;
            if (chkAuth(objPTR001.CouponCategory, objPTR001.Status))
                ViewBag.auth = "yes";
            else
                ViewBag.auth = "no";
            if (Id > 0)
            {
                objPTR004 = db.PTR004.Where(x => x.LineId == Id).FirstOrDefault();
                //objPTR004.SampleLocation = db.GLB002.Where(a => a.BU == objPTR004.BU && a.Location == objPTR004.Location && a.Code == objPTR004.SampleLocation).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                objPTR004.Elements = db.GLB002.Where(a => a.BU == objPTR004.BU && a.Location == objPTR004.Location && a.Code == objPTR004.Elements).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                objPTR004.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR004.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR004.Project = db.COM001.Where(i => i.t_cprj == objPTR004.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR004.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR004.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                return PartialView("_ChemLineDetails", objPTR004);
            }
            else
            {
                objPTR004.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR004.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR004.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR004.HeaderId = objPTR001.HeaderId;
                objPTR004.RevNo = objPTR001.RevNo;
                objPTR004.PTRNo = objPTR001.PTRNo;
                return PartialView("_ChemLineDetails", objPTR004);
            }

        }
        public ActionResult HeatPlateLineDetails(int? Id, int? HId)
        {
            PTR005 objPTR005 = new PTR005();
            PTR001 objPTR001 = new PTR001();
            ViewBag.HeaderId = HId;
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HId).FirstOrDefault();
            if (objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue()) || objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue()))
            {
                ViewBag.partNoRequired = true;
            }
            ViewBag.status = objPTR001.Status;
            ViewBag.cc = objPTR001.CouponCategory;
            if (chkAuth(objPTR001.CouponCategory, objPTR001.Status))
                ViewBag.auth = "yes";
            else
                ViewBag.auth = "no";
            if (Id > 0)
            {
                objPTR005 = db.PTR005.Where(x => x.LineId == Id).FirstOrDefault();
                objPTR005.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR005.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR005.Project = db.COM001.Where(i => i.t_cprj == objPTR005.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR005.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR005.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                return PartialView("_HeatPlateLineDetails", objPTR005);
            }
            else
            {
                objPTR005.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR005.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR005.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR005.HeaderId = objPTR001.HeaderId;
                objPTR005.RevNo = objPTR001.RevNo;
                objPTR005.PTRNo = objPTR001.PTRNo;
                return PartialView("_HeatPlateLineDetails", objPTR005);
            }
        }
        public ActionResult GetMechDetailsPartial(int HeaderId) //remove
        {
            ViewBag.HeaderId = HeaderId;
            PTR001 objPTR001 = new PTR001();
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            return PartialView("_GetMechDetailsPartial", objPTR001);
        }
        public ActionResult GetHTGridDataPartial(int HeaderId)  //remove
        {
            ViewBag.HeaderId = HeaderId;
            PTR001 objPTR001 = new PTR001();
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            return PartialView("_GetHTGridDataPartial", objPTR001);
        }
        public ActionResult GetChemGridDataPartial(int HeaderId)    //remove
        {
            ViewBag.HeaderId = HeaderId;
            PTR001 objPTR001 = new PTR001();
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            return PartialView("_GetChemGridDataPartial", objPTR001);
        }
        #endregion

        #region Mechanical Details
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult MechDetails(int HeaderId)
        {
            var user = objClsLoginInfo.UserName;
            ViewBag.HeaderId = HeaderId;
            PTR001 objPTR001 = new PTR001();
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            string location = objPTR001.Location;
            string bu = objPTR001.BU;

            ViewBag.IsDifferentLocationUser = (objPTR001.Location.Trim().ToUpper() != objClsLoginInfo.Location.Trim().ToUpper());
            ViewBag.TestTypeOrientation = Manager.GetSubCatagories("Test Type Orientation", objPTR001.BU, objPTR001.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.CouponID = db.PTR002.Where(x => x.HeaderId == HeaderId).Select(i => new CategoryData { Value = i.CouponID, Code = i.CouponID, CategoryDescription = i.CouponID }).ToList();
            //ViewBag.SampleLocation = GetCatagoryList("", "Sample Location Master", objPTR001.BU, objPTR001.Location);
            //ViewBag.TestTemperature = GetCatagoryList("", "Test Temperature Master", objPTR001.BU, objPTR001.Location);
            objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            if (chkAuth(objPTR001.CouponCategory, objPTR001.Status))
                ViewBag.auth = "yes";
            else
                ViewBag.auth = "no";

            List<UserRoleDetails> role = GetRoleByLocation(user, objClsLoginInfo.Location);
            if (role.Where(x => x.UserRoleDesc == clsImplementationEnum.UserRoleName.QA1.GetStringValue()).Count() > 0
                || role.Where(x => x.UserRoleDesc == clsImplementationEnum.UserRoleName.QA2.GetStringValue()).Count() > 0
                || role.Where(x => x.UserRoleDesc == clsImplementationEnum.UserRoleName.QA3.GetStringValue()).Count() > 0)
                ViewBag.IsRoleQA = true;
            else
                ViewBag.IsRoleQA = false;

            string condition = string.Empty;
            condition += "where HeaderId=" + HeaderId + " ";
            var lstResult = db.SP_PTR_GET_MECH_LINE_DETAILS(null, null, "", condition).ToList();
            List<string> testTypes = lstResult.Select(x => x.TestTypeOrientation).Distinct().ToList();
            List<string> testTypesDesc = new List<string>();

            foreach (var type in testTypes)
            {
                string code = db.PTR000.Where(x => x.TestType == type && x.Location == location && x.BU == bu).Select(x => x.Code).FirstOrDefault();
                string description = db.GLB002.Where(x => x.Code == code && x.Location == location && x.BU == bu).Select(x => x.Description).FirstOrDefault();
                if (description != null)
                    testTypesDesc.Add(description);
            }

            if (testTypesDesc.Count() > 0)
                ViewBag.ReportButtonVisible = true;
            else
                ViewBag.ReportButtonVisible = false;


            return View(objPTR001);
        }
        public ActionResult LoadMechDetailData(string auth, JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var HeaderId = Convert.ToInt32(param.Headerid);
                string condition = string.Empty;
                condition += "where HeaderId=" + HeaderId + " ";
                bool isEditable = (auth == "yes" ? true : false) && (isLinesEditable(HeaderId));
                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                var lstSampleLocation = GetCatagoryList("", "Sample Location Master", objPTR001.BU, objPTR001.Location);
                var lstTestTemperature = GetCatagoryList("", "Test Temperature Master", objPTR001.BU, objPTR001.Location);
                //lstSampleLocation = lstSampleLocation.Select(i => new CategoryData { Code = i.Description, CategoryDescription = i.Description, Description = i.Description }).ToList();
                //lstTestTemperature = lstTestTemperature.Select(i => new CategoryData { Code = i.Description, CategoryDescription = i.Description, Description = i.Description }).ToList();
                //if (!string.IsNullOrWhiteSpace(param.sSearch))
                //{
                //    condition += " and ( LineId like '%" + param.sSearch + "%'  or CouponID like '%" + param.sSearch + "%'  or TestTypeOrientation like '%" + param.sSearch + "%' or SimpleLocation like '%" + param.sSearch + "%' or TestTemperature like '%" + param.sSearch + "%' or NoofSamples like '%" + param.sSearch + "%'or AcceptanceCriteria like '%" + param.sSearch + "%'or TestingSpecification like '%" + param.sSearch + "%'or Remarks like '%" + param.sSearch + "%' )";
                //}
                string[] columnName = { "CouponID", "TestTypeOrientation", "Orientation", "SimpleLocation", "TestTemperature", "NoofSamples", "AcceptanceCriteria", "TestingSpecification", "Remarks" };
                condition += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PTR_GET_MECH_LINE_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, condition
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                    "0",
                    GenerateAutoCompleteonBlur(newRecordId,"txtCouponID","","",false,"","CouponID",false,"25")+""+Helper.GenerateHidden(newRecordId,"CouponID"),
                    GenerateAutoCompleteonBlur(newRecordId,"txtTestTypeOrientation","","",false,"","TestTypeOrientation")+""+Helper.GenerateHidden(newRecordId,"TestTypeOrientation"),
                    GenerateAutoCompleteonBlur(newRecordId,"txtOrientation","","",false,"","Orientation")+""+Helper.GenerateHidden(newRecordId,"Orientation"),
                    MultiSelectDropdown(newRecordId,lstSampleLocation,"", false, "","SimpleLocation"),
                    MultiSelectDropdown(newRecordId,lstTestTemperature,"", false, "","TestTemperature"),
                    //GenerateAutoCompleteonBlur(newRecordId,"txtSimpleLocation","","",false,"","SimpleLocation",false,"100")+""+Helper.GenerateHidden(newRecordId,"SimpleLocation"),
                    //GenerateAutoCompleteonBlur(newRecordId,"txtTestTemperature","","",false,"","TestTemperature",false,"100")+""+Helper.GenerateHidden(newRecordId,"TestTemperature"),
                    GenerateTextbox(newRecordId,"NoofSamples","","","","",false,"",""),
                    GenerateTextbox(newRecordId,"AcceptanceCriteria","","","","",false,"",""),
                    GenerateTextbox(newRecordId,"TestingSpecification","","","","",false,"",""),
                    GenerateTextbox(newRecordId,"Remarks","","","","",false,"",""),
                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewStage();" ),
                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.ROW_NO),
                                isEditable?GenerateAutoCompleteonBlur(uc.LineId,"txtCouponID",uc.CouponID,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","CouponID",false,"25")+""+Helper.GenerateHidden(uc.LineId,"CouponID",uc.CouponID):uc.CouponID,
                                isEditable?GenerateAutoCompleteonBlur(uc.LineId,"txtTestTypeOrientation",getCategoryDesc2("Test Type Orientation",objPTR001.BU,objPTR001.Location,uc.TestTypeOrientation),"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","TestTypeOrientation",false)+""+Helper.GenerateHidden(uc.LineId,"TestTypeOrientation",uc.TestTypeOrientation):getCategoryDesc("Test Type Orientation",objPTR001.BU,objPTR001.Location,uc.TestTypeOrientation),
                                //MultiSelectDropdown(uc.LineId,lstOrientation,"Transverse", !isEditable, "UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","Orientation"),
                                isEditable?GenerateAutoCompleteonBlur(uc.LineId,"txtOrientation",uc.Orientation,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","Orientation",false,"25")+""+Helper.GenerateHidden(uc.LineId,"Orientation",uc.Orientation):uc.Orientation,

                                MultiSelectDropdown(uc.LineId,lstSampleLocation,uc.SimpleLocation, !isEditable, "UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","SimpleLocation"),
                                MultiSelectDropdown(uc.LineId,lstTestTemperature,uc.TestTemperature, !isEditable, "UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","TestTemperature"),
                                //isEditable?GenerateAutoCompleteonBlur(uc.LineId,"txtSimpleLocation",uc.SimpleLocation,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","SimpleLocation",false,"100")+""+Helper.GenerateHidden(uc.LineId,"SimpleLocation",uc.SimpleLocation):uc.SimpleLocation,
                                //isEditable?GenerateAutoCompleteonBlur(uc.LineId,"txtTestTemperature",uc.TestTemperature,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","TestTemperature",false,"100")+""+Helper.GenerateHidden(uc.LineId,"TestTemperature",uc.TestTemperature):uc.TestTemperature,
                                isEditable?GenerateTextbox(uc.LineId,"NoofSamples",uc.NoofSamples,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"",""):uc.NoofSamples,
                                isEditable?GenerateTextbox(uc.LineId,"AcceptanceCriteria",uc.AcceptanceCriteria,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"",""):uc.AcceptanceCriteria,
                                isEditable?GenerateTextbox(uc.LineId,"TestingSpecification",uc.TestingSpecification,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"",""):uc.TestingSpecification,
                                isEditable?GenerateTextbox(uc.LineId,"Remarks",uc.Remarks,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"",""):uc.Remarks,
                                "<center>"+HTMLActionString(uc.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteLines(" + uc.LineId + "," + uc.HeaderId + ")",!isEditable)+"</center>",
                                Helper.GenerateHidden(uc.LineId, "HeaderId", Convert.ToString(uc.HeaderId)),
                         }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveMechDetails(FormCollection fc)
        {
            PTR003 objPTR003 = new PTR003();
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            //string Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
            try
            {
                //string location = fc["Location"].Split('-')[0].Trim();
                //string auth = fc["auth"];
                //string status = fc["status"];
                //if (auth.Equals("yes") && (status.Equals("Draft") || status.Equals("Closed")))
                //{
                int newRowIndex = 0;
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId" + newRowIndex]) ? Convert.ToInt32(fc["HeaderId" + newRowIndex]) : 0;
                if (refHeaderId > 0)
                {
                    PTR001 objPTR001 = db.PTR001.Where(m => m.HeaderId == refHeaderId).FirstOrDefault();
                    objPTR003.HeaderId = objPTR001.HeaderId;
                    objPTR003.Project = objPTR001.Project;
                    objPTR003.BU = objPTR001.BU;
                    objPTR003.Location = objPTR001.Location;
                    objPTR003.PTRNo = objPTR001.PTRNo;
                    if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                    {
                        objPTR003.RevNo = Convert.ToInt32(objPTR001.RevNo) + 1;
                        objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                        objPTR001.RevNo = Convert.ToInt32(objPTR001.RevNo) + 1;
                        objPTR001.InitiatorRemarks = null;
                        objPTR001.WE2Remarks = null;
                        objPTR001.QC3Remarks = null;
                        objPTR001.QA3WTC3Remarks = null;
                        objPTR001.SubmittedBy = null;
                        objPTR001.SubmittedOn = null;
                        objPTR001.WE2ApprovalBy = null;
                        objPTR001.WE2ApprovalOn = null;
                        objPTR001.WE2ReturnBy = null;
                        objPTR001.WE2ReturnOn = null;
                        objPTR001.QC3ApprovalBy = null;
                        objPTR001.QC3ApprovalOn = null;
                        objPTR001.QC3ReturnBy = null;
                        objPTR001.QC3ReturnOn = null;
                        objPTR001.QA3WTC3ApprovalBy = null;
                        objPTR001.QA3WTC3ApprovalOn = null;
                        objPTR001.QA3WTC3ReturnBy = null;
                        objPTR001.QA3WTC3ReturnOn = null;
                        objPTR001.RetractedBy = null;
                        objPTR001.RetractedOn = null;
                        foreach (var i in objPTR001.PTR002)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR003)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR004)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR005)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR006)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                    }
                    else
                    {
                        objPTR003.RevNo = objPTR001.RevNo;
                    }
                    objPTR003.CouponID = fc["CouponID" + newRowIndex];
                    objPTR003.TestTypeOrientation = fc["TestTypeOrientation" + newRowIndex];
                    objPTR003.Orientation = fc["Orientation" + newRowIndex];
                    objPTR003.SimpleLocation = (fc["SimpleLocation" + newRowIndex] + "").TrimEnd(',');
                    objPTR003.TestTemperature = (fc["TestTemperature" + newRowIndex] + "").TrimEnd(',');
                    objPTR003.NoofSamples = fc["NoofSamples" + newRowIndex];
                    objPTR003.AcceptanceCriteria = fc["AcceptanceCriteria" + newRowIndex];
                    objPTR003.TestingSpecification = fc["TestingSpecification" + newRowIndex];
                    objPTR003.Remarks = fc["Remarks" + newRowIndex];
                    objPTR003.CreatedOn = DateTime.Now;
                    objPTR003.CreatedBy = objClsLoginInfo.UserName;
                    db.PTR003.Add(objPTR003);
                    db.SaveChanges();
                    objResponseMsg.Value = "Added Succesfully";
                    objResponseMsg.Key = true;

                    objResponseMsg.LineId = objPTR003.LineId;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Try Again";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateMechLine(int lineId, int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PTR003 objPTR003 = new PTR003();
            try
            {
                if (!string.IsNullOrEmpty(columnName))
                {
                    objPTR003 = db.PTR003.Where(i => i.LineId == lineId).FirstOrDefault();
                    if ((columnName == "CouponID" || columnName == "Orientation" || columnName == "TestTypeOrientation" || columnName == "SimpleLocation" || columnName == "TestTemperature" ||
                        columnName == "NoofSamples" || columnName == "AcceptanceCriteria" || columnName == "TestingSpecification")
                        && string.IsNullOrEmpty(columnValue)) //Not Nullable
                    {
                        objResponseMsg.Value = columnName + " is required!";
                    }
                    else
                    {
                        db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, "PTR003", objClsLoginInfo.UserName);
                        PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                        if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                        {
                            objPTR003.RevNo = objPTR003.RevNo + 1;
                            objPTR001.RevNo = objPTR001.RevNo + 1;
                            objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                            objPTR001.InitiatorRemarks = null;
                            objPTR001.WE2Remarks = null;
                            objPTR001.QC3Remarks = null;
                            objPTR001.QA3WTC3Remarks = null;
                            objPTR001.SubmittedBy = null;
                            objPTR001.SubmittedOn = null;
                            objPTR001.WE2ApprovalBy = null;
                            objPTR001.WE2ApprovalOn = null;
                            objPTR001.WE2ReturnBy = null;
                            objPTR001.WE2ReturnOn = null;
                            objPTR001.QC3ApprovalBy = null;
                            objPTR001.QC3ApprovalOn = null;
                            objPTR001.QC3ReturnBy = null;
                            objPTR001.QC3ReturnOn = null;
                            objPTR001.QA3WTC3ApprovalBy = null;
                            objPTR001.QA3WTC3ApprovalOn = null;
                            objPTR001.QA3WTC3ReturnBy = null;
                            objPTR001.QA3WTC3ReturnOn = null;
                            objPTR001.RetractedBy = null;
                            objPTR001.RetractedOn = null;
                            foreach (var i in objPTR001.PTR002)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR003)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR004)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR005)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR006)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Updated Successfully";
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteMechLines(int lineId, int headerId)
        {
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            try
            {
                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == headerId).FirstOrDefault();

                if (objPTR001 != null)
                {
                    PTR003 objPTR003 = db.PTR003.Where(i => i.LineId == lineId && i.HeaderId == headerId).FirstOrDefault();
                    db.PTR003.Remove(objPTR003);
                    if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                    {
                        objPTR001.RevNo = objPTR001.RevNo + 1;
                        objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                        objPTR001.InitiatorRemarks = null;
                        objPTR001.WE2Remarks = null;
                        objPTR001.QC3Remarks = null;
                        objPTR001.QA3WTC3Remarks = null;
                        objPTR001.SubmittedBy = null;
                        objPTR001.SubmittedOn = null;
                        objPTR001.WE2ApprovalBy = null;
                        objPTR001.WE2ApprovalOn = null;
                        objPTR001.WE2ReturnBy = null;
                        objPTR001.WE2ReturnOn = null;
                        objPTR001.QC3ApprovalBy = null;
                        objPTR001.QC3ApprovalOn = null;
                        objPTR001.QC3ReturnBy = null;
                        objPTR001.QC3ReturnOn = null;
                        objPTR001.QA3WTC3ApprovalBy = null;
                        objPTR001.QA3WTC3ApprovalOn = null;
                        objPTR001.QA3WTC3ReturnBy = null;
                        objPTR001.QA3WTC3ReturnOn = null;
                        objPTR001.RetractedBy = null;
                        objPTR001.RetractedOn = null;
                        foreach (var i in objPTR001.PTR002)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR003)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR004)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR005)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR006)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Deleted Successfully";

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Try Again";
            }

            return Json(objResponseMsg);
        }
        #endregion

        #region HTDetails
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult HTDetails(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            PTR001 objPTR001 = new PTR001();
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            ViewBag.IsDifferentLocationUser = (objPTR001.Location.Trim().ToUpper() != objClsLoginInfo.Location.Trim().ToUpper());
            ViewBag.HeatTreatmentType = Manager.GetSubCatagories("Heat Treatment Type", objPTR001.BU, objPTR001.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            if (chkAuth(objPTR001.CouponCategory, objPTR001.Status))
                ViewBag.auth = "yes";
            else
                ViewBag.auth = "no";
            return View(objPTR001);
        }
        public ActionResult LoadHTDetailData(string auth, JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                var HeaderId = Convert.ToInt32(param.Headerid);
                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                string condition = string.Empty;
                condition += "where HeaderId=" + HeaderId + " ";
                var couNo = (from a in db.PTR002
                             where a.HeaderId == HeaderId
                             select a).Max(a => a.CouponNo);
                if (couNo != null)
                    couNo = couNo + 1;
                else
                    couNo = 1;
                bool isEditable = (auth == "yes" ? true : false) && (isLinesEditable(HeaderId));
                //if (!string.IsNullOrWhiteSpace(param.sSearch))
                //{
                //    condition += " and ( CouponNo like '%" + param.sSearch + "%'  or CouponID like '%" + param.sSearch + "%'  or HTType1 like '%" + param.sSearch + "%' or HTTemp1 like '%" + param.sSearch + "%' or HTTime1 like '%" + param.sSearch + "%'  or HTType2 like '%" + param.sSearch + "%' or HTTemp2 like '%" + param.sSearch + "%' or HTTime2 like '%" + param.sSearch + "%' or HTType3 like '%" + param.sSearch + "%' or HTTemp3 like '%" + param.sSearch + "%' or HTTime3 like '%" + param.sSearch + "%' or HTType4 like '%" + param.sSearch + "%' or HTTemp4 like '%" + param.sSearch + "%' or HTTime4 like '%" + param.sSearch + "%' or HTType5 like '%" + param.sSearch + "%' or HTTemp5 like '%" + param.sSearch + "%' or HTTime5 like '%" + param.sSearch + "%' or HTType6 like '%" + param.sSearch + "%' or HTTemp6 like '%" + param.sSearch + "%' or HTTime6 like '%" + param.sSearch + "%'  )";
                //}
                string[] columnName = { "CouponNo", "CouponID", "HTType1", "HTTemp1", "HTTime1", "HTType2", "HTTemp2", "HTTime2", "HTType3", "HTTemp3", "HTTime3", "HTType4", "HTTemp4", "HTTime4", "HTType5", "HTTemp5", "HTTime5", "HTType6", "HTTemp6", "HTTime6" };
                condition += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PTR_GET_HT_LINE_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, condition
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                        GenerateTextbox(newRecordId,"CouponNo",couNo.ToString(),"","","",true),
                                        GenerateTextbox(newRecordId,"CouponID","","","","",false,"","10"),
                                        //GenerateTextbox(newRecordId,"HTType1","","","","",false,"","30"),
                                        GenerateAutoCompleteBlur(newRecordId,"txtHTType1","","",false,"","HTType1")+""+Helper.GenerateHidden(newRecordId,"HTType1"),
                                        GenerateTextbox(newRecordId,"HTTemp1","","","","",false,"","20"),
                                        GenerateTextbox(newRecordId,"HTTime1","","","","",false,"","20"),
                                        //GenerateTextbox(newRecordId,"HTType2","","","","",false,"","30"),
                                        GenerateAutoCompleteonBlur(newRecordId,"txtHTType2","","",false,"","HTType2")+""+Helper.GenerateHidden(newRecordId,"HTType2"),
                                        GenerateTextbox(newRecordId,"HTTemp2","","","","",false,"","20"),
                                        GenerateTextbox(newRecordId,"HTTime2","","","","",false,"","20"),
                                        //GenerateTextbox(newRecordId,"HTType3","","","","",false,"","30"),
                                        GenerateAutoCompleteonBlur(newRecordId,"txtHTType3","","",false,"","HTType3")+""+Helper.GenerateHidden(newRecordId,"HTType3"),
                                        GenerateTextbox(newRecordId,"HTTemp3","","","","",false,"","20"),
                                        GenerateTextbox(newRecordId,"HTTime3","","","","",false,"","20"),
                                        //GenerateTextbox(newRecordId,"HTType4","","","","",false,"","30"),
                                        GenerateAutoCompleteonBlur(newRecordId,"txtHTType4","","",false,"","HTType4")+""+Helper.GenerateHidden(newRecordId,"HTType4"),
                                        GenerateTextbox(newRecordId,"HTTemp4","","","","",false,"","20"),
                                        GenerateTextbox(newRecordId,"HTTime4","","","","",false,"","20"),
                                        //GenerateTextbox(newRecordId,"HTType5","","","","",false,"","30"),
                                        GenerateAutoCompleteonBlur(newRecordId,"txtHTType5","","",false,"","HTType5")+""+Helper.GenerateHidden(newRecordId,"HTType5"),
                                        GenerateTextbox(newRecordId,"HTTemp5","","","","",false,"","20"),
                                        GenerateTextbox(newRecordId,"HTTime5","","","","",false,"","20"),
                                        //GenerateTextbox(newRecordId,"HTType6","","","","",false,"","30"),
                                        GenerateAutoCompleteonBlur(newRecordId,"txtHTType6","","",false,"","HTType6")+""+Helper.GenerateHidden(newRecordId,"HTType6"),
                                        GenerateTextbox(newRecordId,"HTTemp6","","","","",false,"","20"),
                                        GenerateTextbox(newRecordId,"HTTime6","","","","",false,"","20"),
                                        GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewStage();" ),
                                        Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),

                                        };
                var data = (from uc in lstResult
                            select new[]
                            {
                                        Convert.ToString(uc.CouponNo),
                                        isEditable?GenerateTextbox(uc.LineId,"CouponID",uc.CouponID,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","10"):uc.CouponID,
                                        //isEditable?GenerateTextbox(uc.LineId,"HTType1",uc.HTType1,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","30"):uc.HTType1,
                                        isEditable?GenerateAutoCompleteBlur(uc.LineId,"txtHTType1",getCategoryDesc("Heat Treatment Type",objPTR001.BU,objPTR001.Location,uc.HTType1),"",false,"","HTType1",false,"",uc.HeaderId)+""+Helper.GenerateHidden(uc.LineId,"HTType1",uc.HTType1):getCategoryDesc("Heat Treatment Type",objPTR001.BU,objPTR001.Location,uc.HTType1),
                                        isEditable?GenerateTextbox(uc.LineId,"HTTemp1",uc.HTTemp1,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HTTemp1,
                                        isEditable?GenerateTextbox(uc.LineId,"HTTime1",uc.HTTime1,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HTTime1,
                                        //isEditable?GenerateTextbox(uc.LineId,"HTType2",uc.HTType2,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","30"):uc.HTType2,
                                        isEditable?GenerateAutoCompleteonBlur(uc.LineId,"txtHTType2",getCategoryDesc("Heat Treatment Type",objPTR001.BU,objPTR001.Location,uc.HTType2),"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","HTType2",false)+""+Helper.GenerateHidden(uc.LineId,"HTType2",uc.HTType2):getCategoryDesc("Heat Treatment Type",objPTR001.BU,objPTR001.Location,uc.HTType2),
                                        isEditable ?GenerateTextbox(uc.LineId,"HTTemp2",uc.HTTemp2,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HTTemp2,
                                        isEditable?GenerateTextbox(uc.LineId,"HTTime2",uc.HTTime2,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HTTime2,
                                        //isEditable?GenerateTextbox(uc.LineId,"HTType3",uc.HTType3,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","30"):uc.HTType3,
                                        isEditable?GenerateAutoCompleteonBlur(uc.LineId,"txtHTType3",getCategoryDesc("Heat Treatment Type",objPTR001.BU,objPTR001.Location,uc.HTType3),"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","HTType3",false)+""+Helper.GenerateHidden(uc.LineId,"HTType3",uc.HTType3):getCategoryDesc("Heat Treatment Type",objPTR001.BU,objPTR001.Location,uc.HTType3),
                                        isEditable ?GenerateTextbox(uc.LineId,"HTTemp3",uc.HTTemp3,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HTTemp3,
                                        isEditable?GenerateTextbox(uc.LineId,"HTTime3",uc.HTTime3,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HTTime3,
                                        //isEditable?GenerateTextbox(uc.LineId,"HTType4",uc.HTType4,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","30"):uc.HTType4,
                                        isEditable?GenerateAutoCompleteonBlur(uc.LineId,"txtHTType4",getCategoryDesc("Heat Treatment Type",objPTR001.BU,objPTR001.Location,uc.HTType4),"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","HTType4",false)+""+Helper.GenerateHidden(uc.LineId,"HTType4",uc.HTType4):getCategoryDesc("Heat Treatment Type",objPTR001.BU,objPTR001.Location,uc.HTType4),
                                        isEditable ?GenerateTextbox(uc.LineId,"HTTemp4",uc.HTTemp4,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HTTemp4,
                                        isEditable?GenerateTextbox(uc.LineId,"HTTime4",uc.HTTime4,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HTTime4,
                                        //isEditable?GenerateTextbox(uc.LineId,"HTType5",uc.HTType5,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","30"):uc.HTType5,
                                        isEditable?GenerateAutoCompleteonBlur(uc.LineId,"txtHTType5",getCategoryDesc("Heat Treatment Type",objPTR001.BU,objPTR001.Location,uc.HTType5),"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","HTType5",false)+""+Helper.GenerateHidden(uc.LineId,"HTType5",uc.HTType5):getCategoryDesc("Heat Treatment Type",objPTR001.BU,objPTR001.Location,uc.HTType5),
                                        isEditable ?GenerateTextbox(uc.LineId,"HTTemp5",uc.HTTemp5,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HTTemp5,
                                        isEditable?GenerateTextbox(uc.LineId,"HTTime5",uc.HTTime5,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HTTime5,
                                        //isEditable?GenerateTextbox(uc.LineId,"HTType6",uc.HTType6,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","30"):uc.HTType6,
                                        isEditable?GenerateAutoCompleteonBlur(uc.LineId,"txtHTType6",getCategoryDesc("Heat Treatment Type",objPTR001.BU,objPTR001.Location,uc.HTType6),"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","HTType6",false)+""+Helper.GenerateHidden(uc.LineId,"HTType6",uc.HTType6):getCategoryDesc("Heat Treatment Type",objPTR001.BU,objPTR001.Location,uc.HTType6),
                                        isEditable ?GenerateTextbox(uc.LineId,"HTTemp6",uc.HTTemp6,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HTTemp6,
                                        isEditable?GenerateTextbox(uc.LineId,"HTTime6",uc.HTTime6,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HTTime6,
                                        "<center>"+HTMLActionString(uc.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteLines(" + uc.LineId + "," + uc.HeaderId + ")",!isEditable)+"</center>",
                                        Helper.GenerateHidden(uc.LineId, "HeaderId", Convert.ToString(HeaderId)),

                         }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveHTDetails(FormCollection fc)
        {
            PTR002 objPTR002 = new PTR002();
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            //string Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
            try
            {
                //string location = fc["Location"].Split('-')[0].Trim();
                //string auth = fc["auth"];
                //string status = fc["status"];
                //if (auth.Equals("yes") && (status.Equals("Draft") || status.Equals("Closed")))
                //{
                int newRowIndex = 0;
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId" + newRowIndex]) ? Convert.ToInt32(fc["HeaderId" + newRowIndex]) : 0;
                if (refHeaderId > 0)
                {
                    PTR001 objPTR001 = db.PTR001.Where(m => m.HeaderId == refHeaderId).FirstOrDefault();
                    objPTR002.HeaderId = objPTR001.HeaderId;
                    objPTR002.Project = objPTR001.Project;
                    objPTR002.BU = objPTR001.BU;
                    objPTR002.Location = objPTR001.Location;
                    objPTR002.PTRNo = objPTR001.PTRNo;
                    if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                    {
                        objPTR002.RevNo = Convert.ToInt32(objPTR001.RevNo) + 1;
                        objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                        objPTR001.RevNo = Convert.ToInt32(objPTR001.RevNo) + 1;
                        objPTR001.InitiatorRemarks = null;
                        objPTR001.WE2Remarks = null;
                        objPTR001.QC3Remarks = null;
                        objPTR001.QA3WTC3Remarks = null;
                        objPTR001.SubmittedBy = null;
                        objPTR001.SubmittedOn = null;
                        objPTR001.WE2ApprovalBy = null;
                        objPTR001.WE2ApprovalOn = null;
                        objPTR001.WE2ReturnBy = null;
                        objPTR001.WE2ReturnOn = null;
                        objPTR001.QC3ApprovalBy = null;
                        objPTR001.QC3ApprovalOn = null;
                        objPTR001.QC3ReturnBy = null;
                        objPTR001.QC3ReturnOn = null;
                        objPTR001.QA3WTC3ApprovalBy = null;
                        objPTR001.QA3WTC3ApprovalOn = null;
                        objPTR001.QA3WTC3ReturnBy = null;
                        objPTR001.QA3WTC3ReturnOn = null;
                        objPTR001.RetractedBy = null;
                        objPTR001.RetractedOn = null;
                        foreach (var i in objPTR001.PTR002)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR003)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR004)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR005)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR006)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                    }
                    else
                    {
                        objPTR002.RevNo = objPTR001.RevNo;
                    }
                    objPTR002.CouponID = fc["CouponID" + newRowIndex];
                    objPTR002.CouponNo = Convert.ToInt32(fc["CouponNo" + newRowIndex]);
                    objPTR002.HTTemp1 = fc["HTTemp1" + newRowIndex];
                    objPTR002.HTTemp2 = fc["HTTemp2" + newRowIndex];
                    objPTR002.HTTemp3 = fc["HTTemp3" + newRowIndex];
                    objPTR002.HTTemp4 = fc["HTTemp4" + newRowIndex];
                    objPTR002.HTTemp5 = fc["HTTemp5" + newRowIndex];
                    objPTR002.HTTemp6 = fc["HTTemp6" + newRowIndex];
                    objPTR002.HTTime1 = fc["HTTime1" + newRowIndex];
                    objPTR002.HTTime2 = fc["HTTime2" + newRowIndex];
                    objPTR002.HTTime3 = fc["HTTime3" + newRowIndex];
                    objPTR002.HTTime4 = fc["HTTime4" + newRowIndex];
                    objPTR002.HTTime5 = fc["HTTime5" + newRowIndex];
                    objPTR002.HTTime6 = fc["HTTime6" + newRowIndex];
                    objPTR002.HTType1 = fc["HTType1" + newRowIndex];
                    objPTR002.HTType2 = fc["HTType2" + newRowIndex];
                    objPTR002.HTType3 = fc["HTType3" + newRowIndex];
                    objPTR002.HTType4 = fc["HTType4" + newRowIndex];
                    objPTR002.HTType5 = fc["HTType5" + newRowIndex];
                    objPTR002.HTType6 = fc["HTType6" + newRowIndex];
                    objPTR002.CreatedOn = DateTime.Now;
                    objPTR002.CreatedBy = objClsLoginInfo.UserName;
                    db.PTR002.Add(objPTR002);
                    db.SaveChanges();
                    addTestCompletionDetail(objPTR002.HeaderId, (int)objPTR002.CouponNo, "Insert");
                    objResponseMsg.Value = "Added Succesfully";
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Value = "Please Try Again";
                    objResponseMsg.Key = false;
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateHTLine(int lineId, int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PTR002 objPTR002 = new PTR002();
            try
            {
                if (!string.IsNullOrEmpty(columnName))
                {
                    objPTR002 = db.PTR002.Where(i => i.LineId == lineId).FirstOrDefault();
                    if (columnName == "CouponID" && string.IsNullOrEmpty(columnValue))
                    {

                    }
                    else
                    {
                        db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, "PTR002", objClsLoginInfo.UserName);
                        addTestCompletionDetail(objPTR002.HeaderId, (int)objPTR002.CouponNo, "Update");
                        PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                        if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                        {
                            objPTR002.RevNo = objPTR002.RevNo + 1;
                            objPTR001.RevNo = objPTR001.RevNo + 1;
                            objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                            objPTR001.InitiatorRemarks = null;
                            objPTR001.WE2Remarks = null;
                            objPTR001.QC3Remarks = null;
                            objPTR001.QA3WTC3Remarks = null;
                            objPTR001.SubmittedBy = null;
                            objPTR001.SubmittedOn = null;
                            objPTR001.WE2ApprovalBy = null;
                            objPTR001.WE2ApprovalOn = null;
                            objPTR001.WE2ReturnBy = null;
                            objPTR001.WE2ReturnOn = null;
                            objPTR001.QC3ApprovalBy = null;
                            objPTR001.QC3ApprovalOn = null;
                            objPTR001.QC3ReturnBy = null;
                            objPTR001.QC3ReturnOn = null;
                            objPTR001.QA3WTC3ApprovalBy = null;
                            objPTR001.QA3WTC3ApprovalOn = null;
                            objPTR001.QA3WTC3ReturnBy = null;
                            objPTR001.QA3WTC3ReturnOn = null;
                            objPTR001.RetractedBy = null;
                            objPTR001.RetractedOn = null;
                            foreach (var i in objPTR001.PTR002)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR003)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR004)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR005)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR006)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                        }
                    }

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Updated Successfully";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteHTLines(int lineId, int headerId)
        {
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            try
            {

                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == headerId).FirstOrDefault();

                if (objPTR001 != null)
                {
                    PTR002 objPTR002 = db.PTR002.Where(i => i.LineId == lineId && i.HeaderId == headerId).FirstOrDefault();
                    db.PTR002.Remove(objPTR002);
                    if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                    {
                        objPTR001.RevNo = objPTR001.RevNo + 1;
                        objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                        objPTR001.InitiatorRemarks = null;
                        objPTR001.WE2Remarks = null;
                        objPTR001.QC3Remarks = null;
                        objPTR001.QA3WTC3Remarks = null;
                        objPTR001.SubmittedBy = null;
                        objPTR001.SubmittedOn = null;
                        objPTR001.WE2ApprovalBy = null;
                        objPTR001.WE2ApprovalOn = null;
                        objPTR001.WE2ReturnBy = null;
                        objPTR001.WE2ReturnOn = null;
                        objPTR001.QC3ApprovalBy = null;
                        objPTR001.QC3ApprovalOn = null;
                        objPTR001.QC3ReturnBy = null;
                        objPTR001.QC3ReturnOn = null;
                        objPTR001.QA3WTC3ApprovalBy = null;
                        objPTR001.QA3WTC3ApprovalOn = null;
                        objPTR001.QA3WTC3ReturnBy = null;
                        objPTR001.QA3WTC3ReturnOn = null;
                        objPTR001.RetractedBy = null;
                        objPTR001.RetractedOn = null;
                        foreach (var i in objPTR001.PTR002)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR003)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR004)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR005)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR006)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                    }
                    db.SaveChanges();
                    addTestCompletionDetail(objPTR002.HeaderId, (int)objPTR002.CouponNo, "Delete");
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Deleted Successfully";

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Try Again";
            }

            return Json(objResponseMsg);
        }
        #endregion

        #region Chemical Details
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult ChemDetails(int HeaderId)
        {
            var user = objClsLoginInfo.UserName;

            ViewBag.HeaderId = HeaderId;
            PTR001 objPTR001 = new PTR001();
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            ViewBag.IsDifferentLocationUser = (objPTR001.Location.Trim().ToUpper() != objClsLoginInfo.Location.Trim().ToUpper());
            ViewBag.Elements = Manager.GetSubCatagories("Element", objPTR001.BU, objPTR001.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            ViewBag.SampleLocation = Manager.GetSubCatagories("Sample Location Master", objPTR001.BU, objPTR001.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            ViewBag.CouponID = db.PTR002.Where(x => x.HeaderId == HeaderId).Select(i => new CategoryData { Value = i.CouponID, Code = i.CouponID, CategoryDescription = i.CouponID }).ToList();
            ViewBag.TestTypeOrientation = Manager.GetSubCatagories("CHEMTEST", objPTR001.BU, objPTR001.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description });
            objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            if (chkAuth(objPTR001.CouponCategory, objPTR001.Status))
                ViewBag.auth = "yes";
            else
                ViewBag.auth = "no";


            List<UserRoleDetails> role = GetRoleByLocation(user, objClsLoginInfo.Location);
            if (role.Where(x => x.UserRoleDesc == clsImplementationEnum.UserRoleName.QA1.GetStringValue()).Count() > 0
                || role.Where(x => x.UserRoleDesc == clsImplementationEnum.UserRoleName.QA2.GetStringValue()).Count() > 0
                || role.Where(x => x.UserRoleDesc == clsImplementationEnum.UserRoleName.QA3.GetStringValue()).Count() > 0)
                ViewBag.IsRoleQA = true;
            else
                ViewBag.IsRoleQA = false;


            return View(objPTR001);
        }
        public ActionResult LoadChemDetailData(string auth, JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                var HeaderId = Convert.ToInt32(param.Headerid);
                string condition = string.Empty;
                condition += "where HeaderId=" + HeaderId + " ";
                bool isEditable = (auth == "yes" ? true : false) && (isLinesEditable(HeaderId));
                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                var lstSampleLocation = GetCatagoryList("", "Sample Location Master", objPTR001.BU, objPTR001.Location);
                var lstElements = GetCatagoryList("", "Element", objPTR001.BU, objPTR001.Location);

                //if (!string.IsNullOrWhiteSpace(param.sSearch))
                //{
                //    condition += " and ( LineId like '%" + param.sSearch + "%'  or CouponID like '%" + param.sSearch + "%'  or SampleLocation like '%" + param.sSearch + "%' or Elements like '%" + param.sSearch + "%' or AcceptanceCriteria like '%" + param.sSearch + "%'  or TestingSpecification like '%" + param.sSearch + "%' or Remarks like '%" + param.sSearch + "%' )";
                //}
                string[] columnName = { "CouponID", "TestTypeOrientation", "Orientation",  "SampleLocation", "Elements", "AcceptanceCriteria", "TestingSpecification", "Remarks" };
                condition += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PTR_GET_CHEM_HEADER_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, condition
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                    "0",
                    GenerateAutoCompleteonBlur(newRecordId,"txtCouponID","","",false,"","CouponID",false,"25")+""+Helper.GenerateHidden(newRecordId,"CouponID"),
                    GenerateAutoCompleteonBlur(newRecordId,"txtTestTypeOrientation","","",false,"","TestTypeOrientation")+""+Helper.GenerateHidden(newRecordId,"TestTypeOrientation"),
                    GenerateAutoCompleteonBlur(newRecordId,"txtOrientation","","",false,"","Orientation")+""+Helper.GenerateHidden(newRecordId,"Orientation"),
                    MultiSelectDropdown(newRecordId,lstSampleLocation,"", false, "","SampleLocation"),
                    //GenerateAutoCompleteonBlur(newRecordId,"txtSampleLocation","","",false,"","SampleLocation")+""+Helper.GenerateHidden(newRecordId,"SampleLocation"),
                    //MultiSelectDropdown(newRecordId,lstElements,"", false, "","Elements"),
                    //GenerateAutoCompleteonBlur(newRecordId,"txtElements","","",false,"","Elements")+""+Helper.GenerateHidden(newRecordId,"Elements"),
                    //GenerateTextbox(newRecordId,"AcceptanceCriteria","","","","",false,"",""),
                    GenerateTextbox(newRecordId,"TestingSpecification","","","","",false,"",""),
                    GenerateTextbox(newRecordId,"Remarks","","","","",false,"",""),
                    "",
                    "",
                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewStage();" ),
                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                uc.ROW_NO==1 ? Convert.ToString(uc.CouponGroupNo):"",
                                uc.ROW_NO==1 ? (isEditable?GenerateAutoCompleteonBlur(uc.LineId,"txtCouponID",uc.CouponID,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","CouponID",false,"25")+""+Helper.GenerateHidden(uc.LineId,"CouponID",uc.CouponID):uc.CouponID):"",
                                uc.ROW_NO==1 ?(isEditable?GenerateAutoCompleteonBlur(uc.LineId,"txtTestTypeOrientation",getCategoryDesc2("CHEMTEST",objPTR001.BU,objPTR001.Location,uc.TestTypeOrientation),"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","TestTypeOrientation",false)+""+Helper.GenerateHidden(uc.LineId,"TestTypeOrientation",uc.TestTypeOrientation):getCategoryDesc("CHEMTEST",objPTR001.BU,objPTR001.Location,uc.TestTypeOrientation)):"",
                                uc.ROW_NO==1 ?(isEditable?GenerateAutoCompleteonBlur(uc.LineId,"txtOrientation",uc.Orientation,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","Orientation",false,"25")+""+Helper.GenerateHidden(uc.LineId,"Orientation",uc.Orientation):uc.Orientation):"",
                                uc.ROW_NO==1 ? (MultiSelectDropdown(uc.LineId,lstSampleLocation,uc.SampleLocation, !isEditable, "UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","SampleLocation")):"",
                                //isEditable ?GenerateAutoCompleteonBlur(uc.LineId,"txtSampleLocation",getCategoryDesc("Sample Location Master",objPTR001.BU,objPTR001.Location,uc.SampleLocation),"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","SampleLocation",false)+""+Helper.GenerateHidden(uc.LineId,"SampleLocation",uc.SampleLocation):getCategoryDesc("Sample Location Master",objPTR001.BU,objPTR001.Location,uc.SampleLocation),
                                //isEditable ?GenerateAutoCompleteonBlur(uc.LineId,"txtElements",getCategoryDesc("Element",objPTR001.BU,objPTR001.Location,uc.Elements),"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");",false,"","Elements",false)+""+Helper.GenerateHidden(uc.LineId,"Elements",uc.Elements):getCategoryDesc("Element",objPTR001.BU,objPTR001.Location,uc.Elements),                                
                                uc.ROW_NO==1 ? (isEditable?GenerateTextbox(uc.LineId,"TestingSpecification",uc.TestingSpecification,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"",""):uc.TestingSpecification):"",
                                uc.ROW_NO==1 ? (isEditable?GenerateTextbox(uc.LineId,"Remarks",uc.Remarks,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"",""):uc.Remarks):"",
                                GetElementDetails(uc.Elements,lstElements),//MultiSelectDropdown(uc.LineId,lstElements,uc.Elements, !isEditable, "UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","Elements"),
                                uc.AcceptanceCriteria,//isEditable?GenerateTextbox(uc.LineId,"AcceptanceCriteria",uc.AcceptanceCriteria,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"",""):uc.AcceptanceCriteria,
                                uc.ROW_NO==1 ? ("<center>"+HTMLActionString(uc.LineId,"","AddElements","Add Elements/Acceptance Criteria","fa fa-plus","AddElementsAcceptanceCriteria(" + uc.LineId + "," + uc.HeaderId + ")",!isEditable) +"&nbsp;&nbsp;"
                                                +HTMLActionString(uc.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteLines(" + uc.LineId + "," + uc.HeaderId + ")",!isEditable)
                                                +"</center>"):"",
                                Helper.GenerateHidden(uc.LineId, "LineId", Convert.ToString(uc.LineId)),
                         }).ToList();
                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetElementDetails(string selectedValue, List<CategoryData> list)
        {
            string ElementDtl = "";
            string[] arrayVal = { };
            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }
                foreach (var item in arrayVal)
                {
                    var obj = list.Where(i => i.Code == item).FirstOrDefault();
                    if (obj != null)
                    {
                        Manager.ConcatString(ref ElementDtl, obj.Description);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return ElementDtl;
        }
        public string GetSampleLocationDetails(string selectedValue, List<CategoryData> list)
        {
            string ElementDtl = "";
            string[] arrayVal = { };
            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }
                foreach (var item in arrayVal)
                {
                    var obj = list.Where(i => i.Code == item).FirstOrDefault();
                    if (obj != null)
                    {
                        Manager.ConcatString(ref ElementDtl, obj.Description);
                    }
                }
                foreach (var item in arrayVal)
                {
                    if (!list.Any(i => i.Code == item))
                    {
                        Manager.ConcatString(ref ElementDtl, item);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return ElementDtl;
        }

        [HttpPost]
        public ActionResult SaveChemDetails(FormCollection fc)
        {
            PTR004 objPTR004 = new PTR004();
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            //string Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
            try
            {
                //string location = fc["Location"].Split('-')[0].Trim();
                //string auth = fc["auth"];
                //string status = fc["status"];
                //if (auth.Equals("yes") && (status.Equals("Draft") || status.Equals("Closed")))
                //{
                int newRowIndex = 0;
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId" + newRowIndex]) ? Convert.ToInt32(fc["HeaderId" + newRowIndex]) : 0;
                if (refHeaderId > 0)
                {
                    PTR001 objPTR001 = db.PTR001.Where(m => m.HeaderId == refHeaderId).FirstOrDefault();
                    objPTR004.HeaderId = objPTR001.HeaderId;
                    objPTR004.Project = objPTR001.Project;
                    objPTR004.BU = objPTR001.BU;
                    objPTR004.Location = objPTR001.Location;
                    objPTR004.PTRNo = objPTR001.PTRNo;
                    if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                    {
                        objPTR004.RevNo = Convert.ToInt32(objPTR001.RevNo) + 1;
                        objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                        objPTR001.RevNo = Convert.ToInt32(objPTR001.RevNo) + 1;
                        objPTR001.InitiatorRemarks = null;
                        objPTR001.WE2Remarks = null;
                        objPTR001.QC3Remarks = null;
                        objPTR001.QA3WTC3Remarks = null;
                        objPTR001.SubmittedBy = null;
                        objPTR001.SubmittedOn = null;
                        objPTR001.WE2ApprovalBy = null;
                        objPTR001.WE2ApprovalOn = null;
                        objPTR001.WE2ReturnBy = null;
                        objPTR001.WE2ReturnOn = null;
                        objPTR001.QC3ApprovalBy = null;
                        objPTR001.QC3ApprovalOn = null;
                        objPTR001.QC3ReturnBy = null;
                        objPTR001.QC3ReturnOn = null;
                        objPTR001.QA3WTC3ApprovalBy = null;
                        objPTR001.QA3WTC3ApprovalOn = null;
                        objPTR001.QA3WTC3ReturnBy = null;
                        objPTR001.QA3WTC3ReturnOn = null;
                        objPTR001.RetractedBy = null;
                        objPTR001.RetractedOn = null;
                        foreach (var i in objPTR001.PTR002)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR003)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR004)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR005)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR006)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                    }
                    else
                    {
                        objPTR004.RevNo = objPTR001.RevNo;
                    }
                    objPTR004.CouponID = fc["CouponID" + newRowIndex];
                    objPTR004.SampleLocation = fc["SampleLocation" + newRowIndex];
                    //objPTR004.Elements = fc["Elements" + newRowIndex];
                    //objPTR004.AcceptanceCriteria = fc["AcceptanceCriteria" + newRowIndex];
                    objPTR004.TestTypeOrientation = fc["TestTypeOrientation" + newRowIndex];
                    objPTR004.Orientation = fc["Orientation" + newRowIndex];
                    objPTR004.TestingSpecification = fc["TestingSpecification" + newRowIndex];
                    objPTR004.Remarks = fc["Remarks" + newRowIndex];
                    objPTR004.CreatedOn = DateTime.Now;
                    objPTR004.CreatedBy = objClsLoginInfo.UserName;
                    db.PTR004.Add(objPTR004);
                    db.SaveChanges();
                    objResponseMsg.Value = "Added Succesfully";
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Value = "Please Try Again";
                    objResponseMsg.Key = false;
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateChemLine(int lineId, int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PTR004 objPTR004 = new PTR004();
            try
            {
                if (!string.IsNullOrEmpty(columnName))
                {
                    objPTR004 = db.PTR004.Where(i => i.LineId == lineId).FirstOrDefault();
                    //if ((columnName == "CouponID" || (columnName == "Elements") || (columnName == "SampleLocation"))
                    //    && string.IsNullOrEmpty(columnValue))
                    if ((columnName == "CouponID" || (columnName == "Elements"))
                        && string.IsNullOrEmpty(columnValue))
                    {
                        objResponseMsg.Value = columnName + " is required!";
                    }
                    else
                    {
                        var listPTR004 = db.PTR004.Where(i => i.HeaderId == objPTR004.HeaderId && i.CouponID == objPTR004.CouponID
                                                    && i.SampleLocation == objPTR004.SampleLocation
                                                    && i.TestingSpecification == objPTR004.TestingSpecification
                                                    && i.Remarks == objPTR004.Remarks).ToList();
                        foreach (var item in listPTR004)
                        {
                            db.SP_COMMON_LINES_UPDATE(item.LineId, columnName, columnValue, "PTR004", objClsLoginInfo.UserName);
                        }

                        PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                        if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                        {
                            foreach (var item in listPTR004)
                            {
                                item.RevNo = objPTR004.RevNo + 1;
                            }
                            objPTR001.RevNo = objPTR001.RevNo + 1;
                            objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                            objPTR001.InitiatorRemarks = null;
                            objPTR001.WE2Remarks = null;
                            objPTR001.QC3Remarks = null;
                            objPTR001.QA3WTC3Remarks = null;
                            objPTR001.SubmittedBy = null;
                            objPTR001.SubmittedOn = null;
                            objPTR001.WE2ApprovalBy = null;
                            objPTR001.WE2ApprovalOn = null;
                            objPTR001.WE2ReturnBy = null;
                            objPTR001.WE2ReturnOn = null;
                            objPTR001.QC3ApprovalBy = null;
                            objPTR001.QC3ApprovalOn = null;
                            objPTR001.QC3ReturnBy = null;
                            objPTR001.QC3ReturnOn = null;
                            objPTR001.QA3WTC3ApprovalBy = null;
                            objPTR001.QA3WTC3ApprovalOn = null;
                            objPTR001.QA3WTC3ReturnBy = null;
                            objPTR001.QA3WTC3ReturnOn = null;
                            objPTR001.RetractedBy = null;
                            objPTR001.RetractedOn = null;
                            foreach (var i in objPTR001.PTR002)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR003)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR004)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR005)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR006)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Updated Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteChemLines(int lineId, int headerId)
        {
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            try
            {
                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == headerId).FirstOrDefault();

                if (objPTR001 != null)
                {
                    PTR004 objPTR004 = db.PTR004.Where(i => i.LineId == lineId && i.HeaderId == headerId).FirstOrDefault();
                    var listPTR004 = db.PTR004.Where(i => i.HeaderId == objPTR004.HeaderId && i.CouponID == objPTR004.CouponID
                                                   && i.SampleLocation == objPTR004.SampleLocation
                                                   && i.TestingSpecification == objPTR004.TestingSpecification
                                                   && i.Remarks == objPTR004.Remarks).ToList();
                    foreach (var item in listPTR004)
                    {
                        db.PTR004.Remove(item);
                    }
                    if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                    {
                        objPTR001.RevNo = objPTR001.RevNo + 1;
                        objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                        objPTR001.InitiatorRemarks = null;
                        objPTR001.WE2Remarks = null;
                        objPTR001.QC3Remarks = null;
                        objPTR001.QA3WTC3Remarks = null;
                        objPTR001.SubmittedBy = null;
                        objPTR001.SubmittedOn = null;
                        objPTR001.WE2ApprovalBy = null;
                        objPTR001.WE2ApprovalOn = null;
                        objPTR001.WE2ReturnBy = null;
                        objPTR001.WE2ReturnOn = null;
                        objPTR001.QC3ApprovalBy = null;
                        objPTR001.QC3ApprovalOn = null;
                        objPTR001.QC3ReturnBy = null;
                        objPTR001.QC3ReturnOn = null;
                        objPTR001.QA3WTC3ApprovalBy = null;
                        objPTR001.QA3WTC3ApprovalOn = null;
                        objPTR001.QA3WTC3ReturnBy = null;
                        objPTR001.QA3WTC3ReturnOn = null;
                        objPTR001.RetractedBy = null;
                        objPTR001.RetractedOn = null;
                        foreach (var i in objPTR001.PTR002)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR003)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR004)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR005)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR006)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Deleted Successfully";

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Try Again";
            }

            return Json(objResponseMsg);
        }
        [SessionExpireFilter]
        public PartialViewResult ChemLineCouponDetails(int LineId)
        {
            ViewBag.LineId = LineId;
            var objPTR004 = db.PTR004.Where(i => i.LineId == LineId).FirstOrDefault();
            var objPTR001 = db.PTR001.Where(i => i.HeaderId == objPTR004.HeaderId).FirstOrDefault();
            ViewBag.HeaderId = objPTR001.HeaderId;
            if (chkAuth(objPTR001.CouponCategory, objPTR001.Status))
                ViewBag.auth = "yes";
            else
                ViewBag.auth = "no";
            return PartialView("_ChemLineCouponDetails");
        }

        public ActionResult LoadChemicalLineCouponDetails(string auth, JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                var HeaderId = Convert.ToInt32(Request["HeaderId"]);
                var LineId = Convert.ToInt32(Request["LineId"]);
                string condition = string.Empty;
                condition += "where HeaderId=" + HeaderId + " ";
                bool isEditable = (auth == "yes" ? true : false) && (isLinesEditable(HeaderId));
                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                var objPTR004 = db.PTR004.Where(x => x.LineId == LineId).FirstOrDefault();
                var lstElements = GetCatagoryList("", "Element", objPTR001.BU, objPTR001.Location);



                string[] columnName = { "Elements", "AcceptanceCriteria" };
                condition += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PTR_GET_CHEM_LINE_DETAILS
                                (
                                0, 0, strSortOrder, condition
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                    "0",
                    MultiSelectDropdown(newRecordId,lstElements,"", false, "","Elements"),
                    GenerateTextbox(newRecordId,"AcceptanceCriteria","","","","",false,"",""),
                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewElementAndAcceptanceCriteria();" ),
                };
                var SrNo = 0;
                lstResult = (from uc in lstResult
                             where uc.CouponID == objPTR004.CouponID && uc.SampleLocation == objPTR004.SampleLocation
                                 && uc.TestingSpecification == objPTR004.TestingSpecification && uc.Remarks == objPTR004.Remarks
                                 && (uc.LineId != LineId || (uc.LineId == LineId && (!string.IsNullOrEmpty(uc.Elements) || !string.IsNullOrEmpty(uc.AcceptanceCriteria))))
                             select uc).ToList();
                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(++SrNo),
                                MultiSelectDropdown(uc.LineId,lstElements,uc.Elements, !isEditable, "UpdateChemicalLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","Elements"),
                                isEditable?GenerateTextbox(uc.LineId,"AcceptanceCriteria",uc.AcceptanceCriteria,"UpdateChemicalLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"",""):uc.AcceptanceCriteria,
                                (uc.LineId!=LineId?("<center>"+HTMLActionString(uc.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteLinesDetails(" + uc.LineId + "," + uc.HeaderId + ")",!isEditable)+"</center>"):""),
                                Convert.ToString(uc.LineId)
                         }).ToList();
                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count() > 0 ? lstResult.Count() : 0),
                    iTotalDisplayRecords = (lstResult.Count() > 0 ? lstResult.Count() : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveChemLineDetails(FormCollection fc)
        {
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            try
            {
                int newRowIndex = 0;
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int LineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    var objPTR004 = db.PTR004.Where(i => i.LineId == LineId).FirstOrDefault();
                    if (objPTR004 != null)
                    {
                        if (string.IsNullOrEmpty(objPTR004.Elements) && string.IsNullOrEmpty(objPTR004.AcceptanceCriteria))
                        {
                            objPTR004.Elements = fc["Elements" + newRowIndex];
                            objPTR004.AcceptanceCriteria = fc["AcceptanceCriteria" + newRowIndex];
                        }
                        else
                        {
                            var objPTR004New = new PTR004();
                            objPTR004New.HeaderId = objPTR004.HeaderId;
                            objPTR004New.Project = objPTR004.Project;
                            objPTR004New.BU = objPTR004.BU;
                            objPTR004New.Location = objPTR004.Location;
                            objPTR004New.PTRNo = objPTR004.PTRNo;
                            objPTR004New.RevNo = objPTR004.RevNo;
                            objPTR004New.CouponID = objPTR004.CouponID;
                            objPTR004New.SampleLocation = objPTR004.SampleLocation;
                            objPTR004New.Elements = fc["Elements" + newRowIndex];
                            objPTR004New.AcceptanceCriteria = fc["AcceptanceCriteria" + newRowIndex];
                            objPTR004New.TestingSpecification = objPTR004.TestingSpecification;
                            objPTR004New.Remarks = objPTR004.Remarks;
                            objPTR004New.CreatedOn = DateTime.Now;
                            objPTR004New.CreatedBy = objClsLoginInfo.UserName;
                            db.PTR004.Add(objPTR004New);
                        }

                        PTR001 objPTR001 = db.PTR001.Where(m => m.HeaderId == refHeaderId).FirstOrDefault();
                        if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                        {
                            objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                            objPTR001.RevNo = Convert.ToInt32(objPTR001.RevNo) + 1;
                            objPTR001.InitiatorRemarks = null;
                            objPTR001.WE2Remarks = null;
                            objPTR001.QC3Remarks = null;
                            objPTR001.QA3WTC3Remarks = null;
                            objPTR001.SubmittedBy = null;
                            objPTR001.SubmittedOn = null;
                            objPTR001.WE2ApprovalBy = null;
                            objPTR001.WE2ApprovalOn = null;
                            objPTR001.WE2ReturnBy = null;
                            objPTR001.WE2ReturnOn = null;
                            objPTR001.QC3ApprovalBy = null;
                            objPTR001.QC3ApprovalOn = null;
                            objPTR001.QC3ReturnBy = null;
                            objPTR001.QC3ReturnOn = null;
                            objPTR001.QA3WTC3ApprovalBy = null;
                            objPTR001.QA3WTC3ApprovalOn = null;
                            objPTR001.QA3WTC3ReturnBy = null;
                            objPTR001.QA3WTC3ReturnOn = null;
                            objPTR001.RetractedBy = null;
                            objPTR001.RetractedOn = null;
                            foreach (var i in objPTR001.PTR002)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR003)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR004)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR005)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR006)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                        }

                        db.SaveChanges();
                        objResponseMsg.Value = "Added Succesfully";
                        objResponseMsg.Key = true;
                    }
                }
                else
                {
                    objResponseMsg.Value = "Please Try Again";
                    objResponseMsg.Key = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateChemLineDetails(int lineId, int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PTR004 objPTR004 = new PTR004();
            try
            {
                if (!string.IsNullOrEmpty(columnName))
                {
                    objPTR004 = db.PTR004.Where(i => i.LineId == lineId).FirstOrDefault();
                    if (columnName == "Elements" && string.IsNullOrEmpty(columnValue))
                    {
                        objResponseMsg.Value = columnName + " is required!";
                    }
                    else
                    {
                        db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, "PTR004", objClsLoginInfo.UserName);

                        PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                        if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                        {
                            objPTR001.RevNo = objPTR001.RevNo + 1;
                            objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                            objPTR001.InitiatorRemarks = null;
                            objPTR001.WE2Remarks = null;
                            objPTR001.QC3Remarks = null;
                            objPTR001.QA3WTC3Remarks = null;
                            objPTR001.SubmittedBy = null;
                            objPTR001.SubmittedOn = null;
                            objPTR001.WE2ApprovalBy = null;
                            objPTR001.WE2ApprovalOn = null;
                            objPTR001.WE2ReturnBy = null;
                            objPTR001.WE2ReturnOn = null;
                            objPTR001.QC3ApprovalBy = null;
                            objPTR001.QC3ApprovalOn = null;
                            objPTR001.QC3ReturnBy = null;
                            objPTR001.QC3ReturnOn = null;
                            objPTR001.QA3WTC3ApprovalBy = null;
                            objPTR001.QA3WTC3ApprovalOn = null;
                            objPTR001.QA3WTC3ReturnBy = null;
                            objPTR001.QA3WTC3ReturnOn = null;
                            objPTR001.RetractedBy = null;
                            objPTR001.RetractedOn = null;
                            foreach (var i in objPTR001.PTR002)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR003)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR004)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR005)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR006)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Updated Successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteChemLinesDetails(int lineId, int headerId)
        {
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            try
            {
                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == headerId).FirstOrDefault();

                if (objPTR001 != null)
                {
                    PTR004 objPTR004 = db.PTR004.Where(i => i.LineId == lineId && i.HeaderId == headerId).FirstOrDefault();
                    if (objPTR004 != null)
                    {
                        db.PTR004.Remove(objPTR004);
                        if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                        {
                            objPTR001.RevNo = objPTR001.RevNo + 1;
                            objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                            objPTR001.InitiatorRemarks = null;
                            objPTR001.WE2Remarks = null;
                            objPTR001.QC3Remarks = null;
                            objPTR001.QA3WTC3Remarks = null;
                            objPTR001.SubmittedBy = null;
                            objPTR001.SubmittedOn = null;
                            objPTR001.WE2ApprovalBy = null;
                            objPTR001.WE2ApprovalOn = null;
                            objPTR001.WE2ReturnBy = null;
                            objPTR001.WE2ReturnOn = null;
                            objPTR001.QC3ApprovalBy = null;
                            objPTR001.QC3ApprovalOn = null;
                            objPTR001.QC3ReturnBy = null;
                            objPTR001.QC3ReturnOn = null;
                            objPTR001.QA3WTC3ApprovalBy = null;
                            objPTR001.QA3WTC3ApprovalOn = null;
                            objPTR001.QA3WTC3ReturnBy = null;
                            objPTR001.QA3WTC3ReturnOn = null;
                            objPTR001.RetractedBy = null;
                            objPTR001.RetractedOn = null;
                            foreach (var i in objPTR001.PTR002)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR003)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR004)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR005)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR006)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Deleted Successfully";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record not found";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Try Again";
            }

            return Json(objResponseMsg);
        }
        #endregion

        #region Heat Plate Details
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult HeatPlateDetails(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            PTR001 objPTR001 = new PTR001();
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            List<UserRoleDetails> role = GetRoleByLocation(objClsLoginInfo.UserName, objClsLoginInfo.Location);
            ViewBag.IsDifferentLocationUser = (objPTR001.Location.Trim().ToUpper() != objClsLoginInfo.Location.Trim().ToUpper());
            objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            if (CheckHeatPlateAllowToAdd(objPTR001.CouponCategory, objPTR001.Status, role))
                ViewBag.auth = "yes";
            else
                ViewBag.auth = "no";
            if (objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue()) || objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue()))
            {
                ViewBag.partNoRequired = true;
            }
            return View(objPTR001);
        }
        public ActionResult LoadHeatPlateDetailData(string auth, JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                var HeaderId = Convert.ToInt32(param.Headerid);
                string condition = string.Empty;
                condition += "where HeaderId=" + HeaderId + " ";
                bool isEditable = (auth == "yes" ? true : false);//&& (isHeatPlateLinesEditable(HeaderId));
                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                //if (!string.IsNullOrWhiteSpace(param.sSearch))
                //{
                //    condition += " and ( LineId like '%" + param.sSearch + "%'  or PartNo like '%" + param.sSearch + "%'  or HeatNo1 like '%" + param.sSearch + "%' or HeatNo2 like '%" + param.sSearch + "%' or Plate1 like '%" + param.sSearch + "%' or PlateNo2 like '%" + param.sSearch + "%'  )";
                //}
                string[] columnName = { "PartNo", "HeatNo1", "HeatNo2", "PlateNo1", "PlateNo2" };
                condition += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PTR_GET_HEAT_PLATE_LINE_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, condition
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                    "0",
                    GenerateTextbox(newRecordId,"PartNo","","","","",false,"","10"),
                    GenerateTextbox(newRecordId,"HeatNo1","","","","",false,"","20"),
                    GenerateTextbox(newRecordId,"PlateNo1","","","","",false,"","20"),
                    GenerateTextbox(newRecordId,"HeatNo2","","","","",false,"","20"),
                    GenerateTextbox(newRecordId,"PlateNo2","","","","",false,"","20"),
                    GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewStage();" ),
                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(HeaderId)),
                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.ROW_NO),
                                isEditable?GenerateTextbox(uc.LineId,"PartNo",uc.PartNo,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","10"):uc.PartNo,
                                isEditable?GenerateTextbox(uc.LineId,"HeatNo1",uc.HeatNo1,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HeatNo1,
                                isEditable?GenerateTextbox(uc.LineId,"PlateNo1",uc.PlateNo1,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.PlateNo1,
                                isEditable?GenerateTextbox(uc.LineId,"HeatNo2",uc.HeatNo2,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.HeatNo2,
                                isEditable?GenerateTextbox(uc.LineId,"PlateNo2",uc.PlateNo2,"UpdateLineDetails(this, "+ uc.LineId +","+uc.HeaderId+");","","",false,"","20"):uc.PlateNo2,
                                "<center>"+HTMLActionString(uc.LineId,"","Delete","Delete Record","fa fa-trash-o","DeleteLines(" + uc.LineId + "," + uc.HeaderId + ")",!isEditable)+"</center>",
                                Helper.GenerateHidden(uc.LineId, "HeaderId", Convert.ToString(uc.HeaderId)),
                         }).ToList();
                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveHeatPlateDetails(FormCollection fc)
        {
            PTR005 objPTR005 = new PTR005();
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            //string Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
            try
            {
                //string location = fc["Location"].Split('-')[0].Trim();
                //string auth = fc["auth"];
                //string status = fc["status"];
                //if (auth.Equals("yes") && (status.Equals("Draft") || status.Equals("Closed")))
                //{
                int newRowIndex = 0;
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId" + newRowIndex]) ? Convert.ToInt32(fc["HeaderId" + newRowIndex]) : 0;
                if (refHeaderId > 0)
                {
                    PTR001 objPTR001 = db.PTR001.Where(m => m.HeaderId == refHeaderId).FirstOrDefault();
                    objPTR005.HeaderId = objPTR001.HeaderId;
                    objPTR005.Project = objPTR001.Project;
                    objPTR005.BU = objPTR001.BU;
                    objPTR005.Location = objPTR001.Location;
                    objPTR005.PTRNo = objPTR001.PTRNo;
                    if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                    {
                        objPTR005.RevNo = Convert.ToInt32(objPTR001.RevNo) + 1;
                        objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                        objPTR001.RevNo = Convert.ToInt32(objPTR001.RevNo) + 1;
                        objPTR001.InitiatorRemarks = null;
                        objPTR001.WE2Remarks = null;
                        objPTR001.QC3Remarks = null;
                        objPTR001.QA3WTC3Remarks = null;
                        objPTR001.SubmittedBy = null;
                        objPTR001.SubmittedOn = null;
                        objPTR001.WE2ApprovalBy = null;
                        objPTR001.WE2ApprovalOn = null;
                        objPTR001.WE2ReturnBy = null;
                        objPTR001.WE2ReturnOn = null;
                        objPTR001.QC3ApprovalBy = null;
                        objPTR001.QC3ApprovalOn = null;
                        objPTR001.QC3ReturnBy = null;
                        objPTR001.QC3ReturnOn = null;
                        objPTR001.QA3WTC3ApprovalBy = null;
                        objPTR001.QA3WTC3ApprovalOn = null;
                        objPTR001.QA3WTC3ReturnBy = null;
                        objPTR001.QA3WTC3ReturnOn = null;
                        objPTR001.RetractedBy = null;
                        objPTR001.RetractedOn = null;
                        foreach (var i in objPTR001.PTR002)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR003)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR004)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR005)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR006)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                    }
                    else
                    {
                        objPTR005.RevNo = objPTR001.RevNo;
                    }
                    objPTR005.PartNo = fc["PartNo" + newRowIndex];
                    objPTR005.HeatNo1 = fc["HeatNo1" + newRowIndex];
                    objPTR005.PlateNo1 = fc["PlateNo1" + newRowIndex];
                    objPTR005.HeatNo2 = fc["HeatNo2" + newRowIndex];
                    objPTR005.PlateNo2 = fc["PlateNo2" + newRowIndex];
                    objPTR005.CreatedOn = DateTime.Now;
                    objPTR005.CreatedBy = objClsLoginInfo.UserName;
                    db.PTR005.Add(objPTR005);
                    db.SaveChanges();
                    objResponseMsg.Value = "Added Succesfully";
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Value = "Please Try Again";
                    objResponseMsg.Key = false;
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateHeatPlateLine(int lineId, int headerId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PTR005 objPTR005 = new PTR005();
            try
            {
                if (!string.IsNullOrEmpty(columnName))
                {
                    PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == headerId).FirstOrDefault();
                    objPTR005 = db.PTR005.Where(i => i.LineId == lineId).FirstOrDefault();
                    if ((columnName == "PartNo")
                        && objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue()) || objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue())
                        && string.IsNullOrEmpty(columnValue))
                    {

                    }
                    else
                    {
                        db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, "PTR005", objClsLoginInfo.UserName);

                        if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                        {
                            objPTR005.RevNo = objPTR005.RevNo + 1;
                            objPTR001.RevNo = objPTR001.RevNo + 1;
                            objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                            objPTR001.InitiatorRemarks = null;
                            objPTR001.WE2Remarks = null;
                            objPTR001.QC3Remarks = null;
                            objPTR001.QA3WTC3Remarks = null;
                            objPTR001.SubmittedBy = null;
                            objPTR001.SubmittedOn = null;
                            objPTR001.WE2ApprovalBy = null;
                            objPTR001.WE2ApprovalOn = null;
                            objPTR001.WE2ReturnBy = null;
                            objPTR001.WE2ReturnOn = null;
                            objPTR001.QC3ApprovalBy = null;
                            objPTR001.QC3ApprovalOn = null;
                            objPTR001.QC3ReturnBy = null;
                            objPTR001.QC3ReturnOn = null;
                            objPTR001.QA3WTC3ApprovalBy = null;
                            objPTR001.QA3WTC3ApprovalOn = null;
                            objPTR001.QA3WTC3ReturnBy = null;
                            objPTR001.QA3WTC3ReturnOn = null;
                            objPTR001.RetractedBy = null;
                            objPTR001.RetractedOn = null;
                            foreach (var i in objPTR001.PTR002)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR003)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR004)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR005)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                            foreach (var i in objPTR001.PTR006)
                            {
                                i.RevNo = objPTR001.RevNo;
                            }
                        }
                    }

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Updated Successfully";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteHeatLines(int lineId, int headerId)
        {
            ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
            try
            {
                PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == headerId).FirstOrDefault();

                if (objPTR001 != null)
                {
                    PTR005 objPTR005 = db.PTR005.Where(i => i.LineId == lineId && i.HeaderId == headerId).FirstOrDefault();
                    db.PTR005.Remove(objPTR005);
                    if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                    {
                        objPTR001.RevNo = objPTR001.RevNo + 1;
                        objPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                        objPTR001.InitiatorRemarks = null;
                        objPTR001.WE2Remarks = null;
                        objPTR001.QC3Remarks = null;
                        objPTR001.QA3WTC3Remarks = null;
                        objPTR001.SubmittedBy = null;
                        objPTR001.SubmittedOn = null;
                        objPTR001.WE2ApprovalBy = null;
                        objPTR001.WE2ApprovalOn = null;
                        objPTR001.WE2ReturnBy = null;
                        objPTR001.WE2ReturnOn = null;
                        objPTR001.QC3ApprovalBy = null;
                        objPTR001.QC3ApprovalOn = null;
                        objPTR001.QC3ReturnBy = null;
                        objPTR001.QC3ReturnOn = null;
                        objPTR001.QA3WTC3ApprovalBy = null;
                        objPTR001.QA3WTC3ApprovalOn = null;
                        objPTR001.QA3WTC3ReturnBy = null;
                        objPTR001.QA3WTC3ReturnOn = null;
                        objPTR001.RetractedBy = null;
                        objPTR001.RetractedOn = null;

                        foreach (var i in objPTR001.PTR002)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR003)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR004)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR005)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                        foreach (var i in objPTR001.PTR006)
                        {
                            i.RevNo = objPTR001.RevNo;
                        }
                    }
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Deleted Successfully";

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Try Again";
            }

            return Json(objResponseMsg);
        }
        #endregion

        #region Test Completion Details
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult TestCompletionDetails(int HeaderId)
        {
            ViewBag.HeaderId = HeaderId;
            PTR001 objPTR001 = new PTR001();
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            List<UserRoleDetails> role = GetRoleByLocation(objClsLoginInfo.UserName, objClsLoginInfo.Location);
            ViewBag.IsDifferentLocationUser = (objPTR001.Location.Trim().ToUpper() != objClsLoginInfo.Location.Trim().ToUpper());
            objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            if (CheckTestCompletionAllowToAdd(objPTR001.CouponCategory, objPTR001.Status, role))
                ViewBag.auth = "yes";
            else
                ViewBag.auth = "no";
            return View(objPTR001);
        }
        public ActionResult LoadTestCompletionDetailData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                var HeaderId = Convert.ToInt32(param.Headerid);
                string auth = param.MTStatus;
                string condition = string.Empty;
                condition += "where HeaderId=" + HeaderId + " ";


                //if (!string.IsNullOrWhiteSpace(param.sSearch))
                //{
                //    condition += " and ( LineId like '%" + param.sSearch + "%'  or CouponNo like '%" + param.sSearch + "%'  or CouponId like '%" + param.sSearch + "%' or Remarks like '%" + param.sSearch + "%'   )";
                //}
                string[] columnName = { "CouponNo", "CouponId", "Remarks" };
                condition += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PTR_GET_TEST_COMPLETION_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, condition
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                       Helper.GenerateTextbox(newRecordId,"CouponNo","","",false,"","9","numeric CouponNo",false,"","Coupon No"),
                                       Helper.GenerateTextbox(newRecordId,"CouponID","","",false,"","10","CouponID",false,"","Coupon ID"),
                                       "<input type='date' name='MaterialReceiptDate"+newRecordId+"' id='MaterialReceiptDate"+newRecordId+"' class='form-control'>",
                                       "<input type='date' name='MechTestCompletionDate"+newRecordId+"' id='MechTestCompletionDate"+newRecordId+"' class='form-control'>",
                                       "<input type='date' name='ChecmicalTestCompletionTime"+newRecordId+"' id='ChecmicalTestCompletionTime"+newRecordId+"' class='form-control'>",
                                       Helper.GenerateTextbox(newRecordId,"Remarks","","",false,"","50","Remarks",false,"","Remarks"),
                                       GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewStage();" ),
                                       Convert.ToString(newRecordId)
                                      };

                var data = (from uc in lstResult
                            select new[]
                            {
                             //Convert.ToString(uc.CouponNo),
                             //Convert.ToString(uc.CouponID),
                             Helper.GenerateTextbox(uc.LineId,"CouponNo",Convert.ToString(uc.CouponNo.Value),"",false,"","9","numeric CouponNo",false,"","Coupon No"),
                             Helper.GenerateTextbox(uc.LineId,"CouponID",Convert.ToString(uc.CouponID),"",false,"","10","CouponID",false,"","Coupon ID"),
                             //"<input type='date' name='MaterialReceiptDate' class='form-control' value="+uc.MaterialReceiptDate!=null ?DateTime.Parse(uc.MaterialReceiptDate.ToString()).ToString("yyyy-MM-dd"):null+" >",
                             //"<input type='date' name='MechTestCompletionDate' class='form-control' value="+uc.MechTestCompletionDate.ToString()!=""?DateTime.Parse(uc.MechTestCompletionDate.ToString()).ToString("yyyy-MM-dd"):null + " >",
                             //"<input type='date' name='ChecmicalTestCompletionTime' class='form-control' value="+uc.ChecmicalTestCompletionTime.ToString()!=""?DateTime.Parse(uc.ChecmicalTestCompletionTime.ToString()).ToString("yyyy-MM-dd"):null + " >",
                             "<input type='date' name='MaterialReceiptDate' class='form-control' value="+dateV(uc.MaterialReceiptDate)+" >",
                             "<input type='date' name='MechTestCompletionDate' class='form-control' value="+dateV(uc.MechTestCompletionDate)+ " >",
                             "<input type='date' name='ChecmicalTestCompletionTime' class='form-control' value="+dateV(uc.ChecmicalTestCompletionTime)+" >",
                             Helper.GenerateTextbox(uc.LineId,"Remarks",uc.Remarks,"",false,"","50","Remarks",false,"","Remarks"),                                        
                             //"<input type='hidden' name='LineId' value="+uc.LineId+">"
                             "",
                            Convert.ToString(uc.LineId)
                         }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveTestCompletionDetails(PTR006 objPTR)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!objPTR.CouponNo.HasValue)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Enter CouponNo!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                else if (string.IsNullOrEmpty(objPTR.CouponID) || string.IsNullOrWhiteSpace(objPTR.CouponID))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Enter CouponID!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                var objPTR001 = db.PTR001.Where(i => i.HeaderId == objPTR.HeaderId).FirstOrDefault();

                PTR006 objPTR006New = new PTR006();
                objPTR006New.HeaderId = objPTR.HeaderId;
                objPTR006New.BU = objPTR001.BU;
                objPTR006New.CouponID = objPTR.CouponID;
                objPTR006New.CouponNo = objPTR.CouponNo;
                objPTR006New.CreatedBy = objClsLoginInfo.UserName;
                objPTR006New.CreatedOn = DateTime.Now;
                objPTR006New.Location = objPTR001.Location;
                objPTR006New.ChecmicalTestCompletionTime = objPTR.ChecmicalTestCompletionTime;
                objPTR006New.MaterialReceiptDate = objPTR.MaterialReceiptDate;
                objPTR006New.MechTestCompletionDate = objPTR.MechTestCompletionDate;
                objPTR006New.Project = objPTR001.Project;
                objPTR006New.PTRNo = objPTR001.PTRNo;
                objPTR006New.Remarks = objPTR.Remarks;
                objPTR006New.RevNo = objPTR001.RevNo;

                db.PTR006.Add(objPTR006New);
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Test Completion Details Save Successfully!";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public void addTestCompletionDetail(int HeaderId, int CouponNo, string action)
        {
            PTR002 srcPTR002 = db.PTR002.Where(x => x.HeaderId == HeaderId && x.CouponNo == CouponNo).FirstOrDefault();
            if (action.Equals("Insert"))
            {
                PTR006 objPTR006 = new PTR006();
                objPTR006.HeaderId = HeaderId;
                objPTR006.Project = srcPTR002.Project;
                objPTR006.Location = srcPTR002.Location;
                objPTR006.BU = srcPTR002.BU;
                objPTR006.RevNo = srcPTR002.RevNo;
                objPTR006.PTRNo = srcPTR002.PTRNo;
                objPTR006.CouponNo = srcPTR002.CouponNo;
                objPTR006.CouponID = srcPTR002.CouponID;
                objPTR006.CreatedBy = objClsLoginInfo.UserName;
                objPTR006.CreatedOn = DateTime.Now;
                objPTR006.EditedBy = "";
                objPTR006.EditedOn = null;
                db.PTR006.Add(objPTR006);
                db.SaveChanges();
            }
            else if (action.Equals("Update"))
            {
                PTR006 objPTR006 = db.PTR006.Where(x => x.HeaderId == HeaderId && x.CouponNo == CouponNo).FirstOrDefault();
                if (objPTR006 != null)
                {
                    objPTR006.CouponNo = srcPTR002.CouponNo;
                    objPTR006.CouponID = srcPTR002.CouponID;
                    objPTR006.EditedBy = objClsLoginInfo.UserName;
                    objPTR006.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }
            }
            else if (action.Equals("Delete"))
            {
                PTR006 objPTR006 = db.PTR006.Where(x => x.HeaderId == HeaderId && x.CouponNo == CouponNo).FirstOrDefault();
                if (objPTR006 != null)
                {
                    db.PTR006.Remove(objPTR006);
                    db.SaveChanges();
                }
            }

        }
        [HttpPost]
        public ActionResult UpdateMaterialReceiptDate(int LineID, string changeText)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PTR006 objPTR006 = db.PTR006.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objPTR006 != null)
                {
                    if (!string.IsNullOrWhiteSpace(changeText))
                    {
                        objPTR006.MaterialReceiptDate = Helper.ToNullIfTooEarlyForDb(DateTime.ParseExact(changeText, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        objPTR006.MaterialReceiptDate = (DateTime?)null;
                    }
                    objPTR006.EditedBy = objClsLoginInfo.UserName;
                    objPTR006.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Material Receipt Date successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Line not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateMechTestCompletionDate(int LineID, string changeText)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PTR006 objPTR006 = db.PTR006.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objPTR006 != null)
                {
                    if (!string.IsNullOrWhiteSpace(changeText))
                    {
                        objPTR006.MechTestCompletionDate = Helper.ToNullIfTooEarlyForDb(DateTime.ParseExact(changeText, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        objPTR006.MechTestCompletionDate = (DateTime?)null;
                    }
                    objPTR006.EditedBy = objClsLoginInfo.UserName;
                    objPTR006.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Mech Test Completion Date successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Line not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateChecmicalTestCompletionTime(int LineID, string changeText)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PTR006 objPTR006 = db.PTR006.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objPTR006 != null)
                {
                    if (!string.IsNullOrWhiteSpace(changeText))
                    {
                        objPTR006.ChecmicalTestCompletionTime = Helper.ToNullIfTooEarlyForDb(DateTime.ParseExact(changeText, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture));
                    }
                    else
                    {
                        objPTR006.ChecmicalTestCompletionTime = (DateTime?)null;
                    }
                    objPTR006.EditedBy = objClsLoginInfo.UserName;
                    objPTR006.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Checmical Test Completion Date successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Line not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateRemarks(int LineID, string changeText)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PTR006 objPTR006 = db.PTR006.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objPTR006 != null)
                {
                    if (!string.IsNullOrWhiteSpace(changeText))
                    {
                        objPTR006.Remarks = changeText;
                    }
                    else
                    {
                        objPTR006.Remarks = null;
                    }
                    objPTR006.EditedBy = objClsLoginInfo.UserName;
                    objPTR006.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Remarks successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Line not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateCouponIdCouponNo(int LineID, string changeText, string colName)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PTR006 objPTR006 = db.PTR006.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objPTR006 != null)
                {
                    if (colName == "CouponNo")
                    {
                        if (!string.IsNullOrWhiteSpace(changeText))
                        {
                            objPTR006.CouponNo = Convert.ToInt32(changeText);
                        }
                        else
                        {
                            objPTR006.CouponNo = null;
                        }
                    }
                    else if (colName == "CouponID")
                    {
                        if (!string.IsNullOrWhiteSpace(changeText))
                        {
                            objPTR006.CouponID = changeText;
                        }
                        else
                        {
                            objPTR006.CouponID = null;
                        }
                    }
                    else if (colName == "Remarks")
                    {
                        if (!string.IsNullOrWhiteSpace(changeText))
                        {
                            objPTR006.Remarks = changeText;
                        }
                        else
                        {
                            objPTR006.Remarks = null;
                        }
                    }
                    objPTR006.EditedBy = objClsLoginInfo.UserName;
                    objPTR006.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Remarks successfully updated.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Line not available";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #region old code
        //[HttpPost]
        //public ActionResult SaveTestCompletionDetails(FormCollection fc)
        //{
        //    ResponceMsgWithHeaderId objResponseMsg = new ResponceMsgWithHeaderId();
        //    try
        //    {

        //        var LineId = fc["LineId"].Split(',');
        //        var MaterialReceiptDate = fc["MaterialReceiptDate"].Split(',');
        //        var MechTestCompletionDate = fc["MechTestCompletionDate"].Split(',');
        //        var ChecmicalTestCompletionTime = fc["ChecmicalTestCompletionTime"].Split(',');
        //        var Remarks = fc["Remarks"].Split(',');
        //        for (int i = 0; i < LineId.Length; i++)
        //        {
        //            int lineid = Convert.ToInt32(LineId[i]);
        //            PTR006 objPTR006 = db.PTR006.Where(x => x.LineId == lineid).FirstOrDefault();
        //            objPTR006.MaterialReceiptDate = Helper.ToNullIfTooEarlyForDb(MaterialReceiptDate[i] != "" ? DateTime.ParseExact(MaterialReceiptDate[i], "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture) : (DateTime?)null);
        //            objPTR006.MechTestCompletionDate = Helper.ToNullIfTooEarlyForDb(MechTestCompletionDate[i] != "" ? DateTime.ParseExact(MechTestCompletionDate[i], "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture) : (DateTime?)null);
        //            objPTR006.ChecmicalTestCompletionTime = Helper.ToNullIfTooEarlyForDb(ChecmicalTestCompletionTime[i] != "" ? DateTime.ParseExact(ChecmicalTestCompletionTime[i], "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture) : (DateTime?)null);
        //            //objPTR006.MaterialReceiptDate = Helper.ToNullIfTooEarlyForDb(Convert.ToDateTime(MaterialReceiptDate[i]));
        //            //objPTR006.MechTestCompletionDate = Helper.ToNullIfTooEarlyForDb(Convert.ToDateTime(MechTestCompletionDate[i]));
        //            //objPTR006.ChecmicalTestCompletionTime = Helper.ToNullIfTooEarlyForDb(Convert.ToDateTime(ChecmicalTestCompletionTime[i]));
        //            objPTR006.Remarks = Remarks[i];
        //            objPTR006.EditedBy = objClsLoginInfo.UserName;
        //            objPTR006.EditedOn = DateTime.Now;
        //            db.SaveChanges();
        //        }
        //        objResponseMsg.Key = true;
        //        objResponseMsg.Value = "Updated Successfully";
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = "Please Try Again";
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        #endregion
        #endregion

        #region Submit PTR
        [HttpPost]
        public ActionResult chkMechChemHTLines(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PTR001 objPTR001 = db.PTR001.Where(u => u.HeaderId == HeaderId).FirstOrDefault();
                int count = 0;
                string msg = "";
                if (((objPTR001.HTDetail.HasValue ? (bool)objPTR001.HTDetail : false)) && !db.PTR002.Any(x => x.HeaderId == HeaderId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Maintain HT Lines";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (objPTR001.MechTest.Equals("true", StringComparison.OrdinalIgnoreCase))
                {
                    if (!db.PTR003.Any(i => i.HeaderId == HeaderId))
                    {
                        count++;
                        msg += " Mechanical ";
                    }
                }
                if (objPTR001.ChemicalTest.Equals("true", StringComparison.OrdinalIgnoreCase))
                {
                    if (!db.PTR004.Any(i => i.HeaderId == HeaderId))
                    {
                        msg += count == 0 ? " Chemical " : " and Chemical ";
                        count++;
                    }
                }
                if (count == 0)
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Maintain" + msg + "Test Details";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (chkHeatPlateLines(HeaderId))
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Maintain Heat & Plate No Details";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (chkTestCompletionDetails(HeaderId))
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Maintain Test Completion Details";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                //if (objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue()))
                //{
                //    PTR002 objPTR002 = db.PTR002.Where(u => u.HeaderId == HeaderId).FirstOrDefault();
                //    if (objPTR002 != null)
                //    {
                //        objResponseMsg.Key = true;
                //    }
                //    else
                //    {
                //        objResponseMsg.Key = false;
                //        objResponseMsg.Value = "Please Maintain HT Lines";
                //    }
                //}
                //else
                //    objResponseMsg.Key = true;

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        public bool chkHTMechChemTestDetails(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PTR001 objPTR001 = db.PTR001.Where(u => u.HeaderId == HeaderId).FirstOrDefault();
                if (((objPTR001.HTDetail.HasValue ? (bool)objPTR001.HTDetail : false)) && !db.PTR002.Any(x => x.HeaderId == HeaderId))
                {
                    return false;
                }
                if (objPTR001.MechTest.Equals("true", StringComparison.OrdinalIgnoreCase))
                {
                    if (!db.PTR003.Any(i => i.HeaderId == HeaderId))
                    {
                        return false;
                    }
                }
                if (objPTR001.ChemicalTest.Equals("true", StringComparison.OrdinalIgnoreCase))
                {
                    if (!db.PTR004.Any(i => i.HeaderId == HeaderId))
                    {
                        return false;
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return false;
            }
        }
        [NonAction]
        public bool chkHeatPlateLines(int HeaderId)
        {
            PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue()) && (objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue()) || objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue())))
            {
                if (db.PTR005.Any(x => x.HeaderId == HeaderId))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            return true;
        }
        [NonAction]
        public bool chkTestCompletionDetails(int HeaderId)
        {
            PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue()))
            {
                foreach (var ptr006 in objPTR001.PTR006)
                {
                    if ((ptr006.MaterialReceiptDate != null) || (ptr006.ChecmicalTestCompletionTime != null) || (ptr006.MechTestCompletionDate != null))
                    { }
                    else
                        return false;
                }
            }
            return true;
        }
        [HttpPost]
        public ActionResult SendtoCompiler(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string compileStatus = clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue();
            try
            {
                string strPMG3 = clsImplementationEnum.UserRoleName.PMG3.GetStringValue();
                string strMCC3 = clsImplementationEnum.UserRoleName.MCC3.GetStringValue();
                string strSCC3 = clsImplementationEnum.UserRoleName.SCC3.GetStringValue();
                string strWE3 = clsImplementationEnum.UserRoleName.WE3.GetStringValue();
                string strWE2 = clsImplementationEnum.UserRoleName.WE2.GetStringValue();
                string strQC3 = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                string strQA3 = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
                string strWTC3 = clsImplementationEnum.UserRoleName.WTC3.GetStringValue();

                PTR001 objPTR001 = db.PTR001.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                String status = objPTR001.Status;
                String CouCat = objPTR001.CouponCategory;
                if (chkAuth(CouCat, status))
                {
                    Hashtable _ht = new Hashtable();
                    EmailSend _objEmail = new EmailSend();
                    string link = WebsiteURL + "/PTR/MaintainPTR/PTRDetails?HId=" + objPTR001.HeaderId;
                    _ht["[Document]"] = objPTR001.PTRNo;
                    _ht["[Link]"] = link;
                    _ht["[Subject]"] = "PTR " + objPTR001.PTRNo + " Sent For Approval";
                    _ht["[Regards]"] = Manager.GetUserNameFromPsNo(objClsLoginInfo.UserName);
                    string approver = "";
                    MAIL001 objTemplateMaster;
                    if (status.Equals(clsImplementationEnum.PTRStatus.Draft.GetStringValue()))
                    {
                        //PTR002 objPTR002 = db.PTR002.Where(u => u.HeaderId == HeaderId).FirstOrDefault();
                        //if (objPTR002 != null)
                        //{
                        if (chkHTMechChemTestDetails(HeaderId))
                        {
                            if (CouCat.Equals(clsImplementationEnum.CouponCat.PerfQualf.GetStringValue()) ||
                            CouCat.Equals(clsImplementationEnum.CouponCat.WaterAnls.GetStringValue()))
                            {
                                objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue();
                                //objPTR001.QA3WTC3ApprovalBy = objClsLoginInfo.UserName;
                                //objPTR001.QA3WTC3ApprovalOn = DateTime.Now;
                                approver += db.ATH004.Where(x => x.Role.Equals(strQA3)).Select(x => x.Description).SingleOrDefault().ToString();
                                approver += " And " + db.ATH004.Where(x => x.Role.Equals(strWTC3)).Select(x => x.Description).SingleOrDefault().ToString();
                                _objEmail.MailToAdd = getEmailIdByRole(getRoleId(strQA3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strWTC3), objPTR001);
                                //if (CouCat.Equals(clsImplementationEnum.CouponCat.PerfQualf.GetStringValue()))
                                //    _objEmail.MailCc = getEmailIdByRole(getRoleId(strWE3), objPTR001);
                                //else
                                //    _objEmail.MailCc = getEmailIdByRole(getRoleId(strQC3), objPTR001);
                            }
                            else if (CouCat.Equals(clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue()))
                            {
                                objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue();
                                approver += db.ATH004.Where(x => x.Role.Equals(strQC3)).Select(x => x.Description).SingleOrDefault().ToString();
                                //objPTR001.QC3ApprovalBy = objClsLoginInfo.UserName;
                                //objPTR001.QC3ApprovalOn = DateTime.Now;                                
                                //_objEmail.MailToAdd = getEmailIdByRole(getRoleId(strQC3), objPTR001);
                                //_objEmail.MailCc = getEmailIdByRole(getRoleId(strPMG3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strMCC3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strSCC3), objPTR001);
                                if (!string.IsNullOrEmpty(objPTR001.QC3ID))
                                {
                                    _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.QC3ID);
                                }
                            }
                            else
                            {
                                objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue();
                                approver += db.ATH004.Where(x => x.Role.Equals(strWE2)).Select(x => x.Description).SingleOrDefault().ToString();
                                //_objEmail.MailToAdd = getEmailIdByRole(getRoleId(strWE2), objPTR001);                                
                                //_objEmail.MailCc = getEmailIdByRole(getRoleId(strWE3), objPTR001);
                                //objPTR001.WE2ApprovalBy = objClsLoginInfo.UserName;
                                //objPTR001.WE2ApprovalOn = DateTime.Now;
                                if (!string.IsNullOrEmpty(objPTR001.WE2ID))
                                {
                                    _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.WE2ID);
                                }
                            }
                            objPTR001.SubmittedBy = objClsLoginInfo.UserName;
                            objPTR001.SubmittedOn = DateTime.Now;
                            // To resolve bug 26585 - Submit->Retrun->Submit
                            objPTR001.WE2ReturnBy = null;
                            objPTR001.WE2ReturnOn = null;
                            objPTR001.QC3ReturnBy = null;
                            objPTR001.QC3ReturnOn = null;
                            objPTR001.QA3WTC3ReturnBy = null;
                            objPTR001.QA3WTC3ReturnOn = null;
                            //addTestCompletionDetail(HeaderId);
                            _objEmail.MailCc = Manager.GetMailIdFromPsNo(objPTR001.CreatedBy);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Please Maintain Mechanical/Chemical Test Details";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else if (status.Equals(clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue()))
                    {
                        //_objEmail.MailCc = getEmailIdByRole(getRoleId(strWE2), objPTR001);
                        _objEmail.MailCc = Manager.GetMailIdFromPsNo(objPTR001.CreatedBy);
                        if (CouCat.Equals(clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue()))
                        {
                            objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue();
                            approver += db.ATH004.Where(x => x.Role.Equals(strQC3)).Select(x => x.Description).SingleOrDefault().ToString();
                            //_objEmail.MailToAdd = getEmailIdByRole(getRoleId(strQC3), objPTR001);
                            //objPTR001.QC3ApprovalBy = objClsLoginInfo.UserName;
                            //objPTR001.QC3ApprovalOn = DateTime.Now;
                            if (!string.IsNullOrEmpty(objPTR001.QC3ID))
                            {
                                _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.QC3ID);
                            }
                            if (!string.IsNullOrEmpty(objPTR001.WE2ID))
                            {
                                _objEmail.MailCc += ";" + Manager.GetMailIdFromPsNo(objPTR001.WE2ID);
                            }
                        }
                        else
                        {
                            objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue();
                            //objPTR001.QA3WTC3ApprovalBy = objClsLoginInfo.UserName;
                            //objPTR001.QA3WTC3ApprovalOn = DateTime.Now;
                            approver += db.ATH004.Where(x => x.Role.Equals(strQA3)).Select(x => x.Description).SingleOrDefault().ToString();
                            approver += " And " + db.ATH004.Where(x => x.Role.Equals(strWTC3)).Select(x => x.Description).SingleOrDefault().ToString();
                            _objEmail.MailToAdd = getEmailIdByRole(getRoleId(strQA3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strWTC3), objPTR001);
                        }
                        objPTR001.WE2ApprovalBy = objClsLoginInfo.UserName;
                        objPTR001.WE2ApprovalOn = DateTime.Now;
                    }
                    else if (status.Equals(clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue()))
                    {
                        if (chkHeatPlateLines(HeaderId))
                        {
                            objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue();
                            //objPTR001.QA3WTC3ApprovalBy = objClsLoginInfo.UserName;
                            //objPTR001.QA3WTC3ApprovalOn = DateTime.Now;
                            objPTR001.QC3ApprovalBy = objClsLoginInfo.UserName;
                            objPTR001.QC3ApprovalOn = DateTime.Now;
                            approver += db.ATH004.Where(x => x.Role.Equals(strQA3)).Select(x => x.Description).SingleOrDefault().ToString();
                            approver += " And " + db.ATH004.Where(x => x.Role.Equals(strWTC3)).Select(x => x.Description).SingleOrDefault().ToString();
                            //_objEmail.MailCc = getEmailIdByRole(getRoleId(strQC3), objPTR001);
                            _objEmail.MailCc = Manager.GetMailIdFromPsNo(objPTR001.CreatedBy);
                            if (!string.IsNullOrEmpty(objPTR001.QC3ID))
                            {
                                _objEmail.MailCc += ";" + Manager.GetMailIdFromPsNo(objPTR001.QC3ID);
                            }
                            _objEmail.MailToAdd = getEmailIdByRole(getRoleId(strQA3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strWTC3), objPTR001);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Please Maintain Heat & Plate No Details";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else if (status.Equals(clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue()))
                    {
                        if (chkTestCompletionDetails(HeaderId))
                        {
                            _ht["[Subject]"] = "PTR " + objPTR001.PTRNo + " Closure";
                            //_objEmail.MailCc = getEmailIdByRole(getRoleId(strQA3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strWTC3), objPTR001);
                            approver += Manager.GetUserNameFromPsNo(objPTR001.CreatedBy);
                            _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.CreatedBy);
                            if (objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue()))
                            {
                                approver += db.ATH004.Where(x => x.Role.Equals(strQC3)).Select(x => x.Description).SingleOrDefault().ToString();
                                //_objEmail.MailToAdd += ";" + getEmailIdByRole(getRoleId(strQC3), objPTR001);
                                if (!string.IsNullOrEmpty(objPTR001.QC3ID))
                                {
                                    _objEmail.MailCc = Manager.GetMailIdFromPsNo(objPTR001.QC3ID);
                                }
                            }
                            _ht["[Initiator]"] = approver;
                            objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.PTR.Close).SingleOrDefault();
                            _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                            objPTR001.Status = clsImplementationEnum.PTRStatus.Closed.GetStringValue();
                            objPTR001.QA3WTC3ApprovalOn = DateTime.Now;
                            objPTR001.QA3WTC3ApprovalBy = objClsLoginInfo.UserName;
                            db.SaveChanges();
                            db.SP_PTR_CLOSE(objPTR001.HeaderId);

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CTQMessages.CompilerAction.ToString();
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Please Maintain Test Completion Details";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else if (status.Equals(clsImplementationEnum.PTRStatus.RetWE2.GetStringValue()))
                    {
                        //PTR002 objPTR002 = db.PTR002.Where(u => u.HeaderId == HeaderId).FirstOrDefault();
                        //if (objPTR002 != null)
                        //{
                        if (chkHTMechChemTestDetails(HeaderId))
                        {
                            objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue();
                            //objPTR001.WE2ApprovalBy = objClsLoginInfo.UserName;
                            //objPTR001.WE2ApprovalOn = DateTime.Now;
                            objPTR001.SubmittedBy = objClsLoginInfo.UserName;
                            objPTR001.SubmittedOn = DateTime.Now;
                            // To resolve bug 26585 - Submit->Retrun->Submit
                            objPTR001.WE2ReturnBy = null;
                            objPTR001.WE2ReturnOn = null;
                            objPTR001.QC3ReturnBy = null;
                            objPTR001.QC3ReturnOn = null;
                            objPTR001.QA3WTC3ReturnBy = null;
                            objPTR001.QA3WTC3ReturnOn = null;
                            approver += db.ATH004.Where(x => x.Role.Equals(strWE2)).Select(x => x.Description).SingleOrDefault().ToString();
                            //_objEmail.MailToAdd = getEmailIdByRole(getRoleId(strWE2), objPTR001);
                            //_objEmail.MailCc = getEmailIdByRole(getRoleId(strWE3), objPTR001);

                            if (!string.IsNullOrEmpty(objPTR001.WE2ID))
                            {
                                _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.WE2ID);
                            }
                            _objEmail.MailCc = Manager.GetMailIdFromPsNo(objPTR001.CreatedBy);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Please Maintain Mechanical/Chemical Test Details";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else if (status.Equals(clsImplementationEnum.PTRStatus.RetQC3.GetStringValue()))
                    {
                        //PTR002 objPTR002 = db.PTR002.Where(u => u.HeaderId == HeaderId).FirstOrDefault();
                        //if (objPTR002 != null)
                        //{
                        if (chkHTMechChemTestDetails(HeaderId))
                        {
                            //_objEmail.MailCc = getEmailIdByRole(getRoleId(strWE3), objPTR001);
                            if (CouCat.Equals(clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue()))
                            {
                                objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue();
                                //objPTR001.QC3ApprovalBy = objClsLoginInfo.UserName;
                                //objPTR001.QC3ApprovalOn = DateTime.Now;
                                objPTR001.SubmittedBy = objClsLoginInfo.UserName;
                                objPTR001.SubmittedOn = DateTime.Now;
                                // To resolve bug 26585 - Submit->Retrun->Submit
                                objPTR001.WE2ReturnBy = null;
                                objPTR001.WE2ReturnOn = null;
                                objPTR001.QC3ReturnBy = null;
                                objPTR001.QC3ReturnOn = null;
                                objPTR001.QA3WTC3ReturnBy = null;
                                objPTR001.QA3WTC3ReturnOn = null;
                                approver += db.ATH004.Where(x => x.Role.Equals(strQC3)).Select(x => x.Description).SingleOrDefault().ToString();
                                //_objEmail.MailToAdd = getEmailIdByRole(getRoleId(strQC3), objPTR001);

                                if (!string.IsNullOrEmpty(objPTR001.QC3ID))
                                {
                                    _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.QC3ID);
                                }
                                if (!string.IsNullOrEmpty(objPTR001.WE2ID))
                                {
                                    _objEmail.MailCc += ";" + Manager.GetMailIdFromPsNo(objPTR001.WE2ID);
                                }
                                _objEmail.MailCc += ";" + Manager.GetMailIdFromPsNo(objPTR001.CreatedBy);
                            }
                            else if (CouCat.Equals(clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue()))
                            {
                                objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue();
                                //objPTR001.WE2ApprovalBy = objClsLoginInfo.UserName;
                                //objPTR001.WE2ApprovalOn = DateTime.Now;
                                objPTR001.SubmittedBy = objClsLoginInfo.UserName;
                                objPTR001.SubmittedOn = DateTime.Now;
                                // To resolve bug 26585 - Submit->Retrun->Submit
                                objPTR001.WE2ReturnBy = null;
                                objPTR001.WE2ReturnOn = null;
                                objPTR001.QC3ReturnBy = null;
                                objPTR001.QC3ReturnOn = null;
                                objPTR001.QA3WTC3ReturnBy = null;
                                objPTR001.QA3WTC3ReturnOn = null;
                                approver += db.ATH004.Where(x => x.Role.Equals(strWE2)).Select(x => x.Description).SingleOrDefault().ToString();
                                //_objEmail.MailToAdd = getEmailIdByRole(getRoleId(strWE2), objPTR001);
                                if (!string.IsNullOrEmpty(objPTR001.WE2ID))
                                {
                                    _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.WE2ID);
                                }
                                _objEmail.MailCc = Manager.GetMailIdFromPsNo(objPTR001.CreatedBy);
                            }
                            //objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue();
                            //objPTR001.QC3ApprovalBy = objClsLoginInfo.UserName;
                            //objPTR001.QC3ApprovalOn = DateTime.Now;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Please Maintain Mechanical/Chemical Test Details";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    else if (status.Equals(clsImplementationEnum.PTRStatus.RetQA3.GetStringValue()))
                    {
                        //PTR002 objPTR002 = db.PTR002.Where(u => u.HeaderId == HeaderId).FirstOrDefault();
                        //if (objPTR002 != null)
                        //{
                        if (chkHTMechChemTestDetails(HeaderId))
                        {
                            if (CouCat.Equals(clsImplementationEnum.CouponCat.WaterAnls.GetStringValue())
                                    || CouCat.Equals(clsImplementationEnum.CouponCat.PerfQualf.GetStringValue()))
                            {
                                objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue();
                                //objPTR001.QA3WTC3ApprovalBy = objClsLoginInfo.UserName;
                                //objPTR001.QA3WTC3ApprovalOn = DateTime.Now;
                                objPTR001.SubmittedBy = objClsLoginInfo.UserName;
                                objPTR001.SubmittedOn = DateTime.Now;
                                // To resolve bug 26585 - Submit->Retrun->Submit
                                objPTR001.WE2ReturnBy = null;
                                objPTR001.WE2ReturnOn = null;
                                objPTR001.QC3ReturnBy = null;
                                objPTR001.QC3ReturnOn = null;
                                objPTR001.QA3WTC3ReturnBy = null;
                                objPTR001.QA3WTC3ReturnOn = null;
                                approver += db.ATH004.Where(x => x.Role.Equals(strQA3)).Select(x => x.Description).SingleOrDefault().ToString();
                                approver += " And " + db.ATH004.Where(x => x.Role.Equals(strWTC3)).Select(x => x.Description).SingleOrDefault().ToString();
                                //_objEmail.MailToAdd = getEmailIdByRole(getRoleId(strQA3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strWTC3), objPTR001);
                                if (CouCat.Equals(clsImplementationEnum.CouponCat.PerfQualf.GetStringValue()))
                                {
                                    //_objEmail.MailCc = getEmailIdByRole(getRoleId(strWE3), objPTR001);
                                    if (!string.IsNullOrEmpty(objPTR001.QC3ID))
                                    {
                                        _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.QC3ID);
                                    }
                                    if (!string.IsNullOrEmpty(objPTR001.WE2ID))
                                    {
                                        _objEmail.MailCc += ";" + Manager.GetMailIdFromPsNo(objPTR001.WE2ID);
                                    }
                                }
                                else
                                {
                                    //_objEmail.MailCc = getEmailIdByRole(getRoleId(strQC3), objPTR001);
                                    _objEmail.MailCc = Manager.GetMailIdFromPsNo(objPTR001.CreatedBy);
                                    if (!string.IsNullOrEmpty(objPTR001.QC3ID))
                                    {
                                        _objEmail.MailCc += ";" + Manager.GetMailIdFromPsNo(objPTR001.QC3ID);
                                    }
                                    _objEmail.MailToAdd = getEmailIdByRole(getRoleId(strQA3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strWTC3), objPTR001);
                                }
                            }
                            else if (CouCat.Equals(clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue()))
                            {
                                objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue();
                                //objPTR001.QC3ApprovalBy = objClsLoginInfo.UserName;
                                //objPTR001.QC3ApprovalOn = DateTime.Now;
                                objPTR001.SubmittedBy = objClsLoginInfo.UserName;
                                objPTR001.SubmittedOn = DateTime.Now;
                                // To resolve bug 26585 - Submit->Retrun->Submit
                                objPTR001.WE2ReturnBy = null;
                                objPTR001.WE2ReturnOn = null;
                                objPTR001.QC3ReturnBy = null;
                                objPTR001.QC3ReturnOn = null;
                                objPTR001.QA3WTC3ReturnBy = null;
                                objPTR001.QA3WTC3ReturnOn = null;
                                approver += db.ATH004.Where(x => x.Role.Equals(strQC3)).Select(x => x.Description).SingleOrDefault().ToString();
                                //_objEmail.MailToAdd = getEmailIdByRole(getRoleId(strQC3), objPTR001);
                                //_objEmail.MailCc = getEmailIdByRole(getRoleId(strPMG3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strMCC3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strSCC3), objPTR001);

                                if (!string.IsNullOrEmpty(objPTR001.QC3ID))
                                {
                                    _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.QC3ID);
                                }
                                if (!string.IsNullOrEmpty(objPTR001.WE2ID))
                                {
                                    _objEmail.MailCc += ";" + Manager.GetMailIdFromPsNo(objPTR001.WE2ID);
                                }
                                _objEmail.MailCc += ";" + Manager.GetMailIdFromPsNo(objPTR001.CreatedBy);
                            }
                            else
                            {
                                objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue();
                                //objPTR001.WE2ApprovalBy = objClsLoginInfo.UserName;
                                //objPTR001.WE2ApprovalOn = DateTime.Now;
                                objPTR001.SubmittedBy = objClsLoginInfo.UserName;
                                objPTR001.SubmittedOn = DateTime.Now;
                                // To resolve bug 26585 - Submit->Retrun->Submit
                                objPTR001.WE2ReturnBy = null;
                                objPTR001.WE2ReturnOn = null;
                                objPTR001.QC3ReturnBy = null;
                                objPTR001.QC3ReturnOn = null;
                                objPTR001.QA3WTC3ReturnBy = null;
                                objPTR001.QA3WTC3ReturnOn = null;
                                approver += db.ATH004.Where(x => x.Role.Equals(strWE2)).Select(x => x.Description).SingleOrDefault().ToString();
                                //_objEmail.MailToAdd = getEmailIdByRole(getRoleId(strWE2), objPTR001);
                                //_objEmail.MailCc = getEmailIdByRole(getRoleId(strWE3), objPTR001);
                                if (!string.IsNullOrEmpty(objPTR001.WE2ID))
                                {
                                    _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.WE2ID);
                                }
                                _objEmail.MailCc = Manager.GetMailIdFromPsNo(objPTR001.CreatedBy);
                            }
                            //objPTR001.Status = clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue();
                            //objPTR001.QA3WTC3ApprovalBy = objClsLoginInfo.UserName;
                            //objPTR001.QA3WTC3ApprovalOn = DateTime.Now;
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Please Maintain Mechanical/Chemical Test Details";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                    db.SaveChanges();
                    _ht["[Approver]"] = approver;
                    objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.PTR.SentforApproval).SingleOrDefault();
                    _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.CompilerAction.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "You Don't Have Permission to Submit PTR";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult SubmitSelectedPTR(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    var a = Json(SendtoCompiler(HeaderId));
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Documents Submitted Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please Try Again";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Return PTR
        [HttpPost]
        public ActionResult ReturnPTR(int HeaderId)
        {
            string strPMG3 = clsImplementationEnum.UserRoleName.PMG3.GetStringValue();
            string strMCC3 = clsImplementationEnum.UserRoleName.MCC3.GetStringValue();
            string strSCC3 = clsImplementationEnum.UserRoleName.SCC3.GetStringValue();
            string strWE3 = clsImplementationEnum.UserRoleName.WE3.GetStringValue();
            string strWE2 = clsImplementationEnum.UserRoleName.WE2.GetStringValue();
            string strQC3 = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
            string strQA3 = clsImplementationEnum.UserRoleName.QA3.GetStringValue();
            string strWTC3 = clsImplementationEnum.UserRoleName.WTC3.GetStringValue();
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string compileStatus = clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue();
            List<UserRoleDetails> role = GetRoleByLocation(objClsLoginInfo.UserName, objClsLoginInfo.Location);
            try
            {
                PTR001 objPTR001 = db.PTR001.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
                String status = objPTR001.Status;
                String CouCat = objPTR001.CouponCategory;
                if (chkAuth(CouCat, status))
                {
                    Hashtable _ht = new Hashtable();
                    EmailSend _objEmail = new EmailSend();
                    string link = WebsiteURL + "/PTR/MaintainPTR/PTRDetails?HId=" + objPTR001.HeaderId;
                    _ht["[Document]"] = objPTR001.PTRNo;
                    _ht["[Link]"] = link;
                    _ht["[Subject]"] = "PTR " + objPTR001.PTRNo + " Sent For Approval";
                    _ht["[Regards]"] = Manager.GetUserNameFromPsNo(objClsLoginInfo.UserName);
                    string initiator = "";
                    MAIL001 objTemplateMaster = db.MAIL001.Where(x => x.TamplateName == MailTemplates.PTR.Return).SingleOrDefault();
                    if (status.Equals(clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue()))
                    {
                        var objPTR006 = db.PTR006.Where(i => i.HeaderId == objPTR001.HeaderId).ToList();
                        foreach (var item in objPTR006)
                        {
                            if (item.MaterialReceiptDate.HasValue || item.MechTestCompletionDate.HasValue || item.ChecmicalTestCompletionTime.HasValue || !string.IsNullOrEmpty(item.Remarks))
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "You can not return PTR due to test completion details entered. Please clear test completion details for return PTR.";
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                        }
                        objPTR001.Status = clsImplementationEnum.PTRStatus.RetQA3.GetStringValue();
                        objPTR001.QA3WTC3ReturnBy = objClsLoginInfo.UserName;
                        objPTR001.QA3WTC3ReturnOn = DateTime.Now;
                        _ht["[Approver]"] = db.ATH004.Where(x => x.Role.Equals(strQA3)).Select(x => x.Description).SingleOrDefault().ToString() + "/" + db.ATH004.Where(x => x.Role.Equals(strWTC3)).Select(x => x.Description).SingleOrDefault().ToString();
                        //_objEmail.MailCc = getEmailIdByRole(getRoleId(strQA3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strWTC3), objPTR001);

                        //if (CouCat.Equals(clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue()))
                        //{
                        //    _objEmail.MailToAdd = getEmailIdByRole(getRoleId(strPMG3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strMCC3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strSCC3), objPTR001);
                        //    initiator += db.ATH004.Where(x => x.Role.Equals(strPMG3)).Select(x => x.Description).SingleOrDefault().ToString() + "," + db.ATH004.Where(x => x.Role.Equals(strMCC3)).Select(x => x.Description).SingleOrDefault().ToString() + " And " + db.ATH004.Where(x => x.Role.Equals(strSCC3)).Select(x => x.Description).SingleOrDefault().ToString();
                        //}
                        //else if (CouCat.Equals(clsImplementationEnum.CouponCat.WaterAnls.GetStringValue()))
                        //{
                        //    _objEmail.MailToAdd = getEmailIdByRole(getRoleId(strQC3), objPTR001);
                        //    initiator += db.ATH004.Where(x => x.Role.Equals(strQC3)).Select(x => x.Description).SingleOrDefault().ToString();
                        //}
                        //else
                        //{
                        //    _objEmail.MailToAdd = getEmailIdByRole(getRoleId(strWE3), objPTR001);
                        //    initiator += db.ATH004.Where(x => x.Role.Equals(strWE3)).Select(x => x.Description).SingleOrDefault().ToString();
                        //}

                        if (!string.IsNullOrEmpty(objPTR001.CreatedBy))
                        {
                            _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.CreatedBy);
                        }

                        if (!string.IsNullOrEmpty(objPTR001.WE2ID))
                        {
                            _objEmail.MailCc += Manager.GetMailIdFromPsNo(objPTR001.WE2ID) + ";";
                        }

                        if (!string.IsNullOrEmpty(objPTR001.QC3ID))
                        {
                            _objEmail.MailCc += Manager.GetMailIdFromPsNo(objPTR001.QC3ID) + ";";
                        }

                        if (!string.IsNullOrEmpty(objPTR001.LabId))
                        {
                            objPTR001.RevNo = objPTR001.RevNo + 1;
                            db.SP_PTR_CLOSE(HeaderId);
                        }
                    }

                    else if (status.Equals(clsImplementationEnum.PTRStatus.SubmitWE2.GetStringValue()))
                    {
                        objPTR001.Status = clsImplementationEnum.PTRStatus.RetWE2.GetStringValue();
                        objPTR001.WE2ReturnBy = objClsLoginInfo.UserName;
                        objPTR001.WE2ReturnOn = DateTime.Now;
                        _ht["[Approver]"] = db.ATH004.Where(x => x.Role.Equals(strWE2)).Select(x => x.Description).SingleOrDefault().ToString();
                        //initiator += db.ATH004.Where(x => x.Role.Equals(strWE3)).Select(x => x.Description).SingleOrDefault().ToString();
                        //_objEmail.MailToAdd = getEmailIdByRole(getRoleId(strWE3), objPTR001);
                        //_objEmail.MailCc = getEmailIdByRole(getRoleId(strWE2), objPTR001);
                        if (!string.IsNullOrEmpty(objPTR001.CreatedBy))
                        {
                            _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.CreatedBy);
                        }
                    }
                    else if (status.Equals(clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue()))
                    {
                        objPTR001.Status = clsImplementationEnum.PTRStatus.RetQC3.GetStringValue();
                        objPTR001.QC3ReturnBy = objClsLoginInfo.UserName;
                        objPTR001.QC3ReturnOn = DateTime.Now;
                        _ht["[Approver]"] = db.ATH004.Where(x => x.Role.Equals(strQC3)).Select(x => x.Description).SingleOrDefault().ToString();
                        //_objEmail.MailCc = getEmailIdByRole(getRoleId(strQC3), objPTR001);
                        //if (CouCat.Equals(clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue()))
                        //{
                        //    _objEmail.MailToAdd = getEmailIdByRole(getRoleId(strWE3), objPTR001);
                        //    initiator += db.ATH004.Where(x => x.Role.Equals(strWE3)).Select(x => x.Description).SingleOrDefault().ToString(); //+ "," + db.ATH004.Where(x => x.Role == "MCC3").Select(x => x.Description).SingleOrDefault().ToString() + " And " + db.ATH004.Where(x => x.Role == "SCC3").Select(x => x.Description).SingleOrDefault().ToString();
                        //}
                        //else if (CouCat.Equals(clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue()))
                        //{
                        //    _objEmail.MailToAdd = getEmailIdByRole(getRoleId(strPMG3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strMCC3), objPTR001) + ";" + getEmailIdByRole(getRoleId(strSCC3), objPTR001);
                        //    initiator += db.ATH004.Where(x => x.Role.Equals(strPMG3)).Select(x => x.Description).SingleOrDefault().ToString() + "," + db.ATH004.Where(x => x.Role.Equals(strMCC3)).Select(x => x.Description).SingleOrDefault().ToString() + " And " + db.ATH004.Where(x => x.Role.Equals(strSCC3)).Select(x => x.Description).SingleOrDefault().ToString();
                        //}
                        if (!string.IsNullOrEmpty(objPTR001.CreatedBy))
                        {
                            _objEmail.MailToAdd = Manager.GetMailIdFromPsNo(objPTR001.CreatedBy);
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Check the Status of the PTR";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    db.SaveChanges();
                    _ht["[Initiator]"] = initiator;
                    objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.PTR.Return).SingleOrDefault();
                    _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.CompilerAction.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "You Don't Have Permission to Return PTR";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Copy PTR
        public ActionResult GetCopyDetails(int HeaderId)
        {
            PTR001 objPTR001 = db.PTR001.Where(u => u.HeaderId == HeaderId).SingleOrDefault();
            return PartialView("_GetCopyDetails", objPTR001);
        }
        [HttpPost]
        public ActionResult copyPTR(string projCode, int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                List<UserRoleDetails> role = GetRoleByLocation(objClsLoginInfo.UserName, objClsLoginInfo.Location);
                //PTR001 objPTR001 = db.PTR001.Where(x => x.Project == projCode && x.Location == currentLoc).FirstOrDefault();
                PTR001 objExistingPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (CheckPTRCopyAllow(objExistingPTR001.CouponCategory, objExistingPTR001.Status, role))
                {
                    if (objExistingPTR001.Location != objClsLoginInfo.Location)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "You can not copy other location PTR!";
                    }
                    else
                    {
                        PTR001 objDesPTR001 = new PTR001();
                        objDesPTR001.QualityProject = projCode;
                        objDesPTR001.Project = db.QMS010.Where(i => i.QualityProject.Equals(projCode, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
                        objDesPTR001.BU = db.COM001.Where(i => i.t_cprj == objDesPTR001.Project).FirstOrDefault().t_entu; ;
                        objDesPTR001.Location = objClsLoginInfo.Location;
                        
                        ResponceMsgProjectBU obj = GetMaxPRTNo();
                        objDesPTR001.PTRNo = obj.PTRNo;
                        objDesPTR001.DocNo = obj.DocNo;

                        if (db.PTR001.Any(x => x.PTRNo.ToLower() == objDesPTR001.PTRNo.ToLower()))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = objDesPTR001.PTRNo + " this PTR No already exists, Please refresh the page.";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }

                        objResponseMsg = InsertNewPTR(objExistingPTR001, objDesPTR001, false);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "You are not authorize to copy this PTR!";
                }

                //}
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public clsHelper.ResponseMsgWithStatus InsertNewPTR(PTR001 objSrcPTR001, PTR001 objDesPTR001, bool IsEdited)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                objDesPTR001.CouponCategory = objSrcPTR001.CouponCategory;
                objDesPTR001.CouponNo = objSrcPTR001.CouponNo;
                objDesPTR001.SeamNo = objSrcPTR001.SeamNo;
                objDesPTR001.MaterialSpec = objSrcPTR001.MaterialSpec;
                objDesPTR001.Size = objSrcPTR001.Size;
                objDesPTR001.LTFPSNo = objSrcPTR001.LTFPSNo;
                objDesPTR001.WPSNo = objSrcPTR001.WPSNo;
                objDesPTR001.WeldingProcess = objSrcPTR001.WeldingProcess;
                objDesPTR001.FillerWire = objSrcPTR001.FillerWire;
                objDesPTR001.Position = objSrcPTR001.Position;
                objDesPTR001.BrandName = objSrcPTR001.BrandName;
                objDesPTR001.BatchNo = objSrcPTR001.BatchNo;
                objDesPTR001.WeldThickness = objSrcPTR001.WeldThickness;
                objDesPTR001.ApplicableCodeSpec = objSrcPTR001.ApplicableCodeSpec;
                objDesPTR001.WQTno = objSrcPTR001.WQTno;
                objDesPTR001.PQRnO = objSrcPTR001.PQRnO;
                objDesPTR001.WelderID = objSrcPTR001.WelderID;
                objDesPTR001.InspectionAgency1 = objSrcPTR001.InspectionAgency1;
                objDesPTR001.InspectionAgency2 = objSrcPTR001.InspectionAgency2;
                objDesPTR001.InspectionAgency3 = objSrcPTR001.InspectionAgency3;
                objDesPTR001.InspectionStage = objSrcPTR001.InspectionStage;
                objDesPTR001.InspectionStage2 = objSrcPTR001.InspectionStage2;
                objDesPTR001.InspectionStage3 = objSrcPTR001.InspectionStage3;
                objDesPTR001.NoofCoupons = objSrcPTR001.NoofCoupons;
                objDesPTR001.MechTest = objSrcPTR001.MechTest;
                objDesPTR001.ChemicalTest = objSrcPTR001.ChemicalTest;
                objDesPTR001.HTDetail = objSrcPTR001.HTDetail;
                objDesPTR001.StampedBy = objSrcPTR001.StampedBy;
                objDesPTR001.CGDP = objSrcPTR001.CGDP;
                objDesPTR001.WE2ID = objSrcPTR001.WE2ID;
                objDesPTR001.QC3ID = objSrcPTR001.QC3ID;
                objDesPTR001.Status = clsImplementationEnum.PTRStatus.Draft.GetStringValue();
                objDesPTR001.RevNo = 0;
                objDesPTR001.CreatedBy = objClsLoginInfo.UserName;
                objDesPTR001.CreatedOn = DateTime.Now;
                db.PTR001.Add(objDesPTR001);

                db.SaveChanges();
                objResponseMsg.HeaderId = objDesPTR001.HeaderId;


                List<PTR002> lstSrcPTR002 = db.PTR002.Where(x => x.HeaderId == objSrcPTR001.HeaderId).ToList();
                foreach (var srcPTR002 in lstSrcPTR002)
                {
                    PTR002 objPTR002 = new PTR002();
                    //objPTR002 = srcPTR002;
                    objPTR002.PTRNo = objDesPTR001.PTRNo;
                    objPTR002.CouponNo = srcPTR002.CouponNo;
                    objPTR002.CouponID = srcPTR002.CouponID;
                    objPTR002.HTType1 = srcPTR002.HTType1;
                    objPTR002.HTTemp1 = srcPTR002.HTTemp1;
                    objPTR002.HTTime1 = srcPTR002.HTTime1;
                    objPTR002.HTType2 = srcPTR002.HTType2;
                    objPTR002.HTTemp2 = srcPTR002.HTTemp2;
                    objPTR002.HTTime2 = srcPTR002.HTTime2;
                    objPTR002.HTType3 = srcPTR002.HTType3;
                    objPTR002.HTTemp3 = srcPTR002.HTTemp3;
                    objPTR002.HTTime3 = srcPTR002.HTTime3;
                    objPTR002.HTType4 = srcPTR002.HTType4;
                    objPTR002.HTTemp4 = srcPTR002.HTTemp4;
                    objPTR002.HTTime4 = srcPTR002.HTTime4;
                    objPTR002.HTType5 = srcPTR002.HTType5;
                    objPTR002.HTTemp5 = srcPTR002.HTTemp5;
                    objPTR002.HTTime5 = srcPTR002.HTTime5;
                    objPTR002.HTType6 = srcPTR002.HTType6;
                    objPTR002.HTTemp6 = srcPTR002.HTTemp6;
                    objPTR002.HTTime6 = srcPTR002.HTTime6;
                    objPTR002.Project = objDesPTR001.Project;
                    objPTR002.Location = objDesPTR001.Location;
                    objPTR002.BU = objDesPTR001.BU;
                    objPTR002.HeaderId = objDesPTR001.HeaderId;
                    objPTR002.RevNo = 0;
                    objPTR002.CreatedBy = objClsLoginInfo.UserName;
                    objPTR002.CreatedOn = DateTime.Now;
                    objPTR002.EditedBy = "";
                    objPTR002.EditedOn = null;
                    db.PTR002.Add(objPTR002);
                    db.SaveChanges();
                }
                List<PTR003> lstSrcPTR003 = db.PTR003.Where(x => x.HeaderId == objSrcPTR001.HeaderId).ToList();
                foreach (var srcPTR003 in lstSrcPTR003)
                {
                    PTR003 objPTR003 = new PTR003();
                    //objPTR003 = srcPTR003;
                    objPTR003.PTRNo = objDesPTR001.PTRNo;
                    objPTR003.CouponID = srcPTR003.CouponID;
                    objPTR003.TestTypeOrientation = srcPTR003.TestTypeOrientation;
                    objPTR003.SimpleLocation = srcPTR003.SimpleLocation;
                    objPTR003.TestTemperature = srcPTR003.TestTemperature;
                    objPTR003.NoofSamples = srcPTR003.NoofSamples;
                    objPTR003.AcceptanceCriteria = srcPTR003.AcceptanceCriteria;
                    objPTR003.TestingSpecification = srcPTR003.TestingSpecification;
                    objPTR003.Remarks = srcPTR003.Remarks;
                    objPTR003.Project = objDesPTR001.Project;
                    objPTR003.Location = objDesPTR001.Location;
                    objPTR003.BU = objDesPTR001.BU;
                    objPTR003.HeaderId = objDesPTR001.HeaderId;
                    objPTR003.RevNo = 0;
                    objPTR003.CreatedBy = objClsLoginInfo.UserName;
                    objPTR003.CreatedOn = DateTime.Now;
                    objPTR003.EditedBy = "";
                    objPTR003.EditedOn = null;
                    db.PTR003.Add(objPTR003);
                    db.SaveChanges();
                }
                List<PTR004> lstSrcPTR004 = db.PTR004.Where(x => x.HeaderId == objSrcPTR001.HeaderId).ToList();
                foreach (var srcPTR004 in lstSrcPTR004)
                {
                    PTR004 objPTR004 = new PTR004();
                    //objPTR004 = srcPTR004;
                    objPTR004.PTRNo = objDesPTR001.PTRNo;
                    objPTR004.CouponID = srcPTR004.CouponID;
                    objPTR004.SampleLocation = srcPTR004.SampleLocation;
                    objPTR004.Elements = srcPTR004.Elements;
                    objPTR004.AcceptanceCriteria = srcPTR004.AcceptanceCriteria;
                    objPTR004.TestingSpecification = srcPTR004.TestingSpecification;
                    objPTR004.Remarks = srcPTR004.Remarks;
                    objPTR004.Project = objDesPTR001.Project;
                    objPTR004.Location = objDesPTR001.Location;
                    objPTR004.BU = objDesPTR001.BU;
                    objPTR004.HeaderId = objDesPTR001.HeaderId;
                    objPTR004.RevNo = 0;
                    objPTR004.CreatedBy = objClsLoginInfo.UserName;
                    objPTR004.CreatedOn = DateTime.Now;
                    objPTR004.EditedBy = "";
                    objPTR004.EditedOn = null;
                    db.PTR004.Add(objPTR004);
                    db.SaveChanges();
                }
                List<PTR005> lstSrcPTR005 = db.PTR005.Where(x => x.HeaderId == objSrcPTR001.HeaderId).ToList();
                foreach (var srcPTR005 in lstSrcPTR005)
                {
                    PTR005 objPTR005 = new PTR005();
                    //objPTR005 = srcPTR005;
                    objPTR005.PTRNo = objDesPTR001.PTRNo;
                    objPTR005.PartNo = srcPTR005.PartNo;
                    objPTR005.HeatNo1 = srcPTR005.HeatNo1;
                    objPTR005.PlateNo1 = srcPTR005.PlateNo1;
                    objPTR005.HeatNo2 = srcPTR005.HeatNo2;
                    objPTR005.PlateNo2 = srcPTR005.PlateNo2;
                    objPTR005.Project = objDesPTR001.Project;
                    objPTR005.Location = objDesPTR001.Location;
                    objPTR005.BU = objDesPTR001.BU;
                    objPTR005.HeaderId = objDesPTR001.HeaderId;
                    objPTR005.RevNo = 0;
                    objPTR005.CreatedBy = objClsLoginInfo.UserName;
                    objPTR005.CreatedOn = DateTime.Now;
                    objPTR005.EditedBy = "";
                    objPTR005.EditedOn = null;
                    db.PTR005.Add(objPTR005);
                    db.SaveChanges();
                }
                List<PTR006> lstSrcPTR006 = db.PTR006.Where(x => x.HeaderId == objSrcPTR001.HeaderId).ToList();
                foreach (var srcPTR006 in lstSrcPTR006)
                {
                    PTR006 objPTR006 = new PTR006();
                    //objPTR006 = srcPTR006;
                    objPTR006.PTRNo = objDesPTR001.PTRNo;
                    objPTR006.CouponNo = srcPTR006.CouponNo;
                    objPTR006.CouponID = srcPTR006.CouponID;
                    objPTR006.MaterialReceiptDate = srcPTR006.MaterialReceiptDate;
                    objPTR006.MechTestCompletionDate = srcPTR006.MechTestCompletionDate;
                    objPTR006.ChecmicalTestCompletionTime = srcPTR006.ChecmicalTestCompletionTime;
                    objPTR006.Remarks = srcPTR006.Remarks;
                    objPTR006.Project = objDesPTR001.Project;
                    objPTR006.Location = objDesPTR001.Location;
                    objPTR006.BU = objDesPTR001.BU;
                    objPTR006.HeaderId = objDesPTR001.HeaderId;
                    objPTR006.RevNo = 0;
                    objPTR006.CreatedBy = objClsLoginInfo.UserName;
                    objPTR006.CreatedOn = DateTime.Now;
                    objPTR006.EditedBy = "";
                    objPTR006.EditedOn = null;
                    db.PTR006.Add(objPTR006);
                    db.SaveChanges();
                }

                //db.SaveChanges();
                objResponseMsg.HeaderStatus = objDesPTR001.PTRNo;
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Document added successfully To Document No : " + objResponseMsg.HeaderStatus + " of " + objDesPTR001.QualityProject + " Project.";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
                objResponseMsg.HeaderId = 0;
            }
            return objResponseMsg;
        }

        public ResponceMsgProjectBU GetMaxPRTNo()
        {
            ResponceMsgProjectBU obj = new ResponceMsgProjectBU();
            int? DocNo = (from a in db.PTR001
                          where a.Location.ToUpper() == objClsLoginInfo.Location.ToUpper()
                          select a).Max(a => a.DocNo);
            var maxDocNo = Convert.ToString(DocNo + 1);
            obj.PTRNo = objClsLoginInfo.Location.Substring(0, 1).ToUpper() + maxDocNo.PadLeft(PTRNoUptoDigit, '0');
            obj.DocNo = Convert.ToInt32(maxDocNo);
            return obj;
        }


                #endregion

        #region TimeLine
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            PTR001 model = new PTR001();

            PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

            //model.ApprovedBy = Manager.GetUserNameFromPsNo(objPTR001.ApprovedBy);
            //model.ApprovedOn = objPTR001.ApprovedOn;
            //model.SubmittedBy = Manager.GetUserNameFromPsNo(objPTR001.SubmittedBy);
            //model.SubmittedOn = objPTR001.SubmittedOn;
            //model.CreatedBy = Manager.GetUserNameFromPsNo(objPTR001.CreatedBy);
            //model.CreatedOn = objPTR001.CreatedOn;
            //model.ReturnedBy = Manager.GetUserNameFromPsNo(objPTR001.ReturnedBy);
            //model.ReturnedOn = objPTR001.ReturnedOn;
            model = objPTR001;
            model.CreatedBy = Manager.GetUserNameFromPsNo(model.CreatedBy);
            model.RetractedBy = Manager.GetUserNameFromPsNo(model.RetractedBy);
            model.SubmittedBy = Manager.GetUserNameFromPsNo(model.SubmittedBy);
            model.WE2ApprovalBy = Manager.GetUserNameFromPsNo(model.WE2ApprovalBy);
            model.QC3ApprovalBy = Manager.GetUserNameFromPsNo(model.QC3ApprovalBy);
            model.QA3WTC3ApprovalBy = Manager.GetUserNameFromPsNo(model.QA3WTC3ApprovalBy);
            model.WE2ReturnBy = Manager.GetUserNameFromPsNo(model.WE2ReturnBy);
            model.QC3ReturnBy = Manager.GetUserNameFromPsNo(model.QC3ReturnBy);
            model.QA3WTC3ReturnBy = Manager.GetUserNameFromPsNo(model.QA3WTC3ReturnBy);
            return PartialView("_TimelineProgress", model);
        }
        public ActionResult ShowLogTimeline(int HeaderId, int LineId = 0)
        {
            PTR001_Log model = new PTR001_Log();

            PTR001_Log objPTR001Log = db.PTR001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();

            //model.ApprovedBy = Manager.GetUserNameFromPsNo(objPTR001.ApprovedBy);
            //model.ApprovedOn = objPTR001.ApprovedOn;
            //model.SubmittedBy = Manager.GetUserNameFromPsNo(objPTR001.SubmittedBy);
            //model.SubmittedOn = objPTR001.SubmittedOn;
            //model.CreatedBy = Manager.GetUserNameFromPsNo(objPTR001.CreatedBy);
            //model.CreatedOn = objPTR001.CreatedOn;
            //model.ReturnedBy = Manager.GetUserNameFromPsNo(objPTR001.ReturnedBy);
            //model.ReturnedOn = objPTR001.ReturnedOn;

            model = objPTR001Log;
            model.CreatedBy = Manager.GetUserNameFromPsNo(model.CreatedBy);
            model.RetractedBy = Manager.GetUserNameFromPsNo(model.RetractedBy);
            model.SubmittedBy = Manager.GetUserNameFromPsNo(model.SubmittedBy);
            model.WE2ApprovalBy = Manager.GetUserNameFromPsNo(model.WE2ApprovalBy);
            model.QC3ApprovalBy = Manager.GetUserNameFromPsNo(model.QC3ApprovalBy);
            model.QA3WTC3ApprovalBy = Manager.GetUserNameFromPsNo(model.QA3WTC3ApprovalBy);
            model.WE2ReturnBy = Manager.GetUserNameFromPsNo(model.WE2ReturnBy);
            model.QC3ReturnBy = Manager.GetUserNameFromPsNo(model.QC3ReturnBy);
            model.QA3WTC3ReturnBy = Manager.GetUserNameFromPsNo(model.QA3WTC3ReturnBy);
            return PartialView("_HistoryTimelineProgress", model);
        }
        #endregion

        #region History
        [HttpPost]
        public ActionResult GetPTRHistoryDetails(int? HeaderId)
        {
            PTR001_Log objPTR001_Log = new PTR001_Log();
            if (HeaderId > 0)
                objPTR001_Log = db.PTR001_Log.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            return PartialView("_GetPTRHistoryDetails", objPTR001_Log);
        }
        public JsonResult LoadPTRHistoryHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                var location = (from a in db.COM003
                                join b in db.COM002 on a.t_loca equals b.t_dimx
                                where b.t_dtyp == 1
                                && a.t_psno.Equals(user, StringComparison.OrdinalIgnoreCase)
                                select b.t_dimx).FirstOrDefault();
                string strWhere = string.Empty;

                //System.Collections.Generic.IEnumerable<IEMQSImplementation.PTR001> lst;
                if (Convert.ToInt32(param.CTQHeaderId) > 0)
                    strWhere += "HeaderId=" + param.CTQHeaderId + "";
                else
                    strWhere += "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (ptr1.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or PTRNo like '%" + param.sSearch + "%' or RevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or QualityProject like '%" + param.sSearch + "%' or CouponCategory like '%" + param.sSearch + "%' or CouponNo like '%" + param.sSearch + "%')";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PTR_GETHEADER_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                            Convert.ToString(uc.QualityProject),
                            Convert.ToString(uc.PTRNo),
                            "R"+Convert.ToString(uc.RevNo),
                            Convert.ToString(uc.Status),
                            Convert.ToString(uc.Id)
                         }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult PTRLogDetails(int? HId)
        {
            PTR001_Log objLogPTR001 = new PTR001_Log();

            objLogPTR001 = db.PTR001_Log.Where(x => x.Id == HId).FirstOrDefault();
            if (objLogPTR001 != null)
            {
                string strBU = db.COM001.Where(i => i.t_cprj == objLogPTR001.Project).FirstOrDefault().t_entu;
                objLogPTR001.InspectionAgency1 = getCategoryDesc("Inspection Agency", objLogPTR001.BU, objLogPTR001.Location, objLogPTR001.InspectionAgency1);
                objLogPTR001.InspectionAgency2 = getCategoryDesc("Inspection Agency", objLogPTR001.BU, objLogPTR001.Location, objLogPTR001.InspectionAgency2);
                objLogPTR001.InspectionAgency3 = getCategoryDesc("Inspection Agency", objLogPTR001.BU, objLogPTR001.Location, objLogPTR001.InspectionAgency3);
                objLogPTR001.InspectionStage = getCategoryDesc("TPI Intervention", objLogPTR001.BU, objLogPTR001.Location, objLogPTR001.InspectionStage);
                objLogPTR001.InspectionStage2 = getCategoryDesc("TPI Intervention", objLogPTR001.BU, objLogPTR001.Location, objLogPTR001.InspectionStage2);
                objLogPTR001.InspectionStage3 = getCategoryDesc("TPI Intervention", objLogPTR001.BU, objLogPTR001.Location, objLogPTR001.InspectionStage3);
                //objLogPTR001.InspectionAgency1 = db.GLB002.Where(a => a.BU == objLogPTR001.BU && a.Location == objLogPTR001.Location && a.Code == objLogPTR001.InspectionAgency1).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                //objLogPTR001.InspectionAgency2 = db.GLB002.Where(a => a.BU == objLogPTR001.BU && a.Location == objLogPTR001.Location && a.Code == objLogPTR001.InspectionAgency2).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                //objLogPTR001.InspectionAgency3 = db.GLB002.Where(a => a.BU == objLogPTR001.BU && a.Location == objLogPTR001.Location && a.Code == objLogPTR001.InspectionAgency3).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                //objLogPTR001.InspectionStage = db.QMS002.Where(a => a.BU == objLogPTR001.BU && a.Location == objLogPTR001.Location && a.StageCode == objLogPTR001.InspectionStage).Select(a => a.StageCode + " - " + a.StageDesc).FirstOrDefault();
                objLogPTR001.WE2ID = db.COM003.Where(a => a.t_psno == objLogPTR001.WE2ID).Select(a => a.t_psno + " - " + a.t_name).FirstOrDefault();
                objLogPTR001.QC3ID = db.COM003.Where(a => a.t_psno == objLogPTR001.QC3ID).Select(a => a.t_psno + " - " + a.t_name).FirstOrDefault();
                objLogPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objLogPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objLogPTR001.Project = db.COM001.Where(i => i.t_cprj == objLogPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objLogPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objLogPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            return View(objLogPTR001);
        }
        public ActionResult MechLogDetails(int HeaderId)
        {
            ViewBag.Id = HeaderId;
            PTR001_Log objLogPTR001 = new PTR001_Log();
            objLogPTR001 = db.PTR001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
            objLogPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objLogPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objLogPTR001.Project = db.COM001.Where(i => i.t_cprj == objLogPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objLogPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objLogPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            return View(objLogPTR001);
        }
        public ActionResult HTLogDetails(int HeaderId)
        {
            ViewBag.Id = HeaderId;
            PTR001_Log objLogPTR001 = new PTR001_Log();
            objLogPTR001 = db.PTR001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
            objLogPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objLogPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objLogPTR001.Project = db.COM001.Where(i => i.t_cprj == objLogPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objLogPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objLogPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            return View(objLogPTR001);
        }
        public ActionResult ChemLogDetails(int HeaderId)
        {
            ViewBag.Id = HeaderId;
            PTR001_Log objLogPTR001 = new PTR001_Log();
            objLogPTR001 = db.PTR001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
            objLogPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objLogPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objLogPTR001.Project = db.COM001.Where(i => i.t_cprj == objLogPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objLogPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objLogPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            return View(objLogPTR001);
        }
        public ActionResult HeatPlateLogDetails(int HeaderId)
        {
            ViewBag.Id = HeaderId;
            PTR001_Log objLogPTR001 = new PTR001_Log();
            objLogPTR001 = db.PTR001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
            objLogPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objLogPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objLogPTR001.Project = db.COM001.Where(i => i.t_cprj == objLogPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objLogPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objLogPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            return View(objLogPTR001);
        }
        public ActionResult TestCompletionLogDetails(int HeaderId)
        {
            ViewBag.Id = HeaderId;
            PTR001_Log objLogPTR001 = new PTR001_Log();
            objLogPTR001 = db.PTR001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
            objLogPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objLogPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objLogPTR001.Project = db.COM001.Where(i => i.t_cprj == objLogPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objLogPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objLogPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            return View(objLogPTR001);
        }

        public ActionResult LoadMechLogDetailData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var RefId = Convert.ToInt32(param.Headerid);
                string condition = string.Empty;
                condition += "where RefId=" + RefId + " ";


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( LineId like '%" + param.sSearch + "%'  or CouponID like '%" + param.sSearch + "%'  or TestTypeOrientation like '%" + param.sSearch + "%' or SimpleLocation like '%" + param.sSearch + "%' or TestTemperature like '%" + param.sSearch + "%' or NoofSamples like '%" + param.sSearch + "%' or AcceptanceCriteria like '%" + param.sSearch + "%'or TestingSpecification like '%" + param.sSearch + "%'or Remarks like '%" + param.sSearch + "%')";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                PTR001_Log objPTR001_Log = new PTR001_Log();
                objPTR001_Log = db.PTR001_Log.Where(x => x.Id == RefId).FirstOrDefault();

                var lstResult = db.SP_PTR_GET_MECH_LINE_DETAILS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, condition
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                             Convert.ToString(uc.ROW_NO),
                             Convert.ToString(uc.CouponID),
                             getCategoryDesc2("Test Type Orientation",objPTR001_Log.BU,objPTR001_Log.Location,uc.TestTypeOrientation),
                             //Convert.ToString(uc.TestTypeOrientation),
                             Convert.ToString(uc.Orientation),
                             Convert.ToString(uc.SimpleLocation),
                             Convert.ToString(uc.TestTemperature),
                             Convert.ToString(uc.NoofSamples),
                             Convert.ToString(uc.AcceptanceCriteria),
                             Convert.ToString(uc.TestingSpecification),
                             Convert.ToString(uc.Remarks),
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.RefId)
                         }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult LoadHTLogDetailData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                var RefId = Convert.ToInt32(param.Headerid);
                string condition = string.Empty;
                condition += "where RefId=" + RefId + " ";


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( CouponNo like '%" + param.sSearch + "%'  or CouponID like '%" + param.sSearch + "%'  or HTType1 like '%" + param.sSearch + "%' or HTTemp1 like '%" + param.sSearch + "%'  or HTTime1 like '%" + param.sSearch + "%'  or HTType2 like '%" + param.sSearch + "%' or HTTemp2 like '%" + param.sSearch + "%' or HTTime2 like '%" + param.sSearch + "%' or HTType3 like '%" + param.sSearch + "%' or HTTemp3 like '%" + param.sSearch + "%' or HTTime3 like '%" + param.sSearch + "%' or HTType4 like '%" + param.sSearch + "%' or HTTemp4 like '%" + param.sSearch + "%' or HTTime4 like '%" + param.sSearch + "%' or HTType5 like '%" + param.sSearch + "%' or HTTemp5 like '%" + param.sSearch + "%' or HTTime5 like '%" + param.sSearch + "%' or HTType6 like '%" + param.sSearch + "%' or HTTemp6 like '%" + param.sSearch + "%' or HTTime6 like '%" + param.sSearch + "%')";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PTR_GET_HT_LINE_DETAILS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, condition
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                             Convert.ToString(uc.CouponNo),
                             Convert.ToString(uc.CouponID),
                             Convert.ToString(uc.HTType1),
                             Convert.ToString(uc.HTTemp1),
                            Convert.ToString(uc.HTTime1),
                            Convert.ToString(uc.HTType2),
                             Convert.ToString(uc.HTTemp2),
                            Convert.ToString(uc.HTTime2),
                            Convert.ToString(uc.HTType3),
                             Convert.ToString(uc.HTTemp3),
                            Convert.ToString(uc.HTTime3),
                            Convert.ToString(uc.HTType4),
                             Convert.ToString(uc.HTTemp4),
                            Convert.ToString(uc.HTTime4),
                            Convert.ToString(uc.HTType5),
                             Convert.ToString(uc.HTTemp5),
                            Convert.ToString(uc.HTTime5),
                            Convert.ToString(uc.HTType6),
                             Convert.ToString(uc.HTTemp6),
                            Convert.ToString(uc.HTTime6),
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.RefId)
                         }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult LoadChemLogDetailData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                var RefId = Convert.ToInt32(param.Headerid);
                string condition = string.Empty;
                condition += "where RefId=" + RefId + " ";


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( LineId like '%" + param.sSearch + "%'  or CouponID like '%" + param.sSearch + "%'  or SampleLocation like '%" + param.sSearch + "%' or Elements like '%" + param.sSearch + "%' or AcceptanceCriteria like '%" + param.sSearch + "%'  or TestingSpecification like '%" + param.sSearch + "%' or Remarks like '%" + param.sSearch + "%' )";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PTR_GET_CHEM_LINE_DETAILS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, condition
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                             Convert.ToString(uc.ROW_NO),
                             Convert.ToString(uc.CouponID),
                             Convert.ToString(uc.SampleLocation),
                             Convert.ToString(uc.Elements),
                            Convert.ToString(uc.AcceptanceCriteria),
                            Convert.ToString(uc.TestingSpecification),
                            Convert.ToString(uc.Remarks),
                            Convert.ToString(uc.Id),
                           Convert.ToString(uc.RefId)
                         }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult LoadHeatPlateLogDetailData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                var RefId = Convert.ToInt32(param.Headerid);
                string condition = string.Empty;
                condition += "where RefId=" + RefId + " ";


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( LineId like '%" + param.sSearch + "%'  or PartNo like '%" + param.sSearch + "%'  or HeatNo1 like '%" + param.sSearch + "%' or HeatNo2 like '%" + param.sSearch + "%' or PlateNo1 like '%" + param.sSearch + "%' or PlateNo2 like '%" + param.sSearch + "%'  )";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PTR_GET_HEAT_PLATE_LINE_DETAILS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, condition
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {Convert.ToString(uc.ROW_NO),
                             Convert.ToString(uc.PartNo),
                             Convert.ToString(uc.HeatNo1),
                             Convert.ToString(uc.PlateNo1),
                             Convert.ToString(uc.HeatNo2),
                            Convert.ToString(uc.PlateNo2),
                           Convert.ToString(uc.Id),
                           Convert.ToString(uc.RefId)
                         }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult LoadTestCompletionLogDetailData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                var RefId = Convert.ToInt32(param.Headerid);
                string condition = string.Empty;
                condition += "where RefId=" + RefId + " ";


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    condition += " and ( LineId like '%" + param.sSearch + "%'  or CouponNo like '%" + param.sSearch + "%'  or CouponId like '%" + param.sSearch + "%' or Remarks like '%" + param.sSearch + "%'   )";
                }
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_PTR_GET_TEST_COMPLETION_DETAILS_HISTORY
                                (
                                StartIndex, EndIndex, strSortOrder, condition
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {Convert.ToString(uc.CouponNo),
                             Convert.ToString(uc.CouponID),
                            // "<input type='date' name='MaterialReceiptDate' class='form-control' value="+dateV(uc.MaterialReceiptDate)+" >",
                            // "<input type='date' name='MechTestCompletionDate' class='form-control' value="+dateV(uc.MechTestCompletionDate)+ " >",
                            // "<input type='date' name='ChecmicalTestCompletionTime' class='form-control' value="+dateV(uc.ChecmicalTestCompletionTime)+" >",
                            //"<input type='text' name='Remarks' class='form-control input-sm input-small input-inline' value='" + uc.Remarks + "' />",
                            Convert.ToString(dateF(uc.MaterialReceiptDate)),
                            Convert.ToString(dateF(uc.MechTestCompletionDate)),
                            Convert.ToString(dateF(uc.ChecmicalTestCompletionTime)),
                            Convert.ToString(uc.Remarks),
                            Convert.ToString(uc.Id)
                         }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = condition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult MechMetLogDetails(int? Id, int? HId)
        {
            PTR003_Log objPTR003 = new PTR003_Log();
            ViewBag.HeaderId = HId;
            if (Id > 0)
            {
                objPTR003 = db.PTR003_Log.Where(x => x.Id == Id).FirstOrDefault();
                objPTR003.TestTypeOrientation = db.GLB002.Where(a => a.BU == objPTR003.BU && a.Location == objPTR003.Location && a.Code == objPTR003.TestTypeOrientation).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                //objPTR003.SimpleLocation = db.GLB002.Where(a => a.BU == objPTR003.BU && a.Location == objPTR003.Location && a.Code == objPTR003.SimpleLocation).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                //objPTR003.TestTemperature = db.GLB002.Where(a => a.BU == objPTR003.BU && a.Location == objPTR003.Location && a.Code == objPTR003.TestTemperature).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                objPTR003.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR003.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR003.Project = db.COM001.Where(i => i.t_cprj == objPTR003.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR003.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR003.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

            }
            return PartialView("_MechMetLogDetails", objPTR003);
        }
        public ActionResult HTLineLogDetails(int? Id, int? HId)
        {
            PTR002_Log objPTR002 = new PTR002_Log();
            ViewBag.HeaderId = HId;
            if (Id > 0)
            {
                objPTR002 = db.PTR002_Log.Where(x => x.Id == Id).FirstOrDefault();
                objPTR002.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR002.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR002.Project = db.COM001.Where(i => i.t_cprj == objPTR002.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR002.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR002.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

            }
            return PartialView("_HTLineLogDetails", objPTR002);
        }
        public ActionResult ChemLineLogDetails(int? Id, int? HId)
        {
            PTR004_Log objPTR004 = new PTR004_Log();
            ViewBag.HeaderId = HId;
            if (Id > 0)
            {
                objPTR004 = db.PTR004_Log.Where(x => x.Id == Id).FirstOrDefault();
                //objPTR004.SampleLocation = db.GLB002.Where(a => a.BU == objPTR004.BU && a.Location == objPTR004.Location && a.Code == objPTR004.SampleLocation).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                objPTR004.Elements = db.GLB002.Where(a => a.BU == objPTR004.BU && a.Location == objPTR004.Location && a.Code == objPTR004.Elements).Select(a => a.Code + " - " + a.Description).FirstOrDefault();
                objPTR004.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR004.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR004.Project = db.COM001.Where(i => i.t_cprj == objPTR004.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR004.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR004.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            return PartialView("_ChemLineLogDetails", objPTR004);


        }
        public ActionResult HeatPlateLineLogDetails(int? Id, int? HId)
        {
            PTR005_Log objPTR005 = new PTR005_Log();
            ViewBag.HeaderId = HId;
            if (Id > 0)
            {
                objPTR005 = db.PTR005_Log.Where(x => x.Id == Id).FirstOrDefault();
                objPTR005.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR005.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objPTR005.Project = db.COM001.Where(i => i.t_cprj == objPTR005.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objPTR005.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR005.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            }
            return PartialView("_HeatPlateLineLogDetails", objPTR005);

        }
        #endregion

        #region common functions
        [NonAction]
        public bool isLinesEditable(int HeeaderId)
        {
            List<string> status = new List<string> { clsImplementationEnum.PTRStatus.Draft.GetStringValue(), clsImplementationEnum.PTRStatus.RetWE2.GetStringValue() ,
                clsImplementationEnum.PTRStatus.RetQC3.GetStringValue() , clsImplementationEnum.PTRStatus.RetQA3.GetStringValue(),clsImplementationEnum.PTRStatus.Closed.GetStringValue() };
            PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeeaderId).FirstOrDefault();
            if (objPTR001 != null)
            {
                if (status.Contains(objPTR001.Status))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        [NonAction]
        public bool isHeatPlateLinesEditable(int HeeaderId)
        {
            List<string> status = new List<string> { clsImplementationEnum.PTRStatus.Draft.GetStringValue(), clsImplementationEnum.PTRStatus.RetWE2.GetStringValue() ,
                clsImplementationEnum.PTRStatus.RetQC3.GetStringValue() , clsImplementationEnum.PTRStatus.RetQA3.GetStringValue(),clsImplementationEnum.PTRStatus.Closed.GetStringValue() };
            PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == HeeaderId).FirstOrDefault();
            if (objPTR001 != null)
            {
                if ((objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue()))
                    || ((objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Draft.GetStringValue()) || objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.RetQA3.GetStringValue())) && objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.WaterAnls.GetStringValue()))
                    || ((objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Draft.GetStringValue()) || objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.RetQA3.GetStringValue()) || objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.RetWE2.GetStringValue()) || objPTR001.Status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()))
                        && (objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.ProcQualf.GetStringValue()) || objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.ConsQualf.GetStringValue()) || objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.MockUp.GetStringValue()) || objPTR001.CouponCategory.Equals(clsImplementationEnum.CouponCat.PerfQualf.GetStringValue()))))
                    return true;
                else
                    return false;
            }
            else
                return false;
        }
        public string dateV(DateTime? str)
        {
            if (str == null)
                return "";
            else
                return DateTime.Parse(str.ToString()).ToString("yyyy-MM-dd");
        }
        public string dateF(DateTime? str)
        {
            if (str == null)
                return "";
            else
                return DateTime.Parse(str.ToString()).ToString("dd-MM-yyyy");
        }
        public string getHistoryBtn(int HeaderId, int? RevNo, string status)
        {
            var btn = "";
            if (status == clsImplementationEnum.PTRStatus.Closed.GetStringValue() || RevNo > 0)
                btn = "<i data-modal = '' id = 'History' name = 'History' style = 'cursor:pointer;margin-left:5px;' Title = 'History' class='fa fa-history' onClick=GetPTRHistory(" + HeaderId + ")></i>";
            else
                btn = btn = "<i data-modal = '' id = 'History' name = 'History' style = 'cursor:pointer;margin-left:5px;opacity:0.5;' Title = 'History' class='fa fa-history'></i>";
            return btn;

        }
        public string getCopyBtn(int HeaderId, string status, string couponcat, List<UserRoleDetails> role)
        {
            var btn = "";
            if (CheckPTRCopyAllow(couponcat, status, role))
            {
                btn = "<i data-modal = '' id = 'Copy' name = 'Copy' style = 'cursor:pointer;margin-left:5px;' Title = 'Copy' class='fa fa-files-o' onClick=CopyPTR(" + HeaderId + ")></i>";
                //if (status.Equals(clsImplementationEnum.PTRStatus.Closed.GetStringValue()) || (RevNo > 0 && status.Equals(clsImplementationEnum.PTRStatus.Draft.GetStringValue())))
                //    btn = "<i data-modal = '' id = 'Copy' name = 'Copy' style = 'cursor:pointer;margin-left:5px;' Title = 'Copy' class='fa fa-files-o' onClick=CopyPTR(" + HeaderId + ")></i>";
                //else
                //    btn = btn = "<i data-modal = '' id = 'Copy' name = 'Copy' style = 'cursor:pointer;margin-left:5px;opacity:0.5;' Title = 'Copy' class='fa fa-files-o'></i>";
            }
            else
            {
                btn = btn = "<i data-modal = '' id = 'Copy' name = 'Copy' style = 'cursor:pointer;margin-left:5px;opacity:0.5;' Title = 'Copy' class='fa fa-files-o'></i>";
            }
            return btn;

        }

        public bool CheckPTRCopyAllow(string CouponCategory, string PTRStatus, List<UserRoleDetails> role)
        {
            var Status = false;
            if ((Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue().ToUpper() ||
                    Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.ProcQualf.GetStringValue().ToUpper() ||
                    Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.ConsQualf.GetStringValue().ToUpper() ||
                    Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.MockUp.GetStringValue().ToUpper() ||
                    Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.PerfQualf.GetStringValue().ToUpper())
                    && (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.WE3.GetStringValue())))
            {
                Status = true;
            }
            else
            {
                if (Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.Draft.GetStringValue().ToUpper() ||
                Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.RetQA3.GetStringValue().ToUpper() ||
                Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.RetQC3.GetStringValue().ToUpper() ||
                Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.RetWE2.GetStringValue().ToUpper() ||
                Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.Closed.GetStringValue().ToUpper())
                {
                    if ((Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue().ToUpper() ||
                        Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.ProcQualf.GetStringValue().ToUpper() ||
                        Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.ConsQualf.GetStringValue().ToUpper() ||
                        Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.MockUp.GetStringValue().ToUpper() ||
                        Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.PerfQualf.GetStringValue().ToUpper())
                        && (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.WE3.GetStringValue())))
                    {
                        Status = true;
                    }
                    else if ((Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue().ToUpper())
                        && (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.PMG3.GetStringValue()) ||
                            role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.MCC3.GetStringValue()) ||
                            role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.SCC3.GetStringValue())))
                    {
                        Status = true;
                    }
                    else if ((Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.WaterAnls.GetStringValue().ToUpper())
                        && (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.QC3.GetStringValue())))
                    {
                        Status = true;
                    }
                }
            }
            return Status;
        }

        public bool CheckHeatPlateAllowToAdd(string CouponCategory, string PTRStatus, List<UserRoleDetails> role)
        {
            var Status = false;
            if ((role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.QC3.GetStringValue()))
                || (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.WE3.GetStringValue()))
                || (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.PMG3.GetStringValue()))
                || (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.MCC3.GetStringValue()))
                || (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.SCC3.GetStringValue())))
            {
                if (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.QC3.GetStringValue()))
                {
                    if ((Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.Draft.GetStringValue().ToUpper()) &&
                        (Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.WaterAnls.GetStringValue().ToUpper()))
                    {
                        Status = true;
                    }
                    else if (Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue().ToUpper() &&
                        (Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue().ToUpper() ||
                         Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue().ToUpper()))
                    {
                        Status = true;
                    }
                }
                if (!Status)
                {
                    if (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.WE3.GetStringValue()))
                    {
                        if ((Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue().ToUpper()
                            || Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.ProcQualf.GetStringValue().ToUpper()
                            || Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.ConsQualf.GetStringValue().ToUpper()
                            || Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.MockUp.GetStringValue().ToUpper()
                            || Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.PerfQualf.GetStringValue().ToUpper())
                          && (Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.Draft.GetStringValue().ToUpper()
                            || Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.RetQA3.GetStringValue().ToUpper()
                            || Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.RetQC3.GetStringValue().ToUpper()
                            || Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.RetWE2.GetStringValue().ToUpper()))
                        {
                            Status = true;
                        }
                    }
                }
                if (!Status)
                {
                    if ((role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.PMG3.GetStringValue()))
                        || (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.MCC3.GetStringValue()))
                        || (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.SCC3.GetStringValue())))
                    {
                        if ((Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue().ToUpper())
                            && (Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.Draft.GetStringValue().ToUpper()
                            || Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.RetQA3.GetStringValue().ToUpper()
                            || Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.RetQC3.GetStringValue().ToUpper()))
                        {
                            Status = true;
                        }
                    }
                }
            }
            return Status;
        }
        public bool CheckTestCompletionAllowToAdd(string CouponCategory, string PTRStatus, List<UserRoleDetails> role)
        {
            var Status = false;
            if ((role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.QC3.GetStringValue()) ||
                role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.QA3.GetStringValue()) ||
                role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.WTC3.GetStringValue())))
            {
                if (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.QC3.GetStringValue()))
                {
                    if ((Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.Draft.GetStringValue().ToUpper()) &&
                        (Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.WaterAnls.GetStringValue().ToUpper()))
                    {
                        Status = true;
                    }
                    else if (Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.SubmitQC3.GetStringValue().ToUpper() &&
                        (Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.ProdTestCpn.GetStringValue().ToUpper() ||
                         Convert.ToString(CouponCategory).ToUpper() == clsImplementationEnum.CouponCat.MatTestCpn.GetStringValue().ToUpper()))
                    {
                        Status = true;
                    }
                }
                if (role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.QA3.GetStringValue()) ||
                    role.Any(i => i.UserRoleDesc == clsImplementationEnum.UserRoleName.WTC3.GetStringValue()))
                {
                    if ((Convert.ToString(PTRStatus).ToUpper() == clsImplementationEnum.PTRStatus.SubmitQA3.GetStringValue().ToUpper()))
                    {
                        Status = true;
                    }
                }
            }
            return Status;
        }
        public string GetTestType(string MechTest, string ChemTest)
        {
            if (MechTest == "true" && ChemTest == "true")
                return "Both";
            else if (MechTest == "true" && ChemTest == "false")
                return "Mechanical";
            else if (MechTest == "false" && ChemTest == "true")
                return "Chemical";
            else
                return "";
        }
        public ActionResult GetQMSProjectDetail(string QMSProject, string location)
        {
            ResponceMsgProjectBU objResponseMsg = new ResponceMsgProjectBU();

            if (!string.IsNullOrEmpty(QMSProject))
            {
                var projectCode = db.QMS010.Where(i => i.QualityProject.Equals(QMSProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.Project).FirstOrDefault();
                var manufacturingCode = db.TMP001.Where(i => i.QualityProject.Equals(QMSProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.ManufacturingCode).FirstOrDefault();
                if (!string.IsNullOrEmpty(projectCode))
                {
                    objResponseMsg.project = (from a in db.COM001
                                              where a.t_cprj.Equals(projectCode, StringComparison.OrdinalIgnoreCase)
                                              select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();

                    var projectDetail = new clsManager().getProjectWiseBULocation(projectCode);
                    objResponseMsg.BU = projectDetail.QCPBU;
                    objResponseMsg.Key = true;
                    ResponceMsgProjectBU obj = GetMaxPRTNo();
                    objResponseMsg.DocNo = obj.DocNo;
                    objResponseMsg.PTRNo = obj.PTRNo;
                }
                else
                {
                    objResponseMsg.Value = "Project Code Not Found";
                    objResponseMsg.Key = false;
                }
            }
            else
            {
                objResponseMsg.Value = "Enter Value of Quality Project";
                objResponseMsg.Key = false;
            }

            return Json(objResponseMsg);
        }
        public ActionResult getSubCategory(string data)
        {
            string param = data;
            int id = Convert.ToInt32(param.Split(',')[0].Trim());
            string Key = param.Split(',')[1].Trim();
            PTR001 objPTR001 = db.PTR001.Where(x => x.HeaderId == id).FirstOrDefault();
            var lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category == Key && glb002.IsActive == true && objPTR001.BU.Equals(glb002.BU) && objPTR001.Location.Equals(glb002.Location) //&& (glb002.Code.Contains(search) || glb002.Description.Contains(search))
                             select new { id = glb002.Code, text = glb002.Code + " - " + glb002.Description }).ToList();
            return Json(lstGLB002, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getSeamNoForDropdown(string QualityProject, string term = "")
        {
            var lstData = db.QMS012_Log.Where(x => (term == "" || x.SeamNo.Contains(term)) && x.QualityProject == QualityProject) // [Obs. 16456] x.SeamNo.StartsWith("PTC") && 
                .Select(s => new { id = s.SeamNo, text = s.SeamNo, Description = s.SeamNo }).Distinct().Take(10).ToList();
            return Json(lstData, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult GetCodeSpecification(string BU = "", string Location = "")
        //{
        //    if (!string.IsNullOrEmpty(BU) && !string.IsNullOrEmpty(Location))
        //    {
        //        var lstData = Manager.GetSubCatagories("Manufacturing Code", BU, Location)
        //      .Select(i => new { id = i.Code, text = i.Code, Description = i.Description }).ToList();
        //        return Json(lstData, JsonRequestBehavior.AllowGet);
        //    }
        //    else {
        //        return Json("", JsonRequestBehavior.AllowGet);
        //    }
        //}

        public ActionResult getWelderIDForAutoComplete(string QualityProject, string SeamNo, string WeldingProcess, string Loc, string BU, string term = "")
        {
            if (string.IsNullOrWhiteSpace(SeamNo) || string.IsNullOrWhiteSpace(WeldingProcess))
                return Json(new string[] { }, JsonRequestBehavior.AllowGet);
            var lstWeldingProcess = WeldingProcess.Split(',');
            var lstSeamNo = SeamNo.Split(',');
            var lstData = db.WPS025.Where(x => x.QualityProject == QualityProject && lstSeamNo.Contains(x.SeamNo) && lstWeldingProcess.Contains(x.WeldingProcess) && x.Location == Loc && x.BU == BU && (term == "" || x.WelderPSNO.Equals(term)))
                .Select(s => new AutoCompleteList { id = s.WelderPSNO, value = s.WelderPSNO, label = s.WelderPSNO }).Distinct().Take(10).ToList();

            foreach (var item in lstData)
            {
                item.label += " - " + Manager.GetUserNameFromPsNo(item.id);
            }
            return Json(lstData, JsonRequestBehavior.AllowGet);
        }

        public bool checkProjectExist(string projectcode, string location)
        {
            bool Flag = false;
            string locationCode = location.Split('-')[0].Trim();
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                PTR001 objPTR001 = db.PTR001.Where(x => x.QualityProject == projectcode && x.Location == locationCode).FirstOrDefault();
                if (objPTR001 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }
        public ActionResult GetBU(string projectcode)
        {
            ResponceMsgProjectBU objResponseMsg = new ResponceMsgProjectBU();
            PTR001 objPTR001 = new PTR001();
            string Project = projectcode.Split('-')[0].Trim();
            string BUid = db.COM001.Where(i => i.t_cprj == Project).FirstOrDefault().t_entu;
            var BUDescription = (from a in db.COM002
                                 where a.t_dtyp == 2 && a.t_dimx == BUid
                                 select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();
            objResponseMsg.BU = BUDescription.BUDesc;
            objResponseMsg.Project = Project;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public int getRoleId(string strRole)
        {
            var Id = db.ATH004.Where(x => x.Role.Equals(strRole)).Select(x => x.Id).FirstOrDefault();
            return Id;
        }
        public string getEmailIdByRole(int role, PTR001 objPTR001)
        {
            List<string> lstEmp = (from a in db.ATH001
                                   where a.Role == role && a.Location == objPTR001.Location
                                   && a.BU == objPTR001.BU
                                   && (a.Contract == "ALL" && a.Project == "ALL" && (from b in db.COM005 select b.t_sprj).ToList().Contains(objPTR001.Project)
                                      || (a.Contract == "ALL" && a.Project != "ALL" && a.Project == objPTR001.Project)
                                      || (a.Contract != "ALL" && a.Project == "ALL" && (from c in db.COM005 where c.t_cono == a.Contract select c.t_sprj).ToList().Contains(objPTR001.Project))
                                      || (a.Contract != "ALL" && a.Project != "ALL" && a.Project == objPTR001.Project))
                                   select a.Employee).ToList();
            for (var i = 0; i < lstEmp.Count; i++)
                lstEmp[i] = Manager.GetMailIdFromPsNo(lstEmp[i]);

            string email = String.Join(";", lstEmp);

            return email;
        }
        [NonAction]
        public void copyDocuments(int HeaderId, int RevNo)
        {
            var folderPath = "PTR001/" + HeaderId + "/R" + RevNo;
            var oldFolderPath = "PTR001/" + HeaderId + "/R" + (RevNo - 1);
            (new clsFileUpload()).CopyFolderContentsAsync(oldFolderPath, folderPath);
        }
        public ActionResult GetWE2Result(string term)
        {
            List<ApproverModel> lstApprovers = Manager.GetApproverList(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term).ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetQC3Result(string term)
        {
            List<ApproverModel> lstApprovers = Manager.GetApproverList(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objClsLoginInfo.Location, objClsLoginInfo.UserName, term).ToList();
            return Json(lstApprovers, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetInspectionStage(string term, string strBU = "", string strLoc = "")
        {
            List<CategoryData> lstGLB002 = new List<CategoryData>();
            if (string.IsNullOrEmpty(term))
            {
                lstGLB002 = Manager.GetSubCatagories("TPI Intervention", strBU, strLoc).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                /*lstGLB002 = (from qms002 in db.QMS002
                             where strBU.Equals(qms002.BU) && strLoc.Equals(qms002.Location)
                             select new CategoryData { Code = qms002.StageCode, CategoryDescription = qms002.StageCode + " - " + qms002.StageDesc }).ToList();*/
            }
            else
            {
                lstGLB002 = Manager.GetSubCatagories("TPI Intervention", strBU, strLoc).Where(w => w.Code.Contains(term) || w.Description.Contains(term) || (w.Code + " - " + w.Description).Contains(term)).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                /*lstGLB002 = (from qms002 in db.QMS002
                             where strBU.Equals(qms002.BU) && strLoc.Equals(qms002.Location)
                             && (qms002.StageCode.Contains(term) || qms002.StageDesc.Contains(term) || (qms002.StageCode + " - " + qms002.StageDesc).Contains(term))
                             select new CategoryData { Code = qms002.StageCode, CategoryDescription = qms002.StageCode + " - " + qms002.StageDesc }).ToList();*/
            }
            return Json(lstGLB002, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetCouponID(string ptrNo, string term)
        {
            var CouponNo = (from ptr002 in db.PTR002
                            where (ptrNo.Equals(ptr002.PTRNo) && ptr002.CouponID.Contains(term))
                            select ptr002.CouponID).ToList();
            return Json(CouponNo, JsonRequestBehavior.AllowGet);
        }
        public string getCategoryDesc(string Key, string BU, string loc, string code)
        {
            string catDesc = (from glb002 in db.GLB002
                              join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                              where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && BU.Contains(glb002.BU) && loc.Equals(glb002.Location) && code.Equals(glb002.Code)
                              select glb002.Description).FirstOrDefault();
            if (string.IsNullOrEmpty(catDesc))
                return code;
            return catDesc;
        }
        public string getCategoryDesc2(string Key, string BU, string loc, string code)
        {
            string catDesc = (from glb002 in db.GLB002
                              join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                              where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && BU.Contains(glb002.BU) && loc.Equals(glb002.Location) && code.Equals(glb002.Code)
                              select glb002.Description).FirstOrDefault();
            if (string.IsNullOrEmpty(catDesc))
                return code;
            return catDesc;
        }
        [HttpPost]
        public ActionResult GetSubCatagory2(string term, string Key, string strBU = "", string strLoc = "")
        {
            List<CategoryData> lstGLB002 = new List<CategoryData>();
            if (string.IsNullOrEmpty(term))
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category == Key && glb002.IsActive == true && strBU.Equals(glb002.BU) && strLoc.Equals(glb002.Location)
                             select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Description }).ToList();
            else
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category == Key && glb002.IsActive == true && strBU.Equals(glb002.BU) && strLoc.Equals(glb002.Location)
                             && (glb002.Code.Contains(term) || glb002.Description.Contains(term) || (glb002.Code + " - " + glb002.Description).Contains(term))
                             select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Description }).ToList();

            return Json(lstGLB002, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetSubCatagory(string term, string Key, string strBU = "", string strLoc = "")
        {
            List<CategoryData> lstGLB002 = new List<CategoryData>();
            if (string.IsNullOrEmpty(term))
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category == Key && glb002.IsActive == true && strBU.Equals(glb002.BU) && strLoc.Equals(glb002.Location)
                             select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Code + " - " + glb002.Description }).ToList();
            else
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category == Key && glb002.IsActive == true && strBU.Equals(glb002.BU) && strLoc.Equals(glb002.Location)
                             && (glb002.Code.Contains(term) || glb002.Description.Contains(term) || (glb002.Code + " - " + glb002.Description).Contains(term))
                             select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Code + " - " + glb002.Description }).ToList();

            return Json(lstGLB002, JsonRequestBehavior.AllowGet);
        }
        [NonAction]
        public List<CategoryData> GetCatagoryList(string term, string Key, string strBU = "", string strLoc = "")
        {
            List<CategoryData> lstGLB002 = new List<CategoryData>();
            if (string.IsNullOrEmpty(term))
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category == Key && glb002.IsActive == true && strBU.Equals(glb002.BU) && strLoc.Equals(glb002.Location)
                             select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Code + " - " + glb002.Description, Description = glb002.Description }).ToList();
            else
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category == Key && glb002.IsActive == true && strBU.Equals(glb002.BU) && strLoc.Equals(glb002.Location)
                             && (glb002.Code.Contains(term) || glb002.Description.Contains(term) || (glb002.Code + " - " + glb002.Description).Contains(term))
                             select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Code + " - " + glb002.Description, Description = glb002.Description }).ToList();

            return lstGLB002;
        }

        public ActionResult GetWPSNumber(string SeamNo, string QualityProject, string Location, string BU)
        {
            List<AutoCompleteList> lstWPS = new List<AutoCompleteList>();
            try
            {
                if (!string.IsNullOrEmpty(SeamNo))
                {
                    var arraySeamNo = SeamNo.Split(',').ToArray();
                    var listSWP = db.SWP010.Where(i => arraySeamNo.Contains(i.SeamNo) && i.QualityProject == QualityProject && i.Location == Location && i.BU == BU).ToList();

                    foreach (var item in listSWP)
                    {
                        if (!string.IsNullOrEmpty(item.MainWPSNumber))
                        {
                            lstWPS.Add(new AutoCompleteList { id = item.MainWPSNumber, value = item.MainWPSNumber, label = item.MainWPSNumber });
                        }
                        if (!string.IsNullOrEmpty(item.AlternateWPS1) && !lstWPS.Any(i => i.id == item.AlternateWPS1))
                        {
                            lstWPS.Add(new AutoCompleteList { id = item.AlternateWPS1, value = item.AlternateWPS1, label = item.AlternateWPS1 });
                        }
                        if (!string.IsNullOrEmpty(item.AlternateWPS2) && !lstWPS.Any(i => i.id == item.AlternateWPS2))
                        {
                            lstWPS.Add(new AutoCompleteList { id = item.AlternateWPS2, value = item.AlternateWPS2, label = item.AlternateWPS2 });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstWPS, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetWPSWeldingProcess(string WPSNo, string term = "")
        {
            List<AutoCompleteList> lstWeldingProcess = new List<AutoCompleteList>();
            if (!string.IsNullOrEmpty(WPSNo))
            {
                lstWeldingProcess = db.WPS011.Where(i => (term == "" || i.WeldingProcess.Contains(term)) && i.WPSNumber.ToLower() == WPSNo.ToLower()).Select(i => new { WeldingProcess = i.WeldingProcess }).Distinct().Select(i => new AutoCompleteList { id = i.WeldingProcess, value = i.WeldingProcess, label = i.WeldingProcess }).ToList();
            }
            var lstData = lstWeldingProcess.Select(s => new { id = s.id.Trim(), text = s.value.Trim(), Description = s.label.Trim() }).Distinct().Take(10).ToList();
            return Json(lstData, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region HTML elements
        [NonAction]
        public string GenerateAutoCompleteonBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control " + columnName;
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + MaxCharcters + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        [NonAction]
        public string GenerateAutoCompleteBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", int hid = 0)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "_" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control " + columnName;
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' hid='" + hid + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + MaxCharcters + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }
        [NonAction]
        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }
        [NonAction]
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (disabled)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;opacity: 0.5;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";

            return htmlControl;
        }
        [NonAction]
        public static string GenerateTextbox(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onKeyPressMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control " + columnName;
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange=\"" + onBlurMethod + "\"" : "";
            string onKeypressEvent = !string.IsNullOrEmpty(onKeyPressMethod) ? "onkeypress='" + onKeyPressMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            //htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' " + MaxCharcters + " value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + onKeypressEvent + " "+ onClickEvent +" spara='" + rowId + "' />";
            htmlControl = "<input type=\"text\" " + (isReadOnly ? "readonly=\"readonly\"" : "") + " id=\"" + inputID + "\" " + MaxCharcters + " value=\"" + inputValue + "\" oldvalue=\"" + inputValue + "\" colname=\"" + columnName + "\" name=\"" + inputID + "\" class=\"" + className + "\" style=\"" + inputStyle + "\"  " + onBlurEvent + "  " + onKeypressEvent + " " + onClickEvent + " spara=\"" + rowId + "\" />";

            return htmlControl;
        }
        [NonAction]
        public static string MultiSelectDropdown(int rowId, List<CategoryData> list, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string ColumnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }

                string inputID = ColumnName + "" + rowId.ToString();
                multipleSelect += "<select name=\"" + inputID + "\" id=\"" + inputID + "\" multiple=\"multiple\"  style=\"width: 100 % \" colname=\"" + ColumnName + "\" class=\"form-control\" " + (Disabled ? "disabled" : "") + (OnChangeEvent != string.Empty ? " onchange=\"" + OnChangeEvent + "\"" : "") + " oldvalue=\"" + selectedValue + "\" >";
                foreach (var item in list)
                {
                    multipleSelect += "<option value=\"" + item.Code + "\" " + ((arrayVal.Contains(item.Code)) ? " selected" : "") + " >" + item.Description + "</option>";
                }
                foreach (var item in arrayVal)
                {
                    if (!list.Any(i => i.Code == item))
                    {
                        multipleSelect += "<option value=\"" + item + "\" selected>" + item + "</option>";
                    }
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }
        #endregion

        #region Import Excel

        [HttpPost]
        public ActionResult LoadImportDataPartial(int headerid)
        {
            ViewBag.HeaderId = headerid;
            return PartialView("_UploadExcelPartial");
        }

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            {
                ExcelPackage package = new ExcelPackage(upload.InputStream);
                string Location = objClsLoginInfo.Location;

                int hID = !string.IsNullOrEmpty(fc["headerid"]) ? Convert.ToInt32(fc["headerid"]) : 0;
                PTR001 objptr001;

                if (hID > 0)
                {
                    objptr001 = db.PTR001.Where(x => x.HeaderId == hID).FirstOrDefault();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Header Value is Not Avaliable. Please try again";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                bool isError = false;
                DataSet ds = GetDataFromExcel(package, objptr001, out isError);
                if (!isError)
                {
                    try
                    {
                        List<PTR003> lstPTR003 = new List<PTR003>();
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("CouponID")) && !string.IsNullOrWhiteSpace(item.Field<string>("TestTypeOrientation")) && !string.IsNullOrWhiteSpace(item.Field<string>("SimpleLocation")) && !string.IsNullOrWhiteSpace(item.Field<string>("TestTemperature")) && !string.IsNullOrWhiteSpace(item.Field<string>("NoofSamples")) && !string.IsNullOrWhiteSpace(item.Field<string>("AcceptanceCriteria")) && !string.IsNullOrWhiteSpace(item.Field<string>("TestingSpecification")))
                            {
                                #region Header 

                                PTR003 objPTR003 = new PTR003();
                                objPTR003.HeaderId = objptr001.HeaderId;
                                objPTR003.Project = objptr001.Project;
                                objPTR003.BU = objptr001.BU;
                                objPTR003.Location = objptr001.Location;
                                objPTR003.RevNo = objptr001.RevNo;
                                objPTR003.PTRNo = objptr001.PTRNo;

                                objPTR003.CouponID = item.Field<string>("CouponID");
                                objPTR003.TestTypeOrientation = getCategoryCode("Test Type Orientation", objptr001.BU.Trim(), objptr001.Location.Trim(), item.Field<string>("TestTypeOrientation"), true);

                                string[] SimpleLocationArray = item.Field<string>("SimpleLocation").ToLower().Split(',').ToArray();

                                List<string> lstSimpleLocation = new List<string>();
                                foreach (var ite in SimpleLocationArray)
                                {
                                    if (!string.IsNullOrWhiteSpace(ite))
                                    {
                                        if (!string.IsNullOrWhiteSpace(getCategoryCode("Sample Location Master", objptr001.BU.Trim(), objptr001.Location.Trim(), ite, true)))
                                        {
                                            lstSimpleLocation.Add(getCategoryCode("Sample Location Master", objptr001.BU.Trim(), objptr001.Location.Trim(), ite, true));
                                        }
                                    }
                                }
                                if (lstSimpleLocation.Count() > 0)
                                {
                                    objPTR003.SimpleLocation = string.Join(",", lstSimpleLocation);
                                }

                                string[] TestTemperatureArray = item.Field<string>("TestTemperature").ToLower().Split(',').ToArray();

                                List<string> lstTestTemperature = new List<string>();
                                foreach (var ite in TestTemperatureArray)
                                {
                                    if (!string.IsNullOrWhiteSpace(ite))
                                    {
                                        if (!string.IsNullOrWhiteSpace(getCategoryCode("Test Temperature Master", objptr001.BU.Trim(), objptr001.Location.Trim(), ite, true)))
                                        {
                                            lstTestTemperature.Add(getCategoryCode("Test Temperature Master", objptr001.BU.Trim(), objptr001.Location.Trim(), ite, true));
                                        }
                                    }
                                }
                                if (lstTestTemperature.Count() > 0)
                                {
                                    objPTR003.TestTemperature = string.Join(",", lstTestTemperature);
                                }

                                //objPTR003.SimpleLocation = item.Field<string>("SimpleLocation");
                                //objPTR003.TestTemperature = item.Field<string>("TestTemperature");
                                objPTR003.NoofSamples = item.Field<string>("NoofSamples");
                                objPTR003.AcceptanceCriteria = item.Field<string>("AcceptanceCriteria");
                                objPTR003.TestingSpecification = item.Field<string>("TestingSpecification");
                                objPTR003.Remarks = item.Field<string>("Remarks");

                                objPTR003.CreatedBy = objClsLoginInfo.UserName;
                                objPTR003.CreatedOn = DateTime.Now;
                                lstPTR003.Add(objPTR003);

                                #endregion
                            }
                        }

                        if (lstPTR003.Count > 0)
                        {
                            db.PTR003.AddRange(lstPTR003);
                            db.SaveChanges();
                        }
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Mechanical Test Details Imported Successfully";
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                    }
                }
                else
                {
                    objResponseMsg = ErrorExportToExcel(ds);
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Excel file is not valid";
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public DataSet GetDataFromExcel(ExcelPackage package, PTR001 objPTR001, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtLinesExcel = new DataTable();
            isError = false;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtLinesExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtLinesExcel.NewRow();

                    foreach (var cell in row)
                    {
                        newRow[cell.Start.Column - 1] = cell.Text;
                    }

                    dtLinesExcel.Rows.Add(newRow);
                }
                #endregion

                #region Error columns for Header Data

                dtLinesExcel.Columns.Add("CouponIDErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("TestTypeOrientationErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("SimpleLocationErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("TestTemperatureErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("NoofSamplesErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("AcceptanceCriteriaErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("TestingSpecificationErrorMsg", typeof(string));
                dtLinesExcel.Columns.Add("RemarksErrorMsg", typeof(string));

                #endregion

                #region Validation 

                List<CategoryData> lstCouponList = db.PTR002.Where(x => x.HeaderId == objPTR001.HeaderId).Select(i => new CategoryData { Value = i.CouponID, Code = i.CouponID, CategoryDescription = i.CouponID }).ToList();
                List<CategoryData> lstTestTypeOrientationList = Manager.GetSubCatagories("Test Type Orientation", objPTR001.BU, objPTR001.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();

                List<CategoryData> lstSampleLocationList = GetCatagoryList("", "Sample Location Master", objPTR001.BU, objPTR001.Location);
                List<CategoryData> lstTestTemperatureList = GetCatagoryList("", "Test Temperature Master", objPTR001.BU, objPTR001.Location);

                foreach (DataRow item in dtLinesExcel.Rows)
                {
                    #region string variables

                    string errorMessage = string.Empty;

                    string CouponID = item.Field<string>("CouponID");
                    string TestTypeOrientation = item.Field<string>("TestTypeOrientation");

                    string SimpleLocation = item.Field<string>("SimpleLocation");
                    string TestTemperature = item.Field<string>("TestTemperature");

                    string NoofSamples = item.Field<string>("NoofSamples");
                    string AcceptanceCriteria = item.Field<string>("AcceptanceCriteria");
                    string TestingSpecification = item.Field<string>("TestingSpecification");
                    string Remarks = item.Field<string>("Remarks");


                    #endregion

                    #region Validate Header

                    if (!string.IsNullOrWhiteSpace(CouponID) || !string.IsNullOrWhiteSpace(TestTypeOrientation) || !string.IsNullOrWhiteSpace(SimpleLocation) ||
                        !string.IsNullOrWhiteSpace(TestTemperature) || !string.IsNullOrWhiteSpace(NoofSamples) || !string.IsNullOrWhiteSpace(AcceptanceCriteria) ||
                        !string.IsNullOrWhiteSpace(TestingSpecification) || !string.IsNullOrWhiteSpace(Remarks))
                    {
                        if (string.IsNullOrWhiteSpace(CouponID))
                        {
                            item["CouponIDErrorMsg"] = "CouponID is Required"; isError = true;
                        }
                        //else
                        //{
                        //    if (!lstCouponList.Any(u => u.CategoryDescription.Trim().ToLower() == CouponID.Trim().ToLower()))
                        //    {
                        //        item["CouponIDErrorMsg"] = "CouponID is not belongs to User"; isError = true;
                        //    }
                        //}

                        if (string.IsNullOrWhiteSpace(TestTypeOrientation))
                        {
                            item["TestTypeOrientationErrorMsg"] = "TestTypeOrientation is Required"; isError = true;
                        }
                        else
                        {
                            if (!lstTestTypeOrientationList.Any(u => u.CategoryDescription.Trim().ToLower() == TestTypeOrientation.Trim().ToLower()))
                            {
                                item["TestTypeOrientationErrorMsg"] = "TestTypeOrientation is not belongs to User"; isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(SimpleLocation))
                        {
                            item["SimpleLocationErrorMsg"] = "SimpleLocation is Required"; isError = true;
                        }
                        else
                        {
                            string[] arraySimpleLocation = SimpleLocation.Split(',');
                            string ismultistringerror1 = string.Empty;
                            bool ismultierror1 = false;
                            bool errorSimple = false;

                            for (int i = 0; i < arraySimpleLocation.Length; i++)
                            {
                                string location = arraySimpleLocation[i];
                                if (!lstSampleLocationList.Any(u => u.Description.Trim().ToLower() == location.Trim().ToLower()))
                                {
                                    if (errorSimple)
                                        ismultistringerror1 += ", " + location;
                                    else
                                    {
                                        ismultistringerror1 += location;
                                        errorSimple = true;
                                    }
                                    ismultierror1 = true;
                                }
                            }

                            if (ismultierror1)
                            {
                                item["SimpleLocationErrorMsg"] = "SimpleLocation " + ismultistringerror1 + " is not belongs to User";
                                isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(TestTemperature))
                        {
                            item["TestTemperatureErrorMsg"] = "TestTemperature is Required";
                            isError = true;
                        }
                        else
                        {
                            string[] arrayTestTemperature = TestTemperature.Split(',');
                            string ismultistringerror2 = string.Empty;
                            bool ismultierror2 = false;
                            bool errortest = false;

                            for (int i = 0; i < arrayTestTemperature.Length; i++)
                            {
                                string Temperature = arrayTestTemperature[i];
                                if (!lstTestTemperatureList.Any(u => u.Description.Trim().ToLower() == Temperature.Trim().ToLower()))
                                {
                                    if (errortest)
                                        ismultistringerror2 += ", " + Temperature;
                                    else
                                    {
                                        ismultistringerror2 += Temperature;
                                        errortest = true;
                                    }

                                    ismultierror2 = true;
                                }
                            }

                            if (ismultierror2)
                            {
                                item["TestTemperatureErrorMsg"] = "TestTemperature " + ismultistringerror2 + " is not belongs to User";
                                isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(NoofSamples))
                        {
                            item["NoofSamplesErrorMsg"] = "NoofSamples is Required"; isError = true;
                        }
                        else
                        {
                            if (NoofSamples.Length > 10)
                            {
                                item["NoofSamplesErrorMsg"] = "NoofSamples have maximum limit of 10 characters"; isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(AcceptanceCriteria))
                        {
                            item["AcceptanceCriteriaErrorMsg"] = "Acceptance Criteria is Required"; isError = true;
                        }
                        else
                        {
                            if (AcceptanceCriteria.Length > 70)
                            {
                                item["AcceptanceCriteriaErrorMsg"] = "Acceptance Criteria have maximum limit of 70 characters"; isError = true;
                            }
                        }

                        if (string.IsNullOrWhiteSpace(TestingSpecification))
                        {
                            item["TestingSpecificationErrorMsg"] = "Testing Specification is Required"; isError = true;
                        }
                        else
                        {
                            if (NoofSamples.Length > 50)
                            {
                                item["TestingSpecificationErrorMsg"] = "Testing Specification have maximum limit of 50 characters"; isError = true;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(Remarks))
                        {
                            if (Remarks.Length > 50)
                            {
                                item["RemarksErrorMsg"] = "Remarks have maximum limit of 50 characters"; isError = true;
                            }
                        }
                    }

                    #endregion
                }

                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtLinesExcel);
            return ds;
        }

        public clsHelper.ResponceMsgWithFileName ErrorExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/PTR/Mechanical Test - Error Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();

                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            #region Generate for Header

                            ///BU

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(8)))
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>(8).ToString() + " )";
                                excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }

                            ///Location
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(9)))
                            {
                                excelWorksheet.Cells[i, 2].Value = item.Field<string>(1) + " (Note: " + item.Field<string>(9).ToString() + " )";
                                excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }

                            ///Equipment
                            if (!string.IsNullOrWhiteSpace(item.Field<string>(10)))
                            {
                                excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>(10).ToString() + " )";
                                excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); ;
                            }
                            else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(11)))
                            {
                                excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>(11).ToString() + " )";
                                excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(12)))
                            {
                                excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>(12).ToString() + " )";
                                excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }

                            #endregion

                            #region Common Columns for Lines
                            ///lines

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(13)))
                            {
                                excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>(13).ToString() + " )";
                                excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(14)))
                            {
                                excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>(14).ToString() + " )";
                                excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }

                            if (!string.IsNullOrWhiteSpace(item.Field<string>(15)))
                            {
                                excelWorksheet.Cells[i, 8].Value = item.Field<string>(7) + " (Note: " + item.Field<string>(15).ToString() + " )";
                                excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red);
                            }
                            else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }

                            #endregion

                            i++;
                        }

                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public string getCategoryCode(string category, string bu, string Location, string code, bool IsDesc = false)
        {
            string categorycode = string.Empty;
            if (IsDesc)
            {
                categorycode = (from glb2 in db.GLB002
                                join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                where glb1.Category == category && glb2.Location.Trim() == Location.Trim() && (glb2.Description.ToLower().Trim() == code.ToLower().Trim())
                                select glb2.Code).FirstOrDefault();
            }
            else
            {
                categorycode = (from glb2 in db.GLB002
                                join glb1 in db.GLB001 on glb2.Category equals glb1.Id
                                where glb1.Category == category && glb2.Location.Trim() == Location.Trim() && (glb2.Code.ToLower().Trim() == code.ToLower().Trim())
                                select glb2.Code).FirstOrDefault();
            }
            if (categorycode == null)
            {
                categorycode = "";
            }
            return categorycode;
        }


        #endregion

        #region Export Excel
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, int? HeaderId, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_PTR_GETHEADER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      PTRNo = li.PTRNo,
                                      RevNo = li.RevNo,
                                      LabeId = li.LABId,
                                      labIdCreatedOn = li.LabIdCreatedOn.HasValue ? li.LabIdCreatedOn.Value.ToString("dd/MM/yyyy") : "",
                                      Project = li.Project,
                                      CouponCategory = li.CouponCategory,
                                      CouponNo = li.CouponNo,
                                      TypeOfTest = GetTestType(li.MechTest, li.ChemicalTest),
                                      Initiator = li.PTRInitiator,
                                      Status = li.Status,
                                      Location = li.Location,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HTTest.GetStringValue())
                {
                    var lst = db.SP_PTR_GET_HT_LINE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      RevNo = li.RevNo,
                                      PTRNo = li.PTRNo,
                                      CouponNo = li.CouponNo,
                                      CouponID = li.CouponID,
                                      HTType1 = li.HTType1,
                                      HTTemp1 = li.HTTemp1,
                                      HTTime1 = li.HTTime1,
                                      HTType2 = li.HTType2,
                                      HTTemp2 = li.HTTemp2,
                                      HTTime2 = li.HTTime2,
                                      HTType3 = li.HTType3,
                                      HTTemp3 = li.HTTemp3,
                                      HTTime3 = li.HTTime3,
                                      HTType4 = li.HTType4,
                                      HTTemp4 = li.HTTemp4,
                                      HTTime4 = li.HTTime4,
                                      HTType5 = li.HTType5,
                                      HTTemp5 = li.HTTemp5,
                                      HTTime5 = li.HTTime5,
                                      HTType6 = li.HTType6,
                                      HTTemp6 = li.HTTemp6,
                                      HTTime6 = li.HTTime6
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.MechTest.GetStringValue())
                {
                    var lst = db.SP_PTR_GET_MECH_LINE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      CouponID = li.CouponID,
                                      TestTypeOrientation = li.TestTypeOrientation,
                                      SimpleLocation = li.SimpleLocation,
                                      TestTemperature = li.TestTemperature,
                                      NoofSamples = li.NoofSamples,
                                      AcceptanceCriteria = li.AcceptanceCriteria,
                                      TestingSpecification = li.TestingSpecification,
                                      Remarks = li.Remarks
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.ChemicalTest.GetStringValue())
                {
                    var lst = db.SP_PTR_GET_CHEM_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var objPTR001 = db.PTR001.Where(i => i.HeaderId == HeaderId.Value).FirstOrDefault();
                    var lstSampleLocation = GetCatagoryList("", "Sample Location Master", objPTR001.BU, objPTR001.Location);
                    var lstElements = GetCatagoryList("", "Element", objPTR001.BU, objPTR001.Location);

                    var newlst = (from li in lst
                                  select new
                                  {
                                      CouponID = li.ROW_NO == 1 ? li.CouponID : "",
                                      SampleLocation = li.ROW_NO == 1 ? GetSampleLocationDetails(li.SampleLocation, lstSampleLocation) : "",
                                      TestingSpecification = li.ROW_NO == 1 ? li.TestingSpecification : "",
                                      Remarks = li.ROW_NO == 1 ? li.Remarks : "",
                                      Elements = GetElementDetails(li.Elements, lstElements),
                                      AcceptanceCriteria = li.AcceptanceCriteria,
                                      CreatedBy = li.CreatedBy,
                                      EditedBy = li.EditedBy
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HeatPlateTest.GetStringValue())
                {
                    var lst = db.SP_PTR_GET_HEAT_PLATE_LINE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      PartNo = li.PartNo,
                                      RevNo = li.RevNo,
                                      HeatNo1 = li.HeatNo1,
                                      PlateNo1 = li.PlateNo1,
                                      HeatNo2 = li.HeatNo2,
                                      PlateNo2 = li.PlateNo2
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.TestCompletion.GetStringValue())
                {
                    var lst = db.SP_PTR_GET_TEST_COMPLETION_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      CouponNo = li.CouponNo,
                                      CouponID = li.CouponID,
                                      MaterialReceiptDate = li.MaterialReceiptDate,
                                      MechTestCompletionDate = li.MechTestCompletionDate,
                                      ChecmicalTestCompletionTime = li.ChecmicalTestCompletionTime,
                                      Remarks = li.Remarks
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LabDPP.GetStringValue())
                {
                    var lst = db.SP_PTR_GET_LAB_DPP(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from h in lst
                                  select new
                                  {
                                      SrNo = Convert.ToString(h.ROW_NO),
                                      LabId = Convert.ToString(h.LabId),
                                      RequestedDate = h.RequestedDate.HasValue ? h.RequestedDate.Value.ToString("dd/MM/yyy") : "",
                                      CouponReceiptDate = h.CouponReceiptDate.HasValue ? h.CouponReceiptDate.Value.ToString("dd/MM/yyy") : "",
                                      TestCompletionDate = h.TestCompletionDate.HasValue ? h.TestCompletionDate.Value.ToString("dd/MM/yyy") : "",
                                      DayTaken = h.DayTaken.HasValue ? Convert.ToString(h.DayTaken) : ""
                                  }).ToList();
                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Lab DPP
        [UserPermissions, SessionExpireFilter]
        public ActionResult LabDPP()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult LoadLabDPP(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName ={"LabId",
                                          "RequestedDate",
                                          "CouponReceiptDate",
                                          "TestCompletionDate",
                                          "DayTaken"
                                         };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstHeader = db.SP_PTR_GET_LAB_DPP(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from h in lstHeader
                          select new
                          {
                              SrNo = Convert.ToString(h.ROW_NO),
                              LabId = Convert.ToString(h.LabId),
                              RequestedDate = h.RequestedDate.HasValue ? h.RequestedDate.Value.ToString("dd/MM/yyy") : "",
                              CouponReceiptDate = h.CouponReceiptDate.HasValue ? h.CouponReceiptDate.Value.ToString("dd/MM/yyy") : "",
                              TestCompletionDate = h.TestCompletionDate.HasValue ? h.TestCompletionDate.Value.ToString("dd/MM/yyy") : "",
                              DayTaken = h.DayTaken.HasValue ? Convert.ToString(h.DayTaken) : ""
                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion


        #region PTRReports

        public ActionResult PTRReports(int HeaderId)
        {
            var user = objClsLoginInfo.UserName;
            ViewBag.HeaderId = HeaderId;
            PTR001 objPTR001 = new PTR001();
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

            string location = objPTR001.Location;
            string bu = objPTR001.BU;


            ViewBag.IsDifferentLocationUser = (objPTR001.Location.Trim().ToUpper() != objClsLoginInfo.Location.Trim().ToUpper());
            ViewBag.TestTypeOrientation = Manager.GetSubCatagories("Test Type Orientation", objPTR001.BU, objPTR001.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.CouponID = db.PTR002.Where(x => x.HeaderId == HeaderId).Select(i => new CategoryData { Value = i.CouponID, Code = i.CouponID, CategoryDescription = i.CouponID }).ToList();
            objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            if (chkAuth(objPTR001.CouponCategory, objPTR001.Status))
                ViewBag.auth = "yes";
            else
                ViewBag.auth = "no";

            string condition = string.Empty;
            condition += "where HeaderId=" + HeaderId + " ";

            var lstResult = db.SP_PTR_GET_MECH_LINE_DETAILS(null, null, "", condition).ToList();

            List<string> testTypes = lstResult.Select(x => x.TestTypeOrientation).Distinct().ToList();

            List<string> testTypesDesc = new List<string>();

            foreach (var type in testTypes)
            {
                string code = db.PTR000.Where(x => x.TestType == type && x.Location == location && x.BU == bu).Select(x => x.Code).FirstOrDefault();
                int categoryId = db.GLB001.Where(x => x.Category == "PTR Protocol Type").Select(x => x.Id).FirstOrDefault();
                string description = db.GLB002.Where(x => x.Code == code && x.Location == location && x.BU == bu && x.Category == categoryId).Select(x => x.Description).FirstOrDefault();
                if (description != null)
                    testTypesDesc.Add(description);
            }

            ViewBag.TestTypesDesc = testTypesDesc.Distinct().ToList();
            return View(objPTR001);
        }



        public ActionResult PTRChemicalReports(int HeaderId)
        {
            var user = objClsLoginInfo.UserName;
            ViewBag.HeaderId = HeaderId;
            PTR001 objPTR001 = new PTR001();
            objPTR001 = db.PTR001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

            string location = objPTR001.Location;
            string bu = objPTR001.BU;

            objPTR001.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objPTR001.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            objPTR001.Project = db.COM001.Where(i => i.t_cprj == objPTR001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            objPTR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objPTR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            if (chkAuth(objPTR001.CouponCategory, objPTR001.Status))
                ViewBag.auth = "yes";
            else
                ViewBag.auth = "no";

            string condition = string.Empty;
            condition += "where HeaderId=" + HeaderId + " ";

            var lstResult = db.SP_PTR_GET_CHEM_LINE_DETAILS(null, null, "", condition).ToList();

            List<string> testTypes = lstResult.Select(x => x.TestTypeOrientation).Distinct().ToList();

            List<string> testTypesDesc = new List<string>();

            foreach (var type in testTypes)
            {
                string code = db.PTR000.Where(x => x.TestType == type && x.Location == location && x.BU == bu).Select(x => x.Code).FirstOrDefault();
                int categoryId = db.GLB001.Where(x => x.Category == "PTR Protocol Type").Select(x => x.Id).FirstOrDefault();
                string description = db.GLB002.Where(x => x.Code == code && x.Location == location && x.BU == bu && x.Category == categoryId).Select(x => x.Description).FirstOrDefault();
                if (description !=null)
                    testTypesDesc.Add(description);
            }

            ViewBag.TestTypesDesc = testTypesDesc.Distinct().ToList();

            return View(objPTR001);
        }



        #endregion



        public class ResponceMsgWithHeaderId : clsHelper.ResponseMsg
        {
            public int HeaderId;
            public int LineId;
            public int? RevNo;
            public string PTRNo;
        }
        public class ResponceMsgProjectBU : clsHelper.ResponseMsg
        {
            public string BU;
            public string Project;
            public Projects project;
            public int DocNo;
            public string PTRNo;
        }
        public class CouponCat
        {
            public string Value { get; set; }
            public string Text { get; set; }
        }
        public class UserRoleDetails
        {
            public int UserRoleId { get; set; }
            public string UserRoleDesc { get; set; }
        }
    }
}