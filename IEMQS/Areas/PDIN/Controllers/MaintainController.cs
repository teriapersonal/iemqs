﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Data.Linq.Mapping;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationEnum;
//using System.Data.Linq.Mapping;

namespace IEMQS.Areas.PDIN.Controllers
{
    public class MaintainController : clsBase
    {
        IEMQSEntitiesContext context = null;
        List<PDINMergeDemergeAttachmentEnt> lstPDINMergeDemergeAttachmentEnt = null;

        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetHeaderDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderDataPartial");
        }

        [HttpPost]
        public JsonResult LoadDINHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                //if (param.CTQCompileStatus.ToUpper() == "ALL")
                //{
                //    strWhere += "1=1";
                //}
                //else
                //{
                //    strWhere += "1=1";
                //}
                strWhere += (param.CTQCompileStatus == "Pending") ? "LOWER([Status])='" + clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue().ToLower() + "' " : "1=1 ";
                //strWhere += "1=1 and pdn001.CreatedBy=('" + objClsLoginInfo.UserName + "')";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pdn001.Project+' - '+com1.t_dsca",
                                                "pdn001.Customer+' - '+com6.t_nama",
                                                "[Status]",
                                                "Product",
                                                "ProcessLicensor",
                                                "JobDescription",
                                                "pdn001.CreatedBy +' - '+com003c.t_name",
                                                "pdn001.IssueBy +' - '+com003e.t_name",
                                                "pdn001.ModifiedOn"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                var lstResult = db.SP_FETCH_PLANNINGDIN_HEADERS_PRJ_ACCESS
                                (
                                objClsLoginInfo.UserName, StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.Customer),
                               Convert.ToString(uc.CDD!=null ?uc.CDD.Value.ToString("dd/MM/yyyy"):string.Empty),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.IssueNo),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString(uc.JobDescription),
                               Convert.ToString(uc.CreatedBy),
                               Convert.ToString(uc.CreatedOn!=null ?uc.CreatedOn.Value.ToString("dd/MM/yyyy"):string.Empty),
                               Convert.ToString(uc.IssueBy),
                               Convert.ToString(uc.IssueDate!=null ?uc.IssueDate.Value.ToString("dd/MM/yyyy"):string.Empty),
                               Convert.ToString(uc.LatestDate!=null?uc.LatestDate.Value.ToString("dd/MM/yyyy"):string.Empty),
                               Convert.ToString(Manager.getPDNIdenticalproject(uc.Project.Split('-')[0])),
                               "<center><a title='View' href='"+WebsiteURL + "/PDIN/Maintain/Create?HeaderID="+Convert.ToInt32(uc.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title=\"Print\" onclick=\"PrintPDIN('"+uc.ProjectCode+"')\" ><i style='margin-left:5px;' class='fa fa-print'></i></a></center>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [NonAction]
        public bool CheckUserAccessOnProject(string Project)
        {
            var proj = Manager.getProjectsByUserAutocomplete(objClsLoginInfo.UserName, Project);
            if (proj.Count > 0 && proj.FirstOrDefault() != null)
                return true;
            else
                return false;
        }

        [SessionExpireFilter]
        public ActionResult Create(int HeaderID = 0)
        {
            string currentUser = objClsLoginInfo.UserName;
            PDN001 objPDN001 = db.PDN001.Where(x => x.HeaderId == HeaderID && x.CreatedBy == currentUser).FirstOrDefault();
            //if (HeaderID > 0 && objPDN001 == null)
            //{
            //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            //}
            ViewBag.HeaderID = HeaderID;
            return View(objPDN001);
        }

        [HttpPost]
        public ActionResult LoadHeaderFormPartial(int HeaderID)
        {
            PDN001 objPDN001 = db.PDN001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPDN001 != null && objPDN001.HeaderId > 0)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPDN001.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string customerName = db.COM006.Where(x => x.t_bpid == objPDN001.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objPDN001.Customer + " - " + customerName;
                //GetPDINLinesHTML(objPDN001.Project);
                if (objPDN001.Location != null && objPDN001.Location != string.Empty)
                {
                    ViewBag.Location = db.COM002.Where(i => i.t_dimx == objPDN001.Location && i.t_dtyp == 1).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                }
                ViewBag.IProject = objPDN001.IProject;
                ViewBag.LastIssueNo = db.PDN001.Where(x => x.Project == objPDN001.Project).Max(x => x.IssueNo);
            }
            else
            {
                objPDN001 = new PDN001();
                // int? maxIssueNo = db.PDN001.Select(x => x.IssueNo).FirstOrDefault();
                objPDN001.IssueNo = 1;//(maxIssueNo != null ? Convert.ToInt32(maxIssueNo + 1) : 1);
                objPDN001.Status = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue();

                ViewBag.Locations = db.SP_PDIN_GET_LOCATION_FOR_PDIN().Select(i => new { CatID = i.Code, CatDesc = i.Code + " - " + i.Name }).ToList();
                ViewBag.LastIssueNo = 1;
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            ViewBag.isbtndisplay = objClsLoginInfo.ListRoles.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.PLNG3.GetStringValue());
            ViewBag.isBtnMergeDemergeProjectEnable = objClsLoginInfo.ListRoles.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.PLNG2.GetStringValue());

            return PartialView("_LoadHeaderFormPartial", objPDN001);
        }
        [HttpPost]
        public ActionResult LoadIdenticalProjectPartial(string project = "", int refheaderid = 0)
        {
            if (!string.IsNullOrWhiteSpace(project))
            {
                project = project.Split('-')[0];
            }
            ViewBag.IdenticalProject = Manager.getPDNIdenticalproject(project, refheaderid);
            return PartialView("~/Areas/PDIN/Views/Shared/_IdenticalProjectPartial.cshtml");
        }

        [HttpPost]
        public JsonResult LoadDINHeaderLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();

                int CTQHeaderId = Convert.ToInt32(param.CTQHeaderId);
                string HeaderStatus = string.Empty;
                var objPDN001 = db.PDN001.FirstOrDefault(i => i.HeaderId == CTQHeaderId);
                if (objPDN001 != null)
                {
                    HeaderStatus = db.PDN001.FirstOrDefault(i => i.HeaderId == CTQHeaderId).Status;
                }
                else
                {
                    HeaderStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue().ToLower();
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "Description",
                                                "[Status]",
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                // strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);

                //var items = db.FN_GET_LOCATIONWISE_DEPARTMENT(objClsLoginInfo.Location, "").Select(x => new SelectItemList { id = x.t_dimx, text = x.t_dimx + "-" + x.t_desc, }).ToList();
                var items = db.FN_GET_LOCATIONWISE_DEPARTMENT_PDIN("", "", -1).Select(x => new SelectItemList { id = x.t_dimx, text = x.t_dimx + "-" + x.t_desc, }).ToList();

                var lstResult = db.SP_FETCH_PLANNINGDIN_HEADERS_LINES
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Description),
                                Convert.ToString(uc.Status),
                                //Helper.MultiSelectDropdown(items,uc.Department,(HeaderStatus.ToLower() == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue().ToLower()),"UpdateDepartment(this,'"+Convert.ToString(uc.LineId)+"')","Department"),
                                Helper.MultiSelectDropdown(items,uc.Department,false,"UpdateDepartment(this,'"+Convert.ToString(uc.LineId)+"')","Department"),
                                //GetTragetDateTextBox(uc.LineId,(HeaderStatus.ToLower() == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue().ToLower()),(uc.TargetDate!= null ? uc.TargetDate.Value.ToString("dd/MM/yyyy") : string.Empty),"TargetDate"),
                                GetTragetDateTextBox(uc.LineId,true,(uc.TargetDate!= null ? uc.TargetDate.Value.ToString("dd/MM/yyyy") : string.Empty),"TargetDate"),
                                (CheckUserAccessOnProject(uc.Project) ? Manager.GenerateLink(uc.PlanName,uc.DocumentNo,Convert.ToInt32(uc.RefId),false) : uc.DocumentNo),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    // isDisplayButton = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? IsDisplaySpecificationButton(lstResult.FirstOrDefault().Project) : false)
                    isDisplayButton = (lstResult.Count > 0 ? (lstResult.Where(x => x.Status == "Approved").Count() > 0 ? true : false) : false)
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = "",
                    isDisplayButton = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool IsDisplaySpecificationButton(string projectCode)
        {
            bool flage = false;
            TLP001 objTLP001 = db.TLP001.Where(x => x.Project == projectCode).OrderByDescending(x => x.Document).FirstOrDefault();
            if (objTLP001 != null)
            {
                string Document = objTLP001.Document.Trim().ToString();
                int LatestDocument = (Convert.ToInt32(Document.Substring(Document.Length - 2)) + 1);
                if (LatestDocument >= 21 && LatestDocument <= 50)
                {
                    flage = true;
                }
                else { flage = false; }
            }
            else
            {
                flage = true;
            }
            return flage;
        }

        [HttpPost]
        public ActionResult GenerateHeader(PDN001 pdn001)
        {
            ResponceMsgWithValues objResponseMsg = new ResponceMsgWithValues();
            string LocationInitial = string.Empty;
            try
            {

                #region Planning Din Header

                PDN001 objPDN001 = db.PDN001.Add(new PDN001
                {
                    Project = pdn001.Project,
                    IssueNo = pdn001.IssueNo,
                    Customer = pdn001.Customer,
                    CDD = pdn001.CDD,
                    Status = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    JobDescription = pdn001.JobDescription,
                    Department = pdn001.Department,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                    IssueBy = objClsLoginInfo.UserName, // It's Same as created by and date after discussion with satish
                    IssueDate = DateTime.Now,
                    Location = pdn001.Location,
                    IProject = pdn001.IProject,
                    Superior = pdn001.Superior,
                });

                #endregion
                LocationInitial = pdn001.Location.Substring(0, 1);
                bool projectExist = false;

                #region Individual Documents
                //// comment by jayraj as per obs 13450
                #region JPP
                //projectExist = db.JPP001.Where(i => i.Project == pdn001.Project).Any();
                //if (projectExist == true)
                //{
                //    goto isProjectExist;
                //}
                //JPP001 objJPP001 = db.JPP001.Add(new JPP001
                //{
                //    Project = pdn001.Project,
                //    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL01",
                //    Customer = pdn001.Customer,
                //    RevNo = 0,
                //    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                //    CDDDate = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                //    CreatedBy = objClsLoginInfo.UserName,
                //    CreatedOn = DateTime.Now,
                //});
                #endregion

                #region PMB
                projectExist = db.PMB001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                PMB001 objPMB001 = db.PMB001.Add(new PMB001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL02",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Improvment Budget
                projectExist = db.IMB001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                IMB001 objIMB001 = db.IMB001.Add(new IMB001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL03",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    Location = pdn001.Location,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Heat Tratement Charge sheet
                projectExist = db.HTC001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                HTC001 objHTC001 = db.HTC001.Add(new HTC001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL04",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    ProcessLicensor = pdn001.ProcessLicensor,
                    Product = pdn001.Product,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });
                #endregion

                #region SubContracting Plan Header
                projectExist = db.SCP001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                SCP001 objSCP001 = db.SCP001.Add(new SCP001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL05",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });
                #endregion

                #region Planning checklist
                projectExist = db.PCL001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                PCL001 objPCL001 = db.PCL001.Add(new PCL001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL06",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProductType = pdn001.Product,
                    //EquipmentNo=pdn001.EquipmentNo,
                    //ApprovedBy=pdn001.ApprovedBy,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });
                #endregion

                #region Weld KG
                projectExist = db.WKG001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                WKG001 objWKG001 = db.WKG001.Add(new WKG001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL07",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Status = clsImplementationEnum.WKGHeaderStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Machining Scope of Work
                projectExist = db.MSW001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                MSW001 objMSW001 = db.MSW001.Add(new MSW001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL08",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    ProcessLicensor = pdn001.ProcessLicensor,
                    Product = pdn001.Product,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });
                #endregion

                #region Fixture
                projectExist = db.FXR001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                FXR001 objFXR001 = db.FXR001.Add(new FXR001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL09",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    CDD = pdn001.CDD,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Reference Sketch
                projectExist = db.PLN003.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                PLN003 objPLN003 = db.PLN003.Add(new PLN003
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL10",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Locking Cleat
                projectExist = db.PLN019.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                PLN019 objPLN019 = db.PLN019.Add(new PLN019
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL11",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    // JointType = pdn001.JointType,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region RCCP

                //PLN013 objPLN013 = db.PLN013.Add(new PLN013
                //{
                //    Project = pdn001.Project,
                //    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL14",
                //    Customer = pdn001.Customer,
                //    RevNo = 0,
                //    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                //    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                //    Product = pdn001.Product,
                //    ProcessLicensor = pdn001.ProcessLicensor,
                //    ProcessPlan = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue(),
                //    CreatedBy = objClsLoginInfo.UserName,
                //    CreatedOn = DateTime.Now,
                //});

                #endregion

                #region Tank Rotator
                projectExist = db.PLN005.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                PLN005 objPLN005 = db.PLN005.Add(new PLN005
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL13",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Tank_Rotator_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });


                #endregion

                #region Positioner Plan
                projectExist = db.PLN013.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                PLN013 objPLN013 = db.PLN013.Add(new PLN013
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL14",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Stool Plan
                projectExist = db.PLN017.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                PLN017 objPLN017 = db.PLN017.Add(new PLN017
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL15",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Stool_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Section Handling
                projectExist = db.PLN009.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                PLN009 objPLN009 = db.PLN009.Add(new PLN009
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL16",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Internal Axel Movement
                projectExist = db.PLN018.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                PLN018 objPLN018 = db.PLN018.Add(new PLN018
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL17",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Internal_Axel_Movement_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Shell C/S WEP Machining / Trimming Plan
                projectExist = db.MCI001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                MCI001 objMCI001 = db.MCI001.Add(new MCI001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL18",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region RCCP Breakup For Equipment
                projectExist = db.RCC001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                RCC001 objRCC001 = db.RCC001.Add(new RCC001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL12",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Dimension Data Capturing Plan
                projectExist = db.DDC001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                DDC001 objDDC001 = db.DDC001.Add(new DDC001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL20",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Elbow Overlay Plan
                projectExist = db.ELB001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                ELB001 objELB001 = db.ELB001.Add(new ELB001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL21",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Top & Bottom Spool Trimming Plan
                projectExist = db.TBS001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                TBS001 objTBS001 = db.TBS001.Add(new TBS001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL22",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Operation Cards
                projectExist = db.OPC001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                OPC001 objOPC001 = db.OPC001.Add(new OPC001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL19",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.FixtureStatus.Draft.GetStringValue(),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Technical Process

                //int docNo;
                //var projectExists = db.TLP001.Where(x => x.Project.Trim() == pdn001.Project.Trim()).FirstOrDefault();
                //if (projectExists == null)
                //{
                //    docNo = 21;
                //}
                //else
                //{
                //    int? existingDocNo = (from doc in db.TLP001
                //                          where doc.Project == pdn001.Project
                //                          select doc.DocumentNo).FirstOrDefault();
                //    docNo = Convert.ToInt32(existingDocNo + 1);
                //}
                //TLP001 objTLP001 = db.TLP001.Add(new TLP001
                //{
                //    Project = pdn001.Project,
                //    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL" + docNo,
                //    DocumentNo = docNo,
                //    Customer = pdn001.Customer,
                //    RevNo = 0,
                //    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                //    JobDescription = pdn001.JobDescription,
                //    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                //    ProcessLicensor = pdn001.ProcessLicensor,
                //    Product = pdn001.Product,
                //    CreatedBy = objClsLoginInfo.UserName,
                //    CreatedOn = DateTime.Now,
                //});
                #endregion

                #region Equipment Plan
                projectExist = db.PLN011.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                PLN011 objPLN011 = db.PLN011.Add(new PLN011
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL91",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Equipment Jacking
                projectExist = db.PLN016.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                PLN016 objPLN016 = db.PLN016.Add(new PLN016
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL92",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Airtest Plan
                projectExist = db.PLN015.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                PLN015 objPLN015 = db.PLN015.Add(new PLN015
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL93",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Hydro Test
                projectExist = db.PLN007.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                PLN007 objPLN007 = db.PLN007.Add(new PLN007
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL94",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    ProcessPlan = clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });

                #endregion

                #region Initiate Exit Meeting
                projectExist = db.EMT001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                EMT001 objEMT001 = db.EMT001.Add(new EMT001
                {
                    Project = pdn001.Project,
                    Document = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL95",
                    Customer = pdn001.Customer,
                    RevNo = 0,
                    CDD = Helper.ToNullIfTooEarlyForDb(pdn001.CDD),
                    Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                    ProcessLicensor = pdn001.ProcessLicensor,
                    Product = pdn001.Product,
                    //ApprovedBy=pdn001.ApprovedBy,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now,
                });
                #endregion

                #region Learning Capture
                projectExist = db.LNC001.Where(i => i.Project == pdn001.Project).Any();
                if (projectExist == true)
                {
                    goto isProjectExist;
                }
                LNC001 objLNC001 = db.LNC001.Add(new LNC001
                {
                    Project = pdn001.Project,
                    DocumentNo = LocationInitial + "02_" + pdn001.Project.Trim().Substring(pdn001.Project.Trim().Length - 4) + "_PL96",
                    Customer = pdn001.Customer,
                    CDD = Convert.ToDateTime(Helper.ToNullIfTooEarlyForDb(pdn001.CDD)),
                    Product = pdn001.Product,
                    ProcessLicensor = pdn001.ProcessLicensor,
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedDate = DateTime.Now
                });
                #endregion
                #endregion

                db.SaveChanges();

                PDN002 objPDN002 = new PDN002();
                #region DIN Lines
                //// comment by jayraj as per obs 13450
                #region JPP
                //objPDN002 = db.PDN002.Add(new PDN002
                //{
                //    HeaderId = objPDN001.HeaderId,
                //    Project = objPDN001.Project,
                //    IssueNo = objPDN001.IssueNo,
                //    Description = "Job Planning & Progress Sheet & Line Sketch",
                //    DocumentNo = objJPP001.Document,
                //    TargetDate = null,
                //    RevNo = objJPP001.RevNo,
                //    RefId = objJPP001.HeaderId,
                //    Status = objJPP001.Status,
                //    Plan = clsImplementationEnum.PlanList.JPP.GetStringValue(),
                //    CreatedBy = objClsLoginInfo.UserName,
                //    CreatedOn = DateTime.Now
                //});
                #endregion

                #region PMB

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Manufacturing Sequence & Schedule for Equipment",
                    DocumentNo = objPMB001.Document,
                    TargetDate = null,
                    RevNo = objPMB001.RevNo,
                    RefId = objPMB001.HeaderId,
                    Status = objPMB001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Pre_Manufacturing.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region IMB

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Improvement Budget Plan",
                    DocumentNo = objIMB001.Document,
                    TargetDate = null,
                    RevNo = objIMB001.RevNo,
                    RefId = objIMB001.HeaderId,
                    Status = objIMB001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Improvement_Budget_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Heat Tratement Charge sheet

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Heat Treatment Charge Sheet",
                    DocumentNo = objHTC001.Document,
                    TargetDate = null,
                    RevNo = objHTC001.RevNo,
                    RefId = objHTC001.HeaderId,
                    Status = objHTC001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region SubContracting Plan Header
                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "SubContracting Plan",
                    DocumentNo = objSCP001.Document,
                    TargetDate = null,
                    RevNo = objSCP001.RevNo,
                    RefId = objSCP001.HeaderId,
                    Status = objSCP001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Sub_Contracting_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Planning checklist

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Planning Checklist",
                    DocumentNo = objPCL001.Document,
                    TargetDate = null,
                    RevNo = objPCL001.RevNo,
                    RefId = objPCL001.HeaderId,
                    Status = objPCL001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Planning_Checklist.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region WKG

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Weld Kg Calculation",
                    DocumentNo = objWKG001.Document,
                    TargetDate = null,
                    RevNo = objWKG001.RevNo,
                    RefId = objWKG001.HeaderId,
                    Status = objWKG001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Weld_KG_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Machining Scope of Work

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Machining Scope of Work",
                    DocumentNo = objMSW001.Document,
                    TargetDate = null,
                    RevNo = objMSW001.RevNo,
                    RefId = objMSW001.HeaderId,
                    Status = objMSW001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Machining_Scope_of_Work.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Fixture

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Fixture Requirement Sheet",
                    DocumentNo = objFXR001.Document,
                    TargetDate = null,
                    RevNo = objFXR001.RevNo,
                    RefId = objFXR001.HeaderId,
                    Status = objFXR001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Fixture.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Reference Sketch

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Reference Line Sketch",
                    DocumentNo = objPLN003.Document,
                    TargetDate = null,
                    RevNo = objPLN003.RevNo,
                    RefId = objPLN003.HeaderId,
                    Status = objPLN003.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Locking Cleat

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Locking Cleat /Tacking Plan",
                    DocumentNo = objPLN019.Document,
                    TargetDate = null,
                    RevNo = objPLN019.RevNo,
                    RefId = objPLN019.HeaderId,
                    Status = objPLN019.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Locking_Cleat.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region RCCP

                //objPDN002 = db.PDN002.Add(new PDN002
                //{
                //    HeaderId = objPDN001.HeaderId,
                //    Project = objPDN001.Project,
                //    IssueNo = objPDN001.IssueNo,
                //    Description = "Locking Cleat Plan",
                //    DocumentNo = objPLN019.Document,
                //    TargetDate = null,
                //    RevNo = objPLN019.RevNo,
                //    RefId = objPLN019.HeaderId,
                //    Status = objPLN019.Status,
                //    Plan = clsImplementationEnum.PlanList.Locking_Cleat.GetStringValue(),
                //    CreatedBy = objClsLoginInfo.UserName,
                //    CreatedOn = DateTime.Now
                //});

                #endregion

                #region Tank Rotator

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Tank Rotator Plan",
                    DocumentNo = objPLN005.Document,
                    TargetDate = null,
                    RevNo = objPLN005.RevNo,
                    RefId = objPLN005.HeaderId,
                    Status = objPLN005.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Tank_Rotator_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Positioner Plan

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Positioner Plan",
                    DocumentNo = objPLN013.Document,
                    TargetDate = null,
                    RevNo = objPLN013.RevNo,
                    RefId = objPLN013.HeaderId,
                    Status = objPLN013.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Stool Plan

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Stool Plan",
                    DocumentNo = objPLN017.Document,
                    TargetDate = null,
                    RevNo = objPLN017.RevNo,
                    RefId = objPLN017.HeaderId,
                    Status = objPLN017.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Stool_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Section Handling

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Section Handling Plan",
                    DocumentNo = objPLN009.Document,
                    TargetDate = null,
                    RevNo = objPLN009.RevNo,
                    RefId = objPLN009.HeaderId,
                    Status = objPLN009.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Internal Axel Movement

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Internal Axel Movement Plan",
                    DocumentNo = objPLN018.Document,
                    TargetDate = null,
                    RevNo = objPLN018.RevNo,
                    RefId = objPLN018.HeaderId,
                    Status = objPLN018.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Internal_Axel_Movement_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Shell C/S WEP Machining / Trimming Plan
                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Shell C/S WEP Machining / Trimming Plan",
                    DocumentNo = objMCI001.Document,
                    TargetDate = null,
                    RevNo = objMCI001.RevNo,
                    RefId = objMCI001.HeaderId,
                    Status = objMCI001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Machining_Instructions.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region RCCP Breakup For Equipment
                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "RCCP Breakup For Equipment",
                    DocumentNo = objRCC001.Document,
                    TargetDate = null,
                    RevNo = objRCC001.RevNo,
                    RefId = objRCC001.HeaderId,
                    Status = objRCC001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.RCCP_Breakup_For_Equipment.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Dimension Data Capturing Plan
                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Dimension Data Capturing Plan",
                    DocumentNo = objDDC001.Document,
                    TargetDate = null,
                    RevNo = objDDC001.RevNo,
                    RefId = objDDC001.HeaderId,
                    Status = objDDC001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Dimension_Data_Capturing_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Elbow Overlay Plan
                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Elbow Overlay Plan",
                    DocumentNo = objELB001.Document,
                    TargetDate = null,
                    RevNo = objELB001.RevNo,
                    RefId = objELB001.HeaderId,
                    Status = objELB001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Elbow_Overlay_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Top & Bottom Spool Trimming Plan
                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Top & Bottom Spool Trimming Plan",
                    DocumentNo = objTBS001.Document,
                    TargetDate = null,
                    RevNo = objTBS001.RevNo,
                    RefId = objTBS001.HeaderId,
                    Status = objTBS001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Top_And_Bottom_Spool_Trimming_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Operation Cards
                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Operation Cards",
                    DocumentNo = objOPC001.Document,
                    TargetDate = null,
                    RevNo = objOPC001.RevNo,
                    RefId = objOPC001.HeaderId,
                    Status = objOPC001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Operation_Cards.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region Technical Procedure 

                //objPDN002 = db.PDN002.Add(new PDN002
                //{
                //    HeaderId = objPDN001.HeaderId,
                //    Project = objPDN001.Project,
                //    IssueNo = objPDN001.IssueNo,
                //    Description = "Technical Procedure",
                //    DocumentNo = objTLP001.Document,
                //    TargetDate = null,
                //    RevNo = objTLP001.RevNo,
                //    RefId = objTLP001.HeaderId,
                //    Status = objTLP001.Status,
                //    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                //    Plan = clsImplementationEnum.PlanList.Technical_Process.GetStringValue(),
                //    CreatedBy = objClsLoginInfo.UserName,
                //    CreatedOn = DateTime.Now
                //});
                #endregion

                #region Equipment Plan

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Equipment Handling Plan",
                    DocumentNo = objPLN011.Document,
                    TargetDate = null,
                    RevNo = objPLN011.RevNo,
                    RefId = objPLN011.HeaderId,
                    Status = objPLN011.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Equipment Jacking

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Equipment Jacking Plan",
                    DocumentNo = objPLN016.Document,
                    TargetDate = null,
                    RevNo = objPLN016.RevNo,
                    RefId = objPLN016.HeaderId,
                    Status = objPLN016.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Airtest Plan

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Air Test Plan (Bolting, Air Filling, Support)",
                    DocumentNo = objPLN015.Document,
                    TargetDate = null,
                    RevNo = objPLN015.RevNo,
                    RefId = objPLN015.HeaderId,
                    Status = objPLN015.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Hydro Test

                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Hydro Test Plan (Bolting, Water Filling, Support, Chemicals)",
                    DocumentNo = objPLN007.Document,
                    TargetDate = null,
                    RevNo = objPLN007.RevNo,
                    RefId = objPLN007.HeaderId,
                    Status = objPLN007.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #region Initiate Exit Meeting
                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Job Closing Summary",
                    DocumentNo = objEMT001.Document,
                    TargetDate = null,
                    RevNo = objEMT001.RevNo,
                    RefId = objEMT001.HeaderId,
                    Status = objEMT001.Status,
                    IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Plan = clsImplementationEnum.PlanList.Initiate_Exit_Meeting.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });
                #endregion

                #region LNC
                objPDN002 = db.PDN002.Add(new PDN002
                {
                    HeaderId = objPDN001.HeaderId,
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo,
                    Description = "Learning from Project",
                    DocumentNo = objLNC001.DocumentNo,
                    TargetDate = null,
                    RefId = objLNC001.HeaderId,
                    Plan = clsImplementationEnum.PlanList.Learning_Capture.GetStringValue(),
                    CreatedBy = objClsLoginInfo.UserName,
                    CreatedOn = DateTime.Now
                });

                #endregion

                #endregion
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Planning DIN created successfully.";
                objResponseMsg.HeaderID = objPDN001.HeaderId;
                objResponseMsg.Status = objPDN001.Status;


                isProjectExist:
                if (projectExist)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Project Already Exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetJobSpecificationFormPartial(string projectCode, string location)
        {
            List<TLP001> lstTLP001 = db.TLP001.Where(x => x.Project == projectCode).OrderByDescending(x => x.Document).ToList();
            var LocationInitial = location.Substring(0, 1);
            if (lstTLP001 != null && lstTLP001.Count > 0)
            {
                string LatestDocument = lstTLP001.Select(x => x.Document).FirstOrDefault();
                ViewBag.Document = LocationInitial + "02_" + projectCode.Trim().Substring(projectCode.Trim().Length - 4) + "_PL" + (Convert.ToInt32(LatestDocument.Trim().Substring(LatestDocument.Trim().Length - 2)) + 1);
            }
            else
            {
                ViewBag.Document = LocationInitial + "02_" + projectCode.Trim().Substring(projectCode.Trim().Length - 4) + "_PL31";
            }
            return PartialView("_GetJobSpecificationFormPartial");
        }
        [HttpPost]
        public ActionResult ReleaseDin(int HeaderID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PDN001 objPDN001 = db.PDN001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPDN001 != null)
            {
                List<SCU003_Log> lstScu003_log = db.SCU003_Log.Where(x => x.Project == objPDN001.Project).ToList();
                var DraftStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue();
                var ApprovedStatus = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                var objPDN002Notification = db.PDN002.Where(i => i.Status == ApprovedStatus && i.IssueStatus == DraftStatus && i.HeaderId == objPDN001.HeaderId).ToList();

                objPDN001.Status = clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue();
                objPDN001.ModifiedBy = objClsLoginInfo.UserName;
                objPDN001.ModifiedOn = DateTime.Now;
                List<PDN002> lstPDN002 = db.PDN002.Where(x => x.HeaderId == objPDN001.HeaderId).ToList();

                var lstpdn = lstPDN002.Where(i => i.Status == "Approved" && i.IssueNo == objPDN001.IssueNo && i.IssueStatus != clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue()).ToList();

                if (lstScu003_log.Count > 1)
                {
                    bool isdoc = false;
                    string docerror = string.Empty;

                    foreach (var item in lstpdn)
                    {
                        if (lstScu003_log.Any(x => x.DoumentNo == item.DocumentNo))
                        {
                            if (!(lstScu003_log.Count(x => x.DoumentNo == item.DocumentNo) > 1))
                            {
                                if (!string.IsNullOrEmpty(docerror))
                                    docerror += item.DocumentNo + ",";
                                else
                                    docerror += item.DocumentNo;
                                isdoc = true;
                            }
                        }
                    }

                    if (isdoc)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "These Documents " + docerror + " are not to be maintained in SCurve Data";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Project Against SCurve Data not available.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                PDN001 newObjPDN001 = db.PDN001.Add(new PDN001
                {
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo + 1,
                    Customer = objPDN001.Customer,
                    CDD = objPDN001.CDD,
                    Status = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Product = objPDN001.Product,
                    ProcessLicensor = objPDN001.ProcessLicensor,
                    JobDescription = objPDN001.JobDescription,
                    Department = objPDN001.Department,
                    CreatedBy = objPDN001.CreatedBy,
                    CreatedOn = DateTime.Now,
                    IssueBy = objClsLoginInfo.UserName, // It's Same as created by and date after discussion with satish
                    IssueDate = DateTime.Now,
                    Location = objPDN001.Location,
                    Superior = objPDN001.Superior,
                    IProject = objPDN001.IProject,
                });
                lstPDN002.Where(i => i.Status == "Approved" && i.IssueNo == objPDN001.IssueNo && i.IssueStatus != clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue()).ToList().ForEach(x => { x.IssueStatus = clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue(); });
                db.SaveChanges();
                lstPDN002.ForEach(x => { x.HeaderId = newObjPDN001.HeaderId; x.IssueNo = newObjPDN001.IssueNo; });

                db.PDN002.AddRange(lstPDN002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Din successfully released.";


                #region Send Notification
                var PDN001Department = objPDN001.Department.Split(',').ToList();
                var listEmployee = getEmployeeFromPDIN(objPDN001.HeaderId, "SHOP");
                var listSubDepartmentEmp = (from p5 in db.PDN005
                                            join p4 in db.PDN004 on p5.RefId equals p4.Id
                                            join c3 in db.COM003 on p5.Employee equals c3.t_psno
                                            where p5.Active == true && p4.Active == true && PDN001Department.Contains(p4.ChildDeptCode)
                                            select new PDINEmployee { department = p4.ChildDeptCode, isActive = 1, name = p5.Employee + " - " + c3.t_name, psnum = p5.Employee }
                                          ).ToList();
                //listEmployee.Union(listSubDepartmentEmp);
                listEmployee.AddRange(listSubDepartmentEmp);
                List<GLB010> NotificationList = new List<GLB010>();
                foreach (var item in objPDN002Notification)
                {
                    if (item.Department != null && item.Department != string.Empty)
                    {
                        var listDepartment = item.Department.Split(',').ToList();
                        List<string> DepartmentEmployee = listEmployee.Where(i => listDepartment.Contains(i.department)).Select(i => i.psnum).ToList();
                        var psno = string.Join(",", DepartmentEmployee);
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.SHOP.GetStringValue(), objPDN001.Project, "", objClsLoginInfo.Location, "Planning document " + item.Project + " - " + item.Description + " of R" + item.RevNo + "   has been released.", clsImplementationEnum.NotificationType.Information.GetStringValue(), GenerateHistoryURLForNotification(item.Plan, item.RefHistoryId.Value), psno);
                    }
                }
                #endregion
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Din not available.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SaveTPDetails(TLP001 tlp001)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PDN001 objPDN001 = db.PDN001.Where(x => x.HeaderId == tlp001.HeaderId).FirstOrDefault();
                if (objPDN001 != null)
                {
                    TLP001 objTLP001 = db.TLP001.Add(new TLP001
                    {
                        Project = objPDN001.Project,
                        DocumentNo = Convert.ToInt32(tlp001.Document.Trim().Substring(tlp001.Document.Trim().Length - 2)),
                        Document = tlp001.Document.Trim(),
                        Customer = objPDN001.Customer,
                        RevNo = 0,
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        CDD = objPDN001.CDD,
                        Product = objPDN001.Product,
                        ProcessLicensor = objPDN001.ProcessLicensor,
                        JobDescription = objPDN001.JobDescription,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    db.SaveChanges();

                    PDN002 objPDN002 = db.PDN002.Add(new PDN002
                    {
                        HeaderId = objPDN001.HeaderId,
                        Project = objPDN001.Project,
                        IssueNo = objPDN001.IssueNo,
                        Description = tlp001.JobDescription,
                        DocumentNo = objTLP001.Document,
                        RevNo = 0,
                        RefId = objTLP001.HeaderId,
                        Plan = "Technical Procedure",
                        Status = clsImplementationEnum.PlanStatus.DRAFT.GetStringValue(),
                        IssueStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        IsApplicable = true
                    });
                    db.SaveChanges();

                    if (db.SCU002.Any(x => x.Project.ToLower() == objPDN001.Project.ToLower()))
                    {
                        SCU002 objSCU002 = db.SCU002.Add(new SCU002
                        {
                            Project = objPDN001.Project,
                            location = objPDN001.Location,
                            ZeroDate = Convert.ToDateTime((new clsManager()).Getzerodate(objPDN001.Project)),
                            CDD = objPDN001.CDD,
                            Phase = (objTLP001.DocumentNo > 90) ? 3 : ((objTLP001.DocumentNo > 7) ? 2 : ((objTLP001.DocumentNo > 1) ? 1 : 1)),
                            DoumentNo = objTLP001.Document,
                            DoumentDesc = tlp001.JobDescription,
                            Days = 1,
                            PlannedQty = 1,
                            ActualQty = 0,
                            HrsPerDoc = 1,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now,
                        });

                        SCU003 objSCU003 = db.SCU003.Add(new SCU003
                        {
                            Project = objPDN001.Project,
                            location = objPDN001.Location,
                            DoumentNo = objTLP001.Document,
                            ActualQtyInterval = Convert.ToDouble(GetActualQtyInterval((new clsManager()).Getzerodate(objPDN001.Project), 1)),
                            ActualQty = 0,
                            phase = objSCU002.Phase,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now,
                        });

                        SCU003_Log objSCU003_log = db.SCU003_Log.Add(new SCU003_Log
                        {
                            Project = objPDN001.Project,
                            location = objPDN001.Location,
                            DoumentNo = objTLP001.Document,
                            ActualQtyInterval = Convert.ToDouble(GetActualQtyInterval((new clsManager()).Getzerodate(objPDN001.Project), 1)),
                            ActualQty = 0,
                            phase = objSCU002.Phase,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now,
                        });
                        db.SaveChanges();
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Job Specific Document added successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Planning DIN header details not available.";
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetCustomerProjectWise(string projectCode)
        {
            ApproverModel objApproverModel = Manager.GetCustomerProjectWise(projectCode);
            string strCdd = Manager.GetContractWiseCdd(projectCode);
            if (!string.IsNullOrWhiteSpace(strCdd))
            {
                DateTime dtCdd = Convert.ToDateTime(strCdd);
                objApproverModel.Cdd = dtCdd.Date.ToShortDateString();
            }
            return Json(objApproverModel, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetProcessLicensorProjectWise(string projectCode)
        {
            string processlicensor = string.Empty;
            var objprocesslicensor = (from c5 in db.COM005
                                      join c8 in db.COM008 on c5.t_cono equals c8.t_cono
                                      where c5.t_sprj == projectCode
                                      select c8.t_epcp
                                      );
            if (objprocesslicensor != null)
            {
                processlicensor = objprocesslicensor.FirstOrDefault();
            }
            return Json(processlicensor, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public bool checkProjectExist(string projectcode)
        {
            bool Flag = false;
            if (!string.IsNullOrWhiteSpace(projectcode))
            {
                PDN001 objPDN001 = db.PDN001.Where(x => x.Project == projectcode).FirstOrDefault();
                if (objPDN001 != null)
                {
                    Flag = true;
                }
            }
            return Flag;
        }

        public JsonResult GetDepartments(string search, string param)
        {
            try
            {
                if (search == null)
                {
                    search = "";
                }
                if (param == null || param == string.Empty)
                {
                    param = "";
                    //return Json(null, JsonRequestBehavior.AllowGet);
                }
                var items = db.FN_GET_LOCATIONWISE_DEPARTMENT_PDIN(param, search, -1).Select(x => new { id = x.t_dimx, text = x.t_desc, }).ToList();
                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetSuperiors(string search, string param)
        {
            try
            {
                if (search != null && search != "")
                {
                    int HeaderId = Convert.ToInt32(param);
                    var objPDN001 = db.PDN001.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                    string[] Superiors = new string[0];
                    if (objPDN001 != null && objPDN001.Superior != null)
                    {
                        Superiors = objPDN001.Superior.Split(',');
                    }
                    var items = db.COM003
                                .Where(i => ((i.t_psno.Contains(search) || i.t_name.Contains(search)) && i.t_actv == 1) || (Superiors.Contains(i.t_psno)))
                                .Select(x => new { id = x.t_psno, text = x.t_psno + "-" + x.t_name, }).ToList();
                    return Json(items, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var items = db.COM003.Where(i => i.t_actv == 1).Select(x => new { id = x.t_psno, text = x.t_psno + "-" + x.t_name, }).ToList();
                    return Json(items, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetIdenticalProjects(string search, string param)
        {
            try
            {
                var objPDN001 = db.PDN001.Where(i => i.Project == param).FirstOrDefault();
                var PSON = objClsLoginInfo.UserName;
                string[] IProjects = new string[0];
                if (objPDN001 != null)
                {
                    IProjects = objPDN001.IProject.Split(',');
                }
                if (search != null && search != "")
                {
                    var items = db.SP_PDIN_GET_IPROJECT_FOR_PDIN(param, PSON)
                                .Where(i => i.Description.Contains(search) && i.Project != param)
                                .Select(x => new { id = x.Project, text = x.Description }).ToList();

                    var items1 = db.COM001.Where(i => IProjects.Contains(i.t_cprj)).Select(x => new { id = x.t_cprj, text = x.t_cprj + "-" + x.t_dsca, }).ToList();
                    items.AddRange(items);

                    return Json(items, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var items = db.SP_PDIN_GET_IPROJECT_FOR_PDIN(param, PSON)
                                .Select(x => new { id = x.Project, text = x.Description }).ToList();
                    return Json(items, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetIdenticalProjectsList(string search, string param, string SelectedProject)
        {
            try
            {
                var objPDN001 = db.PDN001.Where(i => i.Project == param).FirstOrDefault();
                var PSON = objClsLoginInfo.UserName;
                string[] IProjects = new string[0];
                List<string> SelectedProj = new List<string>();

                SelectedProj = SelectedProject.Split(',').ToList();
                SelectedProj.Add(param);

                if (objPDN001 != null && objPDN001.IProject != null)
                {
                    IProjects = objPDN001.IProject.Split(',');
                }
                if (search != null && search != "")
                {
                    var items = db.SP_PDIN_GET_IPROJECT_FOR_PDIN(param, PSON)
                                .Where(i => i.Description.Contains(search) && !SelectedProj.Contains(i.Project))
                                .Select(x => new AutoCompleteModel { Value = x.Project, Text = x.Description }).ToList();

                    //var items1 = db.COM001.Where(i => IProjects.Contains(i.t_cprj)).Select(x => new { id = x.t_cprj, text = x.t_cprj + "-" + x.t_dsca, }).ToList();
                    //items.AddRange(items);

                    return Json(items, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var items = db.SP_PDIN_GET_IPROJECT_FOR_PDIN(param, PSON)
                                .Where(i => !SelectedProj.Contains(i.Project))
                                .Select(x => new AutoCompleteModel { Value = x.Project, Text = x.Description }).ToList();
                    return Json(items, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetDepartmentsValue(int headerId)
        {
            try
            {
                string strDepartments = (db.PDN001.Where(x => x.HeaderId == headerId).Select(x => x.Department).FirstOrDefault());
                string[] arrayDepartments = null;

                if (!string.IsNullOrWhiteSpace(strDepartments))
                {
                    arrayDepartments = strDepartments.Split(',');
                }
                return Json(arrayDepartments, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetSuperiorValue(int headerId)
        {
            try
            {
                string strSuperior = (db.PDN001.Where(x => x.HeaderId == headerId).Select(x => x.Superior).FirstOrDefault());
                string[] arraySuperior = null;

                if (!string.IsNullOrWhiteSpace(strSuperior))
                {
                    arraySuperior = strSuperior.Split(',');
                    var items = db.COM003.Where(i => arraySuperior.Contains(i.t_psno)).Select(x => new { id = x.t_psno, text = x.t_psno + "-" + x.t_name, }).ToList();
                    return Json(items, JsonRequestBehavior.AllowGet);
                }
                return Json(arraySuperior, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult GetIProjectValue(int headerId)
        {
            try
            {
                string strIProject = (db.PDN001.Where(x => x.HeaderId == headerId).Select(x => x.IProject).FirstOrDefault());
                string[] arrayIProject = null;

                if (!string.IsNullOrWhiteSpace(strIProject))
                {
                    arrayIProject = strIProject.Split(',');
                    var items = db.COM001.Where(i => arrayIProject.Contains(i.t_cprj)).Select(x => new { id = x.t_cprj, text = x.t_cprj + "-" + x.t_dsca, }).ToList();
                    return Json(items, JsonRequestBehavior.AllowGet);
                }
                return Json(arrayIProject, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        public string GetDepartmentDropdown(int LineId, string Status, List<CategoryData> departments)
        {
            string rtn = string.Empty;
            try
            {
                rtn += "<select name='ddlDepartments'" + LineId.ToString() + "'' id='ddlDepartments'" + LineId.ToString() + "'' multiple='multiple' style=' width: 100 % ' class='form-control' " + (Status.ToLower() == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue().ToLower() ? "disable" : "") + " >";
                foreach (var item in departments)
                {
                    rtn += "<option value='" + item.Code + "'>" + item.CategoryDescription + "</option>";
                }
                rtn += "</select>";
            }
            catch (Exception)
            {

                throw;
            }
            return rtn;
        }

        public string GetTragetDateTextBox(int LineId, bool Disabled, string Date, string ColumnName)
        {
            string rtn = string.Empty;
            try
            {
                //rtn += "<div id='datetime1' class='input-group date date-picker' data-date-format='dd/mm/yyyy' data-date-start-date='+0d'>";
                rtn += "<input type='text' class='form-control' id='txtTargetDate' value='" + Date + "' colname='" + ColumnName + "' " + (Disabled ? "disabled " : "") + " ></input>";
                //rtn += "<span class='input-group-btn'>";
                //rtn += "<button class='btn default' id='btncal1' type='button'>";
                //rtn += "<i class='fa fa-calendar'></i>";
                //rtn += "</button></span></div>";

            }
            catch (Exception)
            {

                throw;
            }
            return rtn;
        }


        [HttpPost]
        public ActionResult UpdateDepartment(string id, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                db.SP_PLN_UPDATE_CHECKLIST_TABLE_COLUMN("PDN002", "LineId", id, "Department", columnValue);
                int lineId = Int32.Parse(id);
                var proj = db.PDN002.FirstOrDefault(q => q.LineId == lineId).Project;
                var docn = db.PDN002.FirstOrDefault(q => q.LineId == lineId).DocumentNo;
                db.Database.ExecuteSqlCommand("update PDN002 set Department = @Dept where Project = @Proj and DocumentNo = @DocNo", new SqlParameter("@Dept", columnValue), new SqlParameter("@Proj", proj), new SqlParameter("@DocNo", docn));
                db.SaveChanges();
                /*     FirstOrDefault(q => q.LineId == lineId).Project;
                 var docn = db.PDN002.FirstOrDefault(q => q.LineId == lineId).DocumentNo;
                 var pdnList = db.PDN002.Where(q => (q.Project == proj && q.DocumentNo == docn)).ToList();
                 foreach(var item in pdnList)
                 {
                     item.Department = columnValue;
                     db.Entry(item).State = EntityState.Modified;
                 }
                 db.SaveChanges();*/
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdatePDINLineDepartment(string project, string documentNo, string department, bool status)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<string> DepartmentList = new List<string>();
            try
            {
                var objPDN002 = db.PDN002.Where(i => i.Project == project && i.DocumentNo == documentNo).ToList();
                var obj2Department = objPDN002.Where(r => r.DocumentNo == documentNo && r.Project == project).FirstOrDefault().Department;
                if (obj2Department != null)
                {
                    DepartmentList = obj2Department.Split(',').ToList();
                    DepartmentList.RemoveAll(str => String.IsNullOrEmpty(str));
                    if (status)
                    {
                        if (!DepartmentList.Contains(department))
                        {
                            DepartmentList.Add(department);
                        }
                    }
                    else
                    {
                        DepartmentList.Remove(department);
                    }
                }
                else
                {
                    if (status)
                    {
                        if (!DepartmentList.Contains(department))
                        {
                            DepartmentList.Add(department);
                        }
                    }
                    else
                    {
                        DepartmentList.Remove(department);
                    }
                }

                var finaldepartment = string.Join(",", DepartmentList);
                objPDN002.ForEach(i => { i.Department = finaldepartment; });

                if (status)
                {
                    PDN003 objPDN003 = db.PDN003.Add(new PDN003
                    {
                        Project = project,
                        DocumentNo = documentNo,
                        Department = department,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdatePDINDepartment(string project, string department, string HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<string> OldDepartmentList = new List<string>();
            List<string> NewDepartmentList = new List<string>();
            List<string> NewAddDepartmentList = new List<string>();
            List<string> RemoveDepartmentList = new List<string>();
            int ActualHeaderId = 0;
            try
            {
                int.TryParse(HeaderId, out ActualHeaderId);
                var objPDN001 = db.PDN001.Where(r => r.Project == project && r.HeaderId == ActualHeaderId).ToList();
                if (objPDN001 != null && objPDN001.Count > 0)
                {
                    var OldDepartment = objPDN001.FirstOrDefault().Department;
                    if (department == string.Empty)
                    {

                    }
                    else
                    {
                        if (OldDepartment != null)
                        {
                            OldDepartmentList = OldDepartment.Split(',').ToList();
                        }
                        NewDepartmentList = department.Split(',').ToList();

                        RemoveDepartmentList = OldDepartmentList.Where(p => !NewDepartmentList.Any(p2 => p2 == p)).ToList();
                        NewAddDepartmentList = NewDepartmentList.Where(p => !OldDepartmentList.Any(p2 => p2 == p)).ToList();

                        if (RemoveDepartmentList.Count > 0)
                        {
                            var objPDN002 = db.PDN002.Where(i => i.Project == project && i.HeaderId == ActualHeaderId).ToList();
                            foreach (var item in objPDN002)
                            {
                                if (!string.IsNullOrEmpty(item.Department))
                                {
                                    OldDepartmentList = item.Department.Split(',').ToList();
                                    foreach (var dep in RemoveDepartmentList)
                                    {
                                        if (OldDepartmentList.Contains(dep))
                                        {
                                            OldDepartmentList.Remove(dep);
                                        }
                                    }

                                    var finaldepartment = string.Join(",", OldDepartmentList);
                                    item.Department = finaldepartment;
                                }
                            }
                            db.SaveChanges();
                        }
                        if (NewAddDepartmentList.Count > 0)
                        {
                            foreach (var item in NewAddDepartmentList)
                            {
                                if (!db.COM002.Any(i => i.t_dimx == item && i.t_dtyp == 3))
                                {
                                    if (!db.PDN005.Any(i => i.ChildDeptCode == item && i.Active == true))
                                    {
                                        objResponseMsg.Key = false;
                                        objResponseMsg.Value = "Department is inactive.";
                                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                                    }
                                }
                            }

                        }
                    }
                    objPDN001.ForEach(i => { i.Department = department; });
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdatePDINSuperior(string project, string superior, string HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            int ActualHeaderId = 0;
            try
            {
                int.TryParse(HeaderId, out ActualHeaderId);
                var objPDN001 = db.PDN001.Where(r => r.Project == project).ToList();
                if (objPDN001 != null && objPDN001.Count > 0)
                {
                    objPDN001.ForEach(i => { i.Superior = superior; });
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdatePDINIProject(string project, string IProject, string HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            int ActualHeaderId = 0;
            try
            {
                int.TryParse(HeaderId, out ActualHeaderId);
                //var objPDN001 = db.PDN001.Where(r => r.Project == project).ToList();
                var objPDN001 = db.PDN001.Where(r => r.HeaderId == ActualHeaderId).ToList();
                if (objPDN001 != null && objPDN001.Count > 0)
                {
                    objPDN001.ForEach(i => { i.IProject = IProject; });
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult UpdateIsApplicable(string lineid, bool status)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<string> DepartmentList = new List<string>();
            int LineId = 0;
            try
            {
                LineId = Convert.ToInt32(lineid);
                var objPDN002 = db.PDN002.Where(i => i.LineId == LineId).FirstOrDefault();

                var objPDN002All = db.PDN002.Where(i => i.Project == objPDN002.Project && i.DocumentNo == objPDN002.DocumentNo).ToList();
                if (objPDN002All != null)
                {
                    objPDN002All.ForEach(i =>
                    {
                        i.IsApplicable = status;
                    });
                    //objPDN002.IsApplicable = status;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }

                var objscu002 = db.SCU002.Where(x => x.Project == objPDN002.Project && x.DoumentNo == objPDN002.DocumentNo).FirstOrDefault();
                if (objscu002 != null)
                {
                    if (status)
                    {
                        string scudocno = string.Empty;
                        if (objscu002.DoumentNo != null)
                        {
                            scudocno = objscu002.DoumentNo.Trim().Substring(objscu002.DoumentNo.Length - 4);
                            string phase = objscu002.DoumentNo.Trim().Substring(objscu002.DoumentNo.Length - 2);
                            int phaseno = Convert.ToInt32(phase);
                            phaseno = (phaseno > 90) ? 3 : ((phaseno > 7) ? 2 : ((phaseno > 1) ? 1 : 1));

                            var lstscu001 = db.SCU001.Select(x => new { phase = x.Phase, days = x.Days, hrsperdoc = x.HrsPerDoc, plnQty = x.PlannedQty, actQty = x.ActualQty, DoumentNo = x.DoumentNo.Trim().Substring(x.DoumentNo.Length - 4) }).ToList();
                            if (lstscu001 != null)
                            {
                                var objscu001 = lstscu001.Where(x => x.DoumentNo.ToLower() == scudocno.ToLower() && x.phase == phaseno).FirstOrDefault();
                                if (objscu001 != null)
                                {
                                    db.SCU003.RemoveRange(db.SCU003.Where(x => x.Project == objscu002.Project && x.DoumentNo == objscu002.DoumentNo).ToList());
                                    db.SCU003_Log.RemoveRange(db.SCU003_Log.Where(x => x.Project == objscu002.Project && x.DoumentNo == objscu002.DoumentNo).ToList());

                                    objscu002.PlannedQty = objscu001.plnQty;
                                    objscu002.ActualQty = 0;
                                    objscu002.Phase = objscu001.phase;
                                    objscu002.Days = objscu001.days;
                                    objscu002.HrsPerDoc = objscu001.hrsperdoc;
                                    objscu002.EditedOn = DateTime.Now;
                                    objscu002.EditedBy = objClsLoginInfo.UserName;

                                    SCU003 objSCU003 = new SCU003();
                                    SCU003_Log objSCU003_log = new SCU003_Log();

                                    string zerodate = (new clsManager()).Getzerodate(objscu002.Project);

                                    objSCU003.Project = objscu002.Project;
                                    objSCU003.location = objscu002.location;
                                    objSCU003.DoumentNo = objscu002.DoumentNo;
                                    objSCU003.ActualQtyInterval = Convert.ToDouble((new clsManager()).GetActualQtyInterval(zerodate, objscu002.Days));
                                    objSCU003.ActualQty = 0;
                                    objSCU003.phase = objscu002.Phase;
                                    objSCU003.CreatedOn = DateTime.Now;
                                    objSCU003.CreatedBy = objClsLoginInfo.UserName;

                                    objSCU003_log.Project = objscu002.Project;
                                    objSCU003_log.location = objscu002.location;
                                    objSCU003_log.DoumentNo = objscu002.DoumentNo;
                                    objSCU003_log.ActualQtyInterval = Convert.ToDouble((new clsManager()).GetActualQtyInterval(zerodate, objscu002.Days));
                                    objSCU003_log.ActualQty = 0;
                                    objSCU003_log.phase = objscu002.Phase;
                                    objSCU003_log.CreatedOn = DateTime.Now;
                                    objSCU003_log.CreatedBy = objClsLoginInfo.UserName;

                                    db.SCU003.Add(objSCU003);
                                    db.SCU003_Log.Add(objSCU003_log);
                                }
                            }
                        }
                    }
                    else
                    {
                        //uncheck
                        objscu002.PlannedQty = 0;
                        objscu002.ActualQty = 0;

                        db.SCU003.RemoveRange(db.SCU003.Where(x => x.Project == objscu002.Project && x.DoumentNo == objscu002.DoumentNo).ToList());
                        db.SCU003_Log.RemoveRange(db.SCU003_Log.Where(x => x.Project == objscu002.Project && x.DoumentNo == objscu002.DoumentNo).ToList());

                        SCU003 objSCU003 = new SCU003();
                        SCU003_Log objSCU003_log = new SCU003_Log();

                        string zerodate = (new clsManager()).Getzerodate(objscu002.Project);

                        objSCU003.Project = objscu002.Project;
                        objSCU003.location = objscu002.location;
                        objSCU003.DoumentNo = objscu002.DoumentNo;
                        objSCU003.ActualQtyInterval = Convert.ToDouble((new clsManager()).GetActualQtyInterval(zerodate, objscu002.Days));
                        objSCU003.ActualQty = 0;
                        objSCU003.phase = objscu002.Phase;
                        objSCU003.CreatedOn = DateTime.Now;
                        objSCU003.CreatedBy = objClsLoginInfo.UserName;

                        objSCU003_log.Project = objscu002.Project;
                        objSCU003_log.location = objscu002.location;
                        objSCU003_log.DoumentNo = objscu002.DoumentNo;
                        objSCU003_log.ActualQtyInterval = Convert.ToDouble((new clsManager()).GetActualQtyInterval(zerodate, objscu002.Days));
                        objSCU003_log.ActualQty = 0;
                        objSCU003_log.phase = objscu002.Phase;
                        objSCU003_log.CreatedOn = DateTime.Now;
                        objSCU003_log.CreatedBy = objClsLoginInfo.UserName;

                        db.SCU003.Add(objSCU003);
                        db.SCU003_Log.Add(objSCU003_log);
                    }
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public string GetPDINLinesHTMLNew(string project, PDINRole Role, bool? ForPrint, bool? IsMobile)
        {
            string rtn = string.Empty;
            int srno = 1;
            List<MaxIssueRevModel> listMaxIssueRevModel = new List<MaxIssueRevModel>();
            bool IsChecked = false;
            bool IsDisabled = false;
            bool isapprover = false;
            List<IssueNoModel> IssueNoList = null;
            IssueNoModel CarryForwardIssueNo = null;
            string ProjectName = string.Empty;
            string CustomerName = string.Empty;
            int NoOfIssueDisplayOnPrint = 10;
            int MinIssueNo = 0;
            List<PDN005> ChildsDept = null;
            try
            {
                var IsIsMobileDevice = !IsMobile.HasValue ? false : IsMobile.Value;
                var RELEASEDStatus = clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue().ToLower();
                var DraftStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue().ToLower();
                var ApprovedStatus = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                var projectDetails = db.COM001.Where(x => x.t_cprj == project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ProjectName = projectDetails.t_cprj + " - " + projectDetails.t_dsca;

                var objPDN001 = db.PDN001.Where(i => i.Project == project).OrderByDescending(i => i.IssueNo).FirstOrDefault();
                var objCOM006 = db.COM006.Where(i => i.t_bpid == objPDN001.Customer).FirstOrDefault();
                CustomerName = objCOM006.t_bpid + " - " + objCOM006.t_nama;
                if (Role == PDINRole.Approver)
                {
                    isapprover = true;
                }

                //Get Current PDIN Issue No For Project
                var CurrentPDINNo = db.PDN001.Where(i => i.Project == project).Max(i => i.IssueNo);
                List<PDINDocumentRevNoEnt> listPDINDocumentRevNoEnt = new List<PDINDocumentRevNoEnt>();
                List<PDINDocumentListEnt> documentList = new List<PDINDocumentListEnt>();
                /*
                 Only applicable documents to be made visible to shop user. Other documents which are not linked with department of the user not to be display.
                 Observation ID : 15013
                */
                if (Role == PDINRole.Shop)
                {
                    ChildsDept = db.PDN005.Where(i => i.Employee == objClsLoginInfo.UserName).ToList();

                    var Superior = objPDN001.Superior;
                    var cu_DIN = db.PDN002.Where(i => i.Project == project && i.HeaderId == objPDN001.HeaderId).ToList().OrderBy(i => i.DocumentNo).ToList();
                    string[] arraySuperior = new string[0];
                    bool IsSuperior = false;
                    if (Superior != null)
                    {
                        arraySuperior = Superior.Split(',');
                        if (arraySuperior.Contains(objClsLoginInfo.UserName))
                        {
                            IsSuperior = true;
                        }
                    }
                    if (IsSuperior)
                    {
                        foreach (var item in cu_DIN)
                        {
                            documentList.Add(new PDINDocumentListEnt
                            {
                                DocumentNo = item.DocumentNo,
                                Description = item.Description,
                                RefId = item.RefId,
                                Plan = item.Plan
                            });
                        }
                    }
                    else
                    {
                        //var ChildsDept = db.PDN005.Where(i => i.Employee == objClsLoginInfo.UserName).ToList();
                        //foreach (var item in cu_DIN)
                        //{
                        //    if (item.Department != null)
                        //    {
                        //        var Dept = item.Department.Split(',');
                        //        if (Dept.Contains(objClsLoginInfo.Department) || ChildsDept.Any(i => Dept.Contains(i.ChildDeptCode)))
                        //        {
                        //            documentList.Add(new PDINDocumentListEnt
                        //            {
                        //                DocumentNo = item.DocumentNo,
                        //                Description = item.Description,
                        //                RefId = item.RefId,
                        //                Plan = item.Plan
                        //            });
                        //        }
                        //    }
                        //}
                        foreach (var item in cu_DIN)
                        {
                            documentList.Add(new PDINDocumentListEnt
                            {
                                DocumentNo = item.DocumentNo,
                                Description = item.Description,
                                RefId = item.RefId,
                                Plan = item.Plan
                            });
                        }
                    }
                    listPDINDocumentRevNoEnt = (from t in db.PDN002
                                                group t by new { t.Project, t.DocumentNo, t.Status }
                                                into grp
                                                where grp.Key.Project == project && grp.Key.Status == ApprovedStatus
                                                select new PDINDocumentRevNoEnt
                                                {
                                                    Project = grp.Key.Project,
                                                    DocumentNo = grp.Key.DocumentNo,
                                                    RevNo = grp.Max(t => t.RevNo)
                                                }
                             ).ToList();
                }
                else
                {
                    //Get All Distinct Document list For Project
                    documentList = db.PDN002.Where(i => i.Project == project && i.HeaderId == objPDN001.HeaderId).Select(i => new PDINDocumentListEnt { DocumentNo = i.DocumentNo, Description = i.Description, RefId = i.RefId, Plan = i.Plan }).ToList().Distinct().OrderBy(i => i.DocumentNo).ToList();
                }
                //Get Current Issue No PDIN Department
                var Department = db.PDN001.Where(i => i.Project == project && i.IssueNo == CurrentPDINNo).FirstOrDefault().Department;
                string[] arrayDepartment = new string[0];
                if (!string.IsNullOrEmpty(Department))
                {
                    arrayDepartment = Department.Split(',');
                }

                //Department Code and Discription
                //var DepartmentList = db.COM002.Where(i => arrayDepartment.Contains(i.t_dimx)).Select(i => new { Code = i.t_dimx, Name = i.t_desc, Init = i.t_init }).Distinct().ToList();
                var DepartmentList = db.FN_GET_LOCATIONWISE_DEPARTMENT_PDIN(objPDN001.Location, "", -1).Where(i => arrayDepartment.Contains(i.t_dimx)).Select(i => new { Code = i.t_dimx, Name = i.t_desc, Init = i.t_init, Active = i.Active }).Distinct().ToList();

                //Display Only Released Issue So we get only released Issue No

                if (ForPrint == true)
                {
                    IssueNoList = (from p1 in db.PDN001
                                   join c3 in db.COM003 on p1.ModifiedBy equals c3.t_psno
                                   where p1.Project == project && p1.Status.ToLower() == RELEASEDStatus //&& c3.t_actv == 1
                                   orderby p1.IssueNo descending
                                   select new IssueNoModel { IssueNo = p1.IssueNo, IssueBy = p1.ModifiedBy, IssueDate = p1.ModifiedOn, t_init = c3.t_init }
                                       ).Take(NoOfIssueDisplayOnPrint).OrderBy(i => i.IssueNo).ToList();
                    if (IssueNoList.Count > 0)
                    {
                        MinIssueNo = IssueNoList.Min(i => i.IssueNo);
                    }
                    //if (IssueNoList.Count > NoOfIssueDisplayOnPrint)
                    //{
                    //    CarryForwardIssueNo = IssueNoList.FirstOrDefault();
                    //    IssueNoList.Remove(CarryForwardIssueNo);
                    //}
                }
                else
                {
                    IssueNoList = (from p1 in db.PDN001
                                   join c3 in db.COM003 on p1.ModifiedBy equals c3.t_psno
                                   where p1.Project == project && p1.Status.ToLower() == RELEASEDStatus //&& c3.t_actv == 1
                                   select new IssueNoModel { IssueNo = p1.IssueNo, IssueBy = p1.ModifiedBy, IssueDate = p1.ModifiedOn, t_init = c3.t_init }
                                       ).ToList();
                }
                //Get All IssueNo and related Details For other action.
                //We use this objects for diplay issueno, revno ,historyid
                var objPDN002 = (from p1 in db.PDN001
                                 join p2 in db.PDN002 on p1.HeaderId equals p2.HeaderId
                                 join c3 in db.COM003 on p1.IssueBy equals c3.t_psno
                                 where p1.Project.Trim() == project.Trim() //&& c3.t_actv == 1
                                 select new { p1.Project, p1.IssueNo, p1.IssueBy, p1.IssueDate, p2.DocumentNo, p2.Description, p2.RevNo, c3.t_init, p2.Department, p2.RefHistoryId, p2.Plan, p1.Status, DocumentStatus = p2.Status, RefId = p2.RefId }
                                  ).ToList();

                //Get Current IssueNo Details like LineId,IsApplicable,Status,DocumentNo and RevNo
                var objPDN002CurentIssueDtl = (from p1 in db.PDN001
                                               join p2 in db.PDN002 on p1.HeaderId equals p2.HeaderId
                                               where p1.Project.Trim() == project.Trim() && p1.IssueNo == p2.IssueNo
                                               && p1.IssueNo == CurrentPDINNo
                                               select new { p2.LineId, p2.IsApplicable, p2.Status, p2.DocumentNo, p2.RevNo, p2.TargetDate, p2.IssueStatus, p2.Department }
                                  ).ToList();

                //var objPDN003list = db.PDN003.Where(i => i.Project == project).ToList();

                //Get Max CreatedOn Date of Department For Document
                var objPDN003list = (from t in db.PDN003
                                     group t by new { t.Project, t.DocumentNo, t.Department }
                             into grp
                                     where grp.Key.Project == project
                                     select new
                                     {
                                         grp.Key.DocumentNo,
                                         grp.Key.Department,
                                         CreatedOn = grp.Max(t => t.CreatedOn)
                                     }

                             ).ToList();



                rtn += "<table id='tblDINLinesDetails' class='table table-bordered'><thead>";
                if (!IsIsMobileDevice)
                {
                    #region Project & Customer
                    if (ForPrint == true)
                    {
                        if (Role == PDINRole.Shop)
                        {
                            rtn += "<tr><th class='pdinheader' rowspan='2' colspan='3' style='vertical-align: middle;'>Planning Document Index/IssueNo</th><th class='pdinheader' colspan='" + (ForPrint == true ? "3" : "2") + "'>Project</th><th class='pdinheader' colspan='" + (IssueNoList.Count + DepartmentList.Count).ToString() + "'> " + ProjectName + " </th></tr>";
                            rtn += "<tr><th class='pdinheader' colspan='" + (ForPrint == true ? "3" : "2") + "'>Customer</th><th class='pdinheader' colspan='" + (IssueNoList.Count + DepartmentList.Count).ToString() + "'> " + CustomerName + " </th></tr>";
                        }
                        else
                        {
                            rtn += "<tr><th class='pdinheader' rowspan='2' colspan='4' style='vertical-align: middle;'>Planning Document Index/IssueNo</th><th class='pdinheader' colspan='" + (ForPrint == true ? "3" : "2") + "'>Project</th><th class='pdinheader' colspan='" + (IssueNoList.Count + DepartmentList.Count).ToString() + "'> " + ProjectName + " </th></tr>";
                            rtn += "<tr><th class='pdinheader' colspan='" + (ForPrint == true ? "3" : "2") + "'>Customer</th><th class='pdinheader' colspan='" + (IssueNoList.Count + DepartmentList.Count).ToString() + "'> " + CustomerName + " </th></tr>";
                        }
                    }
                    else
                    {
                        if (Role == PDINRole.Shop)
                        {
                            rtn += "<tr><th class='pdinheader'  colspan='3' style='vertical-align: middle;'>Planning Document Index/IssueNo</th><th class='pdinheader' colspan='" + (ForPrint == true ? "3" : "2") + "'></th><th class='pdinheader' colspan='" + (IssueNoList.Count + DepartmentList.Count).ToString() + "'>  </th></tr>";
                        }
                        else
                        {
                            rtn += "<tr><th class='pdinheader'  colspan='4' style='vertical-align: middle;'>Planning Document Index/IssueNo</th><th class='pdinheader' colspan='" + (ForPrint == true ? "3" : "2") + "'></th><th class='pdinheader' colspan='" + (IssueNoList.Count + DepartmentList.Count).ToString() + "'>  </th></tr>";
                        }
                    }
                    #endregion

                    #region IssueBy
                    //Create Issue By Row         
                    if (Role == PDINRole.Shop)
                    {
                        rtn += "<tr><th class='pdinheader' rowspan='3' colspan='3'><img alt='L&T Logo' src='" + WebsiteURL + "/Images/Logo.jpg' style='height:90px;' /></th><th class='pdinheader' colspan='" + (ForPrint == true ? "3" : "2") + "'> Issue By </th>";
                    }
                    else
                    {
                        rtn += "<tr><th class='pdinheader' rowspan='3' colspan='4'><img alt='L&T Logo' src='" + WebsiteURL + "/Images/Logo.jpg' style='height:90px;' /></th><th class='pdinheader' colspan='" + (ForPrint == true ? "3" : "2") + "'> Issue By </th>";
                    }
                    //Loop through IssueNoList and create table column header to display Issue By
                    foreach (var issue in IssueNoList)
                    {
                        //rtn += "<th class='pdinheader' style=\"text-align:center;\">" + objPDN002.Where(r => r.IssueNo == issue.IssueNo).FirstOrDefault().t_init + "</th>";
                        rtn += "<th class='pdinheader' style=\"text-align:center;\">" + issue.t_init + "</th>";
                    }
                    rtn += "<th class='pdinheader' colspan='" + DepartmentList.Count + "'></th>";
                    rtn += "</tr>";
                    //End Issue By Row
                    #endregion

                    #region IssueDate
                    //Create Issue Date Row
                    rtn += "<tr><th class='pdinheader' colspan='" + (ForPrint == true ? "3" : "2") + "'> Issue Date </th>";
                    //Loop through IssueNoList and create table column header to display Issue Date
                    foreach (var issue in IssueNoList)
                    {
                        //rtn += "<th class='pdinheader' style=\"text-align:center;\">" + objPDN002.Where(r => r.IssueNo == issue.IssueNo).FirstOrDefault().IssueDate.Value.ToString("dd-MMM") + "</th>";
                        rtn += "<th class='pdinheader' style=\"text-align:center;\">" + issue.IssueDate.Value.ToString("dd-MMM") + "</th>";
                    }
                    rtn += "<th class='pdinheader center' colspan='" + DepartmentList.Count + "'><b>Departments</b></th>";
                    rtn += "</tr>";
                    //End Issue Date Row
                    #endregion


                    #region Released DIN
                    //Create Issue No and DepartmentName Row
                    rtn += "<tr><th class='pdinheader' colspan='" + (ForPrint == true ? "3" : "2") + "'> Released DIN </th>";
                    //Loop through IssueNoList and create table column header to display Issue No
                    foreach (var issue in IssueNoList)
                    {
                        rtn += "<th class='pdinheader' colspan='1' style=\"text-align:center;\">" + issue.IssueNo.ToString() + "</th>";
                    }
                    //Loop through DepartmentList and create table column header to display Department Name
                    foreach (var depart in DepartmentList)
                    {
                        if (depart.Active.Value)
                        {
                            rtn += "<th class='pdinheader' colspan='1' style=\"text-align:center;\">" + (string.IsNullOrEmpty(depart.Init) ? depart.Name : depart.Init) + "</th>";
                        }
                        else
                        {
                            rtn += "<th class='pdinheader' colspan='1' style=\"text-align:center;color:red;\">" + (string.IsNullOrEmpty(depart.Init) ? depart.Name : depart.Init) + "</th>";
                        }
                    }
                    rtn += "</tr>";
                    //End Issue No and DepartmentName Row
                    #endregion                
                }
                #region Header
                //Create Header Row
                rtn += "<tr><th class='srno" + (!IsIsMobileDevice ? "" : " all") + "'>#</th><th class='descriptions" + (!IsIsMobileDevice ? "" : " all") + "'> Document Name </th><th class='docno" + (!IsIsMobileDevice ? "" : " all") + "'> Document No. </th>";
                if (Role != PDINRole.Shop)
                {
                    rtn += "<th class='docno" + (!IsIsMobileDevice ? "" : " all") + "'>Status</th>";
                }
                rtn += "<th class=''>Doc Applicable?</th><th class=''>Target Date</th>";

                if (ForPrint == true)
                {
                    rtn += "<th class=''>C.F</th>";
                }
                //foreach (var item in IssueNoList)
                //{
                //    rtn += "<th></th>";
                //}
                foreach (var issue in IssueNoList)
                {
                    rtn += "<td class=''>" + (!IsIsMobileDevice ? "" : issue.IssueNo.ToString()) + "</td>";
                }
                foreach (var depart in DepartmentList)
                {
                    rtn += "<td class=''>" + (!IsIsMobileDevice ? "" : (string.IsNullOrEmpty(depart.Init) ? depart.Name : depart.Init)) + "</td>";
                }

                rtn += "</tr>";
                #endregion

                rtn += "</thead>";

                //Start table tbody section
                rtn += "<tbody>";

                //Check JPP Exist in Document List. If not Exist then display Blank row for JPP
                var JPPDescription = PlanList.JPP.GetStringValue();
                //var JPPExist = documentList.Where(i => i.Description == JPPDescription).FirstOrDefault();
                var JPPExist = objPDN001.PDN002.Where(i => i.Description == JPPDescription).FirstOrDefault();
                if (JPPExist == null && db.JPP001.Any(i => i.Project == project))
                {
                    var objjppoo1 = db.JPP001.Where(x => x.Project == project).FirstOrDefault();
                    if (objjppoo1 != null)
                    {

                        Manager.AddPDN002(objjppoo1.HeaderId, objjppoo1.Status, objjppoo1.RevNo, objjppoo1.Project, objjppoo1.Document, PlanList.JPP.GetStringValue(), clsImplementationEnum.PlanList.JPP.GetStringValue());
                        //calling this method again because Jpp added in PDN
                        rtn = GetPDINLinesHTMLNew(project, Role, ForPrint, IsMobile);
                        return rtn;
                    }
                }
                if (JPPExist == null && Role != PDINRole.Shop)
                {
                    rtn += "<tr>";
                    rtn += "<td class='srno' style=\"text-align:center;\">" + srno.ToString() + "</td>";//SrNo

                    rtn += "<td class='descriptions'>" + (!IsIsMobileDevice ? "<nobr>" : "") + "Job Planning &Progress Sheet & Line Sketch" + (!IsIsMobileDevice ? "</nobr>" : "") + "</td>";//Description
                    rtn += "<td class='docno'>" + (!IsIsMobileDevice ? "<nobr></nobr>" : "") + "</td>"; ;//Docno 

                    if (Role != PDINRole.Shop)
                    {
                        rtn += "<td class='descriptions'></td>";//Status
                    }
                    rtn += "<td class='center' ><b>" + CreateIsApplicableCheckBox(false, true, "0") + "</b></td>";//DocApplicable
                    rtn += "<td class='center' style=\"text-align:center;\"></td>";//Target Date
                    if (ForPrint == true)
                    {
                        rtn += "<td class=''></td>";
                    }
                    foreach (var issue in IssueNoList)
                    {
                        rtn += "<td class=''></td>";
                    }
                    foreach (var issue in DepartmentList)
                    {
                        rtn += "<td class=''></td>";
                    }
                    rtn += "</tr>";
                    srno++;
                }
                //loop all document list for generate document details row
                foreach (var item in documentList)
                {
                    IsDisabled = false;
                    //If login user is Approver and Shop user then disabled IsApplicable Checkbox
                    if (Role == PDINRole.Approver || Role == PDINRole.Shop)
                    {
                        IsDisabled = true;
                    }

                    var objLastApprovedRevDtl = listPDINDocumentRevNoEnt.Where(i => i.DocumentNo == item.DocumentNo).FirstOrDefault();
                    //Get Current Issue Details For Document from objPDN002CurentIssueDtl list
                    var objLine = objPDN002CurentIssueDtl.Where(i => i.DocumentNo == item.DocumentNo).FirstOrDefault();
                    //Get IsApplicable is check or unchecked in current issueNo
                    //If IsApplicable is unchecked then disabled all department checkbox and Document Link                
                    bool Applicable = ((objLine == null || objLine.IsApplicable == null) ? true : Convert.ToBoolean(objLine.IsApplicable));
                    if (Role == PDINRole.Shop && !Applicable)
                    {
                        continue;
                    }
                    bool IsHighlight = false;
                    if (Role == PDINRole.Maintainer && objLine?.IssueStatus != null && objLine?.Status == clsImplementationEnum.CommonStatus.Approved.GetStringValue() && objLine?.IssueStatus.ToLower() == DraftStatus)
                    {
                        IsHighlight = true;
                    }
                    rtn += "<tr " + (IsHighlight ? " style='color: green;font-weight: bold;'" : "") + " >";
                    rtn += "<td class='srno' style=\"text-align:center;\">" + srno.ToString() + "</td>";

                    rtn += "<td class='descriptions'>" + (!IsIsMobileDevice ? "<nobr>" : "") + item.Description + (!IsIsMobileDevice ? "</nobr>" : "") + "</td>";
                    rtn += "<td class='docno'>" + (!IsIsMobileDevice ? "<nobr>" : "");
                    //If Login user is shop user then no need to link for DocumentNo
                    if (Role == PDINRole.Shop)
                    {
                        var arrayDepartmenttemp = new string[0];
                        if (!string.IsNullOrEmpty(objLine.Department))
                            arrayDepartmenttemp = objLine.Department.Split(',');

                        //if (objLastApprovedRevDtl != null && arrayDepartmenttemp.Contains(objClsLoginInfo.Department) && DepartmentList.Any(w => arrayDepartmenttemp.Contains(w.Code)))
                        //if (objLastApprovedRevDtl != null && (arrayDepartmenttemp.Contains(objClsLoginInfo.Department) || ChildsDept.Any(i => arrayDepartmenttemp.Contains(i.ChildDeptCode))))
                        if ((objLastApprovedRevDtl != null && objClsLoginInfo.ListRoles.Contains(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue())) || (objLastApprovedRevDtl != null && (arrayDepartmenttemp.Contains(objClsLoginInfo.Department) || ChildsDept.Any(i => arrayDepartmenttemp.Contains(i.ChildDeptCode)))))
                        {
                            var refHistoryId = objPDN002.Where(i => i.RevNo == objLastApprovedRevDtl.RevNo && i.DocumentNo == item.DocumentNo.ToString() && i.DocumentStatus == ApprovedStatus).FirstOrDefault().RefHistoryId;
                            var RefId = objPDN002.Where(i => i.RevNo == objLastApprovedRevDtl.RevNo && i.DocumentNo == item.DocumentNo.ToString() && i.DocumentStatus == ApprovedStatus).FirstOrDefault().RefId;
                            rtn += "<a onclick=\"ShopUserAction('" + item.Plan + "'," + refHistoryId + "," + objLastApprovedRevDtl.RevNo + ",'" + item.DocumentNo.ToString() + "'," + RefId + ")\">" + item.DocumentNo.ToString() + "</a>";// + " - R" + objLine?.RevNo.ToString();                       
                        }
                        else
                        {
                            rtn += item.DocumentNo.ToString();
                        }
                    }
                    else
                    {
                        //If Function called from Print PDIN then no need to display link. Display only text
                        if (ForPrint == true)
                        {
                            rtn += item.DocumentNo.ToString() + " - R" + objLine?.RevNo.ToString();
                        }
                        else
                        {
                            //If not shop user and Document is Applicable then set link for Document No other wise display only Document No
                            string DocumentwithRev = item.DocumentNo + " - R" + objLine?.RevNo;
                            if (item.Plan == clsImplementationEnum.PlanList.Learning_Capture.GetStringValue())
                                DocumentwithRev = item.DocumentNo;
                            rtn += (Applicable ? Manager.GenerateLink(item.Plan, DocumentwithRev, Convert.ToInt32(item.RefId), isapprover) : item.DocumentNo);
                        }
                    }
                    rtn += (!IsIsMobileDevice ? "</nobr>" : "") + "</td>";
                    if (Role != PDINRole.Shop)
                    {
                        if (objLine?.IssueStatus == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue() && objLine?.Status.ToLower() == ApprovedStatus.ToLower())
                        {
                            rtn += "<td class='descriptions'>" + objLine?.IssueStatus + "</td>";
                        }
                        else
                        { rtn += "<td class='descriptions'>" + objLine?.Status + "</td>"; }
                    }
                    //Create IsApplicable Checkbox.If Maintainer user login then enabled checkbox otherwise disabled
                    rtn += "<td class='center' ><b>" + CreateIsApplicableCheckBox(Applicable, IsDisabled, objLine?.LineId.ToString()) + "</b></td>";
                    rtn += "<td class='center' style=\"text-align:center;\">" + (objLine?.TargetDate != null ? Convert.ToDateTime(objLine?.TargetDate).ToString("dd/MM/yyyy") : string.Empty) + "</td>";
                    if (ForPrint == true)
                    {
                        CarryForwardIssueNo = (from p1 in db.PDN001
                                               join p2 in db.PDN002 on p1.HeaderId equals p2.HeaderId
                                               join c3 in db.COM003 on p1.IssueBy equals c3.t_psno
                                               where p1.Project == project
                                                    && p2.IssueStatus.ToLower() == RELEASEDStatus
                                                    && p2.IssueNo < MinIssueNo
                                                    && p2.DocumentNo == item.DocumentNo //&& c3.t_actv == 1
                                               orderby p1.IssueNo descending
                                               select new IssueNoModel { IssueNo = p1.IssueNo, IssueBy = p1.IssueBy, IssueDate = p1.IssueDate, t_init = c3.t_init }
                                      ).OrderByDescending(i => i.IssueNo).Take(1).FirstOrDefault();
                        if (CarryForwardIssueNo == null)
                        {
                            rtn += "<td class=''></td>";
                        }
                        else
                        {
                            var carryForward = objPDN002.Where(i => i.IssueNo == CarryForwardIssueNo.IssueNo && i.DocumentNo == item.DocumentNo).FirstOrDefault();
                            if (carryForward != null && carryForward.RevNo != null)
                            {
                                ////add item in listMaxIssueRevModel 
                                //listMaxIssueRevModel.Add(new MaxIssueRevModel { DocumentNo = carryForward.DocumentNo, IssueNo = carryForward.IssueNo, MaxRevNo = (carryForward.RevNo == null ? 0 : carryForward.RevNo) });
                                listMaxIssueRevModel.Add(new MaxIssueRevModel { DocumentNo = carryForward.DocumentNo, IssueNo = carryForward.IssueNo, MaxRevNo = (carryForward.RevNo == null ? 0 : carryForward.RevNo) });
                                rtn += "<td class=''>R" + carryForward.RevNo + "</td>";
                            }
                            else
                            {
                                rtn += "<td class=''></td>";
                            }
                        }
                    }

                    #region IssueNo Logic
                    //Loop through IssueNo list and create column for issueNo
                    foreach (var issue in IssueNoList)
                    {
                        //Here we maintain one list for RevNo & IssueNo for Document which is add in listMaxIssueRevModel
                        //We check whether there is any revNo find in listMaxIssueRevModel for document.
                        //This list is used for display arrow or revno.
                        var currentRevNo = listMaxIssueRevModel.Where(i => i.DocumentNo == item.DocumentNo).OrderByDescending(i => i.MaxRevNo).FirstOrDefault();

                        //Get All Document Details For Document and IssueNo
                        var obj2 = objPDN002.Where(r => r.IssueNo == issue.IssueNo && r.DocumentNo == item.DocumentNo && r.Project == project).FirstOrDefault();

                        //If There is not any record for document in listMaxIssueRevModel then display revisionNo
                        if (currentRevNo == null)
                        {
                            //check obj2 is not null. if is null then display blank cell otherwise display revno base on other logic 
                            if (obj2 != null)
                            {
                                //If obj2.Status is null then display blank cell other wise check other condition
                                if (obj2.Status != null)
                                {
                                    //If Status is released and ref history id is grether than 0 then add that revision in listMaxIssueRevModel and generate history link base on other condition
                                    if (obj2.Status.ToLower() == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue().ToLower() && obj2.RefHistoryId > 0 &&
                                        obj2.DocumentStatus == (Role == PDINRole.Shop ? clsImplementationEnum.PlanStatus.Approved.GetStringValue() : obj2.DocumentStatus))
                                    {
                                        //add item in listMaxIssueRevModel 
                                        listMaxIssueRevModel.Add(new MaxIssueRevModel { DocumentNo = obj2.DocumentNo, IssueNo = obj2.IssueNo, MaxRevNo = (obj2.RevNo == null ? 0 : obj2.RevNo) });

                                        //if login user is shop then execute if part otherwise else part
                                        if (Role == PDINRole.Shop)
                                        {
                                            //check whether document is assign to user department or not and also check revNo is Current IssueNo revno.
                                            //If Both condition match then display history link other wise display only revNo.
                                            arrayDepartment = new string[0];
                                            if (!string.IsNullOrEmpty(obj2.Department))
                                            {
                                                //Get Assign department for particular document
                                                arrayDepartment = obj2.Department.Split(',');
                                            }

                                            IsChecked = arrayDepartment.Contains(objClsLoginInfo.Department);
                                            //if (IsChecked && obj2.RevNo == objLine.RevNo)
                                            if (IsChecked && (objLastApprovedRevDtl != null && obj2.RevNo == objLastApprovedRevDtl.RevNo))
                                            {
                                                ////If Function called from Print PDIN then no need to display link. Display only text
                                                //if (ForPrint == true)
                                                //{
                                                //    //Display only Revision No
                                                //    rtn += "<td class='center revno'><b>R" + obj2.RevNo.ToString() + "</b></td>";
                                                //}
                                                //else
                                                //{
                                                //    //Create History link for document
                                                //    rtn += "<td class='center revno'><b>" + GenerateHistoryLink(obj2.Plan, Convert.ToInt32(obj2.RefHistoryId), "R" + obj2.RevNo.ToString()) + "</b></td>";
                                                //}

                                                rtn += "<td class='center revno'><b>R" + obj2.RevNo.ToString() + "</b></td>";
                                            }
                                            else
                                            {
                                                //Display only Revision No
                                                rtn += "<td class='center revno'><b>R" + obj2.RevNo.ToString() + "</b></td>";
                                            }
                                        }
                                        else
                                        {
                                            if (ForPrint == true)
                                            {
                                                //Display only Revision No
                                                rtn += "<td class='center revno'><b>R" + obj2.RevNo.ToString() + "</b></td>";
                                            }
                                            else
                                            {
                                                //Create History link for document
                                                rtn += "<td class='center revno' ><b>" + GenerateHistoryLink(obj2.Plan, Convert.ToInt32(obj2.RefHistoryId), "R" + obj2.RevNo.ToString()) + "</b></td>";
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //Display blank cell
                                        rtn += "<td class='center' ><b></b></td>";
                                    }
                                }
                                else
                                {
                                    //Display blank cell
                                    rtn += "<td class='center' ><b></b></td>";
                                }
                            }
                            else
                            {
                                //Display blank cell
                                rtn += "<td class='center' ><b></b></td>";
                            }
                        }
                        //If we find any item in listMaxIssueRevModel then execute this else part
                        else
                        {
                            //check obj2 is not null. if is null then display blank cell otherwise display revno base on other logic 
                            if (obj2 != null)
                            {
                                //if listMaxIssueRevModel currentRevNo is same as loop issueno revision then diplay arrow other wise diplay RevisionNo
                                if (currentRevNo.MaxRevNo == (obj2.RevNo == null ? 0 : obj2.RevNo))
                                {
                                    //If RefHistoryId is grether than 0 then display arrow otherwise display blank cell
                                    if (obj2.RefHistoryId > 0)
                                    {
                                        //Display right arrow for continous revision means revision not change in next issue
                                        rtn += "<td class='center'> <i style='font-weight: 100; font-size: 10px;' class='fa fa-arrow-right' aria-hidden='true'></i> </td>";
                                    }
                                    else
                                    {
                                        //Display blank cell
                                        rtn += "<td class='center' ><b></b></td>";
                                    }
                                }
                                else
                                {
                                    //If obj2.Status is null then display blank cell other wise check other condition
                                    if (obj2.Status != null)
                                    {
                                        //If Status is released then add that revision in listMaxIssueRevModel
                                        if (obj2.Status.ToLower() == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue().ToLower())
                                        {

                                            //if login user is shop then execute if part otherwise else part
                                            if (Role == PDINRole.Shop)
                                            {
                                                if (obj2.DocumentStatus == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                                                {
                                                    //add item in listMaxIssueRevModel 
                                                    listMaxIssueRevModel.Add(new MaxIssueRevModel { DocumentNo = obj2.DocumentNo, IssueNo = obj2.IssueNo, MaxRevNo = (obj2.RevNo == null ? 0 : obj2.RevNo) });
                                                }
                                                //check whether document is assign to user department or not and also check revNo is Current IssueNo revno.
                                                //If Both condition match then display history link other wise display only revNo.

                                                arrayDepartment = new string[0];
                                                if (!string.IsNullOrEmpty(obj2.Department))
                                                {
                                                    arrayDepartment = obj2.Department.Split(',');
                                                }

                                                IsChecked = arrayDepartment.Contains(objClsLoginInfo.Department);
                                                //if (IsChecked && obj2.RevNo == objLine.RevNo)
                                                if (IsChecked && (objLastApprovedRevDtl != null && obj2.RevNo == objLastApprovedRevDtl.RevNo))
                                                {
                                                    if (ForPrint == true)
                                                    {
                                                        //Display only Revision No
                                                        rtn += "<td class='center revno'><b>R" + obj2.RevNo.ToString() + "</b></td>";
                                                    }
                                                    else
                                                    {
                                                        if (currentRevNo.MaxRevNo < obj2.RevNo && obj2.DocumentStatus != clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                                                        {
                                                            rtn += "<td class='center'> <i style='font-weight: 100; font-size: 10px;' class='fa fa-arrow-right' aria-hidden='true'></i> </td>";
                                                        }
                                                        else
                                                        {
                                                            //if (obj2.DocumentStatus == clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                                                            //{
                                                            //    //Create History link for document
                                                            //    rtn += "<td class='center revno'><b>" + GenerateHistoryLink(obj2.Plan, Convert.ToInt32(obj2.RefHistoryId), "R" + obj2.RevNo.ToString()) + "</b></td>";
                                                            //}
                                                            //else
                                                            //{
                                                            //    rtn += "<td class='center revno'><b>R" + obj2.RevNo.ToString() + "</b></td>";
                                                            //}
                                                            rtn += "<td class='center revno'><b>R" + obj2.RevNo.ToString() + "</b></td>";
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    if ((objLastApprovedRevDtl != null && objLastApprovedRevDtl.RevNo < obj2.RevNo) && obj2.DocumentStatus != clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                                                    {
                                                        rtn += "<td class='center'> <i style='font-weight: 100; font-size: 10px;' class='fa fa-arrow-right' aria-hidden='true'></i> </td>";
                                                    }
                                                    else if ((currentRevNo != null && currentRevNo.MaxRevNo < obj2.RevNo) && obj2.DocumentStatus != clsImplementationEnum.PlanStatus.Approved.GetStringValue())
                                                    {
                                                        rtn += "<td class='center'> <i style='font-weight: 100; font-size: 10px;' class='fa fa-arrow-right' aria-hidden='true'></i> </td>";
                                                    }
                                                    else
                                                    {
                                                        //Display only Revision No
                                                        rtn += "<td class='center revno'><b>R" + obj2.RevNo.ToString() + "</b></td>";
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                //add item in listMaxIssueRevModel 
                                                listMaxIssueRevModel.Add(new MaxIssueRevModel { DocumentNo = obj2.DocumentNo, IssueNo = obj2.IssueNo, MaxRevNo = (obj2.RevNo == null ? 0 : obj2.RevNo) });

                                                if (ForPrint == true)
                                                {
                                                    //Display only Revision No
                                                    rtn += "<td class='center revno'><b>R" + obj2.RevNo.ToString() + "</b></td>";
                                                }
                                                else
                                                {
                                                    //Create History link for document
                                                    rtn += "<td class='center revno'><b>" + GenerateHistoryLink(obj2.Plan, Convert.ToInt32(obj2.RefHistoryId), "R" + obj2.RevNo.ToString()) + "</b></td>";
                                                }
                                            }
                                        }
                                        else
                                        {
                                            //Display blank cell
                                            rtn += "<td class='center' ><b></b></td>";
                                        }
                                    }
                                    else
                                    {
                                        //Display blank cell
                                        rtn += "<td class='center' ><b></b></td>";
                                    }
                                }
                            }
                            else
                            {
                                //Display blank cell
                                rtn += "<td class='center' ><b></b></td>";
                            }
                        }

                    }

                    #endregion

                    #region Department Logic
                    //Loop through Department list and create column for Department
                    foreach (var depart in DepartmentList)
                    {
                        IsChecked = false;
                        //Get Department strings from PDN002 for document and project
                        //var obj2Department = objPDN002.Where(r => r.DocumentNo == item.DocumentNo && r.Project == project).FirstOrDefault().Department;
                        var obj2Department = objLine?.Department;

                        //Get Last Date for Department assign For that Document
                        var lastDeptDate = objPDN003list.Where(i => i.Department == depart.Code && i.DocumentNo == item.DocumentNo).Max(i => i.CreatedOn);

                        //if obj2Department is not null then execute this part
                        if (obj2Department != null)
                        {
                            arrayDepartment = new string[0];
                            if (!string.IsNullOrEmpty(obj2Department))
                            {
                                arrayDepartment = obj2Department.Split(',');
                            }
                            //if department assign to document then default check display
                            IsChecked = arrayDepartment.Contains(depart.Code);

                            //if login user is Maintainer and Document is not applicable then department checkbox is disabled
                            if (Role == PDINRole.Maintainer && !Applicable)
                            {
                                IsDisabled = true;
                            }
                            if (!depart.Active.Value)
                            {
                                IsDisabled = true;
                            }
                        }
                        else
                        {
                            if (!depart.Active.Value)
                            {
                                IsDisabled = true;
                            }
                        }
                        int headerid = 0;
                        headerid = Convert.ToInt32(objPDN001.HeaderId);
                        var ObjPDN002DeptDate = db.PDN002.Where(m => m.Project == project && m.DocumentNo == item.DocumentNo && m.IssueStatus == clsImplementationEnum.PlanningDinStatus.RELEASED.ToString() && m.HeaderId == headerid).FirstOrDefault();
                        if (ObjPDN002DeptDate != null)
                        {
                            if (lastDeptDate != null)
                            {
                                if (lastDeptDate < objPDN001.IssueDate)
                                {
                                    lastDeptDate = objPDN001.IssueDate;
                                }
                            }
                        }
                        //create department checkbox and it's column
                        rtn += "<td class='center' ><b>" + CreateDepartmentCheckBox(IsChecked, IsDisabled, item.DocumentNo, project, depart.Code, lastDeptDate) + "</b></td>";
                    }
                    #endregion

                    rtn += "</tr>";
                    srno++;
                }

                rtn += "</tbody></table>";

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return rtn;
        }

        private string CreateDepartmentCheckBox(bool IsChecked, bool IsDisabled, string DocumentNo, string Project, string Department, DateTime? Date)
        {
            string rtnchecjbox = "";
            try
            {
                string DepartmentTitle = string.Empty;
                if (Date != null && Date != DateTime.MinValue)
                {
                    DepartmentTitle = Convert.ToDateTime(Date).ToString("dd/MM/yyyy");
                }
                if (IsChecked == false)
                {
                    DepartmentTitle = string.Empty;
                }
                rtnchecjbox = "<input type='checkbox' title='" + DepartmentTitle + "' name='chkPDINDepartment' data-dcno='" + DocumentNo + "' data-prjno='" + Project + "' data-dptno='" + Department + "' id='chkPDINDepartment' onchange='UpdatePDINDepartment(this)' ";
                if (IsChecked == true)
                {
                    rtnchecjbox += " checked='checked' ";

                }
                if (IsDisabled)
                {
                    rtnchecjbox += " disabled= 'disabled' ";
                }
                rtnchecjbox += "/>";
            }
            catch (Exception)
            {

                throw;
            }
            return rtnchecjbox;
        }

        private string CreateIsApplicableCheckBox(bool? IsChecked, bool IsDisabled, string LineId)
        {
            string rtnchecjbox = "";
            try
            {
                if (IsChecked == null)
                {
                    IsChecked = true;
                }
                rtnchecjbox = "<input type='checkbox' class='PDINIsApplicable' name='chkPDINIsApplicable" + LineId + "' data-lineid='" + LineId + "' id='chkPDINIsApplicable" + LineId + "' onchange='UpdateIsApplicable(this)' ";
                if (IsChecked == true)
                {
                    rtnchecjbox += " checked='checked' ";

                }
                if (IsDisabled)
                {
                    rtnchecjbox += " disabled='disabled' ";
                }
                rtnchecjbox += "/>";

            }
            catch (Exception)
            {
                throw;
            }
            return rtnchecjbox;
        }

        public string GenerateHistoryLink(string PlanName, int RefHistoryId, string RevNo)
        {
            string strLink = string.Empty;

            switch (PlanName)
            {
                case "Job Planning & Progress Sheet & Line Sketch":
                    strLink = "<a href='" + WebsiteURL + "/JPP/JPPHeader/JPPHisotyDetail?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Pre Manufacturing":
                    strLink = "<a href='" + WebsiteURL + "/PMB/Header/getHistoryDetail?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Improvement Budget Plan":
                    strLink = "<a href='" + WebsiteURL + "/IMB/Maintain/HistoryView?id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Heat Treatment Charge Sheet":
                    strLink = "<a href='" + WebsiteURL + "/HTC/MaintainHTC/getHistoryDetail?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Sub Contracting Plan":
                    strLink = "<a href='" + WebsiteURL + "/SCP/MaintainSCP/SCPHistoryDetails?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Planning Checklist":
                    strLink = "<a href='" + WebsiteURL + "/PLC/MaintainHistoryCheckList/ViewLogDetails?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Weld KG Plan":
                    strLink = "<a href='" + WebsiteURL + "/WKG/MaintainWeldKG/ViewHistory?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Machining Scope of Work":
                    strLink = "<a href='" + WebsiteURL + "/MSW/Maintain/getHistoryDetail?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Fixture":
                    strLink = "<a href='" + WebsiteURL + "/PLN/MaintainFXR/ViewHistory?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Reference Sketch":
                    strLink = "<a href='" + WebsiteURL + "/PLN/MaintainReferenceSketch/ViewHistory?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Locking Cleat":
                    strLink = "<a href='" + WebsiteURL + "/PLN/MaintainLockingCleat/ViewLogDetails?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Tank Rotator Plan":
                    strLink = "<a href='" + WebsiteURL + "/PLN/MaintainTankRotator/ViewHistory?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Positioner Plan":
                    strLink = "<a href='" + WebsiteURL + "/PLN/MaintainPositionerPlan/ViewHistory?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Stool Plan":
                    strLink = "<a href='" + WebsiteURL + "/PLN/MaintainStoolPlan/ViewHistory?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Section Handling Plan":
                    strLink = "<a href='" + WebsiteURL + "/PLN/MaintainSectionHandling/ViewHistory?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Internal Axel Movement Plan":
                    strLink = "<a href='" + WebsiteURL + "/PLN/MaintaineInternalAxelMovement/ViewHistory?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Technical Procedure":
                    strLink = "<a href='" + WebsiteURL + "/TP/Header/getHistoryDetail?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Equipment Handling Plan":
                    strLink = "<a href='" + WebsiteURL + "/PLN/MaintainEquipment/ViewHistory?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Equipment Jacking Plan":
                    strLink = "<a href='" + WebsiteURL + "/PLN/MaintainEquipmentJacking/ViewHistory?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Air Test Plan":
                    strLink = "<a href='" + WebsiteURL + "/PLN/MaintainAirtestPlan/ViewHistory?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Hydro Test Plan":
                    strLink = "<a href='" + WebsiteURL + "/PLN/MaintainHydroTest/ViewHistory?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Initiate Exit Meeting":
                    strLink = "<a href='" + WebsiteURL + "/EMT/MaintainEMT/EMTHistoryDetail?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Learning Capture":
                    strLink = "<a href='/#" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Shell C/S WEP Machining / Trimming Plan":
                    strLink = "<a href='" + WebsiteURL + "/MCI/History/ViewLogDetail?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "RCCP Breakup For Equipment":
                    strLink = "<a href='" + WebsiteURL + "/RCC/History/ViewLogDetail?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Dimension Data Capturing Plan":
                    strLink = "<a href='" + WebsiteURL + "/DDC/History/ViewLogDetail?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Elbow Overlay Plan":
                    strLink = "<a href='" + WebsiteURL + "/ELB/History/ViewLogDetail?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Top & Bottom Spool Trimming Plan":
                    strLink = "<a href='" + WebsiteURL + "/TBS/History/ViewLogDetail?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                case "Operation Cards":
                    strLink = "<a href='" + WebsiteURL + "/OPC/History/ViewLogDetail?Id=" + RefHistoryId + "'>" + RevNo + "</a>";
                    break;
                default:
                    strLink = string.Empty;
                    break;
            }


            return strLink;
        }
        public string GenerateHistoryURLForNotification(string PlanName, int RefHistoryId)
        {
            string strLink = string.Empty;

            switch (PlanName)
            {
                case "Job Planning & Progress Sheet & Line Sketch":
                    strLink = "/JPP/JPPHeader/JPPHisotyDetail?Id=" + RefHistoryId;
                    break;
                case "Pre Manufacturing":
                    strLink = "/PMB/Header/getHistoryDetail?Id=" + RefHistoryId;
                    break;
                case "Improvement Budget Plan":
                    strLink = "/IMB/Maintain/HistoryView?id=" + RefHistoryId;
                    break;
                case "Heat Treatment Charge Sheet":
                    strLink = "/HTC/MaintainHTC/getHistoryDetail?Id=" + RefHistoryId;
                    break;
                case "Sub Contracting Plan":
                    strLink = "/SCP/MaintainSCP/SCPHistoryDetails?Id=" + RefHistoryId;
                    break;
                case "Planning Checklist":
                    strLink = "";
                    break;
                case "Weld KG Plan":
                    strLink = "/WKG/MaintainWeldKG/ViewHistory?Id=" + RefHistoryId;
                    break;
                case "Machining Scope of Work":
                    strLink = "/MSW/Maintain/getHistoryDetail?Id=" + RefHistoryId;
                    break;
                case "Fixture":
                    strLink = "/PLN/MaintainFXR/ViewHistory?Id=" + RefHistoryId;
                    break;
                case "Reference Sketch":
                    strLink = "/PLN/MaintainReferenceSketch/ViewHistory?Id=" + RefHistoryId;
                    break;
                case "Locking Cleat":
                    strLink = "/PLN/MaintainLockingCleat/ViewLogDetails?Id=" + RefHistoryId;
                    break;
                case "Tank Rotator Plan":
                    strLink = "/PLN/MaintainTankRotator/ViewHistory?Id=" + RefHistoryId;
                    break;
                case "Positioner Plan":
                    strLink = "/PLN/MaintainPositionerPlan/ViewHistory?Id=" + RefHistoryId;
                    break;
                case "Stool Plan":
                    strLink = "/PLN/MaintainStoolPlan/ViewHistory?Id=" + RefHistoryId;
                    break;
                case "Section Handling Plan":
                    strLink = "/PLN/MaintainSectionHandling/ViewHistory?Id=" + RefHistoryId;
                    break;
                case "Internal Axel Movement Plan":
                    strLink = "/PLN/MaintaineInternalAxelMovement/ViewHistory?Id=" + RefHistoryId;
                    break;
                case "Technical Procedure":
                    strLink = "/TP/Header/getHistoryDetail?Id=" + RefHistoryId;
                    break;
                case "Equipment Handling Plan":
                    strLink = "/PLN/MaintainEquipment/ViewHistory?Id=" + RefHistoryId;
                    break;
                case "Equipment Jacking Plan":
                    strLink = "/PLN/MaintainEquipmentJacking/ViewHistory?Id=" + RefHistoryId;
                    break;
                case "Air Test Plan":
                    strLink = "/PLN/MaintainAirtestPlan/ViewHistory?Id=" + RefHistoryId;
                    break;
                case "Hydro Test Plan":
                    strLink = "/PLN/MaintainHydroTest/ViewHistory?Id=" + RefHistoryId;
                    break;
                case "Initiate Exit Meeting":
                    strLink = "/EMT/MaintainEMT/EMTHistoryDetail?Id=" + RefHistoryId;
                    break;
                case "Learning Capture":
                    strLink = "";
                    break;
                default:
                    strLink = string.Empty;
                    break;
            }


            return strLink;
        }
        [SessionExpireFilter]
        public ActionResult PrintPDIN(string Project, string Role)
        {
            ViewBag.Project = Project;
            ViewBag.Role = Role;
            return View();
        }
        public List<PDINEmployee> getEmployeeFromPDIN(int HeaderId, string Role)
        {
            List<PDINEmployee> lstEmployee = new List<PDINEmployee>();
            var objPDN001 = db.PDN001.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
            if (objPDN001 != null && objPDN001.Department != null && objPDN001.Department != string.Empty)
            {
                List<string> listDepartment = objPDN001.Department.Split(',').ToList();

                lstEmployee = (from ath1 in db.ATH001
                               join com3 in db.COM003 on ath1.Employee equals com3.t_psno
                               join ath4 in db.ATH004 on ath1.Role equals ath4.Id
                               where (ath1.Location.Equals(objClsLoginInfo.Location)) && ath4.Role.ToString().ToLower().Equals(Role.ToLower()) && listDepartment.Contains(com3.t_depc) && com3.t_actv == 1
                               select
                               new PDINEmployee { isActive = com3.t_actv, psnum = com3.t_psno, name = com3.t_psno + " - " + com3.t_name, department = com3.t_depc }).Distinct().ToList();

            }

            return lstEmployee;
        }

        public int GetActualQtyInterval(string zerodate, double days)
        {
            int ActualQtyInterval = 0;
            try
            {
                if (!string.IsNullOrEmpty(zerodate))
                {
                    string dur1 = FN_GET_ACTUALQTY(zerodate, zerodate, days);

                    string zerodate2 = string.Format("{0:MMM dd yyyy hh:mmtt}", DateTime.Now);
                    string dur2 = FN_GET_ACTUALQTY(zerodate, zerodate2, 0);

                    int duration1 = Convert.ToInt32(dur1);
                    int duration2 = Convert.ToInt32(dur2);

                    if (duration1 > duration2)
                        ActualQtyInterval = duration1 > 0 ? duration1 : 0;
                    else
                        ActualQtyInterval = duration2 > 0 ? duration2 : 0;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return ActualQtyInterval;
        }

        public string FN_GET_ACTUALQTY(string zerodate1, string zerodate2, double days)
        {
            int day = Convert.ToInt32(days);
            return db.Database.SqlQuery<string>("SELECT dbo.FN_GET_ACTUALQTY('" + zerodate1 + "','" + zerodate2 + "','" + day + "')").FirstOrDefault();
        }
        #region Merge & Demerge Project

        [HttpPost]
        public ActionResult GetMergeProjectPartial(int HeaderId)
        {
            var objPDN001 = db.PDN001.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
            var MainProject = db.COM001.Where(i => i.t_cprj == objPDN001.Project).FirstOrDefault();
            ViewBag.MainProject = MainProject != null ? (MainProject.t_cprj + "-" + MainProject.t_dsca) : "";
            if (objPDN001.IProject != null)
            {
                var lstIProjects = objPDN001.IProject.Split(',');
                ViewBag.IProject = (from c1 in db.COM001
                                    where lstIProjects.Contains(c1.t_cprj)
                                    select new { Project = c1.t_cprj + "-" + c1.t_dsca, ProjectNo = c1.t_cprj }
                                   ).OrderByDescending(i => i.ProjectNo).ToList();
            }
            return PartialView("_MergeProjectPartial");
        }

        [HttpPost]
        public ActionResult MergePDINIProject(string HeaderId, string IProjectMerge, string IProjectDemerge)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            int ActualHeaderId = 0;
            try
            {
                int.TryParse(HeaderId, out ActualHeaderId);
                //var objPDN001 = db.PDN001.Where(r => r.Project == project).ToList();
                var objPDN001 = db.PDN001.Where(r => r.HeaderId == ActualHeaderId).FirstOrDefault();
                if (objPDN001.Status == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue() && !objClsLoginInfo.ListRoles.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.PLNG2.GetStringValue()))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.ReleasedDINProjectMerge.ToString();
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (objPDN001 != null && objPDN001.IProject != null)
                {
                    var existIProject = objPDN001.IProject.Split(',').ToArray();
                    var DemergeIProject = IProjectDemerge.Split(',').ToArray();
                    var newIProjects = existIProject.Where(i => !DemergeIProject.Contains(i)).ToArray();
                    var newIProjectMerge = IProjectMerge.Split(',').ToArray();
                    var listIProjectMerge = newIProjectMerge.Union(newIProjects);

                    objPDN001.IProject = GetFinalIProject(objPDN001.IProject, IProjectMerge, IProjectDemerge);

                    objPDN001.ModifiedBy = objClsLoginInfo.UserName;
                    objPDN001.ModifiedOn = DateTime.Now;
                }
                else
                {
                    objPDN001.IProject = IProjectMerge;
                    objPDN001.ModifiedBy = objClsLoginInfo.UserName;
                    objPDN001.ModifiedOn = DateTime.Now;
                }
                db.SaveChanges();

                if (objClsLoginInfo.ListRoles.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.PLNG2.GetStringValue()))
                {
                    var issueList = db.PDN001.Where(r => r.Project == objPDN001.Project).ToList();
                    foreach (var item in issueList)
                    {
                        item.IProject = objPDN001.IProject;
                        item.ModifiedBy = objClsLoginInfo.UserName;
                        item.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDeMergeProjectPartial(int HeaderId, string IProjectMerge)
        {
            var objPDN001 = db.PDN001.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
            var MainProject = db.COM001.Where(i => i.t_cprj == objPDN001.Project).FirstOrDefault();
            ViewBag.MainProject = MainProject != null ? (MainProject.t_cprj + "-" + MainProject.t_dsca) : "";
            ViewBag.IProjectMerge = IProjectMerge;
            return PartialView("_DeMergeProjectPartial");
        }

        public ActionResult DemergePDINIProject(string HeaderId, string IProjectMerge, List<DemergeIProjectEnt> lstDemergeIProjectEnt)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            int ActualHeaderId = 0;
            string NewIProjects = "";
            List<string> DemergeProjectList = new List<string>();
            lstPDINMergeDemergeAttachmentEnt = new List<PDINMergeDemergeAttachmentEnt>();
            try
            {
                string project = "";
                string newIProject = "";
                context = new IEMQSEntitiesContext();
                using (DbContextTransaction dbTran = context.Database.BeginTransaction())
                {
                    try
                    {
                        int.TryParse(HeaderId, out ActualHeaderId);
                        var objPDN001 = context.PDN001.Where(r => r.HeaderId == ActualHeaderId).FirstOrDefault();
                        if (objPDN001.Status == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue() && !objClsLoginInfo.ListRoles.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.PLNG2.GetStringValue()))
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.ReleasedDINProjectMerge.ToString();
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }

                        var IProjectDemerge = string.Join(",", lstDemergeIProjectEnt.Select(i => i.IProject).ToArray());
                        //if (objPDN001.IssueNo == 1)
                        //{
                        objPDN001.IProject = GetFinalIProject(objPDN001.IProject, IProjectMerge, IProjectDemerge);
                        //}

                        if (lstDemergeIProjectEnt.Any(i => i.IsPrimary == false))
                        {
                            var arrayIProject = lstDemergeIProjectEnt.Where(i => i.IsPrimary == false).Select(i => i.IProject).ToArray();
                            NewIProjects = string.Join(",", arrayIProject);
                        }
                        if (NewIProjects != string.Empty)
                        {
                            var objNewProject = lstDemergeIProjectEnt.Where(i => i.IsPrimary == true).FirstOrDefault();
                            DemergeProjectList.Add(objNewProject.IProject);
                            CreatePDINFromDemergeIProject(objPDN001, objNewProject.IProject, NewIProjects);
                        }
                        else
                        {
                            foreach (var item in lstDemergeIProjectEnt)
                            {
                                DemergeProjectList.Add(item.IProject);
                                CreatePDINFromDemergeIProject(objPDN001, item.IProject, NewIProjects);
                            }
                        }
                        context.SaveChanges();
                        dbTran.Commit();

                        project = objPDN001.Project;
                        newIProject = objPDN001.IProject;
                    }
                    catch (DbEntityValidationException dex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(dex);
                        dbTran.Rollback();
                        throw dex;
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        dbTran.Rollback();
                        throw ex;
                    }
                    finally
                    {
                        if (context != null)
                        {
                            context = null;
                        }
                    }
                }

                if (objClsLoginInfo.ListRoles.Any(x => x.ToString() == clsImplementationEnum.UserRoleName.PLNG2.GetStringValue()))
                {
                    var issueList = db.PDN001.Where(r => r.Project == project).ToList();
                    foreach (var item in issueList)
                    {
                        item.IProject = newIProject;
                        item.ModifiedBy = objClsLoginInfo.UserName;
                        item.ModifiedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProjectMergeDemerge.ToString();

                CopyDINAttachDocumentsSourceToDestination();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            finally
            {
                if (context != null)
                {
                    context = null;
                }
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        private void CreatePDINFromDemergeIProject(PDN001 objPDN001, string NewProject, string MergeIProject)
        {
            try
            {
                #region Header    

                PDN001 objNewPDN001 = new PDN001();
                context.PDN001.Add(objNewPDN001);
                var objPDN001Values = context.Entry(objPDN001).CurrentValues;
                context.Entry(objNewPDN001).CurrentValues.SetValues(objPDN001Values);
                objNewPDN001.Project = NewProject;
                objNewPDN001.IProject = MergeIProject;
                objNewPDN001.CreatedBy = objClsLoginInfo.UserName;
                objNewPDN001.CreatedOn = DateTime.Now;
                objNewPDN001.IssueBy = objClsLoginInfo.UserName;
                objNewPDN001.IssueDate = DateTime.Now;
                objNewPDN001.Status = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue();
                objNewPDN001.PDN002 = new List<PDN002>();
                context.SaveChanges();
                #endregion

                var LocationInitial = objNewPDN001.Location.Substring(0, 1);

                #region Lines
                foreach (var item in objPDN001.PDN002.ToList())
                {
                    var SourcePath = "";
                    var DestinationPath = "";
                    var DocNo = item.DocumentNo.Split('_').Last();
                    var Document = LocationInitial + "02_" + objNewPDN001.Project.Trim().Substring(objNewPDN001.Project.Trim().Length - 4) + "_" + DocNo;

                    var objNewPDN002 = new PDN002();
                    objNewPDN001.PDN002.Add(objNewPDN002);
                    var objNewPDN002Values = context.Entry(item).CurrentValues;
                    context.Entry(objNewPDN002).CurrentValues.SetValues(objNewPDN002Values);
                    objNewPDN002.Project = objNewPDN001.Project;
                    objNewPDN002.LineId = 0;
                    objNewPDN002.DocumentNo = Document;
                    objNewPDN002.CreatedBy = objClsLoginInfo.UserName;
                    objNewPDN002.CreatedOn = DateTime.Now;
                    objNewPDN002.HeaderId = objNewPDN001.HeaderId;
                    context.SaveChanges();

                    #region Air_Test_Plan
                    if (item.Plan == clsImplementationEnum.PlanList.Air_Test_Plan.GetStringValue())
                    {
                        var objPLN015 = context.PLN015.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewPLN015 = new PLN015();
                        context.PLN015.Add(objNewPLN015);
                        var objNewPLN015Values = context.Entry(objPLN015).CurrentValues;
                        context.Entry(objNewPLN015).CurrentValues.SetValues(objNewPLN015Values);
                        objNewPLN015.CreatedBy = objClsLoginInfo.UserName;
                        objNewPLN015.CreatedOn = DateTime.Now;
                        objNewPLN015.Document = objNewPDN002.DocumentNo;
                        objNewPLN015.Project = objNewPDN002.Project;
                        objNewPLN015.HeaderId = 0;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewPLN015.HeaderId;

                        SourcePath = "PLN015/" + item.RefId + "/" + item.RevNo;
                        DestinationPath = "PLN015/" + objNewPLN015.HeaderId + "/" + objNewPLN015.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                    }
                    #endregion

                    #region Equipment_Handling_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Equipment_Handling_Plan.GetStringValue())
                    {
                        var objPLN011 = context.PLN011.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewPLN011 = new PLN011();
                        context.PLN011.Add(objNewPLN011);
                        var objNewPLN011Values = context.Entry(objPLN011).CurrentValues;
                        context.Entry(objNewPLN011).CurrentValues.SetValues(objNewPLN011Values);
                        objNewPLN011.CreatedBy = objClsLoginInfo.UserName;
                        objNewPLN011.CreatedOn = DateTime.Now;
                        objNewPLN011.Document = objNewPDN002.DocumentNo;
                        objNewPLN011.Project = objNewPDN002.Project;
                        objNewPLN011.HeaderId = 0;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewPLN011.HeaderId;

                        SourcePath = "PLN011/" + item.RefId + "/" + item.RevNo;
                        DestinationPath = "PLN011/" + objNewPLN011.HeaderId + "/" + objNewPLN011.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });

                        foreach (var objPLN012 in objPLN011.PLN012.ToList())
                        {
                            var objNewPLN012 = new PLN012();
                            objNewPLN011.PLN012.Add(objNewPLN012);
                            var objCurrentChildValues = context.Entry(objPLN012).CurrentValues;
                            context.Entry(objNewPLN012).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPLN012.CreatedBy = objClsLoginInfo.UserName;
                            objNewPLN012.CreatedOn = DateTime.Now;
                            objNewPLN012.Document = objNewPLN011.Document;
                            objNewPLN012.Project = objNewPLN011.Project;
                            objNewPLN012.LineId = 0;

                            //SourcePath = "PLN012/" + item.RefId + "/R" + item.RevNo;
                            //DestinationPath = "PLN012/" + objNewPLN012.HeaderId + "/R" + objNewPLN012.RevNo;
                            //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region Equipment_Jacking_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Equipment_Jacking_Plan.GetStringValue())
                    {
                        var objPLN016 = context.PLN016.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewPLN016 = new PLN016();
                        context.PLN016.Add(objNewPLN016);
                        var objCurrentValues = context.Entry(objPLN016).CurrentValues;
                        context.Entry(objNewPLN016).CurrentValues.SetValues(objCurrentValues);
                        objNewPLN016.CreatedBy = objClsLoginInfo.UserName;
                        objNewPLN016.CreatedOn = DateTime.Now;
                        objNewPLN016.Document = objNewPDN002.DocumentNo;
                        objNewPLN016.Project = NewProject;
                        objNewPLN016.HeaderId = 0;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewPLN016.HeaderId;

                        SourcePath = "PLN016/" + item.RefId + "/" + item.RevNo;
                        DestinationPath = "PLN016/" + objNewPLN016.HeaderId + "/" + objNewPLN016.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });

                        foreach (var objPLN022 in objPLN016.PLN022.ToList())
                        {
                            var objNewPLN022 = new PLN022();
                            objNewPLN016.PLN022.Add(objNewPLN022);
                            var objCurrentChildValues = context.Entry(objPLN022).CurrentValues;
                            context.Entry(objNewPLN022).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPLN022.CreatedBy = objClsLoginInfo.UserName;
                            objNewPLN022.CreatedOn = DateTime.Now;
                            objNewPLN022.Document = objNewPLN016.Document;
                            objNewPLN022.Project = objNewPLN016.Project;
                            objNewPLN022.LineId = 0;

                            //SourcePath = "PLN022/" + item.RefId + "/R" + item.RevNo;
                            //DestinationPath = "PLN022/" + objNewPLN022.HeaderId + "/R" + objNewPLN022.RevNo;
                            //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region Fixture
                    else if (item.Plan == clsImplementationEnum.PlanList.Fixture.GetStringValue())
                    {
                        var objFXR001 = context.FXR001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewFXR001 = new FXR001();
                        context.FXR001.Add(objNewFXR001);
                        var objCurrentValues = context.Entry(objFXR001).CurrentValues;
                        context.Entry(objNewFXR001).CurrentValues.SetValues(objCurrentValues);
                        objNewFXR001.CreatedBy = objClsLoginInfo.UserName;
                        objNewFXR001.CreatedOn = DateTime.Now;
                        objNewFXR001.Document = objNewPDN002.DocumentNo;
                        objNewFXR001.HeaderId = 0;
                        objNewFXR001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewFXR001.HeaderId;

                        //SourcePath = "FXR001/" + item.RefId + "/R" + item.RevNo;
                        //DestinationPath = "FXR001/" + objNewFXR001.HeaderId + "/R" + objNewFXR001.RevNo;
                        //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });

                        foreach (var objFXR002 in objFXR001.FXR002.ToList())
                        {
                            var objNewFXR002 = new FXR002();
                            objNewFXR001.FXR002.Add(objNewFXR002);
                            var objCurrentChildValues = context.Entry(objFXR002).CurrentValues;
                            context.Entry(objNewFXR002).CurrentValues.SetValues(objCurrentChildValues);
                            objNewFXR002.CreatedBy = objClsLoginInfo.UserName;
                            objNewFXR002.CreatedOn = DateTime.Now;
                            objNewFXR002.Document = objNewFXR001.Document;
                            objNewFXR002.Project = objNewFXR001.Project;
                            objNewFXR002.LineId = 0;
                            context.SaveChanges();

                            SourcePath = "FXR002/" + item.RevNo + "/" + objFXR002.LineId;
                            DestinationPath = "FXR002/" + objNewFXR001.RevNo + "/" + objNewFXR002.LineId;
                            lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }
                    }
                    #endregion

                    #region Heat_Treatment_Charge_Sheet
                    else if (item.Plan == clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet.GetStringValue())
                    {
                        //HTC001
                        var objHTC001 = context.HTC001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewHTC001 = new HTC001();
                        context.HTC001.Add(objNewHTC001);
                        var objCurrentValues = context.Entry(objHTC001).CurrentValues;
                        context.Entry(objNewHTC001).CurrentValues.SetValues(objCurrentValues);
                        objNewHTC001.CreatedBy = objClsLoginInfo.UserName;
                        objNewHTC001.CreatedOn = DateTime.Now;
                        objNewHTC001.Document = objNewPDN002.DocumentNo;
                        objNewHTC001.HeaderId = 0;
                        objNewHTC001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewHTC001.HeaderId;

                        foreach (var objHTC002 in objHTC001.HTC002.ToList())
                        {
                            var objNewHTC002 = new HTC002();
                            objNewHTC001.HTC002.Add(objNewHTC002);
                            var objCurrentChildValues = context.Entry(objHTC002).CurrentValues;
                            context.Entry(objNewHTC002).CurrentValues.SetValues(objCurrentChildValues);
                            objNewHTC002.CreatedBy = objClsLoginInfo.UserName;
                            objNewHTC002.CreatedOn = DateTime.Now;
                            objNewHTC002.Document = objNewHTC001.Document;
                            objNewHTC002.Project = objNewHTC001.Project;
                            objNewHTC002.LineId = 0;
                            objNewHTC002.HeaderId = objNewHTC001.HeaderId;
                            context.SaveChanges();
                        }

                    }
                    #endregion

                    #region Hydro_Test_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Hydro_Test_Plan.GetStringValue())
                    {
                        //PLN007
                        var objPLN007 = context.PLN007.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewPLN007 = new PLN007();
                        context.PLN007.Add(objNewPLN007);
                        var objCurrentValues = context.Entry(objPLN007).CurrentValues;
                        context.Entry(objNewPLN007).CurrentValues.SetValues(objCurrentValues);
                        objNewPLN007.CreatedBy = objClsLoginInfo.UserName;
                        objNewPLN007.CreatedOn = DateTime.Now;
                        objNewPLN007.Document = objNewPDN002.DocumentNo;
                        objNewPLN007.HeaderId = 0;
                        objNewPLN007.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewPLN007.HeaderId;

                        SourcePath = "PLN007/" + item.RefId + "/" + item.RevNo;
                        DestinationPath = "PLN007/" + objNewPLN007.HeaderId + "/" + objNewPLN007.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });

                        foreach (var objPLN008 in objPLN007.PLN008.ToList())
                        {
                            var objNewPLN008 = new PLN008();
                            objNewPLN007.PLN008.Add(objNewPLN008);
                            var objCurrentChildValues = context.Entry(objPLN008).CurrentValues;
                            context.Entry(objNewPLN008).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPLN008.CreatedBy = objClsLoginInfo.UserName;
                            objNewPLN008.CreatedOn = DateTime.Now;
                            objNewPLN008.Document = objNewPLN007.Document;
                            objNewPLN008.Project = objNewPLN007.Project;
                            objNewPLN008.LineId = 0;

                            //SourcePath = "PLN008/" + item.RefId + "/R" + item.RevNo;
                            //DestinationPath = "PLN008/" + objNewPLN008.HeaderId + "/R" + objNewPLN008.RevNo;
                            //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }

                        context.SaveChanges();
                    }
                    #endregion

                    #region Improvement_Budget_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Improvement_Budget_Plan.GetStringValue())
                    {
                        //IMB001
                        var objIMB001 = context.IMB001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewIMB001 = new IMB001();
                        context.IMB001.Add(objNewIMB001);
                        var objCurrentValues = context.Entry(objIMB001).CurrentValues;
                        context.Entry(objNewIMB001).CurrentValues.SetValues(objCurrentValues);
                        objNewIMB001.CreatedBy = objClsLoginInfo.UserName;
                        objNewIMB001.CreatedOn = DateTime.Now;
                        objNewIMB001.Document = objNewPDN002.DocumentNo;
                        objNewIMB001.HeaderId = 0;
                        objNewIMB001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewIMB001.HeaderId;

                        SourcePath = "IMB001/" + item.RefId + "/R" + item.RevNo;
                        DestinationPath = "IMB001/" + objNewIMB001.HeaderId + "/R" + objNewIMB001.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });

                        foreach (var objIMB002 in objIMB001.IMB002.ToList())
                        {
                            var objNewIMB002 = new IMB002();
                            objNewIMB001.IMB002.Add(objNewIMB002);
                            var objCurrentChildValues = context.Entry(objIMB002).CurrentValues;
                            context.Entry(objNewIMB002).CurrentValues.SetValues(objCurrentChildValues);
                            objNewIMB002.CreatedBy = objClsLoginInfo.UserName;
                            objNewIMB002.CreatedOn = DateTime.Now;
                            objNewIMB002.Document = objNewIMB001.Document;
                            objNewIMB002.Project = objNewIMB001.Project;
                            objNewIMB002.LineId = 0;
                            context.SaveChanges();

                            SourcePath = "IMB002/" + objIMB002.LineId;
                            DestinationPath = "IMB002/" + objNewIMB002.LineId;
                            lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }

                    }
                    #endregion

                    #region Initiate_Exit_Meeting
                    else if (item.Plan == clsImplementationEnum.PlanList.Initiate_Exit_Meeting.GetStringValue())
                    {
                        //EMT001
                        var objEMT001 = context.EMT001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewEMT001 = new EMT001();
                        context.EMT001.Add(objNewEMT001);
                        var objCurrentValues = context.Entry(objEMT001).CurrentValues;
                        context.Entry(objNewEMT001).CurrentValues.SetValues(objCurrentValues);
                        objNewEMT001.CreatedBy = objClsLoginInfo.UserName;
                        objNewEMT001.CreatedOn = DateTime.Now;
                        objNewEMT001.Document = objNewPDN002.DocumentNo;
                        objNewEMT001.Project = NewProject;
                        objNewEMT001.HeaderId = 0;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewEMT001.HeaderId;

                        SourcePath = "EMT001/" + item.RefId;
                        DestinationPath = "EMT001/" + objNewEMT001.HeaderId;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath, Type = UploadType.Normal.GetStringValue() });
                    }
                    #endregion

                    #region Internal_Axel_Movement_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Internal_Axel_Movement_Plan.GetStringValue())
                    {
                        //PLN018
                        var objPLN018 = context.PLN018.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewPLN018 = new PLN018();
                        context.PLN018.Add(objNewPLN018);
                        var objCurrentValues = context.Entry(objPLN018).CurrentValues;
                        context.Entry(objNewPLN018).CurrentValues.SetValues(objCurrentValues);
                        objNewPLN018.CreatedBy = objClsLoginInfo.UserName;
                        objNewPLN018.CreatedOn = DateTime.Now;
                        objNewPLN018.Document = objNewPDN002.DocumentNo;
                        objNewPLN018.HeaderId = 0;
                        objNewPLN018.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewPLN018.HeaderId;

                        SourcePath = "PLN018/" + item.RefId + "/" + item.RevNo;
                        DestinationPath = "PLN018/" + objNewPLN018.HeaderId + "/" + objNewPLN018.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });

                    }
                    #endregion

                    #region JPP
                    else if (item.Plan == clsImplementationEnum.PlanList.JPP.GetStringValue())
                    {
                        #region comment
                        //if (item.RefId > 0)
                        //{
                        //    var objJPP001 = context.JPP001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        //    var objNewJPP001 = new JPP001();
                        //    context.JPP001.Add(objNewJPP001);
                        //    var objCurrentValues = context.Entry(objJPP001).CurrentValues;
                        //    context.Entry(objNewJPP001).CurrentValues.SetValues(objCurrentValues);
                        //    objNewJPP001.CreatedBy = objClsLoginInfo.UserName;
                        //    objNewJPP001.CreatedOn = DateTime.Now;
                        //    objNewJPP001.Document = objNewPDN002.DocumentNo;
                        //    objNewJPP001.HeaderId = 0;
                        //    objNewJPP001.Project = NewProject;
                        //    foreach (var objJPP002 in objJPP001.JPP002.ToList())
                        //    {
                        //        var objNewJPP002 = new JPP002();
                        //        objNewJPP001.JPP002.Add(objNewJPP002);
                        //        var objCurrentChildValues = context.Entry(objJPP002).CurrentValues;
                        //        context.Entry(objNewJPP002).CurrentValues.SetValues(objCurrentChildValues);
                        //        objNewJPP002.CreatedBy = objClsLoginInfo.UserName;
                        //        objNewJPP002.CreatedOn = DateTime.Now;
                        //        objNewJPP002.Document = objNewJPP001.Document;
                        //        objNewJPP002.Project = objNewJPP001.Project;
                        //        objNewJPP002.LineId = 0;
                        //    }
                        //    foreach (var objJPP003 in objJPP001.JPP003.ToList())
                        //    {
                        //        var objNewJPP003 = new JPP003();
                        //        objNewJPP001.JPP003.Add(objNewJPP003);
                        //        var objCurrentChildValues = context.Entry(objJPP003).CurrentValues;
                        //        context.Entry(objNewJPP003).CurrentValues.SetValues(objCurrentChildValues);
                        //        objNewJPP003.CreatedBy = objClsLoginInfo.UserName;
                        //        objNewJPP003.CreatedOn = DateTime.Now;
                        //        objNewJPP003.Document = objNewJPP001.Document;
                        //        objNewJPP003.Project = objNewJPP001.Project;
                        //        objNewJPP003.LineId = 0;
                        //    }
                        //    foreach (var objJPP004 in objJPP001.JPP004.ToList())
                        //    {
                        //        var objNewJPP004 = new JPP004();
                        //        objNewJPP001.JPP004.Add(objNewJPP004);
                        //        var objCurrentChildValues = context.Entry(objJPP004).CurrentValues;
                        //        context.Entry(objNewJPP004).CurrentValues.SetValues(objCurrentChildValues);
                        //        objNewJPP004.CreatedBy = objClsLoginInfo.UserName;
                        //        objNewJPP004.CreatedOn = DateTime.Now;
                        //        objNewJPP004.Document = objNewJPP001.Document;
                        //        objNewJPP004.Project = objNewJPP001.Project;
                        //        objNewJPP004.LineId = 0;
                        //    }
                        //    foreach (var objJPP005 in objJPP001.JPP005.ToList())
                        //    {
                        //        var objNewJPP005 = new JPP005();
                        //        objNewJPP001.JPP005.Add(objNewJPP005);
                        //        var objCurrentChildValues = context.Entry(objJPP005).CurrentValues;
                        //        context.Entry(objNewJPP005).CurrentValues.SetValues(objCurrentChildValues);
                        //        objNewJPP005.CreatedBy = objClsLoginInfo.UserName;
                        //        objNewJPP005.CreatedOn = DateTime.Now;
                        //        objNewJPP005.Document = objNewJPP001.Document;
                        //        objNewJPP005.Project = objNewJPP001.Project;
                        //        objNewJPP005.LineId = 0;
                        //    }
                        //    foreach (var objJPP006 in objJPP001.JPP006.ToList())
                        //    {
                        //        var objNewJPP006 = new JPP006();
                        //        objNewJPP001.JPP006.Add(objNewJPP006);
                        //        var objCurrentChildValues = context.Entry(objJPP006).CurrentValues;
                        //        context.Entry(objNewJPP006).CurrentValues.SetValues(objCurrentChildValues);
                        //        objNewJPP006.CreatedBy = objClsLoginInfo.UserName;
                        //        objNewJPP006.CreatedOn = DateTime.Now;
                        //        objNewJPP006.Document = objNewJPP001.Document;
                        //        objNewJPP006.Project = objNewJPP001.Project;
                        //        objNewJPP006.LineId = 0;
                        //    }
                        //    foreach (var objJPP007 in objJPP001.JPP007.ToList())
                        //    {
                        //        var objNewJPP007 = new JPP007();
                        //        objNewJPP001.JPP007.Add(objNewJPP007);
                        //        var objCurrentChildValues = context.Entry(objJPP007).CurrentValues;
                        //        context.Entry(objNewJPP007).CurrentValues.SetValues(objCurrentChildValues);
                        //        objNewJPP007.CreatedBy = objClsLoginInfo.UserName;
                        //        objNewJPP007.CreatedOn = DateTime.Now;
                        //        objNewJPP007.Document = objNewJPP001.Document;
                        //        objNewJPP007.Project = objNewJPP001.Project;
                        //        objNewJPP007.LineId = 0;
                        //    }
                        //    foreach (var objJPP008 in objJPP001.JPP008.ToList())
                        //    {
                        //        var objNewJPP008 = new JPP008();
                        //        objNewJPP001.JPP008.Add(objNewJPP008);
                        //        var objCurrentChildValues = context.Entry(objJPP008).CurrentValues;
                        //        context.Entry(objNewJPP008).CurrentValues.SetValues(objCurrentChildValues);
                        //        objNewJPP008.CreatedBy = objClsLoginInfo.UserName;
                        //        objNewJPP008.CreatedOn = DateTime.Now;
                        //        objNewJPP008.Document = objNewJPP001.Document;
                        //        objNewJPP008.Project = objNewJPP001.Project;
                        //        objNewJPP008.LineId = 0;
                        //    }
                        //    foreach (var objJPP009 in objJPP001.JPP009.ToList())
                        //    {
                        //        var objNewJPP009 = new JPP009();
                        //        objNewJPP001.JPP009.Add(objNewJPP009);
                        //        var objCurrentChildValues = context.Entry(objJPP009).CurrentValues;
                        //        context.Entry(objNewJPP009).CurrentValues.SetValues(objCurrentChildValues);
                        //        objNewJPP009.CreatedBy = objClsLoginInfo.UserName;
                        //        objNewJPP009.CreatedOn = DateTime.Now;
                        //        objNewJPP009.Document = objNewJPP001.Document;
                        //        objNewJPP009.Project = objNewJPP001.Project;
                        //        objNewJPP009.LineId = 0;
                        //    }
                        //    context.SaveChanges();
                        //    objNewPDN002.RefId = objNewJPP001.HeaderId;
                        //}
                        #endregion
                    }
                    #endregion

                    #region Learning_Capture
                    else if (item.Plan == clsImplementationEnum.PlanList.Learning_Capture.GetStringValue())
                    {
                        var objLNC001 = context.LNC001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewLNC001 = new LNC001();
                        context.LNC001.Add(objNewLNC001);
                        var objCurrentValues = context.Entry(objLNC001).CurrentValues;
                        context.Entry(objNewLNC001).CurrentValues.SetValues(objCurrentValues);
                        objNewLNC001.CreatedBy = objClsLoginInfo.UserName;
                        objNewLNC001.CreatedDate = DateTime.Now;
                        objNewLNC001.DocumentNo = objNewPDN002.DocumentNo;
                        objNewLNC001.HeaderId = 0;
                        objNewLNC001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewLNC001.HeaderId;

                        //SourcePath = "LNC001/" + item.RefId ;
                        //DestinationPath = "LNC001/" + objNewLNC001.HeaderId ;
                        //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });

                        foreach (var objLNC002 in objLNC001.LNC002.ToList())
                        {
                            var objNewLNC002 = new LNC002();
                            objNewLNC001.LNC002.Add(objNewLNC002);
                            var objCurrentChildValues = context.Entry(objLNC002).CurrentValues;
                            context.Entry(objNewLNC002).CurrentValues.SetValues(objCurrentChildValues);
                            objNewLNC002.CreatedBy = objClsLoginInfo.UserName;
                            objNewLNC002.CreatedOn = DateTime.Now;
                            objNewLNC002.DocumentNo = objNewLNC001.DocumentNo;
                            objNewLNC002.Project = objNewLNC001.Project;
                            objNewLNC002.LineId = 0;
                            context.SaveChanges();

                            SourcePath = "LNC002/" + item.RefId;
                            DestinationPath = "LNC002/" + objNewLNC002.HeaderId;
                            lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath, Type = UploadType.Normal.GetStringValue() });
                        }
                    }
                    #endregion

                    #region Locking_Cleat
                    else if (item.Plan == clsImplementationEnum.PlanList.Locking_Cleat.GetStringValue())
                    {
                        //PLN019
                        var objPLN019 = context.PLN019.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewPLN019 = new PLN019();
                        context.PLN019.Add(objNewPLN019);
                        var objCurrentValues = context.Entry(objPLN019).CurrentValues;
                        context.Entry(objNewPLN019).CurrentValues.SetValues(objCurrentValues);
                        objNewPLN019.CreatedBy = objClsLoginInfo.UserName;
                        objNewPLN019.CreatedOn = DateTime.Now;
                        objNewPLN019.Document = objNewPDN002.DocumentNo;
                        objNewPLN019.HeaderId = 0;
                        objNewPLN019.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewPLN019.HeaderId;

                        //SourcePath = "PLN019/" + item.RefId + "/R" + item.RevNo;
                        //DestinationPath = "PLN019/" + objNewPLN019.HeaderId + "/R" + objNewPLN019.RevNo;
                        //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });

                        var lstPLN020 = db.PLN020.Where(i => i.HeaderId == objPLN019.HeaderId).ToList();
                        foreach (var objPLN020 in lstPLN020.ToList())
                        {
                            var objNewPLN020 = new PLN020();
                            context.PLN020.Add(objNewPLN020);
                            var objCurrentChildValues = context.Entry(objPLN020).CurrentValues;
                            context.Entry(objNewPLN020).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPLN020.CreatedBy = objClsLoginInfo.UserName;
                            objNewPLN020.CreatedOn = DateTime.Now;
                            objNewPLN020.Document = objNewPLN019.Document;
                            objNewPLN020.Project = objNewPLN019.Project;
                            objNewPLN020.LineId = 0;
                            objNewPLN020.HeaderId = objNewPLN019.HeaderId;

                            //SourcePath = "PLN020/" + item.RefId + "/R" + item.RevNo;
                            //DestinationPath = "PLN020/" + objNewPLN020.HeaderId + "/R" + objNewPLN020.RevNo;
                            //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }

                        var lstPLN021 = db.PLN021.Where(i => i.HeaderId == objPLN019.HeaderId).ToList();
                        foreach (var objPLN021 in lstPLN021.ToList())
                        {
                            var objNewPLN021 = new PLN021();
                            context.PLN021.Add(objNewPLN021);
                            var objCurrentChildValues = context.Entry(objPLN021).CurrentValues;
                            context.Entry(objNewPLN021).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPLN021.CreatedBy = objClsLoginInfo.UserName;
                            objNewPLN021.CreatedOn = DateTime.Now;
                            objNewPLN021.Document = objNewPLN019.Document;
                            objNewPLN021.Project = objNewPLN019.Project;
                            objNewPLN021.LineId = 0;
                            objNewPLN021.HeaderId = objNewPLN019.HeaderId;

                            //SourcePath = "PLN021/" + item.RefId + "/R" + item.RevNo;
                            //DestinationPath = "PLN021/" + objNewPLN021.HeaderId + "/R" + objNewPLN021.RevNo;
                            //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region Machining_Instructions
                    else if (item.Plan == clsImplementationEnum.PlanList.Machining_Instructions.GetStringValue())
                    {
                        //MCI001
                        var objMCI001 = context.MCI001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewMCI001 = new MCI001();
                        context.MCI001.Add(objNewMCI001);
                        var objCurrentValues = context.Entry(objMCI001).CurrentValues;
                        context.Entry(objNewMCI001).CurrentValues.SetValues(objCurrentValues);
                        objNewMCI001.CreatedBy = objClsLoginInfo.UserName;
                        objNewMCI001.CreatedOn = DateTime.Now;
                        objNewMCI001.Document = objNewPDN002.DocumentNo;
                        objNewMCI001.HeaderId = 0;
                        objNewMCI001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewMCI001.HeaderId;

                        SourcePath = "MCI001/" + item.RefId + "/R" + item.RevNo;
                        DestinationPath = "MCI001/" + objNewMCI001.HeaderId + "/R" + objNewMCI001.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                    }
                    #endregion
                    #region RCCP_Breakup_For_Equipment
                    else if (item.Plan == clsImplementationEnum.PlanList.RCCP_Breakup_For_Equipment.GetStringValue())
                    {
                        //RCC001
                        var objRCC001 = context.RCC001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewRCC001 = new RCC001();
                        context.RCC001.Add(objNewRCC001);
                        var objCurrentValues = context.Entry(objRCC001).CurrentValues;
                        context.Entry(objNewRCC001).CurrentValues.SetValues(objCurrentValues);
                        objNewRCC001.CreatedBy = objClsLoginInfo.UserName;
                        objNewRCC001.CreatedOn = DateTime.Now;
                        objNewRCC001.Document = objNewPDN002.DocumentNo;
                        objNewRCC001.HeaderId = 0;
                        objNewRCC001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewRCC001.HeaderId;

                        SourcePath = "RCC001/" + item.RefId + "/R" + item.RevNo;
                        DestinationPath = "RCC001/" + objNewRCC001.HeaderId + "/R" + objNewRCC001.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                    }
                    #endregion

                    #region Dimension_Data_Capturing_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Dimension_Data_Capturing_Plan.GetStringValue())
                    {
                        var objDDC001 = context.DDC001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewDDC001 = new DDC001();
                        context.DDC001.Add(objNewDDC001);
                        var objCurrentValues = context.Entry(objDDC001).CurrentValues;
                        context.Entry(objNewDDC001).CurrentValues.SetValues(objCurrentValues);
                        objNewDDC001.CreatedBy = objClsLoginInfo.UserName;
                        objNewDDC001.CreatedOn = DateTime.Now;
                        objNewDDC001.Document = objNewPDN002.DocumentNo;
                        objNewDDC001.HeaderId = 0;
                        objNewDDC001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewDDC001.HeaderId;

                        SourcePath = "DDC001/" + item.RefId + "/R" + item.RevNo;
                        DestinationPath = "DDC001/" + objNewDDC001.HeaderId + "/R" + objNewDDC001.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                    }
                    #endregion

                    #region Elbow_Overlay_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Elbow_Overlay_Plan.GetStringValue())
                    {
                        var objELB001 = context.ELB001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewELB001 = new ELB001();
                        context.ELB001.Add(objNewELB001);
                        var objCurrentValues = context.Entry(objELB001).CurrentValues;
                        context.Entry(objNewELB001).CurrentValues.SetValues(objCurrentValues);
                        objNewELB001.CreatedBy = objClsLoginInfo.UserName;
                        objNewELB001.CreatedOn = DateTime.Now;
                        objNewELB001.Document = objNewPDN002.DocumentNo;
                        objNewELB001.HeaderId = 0;
                        objNewELB001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewELB001.HeaderId;

                        SourcePath = "ELB001/" + item.RefId + "/R" + item.RevNo;
                        DestinationPath = "ELB001/" + objNewELB001.HeaderId + "/R" + objNewELB001.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                    }
                    #endregion

                    #region Top_And_Bottom_Spool_Trimming_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Top_And_Bottom_Spool_Trimming_Plan.GetStringValue())
                    {
                        var objTBS001 = context.TBS001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewTBS001 = new TBS001();
                        context.TBS001.Add(objNewTBS001);
                        var objCurrentValues = context.Entry(objTBS001).CurrentValues;
                        context.Entry(objNewTBS001).CurrentValues.SetValues(objCurrentValues);
                        objNewTBS001.CreatedBy = objClsLoginInfo.UserName;
                        objNewTBS001.CreatedOn = DateTime.Now;
                        objNewTBS001.Document = objNewPDN002.DocumentNo;
                        objNewTBS001.HeaderId = 0;
                        objNewTBS001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewTBS001.HeaderId;

                        SourcePath = "TBS001/" + item.RefId + "/R" + item.RevNo;
                        DestinationPath = "TBS001/" + objNewTBS001.HeaderId + "/R" + objNewTBS001.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                    }
                    #endregion

                    #region Machining_Scope_of_Work
                    else if (item.Plan == clsImplementationEnum.PlanList.Machining_Scope_of_Work.GetStringValue())
                    {
                        //MSW001
                        var objMSW001 = context.MSW001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewMSW001 = new MSW001();
                        context.MSW001.Add(objNewMSW001);
                        var objCurrentValues = context.Entry(objMSW001).CurrentValues;
                        context.Entry(objNewMSW001).CurrentValues.SetValues(objCurrentValues);
                        objNewMSW001.CreatedBy = objClsLoginInfo.UserName;
                        objNewMSW001.CreatedOn = DateTime.Now;
                        objNewMSW001.Document = objNewPDN002.DocumentNo;
                        objNewMSW001.HeaderId = 0;
                        objNewMSW001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewMSW001.HeaderId;

                        //SourcePath = "MSW001/" + item.RefId + "/R" + item.RevNo;
                        //DestinationPath = "MSW001/" + objNewMSW001.HeaderId + "/R" + objNewMSW001.RevNo;
                        //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });

                        foreach (var objMSW002 in objMSW001.MSW002.ToList())
                        {
                            var objNewMSW002 = new MSW002();
                            objNewMSW001.MSW002.Add(objNewMSW002);
                            var objCurrentChildValues = context.Entry(objMSW002).CurrentValues;
                            context.Entry(objNewMSW002).CurrentValues.SetValues(objCurrentChildValues);
                            objNewMSW002.CreatedBy = objClsLoginInfo.UserName;
                            objNewMSW002.CreatedOn = DateTime.Now;
                            objNewMSW002.Document = objNewMSW001.Document;
                            objNewMSW002.Project = objNewMSW001.Project;
                            objNewMSW002.LineId = 0;

                            //SourcePath = "MSW002/" + item.RefId + "/R" + item.RevNo;
                            //DestinationPath = "MSW002/" + objNewMSW002.HeaderId + "/R" + objNewMSW002.RevNo;
                            //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region Operation_Cards
                    else if (item.Plan == clsImplementationEnum.PlanList.Operation_Cards.GetStringValue())
                    {
                        //OPC001
                        var objOPC001 = context.OPC001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewOPC001 = new OPC001();
                        context.OPC001.Add(objNewOPC001);
                        var objCurrentValues = context.Entry(objOPC001).CurrentValues;
                        context.Entry(objNewOPC001).CurrentValues.SetValues(objCurrentValues);
                        objNewOPC001.CreatedBy = objClsLoginInfo.UserName;
                        objNewOPC001.CreatedOn = DateTime.Now;
                        objNewOPC001.Document = objNewPDN002.DocumentNo;
                        objNewOPC001.HeaderId = 0;
                        objNewOPC001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewOPC001.HeaderId;

                        SourcePath = "OPC001/" + item.RefId + "/R" + item.RevNo;
                        DestinationPath = "OPC001/" + objNewOPC001.HeaderId + "/R" + objNewOPC001.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                    }
                    #endregion

                    #region Planning_Checklist
                    else if (item.Plan == clsImplementationEnum.PlanList.Planning_Checklist.GetStringValue())
                    {
                        //PCL001
                        var objPCL001 = context.PCL001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewPCL001 = new PCL001();
                        context.PCL001.Add(objNewPCL001);
                        var objCurrentValues = context.Entry(objPCL001).CurrentValues;
                        context.Entry(objNewPCL001).CurrentValues.SetValues(objCurrentValues);
                        objNewPCL001.CreatedBy = objClsLoginInfo.UserName;
                        objNewPCL001.CreatedOn = DateTime.Now;
                        objNewPCL001.Document = objNewPDN002.DocumentNo;
                        objNewPCL001.HeaderId = 0;
                        objNewPCL001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewPCL001.HeaderId;

                        foreach (var objPCL002 in objPCL001.PCL002.ToList())
                        {
                            var objNewPCL002 = new PCL002();
                            objNewPCL001.PCL002.Add(objNewPCL002);
                            var objCurrentChildValues = context.Entry(objPCL002).CurrentValues;
                            context.Entry(objNewPCL002).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL002.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL002.CreatedOn = DateTime.Now;
                            objNewPCL002.Document = objNewPCL001.Document;
                            objNewPCL002.Project = objNewPCL001.Project;
                            objNewPCL002.LineId = 0;
                            objNewPCL002.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL003 in objPCL001.PCL003.ToList())
                        {
                            var objNewPCL003 = new PCL003();
                            objNewPCL001.PCL003.Add(objNewPCL003);
                            var objCurrentChildValues = context.Entry(objPCL003).CurrentValues;
                            context.Entry(objNewPCL003).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL003.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL003.CreatedOn = DateTime.Now;
                            objNewPCL003.Document = objNewPCL001.Document;
                            objNewPCL003.Project = objNewPCL001.Project;
                            objNewPCL003.LineId = 0;
                            objNewPCL003.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL004 in objPCL001.PCL004.ToList())
                        {
                            var objNewPCL004 = new PCL004();
                            objNewPCL001.PCL004.Add(objNewPCL004);
                            var objCurrentChildValues = context.Entry(objPCL004).CurrentValues;
                            context.Entry(objNewPCL004).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL004.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL004.CreatedOn = DateTime.Now;
                            objNewPCL004.Document = objNewPCL001.Document;
                            objNewPCL004.Project = objNewPCL001.Project;
                            objNewPCL004.LineId = 0;
                            objNewPCL004.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL005 in objPCL001.PCL005.ToList())
                        {
                            var objNewPCL005 = new PCL005();
                            objNewPCL001.PCL005.Add(objNewPCL005);
                            var objCurrentChildValues = context.Entry(objPCL005).CurrentValues;
                            context.Entry(objNewPCL005).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL005.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL005.CreatedOn = DateTime.Now;
                            objNewPCL005.Document = objNewPCL001.Document;
                            objNewPCL005.Project = objNewPCL001.Project;
                            objNewPCL005.LineId = 0;
                            objNewPCL005.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL006 in objPCL001.PCL006.ToList())
                        {
                            var objNewPCL006 = new PCL006();
                            objNewPCL001.PCL006.Add(objNewPCL006);
                            var objCurrentChildValues = context.Entry(objPCL006).CurrentValues;
                            context.Entry(objNewPCL006).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL006.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL006.CreatedOn = DateTime.Now;
                            objNewPCL006.Document = objNewPCL001.Document;
                            objNewPCL006.Project = objNewPCL001.Project;
                            objNewPCL006.LineId = 0;
                            objNewPCL006.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL007 in objPCL001.PCL007.ToList())
                        {
                            var objNewPCL007 = new PCL007();
                            objNewPCL001.PCL007.Add(objNewPCL007);
                            var objCurrentChildValues = context.Entry(objPCL007).CurrentValues;
                            context.Entry(objNewPCL007).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL007.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL007.CreatedOn = DateTime.Now;
                            objNewPCL007.Document = objNewPCL001.Document;
                            objNewPCL007.Project = objNewPCL001.Project;
                            objNewPCL007.LineId = 0;
                            objNewPCL007.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL008 in objPCL001.PCL008.ToList())
                        {
                            var objNewPCL008 = new PCL008();
                            objNewPCL001.PCL008.Add(objNewPCL008);
                            var objCurrentChildValues = context.Entry(objPCL008).CurrentValues;
                            context.Entry(objNewPCL008).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL008.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL008.CreatedOn = DateTime.Now;
                            objNewPCL008.Document = objNewPCL001.Document;
                            objNewPCL008.Project = objNewPCL001.Project;
                            objNewPCL008.LineId = 0;
                            objNewPCL008.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL009 in objPCL001.PCL009.ToList())
                        {
                            var objNewPCL009 = new PCL009();
                            objNewPCL001.PCL009.Add(objNewPCL009);
                            var objCurrentChildValues = context.Entry(objPCL009).CurrentValues;
                            context.Entry(objNewPCL009).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL009.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL009.CreatedOn = DateTime.Now;
                            objNewPCL009.Document = objNewPCL001.Document;
                            objNewPCL009.Project = objNewPCL001.Project;
                            objNewPCL009.LineId = 0;
                            objNewPCL009.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL010 in objPCL001.PCL010.ToList())
                        {
                            var objNewPCL010 = new PCL010();
                            objNewPCL001.PCL010.Add(objNewPCL010);
                            var objCurrentChildValues = context.Entry(objPCL010).CurrentValues;
                            context.Entry(objNewPCL010).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL010.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL010.CreatedOn = DateTime.Now;
                            objNewPCL010.Document = objNewPCL001.Document;
                            objNewPCL010.Project = objNewPCL001.Project;
                            objNewPCL010.LineId = 0;
                            objNewPCL010.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL011 in objPCL001.PCL011.ToList())
                        {
                            var objNewPCL011 = new PCL011();
                            objNewPCL001.PCL011.Add(objNewPCL011);
                            var objCurrentChildValues = context.Entry(objPCL011).CurrentValues;
                            context.Entry(objNewPCL011).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL011.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL011.CreatedOn = DateTime.Now;
                            objNewPCL011.Document = objNewPCL001.Document;
                            objNewPCL011.Project = objNewPCL001.Project;
                            objNewPCL011.LineId = 0;
                            objNewPCL011.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL012 in objPCL001.PCL012.ToList())
                        {
                            var objNewPCL012 = new PCL012();
                            objNewPCL001.PCL012.Add(objNewPCL012);
                            var objCurrentChildValues = context.Entry(objPCL012).CurrentValues;
                            context.Entry(objNewPCL012).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL012.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL012.CreatedOn = DateTime.Now;
                            objNewPCL012.Document = objNewPCL001.Document;
                            objNewPCL012.Project = objNewPCL001.Project;
                            objNewPCL012.LineId = 0;
                            objNewPCL012.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL013 in objPCL001.PCL013.ToList())
                        {
                            var objNewPCL013 = new PCL013();
                            objNewPCL001.PCL013.Add(objNewPCL013);
                            var objCurrentChildValues = context.Entry(objPCL013).CurrentValues;
                            context.Entry(objNewPCL013).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL013.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL013.CreatedOn = DateTime.Now;
                            objNewPCL013.Document = objNewPCL001.Document;
                            objNewPCL013.Project = objNewPCL001.Project;
                            objNewPCL013.LineId = 0;
                            objNewPCL013.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL016 in objPCL001.PCL016.ToList())
                        {
                            var objNewPCL016 = new PCL016();
                            objNewPCL001.PCL016.Add(objNewPCL016);
                            var objCurrentChildValues = context.Entry(objPCL016).CurrentValues;
                            context.Entry(objNewPCL016).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL016.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL016.CreatedOn = DateTime.Now;
                            objNewPCL016.Document = objNewPCL001.Document;
                            objNewPCL016.Project = objNewPCL001.Project;
                            objNewPCL016.Id = 0;
                            objNewPCL016.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL017 in objPCL001.PCL017.ToList())
                        {
                            var objNewPCL017 = new PCL017();
                            objNewPCL001.PCL017.Add(objNewPCL017);
                            var objCurrentChildValues = context.Entry(objPCL017).CurrentValues;
                            context.Entry(objNewPCL017).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL017.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL017.CreatedOn = DateTime.Now;
                            objNewPCL017.Document = objNewPCL001.Document;
                            objNewPCL017.Project = objNewPCL001.Project;
                            objNewPCL017.LineId = 0;
                            objNewPCL017.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL018 in objPCL001.PCL018.ToList())
                        {
                            var objNewPCL018 = new PCL018();
                            objNewPCL001.PCL018.Add(objNewPCL018);
                            var objCurrentChildValues = context.Entry(objPCL018).CurrentValues;
                            context.Entry(objNewPCL018).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL018.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL018.CreatedOn = DateTime.Now;
                            objNewPCL018.Document = objNewPCL001.Document;
                            objNewPCL018.Project = objNewPCL001.Project;
                            objNewPCL018.LineId = 0;
                            objNewPCL018.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL019 in objPCL001.PCL019.ToList())
                        {
                            var objNewPCL019 = new PCL019();
                            objNewPCL001.PCL019.Add(objNewPCL019);
                            var objCurrentChildValues = context.Entry(objPCL019).CurrentValues;
                            context.Entry(objNewPCL019).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL019.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL019.CreatedOn = DateTime.Now;
                            objNewPCL019.Document = objNewPCL001.Document;
                            objNewPCL019.Project = objNewPCL001.Project;
                            objNewPCL019.LineId = 0;
                            objNewPCL019.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL020 in objPCL001.PCL020.ToList())
                        {
                            var objNewPCL020 = new PCL020();
                            objNewPCL001.PCL020.Add(objNewPCL020);
                            var objCurrentChildValues = context.Entry(objPCL020).CurrentValues;
                            context.Entry(objNewPCL020).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL020.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL020.CreatedOn = DateTime.Now;
                            objNewPCL020.Document = objNewPCL001.Document;
                            objNewPCL020.Project = objNewPCL001.Project;
                            objNewPCL020.LineId = 0;
                            objNewPCL020.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL021 in objPCL001.PCL021.ToList())
                        {
                            var objNewPCL021 = new PCL021();
                            objNewPCL001.PCL021.Add(objNewPCL021);
                            var objCurrentChildValues = context.Entry(objPCL021).CurrentValues;
                            context.Entry(objNewPCL021).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL021.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL021.CreatedOn = DateTime.Now;
                            objNewPCL021.Document = objNewPCL001.Document;
                            objNewPCL021.Project = objNewPCL001.Project;
                            objNewPCL021.LineId = 0;
                            objNewPCL021.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL022 in objPCL001.PCL022.ToList())
                        {
                            var objNewPCL022 = new PCL022();
                            objNewPCL001.PCL022.Add(objNewPCL022);
                            var objCurrentChildValues = context.Entry(objPCL022).CurrentValues;
                            context.Entry(objNewPCL022).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL022.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL022.CreatedOn = DateTime.Now;
                            objNewPCL022.Document = objNewPCL001.Document;
                            objNewPCL022.Project = objNewPCL001.Project;
                            objNewPCL022.LineId = 0;
                            objNewPCL022.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL023 in objPCL001.PCL023.ToList())
                        {
                            var objNewPCL023 = new PCL023();
                            objNewPCL001.PCL023.Add(objNewPCL023);
                            var objCurrentChildValues = context.Entry(objPCL023).CurrentValues;
                            context.Entry(objNewPCL023).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL023.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL023.CreatedOn = DateTime.Now;
                            objNewPCL023.Document = objNewPCL001.Document;
                            objNewPCL023.Project = objNewPCL001.Project;
                            objNewPCL023.LineId = 0;
                            objNewPCL023.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL024 in objPCL001.PCL024.ToList())
                        {
                            var objNewPCL024 = new PCL024();
                            objNewPCL001.PCL024.Add(objNewPCL024);
                            var objCurrentChildValues = context.Entry(objPCL024).CurrentValues;
                            context.Entry(objNewPCL024).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL024.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL024.CreatedOn = DateTime.Now;
                            objNewPCL024.Document = objNewPCL001.Document;
                            objNewPCL024.Project = objNewPCL001.Project;
                            objNewPCL024.LineId = 0;
                            objNewPCL024.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL025 in objPCL001.PCL025.ToList())
                        {
                            var objNewPCL025 = new PCL025();
                            objNewPCL001.PCL025.Add(objNewPCL025);
                            var objCurrentChildValues = context.Entry(objPCL025).CurrentValues;
                            context.Entry(objNewPCL025).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL025.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL025.CreatedOn = DateTime.Now;
                            objNewPCL025.Document = objNewPCL001.Document;
                            objNewPCL025.Project = objNewPCL001.Project;
                            objNewPCL025.LineId = 0;
                            objNewPCL025.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL026 in objPCL001.PCL026.ToList())
                        {
                            var objNewPCL026 = new PCL026();
                            objNewPCL001.PCL026.Add(objNewPCL026);
                            var objCurrentChildValues = context.Entry(objPCL026).CurrentValues;
                            context.Entry(objNewPCL026).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL026.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL026.CreatedOn = DateTime.Now;
                            objNewPCL026.Document = objNewPCL001.Document;
                            objNewPCL026.Project = objNewPCL001.Project;
                            objNewPCL026.LineId = 0;
                            objNewPCL026.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL027 in objPCL001.PCL027.ToList())
                        {
                            var objNewPCL027 = new PCL027();
                            objNewPCL001.PCL027.Add(objNewPCL027);
                            var objCurrentChildValues = context.Entry(objPCL027).CurrentValues;
                            context.Entry(objNewPCL027).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL027.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL027.CreatedOn = DateTime.Now;
                            objNewPCL027.Document = objNewPCL001.Document;
                            objNewPCL027.Project = objNewPCL001.Project;
                            objNewPCL027.LineId = 0;
                            objNewPCL027.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL028 in objPCL001.PCL028.ToList())
                        {
                            var objNewPCL028 = new PCL028();
                            objNewPCL001.PCL028.Add(objNewPCL028);
                            var objCurrentChildValues = context.Entry(objPCL028).CurrentValues;
                            context.Entry(objNewPCL028).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL028.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL028.CreatedOn = DateTime.Now;
                            objNewPCL028.Document = objNewPCL001.Document;
                            objNewPCL028.Project = objNewPCL001.Project;
                            objNewPCL028.LineId = 0;
                            objNewPCL028.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL029 in objPCL001.PCL029.ToList())
                        {
                            var objNewPCL029 = new PCL029();
                            objNewPCL001.PCL029.Add(objNewPCL029);
                            var objCurrentChildValues = context.Entry(objPCL029).CurrentValues;
                            context.Entry(objNewPCL029).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL029.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL029.CreatedOn = DateTime.Now;
                            objNewPCL029.Document = objNewPCL001.Document;
                            objNewPCL029.Project = objNewPCL001.Project;
                            objNewPCL029.LineId = 0;
                            objNewPCL029.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL030 in objPCL001.PCL030.ToList())
                        {
                            var objNewPCL030 = new PCL030();
                            objNewPCL001.PCL030.Add(objNewPCL030);
                            var objCurrentChildValues = context.Entry(objPCL030).CurrentValues;
                            context.Entry(objNewPCL030).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL030.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL030.CreatedOn = DateTime.Now;
                            objNewPCL030.Document = objNewPCL001.Document;
                            objNewPCL030.Project = objNewPCL001.Project;
                            objNewPCL030.LineId = 0;
                            objNewPCL030.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL031 in objPCL001.PCL031.ToList())
                        {
                            var objNewPCL031 = new PCL031();
                            objNewPCL001.PCL031.Add(objNewPCL031);
                            var objCurrentChildValues = context.Entry(objPCL031).CurrentValues;
                            context.Entry(objNewPCL031).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL031.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL031.CreatedOn = DateTime.Now;
                            objNewPCL031.Document = objNewPCL001.Document;
                            objNewPCL031.Project = objNewPCL001.Project;
                            objNewPCL031.LineId = 0;
                            objNewPCL031.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL032 in objPCL001.PCL032.ToList())
                        {
                            var objNewPCL032 = new PCL032();
                            objNewPCL001.PCL032.Add(objNewPCL032);
                            var objCurrentChildValues = context.Entry(objPCL032).CurrentValues;
                            context.Entry(objNewPCL032).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL032.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL032.CreatedOn = DateTime.Now;
                            objNewPCL032.Document = objNewPCL001.Document;
                            objNewPCL032.Project = objNewPCL001.Project;
                            objNewPCL032.LineId = 0;
                            objNewPCL032.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL033 in objPCL001.PCL033.ToList())
                        {
                            var objNewPCL033 = new PCL033();
                            objNewPCL001.PCL033.Add(objNewPCL033);
                            var objCurrentChildValues = context.Entry(objPCL033).CurrentValues;
                            context.Entry(objNewPCL033).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL033.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL033.CreatedOn = DateTime.Now;
                            objNewPCL033.Document = objNewPCL001.Document;
                            objNewPCL033.Project = objNewPCL001.Project;
                            objNewPCL033.LineId = 0;
                            objNewPCL033.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL034 in objPCL001.PCL034.ToList())
                        {
                            var objNewPCL034 = new PCL034();
                            objNewPCL001.PCL034.Add(objNewPCL034);
                            var objCurrentChildValues = context.Entry(objPCL034).CurrentValues;
                            context.Entry(objNewPCL034).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL034.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL034.CreatedOn = DateTime.Now;
                            objNewPCL034.Document = objNewPCL001.Document;
                            objNewPCL034.Project = objNewPCL001.Project;
                            objNewPCL034.LineId = 0;
                            objNewPCL034.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL035 in objPCL001.PCL035.ToList())
                        {
                            var objNewPCL035 = new PCL035();
                            objNewPCL001.PCL035.Add(objNewPCL035);
                            var objCurrentChildValues = context.Entry(objPCL035).CurrentValues;
                            context.Entry(objNewPCL035).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL035.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL035.CreatedOn = DateTime.Now;
                            objNewPCL035.Document = objNewPCL001.Document;
                            objNewPCL035.Project = objNewPCL001.Project;
                            objNewPCL035.LineId = 0;
                            objNewPCL035.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL036 in objPCL001.PCL036.ToList())
                        {
                            var objNewPCL036 = new PCL036();
                            objNewPCL001.PCL036.Add(objNewPCL036);
                            var objCurrentChildValues = context.Entry(objPCL036).CurrentValues;
                            context.Entry(objNewPCL036).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL036.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL036.CreatedOn = DateTime.Now;
                            objNewPCL036.Document = objNewPCL001.Document;
                            objNewPCL036.Project = objNewPCL001.Project;
                            objNewPCL036.LineId = 0;
                            objNewPCL036.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL037 in objPCL001.PCL037.ToList())
                        {
                            var objNewPCL037 = new PCL037();
                            objNewPCL001.PCL037.Add(objNewPCL037);
                            var objCurrentChildValues = context.Entry(objPCL037).CurrentValues;
                            context.Entry(objNewPCL037).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL037.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL037.CreatedOn = DateTime.Now;
                            objNewPCL037.Document = objNewPCL001.Document;
                            objNewPCL037.Project = objNewPCL001.Project;
                            objNewPCL037.LineId = 0;
                            objNewPCL037.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL038 in objPCL001.PCL038.ToList())
                        {
                            var objNewPCL038 = new PCL038();
                            objNewPCL001.PCL038.Add(objNewPCL038);
                            var objCurrentChildValues = context.Entry(objPCL038).CurrentValues;
                            context.Entry(objNewPCL038).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL038.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL038.CreatedOn = DateTime.Now;
                            objNewPCL038.Document = objNewPCL001.Document;
                            objNewPCL038.Project = objNewPCL001.Project;
                            objNewPCL038.LineId = 0;
                            objNewPCL038.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL039 in objPCL001.PCL039.ToList())
                        {
                            var objNewPCL039 = new PCL039();
                            objNewPCL001.PCL039.Add(objNewPCL039);
                            var objCurrentChildValues = context.Entry(objPCL039).CurrentValues;
                            context.Entry(objNewPCL039).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL039.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL039.CreatedOn = DateTime.Now;
                            objNewPCL039.Document = objNewPCL001.Document;
                            objNewPCL039.Project = objNewPCL001.Project;
                            objNewPCL039.LineId = 0;
                            objNewPCL039.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL040 in objPCL001.PCL040.ToList())
                        {
                            var objNewPCL040 = new PCL040();
                            objNewPCL001.PCL040.Add(objNewPCL040);
                            var objCurrentChildValues = context.Entry(objPCL040).CurrentValues;
                            context.Entry(objNewPCL040).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL040.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL040.CreatedOn = DateTime.Now;
                            objNewPCL040.Document = objNewPCL001.Document;
                            objNewPCL040.Project = objNewPCL001.Project;
                            objNewPCL040.LineId = 0;
                            objNewPCL040.HeaderId = objNewPCL001.HeaderId;
                        }

                        foreach (var objPCL041 in objPCL001.PCL041.ToList())
                        {
                            var objNewPCL041 = new PCL041();
                            objNewPCL001.PCL041.Add(objNewPCL041);
                            var objCurrentChildValues = context.Entry(objPCL041).CurrentValues;
                            context.Entry(objNewPCL041).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPCL041.CreatedBy = objClsLoginInfo.UserName;
                            objNewPCL041.CreatedOn = DateTime.Now;
                            objNewPCL041.Document = objNewPCL001.Document;
                            objNewPCL041.Project = objNewPCL001.Project;
                            objNewPCL041.LineId = 0;
                            objNewPCL041.HeaderId = objNewPCL001.HeaderId;
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region Positioner_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Positioner_Plan.GetStringValue())
                    {
                        //PLN013
                        var objPLN013 = context.PLN013.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewPLN013 = new PLN013();
                        context.PLN013.Add(objNewPLN013);
                        var objCurrentValues = context.Entry(objPLN013).CurrentValues;
                        context.Entry(objNewPLN013).CurrentValues.SetValues(objCurrentValues);
                        objNewPLN013.CreatedBy = objClsLoginInfo.UserName;
                        objNewPLN013.CreatedOn = DateTime.Now;
                        objNewPLN013.Document = objNewPDN002.DocumentNo;
                        objNewPLN013.HeaderId = 0;
                        objNewPLN013.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewPLN013.HeaderId;

                        SourcePath = "PLN013/" + item.RefId + "/" + item.RevNo;
                        DestinationPath = "PLN013/" + objNewPLN013.HeaderId + "/" + objNewPLN013.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });

                        foreach (var objPLN014 in objPLN013.PLN014.ToList())
                        {
                            var objNewPLN014 = new PLN014();
                            objNewPLN013.PLN014.Add(objNewPLN014);
                            var objCurrentChildValues = context.Entry(objPLN014).CurrentValues;
                            context.Entry(objNewPLN014).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPLN014.CreatedBy = objClsLoginInfo.UserName;
                            objNewPLN014.CreatedOn = DateTime.Now;
                            objNewPLN014.Document = objNewPLN013.Document;
                            objNewPLN014.Project = objNewPLN013.Project;
                            objNewPLN014.LineId = 0;

                            //SourcePath = "PLN014/" + item.RefId + "/R" + item.RevNo;
                            //DestinationPath = "PLN014/" + objNewPLN014.HeaderId + "/R" + objNewPLN014.RevNo;
                            //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region Pre_Manufacturing
                    else if (item.Plan == clsImplementationEnum.PlanList.Pre_Manufacturing.GetStringValue())
                    {
                        //PMB001
                        var objPMB001 = context.PMB001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewPMB001 = new PMB001();
                        context.PMB001.Add(objNewPMB001);
                        var objCurrentValues = context.Entry(objPMB001).CurrentValues;
                        context.Entry(objNewPMB001).CurrentValues.SetValues(objCurrentValues);
                        objNewPMB001.CreatedBy = objClsLoginInfo.UserName;
                        objNewPMB001.CreatedOn = DateTime.Now;
                        objNewPMB001.Document = objNewPDN002.DocumentNo;
                        objNewPMB001.HeaderId = 0;
                        objNewPMB001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewPMB001.HeaderId;

                        SourcePath = "PMB001/" + item.RefId + "/R" + item.RevNo;
                        DestinationPath = "PMB001/" + objNewPMB001.HeaderId + "/R" + objNewPMB001.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                    }
                    #endregion

                    #region Reference_Sketch
                    else if (item.Plan == clsImplementationEnum.PlanList.Reference_Sketch.GetStringValue())
                    {
                        //PLN003
                        var objPLN003 = context.PLN003.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewPLN003 = new PLN003();
                        context.PLN003.Add(objNewPLN003);
                        var objCurrentValues = context.Entry(objPLN003).CurrentValues;
                        context.Entry(objNewPLN003).CurrentValues.SetValues(objCurrentValues);
                        objNewPLN003.CreatedBy = objClsLoginInfo.UserName;
                        objNewPLN003.CreatedOn = DateTime.Now;
                        objNewPLN003.Document = objNewPDN002.DocumentNo;
                        objNewPLN003.HeaderId = 0;
                        objNewPLN003.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewPLN003.HeaderId;

                        SourcePath = "PLN003/" + item.RefId + "/" + item.RevNo;
                        DestinationPath = "PLN003/" + objNewPLN003.HeaderId + "/" + objNewPLN003.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });

                        foreach (var objPLN004 in objPLN003.PLN004.ToList())
                        {
                            var objNewPLN004 = new PLN004();
                            objNewPLN003.PLN004.Add(objNewPLN004);
                            var objCurrentChildValues = context.Entry(objPLN004).CurrentValues;
                            context.Entry(objNewPLN004).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPLN004.CreatedBy = objClsLoginInfo.UserName;
                            objNewPLN004.CreatedOn = DateTime.Now;
                            objNewPLN004.Document = objNewPLN003.Document;
                            objNewPLN004.Project = objNewPLN003.Project;
                            objNewPLN004.LineId = 0;

                            //SourcePath = "PLN004/" + item.RefId + "/R" + item.RevNo;
                            //DestinationPath = "PLN004/" + objNewPLN004.HeaderId + "/R" + objNewPLN004.RevNo;
                            //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region Section_Handling_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Section_Handling_Plan.GetStringValue())
                    {
                        //PLN009
                        var objPLN009 = context.PLN009.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewPLN009 = new PLN009();
                        context.PLN009.Add(objNewPLN009);
                        var objCurrentValues = context.Entry(objPLN009).CurrentValues;
                        context.Entry(objNewPLN009).CurrentValues.SetValues(objCurrentValues);
                        objNewPLN009.CreatedBy = objClsLoginInfo.UserName;
                        objNewPLN009.CreatedOn = DateTime.Now;
                        objNewPLN009.Document = objNewPDN002.DocumentNo;
                        objNewPLN009.HeaderId = 0;
                        objNewPLN009.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewPLN009.HeaderId;

                        SourcePath = "PLN009/" + item.RefId + "/" + item.RevNo;
                        DestinationPath = "PLN009/" + objNewPLN009.HeaderId + "/" + objNewPLN009.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });

                        foreach (var objPLN010 in objPLN009.PLN010.ToList())
                        {
                            var objNewPLN010 = new PLN010();
                            objNewPLN009.PLN010.Add(objNewPLN010);
                            var objCurrentChildValues = context.Entry(objPLN010).CurrentValues;
                            context.Entry(objNewPLN010).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPLN010.CreatedBy = objClsLoginInfo.UserName;
                            objNewPLN010.CreatedOn = DateTime.Now;
                            objNewPLN010.Document = objNewPLN009.Document;
                            objNewPLN010.Project = objNewPLN009.Project;
                            objNewPLN010.LineId = 0;

                            //SourcePath = "PLN010/" + item.RefId + "/R" + item.RevNo;
                            //DestinationPath = "PLN010/" + objNewPLN010.HeaderId + "/R" + objNewPLN010.RevNo;
                            //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region Stool_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Stool_Plan.GetStringValue())
                    {
                        //PLN017
                        var objPLN017 = context.PLN017.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewPLN017 = new PLN017();
                        context.PLN017.Add(objNewPLN017);
                        var objCurrentValues = context.Entry(objPLN017).CurrentValues;
                        context.Entry(objNewPLN017).CurrentValues.SetValues(objCurrentValues);
                        objNewPLN017.CreatedBy = objClsLoginInfo.UserName;
                        objNewPLN017.CreatedOn = DateTime.Now;
                        objNewPLN017.Document = objNewPDN002.DocumentNo;
                        objNewPLN017.HeaderId = 0;
                        objNewPLN017.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewPLN017.HeaderId;

                        SourcePath = "PLN017/" + item.RefId + "/" + item.RevNo;
                        DestinationPath = "PLN017/" + objNewPLN017.HeaderId + "/" + objNewPLN017.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                    }
                    #endregion

                    #region Sub_Contracting_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Sub_Contracting_Plan.GetStringValue())
                    {
                        //SCP001
                        var objSCP001 = context.SCP001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewSCP001 = new SCP001();
                        context.SCP001.Add(objNewSCP001);
                        var objCurrentValues = context.Entry(objSCP001).CurrentValues;
                        context.Entry(objNewSCP001).CurrentValues.SetValues(objCurrentValues);
                        objNewSCP001.CreatedBy = objClsLoginInfo.UserName;
                        objNewSCP001.CreatedOn = DateTime.Now;
                        objNewSCP001.Document = objNewPDN002.DocumentNo;
                        objNewSCP001.HeaderId = 0;
                        objNewSCP001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewSCP001.HeaderId;

                        //SourcePath = "SCP001/" + item.RefId + "/R" + item.RevNo;
                        //DestinationPath = "SCP001/" + objNewSCP001.HeaderId + "/R" + objNewSCP001.RevNo;
                        //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });                        
                        foreach (var objSCP002 in objSCP001.SCP002.ToList())
                        {
                            var objNewSCP002 = new SCP002();
                            objNewSCP001.SCP002.Add(objNewSCP002);
                            var objCurrentChildValues = context.Entry(objSCP002).CurrentValues;
                            context.Entry(objNewSCP002).CurrentValues.SetValues(objCurrentChildValues);
                            objNewSCP002.CreatedBy = objClsLoginInfo.UserName;
                            objNewSCP002.CreatedOn = DateTime.Now;
                            objNewSCP002.Document = objNewSCP001.Document;
                            objNewSCP002.Project = objNewSCP001.Project;
                            objNewSCP002.LineId = 0;
                            objNewSCP002.HeaderId = objNewSCP001.HeaderId;

                            //SourcePath = "SCP002/" + item.RefId + "/R" + item.RevNo;
                            //DestinationPath = "SCP002/" + objNewSCP002.HeaderId + "/R" + objNewSCP002.RevNo;
                            //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region Tank_Rotator_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Tank_Rotator_Plan.GetStringValue())
                    {
                        //PLN005
                        var objPLN005 = context.PLN005.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewPLN005 = new PLN005();
                        context.PLN005.Add(objNewPLN005);
                        var objCurrentValues = context.Entry(objPLN005).CurrentValues;
                        context.Entry(objNewPLN005).CurrentValues.SetValues(objCurrentValues);
                        objNewPLN005.CreatedBy = objClsLoginInfo.UserName;
                        objNewPLN005.CreatedOn = DateTime.Now;
                        objNewPLN005.Document = objNewPDN002.DocumentNo;
                        objNewPLN005.HeaderId = 0;
                        objNewPLN005.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewPLN005.HeaderId;

                        SourcePath = "PLN005/" + item.RefId + "/" + item.RevNo;
                        DestinationPath = "PLN005/" + objNewPLN005.HeaderId + "/" + objNewPLN005.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        foreach (var objPLN006 in objPLN005.PLN006.ToList())
                        {
                            var objNewPLN006 = new PLN006();
                            objNewPLN005.PLN006.Add(objNewPLN006);
                            var objCurrentChildValues = context.Entry(objPLN006).CurrentValues;
                            context.Entry(objNewPLN006).CurrentValues.SetValues(objCurrentChildValues);
                            objNewPLN006.CreatedBy = objClsLoginInfo.UserName;
                            objNewPLN006.CreatedOn = DateTime.Now;
                            objNewPLN006.Document = objNewPLN005.Document;
                            objNewPLN006.Project = objNewPLN005.Project;
                            objNewPLN006.LineId = 0;

                            //SourcePath = "PLN006/" + item.RefId + "/R" + item.RevNo;
                            //DestinationPath = "PLN006/" + objNewPLN006.HeaderId + "/R" + objNewPLN006.RevNo;
                            //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region Technical_Process
                    else if (item.Plan == clsImplementationEnum.PlanList.Technical_Process.GetStringValue())
                    {
                        //TLP001
                        var objTLP001 = context.TLP001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewTLP001 = new TLP001();
                        context.TLP001.Add(objNewTLP001);
                        var objCurrentValues = context.Entry(objTLP001).CurrentValues;
                        context.Entry(objNewTLP001).CurrentValues.SetValues(objCurrentValues);
                        objNewTLP001.CreatedBy = objClsLoginInfo.UserName;
                        objNewTLP001.CreatedOn = DateTime.Now;
                        objNewTLP001.Document = objNewPDN002.DocumentNo;
                        objNewTLP001.HeaderId = 0;
                        objNewTLP001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewTLP001.HeaderId;

                        SourcePath = "TLP001/" + item.RefId + "/R" + item.RevNo;
                        DestinationPath = "TLP001/" + objNewTLP001.HeaderId + "/R" + objNewTLP001.RevNo;
                        lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                    }
                    #endregion

                    #region Weld_KG_Plan
                    else if (item.Plan == clsImplementationEnum.PlanList.Weld_KG_Plan.GetStringValue())
                    {
                        //WKG001
                        var objWKG001 = context.WKG001.Where(i => i.HeaderId == item.RefId).FirstOrDefault();
                        var objNewWKG001 = new WKG001();
                        context.WKG001.Add(objNewWKG001);
                        var objCurrentValues = context.Entry(objWKG001).CurrentValues;
                        context.Entry(objNewWKG001).CurrentValues.SetValues(objCurrentValues);
                        objNewWKG001.CreatedBy = objClsLoginInfo.UserName;
                        objNewWKG001.CreatedOn = DateTime.Now;
                        objNewWKG001.Document = objNewPDN002.DocumentNo;
                        objNewWKG001.HeaderId = 0;
                        objNewWKG001.Project = NewProject;
                        context.SaveChanges();
                        objNewPDN002.RefId = objNewWKG001.HeaderId;

                        //SourcePath = "WKG001/" + item.RefId + "/R" + item.RevNo;
                        //DestinationPath = "WKG001/" + objNewWKG001.HeaderId + "/R" + objNewWKG001.RevNo;
                        //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });

                        foreach (var objWKG002 in objWKG001.WKG002.ToList())
                        {
                            var objNewWKG002 = new WKG002();
                            objNewWKG001.WKG002.Add(objNewWKG002);
                            var objCurrentChildValues = context.Entry(objWKG002).CurrentValues;
                            context.Entry(objNewWKG002).CurrentValues.SetValues(objCurrentChildValues);
                            objNewWKG002.CreatedBy = objClsLoginInfo.UserName;
                            objNewWKG002.CreatedOn = DateTime.Now;
                            objNewWKG002.Document = objNewWKG001.Document;
                            objNewWKG002.Project = objNewWKG001.Project;
                            objNewWKG002.LineId = 0;

                            //SourcePath = "WKG002/" + item.RefId + "/R" + item.RevNo;
                            //DestinationPath = "WKG002/" + objNewWKG002.HeaderId + "/R" + objNewWKG002.RevNo;
                            //lstPDINMergeDemergeAttachmentEnt.Add(new PDINMergeDemergeAttachmentEnt { SourcePath = SourcePath, DestinationPath = DestinationPath });
                        }
                        context.SaveChanges();
                    }
                    #endregion

                    #region Department Details
                    var lstPDN003 = context.PDN003.Where(i => i.Project == item.Project && i.DocumentNo == item.DocumentNo).ToList();
                    foreach (var objPDN003 in lstPDN003)
                    {
                        var objNewPDN003 = new PDN003();
                        context.PDN003.Add(objNewPDN003);
                        var objCurrentChildValues = context.Entry(objPDN003).CurrentValues;
                        context.Entry(objNewPDN003).CurrentValues.SetValues(objCurrentChildValues);
                        objNewPDN003.CreatedBy = objClsLoginInfo.UserName;
                        objNewPDN003.CreatedOn = DateTime.Now;
                        objNewPDN003.DocumentNo = Document;
                        objNewPDN003.Project = NewProject;
                    }
                    #endregion

                    context.SaveChanges();
                }
                #endregion

                context.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
        }

        private void CopyDINAttachDocumentsSourceToDestination()
        {
            foreach (var item in lstPDINMergeDemergeAttachmentEnt)
            {
                (new clsFileUpload(item.Type)).CopyFolderContentsAsync(item.SourcePath, item.DestinationPath);
            }
        }

        public string GetFinalIProject(string CurrentIProject, string IProjectMerge, string IProjectDemerge)
        {
            var FinalIProject = "";
            var existIProject = CurrentIProject.Split(',').ToArray();
            var DemergeIProject = IProjectDemerge.Split(',').ToArray();
            var newIProjects = existIProject.Where(i => !DemergeIProject.Contains(i)).ToArray();
            var newIProjectMerge = IProjectMerge.Split(',').ToArray();
            var listIProjectMerge = newIProjectMerge.Union(newIProjects);
            FinalIProject = string.Join(",", listIProjectMerge);
            return FinalIProject;
        }
        #endregion

        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                var lst = db.SP_FETCH_PLANNINGDIN_HEADERS_PRJ_ACCESS(objClsLoginInfo.UserName, 1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from uc in lst
                              select new
                              {
                                  Project = Convert.ToString(uc.Project),
                                  Customer = Convert.ToString(uc.Customer),
                                  CDD = Convert.ToString(uc.CDD != null ? uc.CDD.Value.ToString("dd/MM/yyyy") : string.Empty),
                                  Product = Convert.ToString(uc.Product),
                                  Status = Convert.ToString(uc.Status),
                                  IssueNo = Convert.ToString(uc.IssueNo),
                                  ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                  JobDescription = Convert.ToString(uc.JobDescription),
                                  CreatedBy = Convert.ToString(uc.CreatedBy),
                                  CreatedOn = Convert.ToString(uc.CreatedOn != null ? uc.CreatedOn.Value.ToString("dd/MM/yyyy") : string.Empty),
                                  IssueBy = Convert.ToString(uc.IssueBy),
                                  IssueDate = Convert.ToString(uc.IssueDate != null ? uc.IssueDate.Value.ToString("dd/MM/yyyy") : string.Empty),
                                  LatestDate = Convert.ToString(uc.LatestDate != null ? uc.LatestDate.Value.ToString("dd/MM/yyyy") : string.Empty),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }



    public class ResponceMsgWithValues : clsHelper.ResponseMsg
    {
        public int HeaderID;
        public string Status { get; set; }
    }

    public class IssueNoModel
    {
        public int IssueNo { get; set; }
        public string IssueBy { get; set; }
        public DateTime? IssueDate { get; set; }
        public string t_init { get; set; }
    }

    public class MaxIssueRevModel
    {
        public int IssueNo { get; set; }
        public string DocumentNo { get; set; }
        public int? MaxRevNo { get; set; }
    }

    public enum PDINRole
    {
        Maintainer = 1,
        Approver = 2,
        Shop = 3
    }
    public class PDINEmployee
    {
        public int isActive { get; set; }
        public string psnum { get; set; }
        public string name { get; set; }
        public string department { get; set; }
    }

    public class PDINDocumentListEnt
    {
        public string DocumentNo { get; set; }
        public string Description { get; set; }
        public int? RefId { get; set; }
        public string Plan { get; set; }
    }
    public class PDINDocumentRevNoEnt
    {
        public string Project { get; set; }
        public string DocumentNo { get; set; }
        public int? RevNo { get; set; }
    }
    public class DemergeIProjectEnt
    {
        public string IProject { get; set; }
        public bool IsPrimary { get; set; }
    }

    public class PDINMergeDemergeAttachmentEnt
    {
        private string _type = "";
        public string SourcePath { get; set; }
        public string DestinationPath { get; set; }
        public string Type { get { return _type; } set { _type = value; } }
    }
}