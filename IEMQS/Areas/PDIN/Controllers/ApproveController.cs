﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PDIN.Controllers
{
    public class ApproveController : clsBase
    {
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetHeaderDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderDataPartial");
        }

        [HttpPost]
        public JsonResult LoadDINHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                //if (param.CTQCompileStatus.ToUpper() == "ALL")
                //{
                //    strWhere += "1=1";
                //}
                //else
                //{
                //    strWhere += "1=1";
                //}
                strWhere += (param.CTQCompileStatus == "Pending") ? "LOWER([Status])='" + clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue().ToLower() + "' and LOWER([DocumentStatus])='" + clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue().ToLower() + "' " : " 1=1 ";
                //strWhere += "1=1";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "pdn001.Project+' - '+com1.t_dsca",
                                                "pdn001.Customer+' - '+com6.t_nama",
                                                "[Status]",
                                                "Product",
                                                "ProcessLicensor",
                                                "JobDescription",
                                                "pdn001.CreatedBy +' - '+com003c.t_name",
                                                "pdn001.IssueBy +' - '+com003e.t_name"
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                #region Sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                var lstResult = db.SP_GET_PLANNING_DIN_ALL_DOCUMENTS_PRJ_ACCESS
                                (
                                objClsLoginInfo.UserName, StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var listData = new List<string[]>();
                var distinctRecords = lstResult.Select(i => new { i.ProjectGroupNo, i.HeaderId, i.Project, i.Customer, i.CDD, i.Product, i.LatestDate, i.IssueNo, i.ProjectCode, i.IProject, i.IssueBy, i.IssueDate, i.JobDescription, i.ProcessLicensor, i.Status, i.CreatedBy, i.CreatedOn }).Distinct().ToList();
                foreach (var item in distinctRecords)
                {
                    var listDocuments = lstResult.Where(i => i.HeaderId == item.HeaderId).ToList();
                    var firstDocument = listDocuments.FirstOrDefault();
                    listData.Add(new string[]{
                        "0",
                        GetCollapseExpandButton(param.CTQCompileStatus,item.ProjectGroupNo.Value.ToString()),
                        Convert.ToString(item.Project),
                        Convert.ToString(item.Customer),
                        Convert.ToString(item.CDD!=null ?item.CDD.Value.ToString("dd/MM/yyyy"):string.Empty),
                        Convert.ToString(item.Product),
                        firstDocument!=null?firstDocument.DocumentName:"",
                        firstDocument!=null?(Manager.GenerateLink(firstDocument.DocumentName, firstDocument.DocumentNo + "-R" + firstDocument.DocumentRevNo, Convert.ToInt32(firstDocument.DocumentRefId), true, false, true)
                                    + (param.CTQCompileStatus == "Pending" ? "" : (firstDocument.DocumentName.ToLower() != "Learning Capture".ToLower() ? " [" + firstDocument.DocumentStatus + "]" : ""))):"",
                        Convert.ToString(item.LatestDate!=null?item.LatestDate.Value.ToString("dd/MM/yyyy"):string.Empty),
                        Convert.ToString(item.IssueNo),
                        "<center><a title='View' href='"+WebsiteURL+"/PDIN/Approve/ViewHeader?HeaderID="+Convert.ToInt32(item.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a><a title=\"Print\" onclick=\"PrintPDIN('"+item.ProjectCode+"')\" ><i style='margin-left:5px;' class='fa fa-print'></i></a></center>",
                        Convert.ToString(item.Status)+"#"+Convert.ToString(item.ProcessLicensor)+"#"+
                            Convert.ToString(item.JobDescription)+"#"+Convert.ToString(item.CreatedBy)+"#"+
                            Convert.ToString(item.CreatedOn!=null ?item.CreatedOn.Value.ToString("dd/MM/yyyy"):string.Empty)+"#"+Convert.ToString(item.IssueBy)+"#"+
                            Convert.ToString(item.IssueDate!=null ?item.IssueDate.Value.ToString("dd/MM/yyyy"):string.Empty)+"#"+Convert.ToString(Manager.getPDNIdenticalproject(item.Project.Split('-')[0])),
                        Convert.ToString(item.ProjectGroupNo)
                    });

                    for (int k = 1; k < listDocuments.Count; k++)
                    {
                        var uc = listDocuments[k];
                        listData.Add(
                            new[]
                        {
                            Convert.ToString(uc.DocumentSrNo),
                            "&nbsp;",
                            "&nbsp;",
                            "&nbsp;",
                            "&nbsp;",
                            "&nbsp;",
                            Convert.ToString(uc.DocumentDescription),
                            (Manager.GenerateLink(uc.DocumentName, uc.DocumentNo + "-R" + uc.DocumentRevNo, Convert.ToInt32(uc.DocumentRefId), true, false, true)
                                    + (param.CTQCompileStatus == "Pending" ? "" : (uc.DocumentName.ToLower() != "Learning Capture".ToLower() ? " [" + uc.DocumentStatus + "]" : ""))),
                            "&nbsp;",
                            "&nbsp;",
                            "&nbsp;",
                            Convert.ToString(item.Status) + "#" + Convert.ToString(item.ProcessLicensor) + "#" +
                                    Convert.ToString(item.JobDescription) + "#" + Convert.ToString(item.CreatedBy) + "#" +
                                    Convert.ToString(item.CreatedOn != null ? item.CreatedOn.Value.ToString("dd/MM/yyyy") : string.Empty) + "#" + Convert.ToString(item.IssueBy) + "#" +
                                    Convert.ToString(item.IssueDate != null ? item.IssueDate.Value.ToString("dd/MM/yyyy") : string.Empty) + "#" + Convert.ToString(Manager.getPDNIdenticalproject(item.Project.Split('-')[0])),
                            Convert.ToString(item.ProjectGroupNo),
                        });
                    }
                    //var data = (from uc in lstResult
                    //            where uc.HeaderId == item.HeaderId
                    //            select new[]
                    //           {
                    //           Convert.ToString(uc.DocumentSrNo),
                    //           "&nbsp;",
                    //           "&nbsp;",
                    //           "&nbsp;",
                    //           "&nbsp;",
                    //           "&nbsp;",
                    //           Convert.ToString(uc.DocumentDescription),
                    //           (Manager.GenerateLink(uc.DocumentName, uc.DocumentNo+"-R"+uc.DocumentRevNo, Convert.ToInt32(uc.DocumentRefId), true,false,true)
                    //                +(param.CTQCompileStatus == "Pending" ? "" : (uc.DocumentName.ToLower()!="Learning Capture".ToLower() ? " ["+uc.DocumentStatus+"]" : ""))),
                    //           "&nbsp;",
                    //           "&nbsp;",
                    //           "&nbsp;",
                    //           Convert.ToString(item.Status)+"#"+Convert.ToString(item.ProcessLicensor)+"#"+
                    //                Convert.ToString(item.JobDescription)+"#"+Convert.ToString(item.CreatedBy)+"#"+
                    //                Convert.ToString(item.CreatedOn!=null ?item.CreatedOn.Value.ToString("dd/MM/yyyy"):string.Empty)+"#"+Convert.ToString(item.IssueBy)+"#"+
                    //                Convert.ToString(item.IssueDate!=null ?item.IssueDate.Value.ToString("dd/MM/yyyy"):string.Empty)+"#"+Convert.ToString(Manager.getPDNIdenticalproject(item.Project.Split('-')[0])),
                    //           Convert.ToString(item.ProjectGroupNo),
                    //       }).ToList();

                    //listData.AddRange(data);
                }



                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = listData,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        private string GetCollapseExpandButton(string Status, string ProjectGroupNo)
        {
            var imagebutton = "";
            //if (Status == "Pending")
            //{
            //    imagebutton = ("<img data-gid=\"" + ProjectGroupNo + "\" onclick=\"ExpandCollapsChild(this)\" class=\"colexp\" src=\"/Images/details_close.png\" style=\"height:17px;\" data-isexpand=true />" +
            //                 "<img data-gid=\"" + ProjectGroupNo + "\" onclick=\"ExpandCollapsChild(this)\" class=\"colexp\" src=\"/Images/details_open.png\" style=\"height:17px;\" data-isexpand=false />");
            //}
            //else
            //{
            //    imagebutton = ("<img data-gid=\"" + ProjectGroupNo + "\" onclick=\"ExpandCollapsChild(this)\" class=\"colexp\" src=\"/Images/details_close.png\" style=\"height:17px;\" data-isexpand=false />" +
            //                    "<img data-gid=\"" + ProjectGroupNo + "\" onclick=\"ExpandCollapsChild(this)\" class=\"colexp\" src=\"/Images/details_open.png\" style=\"height:17px;\" data-isexpand=true />");
            //}
            if (Status == "Pending")
            {
                imagebutton = "<img data-gid=\"" + ProjectGroupNo + "\" onclick=\"ExpandCollapsChild(this)\" class=\"colexp\" src=\"/Images/details_close.png\" style=\"height:17px;\" data-isexpand=\"true\" />";
            }
            else
            {
                imagebutton = "<img data-gid=\"" + ProjectGroupNo + "\" onclick=\"ExpandCollapsChild(this)\" class=\"colexp\" src=\"/Images/details_open.png\" style=\"height:17px;\" data-isexpand=\"false\" />";
            }
            return imagebutton;
        }

        [SessionExpireFilter]
        public ActionResult ViewHeader(int HeaderID = 0)
        {
            PDN001 objPDN001 = db.PDN001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (HeaderID > 0 && objPDN001 == null)
            {
                return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
            }
            ViewBag.HeaderID = HeaderID;
            return View(objPDN001);
        }

        [HttpPost]
        public ActionResult LoadHeaderFormPartial(int HeaderID)
        {
            PDN001 objPDN001 = db.PDN001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPDN001 != null && objPDN001.HeaderId > 0)
            {
                var projectDetails = db.COM001.Where(x => x.t_cprj == objPDN001.Project).Select(x => new { x.t_dsca, x.t_cprj }).FirstOrDefault();
                ViewBag.Project = projectDetails.t_cprj + " - " + projectDetails.t_dsca;
                string customerName = db.COM006.Where(x => x.t_bpid == objPDN001.Customer).Select(x => x.t_nama).FirstOrDefault();
                ViewBag.customerName = objPDN001.Customer + " - " + customerName;
            }
            else
            {
                objPDN001 = new PDN001();
                int? maxIssueNo = db.PDN001.Select(x => x.IssueNo).FirstOrDefault();
                objPDN001.IssueNo = (maxIssueNo != null ? Convert.ToInt32(maxIssueNo + 1) : 1);
                objPDN001.Status = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue();
            }
            if (objPDN001.Location != null && objPDN001.Location != string.Empty)
            {
                ViewBag.Location = db.COM002.Where(i => i.t_dimx == objPDN001.Location && i.t_dtyp == 1).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
            }
            ViewBag.Products = clsImplementationEnum.getProductCategory().ToArray();
            return PartialView("_LoadHeaderFormPartial", objPDN001);
        }

        [HttpPost]
        public JsonResult LoadDINHeaderLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] arrayLikeCon = {
                                                "Description",
                                                "[Status]",
                                            };
                    strWhere += arrayLikeCon.MakeDatatableSearchCondition(param.sSearch);
                }

                int CTQHeaderId = Convert.ToInt32(param.CTQHeaderId);
                string HeaderStatus = string.Empty;
                var objPDN001 = db.PDN001.FirstOrDefault(i => i.HeaderId == CTQHeaderId);
                if (objPDN001 != null)
                {
                    HeaderStatus = db.PDN001.FirstOrDefault(i => i.HeaderId == CTQHeaderId).Status;
                }
                else
                {
                    HeaderStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue().ToLower();
                }

                var items = db.FN_GET_LOCATIONWISE_DEPARTMENT_PDIN(objClsLoginInfo.Location, "", -1).Select(x => new SelectItemList { id = x.t_dimx, text = x.t_dimx + "-" + x.t_desc, }).ToList();

                // strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName);
                var lstResult = db.SP_FETCH_PLANNINGDIN_HEADERS_LINES
                                (
                               StartIndex,
                               EndIndex,
                               "",
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.Description),
                                Convert.ToString(uc.Status),
                                //Helper.MultiSelectDropdown(items,uc.Department,(HeaderStatus.ToLower() == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue().ToLower()),"UpdateDepartment(this,'"+Convert.ToString(uc.LineId)+"')","Department"),
                                Helper.MultiSelectDropdown(items,uc.Department,true,"UpdateDepartment(this,'"+Convert.ToString(uc.LineId)+"')","Department"),
                                //GetTragetDateTextBox(uc.LineId,(HeaderStatus.ToLower() == clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue().ToLower()),(uc.TargetDate!= null ? uc.TargetDate.Value.ToString("dd/MM/yyyy") : string.Empty),"TargetDate"),
                                GetTragetDateTextBox(uc.LineId,true,(uc.TargetDate!= null ? uc.TargetDate.Value.ToString("dd/MM/yyyy") : string.Empty),"TargetDate"),
                                ((uc.Status != null && uc.Status.Trim().ToLower().Equals("draft")) ? uc.DocumentNo: Manager.GenerateLink(uc.PlanName,uc.DocumentNo,Convert.ToInt32(uc.RefId),true)),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    isDisplayButton = (lstResult.Count > 0 ? (lstResult.Where(x => x.Status == "Approved").Count() > 0 ? true : false) : false)
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = 0,
                    iTotalDisplayRecords = 0,
                    aaData = "",
                    isDisplayButton = false
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ReleaseDin(int HeaderID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PDN001 objPDN001 = db.PDN001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();
            if (objPDN001 != null)
            {
                objPDN001.Status = clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue();
                List<PDN002> lstPDN002 = db.PDN002.Where(x => x.HeaderId == objPDN001.HeaderId).ToList();

                PDN001 newObjPDN001 = db.PDN001.Add(new PDN001
                {
                    Project = objPDN001.Project,
                    IssueNo = objPDN001.IssueNo + 1,
                    Customer = objPDN001.Customer,
                    CDD = objPDN001.CDD,
                    Status = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue(),
                    Product = objPDN001.Product,
                    ProcessLicensor = objPDN001.ProcessLicensor,
                    JobDescription = objPDN001.JobDescription,
                    Department = objPDN001.Department,
                    CreatedBy = objPDN001.CreatedBy,
                    CreatedOn = DateTime.Now,
                    IssueBy = objClsLoginInfo.UserName, // It's Same as created by and date after discussion with satish
                    IssueDate = DateTime.Now,
                });

                db.SaveChanges();
                lstPDN002.ForEach(x => { x.HeaderId = newObjPDN001.HeaderId; x.IssueNo = newObjPDN001.IssueNo; });
                db.PDN002.AddRange(lstPDN002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Din successfully released.";
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Din not available.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string GetTragetDateTextBox(int LineId, bool Disabled, string Date, string ColumnName)
        {
            string rtn = string.Empty;
            try
            {
                //rtn += "<div id='datetime1' class='input-group date date-picker' data-date-format='dd/mm/yyyy' data-date-start-date='+0d'>";
                rtn += "<input type='text' class='form-control' id='txtTargetDate' value='" + Date + "' colname='" + ColumnName + "' " + (Disabled ? "disabled " : "") + " ></input>";
                //rtn += "<span class='input-group-btn'>";
                //rtn += "<button class='btn default' id='btncal1' type='button'>";
                //rtn += "<i class='fa fa-calendar'></i>";
                //rtn += "</button></span></div>";

            }
            catch (Exception)
            {

                throw;
            }
            return rtn;
        }

    }
}