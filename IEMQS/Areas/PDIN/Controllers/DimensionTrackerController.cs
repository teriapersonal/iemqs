﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PDIN.Controllers
{
    public class DimensionTrackerController : clsBase
    {
        // GET: PDIN/DimensionTracker
        #region Parent Dimension Tracker Controller
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Shell_Shrinkage_Data()
        {
            return View();
        }
        public ActionResult LoadDataTableShell_Shrinkage_Data(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                //string qualityProject = param.QualityProject;
                //string seamNumber = param.SeamNo;
                string whereCondition = "";

                //string whereCondition = "1=1 and qms31.ProtocolType is not null ";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms31.BU", "qms31.Location");
                
                //string[] columnName = { "qms31.SeamNo", "StageSequance", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms31.StageCode, qms31.BU, qms31.Location) ", "ProtocolType" };
                //if (!string.IsNullOrWhiteSpace(param.sSearch))
                //{
                //    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                //}
                //else
                //{
                //    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                //}
                //if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(seamNumber))
                //{
                //    whereCondition += (seamNumber.Equals("ALL") ? " AND qms31.QualityProject = '" + qualityProject + "' " : " AND qms31.QualityProject = '" + qualityProject + "' AND qms31.SeamNo ='" + seamNumber + "'");
                //}

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                //var lstHeader = db.SP_IPI_DISPLAY_PROTOCOL_LIST_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var lstHeader = db.SP_PDIN_DIMENSION_TRACKER_SHELL_SHRINKAGE(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                                select new[] {
                                Convert.ToString(h.Project),
                               Convert.ToString(h.Customer),
                               Convert.ToString(h.SeamNo),
                               Convert.ToString(h.Position),
                               Convert.ToString(h.PartDescription),//Part Description
                               Convert.ToString(h.Material),
                               Convert.ToString(h.Thickness),
                               Convert.ToString(h.ReqOutOfRoundness),
                               Convert.ToString(h.ReqOdCF),
                               Convert.ToString(h.ActualODCFBeforeTOP),
                               Convert.ToString(h.ActualODCFBeforeMIDDLE),
                               Convert.ToString(h.ActualODCFBeforeBOTTOM),
                               Convert.ToString(h.ActualODCFAfterTOP),
                               Convert.ToString(h.ActualODCFAfterMIDDLE),
                               Convert.ToString(h.ActualODCFAfterBOTTOM),
                               Convert.ToString(h.ActualShrinkageTOP),
                               Convert.ToString(h.ActualShrinkageMIDDLE),
                               Convert.ToString(h.ActualShrinkageBOTTOM),
                               Convert.ToString(h.PercentageShrinkageCFTOP),
                               Convert.ToString(h.PercentageShrinkageCFMIDDLE),
                               Convert.ToString(h.PercentageShrinkageCFBOTTOM),
                               Convert.ToString(h.PercentageShrinkageCFAVERAGE),
                               Convert.ToString(h.ReqTotalShellHeight),
                               Convert.ToString(h.ActualLengthBeforeOLAT0),
                               Convert.ToString(h.ActualLengthBeforeOLAT90),
                               Convert.ToString(h.ActualLengthBeforeOLAT180),
                               Convert.ToString(h.ActualLengthBeforeOLAT270),
                               Convert.ToString(h.ActualLengthAfterOLAT0),
                               Convert.ToString(h.ActualLengthAfterOLAT90),
                               Convert.ToString(h.ActualLengthAfterOLAT180),
                               Convert.ToString(h.ActualLengthAfterOLAT270),
                               Convert.ToString(h.ActualShrinkageAT0),
                               Convert.ToString(h.ActualShrinkageAT90),
                               Convert.ToString(h.ActualShrinkageAT180),
                               Convert.ToString(h.ActualShrinkageAT270),
                               Convert.ToString(h.PercentShrinkageInLength0),
                               Convert.ToString(h.PercentShrinkageInLength90),
                               Convert.ToString(h.PercentShrinkageInLength180),
                               Convert.ToString(h.PercentShrinkageInLength270),
                               Convert.ToString(h.OvalityAfterOLTOP),
                               Convert.ToString(h.OvalityAfterOLBottom),
                               Convert.ToString(h.WidthOfEsscStripUsedForOverlay),
                               Convert.ToString(h.ESSCsequence),
                      };
                //      select new[] {
                //           Convert.ToString(h.SeamNo),
                //           Convert.ToString(h.StageCode),
                //           Convert.ToString(h.StageSequance),
                //           Convert.ToString(h.ProtocolDescription),
                //           Convert.ToString(h.ProtocolType),
                //           Convert.ToString(h.ProtocolId),
                //           Convert.ToString(h.ProtocolLinkedBy),
                //           Convert.ToString(h.ProtocolLinkedOn),
                //        (!string.IsNullOrWhiteSpace(h.ProtocolType) ? (h.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? ("&nbsp;&nbsp;<a target=\"_blank\" onclick=\"ShowOtherFiles("+ h.LineId +")\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>") : ("&nbsp;&nbsp;<a target=\"_blank\"  href=\"" + WebsiteURL + "/PROTOCOL/"+h.ProtocolType+"/Linkage/"+h.ProtocolId + "" +"\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>")) : "<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>")

                //};
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult Head_Shrinkage_Data()
        {
            return View();
        }
        public ActionResult Shell_Ovality_Data()
        {
            return View();
        }
        public ActionResult Shell_Length_Data()
        {
            return View();
        }
        public ActionResult Nozzle_Elevation_Data()
        {
            ViewBag.Title = "Nozzle_Elevation_Data";
            return View();
        }
        public ActionResult LoadDataTableNozzle_Elevation_Data(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string Project = param.Project;
                //string seamNumber = param.SeamNo;
                string whereCondition = "";

                //string whereCondition = "1=1 and qms31.ProtocolType is not null ";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms31.BU", "qms31.Location");

                //string[] columnName = { "qms31.SeamNo", "StageSequance", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms31.StageCode, qms31.BU, qms31.Location) ", "ProtocolType" };
                //if (!string.IsNullOrWhiteSpace(param.sSearch))
                //{
                //    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                //}
                //else
                //{
                //    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                //}
                //if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(seamNumber))
                //{
                //    whereCondition += (seamNumber.Equals("ALL") ? " AND qms31.QualityProject = '" + qualityProject + "' " : " AND qms31.QualityProject = '" + qualityProject + "' AND qms31.SeamNo ='" + seamNumber + "'");
                //}

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                //var lstHeader = db.SP_IPI_DISPLAY_PROTOCOL_LIST_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                var lstHeader = db.SP_PDIN_DIMENSION_TRACKER_NOZZLE_ELEVATION(StartIndex, EndIndex, strSortOrder, whereCondition,Convert.ToString(Project)).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                string Customer = lstHeader.Select(i => i.Customer).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                                Convert.ToString(h.SeamNo),
                               Convert.ToString(h.Position),
                               Convert.ToString(h.PartDescription),//Part Description
                               Convert.ToString(h.ShellDendOverlay),
                               Convert.ToString(h.RequiredValue),
                               Convert.ToString(h.ActualValue),
                      };
                //      select new[] {
                //           Convert.ToString(h.SeamNo),
                //           Convert.ToString(h.StageCode),
                //           Convert.ToString(h.StageSequance),
                //           Convert.ToString(h.ProtocolDescription),
                //           Convert.ToString(h.ProtocolType),
                //           Convert.ToString(h.ProtocolId),
                //           Convert.ToString(h.ProtocolLinkedBy),
                //           Convert.ToString(h.ProtocolLinkedOn),
                //        (!string.IsNullOrWhiteSpace(h.ProtocolType) ? (h.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? ("&nbsp;&nbsp;<a target=\"_blank\" onclick=\"ShowOtherFiles("+ h.LineId +")\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>") : ("&nbsp;&nbsp;<a target=\"_blank\"  href=\"" + WebsiteURL + "/PROTOCOL/"+h.ProtocolType+"/Linkage/"+h.ProtocolId + "" +"\" Title=\"View Protocol Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>")) : "<a Title=\"No Protocol Attached\" ><i style=\"opacity:0.3;\" class=\"fa fa-file-text\"></i></a>")

                //};
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    Customer= Customer
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

    }
}