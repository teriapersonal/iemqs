﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.TEMP.Controllers
{
    public class TempAHController : Controller
    {
        // GET: TEMP/TempAH
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadSidebarPartial()
        {
            return PartialView("_SidebarPartial");
        }
        [HttpPost]
        public ActionResult PageActionPartial()
        {
            return PartialView("_PageActionPartial");
        }
        [HttpPost]
        public ActionResult RightSidebarPartial()
        {
            return PartialView("_RightSidebarPartial");
        }
    }
}