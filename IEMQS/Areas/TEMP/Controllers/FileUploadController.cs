﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
using Microsoft.SharePoint.Client;
using Shp = Microsoft.SharePoint.Client;
using System.Text;

namespace IEMQS.Areas.TEMP.Controllers
{
    public class FileUploadController : clsBase
    {
        // GET: TEMP/FileUpload
        public ActionResult Index()
        {
            return View();
        }

        [SessionExpireFilter]
        public ActionResult tempupload()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetAttachmentDetails(string folderPath, bool includeFromSubfolders = false, string Type = "Normal")
        {
            if (string.IsNullOrWhiteSpace(folderPath))
                return Json("", JsonRequestBehavior.AllowGet);
            Type = (Type == "Normal" ? "" : Type);
            var Files = (new clsFileUpload(Type)).GetDocuments(folderPath, includeFromSubfolders);
            return Json(Files, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RemoveDeletedFile(string folderPath, string[] toDelete, string Type = "Normal")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objATH008 = Manager.GetATHObject(Type);
                foreach (var item in toDelete)
                {
                    var FileLibraryPath = System.Configuration.ConfigurationManager.AppSettings["IEMQSDOC_Path"];
                    var path = FileLibraryPath + folderPath + "/" + item;
                    //if (System.IO.File.Exists(path))
                    //{
                    clsUploadCopy.DeleteFile(folderPath, item, objATH008);
                    //}
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "File Removed Successfully!";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public HttpResponseMessage UploadFile()
        {
            try
            {
                foreach (string file in Request.Files)
                {
                    var FileDataContent = Request.Files[file];
                    var FolderPath = Request.Form["folderpath"];
                    var comment = Request.Form["comments"];
                    bool onFTP = Request.Form["onFTP"] != null ? Convert.ToBoolean(Request.Form["onFTP"]) : false;
                    if (FileDataContent != null && FileDataContent.ContentLength > 0)
                    {
                        // take the input stream, and save it to a temp folder using the original file.part name posted
                        var stream = FileDataContent.InputStream;
                        var fileName = Path.GetFileName(FileDataContent.FileName);
                        var FileLibraryPath = System.Configuration.ConfigurationManager.AppSettings["IEMQSDOC_Path"];
                        //var UploadPath = Server.MapPath("~/App_Data/"+ FolderPath);
                        string mergePath = string.Empty;
                        if (FileLibraryPath != null)
                        {
                            mergePath = FileLibraryPath + FolderPath + "/";
                        }

                        var UploadPath = Server.MapPath("~/App_Data/" + FolderPath);

                        Directory.CreateDirectory(UploadPath);
                        string path = Path.Combine(UploadPath, fileName);
                        try
                        {
                            if (System.IO.File.Exists(path))
                                System.IO.File.Delete(path);
                            using (var fileStream = System.IO.File.Create(path))
                            {
                                try
                                {
                                    stream.CopyTo(fileStream);
                                }
                                catch (Exception ex)
                                {
                                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                    throw ex;
                                }
                            }
                            // Once the file part is saved, see if we have enough to merge it
                            ChunkMergeUtility cmUtility = new ChunkMergeUtility();
                            try
                            {
                                var IsFileMerge = cmUtility.MergeFile(path, mergePath, FolderPath, objClsLoginInfo.UserName, onFTP, comment);
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                throw ex;
                            }
                            //var IsFileMerge = cmUtility.MergeFile(path, mergePath, FolderPath, "90348363", comment);
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            throw ex;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }

            return new HttpResponseMessage()
            {
                StatusCode = HttpStatusCode.OK,
                Content = new StringContent("File uploaded.")
            };
        }

        public ActionResult GetFilesFromMultipleFolder()
        {
            List<string> folders = new List<string>();
            folders.Add("FKM111/45/634/R0");
            folders.Add("FKm118/45/634/1392");
            List<SharePointFile> Files = new List<SharePointFile>();
            List<SharePointFile> OtherFoldersFiles = new List<SharePointFile>();
            foreach (var f in folders)
            {
                OtherFoldersFiles = (new clsFileUpload()).GetDocuments(f, false, "").ToList();
                Files.AddRange(OtherFoldersFiles);
            }
           
            return Json(Files.ToArray(), JsonRequestBehavior.AllowGet);
        }
    }

    public class FileUploadEnt
    {
        int chunkSize = 2;
        int chunkInterval = 0;
        public FileUploadEnt(string ControlID, string folderPath)
        {
            this.FolderPath = folderPath;
            this.ControlID = ControlID;
            this.Type = "Normal";
            this.ShowDeleteAction = false;
            this.MaxChunkSizeInMB = 2;
            this.ShowAttachmentBtn = true;
            this.IsDisabled = false;
            this.ReloadAfterUpload = true;
            this.IsRequired = false;
            this.AllowFileExtension = "*";
            //this.ChunkIntervalInSec = this.MaxChunkSizeInMB;
        }

        public string ControlID { get; set; }
        public string ActionURL { get; set; }
        public string FolderPath { get; set; }
        public string Type { get; set; }

        public bool ShowDeleteAction { get; set; }
        public int MaxChunkSizeInMB
        {
            get { return chunkSize; }
            set { this.chunkSize = value; }
        }
        public bool ShowAttachmentBtn { get; set; }
        public int ChunkIntervalInSec
        {
            get
            {
                if (this.chunkInterval == 0)
                {
                    return 1;
                }
                else
                {
                    return chunkInterval;
                }
            }
            set
            {
                this.chunkInterval = value;
            }
        }

        public bool IsDisabled { get; set; }

        public bool ReloadAfterUpload { get; set; }
        public bool IsRequired { get; set; }
        public string AllowFileExtension { get; set; }
    }



    class ChunkMergeUtility
    {
        public string FileName { get; set; }
        public string TempFolder { get; set; }
        public int MaxFileSizeMB { get; set; }
        public List<String> FileParts { get; set; }

        public ChunkMergeUtility()
        {
            FileParts = new List<string>();
        }

        /// <summary>
        /// original name + ".part_N.X" (N = file part number, X = total files)
        /// Objective = enumerate files in folder, look for all matching parts of split file. If found, merge and return true.
        /// </summary>
        /// <param name="FileName"></param>
        /// <returns></returns>
        public bool MergeFile(string FileName, string mergePath, string FolderPath, string uploader, bool onFTP, string comment = "")
        {
            bool rslt = false;
            try
            {
                // parse out the different tokens from the filename according to the convention
                string partToken = ".part_";
                string baseFileName = FileName.Substring(0, FileName.IndexOf(partToken));
                string trailingTokens = FileName.Substring(FileName.IndexOf(partToken) + partToken.Length);
                int FileIndex = 0;
                int FileCount = 0;
                int.TryParse(trailingTokens.Substring(0, trailingTokens.IndexOf(".")), out FileIndex);
                int.TryParse(trailingTokens.Substring(trailingTokens.IndexOf(".") + 1), out FileCount);
                // get a list of all file parts in the temp folder
                string Searchpattern = Path.GetFileName(baseFileName) + partToken + "*";
                string[] FilesList = Directory.GetFiles(Path.GetDirectoryName(FileName), Searchpattern);
                //  merge .. improvement would be to confirm individual parts are there / correctly in sequence, a security check would also be important
                // only proceed if we have received all the file chunks

                if (FilesList.Count() == FileCount)
                {
                    // use a singleton to stop overlapping processes
                    if (!MergeFileManager.Instance.InUse(baseFileName))
                    {
                        MergeFileManager.Instance.AddFile(baseFileName);
                        var MergeFileName = mergePath + baseFileName.Split('\\').Last();

                        try
                        {
                            if (System.IO.File.Exists(MergeFileName))
                                System.IO.File.Delete(MergeFileName);
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            throw ex;
                        }

                        try
                        {
                            // add each file located to a list so we can get them into 
                            // the correct order for rebuilding the file
                            List<SortedFile> MergeList = new List<SortedFile>();
                            try
                            {
                                foreach (string File in FilesList)
                                {
                                    SortedFile sFile = new SortedFile();
                                    sFile.FileName = File;
                                    baseFileName = File.Substring(0, File.IndexOf(partToken));
                                    trailingTokens = File.Substring(File.IndexOf(partToken) + partToken.Length);
                                    int.TryParse(trailingTokens.Substring(0, trailingTokens.IndexOf(".")), out FileIndex);
                                    sFile.FileOrder = FileIndex;
                                    MergeList.Add(sFile);
                                }
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                throw ex;
                            }


                            // sort by the file-part number to ensure we merge back in the correct order
                            var MergeOrder = MergeList.OrderBy(s => s.FileOrder).ToList();

                            #region Merge chunk file in Stream and save using Sharepoint Library
                            using (Stream stream = new MemoryStream())
                            {
                                try
                                {
                                    if (onFTP)
                                        FtpCreateFolder(FolderPath, baseFileName.Split('\\').Last());

                                    foreach (var chunk in MergeOrder)
                                    {
                                        if (onFTP)
                                        {
                                            try
                                            {
                                                UploadFileToFtp(FolderPath, baseFileName.Split('\\').Last(), chunk.FileName);
                                                byte[] buffer = Encoding.ASCII.GetBytes(baseFileName.Split('\\').Last());
                                                stream.Write(buffer, 0, buffer.Length);
                                            }
                                            catch (Exception ex)
                                            {
                                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                                throw ex;
                                            }

                                        }
                                        else
                                        {
                                            try
                                            {
                                                using (FileStream fileChunk = new FileStream(chunk.FileName, FileMode.Open))
                                                {
                                                    fileChunk.CopyTo(stream);
                                                }
                                            }
                                            catch (IOException ex)
                                            {
                                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                                throw ex;

                                            }
                                        }
                                    }
                                    var UploadFileName = baseFileName.Split('\\').Last();
                                    clsManager Manager = new clsManager();
                                    try
                                    {
                                        clsUploadCopy.FileUpload(UploadFileName, FolderPath, Manager.GetATHObject("Normal"), stream, uploader, comment, onFTP);
                                    }
                                    catch (Exception ex)
                                    {
                                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                        throw ex;
                                    }
                                }
                                catch (Exception ex)
                                {
                                    Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                                    throw ex;
                                }
                                finally
                                {
                                    foreach (var chunk in MergeOrder)
                                    {
                                        if (System.IO.File.Exists(chunk.FileName))
                                        {
                                            System.IO.File.Delete(chunk.FileName);
                                        }
                                    }
                                }
                            }

                            #endregion
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            throw ex;
                        }
                        #region Old Code with append File 
                        //using (FileStream FS = new FileStream(MergeFileName, FileMode.Append))
                        //{
                        //    try
                        //    {
                        //        // merge each file chunk back into one contiguous file stream
                        //        foreach (var chunk in MergeOrder)
                        //        {
                        //            try
                        //            {
                        //                using (FileStream fileChunk = new FileStream(chunk.FileName, FileMode.Open))
                        //                {
                        //                    fileChunk.CopyTo(FS);
                        //                }
                        //            }
                        //            catch (IOException ex)
                        //            {
                        //                // handle                                
                        //            }
                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {

                        //    }
                        //    finally
                        //    {
                        //        foreach (var chunk in MergeOrder)
                        //        {
                        //            if (System.IO.File.Exists(chunk.FileName))
                        //            {
                        //                System.IO.File.Delete(chunk.FileName);
                        //            }
                        //        }
                        //    }
                        //}
                        #endregion

                        rslt = true;
                        // unlock the file from singleton
                        MergeFileManager.Instance.RemoveFile(baseFileName);
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw ex;
            }
            return rslt;
        }

        private void UploadFileToFtp(string folderPath, string fileName, string localFile)
        {
            //string host = "ftp://localhost";
            //string user = @"LTHED\20036413";
            //string pass = "Abhi@2018";
            FtpWebRequest ftpRequest = null;
            Stream ftpStream = null;
            try
            {
                clsUploadCopy.FTPDetailEnt ftpEnt = null;
                ftpEnt = clsUploadCopy.GetFTPDetails();

                /* Create an FTP Request */
                ftpRequest = (FtpWebRequest)FtpWebRequest.Create(ftpEnt.Host + "/" + folderPath + "/" + fileName);
                /* Log in to the FTP Server with the User Name and Password Provided */
                ftpRequest.Credentials = new NetworkCredential(ftpEnt.UserName, ftpEnt.Password);


                /* When in doubt, use these options */
                ftpRequest.UseBinary = true;
                ftpRequest.UsePassive = true;
                ftpRequest.KeepAlive = true;
                /* Specify the Type of FTP Request */

                ftpRequest.Method = WebRequestMethods.Ftp.AppendFile;
                /* Establish Return Communication with the FTP Server */
                ftpStream = ftpRequest.GetRequestStream();
                /* Open a File Stream to Read the File for Upload */
                FileStream localFileStream = new FileStream(localFile, FileMode.Open);
                /* Buffer for the Downloaded Data */
                byte[] byteBuffer = new byte[localFileStream.Length];
                int bytesSent = localFileStream.Read(byteBuffer, 0, byteBuffer.Length);
                /* Upload the File by Sending the Buffered Data Until the Transfer is Complete */
                try
                {
                    //while (bytesSent != 0)
                    //{
                    ftpStream.Write(byteBuffer, 0, byteBuffer.Length);
                    //bytesSent = localFileStream.Read(byteBuffer, 0, bufferSize);
                    //}
                }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                /* Resource Cleanup */
                localFileStream.Close();
                ftpStream.Close();
                ftpRequest = null;
            }
            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            return;

        }
        private void FtpCreateFolder(string folderPath, string fileName)
        {
            var ftpEnt = clsUploadCopy.GetFTPDetails();

            string Path = string.Empty;
            foreach (var item in folderPath.Split('/'))
            {
                Path += "/" + item;

                try
                {

                    FtpWebRequest request = (FtpWebRequest)WebRequest.Create(ftpEnt.Host + Path);
                    request.Method = WebRequestMethods.Ftp.ListDirectory;
                    request.Credentials = new NetworkCredential(ftpEnt.UserName, ftpEnt.Password);

                    using (FtpWebResponse response = (FtpWebResponse)request.GetResponse())
                    {
                        if (item == fileName)
                        {
                            FtpWebRequest requestDelete = (FtpWebRequest)WebRequest.Create(ftpEnt.Host + Path);
                            request.Method = WebRequestMethods.Ftp.DeleteFile;
                            request.Credentials = new NetworkCredential(ftpEnt.UserName, ftpEnt.Password);
                            using (FtpWebResponse responseDelete = (FtpWebResponse)request.GetResponse())
                            {
                            }
                        }
                        continue;
                    }
                }
                catch (WebException ex)
                {
                    if (ex.Response != null)
                    {
                        FtpWebResponse response = (FtpWebResponse)ex.Response;
                        if (response.StatusCode == FtpStatusCode.ActionNotTakenFileUnavailable)
                        {
                            WebRequest request = WebRequest.Create(ftpEnt.Host + Path);
                            request.Method = WebRequestMethods.Ftp.MakeDirectory;
                            request.Credentials = new NetworkCredential(ftpEnt.UserName, ftpEnt.Password);
                            using (var resp = (FtpWebResponse)request.GetResponse())
                            {
                                continue;
                            }
                        }
                    }
                }
            }

        }
    }

    public struct SortedFile
    {
        public int FileOrder { get; set; }
        public String FileName { get; set; }
    }

    public class MergeFileManager
    {
        private static MergeFileManager instance;
        private List<string> MergeFileList;

        private MergeFileManager()
        {
            try
            {
                MergeFileList = new List<string>();
            }
            catch { }
        }

        public static MergeFileManager Instance
        {
            get
            {
                if (instance == null)
                    instance = new MergeFileManager();
                return instance;
            }
        }

        public void AddFile(string BaseFileName)
        {
            MergeFileList.Add(BaseFileName);
        }

        public bool InUse(string BaseFileName)
        {
            return MergeFileList.Contains(BaseFileName);
        }

        public bool RemoveFile(string BaseFileName)
        {
            return MergeFileList.Remove(BaseFileName);
        }
    }

}