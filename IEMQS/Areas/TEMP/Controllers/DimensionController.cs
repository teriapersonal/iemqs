﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;
//using Microsoft.Office.Interop.Excel;
using Microsoft.SharePoint.Client;
using Shp = Microsoft.SharePoint.Client;
using System.Text;
using Ionic.Zip;
using System.IO.Compression;
using Microsoft.ReportingServices.Diagnostics.Internal;
using static System.Net.Mime.MediaTypeNames;
using System.Collections;
using System.Diagnostics;
using iTextSharp.text;
using System.Drawing.Printing;


namespace IEMQS.Areas.TEMP.Controllers
{
    public class DimensionController : clsBase
    {
        // GET: TEMP/Dimension
        public ActionResult Index()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult FormControl()
        {
            return View();
        }
        public ActionResult copyfile()
        {
            return View();
        }
        public ActionResult copyDemo()
        {
            //var ce = new EmailSend();


            //MathParser parser = new MathParser();
            //string s1 = "pi+5*5+5*3-5*5-5*3+1E1";
            //string s2 = "sin(cos(tg(sh(ch(th(100))))))";
            //bool isRadians = false;
            //double d1 = parser.Parse(s1, isRadians);
            //double d2 = parser.Parse(s2, isRadians);
            //string formula = ""
            //CalculationExtension.Calculate();


            //Expression e = new Expression("Round(Pow(Pi, 2) + Pow([Pi2], 2) + X, 2)");

            //e.Parameters["Pi2"] = new Expression("Pi * Pi");
            //e.Parameters["X"] = 10;

            //e.EvaluateParameter += delegate (string name, ParameterArgs args)
            //{
            //    if (name == "Pi")
            //        args.Result = 3.14;
            //};

            //Debug.Assert(117.07 == e.Evaluate());

            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            TVL050 objsrcTVL050 = db.TVL050.Where(x => x.RequestId == 1070).FirstOrDefault();
            int? iteration = objsrcTVL050.ReqIterationNo + 1;
            #region copy documents
            var oldfolderPath = "TVL050/" + objsrcTVL050.HeaderId + "/" + objsrcTVL050.ReqIterationNo + "/" + objsrcTVL050.ReqSequenceNo;
            var newfolderPath = "TVL050/" + objsrcTVL050.HeaderId + "/" + iteration + "/" + objsrcTVL050.ReqSequenceNo;
            //clsUpload.CopyFolderContents(oldfolderPath, newfolderPath);
            (new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);
            #endregion

            objResponseMsg.Key = true;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult PrintReportDirectly(string ReportPath, List<SSRSParam> reportParams)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string filenameformat = "TestPrintViaName";
                // string downloadpath = "~/Resources/TRAVELER/CompleteTraveler/";
                string downloadpath = "~/Resources/TRAVELER/CompleteTraveler/";

                string file = (new Utility.Controllers.GeneralController()).GeneratePDFfromSSRSreport(Server.MapPath(downloadpath + filenameformat + ".pdf"), ReportPath, reportParams);

                objResponseMsg.Key = true;
                objResponseMsg.Value = file;
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PrintReportDirectly1(string ReportPath, List<SSRSParam> reportParams)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string filenameformat = "TestPrintViaMethod1";
                // string downloadpath = "~/Resources/TRAVELER/CompleteTraveler/";
                string downloadpath = "~/Resources/TRAVELER/CompleteTraveler/";

                string file = (new Utility.Controllers.GeneralController()).GeneratePDFfromSSRSreport(Server.MapPath(downloadpath + filenameformat + ".pdf"), ReportPath, reportParams);

                #region Send Mail
                ProcessStartInfo info = new ProcessStartInfo(file);
                info.Verb = "Print";
                info.CreateNoWindow = true;
                info.WindowStyle = ProcessWindowStyle.Hidden;
                Process.Start(info);
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = file;
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult PrintReportDirectly2(string ReportPath, List<SSRSParam> reportParams)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string filenameformat = "TestPrintviaMethod2";
                // string downloadpath = "~/Resources/TRAVELER/CompleteTraveler/";
                string downloadpath = "~/Resources/TRAVELER/CompleteTraveler/";

                string file = (new Utility.Controllers.GeneralController()).GeneratePDFfromSSRSreport(Server.MapPath(downloadpath + filenameformat + ".pdf"), ReportPath, reportParams);


                #region Send Mail
                //var exe = new Process();
                //var p = new Process();

                //exe.StartInfo.FileName = "TestPrintviaMethod2";
                //p.StartInfo.Arguments = file.Replace("/\\/ g", "/");
                //p.Start();
               

                if (!string.IsNullOrWhiteSpace(file))
                {
                    PrintDocument pdoc = new PrintDocument();
                    pdoc.PrinterSettings.PrintFileName = file;
                    pdoc.PrinterSettings.PrintToFile = true;
                    //pdoc.PrintPage += file;
                    pdoc.Print();
                }

                #endregion
                objResponseMsg.Key = true;
                objResponseMsg.Value = file;
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

      

        //[HttpPost]
        //public ActionResult ExportToTTSExcel()
        //{
        //    string Calculate = "";
        //    //string Calculate1 = "";
        //    //string Calculate4 = "";
        //    //string Calculate5 = "";
        //    clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
        //    try
        //    {
        //        //var application = new Microsoft.Office.Interop.Excel.Application();
        //        //var workbook = application.Workbooks.Open(Server.MapPath("~/Resources/Excel/Excel Formula.xlsx"));

        //        Microsoft.Office.Interop.Excel.Application xlApp;
        //        Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
        //        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;

        //        xlApp = new Microsoft.Office.Interop.Excel.Application();
        //        xlWorkBook = xlApp.Workbooks.Open(Server.MapPath("~/Resources/Excel/Excel Formula.xlsx"));
        //        xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Sheets[1];// (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(6);
        //        //xlWorkSheet.Cells[6, 2].Value = "s"; //b6
        //        ////xlWorkSheet.Cells[6, 9].Value = ""; //i6
        //        //xlWorkSheet.Cells[6, 3].Value = 3857; //c
        //        //xlWorkSheet.Cells[6, 5].Value = 2230; //e
        //        //xlWorkSheet.Cells[6, 7].Value = 116.5; //g
        //        //xlWorkSheet.Cells[6, 11].Value = 25457.95621; //e6
        //        //xlWorkSheet.Cells[6, 12].Value = 1009.5; //e6
        //        //xlWorkSheet.Cells[1, 3].Formula = clsImplementationEnum.WCFormula.Formula1.GetStringValue().Replace("'", "\"");// "=IF(LOWER(B6) = \"o\", I6, IF(LOWER(B6) = \"s\", ((PI()) * 7.85 / 4000000) * ((C6 + 2 * G6) ^ 2 - (C6 ^ 2)) * E6, IF(LOWER(B6) = \"t\", ((PI()) * 7.85 / 4000000) * ((C6 ^ 2) - (C6 - 2 * G6) ^ 2) * E6, IF(LOWER(B6) = \"ed\", ((PI()) * 7.85 / 4000000) * ((1.25 * C6) ^ 2 * G6), IF(LOWER(B6) = \"ts\", ((PI() * 7.85 / 4000000) * (C6 - 2 * E6 + PI() * E6) ^ 2 * G6), IF(LOWER(B6) = \"c\", ((PI() * 7.85 / 1000000) * ((C6 + I6) / 2 + 2 * G6) * G6 * E6), IF(B6 = \"\", \"\")))))))";

        //        //xlWorkSheet.Cells[1, 4].Formula = "=IF(NOT(K6=\"\")=TRUE,K6*L6,\"\")";
        //        //xlWorkSheet.Cells[1, 5].Formula = "=CEILING(599830+ 26053.25373,500)";
        //        //xlWorkSheet.Cells[14, 2].Value = "o"; //b6

        //        //xlWorkSheet.Cells[1, 6].Formula = "=IF(N8=\"\",IF(LOWER(B14)=\"o\",\"KG\",IF(LOWER(B14)=\"s\",\"\",IF(LOWER(B14)=\"t\",\"\",IF(LOWER(B14)=\"ed\",\"\",IF(LOWER(B14)=\"ts\",\"\",IF(LOWER(B14)=\"c\",\"SID\",IF(B14=\"\",\"\"))))))),IF(LOWER(B14)=\"o\",\"KG\",IF(LOWER(B14)=\"s\",\"O\",IF(LOWER(B14)=\"t\",\"O\",IF(LOWER(B14)=\"ed\",\"O\",IF(LOWER(B14)=\"ts\",\"O\",IF(LOWER(B14)=\"c\",\"SID\",IF(B14=\"\",\"\"))))))))";

        //        //xlWorkSheet.Calculate();
        //        //Calculate = xlWorkSheet.Cells[1, 4].Value.ToString();
        //        //Calculate1 = xlWorkSheet.Cells[1, 5].Value.ToString();
        //        //Calculate4 = xlWorkSheet.Cells[1, 6].Value.ToString();
        //        string form = "=IF(LOWER(param1) = 'o', param5, IF(LOWER(param1) = 's', ((PI()) * 7.85 / 4000000) * ((param2 + 2 * param4) ^ 2 - (param2 ^ 2)) * param3, IF(LOWER(param1) = 't', ((PI()) * 7.85 / 4000000) * ((param2 ^ 2) - (param2 - 2 * param4) ^ 2) * param3, IF(LOWER(param1) = 'ed', ((PI()) * 7.85 / 4000000) * ((1.25 * param2) ^ 2 * param4), IF(LOWER(param1) = 'ts', ((PI() * 7.85 / 4000000) * (param2 - 2 * param3 + PI() * param3) ^ 2 * param4), IF(LOWER(param1) = 'c', ((PI() * 7.85 / 1000000) * ((param2 + param5) / 2 + 2 * param4) * param4 * param3), IF(param1 = '', '')))))))";
        //        Hashtable ht = new Hashtable();
        //        ht["param1"] = "\"s\"";
        //        ht["param2"] = 3857;
        //        ht["param3"] = 2230;
        //        ht["param4"] = 116.5;
        //        ht["param5"] = "\"\"";
        //        StringBuilder _strFormula = null;
        //        if (form != string.Empty)
        //        {
        //            _strFormula = new StringBuilder(form).Replace("'", "\"");
        //            if (ht != null && ht.Count > 0)
        //            {
        //                foreach (string key in ht.Keys)
        //                {
        //                    if (form.Contains(key))
        //                    {
        //                        if (ht[key] != null)
        //                        {
        //                            _strFormula.Replace(key.ToString(), ht[key].ToString());
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        xlWorkSheet.Cells[1, 1].Formula = _strFormula.ToString();
        //        xlWorkSheet.Calculate();
        //        Calculate = xlWorkSheet.Cells[1, 1].Value.ToString();
        //        objResponseMsg.Key = true;
        //        objResponseMsg.Value = Calculate;// + " ;" + Calculate1 + " ;" + Calculate4 + " ;" + Calculate5;
        //        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        throw;
        //    }
        //}

        //[HttpPost]
        //public ActionResult frmcalculations(string formula, string param1, string param2, string param3)
        //{
        //    clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
        //    try
        //    {
        //        var application = new Microsoft.Office.Interop.Excel.Application();
        //        var workbook = application.Workbooks.Open(Server.MapPath("~/Resources/Excel/Excel Formula.xlsx"));

        //        Microsoft.Office.Interop.Excel.Application xlApp;
        //        Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
        //        Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;

        //        xlApp = new Microsoft.Office.Interop.Excel.Application();
        //        xlWorkBook = xlApp.Workbooks.Open(Server.MapPath("~/Resources/Excel/Excel Formula.xlsx"));
        //        xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Sheets[1];// (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(6);
        //        string form = "=" + formula.ToLower();
        //        Hashtable ht = new Hashtable();
        //        ht["param1"] = param1;
        //        ht["param2"] = param2;
        //        ht["param3"] = param3;
        //        StringBuilder _strFormula = null;
        //        if (form != string.Empty)
        //        {
        //            _strFormula = new StringBuilder(form).Replace("'", "\"");
        //            if (ht != null && ht.Count > 0)
        //            {
        //                foreach (string key in ht.Keys)
        //                {
        //                    if (form.Contains(key))
        //                    {
        //                        if (ht[key] != null)
        //                        {
        //                            _strFormula.Replace(key.ToString(), ht[key].ToString());
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //        xlWorkSheet.Cells[1, 1].Formula = _strFormula.ToString();
        //        xlWorkSheet.Calculate();
        //        objResponseMsg.Key = true;
        //        objResponseMsg.Value = xlWorkSheet.Cells[1, 1].Value.ToString();// + " ;" + Calculate1 + " ;" + Calculate4 + " ;" + Calculate5;
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        throw;
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
    }
}