﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQSImplementation.Models;
using System.Collections;
using System.Xml;
using System.IO;
using OfficeOpenXml;
using System.Data;
using System.Dynamic;
using Newtonsoft.Json;
using System.Data.SqlClient;
using System.Reflection;
using System.Web.Script.Serialization;
using System.Text;
using System.Text.RegularExpressions;

namespace IEMQS.Areas.TEMP.Controllers
{
    using System.Threading.Tasks;
    public class CodingStandardController : clsBase
    {
        List<FKM102> listFKM102 = null;
        List<TreeviewEnt> listTreeviewEnt = null;
        List<SelectItemList> items;
        public CodingStandardController()
        {
            items = new List<SelectItemList>();
            items = (from li in db.ATH004
                     select new SelectItemList
                     {
                         id = li.Id.ToString(),
                         text = li.Role,
                     }).ToList();
        }
        // GET: TEMP/CodingStandard
        public ActionResult Index()
        {
            return View();
        }

        #region 1. Data Grid (Data Table)
        [SessionExpireFilter]
        public ActionResult DataGrid()
        {
            ViewBag.Title = "Coding Standard";
            return View();
        }

        //partial view for Tab
        [HttpPost]
        public ActionResult LoadDataGridPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_DataGridPartial");
        }

        //partial view for import popup
        [HttpPost]
        public ActionResult LoadExcelImportPartial()
        {
            return PartialView("_ExcelImportPartial");
        }

        [HttpPost]
        [SessionExpireFilter]
        public ActionResult ImportExcel(HttpPostedFileBase upload)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string qualityProject = string.Empty;
            string LTFPSNo = string.Empty;
            string LTFPSDes = string.Empty;
            if (upload != null)
            {
                if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
                {
                    ExcelPackage package = new ExcelPackage(upload.InputStream);
                    ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();

                    bool isError;
                    DataTable dt = ToDataTable(package, out isError);
                    if (!isError)
                    {
                        List<CodeStandard> lstCodeStandard = new List<CodeStandard>();
                        foreach (DataRow item in dt.Rows)
                        {
                            CodeStandard objCodeStandard = new CodeStandard();
                            objCodeStandard.Title = item.Field<string>("TITLE");
                            objCodeStandard.Title = item.Field<string>("FIRST NAME");
                            objCodeStandard.Title = item.Field<string>("LAST NAME");
                            objCodeStandard.Title = item.Field<string>("ROLES");
                            objCodeStandard.Title = item.Field<string>("ADDRESS");
                            objCodeStandard.Title = item.Field<string>("BIRTHDATE");
                            objCodeStandard.Title = item.Field<string>("GENDER");
                            objCodeStandard.Title = item.Field<string>("SALARY");
                            objCodeStandard.Title = item.Field<string>("MARRIED");
                            objCodeStandard.CreatedBy = objClsLoginInfo.UserName;
                            objCodeStandard.CreatedOn = DateTime.Now;
                            objCodeStandard.Status = clsImplementationEnum.CommonStatus.Active.GetStringValue();
                            lstCodeStandard.Add(objCodeStandard);
                        }
                        db.CodeStandard.AddRange(lstCodeStandard);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "User data imported successfully";
                    }
                    else
                    {
                        objResponseMsg = ExportToExcel(dt, qualityProject, LTFPSNo, LTFPSDes);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Allow extension should be .xlsx and .xls only. please select proper excel file.";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please select file";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponceMsgWithFileName ExportToExcel(DataTable dt, string qualityProject, string LTFPSNo, string LTFPSDescription)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath(@"\Resources\LTFPS\LTFPS - ERROR TEMPLATE.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        excelWorksheet.Cells[1, 1].Value = qualityProject;
                        excelWorksheet.Cells[2, 1].Value = LTFPSNo;
                        excelWorksheet.Cells[3, 1].Value = LTFPSDescription;
                        int i = 5;
                        foreach (DataRow item in dt.Rows)
                        {
                            excelWorksheet.Cells[i, 1].Value = item.Field<string>(0);
                            excelWorksheet.Cells[i, 2].Value = item.Field<string>(1);
                            excelWorksheet.Cells[i, 3].Value = item.Field<string>(2);
                            excelWorksheet.Cells[i, 4].Value = item.Field<string>(3);
                            excelWorksheet.Cells[i, 5].Value = item.Field<string>(4);
                            excelWorksheet.Cells[i, 6].Value = item.Field<string>(5);
                            excelWorksheet.Cells[i, 7].Value = item.Field<string>(6);
                            excelWorksheet.Cells[i, 8].Value = item.Field<string>(7);
                            excelWorksheet.Cells[i, 9].Value = item.Field<string>(8);
                            excelWorksheet.Cells[i, 10].Value = item.Field<string>(9);
                            excelWorksheet.Cells[i, 11].Value = item.Field<string>(10);
                            excelWorksheet.Cells[i, 12].Value = item.Field<string>(11);
                            excelWorksheet.Cells[i, 13].Value = item.Field<string>(12);
                            excelWorksheet.Cells[i, 14].Value = item.Field<string>(13);
                            excelWorksheet.Cells[i, 15].Value = item.Field<string>(14);
                            excelWorksheet.Cells[i, 16].Value = item.Field<string>(15);
                            excelWorksheet.Cells[i, 17].Value = item.Field<string>(16);
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath("ErrorList", out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public DataTable ToDataTable(ExcelPackage package, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtFromExcel = new DataTable();
            isError = false;
            foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
            {
                dtFromExcel.Columns.Add(firstRowCell.Text);
            }
            for (var rowNumber = 5; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
            {
                var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                var newRow = dtFromExcel.NewRow();
                foreach (var cell in row)
                {
                    newRow[cell.Start.Column - 1] = cell.Text;
                }
                dtFromExcel.Rows.Add(newRow);
            }
            dtFromExcel.Columns.Add("OperationNoErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("RefDocumentErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("SeamErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("ItemsErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("OperationErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("TestTypeErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("AggencyErrorMsg", typeof(string));
            dtFromExcel.Columns.Add("TPTErrorMsgMsg", typeof(string));

            //List<int> lstFindNo = db.VW_IPI_GETHBOMLIST.Where(x => x.Project == project).Select(x => x.FindNo).ToList();

            //List<string> lstTestTypeValues = (from glb2 in db.GLB002
            //                                  join glb1 in db.GLB001 on glb2.Category equals glb1.Id
            //                                  where glb1.Category == "LTFPS Test Type" && glb2.BU == BU && glb2.Location == location
            //                                  select glb2.Code).ToList();
            //List<string> lstAgencyValues = (from glb2 in db.GLB002
            //                                join glb1 in db.GLB001 on glb2.Category equals glb1.Id
            //                                where glb1.Category == "LTFPS Agency" && glb2.BU == BU && glb2.Location == location
            //                                select glb2.Code).ToList();
            //List<string> lstTPIValues = (from glb2 in db.GLB002
            //                             join glb1 in db.GLB001 on glb2.Category equals glb1.Id
            //                             where glb1.Category == "LTFPS TPI" && glb2.BU == BU && glb2.Location == location
            //                             select glb2.Code).ToList();

            //db.GLB002.Where(x => x.BU == BU && x.Location == location && lstFindNo.Contains(x.Category)).ToList();

            //foreach (DataRow item in dtFromExcel.Rows)
            //{


            //    string errorMessage = string.Empty;
            //    int opNo;
            //    if (!int.TryParse(item.Field<string>("OPN. NO."), out opNo))
            //    {
            //        item["OperationNoErrorMsg"] = "Operation No is Not Valid, Please Enter Only Numeric Value";
            //        isError = true;
            //    }

            //    if (string.IsNullOrWhiteSpace(item.Field<string>("REFERENCE DOCUMENT")))
            //    {
            //        item["RefDocumentErrorMsg"] = "Reference Document Required";
            //        isError = true;
            //    }

            //    if (string.IsNullOrWhiteSpace(item.Field<string>("SEAMS")) && string.IsNullOrWhiteSpace(item.Field<string>("ITEMS")))
            //    {
            //        item["SeamErrorMsg"] = "Seam Or Part Required";
            //        item["ItemsErrorMsg"] = "Seam Or Part Required";
            //        isError = true;
            //    }
            //    else if (string.IsNullOrWhiteSpace(item.Field<string>("SEAMS")) && !int.TryParse(item.Field<string>("ITEMS"), out opNo))
            //    {
            //        item["ItemsErrorMsg"] = "Valid Part No Required";
            //        isError = true;
            //    }
            //    else if (!string.IsNullOrWhiteSpace(item.Field<string>("SEAMS")))
            //    {
            //        string seamNo = item.Field<string>("SEAMS");
            //        QMS012 objQMS012 = db.QMS012.Where(x => x.SeamNo == seamNo && x.QualityProject == qualityProject).FirstOrDefault();
            //        if (objQMS012 == null)
            //        {
            //            item["SeamErrorMsg"] = item.Field<string>("SEAMS") + " Seam No not available for Quality Project " + qualityProject;
            //            isError = true;
            //        }
            //        else if (objQMS012.Status != clsImplementationEnum.SeamListStatus.Approved.GetStringValue())
            //        {
            //            item["SeamErrorMsg"] = item.Field<string>("SEAMS") + " Seam No is not approved.";
            //            isError = true;
            //        }
            //    }
            //    else if (int.TryParse(item.Field<string>("ITEMS"), out opNo))
            //    {

            //        if (!lstFindNo.Contains(opNo))
            //        {
            //            item["ItemsErrorMsg"] = "Partno is not available for this project";
            //            isError = true;
            //        }
            //    }
            //    //if (string.IsNullOrWhiteSpace(item.Field<string>("OPERATION")))
            //    //{
            //    //    item["OperationErrorMsg"] = "OPERATION Description is required";
            //    //    isError = true;
            //    //}

            //    if (string.IsNullOrWhiteSpace(item.Field<string>("TEST TYPE")))
            //    {
            //        item["TestTypeErrorMsg"] = "TEST TYPE is required";
            //        isError = true;
            //    }
            //    else if (!lstTestTypeValues.Contains(item.Field<string>("TEST TYPE")))
            //    {
            //        item["TestTypeErrorMsg"] = "TEST TYPE is not valid";
            //        isError = true;
            //    }

            //    if (string.IsNullOrWhiteSpace(item.Field<string>("AGENCY")))
            //    {
            //        item["AggencyErrorMsg"] = "Agency is Required";
            //        isError = true;
            //    }
            //    else if (!lstAgencyValues.Contains(item.Field<string>("AGENCY")))
            //    {
            //        item["AggencyErrorMsg"] = "AGENCY is not valid";
            //        isError = true;
            //    }

            //    if (string.IsNullOrWhiteSpace(item.Field<string>("TPI")))
            //    {
            //        item["TPTErrorMsgMsg"] = "TPI is Required";
            //        isError = true;
            //    }
            //    else if (!lstTPIValues.Contains(item.Field<string>("TPI")))
            //    {
            //        item["TPTErrorMsgMsg"] = "TPI is not valid";
            //        isError = true;
            //    }
            //}
            return dtFromExcel;
        }

        //bind datatable for Data Grid
        [HttpPost]
        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                if (param.Status.ToUpper() == clsImplementationEnum.CommonStatus.Active.GetStringValue().ToUpper())
                {
                    strWhereCondition += "1=1 and Status  = '" + clsImplementationEnum.CommonStatus.Active.GetStringValue() + "'";
                }
                else
                {
                    strWhereCondition += "1=1";
                }
                //search Condition 
                string[] columnName = { "Title", "FirstName", "LastName", "Roles", "Address", "Birthdate", "Gender", "Salary", "Married", "Status" };
                if (!String.IsNullOrWhiteSpace(param.sSearch))
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                var lstResult = db.SP_CODINGSTANDARD_GETUSERDETAIL(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.FullName),
                            Convert.ToString(uc.Title),
                            Convert.ToString(uc.FullName),
                            Convert.ToString(uc.Roles),
                            Convert.ToString(uc.Address),
                            uc.Birthdate == null || uc.Birthdate.Value==DateTime.MinValue? "" :uc.Birthdate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                            Convert.ToString(uc.Gender),
                            Convert.ToString(uc.Salary),
                            Convert.ToString(uc.Married == true ? "Yes":"No"),
                            Convert.ToString(uc.Status),
                            Helper.GenerateActionIcon(uc.UserId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/TEMP/CodingStandard/SubmitForm/"+uc.UserId,false)
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region 1.2 Data Grid (Data Table) – Inline Editing
        [SessionExpireFilter]
        public ActionResult InlineEditableDataGrid()
        {
            ViewBag.NameTitle = new List<CategoryData> { new CategoryData { Value = "Mr.", Code = "Mr.", CategoryDescription = "Mr." }, new CategoryData { Value = "Ms.", Code = "Ms.", CategoryDescription = "Ms." }, new CategoryData { Value = "Mrs.", Code = "Mrs.", CategoryDescription = "Mrs." } };
            ViewBag.Gender = new List<CategoryData> { new CategoryData { Value = "Male", Code = "Male", CategoryDescription = "Male" }, new CategoryData { Value = "Female", Code = "Female", CategoryDescription = "Female" } };

            var items = (from li in db.ATH004
                         select new SelectItemList
                         {
                             id = li.Id.ToString(),
                             text = li.Role,
                         }).ToList();

            ViewBag.DropDownListItem = items;
            Session["LastIndexGrid"] = 0;
            //ViewBag.LastIndexGrid = 0;

            return View();
        }

        [SessionExpireFilter]
        public ActionResult FixHeaderFilterDataTable()
        {
            ViewBag.NameTitle = new List<CategoryData> { new CategoryData { Value = "Mr.", Code = "Mr.", CategoryDescription = "Mr." }, new CategoryData { Value = "Ms.", Code = "Ms.", CategoryDescription = "Ms." }, new CategoryData { Value = "Mrs.", Code = "Mrs.", CategoryDescription = "Mrs." } };
            ViewBag.Gender = new List<CategoryData> { new CategoryData { Value = "Male", Code = "Male", CategoryDescription = "Male" }, new CategoryData { Value = "Female", Code = "Female", CategoryDescription = "Female" } };

            var items = (from li in db.ATH004
                         select new SelectItemList
                         {
                             id = li.Id.ToString(),
                             text = li.Role,
                         }).ToList();
            ViewBag.DropDownListItem = items;
            return View();
        }
        //bind datatable for Editable Data Grid (Rahul)
        [HttpPost]
        public JsonResult LoadInlineEditableDataGridData(JQueryDataTableParamModel param)
        {
            try
            {

                int StartIndex = 0;
                int EndIndex = 0;

                StartIndex = param.iDisplayStart + 1;
                EndIndex = param.iDisplayStart + param.iDisplayLength;

                //if (Indexes!= null)
                //{
                //    StartIndex = Convert.ToInt32(Indexes.GetValue(0)) + 1;
                //    EndIndex = Convert.ToInt32(Indexes.GetValue(1));
                //}
                //else
                //{
                //    StartIndex = param.iDisplayStart + 1;
                //    EndIndex = param.iDisplayStart + param.iDisplayLength;
                //}



                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                //string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortColumnName = "Title";
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                strWhereCondition += "1=1 ";
                //search Condition 
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "FirstName", "LastName", "Roles", "Address", "Birthdate", "Gender", "Salary" };

                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))

                        strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }


                var lstResult = db.SP_CODINGSTANDARD_GETUSERDETAIL(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                        Helper.GenerateHTMLTextbox(newRecordId,"UserId","","",true,"display:none",true),
                                        Helper.HTMLAutoComplete(newRecordId,"Title","","",false,"","Title"),
                                        Helper.GenerateTextbox(newRecordId, "FirstName",   "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "LastName",  "", "", false, "", "50"),
                                        Helper.MultiSelectDropdown(items,newRecordId,"Roles",false,"","","Roles"),
                                        Helper.GenerateTextArea(newRecordId, "Address",  "", "", false, "", "500"),
                                        Helper.GenerateTextbox(newRecordId, "Birthdate"),
                                        Helper.HTMLAutoComplete(newRecordId,"Gender","","",false,"","Gender"),
                                        Helper.GenerateNumericTextbox(newRecordId, "Salary",  "", "", false, "", "20"),
                                        Helper.GenerateActionCheckbox(newRecordId,"Married",false,"",false),
                                        clsImplementationEnum.CommonStatus.Active.GetStringValue(),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord();" ),
                                    };


                var data = (from uc in lstResult
                            select new[]
                           {

                                //Helper.GenerateHidden(uc.UserId,"UserId",Convert.ToString(uc.UserId)),
                                 uc.UserId.ToString(),
                               "<span class=' ' data-col='description' data-maxlength='100' >"+Convert.ToString(uc.Title)+ "</span>",
                                //Helper.HTMLAutoComplete(uc.UserId, "Title",    Convert.ToString(uc.Title),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50"),

                                "<span class=' ' data-col='description' data-maxlength='100' >" +Convert.ToString(uc.FirstName)+ "</span>",
                                //Helper.GenerateTextbox(uc.UserId, "FirstName",    Convert.ToString(uc.FirstName),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50"),

                                "<span class=' ' data-col='description' data-maxlength='100' >" +Convert.ToString(uc.LastName)+ "</span>",
                                //Helper.GenerateTextbox(uc.UserId, "LastName",    Convert.ToString(uc.LastName),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50"),
                                                           
                               "<span class=' ' data-col='description' data-maxlength='100' >" +  string.Join(",", GetRoles(uc.Roles)) + "</span>",//string.Join(",", GetRoles(uc.Roles))
                                //Helper.MultiSelectDropdown(items,uc.UserId,uc.Roles, (string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true) ,"UpdateData(this,'"+ uc.UserId  +"',true);","","Roles"),
                                   
                                "<span class=' ' data-col='description' data-maxlength='100' >"+Convert.ToString(uc.Address)+ "</span>",
                                //Helper.GenerateTextArea(uc.UserId, "Address",    Convert.ToString(uc.Address),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"width:200px;","500"),

                               "<span class=' ' data-col='description' data-maxlength='100' >" +Convert.ToString(Convert.ToDateTime(uc.Birthdate))+ "</span>",
                               // Helper.GenerateTextbox(uc.UserId, "Birthdate",uc.Birthdate==null || uc.Birthdate.Value==DateTime.MinValue ? "":Convert.ToDateTime(uc.Birthdate).ToString("yyyy-MM-dd") , "UpdateData(this, "+ uc.UserId +");",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true)),

                                "<span class=' ' data-col='description' data-maxlength='100' >" + Convert.ToString(uc.Gender)+ "</span>",
                               // Helper.HTMLAutoComplete(uc.UserId, "Gender",    Convert.ToString(uc.Gender),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50"),

                               "<span class=' ' data-col='description' data-maxlength='100' >" +Convert.ToString(uc.Salary)+ "</span>",
                                // Helper.GenerateNumericTextbox(uc.UserId, "Salary",    Convert.ToString(uc.Salary),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"","50"),

                               "<span class=' ' data-col='description' data-maxlength='100' >" +Convert.ToString(uc.Married)+ "</span>",
                                //Helper.GenerateActionCheckbox(uc.UserId,"Married",uc.Married,"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true)),

                                "<span class=' ' data-col='description' data-maxlength='100' >" + Convert.ToString(uc.Status)+ "</span>",
                               // Convert.ToString(uc.Status),

                             Helper.GenerateActionIcon(uc.UserId, "Edit", "Edit Record", "editTableRow fa fa-edit", "","",false) + "|" +
                             Helper.GenerateActionIcon(uc.UserId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteData("+uc.UserId+")","",false)

                          }).ToList();
                data.Insert(0, newRecord);

                int sizeOfData = 0;



                sizeOfData = string.IsNullOrWhiteSpace(Session["LastIndexGrid"] + "") ? 0 : (Int32)Session["LastIndexGrid"];
                if (sizeOfData != 0)
                {
                    Session["LastIndexGrid"] = (Int32)Session["LastIndexGrid"] + data.Count;
                }
                else
                {
                    Session["LastIndexGrid"] = data.Count;
                }



                //int sizeOfData = data.Count;

                //if (sizeOfData > 50)
                //{

                //    var SplitData = data.Take(50).Select(i => i.ToString()).ToList();

                //   var  lists = Enumerable.Range(0, ( data.Count + 50 - 1) / 50)
                //       .Select(index => data.GetRange(index * 50,
                //                                      Math.Min(50, data.Count - index * 50)))
                //       .ToList();

                //    foreach (var item in lists)
                //    {
                //        return Json(new
                //        {
                //            sEcho = Convert.ToInt32(param.sEcho),
                //            iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                //            iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                //            aaData = item,

                //            whereCondition = strWhereCondition,
                //            strSortOrder = strSortOrder
                //        }, JsonRequestBehavior.AllowGet);
                //    }

                //}


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,

                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //Genrating value for DropDown (Rahul)
        public List<string> GetRoles(string roles)
        {
            List<string> item = new List<string>();
            foreach (var role in roles.Split(','))
            {

                var roleName = items.Where(x => x.id == role).FirstOrDefault();
                if (roleName != null)
                    item.Add(roleName.text);
            }
            return item;
        }

        //Genrating Singel line for Editing (Rahul)
        [HttpPost]
        public JsonResult LoadInlineEditableDataSingleData(int id, string tablename)
        {
            try
            {

                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                //string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortColumnName = "Title";
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                strWhereCondition += "1=1";

                var lstResult = db.SP_CODINGSTANDARD_GETUSERDETAIL(1, 0, strSortOrder, strWhereCondition).Where(x => x.UserId.Equals(id)).ToList();

                var items = (from li in db.ATH004
                             select new SelectItemList
                             {
                                 id = li.Id.ToString(),
                                 text = li.Role,
                             }).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {

                            uc.UserId.ToString(),
                            //Helper.GenerateHidden(uc.UserId,"UserId",Convert.ToString(uc.UserId)),
                            Helper.HTMLAutoComplete(uc.UserId, "Title",   Convert.ToString(uc.Title),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50","",false,"","","","editable"),
                            Helper.GenerateTextbox(uc.UserId, "FirstName",    Convert.ToString(uc.FirstName),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"","50","editable"),
                            Helper.GenerateTextbox(uc.UserId, "LastName",    Convert.ToString(uc.LastName),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"","50","editable"),
                            Helper.MultiSelectDropdown(items,uc.UserId,uc.Roles, (string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true) ,"UpdateData(this,'"+ uc.UserId  +"',true);","","Roles","editable"),
                            Helper.GenerateTextArea(uc.UserId, "Address",    Convert.ToString(uc.Address),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"width:200px;","","500","editable"),
                            Helper.GenerateTextbox(uc.UserId, "Birthdate",uc.Birthdate==null || uc.Birthdate.Value==DateTime.MinValue ? "":Convert.ToDateTime(uc.Birthdate).ToString("yyyy-MM-dd") , "UpdateData(this, "+ uc.UserId +");",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"","","editable"),
                            Helper.HTMLAutoComplete(uc.UserId, "Gender",    Convert.ToString(uc.Gender),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50","",false,"","","","editable"),
                            Helper.GenerateNumericTextbox(uc.UserId, "Salary",    Convert.ToString(uc.Salary),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"","50","editable"),
                            Helper.GenerateActionCheckbox(uc.UserId,"Married",uc.Married,"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"editable"),
                            Convert.ToString(uc.Status),
                            Helper.GenerateActionIcon(uc.UserId, "Edit", "Edit Record", "editTableRow fa fa-edit", "","",false) + "|"+
                            Helper.GenerateActionIcon(uc.UserId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteData("+uc.UserId+")","",false)
                          }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(0),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = 0,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        //Creating Client Side grid (Rahul)
        //  [HttpPost]
        public JsonResult ServerSideGrid(JQueryDataTableParamModel param)
        {
            try
            {

                int StartIndex = param.iDisplayStart + 1;

                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                //string sortColumnName = "Title";
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                strWhereCondition += "1=1 ";
                //search Condition 

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {

                    string[] columnName = { "FirstName", "LastName", "Roles", "Address", "Birthdate", "Gender", "Salary" };
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    if (!string.IsNullOrWhiteSpace(param.SearchFilter))
                        strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }


                var lstResult = db.SP_CODINGSTANDARD_GETUSERDETAIL(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {

                                        Helper.HTMLAutoComplete(newRecordId,"Title","","",false,"","Title"),
                                        Helper.GenerateTextbox(newRecordId, "FirstName",   "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "LastName",  "", "", false, "", "50"),
                                        Helper.MultiSelectDropdown(items,newRecordId,"Roles",false,"","","Roles"),
                                        Helper.GenerateTextArea(newRecordId, "Address",  "", "", false, "", "500"),
                                        Helper.GenerateTextbox(newRecordId, "Birthdate"),
                                        Helper.HTMLAutoComplete(newRecordId,"Gender","","",false,"","Gender"),
                                        Helper.GenerateNumericTextbox(newRecordId, "Salary",  "", "", false, "", "20"),
                                        Helper.GenerateActionCheckbox(newRecordId,"Married",false,"",false),
                                        clsImplementationEnum.CommonStatus.Active.GetStringValue(),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewServerSideRecord();" ),
                                    };


                var data = (from uc in lstResult
                            select new[]
                           {


                                Helper.HTMLAutoComplete(uc.UserId, "Title",    Convert.ToString(uc.Title),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50"),
                                Helper.GenerateTextbox(uc.UserId, "FirstName",    Convert.ToString(uc.FirstName),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50"),
                                Helper.GenerateTextbox(uc.UserId, "LastName",    Convert.ToString(uc.LastName),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50"),
                                Helper.MultiSelectDropdown(items,uc.UserId,uc.Roles, (string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true) ,"UpdateData(this,'"+ uc.UserId  +"',true);","","Roles"),
                                Helper.GenerateTextArea(uc.UserId, "Address",    Convert.ToString(uc.Address),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"width:200px;","500"),
                                Helper.GenerateTextbox(uc.UserId, "Birthdate",uc.Birthdate==null || uc.Birthdate.Value==DateTime.MinValue ? "":Convert.ToDateTime(uc.Birthdate).ToString("yyyy-MM-dd") , "UpdateData(this, "+ uc.UserId +");",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true)),
                                Helper.HTMLAutoComplete(uc.UserId, "Gender",    Convert.ToString(uc.Gender),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50"),
                                 Helper.GenerateNumericTextbox(uc.UserId, "Salary",    Convert.ToString(uc.Salary),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"","50"),
                                Helper.GenerateActionCheckbox(uc.UserId,"Married",uc.Married,"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true)),
                               Convert.ToString(uc.Status),


                            Helper.GenerateActionIcon(uc.UserId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteData("+uc.UserId+")","",false)

                          }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,

                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        //Save New Record
        [HttpPost]
        public ActionResult SaveNewRecord(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int newRowIndex = 0;
            int userId = Convert.ToInt32(fc["UserId" + newRowIndex]);
            string tableName = string.Empty;
            try
            {

                CodeStandard objCodeStandard = new CodeStandard();
                objCodeStandard.Title = fc["Title" + newRowIndex];
                objCodeStandard.FirstName = fc["FirstName" + newRowIndex];
                objCodeStandard.LastName = fc["LastName" + newRowIndex];
                objCodeStandard.Roles = fc["Roles" + newRowIndex];
                objCodeStandard.Address = fc["Address" + newRowIndex];
                if (fc["Birthdate" + newRowIndex] != null && CheckDate(fc["Birthdate" + newRowIndex]))
                {
                    objCodeStandard.Birthdate = Convert.ToDateTime(fc["Birthdate" + newRowIndex]);
                }
                objCodeStandard.Gender = fc["Gender" + newRowIndex];
                objCodeStandard.Salary = Convert.ToDecimal(fc["Salary" + newRowIndex]);
                objCodeStandard.Married = Convert.ToBoolean(fc["Married" + newRowIndex]);
                objCodeStandard.Status = clsImplementationEnum.CommonStatus.Active.GetStringValue();
                objCodeStandard.CreatedBy = objClsLoginInfo.UserName;
                objCodeStandard.CreatedOn = DateTime.Now;
                db.CodeStandard.Add(objCodeStandard);
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Insert;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //update existing column
        [HttpPost]
        public ActionResult UpdateDetails(int id, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool isValid = false;
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    if (columnName == "Birthdate")
                    {
                        if (CheckDate(columnValue))
                        {
                            isValid = true;
                        }
                    }
                    else
                    {
                        isValid = true;
                    }
                    if (isValid)
                    {
                        db.SP_CODINGSTANDARD_UPDATE(id, columnName, columnValue, "CodeStandard", objClsLoginInfo.UserName);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Invalid Birthdate";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteDetails(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                CodeStandard objCodeStandard = db.CodeStandard.Where(x => x.UserId == id).FirstOrDefault();
                if (objCodeStandard != null)
                {
                    db.CodeStandard.Remove(objCodeStandard);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details successfully deleted.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        protected bool CheckDate(String date)
        {
            try
            {
                DateTime dt = DateTime.Parse(date);
                return true;
            }
            catch
            {
                return false;
            }
        }
        #region DataGrid with Column Level Filter, Fixed Column, Inline Editable and Lazy Loading 
        [SessionExpireFilter]
        public ActionResult DataTableWithAllUtility()
        {
            ViewBag.NameTitle = new string[] { "Mr.", "Ms.", "Mrs." };
            ViewBag.Gender = new string[] { "Male", "Female" };

            /*var items = (from li in db.ATH004
                         select new SelectItemList
                         {
                             id = li.Id.ToString(),
                             text = li.Role,
                         }).ToList();
            ViewBag.DropDownListItem = items;*/
            return View();
        }

        [HttpPost]
        public JsonResult LoadDataTableWithAllUtilityGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;


                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                //string sortColumnName = "Title";
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                string strWhereCondition = "1=1 ";
                //search Condition 
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "FirstName", "LastName", "Roles", "Address", "Birthdate", "Gender", "Salary" };
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_CODINGSTANDARD_GETUSERDETAIL(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var CheckedIds = "";
                if (param.bCheckedAll && lstResult.Any())
                    CheckedIds = String.Join(",", db.SP_CODINGSTANDARD_GETUSERDETAIL(1, 0, "", strWhereCondition).Select(s => s.UserId).ToList());

                var data = (from uc in lstResult
                            select new[] {
                                uc.UserId.ToString(),
                                uc.Title,
                                uc.FirstName,
                                uc.LastName,
                                string.Join(",", GetRoles(uc.Roles)),
                                uc.Address,
                                Convert.ToString(uc.Birthdate),
                                uc.Gender,
                                Convert.ToString(uc.Salary),
                                Convert.ToString(uc.Married),
                                uc.LastName,
                                uc.Address,
                                Convert.ToString(uc.Birthdate),
                                uc.Gender,
                                Convert.ToString(uc.Salary),
                                uc.Status,
                                Helper.GenerateActionIcon(uc.UserId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteData("+uc.UserId+")","",false)
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder,
                    checkedIds = CheckedIds
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id)
        {
            try
            {
                var items = new List<SelectItemList>();
                if (TempData["ROLES"] == null)
                    items = db.ATH004.Select(s => new SelectItemList
                    {
                        id = s.Id.ToString(),
                        text = s.Role,
                    }).Distinct().ToList();
                else
                    items = (List<SelectItemList>)TempData["ROLES"];
                TempData["ROLES"] = items;

                var data = new List<string[]>();
                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        Helper.GenerateHTMLTextbox(newRecordId,"UserId","","",true,"display:none",true),
                                        Helper.HTMLAutoComplete(newRecordId,"Title","","",false,"","Title"),
                                        Helper.GenerateTextbox(newRecordId, "FirstName",   "", "", false, "", "50"),
                                        Helper.GenerateTextbox(newRecordId, "LastName",  "", "", false, "", "50"),
                                        Helper.MultiSelectDropdown(items,newRecordId,"Roles",false,"","","Roles"),
                                        Helper.GenerateTextArea(newRecordId, "Address",  "", "", false, "", "500"),
                                        Helper.GenerateTextbox(newRecordId, "Birthdate"),
                                        Helper.HTMLAutoComplete(newRecordId,"Gender","","",false,"","Gender"),
                                        Helper.GenerateNumericTextbox(newRecordId, "Salary",  "", "", false, "", "20"),
                                        Helper.GenerateActionCheckbox(newRecordId,"Married",false,"",false),
                                        clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                        clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                        clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                        clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                        clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                        clsImplementationEnum.CommonStatus.Active.GetStringValue(),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord();" ),
                                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_CODINGSTANDARD_GETUSERDETAIL(1, 0, "", "UserId = " + id).Take(1).ToList();
                    data = (from uc in lstResult
                            select new[] {
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                Helper.HTMLAutoComplete(uc.UserId, "Title", uc.Title,"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50","",false,"","","","editable"),
                                Helper.GenerateTextbox(uc.UserId, "FirstName", uc.FirstName,"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"","50","editable"),
                                Helper.GenerateTextbox(uc.UserId, "LastName", uc.LastName,"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"","50","editable"),
                                Helper.MultiSelectDropdown(items,uc.UserId,uc.Roles, (string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true) ,"UpdateData(this,'"+ uc.UserId  +"',true);","","Roles","editable"),
                                Helper.GenerateTextArea(uc.UserId, "Address", uc.Address,"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"width:200px;","","500","editable"),
                                Helper.GenerateTextbox(uc.UserId, "Birthdate",uc.Birthdate==null || uc.Birthdate.Value==DateTime.MinValue ? "":Convert.ToDateTime(uc.Birthdate).ToString("yyyy-MM-dd") , "UpdateData(this, "+ uc.UserId +");",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"","","editable"),
                                Helper.HTMLAutoComplete(uc.UserId, "Gender", uc.Gender,"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"50","",false,"","","","editable"),
                                Helper.GenerateNumericTextbox(uc.UserId, "Salary", Convert.ToString(uc.Salary),"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"","50","editable"),
                                Helper.GenerateActionCheckbox(uc.UserId,"Married",uc.Married,"UpdateData(this, "+ uc.UserId+",true);",(string.Equals(uc.Status,clsImplementationEnum.CommonStatus.Active.GetStringValue(),StringComparison.OrdinalIgnoreCase)  ? false : true),"editable"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                Helper.GenerateActionIcon(uc.UserId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteData("+uc.UserId+")","",false)
                          }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #endregion

        #region 1.3 Jq Grid

        [SessionExpireFilter]
        public ActionResult JqGridData()
        {
            ViewBag.NameTitle = new List<CategoryData> { new CategoryData { Value = "Mr.", Code = "Mr.", CategoryDescription = "Mr." }, new CategoryData { Value = "Ms.", Code = "Ms.", CategoryDescription = "Ms." }, new CategoryData { Value = "Mrs.", Code = "Mrs.", CategoryDescription = "Mrs." } };
            ViewBag.Gender = new List<CategoryData> { new CategoryData { Value = "Male", Code = "Male", CategoryDescription = "Male" }, new CategoryData { Value = "Female", Code = "Female", CategoryDescription = "Female" } };

            var items = (from li in db.ATH004
                         select new SelectItemList
                         {
                             id = li.Id.ToString(),
                             text = li.Role,
                         }).ToList();

            ViewBag.DropDownListItem = items;
            Session["LastIndexGrid"] = 0; ;

            return View();
        }



        [HttpGet]
        public JsonResult LoadJqGridData(JqGridParamModel model, string filters = "")
        {
            JqGridParamModel param = new JqGridParamModel();
            try
            {

                if (!string.IsNullOrWhiteSpace(filters))
                {
                    filters oFilters = new filters();
                    oFilters = new JavaScriptSerializer().Deserialize<filters>(filters);
                    model.filters = oFilters;
                }
                int StartIndex = 0;
                int EndIndex = 0;

                StartIndex = ((model.page * model.rows) - model.rows) + 1;
                EndIndex = (model.page * model.rows);

                string strWhereCondition = string.Empty;

                #region JqGrid Sorting               
                string sortColumnName = model.sidx;
                string sortDirection = model.sord;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion
                strWhereCondition += "1=1 ";
                //search Condition
                if ((model._search))
                {
                    if (!string.IsNullOrWhiteSpace(model.searchField))
                    {
                        strWhereCondition += MakeJqSearchWithMultipleCondition(model.searchOper, model.searchField, model.searchString, "string", "AND");
                    }
                    else if (model.filters != null)
                    {
                        strWhereCondition += MakeJqGridSearchCondition(model.filters);
                    }
                }


                var lstResult = db.SP_CODINGSTANDARD_GETUSERDETAIL(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                //int newRecordId = 0;
                //var newRecord = new[] {
                //                        Helper.GenerateHTMLTextbox(newRecordId,"UserId","","",true,"display:none",true),
                //                        Helper.HTMLAutoComplete(newRecordId,"Title","","",false,"","Title"),
                //                        Helper.GenerateTextbox(newRecordId, "FirstName",   "", "", false, "", "50"),
                //                        Helper.GenerateTextbox(newRecordId, "LastName",  "", "", false, "", "50"),
                //                        Helper.MultiSelectDropdown(items,newRecordId,"Roles",false,"","","Roles"),
                //                        Helper.GenerateTextArea(newRecordId, "Address",  "", "", false, "", "500"),
                //                        Helper.GenerateTextbox(newRecordId, "Birthdate"),
                //                        Helper.HTMLAutoComplete(newRecordId,"Gender","","",false,"","Gender"),
                //                        Helper.GenerateNumericTextbox(newRecordId, "Salary",  "", "", false, "", "20"),
                //                        Helper.GenerateActionCheckbox(newRecordId,"Married",false,"",false),
                //                        clsImplementationEnum.CommonStatus.Active.GetStringValue(),
                //                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewRecord();" ),
                //                    };


                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.UserId),
                            "",
                             Convert.ToString(uc.FullName),
                             Convert.ToString(uc.Title),
                            Convert.ToString(uc.FirstName),
                            Convert.ToString(uc.LastName),
                            Convert.ToString(uc.Roles),
                            uc.Birthdate == null || uc.Birthdate.Value==DateTime.MinValue? "" :uc.Birthdate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                            Convert.ToString(uc.Gender),
                            Convert.ToString(uc.Address),
                            Convert.ToString(uc.Salary),
                            Convert.ToString(uc.Married == true ? "Yes":"No"),
                            Convert.ToString(uc.Status),
                            Helper.GenerateActionIcon(uc.UserId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/TEMP/CodingStandard/SubmitForm/"+uc.UserId,false)+" "+
                            Helper.GenerateActionIcon(uc.UserId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteData("+uc.UserId+")","",false)

                          }).ToList();
                //data.Insert(0, newRecord);

                int sizeOfData = 0;

                int totalRecords = (int)(lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0);
                var totalPages = (int)Math.Ceiling((float)totalRecords / (float)model.rows);

                sizeOfData = (Int32)Session["LastIndexGrid"];
                if (sizeOfData != 0)
                {
                    Session["LastIndexGrid"] = (Int32)Session["LastIndexGrid"] + data.Count;
                }
                else
                {
                    Session["LastIndexGrid"] = data.Count;
                }

                return Json(new
                {
                    total = totalPages,
                    model.page,
                    records = totalRecords,
                    rows = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    //sEcho = model.sEcho,
                    //iTotalRecords = "0",
                    //iTotalDisplayRecords = "0",
                    //aaData = ""
                    total = "0",
                    model.page,
                    records = "0",
                    rows = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult AddEditDeleteJqGridUserData()
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int userId = Request.Form["UserId"] != null ? (Regex.IsMatch(Request.Form["UserId"], @"^\d+$") == true) ? Convert.ToInt32(Request.Form["UserId"]) : 0 : 0; ;
                if (userId == 0)
                {
                    userId = Request.Form["id"] != null ? (Regex.IsMatch(Request.Form["id"], @"^\d+$") == true) ? Convert.ToInt32(Request.Form["id"]) : 0 : 0;
                }
                // Add New Record
                if (userId == 0 && Request.Form["oper"].ToString() == "add")
                {
                    string tableName = string.Empty;
                    try
                    {
                        CodeStandard objCodeStandard = new CodeStandard();
                        objCodeStandard.Title = Request.Form["Title"];
                        objCodeStandard.FirstName = Request.Form["FirstName"];
                        objCodeStandard.LastName = Request.Form["LastName"];
                        objCodeStandard.Roles = Request.Form["Roles"];
                        objCodeStandard.Address = Request.Form["Address"];
                        if (Request.Form["Birthdate"] != null && CheckDate(Request.Form["Birthdate"]))
                        {
                            objCodeStandard.Birthdate = Convert.ToDateTime(Request.Form["Birthdate"]);
                        }
                        objCodeStandard.Gender = Request.Form["Gender"];
                        objCodeStandard.Salary = Convert.ToDecimal(Request.Form["Salary"]);
                        objCodeStandard.Married = Convert.ToBoolean(Convert.ToInt16(Request.Form["Married"]));
                        objCodeStandard.Status = clsImplementationEnum.CommonStatus.Active.GetStringValue();
                        objCodeStandard.CreatedBy = objClsLoginInfo.UserName;
                        objCodeStandard.CreatedOn = DateTime.Now;
                        db.CodeStandard.Add(objCodeStandard);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Insert;
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
                    }
                }
                else if (userId > 0 && Request.Form["oper"].ToString() == "edit")// Edit Record
                {
                    bool isValid = false;
                    string columnName = string.Empty;
                    string columnValue = string.Empty;

                    foreach (string key in Request.Form.Keys)
                    {
                        columnName = key;
                        columnValue = Request.Form[key];

                        if (!string.IsNullOrEmpty(columnName) && columnName != "oper" && columnName != "id" && columnName != "UserId" && !string.IsNullOrEmpty(columnValue))
                        {
                            if (columnName == "Birthdate")
                            {
                                if (CheckDate(columnValue))
                                {
                                    isValid = true;
                                }
                            }
                            else
                            {
                                isValid = true;
                            }
                            if (isValid)
                            {
                                db.SP_CODINGSTANDARD_UPDATE(userId, columnName, columnValue, "CodeStandard", objClsLoginInfo.UserName);
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Invalid Birthdate";
                            }
                        }
                        columnName = string.Empty;
                        columnValue = string.Empty;
                    }

                }
                else if (userId > 0 && Request.Form["oper"].ToString() == "del")// Edit Record)
                {
                    try
                    {
                        CodeStandard objCodeStandard = db.CodeStandard.Where(x => x.UserId == userId).FirstOrDefault();
                        if (objCodeStandard != null)
                        {
                            db.CodeStandard.Remove(objCodeStandard);
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Details successfully deleted.";
                        }
                        else
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Details not available for delete.";
                        }
                    }
                    catch (Exception ex)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                    }
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public ActionResult DataFlow()
        {

            return View();
        }

        public JsonResult LoadDataFlowData(bool status = true)
        {
            try
            {
                clsDataFlow objData = new clsDataFlow();
                objData.nodeDataArray = new List<nodeDataArray>();
                objData.linkDataArray = new List<linkDataArray>();
                nodeDataArray aNode = new nodeDataArray();
                linkDataArray aLink = new linkDataArray();
                aNode.key = "1";
                aNode.type = "L&T";
                aNode.name = "IEMQS";
                objData.nodeDataArray.Add(aNode);
                aNode = new nodeDataArray();
                aNode.key = "2";
                aNode.type = "Contractor";
                aNode.name = "Rigel";
                objData.nodeDataArray.Add(aNode);
                aNode = new nodeDataArray();
                aNode.key = "3";
                aNode.type = "Contractor";
                aNode.name = "Pawai";
                objData.nodeDataArray.Add(aNode);
                aNode = new nodeDataArray();
                aNode.key = "4";
                aNode.type = "Contractor";
                aNode.name = "Others";
                objData.nodeDataArray.Add(aNode);

                aNode = new nodeDataArray();
                aNode.key = "5";
                aNode.type = "Group";
                aNode.name = "Satish Pawar";
                objData.nodeDataArray.Add(aNode);

                aNode = new nodeDataArray();
                aNode.key = "6";
                aNode.type = "Group";
                aNode.name = "Trishul Tandel";
                objData.nodeDataArray.Add(aNode);

                aNode = new nodeDataArray();
                aNode.key = "7";
                aNode.type = "Group";
                aNode.name = "Jayraj Desai";
                objData.nodeDataArray.Add(aNode);

                aNode = new nodeDataArray();
                aNode.key = "8";
                aNode.type = "Group";
                aNode.name = "Het";
                objData.nodeDataArray.Add(aNode);

                aLink.from = "1";
                aLink.frompid = "Suc";
                aLink.to = "2";
                aLink.topid = "Pre";
                objData.linkDataArray.Add(aLink);

                aLink = new linkDataArray();
                aLink.from = "1";
                aLink.frompid = "Suc";
                aLink.to = "3";
                aLink.topid = "Pre";
                objData.linkDataArray.Add(aLink);
                aLink = new linkDataArray();

                aLink.from = "1";
                aLink.frompid = "Suc";
                aLink.to = "4";
                aLink.topid = "Pre";
                objData.linkDataArray.Add(aLink);
                aLink = new linkDataArray();

                aLink.from = "2";
                aLink.frompid = "Suc";
                aLink.to = "5";
                aLink.topid = "Pre";
                objData.linkDataArray.Add(aLink);
                aLink = new linkDataArray();

                aLink.from = "5";
                aLink.frompid = "Suc";
                aLink.to = "6";
                aLink.topid = "Pre";
                objData.linkDataArray.Add(aLink);

                aLink = new linkDataArray();
                aLink.from = "3";
                aLink.frompid = "Suc";
                aLink.to = "7";
                aLink.topid = "Pre";
                objData.linkDataArray.Add(aLink);

                aLink = new linkDataArray();
                aLink.from = "7";
                aLink.frompid = "Suc";
                aLink.to = "8";
                aLink.topid = "Pre";
                objData.linkDataArray.Add(aLink);
                aLink = new linkDataArray();

                return Json(objData, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        #region 3. Form Design, Validations & Submission
        // Developed By : Ajay Chauhan
        //Dated On : 06/02/2018
        #region Utility
        public JsonResult GetAllRoles()
        {
            try
            {
                var items = (from li in db.ATH004
                             select new
                             {
                                 id = li.Role,
                                 text = li.Role,
                             }).ToList();

                return Json(items, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        public ContentResult GetAllRolesHtmlString()
        {
            try
            {
                var items = (from li in db.ATH004
                             select new
                             {
                                 id = li.Role,
                                 text = li.Role,
                             }).ToList();

                string multipleSelect = "<select role='select'>";

                foreach (var itm in items)
                {
                    multipleSelect += "<option value='" + itm.id + "'>" + itm.text + " </option> ";
                }
                multipleSelect += "</select>";
                return Content(multipleSelect);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Content("");
            }
        }
        #endregion
        public ActionResult SubmitForm(int? id)
        {
            ViewBag.UserId = id;
            return View();
        }

        [HttpPost]
        public ActionResult LoadFormPartial(int? UserId)
        {
            CodeStandard objCodeStandard = null;
            List<BULocWiseCategoryModel> lstTitle = new List<BULocWiseCategoryModel>();

            if (UserId > 0)
            {
                objCodeStandard = db.CodeStandard.Where(x => x.UserId == UserId).FirstOrDefault();
            }
            else
            {
                objCodeStandard = new CodeStandard();
            }

            lstTitle.Add(new BULocWiseCategoryModel { CatDesc = "Mr.", CatID = "Mr." });
            lstTitle.Add(new BULocWiseCategoryModel { CatDesc = "Mrs.", CatID = "Mrs." });
            ViewBag.Title = lstTitle.ToList();

            return PartialView("_SubmitFormPartial", objCodeStandard);
        }

        [HttpPost]
        public ActionResult SaveData(CodeStandard model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (model.UserId > 0)
                {
                    var objCodeStandard = db.CodeStandard.Where(x => x.UserId == model.UserId).FirstOrDefault();

                    objCodeStandard.Title = model.Title;
                    objCodeStandard.FirstName = model.FirstName;
                    objCodeStandard.LastName = model.LastName;
                    objCodeStandard.Roles = model.Roles;
                    objCodeStandard.Address = model.Address;
                    objCodeStandard.Birthdate = model.Birthdate;
                    objCodeStandard.Gender = model.Gender;
                    objCodeStandard.Salary = model.Salary;
                    objCodeStandard.Married = model.Married;
                    objCodeStandard.EditedBy = objClsLoginInfo.UserName;
                    objCodeStandard.EditedOn = DateTime.Now;

                    db.SaveChanges();
                    objResponseMsg.Value = "Information Updated Successfully.";
                }
                else
                {
                    CodeStandard objCodeStandard = new CodeStandard
                    {
                        Title = model.Title,
                        FirstName = model.FirstName,
                        LastName = model.LastName,
                        Roles = model.Roles,
                        Address = model.Address,
                        Birthdate = model.Birthdate,
                        Gender = model.Gender,
                        Salary = model.Salary,
                        Married = model.Married,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    };

                    db.CodeStandard.Add(objCodeStandard);
                    db.SaveChanges();
                    objResponseMsg.Value = "Data save successfully.";
                }

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 4. User Role Access, Session Expiry
        public ActionResult UserAccessSession()
        {
            return View();
        }
        #endregion

        #region 5. Boot Box Popup (partial view)/alert/confirmation
        public ActionResult BootBoxPopups()
        {
            return View();
        }

        #endregion

        #region 6. File Attachment
        [SessionExpireFilter]
        public ActionResult FileAttachment()
        {
            return View();
        }
        #endregion

        #region 7. Open SSRS reports
        public ActionResult SSRSReports()
        {
            return View();
        }
        #endregion

        #region 8. Graphs
        public ActionResult Graphs()
        {
            return View();
        }
        #endregion

        #region 9. Charts
        [SessionExpireFilter]
        public ActionResult Charts()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ChartsUserDetail()
        {
            clsHelper.DashboardData objResponseMsg = new clsHelper.DashboardData();
            try
            {
                var result = db.SP_CODINGSTANDARD_GETUSERDETAIL(1, int.MaxValue, "", " 1=1").ToList();
                objResponseMsg.ChartCodeStandard = result.Select(s => new clsHelper.DashboardChartData { SrNO = Convert.ToInt32(s.ROW_NO), UserName = s.FirstName, Salary = s.Salary }).ToList();

                var pieChart = db.SP_CODINGSTANDARD_CHARTS().ToList();
                objResponseMsg.PieChartCodeStandard = pieChart.Select(s => new clsHelper.DashboardChartData { Salary = Convert.ToDecimal(s.Salary), TotalCount = s.TotalCount }).ToList();
                objResponseMsg.Key = true;
                return Json(objResponseMsg);
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
                return Json(objResponseMsg);
            }
        }

        #region EjDiagram
        [SessionExpireFilter]
        public ActionResult ejDiagram()
        {
            return View();
        }
        public JsonResult GetHBOMNodes(string project)
        {
            var Nodes = db.SP_IPI_GETHBOMLIST_VW(project).OrderBy(x => x.PartLevel).ToList();
            if (Nodes.Any())
            {
                var node = db.SP_IPI_GETHBOMLIST_VW(project).OrderBy(x => x.PartLevel).FirstOrDefault();
                node.Part = node.ParentPart;
                node.ParentPart = "";
                Nodes.Insert(0, node);
            }
            return Json(Nodes, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMenuNodes()
        {
            var Nodes = db.ATH002.Where(w => w.IsActive == "1").Take(100).ToList();
            return Json(Nodes, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region 10. All Action Buttons
        public ActionResult Buttons()
        {
            return View();
        }
        #endregion

        #region 11. Timeline
        public ActionResult Timeline(int id)
        {
            ViewBag.UserId = id;
            return View();
        }

        [HttpPost]
        public ActionResult ShowTimeline(int UserId)
        {
            TimelineViewModel model = new TimelineViewModel();

            CodeStandard objCodeStandard = db.CodeStandard.Where(x => x.UserId == UserId).FirstOrDefault();
            model.SubmittedBy = Manager.GetUserNameFromPsNo(objCodeStandard.EditedBy);
            model.SubmittedOn = objCodeStandard.EditedOn;
            model.CreatedBy = Manager.GetUserNameFromPsNo(objCodeStandard.CreatedBy);
            model.CreatedOn = objCodeStandard.CreatedOn;
            model.Title = "QCP";
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        #endregion

        #region 12. Notification
        public ActionResult Notification()
        {
            return View();
        }
        #endregion

        #region 13. Send Emails
        [SessionExpireFilter]
        public ActionResult Emails()
        {
            MailConfiguration mailConfiguration = new MailConfiguration();
            ViewBag.Title = "Send Emails";
            return View(mailConfiguration);
        }

        [HttpPost]
        public ActionResult SendMail(MailConfiguration objemail)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                #region Send Mail
                if (!string.IsNullOrWhiteSpace(objemail.TemplateName))
                {
                    EmailSend _objEmail = new EmailSend();
                    _objEmail.MailCc = objemail.MAIL001.CCEmail;
                    _objEmail.MailSub = objemail.MAIL001.Subject;
                    _objEmail.MailBcc = objemail.MAIL001.BCCEmail;
                    _objEmail.MailBody = objemail.ContentBody;
                    _objEmail.MailToAdd = objemail.TemplateName; //initiator
                    _objEmail.SendMail(_objEmail, null, objemail.MAIL001);
                }
                #endregion
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Mail Sent Successfully";
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region 14. Common Methods
        public ActionResult CommonMethods()
        {
            return View();
        }

        #endregion

        #region 15. Export Excel
        //Code By : Nikita Vibhandik 06/02/2018 
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                //Main grid(user detail)
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_CODINGSTANDARD_GETUSERDETAIL(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Title = Convert.ToString(uc.Title),
                                      FullName = Convert.ToString(uc.FullName),
                                      Roles = Convert.ToString(uc.Roles),
                                      Address = Convert.ToString(uc.Address),
                                      Birthdate = uc.Birthdate == null || uc.Birthdate.Value == DateTime.MinValue ? "" : uc.Birthdate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),
                                      Gender = Convert.ToString(uc.Gender),
                                      Salary = Convert.ToString(uc.Salary),
                                      Married = Convert.ToString(uc.Married == true ? "Yes" : "No"),
                                      Status = Convert.ToString(uc.Status),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region 16. Tree View

        public ActionResult Treeview()
        {
            return View();
        }
        [HttpPost]
        public ActionResult LoadTreeViewPartial()
        {
            return PartialView("_LoadTreeview");
        }

        public ActionResult GetTreeNodes()
        {
            //List<TreeviewEnt> listTreeviewEnt = new List<TreeviewEnt>();
            try
            {

                //TreeviewEnt treeviewEnt = new TreeviewEnt();
                //treeviewEnt.text = "Parent Node 1";
                //TreeviewEnt chileTreeviewEnt = new TreeviewEnt();
                //chileTreeviewEnt.text = "P1-C1";
                //chileTreeviewEnt.state.opened = true;
                //treeviewEnt.children.Add(chileTreeviewEnt);
                //listTreeviewEnt.Add(treeviewEnt);

                //treeviewEnt = new TreeviewEnt();
                //treeviewEnt.text = "Parent Node 2";
                //chileTreeviewEnt = new TreeviewEnt();
                //chileTreeviewEnt.text = "P2-C1";
                //chileTreeviewEnt.state.opened = true;
                //treeviewEnt.children.Add(chileTreeviewEnt);
                //listTreeviewEnt.Add(treeviewEnt);

                listTreeviewEnt = new List<TreeviewEnt>();
                listFKM102 = db.FKM102.Where(i => i.HeaderId == 1).ToList();
                var listParentNode = listFKM102.Where(i => i.ParentNodeId == 0).ToList();
                foreach (var item in listParentNode)
                {
                    TreeviewEnt mainTreeviewEnt = new TreeviewEnt();
                    mainTreeviewEnt = GetNodesStructure(item, mainTreeviewEnt);
                    listTreeviewEnt.Add(mainTreeviewEnt);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(listTreeviewEnt, JsonRequestBehavior.AllowGet);
            }
            return Json(listTreeviewEnt, JsonRequestBehavior.AllowGet);
        }

        public TreeviewEnt GetNodesStructure(FKM102 objFKM102, TreeviewEnt treeviewEnt)
        {
            //TreeviewEnt treeviewEnt = new TreeviewEnt();
            if (objFKM102.ParentNodeId == 0)
            {

                treeviewEnt.text = objFKM102.NodeName;
                if (listFKM102.Any(i => i.ParentNodeId == objFKM102.NodeId))
                {
                    var childs = listFKM102.Where(i => i.ParentNodeId == objFKM102.NodeId).ToList();
                    foreach (var item in childs)
                    {
                        TreeviewEnt childnode = new TreeviewEnt();
                        childnode = GetNodesStructure(item, childnode);
                        treeviewEnt.children.Add(childnode);
                    }
                }
            }
            else
            {
                treeviewEnt.text = objFKM102.NodeName;
                if (listFKM102.Any(i => i.ParentNodeId == objFKM102.NodeId))
                {
                    var childs = listFKM102.Where(i => i.ParentNodeId == objFKM102.NodeId).ToList();
                    foreach (var item in childs)
                    {
                        TreeviewEnt childnode = new TreeviewEnt();
                        childnode = GetNodesStructure(item, childnode);
                        treeviewEnt.children.Add(childnode);
                    }
                }
            }
            return treeviewEnt;
        }

        public void PopulateTree(ref TreeNode root, List<FKM102> departments)
        {
            if (root == null)
            {
                root = new TreeNode();
                root.Text = "Equipment";
                root.Tag = 0;
                // get all departments in the list with parent is null
                var details = departments.Where(x => x.ParentNodeId == 0).ToList();
                foreach (var detail in details)
                {
                    var child = new TreeNode()
                    {
                        Text = detail.NodeName,
                        Tag = detail.NodeId,
                    };
                    PopulateTree(ref child, departments);
                    root.Nodes.Add(child);
                }
            }
            else
            {
                var id = (int)root.Tag;
                var details = departments.Where(t => t.ParentNodeId == id);
                foreach (var detail in details)
                {
                    var child = new TreeNode()
                    {
                        Text = detail.NodeName,
                        Tag = detail.NodeId,
                    };
                    PopulateTree(ref child, departments);
                    root.Nodes.Add(child);
                }
            }
        }


        #endregion

        #region 17. Elmah_Error 
        public ActionResult ElmahError()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial()
        {
            return PartialView("_LoadElmahErrorGridPartial");
        }
        public ActionResult LoadElmahErrorData(JQueryDataTableParamModel param)
        {
            try
            {
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";


                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "ColumnInLocalTime", "Type", "StatusCode", "Application", "Host", "IPAddress", "Browser" };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                string strSortOrder = string.Empty;
                var lstPam = db.SP_ELMAHERROR_GETERRORDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.ColumnInLocalTime),
                               Helper.GenerateActionIcon(h.ErrorId.ToString(), "Error", "Error Details", "fa fa-bug fa-lg", "OpenErrorDetailsPopup("+ "'"+ h.ErrorId + "'" +")","",false,false,"color:red;"),
                               Convert.ToString(h.Type),
                               Convert.ToString(h.StatusCode),
                               //Convert.ToString(h.Message),
                               Helper.GenerateActionIcon(h.ErrorId.ToString(), "Message", "Meassage", "iconspace fa fa-envelope-o fa-lg", "OpenMessagePopup("+ "'"+ h.ErrorId + "'" +")","",false,false,"color:green;"),
                               Convert.ToString(h.Application),
                               Convert.ToString(h.Host),
                               Convert.ToString(h.IPAddress),
                               Convert.ToString(h.Browser),
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult ElmahErrorDetails(string ErrorId)
        {
            var ErrorIdParse = Guid.Parse(ErrorId);
            var objElmaherror = db.ELMAH_Error.Where(m => m.ErrorId == ErrorIdParse).Select(x => x.AllXml).FirstOrDefault();

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(objElmaherror);
            XmlElement root = doc.DocumentElement;
            string str = null;
            if (root.HasAttribute("detail"))
            {
                str = root.GetAttribute("detail");
                str = str.Replace(" at ", "<br/> at ");
            }
            return Json(str, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowErrorMessage(string ErrorId)
        {
            var ErrorIdParse = Guid.Parse(ErrorId);
            var objElmaherror = db.ELMAH_Error.Where(m => m.ErrorId == ErrorIdParse).Select(x => x.Message).FirstOrDefault();
            return Json(objElmaherror, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region 18. SQL Query Pad
        public JsonResult GetAllTableData()
        {
            var lst = db.Table_Data.Select(m => new { m.Id, m.Name, m.Description }).OrderBy(m => m.Id).ToList();
            return Json(lst, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CurrentStateQueryPadPartial()//string currentState
        {
            return PartialView("~/Views/Shared/_QueryPadPartial.cshtml");
        }
        public ActionResult QueryPadExecuteQuery(string query)
        {
            try
            {
                string[] exclude = { "insert", "update", "delete", "drop", "create", "alter", "truncate", "comment", "rename", "merge", "call", "grant", "revoke", "commit", "rollback", "transaction", "lock" };
                if (query.ToLower().StartsWith("select") && !exclude.Any(query.ToLower().Contains))
                {
                    DataTable datatable = Helper.GetDatableFromSql(query.Replace('\n', ' ').Replace('\t', ' '));
                    var results = Helper.ConvertDatatableToDynamicList(datatable);
                    string[] FieldNames = { "project", "t_cprj", "project_no" };
                    List<string> propertyKeys = ((IDictionary<string, object>)results?.FirstOrDefault())?.Keys.Select(m => m.ToLower()).ToList();
                    string column = Helper.GetPropertyName(propertyKeys, FieldNames);
                    if (!string.IsNullOrEmpty(column))
                    {
                        var projects = db.fn_GetProjectAccessForEmployee(objClsLoginInfo.UserName, string.Empty).ToList();
                        if (column == "project")
                            results = results.Where(m => projects.Contains(m.Project)).ToList();
                        else if (column == "t_cprj")
                            results = results.Where(m => projects.Contains(m.t_cprj)).ToList();
                        else if (column == "project_no")
                            results = results.Where(m => projects.Contains(m.project_no)).ToList();
                    }
                    var jsonSettings = new JsonSerializerSettings();
                    jsonSettings.DateFormatString = "dd/MM/yyy";
                    var json = JsonConvert.SerializeObject(results, jsonSettings);
                    if (results.Count() == 0)
                        return Json(new { msg = "No records found. Please select more records", success = false }, JsonRequestBehavior.AllowGet);
                    else if (results.Count() > 1000)
                        return Json(new { msg = "Please select top 1-100 records only.", success = false }, JsonRequestBehavior.AllowGet);
                    return Json(new { data = json, success = true }, JsonRequestBehavior.AllowGet);
                }
                return Json(new { msg = "You can only perform SELECT statment only.", success = false }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new { msg = ex.Message, success = false }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region 19. Return Result

        // if returning a View then signature of method should be ViewResult
        // instead of ActionResult.
        public ViewResult ReturnView()
        {
            return View();
        }

        public JsonResult GetJsonResult()
        {
            return Json(null, JsonRequestBehavior.AllowGet);
        }
        public PartialViewResult GetPartialView()
        {
            return PartialView("_[Name of Partial View]", new object());
        }

        /// <summary>
        /// File result to return actual file from path
        /// specify content type to identify type of file
        /// whatever it is PDF or excel or doc etc...
        /// </summary>
        /// <returns>FileResult</returns>
        public FileResult GetFileResult()
        {
            // add content-type if required
            return File("[file path]", "application/octet-stram");
        }

        #endregion

        #region 20. Avoid using NoAction attribute

        [NonAction]
        public ActionResult TestActionWithAttribute()
        {
            return View();
        }

        // Instead of above. use below.
        // end result of this both will be same by avoiding extra action filter(s).
        private ActionResult TestActionWithNoAttribute()
        {
            return View();
        }

        #endregion

        #region 21. use async await whenever needed at server side or client side
        public async Task<ActionResult> GetResultAsync()
        {
            dynamic myVar = 0;
            return await Task.FromResult<dynamic>(myVar);
        }
        #endregion

        #region Protocol Editable grid
        [SessionExpireFilter]
        public ActionResult ProtocolsEditableDataGrid(int? id, int? m)
        {
            PRO175 objPRO175 = new PRO175();
            string ControllerURL = "/TEMP/CodingStandard/";
            string Title = "SET-UP & DIMENSION REPORT FOR ROLLED SHELL LONGITUDINAL SEAM";

            if (!id.HasValue)
            {
                try
                {
                    objPRO175.ProtocolNo = string.Empty;
                    objPRO175.CreatedBy = objClsLoginInfo.UserName;
                    objPRO175.CreatedOn = DateTime.Now;

                    objPRO175.PRO178.Add(new PRO178
                    {
                        Location = "TOP",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    objPRO175.PRO178.Add(new PRO178
                    {
                        Location = "MIDDLE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO175.PRO178.Add(new PRO178
                    {
                        Location = "BOTTOM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    db.PRO175.Add(objPRO175);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO175.HeaderId;
            }
            else
            {
                objPRO175 = db.PRO175.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            List<PRO178> lstPRO178 = db.PRO178.Where(x => x.HeaderId == objPRO175.HeaderId).ToList();
            ViewBag.Line3Data = lstPRO178;

            return View(objPRO175);
        }

        [HttpPost]
        public JsonResult SaveProtocolLine(List<PRO178> lst)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO178> lstAddPRO178 = new List<PRO178>();
                List<PRO178> lstDeletePRO178 = new List<PRO178>();

                foreach (var item in lst)
                {
                    if (item.LineId > 0)
                    {
                        PRO178 obj = db.PRO178.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.Location = item.Location;
                            obj.ActIDCF = item.ActIDCF;
                            obj.ActODCF = item.ActODCF;
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                    }
                    else
                    {
                        PRO178 obj = new PRO178();
                        obj.HeaderId = item.HeaderId;
                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.ActIDCF = item.ActIDCF;
                            obj.ActODCF = item.ActODCF;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            lstAddPRO178.Add(obj);
                        }
                    }
                }
                if (lstAddPRO178.Count > 0)
                {
                    db.PRO178.AddRange(lstAddPRO178);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                lstDeletePRO178 = db.PRO178.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                if (lstDeletePRO178.Count > 0)
                {
                    db.PRO178.RemoveRange(lstDeletePRO178);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public class TreeviewEnt
        {
            public TreeviewEnt()
            {
                state = new TreeViewStateEnt();
                children = new List<TreeviewEnt>();
            }
            public string text { get; set; }
            public string icon { get; set; }
            public TreeViewStateEnt state { get; set; }
            public List<TreeviewEnt> children { get; set; }
        }
        public class TreeViewStateEnt
        {
            public bool opened { get; set; }
            public bool disabled { get; set; }
        }

        public class TreeNode
        {
            public TreeNode()
            {
                Nodes = new List<TreeNode>();
            }
            public string Text { get; set; }
            public int Tag { get; set; }

            public List<TreeNode> Nodes { get; set; }
        }

        public class clsDataFlow
        {
            [JsonProperty("class")]
            public string Class { get; set; }
            public string nodeCategoryProperty { get; set; }
            public string linkFromPortIdProperty { get; set; }
            public string linkToPortIdProperty { get; set; }
            public List<nodeDataArray> nodeDataArray { get; set; }
            public List<linkDataArray> linkDataArray { get; set; }
            public clsDataFlow()
            {
                Class = "go.GraphLinksModel";
                nodeCategoryProperty = "type";
                linkFromPortIdProperty = "frompid";
                linkToPortIdProperty = "topid";
            }
        }

        public class nodeDataArray
        {
            public string key { get; set; }
            public string type { get; set; }
            public string name { get; set; }
        }

        public class linkDataArray
        {
            public string from { get; set; }
            public string frompid { get; set; }
            public string to { get; set; }
            public string topid { get; set; }
        }

        public string MakeJqGridSearchCondition(filters oFilters)
        {
            string strWhereCondition = string.Empty;
            try
            {
                int count = 0;
                bool bData = false;

                foreach (var item in oFilters.rules)
                {
                    if (!string.IsNullOrWhiteSpace(item.data) && item.data != "false")
                    {
                        bData = true;
                        if (count == 0)
                        {

                            strWhereCondition += MakeJqSearchWithMultipleCondition(item.op, item.field, item.data, "string", "");
                        }
                        else
                        {
                            strWhereCondition += MakeJqSearchWithMultipleCondition(item.op, item.field, item.data, "string", oFilters.groupOp);
                        }
                        count++;
                    }
                }
                if (bData && count > 0)
                {
                    strWhereCondition = "and ( " + strWhereCondition + ") ";
                }

            }
            catch (Exception ex)
            {
                ex.Message.ToString();
            }
            return strWhereCondition;
        }
        public string MakeJqSearchWithMultipleCondition(string filterstatus, string column_name, string column_value, string column_type = "string", string operation = "and")
        {
            string strLikeFormat = string.Empty;
            StringBuilder sb = new StringBuilder();
            if (column_type == "date")
                column_name = "CONVERT(date, " + column_name + ")";
            sb.Append(" " + operation + " ( " + column_name + " ");
            if (filterstatus == "eq")
            {
                sb.Append("= '" + column_value + "' ");
            }
            else if (filterstatus == "ne")
            {
                sb.Append("!= '" + column_value + "' ");
            }
            else if (filterstatus == "bw")
            {
                sb.Append("like '" + column_value.Replace("%", "[%]").Replace("_", "[_]") + "%' ");
            }
            else if (filterstatus == "gt")
            {
                sb.Append(">= '" + column_value + "' ");
            }
            else if (filterstatus == "le")
            {
                sb.Append("<= '" + column_value + "' ");
            }
            else if (filterstatus == "gt")
            {
                sb.Append("> '" + column_value + "' ");
            }
            else if (filterstatus == "lt")
            {
                sb.Append("< '" + column_value + "' ");
            }
            else if (filterstatus == "cn")
            {
                sb.Append("like '%" + column_value.Replace("%", "[%]").Replace("_", "[_]") + "%'");
            }
            else if (filterstatus == "nc")
            {
                sb.Append("not like '%" + column_value.Replace("%", "[%]").Replace("_", "[_]") + "%'");
            }
            else if (filterstatus == "ew")
            {
                sb.Append("like '%" + column_value.Replace("%", "[%]").Replace("_", "[_]") + "'");
            }
            else if (filterstatus == "en")
            {
                sb.Append("not like '%" + column_value.Replace("%", "[%]").Replace("_", "[_]") + "'");
            }
            else // default by data type
            {
                if (column_type == "string")
                    sb.Append("like '%" + column_value.Replace("%", "[%]").Replace("_", "[_]") + "%'");
                else
                    sb.Append("= '" + column_value + "' ");
            }

            sb.Append(") ");
            strLikeFormat = sb.ToString();
            return strLikeFormat;
        }
    }
}