﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace IEMQS.Areas.TEMP
{
    public partial class TestReport : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            ReportViewer1.LocalReport.ReportPath = "https://bi.lthed.com/BIReports/powerbi/SamplePBI/IT%20Spend%20Analysis%20Sample%20PBIX";
            ReportViewer1.LocalReport.Refresh();
        }
    }
}