﻿using System.Web.Mvc;

namespace IEMQS.Areas.TEMP
{
    public class TEMPAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "TEMP";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ElmahError",                                           // Route name
                "ErrorLog",                            // URL with parameters
                new { AreaName= "TEMP", controller = "CodingStandard", action = "ElmahError" }  // Parameter defaults
            );

            context.MapRoute(
                "TEMP_default",
                "TEMP/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}