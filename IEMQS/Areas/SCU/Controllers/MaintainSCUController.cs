﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.SCU.Controllers
{
    public class MaintainSCUController : clsBase
    {
        // GET: SCU/MaintainSCU
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.IsDisplay = false;
            ViewBag.Title = "Maintain S-Curve Master Data";
            return View();
        }

        [SessionExpireFilter]
        public ActionResult Display()
        {
            ViewBag.IsDisplay = true;
            ViewBag.Title = "Display S-Curve Master Data";
            return View("Index", new SCU001());
        }

        #region Master Data 

        [HttpPost]
        public ActionResult GetIndexGridDataPartial(bool isDisplay = false)
        {
            ViewBag.IsDisplay = isDisplay;

            return PartialView("_GetIndexGridDataPartial");
        }

        public JsonResult LoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = (Convert.ToInt32(Request["iSortCol_0"]) == 0 ? 2 : Convert.ToInt32(Request["iSortCol_0"]));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = "1=1 ";

                string[] columnName = { "Phase", "DoumentNo", "DoumentDesc", "Days", "PlannedQty", "ActualQty", "HrsPerDoc", "TotalHrs", "CONVERT(nvarchar(20),CreatedOn,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SCU_GET_MAINTAIN_DATA(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Id),
                               Convert.ToString(uc.Phase),
                               Convert.ToString(uc.DoumentNo),
                               Convert.ToString(uc.DoumentDesc),
                               Convert.ToString(uc.Days),
                               Convert.ToString(uc.PlannedQty),
                               Convert.ToString(uc.ActualQty),
                               Convert.ToString(uc.HrsPerDoc),
                               Convert.ToString((uc.TotalHrs)),
                               (param.IsDisplayOnly ? "": "<span class='editable'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='DeleteLine("+uc.Id+")'><i class='fa fa-trash'></i></a></span>")
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();

                if (id == 0)
                {
                    var QPitems = new SelectList(new List<string>());
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        "0",
                                        Helper.GenerateTextbox(newRecordId,"Phase",  "", "", false, "", "3","numeric3 Phase"),
                                        Helper.GenerateTextbox(newRecordId, "DoumentNo",  "", "", false, "", "50","form-control DoumentNo"),
                                        Helper.GenerateTextbox(newRecordId, "DoumentDesc",  "", "", false, "", "100","form-control DoumentDesc"),
                                        Helper.GenerateTextbox(newRecordId,"Days",  "", "", false, "", "3","numeric3 Days"),
                                        Helper.GenerateTextbox(newRecordId,"PlannedQty",  "", "", false, "", "3","numeric3 PlannedQty"),
                                        Helper.GenerateTextbox(newRecordId,"ActualQty",  "", "", false, "", "3","numeric3 ActualQty"),
                                        Helper.GenerateTextbox(newRecordId,"HrsPerDoc",  "", "", false, "", "3","numeric3 HrsPerDoc"),
                                        Helper.GenerateTextbox(newRecordId,"TotalHrs",  "", "", true),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "Savedata()" ),
                                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_SCU_GET_MAINTAIN_DATA(1, 0, "", "Id = " + id).Take(1).ToList();
                    if (isReadOnly)
                    {
                        data = (from uc in lstResult
                                select new[]
                               {
                                    Convert.ToString(uc.Id),
                                    Convert.ToString(uc.Phase),
                                    Convert.ToString(uc.DoumentNo),
                                    Convert.ToString(uc.DoumentDesc),
                                    Convert.ToString(uc.Days),
                                    Convert.ToString(uc.PlannedQty),
                                    Convert.ToString(uc.ActualQty),
                                    Convert.ToString(uc.HrsPerDoc),
                                    Convert.ToString((uc.TotalHrs)),
                                    "<span class='editable'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='DeleteLine("+uc.Id+")'><i class='fa fa-trash'></i></a></span>"
                          }).ToList();
                    }
                    else
                    {
                        data = (from uc in lstResult
                                select new[] {
                                        uc.Id.ToString(),
                                        Helper.GenerateTextbox(uc.Id, "Phase",  Convert.ToString(uc.Phase), "UpdateData(this, "+ uc.Id+",true)", false, "", "3","Phase numeric3"),
                                        Helper.GenerateTextbox(uc.Id, "DoumentNo",  Convert.ToString(uc.DoumentNo), "UpdateData(this, "+ uc.Id+",true)", false, "", "50","form-control DoumentNo"),
                                        Helper.GenerateTextbox(uc.Id, "DoumentDesc",  Convert.ToString(uc.DoumentDesc), "UpdateData(this, "+ uc.Id+",true)", false, "", "100","form-control DoumentDesc"),
                                        Helper.GenerateTextbox(uc.Id, "Days",  Convert.ToString(uc.Days), "UpdateData(this, "+ uc.Id+",true)", false, "", "3","Days numeric3"),
                                        Helper.GenerateTextbox(uc.Id, "PlannedQty",  Convert.ToString(uc.PlannedQty), "UpdateData(this, "+ uc.Id+",true)", false, "", "3","PlannedQty numeric3"),
                                        Helper.GenerateTextbox(uc.Id, "ActualQty",  Convert.ToString(uc.ActualQty), "UpdateData(this, "+ uc.Id+",true)", false, "", "3","ActualQty numeric3"),
                                        Helper.GenerateTextbox(uc.Id, "HrsPerDoc",  Convert.ToString(uc.HrsPerDoc), "UpdateData(this, "+ uc.Id+",true)", false, "", "3","HrsPerDoc numeric3"),
                                        Helper.GenerateTextbox(uc.Id, "TotalHrs",  Convert.ToString((uc.TotalHrs)), "", true),
                                        ("<span class='editable'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='DeleteLine("+uc.Id+")'><i class='fa fa-trash'></i></a></span> &nbsp" ) +
                                        "<span class='editable'><a id='Cancel' name='Cancel' title='Cancel' class='blue action' onclick='ReloadGrid()'><i class='fa fa-close'></i></a></span>",
                          }).ToList();
                    }
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveNewRecord(FormCollection fc)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objSCU001 = new SCU001();

                objSCU001.DoumentNo = fc["DoumentNo" + 0];
                objSCU001.Phase = Convert.ToInt32(!string.IsNullOrEmpty(fc["Phase" + 0]) ? Convert.ToInt32(fc["Phase" + 0]) : 0);

                if (!string.IsNullOrEmpty(objSCU001.DoumentNo))
                {
                    if (db.SCU001.Any(x => x.DoumentNo.Trim().ToLower() == objSCU001.DoumentNo.Trim().ToLower() && x.Phase == objSCU001.Phase))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = objSCU001.DoumentNo + " Already Exists";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                }

                objSCU001.DoumentDesc = fc["DoumentDesc" + 0];
                objSCU001.Days = Convert.ToDouble(fc["Days" + 0]);
                objSCU001.PlannedQty = Convert.ToDouble(fc["PlannedQty" + 0]);
                objSCU001.ActualQty = Convert.ToDouble(fc["ActualQty" + 0]);
                objSCU001.HrsPerDoc = Convert.ToDouble(fc["HrsPerDoc" + 0]);
                objSCU001.CreatedOn = DateTime.Now;
                objSCU001.CreatedBy = objClsLoginInfo.UserName;

                db.SCU001.Add(objSCU001);
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data save successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteLines(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            var objSCU001 = db.SCU001.FirstOrDefault(x => x.Id == HeaderId);
            try
            {
                if (objSCU001 != null)
                {
                    db.SCU001.Remove(objSCU001);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record deleted successfully";
                }
                else
                {
                    objResponseMsg.Value = "Some thing Went to Wrong, Please try Again Later";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult UpdateDetails(int id, string columnName, string columnValue)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                columnValue = columnValue.Replace("'", "");

                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    var objscu001 = db.SCU001.Where(x => x.Id == id).FirstOrDefault();
                    if (columnName.ToLower() == "doumentno")
                    {
                        if (objscu001.DoumentNo.Trim().ToLower() != columnValue.Trim().ToLower())
                        {
                            if (db.SCU001.Any(x => x.DoumentNo.Trim().ToLower() == columnValue.Trim().ToLower() && x.Phase == objscu001.Phase))
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = columnValue + " Already Exists";
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                    else if (columnName.ToLower() == "phase")
                    {
                        int objphase = Convert.ToInt32(columnValue);
                        if (objscu001.Phase != objphase)
                        {
                            if (db.SCU001.Any(x => x.DoumentNo == objscu001.DoumentNo && x.Phase == objphase))
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = columnValue + " Already Exists";
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }

                    db.SP_COMMON_TABLE_UPDATE("SCU001", id, "Id", columnName, columnValue, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
                else
                {
                    db.SP_COMMON_TABLE_UPDATE("SCU001", id, "Id", columnName, columnValue, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Project wise Master Data

        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Detail(int HeaderId = 0)
        {
            DefaultMasterEntry(HeaderId);
            ViewBag.headerid = HeaderId;
            var objpdn001 = db.PDN001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            if (objpdn001 != null)
            {
                ViewBag.SCUDashboardURL = Manager.GetSCUDashboardLink(objpdn001.Project);
            }
            ViewBag.Title = "Maintain S-Curve Project wise";

            return View();
        }

        [HttpPost]
        public ActionResult GetProjectWiseGridDataPartial(int HeaderId, bool isrelease = false, bool isissue = false)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                ViewBag.headerid = HeaderId;
                ViewBag.isissue = isissue;
                var objpdn001 = db.PDN001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                ViewBag.isrelease = isrelease;
                if (objpdn001 != null)
                {
                    ViewBag.SCUDashboardURL = Manager.GetSCUDashboardLink(objpdn001.Project);
                    string zerodate = Manager.Getzerodate(objpdn001.Project);
                    if (string.IsNullOrEmpty(zerodate))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "This Project Against Not Maintained Zero Date";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        DateTime objZeroDate = Convert.ToDateTime(zerodate);
                        zerodate = objZeroDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    ViewBag.Project = objpdn001.Project;
                    ViewBag.Zero = zerodate;
                    ViewBag.CDD = Convert.ToString(objpdn001.CDD.HasValue ? objpdn001.CDD.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : "");
                    DefaultMasterEntry(HeaderId);
                }
                return PartialView("_GetProjectWiseGridDataPartial");
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ProjectwiseLoadDataGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = (Convert.ToInt32(Request["iSortCol_0"]) == 0 ? 5 : Convert.ToInt32(Request["iSortCol_0"]));
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = "1=1 ";
                int HeaderId = 0;
                if (!string.IsNullOrEmpty(param.Headerid))
                {
                    HeaderId = Convert.ToInt32(param.Headerid);
                    var objpdn001 = db.PDN001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    if (objpdn001 != null)
                    {
                        strWhere += " and Project='" + objpdn001.Project + "' and location = '" + objpdn001.Location + "'  ";
                    }
                    else
                    {
                        return Json(new
                        {
                            sEcho = param.sEcho,
                            iTotalRecords = "0",
                            iTotalDisplayRecords = "0",
                            aaData = new string[] { }
                        }, JsonRequestBehavior.AllowGet);
                    }
                }

                if (param.Flag)
                {
                    var DraftStatus = clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue();
                    var ApprovedStatus = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                    var objPDN002Notification = db.PDN002.Where(i => i.Status == ApprovedStatus && i.IssueStatus == DraftStatus && i.HeaderId == HeaderId).Select(x => x.DocumentNo).ToList();

                    strWhere += " and DoumentNo in ('" + string.Join("','", objPDN002Notification) + "') ";
                }

                string[] columnName = { "Phase", "DoumentNo", "DoumentDesc", "Days", "PlannedQty", "ActualQty", "HrsPerDoc", "Project", "TotalHrs", "CONVERT(nvarchar(20),CDD,103)", "CONVERT(nvarchar(20),ZeroDate,103)" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_SCU_GET_MAINTAIN_PROJECT_DATA(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.Id),
                               Convert.ToString(uc.Project),
                               Convert.ToString(uc.ZeroDate.HasValue ? uc.ZeroDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),
                               Convert.ToString(uc.CDD.HasValue ? uc.CDD.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : ""),
                               Convert.ToString(uc.Phase),
                               Convert.ToString(uc.DoumentNo),
                               Generatetooltipdesc(Convert.ToString(uc.DoumentDesc),"DoumentDesc"),
                               Convert.ToString(uc.Days),
                               Convert.ToString(uc.PlannedQty),
                               Convert.ToString(uc.ActuQTY),
                               Convert.ToString(uc.HrsPerDoc),
                               Convert.ToString((uc.TotalHrs)),
                               (param.Flag ? "" : (param.IsDisplayOnly ? "" : (uc.Isundo == 1 ? "<span class='editable'><a id='Undo' name='Undo' title='Undo' class='blue action' onclick='UndoLine("+uc.Id + "," + uc.sid+")'><i class='fa fa-undo'></i></a></span>&nbsp":"") + "<span class='editable'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='Delete("+uc.Id+")'><i class='fa fa-trash'></i></a></span>"))
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult ProjectwiseGetInlineEditableRow(int id, string hid, bool isReadOnly = false, bool isrelease = false, bool isissue = false)
        {
            try
            {
                var data = new List<string[]>();

                if (id == 0)
                {
                    int headerid = Convert.ToInt32(hid);
                    var objpdn001 = db.PDN001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                    string zerodate = string.Empty;
                    if (objpdn001 != null)
                    {
                        zerodate = Manager.Getzerodate(objpdn001.Project);
                        DateTime objZeroDate = Convert.ToDateTime(zerodate);
                        zerodate = objZeroDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture);
                    }
                    var QPitems = new SelectList(new List<string>());
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        "0",
                                        Helper.GenerateTextbox(newRecordId, "Project", (objpdn001 != null?objpdn001.Project:""), "", true, "", "50","form-control Project"),
                                        Helper.GenerateTextbox(newRecordId, "ZeroDate",  zerodate, "",true,"","","form-control datePicker ZeroDate"),
                                        Helper.GenerateTextbox(newRecordId, "CDD", (objpdn001 != null?objpdn001.CDD.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture):""), "",true,"","","form-control datePicker CDD"),
                                        Helper.GenerateTextbox(newRecordId,"Phase",  "", "", false, "", "3","numeric3 Phase"),
                                        Helper.GenerateTextbox(newRecordId, "DoumentNo",  "", "", false, "", "50","form-control DoumentNo"),
                                        Helper.GenerateTextbox(newRecordId, "DoumentDesc",  "", "", false, "", "100","form-control DoumentDesc"),
                                        Helper.GenerateTextbox(newRecordId,"Days",  "", "", false, "", "3","numeric3 Days"),
                                        Helper.GenerateTextbox(newRecordId,"PlannedQty",  "", "", false, "", "3","numeric3 PlannedQty"),
                                        Helper.GenerateTextbox(newRecordId,"ActualQty",  "", "", false, "", "3","numeric3 ActualQty"),
                                        Helper.GenerateTextbox(newRecordId,"HrsPerDoc",  "", "", false, "", "3","numeric3 HrsPerDoc"),
                                        Helper.GenerateTextbox(newRecordId,"TotalHrs",  "", "", true),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "ProjectSavedata()" ),
                                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_SCU_GET_MAINTAIN_PROJECT_DATA(1, 0, "", "Id = " + id).Take(1).ToList();
                    if (isReadOnly)
                    {
                        data = (from uc in lstResult
                                select new[]
                               {
                                    Convert.ToString(uc.Id)
                                    ,Convert.ToString(uc.Project)
                                    ,Convert.ToString(uc.ZeroDate.HasValue ? uc.ZeroDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : "")
                                    ,Convert.ToString(uc.CDD.HasValue ? uc.CDD.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : "")
                                    ,Convert.ToString(uc.Phase)
                                    ,Convert.ToString(uc.DoumentNo)
                                    ,Convert.ToString(uc.DoumentDesc)
                                    ,Convert.ToString(uc.Days)
                                    ,Convert.ToString(uc.PlannedQty)
                                    ,Convert.ToString(uc.ActuQTY)
                                    ,Convert.ToString(uc.HrsPerDoc)
                                    ,Convert.ToString(uc.TotalHrs)
                                    ,"<span class='editable'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='Delete("+uc.Id+")'><i class='fa fa-trash'></i></a></span>" 
                          }).ToList();
                    }
                    else if (isrelease)
                    {
                        data = (from uc in lstResult
                                select new[]
                               {
                                    Convert.ToString(uc.Id)
                                    ,Convert.ToString(uc.Project)
                                    ,Convert.ToString(uc.ZeroDate.HasValue ? uc.ZeroDate.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : "")
                                    ,Convert.ToString(uc.CDD.HasValue ? uc.CDD.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture) : "")
                                    ,Convert.ToString(uc.Phase)
                                    ,Convert.ToString(uc.DoumentNo)
                                    ,Convert.ToString(uc.DoumentDesc)
                                    ,Convert.ToString(uc.Days)
                                    ,Convert.ToString(uc.PlannedQty)
                                    ,(uc.ActQTY > 0 ? "<div class='dateinline'>" + Helper.GenerateHidden(uc.Id,"ActQTY",Convert.ToString(uc.ActQTY)) +  Helper.GenerateTextbox(uc.Id, "ActuQTY", Convert.ToString(uc.ActuQTY), "", true, "", "","ActuQTY clrgreen pad1") + Helper.GenerateTextbox(uc.Id, "ActualQty",  "", "UpdateData(this, "+ uc.Id+",true)", false, "", "3","ActualQty numeric3 pad1" ) + Helper.GenerateTextbox(uc.Id, "ActQTY", Convert.ToString(uc.ActQTY), "", true, "", "","ActQTY clrorange pad1")  + "</div>" : Helper.GenerateTextbox(uc.Id, "ActuQTY",  Convert.ToString(uc.PlannedQty), "", true, "", "3","ActuQTY numeric3 clrgreen"))
                                    ,Convert.ToString(uc.HrsPerDoc)
                                    ,Convert.ToString(uc.TotalHrs)
                          }).ToList();
                    }
                    else
                    {
                        data = (from uc in lstResult
                                select new[] {

                                        uc.Id.ToString(),
                                        Helper.GenerateTextbox(uc.Id, "Project",  Convert.ToString(uc.Project), "", true, "", "50","form-control Project") + Helper.GenerateHidden(uc.Id,"ActQTY",Convert.ToString(uc.ActQTY)) + Helper.GenerateHidden(uc.Id,"ActuQTY",Convert.ToString(uc.ActuQTY)) ,
                                        Helper.GenerateTextbox(uc.Id, "ZeroDate",uc.ZeroDate.HasValue?uc.ZeroDate.Value.ToString("dd/MM/yyyy"):"","", true, "", "","form-control editable datePicker ZeroDate"),
                                        Helper.GenerateTextbox(uc.Id, "CDD",uc.CDD.HasValue?uc.CDD.Value.ToString("dd/MM/yyyy"):"","", true, "", "","form-control editable datePicker CDD"),
                                        Helper.GenerateTextbox(uc.Id, "Phase",  Convert.ToString(uc.Phase), "UpdateData(this, "+ uc.Id+",true)", (uc.DoumentNo == "HTR" ? true:false), "", "3","Phase numeric3"),
                                        Helper.GenerateTextbox(uc.Id, "DoumentNo",  Convert.ToString(uc.DoumentNo), "UpdateData(this, "+ uc.Id+",true)", true, "", "50","form-control DoumentNo"),
                                        Helper.GenerateTextbox(uc.Id, "DoumentDesc",  Convert.ToString(uc.DoumentDesc), "UpdateData(this, "+ uc.Id+",true)", (uc.DoumentNo == "HTR" ? true:false), "", "100","form-control DoumentDesc"),
                                        Helper.GenerateTextbox(uc.Id, "Days",  Convert.ToString(uc.Days), "UpdateData(this, "+ uc.Id+",true)", (uc.DoumentNo == "HTR" ? true:(isissue ? true:false)), "", "3","Days numeric3"),
                                        Helper.GenerateTextbox(uc.Id, "PlannedQty",  Convert.ToString(uc.PlannedQty), "UpdateData(this, "+ uc.Id+",true)", (uc.DoumentNo == "HTR" ? true:false), "", "3","PlannedQty numeric3"),
                                        (uc.DoumentNo == "HTR" ? Helper.GenerateTextbox(uc.Id, "ActuQTY",  Convert.ToString(uc.ActuQTY), "", true, "", "3","ActuQTY numeric3"): (uc.ActQTY > 0 ? "<div class='dateinline'>" + Helper.GenerateTextbox(uc.Id, "ActuQTY", Convert.ToString(uc.ActuQTY), "", true, "", "","ActuQTY clrgreen pad1") + Helper.GenerateTextbox(uc.Id, "ActualQty",  "", "UpdateData(this, "+ uc.Id+",true)", false, "", "3","ActualQty numeric3 pad1" ) + Helper.GenerateTextbox(uc.Id, "ActQTY", Convert.ToString(uc.ActQTY), "", true, "", "","ActQTY clrorange pad1")  + "</div>" : Helper.GenerateTextbox(uc.Id, "ActuQTY",  Convert.ToString(uc.PlannedQty), "", true, "", "3","ActuQTY numeric3 clrgreen"))),
                                        Helper.GenerateTextbox(uc.Id, "HrsPerDoc",  Convert.ToString(uc.HrsPerDoc), "UpdateData(this, "+ uc.Id+",true)", true, "", "3","HrsPerDoc numeric3"),
                                        Helper.GenerateTextbox(uc.Id, "TotalHrs",  Convert.ToString((uc.TotalHrs)), "", true),

                                        (uc.DoumentNo == "HTR" ? "": (uc.Isundo == 1 ? "<span class='editable'><a id='Undo' name='Undo' title='Undo' class='blue action' onclick='UndoLine("+uc.Id + "," + uc.sid+")'><i class='fa fa-undo'></i></a></span> &nbsp":"") +
                                        ("<span class='editable'><a id='Delete' name='Delete' title='Delete' class='blue action' onclick='Delete("+uc.Id+")'><i class='fa fa-trash'></i></a></span> &nbsp" ))+
                                        "<span class='editable'><a id='Cancel' name='Cancel' title='Cancel' class='blue action' onclick='ReloadGrid()'><i class='fa fa-close'></i></a></span>",
                          }).ToList();
                    }
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveRecord(FormCollection fc)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objSCU002 = new SCU002();
                var objSCU003 = new SCU003();
                var objSCU003_log = new SCU003_Log();

                int headerid = Convert.ToInt32(!string.IsNullOrEmpty(fc["headerid"]) ? Convert.ToInt32(fc["headerid"]) : 0);

                if (headerid > 0)
                {
                    var objpdn001 = db.PDN001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                    string zerodate = Manager.Getzerodate(objpdn001.Project);
                    if (objpdn001 != null)
                    {
                        objSCU002.DoumentNo = fc["DoumentNo" + 0];
                        objSCU002.Project = objpdn001.Project;
                        objSCU002.location = objpdn001.Location;
                        objSCU002.Phase = Convert.ToInt32(!string.IsNullOrEmpty(fc["Phase" + 0]) ? Convert.ToInt32(fc["Phase" + 0]) : 0);

                        if (!string.IsNullOrEmpty(objSCU002.DoumentNo))
                        {
                            if (db.SCU002.Any(x => x.DoumentNo.ToLower() == objSCU002.DoumentNo.ToLower() && x.location == objSCU002.location && x.Project == objSCU002.Project && x.Phase == objSCU002.Phase))
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = objSCU002.DoumentNo + " Already Exists";
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                        }

                        objSCU002.CDD = objpdn001.CDD;
                        objSCU002.ZeroDate = Convert.ToDateTime(zerodate);
                        //objSCU002.Phase = Convert.ToInt32(!string.IsNullOrEmpty(fc["Phase" + 0]) ? Convert.ToInt32(fc["Phase" + 0]) : 0);
                        objSCU002.DoumentDesc = fc["DoumentDesc" + 0];
                        objSCU002.Days = Convert.ToDouble(fc["Days" + 0]);
                        objSCU002.PlannedQty = Convert.ToDouble(fc["PlannedQty" + 0]);
                        objSCU002.ActualQty = Convert.ToDouble(fc["ActualQty" + 0]);
                        objSCU002.HrsPerDoc = Convert.ToDouble(fc["HrsPerDoc" + 0]);
                        objSCU002.CreatedOn = DateTime.Now;
                        objSCU002.CreatedBy = objClsLoginInfo.UserName;

                        objSCU003.Project = objSCU002.Project;
                        objSCU003.location = objpdn001.Location;
                        objSCU003.DoumentNo = objSCU002.DoumentNo;
                        objSCU003.ActualQtyInterval = Convert.ToDouble(Manager.GetActualQtyInterval(zerodate, objSCU002.Days));
                        objSCU003.ActualQty = objSCU002.ActualQty;
                        objSCU003.phase = objSCU002.Phase;
                        objSCU003.CreatedOn = DateTime.Now;
                        objSCU003.CreatedBy = objClsLoginInfo.UserName;

                        objSCU003_log.Project = objSCU002.Project;
                        objSCU003_log.location = objpdn001.Location;
                        objSCU003_log.DoumentNo = objSCU002.DoumentNo;
                        objSCU003_log.ActualQtyInterval = Convert.ToDouble(Manager.GetActualQtyInterval(zerodate, objSCU002.Days));
                        objSCU003_log.ActualQty = objSCU002.ActualQty;
                        objSCU003_log.phase = objSCU002.Phase;
                        objSCU003_log.CreatedOn = DateTime.Now;
                        objSCU003_log.CreatedBy = objClsLoginInfo.UserName;


                        db.SCU002.Add(objSCU002);
                        db.SCU003.Add(objSCU003);
                        db.SCU003_Log.Add(objSCU003_log);
                        db.SaveChanges();


                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Data save successfully.";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please Try Again Later";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Try Again Later";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(int HeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            var objSCU002 = db.SCU002.FirstOrDefault(x => x.Id == HeaderId);
            try
            {
                if (objSCU002 != null)
                {
                    db.SCU003.RemoveRange(db.SCU003.Where(x => x.Project == objSCU002.Project && x.location == objSCU002.location && x.DoumentNo == objSCU002.DoumentNo && x.phase == objSCU002.Phase).ToList());
                    db.SCU003_Log.RemoveRange(db.SCU003_Log.Where(x => x.Project == objSCU002.Project && x.location == objSCU002.location && x.DoumentNo == objSCU002.DoumentNo && x.phase == objSCU002.Phase).ToList());
                    db.SCU002.Remove(objSCU002);

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record deleted successfully";
                }
                else
                {
                    objResponseMsg.Value = "Please try Again Later";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public JsonResult Undo(int HeaderId, int SId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            objResponseMsg.Value = "Please try Again Later";
            var objSCU002 = db.SCU002.FirstOrDefault(x => x.Id == HeaderId);
            var objSCU003_log = db.SCU003_Log.FirstOrDefault(x => x.Id == SId);
            try
            {
                if (objSCU002 != null && objSCU003_log != null)
                {
                    var objSCU003 = db.SCU003.Where(x => x.Project == objSCU003_log.Project && x.location == objSCU003_log.location && x.DoumentNo == objSCU003_log.DoumentNo && x.ActualQtyInterval == objSCU003_log.ActualQtyInterval && x.phase == objSCU003_log.phase).FirstOrDefault();
                    if (objSCU003 != null)
                    {
                        objSCU003.ActualQty = objSCU002.ActualQty - objSCU003_log.ActualQty;
                        objSCU002.ActualQty = objSCU002.ActualQty - objSCU003_log.ActualQty;
                        db.SCU003_Log.Remove(objSCU003_log);
                        db.SaveChanges();

                        if (!db.SCU003_Log.Any(x => x.Project == objSCU003_log.Project && x.location == objSCU003_log.location && x.DoumentNo == objSCU003_log.DoumentNo && x.ActualQtyInterval == objSCU003_log.ActualQtyInterval && x.phase == objSCU003_log.phase))
                        {
                            db.SCU003.Remove(objSCU003);
                            db.SaveChanges();
                        }

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Record Undo successfully";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult EditDetails(int id, string columnName, string columnValue, int actqty = 0)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                columnValue = columnValue.Replace("'", "");

                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    if (columnName.ToLower() == "actualqty")
                    {
                        EntryReportdata(id, columnValue);
                        if (actqty > 0)
                        {
                            int actq = Convert.ToInt32(columnValue);
                            actqty = actq + actqty;
                            columnValue = Convert.ToString(actqty);
                        }
                    }

                    db.SP_COMMON_TABLE_UPDATE("SCU002", id, "Id", columnName, columnValue, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
                else
                {
                    db.SP_COMMON_TABLE_UPDATE("SCU002", id, "Id", columnName, columnValue, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChkValidPlannedQTY(int id, string columnValue)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                columnValue = columnValue.Replace("'", "");
                int plannedqty = 0;
                if (!string.IsNullOrEmpty(columnValue))
                {
                    plannedqty = Convert.ToInt32(columnValue);

                    if (plannedqty > 0)
                    {
                        var objscu002 = db.SCU002.Where(x => x.Id == id).FirstOrDefault();
                        if (objscu002 != null)
                        {
                            var actqty = db.SCU003.Where(x => x.Project == objscu002.Project && x.location == objscu002.location && x.DoumentNo == objscu002.DoumentNo).Sum(x => x.ActualQty);

                            if (plannedqty >= actqty)
                            {
                                objResponseMsg.Key = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Export Excel
        // Export Functionality
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                string location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

                var lst = db.SP_SCU_GET_MAINTAIN_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from uc in lst
                              select new
                              {
                                  Phase = Convert.ToString(uc.Phase),
                                  DoumentNo = Convert.ToString(uc.DoumentNo),
                                  DoumentDesc = Convert.ToString(uc.DoumentDesc),
                                  Days = Convert.ToString(uc.Days),
                                  PlannedQty = Convert.ToString(uc.PlannedQty),
                                  ActualQty = Convert.ToString(uc.ActualQty),
                                  HrsPerDoc = Convert.ToString(uc.HrsPerDoc),
                                  TotalHrs = Convert.ToString(uc.TotalHrs),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ProjectwiseGenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                string location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();

                var lst = db.SP_SCU_GET_MAINTAIN_PROJECT_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from uc in lst
                              select new
                              {
                                  Project = Convert.ToString(uc.Project),
                                  ZeroDate = Convert.ToString(uc.ZeroDate.HasValue ? uc.ZeroDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""),
                                  CDD = Convert.ToString(uc.CDD.HasValue ? uc.CDD.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""),
                                  Phase = Convert.ToString(uc.Phase),
                                  DoumentNo = Convert.ToString(uc.DoumentNo),
                                  DoumentDesc = Convert.ToString(uc.DoumentDesc),
                                  Days = Convert.ToString(uc.Days),
                                  PlannedQty = Convert.ToString(uc.PlannedQty),
                                  ActualQty = Convert.ToString(uc.ActualQty),
                                  HrsPerDoc = Convert.ToString(uc.HrsPerDoc),
                                  TotalHrs = Convert.ToString(uc.TotalHrs),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        public void DefaultMasterEntry(int HeaderId)
        {
            try
            {
                List<SCU001> lstSCU001 = db.SCU001.ToList();
                List<SCU002> lstSCU002 = new List<SCU002>();
                List<SCU003> lstSCU003 = new List<SCU003>();
                List<SCU003_Log> lstSCU003_log = new List<SCU003_Log>();

                var objpdn001 = db.PDN001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                if (objpdn001 != null)
                {
                    var objscu002 = db.SCU002.Where(x => x.Project == objpdn001.Project && x.location == objpdn001.Location).FirstOrDefault();

                    if (objscu002 == null)
                    {
                        string zerodate = Manager.Getzerodate(objpdn001.Project);

                        var pdn002 = db.PDN002
                            .Where(x => x.HeaderId == objpdn001.HeaderId && x.DocumentNo != null).ToList()
                            .Select(x => new
                            {
                                phase = getno(x.DocumentNo, "phase"),
                                IsApplicable = x.IsApplicable,
                                docno = getno(x.DocumentNo, "docno"),
                                trimdocno = getno(x.DocumentNo, "trimdocno")
                            })
                            .ToList();

                        foreach (var item in lstSCU001)
                        {
                            string scudocno = string.Empty;
                            string pdndocno = string.Empty;
                            if (!string.IsNullOrEmpty(item.DoumentNo))
                            {
                                scudocno = item.DoumentNo.Trim().Substring(item.DoumentNo.Length - 4);
                            }
                            if (pdn002 != null)
                            {
                                pdndocno = pdn002.Where(x => x.docno.ToLower() == scudocno.ToLower() && x.phase == Convert.ToString(item.Phase)).Select(x => x.trimdocno).FirstOrDefault();

                                //if (!string.IsNullOrEmpty(pdndocno))
                                //{
                                    bool isapplicable = false;
                                    var applicable = pdn002.Where(x => x.docno.ToLower() == scudocno.ToLower() && x.phase == Convert.ToString(item.Phase)).Select(x => x.IsApplicable).FirstOrDefault();

                                    if (applicable != null)
                                        isapplicable = Convert.ToBoolean(applicable);
                                    else
                                        isapplicable = true;

                                    //if (isapplicable)
                                    //{
                                        SCU002 objSCU002 = new SCU002();
                                        SCU003 objSCU003 = new SCU003();
                                        SCU003_Log objSCU003_log = new SCU003_Log();

                                        objSCU003.Project = objpdn001.Project;
                                        objSCU003.location = objpdn001.Location;
                                        objSCU003.DoumentNo = (!string.IsNullOrEmpty(pdndocno) ? pdndocno + scudocno : item.DoumentNo);
                                        objSCU003.ActualQtyInterval = Convert.ToDouble(Manager.GetActualQtyInterval(zerodate, item.Days));
                                        objSCU003.ActualQty = 0;
                                        objSCU003.phase = item.Phase;
                                        objSCU003.CreatedOn = DateTime.Now;
                                        objSCU003.CreatedBy = objClsLoginInfo.UserName;

                                        objSCU003_log.Project = objpdn001.Project;
                                        objSCU003_log.location = objpdn001.Location;
                                        objSCU003_log.DoumentNo = (!string.IsNullOrEmpty(pdndocno) ? pdndocno + scudocno : item.DoumentNo);
                                        objSCU003_log.ActualQtyInterval = Convert.ToDouble(Manager.GetActualQtyInterval(zerodate, item.Days));
                                        objSCU003_log.ActualQty = 0;
                                        objSCU003_log.phase = item.Phase;
                                        objSCU003_log.CreatedOn = DateTime.Now;
                                        objSCU003_log.CreatedBy = objClsLoginInfo.UserName;

                                        objSCU002.Project = objpdn001.Project;
                                        objSCU002.location = objpdn001.Location;
                                        objSCU002.CDD = objpdn001.CDD;
                                        objSCU002.ZeroDate = Convert.ToDateTime(zerodate);
                                        objSCU002.DoumentNo = (!string.IsNullOrEmpty(pdndocno) ? pdndocno + scudocno : item.DoumentNo);
                                        objSCU002.Phase = item.Phase;
                                        objSCU002.DoumentDesc = item.DoumentDesc;
                                        objSCU002.Days = item.Days;
                                        objSCU002.PlannedQty = item.PlannedQty;
                                        objSCU002.ActualQty = 0;
                                        objSCU002.HrsPerDoc = item.HrsPerDoc;
                                        objSCU002.CreatedOn = DateTime.Now;
                                        objSCU002.CreatedBy = objClsLoginInfo.UserName;

                                        lstSCU002.Add(objSCU002);
                                        lstSCU003.Add(objSCU003);
                                        lstSCU003_log.Add(objSCU003_log);
                                    //}
                                //}
                            }
                        }
                    }

                    if (lstSCU002.Count() > 0)
                    {
                        db.SCU002.AddRange(lstSCU002);
                    }

                    if (lstSCU003.Count() > 0)
                    {
                        db.SCU003.AddRange(lstSCU003);
                    }

                    if (lstSCU003_log.Count() > 0)
                    {
                        db.SCU003_Log.AddRange(lstSCU003_log);
                    }
                    db.SaveChanges();
                    Manager.ScurveHTC(objpdn001.Project, objpdn001.Location);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
        }

        public void EntryReportdata(int id, string value)
        {
            var objscu002 = db.SCU002.Where(x => x.Id == id).FirstOrDefault();
            if (objscu002 != null)
            {
                string zerodate = string.Format("{0:MMM dd yyyy hh:mmtt}", objscu002.ZeroDate);
                double intervalactualqty = Convert.ToDouble(Manager.GetActualQtyInterval(zerodate, objscu002.Days));
                double actualqty = Convert.ToDouble(value);

                SCU003 objSCU003 = null;

                var lstscu003 = db.SCU003.Where(x => x.Project == objscu002.Project && x.location == objscu002.location && x.DoumentNo == objscu002.DoumentNo && x.phase == objscu002.Phase).ToList();

                objSCU003 = lstscu003.Where(x => x.ActualQtyInterval == intervalactualqty).FirstOrDefault();

                if (objSCU003 != null)
                {
                    objSCU003.ActualQty = (objSCU003.ActualQty + actualqty);
                    objSCU003.EditedOn = DateTime.Now;
                    objSCU003.EditedBy = objClsLoginInfo.UserName;
                }
                else
                {
                    objSCU003 = new SCU003();

                    objSCU003.Project = objscu002.Project;
                    objSCU003.location = objscu002.location;
                    objSCU003.DoumentNo = objscu002.DoumentNo;
                    objSCU003.ActualQtyInterval = intervalactualqty;
                    objSCU003.ActualQty = (lstscu003.Count > 0 ? lstscu003.Sum(x => x.ActualQty) : 0) + actualqty;
                    objSCU003.phase = objscu002.Phase;
                    objSCU003.CreatedOn = DateTime.Now;
                    objSCU003.CreatedBy = objClsLoginInfo.UserName;
                    db.SCU003.Add(objSCU003);
                }

                SCU003_Log objSCU003_log = new SCU003_Log();

                objSCU003_log.Project = objscu002.Project;
                objSCU003_log.location = objscu002.location;
                objSCU003_log.DoumentNo = objscu002.DoumentNo;
                objSCU003_log.ActualQtyInterval = intervalactualqty;
                objSCU003_log.ActualQty = actualqty;
                objSCU003_log.phase = objscu002.Phase;
                objSCU003_log.CreatedOn = DateTime.Now;
                objSCU003_log.CreatedBy = objClsLoginInfo.UserName;
                db.SCU003_Log.Add(objSCU003_log);

                db.SaveChanges();
            }
        }

        [NonAction]
        public static string Generatetooltipdesc(string columnValue = "", string columnName = "")
        {
            string htmlControl = string.Empty;
            string inputValue = string.Empty;

            if (!string.IsNullOrEmpty(columnValue))
            {
                if (columnName == "DoumentNo")
                    inputValue = Ellipsis(columnValue, columnName, 10);
                else if (columnName == "DoumentDesc")
                    inputValue = Ellipsis(columnValue, columnName, 20);

                htmlControl = "<span title='" + columnValue + "'>" + inputValue + "</span>";
            }
            return htmlControl;
        }

        [NonAction]
        public static string Ellipsis(string text, string name, int length)
        {
            string value = string.Empty;
            value = text;
            if (value.Length > length)
                return value.Substring(0, length) + "...";

            return value;
        }

        public string getno(string DocumentNo, string type)
        {
            if (type == "phase")
            {
                DocumentNo = DocumentNo.Trim().Substring(DocumentNo.Length - 2);
                int docno = Convert.ToInt32(DocumentNo);
                docno = (docno > 90) ? 3 : ((docno > 7) ? 2 : ((docno > 1) ? 1 : 1));
                DocumentNo = Convert.ToString(docno);
            }
            else if (type == "docno")
            {
                DocumentNo = DocumentNo.Trim().Substring(DocumentNo.Length - 4);
            }
            else if (type == "trimdocno")
            {
                DocumentNo = DocumentNo.Trim().Substring(0, DocumentNo.Length - 4);
            }

            return DocumentNo;
        }
    }
}