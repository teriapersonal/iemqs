﻿using IEMQS.Areas.MSW.Controllers;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsHelper;
namespace IEMQS.Areas.OPC.Controllers
{
    public class ApproveController : clsBase
    {
        // GET: OPC/Approve
        [SessionExpireFilter]

        #region Main Grid
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetOPCGridPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetOPCGridPartial");
        }   //datatable function for header
        [HttpPost]
        public JsonResult LoadOPCHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue() + "') and ApprovedBy=" + objClsLoginInfo.UserName.Trim()+"";
                }
                else
                {
                    strWhere += "1=1";
                }
             //   strWhere += " and ApprovedBy=" + objClsLoginInfo.UserName.Trim();

                #region sorting
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                #endregion

                #region searching
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "com1.t_dsca", "Project", "Document", "Customer", "RevNo", "Product", "ProcessLicensor", "Status" };
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                #endregion

                var lstResult = db.SP_OPC_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.Project),
                                Convert.ToString(uc.Document),
                                Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                Convert.ToString(uc.Product),
                                Convert.ToString(uc.ProcessLicensor),
                                Convert.ToString("R"+uc.RevNo),
                                Convert.ToString(uc.Status),
                                Convert.ToString(uc.SubmittedBy),
                                uc.SubmittedOn == null || uc.SubmittedOn.Value==DateTime.MinValue? "NA" : uc.SubmittedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                                Convert.ToString(uc.ApprovedBy),
                                uc.ApprovedOn == null || uc.ApprovedOn.Value==DateTime.MinValue? "NA" : uc.ApprovedOn.Value.ToString("dd/MM/yyyy" , CultureInfo.InvariantCulture),
                                "<a title='View' href='"+WebsiteURL+"/OPC/Approve/GetOPCDetails?Id="+Convert.ToInt32(uc.HeaderId)+"'><i class='iconspace fa fa-eye'></i></a>"+" "+(uc.RevNo>0 ?"<i title=\"History\" onclick=\"ViewHistoryForProjectPLN('"+uc.HeaderId+"','Approver','/OPC/History/GetHistoryDetails','Operation Cards')\"  class='iconspace fa fa-history'></i>":"<i title=\"History\" class='disabledicon fa fa-history'></i>")+"<i title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/OPC/Maintain/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')  class='iconspace fa fa-clock-o'></i>",
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region Maintain Header
        [SessionExpireFilter]
        //main page
        public ActionResult GetOPCDetails(int Id = 0)
        {
            OPC001 objOPC001 = new OPC001();

            if (Id > 0)
            {
                objOPC001 = db.OPC001.Where(x => x.HeaderId == Id).FirstOrDefault();
                ViewBag.Project = db.COM001.Where(i => i.t_cprj == objOPC001.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault(); ;
                if (objOPC001.ApprovedBy != null)
                {
                   
                    ViewBag.ApproverName = db.COM003.Where(x => x.t_psno == objOPC001.ApprovedBy && x.t_actv == 1).Select(x => objOPC001.ApprovedBy + "-" + x.t_name).FirstOrDefault();
                }
                else
                {
                    ViewBag.ApproverName = "";
                   // return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(objOPC001.Project);
                var PlanningDinID = db.PDN002.Where(x => x.RefId == Id && x.DocumentNo == objOPC001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;
                ViewBag.Revoke = Manager.IsRevokeApplicable(PlanningDinID, Id, clsImplementationEnum.PlanList.Operation_Cards.GetStringValue());
                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objOPC001.HeaderId, objOPC001.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
                if (objOPC001.ApprovedBy != null)
                {
                    if (objOPC001.ApprovedBy.Trim() != objClsLoginInfo.UserName.Trim())
                    {
                        ViewBag.IsDisplayMode = true;
                    }
                    else
                    {
                        ViewBag.IsDisplayMode = false;
                    }
                }
                else
                {
                    ViewBag.IsDisplayMode = true;
                }
            }

            return View(objOPC001);
        }
        //Update header
        [HttpPost]
        public ActionResult SaveHeader(OPC001 ocp001)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                if (ocp001.HeaderId > 0)
                {
                    OPC001 objOPC001 = db.OPC001.Where(x => x.HeaderId == ocp001.HeaderId).FirstOrDefault();
                    if (objOPC001 != null)
                    {
                        objOPC001.EditedBy = objClsLoginInfo.UserName;
                        objOPC001.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.status = objOPC001.Status;
                        objResponseMsg.Value = clsImplementationMessage.PlanMessages.Update.ToString();
                        objResponseMsg.Revision = "R" + objOPC001.RevNo.ToString();
                        Manager.UpdatePDN002(objOPC001.HeaderId, objOPC001.Status, objOPC001.RevNo, objOPC001.Project, objOPC001.Document);

                        //var folderPath = "OPC001/" + objOPC001.HeaderId + "/R" + objOPC001.RevNo;
                        //Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Process Details not available.";
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //approve header
        public ActionResult ApproveSelectedOPC(string strHeaderIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    db.SP_OPC_APPROVE(HeaderId, objClsLoginInfo.UserName);
                    OPC001 objOPC001 = db.OPC001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    int newId = db.OPC001_Log.Where(q => q.HeaderId == HeaderId).OrderByDescending(q => q.ApprovedOn).FirstOrDefault().Id;
                    Manager.UpdatePDN002(objOPC001.HeaderId, objOPC001.Status, objOPC001.RevNo, objOPC001.Project, objOPC001.Document, newId);

                    int? currentRevisionNull = db.OPC001.Where(q => q.HeaderId == HeaderId).FirstOrDefault().RevNo;
                    int currentRevision = ((currentRevisionNull == null) ? 0 : currentRevisionNull.Value);
                    //clsUpload.CopyFolderContents("OPC001/" + HeaderId + "/R" + currentRevision, "OPC001/" + HeaderId + "/R" + (currentRevision + 1));
                    var oldfolderPath = "OPC001//" + HeaderId + "//R" + currentRevision;
                    var newfolderPath = "OPC001//" + HeaderId + "//R" + (currentRevision + 1);
                    //(new clsFileUpload()).CopyFolderContentsAsync(oldfolderPath, newfolderPath);

                    IEMQS.Areas.Utility.Controllers.FileUploadController _objFUC = new Utility.Controllers.FileUploadController();
                    _objFUC.CopyDataOnFCSServerAsync(oldfolderPath, newfolderPath, oldfolderPath, HeaderId, newfolderPath, HeaderId, IEMQS.DESServices.CommonService.GetUseIPConfig, IEMQS.DESServices.CommonService.objClsLoginInfo.UserName, 0);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objOPC001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objOPC001.Project, clsImplementationEnum.PlanList.Operation_Cards.GetStringValue(), objOPC001.RevNo.Value.ToString(), objOPC001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Operation_Cards.GetStringValue(), objOPC001.HeaderId.ToString(), false),
                                                        objOPC001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Approve.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //return header
        public ActionResult ReturnSelectedOPC(string strHeaderIds, string remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string[] arrayHeaderIds = strHeaderIds.Split(',').ToArray();
                for (int i = 0; i < arrayHeaderIds.Length; i++)
                {
                    int HeaderId = Convert.ToInt32(arrayHeaderIds[i]);
                    OPC001 objOPC001 = db.OPC001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                    objOPC001.Status = clsImplementationEnum.CTQStatus.Returned.GetStringValue();
                    objOPC001.ApprovedBy = objClsLoginInfo.UserName;
                    objOPC001.ApprovedOn = DateTime.Now;
                    objOPC001.ReturnRemark = remarks;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objOPC001.HeaderId, objOPC001.Status, objOPC001.RevNo, objOPC001.Project, objOPC001.Document);

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG3.GetStringValue(),
                                                        objOPC001.Project,
                                                        "",
                                                        "",
                                                        Manager.GetPDINDocumentNotificationMsg(objOPC001.Project, clsImplementationEnum.PlanList.Operation_Cards.GetStringValue(), objOPC001.RevNo.Value.ToString(), objOPC001.Status),
                                                        clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(),
                                                        Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Operation_Cards.GetStringValue(), objOPC001.HeaderId.ToString(), false),
                                                        objOPC001.CreatedBy);
                    #endregion
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.QCPMessages.Return.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        //revoke header 
        [HttpPost]
        public ActionResult RevokeDocument(int HeaderId, int pdinId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (Manager.IsRevokeApplicable(pdinId, HeaderId, clsImplementationEnum.PlanList.Operation_Cards.GetStringValue()))
                {
                    objResponseMsg.Key = Manager.RevokePDINDocument(HeaderId, clsImplementationEnum.PlanList.Operation_Cards, Remarks);
                    if (objResponseMsg.Key)
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revoke;
                    }
                    else
                    {
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.UnSuccess;
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.pdnRevokeMessage;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Excel
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_OPC_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      Project = li.Project,
                                      Document = li.Document,
                                      Customer = li.Customer,
                                      Product = li.Product,
                                      ProcessLicensor = li.ProcessLicensor,
                                      Status = li.Status,
                                      RevNo = "R" + li.RevNo,
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}