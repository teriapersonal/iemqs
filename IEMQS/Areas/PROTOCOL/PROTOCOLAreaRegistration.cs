﻿using System.Web.Mvc;

namespace IEMQS.Areas.PROTOCOL
{
    public class PROTOCOLAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "PROTOCOL";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "PROTOCOL_default",
                "PROTOCOL/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}