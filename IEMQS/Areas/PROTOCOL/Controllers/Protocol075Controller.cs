﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol075Controller : clsBase
    {
        // GET: PROTOCOL/Protocol075
        string ControllerURL = "/PROTOCOL/Protocol075/";
        string Title = "SET-UP  & DIMENSION REPORT FOR FORMED CONE";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO370 objPRO370 = new PRO370();
            if (!id.HasValue)
            {
                try
                {
                    objPRO370.ProtocolNo = string.Empty;
                    objPRO370.CreatedBy = objClsLoginInfo.UserName;
                    objPRO370.CreatedOn = DateTime.Now;


                    #region TYPE
                    objPRO370.RemarksInsideCirc2 = "NOTE : IN CASE OF CONE CIRCUMFERENCE TO BE CHECKED IN CLOCK WISE AND ANTI CLOCK WISE THAN AVERAGE SHALL BE REPORTED \r \n ABBREVIATION \r \n C.W: CLOCK WISE, \r \n A.C.W: ANTI CLOCK WISE, \r \n AVG: AVERAGE";
                    #endregion

                    #region OVALITY
                    objPRO370.PRO371.Add(new PRO371
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO371.Add(new PRO371
                    {
                        Orientation = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO371.Add(new PRO371
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO371.Add(new PRO371
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO371.Add(new PRO371
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO371.Add(new PRO371
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO371.Add(new PRO371
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO371.Add(new PRO371
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region OVERALL HEIGHT OF CONE
                    objPRO370.PRO372.Add(new PRO372
                    {
                        Orientation = "0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO372.Add(new PRO372
                    {
                        Orientation = "90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO372.Add(new PRO372
                    {
                        Orientation = "180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO372.Add(new PRO372
                    {
                        Orientation = "270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    #region  THICKNESS  READINGS AT TRIMMING LINE
                    objPRO370.PRO373.Add(new PRO373
                    {
                        SrNo = 1,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO373.Add(new PRO373
                    {
                        SrNo = 2,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO373.Add(new PRO373
                    {
                        SrNo = 3,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO373.Add(new PRO373
                    {
                        SrNo = 4,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO373.Add(new PRO373
                    {
                        SrNo = 5,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO373.Add(new PRO373
                    {
                        SrNo = 6,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO373.Add(new PRO373
                    {
                        SrNo = 7,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO373.Add(new PRO373
                    {
                        SrNo = 8,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO373.Add(new PRO373
                    {
                        SrNo = 9,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO373.Add(new PRO373
                    {
                        SrNo = 10,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO373.Add(new PRO373
                    {
                        SrNo = 10,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO373.Add(new PRO373
                    {
                        SrNo = 11,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO373.Add(new PRO373
                    {
                        SrNo = 12,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion


                    #region DETAIL THICKNESS REPORT
                    objPRO370.PRO374.Add(new PRO374
                    {
                        Description = "PETAL 1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374.Add(new PRO374
                    {
                        Description = "PETAL 2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374.Add(new PRO374
                    {
                        Description = "PETAL 3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374.Add(new PRO374
                    {
                        Description = "PETAL 4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region  " e " VALUE TEMPLATE READING AS APPLICABLE
                    objPRO370.PRO374_2.Add(new PRO374_2
                    {
                        ReqAtTop = "PETAL-1",
                        ReqAtMiddle = "PETAL-1",
                        ReqAtBottom = "PETAL-1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_2.Add(new PRO374_2
                    {
                        ReqAtTop = "PETAL-2",
                        ReqAtMiddle = "PETAL-2",
                        ReqAtBottom = "PETAL-2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_2.Add(new PRO374_2
                    {
                        ReqAtTop = "PETAL-3",
                        ReqAtMiddle = "PETAL-3",
                        ReqAtBottom = "PETAL-3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_2.Add(new PRO374_2
                    {
                        ReqAtTop = "PETAL-4",
                        ReqAtMiddle = "PETAL-4",
                        ReqAtBottom = "PETAL-4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_2.Add(new PRO374_2
                    {
                        ReqAtTop = "PETAL-5",
                        ReqAtMiddle = "PETAL-5",
                        ReqAtBottom = "PETAL-5",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_2.Add(new PRO374_2
                    {
                        ReqAtTop = "PETAL-6",
                        ReqAtMiddle = "PETAL-6",
                        ReqAtBottom = "PETAL-6",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_2.Add(new PRO374_2
                    {
                        ReqAtTop = "PETAL-7",
                        ReqAtMiddle = "PETAL-7",
                        ReqAtBottom = "PETAL-7",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_2.Add(new PRO374_2
                    {
                        ReqAtTop = "PETAL-8",
                        ReqAtMiddle = "PETAL-8",
                        ReqAtBottom = "PETAL-8",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });


                    #endregion

                    #region  GAP BY USING 2D PROFILE TEMPLATE
                    objPRO370.PRO374_7.Add(new PRO374_7
                    {
                        PetalNo = "1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_7.Add(new PRO374_7
                    {
                        PetalNo = "2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_7.Add(new PRO374_7
                    {
                        PetalNo = "3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_7.Add(new PRO374_7
                    {
                        PetalNo = "4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_7.Add(new PRO374_7
                    {
                        PetalNo = "5",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_7.Add(new PRO374_7
                    {
                        PetalNo = "6",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_7.Add(new PRO374_7
                    {
                        PetalNo = "7",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_7.Add(new PRO374_7
                    {
                        PetalNo = "8",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    #region ACTUAL GAP BY USING KNUCKLE RADIUS TEMPLATE 

                    objPRO370.PRO374_8.Add(new PRO374_8
                    {
                        PetalNo = "1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_8.Add(new PRO374_8
                    {
                        PetalNo = "2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_8.Add(new PRO374_8
                    {
                        PetalNo = "3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_8.Add(new PRO374_8
                    {
                        PetalNo = "4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_8.Add(new PRO374_8
                    {
                        PetalNo = "5",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_8.Add(new PRO374_8
                    {
                        PetalNo = "6",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_8.Add(new PRO374_8
                    {
                        PetalNo = "7",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO370.PRO374_8.Add(new PRO374_8
                    {
                        PetalNo = "8",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    db.PRO370.Add(objPRO370);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO370.HeaderId;
            }
            else
            {
                objPRO370 = db.PRO370.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };
            List<string> lsttype = new List<string> { "TORI CONE", "STRAIGHT CONE" };

            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.Type = lsttype.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO371> lstPRO371 = db.PRO371.Where(x => x.HeaderId == objPRO370.HeaderId).ToList();
            List<PRO372> lstPRO372 = db.PRO372.Where(x => x.HeaderId == objPRO370.HeaderId).ToList();
            List<PRO373> lstPRO373 = db.PRO373.Where(x => x.HeaderId == objPRO370.HeaderId).ToList();
            List<PRO374> lstPRO374 = db.PRO374.Where(x => x.HeaderId == objPRO370.HeaderId).ToList();
            List<PRO374_2> lstPRO374_2 = db.PRO374_2.Where(x => x.HeaderId == objPRO370.HeaderId).ToList();
            List<PRO374_3> lstPRO374_3 = db.PRO374_3.Where(x => x.HeaderId == objPRO370.HeaderId).ToList();
            List<PRO374_4> lstPRO374_4 = db.PRO374_4.Where(x => x.HeaderId == objPRO370.HeaderId).ToList();
            List<PRO374_5> lstPRO374_5 = db.PRO374_5.Where(x => x.HeaderId == objPRO370.HeaderId).ToList();
            List<PRO374_6> lstPRO374_6 = db.PRO374_6.Where(x => x.HeaderId == objPRO370.HeaderId).ToList();
            List<PRO374_7> lstPRO374_7 = db.PRO374_7.Where(x => x.HeaderId == objPRO370.HeaderId).ToList();
            List<PRO374_8> lstPRO374_8 = db.PRO374_8.Where(x => x.HeaderId == objPRO370.HeaderId).ToList();
            List<PRO374_9> lstPRO374_9 = db.PRO374_9.Where(x => x.HeaderId == objPRO370.HeaderId).ToList();


            List<SP_IPI_PROTOCOL075_GET_LINES13_DATA_Result> lstPRO374_10 = db.SP_IPI_PROTOCOL075_GET_LINES13_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO370.HeaderId).ToList();
            List<SP_IPI_PROTOCOL075_GET_LINES14_DATA_Result> lstPRO374_11 = db.SP_IPI_PROTOCOL075_GET_LINES14_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO370.HeaderId).ToList();


            ViewBag.lstPRO371 = lstPRO371;
            ViewBag.lstPRO372 = lstPRO372;
            ViewBag.lstPRO373 = lstPRO373;
            ViewBag.lstPRO374 = lstPRO374;
            ViewBag.lstPRO374_2 = lstPRO374_2;
            ViewBag.lstPRO374_3 = lstPRO374_3;
            ViewBag.lstPRO374_4 = lstPRO374_4;
            ViewBag.lstPRO374_5 = lstPRO374_5;
            ViewBag.lstPRO374_6 = lstPRO374_6;
            ViewBag.lstPRO374_7 = lstPRO374_7;
            ViewBag.lstPRO374_8 = lstPRO374_8;
            ViewBag.lstPRO374_9 = lstPRO374_9;
            ViewBag.lstPRO374_10 = lstPRO374_10;
            ViewBag.lstPRO374_11 = lstPRO374_11;

            #endregion

            return View(objPRO370);
        }
        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO370 PRO370)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO370.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO370 objPRO370 = db.PRO370.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO370.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO370.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO370.ProtocolNo = PRO370.ProtocolNo;
                        objPRO370.Type = PRO370.Type;
                        objPRO370.DiscOutsideCirc = PRO370.DiscOutsideCirc;
                        objPRO370.ReqTopOutsideCirc = PRO370.ReqTopOutsideCirc;
                        objPRO370.ActTopOutsideCirc = PRO370.ActTopOutsideCirc;
                        objPRO370.RemarksOutsideCirc = PRO370.RemarksOutsideCirc;
                        objPRO370.ReqBottomOutsideCirc = PRO370.ReqBottomOutsideCirc;
                        objPRO370.ActBottomOutsideCirc = PRO370.ActBottomOutsideCirc;
                        objPRO370.DiscInsideCirc1 = PRO370.DiscInsideCirc1;
                        objPRO370.ReqTopInsideCirc = PRO370.ReqTopInsideCirc;
                        objPRO370.ActTopInsideCirc = PRO370.ActTopInsideCirc;
                        objPRO370.RemarksInsideCirc = PRO370.RemarksInsideCirc;
                        objPRO370.ReqBottomInsideCirc = PRO370.ReqBottomInsideCirc;
                        objPRO370.ActBottomInsideCirc = PRO370.ActBottomInsideCirc;
                        objPRO370.DiscInsideCirc2 = PRO370.DiscInsideCirc2;
                        objPRO370.ReqTopInsideCirc2 = PRO370.ReqTopInsideCirc2;
                        objPRO370.ActCwTopInsideCirc2 = PRO370.ActCwTopInsideCirc2;
                        objPRO370.ActAcwTopInsideCirc2 = PRO370.ActAcwTopInsideCirc2;
                        objPRO370.ActAvgTopInsideCirc2 = PRO370.ActAvgTopInsideCirc2;
                        objPRO370.RemarksInsideCirc2 = PRO370.RemarksInsideCirc2;
                        objPRO370.ReqBottomInsideCirc2 = PRO370.ReqBottomInsideCirc2;
                        objPRO370.ActCwBottomInsideCirc2 = PRO370.ActCwBottomInsideCirc2;
                        objPRO370.ActAcwBottomInsideCirc2 = PRO370.ActAcwBottomInsideCirc2;
                        objPRO370.ActAvgBottomInsideCirc2 = PRO370.ActAvgBottomInsideCirc2;
                        objPRO370.TypeOfFormingRequired = PRO370.TypeOfFormingRequired;
                        objPRO370.TypeOfFormingActual = PRO370.TypeOfFormingActual;
                        objPRO370.TypeOfFormingRemarks = PRO370.TypeOfFormingRemarks;
                        objPRO370.TypeOfHeatRequired = PRO370.TypeOfHeatRequired;
                        objPRO370.TypeOfHeatActual = PRO370.TypeOfHeatActual;
                        objPRO370.TypeOfHeatRemarks = PRO370.TypeOfHeatRemarks;
                        objPRO370.Ovality = PRO370.Ovality;
                        objPRO370.OutByActTop = PRO370.OutByActTop;
                        objPRO370.OutByActBottom = PRO370.OutByActBottom;
                        objPRO370.ReqHieghtOfCone = PRO370.ReqHieghtOfCone;
                        objPRO370.ActCfTop = PRO370.ActCfTop;
                        objPRO370.ActCfBot = PRO370.ActCfBot;
                        objPRO370.FormulaTop1 = PRO370.FormulaTop1;
                        objPRO370.FormulaTop2 = PRO370.FormulaTop2;
                        objPRO370.FormulaTop3 = PRO370.FormulaTop3;
                        objPRO370.FormulaTop4 = PRO370.FormulaTop4;
                        objPRO370.FormulaBot1 = PRO370.FormulaBot1;
                        objPRO370.FormulaBot2 = PRO370.FormulaBot2;
                        objPRO370.FormulaBot3 = PRO370.FormulaBot3;
                        objPRO370.FormulaBot4 = PRO370.FormulaBot4;
                        objPRO370.ActTop1 = PRO370.ActTop1;
                        objPRO370.ActTop2 = PRO370.ActTop2;
                        objPRO370.ActTop3 = PRO370.ActTop3;
                        objPRO370.ActTop4 = PRO370.ActTop4;
                        objPRO370.ActBot1 = PRO370.ActBot1;
                        objPRO370.ActBot2 = PRO370.ActBot2;
                        objPRO370.ActBot3 = PRO370.ActBot3;
                        objPRO370.ActBot4 = PRO370.ActBot4;
                        objPRO370.ReqThicknessreadings = PRO370.ReqThicknessreadings;
                        objPRO370.AverageAtTop = PRO370.AverageAtTop;
                        objPRO370.AverageAtBottom = PRO370.AverageAtBottom;
                        objPRO370.Act0 = PRO370.Act0;
                        objPRO370.Act90 = PRO370.Act90;
                        objPRO370.Act180 = PRO370.Act180;
                        objPRO370.Act270 = PRO370.Act270;
                        objPRO370.ReqConcentricity = PRO370.ReqConcentricity;
                        objPRO370.ActConcentricity = PRO370.ActConcentricity;
                        objPRO370.DetailThiknessReport = PRO370.DetailThiknessReport;
                        objPRO370.evalueTemplate = PRO370.evalueTemplate;
                        objPRO370.AtMiddle = PRO370.AtMiddle;
                        objPRO370.ReqChordLengthTop = PRO370.ReqChordLengthTop;
                        objPRO370.ReqRadiusTop = PRO370.ReqRadiusTop;
                        objPRO370.ReqChordLengthMiddle = PRO370.ReqChordLengthMiddle;
                        objPRO370.ReqRadiusMiddle = PRO370.ReqRadiusMiddle;
                        objPRO370.ReqChordLengthBottom = PRO370.ReqChordLengthBottom;
                        objPRO370.ReqRadiusBottom = PRO370.ReqRadiusBottom;
                        objPRO370.ActChordLengthTop = PRO370.ActChordLengthTop;
                        objPRO370.ActRadiusTop = PRO370.ActRadiusTop;
                        objPRO370.ActChordLengthMiddle = PRO370.ActChordLengthMiddle;
                        objPRO370.ActRadiusMiddle = PRO370.ActRadiusMiddle;
                        objPRO370.ActChordLengthAtBottom = PRO370.ActChordLengthAtBottom;
                        objPRO370.ActRadiusAtBottom = PRO370.ActRadiusAtBottom;
                        objPRO370.TopGapAllowed = PRO370.TopGapAllowed;
                        objPRO370.MiddleGapAllowed = PRO370.MiddleGapAllowed;
                        objPRO370.BottomGapAllowed = PRO370.BottomGapAllowed;
                        objPRO370.RootGapReq = PRO370.RootGapReq;
                        objPRO370.RootFaceReq = PRO370.RootFaceReq;
                        objPRO370.Pick_in_out = PRO370.Pick_in_out;

                        objPRO370.maxAllowedOffset = PRO370.maxAllowedOffset;
                        objPRO370.ReqInsideWebAngle = PRO370.ReqInsideWebAngle;
                        objPRO370.ReqoutsideWebAngle = PRO370.ReqoutsideWebAngle;

                        objPRO370.WepDimensionsForCircmferantionSeamOfTierToTireJoint = PRO370.WepDimensionsForCircmferantionSeamOfTierToTireJoint;
                        objPRO370.HeightOfTier = PRO370.HeightOfTier;
                        objPRO370.ReqKnuckleRadiusAtTop = PRO370.ReqKnuckleRadiusAtTop;
                        objPRO370.ActKnuckleRadiusAtTop = PRO370.ActKnuckleRadiusAtTop;
                        objPRO370.ReqKnuckleRadiusAtBottom = PRO370.ReqKnuckleRadiusAtBottom;
                        objPRO370.ActKnuckleRadiusAtBottom = PRO370.ActKnuckleRadiusAtBottom;
                        objPRO370.ReqStraightFace = PRO370.ReqStraightFace;
                        objPRO370.CheckPoint1 = PRO370.CheckPoint1;
                        objPRO370.CheckPoint2 = PRO370.CheckPoint2;
                        objPRO370.CheckPoint3 = PRO370.CheckPoint3;
                        objPRO370.CheckPoint4 = PRO370.CheckPoint4;
                        objPRO370.CheckPoint5 = PRO370.CheckPoint5;
                        objPRO370.CheckPoint6 = PRO370.CheckPoint6;
                        objPRO370.CheckPoint7 = PRO370.CheckPoint7;
                        objPRO370.CheckPoint7_2 = PRO370.CheckPoint7_2;
                        objPRO370.CheckPoint7_3 = PRO370.CheckPoint7_3;
                        objPRO370.CheckPoint8 = PRO370.CheckPoint8;
                        objPRO370.CheckPoint8_2 = PRO370.CheckPoint8_2;
                        objPRO370.CheckPoint8_3 = PRO370.CheckPoint8_3;
                        objPRO370.CheckPoint8_4 = PRO370.CheckPoint8_4;
                        objPRO370.CheckPoint10 = PRO370.CheckPoint10;
                        objPRO370.CheckPoint10_2 = PRO370.CheckPoint10_2;
                        objPRO370.CheckPoint11 = PRO370.CheckPoint11;
                        objPRO370.QCRemarks = PRO370.QCRemarks;
                        objPRO370.Result = PRO370.Result;
                        objPRO370.ReqWidth = PRO370.ReqWidth;
                        objPRO370.ReqDepth = PRO370.ReqDepth;
                        objPRO370.ReqOffset = PRO370.ReqOffset;
                        objPRO370.Note1 = PRO370.Note1;
                        objPRO370.Note2 = PRO370.Note2;
                        objPRO370.Note3 = PRO370.Note3;
                        objPRO370.IdentificationDetailsOfCleats = PRO370.IdentificationDetailsOfCleats;

                        objPRO370.EditedBy = objClsLoginInfo.UserName;
                        objPRO370.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO370.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO370 objPRO370 = db.PRO370.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO370 != null && string.IsNullOrWhiteSpace(objPRO370.ProtocolNo))
                    {
                        db.PRO370.Remove(objPRO370);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO370.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO371> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO371> lstAddPRO371 = new List<PRO371>();
                List<PRO371> lstDeletePRO371 = new List<PRO371>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO371 obj = db.PRO371.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTop = item.ActAtTop;
                                obj.ActAtBottom = item.ActAtBottom;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO371 obj = new PRO371();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTop = item.ActAtTop;
                                obj.ActAtBottom = item.ActAtBottom;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO371.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO371.Count > 0)
                    {
                        db.PRO371.AddRange(lstAddPRO371);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO371 = db.PRO371.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO371.Count > 0)
                    {
                        db.PRO371.RemoveRange(lstDeletePRO371);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO371 = db.PRO371.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO371.Count > 0)
                    {
                        db.PRO371.RemoveRange(lstDeletePRO371);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO372> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO372> lstAddPRO372 = new List<PRO372>();
                List<PRO372> lstDeletePRO372 = new List<PRO372>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO372 obj = db.PRO372.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO372();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.Actual = item.Actual;

                        if (isAdded)
                        {
                            lstAddPRO372.Add(obj);
                        }
                    }
                    if (lstAddPRO372.Count > 0)
                    {
                        db.PRO372.AddRange(lstAddPRO372);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO372 = db.PRO372.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO372.Count > 0)
                    {
                        db.PRO372.RemoveRange(lstDeletePRO372);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO372 = db.PRO372.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO372.Count > 0)
                    {
                        db.PRO372.RemoveRange(lstDeletePRO372);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO373> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO373> lstAddPRO373 = new List<PRO373>();
                List<PRO373> lstDeletePRO373 = new List<PRO373>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO373 obj = db.PRO373.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO373();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.SrNo = item.SrNo;
                        obj.AtTopThickness = item.AtTopThickness;
                        obj.AtBottomThickness = item.AtBottomThickness;
                        if (isAdded)
                        {
                            lstAddPRO373.Add(obj);
                        }
                    }
                    if (lstAddPRO373.Count > 0)
                    {
                        db.PRO373.AddRange(lstAddPRO373);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO373 = db.PRO373.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO373.Count > 0)
                    {
                        db.PRO373.RemoveRange(lstDeletePRO373);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO373 = db.PRO373.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO373.Count > 0)
                    {
                        db.PRO373.RemoveRange(lstDeletePRO373);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO374> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO374> lstAddPRO374 = new List<PRO374>();
                List<PRO374> lstDeletePRO374 = new List<PRO374>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO374 obj = db.PRO374.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO374();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Description = item.Description;
                        obj.Petal_1 = item.Petal_1;
                        obj.Petal_2 = item.Petal_2;
                        obj.Petal_3 = item.Petal_3;
                        obj.Petal_4 = item.Petal_4;
                        obj.Petal_5 = item.Petal_5;
                        obj.Petal_6 = item.Petal_6;
                        obj.Petal_7 = item.Petal_7;
                        obj.Petal_8 = item.Petal_8;
                        obj.Petal_9 = item.Petal_9;
                        obj.Petal_10 = item.Petal_10;

                        if (isAdded)
                        {
                            lstAddPRO374.Add(obj);
                        }
                    }
                    if (lstAddPRO374.Count > 0)
                    {
                        db.PRO374.AddRange(lstAddPRO374);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO374 = db.PRO374.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374.Count > 0)
                    {
                        db.PRO374.RemoveRange(lstDeletePRO374);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO374 = db.PRO374.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374.Count > 0)
                    {
                        db.PRO374.RemoveRange(lstDeletePRO374);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO374_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO374_2> lstAddPRO374_2 = new List<PRO374_2>();
                List<PRO374_2> lstDeletePRO374_2 = new List<PRO374_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO374_2 obj = db.PRO374_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO374_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.ReqAtTop = item.ReqAtTop;
                        obj.ActAtTop = item.ActAtTop;
                        obj.ReqAtMiddle = item.ReqAtMiddle;
                        obj.ActAtMiddle = item.ActAtMiddle;
                        obj.ReqAtBottom = item.ReqAtBottom;
                        obj.ActAtBottom = item.ActAtBottom;

                        if (isAdded)
                        {
                            lstAddPRO374_2.Add(obj);
                        }
                    }
                    if (lstAddPRO374_2.Count > 0)
                    {
                        db.PRO374_2.AddRange(lstAddPRO374_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO374_2 = db.PRO374_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_2.Count > 0)
                    {
                        db.PRO374_2.RemoveRange(lstDeletePRO374_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO374_2 = db.PRO374_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_2.Count > 0)
                    {
                        db.PRO374_2.RemoveRange(lstDeletePRO374_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6(List<PRO374_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO374_3> lstAddPRO374_3 = new List<PRO374_3>();
                List<PRO374_3> lstDeletePRO374_3 = new List<PRO374_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO374_3 obj = db.PRO374_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO374_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqAtTop = item.ReqAtTop;
                        obj.ActAtTop = item.ActAtTop;
                        obj.ReqAtBottom = item.ReqAtBottom;
                        obj.ActAtBottom = item.ActAtBottom;

                        if (isAdded)
                        {
                            lstAddPRO374_3.Add(obj);
                        }
                    }
                    if (lstAddPRO374_3.Count > 0)
                    {
                        db.PRO374_3.AddRange(lstAddPRO374_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO374_3 = db.PRO374_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_3.Count > 0)
                    {
                        db.PRO374_3.RemoveRange(lstDeletePRO374_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO374_3 = db.PRO374_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_3.Count > 0)
                    {
                        db.PRO374_3.RemoveRange(lstDeletePRO374_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7(List<PRO374_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO374_4> lstAddPRO374_4 = new List<PRO374_4>();
                List<PRO374_4> lstDeletePRO374_4 = new List<PRO374_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO374_4 obj = db.PRO374_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO374_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.RootGapMin = item.RootGapMin;
                        obj.RootGapMax = item.RootGapMax;
                        obj.RootFaceMin = item.RootFaceMin;
                        obj.RootFaceMax = item.RootFaceMax;
                        obj.Peak_in_out_Top = item.Peak_in_out_Top;
                        obj.Peak_in_out_Mid = item.Peak_in_out_Mid;
                        obj.Peak_in_out_Bottom = item.Peak_in_out_Bottom;
                        obj.OffSetMin = item.OffSetMin;
                        obj.OffSetMax = item.OffSetMax;
                        obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
                        obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
                        obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
                        obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;
                        if (isAdded)
                        {
                            lstAddPRO374_4.Add(obj);
                        }
                    }
                    if (lstAddPRO374_4.Count > 0)
                    {
                        db.PRO374_4.AddRange(lstAddPRO374_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO374_4 = db.PRO374_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_4.Count > 0)
                    {
                        db.PRO374_4.RemoveRange(lstDeletePRO374_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO374_4 = db.PRO374_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_4.Count > 0)
                    {
                        db.PRO374_4.RemoveRange(lstDeletePRO374_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details
        [HttpPost]
        public JsonResult SaveProtocolLine8(List<PRO374_5> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO374_5> lstAddPRO374_5 = new List<PRO374_5>();
                List<PRO374_5> lstDeletePRO374_5 = new List<PRO374_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO374_5 obj = db.PRO374_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO374_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.TTSeamNo = item.TTSeamNo;
                        obj.TTRootGapAllowed = item.TTRootGapAllowed;
                        obj.TTRootGapActualMinimum = item.TTRootGapActualMinimum;
                        obj.TTRootGapActualMaximum = item.TTRootGapActualMaximum;
                        obj.TTOffsetAllowed = item.TTOffsetAllowed;
                        obj.TTOffsetActualMinimum = item.TTOffsetActualMinimum;
                        obj.TTOffsetActualMaximum = item.TTOffsetActualMaximum;
                        obj.TTISWepAngleRequired = item.TTISWepAngleRequired;
                        obj.TTISWepAngleActualMinimum = item.TTISWepAngleActualMinimum;
                        obj.TTISWepAngleActualMaximum = item.TTISWepAngleActualMaximum;
                        obj.TTOSWepAngleRequired = item.TTOSWepAngleRequired;
                        obj.TTOSWepAngleActualMinimum = item.TTOSWepAngleActualMinimum;
                        obj.TTOSWepAngleActualMaximum = item.TTOSWepAngleActualMaximum;
                        obj.TTRootFaceAllowed = item.TTRootFaceAllowed;
                        obj.TTRootFaceActualMinimum = item.TTRootFaceActualMinimum;
                        obj.TTRootFaceActualMaximum = item.TTRootFaceActualMaximum;

                        if (isAdded)
                        {
                            lstAddPRO374_5.Add(obj);
                        }
                    }
                    if (lstAddPRO374_5.Count > 0)
                    {
                        db.PRO374_5.AddRange(lstAddPRO374_5);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO374_5 = db.PRO374_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_5.Count > 0)
                    {
                        db.PRO374_5.RemoveRange(lstDeletePRO374_5);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO374_5 = db.PRO374_5.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_5.Count > 0)
                    {
                        db.PRO374_5.RemoveRange(lstDeletePRO374_5);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line9 details
        [HttpPost]
        public JsonResult SaveProtocolLine9(List<PRO374_6> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO374_6> lstAddPRO374_6 = new List<PRO374_6>();
                List<PRO374_6> lstDeletePRO374_6 = new List<PRO374_6>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO374_6 obj = db.PRO374_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO374_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.HeightOfTierNo1 = item.HeightOfTierNo1;
                        obj.HeightOfTierReq1 = item.HeightOfTierReq1;
                        obj.HeightOfTierActual1 = item.HeightOfTierActual1;
                        obj.HeightOfTierNo2 = item.HeightOfTierNo2;
                        obj.HeightOfTierReq2 = item.HeightOfTierReq2;
                        obj.HeightOfTierActual2 = item.HeightOfTierActual2;
                        obj.HeightOfTierNo3 = item.HeightOfTierNo3;
                        obj.HeightOfTierReq3 = item.HeightOfTierReq3;
                        obj.HeightOfTierActual3 = item.HeightOfTierActual3;


                        if (isAdded)
                        {
                            lstAddPRO374_6.Add(obj);
                        }
                    }
                    if (lstAddPRO374_6.Count > 0)
                    {
                        db.PRO374_6.AddRange(lstAddPRO374_6);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO374_6 = db.PRO374_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_6.Count > 0)
                    {
                        db.PRO374_6.RemoveRange(lstDeletePRO374_6);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO374_6 = db.PRO374_6.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_6.Count > 0)
                    {
                        db.PRO374_6.RemoveRange(lstDeletePRO374_6);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line10 details
        [HttpPost]
        public JsonResult SaveProtocolLine10(List<PRO374_7> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO374_7> lstAddPRO374_7 = new List<PRO374_7>();
                List<PRO374_7> lstDeletePRO374_7 = new List<PRO374_7>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO374_7 obj = db.PRO374_7.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO374_7();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.PetalNo = item.PetalNo;
                        obj.GapPetal_1 = item.GapPetal_1;
                        obj.GapPetal_2 = item.GapPetal_2;
                        obj.GapPetal_3 = item.GapPetal_3;
                        obj.GapPetal_4 = item.GapPetal_4;
                        obj.GapPetal_5 = item.GapPetal_5;
                        obj.GapPetal_6 = item.GapPetal_6;
                        obj.GapPetal_7 = item.GapPetal_7;
                        obj.GapPetal_8 = item.GapPetal_8;
                        obj.GapPetal_9 = item.GapPetal_9;
                        obj.GapPetal_10 = item.GapPetal_10;
                        obj.GapPetal_11 = item.GapPetal_11;

                        if (isAdded)
                        {
                            lstAddPRO374_7.Add(obj);
                        }
                    }
                    if (lstAddPRO374_7.Count > 0)
                    {
                        db.PRO374_7.AddRange(lstAddPRO374_7);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO374_7 = db.PRO374_7.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_7.Count > 0)
                    {
                        db.PRO374_7.RemoveRange(lstDeletePRO374_7);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO374_7 = db.PRO374_7.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_7.Count > 0)
                    {
                        db.PRO374_7.RemoveRange(lstDeletePRO374_7);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line11 details
        [HttpPost]
        public JsonResult SaveProtocolLine11(List<PRO374_8> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO374_8> lstAddPRO374_8 = new List<PRO374_8>();
                List<PRO374_8> lstDeletePRO374_8 = new List<PRO374_8>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO374_8 obj = db.PRO374_8.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO374_8();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.PetalNo = item.PetalNo;
                        obj.K1 = item.K1;
                        obj.K2 = item.K2;
                        obj.K3 = item.K3;
                        obj.K1T = item.K1T;
                        obj.K2T = item.K2T;
                        obj.K3T = item.K3T;
                        obj.ActStraightAtTop = item.ActStraightAtTop;
                        obj.ActStraightAtBottom = item.ActStraightAtBottom;

                        if (isAdded)
                        {
                            lstAddPRO374_8.Add(obj);
                        }
                    }
                    if (lstAddPRO374_8.Count > 0)
                    {
                        db.PRO374_8.AddRange(lstAddPRO374_8);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO374_8 = db.PRO374_8.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_8.Count > 0)
                    {
                        db.PRO374_8.RemoveRange(lstDeletePRO374_8);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO374_8 = db.PRO374_8.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_8.Count > 0)
                    {
                        db.PRO374_8.RemoveRange(lstDeletePRO374_8);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line12 details
        [HttpPost]
        public JsonResult SaveProtocolLine12(List<PRO374_9> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO374_9> lstAddPRO374_9 = new List<PRO374_9>();
                List<PRO374_9> lstDeletePRO374_9 = new List<PRO374_9>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO374_9 obj = db.PRO374_9.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO374_9();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.MaterialIdentificationReport = item.MaterialIdentificationReport;

                        if (isAdded)
                        {
                            lstAddPRO374_9.Add(obj);
                        }
                    }
                    if (lstAddPRO374_9.Count > 0)
                    {
                        db.PRO374_9.AddRange(lstAddPRO374_9);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO374_9 = db.PRO374_9.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_9.Count > 0)
                    {
                        db.PRO374_9.RemoveRange(lstDeletePRO374_9);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO374_9 = db.PRO374_9.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO374_9.Count > 0)
                    {
                        db.PRO374_9.RemoveRange(lstDeletePRO374_9);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line13 details

        [HttpPost]
        public JsonResult SaveProtocolLine13(List<PRO374_10> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO374_10> lstAddPRO374_10 = new List<PRO374_10>();
                List<PRO374_10> lstDeletePRO374_10 = new List<PRO374_10>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO374_10 obj = db.PRO374_10.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO374_10 objFecthSeam = new PRO374_10();
                        if (obj == null)
                        {
                            obj = new PRO374_10();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO374_10.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRO374_10.Add(obj);
                        }
                    }
                }
                if (lstAddPRO374_10.Count > 0)
                {
                    db.PRO374_10.AddRange(lstAddPRO374_10);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO374_10 = db.PRO374_10.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRO374_10 = db.PRO374_10.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRO374_10.Count > 0)
                {
                    db.PRO374_10.RemoveRange(lstDeletePRO374_10);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line14 details

        [HttpPost]
        public JsonResult SaveProtocolLine14(List<PRO374_11> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO374_11> lstAddPRO374_11 = new List<PRO374_11>();
                List<PRO374_11> lstDeletePRO374_11 = new List<PRO374_11>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO374_11 obj = db.PRO374_11.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO374_11 objFecthSeam = new PRO374_11();
                        if (obj == null)
                        {
                            obj = new PRO374_11();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO374_11.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }

                        obj.CleatNo = item.CleatNo;
                        obj.SizeWelded = item.SizeWelded;
                        obj.Location = item.Location;
                        obj.OutsideOrInside = item.OutsideOrInside;
                        obj.DistanceFromSeam = item.DistanceFromSeam;
                        obj.Elevation = item.Elevation;
                        if (isAdded)
                        {
                            lstAddPRO374_11.Add(obj);
                        }
                    }
                }
                if (lstAddPRO374_11.Count > 0)
                {
                    db.PRO374_11.AddRange(lstAddPRO374_11);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO374_11 = db.PRO374_11.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else { lstDeletePRO374_11 = db.PRO374_11.Where(x => x.HeaderId == HeaderId).ToList(); }
                if (lstDeletePRO374_11.Count > 0)
                {
                    db.PRO374_11.RemoveRange(lstDeletePRO374_11);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)

        {
            PRL370 objPRL370 = new PRL370();
            objPRL370 = db.PRL370.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL370 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL370.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };
            List<string> lsttype = new List<string> { "TORI CONE", "STRAIGHT CONE" };

            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.Type = lsttype.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL371> lstPRL371 = db.PRL371.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL372> lstPRL372 = db.PRL372.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL373> lstPRL373 = db.PRL373.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374> lstPRL374 = db.PRL374.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_2> lstPRL374_2 = db.PRL374_2.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_3> lstPRL374_3 = db.PRL374_3.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_4> lstPRL374_4 = db.PRL374_4.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_5> lstPRL374_5 = db.PRL374_5.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_6> lstPRL374_6 = db.PRL374_6.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_7> lstPRL374_7 = db.PRL374_7.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_8> lstPRL374_8 = db.PRL374_8.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_9> lstPRL374_9 = db.PRL374_9.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();

            List<SP_IPI_PROTOCOL075_LINKAGE_GET_LINES13_DATA_Result> lstPRL374_10 = db.SP_IPI_PROTOCOL075_LINKAGE_GET_LINES13_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL370.HeaderId).ToList();
            List<SP_IPI_PROTOCOL075_LINKAGE_GET_LINES14_DATA_Result> lstPRL374_11 = db.SP_IPI_PROTOCOL075_LINKAGE_GET_LINES14_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL370.HeaderId).ToList();

            ViewBag.lstPRL371 = lstPRL371;
            ViewBag.lstPRL372 = lstPRL372;
            ViewBag.lstPRL373 = lstPRL373;
            ViewBag.lstPRL374 = lstPRL374;
            ViewBag.lstPRL374_2 = lstPRL374_2;
            ViewBag.lstPRL374_3 = lstPRL374_3;
            ViewBag.lstPRL374_4 = lstPRL374_4;
            ViewBag.lstPRL374_5 = lstPRL374_5;
            ViewBag.lstPRL374_6 = lstPRL374_6;
            ViewBag.lstPRL374_7 = lstPRL374_7;
            ViewBag.lstPRL374_8 = lstPRL374_8;
            ViewBag.lstPRL374_9 = lstPRL374_9;
            ViewBag.lstPRL374_10 = lstPRL374_10;
            ViewBag.lstPRL374_11 = lstPRL374_11;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL370.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL370.ActFilledBy) && (objPRL370.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL370.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL370);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL370 objPRL370 = new PRL370();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL370 = db.PRL370.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL370).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL370 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL370.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }

            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };
            List<string> lsttype = new List<string> { "TORI CONE", "STRAIGHT CONE" };

            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            var TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var Type = lsttype.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            var OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL371> lstPRL371 = db.PRL371.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL372> lstPRL372 = db.PRL372.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL373> lstPRL373 = db.PRL373.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374> lstPRL374 = db.PRL374.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_2> lstPRL374_2 = db.PRL374_2.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_3> lstPRL374_3 = db.PRL374_3.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_4> lstPRL374_4 = db.PRL374_4.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_5> lstPRL374_5 = db.PRL374_5.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_6> lstPRL374_6 = db.PRL374_6.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_7> lstPRL374_7 = db.PRL374_7.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_8> lstPRL374_8 = db.PRL374_8.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();
            List<PRL374_9> lstPRL374_9 = db.PRL374_9.Where(x => x.HeaderId == objPRL370.HeaderId).ToList();

            List<SP_IPI_PROTOCOL075_LINKAGE_GET_LINES13_DATA_Result> lstPRL374_10 = db.SP_IPI_PROTOCOL075_LINKAGE_GET_LINES13_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL370.HeaderId).ToList();
            List<SP_IPI_PROTOCOL075_LINKAGE_GET_LINES14_DATA_Result> lstPRL374_11 = db.SP_IPI_PROTOCOL075_LINKAGE_GET_LINES14_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL370.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL370.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL370.ActFilledBy) && (objPRL370.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL370.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    TypesOfForming = TypesOfForming,
                    Type = Type,
                    OutsideInside = OutsideOrInside,

                    objPRL370 = objPRL370,

                    lstPRL371 = lstPRL371,
                    lstPRL372 = lstPRL372,
                    lstPRL373 = lstPRL373,
                    lstPRL374 = lstPRL374,
                    lstPRL374_2 = lstPRL374_2,
                    lstPRL374_3 = lstPRL374_3,
                    lstPRL374_4 = lstPRL374_4,
                    lstPRL374_5 = lstPRL374_5,
                    lstPRL374_6 = lstPRL374_6,
                    lstPRL374_7 = lstPRL374_7,
                    lstPRL374_8 = lstPRL374_8,
                    lstPRL374_9 = lstPRL374_9,
                    lstPRL374_10 = lstPRL374_10,
                    lstPRL374_11 = lstPRL374_11,

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                // Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL370 PRL370, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL370.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL370 objPRL370 = db.PRL370.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL370.DrawingRevisionNo = PRL370.DrawingRevisionNo;
                    objPRL370.DrawingNo = PRL370.DrawingNo;
                    objPRL370.DCRNo = PRL370.DCRNo;
                    objPRL370.ProtocolNo = PRL370.ProtocolNo;

                    objPRL370.Type = PRL370.Type;
                    objPRL370.DiscOutsideCirc = PRL370.DiscOutsideCirc;
                    objPRL370.ReqTopOutsideCirc = PRL370.ReqTopOutsideCirc;
                    objPRL370.ActTopOutsideCirc = PRL370.ActTopOutsideCirc;
                    objPRL370.RemarksOutsideCirc = PRL370.RemarksOutsideCirc;
                    objPRL370.ReqBottomOutsideCirc = PRL370.ReqBottomOutsideCirc;
                    objPRL370.ActBottomOutsideCirc = PRL370.ActBottomOutsideCirc;
                    objPRL370.DiscInsideCirc1 = PRL370.DiscInsideCirc1;
                    objPRL370.ReqTopInsideCirc = PRL370.ReqTopInsideCirc;
                    objPRL370.ActTopInsideCirc = PRL370.ActTopInsideCirc;
                    objPRL370.RemarksInsideCirc = PRL370.RemarksInsideCirc;
                    objPRL370.ReqBottomInsideCirc = PRL370.ReqBottomInsideCirc;
                    objPRL370.ActBottomInsideCirc = PRL370.ActBottomInsideCirc;
                    objPRL370.DiscInsideCirc2 = PRL370.DiscInsideCirc2;
                    objPRL370.ReqTopInsideCirc2 = PRL370.ReqTopInsideCirc2;
                    objPRL370.ActCwTopInsideCirc2 = PRL370.ActCwTopInsideCirc2;
                    objPRL370.ActAcwTopInsideCirc2 = PRL370.ActAcwTopInsideCirc2;
                    objPRL370.ActAvgTopInsideCirc2 = PRL370.ActAvgTopInsideCirc2;
                    objPRL370.RemarksInsideCirc2 = PRL370.RemarksInsideCirc2;
                    objPRL370.ReqBottomInsideCirc2 = PRL370.ReqBottomInsideCirc2;
                    objPRL370.ActCwBottomInsideCirc2 = PRL370.ActCwBottomInsideCirc2;
                    objPRL370.ActAcwBottomInsideCirc2 = PRL370.ActAcwBottomInsideCirc2;
                    objPRL370.ActAvgBottomInsideCirc2 = PRL370.ActAvgBottomInsideCirc2;
                    objPRL370.TypeOfFormingRequired = PRL370.TypeOfFormingRequired;
                    objPRL370.TypeOfFormingActual = PRL370.TypeOfFormingActual;
                    objPRL370.TypeOfFormingRemarks = PRL370.TypeOfFormingRemarks;
                    objPRL370.TypeOfHeatRequired = PRL370.TypeOfHeatRequired;
                    objPRL370.TypeOfHeatActual = PRL370.TypeOfHeatActual;
                    objPRL370.TypeOfHeatRemarks = PRL370.TypeOfHeatRemarks;
                    objPRL370.Ovality = PRL370.Ovality;
                    objPRL370.OutByActTop = PRL370.OutByActTop;
                    objPRL370.OutByActBottom = PRL370.OutByActBottom;
                    objPRL370.ReqHieghtOfCone = PRL370.ReqHieghtOfCone;
                    objPRL370.ActCfTop = PRL370.ActCfTop;
                    objPRL370.ActCfBot = PRL370.ActCfBot;
                    objPRL370.FormulaTop1 = PRL370.FormulaTop1;
                    objPRL370.FormulaTop2 = PRL370.FormulaTop2;
                    objPRL370.FormulaTop3 = PRL370.FormulaTop3;
                    objPRL370.FormulaTop4 = PRL370.FormulaTop4;
                    objPRL370.FormulaBot1 = PRL370.FormulaBot1;
                    objPRL370.FormulaBot2 = PRL370.FormulaBot2;
                    objPRL370.FormulaBot3 = PRL370.FormulaBot3;
                    objPRL370.FormulaBot4 = PRL370.FormulaBot4;
                    objPRL370.ActTop1 = PRL370.ActTop1;
                    objPRL370.ActTop2 = PRL370.ActTop2;
                    objPRL370.ActTop3 = PRL370.ActTop3;
                    objPRL370.ActTop4 = PRL370.ActTop4;
                    objPRL370.ActBot1 = PRL370.ActBot1;
                    objPRL370.ActBot2 = PRL370.ActBot2;
                    objPRL370.ActBot3 = PRL370.ActBot3;
                    objPRL370.ActBot4 = PRL370.ActBot4;
                    objPRL370.ReqThicknessreadings = PRL370.ReqThicknessreadings;
                    objPRL370.AverageAtTop = PRL370.AverageAtTop;
                    objPRL370.AverageAtBottom = PRL370.AverageAtBottom;
                    objPRL370.Act0 = PRL370.Act0;
                    objPRL370.Act90 = PRL370.Act90;
                    objPRL370.Act180 = PRL370.Act180;
                    objPRL370.Act270 = PRL370.Act270;
                    objPRL370.ReqConcentricity = PRL370.ReqConcentricity;
                    objPRL370.ActConcentricity = PRL370.ActConcentricity;
                    objPRL370.DetailThiknessReport = PRL370.DetailThiknessReport;
                    objPRL370.evalueTemplate = PRL370.evalueTemplate;
                    objPRL370.AtMiddle = PRL370.AtMiddle;
                    objPRL370.ReqChordLengthTop = PRL370.ReqChordLengthTop;
                    objPRL370.ReqRadiusTop = PRL370.ReqRadiusTop;
                    objPRL370.ReqChordLengthMiddle = PRL370.ReqChordLengthMiddle;
                    objPRL370.ReqRadiusMiddle = PRL370.ReqRadiusMiddle;
                    objPRL370.ReqChordLengthBottom = PRL370.ReqChordLengthBottom;
                    objPRL370.ReqRadiusBottom = PRL370.ReqRadiusBottom;
                    objPRL370.ActChordLengthTop = PRL370.ActChordLengthTop;
                    objPRL370.ActRadiusTop = PRL370.ActRadiusTop;
                    objPRL370.ActChordLengthMiddle = PRL370.ActChordLengthMiddle;
                    objPRL370.ActRadiusMiddle = PRL370.ActRadiusMiddle;
                    objPRL370.ActChordLengthAtBottom = PRL370.ActChordLengthAtBottom;
                    objPRL370.ActRadiusAtBottom = PRL370.ActRadiusAtBottom;
                    objPRL370.TopGapAllowed = PRL370.TopGapAllowed;
                    objPRL370.MiddleGapAllowed = PRL370.MiddleGapAllowed;
                    objPRL370.BottomGapAllowed = PRL370.BottomGapAllowed;
                    objPRL370.RootGapReq = PRL370.RootGapReq;
                    objPRL370.RootFaceReq = PRL370.RootFaceReq;
                    objPRL370.Pick_in_out = PRL370.Pick_in_out;

                    objPRL370.maxAllowedOffset = PRL370.maxAllowedOffset;
                    objPRL370.ReqInsideWebAngle = PRL370.ReqInsideWebAngle;
                    objPRL370.ReqoutsideWebAngle = PRL370.ReqoutsideWebAngle;

                    objPRL370.WepDimensionsForCircmferantionSeamOfTierToTireJoint = PRL370.WepDimensionsForCircmferantionSeamOfTierToTireJoint;
                    objPRL370.HeightOfTier = PRL370.HeightOfTier;
                    objPRL370.ReqKnuckleRadiusAtTop = PRL370.ReqKnuckleRadiusAtTop;
                    objPRL370.ActKnuckleRadiusAtTop = PRL370.ActKnuckleRadiusAtTop;
                    objPRL370.ReqKnuckleRadiusAtBottom = PRL370.ReqKnuckleRadiusAtBottom;
                    objPRL370.ActKnuckleRadiusAtBottom = PRL370.ActKnuckleRadiusAtBottom;
                    objPRL370.ReqStraightFace = PRL370.ReqStraightFace;
                    objPRL370.CheckPoint1 = PRL370.CheckPoint1;
                    objPRL370.CheckPoint2 = PRL370.CheckPoint2;
                    objPRL370.CheckPoint3 = PRL370.CheckPoint3;
                    objPRL370.CheckPoint4 = PRL370.CheckPoint4;
                    objPRL370.CheckPoint5 = PRL370.CheckPoint5;
                    objPRL370.CheckPoint6 = PRL370.CheckPoint6;
                    objPRL370.CheckPoint7 = PRL370.CheckPoint7;
                    objPRL370.CheckPoint7_2 = PRL370.CheckPoint7_2;
                    objPRL370.CheckPoint7_3 = PRL370.CheckPoint7_3;
                    objPRL370.CheckPoint8 = PRL370.CheckPoint8;
                    objPRL370.CheckPoint8_2 = PRL370.CheckPoint8_2;
                    objPRL370.CheckPoint8_3 = PRL370.CheckPoint8_3;
                    objPRL370.CheckPoint8_4 = PRL370.CheckPoint8_4;
                    objPRL370.CheckPoint10 = PRL370.CheckPoint10;
                    objPRL370.CheckPoint10_2 = PRL370.CheckPoint10_2;
                    objPRL370.CheckPoint11 = PRL370.CheckPoint11;
                    objPRL370.QCRemarks = PRL370.QCRemarks;
                    objPRL370.Result = PRL370.Result;
                    objPRL370.ReqWidth = PRL370.ReqWidth;
                    objPRL370.ReqDepth = PRL370.ReqDepth;
                    objPRL370.ReqOffset = PRL370.ReqOffset;
                    objPRL370.Note1 = PRL370.Note1;
                    objPRL370.Note2 = PRL370.Note2;
                    objPRL370.Note3 = PRL370.Note3;
                    objPRL370.IdentificationDetailsOfCleats = PRL370.IdentificationDetailsOfCleats;

                    objPRL370.EditedBy = UserName;
                    objPRL370.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL370.ActFilledBy = UserName;
                            objPRL370.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL370.ReqFilledBy = UserName;
                            objPRL370.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL370.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL370 objPRL370 = db.PRL370.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL370 != null && string.IsNullOrWhiteSpace(objPRL370.ProtocolNo))
                    {
                        db.PRL370.Remove(objPRL370);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL370.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL371> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL371> lstAddPRL371 = new List<PRL371>();
                List<PRL371> lstDeletePRL371 = new List<PRL371>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL371 obj = db.PRL371.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTop = item.ActAtTop;
                                obj.ActAtBottom = item.ActAtBottom;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL371 obj = new PRL371();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTop = item.ActAtTop;
                                obj.ActAtBottom = item.ActAtBottom;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL371.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL371.Count > 0)
                    {
                        db.PRL371.AddRange(lstAddPRL371);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL371 = db.PRL371.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL371.Count > 0)
                    {
                        db.PRL371.RemoveRange(lstDeletePRL371);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL371 = db.PRL371.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL371.Count > 0)
                    {
                        db.PRL371.RemoveRange(lstDeletePRL371);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL372> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL372> lstAddPRL372 = new List<PRL372>();
                List<PRL372> lstDeletePRL372 = new List<PRL372>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL372 obj = db.PRL372.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL372();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.Actual = item.Actual;

                        if (isAdded)
                        {
                            lstAddPRL372.Add(obj);
                        }
                    }
                    if (lstAddPRL372.Count > 0)
                    {
                        db.PRL372.AddRange(lstAddPRL372);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL372 = db.PRL372.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL372.Count > 0)
                    {
                        db.PRL372.RemoveRange(lstDeletePRL372);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL372 = db.PRL372.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL372.Count > 0)
                    {
                        db.PRL372.RemoveRange(lstDeletePRL372);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL373> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL373> lstAddPRL373 = new List<PRL373>();
                List<PRL373> lstDeletePRL373 = new List<PRL373>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (!string.IsNullOrWhiteSpace(item.AtTopThickness) && item.AtTopThickness != null)
                        {
                            bool isAdded = false;
                            PRL373 obj = db.PRL373.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj == null)
                            {
                                obj = new PRL373();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                            else
                            {
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                            obj.SrNo = item.SrNo;
                            obj.AtTopThickness = item.AtTopThickness;
                            obj.AtBottomThickness = item.AtBottomThickness;
                            if (isAdded)
                            {
                                lstAddPRL373.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL373.Count > 0)
                    {
                        db.PRL373.AddRange(lstAddPRL373);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL373 = db.PRL373.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL373.Count > 0)
                    {
                        db.PRL373.RemoveRange(lstDeletePRL373);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL373 = db.PRL373.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL373.Count > 0)
                    {
                        db.PRL373.RemoveRange(lstDeletePRL373);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL374> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL374> lstAddPRL374 = new List<PRL374>();
                List<PRL374> lstDeletePRL374 = new List<PRL374>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL374 obj = db.PRL374.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL374();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Description = item.Description;
                        obj.Petal_1 = item.Petal_1;
                        obj.Petal_2 = item.Petal_2;
                        obj.Petal_3 = item.Petal_3;
                        obj.Petal_4 = item.Petal_4;
                        obj.Petal_5 = item.Petal_5;
                        obj.Petal_6 = item.Petal_6;
                        obj.Petal_7 = item.Petal_7;
                        obj.Petal_8 = item.Petal_8;
                        obj.Petal_9 = item.Petal_9;
                        obj.Petal_10 = item.Petal_10;

                        if (isAdded)
                        {
                            lstAddPRL374.Add(obj);
                        }
                    }
                    if (lstAddPRL374.Count > 0)
                    {
                        db.PRL374.AddRange(lstAddPRL374);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL374 = db.PRL374.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374.Count > 0)
                    {
                        db.PRL374.RemoveRange(lstDeletePRL374);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL374 = db.PRL374.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374.Count > 0)
                    {
                        db.PRL374.RemoveRange(lstDeletePRL374);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL374_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL374_2> lstAddPRL374_2 = new List<PRL374_2>();
                List<PRL374_2> lstDeletePRL374_2 = new List<PRL374_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL374_2 obj = db.PRL374_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL374_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.ReqAtTop = item.ReqAtTop;
                        obj.ActAtTop = item.ActAtTop;
                        obj.ReqAtMiddle = item.ReqAtMiddle;
                        obj.ActAtMiddle = item.ActAtMiddle;
                        obj.ReqAtBottom = item.ReqAtBottom;
                        obj.ActAtBottom = item.ActAtBottom;

                        if (isAdded)
                        {
                            lstAddPRL374_2.Add(obj);
                        }
                    }
                    if (lstAddPRL374_2.Count > 0)
                    {
                        db.PRL374_2.AddRange(lstAddPRL374_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL374_2 = db.PRL374_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_2.Count > 0)
                    {
                        db.PRL374_2.RemoveRange(lstDeletePRL374_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL374_2 = db.PRL374_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_2.Count > 0)
                    {
                        db.PRL374_2.RemoveRange(lstDeletePRL374_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine6Linkage(List<PRL374_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL374_3> lstAddPRL374_3 = new List<PRL374_3>();
                List<PRL374_3> lstDeletePRL374_3 = new List<PRL374_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL374_3 obj = db.PRL374_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL374_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqAtTop = item.ReqAtTop;
                        obj.ActAtTop = item.ActAtTop;
                        obj.ReqAtBottom = item.ReqAtBottom;
                        obj.ActAtBottom = item.ActAtBottom;

                        if (isAdded)
                        {
                            lstAddPRL374_3.Add(obj);
                        }
                    }
                    if (lstAddPRL374_3.Count > 0)
                    {
                        db.PRL374_3.AddRange(lstAddPRL374_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL374_3 = db.PRL374_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_3.Count > 0)
                    {
                        db.PRL374_3.RemoveRange(lstDeletePRL374_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL374_3 = db.PRL374_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_3.Count > 0)
                    {
                        db.PRL374_3.RemoveRange(lstDeletePRL374_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line7 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine7Linkage(List<PRL374_4> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL374_4> lstAddPRL374_4 = new List<PRL374_4>();
                List<PRL374_4> lstDeletePRL374_4 = new List<PRL374_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL374_4 obj = db.PRL374_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL374_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.RootGapMin = item.RootGapMin;
                        obj.RootGapMax = item.RootGapMax;
                        obj.RootFaceMin = item.RootFaceMin;
                        obj.RootFaceMax = item.RootFaceMax;
                        obj.Peak_in_out_Top = item.Peak_in_out_Top;
                        obj.Peak_in_out_Mid = item.Peak_in_out_Mid;
                        obj.Peak_in_out_Bottom = item.Peak_in_out_Bottom;
                        obj.OffSetMin = item.OffSetMin;
                        obj.OffSetMax = item.OffSetMax;
                        obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
                        obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
                        obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
                        obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;
                        if (isAdded)
                        {
                            lstAddPRL374_4.Add(obj);
                        }
                    }
                    if (lstAddPRL374_4.Count > 0)
                    {
                        db.PRL374_4.AddRange(lstAddPRL374_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL374_4 = db.PRL374_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_4.Count > 0)
                    {
                        db.PRL374_4.RemoveRange(lstDeletePRL374_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL374_4 = db.PRL374_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_4.Count > 0)
                    {
                        db.PRL374_4.RemoveRange(lstDeletePRL374_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine8Linkage(List<PRL374_5> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL374_5> lstAddPRL374_5 = new List<PRL374_5>();
                List<PRL374_5> lstDeletePRL374_5 = new List<PRL374_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL374_5 obj = db.PRL374_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL374_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.TTSeamNo = item.TTSeamNo;
                        obj.TTRootGapAllowed = item.TTRootGapAllowed;
                        obj.TTRootGapActualMinimum = item.TTRootGapActualMinimum;
                        obj.TTRootGapActualMaximum = item.TTRootGapActualMaximum;
                        obj.TTOffsetAllowed = item.TTOffsetAllowed;
                        obj.TTOffsetActualMinimum = item.TTOffsetActualMinimum;
                        obj.TTOffsetActualMaximum = item.TTOffsetActualMaximum;
                        obj.TTISWepAngleRequired = item.TTISWepAngleRequired;
                        obj.TTISWepAngleActualMinimum = item.TTISWepAngleActualMinimum;
                        obj.TTISWepAngleActualMaximum = item.TTISWepAngleActualMaximum;
                        obj.TTOSWepAngleRequired = item.TTOSWepAngleRequired;
                        obj.TTOSWepAngleActualMinimum = item.TTOSWepAngleActualMinimum;
                        obj.TTOSWepAngleActualMaximum = item.TTOSWepAngleActualMaximum;
                        obj.TTRootFaceAllowed = item.TTRootFaceAllowed;
                        obj.TTRootFaceActualMinimum = item.TTRootFaceActualMinimum;
                        obj.TTRootFaceActualMaximum = item.TTRootFaceActualMaximum;

                        if (isAdded)
                        {
                            lstAddPRL374_5.Add(obj);
                        }
                    }
                    if (lstAddPRL374_5.Count > 0)
                    {
                        db.PRL374_5.AddRange(lstAddPRL374_5);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL374_5 = db.PRL374_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_5.Count > 0)
                    {
                        db.PRL374_5.RemoveRange(lstDeletePRL374_5);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL374_5 = db.PRL374_5.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_5.Count > 0)
                    {
                        db.PRL374_5.RemoveRange(lstDeletePRL374_5);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line9 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine9Linkage(List<PRL374_6> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL374_6> lstAddPRL374_6 = new List<PRL374_6>();
                List<PRL374_6> lstDeletePRL374_6 = new List<PRL374_6>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL374_6 obj = db.PRL374_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL374_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.HeightOfTierNo1 = item.HeightOfTierNo1;
                        obj.HeightOfTierReq1 = item.HeightOfTierReq1;
                        obj.HeightOfTierActual1 = item.HeightOfTierActual1;
                        obj.HeightOfTierNo2 = item.HeightOfTierNo2;
                        obj.HeightOfTierReq2 = item.HeightOfTierReq2;
                        obj.HeightOfTierActual2 = item.HeightOfTierActual2;
                        obj.HeightOfTierNo3 = item.HeightOfTierNo3;
                        obj.HeightOfTierReq3 = item.HeightOfTierReq3;
                        obj.HeightOfTierActual3 = item.HeightOfTierActual3;


                        if (isAdded)
                        {
                            lstAddPRL374_6.Add(obj);
                        }
                    }
                    if (lstAddPRL374_6.Count > 0)
                    {
                        db.PRL374_6.AddRange(lstAddPRL374_6);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL374_6 = db.PRL374_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_6.Count > 0)
                    {
                        db.PRL374_6.RemoveRange(lstDeletePRL374_6);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL374_6 = db.PRL374_6.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_6.Count > 0)
                    {
                        db.PRL374_6.RemoveRange(lstDeletePRL374_6);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line10 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine10Linkage(List<PRL374_7> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL374_7> lstAddPRL374_7 = new List<PRL374_7>();
                List<PRL374_7> lstDeletePRL374_7 = new List<PRL374_7>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL374_7 obj = db.PRL374_7.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL374_7();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.PetalNo = item.PetalNo;
                        obj.GapPetal_1 = item.GapPetal_1;
                        obj.GapPetal_2 = item.GapPetal_2;
                        obj.GapPetal_3 = item.GapPetal_3;
                        obj.GapPetal_4 = item.GapPetal_4;
                        obj.GapPetal_5 = item.GapPetal_5;
                        obj.GapPetal_6 = item.GapPetal_6;
                        obj.GapPetal_7 = item.GapPetal_7;
                        obj.GapPetal_8 = item.GapPetal_8;
                        obj.GapPetal_9 = item.GapPetal_9;
                        obj.GapPetal_10 = item.GapPetal_10;
                        obj.GapPetal_11 = item.GapPetal_11;

                        if (isAdded)
                        {
                            lstAddPRL374_7.Add(obj);
                        }
                    }
                    if (lstAddPRL374_7.Count > 0)
                    {
                        db.PRL374_7.AddRange(lstAddPRL374_7);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL374_7 = db.PRL374_7.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_7.Count > 0)
                    {
                        db.PRL374_7.RemoveRange(lstDeletePRL374_7);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL374_7 = db.PRL374_7.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_7.Count > 0)
                    {
                        db.PRL374_7.RemoveRange(lstDeletePRL374_7);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line11 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine11Linkage(List<PRL374_8> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL374_8> lstAddPRL374_8 = new List<PRL374_8>();
                List<PRL374_8> lstDeletePRL374_8 = new List<PRL374_8>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL374_8 obj = db.PRL374_8.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL374_8();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.PetalNo = item.PetalNo;
                        obj.K1 = item.K1;
                        obj.K2 = item.K2;
                        obj.K3 = item.K3;
                        obj.K1T = item.K1T;
                        obj.K2T = item.K2T;
                        obj.K3T = item.K3T;
                        obj.ActStraightAtTop = item.ActStraightAtTop;
                        obj.ActStraightAtBottom = item.ActStraightAtBottom;

                        if (isAdded)
                        {
                            lstAddPRL374_8.Add(obj);
                        }
                    }
                    if (lstAddPRL374_8.Count > 0)
                    {
                        db.PRL374_8.AddRange(lstAddPRL374_8);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL374_8 = db.PRL374_8.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_8.Count > 0)
                    {
                        db.PRL374_8.RemoveRange(lstDeletePRL374_8);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL374_8 = db.PRL374_8.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_8.Count > 0)
                    {
                        db.PRL374_8.RemoveRange(lstDeletePRL374_8);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line12 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine12Linkage(List<PRL374_9> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL374_9> lstAddPRL374_9 = new List<PRL374_9>();
                List<PRL374_9> lstDeletePRL374_9 = new List<PRL374_9>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL374_9 obj = db.PRL374_9.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL374_9();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.MaterialIdentificationReport = item.MaterialIdentificationReport;

                        if (isAdded)
                        {
                            lstAddPRL374_9.Add(obj);
                        }
                    }
                    if (lstAddPRL374_9.Count > 0)
                    {
                        db.PRL374_9.AddRange(lstAddPRL374_9);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL374_9 = db.PRL374_9.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_9.Count > 0)
                    {
                        db.PRL374_9.RemoveRange(lstDeletePRL374_9);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL374_9 = db.PRL374_9.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL374_9.Count > 0)
                    {
                        db.PRL374_9.RemoveRange(lstDeletePRL374_9);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line13 Linkage

        [HttpPost]
        public JsonResult SaveProtocolLine13Linkage(List<PRL374_10> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL374_10> lstAddPRL374_10 = new List<PRL374_10>();
                List<PRL374_10> lstDeletePRL374_10 = new List<PRL374_10>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL374_10 obj = db.PRL374_10.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL374_10 objFecthSeam = new PRL374_10();
                        if (obj == null)
                        {
                            obj = new PRL374_10();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL374_10.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRL374_10.Add(obj);
                        }
                    }
                }
                if (lstAddPRL374_10.Count > 0)
                {
                    db.PRL374_10.AddRange(lstAddPRL374_10);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL374_10 = db.PRL374_10.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL374_10 = db.PRL374_10.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL374_10.Count > 0)
                {
                    db.PRL374_10.RemoveRange(lstDeletePRL374_10);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line14 Linkage

        [HttpPost]
        public JsonResult SaveProtocolLine14Linkage(List<PRL374_11> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL374_11> lstAddPRL374_11 = new List<PRL374_11>();
                List<PRL374_11> lstDeletePRL374_11 = new List<PRL374_11>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL374_11 obj = db.PRL374_11.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL374_11 objFecthSeam = new PRL374_11();
                        if (obj == null)
                        {
                            obj = new PRL374_11();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL374_11.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }

                        obj.CleatNo = item.CleatNo;
                        obj.SizeWelded = item.SizeWelded;
                        obj.Location = item.Location;
                        obj.OutsideOrInside = item.OutsideOrInside;
                        obj.DistanceFromSeam = item.DistanceFromSeam;
                        obj.Elevation = item.Elevation;
                        if (isAdded)
                        {
                            lstAddPRL374_11.Add(obj);
                        }
                    }
                }
                if (lstAddPRL374_11.Count > 0)
                {
                    db.PRL374_11.AddRange(lstAddPRL374_11);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL374_11 = db.PRL374_11.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else { lstDeletePRL374_11 = db.PRL374_11.Where(x => x.HeaderId == HeaderId).ToList(); }
                if (lstDeletePRL374_11.Count > 0)
                {
                    db.PRL374_11.RemoveRange(lstDeletePRL374_11);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

    }
}