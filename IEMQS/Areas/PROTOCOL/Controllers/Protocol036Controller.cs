﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol036Controller : clsBase
    {
        // GET: PROTOCOL/Protocol036
        string ControllerURL = "/PROTOCOL/Protocol036/";
        string Title = "SET-UP & DIMENSION REPORT FOR ROLLED SHELL LONGITUDINAL SEAM";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO175 objPRO175 = new PRO175();
            if (!id.HasValue)
            {
                try
                {
                    objPRO175.ProtocolNo = string.Empty;
                    objPRO175.CreatedBy = objClsLoginInfo.UserName;
                    objPRO175.CreatedOn = DateTime.Now;

                    #region  CIRCUMFERENCE MEASUREMENT
                    objPRO175.PRO178.Add(new PRO178
                    {
                        Location = "TOP",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    objPRO175.PRO178.Add(new PRO178
                    {
                        Location = "MIDDLE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO175.PRO178.Add(new PRO178
                    {
                        Location = "BOTTOM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region TOTAL SHELL HEIGHT
                    objPRO175.PRO179.Add(new PRO179
                    {
                        Orientation = "AT 0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO175.PRO179.Add(new PRO179
                    {
                        Orientation = "AT 90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO175.PRO179.Add(new PRO179
                    {
                        Orientation = "AT 180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO175.PRO179.Add(new PRO179
                    {
                        Orientation = "AT 270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region OUT OF ROUNDNESS 
                    objPRO175.PRO179_2.Add(new PRO179_2
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO175.PRO179_2.Add(new PRO179_2
                    {
                        Orientation = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO175.PRO179_2.Add(new PRO179_2
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO175.PRO179_2.Add(new PRO179_2
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO175.PRO179_2.Add(new PRO179_2
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO175.PRO179_2.Add(new PRO179_2
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO175.PRO179_2.Add(new PRO179_2
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO175.PRO179_2.Add(new PRO179_2
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO175.Add(objPRO175);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO175.HeaderId;
            }
            else
            {
                objPRO175 = db.PRO175.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO176> lstPRO176 = db.PRO176.Where(x => x.HeaderId == objPRO175.HeaderId).ToList();
            List<PRO177> lstPRO177 = db.PRO177.Where(x => x.HeaderId == objPRO175.HeaderId).ToList();
            List<PRO178> lstPRO178 = db.PRO178.Where(x => x.HeaderId == objPRO175.HeaderId).ToList();
            List<PRO179> lstPRO179 = db.PRO179.Where(x => x.HeaderId == objPRO175.HeaderId).ToList();
            List<PRO179_2> lstPRO179_2 = db.PRO179_2.Where(x => x.HeaderId == objPRO175.HeaderId).ToList();
            List<PRO179_3> lstPRO179_3 = db.PRO179_3.Where(x => x.HeaderId == objPRO175.HeaderId).ToList();
            List<PRO179_4> lstPRO179_4 = db.PRO179_4.Where(x => x.HeaderId == objPRO175.HeaderId).ToList();
            List<SP_IPI_PROTOCOL036_GET_LINES8_DATA_Result> lstPRO179_5 = db.SP_IPI_PROTOCOL036_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO175.HeaderId).ToList();
            List<SP_IPI_PROTOCOL036_GET_LINES9_DATA_Result> lstPRO179_6 = db.SP_IPI_PROTOCOL036_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO175.HeaderId).ToList();
            //List<PRO179_5> lstPRO179_5 = db.PRO179_5.Where(x => x.HeaderId == objPRO175.HeaderId).OrderBy(x => new { x.SeamNo, x.LineId }).ToList();
            //List<PRO179_6> lstPRO179_6 = db.PRO179_6.Where(x => x.HeaderId == objPRO175.HeaderId).OrderBy(x => new { x.SeamNo, x.LineId }).ToList();

            ViewBag.lstPRO176 = lstPRO176;
            ViewBag.lstPRO177 = lstPRO177;
            ViewBag.lstPRO178 = lstPRO178;
            ViewBag.lstPRO179 = lstPRO179;
            ViewBag.lstPRO179_2 = lstPRO179_2;
            ViewBag.lstPRO179_3 = lstPRO179_3;
            ViewBag.lstPRO179_4 = lstPRO179_4;
            ViewBag.lstPRO179_5 = lstPRO179_5;
            ViewBag.lstPRO179_6 = lstPRO179_6;
            #endregion

            return View(objPRO175);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO175 PRO175)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO175.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO175 objPRO175 = db.PRO175.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO175.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO175.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO175.ProtocolNo = PRO175.ProtocolNo;
                        objPRO175.ReqShellThickness = PRO175.ReqShellThickness;
                        objPRO175.ActShellThickness1 = PRO175.ActShellThickness1;
                        objPRO175.ActShellThickness2 = PRO175.ActShellThickness2;
                        objPRO175.ActShellThickness3 = PRO175.ActShellThickness3;
                        objPRO175.ActShellThickness4 = PRO175.ActShellThickness4;
                        objPRO175.ActShellThickness5 = PRO175.ActShellThickness5;
                        objPRO175.ActShellThickness6 = PRO175.ActShellThickness6;
                        objPRO175.ActShellThickness7 = PRO175.ActShellThickness7;
                        objPRO175.ActShellThickness8 = PRO175.ActShellThickness8;
                        objPRO175.ActShellThickness9 = PRO175.ActShellThickness9;
                        objPRO175.ReqWEPDimensionRootGap = PRO175.ReqWEPDimensionRootGap;
                        objPRO175.ReqWEPDimensionRootFace = PRO175.ReqWEPDimensionRootFace;
                        objPRO175.ReqWEPDimensionInsideWEPAngle = PRO175.ReqWEPDimensionInsideWEPAngle;
                        objPRO175.ReqWEPDimensionOutsideWEPAngle = PRO175.ReqWEPDimensionOutsideWEPAngle;
                        objPRO175.ReqWEPDimensionOffset = PRO175.ReqWEPDimensionOffset;
                        objPRO175.CircumferenceMeasurementReqIDCF = PRO175.CircumferenceMeasurementReqIDCF;
                        objPRO175.CircumferenceMeasurementReqODCF = PRO175.CircumferenceMeasurementReqODCF;
                        objPRO175.ReqTotalShellHeight = PRO175.ReqTotalShellHeight;
                        objPRO175.OutOfRoundnessReqID = PRO175.OutOfRoundnessReqID;
                        objPRO175.OutOfRoundnessOutBy = PRO175.OutOfRoundnessOutBy;
                        objPRO175.ProfileMeasurementAllowed = PRO175.ProfileMeasurementAllowed;
                        objPRO175.ExternalPressureVessel = PRO175.ExternalPressureVessel;
                        objPRO175.PressureVesselReqChordLength = PRO175.PressureVesselReqChordLength;
                        objPRO175.PressureVesselActChordLength = PRO175.PressureVesselActChordLength;
                        objPRO175.PressureVesselReqRadius = PRO175.PressureVesselReqRadius;
                        objPRO175.PressureVesselActRadius = PRO175.PressureVesselActRadius;
                        objPRO175.PressureVesselAllowableGap = PRO175.PressureVesselAllowableGap;
                        objPRO175.PressureVesselActGap = PRO175.PressureVesselActGap;
                        objPRO175.AdjacentLongSeam = PRO175.AdjacentLongSeam;
                        objPRO175.CheckPoint1 = PRO175.CheckPoint1;
                        objPRO175.CheckPoint2 = PRO175.CheckPoint2;
                        objPRO175.CheckPoint3 = PRO175.CheckPoint3;
                        objPRO175.CheckPoint4 = PRO175.CheckPoint4;
                        objPRO175.CheckPoint5 = PRO175.CheckPoint5;
                        objPRO175.CheckPoint6 = PRO175.CheckPoint6;
                        objPRO175.CheckPoint7 = PRO175.CheckPoint7;
                        objPRO175.CheckPoint8 = PRO175.CheckPoint8;
                        objPRO175.CheckPoint10 = PRO175.CheckPoint10;
                        objPRO175.CheckPoint10_2 = PRO175.CheckPoint10_2;
                        objPRO175.CheckPoint11 = PRO175.CheckPoint11;
                        objPRO175.CheckPoint11_2 = PRO175.CheckPoint11_2;
                        objPRO175.CheckPoint11_3 = PRO175.CheckPoint11_3;
                        objPRO175.CheckPoint12 = PRO175.CheckPoint12;
                        objPRO175.QCRemarks = PRO175.QCRemarks;
                        objPRO175.Result = PRO175.Result;
                        objPRO175.Note1 = PRO175.Note1;
                        objPRO175.Note2 = PRO175.Note2;
                        objPRO175.Note3 = PRO175.Note3;

                        objPRO175.ReqDepth = PRO175.ReqDepth;
                        objPRO175.ReqOffset = PRO175.ReqOffset;
                        objPRO175.ReqWidth = PRO175.ReqWidth;

                        objPRO175.IdentificationOfCleats = PRO175.IdentificationOfCleats;
                        objPRO175.EditedBy = objClsLoginInfo.UserName;
                        objPRO175.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO175.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO175 objPRO175 = db.PRO175.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO175 != null && string.IsNullOrWhiteSpace(objPRO175.ProtocolNo))
                    {
                        db.PRO175.Remove(objPRO175);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO175.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO176> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO176> lstAddPRO176 = new List<PRO176>();
                List<PRO176> lstDeletePRO176 = new List<PRO176>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO176 obj = db.PRO176.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO176 obj = new PRO176();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO176.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO176.Count > 0)
                    {
                        db.PRO176.AddRange(lstAddPRO176);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO176 = db.PRO176.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO176.Count > 0)
                    {
                        db.PRO176.RemoveRange(lstDeletePRO176);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO176 = db.PRO176.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO176.Count > 0)
                    {
                        db.PRO176.RemoveRange(lstDeletePRO176);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO177> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO177> lstAddPRO177 = new List<PRO177>();
                List<PRO177> lstDeletePRO177 = new List<PRO177>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO177 obj = db.PRO177.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO177();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ActMaxRootGap = item.ActMaxRootGap;
                        obj.ActMinRootGap = item.ActMinRootGap;
                        obj.ActMaxRootFace = item.ActMaxRootFace;
                        obj.ActMinRootFace = item.ActMinRootFace;
                        obj.ActMaxInsideWEBAngle = item.ActMaxInsideWEBAngle;
                        obj.ActMinInsideWEBAngle = item.ActMinInsideWEBAngle;
                        obj.ActMaxOutsideWEBAngle = item.ActMaxOutsideWEBAngle;
                        obj.ActMinOutsideWEBAngle = item.ActMinOutsideWEBAngle;
                        obj.ActMaxOffset = item.ActMaxOffset;
                        obj.ActMinOffset = item.ActMinOffset;

                        if (isAdded)
                        {
                            lstAddPRO177.Add(obj);
                        }
                    }
                    if (lstAddPRO177.Count > 0)
                    {
                        db.PRO177.AddRange(lstAddPRO177);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO177 = db.PRO177.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO177.Count > 0)
                    {
                        db.PRO177.RemoveRange(lstDeletePRO177);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO177 = db.PRO177.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO177.Count > 0)
                    {
                        db.PRO177.RemoveRange(lstDeletePRO177);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO178> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO178> lstAddPRO178 = new List<PRO178>();
                List<PRO178> lstDeletePRO178 = new List<PRO178>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO178 obj = db.PRO178.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRO178();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.ActIDCF = item.ActIDCF;
                            obj.ActODCF = item.ActODCF;
                        }
                        if (isAdded)
                        {
                            lstAddPRO178.Add(obj);
                        }
                    }
                    if (lstAddPRO178.Count > 0)
                    {
                        db.PRO178.AddRange(lstAddPRO178);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO178 = db.PRO178.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO178.Count > 0)
                    {
                        db.PRO178.RemoveRange(lstDeletePRO178);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO178 = db.PRO178.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO178.Count > 0)
                    {
                        db.PRO178.RemoveRange(lstDeletePRO178);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO179> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO179> lstAddPRO179 = new List<PRO179>();
                List<PRO179> lstDeletePRO179 = new List<PRO179>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO179 obj = db.PRO179.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO179();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActValue = item.ActValue;
                        if (isAdded)
                        {
                            lstAddPRO179.Add(obj);
                        }
                    }
                    if (lstAddPRO179.Count > 0)
                    {
                        db.PRO179.AddRange(lstAddPRO179);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO179 = db.PRO179.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO179.Count > 0)
                    {
                        db.PRO179.RemoveRange(lstDeletePRO179);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO179 = db.PRO179.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO179.Count > 0)
                    {
                        db.PRO179.RemoveRange(lstDeletePRO179);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO179_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO179_2> lstAddPRO179_2 = new List<PRO179_2>();
                List<PRO179_2> lstDeletePRO179_2 = new List<PRO179_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO179_2 obj = db.PRO179_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO179_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Orientation = item.Orientation;
                        obj.ActAtTop = item.ActAtTop;
                        obj.ActAtBottom = item.ActAtBottom;

                        if (isAdded)
                        {
                            lstAddPRO179_2.Add(obj);
                        }
                    }
                    if (lstAddPRO179_2.Count > 0)
                    {
                        db.PRO179_2.AddRange(lstAddPRO179_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO179_2 = db.PRO179_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO179_2.Count > 0)
                    {
                        db.PRO179_2.RemoveRange(lstDeletePRO179_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO179_2 = db.PRO179_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO179_2.Count > 0)
                    {
                        db.PRO179_2.RemoveRange(lstDeletePRO179_2);
                    }
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6(List<PRO179_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO179_3> lstAddPRO179_3 = new List<PRO179_3>();
                List<PRO179_3> lstDeletePRO179_3 = new List<PRO179_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO179_3 obj = db.PRO179_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO179_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.SeamNo = item.SeamNo;
                        obj.ActTopPeakIn = item.ActTopPeakIn;
                        obj.ActTopPeakOut = item.ActTopPeakOut;
                        obj.ActMidPeakIn = item.ActMidPeakIn;
                        obj.ActMidPeakOut = item.ActMidPeakOut;
                        obj.ActBottomPeakIn = item.ActBottomPeakIn;
                        obj.ActBottomPeakOut = item.ActBottomPeakOut;
                        if (isAdded)
                        {
                            lstAddPRO179_3.Add(obj);
                        }
                    }
                    if (lstAddPRO179_3.Count > 0)
                    {
                        db.PRO179_3.AddRange(lstAddPRO179_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO179_3 = db.PRO179_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO179_3.Count > 0)
                    {
                        db.PRO179_3.RemoveRange(lstDeletePRO179_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO179_3 = db.PRO179_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO179_3.Count > 0)
                    {
                        db.PRO179_3.RemoveRange(lstDeletePRO179_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7(List<PRO179_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO179_4> lstAddPRO179_4 = new List<PRO179_4>();
                List<PRO179_4> lstDeletePRO179_4 = new List<PRO179_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO179_4 obj = db.PRO179_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO179_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.SeamNo = item.SeamNo;
                        obj.ReqValue = item.ReqValue;
                        obj.ActValue = item.ActValue;
                        if (isAdded)
                        {
                            lstAddPRO179_4.Add(obj);
                        }
                    }
                    if (lstAddPRO179_4.Count > 0)
                    {
                        db.PRO179_4.AddRange(lstAddPRO179_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO179_4 = db.PRO179_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO179_4.Count > 0)
                    {
                        db.PRO179_4.RemoveRange(lstDeletePRO179_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO179_4 = db.PRO179_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO179_4.Count > 0)
                    {
                        db.PRO179_4.RemoveRange(lstDeletePRO179_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details

        [HttpPost]
        public JsonResult SaveProtocolLine8(List<PRO179_5> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO179_5> lstAddPRO179_5 = new List<PRO179_5>();
                List<PRO179_5> lstDeletePRO179_5 = new List<PRO179_5>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO179_5 obj = db.PRO179_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO179_5 objFecthSeam = new PRO179_5();
                        if (obj == null)
                        {
                            obj = new PRO179_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO179_5.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            obj.ReqDepth = objFecthSeam.ReqDepth;
                            obj.ReqWidth = objFecthSeam.ReqWidth;
                            obj.ReqOffset = objFecthSeam.ReqOffset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            obj.ReqDepth = item.ReqDepth;
                            obj.ReqWidth = item.ReqWidth;
                            obj.ReqOffset = item.ReqOffset;
                        }
                        obj.ActDepth = item.ActDepth;
                        obj.ActWidth = item.ActWidth;
                        obj.ActOffset = item.ActOffset;
                        obj.SpotNo = item.SpotNo;
                        if (isAdded)
                        {
                            lstAddPRO179_5.Add(obj);
                        }
                    }
                }
                if (lstAddPRO179_5.Count > 0)
                {
                    db.PRO179_5.AddRange(lstAddPRO179_5);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO179_5 = db.PRO179_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRO179_5 = db.PRO179_5.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRO179_5.Count > 0)
                {
                    db.PRO179_5.RemoveRange(lstDeletePRO179_5);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 details

        [HttpPost]
        public JsonResult SaveProtocolLine9(List<PRO179_6> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO179_6> lstAddPRO179_6 = new List<PRO179_6>();
                List<PRO179_6> lstDeletePRO179_6 = new List<PRO179_6>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO179_6 obj = db.PRO179_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO179_6 objFecthSeam = new PRO179_6();
                        if (obj == null)
                        {
                            obj = new PRO179_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO179_6.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }

                        obj.CleatNo = item.CleatNo;
                        obj.ParentMetalSize = item.ParentMetalSize;
                        obj.Location = item.Location;
                        obj.OutsideInside = item.OutsideInside;
                        obj.Distance = item.Distance;
                        obj.ShellEvelution = item.ShellEvelution;
                        if (isAdded)
                        {
                            lstAddPRO179_6.Add(obj);
                        }
                    }
                }
                if (lstAddPRO179_6.Count > 0)
                {
                    db.PRO179_6.AddRange(lstAddPRO179_6);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO179_6 = db.PRO179_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else { lstDeletePRO179_6 = db.PRO179_6.Where(x => x.HeaderId == HeaderId).ToList(); }
                if (lstDeletePRO179_6.Count > 0)
                {
                    db.PRO179_6.RemoveRange(lstDeletePRO179_6);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL175 objPRL175 = new PRL175();
            objPRL175 = db.PRL175.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL175 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL175.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL176> lstPRL176 = db.PRL176.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<PRL177> lstPRL177 = db.PRL177.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<PRL178> lstPRL178 = db.PRL178.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<PRL179> lstPRL179 = db.PRL179.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<PRL179_2> lstPRL179_2 = db.PRL179_2.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<PRL179_3> lstPRL179_3 = db.PRL179_3.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<PRL179_4> lstPRL179_4 = db.PRL179_4.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<SP_IPI_PROTOCOL036_LINKAGE_GET_LINES8_DATA_Result> lstPRL179_5 = db.SP_IPI_PROTOCOL036_LINKAGE_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL175.HeaderId).ToList();
            List<SP_IPI_PROTOCOL036_LINKAGE_GET_LINES9_DATA_Result> lstPRL179_6 = db.SP_IPI_PROTOCOL036_LINKAGE_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL175.HeaderId).ToList();

            ViewBag.lstPRL176 = lstPRL176;
            ViewBag.lstPRL177 = lstPRL177;
            ViewBag.lstPRL178 = lstPRL178;
            ViewBag.lstPRL179 = lstPRL179;
            ViewBag.lstPRL179_2 = lstPRL179_2;
            ViewBag.lstPRL179_3 = lstPRL179_3;
            ViewBag.lstPRL179_4 = lstPRL179_4;
            ViewBag.lstPRL179_5 = lstPRL179_5;
            ViewBag.lstPRL179_6 = lstPRL179_6;
            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL175.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL175.ActFilledBy) && (objPRL175.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL175.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL175);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL175 objPRL175 = new PRL175();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL175 = db.PRL175.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL175).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL175 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL175.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;


            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL176> lstPRL176 = db.PRL176.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<PRL177> lstPRL177 = db.PRL177.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<PRL178> lstPRL178 = db.PRL178.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<PRL179> lstPRL179 = db.PRL179.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<PRL179_2> lstPRL179_2 = db.PRL179_2.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<PRL179_3> lstPRL179_3 = db.PRL179_3.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<PRL179_4> lstPRL179_4 = db.PRL179_4.Where(x => x.HeaderId == objPRL175.HeaderId).ToList();
            List<SP_IPI_PROTOCOL036_LINKAGE_GET_LINES8_DATA_Result> lstPRL179_5 = db.SP_IPI_PROTOCOL036_LINKAGE_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL175.HeaderId).ToList();
            List<SP_IPI_PROTOCOL036_LINKAGE_GET_LINES9_DATA_Result> lstPRL179_6 = db.SP_IPI_PROTOCOL036_LINKAGE_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL175.HeaderId).ToList();


            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;

                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL175.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL175.ActFilledBy) && (objPRL175.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL175.Project).ToList();
                            //DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { isEditable = false; }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    OutsideInside = OutsideInside,
                    AccessRole = AccessRole,
                    LeftRight = LeftRight,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                   
                    objPRL175 = objPRL175,

                    lstPRL176 = lstPRL176,
                    lstPRL177 = lstPRL177,
                    lstPRL178 = lstPRL178,
                    lstPRL179 = lstPRL179,
                    lstPRL179_2 = lstPRL179_2,
                    lstPRL179_3 = lstPRL179_3,
                    lstPRL179_4 = lstPRL179_4,
                    lstPRL179_5 = lstPRL179_5,
                    lstPRL179_6 = lstPRL179_6

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL175 prl175, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl175.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL175 objPRL175 = db.PRL175.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL175.DrawingRevisionNo = prl175.DrawingRevisionNo;
                    objPRL175.DrawingNo = prl175.DrawingNo;
                    objPRL175.DCRNo = prl175.DCRNo;
                    objPRL175.ProtocolNo = prl175.ProtocolNo != null ? prl175.ProtocolNo : "";
                    objPRL175.ReqShellThickness = prl175.ReqShellThickness;
                    objPRL175.ActShellThickness1 = prl175.ActShellThickness1;
                    objPRL175.ActShellThickness2 = prl175.ActShellThickness2;
                    objPRL175.ActShellThickness3 = prl175.ActShellThickness3;
                    objPRL175.ActShellThickness4 = prl175.ActShellThickness4;
                    objPRL175.ActShellThickness5 = prl175.ActShellThickness5;
                    objPRL175.ActShellThickness6 = prl175.ActShellThickness6;
                    objPRL175.ActShellThickness7 = prl175.ActShellThickness7;
                    objPRL175.ActShellThickness8 = prl175.ActShellThickness8;
                    objPRL175.ActShellThickness9 = prl175.ActShellThickness9;
                    objPRL175.ReqWEPDimensionRootGap = prl175.ReqWEPDimensionRootGap;
                    objPRL175.ReqWEPDimensionRootFace = prl175.ReqWEPDimensionRootFace;
                    objPRL175.ReqWEPDimensionInsideWEPAngle = prl175.ReqWEPDimensionInsideWEPAngle;
                    objPRL175.ReqWEPDimensionOutsideWEPAngle = prl175.ReqWEPDimensionOutsideWEPAngle;
                    objPRL175.ReqWEPDimensionOffset = prl175.ReqWEPDimensionOffset;
                    objPRL175.CircumferenceMeasurementReqIDCF = prl175.CircumferenceMeasurementReqIDCF;
                    objPRL175.CircumferenceMeasurementReqODCF = prl175.CircumferenceMeasurementReqODCF;
                    objPRL175.ReqTotalShellHeight = prl175.ReqTotalShellHeight;
                    objPRL175.OutOfRoundnessReqID = prl175.OutOfRoundnessReqID;
                    objPRL175.OutOfRoundnessOutBy = prl175.OutOfRoundnessOutBy;
                    objPRL175.ProfileMeasurementAllowed = prl175.ProfileMeasurementAllowed;
                    objPRL175.ExternalPressureVessel = prl175.ExternalPressureVessel;
                    objPRL175.PressureVesselReqChordLength = prl175.PressureVesselReqChordLength;
                    objPRL175.PressureVesselActChordLength = prl175.PressureVesselActChordLength;
                    objPRL175.PressureVesselReqRadius = prl175.PressureVesselReqRadius;
                    objPRL175.PressureVesselActRadius = prl175.PressureVesselActRadius;
                    objPRL175.PressureVesselAllowableGap = prl175.PressureVesselAllowableGap;
                    objPRL175.PressureVesselActGap = prl175.PressureVesselActGap;
                    objPRL175.AdjacentLongSeam = prl175.AdjacentLongSeam;
                    objPRL175.CheckPoint1 = prl175.CheckPoint1;
                    objPRL175.CheckPoint2 = prl175.CheckPoint2;
                    objPRL175.CheckPoint3 = prl175.CheckPoint3;
                    objPRL175.CheckPoint4 = prl175.CheckPoint4;
                    objPRL175.CheckPoint5 = prl175.CheckPoint5;
                    objPRL175.CheckPoint6 = prl175.CheckPoint6;
                    objPRL175.CheckPoint7 = prl175.CheckPoint7;
                    objPRL175.CheckPoint8 = prl175.CheckPoint8;
                    objPRL175.CheckPoint10 = prl175.CheckPoint10;
                    objPRL175.CheckPoint10_2 = prl175.CheckPoint10_2;
                    objPRL175.CheckPoint11 = prl175.CheckPoint11;
                    objPRL175.CheckPoint11_2 = prl175.CheckPoint11_2;
                    objPRL175.CheckPoint11_3 = prl175.CheckPoint11_3;
                    objPRL175.CheckPoint12 = prl175.CheckPoint12;
                    objPRL175.QCRemarks = prl175.QCRemarks;
                    objPRL175.Result = prl175.Result;
                    objPRL175.Note1 = prl175.Note1;
                    objPRL175.Note2 = prl175.Note2;
                    objPRL175.Note3 = prl175.Note3;
                    objPRL175.ReqDepth = prl175.ReqDepth;
                    objPRL175.ReqOffset = prl175.ReqOffset;
                    objPRL175.ReqWidth = prl175.ReqWidth;
                    objPRL175.IdentificationOfCleats = prl175.IdentificationOfCleats;
                    objPRL175.EditedBy = UserName;
                    objPRL175.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL175.ActFilledBy = UserName;
                            objPRL175.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL175.ReqFilledBy = UserName;
                            objPRL175.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL175.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL175 objPRL175 = db.PRL175.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL175 != null && string.IsNullOrWhiteSpace(objPRL175.ProtocolNo))
                    {
                        db.PRL175.Remove(objPRL175);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL175.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL176> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL176> lstAddPRL176 = new List<PRL176>();
                List<PRL176> lstDeletePRL176 = new List<PRL176>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL176 obj = db.PRL176.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL176 obj = new PRL176();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL176.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL176.Count > 0)
                {
                    db.PRL176.AddRange(lstAddPRL176);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL176 = db.PRL176.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL176 = db.PRL176.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL176.Count > 0)
                {
                    db.PRL176.RemoveRange(lstDeletePRL176);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL177> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL177> lstAddPRL177 = new List<PRL177>();
                List<PRL177> lstDeletePRL177 = new List<PRL177>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL177 obj = db.PRL177.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL177();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ActMaxRootGap = item.ActMaxRootGap;
                        obj.ActMinRootGap = item.ActMinRootGap;
                        obj.ActMaxRootFace = item.ActMaxRootFace;
                        obj.ActMinRootFace = item.ActMinRootFace;
                        obj.ActMaxInsideWEBAngle = item.ActMaxInsideWEBAngle;
                        obj.ActMinInsideWEBAngle = item.ActMinInsideWEBAngle;
                        obj.ActMaxOutsideWEBAngle = item.ActMaxOutsideWEBAngle;
                        obj.ActMinOutsideWEBAngle = item.ActMinOutsideWEBAngle;
                        obj.ActMaxOffset = item.ActMaxOffset;
                        obj.ActMinOffset = item.ActMinOffset;

                        if (isAdded)
                        {
                            lstAddPRL177.Add(obj);
                        }
                    }
                }
                if (lstAddPRL177.Count > 0)
                {
                    db.PRL177.AddRange(lstAddPRL177);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL177 = db.PRL177.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL177 = db.PRL177.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL177.Count > 0)
                {
                    db.PRL177.RemoveRange(lstDeletePRL177);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL178> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL178> lstAddPRL178 = new List<PRL178>();
                List<PRL178> lstDeletePRL178 = new List<PRL178>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL178 obj = db.PRL178.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL178();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.ActIDCF = item.ActIDCF;
                            obj.ActODCF = item.ActODCF;
                        }
                        if (isAdded)
                        {
                            lstAddPRL178.Add(obj);
                        }
                    }
                    if (lstAddPRL178.Count > 0)
                    {
                        db.PRL178.AddRange(lstAddPRL178);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL178 = db.PRL178.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL178 = db.PRL178.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL178.Count > 0)
                {
                    db.PRL178.RemoveRange(lstDeletePRL178);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL179> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL179> lstAddPRL179 = new List<PRL179>();
                List<PRL179> lstDeletePRL179 = new List<PRL179>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL179 obj = db.PRL179.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL179();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActValue = item.ActValue;
                        if (isAdded)
                        {
                            lstAddPRL179.Add(obj);
                        }
                    }

                    if (lstAddPRL179.Count > 0)
                    {
                        db.PRL179.AddRange(lstAddPRL179);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL179 = db.PRL179.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL179 = db.PRL179.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL179.Count > 0)
                {
                    db.PRL179.RemoveRange(lstDeletePRL179);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL179_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL179_2> lstAddPRL179_2 = new List<PRL179_2>();
                List<PRL179_2> lstDeletePRL179_2 = new List<PRL179_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL179_2 obj = db.PRL179_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL179_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Orientation = item.Orientation;
                        obj.ActAtTop = item.ActAtTop;
                        obj.ActAtBottom = item.ActAtBottom;

                        if (isAdded)
                        {
                            lstAddPRL179_2.Add(obj);
                        }
                    }

                    if (lstAddPRL179_2.Count > 0)
                    {
                        db.PRL179_2.AddRange(lstAddPRL179_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL179_2 = db.PRL179_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL179_2 = db.PRL179_2.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL179_2.Count > 0)
                {
                    db.PRL179_2.RemoveRange(lstDeletePRL179_2);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6Linkage(List<PRL179_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL179_3> lstAddPRL179_3 = new List<PRL179_3>();
                List<PRL179_3> lstDeletePRL179_3 = new List<PRL179_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL179_3 obj = db.PRL179_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL179_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.SeamNo = item.SeamNo;
                        obj.ActTopPeakIn = item.ActTopPeakIn;
                        obj.ActTopPeakOut = item.ActTopPeakOut;
                        obj.ActMidPeakIn = item.ActMidPeakIn;
                        obj.ActMidPeakOut = item.ActMidPeakOut;
                        obj.ActBottomPeakIn = item.ActBottomPeakIn;
                        obj.ActBottomPeakOut = item.ActBottomPeakOut;
                        if (isAdded)
                        {
                            lstAddPRL179_3.Add(obj);
                        }
                    }

                    if (lstAddPRL179_3.Count > 0)
                    {
                        db.PRL179_3.AddRange(lstAddPRL179_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL179_3 = db.PRL179_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL179_3 = db.PRL179_3.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL179_3.Count > 0)
                {
                    db.PRL179_3.RemoveRange(lstDeletePRL179_3);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7Linkage(List<PRL179_4> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL179_4> lstAddPRL179_4 = new List<PRL179_4>();
                List<PRL179_4> lstDeletePRL179_4 = new List<PRL179_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL179_4 obj = db.PRL179_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL179_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.SeamNo = item.SeamNo;
                        obj.ReqValue = item.ReqValue;
                        obj.ActValue = item.ActValue;
                        if (isAdded)
                        {
                            lstAddPRL179_4.Add(obj);
                        }
                    }

                    if (lstAddPRL179_4.Count > 0)
                    {
                        db.PRL179_4.AddRange(lstAddPRL179_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL179_4 = db.PRL179_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL179_4 = db.PRL179_4.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL179_4.Count > 0)
                {
                    db.PRL179_4.RemoveRange(lstDeletePRL179_4);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details

        [HttpPost]
        public JsonResult SaveProtocolLine8Linkage(List<PRL179_5> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL179_5> lstAddPRL179_5 = new List<PRL179_5>();
                List<PRL179_5> lstDeletePRL179_5 = new List<PRL179_5>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL179_5 obj = db.PRL179_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL179_5 objFecthSeam = new PRL179_5();
                        if (obj == null)
                        {
                            obj = new PRL179_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL179_5.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            obj.ReqDepth = objFecthSeam.ReqDepth;
                            obj.ReqWidth = objFecthSeam.ReqWidth;
                            obj.ReqOffset = objFecthSeam.ReqOffset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            obj.ReqDepth = item.ReqDepth;
                            obj.ReqWidth = item.ReqWidth;
                            obj.ReqOffset = item.ReqOffset;
                        }
                        obj.ActDepth = item.ActDepth;
                        obj.ActWidth = item.ActWidth;
                        obj.ActOffset = item.ActOffset;
                        obj.SpotNo = item.SpotNo;
                        if (isAdded)
                        {
                            lstAddPRL179_5.Add(obj);
                        }
                    }
                    if (lstAddPRL179_5.Count > 0)
                    {
                        db.PRL179_5.AddRange(lstAddPRL179_5);
                    }
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL179_5 = db.PRL179_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL179_5 = db.PRL179_5.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL179_5.Count > 0)
                {
                    db.PRL179_5.RemoveRange(lstDeletePRL179_5);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 details

        [HttpPost]
        public JsonResult SaveProtocolLine9Linkage(List<PRL179_6> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL179_6> lstAddPRL179_6 = new List<PRL179_6>();
                List<PRL179_6> lstDeletePRL179_6 = new List<PRL179_6>();
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL179_6 obj = db.PRL179_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL179_6 objFecthSeam = new PRL179_6();
                        if (obj == null)
                        {
                            obj = new PRL179_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL179_6.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        obj.CleatNo = item.CleatNo;
                        obj.ParentMetalSize = item.ParentMetalSize;
                        obj.Location = item.Location;
                        obj.OutsideInside = item.OutsideInside;
                        obj.Distance = item.Distance;
                        obj.ShellEvelution = item.ShellEvelution;
                        if (isAdded)
                        {
                            lstAddPRL179_6.Add(obj);
                        }
                    }
                    if (lstAddPRL179_6.Count > 0)
                    {
                        db.PRL179_6.AddRange(lstAddPRL179_6);
                    }
                    lstDeletePRL179_6 = db.PRL179_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL179_6 = db.PRL179_6.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL179_6.Count > 0)
                {
                    db.PRL179_6.RemoveRange(lstDeletePRL179_6);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion
    }
}