﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol026Controller : clsBase
    {
        // GET: PROTOCOL/Protocol021
        string ControllerURL = "/PROTOCOL/Protocol026/";
        string Title = "REPORT FOR N2 FILLING INSPECTION";
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id,int? m)
        {
            PRO125 objPRO125 = new PRO125();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            if (!id.HasValue)
            {
                objPRO125.ProtocolNo = string.Empty;
                objPRO125.CreatedBy = objClsLoginInfo.UserName;
                objPRO125.CreatedOn = DateTime.Now;
                db.PRO125.Add(objPRO125);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO125.HeaderId;
            }
            else
            {
                objPRO125 = db.PRO125.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO125);

        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO125 PRO125)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO125.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO125 objPRO125 = db.PRO125.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO125.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO125.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO125.ProtocolNo = PRO125.ProtocolNo;
                        objPRO125.InspectionCertificateNo = PRO125.InspectionCertificateNo;
                        objPRO125.RequireN2Pressure = PRO125.RequireN2Pressure;
                        objPRO125.ActualN2Pressure = PRO125.ActualN2Pressure;
                        objPRO125.RequireO2Level = PRO125.RequireO2Level;
                        objPRO125.ActualO2Level = PRO125.ActualO2Level;
                        objPRO125.Remark1 = PRO125.Remark1;
                        objPRO125.Remark2 = PRO125.Remark2;
                        objPRO125.Remark3 = PRO125.Remark3;
                        objPRO125.Remark4 = PRO125.Remark4;
                        objPRO125.Remark5 = PRO125.Remark5;

                        objPRO125.EditedBy = objClsLoginInfo.UserName;
                        objPRO125.EditedOn = DateTime.Now;


                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO125.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId > 0)
                {
                    PRO125 objPRO125 = db.PRO125.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO125 != null && string.IsNullOrWhiteSpace(objPRO125.ProtocolNo))
                    {
                        

                        db.PRO125.Remove(objPRO125);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.REPORT_FOR_N2_FILLING_INSPECTION.GetStringValue(), refHeaderId);
                        
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO125.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Linkage Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Linkage(int? id, int? m)
        {//PRL125 
            PRL125 objPRL125 = new PRL125();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();


            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;


            objPRL125 = db.PRL125.Where(x => x.HeaderId == id).FirstOrDefault();
            if (objPRL125  != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL125.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL125);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL125 PRL125)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL125.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL125 objPRL125 = db.PRL125.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL125.DCRNo = PRL125.DCRNo;
                    objPRL125.ProtocolNo = PRL125.ProtocolNo != null ? PRL125.ProtocolNo : "";

                    objPRL125.EquipmentNo = PRL125.EquipmentNo;
                    objPRL125.DrawingNo = PRL125.DrawingNo;
                    objPRL125.DrawingRevisionNo = PRL125.DrawingRevisionNo;
                    objPRL125.DCRNo = PRL125.DCRNo;
                    objPRL125.InspectionAgency = PRL125.InspectionAgency;
                    objPRL125.ManufacturingCode = PRL125.ManufacturingCode;
                    
                    objPRL125.InspectionDate = PRL125.InspectionDate;
                    objPRL125.Project = PRL125.Project;
                    objPRL125.InspectionCertificateNo = PRL125.InspectionCertificateNo;
                    objPRL125.RequireN2Pressure = PRL125.RequireN2Pressure;
                    objPRL125.ActualN2Pressure = PRL125.ActualN2Pressure;
                    objPRL125.RequireO2Level = PRL125.RequireO2Level;
                    objPRL125.ActualO2Level = PRL125.ActualO2Level;
                    objPRL125.Remark1 = PRL125.Remark1;
                    objPRL125.Remark2 = PRL125.Remark2;
                    objPRL125.Remark3 = PRL125.Remark3;
                    objPRL125.Remark4 = PRL125.Remark4;
                    objPRL125.Remark5 = PRL125.Remark5;

                    objPRL125.EditedBy = objClsLoginInfo.UserName;
                    objPRL125.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL125.HeaderId;

                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion


    }
}