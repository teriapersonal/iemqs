﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol068Controller : clsBase
    {
        // GET: PROTOCOL/Protocol068
        string ControllerURL = "/PROTOCOL/Protocol068/";
        string Title = "AIR TEST REPORT OF REINFORCEMENT PADS COVERING WELD SEAMS";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO335 objPRO335 = new PRO335();
            if (!id.HasValue)
            {
                try
                {
                    objPRO335.ProtocolNo = string.Empty;
                    objPRO335.CreatedBy = objClsLoginInfo.UserName;
                    objPRO335.CreatedOn = DateTime.Now;

                    #region Notes

                    objPRO335.Notes = " 1.AIR TEST OF REINFORCEMENT PAD CHECKED AND FOUND SATISFACTORY. \r\n 2.COMPLETE WELD JOINTS EXAMINED WITH SOAP SOLUTION TO ENSURE NO LEAKAGE.";

                    #endregion

                    db.PRO335.Add(objPRO335);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO335.HeaderId;
            }
            else
            {
                objPRO335 = db.PRO335.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstStage = new List<string> { "BEFORE PWHT", "AFTER PWHT", "BEFORE HYDRO", "AFTER HYDRO", "AFTER COMPLETE WELD", "AFTER PARTIAL WELD", "AFTER ROOT RUN WELDING" };

            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.Stage = lstStage.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO336> lstPRO336 = db.PRO336.Where(x => x.HeaderId == objPRO335.HeaderId).ToList();
            List<PRO337> lstPRO337 = db.PRO337.Where(x => x.HeaderId == objPRO335.HeaderId).ToList();

            ViewBag.lstPRO336 = lstPRO336;
            ViewBag.lstPRO337 = lstPRO337;

            #endregion

            return View(objPRO335);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO335 PRO335)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO335.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO335 objPRO335 = db.PRO335.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO335.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO335.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO335.ProtocolNo = PRO335.ProtocolNo;
                        objPRO335.Stage = PRO335.Stage;
                        objPRO335.PressureGaugeDetails = PRO335.PressureGaugeDetails;
                        objPRO335.Notes = PRO335.Notes;
                        objPRO335.Result = PRO335.Result;

                        objPRO335.EditedBy = objClsLoginInfo.UserName;
                        objPRO335.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO335.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO335 objPRO335 = db.PRO335.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO335 != null && string.IsNullOrWhiteSpace(objPRO335.ProtocolNo))
                    {
                        db.PRO335.Remove(objPRO335);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO335.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO336> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO336> lstAddPRO336 = new List<PRO336>();
                List<PRO336> lstDeletePRO336 = new List<PRO336>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO336 obj = db.PRO336.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO336();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.SeamNoOfCoveredWeldSeam = item.SeamNoOfCoveredWeldSeam;
                        obj.RequiredPressure = item.RequiredPressure;
                        obj.ActualPressure = item.ActualPressure;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO336.Add(obj);
                        }
                    }
                    if (lstAddPRO336.Count > 0)
                    {
                        db.PRO336.AddRange(lstAddPRO336);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO336 = db.PRO336.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO336.Count > 0)
                    {
                        db.PRO336.RemoveRange(lstDeletePRO336);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO336 = db.PRO336.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO336.Count > 0)
                    {
                        db.PRO336.RemoveRange(lstDeletePRO336);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO337> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO337> lstAddPRO337 = new List<PRO337>();
                List<PRO337> lstDeletePRO337 = new List<PRO337>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO337 obj = db.PRO337.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO337();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.GaugeSerialNumber = item.GaugeSerialNumber;
                        obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;

                        if (isAdded)
                        {
                            lstAddPRO337.Add(obj);
                        }
                    }
                    if (lstAddPRO337.Count > 0)
                    {
                        db.PRO337.AddRange(lstAddPRO337);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO337 = db.PRO337.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO337.Count > 0)
                    {
                        db.PRO337.RemoveRange(lstDeletePRO337);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO337 = db.PRO337.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO337.Count > 0)
                    {
                        db.PRO337.RemoveRange(lstDeletePRO337);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL335 objPRL335 = new PRL335();
            objPRL335 = db.PRL335.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL335 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL335.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstStage = new List<string> { "BEFORE PWHT", "AFTER PWHT", "BEFORE HYDRO", "AFTER HYDRO", "AFTER COMPLETE WELD", "AFTER PARTIAL WELD", "AFTER ROOT RUN WELDING" };

            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.Stage = lstStage.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL336> lstPRL336 = db.PRL336.Where(x => x.HeaderId == objPRL335.HeaderId).ToList();
            List<PRL337> lstPRL337 = db.PRL337.Where(x => x.HeaderId == objPRL335.HeaderId).ToList();

            ViewBag.lstPRL336 = lstPRL336;
            ViewBag.lstPRL337 = lstPRL337;

            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL335.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL335.ActFilledBy) && (objPRL335.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL335.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL335);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL335 objPRL335 = new PRL335();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL335 = db.PRL335.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL335).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL335 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL335.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
            }
            
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;
            
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstStage = new List<string> { "BEFORE PWHT", "AFTER PWHT", "BEFORE HYDRO", "AFTER HYDRO", "AFTER COMPLETE WELD", "AFTER PARTIAL WELD", "AFTER ROOT RUN WELDING" };

            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var Stage = lstStage.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL336> lstPRL336 = db.PRL336.Where(x => x.HeaderId == objPRL335.HeaderId).ToList();
            List<PRL337> lstPRL337 = db.PRL337.Where(x => x.HeaderId == objPRL335.HeaderId).ToList();


            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL335.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL335.ActFilledBy) && (objPRL335.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL335.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    Stage = Stage,
                    YesNAEnum = YesNAEnum,

                    objPRL335 = objPRL335,

                    lstPRL336 = lstPRL336,
                    lstPRL337 = lstPRL337

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL335 PRL335, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL335.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL335 objPRL335 = db.PRL335.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL335.DrawingRevisionNo = PRL335.DrawingRevisionNo;
                    objPRL335.DrawingNo = PRL335.DrawingNo;
                    objPRL335.DCRNo = PRL335.DCRNo;
                    objPRL335.ProtocolNo = PRL335.ProtocolNo != null ? PRL335.ProtocolNo : "";
                    objPRL335.Stage = PRL335.Stage;
                    objPRL335.PressureGaugeDetails = PRL335.PressureGaugeDetails;
                    objPRL335.Notes = PRL335.Notes;
                    objPRL335.Result = PRL335.Result;

                    objPRL335.EditedBy = UserName;
                    objPRL335.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL335.ActFilledBy = UserName;
                            objPRL335.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL335.ReqFilledBy = UserName;
                            objPRL335.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL335.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL335 objPRL335 = db.PRL335.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL335 != null && string.IsNullOrWhiteSpace(objPRL335.ProtocolNo))
                    {
                        db.PRL335.Remove(objPRL335);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL335.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL336> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL336> lstAddPRL336 = new List<PRL336>();
                List<PRL336> lstDeletePRL336 = new List<PRL336>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL336 obj = db.PRL336.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL336();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.SeamNoOfCoveredWeldSeam = item.SeamNoOfCoveredWeldSeam;
                        obj.RequiredPressure = item.RequiredPressure;
                        obj.ActualPressure = item.ActualPressure;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL336.Add(obj);
                        }
                    }
                    if (lstAddPRL336.Count > 0)
                    {
                        db.PRL336.AddRange(lstAddPRL336);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL336 = db.PRL336.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL336.Count > 0)
                    {
                        db.PRL336.RemoveRange(lstDeletePRL336);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL336 = db.PRL336.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL336.Count > 0)
                    {
                        db.PRL336.RemoveRange(lstDeletePRL336);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL337> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL337> lstAddPRL337 = new List<PRL337>();
                List<PRL337> lstDeletePRL337 = new List<PRL337>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL337 obj = db.PRL337.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL337();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.GaugeSerialNumber = item.GaugeSerialNumber;
                        obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;

                        if (isAdded)
                        {
                            lstAddPRL337.Add(obj);
                        }
                    }
                    if (lstAddPRL337.Count > 0)
                    {
                        db.PRL337.AddRange(lstAddPRL337);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL337 = db.PRL337.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL337.Count > 0)
                    {
                        db.PRL337.RemoveRange(lstDeletePRL337);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL337 = db.PRL337.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL337.Count > 0)
                    {
                        db.PRL337.RemoveRange(lstDeletePRL337);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}