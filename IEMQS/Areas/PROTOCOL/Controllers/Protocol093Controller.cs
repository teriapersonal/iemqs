﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol093Controller : clsBase
    {
        // GET: PROTOCOL/Protocol093
        string ControllerURL = "/PROTOCOL/Protocol093/";
        string Title = "REPORT FOR SCREW PLUG ACME HEADER MACHINING INSPECTION";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO460 objPRO460 = new PRO460();
            if (!id.HasValue)
            {
                try
                {
                    objPRO460.ProtocolNo = string.Empty;
                    objPRO460.CreatedBy = objClsLoginInfo.UserName;
                    objPRO460.CreatedOn = DateTime.Now;

                    #region  FLANGE & ID MACHINING  | PRO462
                    objPRO460.PRO462.Add(new PRO462
                    {
                        SrNo = "1",
                        Description = "Identifiaction",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO462.Add(new PRO462
                    {
                        SrNo = "2",
                        Description = "Channel header ID",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO462.Add(new PRO462
                    {
                        SrNo = "3",
                        Description = "Groove OD",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO462.Add(new PRO462
                    {
                        SrNo = "4",
                        Description = "Groove ID",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO462.Add(new PRO462
                    {
                        SrNo = "5",
                        Description = "Taper",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO462.Add(new PRO462
                    {
                        SrNo = "6",
                        Description = "Radius",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO462.Add(new PRO462
                    {
                        SrNo = "7",
                        Description = "Surface finish in groove",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO462.Add(new PRO462
                    {
                        SrNo = "8",
                        Description = "ID Finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO462.Add(new PRO462
                    {
                        SrNo = "9",
                        Description = "Gasket fgroove step face finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO462.Add(new PRO462
                    {
                        SrNo = "10",
                        Description = "Gasket face to groove depth",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO462.Add(new PRO462
                    {
                        SrNo = "11",
                        Description = "Gasket face to Taper length",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO462.Add(new PRO462
                    {
                        SrNo = "12",
                        Description = "Open end to Gasket face height",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO462.Add(new PRO462
                    {
                        SrNo = "13",
                        Description = "Chanel header ID",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region ACME ID MACHINING | PRO463
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "1",
                        Description = "Identifiaction",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "2",
                        Description = "Front side relief dia.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "3",
                        Description = "Step ID depth",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "4",
                        Description = "Radius",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "5",
                        Description = "Back end relief",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "6",
                        Description = "Relief groove width",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "7",
                        Description = "Relief groove Distance from front face",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "8",
                        Description = "Groove both side radius",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "9",
                        Description = "Step ID",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "10",
                        Description = "Step ID  Distance from front face",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "11",
                        Description = "Front corner radius",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "12",
                        Description = "Step ID depth",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "13",
                        Description = "End coner radius",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "14",
                        Description = "Gasket groove depth",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "15",
                        Description = "Gasket groove ID",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "16",
                        Description = "Radius",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "17",
                        Description = "Gasket groove OD",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "18",
                        Description = "Dia.Ø  depth",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "19",
                        Description = "Finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "20",
                        Description = "Channel header open end face to serrated gasket",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "21",
                        Description = "ID",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "22",
                        Description = "ID Length",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "23",
                        Description = "Groove ID",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "24",
                        Description = "Dia.Ø width",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "25",
                        Description = "Finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "26",
                        Description = "Gasket face finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO463.Add(new PRO463
                    {
                        SrNo = "27",
                        Description = "Ref.OD up to mm",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region ACME THREADING | PRO464
                    objPRO460.PRO464.Add(new PRO464
                    {
                        SrNo = "1",
                        Description = "Threading pitch",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464.Add(new PRO464
                    {
                        SrNo = "2",
                        Description = "Angle 29° ACME CLASS 2G STD,ASME B1.5",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464.Add(new PRO464
                    {
                        SrNo = "3",
                        Description = "Threading depth from threading ID",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464.Add(new PRO464
                    {
                        SrNo = "4",
                        Description = "Minor dia.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464.Add(new PRO464
                    {
                        SrNo = "5",
                        Description = "Pitch circle dia.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464.Add(new PRO464
                    {
                        SrNo = "6",
                        Description = "Thread length",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464.Add(new PRO464
                    {
                        SrNo = "7",
                        Description = "Thread Go template",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464.Add(new PRO464
                    {
                        SrNo = "8",
                        Description = "Threading Not go template",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region TLR BRACKET | PRO464_2
                    objPRO460.PRO464_2.Add(new PRO464_2
                    {
                        SrNo = "1",
                        Description = "Bracket height from job center",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464_2.Add(new PRO464_2
                    {
                        SrNo = "2",
                        Description = "Bracket width",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464_2.Add(new PRO464_2
                    {
                        SrNo = "3",
                        Description = "Bracket both side finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464_2.Add(new PRO464_2
                    {
                        SrNo = "4",
                        Description = "Bracket Length",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464_2.Add(new PRO464_2
                    {
                        SrNo = "5",
                        Description = "Both side angle",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464_2.Add(new PRO464_2
                    {
                        SrNo = "6",
                        Description = "Channel header open end face to bracket face dis.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464_2.Add(new PRO464_2
                    {
                        SrNo = "7",
                        Description = "_Nos.  UNC X Deep , Drill hole deep (Part-)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464_2.Add(new PRO464_2
                    {
                        SrNo = "8",
                        Description = "PCD",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464_2.Add(new PRO464_2
                    {
                        SrNo = "9",
                        Description = "Nos. holes Go, No go",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464_2.Add(new PRO464_2
                    {
                        SrNo = "10",
                        Description = "Distance from front face od to item",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464_2.Add(new PRO464_2
                    {
                        SrNo = "11",
                        Description = "Between two holes pitch",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO460.PRO464_2.Add(new PRO464_2
                    {
                        SrNo = "12",
                        Description = "Between two holes pitch",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO460.Add(objPRO460);
                    db.SaveChanges();
                }                
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO460.HeaderId;
            }
            else
            {
                objPRO460 = db.PRO460.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO461> lstPRO461 = db.PRO461.Where(x => x.HeaderId == objPRO460.HeaderId).ToList();
            List<PRO462> lstPRO462 = db.PRO462.Where(x => x.HeaderId == objPRO460.HeaderId).ToList();
            List<PRO463> lstPRO463 = db.PRO463.Where(x => x.HeaderId == objPRO460.HeaderId).ToList();
            List<PRO464> lstPRO464 = db.PRO464.Where(x => x.HeaderId == objPRO460.HeaderId).ToList();
            List<PRO464_2> lstPRO464_2 = db.PRO464_2.Where(x => x.HeaderId == objPRO460.HeaderId).ToList();

            ViewBag._lstPRO461 = lstPRO461;
            ViewBag._lstPRO462 = lstPRO462;
            ViewBag._lstPRO463 = lstPRO463;
            ViewBag._lstPRO464 = lstPRO464;
            ViewBag._lstPRO464_2 = lstPRO464_2;

            #endregion

            return View(objPRO460);
            //return View();
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO460 PRO460)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO460.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO460 objPRO460 = db.PRO460.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO460.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO460.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO460.ProtocolNo = PRO460.ProtocolNo;
                        objPRO460.PartDescription = PRO460.PartDescription;
                        objPRO460.ItemNo = PRO460.ItemNo;

                        objPRO460.CheckPoint1 = PRO460.CheckPoint1;
                        objPRO460.CheckPoint2 = PRO460.CheckPoint2;
                        objPRO460.CheckPoint4_1_1 = PRO460.CheckPoint4_1_1;
                        objPRO460.CheckPoint4_1_2 = PRO460.CheckPoint4_1_2;
                        objPRO460.CheckPoint4_2_1 = PRO460.CheckPoint4_2_1;
                        objPRO460.CheckPoint4_2_2 = PRO460.CheckPoint4_2_2;
                        objPRO460.CheckPoint4_3_1 = PRO460.CheckPoint4_3_1;
                        objPRO460.CheckPoint4_3_2 = PRO460.CheckPoint4_3_2;
                        objPRO460.CheckPoint4_4_1 = PRO460.CheckPoint4_4_1;
                        objPRO460.CheckPoint4_4_2 = PRO460.CheckPoint4_4_2;
                        objPRO460.CheckPoint4_5_1 = PRO460.CheckPoint4_5_1;
                        objPRO460.CheckPoint4_5_2 = PRO460.CheckPoint4_5_2;

                        objPRO460.QCRemarks = PRO460.QCRemarks;
                        objPRO460.Result = PRO460.Result;

                        objPRO460.EditedBy = objClsLoginInfo.UserName;
                        objPRO460.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO460.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO460 objPRO460 = db.PRO460.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO460 != null && string.IsNullOrWhiteSpace(objPRO460.ProtocolNo))
                    {
                        db.PRO460.Remove(objPRO460);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO460.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO461> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO461> lstAddPRO461 = new List<PRO461>();
                List<PRO461> lstDeletePRO461 = new List<PRO461>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO461 obj = db.PRO461.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO461 obj = new PRO461();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO461.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO461.Count > 0)
                    {
                        db.PRO461.AddRange(lstAddPRO461);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO461 = db.PRO461.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO461.Count > 0)
                    {
                        db.PRO461.RemoveRange(lstDeletePRO461);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO461 = db.PRO461.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO461.Count > 0)
                    {
                        db.PRO461.RemoveRange(lstDeletePRO461);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO462> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO462> lstAddPRO462 = new List<PRO462>();
                List<PRO462> lstDeletePRO462 = new List<PRO462>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO462 obj = db.PRO462.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO462();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO462.Add(obj);
                        }
                    }
                    if (lstAddPRO462.Count > 0)
                    {
                        db.PRO462.AddRange(lstAddPRO462);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO462 = db.PRO462.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO462.Count > 0)
                    {
                        db.PRO462.RemoveRange(lstDeletePRO462);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO462 = db.PRO462.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO462.Count > 0)
                    {
                        db.PRO462.RemoveRange(lstDeletePRO462);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO463> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO463> lstAddPRO463 = new List<PRO463>();
                List<PRO463> lstDeletePRO463 = new List<PRO463>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO463 obj = db.PRO463.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO463();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO463.Add(obj);
                        }
                    }
                    if (lstAddPRO463.Count > 0)
                    {
                        db.PRO463.AddRange(lstAddPRO463);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO463 = db.PRO463.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO463.Count > 0)
                    {
                        db.PRO463.RemoveRange(lstDeletePRO463);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO463 = db.PRO463.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO463.Count > 0)
                    {
                        db.PRO463.RemoveRange(lstDeletePRO463);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO464> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO464> lstAddPRO464 = new List<PRO464>();
                List<PRO464> lstDeletePRO464 = new List<PRO464>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO464 obj = db.PRO464.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO464();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO464.Add(obj);
                        }
                    }
                    if (lstAddPRO464.Count > 0)
                    {
                        db.PRO464.AddRange(lstAddPRO464);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO464 = db.PRO464.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO464.Count > 0)
                    {
                        db.PRO464.RemoveRange(lstDeletePRO464);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO464 = db.PRO464.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO464.Count > 0)
                    {
                        db.PRO464.RemoveRange(lstDeletePRO464);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO464_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO464_2> lstAddPRO464_2 = new List<PRO464_2>();
                List<PRO464_2> lstDeletePRO464_2 = new List<PRO464_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO464_2 obj = db.PRO464_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO464_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO464_2.Add(obj);
                        }
                    }
                    if (lstAddPRO464_2.Count > 0)
                    {
                        db.PRO464_2.AddRange(lstAddPRO464_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO464_2 = db.PRO464_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO464_2.Count > 0)
                    {
                        db.PRO464_2.RemoveRange(lstDeletePRO464_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO464_2 = db.PRO464_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO464_2.Count > 0)
                    {
                        db.PRO464_2.RemoveRange(lstDeletePRO464_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL460 objPRL460 = new PRL460();
            objPRL460 = db.PRL460.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL460 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL460.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL461> lstPRL461 = db.PRL461.Where(x => x.HeaderId == objPRL460.HeaderId).ToList();
            List<PRL462> lstPRL462 = db.PRL462.Where(x => x.HeaderId == objPRL460.HeaderId).ToList();
            List<PRL463> lstPRL463 = db.PRL463.Where(x => x.HeaderId == objPRL460.HeaderId).ToList();
            List<PRL464> lstPRL464 = db.PRL464.Where(x => x.HeaderId == objPRL460.HeaderId).ToList();
            List<PRL464_2> lstPRL464_2 = db.PRL464_2.Where(x => x.HeaderId == objPRL460.HeaderId).ToList();

            ViewBag._lstPRL461 = lstPRL461;
            ViewBag._lstPRL462 = lstPRL462;
            ViewBag._lstPRL463 = lstPRL463;
            ViewBag._lstPRL464 = lstPRL464;
            ViewBag._lstPRL464_2 = lstPRL464_2;
            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL460.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL460.ActFilledBy) && (objPRL460.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL460.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL460);

            //return View();
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL460 prl460, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl460.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL460 objPRL460 = db.PRL460.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL460.DrawingRevisionNo = prl460.DrawingRevisionNo;
                    objPRL460.DrawingNo = prl460.DrawingNo;
                    objPRL460.DCRNo = prl460.DCRNo;
                    objPRL460.ProtocolNo = prl460.ProtocolNo;

                    objPRL460.ProtocolNo = prl460.ProtocolNo;
                    objPRL460.PartDescription = prl460.PartDescription;
                    objPRL460.ItemNo = prl460.ItemNo;

                    objPRL460.CheckPoint1 = prl460.CheckPoint1;
                    objPRL460.CheckPoint2 = prl460.CheckPoint2;
                    objPRL460.CheckPoint4_1_1 = prl460.CheckPoint4_1_1;
                    objPRL460.CheckPoint4_1_2 = prl460.CheckPoint4_1_2;
                    objPRL460.CheckPoint4_2_1 = prl460.CheckPoint4_2_1;
                    objPRL460.CheckPoint4_2_2 = prl460.CheckPoint4_2_2;
                    objPRL460.CheckPoint4_3_1 = prl460.CheckPoint4_3_1;
                    objPRL460.CheckPoint4_3_2 = prl460.CheckPoint4_3_2;
                    objPRL460.CheckPoint4_4_1 = prl460.CheckPoint4_4_1;
                    objPRL460.CheckPoint4_4_2 = prl460.CheckPoint4_4_2;
                    objPRL460.CheckPoint4_5_1 = prl460.CheckPoint4_5_1;
                    objPRL460.CheckPoint4_5_2 = prl460.CheckPoint4_5_2;

                    objPRL460.QCRemarks = prl460.QCRemarks;
                    objPRL460.Result = prl460.Result;

                    objPRL460.EditedBy = UserName;
                    objPRL460.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL460.ActFilledBy = UserName;
                            objPRL460.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL460.ReqFilledBy = UserName;
                            objPRL460.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL460.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL460 objPRL460 = db.PRL460.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL460 != null && string.IsNullOrWhiteSpace(objPRL460.ProtocolNo))
                    {
                        db.PRL460.Remove(objPRL460);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL460.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL461> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL461> lstAddPRL461 = new List<PRL461>();
                List<PRL461> lstDeletePRL461 = new List<PRL461>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL461 obj = db.PRL461.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL461 obj = new PRL461();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL461.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL461.Count > 0)
                    {
                        db.PRL461.AddRange(lstAddPRL461);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL461 = db.PRL461.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL461.Count > 0)
                    {
                        db.PRL461.RemoveRange(lstDeletePRL461);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL461 = db.PRL461.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL461.Count > 0)
                    {
                        db.PRL461.RemoveRange(lstDeletePRL461);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL462> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL462> lstAddPRL462 = new List<PRL462>();
                List<PRL462> lstDeletePRL462 = new List<PRL462>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL462 obj = db.PRL462.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL462();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL462.Add(obj);
                        }
                    }
                    if (lstAddPRL462.Count > 0)
                    {
                        db.PRL462.AddRange(lstAddPRL462);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL462 = db.PRL462.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL462.Count > 0)
                    {
                        db.PRL462.RemoveRange(lstDeletePRL462);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL462 = db.PRL462.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL462.Count > 0)
                    {
                        db.PRL462.RemoveRange(lstDeletePRL462);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL463> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL463> lstAddPRL463 = new List<PRL463>();
                List<PRL463> lstDeletePRL463 = new List<PRL463>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL463 obj = db.PRL463.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL463();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL463.Add(obj);
                        }
                    }
                    if (lstAddPRL463.Count > 0)
                    {
                        db.PRL463.AddRange(lstAddPRL463);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL463 = db.PRL463.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL463.Count > 0)
                    {
                        db.PRL463.RemoveRange(lstDeletePRL463);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL463 = db.PRL463.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL463.Count > 0)
                    {
                        db.PRL463.RemoveRange(lstDeletePRL463);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL464> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL464> lstAddPRL464 = new List<PRL464>();
                List<PRL464> lstDeletePRL464 = new List<PRL464>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL464 obj = db.PRL464.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL464();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL464.Add(obj);
                        }
                    }
                    if (lstAddPRL464.Count > 0)
                    {
                        db.PRL464.AddRange(lstAddPRL464);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL464 = db.PRL464.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL464.Count > 0)
                    {
                        db.PRL464.RemoveRange(lstDeletePRL464);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL464 = db.PRL464.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL464.Count > 0)
                    {
                        db.PRL464.RemoveRange(lstDeletePRL464);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL464_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL464_2> lstAddPRL464_2 = new List<PRL464_2>();
                List<PRL464_2> lstDeletePRL464_2 = new List<PRL464_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL464_2 obj = db.PRL464_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL464_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL464_2.Add(obj);
                        }
                    }
                    if (lstAddPRL464_2.Count > 0)
                    {
                        db.PRL464_2.AddRange(lstAddPRL464_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL464_2 = db.PRL464_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL464_2.Count > 0)
                    {
                        db.PRL464_2.RemoveRange(lstDeletePRL464_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL464_2 = db.PRL464_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL464_2.Count > 0)
                    {
                        db.PRL464_2.RemoveRange(lstDeletePRL464_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}