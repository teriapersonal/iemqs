﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;
namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol056Controller : clsBase
    {
        //GET: PROTOCOL/Protocol056
        string ControllerURL = "/PROTOCOL/Protocol056/";
        string Title = "REPORT FOR VISUAL AND DIMENSION REPORT AFTER MACHINING OF GASKET FACE AND OTHER DIMENSIONS OF NOZZLE";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO275 objPRO275 = new PRO275();
            if (!id.HasValue)
            {
                try
                {
                    objPRO275.ProtocolNo = string.Empty;
                    objPRO275.CreatedBy = objClsLoginInfo.UserName;
                    objPRO275.CreatedOn = DateTime.Now;

                    #region  DESCRIPTION | PRO276
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 1,
                        Description = "Identification",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 2,
                        Description = "Visual inspection",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 3,
                        Description = "ID before O/L",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 4,
                        Description = "ID after O/L",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 5,
                        Description = "ID Surface finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 6,
                        Description = "Gasket face step OD",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 7,
                        Description = "Gasket height",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 8,
                        Description = "Gasket face finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 9,
                        Description = "Gasket step OD radius Top",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 10,
                        Description = "Gasket step OD radius Bottom",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 11,
                        Description = "Groove width",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 12,
                        Description = "Groove depth",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 13,
                        Description = "Groove corner radius",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 14,
                        Description = "Groove angle",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 15,
                        Description = "Groove taper finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 16,
                        Description = "Groove face finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 17,
                        Description = "Gasket groove P.C.D",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 18,
                        Description = "Radius Gasket face to ID",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 19,
                        Description = "Final Visual inspection after O/L",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO275.PRO276.Add(new PRO276
                    {
                        SrNo = 20,
                        Description = "O/L Thk.",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });

                    #endregion

                    db.PRO275.Add(objPRO275);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO275.HeaderId;
            }
            else
            {
                objPRO275 = db.PRO275.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO276> lstPRO276 = db.PRO276.Where(x => x.HeaderId == objPRO275.HeaderId).ToList();


            ViewBag.lstPRO276 = lstPRO276;

            #endregion

            return View(objPRO275);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO275 PRO275)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO275.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO275 objPRO275 = db.PRO275.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO275.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO275.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO275.ProtocolNo = PRO275.ProtocolNo;
                        objPRO275.NozzleNo = PRO275.NozzleNo;
                        objPRO275.GasketFaceRaisedFaceSeamNo = PRO275.GasketFaceRaisedFaceSeamNo;
                        objPRO275.PartDescription = PRO275.PartDescription;
                        objPRO275.GasketGrooveSeamNo = PRO275.GasketGrooveSeamNo;
                        objPRO275.ItemNo = PRO275.ItemNo;
                        objPRO275.NozzleBackFaceSeamNo = PRO275.NozzleBackFaceSeamNo;

                        objPRO275.NozzleIDSeamNo = PRO275.NozzleIDSeamNo;
                        objPRO275.NozzleBackFaceToIDRadiusSeamNo = PRO275.NozzleBackFaceToIDRadiusSeamNo;

                        objPRO275.CheckPoint1 = PRO275.CheckPoint1;
                        objPRO275.CheckPoint2 = PRO275.CheckPoint2;
                        objPRO275.CheckPoint4 = PRO275.CheckPoint4;
                        objPRO275.CheckPoint4_2 = PRO275.CheckPoint4_2;

                        objPRO275.QCRemarks = PRO275.QCRemarks;
                        objPRO275.Result = PRO275.Result;

                        objPRO275.EditedBy = objClsLoginInfo.UserName;
                        objPRO275.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO275.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO275 objPRO275 = db.PRO275.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO275 != null && string.IsNullOrWhiteSpace(objPRO275.ProtocolNo))
                    {
                        db.PRO275.Remove(objPRO275);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO275.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO276> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO276> lstAddPRO276 = new List<PRO276>();
                List<PRO276> lstDeletePRO276 = new List<PRO276>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO276 obj = db.PRO276.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO276();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.RequiredDimension = item.RequiredDimension;
                        obj.ActualDimension = item.ActualDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO276.Add(obj);
                        }
                    }
                    if (lstAddPRO276.Count > 0)
                    {
                        db.PRO276.AddRange(lstAddPRO276);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO276 = db.PRO276.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO276.Count > 0)
                    {
                        db.PRO276.RemoveRange(lstDeletePRO276);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO276 = db.PRO276.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO276.Count > 0)
                    {
                        db.PRO276.RemoveRange(lstDeletePRO276);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region  Linkage View Code   

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL275 objPRL275 = new PRL275();
            objPRL275 = db.PRL275.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL275 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL275.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL276> lstPRL276 = db.PRL276.Where(x => x.HeaderId == objPRL275.HeaderId).ToList();


            ViewBag.lstPRL276 = lstPRL276;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL275.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL275.ActFilledBy) && (objPRL275.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL275.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL275);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL275 objPRL275 = new PRL275();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL275 = db.PRL275.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL275).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL275 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL275.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;


            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL276> lstPRL276 = db.PRL276.Where(x => x.HeaderId == objPRL275.HeaderId).ToList();

            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL275.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL275.ActFilledBy) && (objPRL275.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL275.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    OutsideInside = OutsideInside,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,

                    objPRL275 = objPRL275,

                    lstPRL276 = lstPRL276

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL275 PRL275, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL275.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL275 objPRL275 = db.PRL275.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL275.DrawingRevisionNo = PRL275.DrawingRevisionNo;
                    objPRL275.DrawingNo = PRL275.DrawingNo;
                    objPRL275.DCRNo = PRL275.DCRNo;
                    objPRL275.ProtocolNo = PRL275.ProtocolNo;
                    objPRL275.NozzleNo = PRL275.NozzleNo;
                    objPRL275.GasketFaceRaisedFaceSeamNo = PRL275.GasketFaceRaisedFaceSeamNo;
                    objPRL275.PartDescription = PRL275.PartDescription;
                    objPRL275.GasketGrooveSeamNo = PRL275.GasketGrooveSeamNo;
                    objPRL275.ItemNo = PRL275.ItemNo;
                    objPRL275.NozzleBackFaceSeamNo = PRL275.NozzleBackFaceSeamNo;
                    objPRL275.NozzleIDSeamNo = PRL275.NozzleIDSeamNo;
                    objPRL275.NozzleBackFaceToIDRadiusSeamNo = PRL275.NozzleBackFaceToIDRadiusSeamNo;

                    objPRL275.CheckPoint1 = PRL275.CheckPoint1;
                    objPRL275.CheckPoint2 = PRL275.CheckPoint2;
                    objPRL275.CheckPoint4 = PRL275.CheckPoint4;
                    objPRL275.CheckPoint4_2 = PRL275.CheckPoint4_2;

                    objPRL275.QCRemarks = PRL275.QCRemarks;
                    objPRL275.Result = PRL275.Result;

                    objPRL275.EditedBy = UserName;
                    objPRL275.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL275.ActFilledBy = UserName;
                            objPRL275.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL275.ReqFilledBy = UserName;
                            objPRL275.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL275.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL275 objPRL275 = db.PRL275.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL275 != null && string.IsNullOrWhiteSpace(objPRL275.ProtocolNo))
                    {
                        db.PRL275.Remove(objPRL275);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL275.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL276> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL276> lstAddPRL276 = new List<PRL276>();
                List<PRL276> lstDeletePRL276 = new List<PRL276>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL276 obj = db.PRL276.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL276();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.RequiredDimension = item.RequiredDimension;
                        obj.ActualDimension = item.ActualDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL276.Add(obj);
                        }
                    }
                }
                if (lstAddPRL276.Count > 0)
                {
                    db.PRL276.AddRange(lstAddPRL276);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL276 = db.PRL276.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL276 = db.PRL276.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL276.Count > 0)
                {
                    db.PRL276.RemoveRange(lstDeletePRL276);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}