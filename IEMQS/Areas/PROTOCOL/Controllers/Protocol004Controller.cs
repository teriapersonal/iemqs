﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol004Controller : clsBase
    {
        // GET: PROTOCOL/Protocol004

        #region Protocol Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id,int? m)
        {//PRO015 
            PRO015 objPRO015 = new PRO015();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolRT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolRT = lstProtocolRT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolUT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolUT = lstProtocolUT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();


            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "SET-UP & DIMENSION REPORT FOR NOZZLE";

            if (!id.HasValue)
            {
                objPRO015.ProtocolNo = string.Empty;
                objPRO015.CreatedBy = objClsLoginInfo.UserName;
                objPRO015.CreatedOn = DateTime.Now;
                db.PRO015.Add(objPRO015);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO015.HeaderId;
            }
            else
            {
                objPRO015 = db.PRO015.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO015);
        }

        [HttpPost]
        public ActionResult SaveProtocolData(PRO015 obj,FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = obj.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO015 objPRO015 = db.PRO015.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var lstPRO015 = db.PRO015.Where(o => o.HeaderId != refHeaderId && o.ProtocolNo == obj.ProtocolNo).ToList();
                    if (lstPRO015.Count == 0)
                    {
                        #region Save Data
                        objPRO015.ProtocolNo = obj.ProtocolNo;
                        objPRO015.IdentificationMarksVerified = obj.IdentificationMarksVerified;
                        objPRO015.BoltHolesStraddled = obj.BoltHolesStraddled;
                        objPRO015.WepPtMtUtCleared = obj.WepPtMtUtCleared;
                        objPRO015.SeamNoPunch = obj.SeamNoPunch;
                        objPRO015.TacksFreeFromCrack = obj.TacksFreeFromCrack;
                        objPRO015.SingleSideWeld = obj.SingleSideWeld;
                        objPRO015.Rt1 = obj.Rt1;
                        objPRO015.Rt2 = obj.Rt2;
                        objPRO015.Rt3 = obj.Rt3;
                        objPRO015.Ut1 = obj.Ut1;
                        objPRO015.Ut2 = obj.Ut2;
                        objPRO015.Ut3 = obj.Ut3;
                        objPRO015.RfPadIdenMarksVerified = obj.RfPadIdenMarksVerified;
                        objPRO015.RootGapReq = obj.RootGapReq;
                        objPRO015.RootGapMax = obj.RootGapMax;
                        objPRO015.RootGapMin = obj.RootGapMin;
                        objPRO015.RootFaceReq = obj.RootFaceReq;
                        objPRO015.RootFaceMax = obj.RootFaceMax;
                        objPRO015.RootFaceMin = obj.RootFaceMin;
                        objPRO015.WepAngleReq = obj.WepAngleReq;
                        objPRO015.WepAngleActIS = obj.WepAngleActIS;
                        objPRO015.WepAngleActOS = obj.WepAngleActOS;
                        objPRO015.MaxTilt = obj.MaxTilt;
                        objPRO015.DisBetweenPairNozzlesReq = obj.DisBetweenPairNozzlesReq;
                        objPRO015.DisBetweenPairNozzlesAct = obj.DisBetweenPairNozzlesAct;
                        objPRO015.ReqOrientationOfNozzle = obj.ReqOrientationOfNozzle;
                        objPRO015.ReqOrientationOfNozzleReq = obj.ReqOrientationOfNozzleReq;
                        objPRO015.ReqOrientationOfNozzleAct = obj.ReqOrientationOfNozzleAct;
                        objPRO015.ElevationOfNozzleReq = obj.ElevationOfNozzleReq;
                        objPRO015.ElevationOfNozzleAct = obj.ElevationOfNozzleAct;
                        objPRO015.ProjectionReq = obj.ProjectionReq;
                        objPRO015.ProjectionAct = obj.ProjectionAct;
                        objPRO015.OffsetDistanceOfNozzleReq = obj.OffsetDistanceOfNozzleReq;
                        objPRO015.OffsetDistanceOfNozzleAct = obj.OffsetDistanceOfNozzleAct;
                        objPRO015.OffsetTol = obj.OffsetTol;
                        objPRO015.ActualMinOffset = obj.ActualMinOffset;
                        objPRO015.ActualMaxOffset = obj.ActualMaxOffset;
                        objPRO015.Remark4 = obj.Remark4;
                        objPRO015.Remark5 = obj.Remark5;
                        objPRO015.Remark6 = obj.Remark6;
                        objPRO015.Remark7 = obj.Remark7;
                        objPRO015.Remark8 = obj.Remark8;
                        objPRO015.Remark9 = obj.Remark9;
                        objPRO015.Remark10 = obj.Remark10;
                        objPRO015.EditedBy = objClsLoginInfo.UserName;
                        objPRO015.EditedOn = DateTime.Now;
                        objPRO015.BoltHolesStraddledTol = obj.BoltHolesStraddledTol;
                        objPRO015.BoltHolesStraddledActual = obj.BoltHolesStraddledActual;
                        objPRO015.WepAngleOS = obj.WepAngleOS;
                        objPRO015.WepAngleIN = obj.WepAngleIN;
                        objPRO015.Identification1 = obj.Identification1;
                        objPRO015.Identification2 = obj.Identification2;
                        objPRO015.Remark1 = obj.Remark1;
                        objPRO015.Remark7_1 = obj.Remark7_1.HasValue ? Manager.getDateTime(obj.Remark7_1.Value.ToShortDateString()) : obj.Remark7_1;
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolSaved;
                        objResponseMsg.HeaderId = objPRO015.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolExists;
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolData(int refHeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                //int? refHeaderId = obj.HeaderId;
                if (refHeaderId > 0)
                {
                    PRO015 objPRO015 = db.PRO015.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO015 != null && string.IsNullOrWhiteSpace(objPRO015.ProtocolNo))
                    {
                        db.PRO015.Remove(objPRO015);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.SETUP_AND_DIMENSION_REPORT_FOR_NOZZLE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO015.HeaderId;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Protocol Linkage Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Linkage(int? id, int? m)
        {//PRL015 
            PRL015 objPRL015 = new PRL015();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolRT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolRT = lstProtocolRT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstProtocolUT = clsImplementationEnum.getProtocolRT().ToList();
            ViewBag.ProtocolUT = lstProtocolUT.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "SET-UP & DIMENSION REPORT FOR NOZZLE";

            if (!id.HasValue)
            {
                //   objPRL015.QualityProject = "0017005A/A";
                objPRL015.ProtocolNo = string.Empty;
                objPRL015.CreatedBy = objClsLoginInfo.UserName;
                objPRL015.CreatedOn = DateTime.Now;
                db.PRL015.Add(objPRL015);
                db.SaveChanges();
                ViewBag.HeaderId = objPRL015.HeaderId;
            }
            else
            {
                objPRL015 = db.PRL015.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL015.QualityProject).FirstOrDefault();
                ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRL015);
        }

        [HttpPost]
        public ActionResult SaveLinkageData(PRL015 obj,FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = obj.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL015 objPRL015 = db.PRL015.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL015.DCRNo = obj.DCRNo;
                    objPRL015.ProtocolNo = obj.ProtocolNo != null ? obj.ProtocolNo : "";
                    objPRL015.IdentificationMarksVerified = obj.IdentificationMarksVerified;
                    objPRL015.BoltHolesStraddled = obj.BoltHolesStraddled;
                    objPRL015.WepPtMtUtCleared = obj.WepPtMtUtCleared;
                    objPRL015.SeamNoPunch = obj.SeamNoPunch;
                    objPRL015.TacksFreeFromCrack = obj.TacksFreeFromCrack;
                    objPRL015.SingleSideWeld = obj.SingleSideWeld;
                    objPRL015.Rt1 = obj.Rt1;
                    objPRL015.Rt2 = obj.Rt2;
                    objPRL015.Rt3 = obj.Rt3;
                    objPRL015.Ut1 = obj.Ut1;
                    objPRL015.Ut2 = obj.Ut2;
                    objPRL015.Ut3 = obj.Ut3;
                    objPRL015.RfPadIdenMarksVerified = obj.RfPadIdenMarksVerified;
                    objPRL015.RootGapReq = obj.RootGapReq;
                    objPRL015.RootGapMax = obj.RootGapMax;
                    objPRL015.RootGapMin = obj.RootGapMin;
                    objPRL015.RootFaceReq = obj.RootFaceReq;
                    objPRL015.RootFaceMax = obj.RootFaceMax;
                    objPRL015.RootFaceMin = obj.RootFaceMin;
                    objPRL015.WepAngleReq = obj.WepAngleReq;
                    objPRL015.WepAngleActIS = obj.WepAngleActIS;
                    objPRL015.WepAngleActOS = obj.WepAngleActOS;
                    objPRL015.MaxTilt = obj.MaxTilt;
                    objPRL015.DisBetweenPairNozzlesReq = obj.DisBetweenPairNozzlesReq;
                    objPRL015.DisBetweenPairNozzlesAct = obj.DisBetweenPairNozzlesAct;
                    objPRL015.ReqOrientationOfNozzle = obj.ReqOrientationOfNozzle;
                    objPRL015.ReqOrientationOfNozzleReq = obj.ReqOrientationOfNozzleReq;
                    objPRL015.ReqOrientationOfNozzleAct = obj.ReqOrientationOfNozzleAct;
                    objPRL015.ElevationOfNozzleReq = obj.ElevationOfNozzleReq;
                    objPRL015.ElevationOfNozzleAct = obj.ElevationOfNozzleAct;
                    objPRL015.ProjectionReq = obj.ProjectionReq;
                    objPRL015.ProjectionAct = obj.ProjectionAct;
                    objPRL015.OffsetDistanceOfNozzleReq = obj.OffsetDistanceOfNozzleReq;
                    objPRL015.OffsetDistanceOfNozzleAct = obj.OffsetDistanceOfNozzleAct;
                    objPRL015.OffsetTol = obj.OffsetTol;
                    objPRL015.ActualMinOffset = obj.ActualMinOffset;
                    objPRL015.ActualMaxOffset = obj.ActualMaxOffset;
                    objPRL015.Remark4 = obj.Remark4;
                    objPRL015.Remark5 = obj.Remark5;
                    objPRL015.Remark6 = obj.Remark6;
                    objPRL015.Remark7 = obj.Remark7;
                    objPRL015.Remark8 = obj.Remark8;
                    objPRL015.Remark9 = obj.Remark9;
                    objPRL015.Remark10 = obj.Remark10;
                    objPRL015.EditedBy = objClsLoginInfo.UserName;
                    objPRL015.EditedOn = DateTime.Now;
                    objPRL015.BoltHolesStraddledTol = obj.BoltHolesStraddledTol;
                    objPRL015.BoltHolesStraddledActual = obj.BoltHolesStraddledActual;
                    objPRL015.WepAngleOS = obj.WepAngleOS;
                    objPRL015.WepAngleIN = obj.WepAngleIN;
                    objPRL015.Remark1 = obj.Remark1;
                    objPRL015.Identification1 = obj.Identification1;
                    objPRL015.Identification2 = obj.Identification2;
                    if (obj.Remark7_1.HasValue)
                    {
                        Elmah.ErrorSignal.FromCurrentContext().Raise(new Exception(obj.Remark7_1.Value.ToShortDateString()));
                    }
                    objPRL015.Remark7_1 = obj.Remark7_1.HasValue ? Manager.getDateTime(obj.Remark7_1.Value.ToShortDateString()) : obj.Remark7_1;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL015.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion
        //public DateTime? getDateTime(string date)
        //{

        //    string[] formats = { "dd/MM/yyyy", "dd/MMM/yyyy" };
        //    DateTime outValue;
        //    if (DateTime.TryParseExact(date,
        //              formats, null,
        //                DateTimeStyles.None,
        //                out outValue))
        //    { return outValue; }
        //    else
        //    { return null; }
        //    //dt = DateTime.ParseExact(date, @"d/M/yyyy", CultureInfo.InvariantCulture);
        //}
        #region Common function
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PROD3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => (i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc == clsImplementationEnum.UserRoleName.QC2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI3.GetStringValue())).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = string.Empty;
                    objUserRoleAccessDetails.UserDesignation = string.Empty;
                }

            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }
        #endregion
    }
}