﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol061Controller : clsBase
    {
        // GET: PROTOCOL/Protocol061

        string ControllerURL = "/PROTOCOL/Protocol061/";
        string Title = "REPORT FOR SET-UP AND DIMENSION OF TAILING LUG";

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO300 objPRO300 = new PRO300();
            if (!id.HasValue)
            {
                try
                {
                    objPRO300.ProtocolNo = string.Empty;
                    objPRO300.CreatedBy = objClsLoginInfo.UserName;
                    objPRO300.CreatedOn = DateTime.Now;



                    #region DIMENSION 
                    objPRO300.PRO301.Add(new PRO301
                    {
                        Dimension = "A",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO300.PRO301.Add(new PRO301
                    {
                        Dimension = "B",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO300.PRO301.Add(new PRO301
                    {
                        Dimension = "C",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO300.PRO301.Add(new PRO301
                    {
                        Dimension = "D",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO300.PRO301.Add(new PRO301
                    {
                        Dimension = "E",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO300.PRO301.Add(new PRO301
                    {
                        Dimension = "F",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO300.PRO301.Add(new PRO301
                    {
                        Dimension = "G",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO300.PRO301.Add(new PRO301
                    {
                        Dimension = "H",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    db.PRO300.Add(objPRO300);
                    db.SaveChanges();
                    // return View();

                }

                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO300.HeaderId;
            }
            else
            {
                objPRO300 = db.PRO300.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();


            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();



            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO301> lstPRO301 = db.PRO301.Where(x => x.HeaderId == objPRO300.HeaderId).ToList();
            List<PRO302> lstPRO302 = db.PRO302.Where(x => x.HeaderId == objPRO300.HeaderId).ToList();
            List<PRO303> lstPRO303 = db.PRO303.Where(x => x.HeaderId == objPRO300.HeaderId).ToList();




            ViewBag.lstPRO301 = lstPRO301;
            ViewBag.lstPRO302 = lstPRO302;
            ViewBag.lstPRO303 = lstPRO303;



            #endregion
            return View(objPRO300);

        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO300 PRO300)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO300.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO300 objPRO300 = db.PRO300.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO300.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO300.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO300.ProtocolNo = PRO300.ProtocolNo;
                        objPRO300.ReqRootGap = PRO300.ReqRootGap;
                        objPRO300.ReqRootFace = PRO300.ReqRootFace;
                        objPRO300.ReqOneSideWepAngle = PRO300.ReqOneSideWepAngle;
                        objPRO300.ReqOtherSideWepAngle = PRO300.ReqOtherSideWepAngle;
                        objPRO300.CheckPoint1 = PRO300.CheckPoint1;
                        objPRO300.CheckPoint2 = PRO300.CheckPoint2;
                        objPRO300.CheckPoint3 = PRO300.CheckPoint3;
                        objPRO300.CheckPoint4 = PRO300.CheckPoint4;
                        objPRO300.CheckPoint5 = PRO300.CheckPoint5;
                        objPRO300.CheckPoint6 = PRO300.CheckPoint6;
                        objPRO300.CheckPoint8 = PRO300.CheckPoint8;
                        objPRO300.CheckPoint8_2 = PRO300.CheckPoint8_2;
                        objPRO300.QCRemarks = PRO300.QCRemarks;
                        objPRO300.Result = PRO300.Result;

                        objPRO300.EditedBy = objClsLoginInfo.UserName;
                        objPRO300.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO300.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO300 objPRO300 = db.PRO300.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO300 != null && string.IsNullOrWhiteSpace(objPRO300.ProtocolNo))
                    {
                        db.PRO300.Remove(objPRO300);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO300.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO301> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO301> lstAddPRO301 = new List<PRO301>();
                List<PRO301> lstDeletePRO301 = new List<PRO301>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO301 obj = db.PRO301.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {


                                obj.Dimension = item.Dimension;
                                obj.Required = item.Required;
                                obj.Actual = item.Actual;
                                obj.Remarks = item.Remarks;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO301 obj = new PRO301();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Dimension))
                            {
                                obj.Dimension = item.Dimension;
                                obj.Required = item.Required;
                                obj.Actual = item.Actual;
                                obj.Remarks = item.Remarks;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO301.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO301.Count > 0)
                    {
                        db.PRO301.AddRange(lstAddPRO301);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO301 = db.PRO301.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO301.Count > 0)
                    {
                        db.PRO301.RemoveRange(lstDeletePRO301);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO301 = db.PRO301.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO301.Count > 0)
                    {
                        db.PRO301.RemoveRange(lstDeletePRO301);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO302> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO302> lstAddPRO302 = new List<PRO302>();
                List<PRO302> lstDeletePRO302 = new List<PRO302>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO302 obj = db.PRO302.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {


                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxOneSideWepAngle = item.ActMaxOneSideWepAngle;
                                obj.ActMaxOtherSideWepAngle = item.ActMaxOtherSideWepAngle;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinOneSideWepAngle = item.ActMinOneSideWepAngle;
                                obj.ActMinOtherSideWepAngle = item.ActMinOtherSideWepAngle;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO302 obj = new PRO302();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxOneSideWepAngle = item.ActMaxOneSideWepAngle;
                                obj.ActMaxOtherSideWepAngle = item.ActMaxOtherSideWepAngle;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinOneSideWepAngle = item.ActMinOneSideWepAngle;
                                obj.ActMinOtherSideWepAngle = item.ActMinOtherSideWepAngle;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO302.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO302.Count > 0)
                    {
                        db.PRO302.AddRange(lstAddPRO302);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO302 = db.PRO302.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO302.Count > 0)
                    {
                        db.PRO302.RemoveRange(lstDeletePRO302);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO302 = db.PRO302.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO302.Count > 0)
                    {
                        db.PRO302.RemoveRange(lstDeletePRO302);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO303> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO303> lstAddPRO303 = new List<PRO303>();
                List<PRO303> lstDeletePRO303 = new List<PRO303>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO303 obj = db.PRO303.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;

                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO303 obj = new PRO303();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO303.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO303.Count > 0)
                    {
                        db.PRO303.AddRange(lstAddPRO303);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO303 = db.PRO303.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO303.Count > 0)
                    {
                        db.PRO303.RemoveRange(lstDeletePRO303);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO303 = db.PRO303.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO303.Count > 0)
                    {
                        db.PRO303.RemoveRange(lstDeletePRO303);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL300 objPRL300 = new PRL300();
            objPRL300 = db.PRL300.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL300 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL300.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();


            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();



            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL301> lstPRL301 = db.PRL301.Where(x => x.HeaderId == objPRL300.HeaderId).ToList();
            List<PRL302> lstPRL302 = db.PRL302.Where(x => x.HeaderId == objPRL300.HeaderId).ToList();
            List<PRL303> lstPRL303 = db.PRL303.Where(x => x.HeaderId == objPRL300.HeaderId).ToList();




            ViewBag.lstPRL301 = lstPRL301;
            ViewBag.lstPRL302 = lstPRL302;
            ViewBag.lstPRL303 = lstPRL303;


            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.lstDrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL300.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL300.ActFilledBy) && (objPRL300.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL300.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL300);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL300 objPRL300 = new PRL300();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL300 = db.PRL300.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL300).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL300 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL300.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }
            
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;
           
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();


            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL301> lstPRL301 = db.PRL301.Where(x => x.HeaderId == objPRL300.HeaderId).ToList();
            List<PRL302> lstPRL302 = db.PRL302.Where(x => x.HeaderId == objPRL300.HeaderId).ToList();
            List<PRL303> lstPRL303 = db.PRL303.Where(x => x.HeaderId == objPRL300.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL300.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL300.ActFilledBy) && (objPRL300.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL300.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    OutsideInside = OutsideInside,

                    objPRL300 = objPRL300,

                    lstPRL301 = lstPRL301,
                    lstPRL302 = lstPRL302,
                    lstPRL303 = lstPRL303

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL300 PRL300, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL300.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL300 objPRL300 = db.PRL300.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data

                    objPRL300.DrawingRevisionNo = PRL300.DrawingRevisionNo;
                    objPRL300.DrawingNo = PRL300.DrawingNo;
                    objPRL300.DCRNo = PRL300.DCRNo;
                    objPRL300.ProtocolNo = PRL300.ProtocolNo != null ? PRL300.ProtocolNo : "";

                    objPRL300.ReqRootGap = PRL300.ReqRootGap;
                    objPRL300.ReqRootFace = PRL300.ReqRootFace;
                    objPRL300.ReqOneSideWepAngle = PRL300.ReqOneSideWepAngle;
                    objPRL300.ReqOtherSideWepAngle = PRL300.ReqOtherSideWepAngle;
                    objPRL300.CheckPoint1 = PRL300.CheckPoint1;
                    objPRL300.CheckPoint2 = PRL300.CheckPoint2;
                    objPRL300.CheckPoint3 = PRL300.CheckPoint3;
                    objPRL300.CheckPoint4 = PRL300.CheckPoint4;
                    objPRL300.CheckPoint5 = PRL300.CheckPoint5;
                    objPRL300.CheckPoint6 = PRL300.CheckPoint6;
                    objPRL300.CheckPoint8 = PRL300.CheckPoint8;
                    objPRL300.CheckPoint8_2 = PRL300.CheckPoint8_2;
                    objPRL300.QCRemarks = PRL300.QCRemarks;
                    objPRL300.Result = PRL300.Result;

                    objPRL300.EditedBy = UserName;
                    objPRL300.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL300.ActFilledBy = UserName;
                            objPRL300.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL300.ReqFilledBy = UserName;
                            objPRL300.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL300.HeaderId;
                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL300 objPRL300 = db.PRL300.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL300 != null && string.IsNullOrWhiteSpace(objPRL300.ProtocolNo))
                    {
                        db.PRL300.Remove(objPRL300);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL300.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL301> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL301> lstAddPRL301 = new List<PRL301>();
                List<PRL301> lstDeletePRL301 = new List<PRL301>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL301 obj = db.PRL301.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Dimension = item.Dimension;
                                obj.Required = item.Required;
                                obj.Actual = item.Actual;
                                obj.Remarks = item.Remarks;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL301 obj = new PRL301();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Dimension))
                            {
                                obj.Dimension = item.Dimension;
                                obj.Required = item.Required;
                                obj.Actual = item.Actual;
                                obj.Remarks = item.Remarks;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL301.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL301.Count > 0)
                {
                    db.PRL301.AddRange(lstAddPRL301);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL301 = db.PRL301.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL301 = db.PRL301.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL301.Count > 0)
                {
                    db.PRL301.RemoveRange(lstDeletePRL301);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL302> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL302> lstAddPRL302 = new List<PRL302>();
                List<PRL302> lstDeletePRL302 = new List<PRL302>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.SeamNo != null)
                        {
                            bool isAdded = false;
                            PRL302 obj = db.PRL302.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj == null)
                            {
                                obj = new PRL302();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                            else
                            {
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }

                            obj.SeamNo = item.SeamNo;
                            obj.ActMaxRootGap = item.ActMaxRootGap;
                            obj.ActMaxRootFace = item.ActMaxRootFace;
                            obj.ActMaxOneSideWepAngle = item.ActMaxOneSideWepAngle;
                            obj.ActMaxOtherSideWepAngle = item.ActMaxOtherSideWepAngle;
                            obj.ActMinRootGap = item.ActMinRootGap;
                            obj.ActMinRootFace = item.ActMinRootFace;
                            obj.ActMinOneSideWepAngle = item.ActMinOneSideWepAngle;
                            obj.ActMinOtherSideWepAngle = item.ActMinOtherSideWepAngle;
                            if (isAdded)
                            {
                                lstAddPRL302.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL302.Count > 0)
                {
                    db.PRL302.AddRange(lstAddPRL302);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL302 = db.PRL302.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL302 = db.PRL302.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL302.Count > 0)
                {
                    db.PRL302.RemoveRange(lstDeletePRL302);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL303> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL303> lstAddPRL303 = new List<PRL303>();
                List<PRL303> lstDeletePRL303 = new List<PRL303>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL303 obj = db.PRL303.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL303();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                        {
                            obj.IdentificationMarking = item.IdentificationMarking;
                        }
                        if (isAdded)
                        {
                            lstAddPRL303.Add(obj);
                        }
                    }
                    if (lstAddPRL303.Count > 0)
                    {
                        db.PRL303.AddRange(lstAddPRL303);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL303 = db.PRL303.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL303 = db.PRL303.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL303.Count > 0)
                {
                    db.PRL303.RemoveRange(lstDeletePRL303);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion



        #endregion
    }
}