﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol045Controller : clsBase
    {
        // GET: PROTOCOL/Protocol045
        string ControllerURL = "/PROTOCOL/Protocol045/";
        string Title = "VISUAL AND DIMENSION INSPECTION REPORT FOR FORMED HEMISPHERICAL D'END";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO220 objPRO220 = new PRO220();
            if (!id.HasValue)
            {
                try
                {
                    objPRO220.ProtocolNo = string.Empty;
                    objPRO220.CreatedBy = objClsLoginInfo.UserName;
                    objPRO220.CreatedOn = DateTime.Now;

                    #region OVALITY 
                    objPRO220.PRO221.Add(new PRO221
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO220.PRO221.Add(new PRO221
                    {
                        Orientation = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO220.PRO221.Add(new PRO221
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO220.PRO221.Add(new PRO221
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO220.PRO221.Add(new PRO221
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO220.PRO221.Add(new PRO221
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO220.PRO221.Add(new PRO221
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO220.PRO221.Add(new PRO221
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region TOTAL HEIGHT
                    objPRO220.PRO223.Add(new PRO223
                    {
                        TotalHeight1 = "AT 0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO220.PRO223.Add(new PRO223
                    {
                        TotalHeight1 = "AT 90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO220.PRO223.Add(new PRO223
                    {
                        TotalHeight1 = "AT 180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO220.PRO223.Add(new PRO223
                    {
                        TotalHeight1 = "AT 270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    db.PRO220.Add(objPRO220);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO220.HeaderId;
            }
            else
            {
                objPRO220 = db.PRO220.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();


            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();



            //  ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO221> lstPRO221 = db.PRO221.Where(x => x.HeaderId == objPRO220.HeaderId).ToList();
            List<PRO222> lstPRO222 = db.PRO222.Where(x => x.HeaderId == objPRO220.HeaderId).ToList();
            List<PRO223> lstPRO223 = db.PRO223.Where(x => x.HeaderId == objPRO220.HeaderId).ToList();
            List<PRO224> lstPRO224 = db.PRO224.Where(x => x.HeaderId == objPRO220.HeaderId).ToList();
            List<PRO224_2> lstPRO224_2 = db.PRO224_2.Where(x => x.HeaderId == objPRO220.HeaderId).ToList();

            //List<PRO194_4> lstPRO194_4 = db.PRO194_4.Where(x => x.HeaderId == objPRO220.HeaderId).ToList();
            //List<PRO194_5> lstPRO194_5 = db.PRO194_5.Where(x => x.HeaderId == objPRO220.HeaderId).ToList();
            //List<PRO194_6> lstPRO194_6 = db.PRO194_6.Where(x => x.HeaderId == objPRO220.HeaderId).ToList();

            List<SP_IPI_PROTOCOL045_GET_LINES6_DATA_Result> lstPRO224_3 = db.SP_IPI_PROTOCOL045_GET_LINES6_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO220.HeaderId).ToList();

            //List<SP_IPI_PROTOCOL039_GET_LINES9_DATA_Result> lstPRO194_8 = db.SP_IPI_PROTOCOL039_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO220.HeaderId).ToList();
            //List<PRO194_7> lstPRO194_7 = db.PRO194_7.Where(x => x.HeaderId == objPRO190.HeaderId).OrderBy(x => new { x.SeamNo, x.LineId }).ToList();
            //List<PRO194_8> lstPRO194_8 = db.PRO194_8.Where(x => x.HeaderId == objPRO190.HeaderId).OrderBy(x => new { x.SeamNo, x.LineId }).ToList();

            ViewBag.lstPRO221 = lstPRO221;
            ViewBag.lstPRO222 = lstPRO222;
            ViewBag.lstPRO223 = lstPRO223;
            ViewBag.lstPRO224 = lstPRO224;
            ViewBag.lstPRO224_2 = lstPRO224_2;
            ViewBag.lstPRO224_3 = lstPRO224_3;


            #endregion
            return View(objPRO220);

        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO220 PRO220)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO220.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO220 objPRO220 = db.PRO220.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO220.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO220.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO220.ProtocolNo = PRO220.ProtocolNo;
                        objPRO220.Ovality = PRO220.Ovality;
                        objPRO220.OutBy = PRO220.OutBy;
                        objPRO220.ReqTotalHeight = PRO220.ReqTotalHeight;
                        objPRO220.ReqCircBottom = PRO220.ReqCircBottom;
                        objPRO220.ActCircBottom = PRO220.ActCircBottom;
                        objPRO220.EvalueTemplateInspection = PRO220.EvalueTemplateInspection;
                        objPRO220.ReqChordLengthBottom = PRO220.ReqChordLengthBottom;
                        objPRO220.ActChordLengthBottom = PRO220.ActChordLengthBottom;
                        objPRO220.ReqRadiusBottom = PRO220.ReqRadiusBottom;
                        objPRO220.ActRadiusBottom = PRO220.ActRadiusBottom;
                        objPRO220.ReqGapBottom = PRO220.ReqGapBottom;
                        objPRO220.ActGapBottom = PRO220.ActGapBottom;
                        objPRO220.ProfileMeasurement = PRO220.ProfileMeasurement;
                        objPRO220.ReqAllowedOffset = PRO220.ReqAllowedOffset;
                        objPRO220.ReqOverCrowing = PRO220.ReqOverCrowing;
                        objPRO220.ActOverCrowing = PRO220.ActOverCrowing;
                        objPRO220.ReqUnderCrowing = PRO220.ReqUnderCrowing;
                        objPRO220.ActUnderCrowing = PRO220.ActUnderCrowing;
                        objPRO220.ReqMinThickness = PRO220.ReqMinThickness;
                        objPRO220.ActMinThickness = PRO220.ActMinThickness;
                        objPRO220.CheckPoint1 = PRO220.CheckPoint1;
                        objPRO220.CheckPoint2 = PRO220.CheckPoint2;
                        objPRO220.CheckPoint2_2 = PRO220.CheckPoint2_2;
                        objPRO220.CheckPoint4 = PRO220.CheckPoint4;
                        objPRO220.CheckPoint5 = PRO220.CheckPoint5;
                        objPRO220.CheckPoint6 = PRO220.CheckPoint6;
                        objPRO220.QCRemarks = PRO220.QCRemarks;
                        objPRO220.Result = PRO220.Result;
                        objPRO220.ReqWidth = PRO220.ReqWidth;
                        objPRO220.ReqDepth = PRO220.ReqDepth;
                        objPRO220.AllowedOffset = PRO220.AllowedOffset;
                        objPRO220.Note1 = PRO220.Note1;
                        objPRO220.Note2 = PRO220.Note2;
                        objPRO220.Note3 = PRO220.Note3;

                        objPRO220.EditedBy = objClsLoginInfo.UserName;
                        objPRO220.EditedOn = DateTime.Now;


                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO220.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO220 objPRO220 = db.PRO220.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO220 != null && string.IsNullOrWhiteSpace(objPRO220.ProtocolNo))
                    {
                        db.PRO220.Remove(objPRO220);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO220.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO221> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO221> lstAddPRO221 = new List<PRO221>();
                List<PRO221> lstDeletePRO221 = new List<PRO221>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO221 obj = db.PRO221.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO221 obj = new PRO221();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO221.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO221.Count > 0)
                    {
                        db.PRO221.AddRange(lstAddPRO221);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO221 = db.PRO221.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO221.Count > 0)
                    {
                        db.PRO221.RemoveRange(lstDeletePRO221);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO221 = db.PRO221.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO221.Count > 0)
                    {
                        db.PRO221.RemoveRange(lstDeletePRO221);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO222> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO222> lstAddPRO222 = new List<PRO222>();
                List<PRO222> lstDeletePRO222 = new List<PRO222>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO222 obj = db.PRO222.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ReqOrien = item.ReqOrien;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActArcLength = item.ActArcLength;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO222 obj = new PRO222();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ReqOrien = item.ReqOrien;
                                obj.ReqArcLength = item.ReqArcLength;
                                obj.ActArcLength = item.ActArcLength;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO222.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO222.Count > 0)
                    {
                        db.PRO222.AddRange(lstAddPRO222);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO222 = db.PRO222.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO222.Count > 0)
                    {
                        db.PRO222.RemoveRange(lstDeletePRO222);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO222 = db.PRO222.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO222.Count > 0)
                    {
                        db.PRO222.RemoveRange(lstDeletePRO222);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO223> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO223> lstAddPRO223 = new List<PRO223>();
                List<PRO223> lstDeletePRO223 = new List<PRO223>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO223 obj = db.PRO223.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.TotalHeight1 = item.TotalHeight1;
                                obj.TotalHeight2 = item.TotalHeight2;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO223 obj = new PRO223();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.TotalHeight1))
                            {
                                obj.TotalHeight1 = item.TotalHeight1;
                                obj.TotalHeight2 = item.TotalHeight2;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO223.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO223.Count > 0)
                    {
                        db.PRO223.AddRange(lstAddPRO223);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO223 = db.PRO223.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO223.Count > 0)
                    {
                        db.PRO223.RemoveRange(lstDeletePRO223);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO223 = db.PRO223.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO223.Count > 0)
                    {
                        db.PRO223.RemoveRange(lstDeletePRO223);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO224> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO224> lstAddPRO224 = new List<PRO224>();
                List<PRO224> lstDeletePRO224 = new List<PRO224>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO224 obj = db.PRO224.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.TopPeakIn = item.TopPeakIn;
                                obj.TopPeakOut = item.TopPeakOut;
                                obj.MidPeakIn = item.MidPeakIn;
                                obj.MidPeakOut = item.MidPeakOut;
                                obj.BottonPeakIn = item.BottonPeakIn;
                                obj.BottonPeakOut = item.BottonPeakOut;
                                obj.ActMin = item.ActMin;
                                obj.ActMax = item.ActMax;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO224 obj = new PRO224();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.TopPeakIn = item.TopPeakIn;
                                obj.TopPeakOut = item.TopPeakOut;
                                obj.MidPeakIn = item.MidPeakIn;
                                obj.MidPeakOut = item.MidPeakOut;
                                obj.BottonPeakIn = item.BottonPeakIn;
                                obj.BottonPeakOut = item.BottonPeakOut;
                                //obj.ActMin = item.ActMin;
                                //obj.ActMax = item.ActMax;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO224.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO224.Count > 0)
                    {
                        db.PRO224.AddRange(lstAddPRO224);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO224 = db.PRO224.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO224.Count > 0)
                    {
                        db.PRO224.RemoveRange(lstDeletePRO224);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO224 = db.PRO224.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO224.Count > 0)
                    {
                        db.PRO224.RemoveRange(lstDeletePRO224);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //#region Line5 details
        //[HttpPost]
        //public JsonResult SaveProtocolLine5(List<PRO224_2> lst, int HeaderId)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        List<PRO224_2> lstAddPRO224_2 = new List<PRO224_2>();
        //        List<PRO224_2> lstDeletePRO224_2 = new List<PRO224_2>();
        //        if (lst != null)
        //        {
        //            foreach (var item in lst)
        //            {
        //                if (item.LineId > 0)
        //                {
        //                    PRO224_2 obj = db.PRO224_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
        //                    if (obj != null)
        //                    {
        //                        obj.ActMin = item.ActMin;
        //                        obj.ActMax = item.ActMax;
        //                       obj.EditedBy = objClsLoginInfo.UserName;
        //                        obj.EditedOn = DateTime.Now;
        //                    }
        //                }
        //                else
        //                {
        //                    PRO224_2 obj = new PRO224_2();
        //                    obj.HeaderId = item.HeaderId;
        //                    if (!string.IsNullOrWhiteSpace(item.ActMin))
        //                    {
        //                        obj.ActMin = item.ActMin;
        //                        obj.ActMax = item.ActMax;
        //                        obj.EditedBy = objClsLoginInfo.UserName;
        //                        obj.EditedOn = DateTime.Now;
        //                        obj.CreatedBy = objClsLoginInfo.UserName;
        //                        obj.CreatedOn = DateTime.Now;
        //                        lstAddPRO224_2.Add(obj);
        //                    }
        //                }
        //            }
        //            if (lstAddPRO224_2.Count > 0)
        //            {
        //                db.PRO224_2.AddRange(lstAddPRO224_2);
        //            }

        //            var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
        //            var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

        //            lstDeletePRO224_2 = db.PRO224_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
        //            if (lstDeletePRO224_2.Count > 0)
        //            {
        //                db.PRO224_2.RemoveRange(lstDeletePRO224_2);
        //            }
        //            db.SaveChanges();
        //        }
        //        else
        //        {
        //            lstDeletePRO224_2 = db.PRO224_2.Where(x => x.HeaderId == HeaderId).ToList();
        //            if (lstDeletePRO224_2.Count > 0)
        //            {
        //                db.PRO224_2.RemoveRange(lstDeletePRO224_2);
        //            }
        //            db.SaveChanges();
        //        }
        //        objResponseMsg.Key = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        //#endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6(List<PRO224_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO224_3> lstAddPRO224_3 = new List<PRO224_3>();
                List<PRO224_3> lstDeletePRO224_3 = new List<PRO224_3>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO224_3 obj = db.PRO224_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO224_3 objFecthSeam = new PRO224_3();
                        if (obj == null)
                        {
                            obj = new PRO224_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO224_3.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                        }
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNo = item.SpotNo;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRO224_3.Add(obj);
                        }
                    }
                }
                if (lstAddPRO224_3.Count > 0)
                {
                    db.PRO224_3.AddRange(lstAddPRO224_3);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO224_3 = db.PRO224_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRO224_3 = db.PRO224_3.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRO224_3.Count > 0)
                {
                    db.PRO224_3.RemoveRange(lstDeletePRO224_3);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion




        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL220 objPRL220 = new PRL220();
            objPRL220 = db.PRL220.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL220 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL220.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();



            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL221> lstPRL221 = db.PRL221.Where(x => x.HeaderId == objPRL220.HeaderId).ToList();
            List<PRL222> lstPRL222 = db.PRL222.Where(x => x.HeaderId == objPRL220.HeaderId).ToList();
            List<PRL223> lstPRL223 = db.PRL223.Where(x => x.HeaderId == objPRL220.HeaderId).ToList();
            List<PRL224> lstPRL224 = db.PRL224.Where(x => x.HeaderId == objPRL220.HeaderId).ToList();
            List<PRL224_2> lstPRL224_2 = db.PRL224_2.Where(x => x.HeaderId == objPRL220.HeaderId).ToList();
            List<SP_IPI_PROTOCOL045_LINKAGE_GET_LINES6_DATA_Result> lstPRL224_3 = db.SP_IPI_PROTOCOL045_LINKAGE_GET_LINES6_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL220.HeaderId).ToList();
            //List<SP_IPI_PROTOCOL045_LINKAGE_GET_LINES6_DATA> lstPRL224_3 = db.SP_IPI_PROTOCOL045_LINKAGE_GET_LINES6_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL220.HeaderId).ToList();

            ViewBag.lstPRL221 = lstPRL221;
            ViewBag.lstPRL222 = lstPRL222;
            ViewBag.lstPRL223 = lstPRL223;
            ViewBag.lstPRL224 = lstPRL224;
            ViewBag.lstPRL224_2 = lstPRL224_2;
            ViewBag.lstPRL224_3 = lstPRL224_3;

            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.lstDrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL220.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL220.ActFilledBy) && (objPRL220.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL220.Project).ToList();
                            ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL220);
        }


        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL220 objPRL220 = new PRL220();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL220 = db.PRL220.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL220).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL220 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL220.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;


            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL221> lstPRL221 = db.PRL221.Where(x => x.HeaderId == objPRL220.HeaderId).ToList();
            List<PRL222> lstPRL222 = db.PRL222.Where(x => x.HeaderId == objPRL220.HeaderId).ToList();
            List<PRL223> lstPRL223 = db.PRL223.Where(x => x.HeaderId == objPRL220.HeaderId).ToList();
            List<PRL224> lstPRL224 = db.PRL224.Where(x => x.HeaderId == objPRL220.HeaderId).ToList();
            List<PRL224_2> lstPRL224_2 = db.PRL224_2.Where(x => x.HeaderId == objPRL220.HeaderId).ToList();
            List<SP_IPI_PROTOCOL045_LINKAGE_GET_LINES6_DATA_Result> lstPRL224_3 = db.SP_IPI_PROTOCOL045_LINKAGE_GET_LINES6_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL220.HeaderId).ToList();
            //List<SP_IPI_PROTOCOL045_LINKAGE_GET_LINES6_DATA> lstPRL224_3 = db.SP_IPI_PROTOCOL045_LINKAGE_GET_LINES6_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL220.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;

                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL220.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL220.ActFilledBy) && (objPRL220.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL220.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    LeftRight = LeftRight,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    OutsideInside = OutsideInside,

                    objPRL220 = objPRL220,

                    lstPRL221 = lstPRL221,
                    lstPRL222 = lstPRL222,
                    lstPRL223 = lstPRL223,
                    lstPRL224 = lstPRL224,
                    lstPRL224_2 = lstPRL224_2,
                    lstPRL224_3 = lstPRL224_3,


                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.lstDrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL220 PRL220, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL220.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL220 objPRL220 = db.PRL220.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data

                    objPRL220.DrawingRevisionNo = PRL220.DrawingRevisionNo;
                    objPRL220.DrawingNo = PRL220.DrawingNo;
                    objPRL220.DCRNo = PRL220.DCRNo;
                    objPRL220.ProtocolNo = PRL220.ProtocolNo != null ? PRL220.ProtocolNo : "";


                    objPRL220.Ovality = PRL220.Ovality;
                    objPRL220.OutBy = PRL220.OutBy;
                    objPRL220.ReqTotalHeight = PRL220.ReqTotalHeight;
                    objPRL220.ReqCircBottom = PRL220.ReqCircBottom;
                    objPRL220.ActCircBottom = PRL220.ActCircBottom;
                    objPRL220.EvalueTemplateInspection = PRL220.EvalueTemplateInspection;
                    objPRL220.ReqChordLengthBottom = PRL220.ReqChordLengthBottom;
                    objPRL220.ActChordLengthBottom = PRL220.ActChordLengthBottom;
                    objPRL220.ReqRadiusBottom = PRL220.ReqRadiusBottom;
                    objPRL220.ActRadiusBottom = PRL220.ActRadiusBottom;
                    objPRL220.ReqGapBottom = PRL220.ReqGapBottom;
                    objPRL220.ActGapBottom = PRL220.ActGapBottom;
                    objPRL220.ProfileMeasurement = PRL220.ProfileMeasurement;
                    objPRL220.ReqAllowedOffset = PRL220.ReqAllowedOffset;
                    objPRL220.ReqOverCrowing = PRL220.ReqOverCrowing;
                    objPRL220.ActOverCrowing = PRL220.ActOverCrowing;
                    objPRL220.ReqUnderCrowing = PRL220.ReqUnderCrowing;
                    objPRL220.ActUnderCrowing = PRL220.ActUnderCrowing;
                    objPRL220.ReqMinThickness = PRL220.ReqMinThickness;
                    objPRL220.ActMinThickness = PRL220.ActMinThickness;
                    objPRL220.CheckPoint1 = PRL220.CheckPoint1;
                    objPRL220.CheckPoint2 = PRL220.CheckPoint2;
                    objPRL220.CheckPoint2_2 = PRL220.CheckPoint2_2;
                    objPRL220.CheckPoint4 = PRL220.CheckPoint4;
                    objPRL220.CheckPoint5 = PRL220.CheckPoint5;
                    objPRL220.CheckPoint6 = PRL220.CheckPoint6;
                    objPRL220.QCRemarks = PRL220.QCRemarks;
                    objPRL220.Result = PRL220.Result;
                    objPRL220.ReqWidth = PRL220.ReqWidth;
                    objPRL220.ReqDepth = PRL220.ReqDepth;
                    objPRL220.AllowedOffset = PRL220.AllowedOffset;
                    objPRL220.Note1 = PRL220.Note1;
                    objPRL220.Note2 = PRL220.Note2;
                    objPRL220.Note3 = PRL220.Note3;

                    objPRL220.EditedBy = UserName;
                    objPRL220.EditedOn = DateTime.Now;


                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL220.ActFilledBy = UserName;
                            objPRL220.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL220.ReqFilledBy = UserName;
                            objPRL220.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL220.HeaderId;
                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL220 objPRL220 = db.PRL220.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL220 != null && string.IsNullOrWhiteSpace(objPRL220.ProtocolNo))
                    {
                        db.PRL220.Remove(objPRL220);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL220.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL221> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL221> lstAddPRL221 = new List<PRL221>();
                List<PRL221> lstDeletePRL221 = new List<PRL221>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL221 obj = db.PRL221.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL221 obj = new PRL221();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL221.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL221.Count > 0)
                {
                    db.PRL221.AddRange(lstAddPRL221);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL221 = db.PRL221.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL221 = db.PRL221.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL221.Count > 0)
                {
                    db.PRL221.RemoveRange(lstDeletePRL221);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL222> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL222> lstAddPRL222 = new List<PRL222>();
                List<PRL222> lstDeletePRL222 = new List<PRL222>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL222 obj = db.PRL222.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL222();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActArcLength = item.ActArcLength;
                        if (isAdded)
                        {
                            lstAddPRL222.Add(obj);
                        }
                    }
                }
                if (lstAddPRL222.Count > 0)
                {
                    db.PRL222.AddRange(lstAddPRL222);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL222 = db.PRL222.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL222 = db.PRL222.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL222.Count > 0)
                {
                    db.PRL222.RemoveRange(lstDeletePRL222);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL223> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL223> lstAddPRL223 = new List<PRL223>();
                List<PRL223> lstDeletePRL223 = new List<PRL223>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL223 obj = db.PRL223.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL223();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        if (!string.IsNullOrWhiteSpace(item.TotalHeight1))
                        {
                            obj.TotalHeight1 = item.TotalHeight1;
                            obj.TotalHeight2 = item.TotalHeight2;
                        }
                        if (isAdded)
                        {
                            lstAddPRL223.Add(obj);
                        }
                    }
                    if (lstAddPRL223.Count > 0)
                    {
                        db.PRL223.AddRange(lstAddPRL223);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL223 = db.PRL223.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL223 = db.PRL223.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL223.Count > 0)
                {
                    db.PRL223.RemoveRange(lstDeletePRL223);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL224> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL224> lstAddPRL224 = new List<PRL224>();
                List<PRL224> lstDeletePRL224 = new List<PRL224>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL224 obj = db.PRL224.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL224();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.TopPeakIn = item.TopPeakIn;
                        obj.TopPeakOut = item.TopPeakOut;
                        obj.MidPeakIn = item.MidPeakIn;
                        obj.MidPeakOut = item.MidPeakOut;
                        obj.BottonPeakIn = item.BottonPeakIn;
                        obj.BottonPeakOut = item.BottonPeakOut;
                        obj.ActMin = item.ActMin;
                        obj.ActMax = item.ActMax;
                        if (isAdded)
                        {
                            lstAddPRL224.Add(obj);
                        }
                    }

                    if (lstAddPRL224.Count > 0)
                    {
                        db.PRL224.AddRange(lstAddPRL224);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL224 = db.PRL224.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL224 = db.PRL224.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL224.Count > 0)
                {
                    db.PRL224.RemoveRange(lstDeletePRL224);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //#region Line5 Linkage
        //[HttpPost]
        //public JsonResult SaveProtocolLine5Linkage(List<PRL224_2> lst, int HeaderId)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        List<PRL224_2> lstAddPRL224_2 = new List<PRL224_2>();
        //        List<PRL224_2> lstDeletePRL224_2 = new List<PRL224_2>();
        //        if (lst != null)
        //        {
        //            foreach (var item in lst)
        //            {
        //                bool isAdded = false;
        //                PRL224_2 obj = db.PRL224_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
        //                if (obj == null)
        //                {
        //                    obj = new PRL224_2();
        //                    obj.HeaderId = item.HeaderId;
        //                    obj.CreatedBy = objClsLoginInfo.UserName;
        //                    obj.CreatedOn = DateTime.Now;
        //                    isAdded = true;
        //                }
        //                else
        //                {
        //                    obj.EditedBy = objClsLoginInfo.UserName;
        //                    obj.EditedOn = DateTime.Now;
        //                }
        //                obj.ActMin = item.ActMin;
        //                obj.ActMax = item.ActMax;

        //                if (isAdded)
        //                {
        //                    lstAddPRL224_2.Add(obj);
        //                }
        //            }

        //            if (lstAddPRL224_2.Count > 0)
        //            {
        //                db.PRL224_2.AddRange(lstAddPRL224_2);
        //            }

        //            var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
        //            lstDeletePRL224_2 = db.PRL224_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
        //        }
        //        else
        //        {
        //            lstDeletePRL224_2 = db.PRL224_2.Where(x => x.HeaderId == HeaderId).ToList();
        //        }
        //        if (lstDeletePRL224_2.Count > 0)
        //        {
        //            db.PRL224_2.RemoveRange(lstDeletePRL224_2);
        //        }
        //        db.SaveChanges();
        //        objResponseMsg.Key = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        //#endregion

        #region Line6 Linkage

        [HttpPost]
        public JsonResult SaveProtocolLine6Linkage(List<PRL224_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL224_3> lstAddPRL224_3 = new List<PRL224_3>();
                List<PRL224_3> lstDeletePRL224_3 = new List<PRL224_3>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL224_3 obj = db.PRL224_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL224_3 objFecthSeam = new PRL224_3();
                        if (obj == null)
                        {
                            obj = new PRL224_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL224_3.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                        }
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNo = item.SpotNo;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRL224_3.Add(obj);
                        }
                    }
                    if (lstAddPRL224_3.Count > 0)
                    {
                        db.PRL224_3.AddRange(lstAddPRL224_3);
                    }
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL224_3 = db.PRL224_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL224_3 = db.PRL224_3.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL224_3.Count > 0)
                {
                    db.PRL224_3.RemoveRange(lstDeletePRL224_3);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}