﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol047Controller : clsBase
    {
        // GET: PROTOCOL/Protocol047
        string ControllerURL = "/PROTOCOL/Protocol047/";
        string Title = "WELD VISUAL & DIMENSION REPORT FOR NOZZLE";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO230 objPRO230 = new PRO230();
            if (!id.HasValue)
            {
                try
                {
                    objPRO230.ProtocolNo = string.Empty;
                    objPRO230.CreatedBy = objClsLoginInfo.UserName;
                    objPRO230.CreatedOn = DateTime.Now;


                    #region OUT OF ROUNDNESS
                    objPRO230.PRO231.Add(new PRO231
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO230.PRO231.Add(new PRO231
                    {
                        Orientation = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO230.PRO231.Add(new PRO231
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO230.PRO231.Add(new PRO231
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO230.PRO231.Add(new PRO231
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO230.PRO231.Add(new PRO231
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO230.PRO231.Add(new PRO231
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO230.PRO231.Add(new PRO231
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO230.Add(objPRO230);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO230.HeaderId;
            }
            else
            {
                objPRO230 = db.PRO230.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstelevation = new List<string> { "T.L.", "REF.LINE", "SHELL WEP" };
            List<string> lstmaxtiltin = new List<string> { "DEG", "MM" };
            List<string> lstHeightFromDrop = new List<string> { "VESSEL C.L.", "SHELL OD" };


            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.TypesOfElevation = lstelevation.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.MAXTILTIN = lstmaxtiltin.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.HeightFromDrop = lstHeightFromDrop.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO231> lstPRO231 = db.PRO231.Where(x => x.HeaderId == objPRO230.HeaderId).ToList();
            List<PRO232> lstPRO232 = db.PRO232.Where(x => x.HeaderId == objPRO230.HeaderId).ToList();


            ViewBag.lstPRO231 = lstPRO231;
            ViewBag.lstPRO232 = lstPRO232;



            #endregion

            return View(objPRO230);
        }
        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO230 PRO230)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO230.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO230 objPRO230 = db.PRO230.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO230.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO230.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO230.ProtocolNo = PRO230.ProtocolNo;


                        objPRO230.BoltHolesStraddled = PRO230.BoltHolesStraddled;
                        objPRO230.TolBoltHoles = PRO230.TolBoltHoles;
                        objPRO230.ActBoltHoles = PRO230.ActBoltHoles;
                        objPRO230.MaxTiltIn = PRO230.MaxTiltIn;
                        objPRO230.TolMaxTiltIn = PRO230.TolMaxTiltIn;
                        objPRO230.ActMaxTiltIn = PRO230.ActMaxTiltIn;
                        objPRO230.ReqOffset = PRO230.ReqOffset;
                        objPRO230.ActOffsetMin = PRO230.ActOffsetMin;
                        objPRO230.ActOffsetMax = PRO230.ActOffsetMax;
                        objPRO230.DisBetweenPairNozzles = PRO230.DisBetweenPairNozzles;
                        objPRO230.ReqDisBetween = PRO230.ReqDisBetween;
                        objPRO230.ActDisBetween = PRO230.ActDisBetween;
                        objPRO230.ReqOrientationOfNozzle = PRO230.ReqOrientationOfNozzle;
                        objPRO230.CircDistFrom = PRO230.CircDistFrom;
                        objPRO230.ReqReqOrientation = PRO230.ReqReqOrientation;
                        objPRO230.ActReqOrientation = PRO230.ActReqOrientation;
                        objPRO230.ElevationOfNozzleFrom = PRO230.ElevationOfNozzleFrom;
                        objPRO230.ReqElevationOfNozzleFrom = PRO230.ReqElevationOfNozzleFrom;
                        objPRO230.ActElevationOfNozzleFrom = PRO230.ActElevationOfNozzleFrom;
                        objPRO230.HeightFromVessel = PRO230.HeightFromVessel;
                        objPRO230.HeightFromDrop = PRO230.HeightFromDrop;
                        objPRO230.ReqHeightFrom = PRO230.ReqHeightFrom;
                        objPRO230.ActheightFrom = PRO230.ActheightFrom;
                        objPRO230.OffsetDistanceOfNozzles = PRO230.OffsetDistanceOfNozzles;
                        objPRO230.ReqOffsetDistanceOfNozzles = PRO230.ReqOffsetDistanceOfNozzles;
                        objPRO230.ActOffsetDistanceOfNozzles = PRO230.ActOffsetDistanceOfNozzles;
                        objPRO230.CheckPoint1 = PRO230.CheckPoint1;
                        objPRO230.CheckPoint2 = PRO230.CheckPoint2;
                        objPRO230.CheckPoint2_2 = PRO230.CheckPoint2_2;
                        objPRO230.CheckPoint4 = PRO230.CheckPoint4;
                        objPRO230.CheckPoint5 = PRO230.CheckPoint5;
                        objPRO230.CheckPoint6 = PRO230.CheckPoint6;
                        objPRO230.CheckPoint7 = PRO230.CheckPoint7;
                        objPRO230.QCRemarks = PRO230.QCRemarks;
                        objPRO230.Result = PRO230.Result;
                        objPRO230.OutOfRoundness = PRO230.OutOfRoundness;
                        objPRO230.OutByOutOfRoundness = PRO230.OutByOutOfRoundness;
                        objPRO230.ReqCladStripingWidth = PRO230.ReqCladStripingWidth;
                        objPRO230.ReqCladStripingDepth = PRO230.ReqCladStripingDepth;
                        objPRO230.ReqCladStripingOffset = PRO230.ReqCladStripingOffset;
                        objPRO230.Note1 = PRO230.Note1;
                        objPRO230.Note2 = PRO230.Note2;
                        objPRO230.Note3 = PRO230.Note3;
                        objPRO230.SpotNoClad = PRO230.SpotNoClad;


                        objPRO230.EditedBy = objClsLoginInfo.UserName;
                        objPRO230.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO230.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO230 objPRO230 = db.PRO230.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO230 != null && string.IsNullOrWhiteSpace(objPRO230.ProtocolNo))
                    {
                        db.PRO230.Remove(objPRO230);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO230.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO231> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO231> lstAddPRO231 = new List<PRO231>();
                List<PRO231> lstDeletePRO231 = new List<PRO231>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO231 obj = db.PRO231.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.AtNozzleLocation = item.AtNozzleLocation;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO231 obj = new PRO231();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.AtNozzleLocation = item.AtNozzleLocation;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO231.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO231.Count > 0)
                    {
                        db.PRO231.AddRange(lstAddPRO231);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO231 = db.PRO231.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO231.Count > 0)
                    {
                        db.PRO231.RemoveRange(lstDeletePRO231);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO231 = db.PRO231.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO231.Count > 0)
                    {
                        db.PRO231.RemoveRange(lstDeletePRO231);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details

        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO232> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO232> lstAddPRO232 = new List<PRO232>();
                List<PRO232> lstDeletePRO232 = new List<PRO232>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO232 obj = db.PRO232.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO232();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Width = item.Width;
                        obj.Depth = item.Depth;
                        obj.Offset = item.Offset;

                        if (isAdded)
                        {
                            lstAddPRO232.Add(obj);
                        }
                    }
                    if (lstAddPRO232.Count > 0)
                    {
                        db.PRO232.AddRange(lstAddPRO232);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO232 = db.PRO232.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO232.Count > 0)
                    {
                        db.PRO232.RemoveRange(lstDeletePRO232);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO232 = db.PRO232.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO232.Count > 0)
                    {
                        db.PRO232.RemoveRange(lstDeletePRO232);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)

        {

            PRL230 objPRL230 = new PRL230();
            objPRL230 = db.PRL230.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL230 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL230.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }


            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstelevation = new List<string> { "T.L.", "REF.LINE", "SHELL WEP" };
            List<string> lstmaxtiltin = new List<string> { "DEG", "MM" };
            List<string> lstHeightFromDrop = new List<string> { "VESSEL C.L", "SHELL OD" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.TypesOfElevation = lstelevation.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.MAXTILTIN = lstmaxtiltin.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.HeightFromDrop = lstHeightFromDrop.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL231> lstPRL231 = db.PRL231.Where(x => x.HeaderId == objPRL230.HeaderId).ToList();
            List<PRL232> lstPRL232 = db.PRL232.Where(x => x.HeaderId == objPRL230.HeaderId).ToList();


            ViewBag.lstPRL231 = lstPRL231;
            ViewBag.lstPRL232 = lstPRL232;



            #endregion


            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL230.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL230.ActFilledBy) && (objPRL230.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL230.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL230);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL230 objPRL230 = new PRL230();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL230 = db.PRL230.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL230).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL230 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL230.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstelevation = new List<string> { "T.L.", "REF.LINE", "SHELL WEP" };
            List<string> lstmaxtiltin = new List<string> { "DEG", "MM" };
            List<string> lstHeightFromDrop = new List<string> { "VESSEL C.L", "SHELL OD" };

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var TypesOfElevation = lstelevation.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var MAXTILTIN = lstmaxtiltin.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var HeightFromDrop = lstHeightFromDrop.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL231> lstPRL231 = db.PRL231.Where(x => x.HeaderId == objPRL230.HeaderId).ToList();
            List<PRL232> lstPRL232 = db.PRL232.Where(x => x.HeaderId == objPRL230.HeaderId).ToList();

            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL230.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL230.ActFilledBy) && (objPRL230.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL230.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    MAXTILTIN = MAXTILTIN,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    HeightFromDrop = HeightFromDrop,
                    TypesOfElevation = TypesOfElevation,

                    objPRL230 = objPRL230,

                    lstPRL231 = lstPRL231,
                    lstPRL232 = lstPRL232

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL230 prl230, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl230.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL230 objPRL230 = db.PRL230.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL230.DrawingRevisionNo = prl230.DrawingRevisionNo;
                    objPRL230.DrawingNo = prl230.DrawingNo;
                    objPRL230.DCRNo = prl230.DCRNo;
                    objPRL230.ProtocolNo = prl230.ProtocolNo;

                    objPRL230.BoltHolesStraddled = prl230.BoltHolesStraddled;
                    objPRL230.TolBoltHoles = prl230.TolBoltHoles;
                    objPRL230.ActBoltHoles = prl230.ActBoltHoles;
                    objPRL230.MaxTiltIn = prl230.MaxTiltIn;
                    objPRL230.TolMaxTiltIn = prl230.TolMaxTiltIn;
                    objPRL230.ActMaxTiltIn = prl230.ActMaxTiltIn;
                    objPRL230.ReqOffset = prl230.ReqOffset;
                    objPRL230.ActOffsetMin = prl230.ActOffsetMin;
                    objPRL230.ActOffsetMax = prl230.ActOffsetMax;
                    objPRL230.DisBetweenPairNozzles = prl230.DisBetweenPairNozzles;
                    objPRL230.ReqDisBetween = prl230.ReqDisBetween;
                    objPRL230.ActDisBetween = prl230.ActDisBetween;
                    objPRL230.ReqOrientationOfNozzle = prl230.ReqOrientationOfNozzle;
                    objPRL230.CircDistFrom = prl230.CircDistFrom;
                    objPRL230.ReqReqOrientation = prl230.ReqReqOrientation;
                    objPRL230.ActReqOrientation = prl230.ActReqOrientation;
                    objPRL230.ElevationOfNozzleFrom = prl230.ElevationOfNozzleFrom;
                    objPRL230.ReqElevationOfNozzleFrom = prl230.ReqElevationOfNozzleFrom;
                    objPRL230.ActElevationOfNozzleFrom = prl230.ActElevationOfNozzleFrom;
                    objPRL230.HeightFromVessel = prl230.HeightFromVessel;
                    objPRL230.HeightFromDrop = prl230.HeightFromDrop;
                    objPRL230.ReqHeightFrom = prl230.ReqHeightFrom;
                    objPRL230.ActheightFrom = prl230.ActheightFrom;
                    objPRL230.OffsetDistanceOfNozzles = prl230.OffsetDistanceOfNozzles;
                    objPRL230.ReqOffsetDistanceOfNozzles = prl230.ReqOffsetDistanceOfNozzles;
                    objPRL230.ActOffsetDistanceOfNozzles = prl230.ActOffsetDistanceOfNozzles;
                    objPRL230.CheckPoint1 = prl230.CheckPoint1;
                    objPRL230.CheckPoint2 = prl230.CheckPoint2;
                    objPRL230.CheckPoint2_2 = prl230.CheckPoint2_2;
                    objPRL230.CheckPoint4 = prl230.CheckPoint4;
                    objPRL230.CheckPoint5 = prl230.CheckPoint5;
                    objPRL230.CheckPoint6 = prl230.CheckPoint6;
                    objPRL230.CheckPoint7 = prl230.CheckPoint7;
                    objPRL230.QCRemarks = prl230.QCRemarks;
                    objPRL230.Result = prl230.Result;
                    objPRL230.OutOfRoundness = prl230.OutOfRoundness;
                    objPRL230.OutByOutOfRoundness = prl230.OutByOutOfRoundness;
                    objPRL230.ReqCladStripingWidth = prl230.ReqCladStripingWidth;
                    objPRL230.ReqCladStripingDepth = prl230.ReqCladStripingDepth;
                    objPRL230.ReqCladStripingOffset = prl230.ReqCladStripingOffset;
                    objPRL230.Note1 = prl230.Note1;
                    objPRL230.Note2 = prl230.Note2;
                    objPRL230.Note3 = prl230.Note3;
                    objPRL230.Note1 = prl230.Note1;
                    objPRL230.Note2 = prl230.Note2;
                    objPRL230.Note3 = prl230.Note3;
                    objPRL230.SpotNoClad = prl230.SpotNoClad;

                    objPRL230.EditedBy = UserName;
                    objPRL230.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL230.ActFilledBy = UserName;
                            objPRL230.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL230.ReqFilledBy = UserName;
                            objPRL230.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL230.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL230 objPRL230 = db.PRL230.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL230 != null && string.IsNullOrWhiteSpace(objPRL230.ProtocolNo))
                    {
                        db.PRL230.Remove(objPRL230);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL230.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL231> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL231> lstAddPRL231 = new List<PRL231>();
                List<PRL231> lstDeletePRL231 = new List<PRL231>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL231 obj = db.PRL231.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.AtNozzleLocation = item.AtNozzleLocation;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL231 obj = new PRL231();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.AtNozzleLocation = item.AtNozzleLocation;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL231.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL231.Count > 0)
                    {
                        db.PRL231.AddRange(lstAddPRL231);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL231 = db.PRL231.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL231.Count > 0)
                    {
                        db.PRL231.RemoveRange(lstDeletePRL231);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL231 = db.PRL231.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL231.Count > 0)
                    {
                        db.PRL231.RemoveRange(lstDeletePRL231);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details

        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL232> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL232> lstAddPRL232 = new List<PRL232>();
                List<PRL232> lstDeletePRL232 = new List<PRL232>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL232 obj = db.PRL232.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL232();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Width = item.Width;
                        obj.Depth = item.Depth;
                        obj.Offset = item.Offset;

                        if (isAdded)
                        {
                            lstAddPRL232.Add(obj);
                        }
                    }
                    if (lstAddPRL232.Count > 0)
                    {
                        db.PRL232.AddRange(lstAddPRL232);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL232 = db.PRL232.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL232.Count > 0)
                    {
                        db.PRL232.RemoveRange(lstDeletePRL232);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL232 = db.PRL232.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL232.Count > 0)
                    {
                        db.PRL232.RemoveRange(lstDeletePRL232);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}