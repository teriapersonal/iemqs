﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol059Controller : clsBase
    {
        // GET: PROTOCOL/Protocol059
        string ControllerURL = "/PROTOCOL/Protocol059/";
        string Title = "SET-UP AND DIMENSION REPORT FOR  LONGITUDINAL SEAM OF RING IN SEGMENT STAGE";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO290 objPRO290 = new PRO290();
            if (!id.HasValue)
            {
                try
                {
                    objPRO290.ProtocolNo = string.Empty;
                    objPRO290.CreatedBy = objClsLoginInfo.UserName;
                    objPRO290.CreatedOn = DateTime.Now;

                    #region  Orientation | PRO291
                    objPRO290.PRO291.Add(new PRO291
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO290.PRO291.Add(new PRO291
                    {
                        Orientation = "45°-222°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO290.PRO291.Add(new PRO291
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO290.PRO291.Add(new PRO291
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    db.PRO290.Add(objPRO290);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO290.HeaderId;
            }
            else
            {
                objPRO290 = db.PRO290.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            #region bind lines
            List<PRO291> lstPRO291 = db.PRO291.Where(x => x.HeaderId == objPRO290.HeaderId).ToList();
            List<PRO292> lstPRO292 = db.PRO292.Where(x => x.HeaderId == objPRO290.HeaderId).ToList();
            List<PRO293> lstPRO293 = db.PRO293.Where(x => x.HeaderId == objPRO290.HeaderId).ToList();

            ViewBag.lstPRO291 = lstPRO291;
            ViewBag.lstPRO292 = lstPRO292;
            ViewBag.lstPRO293 = lstPRO293;

            #endregion

            return View(objPRO290);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO290 PRO290)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO290.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO290 objPRO290 = db.PRO290.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO290.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO290.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO290.ProtocolNo = PRO290.ProtocolNo;
                        objPRO290.ReqOutsideDiameter = PRO290.ReqOutsideDiameter;
                        objPRO290.ReqInsideDiameter = PRO290.ReqInsideDiameter;
                        objPRO290.ReqRootGap = PRO290.ReqRootGap;
                        objPRO290.ReqRootFace = PRO290.ReqRootFace;
                        objPRO290.ReqTopSideWepAngle = PRO290.ReqTopSideWepAngle;
                        objPRO290.ReqBottomSideWepAngle = PRO290.ReqBottomSideWepAngle;
                        objPRO290.ReqOffset = PRO290.ReqOffset;
                        objPRO290.CheckPoint1 = PRO290.CheckPoint1;
                        objPRO290.CheckPoint2 = PRO290.CheckPoint2;
                        objPRO290.CheckPoint3 = PRO290.CheckPoint3;
                        objPRO290.CheckPoint4 = PRO290.CheckPoint4;
                        objPRO290.CheckPoint5 = PRO290.CheckPoint5;
                        objPRO290.CheckPoint6 = PRO290.CheckPoint6;
                        objPRO290.CheckPoint8 = PRO290.CheckPoint8;
                        objPRO290.CheckPoint8_2 = PRO290.CheckPoint8_2;
                        objPRO290.QCRemarks = PRO290.QCRemarks;
                        objPRO290.Result = PRO290.Result;

                        objPRO290.EditedBy = objClsLoginInfo.UserName;
                        objPRO290.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO290.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO290 objPRO290 = db.PRO290.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO290 != null && string.IsNullOrWhiteSpace(objPRO290.ProtocolNo))
                    {
                        db.PRO290.Remove(objPRO290);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO290.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO291> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO291> lstAddPRO291 = new List<PRO291>();
                List<PRO291> lstDeletePRO291 = new List<PRO291>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO291 obj = db.PRO291.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO291();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActOutsideDiameter = item.ActOutsideDiameter;
                        obj.ActInsideDiameter = item.ActInsideDiameter;

                        if (isAdded)
                        {
                            lstAddPRO291.Add(obj);
                        }
                    }
                    if (lstAddPRO291.Count > 0)
                    {
                        db.PRO291.AddRange(lstAddPRO291);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO291 = db.PRO291.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO291.Count > 0)
                    {
                        db.PRO291.RemoveRange(lstDeletePRO291);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO291 = db.PRO291.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO291.Count > 0)
                    {
                        db.PRO291.RemoveRange(lstDeletePRO291);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO292> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO292> lstAddPRO292 = new List<PRO292>();
                List<PRO292> lstDeletePRO292 = new List<PRO292>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO292 obj = db.PRO292.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {

                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxTopSideWepAngle = item.ActMaxTopSideWepAngle;
                                obj.ActMaxBottomSideWepAngle = item.ActMaxBottomSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinTopSideWepAngle = item.ActMinTopSideWepAngle;
                                obj.ActMinBottomSideWepAngle = item.ActMinBottomSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO292 obj = new PRO292();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxTopSideWepAngle = item.ActMaxTopSideWepAngle;
                                obj.ActMaxBottomSideWepAngle = item.ActMaxBottomSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinTopSideWepAngle = item.ActMinTopSideWepAngle;
                                obj.ActMinBottomSideWepAngle = item.ActMinBottomSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO292.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO292.Count > 0)
                    {
                        db.PRO292.AddRange(lstAddPRO292);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO292 = db.PRO292.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO292.Count > 0)
                    {
                        db.PRO292.RemoveRange(lstDeletePRO292);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO292 = db.PRO292.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO292.Count > 0)
                    {
                        db.PRO292.RemoveRange(lstDeletePRO292);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO293> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO293> lstAddPRO293 = new List<PRO293>();
                List<PRO293> lstDeletePRO293 = new List<PRO293>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO293 obj = db.PRO293.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO293 obj = new PRO293();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO293.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO293.Count > 0)
                    {
                        db.PRO293.AddRange(lstAddPRO293);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO293 = db.PRO293.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO293.Count > 0)
                    {
                        db.PRO293.RemoveRange(lstDeletePRO293);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO293 = db.PRO293.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO293.Count > 0)
                    {
                        db.PRO293.RemoveRange(lstDeletePRO293);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region  Linkage View Code   

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL290 objPRL290 = new PRL290();
            objPRL290 = db.PRL290.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL290 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL290.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            #region bind lines
            List<PRL291> lstPRL291 = db.PRL291.Where(x => x.HeaderId == objPRL290.HeaderId).ToList();
            List<PRL292> lstPRL292 = db.PRL292.Where(x => x.HeaderId == objPRL290.HeaderId).ToList();
            List<PRL293> lstPRL293 = db.PRL293.Where(x => x.HeaderId == objPRL290.HeaderId).ToList();

            ViewBag.lstPRL291 = lstPRL291;
            ViewBag.lstPRL292 = lstPRL292;
            ViewBag.lstPRL293 = lstPRL293;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL290.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL290.ActFilledBy) && (objPRL290.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL290.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL290);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL290 objPRL290 = new PRL290();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL290 = db.PRL290.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL290).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL290 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL290.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            #region bind lines
            List<PRL291> lstPRL291 = db.PRL291.Where(x => x.HeaderId == objPRL290.HeaderId).ToList();
            List<PRL292> lstPRL292 = db.PRL292.Where(x => x.HeaderId == objPRL290.HeaderId).ToList();
            List<PRL293> lstPRL293 = db.PRL293.Where(x => x.HeaderId == objPRL290.HeaderId).ToList();

            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL290.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL290.ActFilledBy) && (objPRL290.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL290.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    objPRL290 = objPRL290,

                    lstPRL291 = lstPRL291,
                    lstPRL292 = lstPRL292,
                    lstPRL293 = lstPRL293

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL290 PRL290, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL290.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL290 objPRL290 = db.PRL290.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL290.DrawingRevisionNo = PRL290.DrawingRevisionNo;
                    objPRL290.DrawingNo = PRL290.DrawingNo;
                    objPRL290.DCRNo = PRL290.DCRNo;
                    objPRL290.ProtocolNo = PRL290.ProtocolNo;
                    objPRL290.ReqOutsideDiameter = PRL290.ReqOutsideDiameter;
                    objPRL290.ReqInsideDiameter = PRL290.ReqInsideDiameter;
                    objPRL290.ReqRootGap = PRL290.ReqRootGap;
                    objPRL290.ReqRootFace = PRL290.ReqRootFace;
                    objPRL290.ReqTopSideWepAngle = PRL290.ReqTopSideWepAngle;
                    objPRL290.ReqBottomSideWepAngle = PRL290.ReqBottomSideWepAngle;
                    objPRL290.ReqOffset = PRL290.ReqOffset;
                    objPRL290.CheckPoint1 = PRL290.CheckPoint1;
                    objPRL290.CheckPoint2 = PRL290.CheckPoint2;
                    objPRL290.CheckPoint3 = PRL290.CheckPoint3;
                    objPRL290.CheckPoint4 = PRL290.CheckPoint4;
                    objPRL290.CheckPoint5 = PRL290.CheckPoint5;
                    objPRL290.CheckPoint6 = PRL290.CheckPoint6;
                    objPRL290.CheckPoint8 = PRL290.CheckPoint8;
                    objPRL290.CheckPoint8_2 = PRL290.CheckPoint8_2;

                    objPRL290.QCRemarks = PRL290.QCRemarks;
                    objPRL290.Result = PRL290.Result;

                    objPRL290.EditedBy = UserName;
                    objPRL290.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL290.ActFilledBy = UserName;
                            objPRL290.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL290.ReqFilledBy = UserName;
                            objPRL290.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL290.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL290 objPRL290 = db.PRL290.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL290 != null && string.IsNullOrWhiteSpace(objPRL290.ProtocolNo))
                    {
                        db.PRL290.Remove(objPRL290);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL290.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL291> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL291> lstAddPRL291 = new List<PRL291>();
                List<PRL291> lstDeletePRL291 = new List<PRL291>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL291 obj = db.PRL291.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL291();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActOutsideDiameter = item.ActOutsideDiameter;
                        obj.ActInsideDiameter = item.ActInsideDiameter;

                        if (isAdded)
                        {
                            lstAddPRL291.Add(obj);
                        }
                    }
                }
                if (lstAddPRL291.Count > 0)
                {
                    db.PRL291.AddRange(lstAddPRL291);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL291 = db.PRL291.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL291 = db.PRL291.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL291.Count > 0)
                {
                    db.PRL291.RemoveRange(lstDeletePRL291);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL292> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL292> lstAddPRL292 = new List<PRL292>();
                List<PRL292> lstDeletePRL292 = new List<PRL292>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL292 obj = db.PRL292.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {

                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxTopSideWepAngle = item.ActMaxTopSideWepAngle;
                                obj.ActMaxBottomSideWepAngle = item.ActMaxBottomSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinTopSideWepAngle = item.ActMinTopSideWepAngle;
                                obj.ActMinBottomSideWepAngle = item.ActMinBottomSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL292 obj = new PRL292();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxTopSideWepAngle = item.ActMaxTopSideWepAngle;
                                obj.ActMaxBottomSideWepAngle = item.ActMaxBottomSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinTopSideWepAngle = item.ActMinTopSideWepAngle;
                                obj.ActMinBottomSideWepAngle = item.ActMinBottomSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL292.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL292.Count > 0)
                    {
                        db.PRL292.AddRange(lstAddPRL292);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL292 = db.PRL292.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL292.Count > 0)
                    {
                        db.PRL292.RemoveRange(lstDeletePRL292);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL292 = db.PRL292.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL292.Count > 0)
                    {
                        db.PRL292.RemoveRange(lstDeletePRL292);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL293> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL293> lstAddPRL293 = new List<PRL293>();
                List<PRL293> lstDeletePRL293 = new List<PRL293>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL293 obj = db.PRL293.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL293 obj = new PRL293();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.IdentificationMarking))
                            {
                                obj.IdentificationMarking = item.IdentificationMarking;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL293.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL293.Count > 0)
                {
                    db.PRL293.AddRange(lstAddPRL293);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL293 = db.PRL293.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL293 = db.PRL293.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL293.Count > 0)
                {
                    db.PRL293.RemoveRange(lstDeletePRL293);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}