﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol072Controller : clsBase
    {
        // GET: PROTOCOL/Protocol072
        string ControllerURL = "/PROTOCOL/Protocol072/";
        string Title = "PNEUMATIC TEST REPORT (PRESSURE TEST)";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO355 objPRO355 = new PRO355();
            if (!id.HasValue)
            {
                try
                {
                    objPRO355.ProtocolNo = string.Empty;
                    objPRO355.CreatedBy = objClsLoginInfo.UserName;
                    objPRO355.CreatedOn = DateTime.Now;

                    #region Remarks

                    objPRO355.QCRemarks = "NO PRESSURE DROP AND NO LEAKAGE OBSERVED.";

                    #endregion

                    db.PRO355.Add(objPRO355);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO355.HeaderId;
            }
            else
            {
                objPRO355 = db.PRO355.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO356> lstPRO356 = db.PRO356.Where(x => x.HeaderId == objPRO355.HeaderId).ToList();

            ViewBag.lstPRO356 = lstPRO356;

            #endregion

            return View(objPRO355);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO355 PRO355)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO355.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO355 objPRO355 = db.PRO355.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO355.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO355.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO355.ProtocolNo = PRO355.ProtocolNo;
                        objPRO355.ProcedureNo = PRO355.ProcedureNo;
                        objPRO355.TestDescription = PRO355.TestDescription;
                        objPRO355.DateOfTest = PRO355.DateOfTest;
                        objPRO355.TestPosition = PRO355.TestPosition;
                        objPRO355.ReqTestPressure = PRO355.ReqTestPressure;
                        objPRO355.ReqHoldingTime = PRO355.ReqHoldingTime;
                        objPRO355.ReqTestTemperature = PRO355.ReqTestTemperature;
                        objPRO355.ActTestPressure = PRO355.ActTestPressure;
                        objPRO355.ActHoldingTime = PRO355.ActHoldingTime;
                        objPRO355.ActTestTemperature = PRO355.ActTestTemperature;
                        objPRO355.GraphAttached = PRO355.GraphAttached;
                        objPRO355.QCRemarks = PRO355.QCRemarks;
                        objPRO355.Result = PRO355.Result;

                        objPRO355.EditedBy = objClsLoginInfo.UserName;
                        objPRO355.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO355.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO355 objPRO355 = db.PRO355.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO355 != null && string.IsNullOrWhiteSpace(objPRO355.ProtocolNo))
                    {
                        db.PRO355.Remove(objPRO355);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO355.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO356> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO356> lstAddPRO356 = new List<PRO356>();
                List<PRO356> lstDeletePRO356 = new List<PRO356>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO356 obj = db.PRO356.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO356();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.GaugeSerialNumber = item.GaugeSerialNumber;
                        obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;

                        if (isAdded)
                        {
                            lstAddPRO356.Add(obj);
                        }
                    }
                    if (lstAddPRO356.Count > 0)
                    {
                        db.PRO356.AddRange(lstAddPRO356);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO356 = db.PRO356.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO356.Count > 0)
                    {
                        db.PRO356.RemoveRange(lstDeletePRO356);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO356 = db.PRO356.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO356.Count > 0)
                    {
                        db.PRO356.RemoveRange(lstDeletePRO356);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL355 objPRL355 = new PRL355();
            objPRL355 = db.PRL355.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL355 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL355.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL356> lstPRL356 = db.PRL356.Where(x => x.HeaderId == objPRL355.HeaderId).ToList();

            ViewBag.lstPRL356 = lstPRL356;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL355.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL355.ActFilledBy) && (objPRL355.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL355.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL355);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL355 objPRL355 = new PRL355();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL355 = db.PRL355.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL355).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL355 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL355.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            var YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL356> lstPRL356 = db.PRL356.Where(x => x.HeaderId == objPRL355.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL355.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL355.ActFilledBy) && (objPRL355.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL355.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoNAEnum = YesNoNAEnum,

                    objPRL355 = objPRL355,

                    lstPRL356 = lstPRL356

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL355 PRL355, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL355.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL355 objPRL355 = db.PRL355.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL355.DrawingRevisionNo = PRL355.DrawingRevisionNo;
                    objPRL355.DrawingNo = PRL355.DrawingNo;
                    objPRL355.DCRNo = PRL355.DCRNo;
                    objPRL355.ProtocolNo = PRL355.ProtocolNo != null ? PRL355.ProtocolNo : "";
                    objPRL355.ProcedureNo = PRL355.ProcedureNo;
                    objPRL355.TestDescription = PRL355.TestDescription;
                    objPRL355.DateOfTest = PRL355.DateOfTest;
                    objPRL355.TestPosition = PRL355.TestPosition;
                    objPRL355.ReqTestPressure = PRL355.ReqTestPressure;
                    objPRL355.ReqHoldingTime = PRL355.ReqHoldingTime;
                    objPRL355.ReqTestTemperature = PRL355.ReqTestTemperature;
                    objPRL355.ActTestPressure = PRL355.ActTestPressure;
                    objPRL355.ActHoldingTime = PRL355.ActHoldingTime;
                    objPRL355.ActTestTemperature = PRL355.ActTestTemperature;
                    objPRL355.GraphAttached = PRL355.GraphAttached;
                    objPRL355.QCRemarks = PRL355.QCRemarks;
                    objPRL355.Result = PRL355.Result;

                    objPRL355.EditedBy = UserName;
                    objPRL355.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL355.ActFilledBy = UserName;
                            objPRL355.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL355.ReqFilledBy = UserName;
                            objPRL355.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL355.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL355 objPRL355 = db.PRL355.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL355 != null && string.IsNullOrWhiteSpace(objPRL355.ProtocolNo))
                    {
                        db.PRL355.Remove(objPRL355);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL355.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL356> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL356> lstAddPRL356 = new List<PRL356>();
                List<PRL356> lstDeletePRL356 = new List<PRL356>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL356 obj = db.PRL356.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL356();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.GaugeSerialNumber = item.GaugeSerialNumber;
                        obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;

                        if (isAdded)
                        {
                            lstAddPRL356.Add(obj);
                        }
                    }
                    if (lstAddPRL356.Count > 0)
                    {
                        db.PRL356.AddRange(lstAddPRL356);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL356 = db.PRL356.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL356.Count > 0)
                    {
                        db.PRL356.RemoveRange(lstDeletePRL356);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL356 = db.PRL356.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL356.Count > 0)
                    {
                        db.PRL356.RemoveRange(lstDeletePRL356);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}