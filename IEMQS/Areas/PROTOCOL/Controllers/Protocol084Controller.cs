﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol084Controller : clsBase
    {
        // GET: PROTOCOL/Protocol084

        string ControllerURL = "/PROTOCOL/Protocol084/";
        string Title = "REPORT FOR VISUAL AND DIMENSION OF SKIRT TEMPLATE";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO415 objPRO415 = new PRO415();

            if (!id.HasValue)
            {
                try
                {
                    objPRO415.ProtocolNo = string.Empty;
                    objPRO415.CreatedBy = objClsLoginInfo.UserName;
                    objPRO415.CreatedOn = DateTime.Now;

                    #region  INSPECTION DESCREPTION | PRO416
                    objPRO415.PRO416.Add(new PRO416
                    {
                        Description = "OUTSIDE  DIAMETER",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO416.Add(new PRO416
                    {
                        Description = "BOLT CIRCLE DIAMETER",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO416.Add(new PRO416
                    {
                        Description = "INSIDE  DIAMETER",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO416.Add(new PRO416
                    {
                        Description = "BOLT HOLE DIAMETER",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO416.Add(new PRO416
                    {
                        Description = "ORIENTATION OF NOTCHES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO416.Add(new PRO416
                    {
                        Description = "NO.OF BOLT HOLES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO416.Add(new PRO416
                    {
                        Description = "BOLT HOLE PITCH",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO416.Add(new PRO416
                    {
                        Description = "THICKNESS OF TEMPLATE RING",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO415.PRO416.Add(new PRO416
                    {
                        Description = "HEIGHT OF TEMPLATE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO416.Add(new PRO416
                    {
                        Description = "ALIGNMENT OF HOLES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO416.Add(new PRO416
                    {
                        Description = "DIAMETER OF ALIGNMENT PIN",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO416.Add(new PRO416
                    {
                        Description = "FLATNESS OF TEMPLATE(IF APPLICABLE)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });


                    #endregion

                    #region BOLT CIRCLE DIAMETER HOLE TO HOLE  | PRO417
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H1",
                        HoleToHole2 = "H17",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H2",
                        HoleToHole2 = "H18",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H3",
                        HoleToHole2 = "H19",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H4",
                        HoleToHole2 = "H20",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H5",
                        HoleToHole2 = "H21",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H6",
                        HoleToHole2 = "H22",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H7",
                        HoleToHole2 = "H23",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H8",
                        HoleToHole2 = "H24",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H9",
                        HoleToHole2 = "H25",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H10",
                        HoleToHole2 = "H26",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H11",
                        HoleToHole2 = "H27",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H12",
                        HoleToHole2 = "H28",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H13",
                        HoleToHole2 = "H29",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H14",
                        HoleToHole2 = "H30",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H15",
                        HoleToHole2 = "H31",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO417.Add(new PRO417
                    {

                        HoleToHole1 = "H16",
                        HoleToHole2 = "H32",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    #region BOLT HOLE PITCH DIMENSION | PRO418
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H1",
                        HoleToHole2 = "H2",
                        HoleToHole3 = "H17",
                        HoleToHole4 = "H18",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H2",
                        HoleToHole2 = "H3",
                        HoleToHole3 = "H18",
                        HoleToHole4 = "H19",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H3",
                        HoleToHole2 = "H4",
                        HoleToHole3 = "H19",
                        HoleToHole4 = "H20",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H4",
                        HoleToHole2 = "H5",
                        HoleToHole3 = "H20",
                        HoleToHole4 = "H21",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H5",
                        HoleToHole2 = "H6",
                        HoleToHole3 = "H21",
                        HoleToHole4 = "H22",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H6",
                        HoleToHole2 = "H7",
                        HoleToHole3 = "H22",
                        HoleToHole4 = "H23",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H7",
                        HoleToHole2 = "H8",
                        HoleToHole3 = "H23",
                        HoleToHole4 = "H24",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H8",
                        HoleToHole2 = "H9",
                        HoleToHole3 = "H24",
                        HoleToHole4 = "H25",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H9",
                        HoleToHole2 = "H10",
                        HoleToHole3 = "H25",
                        HoleToHole4 = "H26",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H10",
                        HoleToHole2 = "H11",
                        HoleToHole3 = "H26",
                        HoleToHole4 = "H27",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H11",
                        HoleToHole2 = "H12",
                        HoleToHole3 = "H27",
                        HoleToHole4 = "H28",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H12",
                        HoleToHole2 = "H13",
                        HoleToHole3 = "H28",
                        HoleToHole4 = "H29",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H13",
                        HoleToHole2 = "H14",
                        HoleToHole3 = "H29",
                        HoleToHole4 = "H30",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H14",
                        HoleToHole2 = "H15",
                        HoleToHole3 = "H30",
                        HoleToHole4 = "H31",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H15",
                        HoleToHole2 = "H16",
                        HoleToHole3 = "H31",
                        HoleToHole4 = "H32",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO418.Add(new PRO418
                    {

                        HoleToHole1 = "H16",
                        HoleToHole2 = "H17",
                        HoleToHole3 = "H32",
                        HoleToHole4 = "H1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    #region BOLT HOLE DIAMETER | PRO419
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H1",
                        Hole2 = "H17",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H2",
                        Hole2 = "H18",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H3",
                        Hole2 = "H19",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H4",
                        Hole2 = "H20",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H5",
                        Hole2 = "H21",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H6",
                        Hole2 = "H22",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H7",
                        Hole2 = "H23",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H8",
                        Hole2 = "H24",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H9",
                        Hole2 = "H25",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H10",
                        Hole2 = "H26",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H11",
                        Hole2 = "H27",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H12",
                        Hole2 = "H28",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H13",
                        Hole2 = "H29",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H14",
                        Hole2 = "H30",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H15",
                        Hole2 = "H31",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419.Add(new PRO419
                    {

                        Hole1 = "H16",
                        Hole2 = "H32",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    #region OVERALL HEIGHT OF TEMPLATE ORIENTATION | PRO419_2
                    objPRO415.PRO419_2.Add(new PRO419_2
                    {

                        Orientation = "0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_2.Add(new PRO419_2
                    {

                        Orientation = "90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_2.Add(new PRO419_2
                    {

                        Orientation = "180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_2.Add(new PRO419_2
                    {

                        Orientation = "270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });


                    #endregion

                    #region  ORIENTATION | PRO419_3
                    objPRO415.PRO419_3.Add(new PRO419_3
                    {

                        Orientation = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_3.Add(new PRO419_3
                    {

                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_3.Add(new PRO419_3
                    {

                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_3.Add(new PRO419_3
                    {

                        Orientation = "135° - 315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });


                    #endregion

                    #region DIMNSION OF ONE HOLE FROM EACH HOLES | PRO419_4
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A1",
                        HoleToHole2 = "A2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A2",
                        HoleToHole2 = "A3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A3",
                        HoleToHole2 = "A4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A4",
                        HoleToHole2 = "A5",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A5",
                        HoleToHole2 = "A6",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A6",
                        HoleToHole2 = "A7",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A7",
                        HoleToHole2 = "A8",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A8",
                        HoleToHole2 = "A9",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A9",
                        HoleToHole2 = "A10",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A10",
                        HoleToHole2 = "A11",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A11",
                        HoleToHole2 = "A12",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A12",
                        HoleToHole2 = "A13",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A13",
                        HoleToHole2 = "A14",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A14",
                        HoleToHole2 = "A15",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A15",
                        HoleToHole2 = "A16",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A16",
                        HoleToHole2 = "A17",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A17",
                        HoleToHole2 = "A18",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A18",
                        HoleToHole2 = "A19",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A19",
                        HoleToHole2 = "A20",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A20",
                        HoleToHole2 = "A21",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A21",
                        HoleToHole2 = "A22",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A22",
                        HoleToHole2 = "A23",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A23",
                        HoleToHole2 = "A24",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A24",
                        HoleToHole2 = "A25",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {
                        HoleToHole1 = "A25",
                        HoleToHole2 = "A26",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A26",
                        HoleToHole2 = "A27",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A27",
                        HoleToHole2 = "A28",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A28",
                        HoleToHole2 = "A29",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {
                        HoleToHole1 = "A29",
                        HoleToHole2 = "A30",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A30",
                        HoleToHole2 = "A31",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A31",
                        HoleToHole2 = "A32",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_4.Add(new PRO419_4
                    {

                        HoleToHole1 = "A32",
                        HoleToHole2 = "A1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    #region DIMNSION OF ONE HOLE FROM EACH HOLES | PRO419_5
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B1",
                        HoleToHole2 = "B2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B2",
                        HoleToHole2 = "B3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B3",
                        HoleToHole2 = "B4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B4",
                        HoleToHole2 = "B5",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B5",
                        HoleToHole2 = "B6",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B6",
                        HoleToHole2 = "B7",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B7",
                        HoleToHole2 = "B8",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B8",
                        HoleToHole2 = "B9",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B9",
                        HoleToHole2 = "B10",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B10",
                        HoleToHole2 = "B11",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B11",
                        HoleToHole2 = "B12",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B12",
                        HoleToHole2 = "B13",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B13",
                        HoleToHole2 = "B14",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B14",
                        HoleToHole2 = "B15",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B15",
                        HoleToHole2 = "B16",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B16",
                        HoleToHole2 = "B17",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B17",
                        HoleToHole2 = "B18",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B18",
                        HoleToHole2 = "B19",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B19",
                        HoleToHole2 = "B20",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B20",
                        HoleToHole2 = "B21",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B21",
                        HoleToHole2 = "B22",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B22",
                        HoleToHole2 = "B23",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B23",
                        HoleToHole2 = "B24",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B24",
                        HoleToHole2 = "B25",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {
                        HoleToHole1 = "B25",
                        HoleToHole2 = "B26",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B26",
                        HoleToHole2 = "B27",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B27",
                        HoleToHole2 = "B28",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B28",
                        HoleToHole2 = "B29",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {
                        HoleToHole1 = "B29",
                        HoleToHole2 = "B30",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B30",
                        HoleToHole2 = "B31",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B31",
                        HoleToHole2 = "B32",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO415.PRO419_5.Add(new PRO419_5
                    {

                        HoleToHole1 = "B32",
                        HoleToHole2 = "B1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    db.PRO415.Add(objPRO415);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO415.HeaderId;

            }
            else
            {
                objPRO415 = db.PRO415.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();


            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            #region bind lines
            List<PRO416> lstPRO416 = db.PRO416.Where(x => x.HeaderId == objPRO415.HeaderId).ToList();
            List<PRO417> lstPRO417 = db.PRO417.Where(x => x.HeaderId == objPRO415.HeaderId).ToList();
            List<PRO418> lstPRO418 = db.PRO418.Where(x => x.HeaderId == objPRO415.HeaderId).ToList();
            List<PRO419> lstPRO419 = db.PRO419.Where(x => x.HeaderId == objPRO415.HeaderId).ToList();
            List<PRO419_2> lstPRO419_2 = db.PRO419_2.Where(x => x.HeaderId == objPRO415.HeaderId).ToList();
            List<PRO419_3> lstPRO419_3 = db.PRO419_3.Where(x => x.HeaderId == objPRO415.HeaderId).ToList();
            List<PRO419_4> lstPRO419_4 = db.PRO419_4.Where(x => x.HeaderId == objPRO415.HeaderId).ToList();
            List<PRO419_5> lstPRO419_5 = db.PRO419_5.Where(x => x.HeaderId == objPRO415.HeaderId).ToList();


            ViewBag.lstPRO416 = lstPRO416;
            ViewBag.lstPRO417 = lstPRO417;
            ViewBag.lstPRO418 = lstPRO418;
            ViewBag.lstPRO419 = lstPRO419;
            ViewBag.lstPRO419_2 = lstPRO419_2;
            ViewBag.lstPRO419_3 = lstPRO419_3;
            ViewBag.lstPRO419_4 = lstPRO419_4;
            ViewBag.lstPRO419_5 = lstPRO419_5;


            #endregion

            return View(objPRO415);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO415 PRO415)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO415.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO415 objPRO415 = db.PRO415.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO415.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO415.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO415.ProtocolNo = PRO415.ProtocolNo;
                        objPRO415.CheckPoint1 = PRO415.CheckPoint1;
                        objPRO415.CheckPoint4 = PRO415.CheckPoint4;
                        objPRO415.CheckPoint5 = PRO415.CheckPoint5;
                        objPRO415.CheckPoint6 = PRO415.CheckPoint6;
                        objPRO415.CheckPoint7 = PRO415.CheckPoint7;
                        objPRO415.CheckPoint8 = PRO415.CheckPoint8;
                        objPRO415.CheckPoint9 = PRO415.CheckPoint9;
                        objPRO415.CheckPoint10 = PRO415.CheckPoint10;
                        objPRO415.CheckPoint11 = PRO415.CheckPoint11;
                        objPRO415.CheckPoint12 = PRO415.CheckPoint12;
                        objPRO415.CheckPoint13 = PRO415.CheckPoint13;
                        objPRO415.CheckPoint14 = PRO415.CheckPoint14;
                        objPRO415.CheckPoint15 = PRO415.CheckPoint15;
                        objPRO415.CheckPoint16 = PRO415.CheckPoint16;
                        objPRO415.CheckPoint17 = PRO415.CheckPoint17;
                        objPRO415.CheckPoint17_2 = PRO415.CheckPoint17_2;
                        objPRO415.QCRemarks = PRO415.QCRemarks;
                        objPRO415.Result = PRO415.Result;
                        objPRO415.EditedBy = objClsLoginInfo.UserName;
                        objPRO415.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO415.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO415 objPRO415 = db.PRO415.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO415 != null && string.IsNullOrWhiteSpace(objPRO415.ProtocolNo))
                    {
                        db.PRO415.Remove(objPRO415);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO415.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO416> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO416> lstAddPRO416 = new List<PRO416>();
                List<PRO416> lstDeletePRO416 = new List<PRO416>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO416 obj = db.PRO416.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO416();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.Required = item.Required;
                        obj.Tolerance = item.Tolerance;
                        obj.Actual = item.Actual;


                        if (isAdded)
                        {
                            lstAddPRO416.Add(obj);
                        }
                    }
                    if (lstAddPRO416.Count > 0)
                    {
                        db.PRO416.AddRange(lstAddPRO416);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO416 = db.PRO416.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO416.Count > 0)
                    {
                        db.PRO416.RemoveRange(lstDeletePRO416);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO416 = db.PRO416.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO416.Count > 0)
                    {
                        db.PRO416.RemoveRange(lstDeletePRO416);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO417> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO417> lstAddPRO417 = new List<PRO417>();
                List<PRO417> lstDeletePRO417 = new List<PRO417>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO417 obj = db.PRO417.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO417();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.HoleToHole1 = item.HoleToHole1;
                        obj.HoleToHole2 = item.HoleToHole2;
                        obj.ActBoltCircle = item.ActBoltCircle;

                        if (isAdded)
                        {
                            lstAddPRO417.Add(obj);
                        }
                    }
                    if (lstAddPRO417.Count > 0)
                    {
                        db.PRO417.AddRange(lstAddPRO417);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO417 = db.PRO417.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO417.Count > 0)
                    {
                        db.PRO417.RemoveRange(lstDeletePRO417);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO417 = db.PRO417.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO417.Count > 0)
                    {
                        db.PRO417.RemoveRange(lstDeletePRO417);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO418> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO418> lstAddPRO418 = new List<PRO418>();
                List<PRO418> lstDeletePRO418 = new List<PRO418>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO418 obj = db.PRO418.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO418();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.HoleToHole1 = item.HoleToHole1;
                        obj.HoleToHole2 = item.HoleToHole2;
                        obj.ActBoltCircle1 = item.ActBoltCircle1;
                        obj.HoleToHole3 = item.HoleToHole3;
                        obj.HoleToHole4 = item.HoleToHole4;
                        obj.ActBoltCircle2 = item.ActBoltCircle2;


                        if (isAdded)
                        {
                            lstAddPRO418.Add(obj);
                        }
                    }
                    if (lstAddPRO418.Count > 0)
                    {
                        db.PRO418.AddRange(lstAddPRO418);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO418 = db.PRO418.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO418.Count > 0)
                    {
                        db.PRO418.RemoveRange(lstDeletePRO418);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO418 = db.PRO418.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO418.Count > 0)
                    {
                        db.PRO418.RemoveRange(lstDeletePRO418);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO419> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO419> lstAddPRO419 = new List<PRO419>();
                List<PRO419> lstDeletePRO419 = new List<PRO419>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO419 obj = db.PRO419.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO419();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Hole1 = item.Hole1;
                        obj.Act1 = item.Act1;
                        obj.Hole2 = item.Hole2;
                        obj.Act2 = item.Act2;

                        if (isAdded)
                        {
                            lstAddPRO419.Add(obj);
                        }
                    }
                    if (lstAddPRO419.Count > 0)
                    {
                        db.PRO419.AddRange(lstAddPRO419);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO419 = db.PRO419.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO419.Count > 0)
                    {
                        db.PRO419.RemoveRange(lstDeletePRO419);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO419 = db.PRO419.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO419.Count > 0)
                    {
                        db.PRO419.RemoveRange(lstDeletePRO419);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO419_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO419_2> lstAddPRO419_2 = new List<PRO419_2>();
                List<PRO419_2> lstDeletePRO419_2 = new List<PRO419_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO419_2 obj = db.PRO419_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO419_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.Actual = item.Actual;

                        if (isAdded)
                        {
                            lstAddPRO419_2.Add(obj);
                        }
                    }
                    if (lstAddPRO419_2.Count > 0)
                    {
                        db.PRO419_2.AddRange(lstAddPRO419_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO419_2 = db.PRO419_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO419_2.Count > 0)
                    {
                        db.PRO419_2.RemoveRange(lstDeletePRO419_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO419_2 = db.PRO419_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO419_2.Count > 0)
                    {
                        db.PRO419_2.RemoveRange(lstDeletePRO419_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6(List<PRO419_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO419_3> lstAddPRO419_3 = new List<PRO419_3>();
                List<PRO419_3> lstDeletePRO419_3 = new List<PRO419_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO419_3 obj = db.PRO419_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO419_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActOutsideDiameter = item.ActOutsideDiameter;
                        obj.ActInsideDiameter = item.ActInsideDiameter;

                        if (isAdded)
                        {
                            lstAddPRO419_3.Add(obj);
                        }
                    }
                    if (lstAddPRO419_3.Count > 0)
                    {
                        db.PRO419_3.AddRange(lstAddPRO419_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO419_3 = db.PRO419_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO419_3.Count > 0)
                    {
                        db.PRO419_3.RemoveRange(lstDeletePRO419_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO419_3 = db.PRO419_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO419_3.Count > 0)
                    {
                        db.PRO419_3.RemoveRange(lstDeletePRO419_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7(List<PRO419_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO419_4> lstAddPRO419_4 = new List<PRO419_4>();
                List<PRO419_4> lstDeletePRO419_4 = new List<PRO419_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO419_4 obj = db.PRO419_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO419_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.HoleToHole1 = item.HoleToHole1;
                        obj.HoleToHole2 = item.HoleToHole2;
                        obj.Required = item.Required;
                        obj.Actual = item.Actual;
                        obj.Difference = item.Difference;

                        if (isAdded)
                        {
                            lstAddPRO419_4.Add(obj);
                        }
                    }
                    if (lstAddPRO419_4.Count > 0)
                    {
                        db.PRO419_4.AddRange(lstAddPRO419_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO419_4 = db.PRO419_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO419_4.Count > 0)
                    {
                        db.PRO419_4.RemoveRange(lstDeletePRO419_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO419_4 = db.PRO419_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO419_4.Count > 0)
                    {
                        db.PRO419_4.RemoveRange(lstDeletePRO419_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details
        [HttpPost]
        public JsonResult SaveProtocolLine8(List<PRO419_5> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO419_5> lstAddPRO419_5 = new List<PRO419_5>();
                List<PRO419_5> lstDeletePRO419_5 = new List<PRO419_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO419_5 obj = db.PRO419_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO419_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.HoleToHole1 = item.HoleToHole1;
                        obj.HoleToHole2 = item.HoleToHole2;
                        obj.Required = item.Required;
                        obj.Actual = item.Actual;
                        obj.Difference = item.Difference;

                        if (isAdded)
                        {
                            lstAddPRO419_5.Add(obj);
                        }
                    }
                    if (lstAddPRO419_5.Count > 0)
                    {
                        db.PRO419_5.AddRange(lstAddPRO419_5);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO419_5 = db.PRO419_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO419_5.Count > 0)
                    {
                        db.PRO419_5.RemoveRange(lstDeletePRO419_5);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO419_5 = db.PRO419_5.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO419_5.Count > 0)
                    {
                        db.PRO419_5.RemoveRange(lstDeletePRO419_5);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region  Linkage View Code  

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL415 objPRL415 = new PRL415();
            objPRL415 = db.PRL415.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL415 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL415.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();


            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            #region bind lines
            List<PRL416> lstPRL416 = db.PRL416.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL417> lstPRL417 = db.PRL417.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL418> lstPRL418 = db.PRL418.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL419> lstPRL419 = db.PRL419.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL419_2> lstPRL419_2 = db.PRL419_2.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL419_3> lstPRL419_3 = db.PRL419_3.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL419_4> lstPRL419_4 = db.PRL419_4.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL419_5> lstPRL419_5 = db.PRL419_5.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();


            ViewBag.lstPRL416 = lstPRL416;
            ViewBag.lstPRL417 = lstPRL417;
            ViewBag.lstPRL418 = lstPRL418;
            ViewBag.lstPRL419 = lstPRL419;
            ViewBag.lstPRL419_2 = lstPRL419_2;
            ViewBag.lstPRL419_3 = lstPRL419_3;
            ViewBag.lstPRL419_4 = lstPRL419_4;
            ViewBag.lstPRL419_5 = lstPRL419_5;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL415.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL415.ActFilledBy) && (objPRL415.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL415.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL415);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL415 objPRL415 = new PRL415();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL415 = db.PRL415.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL415).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL415 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL415.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            #region bind lines
            List<PRL416> lstPRL416 = db.PRL416.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL417> lstPRL417 = db.PRL417.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL418> lstPRL418 = db.PRL418.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL419> lstPRL419 = db.PRL419.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL419_2> lstPRL419_2 = db.PRL419_2.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL419_3> lstPRL419_3 = db.PRL419_3.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL419_4> lstPRL419_4 = db.PRL419_4.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();
            List<PRL419_5> lstPRL419_5 = db.PRL419_5.Where(x => x.HeaderId == objPRL415.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL415.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL415.ActFilledBy) && (objPRL415.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL415.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    YesNoNAEnum = YesNoNAEnum,

                    objPRL415 = objPRL415,

                    lstPRL416 = lstPRL416,
                    lstPRL417 = lstPRL417,
                    lstPRL418 = lstPRL418,
                    lstPRL419 = lstPRL419,
                    lstPRL419_2 = lstPRL419_2,
                    lstPRL419_3 = lstPRL419_3,
                    lstPRL419_4 = lstPRL419_4,
                    lstPRL419_5 = lstPRL419_5

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL415 PRL415, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL415.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL415 objPRL415 = db.PRL415.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL415.DrawingRevisionNo = PRL415.DrawingRevisionNo;
                    objPRL415.DrawingNo = PRL415.DrawingNo;
                    objPRL415.DCRNo = PRL415.DCRNo;
                    objPRL415.ProtocolNo = PRL415.ProtocolNo;
                    objPRL415.CheckPoint1 = PRL415.CheckPoint1;
                    objPRL415.CheckPoint4 = PRL415.CheckPoint4;
                    objPRL415.CheckPoint5 = PRL415.CheckPoint5;
                    objPRL415.CheckPoint6 = PRL415.CheckPoint6;
                    objPRL415.CheckPoint7 = PRL415.CheckPoint7;
                    objPRL415.CheckPoint8 = PRL415.CheckPoint8;
                    objPRL415.CheckPoint9 = PRL415.CheckPoint9;
                    objPRL415.CheckPoint10 = PRL415.CheckPoint10;
                    objPRL415.CheckPoint11 = PRL415.CheckPoint11;
                    objPRL415.CheckPoint12 = PRL415.CheckPoint12;
                    objPRL415.CheckPoint13 = PRL415.CheckPoint13;
                    objPRL415.CheckPoint14 = PRL415.CheckPoint14;
                    objPRL415.CheckPoint15 = PRL415.CheckPoint15;
                    objPRL415.CheckPoint16 = PRL415.CheckPoint16;
                    objPRL415.CheckPoint17 = PRL415.CheckPoint17;
                    objPRL415.CheckPoint17_2 = PRL415.CheckPoint17_2;
                    objPRL415.QCRemarks = PRL415.QCRemarks;
                    objPRL415.Result = PRL415.Result;
                    objPRL415.EditedBy = UserName;
                    objPRL415.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL415.ActFilledBy = UserName;
                            objPRL415.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL415.ReqFilledBy = UserName;
                            objPRL415.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL415.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL415 objPRL415 = db.PRL415.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL415 != null && string.IsNullOrWhiteSpace(objPRL415.ProtocolNo))
                    {
                        db.PRL415.Remove(objPRL415);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL415.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL416> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL416> lstAddPRL416 = new List<PRL416>();
                List<PRL416> lstDeletePRL416 = new List<PRL416>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL416 obj = db.PRL416.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL416();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.Required = item.Required;
                        obj.Tolerance = item.Tolerance;
                        obj.Actual = item.Actual;


                        if (isAdded)
                        {
                            lstAddPRL416.Add(obj);
                        }
                    }
                    if (lstAddPRL416.Count > 0)
                    {
                        db.PRL416.AddRange(lstAddPRL416);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL416 = db.PRL416.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL416.Count > 0)
                    {
                        db.PRL416.RemoveRange(lstDeletePRL416);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL416 = db.PRL416.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL416.Count > 0)
                    {
                        db.PRL416.RemoveRange(lstDeletePRL416);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL417> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL417> lstAddPRL417 = new List<PRL417>();
                List<PRL417> lstDeletePRL417 = new List<PRL417>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL417 obj = db.PRL417.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL417();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.HoleToHole1 = item.HoleToHole1;
                        obj.HoleToHole2 = item.HoleToHole2;
                        obj.ActBoltCircle = item.ActBoltCircle;

                        if (isAdded)
                        {
                            lstAddPRL417.Add(obj);
                        }
                    }
                    if (lstAddPRL417.Count > 0)
                    {
                        db.PRL417.AddRange(lstAddPRL417);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL417 = db.PRL417.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL417.Count > 0)
                    {
                        db.PRL417.RemoveRange(lstDeletePRL417);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL417 = db.PRL417.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL417.Count > 0)
                    {
                        db.PRL417.RemoveRange(lstDeletePRL417);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL418> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL418> lstAddPRL418 = new List<PRL418>();
                List<PRL418> lstDeletePRL418 = new List<PRL418>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL418 obj = db.PRL418.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL418();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.HoleToHole1 = item.HoleToHole1;
                        obj.HoleToHole2 = item.HoleToHole2;
                        obj.ActBoltCircle1 = item.ActBoltCircle1;
                        obj.HoleToHole3 = item.HoleToHole3;
                        obj.HoleToHole4 = item.HoleToHole4;
                        obj.ActBoltCircle2 = item.ActBoltCircle2;


                        if (isAdded)
                        {
                            lstAddPRL418.Add(obj);
                        }
                    }
                    if (lstAddPRL418.Count > 0)
                    {
                        db.PRL418.AddRange(lstAddPRL418);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL418 = db.PRL418.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL418.Count > 0)
                    {
                        db.PRL418.RemoveRange(lstDeletePRL418);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL418 = db.PRL418.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL418.Count > 0)
                    {
                        db.PRL418.RemoveRange(lstDeletePRL418);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL419> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL419> lstAddPRL419 = new List<PRL419>();
                List<PRL419> lstDeletePRL419 = new List<PRL419>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL419 obj = db.PRL419.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL419();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Hole1 = item.Hole1;
                        obj.Act1 = item.Act1;
                        obj.Hole2 = item.Hole2;
                        obj.Act2 = item.Act2;

                        if (isAdded)
                        {
                            lstAddPRL419.Add(obj);
                        }
                    }
                    if (lstAddPRL419.Count > 0)
                    {
                        db.PRL419.AddRange(lstAddPRL419);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL419 = db.PRL419.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL419.Count > 0)
                    {
                        db.PRL419.RemoveRange(lstDeletePRL419);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL419 = db.PRL419.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL419.Count > 0)
                    {
                        db.PRL419.RemoveRange(lstDeletePRL419);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL419_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL419_2> lstAddPRL419_2 = new List<PRL419_2>();
                List<PRL419_2> lstDeletePRL419_2 = new List<PRL419_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL419_2 obj = db.PRL419_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL419_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.Actual = item.Actual;

                        if (isAdded)
                        {
                            lstAddPRL419_2.Add(obj);
                        }
                    }
                    if (lstAddPRL419_2.Count > 0)
                    {
                        db.PRL419_2.AddRange(lstAddPRL419_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL419_2 = db.PRL419_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL419_2.Count > 0)
                    {
                        db.PRL419_2.RemoveRange(lstDeletePRL419_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL419_2 = db.PRL419_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL419_2.Count > 0)
                    {
                        db.PRL419_2.RemoveRange(lstDeletePRL419_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 
        [HttpPost]
        public JsonResult SaveProtocolLine6Linkage(List<PRL419_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL419_3> lstAddPRL419_3 = new List<PRL419_3>();
                List<PRL419_3> lstDeletePRL419_3 = new List<PRL419_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL419_3 obj = db.PRL419_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL419_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActOutsideDiameter = item.ActOutsideDiameter;
                        obj.ActInsideDiameter = item.ActInsideDiameter;

                        if (isAdded)
                        {
                            lstAddPRL419_3.Add(obj);
                        }
                    }
                    if (lstAddPRL419_3.Count > 0)
                    {
                        db.PRL419_3.AddRange(lstAddPRL419_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL419_3 = db.PRL419_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL419_3.Count > 0)
                    {
                        db.PRL419_3.RemoveRange(lstDeletePRL419_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL419_3 = db.PRL419_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL419_3.Count > 0)
                    {
                        db.PRL419_3.RemoveRange(lstDeletePRL419_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line7 
        [HttpPost]
        public JsonResult SaveProtocolLine7Linkage(List<PRL419_4> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL419_4> lstAddPRL419_4 = new List<PRL419_4>();
                List<PRL419_4> lstDeletePRL419_4 = new List<PRL419_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL419_4 obj = db.PRL419_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL419_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.HoleToHole1 = item.HoleToHole1;
                        obj.HoleToHole2 = item.HoleToHole2;
                        obj.Required = item.Required;
                        obj.Actual = item.Actual;
                        obj.Difference = item.Difference;

                        if (isAdded)
                        {
                            lstAddPRL419_4.Add(obj);
                        }
                    }
                    if (lstAddPRL419_4.Count > 0)
                    {
                        db.PRL419_4.AddRange(lstAddPRL419_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL419_4 = db.PRL419_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL419_4.Count > 0)
                    {
                        db.PRL419_4.RemoveRange(lstDeletePRL419_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL419_4 = db.PRL419_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL419_4.Count > 0)
                    {
                        db.PRL419_4.RemoveRange(lstDeletePRL419_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8
        [HttpPost]
        public JsonResult SaveProtocolLine8Linkage(List<PRL419_5> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL419_5> lstAddPRL419_5 = new List<PRL419_5>();
                List<PRL419_5> lstDeletePRL419_5 = new List<PRL419_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL419_5 obj = db.PRL419_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL419_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.HoleToHole1 = item.HoleToHole1;
                        obj.HoleToHole2 = item.HoleToHole2;
                        obj.Required = item.Required;
                        obj.Actual = item.Actual;
                        obj.Difference = item.Difference;

                        if (isAdded)
                        {
                            lstAddPRL419_5.Add(obj);
                        }
                    }
                    if (lstAddPRL419_5.Count > 0)
                    {
                        db.PRL419_5.AddRange(lstAddPRL419_5);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL419_5 = db.PRL419_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL419_5.Count > 0)
                    {
                        db.PRL419_5.RemoveRange(lstDeletePRL419_5);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL419_5 = db.PRL419_5.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL419_5.Count > 0)
                    {
                        db.PRL419_5.RemoveRange(lstDeletePRL419_5);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}