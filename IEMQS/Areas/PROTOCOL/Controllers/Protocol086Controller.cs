﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol086Controller : clsBase
    {
        // GET: PROTOCOL/Protocol086
        string ControllerURL = "/PROTOCOL/Protocol086/";
        string Title = "REPORT FOR WELD VISUAL & DIMENSION OF SPOOL TYPE-2 (ONE  SIDE FLANGE)";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO425 objPRO425 = new PRO425();
            if (!id.HasValue)
            {
                try
                {
                    objPRO425.ProtocolNo = string.Empty;
                    objPRO425.CreatedBy = objClsLoginInfo.UserName;
                    objPRO425.CreatedOn = DateTime.Now;

                    db.PRO425.Add(objPRO425);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO425.HeaderId;
            }
            else
            {
                objPRO425 = db.PRO425.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO426> lstPRO426 = db.PRO426.Where(x => x.HeaderId == objPRO425.HeaderId).ToList();

            ViewBag.lstPRO426 = lstPRO426;

            #endregion

            return View(objPRO425);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO425 PRO425)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO425.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO425 objPRO425 = db.PRO425.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO425.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO425.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO425.ProtocolNo = PRO425.ProtocolNo;
                        objPRO425.IsBoltHolesStraddledForFlange = PRO425.IsBoltHolesStraddledForFlange;
                        objPRO425.ReqTolTiltingOfFlange = PRO425.ReqTolTiltingOfFlange;
                        objPRO425.ReqTolSquarenessOfOpenEndOfPipe = PRO425.ReqTolSquarenessOfOpenEndOfPipe;
                        objPRO425.ReqTolBoltHolesStraddledForFlange = PRO425.ReqTolBoltHolesStraddledForFlange;
                        objPRO425.ActBoltHolesStraddledForFlange = PRO425.ActBoltHolesStraddledForFlange;
                        objPRO425.ActTiltingOfFlange = PRO425.ActTiltingOfFlange;
                        objPRO425.ActSquarenessOfOpenEndOfPipe = PRO425.ActSquarenessOfOpenEndOfPipe;
                        objPRO425.ReqElevationOfFlangeCenter = PRO425.ReqElevationOfFlangeCenter;
                        objPRO425.ActElevationOfFlangeCenter = PRO425.ActElevationOfFlangeCenter;
                        objPRO425.ReqProjectionFromSpool = PRO425.ReqProjectionFromSpool;
                        objPRO425.ActProjectionFromSpool = PRO425.ActProjectionFromSpool;
                        objPRO425.CheckPoint1 = PRO425.CheckPoint1;
                        objPRO425.CheckPoint3 = PRO425.CheckPoint3;
                        objPRO425.CheckPoint4 = PRO425.CheckPoint4;
                        objPRO425.CheckPoint5 = PRO425.CheckPoint5;
                        objPRO425.CheckPoint6 = PRO425.CheckPoint6;
                        objPRO425.CheckPoint7 = PRO425.CheckPoint7;
                        objPRO425.CheckPoint8 = PRO425.CheckPoint8;
                        objPRO425.CheckPoint9 = PRO425.CheckPoint9;
                        objPRO425.CheckPoint10 = PRO425.CheckPoint10;
                        objPRO425.CheckPoint11 = PRO425.CheckPoint11;
                        objPRO425.CheckPoint12 = PRO425.CheckPoint12;
                        objPRO425.CheckPoint12_2 = PRO425.CheckPoint12_2;

                        objPRO425.QCRemarks = PRO425.QCRemarks;
                        objPRO425.Result = PRO425.Result;

                        objPRO425.EditedBy = objClsLoginInfo.UserName;
                        objPRO425.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO425.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO425 objPRO425 = db.PRO425.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO425 != null && string.IsNullOrWhiteSpace(objPRO425.ProtocolNo))
                    {
                        db.PRO425.Remove(objPRO425);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO425.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO426> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO426> lstAddPRO426 = new List<PRO426>();
                List<PRO426> lstDeletePRO426 = new List<PRO426>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO426 obj = db.PRO426.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Description = item.Description;
                                obj.ReqElevation = item.ReqElevation;
                                obj.ActElevation = item.ActElevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.ActOrientation = item.ActOrientation;
                                obj.ReqProjection = item.ReqProjection;
                                obj.ActProjection = item.ActProjection;
                                obj.ReqSize = item.ReqSize;
                                obj.ActSize = item.ActSize;
                                obj.ReqTilt = item.ReqTilt;
                                obj.ActTilt = item.ActTilt;

                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO426 obj = new PRO426();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Description))
                            {
                                obj.Description = item.Description;
                                obj.ReqElevation = item.ReqElevation;
                                obj.ActElevation = item.ActElevation;
                                obj.ReqOrientation = item.ReqOrientation;
                                obj.ActOrientation = item.ActOrientation;
                                obj.ReqProjection = item.ReqProjection;
                                obj.ActProjection = item.ActProjection;
                                obj.ReqSize = item.ReqSize;
                                obj.ActSize = item.ActSize;
                                obj.ReqTilt = item.ReqTilt;
                                obj.ActTilt = item.ActTilt;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO426.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO426.Count > 0)
                    {
                        db.PRO426.AddRange(lstAddPRO426);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO426 = db.PRO426.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO426.Count > 0)
                    {
                        db.PRO426.RemoveRange(lstDeletePRO426);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO426 = db.PRO426.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO426.Count > 0)
                    {
                        db.PRO426.RemoveRange(lstDeletePRO426);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL425 objPRL425 = new PRL425();
            objPRL425 = db.PRL425.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL425 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL425.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL426> lstPRL426 = db.PRL426.Where(x => x.HeaderId == objPRL425.HeaderId).ToList();

            ViewBag.lstPRL426 = lstPRL426;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL425.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL425.ActFilledBy) && (objPRL425.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL425.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL425);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL425 objPRL425 = new PRL425();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL425 = db.PRL425.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL425).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL425 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL425.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }

            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;
            
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL426> lstPRL426 = db.PRL426.Where(x => x.HeaderId == objPRL425.HeaderId).ToList();


            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL425.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL425.ActFilledBy) && (objPRL425.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL425.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    YesNoNAEnum = YesNoNAEnum,

                    objPRL425 = objPRL425,

                    lstPRL426 = lstPRL426

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL425 prl425, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl425.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL425 objPRL425 = db.PRL425.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL425.DrawingRevisionNo = prl425.DrawingRevisionNo;
                    objPRL425.DrawingNo = prl425.DrawingNo;
                    objPRL425.DCRNo = prl425.DCRNo;
                    objPRL425.ProtocolNo = prl425.ProtocolNo;

                    objPRL425.IsBoltHolesStraddledForFlange = prl425.IsBoltHolesStraddledForFlange;
                    objPRL425.ReqTolTiltingOfFlange = prl425.ReqTolTiltingOfFlange;
                    objPRL425.ReqTolSquarenessOfOpenEndOfPipe = prl425.ReqTolSquarenessOfOpenEndOfPipe;
                    objPRL425.ReqTolBoltHolesStraddledForFlange = prl425.ReqTolBoltHolesStraddledForFlange;
                    objPRL425.ActBoltHolesStraddledForFlange = prl425.ActBoltHolesStraddledForFlange;
                    objPRL425.ActTiltingOfFlange = prl425.ActTiltingOfFlange;
                    objPRL425.ActSquarenessOfOpenEndOfPipe = prl425.ActSquarenessOfOpenEndOfPipe;
                    objPRL425.ReqElevationOfFlangeCenter = prl425.ReqElevationOfFlangeCenter;
                    objPRL425.ActElevationOfFlangeCenter = prl425.ActElevationOfFlangeCenter;
                    objPRL425.ReqProjectionFromSpool = prl425.ReqProjectionFromSpool;
                    objPRL425.ActProjectionFromSpool = prl425.ActProjectionFromSpool;
                    objPRL425.CheckPoint1 = prl425.CheckPoint1;
                    objPRL425.CheckPoint3 = prl425.CheckPoint3;
                    objPRL425.CheckPoint4 = prl425.CheckPoint4;
                    objPRL425.CheckPoint5 = prl425.CheckPoint5;
                    objPRL425.CheckPoint6 = prl425.CheckPoint6;
                    objPRL425.CheckPoint7 = prl425.CheckPoint7;
                    objPRL425.CheckPoint8 = prl425.CheckPoint8;
                    objPRL425.CheckPoint9 = prl425.CheckPoint9;
                    objPRL425.CheckPoint10 = prl425.CheckPoint10;
                    objPRL425.CheckPoint11 = prl425.CheckPoint11;
                    objPRL425.CheckPoint12 = prl425.CheckPoint12;
                    objPRL425.CheckPoint12_2 = prl425.CheckPoint12_2;

                    objPRL425.QCRemarks = prl425.QCRemarks;
                    objPRL425.Result = prl425.Result;

                    objPRL425.EditedBy = UserName;
                    objPRL425.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL425.ActFilledBy = UserName;
                            objPRL425.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL425.ReqFilledBy = UserName;
                            objPRL425.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL425.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL425 objPRL425 = db.PRL425.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL425 != null && string.IsNullOrWhiteSpace(objPRL425.ProtocolNo))
                    {
                        db.PRL425.Remove(objPRL425);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL425.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL426> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL426> lstAddPRL426 = new List<PRL426>();
                List<PRL426> lstDeletePRL426 = new List<PRL426>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL426 obj = db.PRL426.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL426();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.ReqElevation = item.ReqElevation;
                        obj.ActElevation = item.ActElevation;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ActOrientation = item.ActOrientation;
                        obj.ReqProjection = item.ReqProjection;
                        obj.ActProjection = item.ActProjection;
                        obj.ReqSize = item.ReqSize;
                        obj.ActSize = item.ActSize;
                        obj.ReqTilt = item.ReqTilt;
                        obj.ActTilt = item.ActTilt;

                        if (isAdded)
                        {
                            lstAddPRL426.Add(obj);
                        }
                    }
                    if (lstAddPRL426.Count > 0)
                    {
                        db.PRL426.AddRange(lstAddPRL426);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL426 = db.PRL426.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL426.Count > 0)
                    {
                        db.PRL426.RemoveRange(lstDeletePRL426);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL426 = db.PRL426.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL426.Count > 0)
                    {
                        db.PRL426.RemoveRange(lstDeletePRL426);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion
    }
}