﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol079Controller : clsBase
    {
        // GET: PROTOCOL/Protocol079
        string ControllerURL = "/PROTOCOL/Protocol079/";
        string Title = "REPORT FOR VISUAL AND DIMENSION INSPECTION FOR TAPER TYPE NUB BEFORE OVERLAY";


        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO390 objPRO390 = new PRO390();

            if (!id.HasValue)
            {
                try
                {
                    objPRO390.ProtocolNo = string.Empty;
                    objPRO390.CreatedBy = objClsLoginInfo.UserName;
                    objPRO390.CreatedOn = DateTime.Now;

                    #region  Orientation 

                    objPRO390.PRO391.Add(new PRO391
                    {

                        Orientation = "0º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO391.Add(new PRO391
                    {

                        Orientation = "45º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO391.Add(new PRO391
                    {

                        Orientation = "90º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO391.Add(new PRO391
                    {

                        Orientation = "135º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO391.Add(new PRO391
                    {

                        Orientation = "180º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO391.Add(new PRO391
                    {

                        Orientation = "225º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO391.Add(new PRO391
                    {

                        Orientation = "270º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO391.Add(new PRO391
                    {

                        Orientation = "315º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });


                    #endregion

                    #region  OrientationReqNubID 

                    objPRO390.PRO392.Add(new PRO392
                    {

                        OrientationReqNubID = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO392.Add(new PRO392
                    {

                        OrientationReqNubID = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO392.Add(new PRO392
                    {

                        OrientationReqNubID = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO392.Add(new PRO392
                    {

                        OrientationReqNubID = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO392.Add(new PRO392
                    {

                        OrientationReqNubID = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO392.Add(new PRO392
                    {

                        OrientationReqNubID = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO392.Add(new PRO392
                    {

                        OrientationReqNubID = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO392.Add(new PRO392
                    {

                        OrientationReqNubID = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });


                    #endregion

                    #region  OrientationAllowedShellOutOfRoundness

                    objPRO390.PRO393.Add(new PRO393
                    {

                        OrientationAllowedShellOutOfRoundness = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO393.Add(new PRO393
                    {

                        OrientationAllowedShellOutOfRoundness = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO393.Add(new PRO393
                    {

                        OrientationAllowedShellOutOfRoundness = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO393.Add(new PRO393
                    {

                        OrientationAllowedShellOutOfRoundness = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO393.Add(new PRO393
                    {

                        OrientationAllowedShellOutOfRoundness = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO393.Add(new PRO393
                    {

                        OrientationAllowedShellOutOfRoundness = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO393.Add(new PRO393
                    {

                        OrientationAllowedShellOutOfRoundness = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO393.Add(new PRO393
                    {

                        OrientationAllowedShellOutOfRoundness = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });


                    #endregion

                    #region  OtherDimensions 

                    objPRO390.PRO394.Add(new PRO394
                    {

                        OtherDimensions = "TOP SIDE CORNER RADIUS",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO394.Add(new PRO394
                    {

                        OtherDimensions = "BOTTOM SIDE CORNER RADIUS",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO394.Add(new PRO394
                    {

                        OtherDimensions = "SURFACE FINISH ON MACHINED SURFACES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO390.PRO394.Add(new PRO394
                    {

                        OtherDimensions = "TAPER ANGLE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });


                    #endregion

                    db.PRO390.Add(objPRO390);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO390.HeaderId;


            }
            else
            {
                objPRO390 = db.PRO390.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };
            List<string> lstElevationOfTopSide = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfTopSide = lstElevationOfTopSide.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO391> lstPRO391 = db.PRO391.Where(x => x.HeaderId == objPRO390.HeaderId).ToList();
            List<PRO392> lstPRO392 = db.PRO392.Where(x => x.HeaderId == objPRO390.HeaderId).ToList();
            List<PRO393> lstPRO393 = db.PRO393.Where(x => x.HeaderId == objPRO390.HeaderId).ToList();
            List<PRO394> lstPRO394 = db.PRO394.Where(x => x.HeaderId == objPRO390.HeaderId).ToList();

            ViewBag.lstPRO391 = lstPRO391;
            ViewBag.lstPRO392 = lstPRO392;
            ViewBag.lstPRO393 = lstPRO393;
            ViewBag.lstPRO394 = lstPRO394;


            #endregion

            return View(objPRO390);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO390 PRO390)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO390.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO390 objPRO390 = db.PRO390.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO390.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO390.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO390.ProtocolNo = PRO390.ProtocolNo;
                        objPRO390.ElevationOfNubFrom = PRO390.ElevationOfNubFrom;
                        objPRO390.ElevationOfTopSide = PRO390.ElevationOfTopSide;
                        objPRO390.ReqElevationOfNubFrom = PRO390.ReqElevationOfNubFrom;
                        objPRO390.ReqElevationOfTopSide = PRO390.ReqElevationOfTopSide;
                        objPRO390.ReqNubWidth = PRO390.ReqNubWidth;
                        objPRO390.ReqNubHeight = PRO390.ReqNubHeight;
                        objPRO390.TolReqElevationOfNubFrom = PRO390.TolReqElevationOfNubFrom;
                        objPRO390.TolReqElevationOfTopSide = PRO390.TolReqElevationOfTopSide;
                        objPRO390.TolReqNubWidth = PRO390.TolReqNubWidth;
                        objPRO390.TolReqNubHeight = PRO390.TolReqNubHeight;
                        objPRO390.NubID = PRO390.NubID;
                        objPRO390.ReqNubID = PRO390.ReqNubID;
                        objPRO390.OutByNubID = PRO390.OutByNubID;
                        objPRO390.ShellOutOfRoundness = PRO390.ShellOutOfRoundness;
                        objPRO390.AllowedShellOutOfRoundness = PRO390.AllowedShellOutOfRoundness;
                        objPRO390.OutByShellOutOfRoundness = PRO390.OutByShellOutOfRoundness;
                        objPRO390.CheckPoint1 = PRO390.CheckPoint1;
                        objPRO390.CheckPoint2 = PRO390.CheckPoint2;
                        objPRO390.CheckPoint3 = PRO390.CheckPoint3;
                        objPRO390.CheckPoint4 = PRO390.CheckPoint4;
                        objPRO390.CheckPoint6 = PRO390.CheckPoint6;
                        objPRO390.CheckPoint6_2 = PRO390.CheckPoint6_2;
                        objPRO390.QCRemarks = PRO390.QCRemarks;
                        objPRO390.Result = PRO390.Result;
                        objPRO390.EditedBy = PRO390.EditedBy;
                        objPRO390.EditedOn = PRO390.EditedOn;
                        objPRO390.EditedBy = objClsLoginInfo.UserName;
                        objPRO390.EditedOn = DateTime.Now;



                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO390.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO390 objPRO390 = db.PRO390.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO390 != null && string.IsNullOrWhiteSpace(objPRO390.ProtocolNo))
                    {
                        db.PRO390.Remove(objPRO390);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO390.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO391> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO391> lstAddPRO391 = new List<PRO391>();
                List<PRO391> lstDeletePRO391 = new List<PRO391>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO391 obj = db.PRO391.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO391();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }




                        obj.Orientation = item.Orientation;
                        obj.ActTolReqElevationOfNubFrom = item.ActTolReqElevationOfNubFrom;
                        obj.ActTolReqElevationOfTopSide = item.ActTolReqElevationOfTopSide;
                        obj.ActTolReqNubWidth = item.ActTolReqNubWidth;
                        obj.ActTolReqNubHeight = item.ActTolReqNubHeight;
                        obj.A1DimensionsChecked = item.A1DimensionsChecked;
                        obj.A2DimensionsChecked = item.A2DimensionsChecked;
                        obj.B1DimensionsChecked = item.B1DimensionsChecked;
                        obj.B2DimensionsChecked = item.B2DimensionsChecked;
                        obj.C1DimensionsChecked = item.C1DimensionsChecked;
                        obj.C2DimensionsChecked = item.C2DimensionsChecked;

                        if (isAdded)
                        {
                            lstAddPRO391.Add(obj);
                        }
                    }
                    if (lstAddPRO391.Count > 0)
                    {
                        db.PRO391.AddRange(lstAddPRO391);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO391 = db.PRO391.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO391.Count > 0)
                    {
                        db.PRO391.RemoveRange(lstDeletePRO391);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO391 = db.PRO391.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO391.Count > 0)
                    {
                        db.PRO391.RemoveRange(lstDeletePRO391);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO392> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO392> lstAddPRO392 = new List<PRO392>();
                List<PRO392> lstDeletePRO392 = new List<PRO392>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO392 obj = db.PRO392.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO392();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.OrientationReqNubID))
                        {


                            obj.OrientationReqNubID = item.OrientationReqNubID;
                            obj.ActualNubID = item.ActualNubID;
                            if (isAdded)
                            {
                                lstAddPRO392.Add(obj);
                            }
                        }


                    }
                    if (lstAddPRO392.Count > 0)
                    {
                        db.PRO392.AddRange(lstAddPRO392);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO392 = db.PRO392.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO392.Count > 0)
                    {
                        db.PRO392.RemoveRange(lstDeletePRO392);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO392 = db.PRO392.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO392.Count > 0)
                    {
                        db.PRO392.RemoveRange(lstDeletePRO392);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO393> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO393> lstAddPRO393 = new List<PRO393>();
                List<PRO393> lstDeletePRO393 = new List<PRO393>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO393 obj = db.PRO393.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO393();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.OrientationAllowedShellOutOfRoundness))
                        {
                            obj.OrientationAllowedShellOutOfRoundness = item.OrientationAllowedShellOutOfRoundness;
                            obj.ActualShellOutOfRoundness = item.ActualShellOutOfRoundness;

                            if (isAdded)
                            {
                                lstAddPRO393.Add(obj);
                            }
                        }



                    }
                    if (lstAddPRO393.Count > 0)
                    {
                        db.PRO393.AddRange(lstAddPRO393);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO393 = db.PRO393.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO393.Count > 0)
                    {
                        db.PRO393.RemoveRange(lstDeletePRO393);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO393 = db.PRO393.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO393.Count > 0)
                    {
                        db.PRO393.RemoveRange(lstDeletePRO393);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO394> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO394> lstAddPRO394 = new List<PRO394>();
                List<PRO394> lstDeletePRO394 = new List<PRO394>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO394 obj = db.PRO394.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO394();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.OtherDimensions = item.OtherDimensions;
                        obj.ReqOtherDimensions = item.ReqOtherDimensions;
                        obj.ActOtherDimensions = item.ActOtherDimensions;


                        if (isAdded)
                        {
                            lstAddPRO394.Add(obj);
                        }
                    }
                    if (lstAddPRO394.Count > 0)
                    {
                        db.PRO394.AddRange(lstAddPRO394);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO394 = db.PRO394.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO394.Count > 0)
                    {
                        db.PRO394.RemoveRange(lstDeletePRO394);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO394 = db.PRO394.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO394.Count > 0)
                    {
                        db.PRO394.RemoveRange(lstDeletePRO394);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region  Linkage View Code   

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL390 objPRL390 = new PRL390();
            objPRL390 = db.PRL390.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL390 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL390.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };
            List<string> lstElevationOfTopSide = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfTopSide = lstElevationOfTopSide.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL391> lstPRL391 = db.PRL391.Where(x => x.HeaderId == objPRL390.HeaderId).ToList();
            List<PRL392> lstPRL392 = db.PRL392.Where(x => x.HeaderId == objPRL390.HeaderId).ToList();
            List<PRL393> lstPRL393 = db.PRL393.Where(x => x.HeaderId == objPRL390.HeaderId).ToList();
            List<PRL394> lstPRL394 = db.PRL394.Where(x => x.HeaderId == objPRL390.HeaderId).ToList();

            ViewBag.lstPRL391 = lstPRL391;
            ViewBag.lstPRL392 = lstPRL392;
            ViewBag.lstPRL393 = lstPRL393;
            ViewBag.lstPRL394 = lstPRL394;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL390.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL390.ActFilledBy) && (objPRL390.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL390.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL390);
        }


        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL390 objPRL390 = new PRL390();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL390 = db.PRL390.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL390).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL390 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL390.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };
            List<string> lstElevationOfTopSide = new List<string> { "BOTTOM SIDE WEP", "BOTTOM REF. LINE", "B.T.L" };

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ElevationOfTopSide = lstElevationOfTopSide.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL391> lstPRL391 = db.PRL391.Where(x => x.HeaderId == objPRL390.HeaderId).ToList();
            List<PRL392> lstPRL392 = db.PRL392.Where(x => x.HeaderId == objPRL390.HeaderId).ToList();
            List<PRL393> lstPRL393 = db.PRL393.Where(x => x.HeaderId == objPRL390.HeaderId).ToList();
            List<PRL394> lstPRL394 = db.PRL394.Where(x => x.HeaderId == objPRL390.HeaderId).ToList();


            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL390.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL390.ActFilledBy) && (objPRL390.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL390.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    ElevationOfNubFrom = ElevationOfNubFrom,
                    ElevationOfTopSide = ElevationOfTopSide,
                    OutsideInside = OutsideInside,
                    LeftRight = LeftRight,

                    objPRL390 = objPRL390,

                    lstPRL391 = lstPRL391,
                    lstPRL392 = lstPRL392,
                    lstPRL393 = lstPRL393,
                    lstPRL394 = lstPRL394,

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                // List<string> lstdrawing = new List<string>();
                // lstdrawing.Add("Dummy Drawing Number");
                // ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,

                }, JsonRequestBehavior.AllowGet);
            }


        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL390 PRL390, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL390.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL390 objPRL390 = db.PRL390.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL390.DrawingRevisionNo = PRL390.DrawingRevisionNo;
                    objPRL390.DrawingNo = PRL390.DrawingNo;
                    objPRL390.DCRNo = PRL390.DCRNo;
                    objPRL390.ProtocolNo = PRL390.ProtocolNo;
                    objPRL390.ElevationOfNubFrom = PRL390.ElevationOfNubFrom;
                    objPRL390.ElevationOfTopSide = PRL390.ElevationOfTopSide;
                    objPRL390.ReqElevationOfNubFrom = PRL390.ReqElevationOfNubFrom;
                    objPRL390.ReqElevationOfTopSide = PRL390.ReqElevationOfTopSide;
                    objPRL390.ReqNubWidth = PRL390.ReqNubWidth;
                    objPRL390.ReqNubHeight = PRL390.ReqNubHeight;
                    objPRL390.TolReqElevationOfNubFrom = PRL390.TolReqElevationOfNubFrom;
                    objPRL390.TolReqElevationOfTopSide = PRL390.TolReqElevationOfTopSide;
                    objPRL390.TolReqNubWidth = PRL390.TolReqNubWidth;
                    objPRL390.TolReqNubHeight = PRL390.TolReqNubHeight;
                    objPRL390.NubID = PRL390.NubID;
                    objPRL390.ReqNubID = PRL390.ReqNubID;
                    objPRL390.OutByNubID = PRL390.OutByNubID;
                    objPRL390.ShellOutOfRoundness = PRL390.ShellOutOfRoundness;
                    objPRL390.AllowedShellOutOfRoundness = PRL390.AllowedShellOutOfRoundness;
                    objPRL390.OutByShellOutOfRoundness = PRL390.OutByShellOutOfRoundness;
                    objPRL390.CheckPoint1 = PRL390.CheckPoint1;
                    objPRL390.CheckPoint2 = PRL390.CheckPoint2;
                    objPRL390.CheckPoint3 = PRL390.CheckPoint3;
                    objPRL390.CheckPoint4 = PRL390.CheckPoint4;
                    objPRL390.CheckPoint6 = PRL390.CheckPoint6;
                    objPRL390.CheckPoint6_2 = PRL390.CheckPoint6_2;
                    objPRL390.QCRemarks = PRL390.QCRemarks;
                    objPRL390.Result = PRL390.Result;
                    objPRL390.EditedBy = UserName;
                    objPRL390.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL390.ActFilledBy = UserName;
                            objPRL390.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL390.ReqFilledBy = UserName;
                            objPRL390.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL390.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL390 objPRL390 = db.PRL390.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL390 != null && string.IsNullOrWhiteSpace(objPRL390.ProtocolNo))
                    {
                        db.PRL390.Remove(objPRL390);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL390.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL391> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL391> lstAddPRL391 = new List<PRL391>();
                List<PRL391> lstDeletePRL391 = new List<PRL391>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL391 obj = db.PRL391.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL391();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActTolReqElevationOfNubFrom = item.ActTolReqElevationOfNubFrom;
                        obj.ActTolReqElevationOfTopSide = item.ActTolReqElevationOfTopSide;
                        obj.ActTolReqNubWidth = item.ActTolReqNubWidth;
                        obj.ActTolReqNubHeight = item.ActTolReqNubHeight;
                        obj.A1DimensionsChecked = item.A1DimensionsChecked;
                        obj.A2DimensionsChecked = item.A2DimensionsChecked;
                        obj.B1DimensionsChecked = item.B1DimensionsChecked;
                        obj.B2DimensionsChecked = item.B2DimensionsChecked;
                        obj.C1DimensionsChecked = item.C1DimensionsChecked;
                        obj.C2DimensionsChecked = item.C2DimensionsChecked;

                        if (isAdded)
                        {
                            lstAddPRL391.Add(obj);
                        }
                    }
                }
                if (lstAddPRL391.Count > 0)
                {
                    db.PRL391.AddRange(lstAddPRL391);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL391 = db.PRL391.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL391 = db.PRL391.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL391.Count > 0)
                {
                    db.PRL391.RemoveRange(lstDeletePRL391);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL392> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL392> lstAddPRL392 = new List<PRL392>();
                List<PRL392> lstDeletePRL392 = new List<PRL392>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL392 obj = db.PRL392.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL392();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.OrientationReqNubID))
                        {

                            obj.OrientationReqNubID = item.OrientationReqNubID;
                            obj.ActualNubID = item.ActualNubID;

                            if (isAdded)
                            {
                                lstAddPRL392.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL392.Count > 0)
                {
                    db.PRL392.AddRange(lstAddPRL392);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL392 = db.PRL392.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL392 = db.PRL392.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL392.Count > 0)
                {
                    db.PRL392.RemoveRange(lstDeletePRL392);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL393> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL393> lstAddPRL393 = new List<PRL393>();
                List<PRL393> lstDeletePRL393 = new List<PRL393>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL393 obj = db.PRL393.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL393();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.OrientationAllowedShellOutOfRoundness))
                        {


                            obj.OrientationAllowedShellOutOfRoundness = item.OrientationAllowedShellOutOfRoundness;
                            obj.ActualShellOutOfRoundness = item.ActualShellOutOfRoundness;

                            if (isAdded)
                            {
                                lstAddPRL393.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL393.Count > 0)
                {
                    db.PRL393.AddRange(lstAddPRL393);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL393 = db.PRL393.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL393 = db.PRL393.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL393.Count > 0)
                {
                    db.PRL393.RemoveRange(lstDeletePRL393);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL394> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL394> lstAddPRL394 = new List<PRL394>();
                List<PRL394> lstDeletePRL394 = new List<PRL394>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL394 obj = db.PRL394.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL394();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.OtherDimensions = item.OtherDimensions;
                        obj.ReqOtherDimensions = item.ReqOtherDimensions;
                        obj.ActOtherDimensions = item.ActOtherDimensions;


                        if (isAdded)
                        {
                            lstAddPRL394.Add(obj);
                        }
                    }
                }
                if (lstAddPRL394.Count > 0)
                {
                    db.PRL394.AddRange(lstAddPRL394);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL394 = db.PRL394.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL394 = db.PRL394.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL394.Count > 0)
                {
                    db.PRL394.RemoveRange(lstDeletePRL394);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}