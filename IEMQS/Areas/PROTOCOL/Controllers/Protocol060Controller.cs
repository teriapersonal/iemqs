﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol060Controller : clsBase
    {
        // GET: PROTOCOL/Protocol060
        string ControllerURL = "/PROTOCOL/Protocol060/";
        string Title = "VISUAL AND DIMENSION REPORT FOR  LONGITUDINAL SEAM OF RING";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO295 objPRO295 = new PRO295();
            if (!id.HasValue)
            {
                try
                {
                    objPRO295.ProtocolNo = string.Empty;
                    objPRO295.CreatedBy = objClsLoginInfo.UserName;
                    objPRO295.CreatedOn = DateTime.Now;

                    #region ORIENTATION 
                    objPRO295.PRO296.Add(new PRO296
                    {
                        Orientation = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO295.PRO296.Add(new PRO296
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO295.PRO296.Add(new PRO296
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO295.PRO296.Add(new PRO296
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region ORIENTATION FLATNESS  
                    objPRO295.PRO298.Add(new PRO298
                    {
                        OrientationFlatness = "0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO295.PRO298.Add(new PRO298
                    {
                        OrientationFlatness = "45°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO295.PRO298.Add(new PRO298
                    {
                        OrientationFlatness = "90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO295.PRO298.Add(new PRO298
                    {
                        OrientationFlatness = "135°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO295.PRO298.Add(new PRO298
                    {
                        OrientationFlatness = "180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO295.PRO298.Add(new PRO298
                    {
                        OrientationFlatness = "225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    }); objPRO295.PRO298.Add(new PRO298
                    {
                        OrientationFlatness = "270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    }); objPRO295.PRO298.Add(new PRO298
                    {
                        OrientationFlatness = "315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO295.Add(objPRO295);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO295.HeaderId;
            }
            else
            {
                objPRO295 = db.PRO295.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO296> lstPRO296 = db.PRO296.Where(x => x.HeaderId == objPRO295.HeaderId).ToList();
            List<PRO297> lstPRO297 = db.PRO297.Where(x => x.HeaderId == objPRO295.HeaderId).ToList();
            List<PRO298> lstPRO298 = db.PRO298.Where(x => x.HeaderId == objPRO295.HeaderId).ToList();


            ViewBag.lstPRO296 = lstPRO296;
            ViewBag.lstPRO297 = lstPRO297;
            ViewBag.lstPRO298 = lstPRO298;

            #endregion

            return View(objPRO295);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO295 PRO295)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO295.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO295 objPRO295 = db.PRO295.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO295.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO295.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO295.ProtocolNo = PRO295.ProtocolNo;
                        objPRO295.ReqOutsideDiameter = PRO295.ReqOutsideDiameter;
                        objPRO295.ReqInsideDiameter = PRO295.ReqInsideDiameter;
                        objPRO295.Offset = PRO295.Offset;
                        objPRO295.Flatness = PRO295.Flatness;
                        objPRO295.OutByFlatness = PRO295.OutByFlatness;
                        objPRO295.OutByLocation1 = PRO295.OutByLocation1;
                        objPRO295.OutByLocation2 = PRO295.OutByLocation2;
                        objPRO295.OutByLocation3 = PRO295.OutByLocation3;
                        objPRO295.CheckPoint1 = PRO295.CheckPoint1;
                        objPRO295.CheckPoint2 = PRO295.CheckPoint2;
                        objPRO295.CheckPoint3 = PRO295.CheckPoint3;
                        objPRO295.CheckPoint5 = PRO295.CheckPoint5;
                        objPRO295.CheckPoint5_2 = PRO295.CheckPoint5_2;
                        objPRO295.QCRemarks = PRO295.QCRemarks;
                        objPRO295.Result = PRO295.Result;

                        objPRO295.EditedBy = objClsLoginInfo.UserName;
                        objPRO295.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO295.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO295 objPRO295 = db.PRO295.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO295 != null && string.IsNullOrWhiteSpace(objPRO295.ProtocolNo))
                    {
                        db.PRO295.Remove(objPRO295);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO295.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO296> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO296> lstAddPRO296 = new List<PRO296>();
                List<PRO296> lstDeletePRO296 = new List<PRO296>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO296 obj = db.PRO296.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO296();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActOutsideDiameter = item.ActOutsideDiameter;
                        obj.ActInsideDiameter = item.ActInsideDiameter;

                        if (isAdded)
                        {
                            lstAddPRO296.Add(obj);
                        }
                    }
                    if (lstAddPRO296.Count > 0)
                    {
                        db.PRO296.AddRange(lstAddPRO296);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO296 = db.PRO296.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO296.Count > 0)
                    {
                        db.PRO296.RemoveRange(lstDeletePRO296);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO296 = db.PRO296.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO296.Count > 0)
                    {
                        db.PRO296.RemoveRange(lstDeletePRO296);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO297> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO297> lstAddPRO297 = new List<PRO297>();
                List<PRO297> lstDeletePRO297 = new List<PRO297>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO297 obj = db.PRO297.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO297();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNoOffset = item.SeamNoOffset;
                        obj.ActMaxOffset = item.ActMaxOffset;

                        if (isAdded)
                        {
                            lstAddPRO297.Add(obj);
                        }
                    }
                    if (lstAddPRO297.Count > 0)
                    {
                        db.PRO297.AddRange(lstAddPRO297);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO297 = db.PRO297.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO297.Count > 0)
                    {
                        db.PRO297.RemoveRange(lstDeletePRO297);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO297 = db.PRO297.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO297.Count > 0)
                    {
                        db.PRO297.RemoveRange(lstDeletePRO297);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO298> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO298> lstAddPRO298 = new List<PRO298>();
                List<PRO298> lstDeletePRO298 = new List<PRO298>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (!string.IsNullOrWhiteSpace(item.OrientationFlatness))
                        {
                            bool isAdded = false;
                            PRO298 obj = db.PRO298.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj == null)
                            {
                                obj = new PRO298();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                            else
                            {
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }

                            obj.OrientationFlatness = item.OrientationFlatness;
                            obj.Location1 = item.Location1;
                            obj.Location2 = item.Location2;
                            obj.Location3 = item.Location3;

                            if (isAdded)
                            {
                                lstAddPRO298.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO298.Count > 0)
                    {
                        db.PRO298.AddRange(lstAddPRO298);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO298 = db.PRO298.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO298.Count > 0)
                    {
                        db.PRO298.RemoveRange(lstDeletePRO298);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO298 = db.PRO298.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO298.Count > 0)
                    {
                        db.PRO298.RemoveRange(lstDeletePRO298);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL295 objPRL295 = new PRL295();
            objPRL295 = db.PRL295.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL295 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL295.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();  

            #region bind lines
            List<PRL296> lstPRL296 = db.PRL296.Where(x => x.HeaderId == objPRL295.HeaderId).ToList();
            List<PRL297> lstPRL297 = db.PRL297.Where(x => x.HeaderId == objPRL295.HeaderId).ToList();
            List<PRL298> lstPRL298 = db.PRL298.Where(x => x.HeaderId == objPRL295.HeaderId).ToList();

            ViewBag.lstPRL296 = lstPRL296;
            ViewBag.lstPRL297 = lstPRL297;
            ViewBag.lstPRL298 = lstPRL298;

            #endregion
            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL295.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL295.ActFilledBy) && (objPRL295.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL295.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL295);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL295 objPRL295 = new PRL295();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL295 = db.PRL295.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL295).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL295 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL295.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;
          
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL296> lstPRL296 = db.PRL296.Where(x => x.HeaderId == objPRL295.HeaderId).ToList();
            List<PRL297> lstPRL297 = db.PRL297.Where(x => x.HeaderId == objPRL295.HeaderId).ToList();
            List<PRL298> lstPRL298 = db.PRL298.Where(x => x.HeaderId == objPRL295.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL295.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL295.ActFilledBy) && (objPRL295.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL295.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,

                    objPRL295 = objPRL295,

                    lstPRL296 = lstPRL296,
                    lstPRL297 = lstPRL297,
                    lstPRL298 = lstPRL298

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL295 PRL295, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;

                int? refHeaderId = PRL295.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL295 objPRL295 = db.PRL295.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL295.DrawingRevisionNo = PRL295.DrawingRevisionNo;
                    objPRL295.DrawingNo = PRL295.DrawingNo;
                    objPRL295.DCRNo = PRL295.DCRNo;
                    objPRL295.ProtocolNo = PRL295.ProtocolNo != null ? PRL295.ProtocolNo : "";
                    objPRL295.ReqOutsideDiameter = PRL295.ReqOutsideDiameter;
                    objPRL295.ReqInsideDiameter = PRL295.ReqInsideDiameter;
                    objPRL295.Offset = PRL295.Offset;
                    objPRL295.Flatness = PRL295.Flatness;
                    objPRL295.OutByFlatness = PRL295.OutByFlatness;
                    objPRL295.OutByLocation1 = PRL295.OutByLocation1;
                    objPRL295.OutByLocation2 = PRL295.OutByLocation2;
                    objPRL295.OutByLocation3 = PRL295.OutByLocation3;
                    objPRL295.CheckPoint1 = PRL295.CheckPoint1;
                    objPRL295.CheckPoint2 = PRL295.CheckPoint2;
                    objPRL295.CheckPoint3 = PRL295.CheckPoint3;
                    objPRL295.CheckPoint5 = PRL295.CheckPoint5;
                    objPRL295.CheckPoint5_2 = PRL295.CheckPoint5_2;
                    objPRL295.QCRemarks = PRL295.QCRemarks;
                    objPRL295.Result = PRL295.Result;

                    objPRL295.EditedBy = UserName;
                    objPRL295.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL295.ActFilledBy = UserName;
                            objPRL295.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL295.ReqFilledBy = UserName;
                            objPRL295.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL295.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL295 objPRL295 = db.PRL295.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL295 != null && string.IsNullOrWhiteSpace(objPRL295.ProtocolNo))
                    {
                        db.PRL295.Remove(objPRL295);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL295.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL296> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL296> lstAddPRL296 = new List<PRL296>();
                List<PRL296> lstDeletePRL296 = new List<PRL296>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL296 obj = db.PRL296.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL296();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActOutsideDiameter = item.ActOutsideDiameter;
                        obj.ActInsideDiameter = item.ActInsideDiameter;

                        if (isAdded)
                        {
                            lstAddPRL296.Add(obj);
                        }
                    }
                    if (lstAddPRL296.Count > 0)
                    {
                        db.PRL296.AddRange(lstAddPRL296);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL296 = db.PRL296.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL296.Count > 0)
                    {
                        db.PRL296.RemoveRange(lstDeletePRL296);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL296 = db.PRL296.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL296.Count > 0)
                    {
                        db.PRL296.RemoveRange(lstDeletePRL296);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL297> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL297> lstAddPRL297 = new List<PRL297>();
                List<PRL297> lstDeletePRL297 = new List<PRL297>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL297 obj = db.PRL297.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL297();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNoOffset = item.SeamNoOffset;
                        obj.ActMaxOffset = item.ActMaxOffset;

                        if (isAdded)
                        {
                            lstAddPRL297.Add(obj);
                        }
                    }
                    if (lstAddPRL297.Count > 0)
                    {
                        db.PRL297.AddRange(lstAddPRL297);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL297 = db.PRL297.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL297.Count > 0)
                    {
                        db.PRL297.RemoveRange(lstDeletePRL297);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL297 = db.PRL297.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL297.Count > 0)
                    {
                        db.PRL297.RemoveRange(lstDeletePRL297);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 Linkage
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL298> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL298> lstAddPRL298 = new List<PRL298>();
                List<PRL298> lstDeletePRL298 = new List<PRL298>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (!string.IsNullOrWhiteSpace(item.OrientationFlatness))
                        {

                            bool isAdded = false;
                            PRL298 obj = db.PRL298.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj == null)
                            {
                                obj = new PRL298();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                            else
                            {
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }

                            obj.OrientationFlatness = item.OrientationFlatness;
                            obj.Location1 = item.Location1;
                            obj.Location2 = item.Location2;
                            obj.Location3 = item.Location3;

                            if (isAdded)
                            {
                                lstAddPRL298.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL298.Count > 0)
                    {
                        db.PRL298.AddRange(lstAddPRL298);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL298 = db.PRL298.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL298.Count > 0)
                    {
                        db.PRL298.RemoveRange(lstDeletePRL298);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL298 = db.PRL298.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL298.Count > 0)
                    {
                        db.PRL298.RemoveRange(lstDeletePRL298);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}