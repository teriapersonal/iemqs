﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol083Controller : clsBase
    {
        // GET: PROTOCOL/Protocol083
        string ControllerURL = "/PROTOCOL/Protocol083/";
        string Title = "REPORT FOR VISUAL AND DIMENSION INSPECTION OF SKIRT ASSEMBLY";


        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO410 objPRO410 = new PRO410();

            if (!id.HasValue)
            {
                try
                {
                    objPRO410.ProtocolNo = string.Empty;
                    objPRO410.CreatedBy = objClsLoginInfo.UserName;
                    objPRO410.CreatedOn = DateTime.Now;

                    #region  CIRCUMFERENCE MEASUREMENT | PRO410
                    objPRO410.PRO411.Add(new PRO411
                    {

                        Location = "TOP",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO411.Add(new PRO411
                    {

                        Location = "BOTTOM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region  TOTAL SKIRT HEIGHT | PRO410
                    objPRO410.PRO412.Add(new PRO412
                    {

                        Orientation = "0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO412.Add(new PRO412
                    {

                        Orientation = "45°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO412.Add(new PRO412
                    {

                        Orientation = "90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO412.Add(new PRO412
                    {

                        Orientation = "135°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO412.Add(new PRO412
                    {

                        Orientation = "180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO412.Add(new PRO412
                    {

                        Orientation = "225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO412.Add(new PRO412
                    {

                        Orientation = "270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO412.Add(new PRO412
                    {

                        Orientation = "315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region  OUT OF ROUNDNESS | PRO410
                    objPRO410.PRO413.Add(new PRO413
                    {

                        Orientation = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO413.Add(new PRO413
                    {

                        Orientation = "22.5°-205.5",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO413.Add(new PRO413
                    {

                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO413.Add(new PRO413
                    {

                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO413.Add(new PRO413
                    {

                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO413.Add(new PRO413
                    {

                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO413.Add(new PRO413
                    {

                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO413.Add(new PRO413
                    {

                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region  CHARACTERISTICS / DESCRIPTION | PRO410
                    objPRO410.PRO414_2.Add(new PRO414_2
                    {
                        SrNo = 1,
                        Characteristics = "ELEVATION OF REF. LINE FROM B.T.L",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_2.Add(new PRO414_2
                    {
                        SrNo = 2,
                        Characteristics = "DIAMETER OF TAILING LUG HOLES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_2.Add(new PRO414_2
                    {
                        SrNo = 3,
                        Characteristics = "DISTANCE BETWEEN BOTH TAILING LUGS",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_2.Add(new PRO414_2
                    {
                        SrNo = 4,
                        Characteristics = "TAILING LUG THICKNESS WITH CHICK PLATES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    #region  HOLE TO HOLE1 | PRO410
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H1",
                        HoleToHole2 = "H17",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H2",
                        HoleToHole2 = "H18",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H3",
                        HoleToHole2 = "H19",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H4",
                        HoleToHole2 = "H20",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H5",
                        HoleToHole2 = "H21",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H6",
                        HoleToHole2 = "H22",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now



                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H7",
                        HoleToHole2 = "H23",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H8",
                        HoleToHole2 = "H24",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H9",
                        HoleToHole2 = "H25",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H10",
                        HoleToHole2 = "H26",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H11",
                        HoleToHole2 = "H27",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H12",
                        HoleToHole2 = "H28",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H13",
                        HoleToHole2 = "H29",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H14",
                        HoleToHole2 = "H30",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H15",
                        HoleToHole2 = "H31",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_3.Add(new PRO414_3
                    {



                        HoleToHole1 = "H16",
                        HoleToHole2 = "H32",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });



                    #endregion

                    #region  HOLE1 | PRO410
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H1",
                        Hole2 = "H17",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H2",
                        Hole2 = "H18",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H3",
                        Hole2 = "H19",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H4",
                        Hole2 = "H20",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H5",
                        Hole2 = "H21",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H6",
                        Hole2 = "H22",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H7",
                        Hole2 = "H23",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H8",
                        Hole2 = "H24",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H9",
                        Hole2 = "H25",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H10",
                        Hole2 = "H26",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H11",
                        Hole2 = "H27",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H12",
                        Hole2 = "H28",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H13",
                        Hole2 = "H29",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H14",
                        Hole2 = "H30",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H15",
                        Hole2 = "H31",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_4.Add(new PRO414_4
                    {

                        Hole1 = "H16",
                        Hole2 = "H32",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    #region  OrientationDistance | PRO410
                    objPRO410.PRO414_5.Add(new PRO414_5
                    {

                        OrientationDistance = "0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_5.Add(new PRO414_5
                    {

                        OrientationDistance = "45°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_5.Add(new PRO414_5
                    {

                        OrientationDistance = "90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_5.Add(new PRO414_5
                    {

                        OrientationDistance = "135°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_5.Add(new PRO414_5
                    {

                        OrientationDistance = "180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_5.Add(new PRO414_5
                    {

                        OrientationDistance = "225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_5.Add(new PRO414_5
                    {

                        OrientationDistance = "270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO410.PRO414_5.Add(new PRO414_5
                    {

                        OrientationDistance = "315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    db.PRO410.Add(objPRO410);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO410.HeaderId;
            }
            else
            {
                objPRO410 = db.PRO410.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();
            List<string> lstElevationFrom = new List<string> { "TAN LINE", "REF. LINE" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationFrom = lstElevationFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();






            #region bind lines
            List<PRO411> lstPRO411 = db.PRO411.Where(x => x.HeaderId == objPRO410.HeaderId).ToList();
            List<PRO412> lstPRO412 = db.PRO412.Where(x => x.HeaderId == objPRO410.HeaderId).ToList();
            List<PRO413> lstPRO413 = db.PRO413.Where(x => x.HeaderId == objPRO410.HeaderId).ToList();
            List<PRO414> lstPRO414 = db.PRO414.Where(x => x.HeaderId == objPRO410.HeaderId).ToList();
            List<PRO414_2> lstPRO414_2 = db.PRO414_2.Where(x => x.HeaderId == objPRO410.HeaderId).ToList();
            List<PRO414_3> lstPRO414_3 = db.PRO414_3.Where(x => x.HeaderId == objPRO410.HeaderId).ToList();
            List<PRO414_4> lstPRO414_4 = db.PRO414_4.Where(x => x.HeaderId == objPRO410.HeaderId).ToList();
            List<PRO414_5> lstPRO414_5 = db.PRO414_5.Where(x => x.HeaderId == objPRO410.HeaderId).ToList();
            List<PRO414_6> lstPRO414_6 = db.PRO414_6.Where(x => x.HeaderId == objPRO410.HeaderId).ToList();
            List<PRO414_7> lstPRO414_7 = db.PRO414_7.Where(x => x.HeaderId == objPRO410.HeaderId).ToList();

            ViewBag.lstPRO411 = lstPRO411;
            ViewBag.lstPRO412 = lstPRO412;
            ViewBag.lstPRO413 = lstPRO413;
            ViewBag.lstPRO414 = lstPRO414;
            ViewBag.lstPRO414_2 = lstPRO414_2;
            ViewBag.lstPRO414_3 = lstPRO414_3;
            ViewBag.lstPRO414_4 = lstPRO414_4;
            ViewBag.lstPRO414_5 = lstPRO414_5;
            ViewBag.lstPRO414_6 = lstPRO414_6;
            ViewBag.lstPRO414_7 = lstPRO414_7;

            #endregion

            return View(objPRO410);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO410 PRO410)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO410.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO410 objPRO410 = db.PRO410.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO410.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO410.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO410.ProtocolNo = PRO410.ProtocolNo;
                        objPRO410.ReqIDCfCircumference = PRO410.ReqIDCfCircumference;
                        objPRO410.ReqODCfCircumference = PRO410.ReqODCfCircumference;
                        objPRO410.ReqTotalSkirtHeight = PRO410.ReqTotalSkirtHeight;
                        objPRO410.ActCfTop = PRO410.ActCfTop;
                        objPRO410.ActCfBot = PRO410.ActCfBot;
                        objPRO410.FormulaTop1 = PRO410.FormulaTop1;
                        objPRO410.FormulaTop2 = PRO410.FormulaTop2;
                        objPRO410.FormulaTop3 = PRO410.FormulaTop3;
                        objPRO410.FormulaTop4 = PRO410.FormulaTop4;
                        objPRO410.FormulaBot1 = PRO410.FormulaBot1;
                        objPRO410.FormulaBot2 = PRO410.FormulaBot2;
                        objPRO410.FormulaBot3 = PRO410.FormulaBot3;
                        objPRO410.FormulaBot4 = PRO410.FormulaBot4;
                        objPRO410.ActTop1 = PRO410.ActTop1;
                        objPRO410.ActTop2 = PRO410.ActTop2;
                        objPRO410.ActTop3 = PRO410.ActTop3;
                        objPRO410.ActTop4 = PRO410.ActTop4;
                        objPRO410.ActBot1 = PRO410.ActBot1;
                        objPRO410.ActBot2 = PRO410.ActBot2;
                        objPRO410.ActBot3 = PRO410.ActBot3;
                        objPRO410.ActBot4 = PRO410.ActBot4;
                        objPRO410.AtTopOutOfRoundness = PRO410.AtTopOutOfRoundness;
                        objPRO410.AtBottomOutOfRoundness = PRO410.AtBottomOutOfRoundness;
                        objPRO410.OutByOrientation = PRO410.OutByOrientation;
                        objPRO410.OutByActAtTop = PRO410.OutByActAtTop;
                        objPRO410.OutByActAtBottom = PRO410.OutByActAtBottom;
                        objPRO410.ElevationFrom = PRO410.ElevationFrom;
                        objPRO410.ReqBoltCircleDiameter = PRO410.ReqBoltCircleDiameter;
                        objPRO410.ReqBoltHoleDiameter = PRO410.ReqBoltHoleDiameter;
                        objPRO410.ReqDistance = PRO410.ReqDistance;
                        objPRO410.OutByLocation = PRO410.OutByLocation;
                        objPRO410.OutByAt0 = PRO410.OutByAt0;
                        objPRO410.OutByAt90 = PRO410.OutByAt90;
                        objPRO410.OutByAt180 = PRO410.OutByAt180;
                        objPRO410.OutByAt270 = PRO410.OutByAt270;
                        objPRO410.Orient0 = PRO410.Orient0;
                        objPRO410.Orient90 = PRO410.Orient90;
                        objPRO410.Orient180 = PRO410.Orient180;
                        objPRO410.Orient270 = PRO410.Orient270;
                        objPRO410.ReqConcentricity = PRO410.ReqConcentricity;
                        objPRO410.ActConcentricity = PRO410.ActConcentricity;
                        objPRO410.TolFlatnessReadings = PRO410.TolFlatnessReadings;
                        objPRO410.CheckPoint1 = PRO410.CheckPoint1;
                        objPRO410.CheckPoint4 = PRO410.CheckPoint4;
                        objPRO410.CheckPoint5 = PRO410.CheckPoint5;
                        objPRO410.CheckPoint6 = PRO410.CheckPoint6;
                        objPRO410.CheckPoint7 = PRO410.CheckPoint7;
                        objPRO410.CheckPoint8 = PRO410.CheckPoint8;
                        objPRO410.CheckPoint9 = PRO410.CheckPoint9;
                        objPRO410.CheckPoint10 = PRO410.CheckPoint10;
                        objPRO410.CheckPoint11 = PRO410.CheckPoint11;
                        objPRO410.CheckPoint12 = PRO410.CheckPoint12;
                        objPRO410.CheckPoint13 = PRO410.CheckPoint13;
                        objPRO410.CheckPoint14 = PRO410.CheckPoint14;
                        objPRO410.CheckPoint15 = PRO410.CheckPoint15;
                        objPRO410.CheckPoint16 = PRO410.CheckPoint16;
                        objPRO410.CheckPoint17 = PRO410.CheckPoint17;
                        objPRO410.CheckPoint18 = PRO410.CheckPoint18;
                        objPRO410.CheckPoint19 = PRO410.CheckPoint19;
                        objPRO410.CheckPoint20 = PRO410.CheckPoint20;
                        objPRO410.CheckPoint20_2 = PRO410.CheckPoint20_2;
                        objPRO410.QCRemarks = PRO410.QCRemarks;
                        objPRO410.Result = PRO410.Result;
                        objPRO410.EditedBy = objClsLoginInfo.UserName;
                        objPRO410.EditedOn = DateTime.Now;



                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO410.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO410 objPRO410 = db.PRO410.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO410 != null && string.IsNullOrWhiteSpace(objPRO410.ProtocolNo))
                    {
                        db.PRO410.Remove(objPRO410);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO410.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO411> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO411> lstAddPRO411 = new List<PRO411>();
                List<PRO411> lstDeletePRO411 = new List<PRO411>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO411 obj = db.PRO411.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO411();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Location = item.Location;
                        obj.ActIDCF = item.ActIDCF;
                        obj.ActIODCF = item.ActIODCF;

                        if (isAdded)
                        {
                            lstAddPRO411.Add(obj);
                        }
                    }
                    if (lstAddPRO411.Count > 0)
                    {
                        db.PRO411.AddRange(lstAddPRO411);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO411 = db.PRO411.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO411.Count > 0)
                    {
                        db.PRO411.RemoveRange(lstDeletePRO411);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO411 = db.PRO411.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO411.Count > 0)
                    {
                        db.PRO411.RemoveRange(lstDeletePRO411);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO412> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO412> lstAddPRO412 = new List<PRO412>();
                List<PRO412> lstDeletePRO412 = new List<PRO412>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO412 obj = db.PRO412.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO412();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.Actual = item.Actual;





                        if (isAdded)
                        {
                            lstAddPRO412.Add(obj);
                        }
                    }
                    if (lstAddPRO412.Count > 0)
                    {
                        db.PRO412.AddRange(lstAddPRO412);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO412 = db.PRO412.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO412.Count > 0)
                    {
                        db.PRO412.RemoveRange(lstDeletePRO412);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO412 = db.PRO412.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO412.Count > 0)
                    {
                        db.PRO412.RemoveRange(lstDeletePRO412);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO413> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO413> lstAddPRO413 = new List<PRO413>();
                List<PRO413> lstDeletePRO413 = new List<PRO413>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO413 obj = db.PRO413.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO413();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.Orientation))
                        {
                            obj.Orientation = item.Orientation;
                            obj.ActAtTop = item.ActAtTop;
                            obj.ActAtBottom = item.ActAtBottom;

                            if (isAdded)
                            {
                                lstAddPRO413.Add(obj);
                            }

                        }
                    }
                    if (lstAddPRO413.Count > 0)
                    {
                        db.PRO413.AddRange(lstAddPRO413);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO413 = db.PRO413.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO413.Count > 0)
                    {
                        db.PRO413.RemoveRange(lstDeletePRO413);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO413 = db.PRO413.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO413.Count > 0)
                    {
                        db.PRO413.RemoveRange(lstDeletePRO413);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO414> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO414> lstAddPRO414 = new List<PRO414>();
                List<PRO414> lstDeletePRO414 = new List<PRO414>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO414 obj = db.PRO414.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO414();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.ReqElevationFrom = item.ReqElevationFrom;
                        obj.ActElevationFrom = item.ActElevationFrom;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ReqArcLengthOrientation = item.ReqArcLengthOrientation;
                        obj.ActArcLengthOrientation = item.ActArcLengthOrientation;
                        obj.ReqProjection = item.ReqProjection;
                        obj.ActProjection = item.ActProjection;
                        obj.ReqTilt = item.ReqTilt;
                        obj.ActTilt = item.ActTilt;
                        obj.ReqSize = item.ReqSize;
                        obj.ActSize = item.ActSize;
                        obj.OrientationOutofRoundness = item.OrientationOutofRoundness;
                        obj.ActualAtTopOutofRoundenss = item.ActualAtTopOutofRoundenss;
                        obj.ActualAtBottomOutofRoundenss = item.ActualAtBottomOutofRoundenss;




                        if (isAdded)
                        {
                            lstAddPRO414.Add(obj);
                        }
                    }
                    if (lstAddPRO414.Count > 0)
                    {
                        db.PRO414.AddRange(lstAddPRO414);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO414 = db.PRO414.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414.Count > 0)
                    {
                        db.PRO414.RemoveRange(lstDeletePRO414);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO414 = db.PRO414.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414.Count > 0)
                    {
                        db.PRO414.RemoveRange(lstDeletePRO414);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO414_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO414_2> lstAddPRO414_2 = new List<PRO414_2>();
                List<PRO414_2> lstDeletePRO414_2 = new List<PRO414_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO414_2 obj = db.PRO414_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO414_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }



                        obj.SrNo = item.SrNo;
                        obj.Characteristics = item.Characteristics;
                        obj.Required = item.Required;
                        obj.Actual = item.Actual;
                        obj.Remarks = item.Remarks;





                        if (isAdded)
                        {
                            lstAddPRO414_2.Add(obj);
                        }
                    }
                    if (lstAddPRO414_2.Count > 0)
                    {
                        db.PRO414_2.AddRange(lstAddPRO414_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO414_2 = db.PRO414_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414_2.Count > 0)
                    {
                        db.PRO414_2.RemoveRange(lstDeletePRO414_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO414_2 = db.PRO414_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414_2.Count > 0)
                    {
                        db.PRO414_2.RemoveRange(lstDeletePRO414_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6(List<PRO414_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO414_3> lstAddPRO414_3 = new List<PRO414_3>();
                List<PRO414_3> lstDeletePRO414_3 = new List<PRO414_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO414_3 obj = db.PRO414_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO414_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.HoleToHole1 = item.HoleToHole1;
                        obj.HoleToHole2 = item.HoleToHole2;
                        obj.ActBoltCircle = item.ActBoltCircle;

                        if (isAdded)
                        {
                            lstAddPRO414_3.Add(obj);
                        }
                    }
                    if (lstAddPRO414_3.Count > 0)
                    {
                        db.PRO414_3.AddRange(lstAddPRO414_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO414_3 = db.PRO414_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414_3.Count > 0)
                    {
                        db.PRO414_3.RemoveRange(lstDeletePRO414_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO414_3 = db.PRO414_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414_3.Count > 0)
                    {
                        db.PRO414_3.RemoveRange(lstDeletePRO414_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7(List<PRO414_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO414_4> lstAddPRO414_4 = new List<PRO414_4>();
                List<PRO414_4> lstDeletePRO414_4 = new List<PRO414_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO414_4 obj = db.PRO414_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO414_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Hole1 = item.Hole1;
                        obj.Act1 = item.Act1;
                        obj.Hole2 = item.Hole2;
                        obj.Act2 = item.Act2;



                        if (isAdded)
                        {
                            lstAddPRO414_4.Add(obj);
                        }
                    }
                    if (lstAddPRO414_4.Count > 0)
                    {
                        db.PRO414_4.AddRange(lstAddPRO414_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO414_4 = db.PRO414_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414_4.Count > 0)
                    {
                        db.PRO414_4.RemoveRange(lstDeletePRO414_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO414_4 = db.PRO414_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414_4.Count > 0)
                    {
                        db.PRO414_4.RemoveRange(lstDeletePRO414_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details
        [HttpPost]
        public JsonResult SaveProtocolLine8(List<PRO414_5> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO414_5> lstAddPRO414_5 = new List<PRO414_5>();
                List<PRO414_5> lstDeletePRO414_5 = new List<PRO414_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO414_5 obj = db.PRO414_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO414_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }




                        obj.OrientationDistance = item.OrientationDistance;
                        obj.ActualDistance = item.ActualDistance;




                        if (isAdded)
                        {
                            lstAddPRO414_5.Add(obj);
                        }
                    }
                    if (lstAddPRO414_5.Count > 0)
                    {
                        db.PRO414_5.AddRange(lstAddPRO414_5);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO414_5 = db.PRO414_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414_5.Count > 0)
                    {
                        db.PRO414_5.RemoveRange(lstDeletePRO414_5);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO414_5 = db.PRO414_5.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414_5.Count > 0)
                    {
                        db.PRO414_5.RemoveRange(lstDeletePRO414_5);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 details
        [HttpPost]
        public JsonResult SaveProtocolLine9(List<PRO414_6> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO414_6> lstAddPRO414_6 = new List<PRO414_6>();
                List<PRO414_6> lstDeletePRO414_6 = new List<PRO414_6>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO414_6 obj = db.PRO414_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO414_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }



                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.ActAt0 = item.ActAt0;
                            obj.ActAt90 = item.ActAt90;
                            obj.ActAt180 = item.ActAt180;
                            obj.ActAt270 = item.ActAt270;


                            if (isAdded)
                            {
                                lstAddPRO414_6.Add(obj);
                            }

                        }
                    }
                    if (lstAddPRO414_6.Count > 0)
                    {
                        db.PRO414_6.AddRange(lstAddPRO414_6);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO414_6 = db.PRO414_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414_6.Count > 0)
                    {
                        db.PRO414_6.RemoveRange(lstDeletePRO414_6);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO414_6 = db.PRO414_6.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414_6.Count > 0)
                    {
                        db.PRO414_6.RemoveRange(lstDeletePRO414_6);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line10 details
        [HttpPost]
        public JsonResult SaveProtocolLine10(List<PRO414_7> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO414_7> lstAddPRO414_7 = new List<PRO414_7>();
                List<PRO414_7> lstDeletePRO414_7 = new List<PRO414_7>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO414_7 obj = db.PRO414_7.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO414_7();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.BrlToTrl))
                        {
                            obj.SrNo = item.SrNo; ;
                            obj.BrlToTrl = item.BrlToTrl;
                            obj.TrlToopenEnd = item.TrlToopenEnd;
                            obj.YInFlatness = item.YInFlatness;
                            obj.YMidFlatness = item.YMidFlatness;
                            obj.YoutFlatness = item.YoutFlatness;
                            obj.Hcalculated = item.Hcalculated;
                            obj.Xcalculated = item.Xcalculated;
                            obj.YMincalculated = item.YMincalculated;
                            obj.TotalH = item.TotalH;

                            if (isAdded)
                            {
                                lstAddPRO414_7.Add(obj);
                            }
                        }



                    }
                    if (lstAddPRO414_7.Count > 0)
                    {
                        db.PRO414_7.AddRange(lstAddPRO414_7);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO414_7 = db.PRO414_7.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414_7.Count > 0)
                    {
                        db.PRO414_7.RemoveRange(lstDeletePRO414_7);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO414_7 = db.PRO414_7.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO414_7.Count > 0)
                    {
                        db.PRO414_7.RemoveRange(lstDeletePRO414_7);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region  Linkage View Code   

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL410 objPRL410 = new PRL410();
            objPRL410 = db.PRL410.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL410 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL410.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstElevationFrom = new List<string> { "TAN LINE", "REF. LINE" };


            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationFrom = lstElevationFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL411> lstPRL411 = db.PRL411.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL412> lstPRL412 = db.PRL412.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL413> lstPRL413 = db.PRL413.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414> lstPRL414 = db.PRL414.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414_2> lstPRL414_2 = db.PRL414_2.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414_3> lstPRL414_3 = db.PRL414_3.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414_4> lstPRL414_4 = db.PRL414_4.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414_5> lstPRL414_5 = db.PRL414_5.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414_6> lstPRL414_6 = db.PRL414_6.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414_7> lstPRL414_7 = db.PRL414_7.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();

            ViewBag.lstPRL411 = lstPRL411;
            ViewBag.lstPRL412 = lstPRL412;
            ViewBag.lstPRL413 = lstPRL413;
            ViewBag.lstPRL414 = lstPRL414;
            ViewBag.lstPRL414_2 = lstPRL414_2;
            ViewBag.lstPRL414_3 = lstPRL414_3;
            ViewBag.lstPRL414_4 = lstPRL414_4;
            ViewBag.lstPRL414_5 = lstPRL414_5;
            ViewBag.lstPRL414_6 = lstPRL414_6;
            ViewBag.lstPRL414_7 = lstPRL414_7;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL410.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL410.ActFilledBy) && (objPRL410.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL410.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL410);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL410 objPRL410 = new PRL410();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL410 = db.PRL410.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL410).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL410 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL410.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstElevationFrom = new List<string> { "TAN LINE", "REF. LINE" };


            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ElevationFrom = lstElevationFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL411> lstPRL411 = db.PRL411.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL412> lstPRL412 = db.PRL412.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL413> lstPRL413 = db.PRL413.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414> lstPRL414 = db.PRL414.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414_2> lstPRL414_2 = db.PRL414_2.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414_3> lstPRL414_3 = db.PRL414_3.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414_4> lstPRL414_4 = db.PRL414_4.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414_5> lstPRL414_5 = db.PRL414_5.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414_6> lstPRL414_6 = db.PRL414_6.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();
            List<PRL414_7> lstPRL414_7 = db.PRL414_7.Where(x => x.HeaderId == objPRL410.HeaderId).ToList();

            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL410.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL410.ActFilledBy) && (objPRL410.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL410.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    ElevationFrom = ElevationFrom,

                    objPRL410 = objPRL410,

                    lstPRL411 = lstPRL411,
                    lstPRL412 = lstPRL412,
                    lstPRL413 = lstPRL413,
                    lstPRL414 = lstPRL414,
                    lstPRL414_2 = lstPRL414_2,
                    lstPRL414_3 = lstPRL414_3,
                    lstPRL414_4 = lstPRL414_4,
                    lstPRL414_5 = lstPRL414_5,
                    lstPRL414_6 = lstPRL414_6,
                    lstPRL414_7 = lstPRL414_7,

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL410 PRL410, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL410.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL410 objPRL410 = db.PRL410.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL410.DrawingRevisionNo = PRL410.DrawingRevisionNo;
                    objPRL410.DrawingNo = PRL410.DrawingNo;
                    objPRL410.DCRNo = PRL410.DCRNo;
                    objPRL410.ProtocolNo = PRL410.ProtocolNo;
                    objPRL410.ReqIDCfCircumference = PRL410.ReqIDCfCircumference;
                    objPRL410.ReqODCfCircumference = PRL410.ReqODCfCircumference;
                    objPRL410.ReqTotalSkirtHeight = PRL410.ReqTotalSkirtHeight;
                    objPRL410.ActCfTop = PRL410.ActCfTop;
                    objPRL410.ActCfBot = PRL410.ActCfBot;
                    objPRL410.FormulaTop1 = PRL410.FormulaTop1;
                    objPRL410.FormulaTop2 = PRL410.FormulaTop2;
                    objPRL410.FormulaTop3 = PRL410.FormulaTop3;
                    objPRL410.FormulaTop4 = PRL410.FormulaTop4;
                    objPRL410.FormulaBot1 = PRL410.FormulaBot1;
                    objPRL410.FormulaBot2 = PRL410.FormulaBot2;
                    objPRL410.FormulaBot3 = PRL410.FormulaBot3;
                    objPRL410.FormulaBot4 = PRL410.FormulaBot4;
                    objPRL410.ActTop1 = PRL410.ActTop1;
                    objPRL410.ActTop2 = PRL410.ActTop2;
                    objPRL410.ActTop3 = PRL410.ActTop3;
                    objPRL410.ActTop4 = PRL410.ActTop4;
                    objPRL410.ActBot1 = PRL410.ActBot1;
                    objPRL410.ActBot2 = PRL410.ActBot2;
                    objPRL410.ActBot3 = PRL410.ActBot3;
                    objPRL410.ActBot4 = PRL410.ActBot4;
                    objPRL410.AtTopOutOfRoundness = PRL410.AtTopOutOfRoundness;
                    objPRL410.AtBottomOutOfRoundness = PRL410.AtBottomOutOfRoundness;
                    objPRL410.OutByOrientation = PRL410.OutByOrientation;
                    objPRL410.OutByActAtTop = PRL410.OutByActAtTop;
                    objPRL410.OutByActAtBottom = PRL410.OutByActAtBottom;
                    objPRL410.ElevationFrom = PRL410.ElevationFrom;
                    objPRL410.ReqBoltCircleDiameter = PRL410.ReqBoltCircleDiameter;
                    objPRL410.ReqBoltHoleDiameter = PRL410.ReqBoltHoleDiameter;
                    objPRL410.ReqDistance = PRL410.ReqDistance;
                    objPRL410.OutByLocation = PRL410.OutByLocation;
                    objPRL410.OutByAt0 = PRL410.OutByAt0;
                    objPRL410.OutByAt90 = PRL410.OutByAt90;
                    objPRL410.OutByAt180 = PRL410.OutByAt180;
                    objPRL410.OutByAt270 = PRL410.OutByAt270;
                    objPRL410.Orient0 = PRL410.Orient0;
                    objPRL410.Orient90 = PRL410.Orient90;
                    objPRL410.Orient180 = PRL410.Orient180;
                    objPRL410.Orient270 = PRL410.Orient270;
                    objPRL410.ReqConcentricity = PRL410.ReqConcentricity;
                    objPRL410.ActConcentricity = PRL410.ActConcentricity;
                    objPRL410.TolFlatnessReadings = PRL410.TolFlatnessReadings;
                    objPRL410.CheckPoint1 = PRL410.CheckPoint1;
                    objPRL410.CheckPoint4 = PRL410.CheckPoint4;
                    objPRL410.CheckPoint5 = PRL410.CheckPoint5;
                    objPRL410.CheckPoint6 = PRL410.CheckPoint6;
                    objPRL410.CheckPoint7 = PRL410.CheckPoint7;
                    objPRL410.CheckPoint8 = PRL410.CheckPoint8;
                    objPRL410.CheckPoint9 = PRL410.CheckPoint9;
                    objPRL410.CheckPoint10 = PRL410.CheckPoint10;
                    objPRL410.CheckPoint11 = PRL410.CheckPoint11;
                    objPRL410.CheckPoint12 = PRL410.CheckPoint12;
                    objPRL410.CheckPoint13 = PRL410.CheckPoint13;
                    objPRL410.CheckPoint14 = PRL410.CheckPoint14;
                    objPRL410.CheckPoint15 = PRL410.CheckPoint15;
                    objPRL410.CheckPoint16 = PRL410.CheckPoint16;
                    objPRL410.CheckPoint17 = PRL410.CheckPoint17;
                    objPRL410.CheckPoint18 = PRL410.CheckPoint18;
                    objPRL410.CheckPoint19 = PRL410.CheckPoint19;
                    objPRL410.CheckPoint20 = PRL410.CheckPoint20;
                    objPRL410.CheckPoint20_2 = PRL410.CheckPoint20_2;
                    objPRL410.QCRemarks = PRL410.QCRemarks;
                    objPRL410.Result = PRL410.Result;
                    objPRL410.EditedBy = UserName;
                    objPRL410.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL410.ActFilledBy = UserName;
                            objPRL410.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL410.ReqFilledBy = UserName;
                            objPRL410.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL410.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL410 objPRL410 = db.PRL410.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL410 != null && string.IsNullOrWhiteSpace(objPRL410.ProtocolNo))
                    {
                        db.PRL410.Remove(objPRL410);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL410.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL411> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL411> lstAddPRL411 = new List<PRL411>();
                List<PRL411> lstDeletePRL411 = new List<PRL411>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL411 obj = db.PRL411.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL411();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Location = item.Location;
                        obj.ActIDCF = item.ActIDCF;
                        obj.ActIODCF = item.ActIODCF;

                        if (isAdded)
                        {
                            lstAddPRL411.Add(obj);
                        }
                    }
                    if (lstAddPRL411.Count > 0)
                    {
                        db.PRL411.AddRange(lstAddPRL411);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL411 = db.PRL411.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL411.Count > 0)
                    {
                        db.PRL411.RemoveRange(lstDeletePRL411);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL411 = db.PRL411.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL411.Count > 0)
                    {
                        db.PRL411.RemoveRange(lstDeletePRL411);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL412> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL412> lstAddPRL412 = new List<PRL412>();
                List<PRL412> lstDeletePRL412 = new List<PRL412>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL412 obj = db.PRL412.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL412();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.Actual = item.Actual;





                        if (isAdded)
                        {
                            lstAddPRL412.Add(obj);
                        }
                    }
                    if (lstAddPRL412.Count > 0)
                    {
                        db.PRL412.AddRange(lstAddPRL412);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL412 = db.PRL412.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL412.Count > 0)
                    {
                        db.PRL412.RemoveRange(lstDeletePRL412);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL412 = db.PRL412.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL412.Count > 0)
                    {
                        db.PRL412.RemoveRange(lstDeletePRL412);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL413> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL413> lstAddPRL413 = new List<PRL413>();
                List<PRL413> lstDeletePRL413 = new List<PRL413>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL413 obj = db.PRL413.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL413();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActAtTop = item.ActAtTop;
                        obj.ActAtBottom = item.ActAtBottom;



                        if (!string.IsNullOrWhiteSpace(item.Orientation))
                        {
                            obj.Orientation = item.Orientation;
                            obj.ActAtTop = item.ActAtTop;
                            obj.ActAtBottom = item.ActAtBottom;

                            if (isAdded)
                            {
                                lstAddPRL413.Add(obj);
                            }

                        }
                    }
                    if (lstAddPRL413.Count > 0)
                    {
                        db.PRL413.AddRange(lstAddPRL413);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL413 = db.PRL413.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL413.Count > 0)
                    {
                        db.PRL413.RemoveRange(lstDeletePRL413);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL413 = db.PRL413.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL413.Count > 0)
                    {
                        db.PRL413.RemoveRange(lstDeletePRL413);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL414> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL414> lstAddPRL414 = new List<PRL414>();
                List<PRL414> lstDeletePRL414 = new List<PRL414>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL414 obj = db.PRL414.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL414();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.ReqElevationFrom = item.ReqElevationFrom;
                        obj.ActElevationFrom = item.ActElevationFrom;
                        obj.ReqOrientation = item.ReqOrientation;
                        obj.ReqArcLengthOrientation = item.ReqArcLengthOrientation;
                        obj.ActArcLengthOrientation = item.ActArcLengthOrientation;
                        obj.ReqProjection = item.ReqProjection;
                        obj.ActProjection = item.ActProjection;
                        obj.ReqTilt = item.ReqTilt;
                        obj.ActTilt = item.ActTilt;
                        obj.ReqSize = item.ReqSize;
                        obj.ActSize = item.ActSize;
                        obj.OrientationOutofRoundness = item.OrientationOutofRoundness;
                        obj.ActualAtTopOutofRoundenss = item.ActualAtTopOutofRoundenss;
                        obj.ActualAtBottomOutofRoundenss = item.ActualAtBottomOutofRoundenss;




                        if (isAdded)
                        {
                            lstAddPRL414.Add(obj);
                        }
                    }
                    if (lstAddPRL414.Count > 0)
                    {
                        db.PRL414.AddRange(lstAddPRL414);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL414 = db.PRL414.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414.Count > 0)
                    {
                        db.PRL414.RemoveRange(lstDeletePRL414);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL414 = db.PRL414.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414.Count > 0)
                    {
                        db.PRL414.RemoveRange(lstDeletePRL414);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL414_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL414_2> lstAddPRL414_2 = new List<PRL414_2>();
                List<PRL414_2> lstDeletePRL414_2 = new List<PRL414_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL414_2 obj = db.PRL414_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL414_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }



                        obj.SrNo = item.SrNo;
                        obj.Characteristics = item.Characteristics;
                        obj.Required = item.Required;
                        obj.Actual = item.Actual;
                        obj.Remarks = item.Remarks;





                        if (isAdded)
                        {
                            lstAddPRL414_2.Add(obj);
                        }
                    }
                    if (lstAddPRL414_2.Count > 0)
                    {
                        db.PRL414_2.AddRange(lstAddPRL414_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL414_2 = db.PRL414_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414_2.Count > 0)
                    {
                        db.PRL414_2.RemoveRange(lstDeletePRL414_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL414_2 = db.PRL414_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414_2.Count > 0)
                    {
                        db.PRL414_2.RemoveRange(lstDeletePRL414_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6Linkage(List<PRL414_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL414_3> lstAddPRL414_3 = new List<PRL414_3>();
                List<PRL414_3> lstDeletePRL414_3 = new List<PRL414_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL414_3 obj = db.PRL414_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL414_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.HoleToHole1 = item.HoleToHole1;
                        obj.HoleToHole2 = item.HoleToHole2;
                        obj.ActBoltCircle = item.ActBoltCircle;

                        if (isAdded)
                        {
                            lstAddPRL414_3.Add(obj);
                        }
                    }
                    if (lstAddPRL414_3.Count > 0)
                    {
                        db.PRL414_3.AddRange(lstAddPRL414_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL414_3 = db.PRL414_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414_3.Count > 0)
                    {
                        db.PRL414_3.RemoveRange(lstDeletePRL414_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL414_3 = db.PRL414_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414_3.Count > 0)
                    {
                        db.PRL414_3.RemoveRange(lstDeletePRL414_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7Linkage(List<PRL414_4> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL414_4> lstAddPRL414_4 = new List<PRL414_4>();
                List<PRL414_4> lstDeletePRL414_4 = new List<PRL414_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL414_4 obj = db.PRL414_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL414_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Hole1 = item.Hole1;
                        obj.Act1 = item.Act1;
                        obj.Hole2 = item.Hole2;
                        obj.Act2 = item.Act2;



                        if (isAdded)
                        {
                            lstAddPRL414_4.Add(obj);
                        }
                    }
                    if (lstAddPRL414_4.Count > 0)
                    {
                        db.PRL414_4.AddRange(lstAddPRL414_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL414_4 = db.PRL414_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414_4.Count > 0)
                    {
                        db.PRL414_4.RemoveRange(lstDeletePRL414_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL414_4 = db.PRL414_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414_4.Count > 0)
                    {
                        db.PRL414_4.RemoveRange(lstDeletePRL414_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details
        [HttpPost]
        public JsonResult SaveProtocolLine8Linkage(List<PRL414_5> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL414_5> lstAddPRL414_5 = new List<PRL414_5>();
                List<PRL414_5> lstDeletePRL414_5 = new List<PRL414_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL414_5 obj = db.PRL414_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL414_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }




                        obj.OrientationDistance = item.OrientationDistance;
                        obj.ActualDistance = item.ActualDistance;




                        if (isAdded)
                        {
                            lstAddPRL414_5.Add(obj);
                        }
                    }
                    if (lstAddPRL414_5.Count > 0)
                    {
                        db.PRL414_5.AddRange(lstAddPRL414_5);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL414_5 = db.PRL414_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414_5.Count > 0)
                    {
                        db.PRL414_5.RemoveRange(lstDeletePRL414_5);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL414_5 = db.PRL414_5.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414_5.Count > 0)
                    {
                        db.PRL414_5.RemoveRange(lstDeletePRL414_5);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 details
        [HttpPost]
        public JsonResult SaveProtocolLine9Linkage(List<PRL414_6> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL414_6> lstAddPRL414_6 = new List<PRL414_6>();
                List<PRL414_6> lstDeletePRL414_6 = new List<PRL414_6>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL414_6 obj = db.PRL414_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL414_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.ActAt0 = item.ActAt0;
                            obj.ActAt90 = item.ActAt90;
                            obj.ActAt180 = item.ActAt180;
                            obj.ActAt270 = item.ActAt270;


                            if (isAdded)
                            {
                                lstAddPRL414_6.Add(obj);
                            }

                        }
                    }
                    if (lstAddPRL414_6.Count > 0)
                    {
                        db.PRL414_6.AddRange(lstAddPRL414_6);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL414_6 = db.PRL414_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414_6.Count > 0)
                    {
                        db.PRL414_6.RemoveRange(lstDeletePRL414_6);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL414_6 = db.PRL414_6.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414_6.Count > 0)
                    {
                        db.PRL414_6.RemoveRange(lstDeletePRL414_6);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line10 details
        [HttpPost]
        public JsonResult SaveProtocolLine10Linkage(List<PRL414_7> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL414_7> lstAddPRL414_7 = new List<PRL414_7>();
                List<PRL414_7> lstDeletePRL414_7 = new List<PRL414_7>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL414_7 obj = db.PRL414_7.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL414_7();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.BrlToTrl))
                        {
                            obj.SrNo = item.SrNo; ;
                            obj.BrlToTrl = item.BrlToTrl;
                            obj.TrlToopenEnd = item.TrlToopenEnd;
                            obj.YInFlatness = item.YInFlatness;
                            obj.YMidFlatness = item.YMidFlatness;
                            obj.YoutFlatness = item.YoutFlatness;
                            obj.Hcalculated = item.Hcalculated;
                            obj.Xcalculated = item.Xcalculated;
                            obj.YMincalculated = item.YMincalculated;
                            obj.TotalH = item.TotalH;

                            if (isAdded)
                            {
                                lstAddPRL414_7.Add(obj);
                            }
                        }

                    }
                    if (lstAddPRL414_7.Count > 0)
                    {
                        db.PRL414_7.AddRange(lstAddPRL414_7);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL414_7 = db.PRL414_7.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414_7.Count > 0)
                    {
                        db.PRL414_7.RemoveRange(lstDeletePRL414_7);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL414_7 = db.PRL414_7.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL414_7.Count > 0)
                    {
                        db.PRL414_7.RemoveRange(lstDeletePRL414_7);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

    }
}