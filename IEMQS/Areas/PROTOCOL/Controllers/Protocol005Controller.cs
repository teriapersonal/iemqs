﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol005Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol005/";

        // GET: PROTOCOL/Protocol005
        #region Protocol Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id, int? m)
        {//PRO020 
            PRO020 objPRO020 = new PRO020();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "VISUAL & DIMENSION REPORT FOR ROLLED SHELL LONGITUDINAL SEAM";
            ViewBag.ControllerURL = ControllerURL;
            if (!id.HasValue)
            {
                objPRO020.ProtocolNo = string.Empty;
                objPRO020.CreatedBy = objClsLoginInfo.UserName;
                objPRO020.CreatedOn = DateTime.Now;
                db.PRO020.Add(objPRO020);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO020.HeaderId;
            }
            else
            {
                objPRO020 = db.PRO020.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO020);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO020 obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = obj.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO020 objPRO020 = db.PRO020.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var lstPRO020 = db.PRO020.Where(o => o.HeaderId != refHeaderId && o.ProtocolNo == obj.ProtocolNo).ToList();
                    if (lstPRO020.Count == 0)
                    {
                        #region Save Data
                        objPRO020.ProtocolNo = obj.ProtocolNo;
                        objPRO020.OvalityReq1 = obj.OvalityReq1;
                        objPRO020.OvalityReq2 = obj.OvalityReq2;
                        objPRO020.OvalityReq3 = obj.OvalityReq3;
                        objPRO020.OvalityReq4 = obj.OvalityReq4;
                        objPRO020.OvalityReq5 = obj.OvalityReq5;
                        objPRO020.OvalityReq6 = obj.OvalityReq6;
                        objPRO020.OvalityReq7 = obj.OvalityReq7;
                        objPRO020.OvalityReq8 = obj.OvalityReq8;
                        objPRO020.OvalityActTop1 = obj.OvalityActTop1;
                        objPRO020.OvalityActTop2 = obj.OvalityActTop2;
                        objPRO020.OvalityActTop3 = obj.OvalityActTop3;
                        objPRO020.OvalityActTop4 = obj.OvalityActTop4;
                        objPRO020.OvalityActTop5 = obj.OvalityActTop5;
                        objPRO020.OvalityActTop6 = obj.OvalityActTop6;
                        objPRO020.OvalityActTop7 = obj.OvalityActTop7;
                        objPRO020.OvalityActTop8 = obj.OvalityActTop8;
                        objPRO020.OvalityActBottom1 = obj.OvalityActBottom1;
                        objPRO020.OvalityActBottom2 = obj.OvalityActBottom2;
                        objPRO020.OvalityActBottom3 = obj.OvalityActBottom3;
                        objPRO020.OvalityActBottom4 = obj.OvalityActBottom4;
                        objPRO020.OvalityActBottom5 = obj.OvalityActBottom5;
                        objPRO020.OvalityActBottom6 = obj.OvalityActBottom6;
                        objPRO020.OvalityActBottom7 = obj.OvalityActBottom7;
                        objPRO020.OvalityActBottom8 = obj.OvalityActBottom8;
                        objPRO020.CreatedBy = obj.CreatedBy;
                        objPRO020.CreatedOn = obj.CreatedOn;
                        objPRO020.EditedBy = obj.EditedBy;
                        objPRO020.EditedOn = obj.EditedOn;
                        objPRO020.OvalityRemarks = obj.OvalityRemarks;
                        objPRO020.Ovality = obj.Ovality;
                        objPRO020.OvalityBottom = obj.OvalityBottom;
                        objPRO020.OutsideCFReq = obj.OutsideCFReq;
                        objPRO020.InsideCFReq = obj.InsideCFReq;
                        objPRO020.OutsideTop = obj.OutsideTop;
                        objPRO020.OutsideMiddle = obj.OutsideMiddle;
                        objPRO020.OutsideBottom = obj.OutsideBottom;
                        objPRO020.OutsideRemark = obj.OutsideRemark;
                        objPRO020.InsideTop = obj.InsideTop;
                        objPRO020.InsideMiddle = obj.InsideMiddle;
                        objPRO020.InsideBottom = obj.InsideBottom;
                        objPRO020.InsideRemark = obj.InsideRemark;
                        objPRO020.ProfileMeasurement = obj.ProfileMeasurement;
                        objPRO020.LabelReq1 = obj.LabelReq1;
                        objPRO020.LabelReq2 = obj.LabelReq2;
                        objPRO020.LabelReq3 = obj.LabelReq3;
                        objPRO020.LabelReq4 = obj.LabelReq4;
                        objPRO020.LabelAct1 = obj.LabelAct1;
                        objPRO020.LabelAct2 = obj.LabelAct2;
                        objPRO020.LabelAct3 = obj.LabelAct3;
                        objPRO020.LabelAct4 = obj.LabelAct4;
                        objPRO020.Note1 = obj.Note1;
                        objPRO020.Note2 = obj.Note2;
                        objPRO020.Note2Date = obj.Note2Date.HasValue ? Manager.getDateTime(obj.Note2Date.Value.ToShortDateString()) : obj.Note2Date;
                        objPRO020.Note4 = obj.Note4;
                        objPRO020.Note5 = obj.Note5;
                        objPRO020.CladStripingWidthMax = obj.CladStripingWidthMax;
                        objPRO020.CladStripingWidthMin = obj.CladStripingWidthMin;
                        objPRO020.CladStripingDepthMax = obj.CladStripingDepthMax;
                        objPRO020.CladStripingDepthMin = obj.CladStripingDepthMin;
                        objPRO020.FNote1 = obj.FNote1;
                        objPRO020.FNote2 = obj.FNote2;
                        objPRO020.FNote3 = obj.FNote3;
                        objPRO020.CladdedVessel = obj.CladdedVessel;
                        objPRO020.EditedBy = objClsLoginInfo.UserName;
                        objPRO020.EditedOn = DateTime.Now;


                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolSaved;
                        objResponseMsg.HeaderId = objPRO020.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolExists;
                    }

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId > 0)
                {
                    List<PRO023> lstPRO023 = db.PRO023.Where(c => c.HeaderId == refHeaderId).ToList();
                    List<PRO022> lstPRO022 = db.PRO022.Where(c => c.HeaderId == refHeaderId).ToList();
                    List<PRO021> lstPRO021 = db.PRO021.Where(c => c.HeaderId == refHeaderId).ToList();
                    PRO020 objPRO020 = db.PRO020.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO020 != null && string.IsNullOrWhiteSpace(objPRO020.ProtocolNo))
                    {
                        if (lstPRO023.Count > 0)
                        {
                            db.PRO023.RemoveRange(lstPRO023);
                            db.SaveChanges();
                        }
                        if (lstPRO022.Count > 0)
                        {
                            db.PRO022.RemoveRange(lstPRO022);
                            db.SaveChanges();
                        }
                        if (lstPRO021.Count > 0)
                        {
                            db.PRO021.RemoveRange(lstPRO021);
                            db.SaveChanges();
                        }
                        db.PRO020.Remove(objPRO020);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_REPORT_FOR_ROLLED_SHELL_LONGITUDINAL_SEAM.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO020.HeaderId;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region protocol Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND  (SeamNo like '%" + param.sSearch
                                         + "%' or  TopPeackIn like '%" + param.sSearch
                                         + "%' or  TopPeakOut like '%" + param.sSearch
                                         + "%' or  MidPeakIn like '%" + param.sSearch
                                         + "%' or MidPeakOut like '%" + param.sSearch
                                         + "%' or  BottomPeakIn like '%" + param.sSearch
                                         + "%' or  BottomPeakOut like '%" + param.sSearch
                                         + "%' or Remarks like '%" + param.sSearch
                                         + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL005_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO020 objPRO020 = db.PRO020.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "1",
                                    Convert.ToString(headerId),
                                    Convert.ToString(newRecordId),
                                    Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "20","QC"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "TopPeackIn", "", "", false, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "TopPeakOut", "", "", false, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "MidPeakIn", "", "", false, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "MidPeakOut", "", "", false, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "BottomPeakIn", "", "", false, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "BottomPeakOut", "", "", false, "", false, "10","PROD"),
                                    Helper.GenerateTextArea(newRecordId, "Remarks", "", "", false, "", "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                    "1",
                                    Convert.ToString(uc.HeaderId),
                                    Convert.ToString(uc.LineId),
                                    Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo",    Convert.ToString(uc.SeamNo), "", true, "", false, "20","QC"),
                                    Helper.GenerateHTMLTextbox(uc.LineId, "TopPeackIn",Convert.ToString(uc.TopPeackIn), "", true, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(uc.LineId, "TopPeakOut",Convert.ToString(uc.TopPeakOut), "", true, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(uc.LineId, "MidPeakIn", Convert.ToString(uc.MidPeakIn), "", true, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(uc.LineId, "MidPeakOut",Convert.ToString(uc.MidPeakOut), "", true, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(uc.LineId, "BottomPeakIn", Convert.ToString(uc.BottomPeakIn),"", true, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(uc.LineId, "BottomPeakOut",Convert.ToString(uc.BottomPeakOut), "", true, "", false, "10","PROD"),
                                    Helper.GenerateTextArea(uc.LineId, "Remarks",  Convert.ToString(uc.Remarks), "", true, "", "100","PROD"),

                                    Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                               + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO021 objPRO021 = new PRO021();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRO020 objPRO020 = db.PRO020.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    string SeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";

                    decimal? strSpotWidth = Convert.ToDecimal(Convert.ToString(fc["SpotWidth" + refLineId]));//  !string.IsNullOrEmpty(fc["SpotWidth" + newRowIndex]) ? Convert.ToDecimal(fc["SpotWidth" + newRowIndex]) : 0;
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : string.Empty;


                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO021 = db.PRO021.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO021.EditedBy = objClsLoginInfo.UserName;
                        objPRO021.EditedOn = DateTime.Now;
                    }

                    objPRO021.HeaderId = refHeaderId;
                    objPRO021.SeamNo = SeamNo;
                    objPRO021.TopPeackIn = !string.IsNullOrEmpty(fc["TopPeackIn" + refLineId]) ? Convert.ToString(fc["TopPeackIn" + refLineId]).Trim() : "";
                    objPRO021.TopPeakOut = !string.IsNullOrEmpty(fc["TopPeakOut" + refLineId]) ? Convert.ToString(fc["TopPeakOut" + refLineId]).Trim() : "";
                    objPRO021.MidPeakIn = !string.IsNullOrEmpty(fc["MidPeakIn" + refLineId]) ? Convert.ToString(fc["MidPeakIn" + refLineId]).Trim() : "";
                    objPRO021.MidPeakOut = !string.IsNullOrEmpty(fc["MidPeakOut" + refLineId]) ? Convert.ToString(fc["MidPeakOut" + refLineId]).Trim() : "";
                    objPRO021.BottomPeakIn = !string.IsNullOrEmpty(fc["BottomPeakIn" + refLineId]) ? Convert.ToString(fc["BottomPeakIn" + refLineId]).Trim() : "";
                    objPRO021.BottomPeakOut = !string.IsNullOrEmpty(fc["BottomPeakOut" + refLineId]) ? Convert.ToString(fc["BottomPeakOut" + refLineId]).Trim() : "";

                    objPRO021.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Seam updated successfully";
                    }
                    else
                    {
                        objPRO021.CreatedBy = objClsLoginInfo.UserName;
                        objPRO021.CreatedOn = DateTime.Now;
                        db.PRO021.Add(objPRO021);
                        db.SaveChanges();
                        objResponseMsg.Value = "Seam added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO021.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO021 objPRO021 = db.PRO021.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO021 != null)
                {
                    db.PRO021.Remove(objPRO021);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Seam deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region protocol Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro022.SpotNo like '%" + param.sSearch
                        + "%' or pro022.Remarks like '%" + param.sSearch
                        + "%' or pro022.Width like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL005_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO020 objPRO020 = db.PRO020.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                    "2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "SpotWidth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "widthRemarks", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                   "2",
                                   Convert.ToString(uc.HeaderId),
                                   Convert.ToString(uc.LineId),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo",      Convert.ToString(uc.SpotNo), "", true, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId, "SpotWidth",Convert.ToString(uc.SpotWidth), "", true, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "widthRemarks",Convert.ToString(uc.Remarks), "", true, "", false, "100","PROD"),
                                   Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") +
                                   (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2','tblProtocolLines2')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO022 objPRO022 = new PRO022();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    PRO020 objPRO020 = db.PRO020.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    string intSpotNo = !string.IsNullOrEmpty(fc["SpotNo" + refLineId]) ? Convert.ToString(fc["SpotNo" + refLineId]).Trim() : "";

                    #region Add New Spot Line2
                    if (refLineId > 0)
                    {
                        objPRO022 = db.PRO022.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO022.EditedBy = objClsLoginInfo.UserName;
                        objPRO022.EditedOn = DateTime.Now;
                    }

                    objPRO022.HeaderId = objPRO020.HeaderId;
                    objPRO022.SpotNo = intSpotNo;
                    objPRO022.Width = Convert.ToDecimal(Convert.ToString(fc["SpotWidth" + refLineId]));
                    objPRO022.Remarks = fc["widthRemarks" + refLineId];
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRO022.CreatedBy = objClsLoginInfo.UserName;
                        objPRO022.CreatedOn = DateTime.Now;
                        db.PRO022.Add(objPRO022);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }


                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO022.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO022 objPRO022 = db.PRO022.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO022 != null)
                {
                    db.PRO022.Remove(objPRO022);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region protocol Line3 details
        [HttpPost]
        public ActionResult GetProtocolLines3Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro023.SpotNo like '%" + param.sSearch
                        + "%' or pro023.Remarks like '%" + param.sSearch
                        + "%' or pro023.Depth like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL005_GET_LINES3_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO020 objPRO020 = db.PRO020.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "3",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo2", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "SpotDepth2", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks2", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine3(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                   "3",
                                   Convert.ToString(uc.HeaderId),
                                   Convert.ToString(uc.LineId),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo2", Convert.ToString(uc.SpotNo), "", true, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId, "SpotDepth2",Convert.ToString(uc.SpotDepth), "", true, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "Remarks2",  Convert.ToString(uc.Remarks),"", true, "", false, "100","PROD"),
                                   Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                               + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue().ToUpper()?   Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o","DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine3','tblProtocolLines3')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine3(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO023 objPRO023 = new PRO023();
            int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRO020 objPRO020 = db.PRO020.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    string intSpotNo = !string.IsNullOrEmpty(fc["SpotNo2" + refLineId]) ? Convert.ToString(fc["SpotNo2" + refLineId]).Trim() : "";
                    decimal? strSpotDepth = Convert.ToDecimal(Convert.ToString(fc["SpotDepth2" + refLineId])); // !string.IsNullOrEmpty(fc["SpotDepth2" + newRowIndex]) ? Convert.ToDecimal(fc["SpotDepth2" + newRowIndex]) : 0;
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks2" + refLineId]) ? Convert.ToString(fc["Remarks2" + refLineId]).Trim() : string.Empty;

                    #region Add New Spot Line3
                    if (refLineId > 0)
                    {
                        objPRO023 = db.PRO023.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO023.EditedBy = objClsLoginInfo.UserName;
                        objPRO023.EditedOn = DateTime.Now;
                    }

                    objPRO023.HeaderId = objPRO020.HeaderId;
                    objPRO023.SpotNo = intSpotNo;
                    objPRO023.Depth = strSpotDepth;
                    objPRO023.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRO023.CreatedBy = objClsLoginInfo.UserName;
                        objPRO023.CreatedOn = DateTime.Now;
                        db.PRO023.Add(objPRO023);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO023.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine3(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO023 objPRO023 = db.PRO023.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO023 != null)
                {
                    db.PRO023.Remove(objPRO023);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Linkage Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Linkage(int? id, int? m)
        {//PRL020 
            PRL020 objPRL020 = new PRL020();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();


            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "VISUAL & DIMENSION REPORT FOR ROLLED SHELL LONGITUDINAL SEAM";
            ViewBag.ControllerURL = ControllerURL;
            if (!id.HasValue)
            {
                //   objPRL020.QualityProject = "0017005A/A";
                objPRL020.ProtocolNo = string.Empty;
                objPRL020.CreatedBy = objClsLoginInfo.UserName;
                objPRL020.CreatedOn = DateTime.Now;
                db.PRL020.Add(objPRL020);
                db.SaveChanges();
                ViewBag.HeaderId = objPRL020.HeaderId;
            }
            else
            {
                objPRL020 = db.PRL020.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL020.QualityProject).FirstOrDefault();
                ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRL020);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL020 obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = obj.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL020 objPRL020 = db.PRL020.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data

                    objPRL020.DCRNo = obj.DCRNo;
                    objPRL020.ProtocolNo = obj.ProtocolNo != null ? obj.ProtocolNo : "";
                    objPRL020.OvalityReq1 = obj.OvalityReq1;
                    objPRL020.OvalityReq2 = obj.OvalityReq2;
                    objPRL020.OvalityReq3 = obj.OvalityReq3;
                    objPRL020.OvalityReq4 = obj.OvalityReq4;
                    objPRL020.OvalityReq5 = obj.OvalityReq5;
                    objPRL020.OvalityReq6 = obj.OvalityReq6;
                    objPRL020.OvalityReq7 = obj.OvalityReq7;
                    objPRL020.OvalityReq8 = obj.OvalityReq8;
                    objPRL020.OvalityActTop1 = obj.OvalityActTop1;
                    objPRL020.OvalityActTop2 = obj.OvalityActTop2;
                    objPRL020.OvalityActTop3 = obj.OvalityActTop3;
                    objPRL020.OvalityActTop4 = obj.OvalityActTop4;
                    objPRL020.OvalityActTop5 = obj.OvalityActTop5;
                    objPRL020.OvalityActTop6 = obj.OvalityActTop6;
                    objPRL020.OvalityActTop7 = obj.OvalityActTop7;
                    objPRL020.OvalityActTop8 = obj.OvalityActTop8;
                    objPRL020.OvalityActBottom1 = obj.OvalityActBottom1;
                    objPRL020.OvalityActBottom2 = obj.OvalityActBottom2;
                    objPRL020.OvalityActBottom3 = obj.OvalityActBottom3;
                    objPRL020.OvalityActBottom4 = obj.OvalityActBottom4;
                    objPRL020.OvalityActBottom5 = obj.OvalityActBottom5;
                    objPRL020.OvalityActBottom6 = obj.OvalityActBottom6;
                    objPRL020.OvalityActBottom7 = obj.OvalityActBottom7;
                    objPRL020.OvalityActBottom8 = obj.OvalityActBottom8;
                    objPRL020.OvalityRemarks = obj.OvalityRemarks;
                    objPRL020.Ovality = obj.Ovality;
                    objPRL020.OvalityBottom = obj.OvalityBottom;
                    objPRL020.OutsideCFReq = obj.OutsideCFReq;
                    objPRL020.InsideCFReq = obj.InsideCFReq;
                    objPRL020.OutsideTop = obj.OutsideTop;
                    objPRL020.OutsideMiddle = obj.OutsideMiddle;
                    objPRL020.OutsideBottom = obj.OutsideBottom;
                    objPRL020.OutsideRemark = obj.OutsideRemark;
                    objPRL020.InsideTop = obj.InsideTop;
                    objPRL020.InsideMiddle = obj.InsideMiddle;
                    objPRL020.InsideBottom = obj.InsideBottom;
                    objPRL020.InsideRemark = obj.InsideRemark;
                    objPRL020.ProfileMeasurement = obj.ProfileMeasurement;
                    objPRL020.LabelReq1 = obj.LabelReq1;
                    objPRL020.LabelReq2 = obj.LabelReq2;
                    objPRL020.LabelReq3 = obj.LabelReq3;
                    objPRL020.LabelReq4 = obj.LabelReq4;
                    objPRL020.LabelAct1 = obj.LabelAct1;
                    objPRL020.LabelAct2 = obj.LabelAct2;
                    objPRL020.LabelAct3 = obj.LabelAct3;
                    objPRL020.LabelAct4 = obj.LabelAct4;
                    objPRL020.Note1 = obj.Note1;
                    objPRL020.Note2 = obj.Note2;
                    objPRL020.Note2Date = obj.Note2Date;

                    objPRL020.Note4 = obj.Note4;
                    objPRL020.Note5 = obj.Note5;
                    objPRL020.CladStripingWidthMax = obj.CladStripingWidthMax;
                    objPRL020.CladStripingWidthMin = obj.CladStripingWidthMin;
                    objPRL020.CladStripingDepthMax = obj.CladStripingDepthMax;
                    objPRL020.CladStripingDepthMin = obj.CladStripingDepthMin;
                    objPRL020.FNote1 = obj.FNote1;
                    objPRL020.FNote2 = obj.FNote2;
                    objPRL020.FNote3 = obj.FNote3;
                    objPRL020.CladdedVessel = obj.CladdedVessel;
                    objPRL020.EditedBy = objClsLoginInfo.UserName;
                    objPRL020.EditedOn = DateTime.Now;


                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.LinkageSaved;
                    objResponseMsg.HeaderId = objPRL020.HeaderId;

                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Linkage Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND  (SeamNo like '%" + param.sSearch
                                        + "%' or  TopPeackIn like '%" + param.sSearch
                                        + "%' or  TopPeakOut like '%" + param.sSearch
                                        + "%' or  MidPeakIn like '%" + param.sSearch
                                        + "%' or MidPeakOut like '%" + param.sSearch
                                        + "%' or  BottomPeakIn like '%" + param.sSearch
                                        + "%' or  BottomPeakOut like '%" + param.sSearch
                                        + "%' or Remarks like '%" + param.sSearch
                                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL005_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRL020 objPRL020 = db.PRL020.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                    "1",
                                    Convert.ToString(headerId),
                                    Convert.ToString(newRecordId),
                                    Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "20","QC"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "TopPeackIn", "", "", false, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "TopPeakOut", "", "", false, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "MidPeakIn", "", "", false, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "MidPeakOut", "", "", false, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "BottomPeakIn", "", "", false, "", false, "10","PROD"),
                                    Helper.GenerateHTMLTextbox(newRecordId, "BottomPeakOut", "", "", false, "", false, "10","PROD"),
                                    Helper.GenerateTextArea(newRecordId, "Remarks", "", "", false, "", "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo",     Convert.ToString(uc.SeamNo), "", true, "", false, "20","QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "TopPeackIn", Convert.ToString(uc.TopPeackIn), "", true, "", false, "10","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "TopPeakOut", Convert.ToString(uc.TopPeakOut), "", true, "", false, "10","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "MidPeakIn",  Convert.ToString(uc.MidPeakIn), "", true, "", false, "10","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "MidPeakOut", Convert.ToString(uc.MidPeakOut), "", true, "", false, "10","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId,"BottomPeakIn",Convert.ToString(uc.BottomPeakIn), "", true, "", false, "10","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId,"BottomPeakOut", Convert.ToString(uc.BottomPeakOut), "", true, "", false, "10","PROD"),
                                Helper.GenerateTextArea(uc.LineId, "Remarks",  Convert.ToString(uc.Remarks), "", true, "", "100","PROD"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Profile Measurement", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL021 objPRL021 = new PRL021();
            int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRL020 objPRL020 = db.PRL020.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    string SeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";

                    decimal? strSpotWidth = Convert.ToDecimal(Convert.ToString(fc["SpotWidth" + refLineId]));//  !string.IsNullOrEmpty(fc["SpotWidth" + newRowIndex]) ? Convert.ToDecimal(fc["SpotWidth" + newRowIndex]) : 0;
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : string.Empty;

                    if (refLineId > 0)
                    {
                        objPRL021 = db.PRL021.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL021.EditedBy = objClsLoginInfo.UserName;
                        objPRL021.EditedOn = DateTime.Now;
                    }
                    #region Add New Spot Line1

                    objPRL021.HeaderId = refHeaderId;
                    objPRL021.SeamNo = SeamNo;
                    objPRL021.TopPeackIn = !string.IsNullOrEmpty(fc["TopPeackIn" + refLineId]) ? Convert.ToString(fc["TopPeackIn" + refLineId]).Trim() : "";
                    objPRL021.TopPeakOut = !string.IsNullOrEmpty(fc["TopPeakOut" + refLineId]) ? Convert.ToString(fc["TopPeakOut" + refLineId]).Trim() : "";
                    objPRL021.MidPeakIn = !string.IsNullOrEmpty(fc["MidPeakIn" + refLineId]) ? Convert.ToString(fc["MidPeakIn" + refLineId]).Trim() : "";
                    objPRL021.MidPeakOut = !string.IsNullOrEmpty(fc["MidPeakOut" + refLineId]) ? Convert.ToString(fc["MidPeakOut" + refLineId]).Trim() : "";
                    objPRL021.BottomPeakIn = !string.IsNullOrEmpty(fc["BottomPeakIn" + refLineId]) ? Convert.ToString(fc["BottomPeakIn" + refLineId]).Trim() : "";
                    objPRL021.BottomPeakOut = !string.IsNullOrEmpty(fc["BottomPeakOut" + refLineId]) ? Convert.ToString(fc["BottomPeakOut" + refLineId]).Trim() : "";
                    objPRL021.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Seam updated successfully";
                    }
                    else
                    {
                        objPRL021.CreatedBy = objClsLoginInfo.UserName;
                        objPRL021.CreatedOn = DateTime.Now;
                        db.PRL021.Add(objPRL021);
                        db.SaveChanges();
                        objResponseMsg.Value = "Seam added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL021.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL021 objPRL021 = db.PRL021.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL021 != null)
                {
                    db.PRL021.Remove(objPRL021);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Seam deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Linkage Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (prl022.SpotNo like '%" + param.sSearch
                        + "%' or prl022.Remarks like '%" + param.sSearch
                        + "%' or prl022.Width like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL005_LINKAGE_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRL020 objPRL020 = db.PRL020.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                  "2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "SpotWidth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "widthRemarks", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                   "2",
                                   Convert.ToString(uc.HeaderId),
                                   Convert.ToString(uc.LineId),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo",       Convert.ToString(uc.SpotNo),"", true, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId, "SpotWidth", Convert.ToString(uc.SpotWidth),"", true, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "widthRemarks", Convert.ToString(uc.Remarks),"", true, "", false, "100","PROD"),
                                   Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot Width", "fa fa-trash-o",  "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2Linkage','tblProtocolLines2')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL022 objPRL022 = new PRL022();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRL020 objPRL020 = db.PRL020.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    string intSpotNo = !string.IsNullOrEmpty(fc["SpotNo" + refLineId]) ? Convert.ToString(fc["SpotNo" + refLineId]).Trim() : "";

                    #region Add New Spot Line2
                    if (refLineId > 0)
                    {
                        objPRL022 = db.PRL022.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL022.EditedBy = objClsLoginInfo.UserName;
                        objPRL022.EditedOn = DateTime.Now;
                    }
                    objPRL022.HeaderId = objPRL020.HeaderId;
                    objPRL022.SpotNo = intSpotNo;
                    objPRL022.Width = Convert.ToDecimal(Convert.ToString(fc["SpotWidth" + refLineId]));
                    objPRL022.Remarks = fc["widthRemarks" + refLineId];
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRL022.CreatedBy = objClsLoginInfo.UserName;
                        objPRL022.CreatedOn = DateTime.Now;
                        db.PRL022.Add(objPRL022);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL022.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL022 objPRL022 = db.PRL022.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL022 != null)
                {
                    db.PRL022.Remove(objPRL022);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Linkage Line3 details
        [HttpPost]
        public ActionResult GetProtocolLines3DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (prl023.SpotNo like '%" + param.sSearch
                        + "%' or prl023.Remarks like '%" + param.sSearch
                        + "%' or prl023.Depth like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL005_LINKAGE_GET_LINES3_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRL020 objPRL020 = db.PRL020.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                  "3",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo2", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "SpotDepth2", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks2", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine3(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                  "3",
                                   Convert.ToString(uc.HeaderId),
                                   Convert.ToString(uc.LineId),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo2",    Convert.ToString(uc.SpotNo), "", true, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId, "SpotDepth2",   Convert.ToString(uc.SpotDepth), "", true, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(uc.LineId, "Remarks2",    Convert.ToString(uc.Remarks), "", true, "", false, "100","PROD"),
                                   Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue()? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot Depth", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine3Linkage','tblProtocolLines3')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine3Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL023 objPRL023 = new PRL023();
            int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRL020 objPRL020 = db.PRL020.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    string intSpotNo = !string.IsNullOrEmpty(fc["SpotNo2" + refLineId]) ? Convert.ToString(fc["SpotNo2" + refLineId]).Trim() : "";
                    decimal? strSpotDepth = Convert.ToDecimal(Convert.ToString(fc["SpotDepth2" + refLineId]));
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks2" + refLineId]) ? Convert.ToString(fc["Remarks2" + refLineId]).Trim() : string.Empty;

                    #region Add New Spot Line3
                    if (refLineId > 0)
                    {
                        objPRL023 = db.PRL023.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL023.EditedBy = objClsLoginInfo.UserName;
                        objPRL023.EditedOn = DateTime.Now;
                    }
                    objPRL023.HeaderId = objPRL020.HeaderId;
                    objPRL023.SpotNo = intSpotNo;
                    objPRL023.Depth = strSpotDepth;
                    objPRL023.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRL023.CreatedBy = objClsLoginInfo.UserName;
                        objPRL023.CreatedOn = DateTime.Now;
                        db.PRL023.Add(objPRL023);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL023.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine3Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL023 objPRL023 = db.PRL023.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL023 != null)
                {
                    db.PRL023.Remove(objPRL023);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Common function
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PROD3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => (i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc == clsImplementationEnum.UserRoleName.QC2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI3.GetStringValue())).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = string.Empty;
                    objUserRoleAccessDetails.UserDesignation = string.Empty;
                }

            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }
        #endregion
    }
}