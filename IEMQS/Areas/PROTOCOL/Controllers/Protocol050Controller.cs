﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol050Controller : clsBase
    {
        // GET: PROTOCOL/Protocol050
        string ControllerURL = "/PROTOCOL/Protocol050/";
        string Title = "VISUAL & DIMENSION REPORT BEFORE WELD OVERALY ON SHELL";

        #region Details View Code

        #region Header

        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO245 objPRO245 = new PRO245();
            if (!id.HasValue)
            {
                try
                {
                    objPRO245.ProtocolNo = string.Empty;
                    objPRO245.CreatedBy = objClsLoginInfo.UserName;
                    objPRO245.CreatedOn = DateTime.Now;


                    #region OUT OF ROUNDNESS
                    objPRO245.PRO246.Add(new PRO246
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO245.PRO246.Add(new PRO246
                    {
                        Orientation = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO245.PRO246.Add(new PRO246
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO245.PRO246.Add(new PRO246
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO245.PRO246.Add(new PRO246
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO245.PRO246.Add(new PRO246
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO245.PRO246.Add(new PRO246
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO245.PRO246.Add(new PRO246
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region CIRCUMFERENCE MEASUREMENT
                    objPRO245.PRO247.Add(new PRO247
                    {
                        Location = "TOP",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO245.PRO247.Add(new PRO247
                    {
                        Location = "MIDDLE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO245.PRO247.Add(new PRO247
                    {
                        Location = "BOTTOM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region TOTAL LENGTH
                    objPRO245.PRO248.Add(new PRO248
                    {
                        RequiredValue = "AT 0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO245.PRO248.Add(new PRO248
                    {
                        RequiredValue = "AT 90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO245.PRO248.Add(new PRO248
                    {
                        RequiredValue = "AT 180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO245.PRO248.Add(new PRO248
                    {
                        RequiredValue = "AT 270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO245.Add(objPRO245);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO245.HeaderId;
            }
            else
            {
                objPRO245 = db.PRO245.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO246> lstPRO246 = db.PRO246.Where(x => x.HeaderId == objPRO245.HeaderId).ToList();
            List<PRO247> lstPRO247 = db.PRO247.Where(x => x.HeaderId == objPRO245.HeaderId).ToList();
            List<PRO248> lstPRO248 = db.PRO248.Where(x => x.HeaderId == objPRO245.HeaderId).ToList();

            ViewBag.lstPRO246 = lstPRO246;
            ViewBag.lstPRO247 = lstPRO247;
            ViewBag.lstPRO248 = lstPRO248;



            #endregion

            return View(objPRO245);
        }
        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO245 PRO245)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO245.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO245 objPRO245 = db.PRO245.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO245.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO245.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO245.ProtocolNo = PRO245.ProtocolNo;

                        objPRO245.ReqIdOutOfRoundness = PRO245.ReqIdOutOfRoundness;
                        objPRO245.OutByOrientation = PRO245.OutByOrientation;
                        objPRO245.OutByActAtTopOpenEnd = PRO245.OutByActAtTopOpenEnd;
                        objPRO245.OutByActAtBottomOpenEnd = PRO245.OutByActAtBottomOpenEnd;
                        objPRO245.ReqIdCf = PRO245.ReqIdCf;
                        objPRO245.ReqOdCf = PRO245.ReqOdCf;
                        objPRO245.ReqTotalLength = PRO245.ReqTotalLength;
                        objPRO245.CheckPoint1 = PRO245.CheckPoint1;
                        objPRO245.CheckPoint2 = PRO245.CheckPoint2;
                        objPRO245.CheckPoint3 = PRO245.CheckPoint3;
                        objPRO245.CheckPoint5 = PRO245.CheckPoint5;
                        objPRO245.CheckPoint5_2 = PRO245.CheckPoint5_2;
                        objPRO245.QCRemarks = PRO245.QCRemarks;
                        objPRO245.Result = PRO245.Result;

                        objPRO245.EditedBy = objClsLoginInfo.UserName;
                        objPRO245.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO245.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO245 objPRO245 = db.PRO245.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO245 != null && string.IsNullOrWhiteSpace(objPRO245.ProtocolNo))
                    {
                        db.PRO245.Remove(objPRO245);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO245.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO246> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO246> lstAddPRO246 = new List<PRO246>();
                List<PRO246> lstDeletePRO246 = new List<PRO246>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO246 obj = db.PRO246.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTopOpenEnd = item.ActAtTopOpenEnd;
                                obj.ActAtBottomOpenEnd = item.ActAtBottomOpenEnd;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO246 obj = new PRO246();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTopOpenEnd = item.ActAtTopOpenEnd;
                                obj.ActAtBottomOpenEnd = item.ActAtBottomOpenEnd;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO246.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO246.Count > 0)
                    {
                        db.PRO246.AddRange(lstAddPRO246);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO246 = db.PRO246.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO246.Count > 0)
                    {
                        db.PRO246.RemoveRange(lstDeletePRO246);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO246 = db.PRO246.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO246.Count > 0)
                    {
                        db.PRO246.RemoveRange(lstDeletePRO246);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO247> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO247> lstAddPRO247 = new List<PRO247>();
                List<PRO247> lstDeletePRO247 = new List<PRO247>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO247 obj = db.PRO247.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO247();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Location = item.Location;
                        obj.ActIdCf = item.ActIdCf;
                        obj.ActOdCf = item.ActOdCf;

                        if (isAdded)
                        {
                            lstAddPRO247.Add(obj);
                        }
                    }
                    if (lstAddPRO247.Count > 0)
                    {
                        db.PRO247.AddRange(lstAddPRO247);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO247 = db.PRO247.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO247.Count > 0)
                    {
                        db.PRO247.RemoveRange(lstDeletePRO247);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO247 = db.PRO247.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO247.Count > 0)
                    {
                        db.PRO247.RemoveRange(lstDeletePRO247);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO248> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO248> lstAddPRO248 = new List<PRO248>();
                List<PRO248> lstDeletePRO248 = new List<PRO248>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO248 obj = db.PRO248.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRO248();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        obj.RequiredValue = item.RequiredValue;
                        obj.ActualValue = item.ActualValue;

                        if (isAdded)
                        {
                            lstAddPRO248.Add(obj);
                        }
                    }
                    if (lstAddPRO248.Count > 0)
                    {
                        db.PRO248.AddRange(lstAddPRO248);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO248 = db.PRO248.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO248.Count > 0)
                    {
                        db.PRO248.RemoveRange(lstDeletePRO248);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO248 = db.PRO248.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO248.Count > 0)
                    {
                        db.PRO248.RemoveRange(lstDeletePRO248);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL245 objPRL245 = new PRL245();
            objPRL245 = db.PRL245.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL245 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL245.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL246> lstPRL246 = db.PRL246.Where(x => x.HeaderId == objPRL245.HeaderId).ToList();
            List<PRL247> lstPRL247 = db.PRL247.Where(x => x.HeaderId == objPRL245.HeaderId).ToList();
            List<PRL248> lstPRL248 = db.PRL248.Where(x => x.HeaderId == objPRL245.HeaderId).ToList();

            ViewBag.lstPRL246 = lstPRL246;
            ViewBag.lstPRL247 = lstPRL247;
            ViewBag.lstPRL248 = lstPRL248;



            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL245.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL245.ActFilledBy) && (objPRL245.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL245.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }

            return View(objPRL245);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL245 objPRL245 = new PRL245();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL245 = db.PRL245.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL245).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL245 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL245.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            if (m == 1)
            { isEditable = true; }
            else
            { isEditable = false; }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL246> lstPRL246 = db.PRL246.Where(x => x.HeaderId == objPRL245.HeaderId).ToList();
            List<PRL247> lstPRL247 = db.PRL247.Where(x => x.HeaderId == objPRL245.HeaderId).ToList();
            List<PRL248> lstPRL248 = db.PRL248.Where(x => x.HeaderId == objPRL245.HeaderId).ToList();

            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL245.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL245.ActFilledBy) && (objPRL245.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL245.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,

                    objPRL245 = objPRL245,

                    lstPRL246 = lstPRL246,
                    lstPRL247 = lstPRL247,
                    lstPRL248 = lstPRL248,


                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message
                }, JsonRequestBehavior.AllowGet);
            }
            
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL245 PRL245, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL245.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL245 objPRL245 = db.PRL245.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data
                    objPRL245.ProtocolNo = PRL245.ProtocolNo;

                    objPRL245.DrawingRevisionNo = PRL245.DrawingRevisionNo;
                    objPRL245.DrawingNo = PRL245.DrawingNo;
                    objPRL245.DCRNo = PRL245.DCRNo;

                    objPRL245.ReqIdOutOfRoundness = PRL245.ReqIdOutOfRoundness;
                    objPRL245.OutByOrientation = PRL245.OutByOrientation;
                    objPRL245.OutByActAtTopOpenEnd = PRL245.OutByActAtTopOpenEnd;
                    objPRL245.OutByActAtBottomOpenEnd = PRL245.OutByActAtBottomOpenEnd;
                    objPRL245.ReqIdCf = PRL245.ReqIdCf;
                    objPRL245.ReqOdCf = PRL245.ReqOdCf;
                    objPRL245.ReqTotalLength = PRL245.ReqTotalLength;
                    objPRL245.CheckPoint1 = PRL245.CheckPoint1;
                    objPRL245.CheckPoint2 = PRL245.CheckPoint2;
                    objPRL245.CheckPoint3 = PRL245.CheckPoint3;
                    objPRL245.CheckPoint5 = PRL245.CheckPoint5;
                    objPRL245.CheckPoint5_2 = PRL245.CheckPoint5_2;
                    objPRL245.QCRemarks = PRL245.QCRemarks;
                    objPRL245.Result = PRL245.Result;
                    objPRL245.EditedBy = UserName;
                    objPRL245.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL245.ActFilledBy = UserName;
                            objPRL245.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL245.ReqFilledBy = UserName;
                            objPRL245.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL245.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL245 objPRL245 = db.PRL245.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL245 != null && string.IsNullOrWhiteSpace(objPRL245.ProtocolNo))
                    {
                        db.PRL245.Remove(objPRL245);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL245.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL246> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL246> lstAddPRL246 = new List<PRL246>();
                List<PRL246> lstDeletePRL246 = new List<PRL246>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL246 obj = db.PRL246.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTopOpenEnd = item.ActAtTopOpenEnd;
                                obj.ActAtBottomOpenEnd = item.ActAtBottomOpenEnd;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL246 obj = new PRL246();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.ActAtTopOpenEnd = item.ActAtTopOpenEnd;
                                obj.ActAtBottomOpenEnd = item.ActAtBottomOpenEnd;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL246.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL246.Count > 0)
                {
                    db.PRL246.AddRange(lstAddPRL246);
                }


                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();
                    lstDeletePRL246 = db.PRL246.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                }
                else
                {
                    lstDeletePRL246 = db.PRL246.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL246.Count > 0)
                {
                    db.PRL246.RemoveRange(lstDeletePRL246);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL247> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL247> lstAddPRL247 = new List<PRL247>();
                List<PRL247> lstDeletePRL247 = new List<PRL247>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL247 obj = db.PRL247.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL247();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Location = item.Location;
                        obj.ActIdCf = item.ActIdCf;
                        obj.ActOdCf = item.ActOdCf;

                        if (isAdded)
                        {
                            lstAddPRL247.Add(obj);
                        }
                    }
                }
                if (lstAddPRL247.Count > 0)
                {
                    db.PRL247.AddRange(lstAddPRL247);
                }

                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL247 = db.PRL247.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL247 = db.PRL247.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL247.Count > 0)
                {
                    db.PRL247.RemoveRange(lstDeletePRL247);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL248> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL248> lstAddPRL248 = new List<PRL248>();
                List<PRL248> lstDeletePRL248 = new List<PRL248>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL248 obj = db.PRL248.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL248();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        obj.RequiredValue = item.RequiredValue;
                        obj.ActualValue = item.ActualValue;

                        if (isAdded)
                        {
                            lstAddPRL248.Add(obj);
                        }
                    }
                }
                if (lstAddPRL248.Count > 0)
                {
                    db.PRL248.AddRange(lstAddPRL248);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL248 = db.PRL248.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL248 = db.PRL248.Where(x => x.HeaderId == HeaderId).ToList();
                }

                if (lstDeletePRL248.Count > 0)
                {
                    db.PRL248.RemoveRange(lstDeletePRL248);
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

    }
}