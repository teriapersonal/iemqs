﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol011Controller : clsBase
    {
        // GET: PROTOCOL/Protocol011

        #region Protocol Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id,int? m)
        {//PRO050 
            PRO050 objPRO050 = new PRO050();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();


            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "WELD VISUAL & DIMENSION REPORT FOR SPOOL TYPE-1";

            if (!id.HasValue)
            {
                objPRO050.ProtocolNo = string.Empty;
                objPRO050.CreatedBy = objClsLoginInfo.UserName;
                objPRO050.CreatedOn = DateTime.Now;
                db.PRO050.Add(objPRO050);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO050.HeaderId;
            }
            else
            {
                objPRO050 = db.PRO050.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO050);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO050 obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = obj.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO050 objPRO050 = db.PRO050.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var lstPRO050 = db.PRO050.Where(o => o.HeaderId != refHeaderId && o.ProtocolNo.Trim() == obj.ProtocolNo.Trim()).ToList();
                    if (lstPRO050.Count == 0)
                    {
                        #region Save Data
                        objPRO050.ProtocolNo = obj.ProtocolNo;
                        objPRO050.BoltHolesStraddledForTopFlange = obj.BoltHolesStraddledForTopFlange;
                        objPRO050.BoltHolesStraddledForTopFlangeTol = obj.BoltHolesStraddledForTopFlangeTol;
                        objPRO050.BoltHolesStraddledForTopFlangeAct = obj.BoltHolesStraddledForTopFlangeAct;
                        objPRO050.BoltHolesStraddledForBottomFlange = obj.BoltHolesStraddledForBottomFlange;
                        objPRO050.BoltHolesStraddledForBottomFlangeTol = obj.BoltHolesStraddledForBottomFlangeTol;
                        objPRO050.BoltHolesStraddledForBottomFlangeAct = obj.BoltHolesStraddledForBottomFlangeAct;
                        objPRO050.WelderStampPunch = obj.WelderStampPunch;
                        objPRO050.SeamNoPunch = obj.SeamNoPunch;
                        objPRO050.TempAttachRemovedGrounded = obj.TempAttachRemovedGrounded;
                        objPRO050.TiltingOfTopFlangeTol = obj.TiltingOfTopFlangeTol;
                        objPRO050.TiltingOfTopFlangeAct = obj.TiltingOfTopFlangeAct;
                        objPRO050.TiltingOfBottomFlangeTol = obj.TiltingOfBottomFlangeTol;
                        objPRO050.TiltingOfBottomFlangeAct = obj.TiltingOfBottomFlangeAct;
                        objPRO050.ReqOrientationOfTopFlangeCenter = obj.ReqOrientationOfTopFlangeCenter;
                        objPRO050.ReqOrientationOfTopFlangeCenterReq = obj.ReqOrientationOfTopFlangeCenterReq;
                        objPRO050.ReqOrientationOfTopFlangeCenterAct = obj.ReqOrientationOfTopFlangeCenterAct;
                        objPRO050.ElevationReq = obj.ElevationReq;
                        objPRO050.ElevationAct = obj.ElevationAct;
                        objPRO050.ProjectionReq = obj.ProjectionReq;
                        objPRO050.ProjectionAct = obj.ProjectionAct;
                        objPRO050.AllPartsAreAttachedToSpool = obj.AllPartsAreAttachedToSpool;
                        objPRO050.DiagramReq = obj.DiagramReq;
                        objPRO050.DiagramAct = obj.DiagramAct;
                        objPRO050.Note1 = obj.Note1;
                        objPRO050.Note3 = obj.Note3;
                        objPRO050.Note4 = obj.Note4;
                        objPRO050.Note5 = obj.Note5;
                     
                        objPRO050.Note5Date = obj.Note5Date.HasValue ? Manager.getDateTime(obj.Note5Date.Value.ToShortDateString()) : obj.Note5Date;

                        objPRO050.Note6 = obj.Note6;
                        objPRO050.Note7 = obj.Note7;
                        objPRO050.Note8 = obj.Note8;
                        objPRO050.Note9 = obj.Note9;
                        objPRO050.Note10 = obj.Note10;
                                           objPRO050.EditedBy = objClsLoginInfo.UserName;
                        objPRO050.EditedOn = DateTime.Now;
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolSaved;
                        objResponseMsg.HeaderId = objPRO050.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolExists;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId > 0)
                {
                    PRO050 objPRO050 = db.PRO050.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO050 != null && string.IsNullOrWhiteSpace(objPRO050.ProtocolNo))
                    {
                        db.PRO050.Remove(objPRO050);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE1.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO050.HeaderId;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Linkage Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Linkage(int? id, int? m)
        {//PRL050 
            PRL050 objPRL050 = db.PRL050.Where(i => i.HeaderId == id).FirstOrDefault();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();


            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "WELD VISUAL & DIMENSION REPORT FOR SPOOL TYPE-1";
            ViewBag.HeaderId = id.Value;
            if (objPRL050 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL050.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRL050);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL050 obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = obj.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL050 objPRL050 = db.PRL050.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data

                    objPRL050.DCRNo = obj.DCRNo;
                    objPRL050.ProtocolNo = obj.ProtocolNo != null ? obj.ProtocolNo : "";
                    objPRL050.BoltHolesStraddledForTopFlange = obj.BoltHolesStraddledForTopFlange;
                    objPRL050.BoltHolesStraddledForTopFlangeTol = obj.BoltHolesStraddledForTopFlangeTol;
                    objPRL050.BoltHolesStraddledForTopFlangeAct = obj.BoltHolesStraddledForTopFlangeAct;
                    objPRL050.BoltHolesStraddledForBottomFlange = obj.BoltHolesStraddledForBottomFlange;
                    objPRL050.BoltHolesStraddledForBottomFlangeTol = obj.BoltHolesStraddledForBottomFlangeTol;
                    objPRL050.BoltHolesStraddledForBottomFlangeAct = obj.BoltHolesStraddledForBottomFlangeAct;
                    objPRL050.WelderStampPunch = obj.WelderStampPunch;
                    objPRL050.SeamNoPunch = obj.SeamNoPunch;
                    objPRL050.TempAttachRemovedGrounded = obj.TempAttachRemovedGrounded;
                    objPRL050.TiltingOfTopFlangeTol = obj.TiltingOfTopFlangeTol;
                    objPRL050.TiltingOfTopFlangeAct = obj.TiltingOfTopFlangeAct;
                    objPRL050.TiltingOfBottomFlangeTol = obj.TiltingOfBottomFlangeTol;
                    objPRL050.TiltingOfBottomFlangeAct = obj.TiltingOfBottomFlangeAct;
                    objPRL050.ReqOrientationOfTopFlangeCenter = obj.ReqOrientationOfTopFlangeCenter;
                    objPRL050.ReqOrientationOfTopFlangeCenterReq = obj.ReqOrientationOfTopFlangeCenterReq;
                    objPRL050.ReqOrientationOfTopFlangeCenterAct = obj.ReqOrientationOfTopFlangeCenterAct;
                    objPRL050.ElevationReq = obj.ElevationReq;
                    objPRL050.ElevationAct = obj.ElevationAct;
                    objPRL050.ProjectionReq = obj.ProjectionReq;
                    objPRL050.ProjectionAct = obj.ProjectionAct;
                    objPRL050.AllPartsAreAttachedToSpool = obj.AllPartsAreAttachedToSpool;
                    objPRL050.DiagramReq = obj.DiagramReq;
                    objPRL050.DiagramAct = obj.DiagramAct;
                    objPRL050.Note1 = obj.Note1;
                    objPRL050.Note3 = obj.Note3;
                    objPRL050.Note4 = obj.Note4;
                    objPRL050.Note5 = obj.Note5;
                    objPRL050.Note5Date = obj.Note5Date.HasValue ? Manager.getDateTime(obj.Note5Date.Value.ToShortDateString()) : obj.Note5Date;

                    objPRL050.Note6 = obj.Note6;
                    objPRL050.Note7 = obj.Note7;
                    objPRL050.Note8 = obj.Note8;
                    objPRL050.Note9 = obj.Note9;
                    objPRL050.Note10 = obj.Note10;
                    objPRL050.EditedBy = objClsLoginInfo.UserName;
                    objPRL050.EditedOn = DateTime.Now;


                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.LinkageSaved;
                    objResponseMsg.HeaderId = objPRL050.HeaderId;

                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Common function
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PROD3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => (i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc == clsImplementationEnum.UserRoleName.QC2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI3.GetStringValue())).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = string.Empty;
                    objUserRoleAccessDetails.UserDesignation = string.Empty;
                }

            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }
        #endregion
    }
}