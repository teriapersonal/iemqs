﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol041Controller : clsBase
    {
        // GET: PROTOCOL/Protocol041


        string ControllerURL = "/PROTOCOL/Protocol041/";
        string Title = "SET-UP INSPECTION REPORT FOR ELLIPSODIAL/TORI SPHERICAL FORMED D'END";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO200 objPRO200 = new PRO200();
            if (!id.HasValue)
            {
                try
                {
                    objPRO200.ProtocolNo = string.Empty;
                    objPRO200.CreatedBy = objClsLoginInfo.UserName;
                    objPRO200.CreatedOn = DateTime.Now;

                    #region DIMENSION
                    objPRO200.OverCrowingRemarks = "TOL:1/1/4(1.25)% OF NOMINAL I/S DIA";
                    objPRO200.UnderCrowingRemarks = "TOL:5/8(0.625)% OF NOMINAL I/S DIA";
                    #endregion

                    #region OVALITY
                    objPRO200.PRO201.Add(new PRO201
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO200.PRO201.Add(new PRO201
                    {
                        Orientation = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO200.PRO201.Add(new PRO201
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO200.PRO201.Add(new PRO201
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO200.PRO201.Add(new PRO201
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO200.PRO201.Add(new PRO201
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO200.PRO201.Add(new PRO201
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO200.PRO201.Add(new PRO201
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region  THICKNESS
                    objPRO200.PRO202.Add(new PRO202
                    {
                        ThiknessColumn1 = "PETAL 1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO200.PRO202.Add(new PRO202
                    {
                        ThiknessColumn1 = "PETAL 2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO200.PRO202.Add(new PRO202
                    {
                        ThiknessColumn1 = "PETAL 3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO200.PRO202.Add(new PRO202
                    {
                        ThiknessColumn1 = "PETAL 4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region DETAIL THICKNESS REPORT
                    objPRO200.PRO204_5.Add(new PRO204_5
                    {
                        Description = "PETAL 1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO200.PRO204_5.Add(new PRO204_5
                    {
                        Description = "PETAL 2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO200.PRO204_5.Add(new PRO204_5
                    {
                        Description = "PETAL 3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO200.PRO204_5.Add(new PRO204_5
                    {
                        Description = "PETAL 4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion



                    db.PRO200.Add(objPRO200);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO200.HeaderId;
            }
            else
            {
                objPRO200 = db.PRO200.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };


            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            ViewBag.OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO201> lstPRO201 = db.PRO201.Where(x => x.HeaderId == objPRO200.HeaderId).ToList();
            List<PRO202> lstPRO202 = db.PRO202.Where(x => x.HeaderId == objPRO200.HeaderId).ToList();
            List<PRO203> lstPRO203 = db.PRO203.Where(x => x.HeaderId == objPRO200.HeaderId).ToList();
            List<PRO204> lstPRO204 = db.PRO204.Where(x => x.HeaderId == objPRO200.HeaderId).ToList();
            List<PRO204_2> lstPRO204_2 = db.PRO204_2.Where(x => x.HeaderId == objPRO200.HeaderId).ToList();
            List<PRO204_3> lstPRO204_3 = db.PRO204_3.Where(x => x.HeaderId == objPRO200.HeaderId).ToList();
            List<PRO204_4> lstPRO204_4 = db.PRO204_4.Where(x => x.HeaderId == objPRO200.HeaderId).ToList();
            List<PRO204_5> lstPRO204_5 = db.PRO204_5.Where(x => x.HeaderId == objPRO200.HeaderId).ToList();
            List<PRO204_6> lstPRO204_6 = db.PRO204_6.Where(x => x.HeaderId == objPRO200.HeaderId).ToList();
            List<PRO204_9> lstPRO204_9 = db.PRO204_9.Where(x => x.HeaderId == objPRO200.HeaderId).ToList();


            List<SP_IPI_PROTOCOL041_GET_LINES10_DATA_Result> lstPRO204_7 = db.SP_IPI_PROTOCOL041_GET_LINES10_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO200.HeaderId).ToList();
            List<SP_IPI_PROTOCOL041_GET_LINES11_DATA_Result> lstPRO204_8 = db.SP_IPI_PROTOCOL041_GET_LINES11_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO200.HeaderId).ToList();


            ViewBag.lstPRO201 = lstPRO201;
            ViewBag.lstPRO202 = lstPRO202;
            ViewBag.lstPRO203 = lstPRO203;
            ViewBag.lstPRO204 = lstPRO204;
            ViewBag.lstPRO204_2 = lstPRO204_2;
            ViewBag.lstPRO204_3 = lstPRO204_3;
            ViewBag.lstPRO204_4 = lstPRO204_4;
            ViewBag.lstPRO204_5 = lstPRO204_5;
            ViewBag.lstPRO204_6 = lstPRO204_6;
            ViewBag.lstPRO204_7 = lstPRO204_7;
            ViewBag.lstPRO204_8 = lstPRO204_8;
            ViewBag.lstPRO204_9 = lstPRO204_9;

            #endregion

            return View(objPRO200);
        }
        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO200 PRO200)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO200.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO200 objPRO200 = db.PRO200.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO200.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO200.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO200.ProtocolNo = PRO200.ProtocolNo;
                        objPRO200.InsideCircumferenceRequired = PRO200.InsideCircumferenceRequired;
                        objPRO200.InsideCircumferenceActual = PRO200.InsideCircumferenceActual;
                        objPRO200.InsideCircumferenceRemarks = PRO200.InsideCircumferenceRemarks;
                        objPRO200.TotalHeightRequired = PRO200.TotalHeightRequired;
                        objPRO200.TotalHeightActual = PRO200.TotalHeightActual;
                        objPRO200.TotalHeightRemarks = PRO200.TotalHeightRemarks;
                        objPRO200.StraightFaceRequired = PRO200.StraightFaceRequired;
                        objPRO200.StraightFaceActual = PRO200.StraightFaceActual;
                        objPRO200.StraightFaceRemarks = PRO200.StraightFaceRemarks;
                        objPRO200.CrownRadiusRequired = PRO200.CrownRadiusRequired;
                        objPRO200.CrownRadiusActual = PRO200.CrownRadiusActual;
                        objPRO200.CrownRadiusRemarks = PRO200.CrownRadiusRemarks;
                        objPRO200.KnuckleRadiusRequired = PRO200.KnuckleRadiusRequired;
                        objPRO200.KnuckleRadiusActual = PRO200.KnuckleRadiusActual;
                        objPRO200.KnuckleRadiusRemarks = PRO200.KnuckleRadiusRemarks;
                        objPRO200.GapAtKnuckleAreaRequired = PRO200.GapAtKnuckleAreaRequired;
                        objPRO200.GapAtKnuckleAreaActualMin = PRO200.GapAtKnuckleAreaActualMin;
                        objPRO200.GapAtKnuckleAreaActualMax = PRO200.GapAtKnuckleAreaActualMax;
                        objPRO200.GapAtKnuckleAreaRemarks1 = PRO200.GapAtKnuckleAreaRemarks1;
                        objPRO200.GapAtKnuckleAreaRemarks2 = PRO200.GapAtKnuckleAreaRemarks2;
                        objPRO200.OverCrowingRequired = PRO200.OverCrowingRequired;
                        objPRO200.OverCrowingActual = PRO200.OverCrowingActual;
                        objPRO200.OverCrowingRemarks = PRO200.OverCrowingRemarks;
                        objPRO200.UnderCrowingRequired = PRO200.UnderCrowingRequired;
                        objPRO200.UnderCrowingActual = PRO200.UnderCrowingActual;
                        objPRO200.UnderCrowingRemarks = PRO200.UnderCrowingRemarks;
                        objPRO200.ConcentricityRequired = PRO200.ConcentricityRequired;
                        objPRO200.ConcentricityActual = PRO200.ConcentricityActual;
                        objPRO200.ConcentricityRemarks = PRO200.ConcentricityRemarks;
                        objPRO200.TypeOfFormingRequired = PRO200.TypeOfFormingRequired;
                        objPRO200.TypeOfFormingActual = PRO200.TypeOfFormingActual;
                        objPRO200.TypeOfFormingRemarks = PRO200.TypeOfFormingRemarks;
                        objPRO200.DimensionsOfCrown = PRO200.DimensionsOfCrown;
                        objPRO200.DiameterOfCrownRequired = PRO200.DiameterOfCrownRequired;
                        objPRO200.DiameterOfCrownActualMin = PRO200.DiameterOfCrownActualMin;
                        objPRO200.DiameterOfCrownActualMax = PRO200.DiameterOfCrownActualMax;
                        objPRO200.DiameterOfCrownRemarks = PRO200.DiameterOfCrownRemarks;
                        objPRO200.ThicknessOfCrownRequired = PRO200.ThicknessOfCrownRequired;
                        objPRO200.ThicknessOfCrownActual = PRO200.ThicknessOfCrownActual;
                        objPRO200.Ovality = PRO200.Ovality;
                        objPRO200.OutBy = PRO200.OutBy;
                        objPRO200.ThiknessReqMin = PRO200.ThiknessReqMin;
                        objPRO200.ThiknessReqNom = PRO200.ThiknessReqNom;
                        objPRO200.EvalueTemplate = PRO200.EvalueTemplate;
                        objPRO200.ChordLengthReq = PRO200.ChordLengthReq;
                        objPRO200.ChordLengthAct = PRO200.ChordLengthAct;
                        objPRO200.RadiusReq = PRO200.RadiusReq;
                        objPRO200.RadiusAct = PRO200.RadiusAct;
                        objPRO200.GapAllowable = PRO200.GapAllowable;
                        objPRO200.GapAct = PRO200.GapAct;
                        objPRO200.OrientationOfCrownLongSeams = PRO200.OrientationOfCrownLongSeams;
                        objPRO200.PTWepDimensionsForCrown = PRO200.PTWepDimensionsForCrown;
                        objPRO200.PTDeatilsRootGapAllowed = PRO200.PTDeatilsRootGapAllowed;
                        objPRO200.PTDeatilsRootGapActualMin = PRO200.PTDeatilsRootGapActualMin;
                        objPRO200.PTDeatilsRootGapActualMax = PRO200.PTDeatilsRootGapActualMax;
                        objPRO200.PTDetailsOffsetAllowed = PRO200.PTDetailsOffsetAllowed;
                        objPRO200.PTDetailsOffsetActualMin = PRO200.PTDetailsOffsetActualMin;
                        objPRO200.PTDetailsOffsetActualMax = PRO200.PTDetailsOffsetActualMax;
                        objPRO200.PTDetailsPickInOutAllowed = PRO200.PTDetailsPickInOutAllowed;
                        objPRO200.PTDetailsPickInOutActualMin = PRO200.PTDetailsPickInOutActualMin;
                        objPRO200.PTDetailsPickInOutActualMax = PRO200.PTDetailsPickInOutActualMax;
                        objPRO200.PTDetailsISWepAngleReq = PRO200.PTDetailsISWepAngleReq;
                        objPRO200.PTDetailsISWepAngleActualMin = PRO200.PTDetailsISWepAngleActualMin;
                        objPRO200.PTDetailsISWepAngleActualMax = PRO200.PTDetailsISWepAngleActualMax;
                        objPRO200.PTDetailsOSWepAngleReq = PRO200.PTDetailsOSWepAngleReq;
                        objPRO200.PTDetailsOSWepAngleActualMin = PRO200.PTDetailsOSWepAngleActualMin;
                        objPRO200.PTDetailsOSWepAngleActualMax = PRO200.PTDetailsOSWepAngleActualMax;
                        objPRO200.PTDetailsRootFaceReq = PRO200.PTDetailsRootFaceReq;
                        objPRO200.PTDetailsRootFaceActualMin = PRO200.PTDetailsRootFaceActualMin;
                        objPRO200.PTDetailsRootFaceActualMax = PRO200.PTDetailsRootFaceActualMax;
                        objPRO200.TTWepDimensionsForCircmferantian = PRO200.TTWepDimensionsForCircmferantian;

                        //objPRO200.TTDetailsSeamNo = PRO200.TTDetailsSeamNo;
                        //objPRO200.TTDeatilsRootGapAllowed = PRO200.TTDeatilsRootGapAllowed;
                        //objPRO200.TTDeatilsRootGapActualMin = PRO200.TTDeatilsRootGapActualMin;
                        //objPRO200.TTDeatilsRootGapActualMax = PRO200.TTDeatilsRootGapActualMax;
                        //objPRO200.TTDetailsOffsetAllowed = PRO200.TTDetailsOffsetAllowed;
                        //objPRO200.TTDetailsOffsetActualMin = PRO200.TTDetailsOffsetActualMin;
                        //objPRO200.TTDetailsOffsetActualMax = PRO200.TTDetailsOffsetActualMax;
                        //objPRO200.TTDetailsPickInOutAllowed = PRO200.RootGapReq;
                        //objPRO200.TTDetailsPickInOutActualMin = PRO200.TTDetailsPickInOutActualMin;
                        //objPRO200.TTDetailsPickInOutActualMax = PRO200.TTDetailsPickInOutActualMax;
                        //objPRO200.TTDetailsISWepAngleReq = PRO200.TTDetailsISWepAngleReq;
                        //objPRO200.TTDetailsISWepAngleActualMin = PRO200.TTDetailsISWepAngleActualMin;
                        //objPRO200.TTDetailsISWepAngleActualMax = PRO200.TTDetailsISWepAngleActualMax;
                        //objPRO200.TTDetailsOSWepAngleReq = PRO200.TTDetailsOSWepAngleReq;
                        //objPRO200.TTDetailsOSWepAngleActualMin = PRO200.TTDetailsOSWepAngleActualMin;
                        //objPRO200.TTDetailsOSWepAngleActualMax = PRO200.TTDetailsOSWepAngleActualMax;
                        //objPRO200.TTDetailsRootFaceReq = PRO200.TTDetailsRootFaceReq;
                        //objPRO200.TTDetailsRootFaceActualMin = PRO200.TTDetailsRootFaceActualMin;
                        //objPRO200.TTDetailsRootFaceActualMax = PRO200.TTDetailsRootFaceActualMax;

                        objPRO200.RootGapReq = PRO200.RootGapReq;
                        objPRO200.RootFaceReq = PRO200.RootFaceReq;
                        objPRO200.PickInOut = PRO200.PickInOut;
                        objPRO200.OffSet = PRO200.OffSet;
                        objPRO200.Inside_Wep_Angle = PRO200.Inside_Wep_Angle;
                        objPRO200.Outside_Wep_Angle = PRO200.Outside_Wep_Angle;
                        objPRO200.HeightOfTier = PRO200.HeightOfTier;
                        objPRO200.HeightOfTierNo1 = PRO200.HeightOfTierNo1;
                        objPRO200.HeightOfTierReq1 = PRO200.HeightOfTierReq1;
                        objPRO200.HeightOfTierActual1 = PRO200.HeightOfTierActual1;
                        objPRO200.HeightOfTierNo2 = PRO200.HeightOfTierNo2;
                        objPRO200.HeightOfTierReq2 = PRO200.HeightOfTierReq2;
                        objPRO200.HeightOfTierActual2 = PRO200.HeightOfTierActual2;
                        objPRO200.HeightOfTierNo3 = PRO200.HeightOfTierNo3;
                        objPRO200.HeightOfTierReq3 = PRO200.HeightOfTierReq3;
                        objPRO200.HeightOfTierActual3 = PRO200.HeightOfTierActual3;
                        objPRO200.Detail_Thikness_Report = PRO200.Detail_Thikness_Report;
                        objPRO200.CrownThicknessReadings = PRO200.CrownThicknessReadings;
                        objPRO200.CheckPoint1 = PRO200.CheckPoint1;
                        objPRO200.CheckPoint2 = PRO200.CheckPoint2;
                        objPRO200.CheckPoint3 = PRO200.CheckPoint3;
                        objPRO200.CheckPoint4 = PRO200.CheckPoint4;
                        objPRO200.CheckPoint5 = PRO200.CheckPoint5;
                        objPRO200.CheckPoint6 = PRO200.CheckPoint6;
                        objPRO200.CheckPoint6_2 = PRO200.CheckPoint6_2;
                        objPRO200.CheckPoint6_3 = PRO200.CheckPoint6_3;
                        objPRO200.CheckPoint7 = PRO200.CheckPoint7;
                        objPRO200.CheckPoint7_2 = PRO200.CheckPoint7_2;
                        objPRO200.CheckPoint7_3 = PRO200.CheckPoint7_3;
                        objPRO200.CheckPoint7_4 = PRO200.CheckPoint7_4;
                        objPRO200.CheckPoint9 = PRO200.CheckPoint9;
                        objPRO200.CheckPoint9_2 = PRO200.CheckPoint9_2;
                        objPRO200.CheckPoint10 = PRO200.CheckPoint10;
                        objPRO200.QCRemarks = PRO200.QCRemarks;
                        objPRO200.Result = PRO200.Result;
                        objPRO200.ReqCladStripingWidth = PRO200.ReqCladStripingWidth;
                        objPRO200.ReqCladStripingDepth = PRO200.ReqCladStripingDepth;
                        objPRO200.ReqCladStripingOffset = PRO200.ReqCladStripingOffset;
                        objPRO200.Note1 = PRO200.Note1;
                        objPRO200.Note2 = PRO200.Note2;
                        objPRO200.Note3 = PRO200.Note3;
                        objPRO200.IdentificationOfCleats = PRO200.IdentificationOfCleats;

                        objPRO200.EditedBy = objClsLoginInfo.UserName;
                        objPRO200.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO200.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO200 objPRO200 = db.PRO200.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO200 != null && string.IsNullOrWhiteSpace(objPRO200.ProtocolNo))
                    {
                        db.PRO200.Remove(objPRO200);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO200.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO201> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO201> lstAddPRO201 = new List<PRO201>();
                List<PRO201> lstDeletePRO201 = new List<PRO201>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO201 obj = db.PRO201.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO201 obj = new PRO201();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO201.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO201.Count > 0)
                    {
                        db.PRO201.AddRange(lstAddPRO201);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO201 = db.PRO201.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO201.Count > 0)
                    {
                        db.PRO201.RemoveRange(lstDeletePRO201);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO201 = db.PRO201.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO201.Count > 0)
                    {
                        db.PRO201.RemoveRange(lstDeletePRO201);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO202> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO202> lstAddPRO202 = new List<PRO202>();
                List<PRO202> lstDeletePRO202 = new List<PRO202>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO202 obj = db.PRO202.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO202();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.ThiknessColumn1 = item.ThiknessColumn1;
                        obj.ThiknessColumn2 = item.ThiknessColumn2;
                        if (isAdded)
                        {
                            lstAddPRO202.Add(obj);
                        }
                    }
                    if (lstAddPRO202.Count > 0)
                    {
                        db.PRO202.AddRange(lstAddPRO202);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO202 = db.PRO202.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO202.Count > 0)
                    {
                        db.PRO202.RemoveRange(lstDeletePRO202);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO202 = db.PRO202.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO202.Count > 0)
                    {
                        db.PRO202.RemoveRange(lstDeletePRO202);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO203> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO203> lstAddPRO203 = new List<PRO203>();
                List<PRO203> lstDeletePRO203 = new List<PRO203>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO203 obj = db.PRO203.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRO203();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActualArcLength = item.ActualArcLength;


                        if (isAdded)
                        {
                            lstAddPRO203.Add(obj);
                        }
                    }
                    if (lstAddPRO203.Count > 0)
                    {
                        db.PRO203.AddRange(lstAddPRO203);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO203 = db.PRO203.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO203.Count > 0)
                    {
                        db.PRO203.RemoveRange(lstDeletePRO203);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO203 = db.PRO203.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO203.Count > 0)
                    {
                        db.PRO203.RemoveRange(lstDeletePRO203);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO204> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO204> lstAddPRO204 = new List<PRO204>();
                List<PRO204> lstDeletePRO204 = new List<PRO204>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO204 obj = db.PRO204.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO204();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.OrientationReq = item.OrientationReq;
                        obj.OrientationAct = item.OrientationAct;
                        obj.ArcLengthAtReq = item.ArcLengthAtReq;
                        obj.ArcLengthAtAct = item.ArcLengthAtAct;
                        obj.OffsetDistReq = item.OffsetDistReq;
                        obj.OffsetDistAct = item.OffsetDistAct;

                        if (isAdded)
                        {
                            lstAddPRO204.Add(obj);
                        }
                    }
                    if (lstAddPRO204.Count > 0)
                    {
                        db.PRO204.AddRange(lstAddPRO204);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO204 = db.PRO204.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO204.Count > 0)
                    {
                        db.PRO204.RemoveRange(lstDeletePRO204);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO204 = db.PRO204.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO204.Count > 0)
                    {
                        db.PRO204.RemoveRange(lstDeletePRO204);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO204_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO204_2> lstAddPRO204_2 = new List<PRO204_2>();
                List<PRO204_2> lstDeletePRO204_2 = new List<PRO204_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO204_2 obj = db.PRO204_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO204_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.RootGapMin = item.RootGapMin;
                        obj.RootGapMax = item.RootGapMax;
                        obj.RootFaceMin = item.RootFaceMin;
                        obj.RootFaceMax = item.RootFaceMax;
                        obj.Peak_in_out_Top = item.Peak_in_out_Top;
                        obj.Peak_in_out_Mid = item.Peak_in_out_Mid;
                        obj.Peak_in_out_Bottom = item.Peak_in_out_Bottom;
                        obj.OffSetMin = item.OffSetMin;
                        obj.OffSetMax = item.OffSetMax;
                        obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
                        obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
                        obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
                        obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;
                        if (isAdded)
                        {
                            lstAddPRO204_2.Add(obj);
                        }
                    }
                    if (lstAddPRO204_2.Count > 0)
                    {
                        db.PRO204_2.AddRange(lstAddPRO204_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO204_2 = db.PRO204_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO204_2.Count > 0)
                    {
                        db.PRO204_2.RemoveRange(lstDeletePRO204_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO204_2 = db.PRO204_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO204_2.Count > 0)
                    {
                        db.PRO204_2.RemoveRange(lstDeletePRO204_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 details
        //[HttpPost]
        //public JsonResult SaveProtocolLine6(List<PRO204_3> lst, int HeaderId)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        List<PRO204_3> lstAddPRO204_3 = new List<PRO204_3>();
        //        List<PRO204_3> lstDeletePRO204_3 = new List<PRO204_3>();
        //        if (lst != null)
        //        {
        //            foreach (var item in lst)
        //            {
        //                bool isAdded = false;
        //                PRO204_3 obj = db.PRO204_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
        //                if (obj == null)
        //                {
        //                    obj = new PRO204_3();
        //                    obj.HeaderId = item.HeaderId;
        //                    obj.CreatedBy = objClsLoginInfo.UserName;
        //                    obj.CreatedOn = DateTime.Now;
        //                    isAdded = true;
        //                }
        //                else
        //                {
        //                    obj.EditedBy = objClsLoginInfo.UserName;
        //                    obj.EditedOn = DateTime.Now;
        //                }
        //                obj.SeamNo = item.SeamNo;
        //                obj.OffSetMin = item.OffSetMin;
        //                obj.OffSetMax = item.OffSetMax;
        //                obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
        //                obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
        //                obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
        //                obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;
        //                if (isAdded)
        //                {
        //                    lstAddPRO204_3.Add(obj);
        //                }
        //            }
        //            if (lstAddPRO204_3.Count > 0)
        //            {
        //                db.PRO204_3.AddRange(lstAddPRO204_3);
        //            }

        //            var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

        //            lstDeletePRO204_3 = db.PRO204_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
        //            if (lstDeletePRO204_3.Count > 0)
        //            {
        //                db.PRO204_3.RemoveRange(lstDeletePRO204_3);
        //            }
        //            db.SaveChanges();
        //        }
        //        else
        //        {
        //            lstDeletePRO204_3 = db.PRO204_3.Where(x => x.HeaderId == HeaderId).ToList();
        //            if (lstDeletePRO204_3.Count > 0)
        //            {
        //                db.PRO204_3.RemoveRange(lstDeletePRO204_3);
        //            }
        //            db.SaveChanges();
        //        }

        //        objResponseMsg.Key = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7(List<PRO204_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO204_4> lstAddPRO204_4 = new List<PRO204_4>();
                List<PRO204_4> lstDeletePRO204_4 = new List<PRO204_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO204_4 obj = db.PRO204_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO204_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.MaterialIdentificationReport = item.MaterialIdentificationReport;

                        if (isAdded)
                        {
                            lstAddPRO204_4.Add(obj);
                        }
                    }
                    if (lstAddPRO204_4.Count > 0)
                    {
                        db.PRO204_4.AddRange(lstAddPRO204_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO204_4 = db.PRO204_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO204_4.Count > 0)
                    {
                        db.PRO204_4.RemoveRange(lstDeletePRO204_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO204_4 = db.PRO204_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO204_4.Count > 0)
                    {
                        db.PRO204_4.RemoveRange(lstDeletePRO204_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line8 details
        [HttpPost]
        public JsonResult SaveProtocolLine8(List<PRO204_5> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO204_5> lstAddPRO204_5 = new List<PRO204_5>();
                List<PRO204_5> lstDeletePRO204_5 = new List<PRO204_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO204_5 obj = db.PRO204_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO204_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Description = item.Description;
                        obj.Petal_1 = item.Petal_1;
                        obj.Petal_2 = item.Petal_2;
                        obj.Petal_3 = item.Petal_3;
                        obj.Petal_4 = item.Petal_4;
                        obj.Petal_5 = item.Petal_5;
                        obj.Petal_6 = item.Petal_6;
                        obj.Petal_7 = item.Petal_7;
                        obj.Petal_8 = item.Petal_8;
                        obj.Petal_9 = item.Petal_9;
                        obj.Petal_10 = item.Petal_10;

                        if (isAdded)
                        {
                            lstAddPRO204_5.Add(obj);
                        }
                    }
                    if (lstAddPRO204_5.Count > 0)
                    {
                        db.PRO204_5.AddRange(lstAddPRO204_5);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO204_5 = db.PRO204_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO204_5.Count > 0)
                    {
                        db.PRO204_5.RemoveRange(lstDeletePRO204_5);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO204_5 = db.PRO204_5.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO204_5.Count > 0)
                    {
                        db.PRO204_5.RemoveRange(lstDeletePRO204_5);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 details
        [HttpPost]
        public JsonResult SaveProtocolLine9(List<PRO204_6> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO204_6> lstAddPRO204_6 = new List<PRO204_6>();
                List<PRO204_6> lstDeletePRO204_6 = new List<PRO204_6>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO204_6 obj = db.PRO204_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO204_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.SrNo = item.SrNo;
                        obj.Crown1 = item.Crown1;
                        obj.Crown2 = item.Crown2;
                        obj.Crown3 = item.Crown3;
                        obj.Crown4 = item.Crown4;
                        obj.Crown5 = item.Crown5;
                        obj.Crown6 = item.Crown6;
                        obj.Crown7 = item.Crown7;
                        obj.Crown8 = item.Crown8;


                        if (isAdded)
                        {
                            lstAddPRO204_6.Add(obj);
                        }
                    }
                    if (lstAddPRO204_6.Count > 0)
                    {
                        db.PRO204_6.AddRange(lstAddPRO204_6);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO204_6 = db.PRO204_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO204_6.Count > 0)
                    {
                        db.PRO204_6.RemoveRange(lstDeletePRO204_6);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO204_6 = db.PRO204_6.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO204_6.Count > 0)
                    {
                        db.PRO204_6.RemoveRange(lstDeletePRO204_6);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line10 details

        [HttpPost]
        public JsonResult SaveProtocolLine10(List<PRO204_7> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO204_7> lstAddPRO204_7 = new List<PRO204_7>();
                List<PRO204_7> lstDeletePRO204_7 = new List<PRO204_7>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO204_7 obj = db.PRO204_7.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO204_7 objFecthSeam = new PRO204_7();
                        if (obj == null)
                        {
                            obj = new PRO204_7();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO204_7.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRO204_7.Add(obj);
                        }
                    }
                }
                if (lstAddPRO204_7.Count > 0)
                {
                    db.PRO204_7.AddRange(lstAddPRO204_7);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO204_7 = db.PRO204_7.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRO204_7 = db.PRO204_7.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRO204_7.Count > 0)
                {
                    db.PRO204_7.RemoveRange(lstDeletePRO204_7);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line11 details

        [HttpPost]
        public JsonResult SaveProtocolLine11(List<PRO204_8> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO204_8> lstAddPRO204_8 = new List<PRO204_8>();
                List<PRO204_8> lstDeletePRO204_8 = new List<PRO204_8>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO204_8 obj = db.PRO204_8.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO204_8 objFecthSeam = new PRO204_8();
                        if (obj == null)
                        {
                            obj = new PRO204_8();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO204_8.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }

                        obj.CleatNo = item.CleatNo;
                        obj.SizeWelded = item.SizeWelded;
                        obj.Location = item.Location;
                        obj.OutsideOrInside = item.OutsideOrInside;
                        obj.DistanceFromSeam = item.DistanceFromSeam;
                        obj.ShellEvelution = item.ShellEvelution;
                        if (isAdded)
                        {
                            lstAddPRO204_8.Add(obj);
                        }
                    }
                }
                if (lstAddPRO204_8.Count > 0)
                {
                    db.PRO204_8.AddRange(lstAddPRO204_8);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO204_8 = db.PRO204_8.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else { lstDeletePRO204_8 = db.PRO204_8.Where(x => x.HeaderId == HeaderId).ToList(); }
                if (lstDeletePRO204_8.Count > 0)
                {
                    db.PRO204_8.RemoveRange(lstDeletePRO204_8);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line12 details
        [HttpPost]
        public JsonResult SaveProtocolLine12(List<PRO204_9> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO204_9> lstAddPRO204_9 = new List<PRO204_9>();
                List<PRO204_9> lstDeletePRO204_9 = new List<PRO204_9>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO204_9 obj = db.PRO204_9.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO204_9();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        obj.TTDetailsSeamNo = item.TTDetailsSeamNo;
                        obj.TTDeatilsRootGapAllowed = item.TTDeatilsRootGapAllowed;
                        obj.TTDeatilsRootGapActualMin = item.TTDeatilsRootGapActualMin;
                        obj.TTDeatilsRootGapActualMax = item.TTDeatilsRootGapActualMax;
                        obj.TTDetailsOffsetAllowed = item.TTDetailsOffsetAllowed;
                        obj.TTDetailsOffsetActualMin = item.TTDetailsOffsetActualMin;
                        obj.TTDetailsOffsetActualMax = item.TTDetailsOffsetActualMax;
                        obj.TTDetailsPickInOutAllowed = item.TTDetailsPickInOutAllowed;
                        obj.TTDetailsPickInOutActualMin = item.TTDetailsPickInOutActualMin;
                        obj.TTDetailsPickInOutActualMax = item.TTDetailsPickInOutActualMax;
                        obj.TTDetailsISWepAngleReq = item.TTDetailsISWepAngleReq;
                        obj.TTDetailsISWepAngleActualMin = item.TTDetailsISWepAngleActualMin;
                        obj.TTDetailsISWepAngleActualMax = item.TTDetailsISWepAngleActualMax;
                        obj.TTDetailsOSWepAngleReq = item.TTDetailsOSWepAngleReq;
                        obj.TTDetailsOSWepAngleActualMin = item.TTDetailsOSWepAngleActualMin;
                        obj.TTDetailsOSWepAngleActualMax = item.TTDetailsOSWepAngleActualMax;
                        obj.TTDetailsRootFaceReq = item.TTDetailsRootFaceReq;
                        obj.TTDetailsRootFaceActualMin = item.TTDetailsRootFaceActualMin;
                        obj.TTDetailsRootFaceActualMax = item.TTDetailsRootFaceActualMax;
                        if (isAdded)
                        {
                            lstAddPRO204_9.Add(obj);
                        }
                    }
                    if (lstAddPRO204_9.Count > 0)
                    {
                        db.PRO204_9.AddRange(lstAddPRO204_9);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO204_9 = db.PRO204_9.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO204_9.Count > 0)
                    {
                        db.PRO204_9.RemoveRange(lstDeletePRO204_9);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO204_9 = db.PRO204_9.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO204_9.Count > 0)
                    {
                        db.PRO204_9.RemoveRange(lstDeletePRO204_9);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)

        {
            PRL200 objPRL200 = new PRL200();
            objPRL200 = db.PRL200.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL200 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL200.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };


            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines


            List<PRL201> lstPRL201 = db.PRL201.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL202> lstPRL202 = db.PRL202.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL203> lstPRL203 = db.PRL203.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204> lstPRL204 = db.PRL204.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204_2> lstPRL204_2 = db.PRL204_2.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204_3> lstPRL204_3 = db.PRL204_3.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204_4> lstPRL204_4 = db.PRL204_4.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204_5> lstPRL204_5 = db.PRL204_5.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204_6> lstPRL204_6 = db.PRL204_6.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204_9> lstPRL204_9 = db.PRL204_9.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();

            List<SP_IPI_PROTOCOL041_LINKAGE_GET_LINES10_DATA_Result> lstPRL204_7 = db.SP_IPI_PROTOCOL041_LINKAGE_GET_LINES10_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL200.HeaderId).ToList();
            List<SP_IPI_PROTOCOL041_LINKAGE_GET_LINES11_DATA_Result> lstPRL204_8 = db.SP_IPI_PROTOCOL041_LINKAGE_GET_LINES11_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL200.HeaderId).ToList();


            ViewBag.lstPRL201 = lstPRL201;
            ViewBag.lstPRL202 = lstPRL202;
            ViewBag.lstPRL203 = lstPRL203;
            ViewBag.lstPRL204 = lstPRL204;
            ViewBag.lstPRL204_2 = lstPRL204_2;
            ViewBag.lstPRL204_3 = lstPRL204_3;
            ViewBag.lstPRL204_4 = lstPRL204_4;
            ViewBag.lstPRL204_5 = lstPRL204_5;
            ViewBag.lstPRL204_6 = lstPRL204_6;
            ViewBag.lstPRL204_7 = lstPRL204_7;
            ViewBag.lstPRL204_8 = lstPRL204_8;
            ViewBag.lstPRL204_9 = lstPRL204_9;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL200.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL200.ActFilledBy) && (objPRL200.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL200.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL200);
        }

        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL200 objPRL200 = new PRL200();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL200 = db.PRL200.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL200).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL200 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL200.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };


            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            var OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines


            List<PRL201> lstPRL201 = db.PRL201.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL202> lstPRL202 = db.PRL202.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL203> lstPRL203 = db.PRL203.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204> lstPRL204 = db.PRL204.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204_2> lstPRL204_2 = db.PRL204_2.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204_3> lstPRL204_3 = db.PRL204_3.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204_4> lstPRL204_4 = db.PRL204_4.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204_5> lstPRL204_5 = db.PRL204_5.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204_6> lstPRL204_6 = db.PRL204_6.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();
            List<PRL204_9> lstPRL204_9 = db.PRL204_9.Where(x => x.HeaderId == objPRL200.HeaderId).ToList();

            List<SP_IPI_PROTOCOL041_LINKAGE_GET_LINES10_DATA_Result> lstPRL204_7 = db.SP_IPI_PROTOCOL041_LINKAGE_GET_LINES10_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL200.HeaderId).ToList();
            List<SP_IPI_PROTOCOL041_LINKAGE_GET_LINES11_DATA_Result> lstPRL204_8 = db.SP_IPI_PROTOCOL041_LINKAGE_GET_LINES11_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL200.HeaderId).ToList();



            #endregion

            try
            {
                ViewBag.isEditable = false;
                ViewBag.isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL200.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL200.ActFilledBy) && (objPRL200.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL200.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    OutsideInside = OutsideOrInside,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    TypesOfForming = TypesOfForming,

                    objPRL200 = objPRL200,


                    lstPRL201 = lstPRL201,
                    lstPRL202 = lstPRL202,
                    lstPRL203 = lstPRL203,
                    lstPRL204 = lstPRL204,
                    lstPRL204_2 = lstPRL204_2,
                    lstPRL204_3 = lstPRL204_3,
                    lstPRL204_4 = lstPRL204_4,
                    lstPRL204_5 = lstPRL204_5,
                    lstPRL204_6 = lstPRL204_6,
                    lstPRL204_7 = lstPRL204_7,
                    lstPRL204_8 = lstPRL204_8,
                    lstPRL204_9 = lstPRL204_9

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL200 prl200, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl200.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL200 objPRL200 = db.PRL200.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL200.DrawingRevisionNo = prl200.DrawingRevisionNo;
                    objPRL200.DrawingNo = prl200.DrawingNo;
                    objPRL200.DCRNo = prl200.DCRNo;
                    objPRL200.ProtocolNo = prl200.ProtocolNo;

                    objPRL200.InsideCircumferenceRequired = prl200.InsideCircumferenceRequired;
                    objPRL200.InsideCircumferenceActual = prl200.InsideCircumferenceActual;
                    objPRL200.InsideCircumferenceRemarks = prl200.InsideCircumferenceRemarks;
                    objPRL200.TotalHeightRequired = prl200.TotalHeightRequired;
                    objPRL200.TotalHeightActual = prl200.TotalHeightActual;
                    objPRL200.TotalHeightRemarks = prl200.TotalHeightRemarks;
                    objPRL200.StraightFaceRequired = prl200.StraightFaceRequired;
                    objPRL200.StraightFaceActual = prl200.StraightFaceActual;
                    objPRL200.StraightFaceRemarks = prl200.StraightFaceRemarks;
                    objPRL200.CrownRadiusRequired = prl200.CrownRadiusRequired;
                    objPRL200.CrownRadiusActual = prl200.CrownRadiusActual;
                    objPRL200.CrownRadiusRemarks = prl200.CrownRadiusRemarks;
                    objPRL200.KnuckleRadiusRequired = prl200.KnuckleRadiusRequired;
                    objPRL200.KnuckleRadiusActual = prl200.KnuckleRadiusActual;
                    objPRL200.KnuckleRadiusRemarks = prl200.KnuckleRadiusRemarks;
                    objPRL200.GapAtKnuckleAreaRequired = prl200.GapAtKnuckleAreaRequired;
                    objPRL200.GapAtKnuckleAreaActualMin = prl200.GapAtKnuckleAreaActualMin;
                    objPRL200.GapAtKnuckleAreaActualMax = prl200.GapAtKnuckleAreaActualMax;
                    objPRL200.GapAtKnuckleAreaRemarks1 = prl200.GapAtKnuckleAreaRemarks1;
                    objPRL200.GapAtKnuckleAreaRemarks2 = prl200.GapAtKnuckleAreaRemarks2;
                    objPRL200.OverCrowingRequired = prl200.OverCrowingRequired;
                    objPRL200.OverCrowingActual = prl200.OverCrowingActual;
                    objPRL200.OverCrowingRemarks = prl200.OverCrowingRemarks;
                    objPRL200.UnderCrowingRequired = prl200.UnderCrowingRequired;
                    objPRL200.UnderCrowingActual = prl200.UnderCrowingActual;
                    objPRL200.UnderCrowingRemarks = prl200.UnderCrowingRemarks;
                    objPRL200.ConcentricityRequired = prl200.ConcentricityRequired;
                    objPRL200.ConcentricityActual = prl200.ConcentricityActual;
                    objPRL200.ConcentricityRemarks = prl200.ConcentricityRemarks;
                    objPRL200.TypeOfFormingRequired = prl200.TypeOfFormingRequired;
                    objPRL200.TypeOfFormingActual = prl200.TypeOfFormingActual;
                    objPRL200.TypeOfFormingRemarks = prl200.TypeOfFormingRemarks;
                    objPRL200.DimensionsOfCrown = prl200.DimensionsOfCrown;
                    objPRL200.DiameterOfCrownRequired = prl200.DiameterOfCrownRequired;
                    objPRL200.DiameterOfCrownActualMin = prl200.DiameterOfCrownActualMin;
                    objPRL200.DiameterOfCrownActualMax = prl200.DiameterOfCrownActualMax;
                    objPRL200.DiameterOfCrownRemarks = prl200.DiameterOfCrownRemarks;
                    objPRL200.ThicknessOfCrownRequired = prl200.ThicknessOfCrownRequired;
                    objPRL200.ThicknessOfCrownActual = prl200.ThicknessOfCrownActual;
                    objPRL200.Ovality = prl200.Ovality;
                    objPRL200.OutBy = prl200.OutBy;
                    objPRL200.ThiknessReqMin = prl200.ThiknessReqMin;
                    objPRL200.ThiknessReqNom = prl200.ThiknessReqNom;
                    objPRL200.EvalueTemplate = prl200.EvalueTemplate;
                    objPRL200.ChordLengthReq = prl200.ChordLengthReq;
                    objPRL200.ChordLengthAct = prl200.ChordLengthAct;
                    objPRL200.RadiusReq = prl200.RadiusReq;
                    objPRL200.RadiusAct = prl200.RadiusAct;
                    objPRL200.GapAllowable = prl200.GapAllowable;
                    objPRL200.GapAct = prl200.GapAct;
                    objPRL200.OrientationOfCrownLongSeams = prl200.OrientationOfCrownLongSeams;
                    objPRL200.PTWepDimensionsForCrown = prl200.PTWepDimensionsForCrown;
                    objPRL200.PTDeatilsRootGapAllowed = prl200.PTDeatilsRootGapAllowed;
                    objPRL200.PTDeatilsRootGapActualMin = prl200.PTDeatilsRootGapActualMin;
                    objPRL200.PTDeatilsRootGapActualMax = prl200.PTDeatilsRootGapActualMax;
                    objPRL200.PTDetailsOffsetAllowed = prl200.PTDetailsOffsetAllowed;
                    objPRL200.PTDetailsOffsetActualMin = prl200.PTDetailsOffsetActualMin;
                    objPRL200.PTDetailsOffsetActualMax = prl200.PTDetailsOffsetActualMax;
                    objPRL200.PTDetailsPickInOutAllowed = prl200.PTDetailsPickInOutAllowed;
                    objPRL200.PTDetailsPickInOutActualMin = prl200.PTDetailsPickInOutActualMin;
                    objPRL200.PTDetailsPickInOutActualMax = prl200.PTDetailsPickInOutActualMax;
                    objPRL200.PTDetailsISWepAngleReq = prl200.PTDetailsISWepAngleReq;
                    objPRL200.PTDetailsISWepAngleActualMin = prl200.PTDetailsISWepAngleActualMin;
                    objPRL200.PTDetailsISWepAngleActualMax = prl200.PTDetailsISWepAngleActualMax;
                    objPRL200.PTDetailsOSWepAngleReq = prl200.PTDetailsOSWepAngleReq;
                    objPRL200.PTDetailsOSWepAngleActualMin = prl200.PTDetailsOSWepAngleActualMin;
                    objPRL200.PTDetailsOSWepAngleActualMax = prl200.PTDetailsOSWepAngleActualMax;
                    objPRL200.PTDetailsRootFaceReq = prl200.PTDetailsRootFaceReq;
                    objPRL200.PTDetailsRootFaceActualMin = prl200.PTDetailsRootFaceActualMin;
                    objPRL200.PTDetailsRootFaceActualMax = prl200.PTDetailsRootFaceActualMax;
                    objPRL200.TTWepDimensionsForCircmferantian = prl200.TTWepDimensionsForCircmferantian;

                    //objPRL200.TTDetailsSeamNo = prl200.TTDetailsSeamNo;
                    //objPRL200.TTDeatilsRootGapAllowed = prl200.TTDeatilsRootGapAllowed;
                    //objPRL200.TTDeatilsRootGapActualMin = prl200.TTDeatilsRootGapActualMin;
                    //objPRL200.TTDeatilsRootGapActualMax = prl200.TTDeatilsRootGapActualMax;
                    //objPRL200.TTDetailsOffsetAllowed = prl200.TTDetailsOffsetAllowed;
                    //objPRL200.TTDetailsOffsetActualMin = prl200.TTDetailsOffsetActualMin;
                    //objPRL200.TTDetailsOffsetActualMax = prl200.TTDetailsOffsetActualMax;
                    //objPRL200.TTDetailsPickInOutAllowed = prl200.RootGapReq;
                    //objPRL200.TTDetailsPickInOutActualMin = prl200.TTDetailsPickInOutActualMin;
                    //objPRL200.TTDetailsPickInOutActualMax = prl200.TTDetailsPickInOutActualMax;
                    //objPRL200.TTDetailsISWepAngleReq = prl200.TTDetailsISWepAngleReq;
                    //objPRL200.TTDetailsISWepAngleActualMin = prl200.TTDetailsISWepAngleActualMin;
                    //objPRL200.TTDetailsISWepAngleActualMax = prl200.TTDetailsISWepAngleActualMax;
                    //objPRL200.TTDetailsOSWepAngleReq = prl200.TTDetailsOSWepAngleReq;
                    //objPRL200.TTDetailsOSWepAngleActualMin = prl200.TTDetailsOSWepAngleActualMin;
                    //objPRL200.TTDetailsOSWepAngleActualMax = prl200.TTDetailsOSWepAngleActualMax;
                    //objPRL200.TTDetailsRootFaceReq = prl200.TTDetailsRootFaceReq;
                    //objPRL200.TTDetailsRootFaceActualMin = prl200.TTDetailsRootFaceActualMin;
                    //objPRL200.TTDetailsRootFaceActualMax = prl200.TTDetailsRootFaceActualMax;

                    objPRL200.RootGapReq = prl200.RootGapReq;
                    objPRL200.RootFaceReq = prl200.RootFaceReq;
                    objPRL200.PickInOut = prl200.PickInOut;
                    objPRL200.OffSet = prl200.OffSet;
                    objPRL200.Inside_Wep_Angle = prl200.Inside_Wep_Angle;
                    objPRL200.Outside_Wep_Angle = prl200.Outside_Wep_Angle;
                    objPRL200.HeightOfTier = prl200.HeightOfTier;
                    objPRL200.HeightOfTierNo1 = prl200.HeightOfTierNo1;
                    objPRL200.HeightOfTierReq1 = prl200.HeightOfTierReq1;
                    objPRL200.HeightOfTierActual1 = prl200.HeightOfTierActual1;
                    objPRL200.HeightOfTierNo2 = prl200.HeightOfTierNo2;
                    objPRL200.HeightOfTierReq2 = prl200.HeightOfTierReq2;
                    objPRL200.HeightOfTierActual2 = prl200.HeightOfTierActual2;
                    objPRL200.HeightOfTierNo3 = prl200.HeightOfTierNo3;
                    objPRL200.HeightOfTierReq3 = prl200.HeightOfTierReq3;
                    objPRL200.HeightOfTierActual3 = prl200.HeightOfTierActual3;
                    objPRL200.Detail_Thikness_Report = prl200.Detail_Thikness_Report;
                    objPRL200.CrownThicknessReadings = prl200.CrownThicknessReadings;
                    objPRL200.CheckPoint1 = prl200.CheckPoint1;
                    objPRL200.CheckPoint2 = prl200.CheckPoint2;
                    objPRL200.CheckPoint3 = prl200.CheckPoint3;
                    objPRL200.CheckPoint4 = prl200.CheckPoint4;
                    objPRL200.CheckPoint5 = prl200.CheckPoint5;
                    objPRL200.CheckPoint6 = prl200.CheckPoint6;
                    objPRL200.CheckPoint6_2 = prl200.CheckPoint6_2;
                    objPRL200.CheckPoint6_3 = prl200.CheckPoint6_3;
                    objPRL200.CheckPoint7 = prl200.CheckPoint7;
                    objPRL200.CheckPoint7_2 = prl200.CheckPoint7_2;
                    objPRL200.CheckPoint7_3 = prl200.CheckPoint7_3;
                    objPRL200.CheckPoint7_4 = prl200.CheckPoint7_4;
                    objPRL200.CheckPoint9 = prl200.CheckPoint9;
                    objPRL200.CheckPoint9_2 = prl200.CheckPoint9_2;
                    objPRL200.CheckPoint10 = prl200.CheckPoint10;
                    objPRL200.QCRemarks = prl200.QCRemarks;
                    objPRL200.Result = prl200.Result;
                    objPRL200.ReqCladStripingWidth = prl200.ReqCladStripingWidth;
                    objPRL200.ReqCladStripingDepth = prl200.ReqCladStripingDepth;
                    objPRL200.ReqCladStripingOffset = prl200.ReqCladStripingOffset;
                    objPRL200.Note1 = prl200.Note1;
                    objPRL200.Note2 = prl200.Note2;
                    objPRL200.Note3 = prl200.Note3;


                    objPRL200.IdentificationOfCleats = prl200.IdentificationOfCleats;
                    objPRL200.EditedBy = UserName;
                    objPRL200.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL200.ActFilledBy = UserName;
                            objPRL200.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL200.ReqFilledBy = UserName;
                            objPRL200.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL200.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL200 objPRL200 = db.PRL200.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL200 != null && string.IsNullOrWhiteSpace(objPRL200.ProtocolNo))
                    {
                        db.PRL200.Remove(objPRL200);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL200.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL201> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL201> lstAddPRL201 = new List<PRL201>();
                List<PRL201> lstDeletePRL201 = new List<PRL201>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL201 obj = db.PRL201.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;


                            }
                        }
                        else
                        {
                            PRL201 obj = new PRL201();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL201.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL201.Count > 0)
                {
                    db.PRL201.AddRange(lstAddPRL201);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL201 = db.PRL201.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL201 = db.PRL201.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL201.Count > 0)
                {
                    db.PRL201.RemoveRange(lstDeletePRL201);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL202> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL202> lstAddPRL202 = new List<PRL202>();
                List<PRL202> lstDeletePRL202 = new List<PRL202>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL202 obj = db.PRL202.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL202();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.ThiknessColumn1 = item.ThiknessColumn1;
                        obj.ThiknessColumn2 = item.ThiknessColumn2;

                        if (isAdded)
                        {
                            lstAddPRL202.Add(obj);
                        }
                    }
                }
                if (lstAddPRL202.Count > 0)
                {
                    db.PRL202.AddRange(lstAddPRL202);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL202 = db.PRL202.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL202 = db.PRL202.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL202.Count > 0)
                {
                    db.PRL202.RemoveRange(lstDeletePRL202);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL203> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL203> lstAddPRL203 = new List<PRL203>();
                List<PRL203> lstDeletePRL203 = new List<PRL203>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL203 obj = db.PRL203.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL203();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActualArcLength = item.ActualArcLength;

                        if (isAdded)
                        {
                            lstAddPRL203.Add(obj);
                        }
                    }
                    if (lstAddPRL203.Count > 0)
                    {
                        db.PRL203.AddRange(lstAddPRL203);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL203 = db.PRL203.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL203 = db.PRL203.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL203.Count > 0)
                {
                    db.PRL203.RemoveRange(lstDeletePRL203);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL204> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL204> lstAddPRL204 = new List<PRL204>();
                List<PRL204> lstDeletePRL204 = new List<PRL204>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL204 obj = db.PRL204.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL204();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.OrientationReq = item.OrientationReq;
                        obj.OrientationAct = item.OrientationAct;
                        obj.ArcLengthAtReq = item.ArcLengthAtReq;
                        obj.ArcLengthAtAct = item.ArcLengthAtAct;
                        obj.OffsetDistReq = item.OffsetDistReq;
                        obj.OffsetDistAct = item.OffsetDistAct;

                        if (isAdded)
                        {
                            lstAddPRL204.Add(obj);
                        }
                    }
                    if (lstAddPRL204.Count > 0)
                    {
                        db.PRL204.AddRange(lstAddPRL204);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL204 = db.PRL204.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL204 = db.PRL204.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL204.Count > 0)
                {
                    db.PRL204.RemoveRange(lstDeletePRL204);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL204_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL204_2> lstAddPRL204_2 = new List<PRL204_2>();
                List<PRL204_2> lstDeletePRL204_2 = new List<PRL204_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL204_2 obj = db.PRL204_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL204_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.RootGapMin = item.RootGapMin;
                        obj.RootGapMax = item.RootGapMax;
                        obj.RootFaceMin = item.RootFaceMin;
                        obj.RootFaceMax = item.RootFaceMax;
                        obj.Peak_in_out_Top = item.Peak_in_out_Top;
                        obj.Peak_in_out_Mid = item.Peak_in_out_Mid;
                        obj.Peak_in_out_Bottom = item.Peak_in_out_Bottom;
                        obj.OffSetMin = item.OffSetMin;
                        obj.OffSetMax = item.OffSetMax;
                        obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
                        obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
                        obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
                        obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;

                        if (isAdded)
                        {
                            lstAddPRL204_2.Add(obj);
                        }
                    }

                    if (lstAddPRL204_2.Count > 0)
                    {
                        db.PRL204_2.AddRange(lstAddPRL204_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL204_2 = db.PRL204_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL204_2 = db.PRL204_2.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL204_2.Count > 0)
                {
                    db.PRL204_2.RemoveRange(lstDeletePRL204_2);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line6 details
        //[HttpPost]
        //public JsonResult SaveProtocolLine6Linkage(List<PRL204_3> lst, int HeaderId)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        List<PRL204_3> lstAddPRL204_3 = new List<PRL204_3>();
        //        List<PRL204_3> lstDeletePRL204_3 = new List<PRL204_3>();
        //        if (lst != null)
        //        {
        //            foreach (var item in lst)
        //            {
        //                bool isAdded = false;
        //                PRL204_3 obj = db.PRL204_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
        //                if (obj == null)
        //                {
        //                    obj = new PRL204_3();
        //                    obj.HeaderId = item.HeaderId;
        //                    obj.CreatedBy = objClsLoginInfo.UserName;
        //                    obj.CreatedOn = DateTime.Now;
        //                    isAdded = true;
        //                }
        //                else
        //                {
        //                    obj.EditedBy = objClsLoginInfo.UserName;
        //                    obj.EditedOn = DateTime.Now;
        //                }
        //                obj.SeamNo = item.SeamNo;
        //                obj.OffSetMin = item.OffSetMin;
        //                obj.OffSetMax = item.OffSetMax;
        //                obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
        //                obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
        //                obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
        //                obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;

        //                if (isAdded)
        //                {
        //                    lstAddPRL204_3.Add(obj);
        //                }
        //            }

        //            if (lstAddPRL204_3.Count > 0)
        //            {
        //                db.PRL204_3.AddRange(lstAddPRL204_3);
        //            }

        //            var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
        //            lstDeletePRL204_3 = db.PRL204_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
        //        }
        //        else
        //        {
        //            lstDeletePRL204_3 = db.PRL204_3.Where(x => x.HeaderId == HeaderId).ToList();
        //        }
        //        if (lstDeletePRL204_3.Count > 0)
        //        {
        //            db.PRL204_3.RemoveRange(lstDeletePRL204_3);
        //        }
        //        db.SaveChanges();
        //        objResponseMsg.Key = true;
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}
        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7Linkage(List<PRL204_4> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL204_4> lstAddPRL204_4 = new List<PRL204_4>();
                List<PRL204_4> lstDeletePRL204_4 = new List<PRL204_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL204_4 obj = db.PRL204_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL204_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.MaterialIdentificationReport = item.MaterialIdentificationReport;
                        if (isAdded)
                        {
                            lstAddPRL204_4.Add(obj);
                        }
                    }

                    if (lstAddPRL204_4.Count > 0)
                    {
                        db.PRL204_4.AddRange(lstAddPRL204_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL204_4 = db.PRL204_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL204_4 = db.PRL204_4.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL204_4.Count > 0)
                {
                    db.PRL204_4.RemoveRange(lstDeletePRL204_4);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line8 details
        [HttpPost]
        public JsonResult SaveProtocolLine8Linkage(List<PRL204_5> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL204_5> lstAddPRL204_5 = new List<PRL204_5>();
                List<PRL204_5> lstDeletePRL204_5 = new List<PRL204_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL204_5 obj = db.PRL204_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL204_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Description = item.Description;
                        obj.Petal_1 = item.Petal_1;
                        obj.Petal_2 = item.Petal_2;
                        obj.Petal_3 = item.Petal_3;
                        obj.Petal_4 = item.Petal_4;
                        obj.Petal_5 = item.Petal_5;
                        obj.Petal_6 = item.Petal_6;
                        obj.Petal_7 = item.Petal_7;
                        obj.Petal_8 = item.Petal_8;
                        obj.Petal_9 = item.Petal_9;
                        obj.Petal_10 = item.Petal_10;

                        if (isAdded)
                        {
                            lstAddPRL204_5.Add(obj);
                        }
                    }

                    if (lstAddPRL204_5.Count > 0)
                    {
                        db.PRL204_5.AddRange(lstAddPRL204_5);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL204_5 = db.PRL204_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL204_5 = db.PRL204_5.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL204_5.Count > 0)
                {
                    db.PRL204_5.RemoveRange(lstDeletePRL204_5);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 details
        [HttpPost]
        public JsonResult SaveProtocolLine9Linkage(List<PRL204_6> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL204_6> lstAddPRL204_6 = new List<PRL204_6>();
                List<PRL204_6> lstDeletePRL204_6 = new List<PRL204_6>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL204_6 obj = db.PRL204_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL204_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.SrNo = item.SrNo;
                        obj.Crown1 = item.Crown1;
                        obj.Crown2 = item.Crown2;
                        obj.Crown3 = item.Crown3;
                        obj.Crown4 = item.Crown4;
                        obj.Crown5 = item.Crown5;
                        obj.Crown6 = item.Crown6;
                        obj.Crown7 = item.Crown7;
                        obj.Crown8 = item.Crown8;

                        if (isAdded)
                        {
                            lstAddPRL204_6.Add(obj);
                        }
                    }

                    if (lstAddPRL204_6.Count > 0)
                    {
                        db.PRL204_6.AddRange(lstAddPRL204_6);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL204_6 = db.PRL204_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL204_6 = db.PRL204_6.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL204_6.Count > 0)
                {
                    db.PRL204_6.RemoveRange(lstDeletePRL204_6);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line10 details

        [HttpPost]
        public JsonResult SaveProtocolLine10Linkage(List<PRL204_7> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL204_7> lstAddPRL204_7 = new List<PRL204_7>();
                List<PRL204_7> lstDeletePRL204_7 = new List<PRL204_7>();

                if (lst != null)
                {

                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL204_7 obj = db.PRL204_7.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL204_7 objFecthSeam = new PRL204_7();
                        if (obj == null)
                        {
                            obj = new PRL204_7();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL204_7.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRL204_7.Add(obj);
                        }
                    }
                    if (lstAddPRL204_7.Count > 0)
                    {
                        db.PRL204_7.AddRange(lstAddPRL204_7);
                    }
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL204_7 = db.PRL204_7.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL204_7 = db.PRL204_7.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL204_7.Count > 0)
                {
                    db.PRL204_7.RemoveRange(lstDeletePRL204_7);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line11 details

        [HttpPost]
        public JsonResult SaveProtocolLine11Linkage(List<PRL204_8> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL204_8> lstAddPRL204_8 = new List<PRL204_8>();
                List<PRL204_8> lstDeletePRL204_8 = new List<PRL204_8>();
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL204_8 obj = db.PRL204_8.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL204_8 objFecthSeam = new PRL204_8();
                        if (obj == null)
                        {
                            obj = new PRL204_8();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL204_8.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        obj.CleatNo = item.CleatNo;
                        obj.SizeWelded = item.SizeWelded;
                        obj.Location = item.Location;
                        obj.OutsideOrInside = item.OutsideOrInside;
                        obj.DistanceFromSeam = item.DistanceFromSeam;
                        obj.ShellEvelution = item.ShellEvelution;
                        if (isAdded)
                        {
                            lstAddPRL204_8.Add(obj);
                        }
                    }
                    if (lstAddPRL204_8.Count > 0)
                    {
                        db.PRL204_8.AddRange(lstAddPRL204_8);
                    }
                    lstDeletePRL204_8 = db.PRL204_8.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL204_8 = db.PRL204_8.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL204_8.Count > 0)
                {
                    db.PRL204_8.RemoveRange(lstDeletePRL204_8);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line12 details
        [HttpPost]
        public JsonResult SaveProtocolLine12Linkage(List<PRL204_9> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL204_9> lstAddPRL204_9 = new List<PRL204_9>();
                List<PRL204_9> lstDeletePRL204_9 = new List<PRL204_9>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL204_9 obj = db.PRL204_9.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL204_9();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.TTDetailsSeamNo = item.TTDetailsSeamNo;
                        obj.TTDeatilsRootGapAllowed = item.TTDeatilsRootGapAllowed;
                        obj.TTDeatilsRootGapActualMin = item.TTDeatilsRootGapActualMin;
                        obj.TTDeatilsRootGapActualMax = item.TTDeatilsRootGapActualMax;
                        obj.TTDetailsOffsetAllowed = item.TTDetailsOffsetAllowed;
                        obj.TTDetailsOffsetActualMin = item.TTDetailsOffsetActualMin;
                        obj.TTDetailsOffsetActualMax = item.TTDetailsOffsetActualMax;
                        obj.TTDetailsPickInOutAllowed = item.TTDetailsPickInOutAllowed;
                        obj.TTDetailsPickInOutActualMin = item.TTDetailsPickInOutActualMin;
                        obj.TTDetailsPickInOutActualMax = item.TTDetailsPickInOutActualMax;
                        obj.TTDetailsISWepAngleReq = item.TTDetailsISWepAngleReq;
                        obj.TTDetailsISWepAngleActualMin = item.TTDetailsISWepAngleActualMin;
                        obj.TTDetailsISWepAngleActualMax = item.TTDetailsISWepAngleActualMax;
                        obj.TTDetailsOSWepAngleReq = item.TTDetailsOSWepAngleReq;
                        obj.TTDetailsOSWepAngleActualMin = item.TTDetailsOSWepAngleActualMin;
                        obj.TTDetailsOSWepAngleActualMax = item.TTDetailsOSWepAngleActualMax;
                        obj.TTDetailsRootFaceReq = item.TTDetailsRootFaceReq;
                        obj.TTDetailsRootFaceActualMin = item.TTDetailsRootFaceActualMin;
                        obj.TTDetailsRootFaceActualMax = item.TTDetailsRootFaceActualMax;

                        if (isAdded)
                        {
                            lstAddPRL204_9.Add(obj);
                        }
                    }

                    if (lstAddPRL204_9.Count > 0)
                    {
                        db.PRL204_9.AddRange(lstAddPRL204_9);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL204_9 = db.PRL204_9.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL204_9 = db.PRL204_9.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL204_9.Count > 0)
                {
                    db.PRL204_9.RemoveRange(lstDeletePRL204_9);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

    }
}
