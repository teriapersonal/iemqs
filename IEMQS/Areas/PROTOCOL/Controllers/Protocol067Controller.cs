﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol067Controller : clsBase
    {
        // GET: PROTOCOL/Protocol067
        string ControllerURL = "/PROTOCOL/Protocol067/";
        string Title = "REPORT FOR NOZZLE CUT OUT MARKING ON SHELL/HEAD";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO330 objPRO330 = new PRO330();
            if (!id.HasValue)
            {
                try
                {
                    objPRO330.ProtocolNo = string.Empty;
                    objPRO330.CreatedBy = objClsLoginInfo.UserName;
                    objPRO330.CreatedOn = DateTime.Now;



                    db.PRO330.Add(objPRO330);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO330.HeaderId;
            }
            else
            {
                objPRO330 = db.PRO330.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstelevation = new List<string> { "TAN LINE", "REF. LINE" };
            List<string> lstclockWise = new List<string> { "CLOCKWISE", "ANTICLOCK WISE" };


            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.Elevation = lstelevation.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ClockWise = lstclockWise.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO331> lstPRO331 = db.PRO331.Where(x => x.HeaderId == objPRO330.HeaderId).ToList();

            ViewBag.lstPRO331 = lstPRO331;

            #endregion

            return View(objPRO330);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO330 PRO330)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO330.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO330 objPRO330 = db.PRO330.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO330.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO330.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO330.ProtocolNo = PRO330.ProtocolNo;
                        objPRO330.ActCirAtCutoutLocation = PRO330.ActCirAtCutoutLocation;
                        objPRO330.EleOfRefLineFromBTL = PRO330.EleOfRefLineFromBTL;
                        objPRO330.ElevationFrom = PRO330.ElevationFrom;
                        objPRO330.ReqCirAtTopSide = PRO330.ReqCirAtTopSide;
                        objPRO330.ActCirAtTopSide = PRO330.ActCirAtTopSide;
                        objPRO330.ReqCirAtBottomSide = PRO330.ReqCirAtBottomSide;
                        objPRO330.ActCirAtBottomSide = PRO330.ActCirAtBottomSide;
                        objPRO330.ActQuarterBottom1 = PRO330.ActQuarterBottom1;
                        objPRO330.ActQuarterBottom2 = PRO330.ActQuarterBottom2;
                        objPRO330.ActQuarterBottom3 = PRO330.ActQuarterBottom3;
                        objPRO330.ActQuarterBottom4 = PRO330.ActQuarterBottom4;
                        objPRO330.ActQuarterTop1 = PRO330.ActQuarterTop1;
                        objPRO330.ActQuarterTop2 = PRO330.ActQuarterTop2;
                        objPRO330.ActQuarterTop3 = PRO330.ActQuarterTop3;
                        objPRO330.ActQuarterTop4 = PRO330.ActQuarterTop4;
                        objPRO330.ReqQuarterBottom1 = PRO330.ReqQuarterBottom1;
                        objPRO330.ReqQuarterBottom2 = PRO330.ReqQuarterBottom2;
                        objPRO330.ReqQuarterBottom3 = PRO330.ReqQuarterBottom3;
                        objPRO330.ReqQuarterBottom4 = PRO330.ReqQuarterBottom4;
                        objPRO330.ReqQuarterTop1 = PRO330.ReqQuarterTop1;
                        objPRO330.ReqQuarterTop2 = PRO330.ReqQuarterTop2;
                        objPRO330.ReqQuarterTop3 = PRO330.ReqQuarterTop3;
                        objPRO330.ReqQuarterTop4 = PRO330.ReqQuarterTop4;
                        objPRO330.CheckPoint1 = PRO330.CheckPoint1;
                        objPRO330.CheckPoint2 = PRO330.CheckPoint2;
                        objPRO330.CheckPoint3 = PRO330.CheckPoint3;
                        objPRO330.CheckPoint5 = PRO330.CheckPoint5;
                        objPRO330.CheckPoint5_2 = PRO330.CheckPoint5_2;
                        objPRO330.QCRemarks = PRO330.QCRemarks;
                        objPRO330.Result = PRO330.Result;

                        objPRO330.EditedBy = objClsLoginInfo.UserName;
                        objPRO330.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO330.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO330 objPRO330 = db.PRO330.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO330 != null && string.IsNullOrWhiteSpace(objPRO330.ProtocolNo))
                    {
                        db.PRO330.Remove(objPRO330);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO330.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details

        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO331> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO331> lstAddPRO331 = new List<PRO331>();
                List<PRO331> lstDeletePRO331 = new List<PRO331>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO331 obj = db.PRO331.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO331();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.NozzleNo = item.NozzleNo;
                        obj.ReqElevationFrom = item.ReqElevationFrom;
                        obj.ActElevationFrom = item.ActElevationFrom;
                        obj.ReqDegOrientation = item.ReqDegOrientation;
                        obj.MainOrientation = item.MainOrientation;
                        obj.ClockWiseOrientation = item.ClockWiseOrientation;
                        obj.ReqArcLengthOrientation = item.ReqArcLengthOrientation;
                        obj.ActArcLengthOrientation = item.ActArcLengthOrientation;
                        obj.ReqCutOutDia = item.ReqCutOutDia;
                        obj.ActCutOutDia = item.ActCutOutDia;
                        obj.ReqOffsetDistance = item.ReqOffsetDistance;
                        obj.ActOffsetDistance = item.ActOffsetDistance;

                        if (isAdded)
                        {
                            lstAddPRO331.Add(obj);
                        }
                    }
                    if (lstAddPRO331.Count > 0)
                    {
                        db.PRO331.AddRange(lstAddPRO331);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO331 = db.PRO331.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO331.Count > 0)
                    {
                        db.PRO331.RemoveRange(lstDeletePRO331);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO331 = db.PRO331.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO331.Count > 0)
                    {
                        db.PRO331.RemoveRange(lstDeletePRO331);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region  Linkage View Code 

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL330 objPRL330 = new PRL330();
            objPRL330 = db.PRL330.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL330 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL330.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstelevation = new List<string> { "TAN LINE", "REF. LINE" };
            List<string> lstclockWise = new List<string> { "CLOCKWISE", "ANTICLOCK WISE" };

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.Elevation = lstelevation.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ClockWise = lstclockWise.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL331> lstPRL331 = db.PRL331.Where(x => x.HeaderId == objPRL330.HeaderId).ToList();

            ViewBag.lstPRL331 = lstPRL331;



            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL330.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL330.ActFilledBy) && (objPRL330.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL330.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL330);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL330 objPRL330 = new PRL330();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL330 = db.PRL330.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL330).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL330 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL330.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;


            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstelevation = new List<string> { "TAN LINE", "REF. LINE" };
            List<string> lstclockWise = new List<string> { "CLOCKWISE", "ANTICLOCK WISE" };

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var Elevation = lstelevation.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ClockWise = lstclockWise.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL331> lstPRL331 = db.PRL331.Where(x => x.HeaderId == objPRL330.HeaderId).ToList();


            #endregion
            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL330.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL330.ActFilledBy) && (objPRL330.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL330.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    Elevation = Elevation,
                    ClockWise = ClockWise,
                    YesNAEnum = YesNAEnum,

                    objPRL335 = objPRL330,

                    lstPRL331 = lstPRL331


                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL330 PRL330, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL330.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL330 objPRL330 = db.PRL330.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL330.DrawingRevisionNo = PRL330.DrawingRevisionNo;
                    objPRL330.DrawingNo = PRL330.DrawingNo;
                    objPRL330.DCRNo = PRL330.DCRNo;
                    objPRL330.ProtocolNo = PRL330.ProtocolNo;
                    objPRL330.ActCirAtCutoutLocation = PRL330.ActCirAtCutoutLocation;
                    objPRL330.EleOfRefLineFromBTL = PRL330.EleOfRefLineFromBTL;
                    objPRL330.ElevationFrom = PRL330.ElevationFrom;
                    objPRL330.ReqCirAtTopSide = PRL330.ReqCirAtTopSide;
                    objPRL330.ActCirAtTopSide = PRL330.ActCirAtTopSide;
                    objPRL330.ReqCirAtBottomSide = PRL330.ReqCirAtBottomSide;
                    objPRL330.ActCirAtBottomSide = PRL330.ActCirAtBottomSide;
                    objPRL330.ActQuarterBottom1 = PRL330.ActQuarterBottom1;
                    objPRL330.ActQuarterBottom2 = PRL330.ActQuarterBottom2;
                    objPRL330.ActQuarterBottom3 = PRL330.ActQuarterBottom3;
                    objPRL330.ActQuarterBottom4 = PRL330.ActQuarterBottom4;
                    objPRL330.ActQuarterTop1 = PRL330.ActQuarterTop1;
                    objPRL330.ActQuarterTop2 = PRL330.ActQuarterTop2;
                    objPRL330.ActQuarterTop3 = PRL330.ActQuarterTop3;
                    objPRL330.ActQuarterTop4 = PRL330.ActQuarterTop4;
                    objPRL330.ReqQuarterBottom1 = PRL330.ReqQuarterBottom1;
                    objPRL330.ReqQuarterBottom2 = PRL330.ReqQuarterBottom2;
                    objPRL330.ReqQuarterBottom3 = PRL330.ReqQuarterBottom3;
                    objPRL330.ReqQuarterBottom4 = PRL330.ReqQuarterBottom4;
                    objPRL330.ReqQuarterTop1 = PRL330.ReqQuarterTop1;
                    objPRL330.ReqQuarterTop2 = PRL330.ReqQuarterTop2;
                    objPRL330.ReqQuarterTop3 = PRL330.ReqQuarterTop3;
                    objPRL330.ReqQuarterTop4 = PRL330.ReqQuarterTop4;
                    objPRL330.CheckPoint1 = PRL330.CheckPoint1;
                    objPRL330.CheckPoint2 = PRL330.CheckPoint2;
                    objPRL330.CheckPoint3 = PRL330.CheckPoint3;
                    objPRL330.CheckPoint5 = PRL330.CheckPoint5;
                    objPRL330.CheckPoint5_2 = PRL330.CheckPoint5_2;
                    objPRL330.QCRemarks = PRL330.QCRemarks;
                    objPRL330.Result = PRL330.Result;

                    objPRL330.EditedBy = UserName;
                    objPRL330.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL330.ActFilledBy = UserName;
                            objPRL330.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL330.ReqFilledBy = UserName;
                            objPRL330.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL330.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL330 objPRL330 = db.PRL330.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL330 != null && string.IsNullOrWhiteSpace(objPRL330.ProtocolNo))
                    {
                        db.PRL330.Remove(objPRL330);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL330.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL331> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL331> lstAddPRL331 = new List<PRL331>();
                List<PRL331> lstDeletePRL331 = new List<PRL331>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL331 obj = db.PRL331.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL331();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.NozzleNo = item.NozzleNo;
                        obj.ReqElevationFrom = item.ReqElevationFrom;
                        obj.ActElevationFrom = item.ActElevationFrom;
                        obj.ReqDegOrientation = item.ReqDegOrientation;
                        obj.MainOrientation = item.MainOrientation;
                        obj.ClockWiseOrientation = item.ClockWiseOrientation;
                        obj.ReqArcLengthOrientation = item.ReqArcLengthOrientation;
                        obj.ActArcLengthOrientation = item.ActArcLengthOrientation;
                        obj.ReqCutOutDia = item.ReqCutOutDia;
                        obj.ActCutOutDia = item.ActCutOutDia;
                        obj.ReqOffsetDistance = item.ReqOffsetDistance;
                        obj.ActOffsetDistance = item.ActOffsetDistance;

                        if (isAdded)
                        {
                            lstAddPRL331.Add(obj);
                        }
                    }
                    if (lstAddPRL331.Count > 0)
                    {
                        db.PRL331.AddRange(lstAddPRL331);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL331 = db.PRL331.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL331.Count > 0)
                    {
                        db.PRL331.RemoveRange(lstDeletePRL331);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL331 = db.PRL331.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL331.Count > 0)
                    {
                        db.PRL331.RemoveRange(lstDeletePRL331);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #endregion

    }
}