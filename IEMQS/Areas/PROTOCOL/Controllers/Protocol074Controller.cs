﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol074Controller : clsBase
    {
        // GET: PROTOCOL/Protocol074
        string ControllerURL = "/PROTOCOL/Protocol074/";
        string Title = "REPORT FOR N2 FILLING INSPECTION";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO365 objPRO365 = new PRO365();

            if (!id.HasValue)
            {
                try
                {
                    objPRO365.ProtocolNo = string.Empty;
                    objPRO365.CreatedBy = objClsLoginInfo.UserName;
                    objPRO365.CreatedOn = DateTime.Now;


                    db.PRO365.Add(objPRO365);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO365.HeaderId;
            }
            else
            {
                objPRO365 = db.PRO365.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO366> lstPRO366 = db.PRO366.Where(x => x.HeaderId == objPRO365.HeaderId).ToList();

            ViewBag.lstPRO366 = lstPRO366;

            List<PRO367> lstPRO367 = db.PRO367.Where(x => x.HeaderId == objPRO365.HeaderId).ToList();

            ViewBag.lstPRO367 = lstPRO367;

            #endregion

            return View(objPRO365);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO365 PRO365)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO365.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO365 objPRO365 = db.PRO365.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO365.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO365.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO365.ProtocolNo = PRO365.ProtocolNo;
                        objPRO365.PhotographsOfAct1 = PRO365.PhotographsOfAct1;
                        objPRO365.PhotographsOfAct2 = PRO365.PhotographsOfAct2;
                        objPRO365.PressureGuageDetails = PRO365.PressureGuageDetails;
                        objPRO365.CheckPoint1 = PRO365.CheckPoint1;
                        objPRO365.CheckPoint2 = PRO365.CheckPoint2;
                        objPRO365.CheckPoint3 = PRO365.CheckPoint3;
                        objPRO365.CheckPoint4 = PRO365.CheckPoint4;
                        objPRO365.CheckPoint5 = PRO365.CheckPoint5;
                        objPRO365.CheckPoint6 = PRO365.CheckPoint6;
                        objPRO365.QCRemarks = PRO365.QCRemarks;
                        objPRO365.Result = PRO365.Result;
                        objPRO365.EditedBy = PRO365.EditedBy;
                        objPRO365.EditedOn = PRO365.EditedOn;
                        objPRO365.EditedBy = objClsLoginInfo.UserName;
                        objPRO365.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO365.HeaderId;

                        #endregion

                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO365 objPRO365 = db.PRO365.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO365 != null && string.IsNullOrWhiteSpace(objPRO365.ProtocolNo))
                    {
                        db.PRO365.Remove(objPRO365);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO365.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO366> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO366> lstAddPRO366 = new List<PRO366>();
                List<PRO366> lstDeletePRO366 = new List<PRO366>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO366 obj = db.PRO366.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO366();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.GaugeSerialNumber = item.GaugeSerialNumber;
                        obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;

                        if (isAdded)
                        {
                            lstAddPRO366.Add(obj);
                        }
                    }
                    if (lstAddPRO366.Count > 0)
                    {
                        db.PRO366.AddRange(lstAddPRO366);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO366 = db.PRO366.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO366.Count > 0)
                    {
                        db.PRO366.RemoveRange(lstDeletePRO366);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO366 = db.PRO366.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO366.Count > 0)
                    {
                        db.PRO366.RemoveRange(lstDeletePRO366);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO367> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO367> lstAddPRO367 = new List<PRO367>();
                List<PRO367> lstDeletePRO367 = new List<PRO367>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO367 obj = db.PRO367.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO367();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.ReqPressure = item.ReqPressure;
                        obj.ActPressure = item.ActPressure;
                        obj.ReqOxygenlevel = item.ReqOxygenlevel;
                        obj.ActOxygenlevel = item.ActOxygenlevel;
                        obj.Date = item.Date;

                        if (isAdded)
                        {
                            lstAddPRO367.Add(obj);
                        }
                    }
                    if (lstAddPRO367.Count > 0)
                    {
                        db.PRO367.AddRange(lstAddPRO367);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO367 = db.PRO367.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO367.Count > 0)
                    {
                        db.PRO367.RemoveRange(lstDeletePRO367);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO367 = db.PRO367.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO367.Count > 0)
                    {
                        db.PRO367.RemoveRange(lstDeletePRO367);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion


        #region  Linkage View Code   

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL365 objPRL365 = new PRL365();
            objPRL365 = db.PRL365.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL365 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL365.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();


            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL366> lstPRL366 = db.PRL366.Where(x => x.HeaderId == objPRL365.HeaderId).ToList();
            List<PRL367> lstPRL367 = db.PRL367.Where(x => x.HeaderId == objPRL365.HeaderId).ToList();

            ViewBag.lstPRL366 = lstPRL366;
            ViewBag.lstPRL367 = lstPRL367;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL365.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL365.ActFilledBy) && (objPRL365.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL365.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL365);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL365 objPRL365 = new PRL365();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL365 = db.PRL365.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL365).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL365 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL365.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();


            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL366> lstPRL366 = db.PRL366.Where(x => x.HeaderId == objPRL365.HeaderId).ToList();
            List<PRL367> lstPRL367 = db.PRL367.Where(x => x.HeaderId == objPRL365.HeaderId).ToList();


            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL365.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL365.ActFilledBy) && (objPRL365.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL365.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    OutsideInside = OutsideInside,
                    LeftRight = LeftRight,

                    objPRL365 = objPRL365,

                    lstPRL366 = lstPRL366,
                    lstPRL367 = lstPRL367

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL365 PRL365, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL365.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL365 objPRL365 = db.PRL365.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL365.DrawingRevisionNo = PRL365.DrawingRevisionNo;
                    objPRL365.DrawingNo = PRL365.DrawingNo;
                    objPRL365.DCRNo = PRL365.DCRNo;
                    objPRL365.ProtocolNo = PRL365.ProtocolNo;
                    objPRL365.PhotographsOfAct1 = PRL365.PhotographsOfAct1;
                    objPRL365.PhotographsOfAct2 = PRL365.PhotographsOfAct2;
                    objPRL365.PressureGuageDetails = PRL365.PressureGuageDetails;
                    objPRL365.CheckPoint1 = PRL365.CheckPoint1;
                    objPRL365.CheckPoint2 = PRL365.CheckPoint2;
                    objPRL365.CheckPoint3 = PRL365.CheckPoint3;
                    objPRL365.CheckPoint4 = PRL365.CheckPoint4;
                    objPRL365.CheckPoint5 = PRL365.CheckPoint5;
                    objPRL365.CheckPoint6 = PRL365.CheckPoint6;
                    objPRL365.QCRemarks = PRL365.QCRemarks;
                    objPRL365.Result = PRL365.Result;
                    objPRL365.EditedBy = UserName;
                    objPRL365.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL365.ActFilledBy = UserName;
                            objPRL365.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL365.ReqFilledBy = UserName;
                            objPRL365.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL365.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL365 objPRL365 = db.PRL365.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL365 != null && string.IsNullOrWhiteSpace(objPRL365.ProtocolNo))
                    {
                        db.PRL365.Remove(objPRL365);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL365.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL366> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL366> lstAddPRL366 = new List<PRL366>();
                List<PRL366> lstDeletePRL366 = new List<PRL366>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL366 obj = db.PRL366.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL366();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        obj.GaugeSerialNumber = item.GaugeSerialNumber;
                        obj.GaugeCelebrationDueDate = item.GaugeCelebrationDueDate;

                        if (isAdded)
                        {
                            lstAddPRL366.Add(obj);
                        }
                    }
                }
                if (lstAddPRL366.Count > 0)
                {
                    db.PRL366.AddRange(lstAddPRL366);
                }

                
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL366 = db.PRL366.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL366 = db.PRL366.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL366.Count > 0)
                {
                    db.PRL366.RemoveRange(lstDeletePRL366);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL367> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL367> lstAddPRL367 = new List<PRL367>();
                List<PRL367> lstDeletePRL367 = new List<PRL367>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL367 obj = db.PRL367.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL367();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        obj.SrNo = item.SrNo;
                        obj.ReqPressure = item.ReqPressure;
                        obj.ActPressure = item.ActPressure;
                        obj.ReqOxygenlevel = item.ReqOxygenlevel;
                        obj.ActOxygenlevel = item.ActOxygenlevel;
                        obj.Date = item.Date;
                        if (isAdded)
                        {
                            lstAddPRL367.Add(obj);
                        }
                    }
                }
                if (lstAddPRL367.Count > 0)
                {
                    db.PRL367.AddRange(lstAddPRL367);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL367 = db.PRL367.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL367 = db.PRL367.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL367.Count > 0)
                {
                    db.PRL367.RemoveRange(lstDeletePRL367);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <param name="PhotoNo">1 First Image | 2 Second Image</param>
        /// <param name="Headerid">record Unique Id</param>
        /// <param name="PageType">0 For Details | 1 for Linkage</param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeletePhoto(int PhotoNo,int Headerid,int PageType)
        {
            try
            {
                if (PageType == 0)
                {
                    var Files = (new clsFileUpload()).GetDocuments("PRO365/" + Headerid + "/" + PhotoNo);
                    foreach (var item in Files)
                    {
                        (new clsFileUpload()).DeleteFile("PRO365/" + Headerid + "/" + PhotoNo, item.Name);
                    }
                }
                else
                {
                    var Files = (new clsFileUpload()).GetDocuments("PRL365/" + Headerid + "/" + PhotoNo);
                    foreach (var item in Files)
                    {
                        (new clsFileUpload()).DeleteFile("PRL365/" + Headerid + "/" + PhotoNo, item.Name);
                    }
                }
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }           
        }
    }
}