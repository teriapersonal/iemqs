﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol024Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol024/";
        string Title = "HYDROSTATIC TEST REPORT (PRESSURE TEST)";
        // GET: PROTOCOL/Protocol024
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id,int? m)
        {
            PRO115 objPRO115 = new PRO115();
            if (!id.HasValue)
            {
                objPRO115.ProtocolNo = string.Empty;
                objPRO115.CreatedBy = objClsLoginInfo.UserName;
                objPRO115.CreatedOn = DateTime.Now;
                db.PRO115.Add(objPRO115);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO115.HeaderId;
            }
            else
            {
                objPRO115 = db.PRO115.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRO115);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO115 PRO115)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO115.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO115 objPRO115 = db.PRO115.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO115.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO115.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO115.ProtocolNo = PRO115.ProtocolNo;
                        objPRO115.InspectionCertificateNo = PRO115.InspectionCertificateNo;
                        objPRO115.InspectionAgency = PRO115.InspectionAgency;
                        objPRO115.DrawingNo = PRO115.DrawingNo;
                        objPRO115.ProcedureNo = PRO115.ProcedureNo;
                        objPRO115.TestDecription = PRO115.TestDecription;
                        objPRO115.HydrotestDate = PRO115.HydrotestDate;
                        objPRO115.TestPosition = PRO115.TestPosition;
                        objPRO115.TestPressureRequired = PRO115.TestPressureRequired;
                        objPRO115.TestPressureActual = PRO115.TestPressureActual;
                        objPRO115.HoldingTimeRequired = PRO115.HoldingTimeRequired;
                        objPRO115.HoldingTimeActual = PRO115.HoldingTimeActual;
                        objPRO115.TemperatureRequired = PRO115.TemperatureRequired;
                        objPRO115.TemperatureActual = PRO115.TemperatureActual;
                        objPRO115.TestMedium = PRO115.TestMedium;
                        objPRO115.ChlorideContentOfWater = PRO115.ChlorideContentOfWater;
                        objPRO115.GraphAttached = PRO115.GraphAttached;
                        objPRO115.Remarks = PRO115.Remarks;
                        objPRO115.EditedBy = objClsLoginInfo.UserName;
                        objPRO115.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO115.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId > 0)
                {
                    PRO115 objPRO115 = db.PRO115.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO115 != null && string.IsNullOrWhiteSpace(objPRO115.ProtocolNo))
                    {
                        db.PRO116.RemoveRange(objPRO115.PRO116.ToList());
                        db.PRO115.Remove(objPRO115);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.HYDROSTATIC_TEST_REPORT.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO115.HeaderId;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.GaugeNo like '%" + param.sSearch
                        + "%' or pro.CalibrationUpto like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL024_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                     Helper.GenerateHTMLTextbox(newRecordId, "GaugeNo", "", "", false, "", false, "100","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "CalibrationUpto", "", "", false, "", false, "","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "GaugeNo", Convert.ToString(uc.GaugeNo), "", true, "", false, "100","QC"),
                             Helper.GenerateHTMLTextbox(uc.LineId, "CalibrationUpto", uc.CalibrationUpto==null || uc.CalibrationUpto.Value==DateTime.MinValue ? "": Convert.ToDateTime(uc.CalibrationUpto).ToString("yyyy-MM-dd"), "", true, "", false,"","QC"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO116 objPRO116 = new PRO116();
            //int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strGaugeNo = !string.IsNullOrEmpty(fc["GaugeNo" + refLineId]) ? Convert.ToString(fc["GaugeNo" + refLineId]).Trim() : "";
                    DateTime dateCalibrationUpto = !string.IsNullOrEmpty(fc["CalibrationUpto" + refLineId]) ? Convert.ToDateTime(fc["CalibrationUpto" + refLineId].ToString()).Date : DateTime.MinValue;


                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO116 = db.PRO116.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO116.EditedBy = objClsLoginInfo.UserName;
                        objPRO116.EditedOn = DateTime.Now;
                    }
                    objPRO116.HeaderId = refHeaderId;
                    objPRO116.GaugeNo = strGaugeNo;
                    if (!string.IsNullOrEmpty(fc["CalibrationUpto" + refLineId]) && dateCalibrationUpto != DateTime.MinValue)
                    {
                        objPRO116.CalibrationUpto = dateCalibrationUpto;
                    }
                    else
                    {
                        objPRO116.CalibrationUpto = null;
                    }
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO116.CreatedBy = objClsLoginInfo.UserName;
                        objPRO116.CreatedOn = DateTime.Now;
                        db.PRO116.Add(objPRO116);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO116.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO116 objPRO116 = db.PRO116.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO116 != null)
                {
                    db.PRO116.Remove(objPRO116);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
         #endregion


        #region Linkage View Code
        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL115 objPRL115 = new PRL115();
            objPRL115 = db.PRL115.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (objPRL115 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL115.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL115);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL115 PRL115)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL115.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL115 objPRL115 = db.PRL115.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL115.ProtocolNo = PRL115.ProtocolNo != null ? PRL115.ProtocolNo : "";
                    objPRL115.EditedBy = objClsLoginInfo.UserName;
                    objPRL115.EditedOn = DateTime.Now;
                    objPRL115.InspectionCertificateNo = PRL115.InspectionCertificateNo;
                    objPRL115.InspectionAgency = PRL115.InspectionAgency;
                    objPRL115.DrawingNo = PRL115.DrawingNo;
                    objPRL115.ProcedureNo = PRL115.ProcedureNo;
                    objPRL115.TestDecription = PRL115.TestDecription;
                    objPRL115.HydrotestDate = PRL115.HydrotestDate;
                    objPRL115.TestPosition = PRL115.TestPosition;
                    objPRL115.TestPressureRequired = PRL115.TestPressureRequired;
                    objPRL115.TestPressureActual = PRL115.TestPressureActual;
                    objPRL115.HoldingTimeRequired = PRL115.HoldingTimeRequired;
                    objPRL115.HoldingTimeActual = PRL115.HoldingTimeActual;
                    objPRL115.TemperatureRequired = PRL115.TemperatureRequired;
                    objPRL115.TemperatureActual = PRL115.TemperatureActual;
                    objPRL115.TestMedium = PRL115.TestMedium;
                    objPRL115.ChlorideContentOfWater = PRL115.ChlorideContentOfWater;
                    objPRL115.GraphAttached = PRL115.GraphAttached;
                    objPRL115.Remarks = PRL115.Remarks;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL115.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL115 objPRL115 = db.PRL115.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL115 != null && string.IsNullOrWhiteSpace(objPRL115.ProtocolNo))
                    {
                        db.PRL116.RemoveRange(objPRL115.PRL116.ToList());
                        db.PRL115.Remove(objPRL115);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL115.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 Linkage
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.GaugeNo like '%" + param.sSearch
                        + "%' or pro.CalibrationUpto like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL024_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                    Helper.GenerateHTMLTextbox(newRecordId, "GaugeNo", "", "", false, "", false, "100","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "CalibrationUpto", "", "", false, "", false, "","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                                 Helper.GenerateHTMLTextbox(uc.LineId, "GaugeNo", Convert.ToString(uc.GaugeNo), "", true, "", false, "100","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "CalibrationUpto", uc.CalibrationUpto==null || uc.CalibrationUpto.Value==DateTime.MinValue ? "": Convert.ToDateTime(uc.CalibrationUpto).ToString("yyyy-MM-dd"), "", true, "", false,"","QC"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL116 objPRL116 = new PRL116();
           // int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strGaugeNo = !string.IsNullOrEmpty(fc["GaugeNo" + refLineId]) ? Convert.ToString(fc["GaugeNo" + refLineId]).Trim() : "";
                    DateTime dateCalibrationUpto = !string.IsNullOrEmpty(fc["CalibrationUpto" + refLineId]) ? Convert.ToDateTime(fc["CalibrationUpto" + refLineId].ToString()).Date : DateTime.MinValue;


                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL116 = db.PRL116.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL116.EditedBy = objClsLoginInfo.UserName;
                        objPRL116.EditedOn = DateTime.Now;
                    }
                    objPRL116.HeaderId = refHeaderId;
                    objPRL116.GaugeNo = strGaugeNo;
                    if (!string.IsNullOrEmpty(fc["CalibrationUpto" + refLineId]) && dateCalibrationUpto != DateTime.MinValue)
                    {
                        objPRL116.CalibrationUpto = dateCalibrationUpto;
                    }
                    else
                    {
                        objPRL116.CalibrationUpto = null;
                    }
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL116.CreatedBy = objClsLoginInfo.UserName;
                        objPRL116.CreatedOn = DateTime.Now;
                        db.PRL116.Add(objPRL116);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL116.LineId;

                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL116 objPRL116 = db.PRL116.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL116 != null)
                {
                    db.PRL116.Remove(objPRL116);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                //htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
    }
    
}