﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol063Controller : clsBase
    {
        //GET: PROTOCOL/Protocol063
        string ControllerURL = "/PROTOCOL/Protocol063/";
        string Title = "SET-UP & DIMENSION REPORT FOR MTB BLOCK / PTC LONG SEAM";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO310 objPRO310 = new PRO310();
            if (!id.HasValue)
            {
                try
                {
                    objPRO310.ProtocolNo = string.Empty;
                    objPRO310.CreatedBy = objClsLoginInfo.UserName;
                    objPRO310.CreatedOn = DateTime.Now;



                    db.PRO310.Add(objPRO310);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO310.HeaderId;
            }
            else
            {
                objPRO310 = db.PRO310.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            #region bind lines
            List<PRO311> lstPRO311 = db.PRO311.Where(x => x.HeaderId == objPRO310.HeaderId).ToList();
            List<PRO312> lstPRO312 = db.PRO312.Where(x => x.HeaderId == objPRO310.HeaderId).ToList();

            ViewBag.lstPRO311 = lstPRO311;
            ViewBag.lstPRO312 = lstPRO312;

            #endregion

            return View(objPRO310);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO310 PRO310)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO310.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO310 objPRO310 = db.PRO310.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO310.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO310.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO310.ProtocolNo = PRO310.ProtocolNo;
                        objPRO310.ReqRootGap = PRO310.ReqRootGap;
                        objPRO310.ReqRootFace = PRO310.ReqRootFace;
                        objPRO310.ReqInSideWepAngle = PRO310.ReqInSideWepAngle;
                        objPRO310.ReqOutSideWepAngle = PRO310.ReqOutSideWepAngle;
                        objPRO310.ReqOffset = PRO310.ReqOffset;
                        objPRO310.ReqTotalLength = PRO310.ReqTotalLength;
                        objPRO310.ReqTotalWidth = PRO310.ReqTotalWidth;
                        objPRO310.MinReqTotalLength = PRO310.MinReqTotalLength;
                        objPRO310.MaxReqTotalLength = PRO310.MaxReqTotalLength;
                        objPRO310.MinReqTotalWidth = PRO310.MinReqTotalWidth;
                        objPRO310.MaxReqTotalWidth = PRO310.MaxReqTotalWidth;
                        objPRO310.CheckPoint1 = PRO310.CheckPoint1;
                        objPRO310.CheckPoint2 = PRO310.CheckPoint2;
                        objPRO310.CheckPoint3 = PRO310.CheckPoint3;
                        objPRO310.CheckPoint4 = PRO310.CheckPoint4;
                        objPRO310.CheckPoint5 = PRO310.CheckPoint5;
                        objPRO310.CheckPoint6 = PRO310.CheckPoint6;
                        objPRO310.CheckPoint7 = PRO310.CheckPoint7;
                        objPRO310.CheckPoint9 = PRO310.CheckPoint9;
                        objPRO310.CheckPoint9_2 = PRO310.CheckPoint9_2;
                        objPRO310.QCRemarks = PRO310.QCRemarks;
                        objPRO310.Result = PRO310.Result;

                        objPRO310.EditedBy = objClsLoginInfo.UserName;
                        objPRO310.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO310.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO310 objPRO310 = db.PRO310.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO310 != null && string.IsNullOrWhiteSpace(objPRO310.ProtocolNo))
                    {
                        db.PRO310.Remove(objPRO310);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO310.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO311> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO311> lstAddPRO311 = new List<PRO311>();
                List<PRO311> lstDeletePRO311 = new List<PRO311>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO311 obj = db.PRO311.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.MaterialIdentification = item.MaterialIdentification;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO311 obj = new PRO311();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.MaterialIdentification))
                            {
                                obj.MaterialIdentification = item.MaterialIdentification;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO311.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO311.Count > 0)
                    {
                        db.PRO311.AddRange(lstAddPRO311);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO311 = db.PRO311.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO311.Count > 0)
                    {
                        db.PRO311.RemoveRange(lstDeletePRO311);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO311 = db.PRO311.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO311.Count > 0)
                    {
                        db.PRO311.RemoveRange(lstDeletePRO311);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO312> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO312> lstAddPRO312 = new List<PRO312>();
                List<PRO312> lstDeletePRO312 = new List<PRO312>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO312 obj = db.PRO312.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {

                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxInSideWepAngle = item.ActMaxInSideWepAngle;
                                obj.ActMaxOutSideWepAngle = item.ActMaxOutSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinInSideWepAngle = item.ActMinInSideWepAngle;
                                obj.ActMinOutSideWepAngle = item.ActMinOutSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO312 obj = new PRO312();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxInSideWepAngle = item.ActMaxInSideWepAngle;
                                obj.ActMaxOutSideWepAngle = item.ActMaxOutSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinInSideWepAngle = item.ActMinInSideWepAngle;
                                obj.ActMinOutSideWepAngle = item.ActMinOutSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO312.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO312.Count > 0)
                    {
                        db.PRO312.AddRange(lstAddPRO312);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO312 = db.PRO312.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO312.Count > 0)
                    {
                        db.PRO312.RemoveRange(lstDeletePRO312);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO312 = db.PRO312.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO312.Count > 0)
                    {
                        db.PRO312.RemoveRange(lstDeletePRO312);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region  Linkage View Code 

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL310 objPRL310 = new PRL310();
            objPRL310 = db.PRL310.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL310 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL310.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            #region bind lines
            List<PRL311> lstPRL311 = db.PRL311.Where(x => x.HeaderId == objPRL310.HeaderId).ToList();
            List<PRL312> lstPRL312 = db.PRL312.Where(x => x.HeaderId == objPRL310.HeaderId).ToList();


            ViewBag.lstPRL311 = lstPRL311;
            ViewBag.lstPRL312 = lstPRL312;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL310.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL310.ActFilledBy) && (objPRL310.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL310.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL310);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL310 objPRL310 = new PRL310();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL310 = db.PRL310.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL310).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL310 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL310.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                
            }
           

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;
            

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            #region bind lines
            List<PRL311> lstPRL311 = db.PRL311.Where(x => x.HeaderId == objPRL310.HeaderId).ToList();
            List<PRL312> lstPRL312 = db.PRL312.Where(x => x.HeaderId == objPRL310.HeaderId).ToList();



            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL310.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL310.ActFilledBy) && (objPRL310.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL310.Project).ToList();
                        }
                    }
                }
                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,

                    objPRL310 = objPRL310,

                    lstPRL311 = lstPRL311,
                    lstPRL312 = lstPRL312,

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL310 PRL310, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL310.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL310 objPRL310 = db.PRL310.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL310.DrawingRevisionNo = PRL310.DrawingRevisionNo;
                    objPRL310.DrawingNo = PRL310.DrawingNo;
                    objPRL310.DCRNo = PRL310.DCRNo;
                    objPRL310.ProtocolNo = PRL310.ProtocolNo;
                    objPRL310.ReqRootGap = PRL310.ReqRootGap;
                    objPRL310.ReqRootFace = PRL310.ReqRootFace;
                    objPRL310.ReqInSideWepAngle = PRL310.ReqInSideWepAngle;
                    objPRL310.ReqOutSideWepAngle = PRL310.ReqOutSideWepAngle;
                    objPRL310.ReqOffset = PRL310.ReqOffset;
                    objPRL310.ReqTotalLength = PRL310.ReqTotalLength;
                    objPRL310.ReqTotalWidth = PRL310.ReqTotalWidth;
                    objPRL310.MinReqTotalLength = PRL310.MinReqTotalLength;
                    objPRL310.MaxReqTotalLength = PRL310.MaxReqTotalLength;
                    objPRL310.MinReqTotalWidth = PRL310.MinReqTotalWidth;
                    objPRL310.MaxReqTotalWidth = PRL310.MaxReqTotalWidth;
                    objPRL310.CheckPoint1 = PRL310.CheckPoint1;
                    objPRL310.CheckPoint2 = PRL310.CheckPoint2;
                    objPRL310.CheckPoint3 = PRL310.CheckPoint3;
                    objPRL310.CheckPoint4 = PRL310.CheckPoint4;
                    objPRL310.CheckPoint5 = PRL310.CheckPoint5;
                    objPRL310.CheckPoint6 = PRL310.CheckPoint6;
                    objPRL310.CheckPoint7 = PRL310.CheckPoint7;
                    objPRL310.CheckPoint9 = PRL310.CheckPoint9;
                    objPRL310.CheckPoint9_2 = PRL310.CheckPoint9_2;
                    objPRL310.QCRemarks = PRL310.QCRemarks;
                    objPRL310.Result = PRL310.Result;

                    objPRL310.EditedBy = UserName;
                    objPRL310.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL310.ActFilledBy = UserName;
                            objPRL310.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL310.ReqFilledBy = UserName;
                            objPRL310.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL310.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL310 objPRL310 = db.PRL310.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL310 != null && string.IsNullOrWhiteSpace(objPRL310.ProtocolNo))
                    {
                        db.PRL310.Remove(objPRL310);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL310.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL311> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL311> lstAddPRL311 = new List<PRL311>();
                List<PRL311> lstDeletePRL311 = new List<PRL311>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL311 obj = db.PRL311.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.MaterialIdentification = item.MaterialIdentification;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL311 obj = new PRL311();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.MaterialIdentification))
                            {
                                obj.MaterialIdentification = item.MaterialIdentification;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL311.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL311.Count > 0)
                {
                    db.PRL311.AddRange(lstAddPRL311);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL311 = db.PRL311.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL311 = db.PRL311.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL311.Count > 0)
                {
                    db.PRL311.RemoveRange(lstDeletePRL311);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL312> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL312> lstAddPRL312 = new List<PRL312>();
                List<PRL312> lstDeletePRL312 = new List<PRL312>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL312 obj = db.PRL312.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {

                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxInSideWepAngle = item.ActMaxInSideWepAngle;
                                obj.ActMaxOutSideWepAngle = item.ActMaxOutSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinInSideWepAngle = item.ActMinInSideWepAngle;
                                obj.ActMinOutSideWepAngle = item.ActMinOutSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL312 obj = new PRL312();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj.SeamNo = item.SeamNo;
                                obj.ActMaxRootGap = item.ActMaxRootGap;
                                obj.ActMaxRootFace = item.ActMaxRootFace;
                                obj.ActMaxInSideWepAngle = item.ActMaxInSideWepAngle;
                                obj.ActMaxOutSideWepAngle = item.ActMaxOutSideWepAngle;
                                obj.ActMaxOffset = item.ActMaxOffset;
                                obj.ActMinRootGap = item.ActMinRootGap;
                                obj.ActMinRootFace = item.ActMinRootFace;
                                obj.ActMinInSideWepAngle = item.ActMinInSideWepAngle;
                                obj.ActMinOutSideWepAngle = item.ActMinOutSideWepAngle;
                                obj.ActMinOffset = item.ActMinOffset;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL312.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL312.Count > 0)
                    {
                        db.PRL312.AddRange(lstAddPRL312);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL312 = db.PRL312.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL312.Count > 0)
                    {
                        db.PRL312.RemoveRange(lstDeletePRL312);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL312 = db.PRL312.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL312.Count > 0)
                    {
                        db.PRL312.RemoveRange(lstDeletePRL312);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion


    }
}