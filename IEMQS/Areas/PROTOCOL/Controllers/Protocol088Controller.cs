﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol088Controller : clsBase
    {
        // GET: PROTOCOL/Protocol088
        string ControllerURL = "/PROTOCOL/Protocol088/";
        string Title = "AFTER PWHT FINAL REPORT FOR MACHINING, VISUAL ,GASKET FACE AND OTHER DIMENSION OF FLANGE";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO435 objPRO435 = new PRO435();
            if (!id.HasValue)
            {
                try
                {
                    objPRO435.ProtocolNo = string.Empty;
                    objPRO435.CreatedBy = objClsLoginInfo.UserName;
                    objPRO435.CreatedOn = DateTime.Now;

                    #region  MACHINING | PRO435
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "1",
                        Description = "Identification",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "2",
                        Description = "Visual inspection",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "3",
                        Description = "Gasket face step OD",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "4",
                        Description = "Gasket height",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "5",
                        Description = "Gasket face finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "6",
                        Description = "Gasket step OD radius Top",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "7",
                        Description = "Gasket step OD radius Bottom",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "8",
                        Description = "Groove width",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "9",
                        Description = "Groove depth",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "10",
                        Description = "Groove corner radius",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "11",
                        Description = "Groove angle",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "12",
                        Description = "Groove taper finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "13",
                        Description = "Groove face finish",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "14",
                        Description = "Gasket groove P.C.D",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO435.PRO437.Add(new PRO437
                    {
                        SrNo = "15",
                        Description = "Final Visual inspection after O/L",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO435.Add(objPRO435);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO435.HeaderId;
            }
            else
            {
                objPRO435 = db.PRO435.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO436> lstPRO436 = db.PRO436.Where(x => x.HeaderId == objPRO435.HeaderId).ToList();
            List<PRO437> lstPRO437 = db.PRO437.Where(x => x.HeaderId == objPRO435.HeaderId).ToList();

            ViewBag._lstPRO436 = lstPRO436;
            ViewBag._lstPRO437 = lstPRO437;

            #endregion

            return View(objPRO435);
            //return View();
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO435 PRO435)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO435.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO435 objPRO435 = db.PRO435.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO435.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO435.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO435.ProtocolNo = PRO435.ProtocolNo;
                        objPRO435.NozzleNo = PRO435.NozzleNo;
                        objPRO435.GasketFaceRaisedFaceSeamNo = PRO435.GasketFaceRaisedFaceSeamNo;
                        objPRO435.PartDescription = PRO435.PartDescription;
                        objPRO435.GasketGrooveSeamNo = PRO435.GasketGrooveSeamNo;
                        objPRO435.ItemNo = PRO435.ItemNo;
                        objPRO435.CheckPoint1 = PRO435.CheckPoint1;
                        objPRO435.CheckPoint2 = PRO435.CheckPoint2;
                        objPRO435.CheckPoint4_1_1 = PRO435.CheckPoint4_1_1;
                        objPRO435.CheckPoint4_1_2 = PRO435.CheckPoint4_1_2;
                        objPRO435.CheckPoint4_2_1 = PRO435.CheckPoint4_2_1;
                        objPRO435.CheckPoint4_2_2 = PRO435.CheckPoint4_2_2;
                        objPRO435.CheckPoint4_3_1 = PRO435.CheckPoint4_3_1;
                        objPRO435.CheckPoint4_3_2 = PRO435.CheckPoint4_3_2;
                        objPRO435.CheckPoint4_4_1 = PRO435.CheckPoint4_4_1;
                        objPRO435.CheckPoint4_4_2 = PRO435.CheckPoint4_4_2;
                        objPRO435.CheckPoint4_5_1 = PRO435.CheckPoint4_5_1;
                        objPRO435.CheckPoint4_5_2 = PRO435.CheckPoint4_5_2;

                        objPRO435.QCRemarks = PRO435.QCRemarks;
                        objPRO435.Result = PRO435.Result;

                        objPRO435.EditedBy = objClsLoginInfo.UserName;
                        objPRO435.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO435.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO435 objPRO435 = db.PRO435.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO435 != null && string.IsNullOrWhiteSpace(objPRO435.ProtocolNo))
                    {
                        db.PRO435.Remove(objPRO435);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO435.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO436> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO436> lstAddPRO436 = new List<PRO436>();
                List<PRO436> lstDeletePRO436 = new List<PRO436>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO436 obj = db.PRO436.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO436 obj = new PRO436();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO436.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO436.Count > 0)
                    {
                        db.PRO436.AddRange(lstAddPRO436);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO436 = db.PRO436.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO436.Count > 0)
                    {
                        db.PRO436.RemoveRange(lstDeletePRO436);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO436 = db.PRO436.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO436.Count > 0)
                    {
                        db.PRO436.RemoveRange(lstDeletePRO436);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO437> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO437> lstAddPRO437 = new List<PRO437>();
                List<PRO437> lstDeletePRO437 = new List<PRO437>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO437 obj = db.PRO437.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO437();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO437.Add(obj);
                        }
                    }
                    if (lstAddPRO437.Count > 0)
                    {
                        db.PRO437.AddRange(lstAddPRO437);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO437 = db.PRO437.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO437.Count > 0)
                    {
                        db.PRO437.RemoveRange(lstDeletePRO437);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO437 = db.PRO437.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO437.Count > 0)
                    {
                        db.PRO437.RemoveRange(lstDeletePRO437);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL435 objPRL435 = new PRL435();
            objPRL435 = db.PRL435.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL435 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL435.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL436> lstPRL436 = db.PRL436.Where(x => x.HeaderId == objPRL435.HeaderId).ToList();
            List<PRL437> lstPRL437 = db.PRL437.Where(x => x.HeaderId == objPRL435.HeaderId).ToList();

            ViewBag._lstPRL436 = lstPRL436;
            ViewBag._lstPRL437 = lstPRL437;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL435.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL435.ActFilledBy) && (objPRL435.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL435.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL435);

            //return View();
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL435 prl435, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl435.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL435 objPRL435 = db.PRL435.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL435.DrawingRevisionNo = prl435.DrawingRevisionNo;
                    objPRL435.DrawingNo = prl435.DrawingNo;
                    objPRL435.DCRNo = prl435.DCRNo;
                    objPRL435.ProtocolNo = prl435.ProtocolNo;

                    objPRL435.ProtocolNo = prl435.ProtocolNo;
                    objPRL435.NozzleNo = prl435.NozzleNo;
                    objPRL435.GasketFaceRaisedFaceSeamNo = prl435.GasketFaceRaisedFaceSeamNo;
                    objPRL435.PartDescription = prl435.PartDescription;
                    objPRL435.GasketGrooveSeamNo = prl435.GasketGrooveSeamNo;
                    objPRL435.ItemNo = prl435.ItemNo;
                    objPRL435.CheckPoint1 = prl435.CheckPoint1;
                    objPRL435.CheckPoint2 = prl435.CheckPoint2;
                    objPRL435.CheckPoint4_1_1 = prl435.CheckPoint4_1_1;
                    objPRL435.CheckPoint4_1_2 = prl435.CheckPoint4_1_2;
                    objPRL435.CheckPoint4_2_1 = prl435.CheckPoint4_2_1;
                    objPRL435.CheckPoint4_2_2 = prl435.CheckPoint4_2_2;
                    objPRL435.CheckPoint4_3_1 = prl435.CheckPoint4_3_1;
                    objPRL435.CheckPoint4_3_2 = prl435.CheckPoint4_3_2;
                    objPRL435.CheckPoint4_4_1 = prl435.CheckPoint4_4_1;
                    objPRL435.CheckPoint4_4_2 = prl435.CheckPoint4_4_2;
                    objPRL435.CheckPoint4_5_1 = prl435.CheckPoint4_5_1;
                    objPRL435.CheckPoint4_5_2 = prl435.CheckPoint4_5_2;

                    objPRL435.QCRemarks = prl435.QCRemarks;
                    objPRL435.Result = prl435.Result;

                    objPRL435.EditedBy = UserName;
                    objPRL435.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL435.ActFilledBy = UserName;
                            objPRL435.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL435.ReqFilledBy = UserName;
                            objPRL435.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL435.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL435 objPRL435 = db.PRL435.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL435 != null && string.IsNullOrWhiteSpace(objPRL435.ProtocolNo))
                    {
                        db.PRL435.Remove(objPRL435);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL435.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL436> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL436> lstAddPRL436 = new List<PRL436>();
                List<PRL436> lstDeletePRL436 = new List<PRL436>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL436 obj = db.PRL436.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL436 obj = new PRL436();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL436.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL436.Count > 0)
                    {
                        db.PRL436.AddRange(lstAddPRL436);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL436 = db.PRL436.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL436.Count > 0)
                    {
                        db.PRL436.RemoveRange(lstDeletePRL436);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL436 = db.PRL436.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL436.Count > 0)
                    {
                        db.PRL436.RemoveRange(lstDeletePRL436);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL437> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL437> lstAddPRL437 = new List<PRL437>();
                List<PRL437> lstDeletePRL437 = new List<PRL437>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL437 obj = db.PRL437.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL437();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL437.Add(obj);
                        }
                    }
                    if (lstAddPRL437.Count > 0)
                    {
                        db.PRL437.AddRange(lstAddPRL437);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL437 = db.PRL437.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL437.Count > 0)
                    {
                        db.PRL437.RemoveRange(lstDeletePRL437);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL437 = db.PRL437.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL437.Count > 0)
                    {
                        db.PRL437.RemoveRange(lstDeletePRL437);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

    }
}