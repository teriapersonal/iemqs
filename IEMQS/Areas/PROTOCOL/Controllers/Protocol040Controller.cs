﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol040Controller : clsBase
    {
        // GET: PROTOCOL/Protocol040

        string ControllerURL = "/PROTOCOL/Protocol040/";
        string Title = "SET-UP INSPECTION REPORT FOR ELLIPSOIDAL/TORI SPHERICAL FORMED D'END";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO195 objPRO195 = new PRO195();
            if (!id.HasValue)
            {
                try
                {
                    objPRO195.ProtocolNo = string.Empty;
                    objPRO195.CreatedBy = objClsLoginInfo.UserName;
                    objPRO195.CreatedOn = DateTime.Now;

                    #region DIMENSION
                    objPRO195.RemOverCrowing = "TOL: 1¼ (1.25) % OF NOMINAL I/S DIA.";
                    objPRO195.RemUnderCrowing = "TOL:5/8 (0.625) % OF NOMINAL I/S DIA.";
                    #endregion

                    #region OVALITY
                    objPRO195.PRO196.Add(new PRO196
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO195.PRO196.Add(new PRO196
                    {
                        Orientation = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO195.PRO196.Add(new PRO196
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO195.PRO196.Add(new PRO196
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO195.PRO196.Add(new PRO196
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO195.PRO196.Add(new PRO196
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO195.PRO196.Add(new PRO196
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO195.PRO196.Add(new PRO196
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region  THICKNESS
                    objPRO195.PRO197.Add(new PRO197
                    {
                        ThiknessColumn1 = "PETAL 1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO195.PRO197.Add(new PRO197
                    {
                        ThiknessColumn1 = "PETAL 2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO195.PRO197.Add(new PRO197
                    {
                        ThiknessColumn1 = "PETAL 3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO195.PRO197.Add(new PRO197
                    {
                        ThiknessColumn1 = "PETAL 4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    #region DETAIL THICKNESS REPORT
                    objPRO195.PRO199_4.Add(new PRO199_4
                    {
                        Description = "PETAL 1",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO195.PRO199_4.Add(new PRO199_4
                    {
                        Description = "PETAL 2",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO195.PRO199_4.Add(new PRO199_4
                    {
                        Description = "PETAL 3",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO195.PRO199_4.Add(new PRO199_4
                    {
                        Description = "PETAL 4",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO195.Add(objPRO195);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO195.HeaderId;
            }
            else
            {
                objPRO195 = db.PRO195.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };
            ViewBag.TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO196> lstPRO196 = db.PRO196.Where(x => x.HeaderId == objPRO195.HeaderId).ToList();
            List<PRO197> lstPRO197 = db.PRO197.Where(x => x.HeaderId == objPRO195.HeaderId).ToList();
            List<PRO198> lstPRO198 = db.PRO198.Where(x => x.HeaderId == objPRO195.HeaderId).ToList();
            List<PRO199> lstPRO199 = db.PRO199.Where(x => x.HeaderId == objPRO195.HeaderId).ToList();
            List<PRO199_2> lstPRO199_2 = db.PRO199_2.Where(x => x.HeaderId == objPRO195.HeaderId).ToList();
            List<PRO199_3> lstPRO199_3 = db.PRO199_3.Where(x => x.HeaderId == objPRO195.HeaderId).ToList();
            List<PRO199_4> lstPRO199_4 = db.PRO199_4.Where(x => x.HeaderId == objPRO195.HeaderId).ToList();
            List<SP_IPI_PROTOCOL040_GET_LINES8_DATA_Result> lstPRO199_5 = db.SP_IPI_PROTOCOL040_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO195.HeaderId).ToList();
            List<SP_IPI_PROTOCOL040_GET_LINES9_DATA_Result> lstPRO199_6 = db.SP_IPI_PROTOCOL040_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRO195.HeaderId).ToList();

            ViewBag.lstPRO196 = lstPRO196;
            ViewBag.lstPRO197 = lstPRO197;
            ViewBag.lstPRO198 = lstPRO198;
            ViewBag.lstPRO199 = lstPRO199;
            ViewBag.lstPRO199_2 = lstPRO199_2;
            ViewBag.lstPRO199_3 = lstPRO199_3;
            ViewBag.lstPRO199_4 = lstPRO199_4;
            ViewBag.lstPRO199_5 = lstPRO199_5;
            ViewBag.lstPRO199_6 = lstPRO199_6;
            #endregion

            return View(objPRO195);
        }
        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO195 PRO195)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO195.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO195 objPRO195 = db.PRO195.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO195.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO195.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO195.ProtocolNo = PRO195.ProtocolNo;
                        objPRO195.ReqInsideCircumference = PRO195.ReqInsideCircumference;
                        objPRO195.ActInsideCircumference = PRO195.ActInsideCircumference;
                        objPRO195.RemInsideCircumference = PRO195.RemInsideCircumference;
                        objPRO195.ReqTotalHeight = PRO195.ReqTotalHeight;
                        objPRO195.ActTotalHeight = PRO195.ActTotalHeight;
                        objPRO195.RemTotalHeight = PRO195.RemTotalHeight;

                        objPRO195.ReqStraightFace = PRO195.ReqStraightFace;
                        objPRO195.ActStraightFace = PRO195.ActStraightFace;
                        objPRO195.RemStraightFace = PRO195.RemStraightFace;
                        objPRO195.ReqCrownRadius = PRO195.ReqCrownRadius;
                        objPRO195.ActCrownRadius = PRO195.ActCrownRadius;
                        objPRO195.RemCrownRadius = PRO195.RemCrownRadius;
                        objPRO195.ReqKnuckleRadius = PRO195.ReqKnuckleRadius;
                        objPRO195.ActKnuckleRadius = PRO195.ActKnuckleRadius;
                        objPRO195.RemKnuckleRadius = PRO195.RemKnuckleRadius;
                        objPRO195.ReqGapAtKnuckleAreaUsingTemplate = PRO195.ReqGapAtKnuckleAreaUsingTemplate;
                        objPRO195.ActGapAtKnuckleAreaUsingTemplate = PRO195.ActGapAtKnuckleAreaUsingTemplate;
                        objPRO195.RemGapAtKnuckleAreaUsingTemplate = PRO195.RemGapAtKnuckleAreaUsingTemplate;

                        objPRO195.ActGapAtKnuckleAreaUsingTemplateMax = PRO195.ActGapAtKnuckleAreaUsingTemplateMax;
                        objPRO195.RemGapAtKnuckleAreaUsingTemplateMax = PRO195.RemGapAtKnuckleAreaUsingTemplateMax;

                        objPRO195.ReqOverCrowing = PRO195.ReqOverCrowing;
                        objPRO195.ActOverCrowing = PRO195.ActOverCrowing;
                        objPRO195.RemOverCrowing = PRO195.RemOverCrowing;
                        objPRO195.ReqUnderCrowing = PRO195.ReqUnderCrowing;
                        objPRO195.ActUnderCrowing = PRO195.ActUnderCrowing;
                        objPRO195.RemUnderCrowing = PRO195.RemUnderCrowing;
                        objPRO195.ReqConcentricity = PRO195.ReqConcentricity;
                        objPRO195.ActConcentricity = PRO195.ActConcentricity;
                        objPRO195.RemConcentricity = PRO195.RemConcentricity;
                        objPRO195.ReqTypeOfForming = PRO195.ReqTypeOfForming;
                        objPRO195.ActTypeOfForming = PRO195.ActTypeOfForming;
                        objPRO195.RemTypeOfForming = PRO195.RemTypeOfForming;
                        objPRO195.Ovality = PRO195.Ovality;
                        objPRO195.OutBy = PRO195.OutBy;
                        objPRO195.ReqThiknessMin = PRO195.ReqThiknessMin;
                        objPRO195.ReqThiknessNom = PRO195.ReqThiknessNom;
                        objPRO195.EvalueTemplate = PRO195.EvalueTemplate;
                        objPRO195.ReqChordLength = PRO195.ReqChordLength;
                        objPRO195.ActChordLength = PRO195.ActChordLength;
                        objPRO195.ReqRadius = PRO195.ReqRadius;
                        objPRO195.ActRadius = PRO195.ActRadius;
                        objPRO195.GapAllowable = PRO195.GapAllowable;
                        objPRO195.ActGap = PRO195.ActGap;
                        objPRO195.ReqRootGap = PRO195.ReqRootGap;
                        objPRO195.ReqRootFace = PRO195.ReqRootFace;
                        objPRO195.Pickinout = PRO195.Pickinout;
                        objPRO195.OffSet = PRO195.OffSet;
                        objPRO195.InsideWepAngle = PRO195.InsideWepAngle;
                        objPRO195.OutsideWepAngle = PRO195.OutsideWepAngle;
                        objPRO195.DetailThiknessReport = PRO195.DetailThiknessReport;
                        objPRO195.CheckPoint1 = PRO195.CheckPoint1;
                        objPRO195.CheckPoint2 = PRO195.CheckPoint2;
                        objPRO195.CheckPoint3 = PRO195.CheckPoint3;
                        objPRO195.CheckPoint4 = PRO195.CheckPoint4;
                        objPRO195.CheckPoint5 = PRO195.CheckPoint5;
                        objPRO195.CheckPoint6 = PRO195.CheckPoint6;
                        objPRO195.CheckPoint6_2 = PRO195.CheckPoint6_2;
                        objPRO195.CheckPoint6_3 = PRO195.CheckPoint6_3;
                        objPRO195.CheckPoint7 = PRO195.CheckPoint7;
                        objPRO195.CheckPoint7_2 = PRO195.CheckPoint7_2;
                        objPRO195.CheckPoint7_3 = PRO195.CheckPoint7_3;
                        objPRO195.CheckPoint7_4 = PRO195.CheckPoint7_4;
                        objPRO195.CheckPoint9 = PRO195.CheckPoint9;
                        objPRO195.CheckPoint10 = PRO195.CheckPoint10;
                        objPRO195.CheckPoint9_2 = PRO195.CheckPoint9_2;
                        objPRO195.QCRemarks = PRO195.QCRemarks;
                        objPRO195.Result = PRO195.Result;
                        objPRO195.ReqCladStripingWidth = PRO195.ReqCladStripingWidth;
                        objPRO195.ReqCladStripingDepth = PRO195.ReqCladStripingDepth;
                        objPRO195.ReqCladStripingOffset = PRO195.ReqCladStripingOffset;
                        objPRO195.Note1 = PRO195.Note1;
                        objPRO195.Note2 = PRO195.Note2;
                        objPRO195.Note3 = PRO195.Note3;
                        objPRO195.IdentificationOfCleats = PRO195.IdentificationOfCleats;
                        objPRO195.EditedBy = objClsLoginInfo.UserName;
                        objPRO195.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO195.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO195 objPRO195 = db.PRO195.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO195 != null && string.IsNullOrWhiteSpace(objPRO195.ProtocolNo))
                    {
                        db.PRO195.Remove(objPRO195);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO195.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO196> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO196> lstAddPRO196 = new List<PRO196>();
                List<PRO196> lstDeletePRO196 = new List<PRO196>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO196 obj = db.PRO196.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO196 obj = new PRO196();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO196.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO196.Count > 0)
                    {
                        db.PRO196.AddRange(lstAddPRO196);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO196 = db.PRO196.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO196.Count > 0)
                    {
                        db.PRO196.RemoveRange(lstDeletePRO196);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO196 = db.PRO196.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO196.Count > 0)
                    {
                        db.PRO196.RemoveRange(lstDeletePRO196);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO197> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO197> lstAddPRO197 = new List<PRO197>();
                List<PRO197> lstDeletePRO197 = new List<PRO197>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO197 obj = db.PRO197.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO197();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.ThiknessColumn1 = item.ThiknessColumn1;
                        obj.ThiknessColumn2 = item.ThiknessColumn2;
                        if (isAdded)
                        {
                            lstAddPRO197.Add(obj);
                        }
                    }
                    if (lstAddPRO197.Count > 0)
                    {
                        db.PRO197.AddRange(lstAddPRO197);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO197 = db.PRO197.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO197.Count > 0)
                    {
                        db.PRO197.RemoveRange(lstDeletePRO197);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO197 = db.PRO197.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO197.Count > 0)
                    {
                        db.PRO197.RemoveRange(lstDeletePRO197);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO198> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO198> lstAddPRO198 = new List<PRO198>();
                List<PRO198> lstDeletePRO198 = new List<PRO198>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO198 obj = db.PRO198.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRO198();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActualArcLength = item.ActualArcLength;


                        if (isAdded)
                        {
                            lstAddPRO198.Add(obj);
                        }
                    }
                    if (lstAddPRO198.Count > 0)
                    {
                        db.PRO198.AddRange(lstAddPRO198);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO198 = db.PRO198.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO198.Count > 0)
                    {
                        db.PRO198.RemoveRange(lstDeletePRO198);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO198 = db.PRO198.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO198.Count > 0)
                    {
                        db.PRO198.RemoveRange(lstDeletePRO198);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO199> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO199> lstAddPRO199 = new List<PRO199>();
                List<PRO199> lstDeletePRO199 = new List<PRO199>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO199 obj = db.PRO199.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO199();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.RootGapMin = item.RootGapMin;
                        obj.RootGapMax = item.RootGapMax;
                        obj.RootFaceMin = item.RootFaceMin;
                        obj.RootFaceMax = item.RootFaceMax;
                        obj.Peak_in_out_Top = item.Peak_in_out_Top;
                        obj.Peak_in_out_Mid = item.Peak_in_out_Mid;
                        obj.Peak_in_out_Bottom = item.Peak_in_out_Bottom;
                        obj.OffSetMin = item.OffSetMin;
                        obj.OffSetMax = item.OffSetMax;
                        obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
                        obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
                        obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
                        obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;

                        if (isAdded)
                        {
                            lstAddPRO199.Add(obj);
                        }
                    }
                    if (lstAddPRO199.Count > 0)
                    {
                        db.PRO199.AddRange(lstAddPRO199);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO199 = db.PRO199.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO199.Count > 0)
                    {
                        db.PRO199.RemoveRange(lstDeletePRO199);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO199 = db.PRO199.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO199.Count > 0)
                    {
                        db.PRO199.RemoveRange(lstDeletePRO199);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6(List<PRO199_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO199_3> lstAddPRO199_3 = new List<PRO199_3>();
                List<PRO199_3> lstDeletePRO199_3 = new List<PRO199_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO199_3 obj = db.PRO199_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO199_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.MaterialIdentificationReport = item.MaterialIdentificationReport;

                        if (isAdded)
                        {
                            lstAddPRO199_3.Add(obj);
                        }
                    }
                    if (lstAddPRO199_3.Count > 0)
                    {
                        db.PRO199_3.AddRange(lstAddPRO199_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO199_3 = db.PRO199_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO199_3.Count > 0)
                    {
                        db.PRO199_3.RemoveRange(lstDeletePRO199_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO199_3 = db.PRO199_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO199_3.Count > 0)
                    {
                        db.PRO199_3.RemoveRange(lstDeletePRO199_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7(List<PRO199_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO199_4> lstAddPRO189_4 = new List<PRO199_4>();
                List<PRO199_4> lstDeletePRO189_4 = new List<PRO199_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO199_4 obj = db.PRO199_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO199_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Description = item.Description;
                        obj.Petal_1 = item.Petal_1;
                        obj.Petal_2 = item.Petal_2;
                        obj.Petal_3 = item.Petal_3;
                        obj.Petal_4 = item.Petal_4;
                        obj.Petal_5 = item.Petal_5;
                        obj.Petal_6 = item.Petal_6;
                        obj.Petal_7 = item.Petal_7;
                        obj.Petal_8 = item.Petal_8;
                        obj.Petal_9 = item.Petal_9;
                        obj.Petal_10 = item.Petal_10;

                        if (isAdded)
                        {
                            lstAddPRO189_4.Add(obj);
                        }
                    }
                    if (lstAddPRO189_4.Count > 0)
                    {
                        db.PRO199_4.AddRange(lstAddPRO189_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO189_4 = db.PRO199_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO189_4.Count > 0)
                    {
                        db.PRO199_4.RemoveRange(lstDeletePRO189_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO189_4 = db.PRO199_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO189_4.Count > 0)
                    {
                        db.PRO199_4.RemoveRange(lstDeletePRO189_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details

        [HttpPost]
        public JsonResult SaveProtocolLine8(List<PRO199_5> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO199_5> lstAddPRO199_5 = new List<PRO199_5>();
                List<PRO199_5> lstDeletePRO199_5 = new List<PRO199_5>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO199_5 obj = db.PRO199_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO199_5 objFecthSeam = new PRO199_5();
                        if (obj == null)
                        {
                            obj = new PRO199_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO199_5.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                        }
                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNo = item.SpotNo;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRO199_5.Add(obj);
                        }
                    }
                }
                if (lstAddPRO199_5.Count > 0)
                {
                    db.PRO199_5.AddRange(lstAddPRO199_5);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO199_5 = db.PRO199_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRO199_5 = db.PRO199_5.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRO199_5.Count > 0)
                {
                    db.PRO199_5.RemoveRange(lstDeletePRO199_5);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 details

        [HttpPost]
        public JsonResult SaveProtocolLine9(List<PRO199_6> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO199_6> lstAddPRO199_6 = new List<PRO199_6>();
                List<PRO199_6> lstDeletePRO199_6 = new List<PRO199_6>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO199_6 obj = db.PRO199_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRO199_6 objFecthSeam = new PRO199_6();
                        if (obj == null)
                        {
                            obj = new PRO199_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRO199_6.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }

                        obj.CleatNo = item.CleatNo;
                        obj.SizeWelded = item.SizeWelded;
                        obj.Location = item.Location;
                        obj.OutsideOrInside = item.OutsideOrInside;
                        obj.DistanceFromSeam = item.DistanceFromSeam;
                        obj.ShellEvelution = item.ShellEvelution;
                        if (isAdded)
                        {
                            lstAddPRO199_6.Add(obj);
                        }
                    }
                }
                if (lstAddPRO199_6.Count > 0)
                {
                    db.PRO199_6.AddRange(lstAddPRO199_6);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRO199_6 = db.PRO199_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else { lstDeletePRO199_6 = db.PRO199_6.Where(x => x.HeaderId == HeaderId).ToList(); }
                if (lstDeletePRO199_6.Count > 0)
                {
                    db.PRO199_6.RemoveRange(lstDeletePRO199_6);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #endregion

        #region Linkage View Code  
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL195 objPRL195 = new PRL195();
            objPRL195 = db.PRL195.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL195 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL195.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;


            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };
            ViewBag.TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL196> lstPRL196 = db.PRL196.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<PRL197> lstPRL197 = db.PRL197.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<PRL198> lstPRL198 = db.PRL198.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<PRL199> lstPRL199 = db.PRL199.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<PRL199_2> lstPRL199_2 = db.PRL199_2.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<PRL199_3> lstPRL199_3 = db.PRL199_3.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<PRL199_4> lstPRL199_4 = db.PRL199_4.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<SP_IPI_PROTOCOL040_LINKAGE_GET_LINES8_DATA_Result> lstPRL199_5 = db.SP_IPI_PROTOCOL040_LINKAGE_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL195.HeaderId).ToList();
            List<SP_IPI_PROTOCOL040_LINKAGE_GET_LINES9_DATA_Result> lstPRL199_6 = db.SP_IPI_PROTOCOL040_LINKAGE_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL195.HeaderId).ToList();

            ViewBag.lstPRL196 = lstPRL196;
            ViewBag.lstPRL197 = lstPRL197;
            ViewBag.lstPRL198 = lstPRL198;
            ViewBag.lstPRL199 = lstPRL199;
            ViewBag.lstPRL199_2 = lstPRL199_2;
            ViewBag.lstPRL199_3 = lstPRL199_3;
            ViewBag.lstPRL199_4 = lstPRL199_4;
            ViewBag.lstPRL199_5 = lstPRL199_5;
            ViewBag.lstPRL199_6 = lstPRL199_6;
            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL195.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL195.ActFilledBy) && (objPRL195.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL195.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }

            return View(objPRL195);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL195 objPRL195 = new PRL195();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL195 = db.PRL195.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL195).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL195 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL195.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            if (m == 1)
            { isEditable = true; }
            else
            { isEditable = false; }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            List<string> lsttypesofforming = new List<string> { "HOT", "COLD", "WARM" };
            var TypesOfForming = lsttypesofforming.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideOrInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL196> lstPRL196 = db.PRL196.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<PRL197> lstPRL197 = db.PRL197.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<PRL198> lstPRL198 = db.PRL198.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<PRL199> lstPRL199 = db.PRL199.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<PRL199_2> lstPRL199_2 = db.PRL199_2.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<PRL199_3> lstPRL199_3 = db.PRL199_3.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<PRL199_4> lstPRL199_4 = db.PRL199_4.Where(x => x.HeaderId == objPRL195.HeaderId).ToList();
            List<SP_IPI_PROTOCOL040_LINKAGE_GET_LINES8_DATA_Result> lstPRL199_5 = db.SP_IPI_PROTOCOL040_LINKAGE_GET_LINES8_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL195.HeaderId).ToList();
            List<SP_IPI_PROTOCOL040_LINKAGE_GET_LINES9_DATA_Result> lstPRL199_6 = db.SP_IPI_PROTOCOL040_LINKAGE_GET_LINES9_DATA(1, int.MaxValue, "", " HeaderId = " + objPRL195.HeaderId).ToList();

            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL195.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL195.ActFilledBy) && (objPRL195.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL195.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    OutsideInside = OutsideOrInside,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    TypesOfForming = TypesOfForming,

                    objPRL195 = objPRL195,

                    lstPRL196 = lstPRL196,
                    lstPRL197 = lstPRL197,
                    lstPRL198 = lstPRL198,
                    lstPRL199 = lstPRL199,
                    lstPRL199_2 = lstPRL199_2,
                    lstPRL199_3 = lstPRL199_3,
                    lstPRL199_4 = lstPRL199_4,
                    lstPRL199_5 = lstPRL199_5,
                    lstPRL199_6 = lstPRL199_6
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL195 PRL195, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL195.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL195 objPRL195 = db.PRL195.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data
                    objPRL195.ProtocolNo = PRL195.ProtocolNo;

                    objPRL195.DrawingRevisionNo = PRL195.DrawingRevisionNo;
                    objPRL195.DrawingNo = PRL195.DrawingNo;
                    objPRL195.DCRNo = PRL195.DCRNo;

                    objPRL195.ReqInsideCircumference = PRL195.ReqInsideCircumference;
                    objPRL195.ActInsideCircumference = PRL195.ActInsideCircumference;
                    objPRL195.RemInsideCircumference = PRL195.RemInsideCircumference;
                    objPRL195.ReqTotalHeight = PRL195.ReqTotalHeight;
                    objPRL195.ActTotalHeight = PRL195.ActTotalHeight;
                    objPRL195.RemTotalHeight = PRL195.RemTotalHeight;

                    objPRL195.ReqStraightFace = PRL195.ReqStraightFace;
                    objPRL195.ActStraightFace = PRL195.ActStraightFace;
                    objPRL195.RemStraightFace = PRL195.RemStraightFace;
                    objPRL195.ReqCrownRadius = PRL195.ReqCrownRadius;
                    objPRL195.ActCrownRadius = PRL195.ActCrownRadius;
                    objPRL195.RemCrownRadius = PRL195.RemCrownRadius;
                    objPRL195.ReqKnuckleRadius = PRL195.ReqKnuckleRadius;
                    objPRL195.ActKnuckleRadius = PRL195.ActKnuckleRadius;
                    objPRL195.RemKnuckleRadius = PRL195.RemKnuckleRadius;
                    objPRL195.ReqGapAtKnuckleAreaUsingTemplate = PRL195.ReqGapAtKnuckleAreaUsingTemplate;
                    objPRL195.ActGapAtKnuckleAreaUsingTemplate = PRL195.ActGapAtKnuckleAreaUsingTemplate;
                    objPRL195.RemGapAtKnuckleAreaUsingTemplate = PRL195.RemGapAtKnuckleAreaUsingTemplate;

                    objPRL195.ActGapAtKnuckleAreaUsingTemplateMax = PRL195.ActGapAtKnuckleAreaUsingTemplateMax;
                    objPRL195.RemGapAtKnuckleAreaUsingTemplateMax = PRL195.RemGapAtKnuckleAreaUsingTemplateMax;


                    objPRL195.ReqOverCrowing = PRL195.ReqOverCrowing;
                    objPRL195.ActOverCrowing = PRL195.ActOverCrowing;
                    objPRL195.RemOverCrowing = PRL195.RemOverCrowing;
                    objPRL195.ReqUnderCrowing = PRL195.ReqUnderCrowing;
                    objPRL195.ActUnderCrowing = PRL195.ActUnderCrowing;
                    objPRL195.RemUnderCrowing = PRL195.RemUnderCrowing;
                    objPRL195.ReqConcentricity = PRL195.ReqConcentricity;
                    objPRL195.ActConcentricity = PRL195.ActConcentricity;
                    objPRL195.RemConcentricity = PRL195.RemConcentricity;
                    objPRL195.ReqTypeOfForming = PRL195.ReqTypeOfForming;
                    objPRL195.ActTypeOfForming = PRL195.ActTypeOfForming;
                    objPRL195.RemTypeOfForming = PRL195.RemTypeOfForming;
                    objPRL195.Ovality = PRL195.Ovality;
                    objPRL195.OutBy = PRL195.OutBy;
                    objPRL195.ReqThiknessMin = PRL195.ReqThiknessMin;
                    objPRL195.ReqThiknessNom = PRL195.ReqThiknessNom;
                    objPRL195.EvalueTemplate = PRL195.EvalueTemplate;
                    objPRL195.ReqChordLength = PRL195.ReqChordLength;
                    objPRL195.ActChordLength = PRL195.ActChordLength;
                    objPRL195.ReqRadius = PRL195.ReqRadius;
                    objPRL195.ActRadius = PRL195.ActRadius;
                    objPRL195.GapAllowable = PRL195.GapAllowable;
                    objPRL195.ActGap = PRL195.ActGap;
                    objPRL195.ReqRootGap = PRL195.ReqRootGap;
                    objPRL195.ReqRootFace = PRL195.ReqRootFace;
                    objPRL195.Pickinout = PRL195.Pickinout;
                    objPRL195.OffSet = PRL195.OffSet;
                    objPRL195.InsideWepAngle = PRL195.InsideWepAngle;
                    objPRL195.OutsideWepAngle = PRL195.OutsideWepAngle;
                    objPRL195.DetailThiknessReport = PRL195.DetailThiknessReport;
                    objPRL195.CheckPoint1 = PRL195.CheckPoint1;
                    objPRL195.CheckPoint2 = PRL195.CheckPoint2;
                    objPRL195.CheckPoint3 = PRL195.CheckPoint3;
                    objPRL195.CheckPoint4 = PRL195.CheckPoint4;
                    objPRL195.CheckPoint5 = PRL195.CheckPoint5;
                    objPRL195.CheckPoint6 = PRL195.CheckPoint6;
                    objPRL195.CheckPoint6_2 = PRL195.CheckPoint6_2;
                    objPRL195.CheckPoint6_3 = PRL195.CheckPoint6_3;
                    objPRL195.CheckPoint7 = PRL195.CheckPoint7;
                    objPRL195.CheckPoint7_2 = PRL195.CheckPoint7_2;
                    objPRL195.CheckPoint7_3 = PRL195.CheckPoint7_3;
                    objPRL195.CheckPoint7_4 = PRL195.CheckPoint7_4;
                    objPRL195.CheckPoint9 = PRL195.CheckPoint9;
                    objPRL195.CheckPoint10 = PRL195.CheckPoint10;
                    objPRL195.CheckPoint9_2 = PRL195.CheckPoint9_2;
                    objPRL195.QCRemarks = PRL195.QCRemarks;
                    objPRL195.Result = PRL195.Result;
                    objPRL195.ReqCladStripingWidth = PRL195.ReqCladStripingWidth;
                    objPRL195.ReqCladStripingDepth = PRL195.ReqCladStripingDepth;
                    objPRL195.ReqCladStripingOffset = PRL195.ReqCladStripingOffset;
                    objPRL195.Note1 = PRL195.Note1;
                    objPRL195.Note2 = PRL195.Note2;
                    objPRL195.Note3 = PRL195.Note3;
                    objPRL195.IdentificationOfCleats = PRL195.IdentificationOfCleats;
                    objPRL195.EditedBy = UserName;
                    objPRL195.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL195.ActFilledBy = UserName;
                            objPRL195.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL195.ReqFilledBy = UserName;
                            objPRL195.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL195.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL195 objPRL195 = db.PRL195.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL195 != null && string.IsNullOrWhiteSpace(objPRL195.ProtocolNo))
                    {
                        db.PRL195.Remove(objPRL195);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL195.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL196> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL196> lstAddPRL196 = new List<PRL196>();
                List<PRL196> lstDeletePRL196 = new List<PRL196>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL196 obj = db.PRL196.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL196 obj = new PRL196();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj.Orientation = item.Orientation;
                                obj.Actual = item.Actual;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL196.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL196.Count > 0)
                {
                    db.PRL196.AddRange(lstAddPRL196);
                }


                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();
                    lstDeletePRL196 = db.PRL196.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                }
                else
                {
                    lstDeletePRL196 = db.PRL196.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL196.Count > 0)
                {
                    db.PRL196.RemoveRange(lstDeletePRL196);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL197> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL197> lstAddPRL197 = new List<PRL197>();
                List<PRL197> lstDeletePRL197 = new List<PRL197>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL197 obj = db.PRL197.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL197();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.ThiknessColumn1 = item.ThiknessColumn1;
                        obj.ThiknessColumn2 = item.ThiknessColumn2;
                        if (isAdded)
                        {
                            lstAddPRL197.Add(obj);
                        }
                    }
                }
                if (lstAddPRL197.Count > 0)
                {
                    db.PRL197.AddRange(lstAddPRL197);
                }

                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL197 = db.PRL197.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL197 = db.PRL197.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL197.Count > 0)
                {
                    db.PRL197.RemoveRange(lstDeletePRL197);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL198> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL198> lstAddPRL198 = new List<PRL198>();
                List<PRL198> lstDeletePRL198 = new List<PRL198>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL198 obj = db.PRL198.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            obj = new PRL198();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActualArcLength = item.ActualArcLength;

                        if (isAdded)
                        {
                            lstAddPRL198.Add(obj);
                        }
                    }
                }
                if (lstAddPRL198.Count > 0)
                {
                    db.PRL198.AddRange(lstAddPRL198);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL198 = db.PRL198.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL198 = db.PRL198.Where(x => x.HeaderId == HeaderId).ToList();
                }

                if (lstDeletePRL198.Count > 0)
                {
                    db.PRL198.RemoveRange(lstDeletePRL198);
                }

                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL199> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL199> lstAddPRL199 = new List<PRL199>();
                List<PRL199> lstDeletePRL199 = new List<PRL199>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL199 obj = db.PRL199.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL199();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.RootGapMin = item.RootGapMin;
                        obj.RootGapMax = item.RootGapMax;
                        obj.RootFaceMin = item.RootFaceMin;
                        obj.RootFaceMax = item.RootFaceMax;
                        obj.Peak_in_out_Top = item.Peak_in_out_Top;
                        obj.Peak_in_out_Mid = item.Peak_in_out_Mid;
                        obj.Peak_in_out_Bottom = item.Peak_in_out_Bottom;
                        obj.OffSetMin = item.OffSetMin;
                        obj.OffSetMax = item.OffSetMax;
                        obj.Inside_Wep_AngleMin = item.Inside_Wep_AngleMin;
                        obj.Inside_Wep_AngleMax = item.Inside_Wep_AngleMax;
                        obj.Outside_Wep_AngleMin = item.Outside_Wep_AngleMin;
                        obj.Outside_Wep_AngleMax = item.Outside_Wep_AngleMax;

                        if (isAdded)
                        {
                            lstAddPRL199.Add(obj);
                        }
                    }
                }
                if (lstAddPRL199.Count > 0)
                {
                    db.PRL199.AddRange(lstAddPRL199);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL199 = db.PRL199.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL199 = db.PRL199.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL199.Count > 0)
                {
                    db.PRL199.RemoveRange(lstDeletePRL199);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6Linkage(List<PRL199_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL199_3> lstAddPRL199_3 = new List<PRL199_3>();
                List<PRL199_3> lstDeletePRL199_3 = new List<PRL199_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL199_3 obj = db.PRL199_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL199_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.MaterialIdentificationReport = item.MaterialIdentificationReport;

                        if (isAdded)
                        {
                            lstAddPRL199_3.Add(obj);
                        }
                    }
                }
                if (lstAddPRL199_3.Count > 0)
                {
                    db.PRL199_3.AddRange(lstAddPRL199_3);
                }

                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL199_3 = db.PRL199_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL199_3 = db.PRL199_3.Where(x => x.HeaderId == HeaderId).ToList();
                }

                if (lstDeletePRL199_3.Count > 0)
                {
                    db.PRL199_3.RemoveRange(lstDeletePRL199_3);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7Linkage(List<PRL199_4> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL199_4> lstAddPRL189_4 = new List<PRL199_4>();
                List<PRL199_4> lstDeletePRL189_4 = new List<PRL199_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL199_4 obj = db.PRL199_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL199_4();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        obj.Description = item.Description;
                        obj.Petal_1 = item.Petal_1;
                        obj.Petal_2 = item.Petal_2;
                        obj.Petal_3 = item.Petal_3;
                        obj.Petal_4 = item.Petal_4;
                        obj.Petal_5 = item.Petal_5;
                        obj.Petal_6 = item.Petal_6;
                        obj.Petal_7 = item.Petal_7;
                        obj.Petal_8 = item.Petal_8;
                        obj.Petal_9 = item.Petal_9;
                        obj.Petal_10 = item.Petal_10;

                        if (isAdded)
                        {
                            lstAddPRL189_4.Add(obj);
                        }
                    }
                }
                if (lstAddPRL189_4.Count > 0)
                {
                    db.PRL199_4.AddRange(lstAddPRL189_4);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL189_4 = db.PRL199_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL189_4 = db.PRL199_4.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL189_4.Count > 0)
                {
                    db.PRL199_4.RemoveRange(lstDeletePRL189_4);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details
        [HttpPost]
        public JsonResult SaveProtocolLine8Linkage(List<PRL199_5> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL199_5> lstAddPRL199_5 = new List<PRL199_5>();
                List<PRL199_5> lstDeletePRL199_5 = new List<PRL199_5>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL199_5 obj = db.PRL199_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL199_5 objFecthSeam = new PRL199_5();
                        if (obj == null)
                        {
                            obj = new PRL199_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL199_5.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = objFecthSeam.Depth;
                            //obj.Width = objFecthSeam.Width;
                            //obj.Offset = objFecthSeam.Offset;
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                            //obj.Depth = item.Depth;
                            //obj.Width = item.Width;
                            //obj.Offset = item.Offset;
                        }

                        obj.Depth = item.Depth;
                        obj.Width = item.Width;
                        obj.Offset = item.Offset;
                        obj.SpotNo = item.SpotNo;
                        obj.SpotNoClad = item.SpotNoClad;
                        if (isAdded)
                        {
                            lstAddPRL199_5.Add(obj);
                        }
                    }
                }
                if (lstAddPRL199_5.Count > 0)
                {
                    db.PRL199_5.AddRange(lstAddPRL199_5);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL199_5 = db.PRL199_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL199_5 = db.PRL199_5.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL199_5.Count > 0)
                {
                    db.PRL199_5.RemoveRange(lstDeletePRL199_5);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line9 details

        [HttpPost]
        public JsonResult SaveProtocolLine9Linkage(List<PRL199_6> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL199_6> lstAddPRL199_6 = new List<PRL199_6>();
                List<PRL199_6> lstDeletePRL199_6 = new List<PRL199_6>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL199_6 obj = db.PRL199_6.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        PRL199_6 objFecthSeam = new PRL199_6();
                        if (obj == null)
                        {
                            obj = new PRL199_6();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        objFecthSeam = db.PRL199_6.Where(x => x.SeamNo == item.SeamNo && x.HeaderId == item.HeaderId).FirstOrDefault();
                        if (objFecthSeam != null)
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }
                        else
                        {
                            obj.SeamNo = !string.IsNullOrEmpty(item.SeamNo) ? Convert.ToString(item.SeamNo).Trim() : "";
                        }

                        obj.CleatNo = item.CleatNo;
                        obj.SizeWelded = item.SizeWelded;
                        obj.Location = item.Location;
                        obj.OutsideOrInside = item.OutsideOrInside;
                        obj.DistanceFromSeam = item.DistanceFromSeam;
                        obj.ShellEvelution = item.ShellEvelution;
                        if (isAdded)
                        {
                            lstAddPRL199_6.Add(obj);
                        }
                    }
                }
                if (lstAddPRL199_6.Count > 0)
                {
                    db.PRL199_6.AddRange(lstAddPRL199_6);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeletePRL199_6 = db.PRL199_6.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else { lstDeletePRL199_6 = db.PRL199_6.Where(x => x.HeaderId == HeaderId).ToList(); }
                if (lstDeletePRL199_6.Count > 0)
                {
                    db.PRL199_6.RemoveRange(lstDeletePRL199_6);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #endregion  
    }
}