﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol006Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol006/";
        string Title = "VISUAL AND DIMENSION INSPECTION REPORT FOR FORMED D'END / CONE";
        // GET: PROTOCOL/Protocol006
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id,int? m)
        {
            PRO025 objPRO025 = new PRO025();
            if (!id.HasValue)
            {
                objPRO025.ProtocolNo = string.Empty;
                objPRO025.CreatedBy = objClsLoginInfo.UserName;
                objPRO025.CreatedOn = DateTime.Now;
                db.PRO025.Add(objPRO025);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO025.HeaderId;
            }
            else
            {
                objPRO025 = db.PRO025.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO025);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO025 PRO025)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO025.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO025 objPRO025 = db.PRO025.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO025.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO025.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO025.ProtocolNo = PRO025.ProtocolNo;
                        objPRO025.ReqOpenEndId = PRO025.ReqOpenEndId;
                        objPRO025.AcceptanceTolerance = PRO025.AcceptanceTolerance;
                        objPRO025.Orientation1 = PRO025.Orientation1;
                        objPRO025.Orientation2 = PRO025.Orientation2;
                        objPRO025.Orientation3 = PRO025.Orientation3;
                        objPRO025.Orientation4 = PRO025.Orientation4;
                        objPRO025.Orientation5 = PRO025.Orientation5;
                        objPRO025.Orientation6 = PRO025.Orientation6;
                        objPRO025.Orientation7 = PRO025.Orientation7;
                        objPRO025.Orientation8 = PRO025.Orientation8;
                        objPRO025.ActOrientation1 = PRO025.ActOrientation1;
                        objPRO025.ActOrientation2 = PRO025.ActOrientation2;
                        objPRO025.ActOrientation3 = PRO025.ActOrientation3;
                        objPRO025.ActOrientation4 = PRO025.ActOrientation4;
                        objPRO025.ActOrientation5 = PRO025.ActOrientation5;
                        objPRO025.ActOrientation6 = PRO025.ActOrientation6;
                        objPRO025.ActOrientation7 = PRO025.ActOrientation7;
                        objPRO025.ActOrientation8 = PRO025.ActOrientation8;
                        objPRO025.Remarks = PRO025.Remarks;
                        objPRO025.Ovality = PRO025.Ovality;
                        objPRO025.ReqTotalHeight = PRO025.ReqTotalHeight;
                        objPRO025.ActTotalHeight0 = PRO025.ActTotalHeight0; ;
                        objPRO025.RemarkHeight0 = PRO025.RemarkHeight0;
                        objPRO025.ActTotalHeight90 = PRO025.ActTotalHeight90;
                        objPRO025.RemarkHeight90 = PRO025.RemarkHeight90;
                        objPRO025.ActTotalHeight180 = PRO025.ActTotalHeight180;
                        objPRO025.RemarkHeight180 = PRO025.RemarkHeight180; ;
                        objPRO025.ActTotalHeight270 = PRO025.ActTotalHeight270;
                        objPRO025.RemarkHeight270 = PRO025.RemarkHeight270;
                        objPRO025.ReqCirTopIs = PRO025.ReqCirTopIs;
                        objPRO025.ActCirTopIs = PRO025.ActCirTopIs;
                        objPRO025.RemarkCirTopIs = PRO025.RemarkCirTopIs;
                        objPRO025.ReqCirTopOs = PRO025.ReqCirTopOs;
                        objPRO025.ActCirTopOs = PRO025.ActCirTopOs;
                        objPRO025.RemarkCirTopOs = PRO025.RemarkCirTopOs;
                        objPRO025.ReqCirBottomIs = PRO025.ReqCirBottomIs;
                        objPRO025.ActCirBottomIs = PRO025.ActCirBottomIs;
                        objPRO025.RemarkCirBottomIs = PRO025.RemarkCirBottomIs;
                        objPRO025.ReqCirBottomOs = PRO025.ReqCirBottomOs;
                        objPRO025.ActCirBottomOs = PRO025.ActCirBottomOs;
                        objPRO025.RemarkCirBottomOs = PRO025.RemarkCirBottomOs;
                        objPRO025.Concentricity = PRO025.Concentricity;
                        objPRO025.StraightFace = PRO025.StraightFace;
                        objPRO025.Act = PRO025.Act;
                        objPRO025.OverCrowing = PRO025.OverCrowing;
                        objPRO025.OverCrowingMax = PRO025.OverCrowingMax;
                        objPRO025.UnderCrowing = PRO025.UnderCrowing;
                        objPRO025.UnderCrowingMax = PRO025.UnderCrowingMax;
                        objPRO025.ReqChordLength = PRO025.ReqChordLength;
                        objPRO025.ActChordLength = PRO025.ActChordLength;
                        objPRO025.ReqRadius = PRO025.ReqRadius;
                        objPRO025.ActRadius = PRO025.ActRadius;
                        objPRO025.ReqGap = PRO025.ReqGap;
                        objPRO025.ActGap = PRO025.ActGap;
                        objPRO025.RemarksTemplateInspection = PRO025.RemarksTemplateInspection;
                        objPRO025.Note1 = PRO025.Note1;
                        objPRO025.Note2 = PRO025.Note2;
                        objPRO025.Note2_1 = PRO025.Note2_1;
                        objPRO025.Note3 = PRO025.Note3;
                        objPRO025.Note4 = PRO025.Note4;
                        objPRO025.Note5 = PRO025.Note5;
                        objPRO025.Result = PRO025.Result;
                        objPRO025.ReqMaxCladStripingWidth = PRO025.ReqMaxCladStripingWidth;
                        objPRO025.ReqMinCladStripingWidth = PRO025.ReqMinCladStripingWidth;
                        objPRO025.CladStripingDepthRequired = PRO025.CladStripingDepthRequired;
                        objPRO025.ProcedureNo = PRO025.ProcedureNo;
                        objPRO025.FNote2 = PRO025.FNote2;
                        objPRO025.FNote3 = PRO025.FNote3;
                        objPRO025.ReqTotalHeight0 = PRO025.ReqTotalHeight0;
                        objPRO025.ReqTotalHeight90 = PRO025.ReqTotalHeight90;
                        objPRO025.ReqTotalHeight180 = PRO025.ReqTotalHeight180;
                        objPRO025.ReqTotalHeight270 = PRO025.ReqTotalHeight270;
                        objPRO025.CladdedVessel = PRO025.CladdedVessel;
                        objPRO025.ProfileMeasurementTemplate = PRO025.ProfileMeasurementTemplate;
                        objPRO025.EditedBy = objClsLoginInfo.UserName;
                        objPRO025.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO025.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO025 objPRO025 = db.PRO025.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO025 != null && string.IsNullOrWhiteSpace(objPRO025.ProtocolNo))
                    {
                        db.PRO026.RemoveRange(objPRO025.PRO026.ToList());
                        db.PRO027.RemoveRange(objPRO025.PRO027.ToList());
                        db.PRO028.RemoveRange(objPRO025.PRO028.ToList());
                        db.PRO029.RemoveRange(objPRO025.PRO029.ToList());
                        db.PRO025.Remove(objPRO025);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_FORMED_D_END_CONE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO025.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.ReqOrien like '%" + param.sSearch
                        + "%' or pro.ReqArcLength like '%" + param.sSearch
                        + "%' or pro.ActArcLength like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL006_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "30","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqOrien", "", "", false, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqArcLength", "", "", false, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ActArcLength", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo), "", true, "", false, "30","QC"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ReqOrien", Convert.ToString(uc.ReqOrien), "", true, "", false, "10","QC"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ReqArcLength", Convert.ToString(uc.ReqArcLength), "", true, "", false, "10","QC"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ActArcLength", Convert.ToString(uc.ActArcLength), "", true, "", false, "10","PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue() ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO026 objPRO026 = new PRO026();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    decimal? decReqOrien = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ReqOrien" + refLineId]));
                    decimal? decReqArcLength = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ReqArcLength" + refLineId]));
                    decimal? decActArcLength = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ActArcLength" + refLineId]));

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO026 = db.PRO026.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO026.EditedBy = objClsLoginInfo.UserName;
                        objPRO026.EditedOn = DateTime.Now;
                    }
                    objPRO026.HeaderId = refHeaderId;
                    objPRO026.SeamNo = strSeamNo;
                    objPRO026.ReqOrien = decReqOrien;
                    objPRO026.ReqArcLength = decReqArcLength;
                    objPRO026.ActArcLength = decActArcLength;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO026.CreatedBy = objClsLoginInfo.UserName;
                        objPRO026.CreatedOn = DateTime.Now;
                        db.PRO026.Add(objPRO026);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO026.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO026 objPRO026 = db.PRO026.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO026 != null)
                {
                    db.PRO026.Remove(objPRO026);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.TopPeakIn like '%" + param.sSearch
                        + "%' or pro.TopPeakOut like '%" + param.sSearch
                        + "%' or pro.MidPeakIn like '%" + param.sSearch
                        + "%' or pro.MidPeakOut like '%" + param.sSearch
                        + "%' or pro.BottomPeakIn like '%" + param.sSearch
                        + "%' or pro.BottomPeakOut like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL006_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo_2_", "", "", false, "", false, "30","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "TopPeakIn", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "TopPeakOut", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "MidPeakIn", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "MidPeakOut", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "BottomPeakIn", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "BottomPeakOut", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks_2_", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {  "2",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId,    "SeamNo_2_", Convert.ToString(uc.SeamNo), "", true, "", false, "30","QC"),
                                Helper.GenerateNumericTextbox(uc.LineId, "TopPeakIn", Convert.ToString(uc.TopPeakIn), "", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "TopPeakOut",Convert.ToString(uc.TopPeakOut), "", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "MidPeakIn", Convert.ToString(uc.MidPeakIn), "", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "MidPeakOut",Convert.ToString(uc.MidPeakOut), "", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId,"BottomPeakIn",Convert.ToString(uc.BottomPeakIn), "", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "BottomPeakOut", Convert.ToString(uc.BottomPeakOut), "", true, "", false, "10","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remarks_2_", Convert.ToString(uc.Remarks), "", true, "", false, "50","PROD"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue() ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2','tblProtocolLines2')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO027 objPRO027 = new PRO027();
          
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo_2_" + refLineId]) ? Convert.ToString(fc["SeamNo_2_" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks_2_" + refLineId]) ? Convert.ToString(fc["Remarks_2_" + refLineId]).Trim() : "";

                    decimal? deTopPeakIn = (new clsManager()).GetDecimalValue(Convert.ToString(fc["TopPeakIn" + refLineId]));
                    decimal? decTopPeakOut = (new clsManager()).GetDecimalValue(Convert.ToString(fc["TopPeakOut" + refLineId]));
                    decimal? decMidPeakIn = (new clsManager()).GetDecimalValue(Convert.ToString(fc["MidPeakIn" + refLineId]));
                    decimal? decMidPeakOut = (new clsManager()).GetDecimalValue(Convert.ToString(fc["MidPeakOut" + refLineId]));
                    decimal? decBottomPeakIn = (new clsManager()).GetDecimalValue(Convert.ToString(fc["BottomPeakIn" + refLineId]));
                    decimal? decBottomPeakOut = (new clsManager()).GetDecimalValue(Convert.ToString(fc["BottomPeakOut" + refLineId]));

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO027 = db.PRO027.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO027.EditedBy = objClsLoginInfo.UserName;
                        objPRO027.EditedOn = DateTime.Now;
                    }
                    objPRO027.HeaderId = refHeaderId;
                    objPRO027.SeamNo = strSeamNo;
                    objPRO027.TopPeakIn = deTopPeakIn;
                    objPRO027.TopPeakOut = decTopPeakOut;
                    objPRO027.MidPeakIn = decMidPeakIn;
                    objPRO027.MidPeakOut = decMidPeakOut;
                    objPRO027.BottomPeakIn = decBottomPeakIn;
                    objPRO027.BottomPeakOut = decBottomPeakOut;
                    objPRO027.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO027.CreatedBy = objClsLoginInfo.UserName;
                        objPRO027.CreatedOn = DateTime.Now;
                        db.PRO027.Add(objPRO027);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO027.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO027 objPRO027 = db.PRO027.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO027 != null)
                {
                    db.PRO027.Remove(objPRO027);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 Width details
        [HttpPost]
        public ActionResult GetProtocolLines3WidthData(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SpotNo like '%" + param.sSearch
                        + "%' or pro.Width like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL006_GET_LINES3_WIDTH_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "3",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNoWidth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Width", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RemarksWidth", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine3(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                            "3",
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.LineId),
                            Helper.GenerateHTMLTextbox(uc.LineId, "SpotNoWidth", Convert.ToString(uc.SpotNo), "", true, "", false, "10","PROD"),
                            Helper.GenerateNumericTextbox(uc.LineId, "Width",    Convert.ToString(uc.Width), "", true, "", false, "10","PROD"),
                            Helper.GenerateHTMLTextbox(uc.LineId, "RemarksWidth",Convert.ToString(uc.Remarks), "", true, "", false, "100","PROD"),
                            Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue() ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine3Width','tblProtocolLines3Width')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine3Width(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO028 objPRO028 = new PRO028();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strSpotNo = !string.IsNullOrEmpty(fc["SpotNoWidth" + refLineId]) ? Convert.ToString(fc["SpotNoWidth" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["RemarksWidth" + refLineId]) ? Convert.ToString(fc["RemarksWidth" + refLineId]).Trim() : "";
                    decimal? decWidth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Width" + refLineId]));

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO028 = db.PRO028.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO028.EditedBy = objClsLoginInfo.UserName;
                        objPRO028.EditedOn = DateTime.Now;
                    }
                    objPRO028.HeaderId = refHeaderId;
                    objPRO028.SpotNo = strSpotNo;
                    objPRO028.Remarks = strRemarks;
                    objPRO028.Width = decWidth;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRO028.CreatedBy = objClsLoginInfo.UserName;
                        objPRO028.CreatedOn = DateTime.Now;
                        db.PRO028.Add(objPRO028);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO028.LineId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine3Width(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO028 objPRO028 = db.PRO028.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO028 != null)
                {
                    db.PRO028.Remove(objPRO028);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 Depth details
        [HttpPost]
        public ActionResult GetProtocolLines3DepthData(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SpotNo like '%" + param.sSearch
                        + "%' or pro.Depth like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL006_GET_LINES3_DEPTH_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "4",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNoDepth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Depth", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RemarksDepth", "", "", false, "", false, "100","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine4(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {"4",
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.LineId),
                            Helper.GenerateHTMLTextbox(uc.LineId, "SpotNoDepth", Convert.ToString(uc.SpotNo),"", true, "", false, "10","PROD"),
                            Helper.GenerateNumericTextbox(uc.LineId, "Depth",    Convert.ToString(uc.Depth), "", true, "", false, "10","PROD"),
                            Helper.GenerateHTMLTextbox(uc.LineId, "RemarksDepth",Convert.ToString(uc.Remarks), "", true, "", false, "100","PROD"),
                            Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue() ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine3Depth','tblProtocolLines3Depth')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine3Depth(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO029 objPRO029 = new PRO029();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strSpotNo = !string.IsNullOrEmpty(fc["SpotNoDepth" + refLineId]) ? Convert.ToString(fc["SpotNoDepth" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["RemarksDepth" + refLineId]) ? Convert.ToString(fc["RemarksDepth" + refLineId]).Trim() : "";
                    decimal? decDepth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Depth" + refLineId]));

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO029 = db.PRO029.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO029.EditedBy = objClsLoginInfo.UserName;
                        objPRO029.EditedOn = DateTime.Now;
                    }
                    objPRO029.HeaderId = refHeaderId;
                    objPRO029.SpotNo = strSpotNo;
                    objPRO029.Remarks = strRemarks;
                    objPRO029.Depth = decDepth;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRO029.CreatedBy = objClsLoginInfo.UserName;
                        objPRO029.CreatedOn = DateTime.Now;
                        db.PRO029.Add(objPRO029);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO029.LineId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine3Depth(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO029 objPRO029 = db.PRO029.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO029 != null)
                {
                    db.PRO029.Remove(objPRO029);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Linkage View Code
        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL025 objPRL025 = new PRL025();
            objPRL025 = db.PRL025.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (objPRL025 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL025.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL025);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL025 PRL025)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL025.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL025 objPRL025 = db.PRL025.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL025.ProtocolNo = PRL025.ProtocolNo != null ? PRL025.ProtocolNo : "";
                    objPRL025.DCRNo = PRL025.DCRNo;
                    objPRL025.ReqOpenEndId = PRL025.ReqOpenEndId;
                    objPRL025.AcceptanceTolerance = PRL025.AcceptanceTolerance;
                    objPRL025.Orientation1 = PRL025.Orientation1;
                    objPRL025.Orientation2 = PRL025.Orientation2;
                    objPRL025.Orientation3 = PRL025.Orientation3;
                    objPRL025.Orientation4 = PRL025.Orientation4;
                    objPRL025.Orientation5 = PRL025.Orientation5;
                    objPRL025.Orientation6 = PRL025.Orientation6;
                    objPRL025.Orientation7 = PRL025.Orientation7;
                    objPRL025.Orientation8 = PRL025.Orientation8;
                    objPRL025.ActOrientation1 = PRL025.ActOrientation1;
                    objPRL025.ActOrientation2 = PRL025.ActOrientation2;
                    objPRL025.ActOrientation3 = PRL025.ActOrientation3;
                    objPRL025.ActOrientation4 = PRL025.ActOrientation4;
                    objPRL025.ActOrientation5 = PRL025.ActOrientation5;
                    objPRL025.ActOrientation6 = PRL025.ActOrientation6;
                    objPRL025.ActOrientation7 = PRL025.ActOrientation7;
                    objPRL025.ActOrientation8 = PRL025.ActOrientation8;
                    objPRL025.Remarks = PRL025.Remarks;
                    objPRL025.Ovality = PRL025.Ovality;
                    objPRL025.ReqTotalHeight = PRL025.ReqTotalHeight;
                    objPRL025.ActTotalHeight0 = PRL025.ActTotalHeight0; ;
                    objPRL025.RemarkHeight0 = PRL025.RemarkHeight0;
                    objPRL025.ActTotalHeight90 = PRL025.ActTotalHeight90;
                    objPRL025.RemarkHeight90 = PRL025.RemarkHeight90;
                    objPRL025.ActTotalHeight180 = PRL025.ActTotalHeight180;
                    objPRL025.RemarkHeight180 = PRL025.RemarkHeight180; ;
                    objPRL025.ActTotalHeight270 = PRL025.ActTotalHeight270;
                    objPRL025.RemarkHeight270 = PRL025.RemarkHeight270;
                    objPRL025.ReqCirTopIs = PRL025.ReqCirTopIs;
                    objPRL025.ActCirTopIs = PRL025.ActCirTopIs;
                    objPRL025.RemarkCirTopIs = PRL025.RemarkCirTopIs;
                    objPRL025.ReqCirTopOs = PRL025.ReqCirTopOs;
                    objPRL025.ActCirTopOs = PRL025.ActCirTopOs;
                    objPRL025.RemarkCirTopOs = PRL025.RemarkCirTopOs;
                    objPRL025.ReqCirBottomIs = PRL025.ReqCirBottomIs;
                    objPRL025.ActCirBottomIs = PRL025.ActCirBottomIs;
                    objPRL025.RemarkCirBottomIs = PRL025.RemarkCirBottomIs;
                    objPRL025.ReqCirBottomOs = PRL025.ReqCirBottomOs;
                    objPRL025.ActCirBottomOs = PRL025.ActCirBottomOs;
                    objPRL025.RemarkCirBottomOs = PRL025.RemarkCirBottomOs;
                    objPRL025.Concentricity = PRL025.Concentricity;
                    objPRL025.StraightFace = PRL025.StraightFace;
                    objPRL025.Act = PRL025.Act;
                    objPRL025.OverCrowing = PRL025.OverCrowing;
                    objPRL025.OverCrowingMax = PRL025.OverCrowingMax;
                    objPRL025.UnderCrowing = PRL025.UnderCrowing;
                    objPRL025.UnderCrowingMax = PRL025.UnderCrowingMax;
                    objPRL025.ReqChordLength = PRL025.ReqChordLength;
                    objPRL025.ActChordLength = PRL025.ActChordLength;
                    objPRL025.ReqRadius = PRL025.ReqRadius;
                    objPRL025.ActRadius = PRL025.ActRadius;
                    objPRL025.ReqGap = PRL025.ReqGap;
                    objPRL025.ActGap = PRL025.ActGap;
                    objPRL025.RemarksTemplateInspection = PRL025.RemarksTemplateInspection;
                    objPRL025.Note1 = PRL025.Note1;
                    objPRL025.Note2 = PRL025.Note2;
                    objPRL025.Note2_1 = PRL025.Note2_1;
                    objPRL025.Note3 = PRL025.Note3;
                    objPRL025.Note4 = PRL025.Note4;
                    objPRL025.Note5 = PRL025.Note5;
                    objPRL025.Result = PRL025.Result;
                    objPRL025.ReqMaxCladStripingWidth = PRL025.ReqMaxCladStripingWidth;
                    objPRL025.ReqMinCladStripingWidth = PRL025.ReqMinCladStripingWidth;
                    objPRL025.CladStripingDepthRequired = PRL025.CladStripingDepthRequired;
                    objPRL025.ProcedureNo = PRL025.ProcedureNo;
                    objPRL025.FNote2 = PRL025.FNote2;
                    objPRL025.FNote3 = PRL025.FNote3;
                    objPRL025.ReqTotalHeight0 = PRL025.ReqTotalHeight0;
                    objPRL025.ReqTotalHeight90 = PRL025.ReqTotalHeight90;
                    objPRL025.ReqTotalHeight180 = PRL025.ReqTotalHeight180;
                    objPRL025.ReqTotalHeight270 = PRL025.ReqTotalHeight270;
                    objPRL025.CladdedVessel = PRL025.CladdedVessel;
                    objPRL025.ProfileMeasurementTemplate = PRL025.ProfileMeasurementTemplate;
                    objPRL025.EditedBy = objClsLoginInfo.UserName;
                    objPRL025.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL025.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL025 objPRL025 = db.PRL025.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL025 != null && string.IsNullOrWhiteSpace(objPRL025.ProtocolNo))
                    {
                        db.PRL026.RemoveRange(objPRL025.PRL026.ToList());
                        db.PRL027.RemoveRange(objPRL025.PRL027.ToList());
                        db.PRL028.RemoveRange(objPRL025.PRL028.ToList());
                        db.PRL029.RemoveRange(objPRL025.PRL029.ToList());
                        db.PRL025.Remove(objPRL025);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL025.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.ReqOrien like '%" + param.sSearch
                        + "%' or pro.ReqArcLength like '%" + param.sSearch
                        + "%' or pro.ActArcLength like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL006_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "30","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqOrien", "", "", false, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqArcLength", "", "", false, "", false, "10","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ActArcLength", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SeamNo", Convert.ToString(uc.SeamNo), "", true, "", false, "30","QC"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ReqOrien", Convert.ToString(uc.ReqOrien), "", true, "", false, "10","QC"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ReqArcLength", Convert.ToString(uc.ReqArcLength), "", true, "", false, "10","QC"),
                               Helper.GenerateNumericTextbox(uc.LineId, "ActArcLength", Convert.ToString(uc.ActArcLength), "", true, "", false, "10","PROD"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue() ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL026 objPRL026 = new PRL026();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : "";
                    decimal? decReqOrien = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ReqOrien" + refLineId]));
                    decimal? decReqArcLength = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ReqArcLength" + refLineId]));
                    decimal? decActArcLength = (new clsManager()).GetDecimalValue(Convert.ToString(fc["ActArcLength" + refLineId]));

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL026 = db.PRL026.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL026.EditedBy = objClsLoginInfo.UserName;
                        objPRL026.EditedOn = DateTime.Now;
                    }
                    objPRL026.HeaderId = refHeaderId;
                    objPRL026.SeamNo = strSeamNo;
                    objPRL026.ReqOrien = decReqOrien;
                    objPRL026.ReqArcLength = decReqArcLength;
                    objPRL026.ActArcLength = decActArcLength;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL026.CreatedBy = objClsLoginInfo.UserName;
                        objPRL026.CreatedOn = DateTime.Now;
                        db.PRL026.Add(objPRL026);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL026.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL026 objPRL026 = db.PRL026.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL026 != null)
                {
                    db.PRL026.Remove(objPRL026);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SeamNo like '%" + param.sSearch
                        + "%' or pro.TopPeakIn like '%" + param.sSearch
                        + "%' or pro.TopPeakOut like '%" + param.sSearch
                        + "%' or pro.MidPeakIn like '%" + param.sSearch
                        + "%' or pro.MidPeakOut like '%" + param.sSearch
                        + "%' or pro.BottomPeakIn like '%" + param.sSearch
                        + "%' or pro.BottomPeakOut like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL006_LINKAGE_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo_2_", "", "", false, "", false, "30","QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "TopPeakIn", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "TopPeakOut", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "MidPeakIn", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "MidPeakOut", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "BottomPeakIn", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "BottomPeakOut", "", "", false, "", false, "10","PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks_2_", "", "", false, "", false, "50","PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {"2",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId,    "SeamNo_2_", Convert.ToString(uc.SeamNo), "", true, "", false, "30","QC"),
                                Helper.GenerateNumericTextbox(uc.LineId, "TopPeakIn", Convert.ToString(uc.TopPeakIn), "", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "TopPeakOut",Convert.ToString(uc.TopPeakOut), "", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "MidPeakIn", Convert.ToString(uc.MidPeakIn), "", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "MidPeakOut",Convert.ToString(uc.MidPeakOut), "", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId,"BottomPeakIn",Convert.ToString(uc.BottomPeakIn), "", true, "", false, "10","PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "BottomPeakOut", Convert.ToString(uc.BottomPeakOut), "", true, "", false, "10","PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remarks_2_", Convert.ToString(uc.Remarks), "", true, "", false, "50","PROD"),
                                Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue() ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2Linkage','tblProtocolLines2')"):"")

                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL027 objPRL027 = new PRL027();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                if (refHeaderId > 0)
                {
                    string strSeamNo = !string.IsNullOrEmpty(fc["SeamNo_2_" + refLineId]) ? Convert.ToString(fc["SeamNo_2_" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks_2_" + refLineId]) ? Convert.ToString(fc["Remarks_2_" + refLineId]).Trim() : "";

                    decimal? deTopPeakIn = (new clsManager()).GetDecimalValue(Convert.ToString(fc["TopPeakIn" + refLineId]));
                    decimal? decTopPeakOut = (new clsManager()).GetDecimalValue(Convert.ToString(fc["TopPeakOut" + refLineId]));
                    decimal? decMidPeakIn = (new clsManager()).GetDecimalValue(Convert.ToString(fc["MidPeakIn" + refLineId]));
                    decimal? decMidPeakOut = (new clsManager()).GetDecimalValue(Convert.ToString(fc["MidPeakOut" + refLineId]));
                    decimal? decBottomPeakIn = (new clsManager()).GetDecimalValue(Convert.ToString(fc["BottomPeakIn" + refLineId]));
                    decimal? decBottomPeakOut = (new clsManager()).GetDecimalValue(Convert.ToString(fc["BottomPeakOut" + refLineId]));


                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL027 = db.PRL027.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL027.EditedBy = objClsLoginInfo.UserName;
                        objPRL027.EditedOn = DateTime.Now;
                    }
                    objPRL027.HeaderId = refHeaderId;
                    objPRL027.SeamNo = strSeamNo;
                    objPRL027.TopPeakIn = deTopPeakIn;
                    objPRL027.TopPeakOut = decTopPeakOut;
                    objPRL027.MidPeakIn = decMidPeakIn;
                    objPRL027.MidPeakOut = decMidPeakOut;
                    objPRL027.BottomPeakIn = decBottomPeakIn;
                    objPRL027.BottomPeakOut = decBottomPeakOut;
                    objPRL027.Remarks = strRemarks;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL027.CreatedBy = objClsLoginInfo.UserName;
                        objPRL027.CreatedOn = DateTime.Now;
                        db.PRL027.Add(objPRL027);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                   
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL027.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL027 objPRL027 = db.PRL027.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL027 != null)
                {
                    db.PRL027.Remove(objPRL027);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 Width details
        [HttpPost]
        public ActionResult GetProtocolLines3WidthDataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SpotNo like '%" + param.sSearch
                        + "%' or pro.Width like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL006_LINKAGE_GET_LINES3_WIDTH_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "3",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNoWidth", "", "", false, "", false, "10"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Width", "", "", false, "", false, "10"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RemarksWidth", "", "", false, "", false, "100"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine3(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                   "3",
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.LineId),
                            Helper.GenerateHTMLTextbox(uc.LineId, "SpotNoWidth", Convert.ToString(uc.SpotNo), "", true, "", false, "10","PROD"),
                            Helper.GenerateNumericTextbox(uc.LineId, "Width",    Convert.ToString(uc.Width), "", true, "", false, "10","PROD"),
                            Helper.GenerateHTMLTextbox(uc.LineId, "RemarksWidth",Convert.ToString(uc.Remarks), "", true, "", false, "100","PROD"),
                            Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue() ?  Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine3WidthLinkage','tblProtocolLines3Width')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine3WidthLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL028 objPRL028 = new PRL028();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strSpotNo = !string.IsNullOrEmpty(fc["SpotNoWidth" + refLineId]) ? Convert.ToString(fc["SpotNoWidth" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["RemarksWidth" + refLineId]) ? Convert.ToString(fc["RemarksWidth" + refLineId]).Trim() : "";
                    decimal? decWidth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Width" + refLineId]));

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL028 = db.PRL028.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL028.EditedBy = objClsLoginInfo.UserName;
                        objPRL028.EditedOn = DateTime.Now;
                    }
                    objPRL028.HeaderId = refHeaderId;
                    objPRL028.SpotNo = strSpotNo;
                    objPRL028.Remarks = strRemarks;
                    objPRL028.Width = decWidth;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRL028.CreatedBy = objClsLoginInfo.UserName;
                        objPRL028.CreatedOn = DateTime.Now;
                        db.PRL028.Add(objPRL028);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL028.LineId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine3WidthLinkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL028 objPRL028 = db.PRL028.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL028 != null)
                {
                    db.PRL028.Remove(objPRL028);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 Depth details
        [HttpPost]
        public ActionResult GetProtocolLines3DepthDataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.SpotNo like '%" + param.sSearch
                        + "%' or pro.Depth like '%" + param.sSearch
                        + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL006_LINKAGE_GET_LINES3_DEPTH_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "4",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNoDepth", "", "", false, "", false, "10"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Depth", "", "", false, "", false, "10"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "RemarksDepth", "", "", false, "", false, "100"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine4(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                            "4",
                            Convert.ToString(uc.HeaderId),
                            Convert.ToString(uc.LineId),
                            Helper.GenerateHTMLTextbox(uc.LineId, "SpotNoDepth", Convert.ToString(uc.SpotNo),"", true, "", false, "10","PROD"),
                            Helper.GenerateNumericTextbox(uc.LineId, "Depth",    Convert.ToString(uc.Depth), "", true, "", false, "10","PROD"),
                            Helper.GenerateHTMLTextbox(uc.LineId, "RemarksDepth",Convert.ToString(uc.Remarks), "", true, "", false, "100","PROD"),
                            Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue() ?Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine3DepthLinkage','tblProtocolLines3Depth')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine3DepthLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL029 objPRL029 = new PRL029();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strSpotNo = !string.IsNullOrEmpty(fc["SpotNoDepth" + refLineId]) ? Convert.ToString(fc["SpotNoDepth" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["RemarksDepth" + refLineId]) ? Convert.ToString(fc["RemarksDepth" + refLineId]).Trim() : "";
                    decimal? decDepth = (new clsManager()).GetDecimalValue(Convert.ToString(fc["Depth" + refLineId]));

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL029 = db.PRL029.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL029.EditedBy = objClsLoginInfo.UserName;
                        objPRL029.EditedOn = DateTime.Now;
                    }
                    objPRL029.HeaderId = refHeaderId;
                    objPRL029.SpotNo = strSpotNo;
                    objPRL029.Remarks = strRemarks;
                    objPRL029.Depth = decDepth;
                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotUpdate;
                    }
                    else
                    {
                        objPRL029.CreatedBy = objClsLoginInfo.UserName;
                        objPRL029.CreatedOn = DateTime.Now;
                        db.PRL029.Add(objPRL029);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.SpotAdded;
                    }
                 
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL029.LineId;
                    #endregion
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine3DepthLinkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL029 objPRL029 = db.PRL029.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL029 != null)
                {
                    db.PRL029.Remove(objPRL029);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
    }
}