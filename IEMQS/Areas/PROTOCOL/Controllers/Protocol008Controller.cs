﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol008Controller : clsBase
    {
        // GET: PROTOCOL/Protocol008
        #region Protocol Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id, int? m)
        {//PRO035 
            PRO035 objPRO035 = new PRO035();
        

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "NUB DIMENSION INSPECTION REPORT FOR SQUARE TYPE";
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            if (!id.HasValue)
            {
                objPRO035.ProtocolNo = string.Empty;
                objPRO035.CreatedBy = objClsLoginInfo.UserName;
                objPRO035.CreatedOn = DateTime.Now;
                db.PRO035.Add(objPRO035);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO035.HeaderId;
            }
            else
            {
                objPRO035 = db.PRO035.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            return View(objPRO035);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO035 obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = obj.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO035 objPRO035 = db.PRO035.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var lstPRO035 = db.PRO035.Where(o => o.HeaderId != refHeaderId && o.ProtocolNo.Trim() == obj.ProtocolNo.Trim()).ToList();
                    if (lstPRO035.Count == 0)
                    {
                        #region Save Data
                        objPRO035.ProtocolNo = obj.ProtocolNo;
                        objPRO035.AfterOverlay = obj.AfterOverlay;
                        objPRO035.NubWidthReq = obj.NubWidthReq;
                        objPRO035.NubHeightReq = obj.NubHeightReq;
                        objPRO035.NubWidthTol = obj.NubWidthTol;
                        objPRO035.NubHeightTol = obj.NubHeightTol;
                        objPRO035.AfterOverlayA1 = obj.AfterOverlayA1;
                        objPRO035.AfterOverlayA2 = obj.AfterOverlayA2;
                        objPRO035.AfterOverlayB1 = obj.AfterOverlayB1;
                        objPRO035.AfterOverlayB2 = obj.AfterOverlayB2;
                        objPRO035.AfterOverlayC1 = obj.AfterOverlayC1;
                        objPRO035.AfterOverlayC2 = obj.AfterOverlayC2;
                        objPRO035.NubWidth0 = obj.NubWidth0;
                        objPRO035.NubHeight0 = obj.NubHeight0;
                        objPRO035.AfterOverlayA10 = obj.AfterOverlayA10;
                        objPRO035.AfterOverlayA20 = obj.AfterOverlayA20;
                        objPRO035.AfterOverlayB10 = obj.AfterOverlayB10;
                        objPRO035.AfterOverlayB20 = obj.AfterOverlayB20;
                        objPRO035.AfterOverlayC10 = obj.AfterOverlayC10;
                        objPRO035.AfterOverlayC20 = obj.AfterOverlayC20;
                        objPRO035.NubWidth30 = obj.NubWidth30;
                        objPRO035.NubHeight30 = obj.NubHeight30;
                        objPRO035.AfterOverlayA130 = obj.AfterOverlayA130;
                        objPRO035.AfterOverlayA230 = obj.AfterOverlayA230;
                        objPRO035.AfterOverlayB130 = obj.AfterOverlayB130;
                        objPRO035.AfterOverlayB230 = obj.AfterOverlayB230;
                        objPRO035.AfterOverlayC130 = obj.AfterOverlayC130;
                        objPRO035.AfterOverlayC230 = obj.AfterOverlayC230;
                        objPRO035.NubWidth60 = obj.NubWidth60;
                        objPRO035.NubHeight60 = obj.NubHeight60;
                        objPRO035.AfterOverlayA160 = obj.AfterOverlayA160;
                        objPRO035.AfterOverlayA260 = obj.AfterOverlayA260;
                        objPRO035.AfterOverlayB160 = obj.AfterOverlayB160;
                        objPRO035.AfterOverlayB260 = obj.AfterOverlayB260;
                        objPRO035.AfterOverlayC160 = obj.AfterOverlayC160;
                        objPRO035.AfterOverlayC260 = obj.AfterOverlayC260;
                        objPRO035.NubWidth90 = obj.NubWidth90;
                        objPRO035.NubHeight90 = obj.NubHeight90;
                        objPRO035.AfterOverlayA190 = obj.AfterOverlayA190;
                        objPRO035.AfterOverlayA290 = obj.AfterOverlayA290;
                        objPRO035.AfterOverlayB190 = obj.AfterOverlayB190;
                        objPRO035.AfterOverlayB290 = obj.AfterOverlayB290;
                        objPRO035.AfterOverlayC190 = obj.AfterOverlayC190;
                        objPRO035.AfterOverlayC290 = obj.AfterOverlayC290;
                        objPRO035.NubWidth120 = obj.NubWidth120;
                        objPRO035.NubHeight120 = obj.NubHeight120;
                        objPRO035.AfterOverlayA1120 = obj.AfterOverlayA1120;
                        objPRO035.AfterOverlayA2120 = obj.AfterOverlayA2120;
                        objPRO035.AfterOverlayB1120 = obj.AfterOverlayB1120;
                        objPRO035.AfterOverlayB2120 = obj.AfterOverlayB2120;
                        objPRO035.AfterOverlayC1120 = obj.AfterOverlayC1120;
                        objPRO035.AfterOverlayC2120 = obj.AfterOverlayC2120;
                        objPRO035.NubWidth150 = obj.NubWidth150;
                        objPRO035.NubHeight150 = obj.NubHeight150;
                        objPRO035.AfterOverlayA1150 = obj.AfterOverlayA1150;
                        objPRO035.AfterOverlayA2150 = obj.AfterOverlayA2150;
                        objPRO035.AfterOverlayB1150 = obj.AfterOverlayB1150;
                        objPRO035.AfterOverlayB2150 = obj.AfterOverlayB2150;
                        objPRO035.AfterOverlayC1150 = obj.AfterOverlayC1150;
                        objPRO035.AfterOverlayC2150 = obj.AfterOverlayC2150;
                        objPRO035.NubWidth180 = obj.NubWidth180;
                        objPRO035.NubHeight180 = obj.NubHeight180;
                        objPRO035.AfterOverlayA1180 = obj.AfterOverlayA1180;
                        objPRO035.AfterOverlayA2180 = obj.AfterOverlayA2180;
                        objPRO035.AfterOverlayB1180 = obj.AfterOverlayB1180;
                        objPRO035.AfterOverlayB2180 = obj.AfterOverlayB2180;
                        objPRO035.AfterOverlayC1180 = obj.AfterOverlayC1180;
                        objPRO035.AfterOverlayC2180 = obj.AfterOverlayC2180;
                        objPRO035.NubWidth210 = obj.NubWidth210;
                        objPRO035.NubHeight210 = obj.NubHeight210;
                        objPRO035.AfterOverlayA1210 = obj.AfterOverlayA1210;
                        objPRO035.AfterOverlayA2210 = obj.AfterOverlayA2210;
                        objPRO035.AfterOverlayB1210 = obj.AfterOverlayB1210;
                        objPRO035.AfterOverlayB2210 = obj.AfterOverlayB2210;
                        objPRO035.AfterOverlayC1210 = obj.AfterOverlayC1210;
                        objPRO035.AfterOverlayC2210 = obj.AfterOverlayC2210;
                        objPRO035.NubWidth240 = obj.NubWidth240;
                        objPRO035.NubHeight240 = obj.NubHeight240;
                        objPRO035.AfterOverlayA1240 = obj.AfterOverlayA1240;
                        objPRO035.AfterOverlayA2240 = obj.AfterOverlayA2240;
                        objPRO035.AfterOverlayB1240 = obj.AfterOverlayB1240;
                        objPRO035.AfterOverlayB2240 = obj.AfterOverlayB2240;
                        objPRO035.AfterOverlayC1240 = obj.AfterOverlayC1240;
                        objPRO035.AfterOverlayC2240 = obj.AfterOverlayC2240;
                        objPRO035.NubWidth270 = obj.NubWidth270;
                        objPRO035.NubHeight270 = obj.NubHeight270;
                        objPRO035.AfterOverlayA1270 = obj.AfterOverlayA1270;
                        objPRO035.AfterOverlayA2270 = obj.AfterOverlayA2270;
                        objPRO035.AfterOverlayB1270 = obj.AfterOverlayB1270;
                        objPRO035.AfterOverlayB2270 = obj.AfterOverlayB2270;
                        objPRO035.AfterOverlayC1270 = obj.AfterOverlayC1270;
                        objPRO035.AfterOverlayC2270 = obj.AfterOverlayC2270;
                        objPRO035.NubWidth300 = obj.NubWidth300;
                        objPRO035.NubHeight300 = obj.NubHeight300;
                        objPRO035.AfterOverlayA1300 = obj.AfterOverlayA1300;
                        objPRO035.AfterOverlayA2300 = obj.AfterOverlayA2300;
                        objPRO035.AfterOverlayB1300 = obj.AfterOverlayB1300;
                        objPRO035.AfterOverlayB2300 = obj.AfterOverlayB2300;
                        objPRO035.AfterOverlayC1300 = obj.AfterOverlayC1300;
                        objPRO035.AfterOverlayC2300 = obj.AfterOverlayC2300;
                        objPRO035.NubWidth330 = obj.NubWidth330;
                        objPRO035.NubHeight330 = obj.NubHeight330;
                        objPRO035.AfterOverlayA1330 = obj.AfterOverlayA1330;
                        objPRO035.AfterOverlayA2330 = obj.AfterOverlayA2330;
                        objPRO035.AfterOverlayB1330 = obj.AfterOverlayB1330;
                        objPRO035.AfterOverlayB2330 = obj.AfterOverlayB2330;
                        objPRO035.AfterOverlayC1330 = obj.AfterOverlayC1330;
                        objPRO035.AfterOverlayC2330 = obj.AfterOverlayC2330;
                        objPRO035.Remark1 = obj.Remark1;
                        objPRO035.Remark2 = obj.Remark2;
                        objPRO035.Remark3 = obj.Remark3;
                        objPRO035.Remark4 = obj.Remark4;
                        objPRO035.Remark5 = obj.Remark5;
                        objPRO035.Remark5_1 = obj.Remark5_1.HasValue ? Manager.getDateTime(obj.Remark5_1.Value.ToShortDateString()) : obj.Remark5_1;
                        objPRO035.Remark6 = obj.Remark6;
                        objPRO035.Remark7_1 = obj.Remark7_1;
                        objPRO035.Remark7_2 = obj.Remark7_2;
                        objPRO035.EditedBy = objClsLoginInfo.UserName;
                        objPRO035.EditedOn = DateTime.Now;
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolSaved;
                        objResponseMsg.HeaderId = objPRO035.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolExists;
                    }

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId > 0)
                {
                    PRO035 objPRO035 = db.PRO035.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO035 != null && string.IsNullOrWhiteSpace(objPRO035.ProtocolNo))
                    {
                        db.PRO035.Remove(objPRO035);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORT_FOR_SQUARE_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO035.HeaderId;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Linkage Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Linkage(int? id, int? m)
        {//PRL035 
            PRL035 objPRL035 = new PRL035();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            objPRL035 = db.PRL035.Where(i => i.HeaderId == id).FirstOrDefault();
            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "NUB DIMENSION INSPECTION REPORT FOR SQUARE TYPE";
            ViewBag.HeaderId = id.Value;
            if (objPRL035 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL035.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
         
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
                ViewBag.HeaderId = objPRL035.HeaderId;
            }

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRL035);
        }

        [HttpPost]
        public ActionResult SaveLinkageHeaderData(PRL035 obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = obj.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL035 objPRL035 = db.PRL035.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data

                    objPRL035.DCRNo = obj.DCRNo;
                    objPRL035.ProtocolNo = obj.ProtocolNo != null ? obj.ProtocolNo : "";
                    objPRL035.AfterOverlay = obj.AfterOverlay;
                    objPRL035.NubWidthReq = obj.NubWidthReq;
                    objPRL035.NubHeightReq = obj.NubHeightReq;
                    objPRL035.NubWidthTol = obj.NubWidthTol;
                    objPRL035.NubHeightTol = obj.NubHeightTol;
                    objPRL035.AfterOverlayA1 = obj.AfterOverlayA1;
                    objPRL035.AfterOverlayA2 = obj.AfterOverlayA2;
                    objPRL035.AfterOverlayB1 = obj.AfterOverlayB1;
                    objPRL035.AfterOverlayB2 = obj.AfterOverlayB2;
                    objPRL035.AfterOverlayC1 = obj.AfterOverlayC1;
                    objPRL035.AfterOverlayC2 = obj.AfterOverlayC2;
                    objPRL035.NubWidth0 = obj.NubWidth0;
                    objPRL035.NubHeight0 = obj.NubHeight0;
                    objPRL035.AfterOverlayA10 = obj.AfterOverlayA10;
                    objPRL035.AfterOverlayA20 = obj.AfterOverlayA20;
                    objPRL035.AfterOverlayB10 = obj.AfterOverlayB10;
                    objPRL035.AfterOverlayB20 = obj.AfterOverlayB20;
                    objPRL035.AfterOverlayC10 = obj.AfterOverlayC10;
                    objPRL035.AfterOverlayC20 = obj.AfterOverlayC20;
                    objPRL035.NubWidth30 = obj.NubWidth30;
                    objPRL035.NubHeight30 = obj.NubHeight30;
                    objPRL035.AfterOverlayA130 = obj.AfterOverlayA130;
                    objPRL035.AfterOverlayA230 = obj.AfterOverlayA230;
                    objPRL035.AfterOverlayB130 = obj.AfterOverlayB130;
                    objPRL035.AfterOverlayB230 = obj.AfterOverlayB230;
                    objPRL035.AfterOverlayC130 = obj.AfterOverlayC130;
                    objPRL035.AfterOverlayC230 = obj.AfterOverlayC230;
                    objPRL035.NubWidth60 = obj.NubWidth60;
                    objPRL035.NubHeight60 = obj.NubHeight60;
                    objPRL035.AfterOverlayA160 = obj.AfterOverlayA160;
                    objPRL035.AfterOverlayA260 = obj.AfterOverlayA260;
                    objPRL035.AfterOverlayB160 = obj.AfterOverlayB160;
                    objPRL035.AfterOverlayB260 = obj.AfterOverlayB260;
                    objPRL035.AfterOverlayC160 = obj.AfterOverlayC160;
                    objPRL035.AfterOverlayC260 = obj.AfterOverlayC260;
                    objPRL035.NubWidth90 = obj.NubWidth90;
                    objPRL035.NubHeight90 = obj.NubHeight90;
                    objPRL035.AfterOverlayA190 = obj.AfterOverlayA190;
                    objPRL035.AfterOverlayA290 = obj.AfterOverlayA290;
                    objPRL035.AfterOverlayB190 = obj.AfterOverlayB190;
                    objPRL035.AfterOverlayB290 = obj.AfterOverlayB290;
                    objPRL035.AfterOverlayC190 = obj.AfterOverlayC190;
                    objPRL035.AfterOverlayC290 = obj.AfterOverlayC290;
                    objPRL035.NubWidth120 = obj.NubWidth120;
                    objPRL035.NubHeight120 = obj.NubHeight120;
                    objPRL035.AfterOverlayA1120 = obj.AfterOverlayA1120;
                    objPRL035.AfterOverlayA2120 = obj.AfterOverlayA2120;
                    objPRL035.AfterOverlayB1120 = obj.AfterOverlayB1120;
                    objPRL035.AfterOverlayB2120 = obj.AfterOverlayB2120;
                    objPRL035.AfterOverlayC1120 = obj.AfterOverlayC1120;
                    objPRL035.AfterOverlayC2120 = obj.AfterOverlayC2120;
                    objPRL035.NubWidth150 = obj.NubWidth150;
                    objPRL035.NubHeight150 = obj.NubHeight150;
                    objPRL035.AfterOverlayA1150 = obj.AfterOverlayA1150;
                    objPRL035.AfterOverlayA2150 = obj.AfterOverlayA2150;
                    objPRL035.AfterOverlayB1150 = obj.AfterOverlayB1150;
                    objPRL035.AfterOverlayB2150 = obj.AfterOverlayB2150;
                    objPRL035.AfterOverlayC1150 = obj.AfterOverlayC1150;
                    objPRL035.AfterOverlayC2150 = obj.AfterOverlayC2150;
                    objPRL035.NubWidth180 = obj.NubWidth180;
                    objPRL035.NubHeight180 = obj.NubHeight180;
                    objPRL035.AfterOverlayA1180 = obj.AfterOverlayA1180;
                    objPRL035.AfterOverlayA2180 = obj.AfterOverlayA2180;
                    objPRL035.AfterOverlayB1180 = obj.AfterOverlayB1180;
                    objPRL035.AfterOverlayB2180 = obj.AfterOverlayB2180;
                    objPRL035.AfterOverlayC1180 = obj.AfterOverlayC1180;
                    objPRL035.AfterOverlayC2180 = obj.AfterOverlayC2180;
                    objPRL035.NubWidth210 = obj.NubWidth210;
                    objPRL035.NubHeight210 = obj.NubHeight210;
                    objPRL035.AfterOverlayA1210 = obj.AfterOverlayA1210;
                    objPRL035.AfterOverlayA2210 = obj.AfterOverlayA2210;
                    objPRL035.AfterOverlayB1210 = obj.AfterOverlayB1210;
                    objPRL035.AfterOverlayB2210 = obj.AfterOverlayB2210;
                    objPRL035.AfterOverlayC1210 = obj.AfterOverlayC1210;
                    objPRL035.AfterOverlayC2210 = obj.AfterOverlayC2210;
                    objPRL035.NubWidth240 = obj.NubWidth240;
                    objPRL035.NubHeight240 = obj.NubHeight240;
                    objPRL035.AfterOverlayA1240 = obj.AfterOverlayA1240;
                    objPRL035.AfterOverlayA2240 = obj.AfterOverlayA2240;
                    objPRL035.AfterOverlayB1240 = obj.AfterOverlayB1240;
                    objPRL035.AfterOverlayB2240 = obj.AfterOverlayB2240;
                    objPRL035.AfterOverlayC1240 = obj.AfterOverlayC1240;
                    objPRL035.AfterOverlayC2240 = obj.AfterOverlayC2240;
                    objPRL035.NubWidth270 = obj.NubWidth270;
                    objPRL035.NubHeight270 = obj.NubHeight270;
                    objPRL035.AfterOverlayA1270 = obj.AfterOverlayA1270;
                    objPRL035.AfterOverlayA2270 = obj.AfterOverlayA2270;
                    objPRL035.AfterOverlayB1270 = obj.AfterOverlayB1270;
                    objPRL035.AfterOverlayB2270 = obj.AfterOverlayB2270;
                    objPRL035.AfterOverlayC1270 = obj.AfterOverlayC1270;
                    objPRL035.AfterOverlayC2270 = obj.AfterOverlayC2270;
                    objPRL035.NubWidth300 = obj.NubWidth300;
                    objPRL035.NubHeight300 = obj.NubHeight300;
                    objPRL035.AfterOverlayA1300 = obj.AfterOverlayA1300;
                    objPRL035.AfterOverlayA2300 = obj.AfterOverlayA2300;
                    objPRL035.AfterOverlayB1300 = obj.AfterOverlayB1300;
                    objPRL035.AfterOverlayB2300 = obj.AfterOverlayB2300;
                    objPRL035.AfterOverlayC1300 = obj.AfterOverlayC1300;
                    objPRL035.AfterOverlayC2300 = obj.AfterOverlayC2300;
                    objPRL035.NubWidth330 = obj.NubWidth330;
                    objPRL035.NubHeight330 = obj.NubHeight330;
                    objPRL035.AfterOverlayA1330 = obj.AfterOverlayA1330;
                    objPRL035.AfterOverlayA2330 = obj.AfterOverlayA2330;
                    objPRL035.AfterOverlayB1330 = obj.AfterOverlayB1330;
                    objPRL035.AfterOverlayB2330 = obj.AfterOverlayB2330;
                    objPRL035.AfterOverlayC1330 = obj.AfterOverlayC1330;
                    objPRL035.AfterOverlayC2330 = obj.AfterOverlayC2330;
                    objPRL035.Remark1 = obj.Remark1;
                    objPRL035.Remark2 = obj.Remark2;
                    objPRL035.Remark3 = obj.Remark3;
                    objPRL035.Remark4 = obj.Remark4;
                    objPRL035.Remark5 = obj.Remark5;
                    objPRL035.Remark5_1 = obj.Remark5_1.HasValue ? Manager.getDateTime(obj.Remark5_1.Value.ToShortDateString()) : obj.Remark5_1;

                    objPRL035.Remark6 = obj.Remark6;
                    objPRL035.Remark7_1 = obj.Remark7_1;
                    objPRL035.Remark7_2 = obj.Remark7_2;
                    objPRL035.EditedBy = objClsLoginInfo.UserName;
                    objPRL035.EditedOn = DateTime.Now;


                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.LinkageSaved;
                    objResponseMsg.HeaderId = objPRL035.HeaderId;

                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Common function
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PROD3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => (i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc == clsImplementationEnum.UserRoleName.QC2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI3.GetStringValue())).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = string.Empty;
                    objUserRoleAccessDetails.UserDesignation = string.Empty;
                }

            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }
        #endregion
    }
}