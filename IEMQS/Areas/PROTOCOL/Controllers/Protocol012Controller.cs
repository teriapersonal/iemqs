﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol012Controller : clsBase
    {
        // GET: PROTOCOL/Protocol012

        #region Protocol Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id, int? m)
        {//PRO055 
            PRO055 objPRO055 = new PRO055();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();


            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "WELD VISUAL & DIMENSION REPORT FOR SPOOL TYPE-2";

            if (!id.HasValue)
            {
                objPRO055.ProtocolNo = string.Empty;
                objPRO055.CreatedBy = objClsLoginInfo.UserName;
                objPRO055.CreatedOn = DateTime.Now;
                db.PRO055.Add(objPRO055);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO055.HeaderId;
            }
            else
            {
                objPRO055 = db.PRO055.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO055);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO055 obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = obj.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO055 objPRO055 = db.PRO055.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var lstPRO055 = db.PRO055.Where(o => o.HeaderId != refHeaderId && o.ProtocolNo.Trim() == obj.ProtocolNo.Trim()).ToList();
                    if (lstPRO055.Count == 0)
                    {
                        #region Save Data
                        objPRO055.ProtocolNo = obj.ProtocolNo;
                        objPRO055.BoltHolesStraddledForFlange = obj.BoltHolesStraddledForFlange;
                        objPRO055.BoltHolesStraddledForFlangeTol = obj.BoltHolesStraddledForFlangeTol;
                        objPRO055.BoltHolesStraddledForFlangeAct = obj.BoltHolesStraddledForFlangeAct;
                        objPRO055.Cuso4IsClearedAtInsideOfWepArea = obj.Cuso4IsClearedAtInsideOfWepArea;
                        objPRO055.WelderStampPunch = obj.WelderStampPunch;
                        objPRO055.SeamNoPunch = obj.SeamNoPunch;
                        objPRO055.TempAttachRemovedGrounded = obj.TempAttachRemovedGrounded;
                        objPRO055.SquarenessTol = obj.SquarenessTol;
                        objPRO055.SquarenessAct = obj.SquarenessAct;
                        objPRO055.TiltingTol = obj.TiltingTol;
                        objPRO055.TiltingAct = obj.TiltingAct;
                        objPRO055.WepDimensinon = obj.WepDimensinon;
                        objPRO055.ElevationReq = obj.ElevationReq;
                        objPRO055.ElevationAct = obj.ElevationAct;
                        objPRO055.ProjectionReq = obj.ProjectionReq;
                        objPRO055.ProjectionAct = obj.ProjectionAct;
                        objPRO055.AllPartsAreAttachedToSpool = obj.AllPartsAreAttachedToSpool;
                        objPRO055.Note1 = obj.Note1;
                        objPRO055.Note3 = obj.Note3;
                        objPRO055.Note4 = obj.Note4;
                        objPRO055.Note5 = obj.Note5;
                        objPRO055.Note5_1 = obj.Note5_1.HasValue ? Manager.getDateTime(obj.Note5_1.Value.ToShortDateString()) : obj.Note5_1;

                        objPRO055.Note6 = obj.Note6;
                        objPRO055.Note7 = obj.Note7;
                        objPRO055.Note8 = obj.Note8;
                        objPRO055.Note9 = obj.Note9;
                        objPRO055.Note10 = obj.Note10;
                        objPRO055.CreatedBy = objClsLoginInfo.UserName;
                        objPRO055.CreatedOn = DateTime.Now;
                        objPRO055.EditedBy = objClsLoginInfo.UserName;
                        objPRO055.EditedOn = DateTime.Now;
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolSaved;
                        objResponseMsg.HeaderId = objPRO055.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolExists;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId > 0)
                {
                    PRO055 objPRO055 = db.PRO055.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO055 != null && string.IsNullOrWhiteSpace(objPRO055.ProtocolNo))
                    {
                        db.PRO055.Remove(objPRO055);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_REPORT_FOR_SPOOL_TYPE2.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO055.HeaderId;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Linkage Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Linkage(int? id, int? m)
        {//PRL055 
            PRL055 objPRL055 = new PRL055();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();


            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = "WELD VISUAL & DIMENSION REPORT FOR SPOOL TYPE-2";

            if (!id.HasValue)
            {
                //objPRL055.QualityProject = "0017005A/A";
                objPRL055.ProtocolNo = string.Empty;
                objPRL055.CreatedBy = objClsLoginInfo.UserName;
                objPRL055.CreatedOn = DateTime.Now;
                db.PRL055.Add(objPRL055);
                db.SaveChanges();
                ViewBag.HeaderId = objPRL055.HeaderId;
            }
            else
            {
                objPRL055 = db.PRL055.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL055.QualityProject).FirstOrDefault();
                ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRL055);
        }

        [HttpPost]
        public ActionResult SaveLinkageHeaderData(PRL055 obj)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = obj.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL055 objPRL055 = db.PRL055.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data

                    objPRL055.DCRNo = obj.DCRNo;
                    objPRL055.ProtocolNo = obj.ProtocolNo != null ? obj.ProtocolNo : "";
                    objPRL055.BoltHolesStraddledForFlange = obj.BoltHolesStraddledForFlange;
                    objPRL055.BoltHolesStraddledForFlangeTol = obj.BoltHolesStraddledForFlangeTol;
                    objPRL055.BoltHolesStraddledForFlangeAct = obj.BoltHolesStraddledForFlangeAct;
                    objPRL055.Cuso4IsClearedAtInsideOfWepArea = obj.Cuso4IsClearedAtInsideOfWepArea;
                    objPRL055.WelderStampPunch = obj.WelderStampPunch;
                    objPRL055.SeamNoPunch = obj.SeamNoPunch;
                    objPRL055.TempAttachRemovedGrounded = obj.TempAttachRemovedGrounded;
                    objPRL055.SquarenessTol = obj.SquarenessTol;
                    objPRL055.SquarenessAct = obj.SquarenessAct;
                    objPRL055.TiltingTol = obj.TiltingTol;
                    objPRL055.TiltingAct = obj.TiltingAct;
                    objPRL055.WepDimensinon = obj.WepDimensinon;
                    objPRL055.ElevationReq = obj.ElevationReq;
                    objPRL055.ElevationAct = obj.ElevationAct;
                    objPRL055.ProjectionReq = obj.ProjectionReq;
                    objPRL055.ProjectionAct = obj.ProjectionAct;
                    objPRL055.AllPartsAreAttachedToSpool = obj.AllPartsAreAttachedToSpool;
                    objPRL055.Note1 = obj.Note1;
                    objPRL055.Note3 = obj.Note3;
                    objPRL055.Note4 = obj.Note4;
                    objPRL055.Note5 = obj.Note5;
                    objPRL055.Note5_1 = obj.Note5_1.HasValue ? Manager.getDateTime(obj.Note5_1.Value.ToShortDateString()) : obj.Note5_1;

                    objPRL055.Note6 = obj.Note6;
                    objPRL055.Note7 = obj.Note7;
                    objPRL055.Note8 = obj.Note8;
                    objPRL055.Note9 = obj.Note9;
                    objPRL055.Note10 = obj.Note10;
                    objPRL055.EditedBy = objClsLoginInfo.UserName;
                    objPRL055.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.LinkageSaved;
                    objResponseMsg.HeaderId = objPRL055.HeaderId;

                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Common function
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PROD3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => (i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc == clsImplementationEnum.UserRoleName.QC2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI3.GetStringValue())).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = string.Empty;
                    objUserRoleAccessDetails.UserDesignation = string.Empty;
                }

            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }
        #endregion
    }
}