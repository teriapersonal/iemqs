﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol015Controller : clsBase
    {
        #region Details View Code
        string ControllerURL = "/PROTOCOL/Protocol015/";
        string Title = "NUB DIMENSION INSPECTION REPORTS FOR TAPER TYPE";
        // GET: PROTOCOL/Protocol015
        [SessionExpireFilter]//, UserPermissions, AllowAnonymous]


        public ActionResult Details(int? id,int? m)
        {
            PRO070 objPRO070 = new PRO070();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();


            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;


            if (!id.HasValue)
            {
                objPRO070.ProtocolNo = string.Empty;
                objPRO070.CreatedBy = objClsLoginInfo.UserName;
                objPRO070.CreatedOn = DateTime.Now;
                db.PRO070.Add(objPRO070);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO070.HeaderId;
            }
            else
            {
                objPRO070 = db.PRO070.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO070);

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO070 PRO070)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO070.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO070 objPRO070 = db.PRO070.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var lstPRO070 = db.PRO070.Where(o => o.HeaderId != refHeaderId && o.ProtocolNo.Trim() == PRO070.ProtocolNo.Trim()).ToList();
                    if (lstPRO070.Count == 0)
                    {
                        #region Save Data
                        objPRO070.ProtocolNo = PRO070.ProtocolNo;

                        objPRO070.nubwidthreq = PRO070.nubwidthreq;
                        objPRO070.nubwidthtol = PRO070.nubwidthtol;
                        objPRO070.nubwidth1 = PRO070.nubwidth1;
                        objPRO070.nubwidth2 = PRO070.nubwidth2;
                        objPRO070.nubwidth3 = PRO070.nubwidth3;
                        objPRO070.nubwidth4 = PRO070.nubwidth4;
                        objPRO070.nubwidth5 = PRO070.nubwidth5;
                        objPRO070.nubwidth6 = PRO070.nubwidth6;
                        objPRO070.nubwidth7 = PRO070.nubwidth7;
                        objPRO070.nubwidth8 = PRO070.nubwidth8;
                        objPRO070.nubwidth9 = PRO070.nubwidth9;
                        objPRO070.nubwidth10 = PRO070.nubwidth10;
                        objPRO070.nubwidth11 = PRO070.nubwidth11;
                        objPRO070.nubwidth12 = PRO070.nubwidth12;

                        objPRO070.nubheightreq = PRO070.nubheightreq;
                        objPRO070.nubheighttol = PRO070.nubheighttol;
                        objPRO070.nubheight1 = PRO070.nubheight1;
                        objPRO070.nubheight2 = PRO070.nubheight2;
                        objPRO070.nubheight3 = PRO070.nubheight3;
                        objPRO070.nubheight4 = PRO070.nubheight4;
                        objPRO070.nubheight5 = PRO070.nubheight5;
                        objPRO070.nubheight6 = PRO070.nubheight6;
                        objPRO070.nubheight7 = PRO070.nubheight7;
                        objPRO070.nubheight8 = PRO070.nubheight8;
                        objPRO070.nubheight9 = PRO070.nubheight9;
                        objPRO070.nubheight10 = PRO070.nubheight10;
                        objPRO070.nubheight11 = PRO070.nubheight11;
                        objPRO070.nubheight12 = PRO070.nubheight12;

                        objPRO070.A1Req = PRO070.A1Req;
                        objPRO070.A11 = PRO070.A11;
                        objPRO070.A12 = PRO070.A12;
                        objPRO070.A13 = PRO070.A13;
                        objPRO070.A14 = PRO070.A14;
                        objPRO070.A15 = PRO070.A15;
                        objPRO070.A16 = PRO070.A16;
                        objPRO070.A17 = PRO070.A17;
                        objPRO070.A18 = PRO070.A18;
                        objPRO070.A19 = PRO070.A19;
                        objPRO070.A110 = PRO070.A110;
                        objPRO070.A111 = PRO070.A111;
                        objPRO070.A112 = PRO070.A112;

                        objPRO070.A2Req = PRO070.A2Req;
                        objPRO070.A21 = PRO070.A21;
                        objPRO070.A22 = PRO070.A22;
                        objPRO070.A23 = PRO070.A23;
                        objPRO070.A24 = PRO070.A24;
                        objPRO070.A25 = PRO070.A25;
                        objPRO070.A26 = PRO070.A26;
                        objPRO070.A27 = PRO070.A27;
                        objPRO070.A28 = PRO070.A28;
                        objPRO070.A29 = PRO070.A29;
                        objPRO070.A210 = PRO070.A210;
                        objPRO070.A211 = PRO070.A211;
                        objPRO070.A212 = PRO070.A212;

                        objPRO070.B1Req = PRO070.B1Req;
                        objPRO070.B11 = PRO070.B11;
                        objPRO070.B12 = PRO070.B12;
                        objPRO070.B13 = PRO070.B13;
                        objPRO070.B14 = PRO070.B14;
                        objPRO070.B15 = PRO070.B15;
                        objPRO070.B16 = PRO070.B16;
                        objPRO070.B17 = PRO070.B17;
                        objPRO070.B18 = PRO070.B18;
                        objPRO070.B19 = PRO070.B19;
                        objPRO070.B110 = PRO070.B110;
                        objPRO070.B111 = PRO070.B111;
                        objPRO070.B112 = PRO070.B112;

                        objPRO070.B2Req = PRO070.B2Req;
                        objPRO070.B21 = PRO070.B21;
                        objPRO070.B22 = PRO070.B22;
                        objPRO070.B23 = PRO070.B23;
                        objPRO070.B24 = PRO070.B24;
                        objPRO070.B25 = PRO070.B25;
                        objPRO070.B26 = PRO070.B26;
                        objPRO070.B27 = PRO070.B27;
                        objPRO070.B28 = PRO070.B28;
                        objPRO070.B29 = PRO070.B29;
                        objPRO070.B210 = PRO070.B210;
                        objPRO070.B211 = PRO070.B211;
                        objPRO070.B212 = PRO070.B112;

                        objPRO070.C1Req = PRO070.C1Req;
                        objPRO070.C11 = PRO070.C11;
                        objPRO070.C12 = PRO070.C12;
                        objPRO070.C13 = PRO070.C13;
                        objPRO070.C14 = PRO070.C14;
                        objPRO070.C15 = PRO070.C15;
                        objPRO070.C16 = PRO070.C16;
                        objPRO070.C17 = PRO070.C17;
                        objPRO070.C18 = PRO070.C18;
                        objPRO070.C19 = PRO070.C19;
                        objPRO070.C110 = PRO070.C110;
                        objPRO070.C111 = PRO070.C111;
                        objPRO070.C112 = PRO070.C112;

                        objPRO070.C2Req = PRO070.C2Req;
                        objPRO070.C21 = PRO070.C21;
                        objPRO070.C22 = PRO070.C22;
                        objPRO070.C23 = PRO070.C23;
                        objPRO070.C24 = PRO070.C24;
                        objPRO070.C25 = PRO070.C25;
                        objPRO070.C26 = PRO070.C26;
                        objPRO070.C27 = PRO070.C27;
                        objPRO070.C28 = PRO070.C28;
                        objPRO070.C29 = PRO070.C29;
                        objPRO070.C210 = PRO070.C210;
                        objPRO070.C211 = PRO070.C211;
                        objPRO070.C212 = PRO070.C212;

                        objPRO070.Remark2 = PRO070.Remark2;
                        objPRO070.Remark3 = PRO070.Remark3;
                        objPRO070.Remark5 = PRO070.Remark5;
                        objPRO070.Remark5_1 = PRO070.Remark5_1.HasValue ? Manager.getDateTime(PRO070.Remark5_1.Value.ToShortDateString()) : PRO070.Remark5_1;

                        objPRO070.Remark6 = PRO070.Remark6;
                        objPRO070.Remark7 = PRO070.Remark7;
                        
                        objPRO070.EditedBy = objClsLoginInfo.UserName;
                        objPRO070.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolSaved;
                        objResponseMsg.HeaderId = objPRO070.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolExists;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId > 0)
                {
                    PRO070 objPRO070 = db.PRO070.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO070 != null && string.IsNullOrWhiteSpace(objPRO070.ProtocolNo))
                    {
                        db.PRO070.Remove(objPRO070);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_AFTER_OVERLAY_FOR_TAPER_TYPE_NUB.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO070.HeaderId;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region Linkage Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Linkage(int? id, int? m)
        {//PRL070 
            PRL070 objPRL070 = new PRL070();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();


            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

          
            objPRL070 = db.PRL070.Where(x => x.HeaderId == id).FirstOrDefault();
            if (objPRL070 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL070.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }
            
            return View(objPRL070);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL070 PRL070)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL070.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL070 objPRL070 = db.PRL070.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL070.DCRNo = PRL070.DCRNo;
                    objPRL070.ProtocolNo = PRL070.ProtocolNo != null ? PRL070.ProtocolNo : "";

                    objPRL070.nubwidthreq = PRL070.nubwidthreq;
                    objPRL070.nubwidthtol = PRL070.nubwidthtol;
                    objPRL070.nubwidth1 = PRL070.nubwidth1;
                    objPRL070.nubwidth2 = PRL070.nubwidth2;
                    objPRL070.nubwidth3 = PRL070.nubwidth3;
                    objPRL070.nubwidth4 = PRL070.nubwidth4;
                    objPRL070.nubwidth5 = PRL070.nubwidth5;
                    objPRL070.nubwidth6 = PRL070.nubwidth6;
                    objPRL070.nubwidth7 = PRL070.nubwidth7;
                    objPRL070.nubwidth8 = PRL070.nubwidth8;
                    objPRL070.nubwidth9 = PRL070.nubwidth9;
                    objPRL070.nubwidth10 = PRL070.nubwidth10;
                    objPRL070.nubwidth11 = PRL070.nubwidth11;
                    objPRL070.nubwidth12 = PRL070.nubwidth12;

                    objPRL070.nubheightreq = PRL070.nubheightreq;
                    objPRL070.nubheighttol = PRL070.nubheighttol;
                    objPRL070.nubheight1 = PRL070.nubheight1;
                    objPRL070.nubheight2 = PRL070.nubheight2;
                    objPRL070.nubheight3 = PRL070.nubheight3;
                    objPRL070.nubheight4 = PRL070.nubheight4;
                    objPRL070.nubheight5 = PRL070.nubheight5;
                    objPRL070.nubheight6 = PRL070.nubheight6;
                    objPRL070.nubheight7 = PRL070.nubheight7;
                    objPRL070.nubheight8 = PRL070.nubheight8;
                    objPRL070.nubheight9 = PRL070.nubheight9;
                    objPRL070.nubheight10 = PRL070.nubheight10;
                    objPRL070.nubheight11 = PRL070.nubheight11;
                    objPRL070.nubheight12 = PRL070.nubheight12;

                    objPRL070.A1Req = PRL070.A1Req;
                    objPRL070.A11 = PRL070.A11;
                    objPRL070.A12 = PRL070.A12;
                    objPRL070.A13 = PRL070.A13;
                    objPRL070.A14 = PRL070.A14;
                    objPRL070.A15 = PRL070.A15;
                    objPRL070.A16 = PRL070.A16;
                    objPRL070.A17 = PRL070.A17;
                    objPRL070.A18 = PRL070.A18;
                    objPRL070.A19 = PRL070.A19;
                    objPRL070.A110 = PRL070.A110;
                    objPRL070.A111 = PRL070.A111;
                    objPRL070.A112 = PRL070.A112;

                    objPRL070.A2Req = PRL070.A2Req;
                    objPRL070.A21 = PRL070.A21;
                    objPRL070.A22 = PRL070.A22;
                    objPRL070.A23 = PRL070.A23;
                    objPRL070.A24 = PRL070.A24;
                    objPRL070.A25 = PRL070.A25;
                    objPRL070.A26 = PRL070.A26;
                    objPRL070.A27 = PRL070.A27;
                    objPRL070.A28 = PRL070.A28;
                    objPRL070.A29 = PRL070.A29;
                    objPRL070.A210 = PRL070.A210;
                    objPRL070.A211 = PRL070.A211;
                    objPRL070.A212 = PRL070.A212;

                    objPRL070.B1Req = PRL070.B1Req;
                    objPRL070.B11 = PRL070.B11;
                    objPRL070.B12 = PRL070.B12;
                    objPRL070.B13 = PRL070.B13;
                    objPRL070.B14 = PRL070.B14;
                    objPRL070.B15 = PRL070.B15;
                    objPRL070.B16 = PRL070.B16;
                    objPRL070.B17 = PRL070.B17;
                    objPRL070.B18 = PRL070.B18;
                    objPRL070.B19 = PRL070.B19;
                    objPRL070.B110 = PRL070.B110;
                    objPRL070.B111 = PRL070.B111;
                    objPRL070.B112 = PRL070.B112;

                    objPRL070.B2Req = PRL070.B2Req;
                    objPRL070.B21 = PRL070.B21;
                    objPRL070.B22 = PRL070.B22;
                    objPRL070.B23 = PRL070.B23;
                    objPRL070.B24 = PRL070.B24;
                    objPRL070.B25 = PRL070.B25;
                    objPRL070.B26 = PRL070.B26;
                    objPRL070.B27 = PRL070.B27;
                    objPRL070.B28 = PRL070.B28;
                    objPRL070.B29 = PRL070.B29;
                    objPRL070.B210 = PRL070.B210;
                    objPRL070.B211 = PRL070.B211;
                    objPRL070.B212 = PRL070.B112;

                    objPRL070.C1Req = PRL070.C1Req;
                    objPRL070.C11 = PRL070.C11;
                    objPRL070.C12 = PRL070.C12;
                    objPRL070.C13 = PRL070.C13;
                    objPRL070.C14 = PRL070.C14;
                    objPRL070.C15 = PRL070.C15;
                    objPRL070.C16 = PRL070.C16;
                    objPRL070.C17 = PRL070.C17;
                    objPRL070.C18 = PRL070.C18;
                    objPRL070.C19 = PRL070.C19;
                    objPRL070.C110 = PRL070.C110;
                    objPRL070.C111 = PRL070.C111;
                    objPRL070.C112 = PRL070.C112;

                    objPRL070.C2Req = PRL070.C2Req;
                    objPRL070.C21 = PRL070.C21;
                    objPRL070.C22 = PRL070.C22;
                    objPRL070.C23 = PRL070.C23;
                    objPRL070.C24 = PRL070.C24;
                    objPRL070.C25 = PRL070.C25;
                    objPRL070.C26 = PRL070.C26;
                    objPRL070.C27 = PRL070.C27;
                    objPRL070.C28 = PRL070.C28;
                    objPRL070.C29 = PRL070.C29;
                    objPRL070.C210 = PRL070.C210;
                    objPRL070.C211 = PRL070.C211;
                    objPRL070.C212 = PRL070.C212;

                    objPRL070.Remark2 = PRL070.Remark2;
                    objPRL070.Remark3 = PRL070.Remark3;
                    objPRL070.Remark5 = PRL070.Remark5;
                    objPRL070.Remark5_1 = PRL070.Remark5_1.HasValue ? Manager.getDateTime(PRL070.Remark5_1.Value.ToShortDateString()) : PRL070.Remark5_1;
                    objPRL070.Remark6 = PRL070.Remark6;
                    objPRL070.Remark7 = PRL070.Remark7;
                   
                    objPRL070.EditedBy = objClsLoginInfo.UserName;
                    objPRL070.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL070.HeaderId;

                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        
    }
}