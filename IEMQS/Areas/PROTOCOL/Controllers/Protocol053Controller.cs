﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol053Controller : clsBase
    {
        // GET: PROTOCOL/Protocol053
        string ControllerURL = "/PROTOCOL/Protocol053/";
        string Title = "VISUAL  & DIMENSION REPORT FOR ";

        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO260 objPRO260 = new PRO260();
            if (!id.HasValue)
            {
                try
                {
                    objPRO260.ProtocolNo = string.Empty;
                    objPRO260.CreatedBy = objClsLoginInfo.UserName;
                    objPRO260.CreatedOn = DateTime.Now;

                    #region  TOTAL HEIGHT REQ | PRO261
                    objPRO260.PRO261.Add(new PRO261
                    {
                        RequiredValue = "At 0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO260.PRO261.Add(new PRO261
                    {
                        RequiredValue = "At 90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO260.PRO261.Add(new PRO261
                    {
                        RequiredValue = "At 180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO260.PRO261.Add(new PRO261
                    {
                        RequiredValue = "At 270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO260.Add(objPRO260);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO260.HeaderId;
            }
            else
            {
                objPRO260 = db.PRO260.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();
            List<string> lstProtocolHeaderName = new List<string> { "PIPE TO PIPE", "PIPE TO FLANGE", "PIPE TO ELBOW", "PIPE TO FORGING", "FORGING TO FLANGE", "ELBOW TO FLANGE" };

            ViewBag.ProtocolHeaderName = lstProtocolHeaderName.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO261> lstPRO261 = db.PRO261.Where(x => x.HeaderId == objPRO260.HeaderId).ToList();
            List<PRO262> lstPRO262 = db.PRO262.Where(x => x.HeaderId == objPRO260.HeaderId).ToList();
            List<PRO263> lstPRO263 = db.PRO263.Where(x => x.HeaderId == objPRO260.HeaderId).ToList();
            List<PRO264> lstPRO264 = db.PRO264.Where(x => x.HeaderId == objPRO260.HeaderId).ToList();
            List<PRO264_2> lstPRO264_2 = db.PRO264_2.Where(x => x.HeaderId == objPRO260.HeaderId).ToList();

            ViewBag.lstPRO261 = lstPRO261;
            ViewBag.lstPRO262 = lstPRO262;
            ViewBag.lstPRO263 = lstPRO263;
            ViewBag.lstPRO264 = lstPRO264;
            ViewBag.lstPRO264_2 = lstPRO264_2;
            #endregion

            return View(objPRO260);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO260 PRO260)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO260.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO260 objPRO260 = db.PRO260.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO260.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO260.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO260.ProtocolNo = PRO260.ProtocolNo;
                        objPRO260.ProtocolHeaderName = PRO260.ProtocolHeaderName;
                        objPRO260.ReqTotalLength = PRO260.ReqTotalLength;
                        objPRO260.ProjectionInCaseOfElbow = PRO260.ProjectionInCaseOfElbow;
                        objPRO260.ReqLengthOfFirstPart = PRO260.ReqLengthOfFirstPart;
                        objPRO260.ReqLengthOfSecondPart = PRO260.ReqLengthOfSecondPart;
                        objPRO260.ReqProjectionInCaseOfElbow = PRO260.ReqProjectionInCaseOfElbow;
                        objPRO260.ActLengthOfFisrtPart = PRO260.ActLengthOfFisrtPart;
                        objPRO260.ActLengthOfSecondPart = PRO260.ActLengthOfSecondPart;
                        objPRO260.ActProjectionInCaseOfElbow = PRO260.ActProjectionInCaseOfElbow;
                        objPRO260.OrientationOfLongSeams = PRO260.OrientationOfLongSeams;
                        objPRO260.AllowableOffset = PRO260.AllowableOffset;
                        objPRO260.IsStraddlingOfBoltHoles = PRO260.IsStraddlingOfBoltHoles;
                        objPRO260.IsTiltingOfFlange = PRO260.IsTiltingOfFlange;
                        objPRO260.ReqStraddlingOfBoltHoles = PRO260.ReqStraddlingOfBoltHoles;
                        objPRO260.ReqTiltingOfFlange = PRO260.ReqTiltingOfFlange;
                        objPRO260.ActStraddlingOfBoltHoles = PRO260.ActStraddlingOfBoltHoles;
                        objPRO260.ActTiltingOfFlange = PRO260.ActTiltingOfFlange;
                        objPRO260.IsAlignmentReadings = PRO260.IsAlignmentReadings;
                        objPRO260.OutByAlignmentReadings = PRO260.OutByAlignmentReadings;
                        objPRO260.OutByAlignmentReadingsAt0 = PRO260.OutByAlignmentReadingsAt0;
                        objPRO260.OutByAlignmentReadingsAt90 = PRO260.OutByAlignmentReadingsAt90;
                        objPRO260.OutByAlignmentReadingsAt180 = PRO260.OutByAlignmentReadingsAt180;
                        objPRO260.OutByAlignmentReadingsAt270 = PRO260.OutByAlignmentReadingsAt270;
                        objPRO260.CheckPoint1 = PRO260.CheckPoint1;
                        objPRO260.CheckPoint2 = PRO260.CheckPoint2;
                        objPRO260.CheckPoint3 = PRO260.CheckPoint3;
                        objPRO260.CheckPoint4 = PRO260.CheckPoint4;
                        objPRO260.CheckPoint6 = PRO260.CheckPoint6;
                        objPRO260.CheckPoint6_2 = PRO260.CheckPoint6_2;
                        objPRO260.CheckPoint7 = PRO260.CheckPoint7;
                        objPRO260.QCRemarks = PRO260.QCRemarks;
                        objPRO260.Result = PRO260.Result;
                        objPRO260.ReqWidth = PRO260.ReqWidth;
                        objPRO260.ReqDepth = PRO260.ReqDepth;
                        objPRO260.Note1 = PRO260.Note1;
                        objPRO260.Note2 = PRO260.Note2;
                        objPRO260.Note3 = PRO260.Note3;
                        objPRO260.TotalSpotNoCladStriping = PRO260.TotalSpotNoCladStriping;
                        objPRO260.TotalSpotNoOffsetReadings = PRO260.TotalSpotNoOffsetReadings;

                        objPRO260.EditedBy = objClsLoginInfo.UserName;
                        objPRO260.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO260.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO260 objPRO260 = db.PRO260.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO260 != null && string.IsNullOrWhiteSpace(objPRO260.ProtocolNo))
                    {
                        db.PRO260.Remove(objPRO260);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO260.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO261> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO261> lstAddPRO261 = new List<PRO261>();
                List<PRO261> lstDeletePRO261 = new List<PRO261>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO261 obj = db.PRO261.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.RequiredValue = item.RequiredValue;
                                obj.ActualValue = item.ActualValue;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO261 obj = new PRO261();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.RequiredValue))
                            {
                                obj.RequiredValue = item.RequiredValue;
                                obj.ActualValue = item.ActualValue;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO261.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO261.Count > 0)
                    {
                        db.PRO261.AddRange(lstAddPRO261);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO261 = db.PRO261.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO261.Count > 0)
                    {
                        db.PRO261.RemoveRange(lstDeletePRO261);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO261 = db.PRO261.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO261.Count > 0)
                    {
                        db.PRO261.RemoveRange(lstDeletePRO261);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO262> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO262> lstAddPRO262 = new List<PRO262>();
                List<PRO262> lstDeletePRO262 = new List<PRO262>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO262 obj = db.PRO262.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO262();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActArcLength = item.ActArcLength;

                        if (isAdded)
                        {
                            lstAddPRO262.Add(obj);
                        }
                    }
                    if (lstAddPRO262.Count > 0)
                    {
                        db.PRO262.AddRange(lstAddPRO262);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO262 = db.PRO262.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO262.Count > 0)
                    {
                        db.PRO262.RemoveRange(lstDeletePRO262);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO262 = db.PRO262.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO262.Count > 0)
                    {
                        db.PRO262.RemoveRange(lstDeletePRO262);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO263> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO263> lstAddPRO263 = new List<PRO263>();
                List<PRO263> lstDeletePRO263 = new List<PRO263>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO263 obj = db.PRO263.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO263();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.OffSet = item.OffSet;

                        if (isAdded)
                        {
                            lstAddPRO263.Add(obj);
                        }
                    }
                    if (lstAddPRO263.Count > 0)
                    {
                        db.PRO263.AddRange(lstAddPRO263);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO263 = db.PRO263.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO263.Count > 0)
                    {
                        db.PRO263.RemoveRange(lstDeletePRO263);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO263 = db.PRO263.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO263.Count > 0)
                    {
                        db.PRO263.RemoveRange(lstDeletePRO263);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO264> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO264> lstAddPRO264 = new List<PRO264>();
                List<PRO264> lstDeletePRO264 = new List<PRO264>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO264 obj = db.PRO264.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Location))
                            {
                                obj = new PRO264();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.At0 = item.At0;
                            obj.At90 = item.At90;
                            obj.At180 = item.At180;
                            obj.At270 = item.At270;
                        }

                        if (isAdded)
                        {
                            lstAddPRO264.Add(obj);
                        }
                    }
                    if (lstAddPRO264.Count > 0)
                    {
                        db.PRO264.AddRange(lstAddPRO264);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO264 = db.PRO264.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO264.Count > 0)
                    {
                        db.PRO264.RemoveRange(lstDeletePRO264);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO264 = db.PRO264.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO264.Count > 0)
                    {
                        db.PRO264.RemoveRange(lstDeletePRO264);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line5 details

        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO264_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO264_2> lstAddPRO264_2 = new List<PRO264_2>();
                List<PRO264_2> lstDeletePRO264_2 = new List<PRO264_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO264_2 obj = db.PRO264_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO264_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.ActWidth = item.ActWidth;
                        obj.ActDepth = item.ActDepth;

                        if (isAdded)
                        {
                            lstAddPRO264_2.Add(obj);
                        }
                    }
                    if (lstAddPRO264_2.Count > 0)
                    {
                        db.PRO264_2.AddRange(lstAddPRO264_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO264_2 = db.PRO264_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO264_2.Count > 0)
                    {
                        db.PRO264_2.RemoveRange(lstDeletePRO264_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO264_2 = db.PRO264_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO264_2.Count > 0)
                    {
                        db.PRO264_2.RemoveRange(lstDeletePRO264_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region  Linkage View Code        
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL260 objPRL260 = new PRL260();
            objPRL260 = db.PRL260.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL260 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL260.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstProtocolHeaderName = new List<string> { "PIPE TO PIPE", "PIPE TO FLANGE", "PIPE TO ELBOW", "PIPE TO FORGING", "FORGING TO FLANGE", "ELBOW TO FLANGE" };


            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.ProtocolHeaderName = lstProtocolHeaderName.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL261> lstPRL261 = db.PRL261.Where(x => x.HeaderId == objPRL260.HeaderId).ToList();
            List<PRL262> lstPRL262 = db.PRL262.Where(x => x.HeaderId == objPRL260.HeaderId).ToList();
            List<PRL263> lstPRL263 = db.PRL263.Where(x => x.HeaderId == objPRL260.HeaderId).ToList();
            List<PRL264> lstPRL264 = db.PRL264.Where(x => x.HeaderId == objPRL260.HeaderId).ToList();
            List<PRL264_2> lstPRL264_2 = db.PRL264_2.Where(x => x.HeaderId == objPRL260.HeaderId).ToList();

            ViewBag.lstPRL261 = lstPRL261;
            ViewBag.lstPRL262 = lstPRL262;
            ViewBag.lstPRL263 = lstPRL263;
            ViewBag.lstPRL264 = lstPRL264;
            ViewBag.lstPRL264_2 = lstPRL264_2;
            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL260.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL260.ActFilledBy) && (objPRL260.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL260.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL260);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;


            PRL260 objPRL260 = new PRL260();
            db.Configuration.ProxyCreationEnabled = false;
            objPRL260 = db.PRL260.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL260).State = System.Data.Entity.EntityState.Detached;
            NDEModels objNDEModels = new NDEModels();
            if (objPRL260 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL260.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstProtocolHeaderName = new List<string> { "PIPE TO PIPE", "PIPE TO FLANGE", "PIPE TO ELBOW", "PIPE TO FORGING", "FORGING TO FLANGE", "ELBOW TO FLANGE" };


            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ProtocolHeaderName = lstProtocolHeaderName.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL261> lstPRL261 = db.PRL261.Where(x => x.HeaderId == objPRL260.HeaderId).ToList();
            List<PRL262> lstPRL262 = db.PRL262.Where(x => x.HeaderId == objPRL260.HeaderId).ToList();
            List<PRL263> lstPRL263 = db.PRL263.Where(x => x.HeaderId == objPRL260.HeaderId).ToList();
            List<PRL264> lstPRL264 = db.PRL264.Where(x => x.HeaderId == objPRL260.HeaderId).ToList();
            List<PRL264_2> lstPRL264_2 = db.PRL264_2.Where(x => x.HeaderId == objPRL260.HeaderId).ToList();

            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL260.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL260.ActFilledBy) && (objPRL260.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL260.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    OutsideInside = OutsideInside,
                    ProtocolHeaderName = ProtocolHeaderName,
                    AccessRole = AccessRole,
                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,

                    objPRL260 = objPRL260,

                    lstPRL261 = lstPRL261,
                    lstPRL262 = lstPRL262,
                    lstPRL263 = lstPRL263,
                    lstPRL264 = lstPRL264,
                    lstPRL264_2 = lstPRL264_2

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);

            }

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL260 prl260, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl260.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL260 objPRL260 = db.PRL260.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data
                    objPRL260.DrawingRevisionNo = prl260.DrawingRevisionNo;
                    objPRL260.DrawingNo = prl260.DrawingNo;
                    objPRL260.DCRNo = prl260.DCRNo;
                    objPRL260.ProtocolHeaderName = prl260.ProtocolHeaderName;
                    objPRL260.ProtocolNo = prl260.ProtocolNo;
                    objPRL260.ReqTotalLength = prl260.ReqTotalLength;
                    objPRL260.ProjectionInCaseOfElbow = prl260.ProjectionInCaseOfElbow;
                    objPRL260.ReqLengthOfFirstPart = prl260.ReqLengthOfFirstPart;
                    objPRL260.ReqLengthOfSecondPart = prl260.ReqLengthOfSecondPart;
                    objPRL260.ReqProjectionInCaseOfElbow = prl260.ReqProjectionInCaseOfElbow;
                    objPRL260.ActLengthOfFisrtPart = prl260.ActLengthOfFisrtPart;
                    objPRL260.ActLengthOfSecondPart = prl260.ActLengthOfSecondPart;
                    objPRL260.ActProjectionInCaseOfElbow = prl260.ActProjectionInCaseOfElbow;
                    objPRL260.OrientationOfLongSeams = prl260.OrientationOfLongSeams;
                    objPRL260.AllowableOffset = prl260.AllowableOffset;
                    objPRL260.IsStraddlingOfBoltHoles = prl260.IsStraddlingOfBoltHoles;
                    objPRL260.IsTiltingOfFlange = prl260.IsTiltingOfFlange;
                    objPRL260.ReqStraddlingOfBoltHoles = prl260.ReqStraddlingOfBoltHoles;
                    objPRL260.ReqTiltingOfFlange = prl260.ReqTiltingOfFlange;
                    objPRL260.ActStraddlingOfBoltHoles = prl260.ActStraddlingOfBoltHoles;
                    objPRL260.ActTiltingOfFlange = prl260.ActTiltingOfFlange;
                    objPRL260.IsAlignmentReadings = prl260.IsAlignmentReadings;
                    objPRL260.OutByAlignmentReadings = prl260.OutByAlignmentReadings;
                    objPRL260.OutByAlignmentReadingsAt0 = prl260.OutByAlignmentReadingsAt0;
                    objPRL260.OutByAlignmentReadingsAt90 = prl260.OutByAlignmentReadingsAt90;
                    objPRL260.OutByAlignmentReadingsAt180 = prl260.OutByAlignmentReadingsAt180;
                    objPRL260.OutByAlignmentReadingsAt270 = prl260.OutByAlignmentReadingsAt270;
                    objPRL260.CheckPoint1 = prl260.CheckPoint1;
                    objPRL260.CheckPoint2 = prl260.CheckPoint2;
                    objPRL260.CheckPoint3 = prl260.CheckPoint3;
                    objPRL260.CheckPoint4 = prl260.CheckPoint4;
                    objPRL260.CheckPoint6 = prl260.CheckPoint6;
                    objPRL260.CheckPoint6_2 = prl260.CheckPoint6_2;
                    objPRL260.CheckPoint7 = prl260.CheckPoint7;
                    objPRL260.QCRemarks = prl260.QCRemarks;
                    objPRL260.Result = prl260.Result;
                    objPRL260.ReqWidth = prl260.ReqWidth;
                    objPRL260.ReqDepth = prl260.ReqDepth;
                    objPRL260.Note1 = prl260.Note1;
                    objPRL260.Note2 = prl260.Note2;
                    objPRL260.Note3 = prl260.Note3;
                    objPRL260.TotalSpotNoCladStriping = prl260.TotalSpotNoCladStriping;
                    objPRL260.TotalSpotNoOffsetReadings = prl260.TotalSpotNoOffsetReadings;
                    objPRL260.EditedBy = UserName;
                    objPRL260.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL260.ActFilledBy = UserName;
                            objPRL260.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL260.ReqFilledBy = UserName;
                            objPRL260.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL260.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL260 objPRL260 = db.PRL260.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL260 != null && string.IsNullOrWhiteSpace(objPRL260.ProtocolNo))
                    {
                        db.PRL260.Remove(objPRL260);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL260.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL261> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL261> lstAddPRL261 = new List<PRL261>();
                List<PRL261> lstDeletePRL261 = new List<PRL261>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL261 obj = db.PRL261.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL261();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.RequiredValue = item.RequiredValue;
                        obj.ActualValue = item.ActualValue;

                        if (isAdded)
                        {
                            lstAddPRL261.Add(obj);
                        }
                    }
                }
                if (lstAddPRL261.Count > 0)
                {
                    db.PRL261.AddRange(lstAddPRL261);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL261 = db.PRL261.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL261 = db.PRL261.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL261.Count > 0)
                {
                    db.PRL261.RemoveRange(lstDeletePRL261);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL262> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL262> lstAddPRL262 = new List<PRL262>();
                List<PRL262> lstDeletePRL262 = new List<PRL262>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL262 obj = db.PRL262.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL262();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SeamNo = item.SeamNo;
                        obj.ReqOrien = item.ReqOrien;
                        obj.ReqArcLength = item.ReqArcLength;
                        obj.ActArcLength = item.ActArcLength;

                        if (isAdded)
                        {
                            lstAddPRL262.Add(obj);
                        }
                    }
                }
                if (lstAddPRL262.Count > 0)
                {
                    db.PRL262.AddRange(lstAddPRL262);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL262 = db.PRL262.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL262 = db.PRL262.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL262.Count > 0)
                {
                    db.PRL262.RemoveRange(lstDeletePRL262);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL263> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL263> lstAddPRL263 = new List<PRL263>();
                List<PRL263> lstDeletePRL263 = new List<PRL263>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL263 obj = db.PRL263.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL263();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.OffSet = item.OffSet;

                        if (isAdded)
                        {
                            lstAddPRL263.Add(obj);
                        }
                    }
                }
                if (lstAddPRL263.Count > 0)
                {
                    db.PRL263.AddRange(lstAddPRL263);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL263 = db.PRL263.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL263 = db.PRL263.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL263.Count > 0)
                {
                    db.PRL263.RemoveRange(lstDeletePRL263);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL264> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL264> lstAddPRL264 = new List<PRL264>();
                List<PRL264> lstDeletePRL264 = new List<PRL264>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL264 obj = db.PRL264.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Location))
                            {
                                obj = new PRL264();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.At0 = item.At0;
                            obj.At90 = item.At90;
                            obj.At180 = item.At180;
                            obj.At270 = item.At270;
                        }

                        if (isAdded)
                        {
                            lstAddPRL264.Add(obj);
                        }
                    }
                }
                if (lstAddPRL264.Count > 0)
                {
                    db.PRL264.AddRange(lstAddPRL264);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL264 = db.PRL264.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL264 = db.PRL264.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL264.Count > 0)
                {
                    db.PRL264.RemoveRange(lstDeletePRL264);
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line5 details

        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL264_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL264_2> lstAddPRL264_2 = new List<PRL264_2>();
                List<PRL264_2> lstDeletePRL264_2 = new List<PRL264_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL264_2 obj = db.PRL264_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL264_2();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.ActWidth = item.ActWidth;
                        obj.ActDepth = item.ActDepth;

                        if (isAdded)
                        {
                            lstAddPRL264_2.Add(obj);
                        }
                    }
                }
                if (lstAddPRL264_2.Count > 0)
                {
                    db.PRL264_2.AddRange(lstAddPRL264_2);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL264_2 = db.PRL264_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL264_2 = db.PRL264_2.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL264_2.Count > 0)
                {
                    db.PRL264_2.RemoveRange(lstDeletePRL264_2);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}