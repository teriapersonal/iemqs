﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol016Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol016/";
        string Title = "TSR DIMENSION INSPECTION REPORTS";
        

        // GET: PROTOCOL/Protocol016
        #region Protocol Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id,int? m)
        {
            PRO075 objPRO075 = new PRO075();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            if (!id.HasValue)
            {
                objPRO075.ProtocolNo = string.Empty;
                objPRO075.CreatedBy = objClsLoginInfo.UserName;
                objPRO075.CreatedOn = DateTime.Now;
                db.PRO075.Add(objPRO075);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO075.HeaderId;
            }
            else
            {
                objPRO075 = db.PRO075.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }

            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO075);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO075 PRO075)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO075.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO075 objPRO075 = db.PRO075.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var lstPRO075 = db.PRO075.Where(o => o.HeaderId != refHeaderId && o.ProtocolNo.Trim() == PRO075.ProtocolNo.Trim()).ToList();
                    if (lstPRO075.Count == 0)
                    {
                        #region Save Data

                        objPRO075.ProtocolNo = PRO075.ProtocolNo;
                        objPRO075.elevationreq = PRO075.elevationreq;
                        objPRO075.elevationtol = PRO075.elevationtol;
                        objPRO075.TSRwidthreq = PRO075.TSRwidthreq;
                        objPRO075.TSRwidthtol = PRO075.TSRwidthtol;
                        objPRO075.TSRheightreq = PRO075.TSRheightreq;
                        objPRO075.TSRheighttol = PRO075.TSRheighttol;
                        objPRO075.TSRIDreq = PRO075.TSRIDreq;
                        objPRO075.TSRIDtol = PRO075.TSRIDtol;
                        objPRO075.levelnesstol = PRO075.levelnesstol;
                        objPRO075.tiltingtol = PRO075.tiltingtol;
                        objPRO075.flatnesstol = PRO075.flatnesstol;
                        objPRO075.elevation1 = PRO075.elevation1;
                        objPRO075.elevation2 = PRO075.elevation2;
                        objPRO075.elevation3 = PRO075.elevation3;
                        objPRO075.elevation4 = PRO075.elevation4;
                        objPRO075.elevation5 = PRO075.elevation5;
                        objPRO075.elevation6 = PRO075.elevation6;
                        objPRO075.elevation7 = PRO075.elevation7;
                        objPRO075.elevation8 = PRO075.elevation8;
                        objPRO075.elevation9 = PRO075.elevation9;
                        objPRO075.elevation10 = PRO075.elevation10;
                        objPRO075.elevation11 = PRO075.elevation11;
                        objPRO075.elevation12 = PRO075.elevation12;
                        objPRO075.TSRwidth1 = PRO075.TSRwidth1;
                        objPRO075.TSRwidth2 = PRO075.TSRwidth2;
                        objPRO075.TSRwidth3 = PRO075.TSRwidth3;
                        objPRO075.TSRwidth4 = PRO075.TSRwidth4;
                        objPRO075.TSRwidth5 = PRO075.TSRwidth5;
                        objPRO075.TSRwidth6 = PRO075.TSRwidth6;
                        objPRO075.TSRwidth7 = PRO075.TSRwidth7;
                        objPRO075.TSRwidth8 = PRO075.TSRwidth8;
                        objPRO075.TSRwidth9 = PRO075.TSRwidth9;
                        objPRO075.TSRwidth10 = PRO075.TSRwidth10;
                        objPRO075.TSRwidth11 = PRO075.TSRwidth11;
                        objPRO075.TSRwidth12 = PRO075.TSRwidth12;
                        objPRO075.TSRheight1 = PRO075.TSRheight1;
                        objPRO075.TSRheight2 = PRO075.TSRheight2;
                        objPRO075.TSRheight3 = PRO075.TSRheight3;
                        objPRO075.TSRheight4 = PRO075.TSRheight4;
                        objPRO075.TSRheight5 = PRO075.TSRheight5;
                        objPRO075.TSRheight6 = PRO075.TSRheight6;
                        objPRO075.TSRheight7 = PRO075.TSRheight7;
                        objPRO075.TSRheight8 = PRO075.TSRheight8;
                        objPRO075.TSRheight9 = PRO075.TSRheight9;
                        objPRO075.TSRheight10 = PRO075.TSRheight10;
                        objPRO075.TSRheight11 = PRO075.TSRheight11;
                        objPRO075.TSRheight12 = PRO075.TSRheight12;
                        objPRO075.TSRID1 = PRO075.TSRID1;
                        objPRO075.TSRID2 = PRO075.TSRID2;
                        objPRO075.TSRID3 = PRO075.TSRID3;
                        objPRO075.TSRID4 = PRO075.TSRID4;
                        objPRO075.TSRID5 = PRO075.TSRID5;
                        objPRO075.TSRID6 = PRO075.TSRID6;
                        objPRO075.TSRID7 = PRO075.TSRID7;
                        objPRO075.TSRID8 = PRO075.TSRID8;
                        objPRO075.TSRID9 = PRO075.TSRID9;
                        objPRO075.TSRID10 = PRO075.TSRID10;
                        objPRO075.TSRID11 = PRO075.TSRID11;
                        objPRO075.TSRID12 = PRO075.TSRID12;
                        objPRO075.levelnessact = PRO075.levelnessact;
                        objPRO075.tilting11 = PRO075.tilting11;
                        objPRO075.tilting12 = PRO075.tilting12;
                        objPRO075.tilting13 = PRO075.tilting13;
                        objPRO075.tilting14 = PRO075.tilting14;
                        objPRO075.tilting15 = PRO075.tilting15;
                        objPRO075.tilting16 = PRO075.tilting16;
                        objPRO075.tilting17 = PRO075.tilting17;
                        objPRO075.tilting18 = PRO075.tilting18;
                        objPRO075.tilting19 = PRO075.tilting19;
                        objPRO075.tilting110 = PRO075.tilting110;
                        objPRO075.tilting111 = PRO075.tilting111;
                        objPRO075.tilting112 = PRO075.tilting112;
                        objPRO075.tilting21 = PRO075.tilting21;
                        objPRO075.tilting22 = PRO075.tilting22;
                        objPRO075.tilting23 = PRO075.tilting23;
                        objPRO075.tilting24 = PRO075.tilting24;
                        objPRO075.tilting25 = PRO075.tilting25;
                        objPRO075.tilting26 = PRO075.tilting26;
                        objPRO075.tilting27 = PRO075.tilting27;
                        objPRO075.tilting28 = PRO075.tilting28;
                        objPRO075.tilting29 = PRO075.tilting29;
                        objPRO075.tilting210 = PRO075.tilting210;
                        objPRO075.tilting211 = PRO075.tilting211;
                        objPRO075.tilting212 = PRO075.tilting212;
                        objPRO075.flatness11 = PRO075.flatness11;
                        objPRO075.flatness12 = PRO075.flatness12;
                        objPRO075.flatness13 = PRO075.flatness13;
                        objPRO075.flatness14 = PRO075.flatness14;
                        objPRO075.flatness15 = PRO075.flatness15;
                        objPRO075.flatness16 = PRO075.flatness16;
                        objPRO075.flatness17 = PRO075.flatness17;
                        objPRO075.flatness18 = PRO075.flatness18;
                        objPRO075.flatness19 = PRO075.flatness19;
                        objPRO075.flatness110 = PRO075.flatness110;
                        objPRO075.flatness111 = PRO075.flatness111;
                        objPRO075.flatness112 = PRO075.flatness112;
                        objPRO075.flatness21 = PRO075.flatness21;
                        objPRO075.flatness22 = PRO075.flatness22;
                        objPRO075.flatness23 = PRO075.flatness23;
                        objPRO075.flatness24 = PRO075.flatness24;
                        objPRO075.flatness25 = PRO075.flatness25;
                        objPRO075.flatness26 = PRO075.flatness26;
                        objPRO075.flatness27 = PRO075.flatness27;
                        objPRO075.flatness28 = PRO075.flatness28;
                        objPRO075.flatness29 = PRO075.flatness29;
                        objPRO075.flatness210 = PRO075.flatness210;
                        objPRO075.flatness211 = PRO075.flatness211;
                        objPRO075.flatness212 = PRO075.flatness212;
                        objPRO075.Remark2 = PRO075.Remark2;
                        objPRO075.Remark3 = PRO075.Remark3;
                        objPRO075.Remark4 = PRO075.Remark4;
                        objPRO075.Remark5_1 = PRO075.Remark5_1.HasValue ? Manager.getDateTime(PRO075.Remark5_1.Value.ToShortDateString()) : PRO075.Remark5_1;

                        objPRO075.EditedBy = objClsLoginInfo.UserName;
                        objPRO075.EditedOn = DateTime.Now;
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolSaved;
                        objResponseMsg.HeaderId = objPRO075.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolExists;
                    }
                }
            }

            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId > 0)
                {
                    PRO075 objPRO075 = db.PRO075.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO075 != null && string.IsNullOrWhiteSpace(objPRO075.ProtocolNo))
                    {
                        db.PRO075.Remove(objPRO075);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.TSR_DIMENSION_INSPECTION_REPORTS.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO075.HeaderId;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Linkage Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Linkage(int? id, int? m)
        {//PRL075
            PRL075 objPRL075 = new PRL075();

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();


            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            objPRL075 = db.PRL075.Where(x => x.HeaderId == id).FirstOrDefault();

            if (objPRL075 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL075.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL075);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL075 PRL075)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL075.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL075 objPRL075 = db.PRL075.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data

                    objPRL075.DCRNo = PRL075.DCRNo;
                    objPRL075.ProtocolNo = PRL075.ProtocolNo != null ? PRL075.ProtocolNo : "";
                    objPRL075.elevationreq = PRL075.elevationreq;
                    objPRL075.elevationtol = PRL075.elevationtol;
                    objPRL075.TSRwidthreq = PRL075.TSRwidthreq;
                    objPRL075.TSRwidthtol = PRL075.TSRwidthtol;
                    objPRL075.TSRheightreq = PRL075.TSRheightreq;
                    objPRL075.TSRheighttol = PRL075.TSRheighttol;
                    objPRL075.TSRIDreq = PRL075.TSRIDreq;
                    objPRL075.TSRIDtol = PRL075.TSRIDtol;
                    objPRL075.levelnesstol = PRL075.levelnesstol;
                    objPRL075.tiltingtol = PRL075.tiltingtol;
                    objPRL075.flatnesstol = PRL075.flatnesstol;
                    objPRL075.elevation1 = PRL075.elevation1;
                    objPRL075.elevation2 = PRL075.elevation2;
                    objPRL075.elevation3 = PRL075.elevation3;
                    objPRL075.elevation4 = PRL075.elevation4;
                    objPRL075.elevation5 = PRL075.elevation5;
                    objPRL075.elevation6 = PRL075.elevation6;
                    objPRL075.elevation7 = PRL075.elevation7;
                    objPRL075.elevation8 = PRL075.elevation8;
                    objPRL075.elevation9 = PRL075.elevation9;
                    objPRL075.elevation10 = PRL075.elevation10;
                    objPRL075.elevation11 = PRL075.elevation11;
                    objPRL075.elevation12 = PRL075.elevation12;
                    objPRL075.TSRwidth1 = PRL075.TSRwidth1;
                    objPRL075.TSRwidth2 = PRL075.TSRwidth2;
                    objPRL075.TSRwidth3 = PRL075.TSRwidth3;
                    objPRL075.TSRwidth4 = PRL075.TSRwidth4;
                    objPRL075.TSRwidth5 = PRL075.TSRwidth5;
                    objPRL075.TSRwidth6 = PRL075.TSRwidth6;
                    objPRL075.TSRwidth7 = PRL075.TSRwidth7;
                    objPRL075.TSRwidth8 = PRL075.TSRwidth8;
                    objPRL075.TSRwidth9 = PRL075.TSRwidth9;
                    objPRL075.TSRwidth10 = PRL075.TSRwidth10;
                    objPRL075.TSRwidth11 = PRL075.TSRwidth11;
                    objPRL075.TSRwidth12 = PRL075.TSRwidth12;
                    objPRL075.TSRheight1 = PRL075.TSRheight1;
                    objPRL075.TSRheight2 = PRL075.TSRheight2;
                    objPRL075.TSRheight3 = PRL075.TSRheight3;
                    objPRL075.TSRheight4 = PRL075.TSRheight4;
                    objPRL075.TSRheight5 = PRL075.TSRheight5;
                    objPRL075.TSRheight6 = PRL075.TSRheight6;
                    objPRL075.TSRheight7 = PRL075.TSRheight7;
                    objPRL075.TSRheight8 = PRL075.TSRheight8;
                    objPRL075.TSRheight9 = PRL075.TSRheight9;
                    objPRL075.TSRheight10 = PRL075.TSRheight10;
                    objPRL075.TSRheight11 = PRL075.TSRheight11;
                    objPRL075.TSRheight12 = PRL075.TSRheight12;
                    objPRL075.TSRID1 = PRL075.TSRID1;
                    objPRL075.TSRID2 = PRL075.TSRID2;
                    objPRL075.TSRID3 = PRL075.TSRID3;
                    objPRL075.TSRID4 = PRL075.TSRID4;
                    objPRL075.TSRID5 = PRL075.TSRID5;
                    objPRL075.TSRID6 = PRL075.TSRID6;
                    objPRL075.TSRID7 = PRL075.TSRID7;
                    objPRL075.TSRID8 = PRL075.TSRID8;
                    objPRL075.TSRID9 = PRL075.TSRID9;
                    objPRL075.TSRID10 = PRL075.TSRID10;
                    objPRL075.TSRID11 = PRL075.TSRID11;
                    objPRL075.TSRID12 = PRL075.TSRID12;
                    objPRL075.levelnessact = PRL075.levelnessact;
                    objPRL075.tilting11 = PRL075.tilting11;
                    objPRL075.tilting12 = PRL075.tilting12;
                    objPRL075.tilting13 = PRL075.tilting13;
                    objPRL075.tilting14 = PRL075.tilting14;
                    objPRL075.tilting15 = PRL075.tilting15;
                    objPRL075.tilting16 = PRL075.tilting16;
                    objPRL075.tilting17 = PRL075.tilting17;
                    objPRL075.tilting18 = PRL075.tilting18;
                    objPRL075.tilting19 = PRL075.tilting19;
                    objPRL075.tilting110 = PRL075.tilting110;
                    objPRL075.tilting111 = PRL075.tilting111;
                    objPRL075.tilting112 = PRL075.tilting112;
                    objPRL075.tilting21 = PRL075.tilting21;
                    objPRL075.tilting22 = PRL075.tilting22;
                    objPRL075.tilting23 = PRL075.tilting23;
                    objPRL075.tilting24 = PRL075.tilting24;
                    objPRL075.tilting25 = PRL075.tilting25;
                    objPRL075.tilting26 = PRL075.tilting26;
                    objPRL075.tilting27 = PRL075.tilting27;
                    objPRL075.tilting28 = PRL075.tilting28;
                    objPRL075.tilting29 = PRL075.tilting29;
                    objPRL075.tilting210 = PRL075.tilting210;
                    objPRL075.tilting211 = PRL075.tilting211;
                    objPRL075.tilting212 = PRL075.tilting212;
                    objPRL075.flatness11 = PRL075.flatness11;
                    objPRL075.flatness12 = PRL075.flatness12;
                    objPRL075.flatness13 = PRL075.flatness13;
                    objPRL075.flatness14 = PRL075.flatness14;
                    objPRL075.flatness15 = PRL075.flatness15;
                    objPRL075.flatness16 = PRL075.flatness16;
                    objPRL075.flatness17 = PRL075.flatness17;
                    objPRL075.flatness18 = PRL075.flatness18;
                    objPRL075.flatness19 = PRL075.flatness19;
                    objPRL075.flatness110 = PRL075.flatness110;
                    objPRL075.flatness111 = PRL075.flatness111;
                    objPRL075.flatness112 = PRL075.flatness112;
                    objPRL075.flatness21 = PRL075.flatness21;
                    objPRL075.flatness22 = PRL075.flatness22;
                    objPRL075.flatness23 = PRL075.flatness23;
                    objPRL075.flatness24 = PRL075.flatness24;
                    objPRL075.flatness25 = PRL075.flatness25;
                    objPRL075.flatness26 = PRL075.flatness26;
                    objPRL075.flatness27 = PRL075.flatness27;
                    objPRL075.flatness28 = PRL075.flatness28;
                    objPRL075.flatness29 = PRL075.flatness29;
                    objPRL075.flatness210 = PRL075.flatness210;
                    objPRL075.flatness211 = PRL075.flatness211;
                    objPRL075.flatness212 = PRL075.flatness212;
                    objPRL075.Remark2 = PRL075.Remark2;
                    objPRL075.Remark3 = PRL075.Remark3;
                    objPRL075.Remark4 = PRL075.Remark4;
                    objPRL075.Remark5 = PRL075.Remark5;
                    objPRL075.Remark5_1 = PRL075.Remark5_1;
                    objPRL075.Remark5_1 = PRL075.Remark5_1.HasValue ? Manager.getDateTime(PRL075.Remark5_1.Value.ToShortDateString()) : PRL075.Remark5_1;

                    objPRL075.EditedBy = objClsLoginInfo.UserName;
                    objPRL075.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.LinkageSaved;
                    objResponseMsg.HeaderId = objPRL075.HeaderId;

                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        
    }
}


