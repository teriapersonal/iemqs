﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol094Controller : clsBase
    {
        // GET: PROTOCOL/Protocol094
        string ControllerURL = "/PROTOCOL/Protocol094/";
        string Title = "Final Assembly Inspection of Rough Liquid Distribution Tray";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO465 objPRO465 = new PRO465();
            if (!id.HasValue)
            {
                try
                {
                    objPRO465.ProtocolNo = string.Empty;
                    objPRO465.CreatedBy = objClsLoginInfo.UserName;
                    objPRO465.CreatedOn = DateTime.Now;

                    #region PRO467
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "1",
                        Description = "TRAY OD",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "2",
                        Description = "HEIGHT OF RLDT",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "3",
                        Description = "GAP BETWEEN TRAY PANELS TO PANELS AT SUPPORT BEAM",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "4",
                        Description = "GAP BETWEEN TRAY PANELS TO TRAY PANELS AT BACKING BAR",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "5",
                        Description = "LEVELNESS OF TRAY",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "6",
                        Description = "DISTANCE BETWEEN RLDT BOTTOM SURFACE TO VLDT TOP SURFACE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "7",
                        Description = "SUPPORT ROD HOLES (QTY)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "8",
                        Description = "SUPPORT ROD HOLES (DIAMETER & PCD)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "9",
                        Description = "SUPPORT ROD HOLES (ORIENTATION)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "10",
                        Description = "HOLES IN TRAY PANELS (DIAMETER)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "11",
                        Description = "HOLES IN TRAY PANELS (QTY)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "12",
                        Description = "HOLES IN TRAY PANELS (PITCH)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "13",
                        Description = "BANDING PLATE HOLE (HOLE TO HOLE DISTANCE)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "14",
                        Description = "LIFTING LUG (SIZE & DIA)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "15",
                        Description = "LIFTING LUG (QTY)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "16",
                        Description = "GAP BETWEEN END OF SPLICE PLATE & BEAM FLANGE",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "17",
                        Description = "VERIFY TRAY PERFORATIONS ARE NOT DIRECTLY ABOVE THE VLDT RISER ASSEMBLY",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "18",
                        Description = "J-CLIPS (DIMENSION & QTY)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "19",
                        Description = "J-CLIPS (LOCATION)",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "20",
                        Description = "OTHER DIMENSIONS",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "21",
                        Description = "IDENTIFICATION & MATCH MARKING",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO465.PRO467.Add(new PRO467
                    {
                        SrNo = "22",
                        Description = "WELD & OVERALL VISUAL",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO465.Add(objPRO465);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO465.HeaderId;
            }
            else
            {
                objPRO465 = db.PRO465.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO466> lstPRO466 = db.PRO466.Where(x => x.HeaderId == objPRO465.HeaderId).ToList();
            List<PRO467> lstPRO467 = db.PRO467.Where(x => x.HeaderId == objPRO465.HeaderId).ToList();
            List<PRO468> lstPRO468 = db.PRO468.Where(x => x.HeaderId == objPRO465.HeaderId).ToList();

            ViewBag._lstPRO466 = lstPRO466;
            ViewBag._lstPRO467 = lstPRO467;
            ViewBag._lstPRO468 = lstPRO468;

            #endregion

            return View(objPRO465);
            //return View();
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO465 PRO465)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO465.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO465 objPRO465 = db.PRO465.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO465.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO465.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data

                        objPRO465.ProtocolNo = PRO465.ProtocolNo;

                        objPRO465.CheckPoint1 = PRO465.CheckPoint1;
                        objPRO465.CheckPoint2 = PRO465.CheckPoint2;
                        objPRO465.CheckPoint4_1_1 = PRO465.CheckPoint4_1_1;
                        objPRO465.CheckPoint4_1_2 = PRO465.CheckPoint4_1_2;
                        objPRO465.CheckPoint4_2_1 = PRO465.CheckPoint4_2_1;
                        objPRO465.CheckPoint4_2_2 = PRO465.CheckPoint4_2_2;
                        objPRO465.CheckPoint4_3_1 = PRO465.CheckPoint4_3_1;
                        objPRO465.CheckPoint4_3_2 = PRO465.CheckPoint4_3_2;
                        objPRO465.CheckPoint4_4_1 = PRO465.CheckPoint4_4_1;
                        objPRO465.CheckPoint4_4_2 = PRO465.CheckPoint4_4_2;
                        objPRO465.CheckPoint4_5_1 = PRO465.CheckPoint4_5_1;
                        objPRO465.CheckPoint4_5_2 = PRO465.CheckPoint4_5_2;

                        objPRO465.QCRemarks = PRO465.QCRemarks;
                        objPRO465.Result = PRO465.Result;

                        objPRO465.EditedBy = objClsLoginInfo.UserName;
                        objPRO465.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO465.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO465 objPRO465 = db.PRO465.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO465 != null && string.IsNullOrWhiteSpace(objPRO465.ProtocolNo))
                    {
                        db.PRO465.Remove(objPRO465);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO465.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO466> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO466> lstAddPRO466 = new List<PRO466>();
                List<PRO466> lstDeletePRO466 = new List<PRO466>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO466 obj = db.PRO466.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO466 obj = new PRO466();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO466.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO466.Count > 0)
                    {
                        db.PRO466.AddRange(lstAddPRO466);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO466 = db.PRO466.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO466.Count > 0)
                    {
                        db.PRO466.RemoveRange(lstDeletePRO466);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO466 = db.PRO466.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO466.Count > 0)
                    {
                        db.PRO466.RemoveRange(lstDeletePRO466);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO467> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO467> lstAddPRO467 = new List<PRO467>();
                List<PRO467> lstDeletePRO467 = new List<PRO467>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO467 obj = db.PRO467.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO467();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRO467.Add(obj);
                        }
                    }
                    if (lstAddPRO467.Count > 0)
                    {
                        db.PRO467.AddRange(lstAddPRO467);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO467 = db.PRO467.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO467.Count > 0)
                    {
                        db.PRO467.RemoveRange(lstDeletePRO467);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO467 = db.PRO467.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO467.Count > 0)
                    {
                        db.PRO467.RemoveRange(lstDeletePRO467);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO468> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO468> lstAddPRO468 = new List<PRO468>();
                List<PRO468> lstDeletePRO468 = new List<PRO468>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO468 obj = db.PRO468.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO468();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.MaterialIdentification = item.MaterialIdentification;

                        if (isAdded)
                        {
                            lstAddPRO468.Add(obj);
                        }
                    }
                    if (lstAddPRO468.Count > 0)
                    {
                        db.PRO468.AddRange(lstAddPRO468);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO468 = db.PRO468.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO468.Count > 0)
                    {
                        db.PRO468.RemoveRange(lstDeletePRO468);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO468 = db.PRO468.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO468.Count > 0)
                    {
                        db.PRO468.RemoveRange(lstDeletePRO468);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL465 objPRL465 = new PRL465();
            objPRL465 = db.PRL465.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL465 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL465.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL466> lstPRL466 = db.PRL466.Where(x => x.HeaderId == objPRL465.HeaderId).ToList();
            List<PRL467> lstPRL467 = db.PRL467.Where(x => x.HeaderId == objPRL465.HeaderId).ToList();
            List<PRL468> lstPRL468 = db.PRL468.Where(x => x.HeaderId == objPRL465.HeaderId).ToList();

            ViewBag._lstPRL466 = lstPRL466;
            ViewBag._lstPRL467 = lstPRL467;
            ViewBag._lstPRL468 = lstPRL468;

            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL465.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL465.ActFilledBy) && (objPRL465.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL465.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL465);

            //return View();
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL465 prl465, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl465.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL465 objPRL465 = db.PRL465.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL465.DrawingRevisionNo = prl465.DrawingRevisionNo;
                    objPRL465.DrawingNo = prl465.DrawingNo;
                    objPRL465.DCRNo = prl465.DCRNo;
                    objPRL465.ProtocolNo = prl465.ProtocolNo;

                    objPRL465.CheckPoint1 = prl465.CheckPoint1;
                    objPRL465.CheckPoint2 = prl465.CheckPoint2;
                    objPRL465.CheckPoint4_1_1 = prl465.CheckPoint4_1_1;
                    objPRL465.CheckPoint4_1_2 = prl465.CheckPoint4_1_2;
                    objPRL465.CheckPoint4_2_1 = prl465.CheckPoint4_2_1;
                    objPRL465.CheckPoint4_2_2 = prl465.CheckPoint4_2_2;
                    objPRL465.CheckPoint4_3_1 = prl465.CheckPoint4_3_1;
                    objPRL465.CheckPoint4_3_2 = prl465.CheckPoint4_3_2;
                    objPRL465.CheckPoint4_4_1 = prl465.CheckPoint4_4_1;
                    objPRL465.CheckPoint4_4_2 = prl465.CheckPoint4_4_2;
                    objPRL465.CheckPoint4_5_1 = prl465.CheckPoint4_5_1;
                    objPRL465.CheckPoint4_5_2 = prl465.CheckPoint4_5_2;

                    objPRL465.QCRemarks = prl465.QCRemarks;
                    objPRL465.Result = prl465.Result;

                    objPRL465.EditedBy = UserName;
                    objPRL465.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL465.ActFilledBy = UserName;
                            objPRL465.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL465.ReqFilledBy = UserName;
                            objPRL465.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL465.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL465 objPRL465 = db.PRL465.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL465 != null && string.IsNullOrWhiteSpace(objPRL465.ProtocolNo))
                    {
                        db.PRL465.Remove(objPRL465);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL465.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL466> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL466> lstAddPRL466 = new List<PRL466>();
                List<PRL466> lstDeletePRL466 = new List<PRL466>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL466 obj = db.PRL466.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL466 obj = new PRL466();

                            if (!string.IsNullOrWhiteSpace(item.DrgNo))
                            {
                                obj.HeaderId = item.HeaderId;
                                obj.DrgNo = item.DrgNo;
                                obj.RevNo = item.RevNo;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL466.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL466.Count > 0)
                    {
                        db.PRL466.AddRange(lstAddPRL466);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL466 = db.PRL466.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL466.Count > 0)
                    {
                        db.PRL466.RemoveRange(lstDeletePRL466);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL466 = db.PRL466.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL466.Count > 0)
                    {
                        db.PRL466.RemoveRange(lstDeletePRL466);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL467> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL467> lstAddPRL467 = new List<PRL467>();
                List<PRL467> lstDeletePRL467 = new List<PRL467>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL467 obj = db.PRL467.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL467();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SrNo = item.SrNo;
                        obj.Description = item.Description;
                        obj.ReqDimension = item.ReqDimension;
                        obj.ActDimension = item.ActDimension;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddPRL467.Add(obj);
                        }
                    }
                    if (lstAddPRL467.Count > 0)
                    {
                        db.PRL467.AddRange(lstAddPRL467);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL467 = db.PRL467.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL467.Count > 0)
                    {
                        db.PRL467.RemoveRange(lstDeletePRL467);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL467 = db.PRL467.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL467.Count > 0)
                    {
                        db.PRL467.RemoveRange(lstDeletePRL467);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL468> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRL468> lstAddPRL468 = new List<PRL468>();
                List<PRL468> lstDeletePRL468 = new List<PRL468>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL468 obj = db.PRL468.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL468();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.MaterialIdentification = item.MaterialIdentification;

                        if (isAdded)
                        {
                            lstAddPRL468.Add(obj);
                        }
                    }
                    if (lstAddPRL468.Count > 0)
                    {
                        db.PRL468.AddRange(lstAddPRL468);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL468 = db.PRL468.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL468.Count > 0)
                    {
                        db.PRL468.RemoveRange(lstDeletePRL468);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL468 = db.PRL468.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL468.Count > 0)
                    {
                        db.PRL468.RemoveRange(lstDeletePRL468);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion
    }
}