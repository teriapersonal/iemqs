﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol019Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol019/";
        string Title = "VISUAL & DIMENSION INSPECTION OF SKIRT TEMPLATE";
        // GET: PROTOCOL/Protocol019
        #region Protocol Details Page

        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id,int? m)
        {//PRO090 
            PRO090 objPRO090 = new PRO090();
            if (!id.HasValue)
            {
                objPRO090.ProtocolNo = string.Empty;
                objPRO090.CreatedBy = objClsLoginInfo.UserName;
                objPRO090.CreatedOn = DateTime.Now;
                db.PRO090.Add(objPRO090);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO090.HeaderId;
            }
            else
            {
                objPRO090 = db.PRO090.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstyesnona = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstyesnona.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO090);
        }


        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO090 PRO090)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO090.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO090 objPRO090 = db.PRO090.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    var ProtocolExist = db.PRO090.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO090.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {

                        objPRO090.ProtocolNo = PRO090.ProtocolNo;
                        objPRO090.ReqDimension1 = PRO090.ReqDimension1;
                        objPRO090.ReqDimension2 = PRO090.ReqDimension2;
                        objPRO090.ReqDimension3 = PRO090.ReqDimension3;
                        objPRO090.ReqDimension4 = PRO090.ReqDimension4;
                        objPRO090.ReqDimension5 = PRO090.ReqDimension5;
                        objPRO090.ReqDimension6 = PRO090.ReqDimension6;
                        objPRO090.ReqDimension7 = PRO090.ReqDimension7;
                        objPRO090.ReqDimension8 = PRO090.ReqDimension8;
                        objPRO090.ReqDimension9 = PRO090.ReqDimension9;
                        objPRO090.ReqDimension10 = PRO090.ReqDimension10;
                        objPRO090.ReqDimension11 = PRO090.ReqDimension11;
                        objPRO090.ActDimension1 = PRO090.ActDimension1;
                        objPRO090.ActDimension2 = PRO090.ActDimension2;
                        objPRO090.ActDimension3 = PRO090.ActDimension3;
                        objPRO090.ActDimension4 = PRO090.ActDimension4;
                        objPRO090.ActDimension5 = PRO090.ActDimension5;
                        objPRO090.ActDimension6 = PRO090.ActDimension6;
                        objPRO090.ActDimension7 = PRO090.ActDimension7;
                        objPRO090.ActDimension8 = PRO090.ActDimension8;
                        objPRO090.ActDimension9 = PRO090.ActDimension9;
                        objPRO090.ActDimension10 = PRO090.ActDimension10;
                        objPRO090.ActDimension11 = PRO090.ActDimension11;
                        objPRO090.AppTolerance11 = PRO090.AppTolerance11;
                        objPRO090.AppTolerance12 = PRO090.AppTolerance12;
                        objPRO090.AppTolerance13 = PRO090.AppTolerance13;
                        objPRO090.AppTolerance14 = PRO090.AppTolerance14;
                        objPRO090.AppTolerance15 = PRO090.AppTolerance15;
                        objPRO090.AppTolerance16 = PRO090.AppTolerance16;
                        objPRO090.AppTolerance17 = PRO090.AppTolerance17;
                        objPRO090.AppTolerance18 = PRO090.AppTolerance18;
                        objPRO090.AppTolerance19 = PRO090.AppTolerance19;
                        objPRO090.AppTolerance110 = PRO090.AppTolerance110;
                        objPRO090.AppTolerance111 = PRO090.AppTolerance111;
                        objPRO090.AppTolerance21 = PRO090.AppTolerance21;
                        objPRO090.AppTolerance22 = PRO090.AppTolerance22;
                        objPRO090.AppTolerance23 = PRO090.AppTolerance23;
                        objPRO090.AppTolerance24 = PRO090.AppTolerance24;
                        objPRO090.AppTolerance25 = PRO090.AppTolerance25;
                        objPRO090.AppTolerance26 = PRO090.AppTolerance26;
                        objPRO090.AppTolerance27 = PRO090.AppTolerance27;
                        objPRO090.AppTolerance28 = PRO090.AppTolerance28;
                        objPRO090.AppTolerance29 = PRO090.AppTolerance29;
                        objPRO090.AppTolerance210 = PRO090.AppTolerance210;
                        objPRO090.AppTolerance211 = PRO090.AppTolerance211;
                        objPRO090.BcdDim1 = PRO090.BcdDim1;
                        objPRO090.BcdDim2 = PRO090.BcdDim2;
                        objPRO090.BcdDim3 = PRO090.BcdDim3;
                        objPRO090.BcdDim4 = PRO090.BcdDim4;
                        objPRO090.BcdDim5 = PRO090.BcdDim5;
                        objPRO090.BcdDim6 = PRO090.BcdDim6;
                        objPRO090.BcdDim7 = PRO090.BcdDim7;
                        objPRO090.BcdDim8 = PRO090.BcdDim8;
                        objPRO090.BcdDim9 = PRO090.BcdDim9;
                        objPRO090.BcdDim10 = PRO090.BcdDim10;
                        objPRO090.BcdDim11 = PRO090.BcdDim11;
                        objPRO090.BcdDim12 = PRO090.BcdDim12;
                        objPRO090.BcdDim13 = PRO090.BcdDim13;
                        objPRO090.BcdDim14 = PRO090.BcdDim14;
                        objPRO090.BcdDim15 = PRO090.BcdDim15;
                        objPRO090.BcdDim16 = PRO090.BcdDim16;
                        objPRO090.BcdDim17 = PRO090.BcdDim17;
                        objPRO090.BcdDim18 = PRO090.BcdDim18;
                        objPRO090.BcdDim19 = PRO090.BcdDim19;
                        objPRO090.BcdDim20 = PRO090.BcdDim20;
                        objPRO090.BcdDim21 = PRO090.BcdDim21;
                        objPRO090.BcdDim22 = PRO090.BcdDim22;
                        objPRO090.PitchDim1 = PRO090.PitchDim1;
                        objPRO090.PitchDim2 = PRO090.PitchDim2;
                        objPRO090.PitchDim3 = PRO090.PitchDim3;
                        objPRO090.PitchDim4 = PRO090.PitchDim4;
                        objPRO090.PitchDim5 = PRO090.PitchDim5;
                        objPRO090.PitchDim6 = PRO090.PitchDim6;
                        objPRO090.PitchDim7 = PRO090.PitchDim7;
                        objPRO090.PitchDim8 = PRO090.PitchDim8;
                        objPRO090.PitchDim9 = PRO090.PitchDim9;
                        objPRO090.PitchDim10 = PRO090.PitchDim10;
                        objPRO090.PitchDim11 = PRO090.PitchDim11;
                        objPRO090.PitchDim12 = PRO090.PitchDim12;
                        objPRO090.PitchDim13 = PRO090.PitchDim13;
                        objPRO090.PitchDim14 = PRO090.PitchDim14;
                        objPRO090.PitchDim15 = PRO090.PitchDim15;
                        objPRO090.PitchDim16 = PRO090.PitchDim16;
                        objPRO090.PitchDim17 = PRO090.PitchDim17;
                        objPRO090.PitchDim18 = PRO090.PitchDim18;
                        objPRO090.PitchDim19 = PRO090.PitchDim19;
                        objPRO090.PitchDim20 = PRO090.PitchDim20;
                        objPRO090.PitchDim21 = PRO090.PitchDim21;
                        objPRO090.PitchDim22 = PRO090.PitchDim22;
                        objPRO090.PitchDim23 = PRO090.PitchDim23;
                        objPRO090.PitchDim24 = PRO090.PitchDim24;
                        objPRO090.PitchDim25 = PRO090.PitchDim25;
                        objPRO090.PitchDim26 = PRO090.PitchDim26;
                        objPRO090.PitchDim27 = PRO090.PitchDim27;
                        objPRO090.PitchDim28 = PRO090.PitchDim28;
                        objPRO090.PitchDim29 = PRO090.PitchDim29;
                        objPRO090.PitchDim30 = PRO090.PitchDim30;
                        objPRO090.PitchDim31 = PRO090.PitchDim31;
                        objPRO090.PitchDim32 = PRO090.PitchDim32;
                        objPRO090.PitchDim33 = PRO090.PitchDim33;
                        objPRO090.PitchDim34 = PRO090.PitchDim34;
                        objPRO090.PitchDim35 = PRO090.PitchDim35;
                        objPRO090.PitchDim36 = PRO090.PitchDim36;
                        objPRO090.PitchDim37 = PRO090.PitchDim37;
                        objPRO090.PitchDim38 = PRO090.PitchDim38;
                        objPRO090.PitchDim39 = PRO090.PitchDim39;
                        objPRO090.PitchDim40 = PRO090.PitchDim40;
                        objPRO090.PitchDim41 = PRO090.PitchDim41;
                        objPRO090.PitchDim42 = PRO090.PitchDim42;
                        objPRO090.PitchDim43 = PRO090.PitchDim43;
                        objPRO090.PitchDim44 = PRO090.PitchDim44;
                        objPRO090.HeightReq = PRO090.HeightReq;
                        objPRO090.HeightAct1 = PRO090.HeightAct1;
                        objPRO090.HeightAct2 = PRO090.HeightAct2;
                        objPRO090.HeightAct3 = PRO090.HeightAct3;
                        objPRO090.HeightAct4 = PRO090.HeightAct4;
                        objPRO090.HeightAct5 = PRO090.HeightAct5;
                        objPRO090.HeightAct6 = PRO090.HeightAct6;
                        objPRO090.HeightAct7 = PRO090.HeightAct7;
                        objPRO090.HeightAct8 = PRO090.HeightAct8;
                        objPRO090.HeightAct9 = PRO090.HeightAct9;
                        objPRO090.HeightAct10 = PRO090.HeightAct10;
                        objPRO090.DiameterReq = PRO090.DiameterReq;
                        objPRO090.DiameterAct1 = PRO090.DiameterAct1;
                        objPRO090.DiameterAct2 = PRO090.DiameterAct2;
                        objPRO090.DiameterAct3 = PRO090.DiameterAct3;
                        objPRO090.DiameterAct4 = PRO090.DiameterAct4;
                        objPRO090.DiameterAct5 = PRO090.DiameterAct5;
                        objPRO090.HoleA1 = PRO090.HoleA1;
                        objPRO090.HoleA2 = PRO090.HoleA2;
                        objPRO090.HoleA3 = PRO090.HoleA3;
                        objPRO090.HoleA4 = PRO090.HoleA4;
                        objPRO090.HoleA5 = PRO090.HoleA5;
                        objPRO090.HoleA6 = PRO090.HoleA6;
                        objPRO090.HoleA7 = PRO090.HoleA7;
                        objPRO090.HoleA8 = PRO090.HoleA8;
                        objPRO090.HoleA9 = PRO090.HoleA9;
                        objPRO090.HoleA10 = PRO090.HoleA10;
                        objPRO090.HoleA11 = PRO090.HoleA11;
                        objPRO090.HoleA12 = PRO090.HoleA12;
                        objPRO090.HoleA13 = PRO090.HoleA13;
                        objPRO090.HoleA14 = PRO090.HoleA14;
                        objPRO090.HoleA15 = PRO090.HoleA15;
                        objPRO090.HoleA16 = PRO090.HoleA16;
                        objPRO090.HoleA17 = PRO090.HoleA17;
                        objPRO090.HoleA18 = PRO090.HoleA18;
                        objPRO090.HoleA19 = PRO090.HoleA19;
                        objPRO090.HoleA20 = PRO090.HoleA20;
                        objPRO090.HoleA21 = PRO090.HoleA21;
                        objPRO090.HoleA22 = PRO090.HoleA22;
                        objPRO090.HoleA23 = PRO090.HoleA23;
                        objPRO090.HoleA24 = PRO090.HoleA24;
                        objPRO090.HoleA25 = PRO090.HoleA25;
                        objPRO090.HoleA26 = PRO090.HoleA26;
                        objPRO090.HoleA27 = PRO090.HoleA27;
                        objPRO090.HoleA28 = PRO090.HoleA28;
                        objPRO090.HoleA29 = PRO090.HoleA29;
                        objPRO090.HoleA30 = PRO090.HoleA30;
                        objPRO090.HoleA31 = PRO090.HoleA31;
                        objPRO090.HoleA32 = PRO090.HoleA32;
                        objPRO090.HoleA33 = PRO090.HoleA33;
                        objPRO090.HoleA34 = PRO090.HoleA34;
                        objPRO090.HoleA35 = PRO090.HoleA35;
                        objPRO090.HoleA36 = PRO090.HoleA36;
                        objPRO090.HoleA37 = PRO090.HoleA37;
                        objPRO090.HoleA38 = PRO090.HoleA38;
                        objPRO090.HoleA39 = PRO090.HoleA39;
                        objPRO090.HoleA40 = PRO090.HoleA40;
                        objPRO090.HoleA41 = PRO090.HoleA41;
                        objPRO090.HoleA42 = PRO090.HoleA42;
                        objPRO090.HoleA43 = PRO090.HoleA43;
                        objPRO090.HoleB1 = PRO090.HoleB1;
                        objPRO090.HoleB2 = PRO090.HoleB2;
                        objPRO090.HoleB3 = PRO090.HoleB3;
                        objPRO090.HoleB4 = PRO090.HoleB4;
                        objPRO090.HoleB5 = PRO090.HoleB5;
                        objPRO090.HoleB6 = PRO090.HoleB6;
                        objPRO090.HoleB7 = PRO090.HoleB7;
                        objPRO090.HoleB8 = PRO090.HoleB8;
                        objPRO090.HoleB9 = PRO090.HoleB9;
                        objPRO090.HoleB10 = PRO090.HoleB10;
                        objPRO090.HoleB11 = PRO090.HoleB11;
                        objPRO090.HoleB12 = PRO090.HoleB12;
                        objPRO090.HoleB13 = PRO090.HoleB13;
                        objPRO090.HoleB14 = PRO090.HoleB14;
                        objPRO090.HoleB15 = PRO090.HoleB15;
                        objPRO090.HoleB16 = PRO090.HoleB16;
                        objPRO090.HoleB17 = PRO090.HoleB17;
                        objPRO090.HoleB18 = PRO090.HoleB18;
                        objPRO090.HoleB19 = PRO090.HoleB19;
                        objPRO090.HoleB20 = PRO090.HoleB20;
                        objPRO090.HoleB21 = PRO090.HoleB21;
                        objPRO090.HoleB22 = PRO090.HoleB22;
                        objPRO090.HoleB23 = PRO090.HoleB23;
                        objPRO090.HoleB24 = PRO090.HoleB24;
                        objPRO090.HoleB25 = PRO090.HoleB25;
                        objPRO090.HoleB26 = PRO090.HoleB26;
                        objPRO090.HoleB27 = PRO090.HoleB27;
                        objPRO090.HoleB28 = PRO090.HoleB28;
                        objPRO090.HoleB29 = PRO090.HoleB29;
                        objPRO090.HoleB30 = PRO090.HoleB30;
                        objPRO090.HoleB31 = PRO090.HoleB31;
                        objPRO090.HoleB32 = PRO090.HoleB32;
                        objPRO090.HoleB33 = PRO090.HoleB33;
                        objPRO090.HoleB34 = PRO090.HoleB34;
                        objPRO090.HoleB35 = PRO090.HoleB35;
                        objPRO090.HoleB36 = PRO090.HoleB36;
                        objPRO090.HoleB37 = PRO090.HoleB37;
                        objPRO090.HoleB38 = PRO090.HoleB38;
                        objPRO090.HoleB39 = PRO090.HoleB39;
                        objPRO090.HoleB40 = PRO090.HoleB40;
                        objPRO090.HoleB41 = PRO090.HoleB41;
                        objPRO090.HoleB42 = PRO090.HoleB42;
                        objPRO090.HoleB43 = PRO090.HoleB43;
                        objPRO090.BoltHoleDia1 = PRO090.BoltHoleDia1;
                        objPRO090.BoltHoleDia2 = PRO090.BoltHoleDia2;
                        objPRO090.BoltHoleDia3 = PRO090.BoltHoleDia3;
                        objPRO090.BoltHoleDia4 = PRO090.BoltHoleDia4;
                        objPRO090.BoltHoleDia5 = PRO090.BoltHoleDia5;
                        objPRO090.BoltHoleDia6 = PRO090.BoltHoleDia6;
                        objPRO090.BoltHoleDia7 = PRO090.BoltHoleDia7;
                        objPRO090.BoltHoleDia8 = PRO090.BoltHoleDia8;
                        objPRO090.BoltHoleDia9 = PRO090.BoltHoleDia9;
                        objPRO090.BoltHoleDia10 = PRO090.BoltHoleDia10;
                        objPRO090.BoltHoleDia11 = PRO090.BoltHoleDia11;
                        objPRO090.BoltHoleDia12 = PRO090.BoltHoleDia12;
                        objPRO090.BoltHoleDia13 = PRO090.BoltHoleDia13;
                        objPRO090.BoltHoleDia14 = PRO090.BoltHoleDia14;
                        objPRO090.BoltHoleDia15 = PRO090.BoltHoleDia15;
                        objPRO090.BoltHoleDia16 = PRO090.BoltHoleDia16;
                        objPRO090.BoltHoleDia17 = PRO090.BoltHoleDia17;
                        objPRO090.BoltHoleDia18 = PRO090.BoltHoleDia18;
                        objPRO090.BoltHoleDia19 = PRO090.BoltHoleDia19;
                        objPRO090.BoltHoleDia20 = PRO090.BoltHoleDia20;
                        objPRO090.BoltHoleDia21 = PRO090.BoltHoleDia21;
                        objPRO090.BoltHoleDia22 = PRO090.BoltHoleDia22;
                        objPRO090.BoltHoleDia23 = PRO090.BoltHoleDia23;
                        objPRO090.BoltHoleDia24 = PRO090.BoltHoleDia24;
                        objPRO090.BoltHoleDia25 = PRO090.BoltHoleDia25;
                        objPRO090.BoltHoleDia26 = PRO090.BoltHoleDia26;
                        objPRO090.BoltHoleDia27 = PRO090.BoltHoleDia27;
                        objPRO090.BoltHoleDia28 = PRO090.BoltHoleDia28;
                        objPRO090.BoltHoleDia29 = PRO090.BoltHoleDia29;
                        objPRO090.BoltHoleDia30 = PRO090.BoltHoleDia30;
                        objPRO090.BoltHoleDia31 = PRO090.BoltHoleDia31;
                        objPRO090.BoltHoleDia32 = PRO090.BoltHoleDia32;
                        objPRO090.BoltHoleDia33 = PRO090.BoltHoleDia33;
                        objPRO090.BoltHoleDia34 = PRO090.BoltHoleDia34;
                        objPRO090.BoltHoleDia35 = PRO090.BoltHoleDia35;
                        objPRO090.BoltHoleDia36 = PRO090.BoltHoleDia36;
                        objPRO090.BoltHoleDia37 = PRO090.BoltHoleDia37;
                        objPRO090.BoltHoleDia38 = PRO090.BoltHoleDia38;
                        objPRO090.BoltHoleDia39 = PRO090.BoltHoleDia39;
                        objPRO090.BoltHoleDia40 = PRO090.BoltHoleDia40;
                        objPRO090.BoltHoleDia41 = PRO090.BoltHoleDia41;
                        objPRO090.BoltHoleDia42 = PRO090.BoltHoleDia42;
                        objPRO090.BoltHoleDia43 = PRO090.BoltHoleDia43;
                        objPRO090.BoltHoleDia44 = PRO090.BoltHoleDia44;
                        objPRO090.Remark1 = PRO090.Remark1;
                        objPRO090.Remark2 = PRO090.Remark2;
                        objPRO090.Remark3 = PRO090.Remark3;
                        objPRO090.Remark4 = PRO090.Remark4;
                        objPRO090.Remark5 = PRO090.Remark5;
                        objPRO090.Remark6 = PRO090.Remark6;
                        objPRO090.Remark7 = PRO090.Remark7;
                        objPRO090.Remark8 = PRO090.Remark8;
                        objPRO090.Remark9 = PRO090.Remark9;
                        objPRO090.Remark10 = PRO090.Remark10;
                        objPRO090.Remark11 = PRO090.Remark11;
                        objPRO090.Remark12 = PRO090.Remark12;
                        objPRO090.Remark13 = PRO090.Remark13;
                        objPRO090.EditedBy = objClsLoginInfo.UserName;
                        objPRO090.EditedOn = DateTime.Now;


                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolSaved;
                        objResponseMsg.HeaderId = objPRO090.HeaderId;

                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolExists;
                    }

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO090 objPRO090 = db.PRO090.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO090 != null && string.IsNullOrWhiteSpace(objPRO090.ProtocolNo))
                    {
                        db.PRO090.Remove(objPRO090);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_INSPECTION_OF_SKIRT_TEMPLATE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO090.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Linkage Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        public ActionResult Linkage(int? id, int? m)
        {//PRL090
            PRL090 objPRL090 = new PRL090();

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();


            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            objPRL090 = db.PRL090.Where(x => x.HeaderId == id).FirstOrDefault();

            if (objPRL090 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL090.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL090);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL090 PRL090)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL090.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {

                    PRL090 objPRL090 = db.PRL090.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    #region Save Data
                    objPRL090.DCRNo = PRL090.DCRNo;
                    objPRL090.ProtocolNo = PRL090.ProtocolNo != null ? PRL090.ProtocolNo : "";
                    objPRL090.ReqDimension1 = PRL090.ReqDimension1;
                    objPRL090.ReqDimension2 = PRL090.ReqDimension2;
                    objPRL090.ReqDimension3 = PRL090.ReqDimension3;
                    objPRL090.ReqDimension4 = PRL090.ReqDimension4;
                    objPRL090.ReqDimension5 = PRL090.ReqDimension5;
                    objPRL090.ReqDimension6 = PRL090.ReqDimension6;
                    objPRL090.ReqDimension7 = PRL090.ReqDimension7;
                    objPRL090.ReqDimension8 = PRL090.ReqDimension8;
                    objPRL090.ReqDimension9 = PRL090.ReqDimension9;
                    objPRL090.ReqDimension10 = PRL090.ReqDimension10;
                    objPRL090.ReqDimension11 = PRL090.ReqDimension11;
                    objPRL090.ActDimension1 = PRL090.ActDimension1;
                    objPRL090.ActDimension2 = PRL090.ActDimension2;
                    objPRL090.ActDimension3 = PRL090.ActDimension3;
                    objPRL090.ActDimension4 = PRL090.ActDimension4;
                    objPRL090.ActDimension5 = PRL090.ActDimension5;
                    objPRL090.ActDimension6 = PRL090.ActDimension6;
                    objPRL090.ActDimension7 = PRL090.ActDimension7;
                    objPRL090.ActDimension8 = PRL090.ActDimension8;
                    objPRL090.ActDimension9 = PRL090.ActDimension9;
                    objPRL090.ActDimension10 = PRL090.ActDimension10;
                    objPRL090.ActDimension11 = PRL090.ActDimension11;
                    objPRL090.AppTolerance11 = PRL090.AppTolerance11;
                    objPRL090.AppTolerance12 = PRL090.AppTolerance12;
                    objPRL090.AppTolerance13 = PRL090.AppTolerance13;
                    objPRL090.AppTolerance14 = PRL090.AppTolerance14;
                    objPRL090.AppTolerance15 = PRL090.AppTolerance15;
                    objPRL090.AppTolerance16 = PRL090.AppTolerance16;
                    objPRL090.AppTolerance17 = PRL090.AppTolerance17;
                    objPRL090.AppTolerance18 = PRL090.AppTolerance18;
                    objPRL090.AppTolerance19 = PRL090.AppTolerance19;
                    objPRL090.AppTolerance110 = PRL090.AppTolerance110;
                    objPRL090.AppTolerance111 = PRL090.AppTolerance111;
                    objPRL090.AppTolerance21 = PRL090.AppTolerance21;
                    objPRL090.AppTolerance22 = PRL090.AppTolerance22;
                    objPRL090.AppTolerance23 = PRL090.AppTolerance23;
                    objPRL090.AppTolerance24 = PRL090.AppTolerance24;
                    objPRL090.AppTolerance25 = PRL090.AppTolerance25;
                    objPRL090.AppTolerance26 = PRL090.AppTolerance26;
                    objPRL090.AppTolerance27 = PRL090.AppTolerance27;
                    objPRL090.AppTolerance28 = PRL090.AppTolerance28;
                    objPRL090.AppTolerance29 = PRL090.AppTolerance29;
                    objPRL090.AppTolerance210 = PRL090.AppTolerance210;
                    objPRL090.AppTolerance211 = PRL090.AppTolerance211;
                    objPRL090.BcdDim1 = PRL090.BcdDim1;
                    objPRL090.BcdDim2 = PRL090.BcdDim2;
                    objPRL090.BcdDim3 = PRL090.BcdDim3;
                    objPRL090.BcdDim4 = PRL090.BcdDim4;
                    objPRL090.BcdDim5 = PRL090.BcdDim5;
                    objPRL090.BcdDim6 = PRL090.BcdDim6;
                    objPRL090.BcdDim7 = PRL090.BcdDim7;
                    objPRL090.BcdDim8 = PRL090.BcdDim8;
                    objPRL090.BcdDim9 = PRL090.BcdDim9;
                    objPRL090.BcdDim10 = PRL090.BcdDim10;
                    objPRL090.BcdDim11 = PRL090.BcdDim11;
                    objPRL090.BcdDim12 = PRL090.BcdDim12;
                    objPRL090.BcdDim13 = PRL090.BcdDim13;
                    objPRL090.BcdDim14 = PRL090.BcdDim14;
                    objPRL090.BcdDim1 = PRL090.BcdDim15;
                    objPRL090.BcdDim16 = PRL090.BcdDim16;
                    objPRL090.BcdDim17 = PRL090.BcdDim17;
                    objPRL090.BcdDim18 = PRL090.BcdDim18;
                    objPRL090.BcdDim19 = PRL090.BcdDim19;
                    objPRL090.BcdDim20 = PRL090.BcdDim20;
                    objPRL090.BcdDim21 = PRL090.BcdDim21;
                    objPRL090.BcdDim22 = PRL090.BcdDim22;
                    objPRL090.PitchDim1 = PRL090.PitchDim1;
                    objPRL090.PitchDim2 = PRL090.PitchDim2;
                    objPRL090.PitchDim3 = PRL090.PitchDim3;
                    objPRL090.PitchDim4 = PRL090.PitchDim4;
                    objPRL090.PitchDim5 = PRL090.PitchDim5;
                    objPRL090.PitchDim6 = PRL090.PitchDim6;
                    objPRL090.PitchDim7 = PRL090.PitchDim7;
                    objPRL090.PitchDim8 = PRL090.PitchDim8;
                    objPRL090.PitchDim9 = PRL090.PitchDim9;
                    objPRL090.PitchDim10 = PRL090.PitchDim10;
                    objPRL090.PitchDim11 = PRL090.PitchDim11;
                    objPRL090.PitchDim12 = PRL090.PitchDim12;
                    objPRL090.PitchDim13 = PRL090.PitchDim13;
                    objPRL090.PitchDim14 = PRL090.PitchDim14;
                    objPRL090.PitchDim15 = PRL090.PitchDim15;
                    objPRL090.PitchDim16 = PRL090.PitchDim16;
                    objPRL090.PitchDim17 = PRL090.PitchDim17;
                    objPRL090.PitchDim18 = PRL090.PitchDim18;
                    objPRL090.PitchDim19 = PRL090.PitchDim19;
                    objPRL090.PitchDim20 = PRL090.PitchDim20;
                    objPRL090.PitchDim21 = PRL090.PitchDim21;
                    objPRL090.PitchDim22 = PRL090.PitchDim22;
                    objPRL090.PitchDim23 = PRL090.PitchDim23;
                    objPRL090.PitchDim24 = PRL090.PitchDim24;
                    objPRL090.PitchDim25 = PRL090.PitchDim25;
                    objPRL090.PitchDim26 = PRL090.PitchDim26;
                    objPRL090.PitchDim27 = PRL090.PitchDim27;
                    objPRL090.PitchDim28 = PRL090.PitchDim28;
                    objPRL090.PitchDim29 = PRL090.PitchDim29;
                    objPRL090.PitchDim30 = PRL090.PitchDim30;
                    objPRL090.PitchDim31 = PRL090.PitchDim31;
                    objPRL090.PitchDim32 = PRL090.PitchDim32;
                    objPRL090.PitchDim33 = PRL090.PitchDim33;
                    objPRL090.PitchDim34 = PRL090.PitchDim34;
                    objPRL090.PitchDim35 = PRL090.PitchDim35;
                    objPRL090.PitchDim36 = PRL090.PitchDim36;
                    objPRL090.PitchDim37 = PRL090.PitchDim37;
                    objPRL090.PitchDim38 = PRL090.PitchDim38;
                    objPRL090.PitchDim39 = PRL090.PitchDim39;
                    objPRL090.PitchDim40 = PRL090.PitchDim40;
                    objPRL090.PitchDim41 = PRL090.PitchDim41;
                    objPRL090.PitchDim42 = PRL090.PitchDim42;
                    objPRL090.PitchDim43 = PRL090.PitchDim43;
                    objPRL090.PitchDim44 = PRL090.PitchDim44;
                    objPRL090.HeightReq = PRL090.HeightReq;
                    objPRL090.HeightAct1 = PRL090.HeightAct1;
                    objPRL090.HeightAct2 = PRL090.HeightAct2;
                    objPRL090.HeightAct3 = PRL090.HeightAct3;
                    objPRL090.HeightAct4 = PRL090.HeightAct4;
                    objPRL090.HeightAct5 = PRL090.HeightAct5;
                    objPRL090.HeightAct6 = PRL090.HeightAct6;
                    objPRL090.HeightAct7 = PRL090.HeightAct7;
                    objPRL090.HeightAct8 = PRL090.HeightAct8;
                    objPRL090.HeightAct9 = PRL090.HeightAct9;
                    objPRL090.HeightAct10 = PRL090.HeightAct10;
                    objPRL090.DiameterReq = PRL090.DiameterReq;
                    objPRL090.DiameterAct1 = PRL090.DiameterAct1;
                    objPRL090.DiameterAct2 = PRL090.DiameterAct2;
                    objPRL090.DiameterAct3 = PRL090.DiameterAct3;
                    objPRL090.DiameterAct4 = PRL090.DiameterAct4;
                    objPRL090.DiameterAct5 = PRL090.DiameterAct5;
                    objPRL090.HoleA1 = PRL090.HoleA1;
                    objPRL090.HoleA2 = PRL090.HoleA2;
                    objPRL090.HoleA3 = PRL090.HoleA3;
                    objPRL090.HoleA4 = PRL090.HoleA4;
                    objPRL090.HoleA5 = PRL090.HoleA5;
                    objPRL090.HoleA6 = PRL090.HoleA6;
                    objPRL090.HoleA7 = PRL090.HoleA7;
                    objPRL090.HoleA8 = PRL090.HoleA8;
                    objPRL090.HoleA9 = PRL090.HoleA9;
                    objPRL090.HoleA10 = PRL090.HoleA10;
                    objPRL090.HoleA11 = PRL090.HoleA11;
                    objPRL090.HoleA12 = PRL090.HoleA12;
                    objPRL090.HoleA13 = PRL090.HoleA13;
                    objPRL090.HoleA14 = PRL090.HoleA14;
                    objPRL090.HoleA15 = PRL090.HoleA15;
                    objPRL090.HoleA16 = PRL090.HoleA16;
                    objPRL090.HoleA17 = PRL090.HoleA17;
                    objPRL090.HoleA18 = PRL090.HoleA18;
                    objPRL090.HoleA19 = PRL090.HoleA19;
                    objPRL090.HoleA20 = PRL090.HoleA20;
                    objPRL090.HoleA21 = PRL090.HoleA21;
                    objPRL090.HoleA22 = PRL090.HoleA22;
                    objPRL090.HoleA23 = PRL090.HoleA23;
                    objPRL090.HoleA24 = PRL090.HoleA24;
                    objPRL090.HoleA25 = PRL090.HoleA25;
                    objPRL090.HoleA26 = PRL090.HoleA26;
                    objPRL090.HoleA27 = PRL090.HoleA27;
                    objPRL090.HoleA28 = PRL090.HoleA28;
                    objPRL090.HoleA29 = PRL090.HoleA29;
                    objPRL090.HoleA30 = PRL090.HoleA30;
                    objPRL090.HoleA31 = PRL090.HoleA31;
                    objPRL090.HoleA32 = PRL090.HoleA32;
                    objPRL090.HoleA33 = PRL090.HoleA33;
                    objPRL090.HoleA34 = PRL090.HoleA34;
                    objPRL090.HoleA35 = PRL090.HoleA35;
                    objPRL090.HoleA36 = PRL090.HoleA36;
                    objPRL090.HoleA37 = PRL090.HoleA37;
                    objPRL090.HoleA38 = PRL090.HoleA38;
                    objPRL090.HoleA39 = PRL090.HoleA39;
                    objPRL090.HoleA40 = PRL090.HoleA40;
                    objPRL090.HoleA41 = PRL090.HoleA41;
                    objPRL090.HoleA42 = PRL090.HoleA42;
                    objPRL090.HoleA43 = PRL090.HoleA43;
                    objPRL090.HoleB1 = PRL090.HoleB1;
                    objPRL090.HoleB2 = PRL090.HoleB2;
                    objPRL090.HoleB3 = PRL090.HoleB3;
                    objPRL090.HoleB4 = PRL090.HoleB4;
                    objPRL090.HoleB5 = PRL090.HoleB5;
                    objPRL090.HoleB6 = PRL090.HoleB6;
                    objPRL090.HoleB7 = PRL090.HoleB7;
                    objPRL090.HoleB8 = PRL090.HoleB8;
                    objPRL090.HoleB9 = PRL090.HoleB9;
                    objPRL090.HoleB10 = PRL090.HoleB10;
                    objPRL090.HoleB11 = PRL090.HoleB11;
                    objPRL090.HoleB12 = PRL090.HoleB12;
                    objPRL090.HoleB13 = PRL090.HoleB13;
                    objPRL090.HoleB14 = PRL090.HoleB14;
                    objPRL090.HoleB15 = PRL090.HoleB15;
                    objPRL090.HoleB16 = PRL090.HoleB16;
                    objPRL090.HoleB17 = PRL090.HoleB17;
                    objPRL090.HoleB18 = PRL090.HoleB18;
                    objPRL090.HoleB19 = PRL090.HoleB19;
                    objPRL090.HoleB20 = PRL090.HoleB20;
                    objPRL090.HoleB21 = PRL090.HoleB21;
                    objPRL090.HoleB22 = PRL090.HoleB22;
                    objPRL090.HoleB23 = PRL090.HoleB23;
                    objPRL090.HoleB24 = PRL090.HoleB24;
                    objPRL090.HoleB25 = PRL090.HoleB25;
                    objPRL090.HoleB26 = PRL090.HoleB26;
                    objPRL090.HoleB27 = PRL090.HoleB27;
                    objPRL090.HoleB28 = PRL090.HoleB28;
                    objPRL090.HoleB29 = PRL090.HoleB29;
                    objPRL090.HoleB30 = PRL090.HoleB30;
                    objPRL090.HoleB31 = PRL090.HoleB31;
                    objPRL090.HoleB32 = PRL090.HoleB32;
                    objPRL090.HoleB33 = PRL090.HoleB33;
                    objPRL090.HoleB34 = PRL090.HoleB34;
                    objPRL090.HoleB35 = PRL090.HoleB35;
                    objPRL090.HoleB36 = PRL090.HoleB36;
                    objPRL090.HoleB37 = PRL090.HoleB37;
                    objPRL090.HoleB38 = PRL090.HoleB38;
                    objPRL090.HoleB39 = PRL090.HoleB39;
                    objPRL090.HoleB40 = PRL090.HoleB40;
                    objPRL090.HoleB41 = PRL090.HoleB41;
                    objPRL090.HoleB42 = PRL090.HoleB42;
                    objPRL090.HoleB43 = PRL090.HoleB43;
                    objPRL090.BoltHoleDia1 = PRL090.BoltHoleDia1;
                    objPRL090.BoltHoleDia2 = PRL090.BoltHoleDia2;
                    objPRL090.BoltHoleDia3 = PRL090.BoltHoleDia3;
                    objPRL090.BoltHoleDia4 = PRL090.BoltHoleDia4;
                    objPRL090.BoltHoleDia5 = PRL090.BoltHoleDia5;
                    objPRL090.BoltHoleDia6 = PRL090.BoltHoleDia6;
                    objPRL090.BoltHoleDia7 = PRL090.BoltHoleDia7;
                    objPRL090.BoltHoleDia8 = PRL090.BoltHoleDia8;
                    objPRL090.BoltHoleDia9 = PRL090.BoltHoleDia9;
                    objPRL090.BoltHoleDia10 = PRL090.BoltHoleDia10;
                    objPRL090.BoltHoleDia11 = PRL090.BoltHoleDia11;
                    objPRL090.BoltHoleDia12 = PRL090.BoltHoleDia12;
                    objPRL090.BoltHoleDia13 = PRL090.BoltHoleDia13;
                    objPRL090.BoltHoleDia14 = PRL090.BoltHoleDia14;
                    objPRL090.BoltHoleDia15 = PRL090.BoltHoleDia15;
                    objPRL090.BoltHoleDia16 = PRL090.BoltHoleDia16;
                    objPRL090.BoltHoleDia17 = PRL090.BoltHoleDia17;
                    objPRL090.BoltHoleDia18 = PRL090.BoltHoleDia18;
                    objPRL090.BoltHoleDia19 = PRL090.BoltHoleDia19;
                    objPRL090.BoltHoleDia20 = PRL090.BoltHoleDia20;
                    objPRL090.BoltHoleDia21 = PRL090.BoltHoleDia21;
                    objPRL090.BoltHoleDia22 = PRL090.BoltHoleDia22;
                    objPRL090.BoltHoleDia23 = PRL090.BoltHoleDia23;
                    objPRL090.BoltHoleDia24 = PRL090.BoltHoleDia24;
                    objPRL090.BoltHoleDia25 = PRL090.BoltHoleDia25;
                    objPRL090.BoltHoleDia26 = PRL090.BoltHoleDia26;
                    objPRL090.BoltHoleDia27 = PRL090.BoltHoleDia27;
                    objPRL090.BoltHoleDia28 = PRL090.BoltHoleDia28;
                    objPRL090.BoltHoleDia29 = PRL090.BoltHoleDia29;
                    objPRL090.BoltHoleDia30 = PRL090.BoltHoleDia30;
                    objPRL090.BoltHoleDia31 = PRL090.BoltHoleDia31;
                    objPRL090.BoltHoleDia32 = PRL090.BoltHoleDia32;
                    objPRL090.BoltHoleDia33 = PRL090.BoltHoleDia33;
                    objPRL090.BoltHoleDia34 = PRL090.BoltHoleDia34;
                    objPRL090.BoltHoleDia35 = PRL090.BoltHoleDia35;
                    objPRL090.BoltHoleDia36 = PRL090.BoltHoleDia36;
                    objPRL090.BoltHoleDia37 = PRL090.BoltHoleDia37;
                    objPRL090.BoltHoleDia38 = PRL090.BoltHoleDia38;
                    objPRL090.BoltHoleDia39 = PRL090.BoltHoleDia39;
                    objPRL090.BoltHoleDia40 = PRL090.BoltHoleDia40;
                    objPRL090.BoltHoleDia41 = PRL090.BoltHoleDia41;
                    objPRL090.BoltHoleDia42 = PRL090.BoltHoleDia42;
                    objPRL090.BoltHoleDia43 = PRL090.BoltHoleDia43;
                    objPRL090.BoltHoleDia44 = PRL090.BoltHoleDia44;
                    objPRL090.Remark1 = PRL090.Remark1;
                    objPRL090.Remark2 = PRL090.Remark2;
                    objPRL090.Remark3 = PRL090.Remark3;
                    objPRL090.Remark4 = PRL090.Remark4;
                    objPRL090.Remark5 = PRL090.Remark5;
                    objPRL090.Remark6 = PRL090.Remark6;
                    objPRL090.Remark7 = PRL090.Remark7;
                    objPRL090.Remark8 = PRL090.Remark8;
                    objPRL090.Remark9 = PRL090.Remark9;
                    objPRL090.Remark10 = PRL090.Remark10;
                    objPRL090.Remark11 = PRL090.Remark11;
                    objPRL090.Remark12 = PRL090.Remark12;
                    objPRL090.Remark13 = PRL090.Remark13;
                    objPRL090.EditedBy = objClsLoginInfo.UserName;
                    objPRL090.EditedOn = DateTime.Now;

                   
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.LinkageSaved;
                    objResponseMsg.HeaderId = objPRL090.HeaderId;

                    #endregion

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
    }
}


