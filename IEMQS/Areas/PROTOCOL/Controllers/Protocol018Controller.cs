﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol018Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol018/";
        string Title = "RECORD FOR TEMPORARY ATTACHMENTS LOCATION";
        // GET: PROTOCOL/Protocol018
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id,int? m)
        {
            PRO085 objPRO085 = new PRO085();
            if (!id.HasValue)
            {
                objPRO085.ProtocolNo = string.Empty;
                objPRO085.CreatedBy = objClsLoginInfo.UserName;
                objPRO085.CreatedOn = DateTime.Now;
                db.PRO085.Add(objPRO085);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO085.HeaderId;
            }
            else
            {
                objPRO085 = db.PRO085.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO085);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO085 PRO085)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO085.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO085 objPRO085 = db.PRO085.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO085.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO085.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO085.ProtocolNo = PRO085.ProtocolNo;
                        objPRO085.IdentificationDetails = PRO085.IdentificationDetails;
                        objPRO085.EditedBy = objClsLoginInfo.UserName;
                        objPRO085.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO085.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO085 objPRO085 = db.PRO085.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO085 != null && string.IsNullOrWhiteSpace(objPRO085.ProtocolNo))
                    {
                        db.PRO086.RemoveRange(objPRO085.PRO086.ToList());
                        db.PRO085.Remove(objPRO085);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.RECORD_FOR_TEMPORARY_ATTACHMENTS_LOCATION.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO085.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (CleatNo like '%" + param.sSearch
                        + "%' or SizeWeldedOnPerentMetal like '%" + param.sSearch
                        + "%' or OSOrIS like '%" + param.sSearch
                        + "%' or Elevation like '%" + param.sSearch
                        + "%' or Orientation like '%" + param.sSearch
                        + "%' or MtPtAfterRemoval like '%" + param.sSearch
                        + "%' or MtPtAfterPwht like '%" + param.sSearch
                        + "%' or Remarks like '%" + param.sSearch

                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL018_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "CleatNo", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SizeWeldedOnPerentMetal", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OSOrIS", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Elevation", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Orientation", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "MtPtAfterRemoval", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "MtPtAfterPwht", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", true, "", false, "100", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "CleatNo", uc.CleatNo, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "SizeWeldedOnPerentMetal", uc.SizeWeldedOnPerentMetal, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "OSOrIS", uc.OSOrIS, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Elevation", uc.Elevation, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Orientation", uc.Orientation, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "MtPtAfterRemoval", uc.MtPtAfterRemoval, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "MtPtAfterPwht", uc.MtPtAfterPwht, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", uc.Remarks, "", true, "", false, "100", "PROD"),
                                HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')") : "")
                                }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO086 objPRO086 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objPRO086 = db.PRO086.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        
                        objPRO086.CleatNo = !string.IsNullOrEmpty(fc["CleatNo" + refLineId]) ? Convert.ToString(fc["CleatNo" + refLineId]).Trim() : "";
                        objPRO086.SizeWeldedOnPerentMetal = !string.IsNullOrEmpty(fc["SizeWeldedOnPerentMetal" + refLineId]) ? Convert.ToString(fc["SizeWeldedOnPerentMetal" + refLineId]).Trim() : "";
                        objPRO086.OSOrIS = !string.IsNullOrEmpty(fc["OSOrIS" + refLineId]) ? Convert.ToString(fc["OSOrIS" + refLineId]).Trim() : "";
                        objPRO086.Elevation = !string.IsNullOrEmpty(fc["Elevation" + refLineId]) ? Convert.ToString(fc["Elevation" + refLineId]).Trim() : "";
                        objPRO086.Orientation = !string.IsNullOrEmpty(fc["Orientation" + refLineId]) ? Convert.ToString(fc["Orientation" + refLineId]).Trim() : "";
                        objPRO086.MtPtAfterRemoval = !string.IsNullOrEmpty(fc["MtPtAfterRemoval" + refLineId]) ? Convert.ToString(fc["MtPtAfterRemoval" + refLineId]).Trim() : "";
                        objPRO086.MtPtAfterPwht = !string.IsNullOrEmpty(fc["MtPtAfterPwht" + refLineId]) ? Convert.ToString(fc["MtPtAfterPwht" + refLineId]).Trim() : "";
                        objPRO086.Remarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";

                        objPRO086.EditedBy = objClsLoginInfo.UserName;
                        objPRO086.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO086 = new PRO086();
                        objPRO086.HeaderId = refHeaderId;
                        objPRO086.CleatNo = !string.IsNullOrEmpty(fc["CleatNo" + newRowIndex]) ? Convert.ToString(fc["CleatNo" + newRowIndex]).Trim() : "";
                        objPRO086.SizeWeldedOnPerentMetal = !string.IsNullOrEmpty(fc["SizeWeldedOnPerentMetal" + newRowIndex]) ? Convert.ToString(fc["SizeWeldedOnPerentMetal" + newRowIndex]).Trim() : "";
                        objPRO086.OSOrIS = !string.IsNullOrEmpty(fc["OSOrIS" + newRowIndex]) ? Convert.ToString(fc["OSOrIS" + newRowIndex]).Trim() : "";
                        objPRO086.Elevation = !string.IsNullOrEmpty(fc["Elevation" + newRowIndex]) ? Convert.ToString(fc["Elevation" + newRowIndex]).Trim() : "";
                        objPRO086.Orientation = !string.IsNullOrEmpty(fc["Orientation" + newRowIndex]) ? Convert.ToString(fc["Orientation" + newRowIndex]).Trim() : "";
                        objPRO086.MtPtAfterRemoval = !string.IsNullOrEmpty(fc["MtPtAfterRemoval" + newRowIndex]) ? Convert.ToString(fc["MtPtAfterRemoval" + newRowIndex]).Trim() : "";
                        objPRO086.MtPtAfterPwht = !string.IsNullOrEmpty(fc["MtPtAfterPwht" + newRowIndex]) ? Convert.ToString(fc["MtPtAfterPwht" + newRowIndex]).Trim() : "";
                        objPRO086.Remarks = !string.IsNullOrEmpty(fc["Remarks" + newRowIndex]) ? Convert.ToString(fc["Remarks" + newRowIndex]).Trim() : "";

                        objPRO086.CreatedBy = objClsLoginInfo.UserName;
                        objPRO086.CreatedOn = DateTime.Now;

                        db.PRO086.Add(objPRO086);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    
                    objResponseMsg.HeaderId = objPRO086.LineId;

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }




        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO086 objPRO086 = db.PRO086.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO086 != null)
                {
                    db.PRO086.Remove(objPRO086);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion



        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL085 objPRL085 = new PRL085();
            objPRL085 = db.PRL085.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (objPRL085 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL085.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            {
                ViewBag.isEditable = "false";
            }

            return View(objPRL085);
        }


        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL085 PRL085)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL085.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL085 objPRL085 = db.PRL085.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL085.ProtocolNo = PRL085.ProtocolNo != null ? PRL085.ProtocolNo : "";
                    objPRL085.DCRNo = PRL085.DCRNo != null ? PRL085.DCRNo : "";
                    objPRL085.IdentificationDetails = PRL085.IdentificationDetails;
                    objPRL085.EditedOn = DateTime.Now;
                    objPRL085.EditedBy = objClsLoginInfo.UserName;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL085.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL085 objPRL085 = db.PRL085.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL085 != null && string.IsNullOrWhiteSpace(objPRL085.ProtocolNo))
                    {
                        db.PRL086.RemoveRange(objPRL085.PRL086.ToList());
                        db.PRL085.Remove(objPRL085);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL085.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion




        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (CleatNo like '%" + param.sSearch
                        + "%' or SizeWeldedOnPerentMetal like '%" + param.sSearch
                        + "%' or OSOrIS like '%" + param.sSearch
                        + "%' or Elevation like '%" + param.sSearch
                        + "%' or Orientation like '%" + param.sSearch
                        + "%' or MtPtAfterRemoval like '%" + param.sSearch
                        + "%' or MtPtAfterPwht like '%" + param.sSearch
                        + "%' or Remarks like '%" + param.sSearch

                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL018_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "CleatNo", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SizeWeldedOnPerentMetal", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "OSOrIS", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Elevation", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Orientation", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "MtPtAfterRemoval", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "MtPtAfterPwht", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", true, "", false, "100", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "CleatNo", uc.CleatNo, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "SizeWeldedOnPerentMetal", uc.SizeWeldedOnPerentMetal, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "OSOrIS", uc.OSOrIS, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Elevation", uc.Elevation, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Orientation", uc.Orientation, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "MtPtAfterRemoval", uc.MtPtAfterRemoval, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "MtPtAfterPwht", uc.MtPtAfterPwht, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", uc.Remarks, "", true, "", false, "100", "PROD"),
                                HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')") : "")
                                }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }



        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL086 objPRL086 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objPRL086 = db.PRL086.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();

                        objPRL086.CleatNo = !string.IsNullOrEmpty(fc["CleatNo" + refLineId]) ? Convert.ToString(fc["CleatNo" + refLineId]).Trim() : "";
                        objPRL086.SizeWeldedOnPerentMetal = !string.IsNullOrEmpty(fc["SizeWeldedOnPerentMetal" + refLineId]) ? Convert.ToString(fc["SizeWeldedOnPerentMetal" + refLineId]).Trim() : "";
                        objPRL086.OSOrIS = !string.IsNullOrEmpty(fc["OSOrIS" + refLineId]) ? Convert.ToString(fc["OSOrIS" + refLineId]).Trim() : "";
                        objPRL086.Elevation = !string.IsNullOrEmpty(fc["Elevation" + refLineId]) ? Convert.ToString(fc["Elevation" + refLineId]).Trim() : "";
                        objPRL086.Orientation = !string.IsNullOrEmpty(fc["Orientation" + refLineId]) ? Convert.ToString(fc["Orientation" + refLineId]).Trim() : "";
                        objPRL086.MtPtAfterRemoval = !string.IsNullOrEmpty(fc["MtPtAfterRemoval" + refLineId]) ? Convert.ToString(fc["MtPtAfterRemoval" + refLineId]).Trim() : "";
                        objPRL086.MtPtAfterPwht = !string.IsNullOrEmpty(fc["MtPtAfterPwht" + refLineId]) ? Convert.ToString(fc["MtPtAfterPwht" + refLineId]).Trim() : "";
                        objPRL086.Remarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";

                        objPRL086.EditedBy = objClsLoginInfo.UserName;
                        objPRL086.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL086 = new PRL086();
                        objPRL086.HeaderId = refHeaderId;
                        objPRL086.CleatNo = !string.IsNullOrEmpty(fc["CleatNo" + newRowIndex]) ? Convert.ToString(fc["CleatNo" + newRowIndex]).Trim() : "";
                        objPRL086.SizeWeldedOnPerentMetal = !string.IsNullOrEmpty(fc["SizeWeldedOnPerentMetal" + newRowIndex]) ? Convert.ToString(fc["SizeWeldedOnPerentMetal" + newRowIndex]).Trim() : "";
                        objPRL086.OSOrIS = !string.IsNullOrEmpty(fc["OSOrIS" + newRowIndex]) ? Convert.ToString(fc["OSOrIS" + newRowIndex]).Trim() : "";
                        objPRL086.Elevation = !string.IsNullOrEmpty(fc["Elevation" + newRowIndex]) ? Convert.ToString(fc["Elevation" + newRowIndex]).Trim() : "";
                        objPRL086.Orientation = !string.IsNullOrEmpty(fc["Orientation" + newRowIndex]) ? Convert.ToString(fc["Orientation" + newRowIndex]).Trim() : "";
                        objPRL086.MtPtAfterRemoval = !string.IsNullOrEmpty(fc["MtPtAfterRemoval" + newRowIndex]) ? Convert.ToString(fc["MtPtAfterRemoval" + newRowIndex]).Trim() : "";
                        objPRL086.MtPtAfterPwht = !string.IsNullOrEmpty(fc["MtPtAfterPwht" + newRowIndex]) ? Convert.ToString(fc["MtPtAfterPwht" + newRowIndex]).Trim() : "";
                        objPRL086.Remarks = !string.IsNullOrEmpty(fc["Remarks" + newRowIndex]) ? Convert.ToString(fc["Remarks" + newRowIndex]).Trim() : "";

                        objPRL086.CreatedBy = objClsLoginInfo.UserName;
                        objPRL086.CreatedOn = DateTime.Now;

                        db.PRL086.Add(objPRL086);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;

                    objResponseMsg.HeaderId = objPRL086.LineId;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL086 objPRL086 = db.PRL086.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL086 != null)
                {
                    db.PRL086.Remove(objPRL086);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                //htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
    }
}