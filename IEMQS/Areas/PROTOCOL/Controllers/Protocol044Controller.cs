﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsBase;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol044Controller : clsBase
    {
        // GET: PROTOCOL/Protocol044
        string ControllerURL = "/PROTOCOL/Protocol044/";
        string Title = "VISUAL & DIMENSION REPORT FOR CIRCUMFERENTIAL SEAM";
        //IEMQS.DrawingReference.hedProjectDocListWebServiceClient drawref = new IEMQS.DrawingReference.hedProjectDocListWebServiceClient();

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO215 objPRO215 = new PRO215();
            if (!id.HasValue)
            {
                try
                {
                    objPRO215.ProtocolNo = string.Empty;
                    objPRO215.CreatedBy = objClsLoginInfo.UserName;
                    objPRO215.CreatedOn = DateTime.Now;

                    #region TOTAL HEIGHT
                    objPRO215.PRO217.Add(new PRO217
                    {
                        Orientation = "0°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO215.PRO217.Add(new PRO217
                    {
                        Orientation = "90°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO215.PRO217.Add(new PRO217
                    {
                        Orientation = "180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO215.PRO217.Add(new PRO217
                    {
                        Orientation = "270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    #region OUT OF ROUNDNESS
                    objPRO215.PRO218.Add(new PRO218
                    {
                        Orientation = "0°-180° ",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO215.PRO218.Add(new PRO218
                    {
                        Orientation = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO215.PRO218.Add(new PRO218
                    {
                        Orientation = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO215.PRO218.Add(new PRO218
                    {
                        Orientation = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO215.PRO218.Add(new PRO218
                    {
                        Orientation = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO215.PRO218.Add(new PRO218
                    {
                        Orientation = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO215.PRO218.Add(new PRO218
                    {
                        Orientation = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO215.PRO218.Add(new PRO218
                    {
                        Orientation = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    #endregion

                    db.PRO215.Add(objPRO215);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO215.HeaderId;
            }
            else
            {
                objPRO215 = db.PRO215.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();



            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();



            #region bind lines
            List<PRO216> lstPRO216 = db.PRO216.Where(x => x.HeaderId == objPRO215.HeaderId).ToList();
            List<PRO217> lstPRO217 = db.PRO217.Where(x => x.HeaderId == objPRO215.HeaderId).ToList();
            List<PRO218> lstPRO218 = db.PRO218.Where(x => x.HeaderId == objPRO215.HeaderId).ToList();
            List<PRO219> lstPRO219 = db.PRO219.Where(x => x.HeaderId == objPRO215.HeaderId).ToList();
            List<PRO219_2> lstPRO219_2 = db.PRO219_2.Where(x => x.HeaderId == objPRO215.HeaderId).ToList();
            List<PRO219_3> lstPRO219_3 = db.PRO219_3.Where(x => x.HeaderId == objPRO215.HeaderId).ToList();
            List<PRO219_4> lstPRO219_4 = db.PRO219_4.Where(x => x.HeaderId == objPRO215.HeaderId).ToList();
            List<PRO219_5> lstPRO219_5 = db.PRO219_5.Where(x => x.HeaderId == objPRO215.HeaderId).ToList();




            ViewBag.lstPRO216 = lstPRO216;
            ViewBag.lstPRO217 = lstPRO217;
            ViewBag.lstPRO218 = lstPRO218;
            ViewBag.lstPRO219 = lstPRO219;
            ViewBag.lstPRO219_2 = lstPRO219_2;
            ViewBag.lstPRO219_3 = lstPRO219_3;
            ViewBag.lstPRO219_4 = lstPRO219_4;
            ViewBag.lstPRO219_5 = lstPRO219_5;


            #endregion

            return View(objPRO215);
        }
        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO215 PRO215)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO215.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO215 objPRO215 = db.PRO215.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO215.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO215.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO215.ProtocolNo = PRO215.ProtocolNo;
                        objPRO215.Offset = PRO215.Offset;
                        objPRO215.SpotNoOffset = PRO215.SpotNoOffset;
                        objPRO215.TotalHeight = PRO215.TotalHeight;
                        objPRO215.ReqOutOfRoundness = PRO215.ReqOutOfRoundness;
                        objPRO215.IsAtSeam = PRO215.IsAtSeam;
                        objPRO215.OutOfRoundness = PRO215.OutOfRoundness;
                        objPRO215.OutByOutOfRoundness = PRO215.OutByOutOfRoundness;
                        objPRO215.OutByAlignmentReadings = PRO215.OutByAlignmentReadings;
                        objPRO215.EvalueTemplate = PRO215.EvalueTemplate;
                        objPRO215.ReqChordLength = PRO215.ReqChordLength;
                        objPRO215.ActChordLength = PRO215.ActChordLength;
                        objPRO215.ReqRadius = PRO215.ReqRadius;
                        objPRO215.ActRadius = PRO215.ActRadius;
                        objPRO215.AllowableGap = PRO215.AllowableGap;
                        objPRO215.ActGap = PRO215.ActGap;
                        objPRO215.ElevationOfNubTsr = PRO215.ElevationOfNubTsr;
                        objPRO215.ElevationOfNozzlesAttechments = PRO215.ElevationOfNozzlesAttechments;
                        objPRO215.TaperLengthShellJoint = PRO215.TaperLengthShellJoint;
                        objPRO215.ReqTaperLength = PRO215.ReqTaperLength;
                        objPRO215.ActTaperLengthMin = PRO215.ActTaperLengthMin;
                        objPRO215.ActTaperLengthMax = PRO215.ActTaperLengthMax;
                        objPRO215.Concentricity1 = PRO215.Concentricity1;
                        objPRO215.Concentricity2 = PRO215.Concentricity2;
                        objPRO215.CheckPoint1 = PRO215.CheckPoint1;
                        objPRO215.CheckPoint2 = PRO215.CheckPoint2;
                        objPRO215.CheckPoint3 = PRO215.CheckPoint3;
                        objPRO215.CheckPoint5 = PRO215.CheckPoint5;
                        objPRO215.CheckPoint5_2 = PRO215.CheckPoint5_2;
                        objPRO215.CheckPoint6 = PRO215.CheckPoint6;
                        objPRO215.QCRemarks = PRO215.QCRemarks;
                        objPRO215.Result = PRO215.Result;
                        objPRO215.ReqCladStripingWidth = PRO215.ReqCladStripingWidth;
                        objPRO215.ReqCladStripingDepth = PRO215.ReqCladStripingDepth;
                        objPRO215.SpotNoClad = PRO215.SpotNoClad;
                        objPRO215.Note1 = PRO215.Note1;
                        objPRO215.Note2 = PRO215.Note2;
                        objPRO215.Note3 = PRO215.Note3;


                        objPRO215.EditedBy = objClsLoginInfo.UserName;
                        objPRO215.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO215.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO215 objPRO215 = db.PRO215.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO215 != null && string.IsNullOrWhiteSpace(objPRO215.ProtocolNo))
                    {
                        db.PRO215.Remove(objPRO215);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO215.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO216> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO216> lstAddPRO216 = new List<PRO216>();
                List<PRO216> lstDeletePRO216 = new List<PRO216>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO216 obj = db.PRO216.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.OrientationSpotNumber = item.OrientationSpotNumber;
                                obj.Offset = item.Offset;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO216 obj = new PRO216();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.OrientationSpotNumber))
                            {
                                obj.OrientationSpotNumber = item.OrientationSpotNumber;
                                obj.Offset = item.Offset;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO216.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO216.Count > 0)
                    {
                        db.PRO216.AddRange(lstAddPRO216);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO216 = db.PRO216.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO216.Count > 0)
                    {
                        db.PRO216.RemoveRange(lstDeletePRO216);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO216 = db.PRO216.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO216.Count > 0)
                    {
                        db.PRO216.RemoveRange(lstDeletePRO216);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO217> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO217> lstAddPRO217 = new List<PRO217>();
                List<PRO217> lstDeletePRO217 = new List<PRO217>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO217 obj = db.PRO217.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO217();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.Actual = item.Actual;
                        if (isAdded)
                        {
                            lstAddPRO217.Add(obj);
                        }
                    }
                    if (lstAddPRO217.Count > 0)
                    {
                        db.PRO217.AddRange(lstAddPRO217);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO217 = db.PRO217.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO217.Count > 0)
                    {
                        db.PRO217.RemoveRange(lstDeletePRO217);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO217 = db.PRO217.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO217.Count > 0)
                    {
                        db.PRO217.RemoveRange(lstDeletePRO217);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO218> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO218> lstAddPRO218 = new List<PRO218>();
                List<PRO218> lstDeletePRO218 = new List<PRO218>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO218 obj = db.PRO218.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj = new PRO218();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(item.Orientation))
                        {
                            obj.Orientation = item.Orientation;
                            obj.AtTopOpenEnd = item.AtTopOpenEnd;
                            obj.AtSeam = item.AtSeam;
                            obj.AtBottomOpenEnd = item.AtBottomOpenEnd;
                        }

                        if (isAdded)
                        {
                            lstAddPRO218.Add(obj);
                        }
                    }
                    if (lstAddPRO218.Count > 0)
                    {
                        db.PRO218.AddRange(lstAddPRO218);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO218 = db.PRO218.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO218.Count > 0)
                    {
                        db.PRO218.RemoveRange(lstDeletePRO218);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO218 = db.PRO218.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO218.Count > 0)
                    {
                        db.PRO218.RemoveRange(lstDeletePRO218);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4(List<PRO219> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO219> lstAddPRO219 = new List<PRO219>();
                List<PRO219> lstDeletePRO219 = new List<PRO219>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO219 obj = db.PRO219.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Location))
                            {
                                obj = new PRO219();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.ActAt0 = item.ActAt0;
                            obj.ActAt90 = item.ActAt90;
                            obj.ActAt180 = item.ActAt180;
                            obj.ActAt270 = item.ActAt270;
                        }

                        if (isAdded)
                        {
                            lstAddPRO219.Add(obj);
                        }
                    }
                    if (lstAddPRO219.Count > 0)
                    {
                        db.PRO219.AddRange(lstAddPRO219);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO219 = db.PRO219.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO219.Count > 0)
                    {
                        db.PRO219.RemoveRange(lstDeletePRO219);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO219 = db.PRO219.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO219.Count > 0)
                    {
                        db.PRO219.RemoveRange(lstDeletePRO219);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5(List<PRO219_2> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO219_2> lstAddPRO219_2 = new List<PRO219_2>();
                List<PRO219_2> lstDeletePRO219_2 = new List<PRO219_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO219_2 obj = db.PRO219_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj = new PRO219_2();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.SeamNo))
                        {
                            obj.SeamNo = item.SeamNo;
                            obj.ReqOrien = item.ReqOrien;
                            obj.ReqArcLength = item.ReqArcLength;
                            obj.ActualArcLength = item.ActualArcLength;

                        }

                        if (isAdded)
                        {
                            lstAddPRO219_2.Add(obj);
                        }
                    }
                    if (lstAddPRO219_2.Count > 0)
                    {
                        db.PRO219_2.AddRange(lstAddPRO219_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO219_2 = db.PRO219_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO219_2.Count > 0)
                    {
                        db.PRO219_2.RemoveRange(lstDeletePRO219_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO219_2 = db.PRO219_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO219_2.Count > 0)
                    {
                        db.PRO219_2.RemoveRange(lstDeletePRO219_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6(List<PRO219_3> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO219_3> lstAddPRO219_3 = new List<PRO219_3>();
                List<PRO219_3> lstDeletePRO219_3 = new List<PRO219_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO219_3 obj = db.PRO219_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO219_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.ReqValue = item.ReqValue;
                        obj.ActAt0 = item.ActAt0;
                        obj.ActAt90 = item.ActAt90;
                        obj.ActAt180 = item.ActAt180;
                        obj.ActAt270 = item.ActAt270;

                        if (isAdded)
                        {
                            lstAddPRO219_3.Add(obj);
                        }
                    }
                    if (lstAddPRO219_3.Count > 0)
                    {
                        db.PRO219_3.AddRange(lstAddPRO219_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO219_3 = db.PRO219_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO219_3.Count > 0)
                    {
                        db.PRO219_3.RemoveRange(lstDeletePRO219_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO219_3 = db.PRO219_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO219_3.Count > 0)
                    {
                        db.PRO219_3.RemoveRange(lstDeletePRO219_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7(List<PRO219_4> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO219_4> lstAddPRO219_4 = new List<PRO219_4>();
                List<PRO219_4> lstDeletePRO219_4 = new List<PRO219_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRO219_4 obj = db.PRO219_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Description = item.Description;
                                obj.Required = item.Required;
                                obj.Actual = item.Actual;
                                obj.EditedBy = objClsLoginInfo.UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRO219_4 obj = new PRO219_4();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Description))
                            {
                                obj.Description = item.Description;
                                obj.Required = item.Required;
                                obj.Actual = item.Actual;
                                obj.CreatedBy = objClsLoginInfo.UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRO219_4.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRO219_4.Count > 0)
                    {
                        db.PRO219_4.AddRange(lstAddPRO219_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRO219_4 = db.PRO219_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRO219_4.Count > 0)
                    {
                        db.PRO219_4.RemoveRange(lstDeletePRO219_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO219_4 = db.PRO219_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO219_4.Count > 0)
                    {
                        db.PRO219_4.RemoveRange(lstDeletePRO219_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details

        [HttpPost]
        public JsonResult SaveProtocolLine8(List<PRO219_5> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO219_5> lstAddPRO219_5 = new List<PRO219_5>();
                List<PRO219_5> lstDeletePRO219_5 = new List<PRO219_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO219_5 obj = db.PRO219_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO219_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Width = item.Width;
                        obj.Depth = item.Depth;

                        if (isAdded)
                        {
                            lstAddPRO219_5.Add(obj);
                        }
                    }
                    if (lstAddPRO219_5.Count > 0)
                    {
                        db.PRO219_5.AddRange(lstAddPRO219_5);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO219_5 = db.PRO219_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO219_5.Count > 0)
                    {
                        db.PRO219_5.RemoveRange(lstDeletePRO219_5);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO219_5 = db.PRO219_5.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO219_5.Count > 0)
                    {
                        db.PRO219_5.RemoveRange(lstDeletePRO219_5);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region Linkage View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)

        {

            PRL215 objPRL215 = new PRL215();
            objPRL215 = db.PRL215.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL215 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL215.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL216> lstPRL216 = db.PRL216.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL217> lstPRL217 = db.PRL217.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL218> lstPRL218 = db.PRL218.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL219> lstPRL219 = db.PRL219.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL219_2> lstPRL219_2 = db.PRL219_2.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL219_3> lstPRL219_3 = db.PRL219_3.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL219_4> lstPRL219_4 = db.PRL219_4.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL219_5> lstPRL219_5 = db.PRL219_5.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();


            ViewBag.lstPRL216 = lstPRL216;
            ViewBag.lstPRL217 = lstPRL217;
            ViewBag.lstPRL218 = lstPRL218;
            ViewBag.lstPRL219 = lstPRL219;
            ViewBag.lstPRL219_2 = lstPRL219_2;
            ViewBag.lstPRL219_3 = lstPRL219_3;
            ViewBag.lstPRL219_4 = lstPRL219_4;
            ViewBag.lstPRL219_5 = lstPRL219_5;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL215.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL215.ActFilledBy) && (objPRL215.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL215.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL215);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;

            PRL215 objPRL215 = new PRL215();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL215 = db.PRL215.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL215).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL215 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL215.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;

                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;


            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL216> lstPRL216 = db.PRL216.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL217> lstPRL217 = db.PRL217.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL218> lstPRL218 = db.PRL218.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL219> lstPRL219 = db.PRL219.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL219_2> lstPRL219_2 = db.PRL219_2.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL219_3> lstPRL219_3 = db.PRL219_3.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL219_4> lstPRL219_4 = db.PRL219_4.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();
            List<PRL219_5> lstPRL219_5 = db.PRL219_5.Where(x => x.HeaderId == objPRL215.HeaderId).ToList();

            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL215.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL215.ActFilledBy) && (objPRL215.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL215.Project).ToList();
                        }
                    }
                }


                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,

                    objPRL215 = objPRL215,

                    lstPRL216 = lstPRL216,
                    lstPRL217 = lstPRL217,
                    lstPRL218 = lstPRL218,
                    lstPRL219 = lstPRL219,
                    lstPRL219_2 = lstPRL219_2,
                    lstPRL219_3 = lstPRL219_3,
                    lstPRL219_4 = lstPRL219_4,
                    lstPRL219_5 = lstPRL219_5,

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL215 prl215, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = prl215.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL215 objPRL215 = db.PRL215.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL215.DrawingRevisionNo = prl215.DrawingRevisionNo;
                    objPRL215.DrawingNo = prl215.DrawingNo;
                    objPRL215.DCRNo = prl215.DCRNo;
                    objPRL215.ProtocolNo = prl215.ProtocolNo;

                    objPRL215.Offset = prl215.Offset;
                    objPRL215.SpotNoOffset = prl215.SpotNoOffset;
                    objPRL215.TotalHeight = prl215.TotalHeight;
                    objPRL215.ReqOutOfRoundness = prl215.ReqOutOfRoundness;
                    objPRL215.IsAtSeam = prl215.IsAtSeam;
                    objPRL215.OutOfRoundness = prl215.OutOfRoundness;
                    objPRL215.OutByOutOfRoundness = prl215.OutByOutOfRoundness;
                    objPRL215.OutByAlignmentReadings = prl215.OutByAlignmentReadings;
                    objPRL215.EvalueTemplate = prl215.EvalueTemplate;
                    objPRL215.ReqChordLength = prl215.ReqChordLength;
                    objPRL215.ActChordLength = prl215.ActChordLength;
                    objPRL215.ReqRadius = prl215.ReqRadius;
                    objPRL215.ActRadius = prl215.ActRadius;
                    objPRL215.AllowableGap = prl215.AllowableGap;
                    objPRL215.ActGap = prl215.ActGap;
                    objPRL215.ElevationOfNubTsr = prl215.ElevationOfNubTsr;
                    objPRL215.ElevationOfNozzlesAttechments = prl215.ElevationOfNozzlesAttechments;
                    objPRL215.TaperLengthShellJoint = prl215.TaperLengthShellJoint;
                    objPRL215.ReqTaperLength = prl215.ReqTaperLength;
                    objPRL215.ActTaperLengthMin = prl215.ActTaperLengthMin;
                    objPRL215.ActTaperLengthMax = prl215.ActTaperLengthMax;
                    objPRL215.Concentricity1 = prl215.Concentricity1;
                    objPRL215.Concentricity2 = prl215.Concentricity2;
                    objPRL215.CheckPoint1 = prl215.CheckPoint1;
                    objPRL215.CheckPoint2 = prl215.CheckPoint2;
                    objPRL215.CheckPoint3 = prl215.CheckPoint3;
                    objPRL215.CheckPoint5 = prl215.CheckPoint5;
                    objPRL215.CheckPoint5_2 = prl215.CheckPoint5_2;
                    objPRL215.CheckPoint6 = prl215.CheckPoint6;
                    objPRL215.QCRemarks = prl215.QCRemarks;
                    objPRL215.Result = prl215.Result;
                    objPRL215.ReqCladStripingWidth = prl215.ReqCladStripingWidth;
                    objPRL215.ReqCladStripingDepth = prl215.ReqCladStripingDepth;
                    objPRL215.SpotNoClad = prl215.SpotNoClad;
                    objPRL215.Note1 = prl215.Note1;
                    objPRL215.Note2 = prl215.Note2;
                    objPRL215.Note3 = prl215.Note3;


                    objPRL215.EditedBy = UserName;
                    objPRL215.EditedOn = DateTime.Now;
                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL215.ActFilledBy = UserName;
                            objPRL215.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL215.ReqFilledBy = UserName;
                            objPRL215.ReqFilledOn = DateTime.Now;
                        }
                    }
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL215.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL215 objPRL215 = db.PRL215.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL215 != null && string.IsNullOrWhiteSpace(objPRL215.ProtocolNo))
                    {
                        db.PRL215.Remove(objPRL215);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL215.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL216> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL216> lstAddPRL216 = new List<PRL216>();
                List<PRL216> lstDeletePRL216 = new List<PRL216>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL216 obj = db.PRL216.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.OrientationSpotNumber = item.OrientationSpotNumber;
                                obj.Offset = item.Offset;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL216 obj = new PRL216();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.OrientationSpotNumber))
                            {
                                obj.OrientationSpotNumber = item.OrientationSpotNumber;
                                obj.Offset = item.Offset;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL216.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL216.Count > 0)
                    {
                        db.PRL216.AddRange(lstAddPRL216);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL216 = db.PRL216.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL216.Count > 0)
                    {
                        db.PRL216.RemoveRange(lstDeletePRL216);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL216 = db.PRL216.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL216.Count > 0)
                    {
                        db.PRL216.RemoveRange(lstDeletePRL216);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL217> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL217> lstAddPRL217 = new List<PRL217>();
                List<PRL217> lstDeletePRL217 = new List<PRL217>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL217 obj = db.PRL217.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL217();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.Actual = item.Actual;
                        if (isAdded)
                        {
                            lstAddPRL217.Add(obj);
                        }
                    }
                    if (lstAddPRL217.Count > 0)
                    {
                        db.PRL217.AddRange(lstAddPRL217);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL217 = db.PRL217.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL217.Count > 0)
                    {
                        db.PRL217.RemoveRange(lstDeletePRL217);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL217 = db.PRL217.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL217.Count > 0)
                    {
                        db.PRL217.RemoveRange(lstDeletePRL217);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL218> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL218> lstAddPRL218 = new List<PRL218>();
                List<PRL218> lstDeletePRL218 = new List<PRL218>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL218 obj = db.PRL218.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj != null)
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(item.Orientation))
                            {
                                obj = new PRL218();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }

                        if (!string.IsNullOrWhiteSpace(item.Orientation))
                        {
                            obj.Orientation = item.Orientation;
                            obj.AtTopOpenEnd = item.AtTopOpenEnd;
                            obj.AtSeam = item.AtSeam;
                            obj.AtBottomOpenEnd = item.AtBottomOpenEnd;
                        }

                        if (isAdded)
                        {
                            lstAddPRL218.Add(obj);
                        }
                    }
                    if (lstAddPRL218.Count > 0)
                    {
                        db.PRL218.AddRange(lstAddPRL218);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL218 = db.PRL218.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL218.Count > 0)
                    {
                        db.PRL218.RemoveRange(lstDeletePRL218);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL218 = db.PRL218.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL218.Count > 0)
                    {
                        db.PRL218.RemoveRange(lstDeletePRL218);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line4 details
        [HttpPost]
        public JsonResult SaveProtocolLine4Linkage(List<PRL219> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL219> lstAddPRL219 = new List<PRL219>();
                List<PRL219> lstDeletePRL219 = new List<PRL219>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL219 obj = db.PRL219.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Location))
                            {
                                obj = new PRL219();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.Location))
                        {
                            obj.Location = item.Location;
                            obj.ActAt0 = item.ActAt0;
                            obj.ActAt90 = item.ActAt90;
                            obj.ActAt180 = item.ActAt180;
                            obj.ActAt270 = item.ActAt270;
                        }

                        if (isAdded)
                        {
                            lstAddPRL219.Add(obj);
                        }
                    }
                    if (lstAddPRL219.Count > 0)
                    {
                        db.PRL219.AddRange(lstAddPRL219);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL219 = db.PRL219.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL219.Count > 0)
                    {
                        db.PRL219.RemoveRange(lstDeletePRL219);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL219 = db.PRL219.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL219.Count > 0)
                    {
                        db.PRL219.RemoveRange(lstDeletePRL219);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line5 details
        [HttpPost]
        public JsonResult SaveProtocolLine5Linkage(List<PRL219_2> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL219_2> lstAddPRL219_2 = new List<PRL219_2>();
                List<PRL219_2> lstDeletePRL219_2 = new List<PRL219_2>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL219_2 obj = db.PRL219_2.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            if (!string.IsNullOrWhiteSpace(item.SeamNo))
                            {
                                obj = new PRL219_2();
                                obj.HeaderId = item.HeaderId;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                isAdded = true;
                            }
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.SeamNo))
                        {
                            obj.SeamNo = item.SeamNo;
                            obj.ReqOrien = item.ReqOrien;
                            obj.ReqArcLength = item.ReqArcLength;
                            obj.ActualArcLength = item.ActualArcLength;

                        }

                        if (isAdded)
                        {
                            lstAddPRL219_2.Add(obj);
                        }
                    }
                    if (lstAddPRL219_2.Count > 0)
                    {
                        db.PRL219_2.AddRange(lstAddPRL219_2);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL219_2 = db.PRL219_2.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL219_2.Count > 0)
                    {
                        db.PRL219_2.RemoveRange(lstDeletePRL219_2);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL219_2 = db.PRL219_2.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL219_2.Count > 0)
                    {
                        db.PRL219_2.RemoveRange(lstDeletePRL219_2);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line6 details
        [HttpPost]
        public JsonResult SaveProtocolLine6Linkage(List<PRL219_3> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL219_3> lstAddPRL219_3 = new List<PRL219_3>();
                List<PRL219_3> lstDeletePRL219_3 = new List<PRL219_3>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL219_3 obj = db.PRL219_3.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL219_3();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Description = item.Description;
                        obj.ReqValue = item.ReqValue;
                        obj.ActAt0 = item.ActAt0;
                        obj.ActAt90 = item.ActAt90;
                        obj.ActAt180 = item.ActAt180;
                        obj.ActAt270 = item.ActAt270;

                        if (isAdded)
                        {
                            lstAddPRL219_3.Add(obj);
                        }
                    }
                    if (lstAddPRL219_3.Count > 0)
                    {
                        db.PRL219_3.AddRange(lstAddPRL219_3);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL219_3 = db.PRL219_3.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL219_3.Count > 0)
                    {
                        db.PRL219_3.RemoveRange(lstDeletePRL219_3);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL219_3 = db.PRL219_3.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL219_3.Count > 0)
                    {
                        db.PRL219_3.RemoveRange(lstDeletePRL219_3);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line7 details
        [HttpPost]
        public JsonResult SaveProtocolLine7Linkage(List<PRL219_4> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL219_4> lstAddPRL219_4 = new List<PRL219_4>();
                List<PRL219_4> lstDeletePRL219_4 = new List<PRL219_4>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        if (item.LineId > 0)
                        {
                            PRL219_4 obj = db.PRL219_4.Where(x => x.LineId == item.LineId).FirstOrDefault();
                            if (obj != null)
                            {
                                obj.Description = item.Description;
                                obj.Required = item.Required;
                                obj.Actual = item.Actual;
                                obj.EditedBy = UserName;
                                obj.EditedOn = DateTime.Now;
                            }
                        }
                        else
                        {
                            PRL219_4 obj = new PRL219_4();
                            obj.HeaderId = item.HeaderId;
                            if (!string.IsNullOrWhiteSpace(item.Description))
                            {
                                obj.Description = item.Description;
                                obj.Required = item.Required;
                                obj.Actual = item.Actual;
                                obj.CreatedBy = UserName;
                                obj.CreatedOn = DateTime.Now;
                                lstAddPRL219_4.Add(obj);
                            }
                        }
                    }
                    if (lstAddPRL219_4.Count > 0)
                    {
                        db.PRL219_4.AddRange(lstAddPRL219_4);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    var Headerid = lst.Where(x => x.HeaderId > 0).Select(a => a.HeaderId).FirstOrDefault();

                    lstDeletePRL219_4 = db.PRL219_4.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == Headerid).ToList();
                    if (lstDeletePRL219_4.Count > 0)
                    {
                        db.PRL219_4.RemoveRange(lstDeletePRL219_4);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL219_4 = db.PRL219_4.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL219_4.Count > 0)
                    {
                        db.PRL219_4.RemoveRange(lstDeletePRL219_4);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line8 details

        [HttpPost]
        public JsonResult SaveProtocolLine8Linkage(List<PRL219_5> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL219_5> lstAddPRL219_5 = new List<PRL219_5>();
                List<PRL219_5> lstDeletePRL219_5 = new List<PRL219_5>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL219_5 obj = db.PRL219_5.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL219_5();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.SpotNo = item.SpotNo;
                        obj.Width = item.Width;
                        obj.Depth = item.Depth;

                        if (isAdded)
                        {
                            lstAddPRL219_5.Add(obj);
                        }
                    }
                    if (lstAddPRL219_5.Count > 0)
                    {
                        db.PRL219_5.AddRange(lstAddPRL219_5);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRL219_5 = db.PRL219_5.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL219_5.Count > 0)
                    {
                        db.PRL219_5.RemoveRange(lstDeletePRL219_5);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRL219_5 = db.PRL219_5.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRL219_5.Count > 0)
                    {
                        db.PRL219_5.RemoveRange(lstDeletePRL219_5);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #endregion


    }
}