﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol081Controller : clsBase
    {
        //GET: PROTOCOL/Protocol081
        string ControllerURL = "/PROTOCOL/Protocol081/";
        string Title = "REPORT FOR VISUAL AND DIMENSION INSPECTION FOR NUB ON D'END BEFORE OVERLAY";

        #region Details View Code

        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO400 objPRO400 = new PRO400();

            if (!id.HasValue)
            {
                try
                {
                    objPRO400.ProtocolNo = string.Empty;
                    objPRO400.CreatedBy = objClsLoginInfo.UserName;
                    objPRO400.CreatedOn = DateTime.Now;

                    #region  Orientation | PRO401
                    objPRO400.PRO401.Add(new PRO401
                    {
                        Orientation = "0º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO401.Add(new PRO401
                    {
                        Orientation = "45º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO401.Add(new PRO401
                    {
                        Orientation = "90º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO401.Add(new PRO401
                    {
                        Orientation = "135º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO401.Add(new PRO401
                    {
                        Orientation = "180º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO401.Add(new PRO401
                    {
                        Orientation = "225º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO401.Add(new PRO401
                    {
                        Orientation = "270º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO401.Add(new PRO401
                    {
                        Orientation = "315º",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });


                    #endregion

                    #region  OrientationReqNubID | PRO402
                    objPRO400.PRO402.Add(new PRO402
                    {

                        OrientationReqNubID = "0°-180°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO402.Add(new PRO402
                    {

                        OrientationReqNubID = "22.5°-202.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO402.Add(new PRO402
                    {

                        OrientationReqNubID = "45°-225°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO402.Add(new PRO402
                    {

                        OrientationReqNubID = "67.5°-247.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO402.Add(new PRO402
                    {

                        OrientationReqNubID = "90°-270°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO402.Add(new PRO402
                    {

                        OrientationReqNubID = "112.5°-292.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO402.Add(new PRO402
                    {

                        OrientationReqNubID = "135°-315°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO402.Add(new PRO402
                    {

                        OrientationReqNubID = "157.5°-337.5°",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now

                    });


                    #endregion


                    #region  OtherDimensions | PRO403
                    objPRO400.PRO403.Add(new PRO403
                    {

                        OtherDimensions = "SURFACE FINISH ON MACHINED SURFACES",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO403.Add(new PRO403
                    {

                        OtherDimensions = "LEVEL OF D'END TRIMMING LINE / REF. LINE / WEP",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    objPRO400.PRO403.Add(new PRO403
                    {

                        OtherDimensions = "CONCENTRICITY OF NUB TO D'END OPEN END",
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });

                    #endregion

                    db.PRO400.Add(objPRO400);
                    db.SaveChanges();
                }
                catch (Exception ex)
                { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }

                ViewBag.HeaderId = objPRO400.HeaderId;

            }
            else
            {
                objPRO400 = db.PRO400.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();
            List<string> lstElevationOfNubFrom = new List<string> { "TRIMMING LINE", "REF. LINE", "B.T.L", "WEP" };


            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRO401> lstPRO401 = db.PRO401.Where(x => x.HeaderId == objPRO400.HeaderId).ToList();
            List<PRO402> lstPRO402 = db.PRO402.Where(x => x.HeaderId == objPRO400.HeaderId).ToList();
            List<PRO403> lstPRO403 = db.PRO403.Where(x => x.HeaderId == objPRO400.HeaderId).ToList();

            ViewBag.lstPRO401 = lstPRO401;
            ViewBag.lstPRO402 = lstPRO402;
            ViewBag.lstPRO403 = lstPRO403;


            #endregion

            return View(objPRO400);
        }

        public bool CheckSeamInSeamList(string seamno, string qualityproject, string location)
        {
            string deleted = clsImplementationEnum.QualityIdStageStatus.DELETED.GetStringValue();
            return db.QMS012.Any(c => c.SeamNo == seamno && c.QualityProject == qualityproject && c.Location == location);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO400 PRO400)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO400.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO400 objPRO400 = db.PRO400.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO400.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO400.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO400.ProtocolNo = PRO400.ProtocolNo;
                        objPRO400.ElevationOfNubFrom = PRO400.ElevationOfNubFrom;
                        objPRO400.ReqElevationOfNubFrom = PRO400.ReqElevationOfNubFrom;
                        objPRO400.ReqDimensionOFNub = PRO400.ReqDimensionOFNub;
                        objPRO400.ReqDimensionA = PRO400.ReqDimensionA;
                        objPRO400.ReqDimensionB = PRO400.ReqDimensionB;
                        objPRO400.ReqTopside = PRO400.ReqTopside;
                        objPRO400.ReqBottomside = PRO400.ReqBottomside;
                        objPRO400.TolReqElevationOfNubFrom = PRO400.TolReqElevationOfNubFrom;
                        objPRO400.TolDimensionOFNub = PRO400.TolDimensionOFNub;
                        objPRO400.TolDimensionA = PRO400.TolDimensionA;
                        objPRO400.TolDimensionB = PRO400.TolDimensionB;
                        objPRO400.TolTopside = PRO400.TolTopside;
                        objPRO400.TolBottomside = PRO400.TolBottomside;
                        objPRO400.NubID = PRO400.NubID;
                        objPRO400.ReqNubID = PRO400.ReqNubID;
                        objPRO400.OutByNubID = PRO400.OutByNubID;
                        objPRO400.CheckPoint1 = PRO400.CheckPoint1;
                        objPRO400.CheckPoint2 = PRO400.CheckPoint2;
                        objPRO400.CheckPoint3 = PRO400.CheckPoint3;
                        objPRO400.CheckPoint5 = PRO400.CheckPoint5;
                        objPRO400.CheckPoint5_2 = PRO400.CheckPoint5_2;
                        objPRO400.QCRemarks = PRO400.QCRemarks;
                        objPRO400.Result = PRO400.Result;
                        objPRO400.EditedBy = PRO400.EditedBy;
                        objPRO400.EditedOn = PRO400.EditedOn;
                        objPRO400.EditedBy = objClsLoginInfo.UserName;
                        objPRO400.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol submitted successfully";
                        objResponseMsg.HeaderId = objPRO400.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO400 objPRO400 = db.PRO400.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO400 != null && string.IsNullOrWhiteSpace(objPRO400.ProtocolNo))
                    {
                        db.PRO400.Remove(objPRO400);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.NUB_DIMENSION_INSPECTION_REPORTS_FOR_TAPER_TYPE.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO400.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1(List<PRO401> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO401> lstAddPRO401 = new List<PRO401>();
                List<PRO401> lstDeletePRO401 = new List<PRO401>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO401 obj = db.PRO401.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO401();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActElevation = item.ActElevation;
                        obj.ActDimension = item.ActDimension;
                        obj.ActADimension = item.ActADimension;
                        obj.ActBDimension = item.ActBDimension;
                        obj.ActTopside = item.ActTopside;
                        obj.ActBottomside = item.ActBottomside;


                        if (isAdded)
                        {
                            lstAddPRO401.Add(obj);
                        }
                    }
                    if (lstAddPRO401.Count > 0)
                    {
                        db.PRO401.AddRange(lstAddPRO401);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO401 = db.PRO401.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO401.Count > 0)
                    {
                        db.PRO401.RemoveRange(lstDeletePRO401);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO401 = db.PRO401.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO401.Count > 0)
                    {
                        db.PRO401.RemoveRange(lstDeletePRO401);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2(List<PRO402> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO402> lstAddPRO402 = new List<PRO402>();
                List<PRO402> lstDeletePRO402 = new List<PRO402>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO402 obj = db.PRO402.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO402();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        if (!string.IsNullOrWhiteSpace(item.OrientationReqNubID))
                        {


                            obj.OrientationReqNubID = item.OrientationReqNubID;
                            obj.ActualNubID = item.ActualNubID;
                            if (isAdded)
                            {
                                lstAddPRO402.Add(obj);
                            }
                        }


                    }
                    if (lstAddPRO402.Count > 0)
                    {
                        db.PRO402.AddRange(lstAddPRO402);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO402 = db.PRO402.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO402.Count > 0)
                    {
                        db.PRO402.RemoveRange(lstDeletePRO402);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO402 = db.PRO402.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO402.Count > 0)
                    {
                        db.PRO402.RemoveRange(lstDeletePRO402);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3(List<PRO403> lst, int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                List<PRO403> lstAddPRO403 = new List<PRO403>();
                List<PRO403> lstDeletePRO403 = new List<PRO403>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRO403 obj = db.PRO403.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRO403();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = objClsLoginInfo.UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = objClsLoginInfo.UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.OtherDimensions = item.OtherDimensions;
                        obj.ReqOtherDimensions = item.ReqOtherDimensions;
                        obj.ActOtherDimensions = item.ActOtherDimensions;


                        if (isAdded)
                        {
                            lstAddPRO403.Add(obj);
                        }
                    }
                    if (lstAddPRO403.Count > 0)
                    {
                        db.PRO403.AddRange(lstAddPRO403);
                    }

                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);

                    lstDeletePRO403 = db.PRO403.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO403.Count > 0)
                    {
                        db.PRO403.RemoveRange(lstDeletePRO403);
                    }
                    db.SaveChanges();
                }
                else
                {
                    lstDeletePRO403 = db.PRO403.Where(x => x.HeaderId == HeaderId).ToList();
                    if (lstDeletePRO403.Count > 0)
                    {
                        db.PRO403.RemoveRange(lstDeletePRO403);
                    }
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #endregion

        #region  Linkage View Code   

        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL400 objPRL400 = new PRL400();
            objPRL400 = db.PRL400.Where(i => i.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            if (objPRL400 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL400.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstElevationOfNubFrom = new List<string> { "TRIMMING LINE", "REF. LINE", "B.T.L", "WEP" };


            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            ViewBag.LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            ViewBag.OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL401> lstPRL401 = db.PRL401.Where(x => x.HeaderId == objPRL400.HeaderId).ToList();
            List<PRL402> lstPRL402 = db.PRL402.Where(x => x.HeaderId == objPRL400.HeaderId).ToList();
            List<PRL403> lstPRL403 = db.PRL403.Where(x => x.HeaderId == objPRL400.HeaderId).ToList();

            ViewBag.lstPRL401 = lstPRL401;
            ViewBag.lstPRL402 = lstPRL402;
            ViewBag.lstPRL403 = lstPRL403;


            #endregion

            try
            {
                ViewBag.isEditable = "false";
                ViewBag.isQCRemarkEditable = "false";
                if (m == 1)
                {
                    ViewBag.isEditable = "true";
                    ViewBag.DrawingNo = null;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL400.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL400.ActFilledBy) && (objPRL400.RequestNo.HasValue))
                        {
                            ViewBag.isEditable = "false";
                            ViewBag.isQCRemarkEditable = "true";
                        }
                        else
                        {
                            List<string> lstdrawing = (new GeneralController()).GetPLMDrawingNumber(objPRL400.Project).ToList();
                            ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
                        }
                    }
                }
                else
                { ViewBag.isEditable = "false"; }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                //List<string> lstdrawing = new List<string>();
                //lstdrawing.Add("Dummy Drawing Number");
                //ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();
            }
            return View(objPRL400);
        }

        [HttpPost]
        public ActionResult LinkageMobile(int? id, int? m, string APIRequestFrom = "", string UserName = "")
        {
            string ProjectDesc = "";
            string ManufacturingCode = "";
            string AccessRole = "";
            bool isEditable = false;
            var DrawingNo = new List<string>();
            bool isQCRemarkEditable = false;
            PRL400 objPRL400 = new PRL400();

            db.Configuration.ProxyCreationEnabled = false;
            objPRL400 = db.PRL400.Where(i => i.HeaderId == id).FirstOrDefault();
            db.Entry(objPRL400).State = System.Data.Entity.EntityState.Detached;

            NDEModels objNDEModels = new NDEModels();
            if (objPRL400 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL400.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_dsca).FirstOrDefault();
                    ManufacturingCode = objNDEModels.GetCategory("Manufacturing Code", objQproject.ManufacturingCode, objQproject.BU, objQproject.Location, true).CategoryDescription;
                }
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
            AccessRole = objUserRoleAccessDetails.UserDesignation;


            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            List<string> lstyesna = clsImplementationEnum.getyesna().ToList();

            List<string> lstElevationOfNubFrom = new List<string> { "TRIMMING LINE", "REF. LINE", "B.T.L", "WEP" };


            List<string> lstLocationLeftRight = clsImplementationEnum.getLocationLeftRight().ToList();
            List<string> lstOutsideInside = clsImplementationEnum.getOutsideInside().ToList();

            var YesNoEnum = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var YesNAEnum = lstyesna.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var ElevationOfNubFrom = lstElevationOfNubFrom.Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();


            var LeftRight = lstLocationLeftRight.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();
            var OutsideInside = lstOutsideInside.AsEnumerable().Select(x => new SelectListItem() { Value = x.ToString(), Text = x.ToString() }).ToList();

            #region bind lines
            List<PRL401> lstPRL401 = db.PRL401.Where(x => x.HeaderId == objPRL400.HeaderId).ToList();
            List<PRL402> lstPRL402 = db.PRL402.Where(x => x.HeaderId == objPRL400.HeaderId).ToList();
            List<PRL403> lstPRL403 = db.PRL403.Where(x => x.HeaderId == objPRL400.HeaderId).ToList();


            #endregion

            try
            {
                isEditable = false;
                isQCRemarkEditable = false;
                if (m == 1)
                {
                    isEditable = true;
                    if (objUserRoleAccessDetails.UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                    {
                        if (objPRL400.OfferDate.HasValue && !string.IsNullOrWhiteSpace(objPRL400.ActFilledBy) && (objPRL400.RequestNo.HasValue))
                        {
                            isEditable = false;
                            isQCRemarkEditable = true;
                        }
                        else
                        {
                            DrawingNo = (new GeneralController()).GetPLMDrawingNumber(objPRL400.Project).ToList();
                        }
                    }
                }

                return Json(new
                {
                    res = "1",
                    msg = "Success",
                    ProjectDesc = ProjectDesc,
                    ManufacturingCode = ManufacturingCode,
                    isEditable = isEditable,
                    isQCRemarkEditable = isQCRemarkEditable,
                    DrawingNo = DrawingNo,
                    AccessRole = AccessRole,

                    YesNoEnum = YesNoEnum,
                    YesNAEnum = YesNAEnum,
                    ElevationOfNubFrom = ElevationOfNubFrom,
                    OutsideInside = OutsideInside,
                    LeftRight = LeftRight,

                    objPRL400 = objPRL400,

                    lstPRL401 = lstPRL401,
                    lstPRL402 = lstPRL402,
                    lstPRL403 = lstPRL403,

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                // Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                // List<string> lstdrawing = new List<string>();
                // lstdrawing.Add("Dummy Drawing Number");
                // ViewBag.DrawingNo = lstdrawing.AsEnumerable().Select(x => new { Code = x, CategoryDescription = x }).ToList();

                return Json(new
                {
                    res = "0",
                    msg = ex.Message,

                }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL400 PRL400, int s = 0, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                int? refHeaderId = PRL400.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL400 objPRL400 = db.PRL400.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL400.DrawingRevisionNo = PRL400.DrawingRevisionNo;
                    objPRL400.DrawingNo = PRL400.DrawingNo;
                    objPRL400.DCRNo = PRL400.DCRNo;
                    objPRL400.ProtocolNo = PRL400.ProtocolNo;
                    objPRL400.ElevationOfNubFrom = PRL400.ElevationOfNubFrom;
                    objPRL400.ReqElevationOfNubFrom = PRL400.ReqElevationOfNubFrom;
                    objPRL400.ReqDimensionOFNub = PRL400.ReqDimensionOFNub;
                    objPRL400.ReqDimensionA = PRL400.ReqDimensionA;
                    objPRL400.ReqDimensionB = PRL400.ReqDimensionB;
                    objPRL400.ReqTopside = PRL400.ReqTopside;
                    objPRL400.ReqBottomside = PRL400.ReqBottomside;
                    objPRL400.TolReqElevationOfNubFrom = PRL400.TolReqElevationOfNubFrom;
                    objPRL400.TolDimensionOFNub = PRL400.TolDimensionOFNub;
                    objPRL400.TolDimensionA = PRL400.TolDimensionA;
                    objPRL400.TolDimensionB = PRL400.TolDimensionB;
                    objPRL400.TolTopside = PRL400.TolTopside;
                    objPRL400.TolBottomside = PRL400.TolBottomside;
                    objPRL400.NubID = PRL400.NubID;
                    objPRL400.ReqNubID = PRL400.ReqNubID;
                    objPRL400.OutByNubID = PRL400.OutByNubID;
                    objPRL400.CheckPoint1 = PRL400.CheckPoint1;
                    objPRL400.CheckPoint2 = PRL400.CheckPoint2;
                    objPRL400.CheckPoint3 = PRL400.CheckPoint3;
                    objPRL400.CheckPoint5 = PRL400.CheckPoint5;
                    objPRL400.CheckPoint5_2 = PRL400.CheckPoint5_2;
                    objPRL400.QCRemarks = PRL400.QCRemarks;
                    objPRL400.Result = PRL400.Result;
                    objPRL400.EditedBy = UserName;
                    objPRL400.EditedOn = DateTime.Now;

                    if (s == 1)
                    {
                        UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol(APIRequestFrom, UserName);
                        string UserRole = objUserRoleAccessDetails.UserRole;
                        if (UserRole == clsImplementationEnum.UserRoleName.PROD3.GetStringValue())
                        {
                            objPRL400.ActFilledBy = UserName;
                            objPRL400.ActFilledOn = DateTime.Now;
                        }
                        if (UserRole == clsImplementationEnum.UserRoleName.QC3.GetStringValue())
                        {
                            objPRL400.ReqFilledBy = UserName;
                            objPRL400.ReqFilledOn = DateTime.Now;
                        }
                    }

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol submitted successfully";
                    objResponseMsg.HeaderId = objPRL400.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL400 objPRL400 = db.PRL400.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL400 != null && string.IsNullOrWhiteSpace(objPRL400.ProtocolNo))
                    {
                        db.PRL400.Remove(objPRL400);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL400.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Line1 details
        [HttpPost]
        public JsonResult SaveProtocolLine1Linkage(List<PRL401> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL401> lstAddPRL401 = new List<PRL401>();
                List<PRL401> lstDeletePRL401 = new List<PRL401>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL401 obj = db.PRL401.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL401();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Orientation = item.Orientation;
                        obj.ActElevation = item.ActElevation;
                        obj.ActDimension = item.ActDimension;
                        obj.ActADimension = item.ActADimension;
                        obj.ActBDimension = item.ActBDimension;
                        obj.ActTopside = item.ActTopside;
                        obj.ActBottomside = item.ActBottomside;

                        if (isAdded)
                        {
                            lstAddPRL401.Add(obj);
                        }
                    }
                }
                if (lstAddPRL401.Count > 0)
                {
                    db.PRL401.AddRange(lstAddPRL401);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL401 = db.PRL401.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL401 = db.PRL401.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL401.Count > 0)
                {
                    db.PRL401.RemoveRange(lstDeletePRL401);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public JsonResult SaveProtocolLine2Linkage(List<PRL402> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL402> lstAddPRL402 = new List<PRL402>();
                List<PRL402> lstDeletePRL402 = new List<PRL402>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL402 obj = db.PRL402.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL402();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        if (!string.IsNullOrWhiteSpace(item.OrientationReqNubID))
                        {

                            obj.OrientationReqNubID = item.OrientationReqNubID;
                            obj.ActualNubID = item.ActualNubID;

                            if (isAdded)
                            {
                                lstAddPRL402.Add(obj);
                            }
                        }
                    }
                }
                if (lstAddPRL402.Count > 0)
                {
                    db.PRL402.AddRange(lstAddPRL402);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL402 = db.PRL402.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL402 = db.PRL402.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL402.Count > 0)
                {
                    db.PRL402.RemoveRange(lstDeletePRL402);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion                      

        #region Line3 details
        [HttpPost]
        public JsonResult SaveProtocolLine3Linkage(List<PRL403> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<PRL403> lstAddPRL403 = new List<PRL403>();
                List<PRL403> lstDeletePRL403 = new List<PRL403>();
                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        PRL403 obj = db.PRL403.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null)
                        {
                            obj = new PRL403();
                            obj.HeaderId = item.HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.OtherDimensions = item.OtherDimensions;
                        obj.ReqOtherDimensions = item.ReqOtherDimensions;
                        obj.ActOtherDimensions = item.ActOtherDimensions;


                        if (isAdded)
                        {
                            lstAddPRL403.Add(obj);
                        }
                    }
                }
                if (lstAddPRL403.Count > 0)
                {
                    db.PRL403.AddRange(lstAddPRL403);
                }

                var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                if (lst != null)
                {
                    lstDeletePRL403 = db.PRL403.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeletePRL403 = db.PRL403.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeletePRL403.Count > 0)
                {
                    db.PRL403.RemoveRange(lstDeletePRL403);
                }
                db.SaveChanges();

                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
    }
}