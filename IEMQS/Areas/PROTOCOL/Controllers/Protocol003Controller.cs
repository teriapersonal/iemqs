﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol003Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol003/";
        string Title = "SET-UP INSPECTION REPORT FOR FORMED Components";

        #region Utility
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PROD3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => (i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc == clsImplementationEnum.UserRoleName.QC2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI3.GetStringValue())).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = string.Empty;
                    objUserRoleAccessDetails.UserDesignation = string.Empty;
                }

            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }

        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                //htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }

        public decimal? GetDecimalValue(string value)
        {
            decimal outValue;
            if (decimal.TryParse(value, out outValue))
            {
                return outValue;
            }
            else
            {
                return null;
            }
        }
        #endregion
        // GET: PROTOCOL/Protocol003

        #region Protocol
        #region Header Details Page
        [SessionExpireFilter]// [UserPermissions, AllowAnonymous]
        public ActionResult Details(int? id, int? m)
        {
            PRO010 objPRO010 = null;
            ViewBag.YesNoEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "No", CategoryDescription = "No" } };
            ViewBag.YesNAEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.YesNoNAEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "No", CategoryDescription = "No" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.RTUTExtentEnum = new List<CategoryData> { new CategoryData { Value = "100%", Code = "100%", CategoryDescription = "100%" }, new CategoryData { Value = "SPOT", Code = "SPOT", CategoryDescription = "SPOT" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.TOFDUTExtentEnum = new List<CategoryData> { new CategoryData { Value = "100%", Code = "100%", CategoryDescription = "100%" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;
            if (id > 0)
            {
                objPRO010 = db.PRO010.Where(x => x.HeaderId == id).FirstOrDefault();
                if (objPRO010 == null)
                {
                    objPRO010 = new PRO010();
                    objPRO010.ProtocolNo = string.Empty;
                    objPRO010.CreatedBy = objClsLoginInfo.UserName;
                    objPRO010.CreatedOn = DateTime.Now;
                    db.PRO010.Add(objPRO010);
                    db.SaveChanges();
                }
            }
            else
            {
                objPRO010 = new PRO010();
                objPRO010.ProtocolNo = string.Empty;
                objPRO010.CreatedBy = objClsLoginInfo.UserName;
                objPRO010.CreatedOn = DateTime.Now;
                db.PRO010.Add(objPRO010);
                db.SaveChanges();
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO010);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO010 model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                bool duplicateProtocol = db.PRO010.Where(x => x.ProtocolNo == model.ProtocolNo && x.HeaderId != model.HeaderId).Any();
                if (duplicateProtocol)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolExists.ToString();
                    objResponseMsg.HeaderId = 0;
                }
                else
                {
                    if (model.HeaderId > 0)
                    {
                        PRO010 objPRO010 = db.PRO010.Where(c => c.HeaderId == model.HeaderId).FirstOrDefault();

                        #region Save Data
                        objPRO010.ProtocolNo = model.ProtocolNo;
                        objPRO010.Component = model.Component;
                        objPRO010.TypeOfDEndCone = model.TypeOfDEndCone;
                        objPRO010.Remarks_Both = model.Remarks_Both;
                        objPRO010.CircumferenceOutSideReqTop = model.CircumferenceOutSideReqTop;
                        objPRO010.CircumferenceOutSideReqBottom = model.CircumferenceOutSideReqBottom;
                        objPRO010.CircumferenceOutSideActTop = model.CircumferenceOutSideActTop;
                        objPRO010.CircumferenceOutSideActBottom = model.CircumferenceOutSideActBottom;
                        objPRO010.DiameterOvalityReqTop = model.DiameterOvalityReqTop;
                        objPRO010.DiameterOvalityReqBottom = model.DiameterOvalityReqBottom;
                        objPRO010.DiameterOvalityActTopMax = model.DiameterOvalityActTopMax;
                        objPRO010.DiameterOvalityActTopMin = model.DiameterOvalityActTopMin;
                        objPRO010.DiameterOvalityActBottomMax = model.DiameterOvalityActBottomMax;
                        objPRO010.DiameterOvalityActBottomMin = model.DiameterOvalityActBottomMin;
                        objPRO010.ThicknessAtTrimmingLineMin = model.ThicknessAtTrimmingLineMin;
                        objPRO010.ThicknessAtTrimmingLineTop = model.ThicknessAtTrimmingLineTop;
                        objPRO010.ThicknessAtTrimmingLineNom = model.ThicknessAtTrimmingLineNom;
                        objPRO010.ThicknessAtTrimmingLineBot = model.ThicknessAtTrimmingLineBot;
                        objPRO010.CircumferenceInsideTop = model.CircumferenceInsideTop;
                        objPRO010.CircumferenceInsideBottom = model.CircumferenceInsideBottom;
                        objPRO010.CircumferenceInsideMax = model.CircumferenceInsideMax;
                        objPRO010.CircumferenceInsideMin = model.CircumferenceInsideMin;
                        objPRO010.TotalHeightReq = model.TotalHeightReq;
                        objPRO010.TotalHeightAct = model.TotalHeightAct;
                        objPRO010.ThicknessReqMin = model.ThicknessReqMin;
                        objPRO010.ThicknessReqNom = model.ThicknessReqNom;
                        objPRO010.ThicknessActMin = model.ThicknessActMin;
                        objPRO010.ThicknessActNom = model.ThicknessActNom;
                        objPRO010.StraightFaceReq = model.StraightFaceReq;
                        objPRO010.StraightFaceAct = model.StraightFaceAct;
                        objPRO010.CrownRadiusReq = model.CrownRadiusReq;
                        objPRO010.CrownRadiusAct = model.CrownRadiusAct;
                        objPRO010.KnuckleRadiusReq = model.KnuckleRadiusReq;
                        objPRO010.KnuckleRadiusAct = model.KnuckleRadiusAct;
                        objPRO010.OverCrowningReq = model.OverCrowningReq;
                        objPRO010.OverCrowningAct = model.OverCrowningAct;
                        objPRO010.UnderCrowningReq = model.UnderCrowningReq;
                        objPRO010.UnderCrowningAct = model.UnderCrowningAct;
                        objPRO010.TemplateCheckedOnLayout = model.TemplateCheckedOnLayout;
                        objPRO010.ProfCheckPatelW3DTmplt = model.ProfCheckPatelW3DTmplt;
                        objPRO010.NonDestructiveTestingRequired1 = model.NonDestructiveTestingRequired1;
                        objPRO010.NonDestructiveTestingRequired2 = model.NonDestructiveTestingRequired2;
                        objPRO010.NonDestructiveTestingRequired3 = model.NonDestructiveTestingRequired3;
                        objPRO010.NonDestructiveTestingISSurface1 = model.NonDestructiveTestingISSurface1;
                        objPRO010.NonDestructiveTestingISSurface2 = model.NonDestructiveTestingISSurface2;
                        objPRO010.NonDestructiveTestingISSurface3 = model.NonDestructiveTestingISSurface3;
                        objPRO010.NonDestructiveTestingOSSurface1 = model.NonDestructiveTestingOSSurface1;
                        objPRO010.NonDestructiveTestingOSSurface2 = model.NonDestructiveTestingOSSurface2;
                        objPRO010.NonDestructiveTestingOSSurface3 = model.NonDestructiveTestingOSSurface3;
                        objPRO010.NonDestructiveTestingKnuckle1 = model.NonDestructiveTestingKnuckle1;
                        objPRO010.NonDestructiveTestingKnuckle2 = model.NonDestructiveTestingKnuckle2;
                        objPRO010.NonDestructiveTestingKnuckle3 = model.NonDestructiveTestingKnuckle3;
                        objPRO010.NonDestructiveTestingWEP1 = model.NonDestructiveTestingWEP1;
                        objPRO010.NonDestructiveTestingWEP2 = model.NonDestructiveTestingWEP2;
                        objPRO010.NonDestructiveTestingWEP3 = model.NonDestructiveTestingWEP3;
                        objPRO010.NonDestructiveTestingRemarks1 = model.NonDestructiveTestingRemarks1;
                        objPRO010.NonDestructiveTestingRemarks2 = model.NonDestructiveTestingRemarks2;
                        objPRO010.NonDestructiveTestingRemarks3 = model.NonDestructiveTestingRemarks3;
                        objPRO010.TypeOfHT = model.TypeOfHT;
                        objPRO010.Cycle = model.Cycle;
                        objPRO010.HTGraphReviewed = model.HTGraphReviewed;
                        objPRO010.HeatTreatmentDetailsRemarks = model.HeatTreatmentDetailsRemarks;
                        objPRO010.TapeNo = model.TapeNo;
                        objPRO010.CalibrationDueDate = model.CalibrationDueDate;
                        objPRO010.SeamNoHardStampTransfer = model.SeamNoHardStampTransfer;
                        objPRO010.OutOfRoundnessOvalityReadings = model.OutOfRoundnessOvalityReadings;
                        objPRO010.RequiredID = model.RequiredID;
                        objPRO010.ActualId1 = model.ActualId1;
                        objPRO010.ActualId2 = model.ActualId2;
                        objPRO010.ActualId3 = model.ActualId3;
                        objPRO010.ActualId4 = model.ActualId4;
                        objPRO010.ActualId5 = model.ActualId5;
                        objPRO010.ActualId6 = model.ActualId6;
                        objPRO010.ActualId7 = model.ActualId7;
                        objPRO010.ActualId8 = model.ActualId8;
                        objPRO010.ActualId9 = model.ActualId9;
                        objPRO010.ActualId10 = model.ActualId10;
                        objPRO010.ActualId11 = model.ActualId11;
                        objPRO010.ActualId12 = model.ActualId12;
                        objPRO010.ActualId13 = model.ActualId13;
                        objPRO010.ActualId14 = model.ActualId14;
                        objPRO010.ActualId15 = model.ActualId15;
                        objPRO010.ActualId16 = model.ActualId16;
                        objPRO010.RemarksId = model.RemarksId;
                        objPRO010.OutOfRoundnessMax = model.OutOfRoundnessMax;
                        objPRO010.OutOfRoundnessMin = model.OutOfRoundnessMin;
                        objPRO010.Reading1 = model.Reading1;
                        objPRO010.Reading2 = model.Reading2;
                        objPRO010.Reading3 = model.Reading3;
                        objPRO010.Reading4 = model.Reading4;
                        objPRO010.Reading5 = model.Reading5;
                        objPRO010.Reading6 = model.Reading6;
                        objPRO010.Reading7 = model.Reading7;
                        objPRO010.Reading8 = model.Reading8;
                        objPRO010.Reading9 = model.Reading9;
                        objPRO010.Reading10 = model.Reading10;
                        objPRO010.Reading11 = model.Reading11;
                        objPRO010.Reading12 = model.Reading12;
                        objPRO010.Reading13 = model.Reading13;
                        objPRO010.Reading14 = model.Reading14;
                        objPRO010.Reading15 = model.Reading15;
                        objPRO010.Reading16 = model.Reading16;
                        objPRO010.Reading17 = model.Reading17;
                        objPRO010.Reading18 = model.Reading18;
                        objPRO010.Reading19 = model.Reading19;
                        objPRO010.Reading20 = model.Reading20;
                        objPRO010.Reading21 = model.Reading21;
                        objPRO010.Reading22 = model.Reading22;
                        objPRO010.Reading23 = model.Reading23;
                        objPRO010.Reading24 = model.Reading24;
                        objPRO010.Reading25 = model.Reading25;
                        objPRO010.Reading26 = model.Reading26;
                        objPRO010.Reading27 = model.Reading27;
                        objPRO010.Reading28 = model.Reading28;
                        objPRO010.Reading29 = model.Reading29;
                        objPRO010.Reading30 = model.Reading30;
                        objPRO010.Reading31 = model.Reading31;
                        objPRO010.Reading32 = model.Reading32;
                        objPRO010.ReadingAverage = model.ReadingAverage;
                        objPRO010.Concntricity = model.Concntricity;
                        objPRO010.ThickCheckedWithThickMeter = model.ThickCheckedWithThickMeter;
                        objPRO010.SeamNo1 = model.SeamNo1;
                        objPRO010.SeamNo2 = model.SeamNo2;
                        objPRO010.SeamNo3 = model.SeamNo3;
                        objPRO010.SeamNo4 = model.SeamNo4;
                        objPRO010.SeamNo5 = model.SeamNo5;
                        objPRO010.SeamNo6 = model.SeamNo6;
                        objPRO010.SeamNo7 = model.SeamNo7;
                        objPRO010.SeamNo8 = model.SeamNo8;
                        objPRO010.RootGapRequired = model.RootGapRequired;
                        objPRO010.PickInOutRequired = model.PickInOutRequired;
                        objPRO010.RootGap1_1 = model.RootGap1_1;
                        objPRO010.RootGap1_2 = model.RootGap1_2;
                        objPRO010.RootGap1_3 = model.RootGap1_3;
                        objPRO010.RootGap1_4 = model.RootGap1_4;
                        objPRO010.RootGap1_5 = model.RootGap1_5;
                        objPRO010.RootGap1_6 = model.RootGap1_6;
                        objPRO010.RootGap1_7 = model.RootGap1_7;
                        objPRO010.RootGap1_8 = model.RootGap1_8;
                        objPRO010.RootGap2_1 = model.RootGap2_1;
                        objPRO010.RootGap2_2 = model.RootGap2_2;
                        objPRO010.RootGap2_3 = model.RootGap2_3;
                        objPRO010.RootGap2_4 = model.RootGap2_4;
                        objPRO010.RootGap2_5 = model.RootGap2_5;
                        objPRO010.RootGap2_6 = model.RootGap2_6;
                        objPRO010.RootGap2_7 = model.RootGap2_7;
                        objPRO010.RootGap2_8 = model.RootGap2_8;
                        objPRO010.RootGap3_1 = model.RootGap3_1;
                        objPRO010.RootGap3_2 = model.RootGap3_2;
                        objPRO010.RootGap3_3 = model.RootGap3_3;
                        objPRO010.RootGap3_4 = model.RootGap3_4;
                        objPRO010.RootGap3_5 = model.RootGap3_5;
                        objPRO010.RootGap3_6 = model.RootGap3_6;
                        objPRO010.RootGap3_7 = model.RootGap3_7;
                        objPRO010.RootGap3_8 = model.RootGap3_8;
                        objPRO010.RootGap4_1 = model.RootGap4_1;
                        objPRO010.RootGap4_2 = model.RootGap4_2;
                        objPRO010.RootGap4_3 = model.RootGap4_3;
                        objPRO010.RootGap4_4 = model.RootGap4_4;
                        objPRO010.RootGap4_5 = model.RootGap4_5;
                        objPRO010.RootGap4_6 = model.RootGap4_6;
                        objPRO010.RootGap4_7 = model.RootGap4_7;
                        objPRO010.RootGap4_8 = model.RootGap4_8;
                        objPRO010.RootGap5_1 = model.RootGap5_1;
                        objPRO010.RootGap5_2 = model.RootGap5_2;
                        objPRO010.RootGap5_3 = model.RootGap5_3;
                        objPRO010.RootGap5_4 = model.RootGap5_4;
                        objPRO010.RootGap5_5 = model.RootGap5_5;
                        objPRO010.RootGap5_6 = model.RootGap5_6;
                        objPRO010.RootGap5_7 = model.RootGap5_7;
                        objPRO010.RootGap5_8 = model.RootGap5_8;
                        objPRO010.PickInOut1_1 = model.PickInOut1_1;
                        objPRO010.PickInOut1_2 = model.PickInOut1_2;
                        objPRO010.PickInOut1_3 = model.PickInOut1_3;
                        objPRO010.PickInOut1_4 = model.PickInOut1_4;
                        objPRO010.PickInOut1_5 = model.PickInOut1_5;
                        objPRO010.PickInOut1_6 = model.PickInOut1_6;
                        objPRO010.PickInOut1_7 = model.PickInOut1_7;
                        objPRO010.PickInOut1_8 = model.PickInOut1_8;
                        objPRO010.PickInOut3_1 = model.PickInOut3_1;
                        objPRO010.PickInOut3_2 = model.PickInOut3_2;
                        objPRO010.PickInOut3_3 = model.PickInOut3_3;
                        objPRO010.PickInOut3_4 = model.PickInOut3_4;
                        objPRO010.PickInOut3_5 = model.PickInOut3_5;
                        objPRO010.PickInOut3_6 = model.PickInOut3_6;
                        objPRO010.PickInOut3_7 = model.PickInOut3_7;
                        objPRO010.PickInOut3_8 = model.PickInOut3_8;
                        objPRO010.PickInOut5_1 = model.PickInOut5_1;
                        objPRO010.PickInOut5_2 = model.PickInOut5_2;
                        objPRO010.PickInOut5_3 = model.PickInOut5_3;
                        objPRO010.PickInOut5_4 = model.PickInOut5_4;
                        objPRO010.PickInOut5_5 = model.PickInOut5_5;
                        objPRO010.PickInOut5_6 = model.PickInOut5_6;
                        objPRO010.PickInOut5_7 = model.PickInOut5_7;
                        objPRO010.PickInOut5_8 = model.PickInOut5_8;
                        objPRO010.RootGapRequired2 = model.RootGapRequired2;
                        objPRO010.PickInOutCheckedWithD4Tmplt = model.PickInOutCheckedWithD4Tmplt;
                        objPRO010.SeamNo9 = model.SeamNo9;
                        objPRO010.SeamNo10 = model.SeamNo10;
                        objPRO010.SeamNo11 = model.SeamNo11;
                        objPRO010.SeamNo12 = model.SeamNo12;
                        objPRO010.SeamNo13 = model.SeamNo13;
                        objPRO010.SeamNo14 = model.SeamNo14;
                        objPRO010.SeamNo15 = model.SeamNo15;
                        objPRO010.SeamNo16 = model.SeamNo16;
                        objPRO010.RootGap1_9 = model.RootGap1_9;
                        objPRO010.RootGap1_10 = model.RootGap1_10;
                        objPRO010.RootGap1_11 = model.RootGap1_11;
                        objPRO010.RootGap1_12 = model.RootGap1_12;
                        objPRO010.RootGap1_13 = model.RootGap1_13;
                        objPRO010.RootGap1_14 = model.RootGap1_14;
                        objPRO010.RootGap1_15 = model.RootGap1_15;
                        objPRO010.RootGap2_9 = model.RootGap2_9;
                        objPRO010.RootGap2_10 = model.RootGap2_10;
                        objPRO010.RootGap2_11 = model.RootGap2_11;
                        objPRO010.RootGap2_12 = model.RootGap2_12;
                        objPRO010.RootGap2_13 = model.RootGap2_13;
                        objPRO010.RootGap2_14 = model.RootGap2_14;
                        objPRO010.RootGap2_15 = model.RootGap2_15;
                        objPRO010.RootGap3_9 = model.RootGap3_9;
                        objPRO010.RootGap3_10 = model.RootGap3_10;
                        objPRO010.RootGap3_11 = model.RootGap3_11;
                        objPRO010.RootGap3_12 = model.RootGap3_12;
                        objPRO010.RootGap3_13 = model.RootGap3_13;
                        objPRO010.RootGap3_14 = model.RootGap3_14;
                        objPRO010.RootGap3_15 = model.RootGap3_15;
                        objPRO010.RootGap4_9 = model.RootGap4_9;
                        objPRO010.RootGap4_10 = model.RootGap4_10;
                        objPRO010.RootGap4_11 = model.RootGap4_11;
                        objPRO010.RootGap4_12 = model.RootGap4_12;
                        objPRO010.RootGap4_13 = model.RootGap4_13;
                        objPRO010.RootGap4_14 = model.RootGap4_14;
                        objPRO010.RootGap4_15 = model.RootGap4_15;
                        objPRO010.RootGap5_9 = model.RootGap5_9;
                        objPRO010.RootGap5_10 = model.RootGap5_10;
                        objPRO010.RootGap5_11 = model.RootGap5_11;
                        objPRO010.RootGap5_12 = model.RootGap5_12;
                        objPRO010.RootGap5_13 = model.RootGap5_13;
                        objPRO010.RootGap5_14 = model.RootGap5_14;
                        objPRO010.RootGap5_15 = model.RootGap5_15;
                        objPRO010.AngleReq = model.AngleReq;
                        objPRO010.AngleAct = model.AngleAct;
                        objPRO010.ISReq = model.ISReq;
                        objPRO010.ISAct = model.ISAct;
                        objPRO010.OWReq = model.OWReq;
                        objPRO010.OWAct = model.OWAct;
                        objPRO010.RootFaceReq = model.RootFaceReq;
                        objPRO010.RootFaceAct = model.RootFaceAct;
                        objPRO010.PetalJointRootGap1 = model.PetalJointRootGap1;
                        objPRO010.PetalJointRootGap2 = model.PetalJointRootGap2;
                        objPRO010.PetalJointRootGap3 = model.PetalJointRootGap3;
                        objPRO010.PetalJointRootGap4 = model.PetalJointRootGap4;
                        objPRO010.PetalJointRootGap5 = model.PetalJointRootGap5;
                        objPRO010.PetalJointRootGap6 = model.PetalJointRootGap6;
                        objPRO010.PetalJointOffset1 = model.PetalJointOffset1;
                        objPRO010.PetalJointOffset2 = model.PetalJointOffset2;
                        objPRO010.PetalJointOffset3 = model.PetalJointOffset3;
                        objPRO010.PetalJointOffset4 = model.PetalJointOffset4;
                        objPRO010.PetalJointOffset5 = model.PetalJointOffset5;
                        objPRO010.PetalJointOffset6 = model.PetalJointOffset6;
                        objPRO010.PetalJointPickInOut1 = model.PetalJointPickInOut1;
                        objPRO010.PetalJointPickInOut2 = model.PetalJointPickInOut2;
                        objPRO010.PetalJointPickInOut3 = model.PetalJointPickInOut3;
                        objPRO010.PetalJointPickInOut4 = model.PetalJointPickInOut4;
                        objPRO010.PetalJointPickInOut5 = model.PetalJointPickInOut5;
                        objPRO010.PetalJointPickInOut6 = model.PetalJointPickInOut6;
                        objPRO010.PetalJointRequired1 = model.PetalJointRequired1;
                        objPRO010.PetalJointRequired2 = model.PetalJointRequired2;
                        objPRO010.PetalJointRequired3 = model.PetalJointRequired3;
                        objPRO010.Checkpoint1 = model.Checkpoint1;
                        objPRO010.Checkpoint2 = model.Checkpoint2;
                        objPRO010.Checkpoint3 = model.Checkpoint3;
                        objPRO010.Checkpoint4 = model.Checkpoint4;
                        objPRO010.Checkpoint5 = model.Checkpoint5;
                        objPRO010.Checkpoint6 = model.Checkpoint6;
                        objPRO010.Checkpoint7 = model.Checkpoint7;
                        objPRO010.Checkpoint8 = model.Checkpoint8;
                        objPRO010.Checkpoint9 = model.Checkpoint9;
                        objPRO010.Checkpoint10 = model.Checkpoint10;
                        objPRO010.Checkpoint11 = model.Checkpoint11;
                        objPRO010.Checkpoint12 = model.Checkpoint12;
                        objPRO010.Checkpoint13 = model.Checkpoint13;
                        objPRO010.WPSNO = model.WPSNO;
                        objPRO010.MaterialIdentificationRPT = model.MaterialIdentificationRPT;
                        objPRO010.CladStripingWidthMax = model.CladStripingWidthMax;
                        objPRO010.CladStripingWidthMin = model.CladStripingWidthMin;
                        objPRO010.CladStripingDepthMax = model.CladStripingDepthMax;
                        objPRO010.CladStripingDepthMin = model.CladStripingDepthMin;
                        objPRO010.FNote1 = model.FNote1;
                        objPRO010.FNote2 = model.FNote2;
                        objPRO010.FNote3 = model.FNote3;
                        objPRO010.EditedBy = objClsLoginInfo.UserName;
                        objPRO010.EditedOn = DateTime.Now;
                        objPRO010.CladdedVessel = model.CladdedVessel;
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO010.HeaderId;
                        #endregion

                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(int refHeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                //int? refHeaderId = obj.HeaderId;
                if (refHeaderId > 0)
                {
                    PRO010 objPRO010 = db.PRO010.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO010 != null && string.IsNullOrWhiteSpace(objPRO010.ProtocolNo))
                    {
                        db.PRO011.RemoveRange(objPRO010.PRO011.ToList());
                        db.PRO012.RemoveRange(objPRO010.PRO012.ToList());
                        db.PRO013.RemoveRange(objPRO010.PRO013.ToList());
                        db.PRO014_1.RemoveRange(objPRO010.PRO014_1.ToList());
                        db.PRO014_2.RemoveRange(objPRO010.PRO014_2.ToList());
                        db.PRO010.Remove(objPRO010);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.SETUP_INSPECTION_REPORT_FOR_FORMED.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO010.HeaderId;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Protocol003 Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                
                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SrNo like '%" + param.sSearch
                        + "%' or Petal1 like '%" + param.sSearch
                        + "%' or Petal2 like '%" + param.sSearch
                        + "%' or Petal3 like '%" + param.sSearch
                        + "%' or Petal4 like '%" + param.sSearch
                        + "%' or Petal5 like '%" + param.sSearch
                        + "%' or Petal6 like '%" + param.sSearch
                        + "%' or Petal7 like '%" + param.sSearch
                        + "%' or Petal8 like '%" + param.sSearch
                        + "%' or Petal9 like '%" + param.sSearch
                        + "%' or Petal10 like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL003_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO010 objPRO010 = db.PRO010.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SrNo", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal1", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal2", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal3", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal4", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal5", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal6", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal7", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal8", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal9", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal10", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                   "1",
                                   Convert.ToString(uc.HeaderId),
                                   Convert.ToString(uc.LineId),
                                   Helper.GenerateHTMLTextbox(uc.LineId ,"SrNo",uc.SrNo,"", true, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId ,"Petal1",Convert.ToString(uc.Petal1 ), "", true, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId ,"Petal2",Convert.ToString(uc.Petal2 ), "", true, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId ,"Petal3",Convert.ToString(uc.Petal3 ), "", true, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId ,"Petal4",Convert.ToString(uc.Petal4 ), "", true, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId ,"Petal5",Convert.ToString(uc.Petal5 ), "", true, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId ,"Petal6",Convert.ToString(uc.Petal6 ), "", true, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId ,"Petal7",Convert.ToString(uc.Petal7 ), "", true, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId ,"Petal8",Convert.ToString(uc.Petal8 ), "", true, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId ,"Petal9",Convert.ToString(uc.Petal9 ), "", true, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(uc.LineId , "Petal10",Convert.ToString(uc.Petal10) , "", true, "", false, "10", "PROD"),
                                   HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO011 objPRO011 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRO010 objPRO010 = db.PRO010.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        #region Edit Petals
                        objPRO011 = db.PRO011.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();

                        objPRO011.SrNo = !string.IsNullOrEmpty(fc["SrNo" + refLineId]) ? Convert.ToString(fc["SrNo" + refLineId]).Trim() : "";
                        objPRO011.Petal1 = GetDecimalValue(Convert.ToString(fc["Petal1" + refLineId]));
                        objPRO011.Petal2 = GetDecimalValue(Convert.ToString(fc["Petal2" + refLineId]));
                        objPRO011.Petal3 = GetDecimalValue(Convert.ToString(fc["Petal3" + refLineId]));
                        objPRO011.Petal4 = GetDecimalValue(Convert.ToString(fc["Petal4" + refLineId]));
                        objPRO011.Petal5 = GetDecimalValue(Convert.ToString(fc["Petal5" + refLineId]));
                        objPRO011.Petal6 = GetDecimalValue(Convert.ToString(fc["Petal6" + refLineId]));
                        objPRO011.Petal7 = GetDecimalValue(Convert.ToString(fc["Petal7" + refLineId]));
                        objPRO011.Petal8 = GetDecimalValue(Convert.ToString(fc["Petal8" + refLineId]));
                        objPRO011.Petal9 = GetDecimalValue(Convert.ToString(fc["Petal9" + refLineId]));
                        objPRO011.Petal10 = GetDecimalValue(Convert.ToString(fc["Petal10" + refLineId]));
                        objPRO011.EditedBy = objClsLoginInfo.UserName;
                        objPRO011.EditedOn = DateTime.Now;
                        db.SaveChanges();

                        objResponseMsg.Value = "Petal updated successfully";
                        #endregion
                    }
                    else
                    {
                        #region Add New Petal
                        objPRO011 = new PRO011();
                        objPRO011.HeaderId = objPRO010.HeaderId;
                        objPRO011.SrNo = !string.IsNullOrEmpty(fc["SrNo" + newRowIndex]) ? Convert.ToString(fc["SrNo" + newRowIndex]).Trim() : "";
                        objPRO011.Petal1 = GetDecimalValue(Convert.ToString(fc["Petal1" + newRowIndex]));
                        objPRO011.Petal2 = GetDecimalValue(Convert.ToString(fc["Petal2" + newRowIndex]));
                        objPRO011.Petal3 = GetDecimalValue(Convert.ToString(fc["Petal3" + newRowIndex]));
                        objPRO011.Petal4 = GetDecimalValue(Convert.ToString(fc["Petal4" + newRowIndex]));
                        objPRO011.Petal5 = GetDecimalValue(Convert.ToString(fc["Petal5" + newRowIndex]));
                        objPRO011.Petal6 = GetDecimalValue(Convert.ToString(fc["Petal6" + newRowIndex]));
                        objPRO011.Petal7 = GetDecimalValue(Convert.ToString(fc["Petal7" + newRowIndex]));
                        objPRO011.Petal8 = GetDecimalValue(Convert.ToString(fc["Petal8" + newRowIndex]));
                        objPRO011.Petal9 = GetDecimalValue(Convert.ToString(fc["Petal9" + newRowIndex]));
                        objPRO011.Petal10 = GetDecimalValue(Convert.ToString(fc["Petal10" + newRowIndex]));
                        objPRO011.CreatedBy = objClsLoginInfo.UserName;
                        objPRO011.CreatedOn = DateTime.Now;
                        db.PRO011.Add(objPRO011);
                        db.SaveChanges();

                        objResponseMsg.Value = "Petal added successfully";
                        #endregion
                    }


                    objResponseMsg.Key = true;

                    objResponseMsg.HeaderId = objPRO011.HeaderId;


                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO011 objPRO011 = db.PRO011.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO011 != null)
                {
                    db.PRO011.Remove(objPRO011);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Petal deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Protocol003 Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SeamNo like '%" + param.sSearch
                        + "%' or ReqOrientation like '%" + param.sSearch
                        + "%' or ReqArcLength like '%" + param.sSearch
                        + "%' or ActArcLength like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL003_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO010 objPRO010 = db.PRO010.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "20", "QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqOrientation", "", "", false, "", false, "10", "QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqArcLength", "", "", false, "", false, "10", "QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ActArcLength", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                "2",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(   uc.LineId, "SeamNo",uc.SeamNo, "", true, "", false, "20", "QC"),
                                Helper.GenerateNumericTextbox(uc.LineId, "ReqOrientation", Convert.ToString(uc.ReqOrientation), "", true, "", false, "10", "QC"),
                                Helper.GenerateNumericTextbox(uc.LineId, "ReqArcLength",   Convert.ToString(uc.ReqArcLength), "", true, "", false, "10", "QC"),
                                Helper.GenerateNumericTextbox(uc.LineId, "ActArcLength",   Convert.ToString(uc.ActArcLength), "", true, "", false, "10", "PROD"),
                                HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") +
                                (GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2','tblProtocolLines2')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO012 objPRO012 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRO010 objPRO010 = db.PRO010.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                    if (refLineId > 0)
                    {
                        #region Edit Seam
                        objPRO012 = db.PRO012.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        
                        objPRO012.SeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : string.Empty;
                        objPRO012.ReqOrientation = GetDecimalValue(Convert.ToString(fc["ReqOrientation" + refLineId]));
                        objPRO012.ReqArcLength = GetDecimalValue(Convert.ToString(fc["ReqArcLength" + refLineId]));
                        objPRO012.ActArcLength = GetDecimalValue(Convert.ToString(fc["ActArcLength" + refLineId]));
                        objPRO012.EditedBy = objClsLoginInfo.UserName;
                        objPRO012.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "SEAM updated successfully";
                        #endregion
                    }
                    else
                    {
                        #region Add New Seam
                        objPRO012 = new PRO012();
                        objPRO012.HeaderId = objPRO010.HeaderId;
                        objPRO012.SeamNo = !string.IsNullOrEmpty(fc["SeamNo" + newRowIndex]) ? Convert.ToString(fc["SeamNo" + newRowIndex]).Trim() : string.Empty;
                        objPRO012.ReqOrientation = GetDecimalValue(Convert.ToString(fc["ReqOrientation" + newRowIndex]));
                        objPRO012.ReqArcLength = GetDecimalValue(Convert.ToString(fc["ReqArcLength" + newRowIndex]));
                        objPRO012.ActArcLength = GetDecimalValue(Convert.ToString(fc["ActArcLength" + newRowIndex]));
                        objPRO012.CreatedBy = objClsLoginInfo.UserName;
                        objPRO012.CreatedOn = DateTime.Now;
                        db.PRO012.Add(objPRO012);
                        db.SaveChanges();
                        objResponseMsg.Value = "SEAM added successfully";
                        #endregion
                    }

                    objResponseMsg.Key = true;
                    
                    objResponseMsg.HeaderId = objPRO012.HeaderId;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO012 objPRO012 = db.PRO012.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO012 != null)
                {
                    db.PRO012.Remove(objPRO012);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Petal deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Protocol003 Line3 details
        [HttpPost]
        public ActionResult GetProtocolLines3Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (PetalNo like '%" + param.sSearch
                        + "%' or Description like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL003_GET_LINES3_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO010 objPRO010 = db.PRO010.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "3",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateNumericTextbox(newRecordId, "PetalNo", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateTextArea(newRecordId, "Description", "", "", false, "", "250", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine3(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                               "3",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateNumericTextbox(uc.LineId, "PetalNo", uc.PetalNo, "", true, "", false, "10", "PROD"),
                               Helper.GenerateTextArea(uc.LineId, "Description", uc.Description, "", true, "", "250", "PROD"),
                               HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") +
                               (GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine3','tblProtocolLines3')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine3(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO013 objPRO013 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRO010 objPRO010 = db.PRO010.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        #region Edit Petal
                        objPRO013 = db.PRO013.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        
                        objPRO013.PetalNo = !string.IsNullOrEmpty(fc["PetalNo" + refLineId]) ? Convert.ToString(fc["PetalNo" + refLineId]).Trim() : string.Empty;
                        objPRO013.Description = !string.IsNullOrEmpty(fc["Description" + refLineId]) ? Convert.ToString(fc["Description" + refLineId]).Trim() : string.Empty;
                        objPRO013.EditedBy = objClsLoginInfo.UserName;
                        objPRO013.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "PETAL updated successfully";
                        #endregion
                    }
                    else
                    {
                        #region Add New Petal
                        objPRO013 = new PRO013();
                        objPRO013.HeaderId = objPRO010.HeaderId;
                        objPRO013.PetalNo = !string.IsNullOrEmpty(fc["PetalNo" + newRowIndex]) ? Convert.ToString(fc["PetalNo" + newRowIndex]).Trim() : string.Empty;
                        objPRO013.Description = !string.IsNullOrEmpty(fc["Description" + newRowIndex]) ? Convert.ToString(fc["Description" + newRowIndex]).Trim() : string.Empty;
                        objPRO013.CreatedBy = objClsLoginInfo.UserName;
                        objPRO013.CreatedOn = DateTime.Now;
                        db.PRO013.Add(objPRO013);
                        db.SaveChanges();
                        objResponseMsg.Value = "PETAL added successfully";
                        #endregion
                    }
                    

                    objResponseMsg.Key = true;
                    
                    objResponseMsg.HeaderId = objPRO013.HeaderId;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine3(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO013 objPRO013 = db.PRO013.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO013 != null)
                {
                    db.PRO013.Remove(objPRO013);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Petal deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Protocol003 Line4 details
        [HttpPost]
        public ActionResult GetProtocolLines4Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SpotNo like '%" + param.sSearch
                        + "%' or Remarks like '%" + param.sSearch
                        + "%' or Width like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL003_GET_LINES4_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "4",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Width", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine4(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                "4",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo", uc.SpotNo, "", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "Width", Convert.ToString(uc.Width), "", true, "", false, "10", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", uc.Remarks, "", true, "", false, "100", "PROD"),
                                HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") +
                                (GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine4','tblProtocolLines4')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine4(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO014_1 objPRO014_1 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRO010 objPRO010 = db.PRO010.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objPRO014_1 = db.PRO014_1.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        
                        objPRO014_1.SpotNo = !string.IsNullOrEmpty(fc["SpotNo" + refLineId]) ? Convert.ToString(fc["SpotNo" + refLineId]).Trim() : "";
                        objPRO014_1.Width = GetDecimalValue(Convert.ToString(fc["Width" + refLineId]));
                        objPRO014_1.Remarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : string.Empty;
                        objPRO014_1.EditedBy = objClsLoginInfo.UserName;
                        objPRO014_1.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Clad striping width updated successfully";
                    }
                    else
                    {
                        objPRO014_1 = new PRO014_1();
                        objPRO014_1.HeaderId = objPRO010.HeaderId;
                        objPRO014_1.SpotNo = !string.IsNullOrEmpty(fc["SpotNo" + newRowIndex]) ? Convert.ToString(fc["SpotNo" + newRowIndex]).Trim() : "";
                        objPRO014_1.Width = GetDecimalValue(Convert.ToString(fc["Width" + newRowIndex]));
                        objPRO014_1.Remarks = !string.IsNullOrEmpty(fc["Remarks" + newRowIndex]) ? Convert.ToString(fc["Remarks" + newRowIndex]).Trim() : string.Empty;
                        objPRO014_1.CreatedBy = objClsLoginInfo.UserName;
                        objPRO014_1.CreatedOn = DateTime.Now;
                        db.PRO014_1.Add(objPRO014_1);
                        db.SaveChanges();
                        objResponseMsg.Value = "Clad striping width added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO014_1.HeaderId;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine4(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO014_1 objPRO014_1 = db.PRO014_1.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO014_1 != null)
                {
                    db.PRO014_1.Remove(objPRO014_1);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Protocol003 Line5 details
        [HttpPost]
        public ActionResult GetProtocolLines5Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SpotNo like '%" + param.sSearch
                        + "%' or Remarks like '%" + param.sSearch
                        + "%' or Depth like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL003_GET_LINES5_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "5",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo2", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Depth", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks2", "", "", false, "", false, "100", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine5(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                               "5",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo2", uc.SpotNo, "", true, "", false, "10", "PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "Depth", Convert.ToString(uc.Depth), "", true, "", false, "10", "PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks2", uc.Remarks, "", true, "", false, "100", "PROD"),
                               HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") +
                               (GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine5','tblProtocolLines5')") : "")
                               
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine5(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO014_2 objPRO014_2 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRO010 objPRO010 = db.PRO010.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                    if (refLineId > 0)
                    {
                        objPRO014_2 = db.PRO014_2.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        
                        objPRO014_2.SpotNo = !string.IsNullOrEmpty(fc["SpotNo2" + refLineId]) ? Convert.ToString(fc["SpotNo2" + refLineId]).Trim() : "";
                        objPRO014_2.Depth = GetDecimalValue(Convert.ToString(fc["Depth" + refLineId]));
                        objPRO014_2.Remarks = !string.IsNullOrEmpty(fc["Remarks2" + refLineId]) ? Convert.ToString(fc["Remarks2" + refLineId]).Trim() : string.Empty;
                        objPRO014_2.EditedBy = objClsLoginInfo.UserName;
                        objPRO014_2.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Clad striping depth updated successfully";
                    }
                    else
                    {
                        objPRO014_2 = new PRO014_2();
                        objPRO014_2.HeaderId = objPRO010.HeaderId;
                        objPRO014_2.SpotNo = !string.IsNullOrEmpty(fc["SpotNo2" + newRowIndex]) ? Convert.ToString(fc["SpotNo2" + newRowIndex]).Trim() : "";
                        objPRO014_2.Depth = GetDecimalValue(Convert.ToString(fc["Depth" + newRowIndex]));
                        objPRO014_2.Remarks = !string.IsNullOrEmpty(fc["Remarks2" + newRowIndex]) ? Convert.ToString(fc["Remarks2" + newRowIndex]).Trim() : string.Empty;
                        objPRO014_2.CreatedBy = objClsLoginInfo.UserName;
                        objPRO014_2.CreatedOn = DateTime.Now;
                        db.PRO014_2.Add(objPRO014_2);
                        db.SaveChanges();
                        objResponseMsg.Value = "Clad striping depth added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO014_2.HeaderId;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine5(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO014_2 objPRO014_2 = db.PRO014_2.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO014_2 != null)
                {
                    db.PRO014_2.Remove(objPRO014_2);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #endregion

        #region Protocol Linkage

        #region Protocol Linkage Header
        [SessionExpireFilter]// [UserPermissions, AllowAnonymous]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL010 objPRL010 = null;
            ViewBag.YesNoEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "No", CategoryDescription = "No" } };
            ViewBag.YesNAEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.YesNoNAEnum = new List<CategoryData> { new CategoryData { Value = "Yes", Code = "Yes", CategoryDescription = "Yes" }, new CategoryData { Value = "No", Code = "No", CategoryDescription = "No" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.RTUTExtentEnum = new List<CategoryData> { new CategoryData { Value = "100%", Code = "100%", CategoryDescription = "100%" }, new CategoryData { Value = "SPOT", Code = "SPOT", CategoryDescription = "SPOT" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };
            ViewBag.TOFDUTExtentEnum = new List<CategoryData> { new CategoryData { Value = "100%", Code = "100%", CategoryDescription = "100%" }, new CategoryData { Value = "NA", Code = "NA", CategoryDescription = "NA" } };

            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            if (id > 0)
            {
                objPRL010 = db.PRL010.Where(x => x.HeaderId == id).FirstOrDefault();
                if (objPRL010 == null)
                {
                    objPRL010 = new PRL010();
                    objPRL010.ProtocolNo = string.Empty;
                    objPRL010.CreatedBy = objClsLoginInfo.UserName;
                    objPRL010.CreatedOn = DateTime.Now;
                    db.PRL010.Add(objPRL010);
                    db.SaveChanges();
                }
                else
                {
                    var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL010.QualityProject).FirstOrDefault();
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
            }
            else
            {
                objPRL010 = new PRL010();
                objPRL010.ProtocolNo = string.Empty;
                objPRL010.CreatedBy = objClsLoginInfo.UserName;
                objPRL010.CreatedOn = DateTime.Now;
                db.PRL010.Add(objPRL010);
                db.SaveChanges();
            }
            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRL010);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL010 model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                if (model.HeaderId > 0)
                {
                    PRL010 objPRL010 = db.PRL010.Where(c => c.HeaderId == model.HeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL010.DCRNo = model.DCRNo;
                    objPRL010.ProtocolNo = model.ProtocolNo != null ? model.ProtocolNo : "";
                    objPRL010.Component = model.Component;
                    objPRL010.TypeOfDEndCone = model.TypeOfDEndCone;
                    objPRL010.Remarks_Both = model.Remarks_Both;
                    objPRL010.CircumferenceOutSideReqTop = model.CircumferenceOutSideReqTop;
                    objPRL010.CircumferenceOutSideReqBottom = model.CircumferenceOutSideReqBottom;
                    objPRL010.CircumferenceOutSideActTop = model.CircumferenceOutSideActTop;
                    objPRL010.CircumferenceOutSideActBottom = model.CircumferenceOutSideActBottom;
                    objPRL010.DiameterOvalityReqTop = model.DiameterOvalityReqTop;
                    objPRL010.DiameterOvalityReqBottom = model.DiameterOvalityReqBottom;
                    objPRL010.DiameterOvalityActTopMax = model.DiameterOvalityActTopMax;
                    objPRL010.DiameterOvalityActTopMin = model.DiameterOvalityActTopMin;
                    objPRL010.DiameterOvalityActBottomMax = model.DiameterOvalityActBottomMax;
                    objPRL010.DiameterOvalityActBottomMin = model.DiameterOvalityActBottomMin;
                    objPRL010.ThicknessAtTrimmingLineMin = model.ThicknessAtTrimmingLineMin;
                    objPRL010.ThicknessAtTrimmingLineTop = model.ThicknessAtTrimmingLineTop;
                    objPRL010.ThicknessAtTrimmingLineNom = model.ThicknessAtTrimmingLineNom;
                    objPRL010.ThicknessAtTrimmingLineBot = model.ThicknessAtTrimmingLineBot;
                    objPRL010.CircumferenceInsideTop = model.CircumferenceInsideTop;
                    objPRL010.CircumferenceInsideBottom = model.CircumferenceInsideBottom;
                    objPRL010.CircumferenceInsideMax = model.CircumferenceInsideMax;
                    objPRL010.CircumferenceInsideMin = model.CircumferenceInsideMin;
                    objPRL010.TotalHeightReq = model.TotalHeightReq;
                    objPRL010.TotalHeightAct = model.TotalHeightAct;
                    objPRL010.ThicknessReqMin = model.ThicknessReqMin;
                    objPRL010.ThicknessReqNom = model.ThicknessReqNom;
                    objPRL010.ThicknessActMin = model.ThicknessActMin;
                    objPRL010.ThicknessActNom = model.ThicknessActNom;
                    objPRL010.StraightFaceReq = model.StraightFaceReq;
                    objPRL010.StraightFaceAct = model.StraightFaceAct;
                    objPRL010.CrownRadiusReq = model.CrownRadiusReq;
                    objPRL010.CrownRadiusAct = model.CrownRadiusAct;
                    objPRL010.KnuckleRadiusReq = model.KnuckleRadiusReq;
                    objPRL010.KnuckleRadiusAct = model.KnuckleRadiusAct;
                    objPRL010.OverCrowningReq = model.OverCrowningReq;
                    objPRL010.OverCrowningAct = model.OverCrowningAct;
                    objPRL010.UnderCrowningReq = model.UnderCrowningReq;
                    objPRL010.UnderCrowningAct = model.UnderCrowningAct;
                    objPRL010.TemplateCheckedOnLayout = model.TemplateCheckedOnLayout;
                    objPRL010.ProfCheckPatelW3DTmplt = model.ProfCheckPatelW3DTmplt;
                    objPRL010.NonDestructiveTestingRequired1 = model.NonDestructiveTestingRequired1;
                    objPRL010.NonDestructiveTestingRequired2 = model.NonDestructiveTestingRequired2;
                    objPRL010.NonDestructiveTestingRequired3 = model.NonDestructiveTestingRequired3;
                    objPRL010.NonDestructiveTestingISSurface1 = model.NonDestructiveTestingISSurface1;
                    objPRL010.NonDestructiveTestingISSurface2 = model.NonDestructiveTestingISSurface2;
                    objPRL010.NonDestructiveTestingISSurface3 = model.NonDestructiveTestingISSurface3;
                    objPRL010.NonDestructiveTestingOSSurface1 = model.NonDestructiveTestingOSSurface1;
                    objPRL010.NonDestructiveTestingOSSurface2 = model.NonDestructiveTestingOSSurface2;
                    objPRL010.NonDestructiveTestingOSSurface3 = model.NonDestructiveTestingOSSurface3;
                    objPRL010.NonDestructiveTestingKnuckle1 = model.NonDestructiveTestingKnuckle1;
                    objPRL010.NonDestructiveTestingKnuckle2 = model.NonDestructiveTestingKnuckle2;
                    objPRL010.NonDestructiveTestingKnuckle3 = model.NonDestructiveTestingKnuckle3;
                    objPRL010.NonDestructiveTestingWEP1 = model.NonDestructiveTestingWEP1;
                    objPRL010.NonDestructiveTestingWEP2 = model.NonDestructiveTestingWEP2;
                    objPRL010.NonDestructiveTestingWEP3 = model.NonDestructiveTestingWEP3;
                    objPRL010.NonDestructiveTestingRemarks1 = model.NonDestructiveTestingRemarks1;
                    objPRL010.NonDestructiveTestingRemarks2 = model.NonDestructiveTestingRemarks2;
                    objPRL010.NonDestructiveTestingRemarks3 = model.NonDestructiveTestingRemarks3;
                    objPRL010.TypeOfHT = model.TypeOfHT;
                    objPRL010.Cycle = model.Cycle;
                    objPRL010.HTGraphReviewed = model.HTGraphReviewed;
                    objPRL010.HeatTreatmentDetailsRemarks = model.HeatTreatmentDetailsRemarks;
                    objPRL010.TapeNo = model.TapeNo;
                    objPRL010.CalibrationDueDate = model.CalibrationDueDate;
                    objPRL010.SeamNoHardStampTransfer = model.SeamNoHardStampTransfer;
                    objPRL010.OutOfRoundnessOvalityReadings = model.OutOfRoundnessOvalityReadings;
                    objPRL010.RequiredID = model.RequiredID;
                    objPRL010.ActualId1 = model.ActualId1;
                    objPRL010.ActualId2 = model.ActualId2;
                    objPRL010.ActualId3 = model.ActualId3;
                    objPRL010.ActualId4 = model.ActualId4;
                    objPRL010.ActualId5 = model.ActualId5;
                    objPRL010.ActualId6 = model.ActualId6;
                    objPRL010.ActualId7 = model.ActualId7;
                    objPRL010.ActualId8 = model.ActualId8;
                    objPRL010.ActualId9 = model.ActualId9;
                    objPRL010.ActualId10 = model.ActualId10;
                    objPRL010.ActualId11 = model.ActualId11;
                    objPRL010.ActualId12 = model.ActualId12;
                    objPRL010.ActualId13 = model.ActualId13;
                    objPRL010.ActualId14 = model.ActualId14;
                    objPRL010.ActualId15 = model.ActualId15;
                    objPRL010.ActualId16 = model.ActualId16;
                    objPRL010.RemarksId = model.RemarksId;
                    objPRL010.OutOfRoundnessMax = model.OutOfRoundnessMax;
                    objPRL010.OutOfRoundnessMin = model.OutOfRoundnessMin;
                    objPRL010.Reading1 = model.Reading1;
                    objPRL010.Reading2 = model.Reading2;
                    objPRL010.Reading3 = model.Reading3;
                    objPRL010.Reading4 = model.Reading4;
                    objPRL010.Reading5 = model.Reading5;
                    objPRL010.Reading6 = model.Reading6;
                    objPRL010.Reading7 = model.Reading7;
                    objPRL010.Reading8 = model.Reading8;
                    objPRL010.Reading9 = model.Reading9;
                    objPRL010.Reading10 = model.Reading10;
                    objPRL010.Reading11 = model.Reading11;
                    objPRL010.Reading12 = model.Reading12;
                    objPRL010.Reading13 = model.Reading13;
                    objPRL010.Reading14 = model.Reading14;
                    objPRL010.Reading15 = model.Reading15;
                    objPRL010.Reading16 = model.Reading16;
                    objPRL010.Reading17 = model.Reading17;
                    objPRL010.Reading18 = model.Reading18;
                    objPRL010.Reading19 = model.Reading19;
                    objPRL010.Reading20 = model.Reading20;
                    objPRL010.Reading21 = model.Reading21;
                    objPRL010.Reading22 = model.Reading22;
                    objPRL010.Reading23 = model.Reading23;
                    objPRL010.Reading24 = model.Reading24;
                    objPRL010.Reading25 = model.Reading25;
                    objPRL010.Reading26 = model.Reading26;
                    objPRL010.Reading27 = model.Reading27;
                    objPRL010.Reading28 = model.Reading28;
                    objPRL010.Reading29 = model.Reading29;
                    objPRL010.Reading30 = model.Reading30;
                    objPRL010.Reading31 = model.Reading31;
                    objPRL010.Reading32 = model.Reading32;
                    objPRL010.ReadingAverage = model.ReadingAverage;
                    objPRL010.Concntricity = model.Concntricity;
                    objPRL010.ThickCheckedWithThickMeter = model.ThickCheckedWithThickMeter;
                    objPRL010.SeamNo1 = model.SeamNo1;
                    objPRL010.SeamNo2 = model.SeamNo2;
                    objPRL010.SeamNo3 = model.SeamNo3;
                    objPRL010.SeamNo4 = model.SeamNo4;
                    objPRL010.SeamNo5 = model.SeamNo5;
                    objPRL010.SeamNo6 = model.SeamNo6;
                    objPRL010.SeamNo7 = model.SeamNo7;
                    objPRL010.SeamNo8 = model.SeamNo8;
                    objPRL010.RootGapRequired = model.RootGapRequired;
                    objPRL010.PickInOutRequired = model.PickInOutRequired;
                    objPRL010.RootGap1_1 = model.RootGap1_1;
                    objPRL010.RootGap1_2 = model.RootGap1_2;
                    objPRL010.RootGap1_3 = model.RootGap1_3;
                    objPRL010.RootGap1_4 = model.RootGap1_4;
                    objPRL010.RootGap1_5 = model.RootGap1_5;
                    objPRL010.RootGap1_6 = model.RootGap1_6;
                    objPRL010.RootGap1_7 = model.RootGap1_7;
                    objPRL010.RootGap1_8 = model.RootGap1_8;
                    objPRL010.RootGap2_1 = model.RootGap2_1;
                    objPRL010.RootGap2_2 = model.RootGap2_2;
                    objPRL010.RootGap2_3 = model.RootGap2_3;
                    objPRL010.RootGap2_4 = model.RootGap2_4;
                    objPRL010.RootGap2_5 = model.RootGap2_5;
                    objPRL010.RootGap2_6 = model.RootGap2_6;
                    objPRL010.RootGap2_7 = model.RootGap2_7;
                    objPRL010.RootGap2_8 = model.RootGap2_8;
                    objPRL010.RootGap3_1 = model.RootGap3_1;
                    objPRL010.RootGap3_2 = model.RootGap3_2;
                    objPRL010.RootGap3_3 = model.RootGap3_3;
                    objPRL010.RootGap3_4 = model.RootGap3_4;
                    objPRL010.RootGap3_5 = model.RootGap3_5;
                    objPRL010.RootGap3_6 = model.RootGap3_6;
                    objPRL010.RootGap3_7 = model.RootGap3_7;
                    objPRL010.RootGap3_8 = model.RootGap3_8;
                    objPRL010.RootGap4_1 = model.RootGap4_1;
                    objPRL010.RootGap4_2 = model.RootGap4_2;
                    objPRL010.RootGap4_3 = model.RootGap4_3;
                    objPRL010.RootGap4_4 = model.RootGap4_4;
                    objPRL010.RootGap4_5 = model.RootGap4_5;
                    objPRL010.RootGap4_6 = model.RootGap4_6;
                    objPRL010.RootGap4_7 = model.RootGap4_7;
                    objPRL010.RootGap4_8 = model.RootGap4_8;
                    objPRL010.RootGap5_1 = model.RootGap5_1;
                    objPRL010.RootGap5_2 = model.RootGap5_2;
                    objPRL010.RootGap5_3 = model.RootGap5_3;
                    objPRL010.RootGap5_4 = model.RootGap5_4;
                    objPRL010.RootGap5_5 = model.RootGap5_5;
                    objPRL010.RootGap5_6 = model.RootGap5_6;
                    objPRL010.RootGap5_7 = model.RootGap5_7;
                    objPRL010.RootGap5_8 = model.RootGap5_8;
                    objPRL010.PickInOut1_1 = model.PickInOut1_1;
                    objPRL010.PickInOut1_2 = model.PickInOut1_2;
                    objPRL010.PickInOut1_3 = model.PickInOut1_3;
                    objPRL010.PickInOut1_4 = model.PickInOut1_4;
                    objPRL010.PickInOut1_5 = model.PickInOut1_5;
                    objPRL010.PickInOut1_6 = model.PickInOut1_6;
                    objPRL010.PickInOut1_7 = model.PickInOut1_7;
                    objPRL010.PickInOut1_8 = model.PickInOut1_8;
                    objPRL010.PickInOut3_1 = model.PickInOut3_1;
                    objPRL010.PickInOut3_2 = model.PickInOut3_2;
                    objPRL010.PickInOut3_3 = model.PickInOut3_3;
                    objPRL010.PickInOut3_4 = model.PickInOut3_4;
                    objPRL010.PickInOut3_5 = model.PickInOut3_5;
                    objPRL010.PickInOut3_6 = model.PickInOut3_6;
                    objPRL010.PickInOut3_7 = model.PickInOut3_7;
                    objPRL010.PickInOut3_8 = model.PickInOut3_8;
                    objPRL010.PickInOut5_1 = model.PickInOut5_1;
                    objPRL010.PickInOut5_2 = model.PickInOut5_2;
                    objPRL010.PickInOut5_3 = model.PickInOut5_3;
                    objPRL010.PickInOut5_4 = model.PickInOut5_4;
                    objPRL010.PickInOut5_5 = model.PickInOut5_5;
                    objPRL010.PickInOut5_6 = model.PickInOut5_6;
                    objPRL010.PickInOut5_7 = model.PickInOut5_7;
                    objPRL010.PickInOut5_8 = model.PickInOut5_8;
                    objPRL010.RootGapRequired2 = model.RootGapRequired2;
                    objPRL010.PickInOutCheckedWithD4Tmplt = model.PickInOutCheckedWithD4Tmplt;
                    objPRL010.SeamNo9 = model.SeamNo9;
                    objPRL010.SeamNo10 = model.SeamNo10;
                    objPRL010.SeamNo11 = model.SeamNo11;
                    objPRL010.SeamNo12 = model.SeamNo12;
                    objPRL010.SeamNo13 = model.SeamNo13;
                    objPRL010.SeamNo14 = model.SeamNo14;
                    objPRL010.SeamNo15 = model.SeamNo15;
                    objPRL010.SeamNo16 = model.SeamNo16;
                    objPRL010.RootGap1_9 = model.RootGap1_9;
                    objPRL010.RootGap1_10 = model.RootGap1_10;
                    objPRL010.RootGap1_11 = model.RootGap1_11;
                    objPRL010.RootGap1_12 = model.RootGap1_12;
                    objPRL010.RootGap1_13 = model.RootGap1_13;
                    objPRL010.RootGap1_14 = model.RootGap1_14;
                    objPRL010.RootGap1_15 = model.RootGap1_15;
                    objPRL010.RootGap2_9 = model.RootGap2_9;
                    objPRL010.RootGap2_10 = model.RootGap2_10;
                    objPRL010.RootGap2_11 = model.RootGap2_11;
                    objPRL010.RootGap2_12 = model.RootGap2_12;
                    objPRL010.RootGap2_13 = model.RootGap2_13;
                    objPRL010.RootGap2_14 = model.RootGap2_14;
                    objPRL010.RootGap2_15 = model.RootGap2_15;
                    objPRL010.RootGap3_9 = model.RootGap3_9;
                    objPRL010.RootGap3_10 = model.RootGap3_10;
                    objPRL010.RootGap3_11 = model.RootGap3_11;
                    objPRL010.RootGap3_12 = model.RootGap3_12;
                    objPRL010.RootGap3_13 = model.RootGap3_13;
                    objPRL010.RootGap3_14 = model.RootGap3_14;
                    objPRL010.RootGap3_15 = model.RootGap3_15;
                    objPRL010.RootGap4_9 = model.RootGap4_9;
                    objPRL010.RootGap4_10 = model.RootGap4_10;
                    objPRL010.RootGap4_11 = model.RootGap4_11;
                    objPRL010.RootGap4_12 = model.RootGap4_12;
                    objPRL010.RootGap4_13 = model.RootGap4_13;
                    objPRL010.RootGap4_14 = model.RootGap4_14;
                    objPRL010.RootGap4_15 = model.RootGap4_15;
                    objPRL010.RootGap5_9 = model.RootGap5_9;
                    objPRL010.RootGap5_10 = model.RootGap5_10;
                    objPRL010.RootGap5_11 = model.RootGap5_11;
                    objPRL010.RootGap5_12 = model.RootGap5_12;
                    objPRL010.RootGap5_13 = model.RootGap5_13;
                    objPRL010.RootGap5_14 = model.RootGap5_14;
                    objPRL010.RootGap5_15 = model.RootGap5_15;
                    objPRL010.AngleReq = model.AngleReq;
                    objPRL010.AngleAct = model.AngleAct;
                    objPRL010.ISReq = model.ISReq;
                    objPRL010.ISAct = model.ISAct;
                    objPRL010.OWReq = model.OWReq;
                    objPRL010.OWAct = model.OWAct;
                    objPRL010.RootFaceReq = model.RootFaceReq;
                    objPRL010.RootFaceAct = model.RootFaceAct;
                    objPRL010.PetalJointRootGap1 = model.PetalJointRootGap1;
                    objPRL010.PetalJointRootGap2 = model.PetalJointRootGap2;
                    objPRL010.PetalJointRootGap3 = model.PetalJointRootGap3;
                    objPRL010.PetalJointRootGap4 = model.PetalJointRootGap4;
                    objPRL010.PetalJointRootGap5 = model.PetalJointRootGap5;
                    objPRL010.PetalJointRootGap6 = model.PetalJointRootGap6;
                    objPRL010.PetalJointOffset1 = model.PetalJointOffset1;
                    objPRL010.PetalJointOffset2 = model.PetalJointOffset2;
                    objPRL010.PetalJointOffset3 = model.PetalJointOffset3;
                    objPRL010.PetalJointOffset4 = model.PetalJointOffset4;
                    objPRL010.PetalJointOffset5 = model.PetalJointOffset5;
                    objPRL010.PetalJointOffset6 = model.PetalJointOffset6;
                    objPRL010.PetalJointPickInOut1 = model.PetalJointPickInOut1;
                    objPRL010.PetalJointPickInOut2 = model.PetalJointPickInOut2;
                    objPRL010.PetalJointPickInOut3 = model.PetalJointPickInOut3;
                    objPRL010.PetalJointPickInOut4 = model.PetalJointPickInOut4;
                    objPRL010.PetalJointPickInOut5 = model.PetalJointPickInOut5;
                    objPRL010.PetalJointPickInOut6 = model.PetalJointPickInOut6;
                    objPRL010.PetalJointRequired1 = model.PetalJointRequired1;
                    objPRL010.PetalJointRequired2 = model.PetalJointRequired2;
                    objPRL010.PetalJointRequired3 = model.PetalJointRequired3;
                    objPRL010.Checkpoint1 = model.Checkpoint1;
                    objPRL010.Checkpoint2 = model.Checkpoint2;
                    objPRL010.Checkpoint3 = model.Checkpoint3;
                    objPRL010.Checkpoint4 = model.Checkpoint4;
                    objPRL010.Checkpoint5 = model.Checkpoint5;
                    objPRL010.Checkpoint6 = model.Checkpoint6;
                    objPRL010.Checkpoint7 = model.Checkpoint7;
                    objPRL010.Checkpoint8 = model.Checkpoint8;
                    objPRL010.Checkpoint9 = model.Checkpoint9;
                    objPRL010.Checkpoint10 = model.Checkpoint10;
                    objPRL010.Checkpoint11 = model.Checkpoint11;
                    objPRL010.Checkpoint12 = model.Checkpoint12;
                    objPRL010.Checkpoint13 = model.Checkpoint13;
                    objPRL010.WPSNO = model.WPSNO;
                    objPRL010.MaterialIdentificationRPT = model.MaterialIdentificationRPT;
                    objPRL010.CladStripingWidthMax = model.CladStripingWidthMax;
                    objPRL010.CladStripingWidthMin = model.CladStripingWidthMin;
                    objPRL010.CladStripingDepthMax = model.CladStripingDepthMax;
                    objPRL010.CladStripingDepthMin = model.CladStripingDepthMin;
                    objPRL010.FNote1 = model.FNote1;
                    objPRL010.FNote2 = model.FNote2;
                    objPRL010.FNote3 = model.FNote3;
                    objPRL010.EditedBy = objClsLoginInfo.UserName;
                    objPRL010.EditedOn = DateTime.Now;
                    objPRL010.CladdedVessel = model.CladdedVessel;
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL010.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Protocol003 Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SrNo like '%" + param.sSearch
                        + "%' or Petal1 like '%" + param.sSearch
                        + "%' or Petal2 like '%" + param.sSearch
                        + "%' or Petal3 like '%" + param.sSearch
                        + "%' or Petal4 like '%" + param.sSearch
                        + "%' or Petal5 like '%" + param.sSearch
                        + "%' or Petal6 like '%" + param.sSearch
                        + "%' or Petal7 like '%" + param.sSearch
                        + "%' or Petal8 like '%" + param.sSearch
                        + "%' or Petal9 like '%" + param.sSearch
                        + "%' or Petal10 like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL003_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO010 objPRO010 = db.PRO010.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SrNo", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal1", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal2", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal3", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal4", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal5", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal6", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal7", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal8", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal9", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Petal10", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId ,"SrNo",uc.SrNo,"", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId ,"Petal1",Convert.ToString(uc.Petal1 ), "", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId ,"Petal2",Convert.ToString(uc.Petal2 ), "", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId ,"Petal3",Convert.ToString(uc.Petal3 ), "", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId ,"Petal4",Convert.ToString(uc.Petal4 ), "", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId ,"Petal5",Convert.ToString(uc.Petal5 ), "", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId ,"Petal6",Convert.ToString(uc.Petal6 ), "", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId ,"Petal7",Convert.ToString(uc.Petal7 ), "", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId ,"Petal8",Convert.ToString(uc.Petal8 ), "", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId ,"Petal9",Convert.ToString(uc.Petal9 ), "", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId , "Petal10",Convert.ToString(uc.Petal10) , "", true, "", false, "10", "PROD"),
                                HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                +(GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL011 objPRL011 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRL010 objPRL010 = db.PRL010.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        #region Edit Petals
                        objPRL011 = db.PRL011.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        
                        objPRL011.SrNo = !string.IsNullOrEmpty(fc["SrNo" + refLineId]) ? Convert.ToString(fc["SrNo" + refLineId]).Trim() : "";
                        objPRL011.Petal1 = GetDecimalValue(Convert.ToString(fc["Petal1" + refLineId]));
                        objPRL011.Petal2 = GetDecimalValue(Convert.ToString(fc["Petal2" + refLineId]));
                        objPRL011.Petal3 = GetDecimalValue(Convert.ToString(fc["Petal3" + refLineId]));
                        objPRL011.Petal4 = GetDecimalValue(Convert.ToString(fc["Petal4" + refLineId]));
                        objPRL011.Petal5 = GetDecimalValue(Convert.ToString(fc["Petal5" + refLineId]));
                        objPRL011.Petal6 = GetDecimalValue(Convert.ToString(fc["Petal6" + refLineId]));
                        objPRL011.Petal7 = GetDecimalValue(Convert.ToString(fc["Petal7" + refLineId]));
                        objPRL011.Petal8 = GetDecimalValue(Convert.ToString(fc["Petal8" + refLineId]));
                        objPRL011.Petal9 = GetDecimalValue(Convert.ToString(fc["Petal9" + refLineId]));
                        objPRL011.Petal10 = GetDecimalValue(Convert.ToString(fc["Petal10" + refLineId]));
                        objPRL011.EditedBy = objClsLoginInfo.UserName;
                        objPRL011.EditedOn = DateTime.Now;
                        db.SaveChanges();

                        objResponseMsg.Value = "Petal updated successfully";
                        #endregion
                    }
                    else
                    {
                        #region Add New Petal
                        objPRL011 = new PRL011();
                        objPRL011.HeaderId = objPRL010.HeaderId;
                        objPRL011.SrNo = !string.IsNullOrEmpty(fc["SrNo" + newRowIndex]) ? Convert.ToString(fc["SrNo" + newRowIndex]).Trim() : "";
                        objPRL011.Petal1 = GetDecimalValue(Convert.ToString(fc["Petal1" + newRowIndex]));
                        objPRL011.Petal2 = GetDecimalValue(Convert.ToString(fc["Petal2" + newRowIndex]));
                        objPRL011.Petal3 = GetDecimalValue(Convert.ToString(fc["Petal3" + newRowIndex]));
                        objPRL011.Petal4 = GetDecimalValue(Convert.ToString(fc["Petal4" + newRowIndex]));
                        objPRL011.Petal5 = GetDecimalValue(Convert.ToString(fc["Petal5" + newRowIndex]));
                        objPRL011.Petal6 = GetDecimalValue(Convert.ToString(fc["Petal6" + newRowIndex]));
                        objPRL011.Petal7 = GetDecimalValue(Convert.ToString(fc["Petal7" + newRowIndex]));
                        objPRL011.Petal8 = GetDecimalValue(Convert.ToString(fc["Petal8" + newRowIndex]));
                        objPRL011.Petal9 = GetDecimalValue(Convert.ToString(fc["Petal9" + newRowIndex]));
                        objPRL011.Petal10 = GetDecimalValue(Convert.ToString(fc["Petal10" + newRowIndex]));
                        objPRL011.CreatedBy = objClsLoginInfo.UserName;
                        objPRL011.CreatedOn = DateTime.Now;
                        db.PRL011.Add(objPRL011);
                        db.SaveChanges();

                        objResponseMsg.Value = "Petal added successfully";
                        #endregion
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL011.HeaderId;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL011 objPRL011 = db.PRL011.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL011 != null)
                {
                    db.PRL011.Remove(objPRL011);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Petal deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Protocol003 Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SeamNo like '%" + param.sSearch
                        + "%' or ReqOrientation like '%" + param.sSearch
                        + "%' or ReqArcLength like '%" + param.sSearch
                        + "%' or ActArcLength like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL003_LINKAGE_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO010 objPRO010 = db.PRO010.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SeamNo", "", "", false, "", false, "20", "QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqOrientation", "", "", false, "", false, "10", "QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqArcLength", "", "", false, "", false, "10", "QC"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ActArcLength", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                "2",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(   uc.LineId, "SeamNo",uc.SeamNo, "", true, "", false, "20", "QC"),
                                Helper.GenerateNumericTextbox(uc.LineId, "ReqOrientation", Convert.ToString(uc.ReqOrientation), "", true, "", false, "10", "QC"),
                                Helper.GenerateNumericTextbox(uc.LineId, "ReqArcLength",   Convert.ToString(uc.ReqArcLength), "", true, "", false, "10", "QC"),
                                Helper.GenerateNumericTextbox(uc.LineId, "ActArcLength",   Convert.ToString(uc.ActArcLength), "", true, "", false, "10", "PROD"),
                                HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                +(GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2Linkage','tblProtocolLines2')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine2Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL012 objPRL012 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRL010 objPRL010 = db.PRL010.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objPRL012 = db.PRL012.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        
                        objPRL012.SeamNo = !string.IsNullOrEmpty(fc["SeamNo" + refLineId]) ? Convert.ToString(fc["SeamNo" + refLineId]).Trim() : string.Empty;
                        objPRL012.ReqOrientation = GetDecimalValue(Convert.ToString(fc["ReqOrientation" + refLineId]));
                        objPRL012.ReqArcLength = GetDecimalValue(Convert.ToString(fc["ReqArcLength" + refLineId]));
                        objPRL012.ActArcLength = GetDecimalValue(Convert.ToString(fc["ActArcLength" + refLineId]));
                        objPRL012.EditedBy = objClsLoginInfo.UserName;
                        objPRL012.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "SEAM updated successfully";
                    }
                    else
                    {
                        objPRL012 = new PRL012();
                        objPRL012.HeaderId = objPRL010.HeaderId;
                        objPRL012.SeamNo = !string.IsNullOrEmpty(fc["SeamNo" + newRowIndex]) ? Convert.ToString(fc["SeamNo" + newRowIndex]).Trim() : string.Empty;
                        objPRL012.ReqOrientation = GetDecimalValue(Convert.ToString(fc["ReqOrientation" + newRowIndex]));
                        objPRL012.ReqArcLength = GetDecimalValue(Convert.ToString(fc["ReqArcLength" + newRowIndex]));
                        objPRL012.ActArcLength = GetDecimalValue(Convert.ToString(fc["ActArcLength" + newRowIndex]));
                        objPRL012.CreatedBy = objClsLoginInfo.UserName;
                        objPRL012.CreatedOn = DateTime.Now;
                        db.PRL012.Add(objPRL012);
                        db.SaveChanges();
                        objResponseMsg.Value = "SEAM added successfully";
                    }

                    objResponseMsg.Key = true;
                    
                    objResponseMsg.HeaderId = objPRL012.HeaderId;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL012 objPRL012 = db.PRL012.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL012 != null)
                {
                    db.PRL012.Remove(objPRL012);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Petal deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Protocol003 Line3 details
        [HttpPost]
        public ActionResult GetProtocolLines3DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (PetalNo like '%" + param.sSearch
                        + "%' or Description like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL003_LINKAGE_GET_LINES3_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "3",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateNumericTextbox(newRecordId, "PetalNo", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateTextArea(newRecordId, "Description", "", "", false, "", "250", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine3(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                               "3",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateNumericTextbox(uc.LineId, "PetalNo", uc.PetalNo, "", true, "", false, "10", "PROD"),
                               Helper.GenerateTextArea(uc.LineId, "Description", uc.Description, "", true, "", "250", "PROD"),
                               HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                               +(GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine3Linkage','tblProtocolLines3')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine3Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL013 objPRL013 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRL010 objPRL010 = db.PRL010.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        #region Edit Petal
                        objPRL013 = db.PRL013.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();

                        objPRL013.PetalNo = !string.IsNullOrEmpty(fc["PetalNo" + refLineId]) ? Convert.ToString(fc["PetalNo" + refLineId]).Trim() : string.Empty;
                        objPRL013.Description = !string.IsNullOrEmpty(fc["Description" + refLineId]) ? Convert.ToString(fc["Description" + refLineId]).Trim() : string.Empty;
                        objPRL013.EditedBy = objClsLoginInfo.UserName;
                        objPRL013.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "PETAL updated successfully";
                        #endregion
                    }
                    else
                    {
                        #region Add New Petal
                        objPRL013 = new PRL013();
                        objPRL013.HeaderId = objPRL010.HeaderId;
                        objPRL013.PetalNo = !string.IsNullOrEmpty(fc["PetalNo" + newRowIndex]) ? Convert.ToString(fc["PetalNo" + newRowIndex]).Trim() : string.Empty;
                        objPRL013.Description = !string.IsNullOrEmpty(fc["Description" + newRowIndex]) ? Convert.ToString(fc["Description" + newRowIndex]).Trim() : string.Empty;
                        objPRL013.CreatedBy = objClsLoginInfo.UserName;
                        objPRL013.CreatedOn = DateTime.Now;
                        db.PRL013.Add(objPRL013);
                        db.SaveChanges();
                        objResponseMsg.Value = "PETAL added successfully";
                        #endregion
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL013.HeaderId;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine3Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL013 objPRL013 = db.PRL013.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL013 != null)
                {
                    db.PRL013.Remove(objPRL013);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Petal deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Protocol003 Line4 details
        [HttpPost]
        public ActionResult GetProtocolLines4DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SpotNo like '%" + param.sSearch
                        + "%' or Remarks like '%" + param.sSearch
                        + "%' or Width like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL003_LINKAGE_GET_LINES4_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                PRO001 objPRO001 = db.PRO001.Where(c => c.HeaderId == intHeaderId).FirstOrDefault();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "4",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Width", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine4(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                "4",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo", uc.SpotNo, "", true, "", false, "10", "PROD"),
                                Helper.GenerateNumericTextbox(uc.LineId, "Width", Convert.ToString(uc.Width), "", true, "", false, "10", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", uc.Remarks, "", true, "", false, "100", "PROD"),
                                HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                                +(GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine4Linkage','tblProtocolLines4')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine4Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL014_1 objPRL014_1 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRL010 objPRL010 = db.PRL010.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    string intSpotNo = !string.IsNullOrEmpty(fc["SpotNo" + newRowIndex]) ? Convert.ToString(fc["SpotNo" + newRowIndex]).Trim() : "";
                    decimal? strWidth = GetDecimalValue(Convert.ToString(fc["Width" + newRowIndex]));//  !string.IsNullOrEmpty(fc["SpotWidth" + newRowIndex]) ? Convert.ToDecimal(fc["SpotWidth" + newRowIndex]) : 0;
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + newRowIndex]) ? Convert.ToString(fc["Remarks" + newRowIndex]).Trim() : string.Empty;


                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objPRL014_1 = db.PRL014_1.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();

                        objPRL014_1.SpotNo = !string.IsNullOrEmpty(fc["SpotNo" + refLineId]) ? Convert.ToString(fc["SpotNo" + refLineId]).Trim() : "";
                        objPRL014_1.Width = GetDecimalValue(Convert.ToString(fc["Width" + refLineId]));
                        objPRL014_1.Remarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : string.Empty;
                        objPRL014_1.EditedBy = objClsLoginInfo.UserName;
                        objPRL014_1.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Clad striping width updated successfully";
                    }
                    else
                    {
                        objPRL014_1 = new PRL014_1();
                        objPRL014_1.HeaderId = objPRL010.HeaderId;
                        objPRL014_1.SpotNo = !string.IsNullOrEmpty(fc["SpotNo" + newRowIndex]) ? Convert.ToString(fc["SpotNo" + newRowIndex]).Trim() : "";
                        objPRL014_1.Width = GetDecimalValue(Convert.ToString(fc["Width" + newRowIndex]));
                        objPRL014_1.Remarks = !string.IsNullOrEmpty(fc["Remarks" + newRowIndex]) ? Convert.ToString(fc["Remarks" + newRowIndex]).Trim() : string.Empty;
                        objPRL014_1.CreatedBy = objClsLoginInfo.UserName;
                        objPRL014_1.CreatedOn = DateTime.Now;
                        db.PRL014_1.Add(objPRL014_1);
                        db.SaveChanges();
                        objResponseMsg.Value = "Clad striping width added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL014_1.HeaderId;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine4Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL014_1 objPRL014_1 = db.PRL014_1.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL014_1 != null)
                {
                    db.PRL014_1.Remove(objPRL014_1);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Protocol003 Line5 details
        [HttpPost]
        public ActionResult GetProtocolLines5DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (SpotNo like '%" + param.sSearch
                        + "%' or Remarks like '%" + param.sSearch
                        + "%' or Depth like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL003_LINKAGE_GET_LINES5_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "5",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "SpotNo2", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Depth", "", "", false, "", false, "10", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks2", "", "", false, "", false, "100", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine5(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                               "5",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "SpotNo2", uc.SpotNo, "", true, "", false, "10", "PROD"),
                               Helper.GenerateNumericTextbox(uc.LineId, "Depth", Convert.ToString(uc.Depth), "", true, "", false, "10", "PROD"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks2", uc.Remarks, "", true, "", false, "100", "PROD"),
                               HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","")
                               +(GetUserAccessRights().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.PROD3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine5Linkage','tblProtocolLines5')") : "")

                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine5Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL014_2 objPRL014_2 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    PRL010 objPRL010 = db.PRL010.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                    if (refLineId > 0)
                    {
                        objPRL014_2 = db.PRL014_2.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();

                        objPRL014_2.SpotNo = !string.IsNullOrEmpty(fc["SpotNo2" + refLineId]) ? Convert.ToString(fc["SpotNo2" + refLineId]).Trim() : "";
                        objPRL014_2.Depth = GetDecimalValue(Convert.ToString(fc["Depth" + refLineId]));
                        objPRL014_2.Remarks = !string.IsNullOrEmpty(fc["Remarks2" + refLineId]) ? Convert.ToString(fc["Remarks2" + refLineId]).Trim() : string.Empty;
                        objPRL014_2.EditedBy = objClsLoginInfo.UserName;
                        objPRL014_2.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Clad striping depth updated successfully";
                    }
                    else
                    {
                        objPRL014_2 = new PRL014_2();
                        objPRL014_2.HeaderId = objPRL010.HeaderId;
                        objPRL014_2.SpotNo = !string.IsNullOrEmpty(fc["SpotNo2" + newRowIndex]) ? Convert.ToString(fc["SpotNo2" + newRowIndex]).Trim() : "";
                        objPRL014_2.Depth = GetDecimalValue(Convert.ToString(fc["Depth" + newRowIndex]));
                        objPRL014_2.Remarks = !string.IsNullOrEmpty(fc["Remarks2" + newRowIndex]) ? Convert.ToString(fc["Remarks2" + newRowIndex]).Trim() : string.Empty;
                        objPRL014_2.CreatedBy = objClsLoginInfo.UserName;
                        objPRL014_2.CreatedOn = DateTime.Now;
                        db.PRL014_2.Add(objPRL014_2);
                        db.SaveChanges();
                        objResponseMsg.Value = "Clad striping depth added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL014_2.HeaderId;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine5Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL014_2 objPRL014_2 = db.PRL014_2.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL014_2 != null)
                {
                    db.PRL014_2.Remove(objPRL014_2);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Spot deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }


            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #endregion

    }
}