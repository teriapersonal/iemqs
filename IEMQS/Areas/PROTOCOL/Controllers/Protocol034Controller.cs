﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol034Controller : clsBase
    {
        // GET: PROTOCOL/Protocol034
        string ControllerURL = "/PROTOCOL/Protocol034/";
        string Title = "THICKNESS REPORT FOR EQUIPMENT";
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id, int? m)
        {
            PRO165 objPRO165 = new PRO165();
            if (!id.HasValue)
            {
                objPRO165.ProtocolNo = string.Empty;
                objPRO165.CreatedBy = objClsLoginInfo.UserName;
                objPRO165.CreatedOn = DateTime.Now;
                db.PRO165.Add(objPRO165);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO165.HeaderId;
            }
            else
            {
                objPRO165 = db.PRO165.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO165);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO165 PRO165)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO165.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO165 objPRO165 = db.PRO165.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO165.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO165.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO165.ProtocolNo = PRO165.ProtocolNo;
                        objPRO165.Service = PRO165.Service;
                        objPRO165.MaxOperTemperature = PRO165.MaxOperTemperature;
                        objPRO165.DesignPressure = PRO165.DesignPressure;
                        objPRO165.MaxOperatingPressure = PRO165.MaxOperatingPressure;
                        objPRO165.ThicknessMeterMake = PRO165.ThicknessMeterMake;
                        objPRO165.ProbeSpecifications = PRO165.ProbeSpecifications;
                        objPRO165.CalibrationBlockThickness = PRO165.CalibrationBlockThickness;
                        objPRO165.Couplant = PRO165.Couplant;
                        objPRO165.EditedBy = objClsLoginInfo.UserName;
                        objPRO165.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO165.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO165 objPRO165 = db.PRO165.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO165 != null && string.IsNullOrWhiteSpace(objPRO165.ProtocolNo))
                    {
                        db.PRO166.RemoveRange(objPRO165.PRO166.ToList());
                        db.PRO165.Remove(objPRO165);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.THICKNESS_REPORT_FOR_EQUIPMENT.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO165.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.TmlNo like '%" + param.sSearch
                        + "%' or pro.ReqThk like '%" + param.sSearch
                        + "%' or pro.ActThk1 like '%" + param.sSearch
                        + "%' or pro.ActThk2 like '%" + param.sSearch
                         + "%' or pro.ActThk3 like '%" + param.sSearch
                          + "%' or pro.AvgThk like '%" + param.sSearch
                         + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL034_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "TmlNo", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ReqThk", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ActThk1", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ActThk2", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ActThk3", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "AvgThk", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "TmlNo", Convert.ToString(uc.TmlNo), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ReqThk", Convert.ToString(uc.ReqThk), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ActThk1", Convert.ToString(uc.ActThk1), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ActThk2", Convert.ToString(uc.ActThk2), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ActThk3", Convert.ToString(uc.ActThk3), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "AvgThk", Convert.ToString(uc.AvgThk), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "", true, "", false, "100","QC"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')"):"")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO166 objPRO166 = new PRO166();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strTmlNo = !string.IsNullOrEmpty(fc["TmlNo" + refLineId]) ? Convert.ToString(fc["TmlNo" + refLineId]).Trim() : "";
                    string strReqThk = !string.IsNullOrEmpty(fc["ReqThk" + refLineId]) ? Convert.ToString(fc["ReqThk" + refLineId]).Trim() : "";
                    string strActThk1 = !string.IsNullOrEmpty(fc["ActThk1" + refLineId]) ? Convert.ToString(fc["ActThk1" + refLineId]).Trim() : "";
                    string strActThk2 = !string.IsNullOrEmpty(fc["ActThk2" + refLineId]) ? Convert.ToString(fc["ActThk2" + refLineId]).Trim() : "";
                    string strActThk3 = !string.IsNullOrEmpty(fc["ActThk3" + refLineId]) ? Convert.ToString(fc["ActThk3" + refLineId]).Trim() : "";
                    string strAvgThk = !string.IsNullOrEmpty(fc["AvgThk" + refLineId]) ? Convert.ToString(fc["AvgThk" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRO166 = db.PRO166.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO166.EditedBy = objClsLoginInfo.UserName;
                        objPRO166.EditedOn = DateTime.Now;
                    }
                    objPRO166.HeaderId = refHeaderId;
                    objPRO166.TmlNo = strTmlNo;
                    objPRO166.ReqThk = strReqThk;
                    objPRO166.ActThk1 = strActThk1;
                    objPRO166.ActThk2 = strActThk2;
                    objPRO166.ActThk3 = strActThk3;
                    objPRO166.AvgThk = strAvgThk;
                    objPRO166.Remarks = strRemarks;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO166.CreatedBy = objClsLoginInfo.UserName;
                        objPRO166.CreatedOn = DateTime.Now;
                        db.PRO166.Add(objPRO166);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO166.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO166 objPRO166 = db.PRO166.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO166 != null)
                {
                    db.PRO166.Remove(objPRO166);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #endregion


        #region Linkage View Code
        #region Header
        public ActionResult Linkage(int? id, int? m)
        {
            PRL165 objPRL165 = new PRL165();
            objPRL165 = db.PRL165.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (objPRL165 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL165.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL165);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL165 PRL165)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL165.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL165 objPRL165 = db.PRL165.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL165.ProtocolNo = PRL165.ProtocolNo;
                    objPRL165.EquipmentNo = PRL165.EquipmentNo;
                    objPRL165.Service = PRL165.Service;
                    objPRL165.MaxOperTemperature = PRL165.MaxOperTemperature;
                    objPRL165.DesignPressure = PRL165.DesignPressure;
                    objPRL165.MaxOperatingPressure = PRL165.MaxOperatingPressure;
                    objPRL165.ThicknessMeterMake = PRL165.ThicknessMeterMake;
                    objPRL165.StageCode = PRL165.StageCode;
                    objPRL165.DrawingNo = PRL165.DrawingNo;
                    objPRL165.DrawingRevisionNo = PRL165.DrawingRevisionNo;
                    objPRL165.ProbeSpecifications = PRL165.ProbeSpecifications;
                    objPRL165.CalibrationBlockThickness = PRL165.CalibrationBlockThickness;
                    objPRL165.Couplant = PRL165.Couplant;
                    objPRL165.EditedBy = objClsLoginInfo.UserName;
                    objPRL165.EditedOn = DateTime.Now;


                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL165.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL165 objPRL165 = db.PRL165.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL165 != null && string.IsNullOrWhiteSpace(objPRL165.ProtocolNo))
                    {
                        db.PRL166.RemoveRange(objPRL165.PRL166.ToList());
                        db.PRL165.Remove(objPRL165);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL165.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.TmlNo like '%" + param.sSearch
                        + "%' or pro.ReqThk like '%" + param.sSearch
                        + "%' or pro.ActThk1 like '%" + param.sSearch
                        + "%' or pro.ActThk2 like '%" + param.sSearch
                         + "%' or pro.ActThk3 like '%" + param.sSearch
                          + "%' or pro.AvgThk like '%" + param.sSearch
                          + "%' or pro.Remarks like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL034_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] { "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "TmlNo", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ReqThk", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ActThk1", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ActThk2", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "ActThk3", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "AvgThk", "", "", false, "", false, "50","QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "Remarks", "", "", false, "", false, "100","QC"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                               "1",
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(uc.LineId),
                               Helper.GenerateHTMLTextbox(uc.LineId, "TmlNo", Convert.ToString(uc.TmlNo), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ReqThk", Convert.ToString(uc.ReqThk), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ActThk1", Convert.ToString(uc.ActThk1), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ActThk2", Convert.ToString(uc.ActThk2), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "ActThk3", Convert.ToString(uc.ActThk3), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "AvgThk", Convert.ToString(uc.AvgThk), "", true, "", false, "50","QC"),
                               Helper.GenerateHTMLTextbox(uc.LineId, "Remarks", Convert.ToString(uc.Remarks), "", true, "", false, "100","QC"),
                               Helper.HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == "QC3" ? Helper.HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')"):"")
                               
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL166 objPRL166 = new PRL166();
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;

                if (refHeaderId > 0)
                {
                    string strTmlNo = !string.IsNullOrEmpty(fc["TmlNo" + refLineId]) ? Convert.ToString(fc["TmlNo" + refLineId]).Trim() : "";
                    string strReqThk = !string.IsNullOrEmpty(fc["ReqThk" + refLineId]) ? Convert.ToString(fc["ReqThk" + refLineId]).Trim() : "";
                    string strActThk1 = !string.IsNullOrEmpty(fc["ActThk1" + refLineId]) ? Convert.ToString(fc["ActThk1" + refLineId]).Trim() : "";
                    string strActThk2 = !string.IsNullOrEmpty(fc["ActThk2" + refLineId]) ? Convert.ToString(fc["ActThk2" + refLineId]).Trim() : "";
                    string strActThk3 = !string.IsNullOrEmpty(fc["ActThk3" + refLineId]) ? Convert.ToString(fc["ActThk3" + refLineId]).Trim() : "";
                    string strAvgThk = !string.IsNullOrEmpty(fc["AvgThk" + refLineId]) ? Convert.ToString(fc["AvgThk" + refLineId]).Trim() : "";
                    string strRemarks = !string.IsNullOrEmpty(fc["Remarks" + refLineId]) ? Convert.ToString(fc["Remarks" + refLineId]).Trim() : "";

                    #region Add New Spot Line1
                    if (refLineId > 0)
                    {
                        objPRL166 = db.PRL166.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL166.EditedBy = objClsLoginInfo.UserName;
                        objPRL166.EditedOn = DateTime.Now;
                    }
                    objPRL166.HeaderId = refHeaderId;
                    objPRL166.TmlNo = strTmlNo;
                    objPRL166.ReqThk = strReqThk;
                    objPRL166.ActThk1 = strActThk1;
                    objPRL166.ActThk2 = strActThk2;
                    objPRL166.ActThk3 = strActThk3;
                    objPRL166.AvgThk = strAvgThk;
                    objPRL166.Remarks = strRemarks;

                    if (refLineId > 0)
                    {
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL166.CreatedBy = objClsLoginInfo.UserName;
                        objPRL166.CreatedOn = DateTime.Now;
                        db.PRL166.Add(objPRL166);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL166.LineId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL166 objPRL166 = db.PRL166.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL166 != null)
                {
                    db.PRL166.Remove(objPRL166);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #endregion

        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }
    }
}