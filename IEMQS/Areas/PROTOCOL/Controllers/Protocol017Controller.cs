﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationMessage;
namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol017Controller : clsBase
    {
        // GET: PROTOCOL/Protocol017
        string ControllerURL = "/PROTOCOL/Protocol017/";
        string Title = "WELD VISUAL & DIMENSION INSPECTION REPORT FOR SUPPORT BRACKET";
        #region Details View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Details(int? id,int? m)
        {
            PRO080 objPRO080 = new PRO080();
            if (!id.HasValue)
            {
                objPRO080.ProtocolNo = string.Empty;
                objPRO080.CreatedBy = objClsLoginInfo.UserName;
                objPRO080.CreatedOn = DateTime.Now;
                db.PRO080.Add(objPRO080);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO080.HeaderId;
            }
            else
            {
                objPRO080 = db.PRO080.Where(i => i.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO080);

        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO080 PRO080)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRO080.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO080 objPRO080 = db.PRO080.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var ProtocolExist = db.PRO080.Where(i => i.HeaderId != refHeaderId && i.ProtocolNo == PRO080.ProtocolNo).FirstOrDefault();
                    if (ProtocolExist == null)
                    {
                        #region Save Data
                        objPRO080.ProtocolNo = PRO080.ProtocolNo;
                        objPRO080.Elevationtol = PRO080.Elevationtol;
                        objPRO080.Orientationtol = PRO080.Orientationtol;
                        objPRO080.Projectiontol = PRO080.Projectiontol;
                        objPRO080.Holetol = PRO080.Holetol;
                        objPRO080.Remark11 = PRO080.Remark11;
                        objPRO080.Remark13 = PRO080.Remark13;
                        objPRO080.Remark14 = PRO080.Remark14;
                        objPRO080.Remark15 = PRO080.Remark15;
                        objPRO080.Remark16 = PRO080.Remark16;
                        objPRO080.Remark17 = PRO080.Remark17;
                        objPRO080.FLATNESSREADINGtol = PRO080.FLATNESSREADINGtol;
                        objPRO080.Remark21 = PRO080.Remark21;
                        objPRO080.Remark23 = PRO080.Remark23;
                        objPRO080.Remark24 = PRO080.Remark24;
                        objPRO080.Remark25 = PRO080.Remark25;
                        objPRO080.Remark26 = PRO080.Remark26;
                        objPRO080.Remark27 = PRO080.Remark27;


                        objPRO080.EditedBy = objClsLoginInfo.UserName;
                        objPRO080.EditedOn = DateTime.Now;
                        objPRO080.Remark15_1 = PRO080.Remark15_1;
                        objPRO080.Remark15_1 = PRO080.Remark15_1.HasValue ? Manager.getDateTime(PRO080.Remark15_1.Value.ToShortDateString()) : PRO080.Remark15_1;

                        objPRO080.Remark25_1 = PRO080.Remark25_1;
                        objPRO080.Remark25_1 = PRO080.Remark25_1.HasValue ? Manager.getDateTime(PRO080.Remark25_1.Value.ToShortDateString()) : PRO080.Remark25_1;


                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol saved successfully";
                        objResponseMsg.HeaderId = objPRO080.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = CommonMessages.ProtocolExists.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO080 objPRO080 = db.PRO080.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO080 != null && string.IsNullOrWhiteSpace(objPRO080.ProtocolNo))
                    {
                        db.PRO081.RemoveRange(objPRO080.PRO081.ToList());
                        db.PRO082.RemoveRange(objPRO080.PRO082.ToList());

                        db.PRO080.Remove(objPRO080);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.WELD_VISUAL_AND_DIMENSION_INSPECTION_REPORT_FOR_SUPPORT_BRACKET.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO080.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.LUGNo like '%" + param.sSearch
                        + "%' or pro.elevationreq like '%" + param.sSearch
                        + "%' or pro.elevationact like '%" + param.sSearch
                        + "%' or pro.orientationreqdeg like '%" + param.sSearch
                        + "%' or pro.orientationreqarc like '%" + param.sSearch
                        + "%' or pro.orientationactarc like '%" + param.sSearch
                        + "%' or pro.projectionreq like '%" + param.sSearch
                        + "%' or pro.projectionact like '%" + param.sSearch
                        + "%' or pro.hole1act like '%" + param.sSearch
                        + "%' or pro.hole2act like '%" + param.sSearch

                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL017_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "LUGNo", "", "", true, "", false, "20", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "elevationreq", "", "", true, "", false, "25", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "elevationact", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "orientationreqdeg", "", "", true, "", false, "25", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "orientationreqarc", "", "", true, "", false, "25", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "orientationactarc", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "projectionreq", "", "", true, "", false, "25", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "projectionact", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "hole1act", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "hole2act", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {

                                "1",
                                Convert.ToString(headerId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "LUGNo", uc.LUGNo, "", true, "", false, "20", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "elevationreq", uc.elevationreq, "", true, "", false, "25", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "elevationact", uc.elevationact, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "orientationreqdeg", uc.orientationreqdeg, "", true, "", false, "25", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "orientationreqarc", uc.orientationreqarc, "", true, "", false, "25", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "orientationactarc", uc.orientationactarc, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "projectionreq", uc.projectionreq, "", true, "", false, "25", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "projectionact", uc.projectionact, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "hole1act", uc.hole1act, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "hole2act", uc.hole2act, "", true, "", false, "25", "PROD"),
                                HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1','tblProtocolLines1')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO081 objPRO081 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objPRO081 = db.PRO081.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();

                        objPRO081.LUGNo = !string.IsNullOrEmpty(fc["LUGNo" + refLineId]) ? Convert.ToString(fc["LUGNo" + refLineId]).Trim() : "";
                        objPRO081.elevationreq = !string.IsNullOrEmpty(fc["elevationreq" + refLineId]) ? Convert.ToString(fc["elevationreq" + refLineId]).Trim() : "";
                        objPRO081.elevationact = !string.IsNullOrEmpty(fc["elevationact" + refLineId]) ? Convert.ToString(fc["elevationact" + refLineId]).Trim() : "";
                        objPRO081.orientationreqdeg = !string.IsNullOrEmpty(fc["orientationreqdeg" + refLineId]) ? Convert.ToString(fc["orientationreqdeg" + refLineId]).Trim() : "";
                        objPRO081.orientationreqarc = !string.IsNullOrEmpty(fc["orientationreqarc" + refLineId]) ? Convert.ToString(fc["orientationreqarc" + refLineId]).Trim() : "";
                        objPRO081.orientationactarc = !string.IsNullOrEmpty(fc["orientationactarc" + refLineId]) ? Convert.ToString(fc["orientationactarc" + refLineId]).Trim() : "";
                        objPRO081.projectionreq = !string.IsNullOrEmpty(fc["projectionreq" + refLineId]) ? Convert.ToString(fc["projectionreq" + refLineId]).Trim() : "";
                        objPRO081.projectionact = !string.IsNullOrEmpty(fc["projectionact" + refLineId]) ? Convert.ToString(fc["projectionact" + refLineId]).Trim() : "";
                        objPRO081.hole1act = !string.IsNullOrEmpty(fc["hole1act" + refLineId]) ? Convert.ToString(fc["hole1act" + refLineId]).Trim() : "";
                        objPRO081.hole2act = !string.IsNullOrEmpty(fc["hole2act" + refLineId]) ? Convert.ToString(fc["hole2act" + refLineId]).Trim() : "";

                        objPRO081.EditedBy = objClsLoginInfo.UserName;
                        objPRO081.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO081 = new PRO081();
                        objPRO081.HeaderId = refHeaderId;
                        objPRO081.LUGNo = !string.IsNullOrEmpty(fc["LUGNo" + newRowIndex]) ? Convert.ToString(fc["LUGNo" + newRowIndex]).Trim() : "";
                        objPRO081.elevationreq = !string.IsNullOrEmpty(fc["elevationreq" + newRowIndex]) ? Convert.ToString(fc["elevationreq" + newRowIndex]).Trim() : "";
                        objPRO081.elevationact = !string.IsNullOrEmpty(fc["elevationact" + newRowIndex]) ? Convert.ToString(fc["elevationact" + newRowIndex]).Trim() : "";
                        objPRO081.orientationreqdeg = !string.IsNullOrEmpty(fc["orientationreqdeg" + newRowIndex]) ? Convert.ToString(fc["orientationreqdeg" + newRowIndex]).Trim() : "";
                        objPRO081.orientationreqarc = !string.IsNullOrEmpty(fc["orientationreqarc" + newRowIndex]) ? Convert.ToString(fc["orientationreqarc" + newRowIndex]).Trim() : "";
                        objPRO081.orientationactarc = !string.IsNullOrEmpty(fc["orientationactarc" + newRowIndex]) ? Convert.ToString(fc["orientationactarc" + newRowIndex]).Trim() : "";
                        objPRO081.projectionreq = !string.IsNullOrEmpty(fc["projectionreq" + newRowIndex]) ? Convert.ToString(fc["projectionreq" + newRowIndex]).Trim() : "";
                        objPRO081.projectionact = !string.IsNullOrEmpty(fc["projectionact" + newRowIndex]) ? Convert.ToString(fc["projectionact" + newRowIndex]).Trim() : "";
                        objPRO081.hole1act = !string.IsNullOrEmpty(fc["hole1act" + newRowIndex]) ? Convert.ToString(fc["hole1act" + newRowIndex]).Trim() : "";
                        objPRO081.hole2act = !string.IsNullOrEmpty(fc["hole2act" + newRowIndex]) ? Convert.ToString(fc["hole2act" + newRowIndex]).Trim() : "";

                        objPRO081.CreatedBy = objClsLoginInfo.UserName;
                        objPRO081.CreatedOn = DateTime.Now;
                        db.PRO081.Add(objPRO081);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO081.LineId;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteProtocolLine1(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO081 objPRO081 = db.PRO081.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO081 != null)
                {
                    db.PRO081.Remove(objPRO081);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2Data(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.LUGNo like '%" + param.sSearch
                        + "%' or pro.A like '%" + param.sSearch
                        + "%' or pro.B like '%" + param.sSearch
                        + "%' or pro.C like '%" + param.sSearch
                        + "%' or pro.D like '%" + param.sSearch
                        + "%' or pro.E like '%" + param.sSearch
                        + "%' or pro.F like '%" + param.sSearch
                        + "%' or pro.G like '%" + param.sSearch
                        + "%' or pro.H like '%" + param.sSearch
                        + "%' or pro.I like '%" + param.sSearch
                        + "%' or pro.J like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL017_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "LUGNo", "", "", true, "", false, "30", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "A", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "B", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "C", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "D", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "E", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "F", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "G", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "H", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "I", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "J", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                "2",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "LUGNo", uc.LUGNo, "", true, "", false, "30", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "A", uc.A, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "B", uc.B, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "C", uc.C, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "D", uc.D, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "E", uc.E, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "F", uc.F, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "G", uc.G, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "H", uc.H, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "I", uc.I, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "J", uc.J, "", true, "", false, "25", "PROD"),
                                HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2','tblProtocolLines2')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveProtocolLine2(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRO082 objPRO082 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objPRO082 = db.PRO082.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRO082.LUGNo = !string.IsNullOrEmpty(fc["LUGNo" + refLineId]) ? Convert.ToString(fc["LUGNo" + refLineId]).Trim() : "";
                        objPRO082.A = !string.IsNullOrEmpty(fc["A" + refLineId]) ? Convert.ToString(fc["A" + refLineId]).Trim() : "";
                        objPRO082.B = !string.IsNullOrEmpty(fc["B" + refLineId]) ? Convert.ToString(fc["B" + refLineId]).Trim() : "";
                        objPRO082.C = !string.IsNullOrEmpty(fc["C" + refLineId]) ? Convert.ToString(fc["C" + refLineId]).Trim() : "";
                        objPRO082.D = !string.IsNullOrEmpty(fc["D" + refLineId]) ? Convert.ToString(fc["D" + refLineId]).Trim() : "";
                        objPRO082.E = !string.IsNullOrEmpty(fc["E" + refLineId]) ? Convert.ToString(fc["E" + refLineId]).Trim() : "";
                        objPRO082.F = !string.IsNullOrEmpty(fc["F" + refLineId]) ? Convert.ToString(fc["F" + refLineId]).Trim() : "";
                        objPRO082.G = !string.IsNullOrEmpty(fc["G" + refLineId]) ? Convert.ToString(fc["G" + refLineId]).Trim() : "";
                        objPRO082.H = !string.IsNullOrEmpty(fc["H" + refLineId]) ? Convert.ToString(fc["H" + refLineId]).Trim() : "";
                        objPRO082.I = !string.IsNullOrEmpty(fc["I" + refLineId]) ? Convert.ToString(fc["I" + refLineId]).Trim() : "";
                        objPRO082.J = !string.IsNullOrEmpty(fc["J" + refLineId]) ? Convert.ToString(fc["J" + refLineId]).Trim() : "";

                        objPRO082.EditedBy = objClsLoginInfo.UserName;
                        objPRO082.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRO082 = new PRO082();
                        objPRO082.HeaderId = refHeaderId;
                        objPRO082.LUGNo = !string.IsNullOrEmpty(fc["LUGNo" + newRowIndex]) ? Convert.ToString(fc["LUGNo" + newRowIndex]).Trim() : "";
                        objPRO082.A = !string.IsNullOrEmpty(fc["A" + newRowIndex]) ? Convert.ToString(fc["A" + newRowIndex]).Trim() : "";
                        objPRO082.B = !string.IsNullOrEmpty(fc["B" + newRowIndex]) ? Convert.ToString(fc["B" + newRowIndex]).Trim() : "";
                        objPRO082.C = !string.IsNullOrEmpty(fc["C" + newRowIndex]) ? Convert.ToString(fc["C" + newRowIndex]).Trim() : "";
                        objPRO082.D = !string.IsNullOrEmpty(fc["D" + newRowIndex]) ? Convert.ToString(fc["D" + newRowIndex]).Trim() : "";
                        objPRO082.E = !string.IsNullOrEmpty(fc["E" + newRowIndex]) ? Convert.ToString(fc["E" + newRowIndex]).Trim() : "";
                        objPRO082.F = !string.IsNullOrEmpty(fc["F" + newRowIndex]) ? Convert.ToString(fc["F" + newRowIndex]).Trim() : "";
                        objPRO082.G = !string.IsNullOrEmpty(fc["G" + newRowIndex]) ? Convert.ToString(fc["G" + newRowIndex]).Trim() : "";
                        objPRO082.H = !string.IsNullOrEmpty(fc["H" + newRowIndex]) ? Convert.ToString(fc["H" + newRowIndex]).Trim() : "";
                        objPRO082.I = !string.IsNullOrEmpty(fc["I" + newRowIndex]) ? Convert.ToString(fc["I" + newRowIndex]).Trim() : "";
                        objPRO082.J = !string.IsNullOrEmpty(fc["J" + newRowIndex]) ? Convert.ToString(fc["J" + newRowIndex]).Trim() : "";

                        objPRO082.CreatedBy = objClsLoginInfo.UserName;
                        objPRO082.CreatedOn = DateTime.Now;
                        db.PRO082.Add(objPRO082);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRO082.LineId;
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteProtocolLine2(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRO082 objPRO082 = db.PRO082.FirstOrDefault(c => c.LineId == lineId);
                if (objPRO082 != null)
                {
                    db.PRO082.Remove(objPRO082);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL080 objPRL080 = new PRL080();
            objPRL080 = db.PRL080.Where(i => i.HeaderId == id).FirstOrDefault();

            if (objPRL080 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL080.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }
            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL080);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL080 PRL080)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL080.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL080 objPRL080 = db.PRL080.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL080.DCRNo = PRL080.DCRNo;
                    objPRL080.ProtocolNo = PRL080.ProtocolNo != null ? PRL080.ProtocolNo : "";
                    objPRL080.Elevationtol = PRL080.Elevationtol;
                    objPRL080.Orientationtol = PRL080.Orientationtol;
                    objPRL080.Projectiontol = PRL080.Projectiontol;
                    objPRL080.Holetol = PRL080.Holetol;
                    objPRL080.Remark11 = PRL080.Remark11;
                    objPRL080.Remark13 = PRL080.Remark13;
                    objPRL080.Remark14 = PRL080.Remark14;
                    objPRL080.Remark15 = PRL080.Remark15;

                    objPRL080.Remark16 = PRL080.Remark16;
                    objPRL080.Remark17 = PRL080.Remark17;
                    objPRL080.FLATNESSREADINGtol = PRL080.FLATNESSREADINGtol;
                    objPRL080.Remark21 = PRL080.Remark21;
                    objPRL080.Remark23 = PRL080.Remark23;
                    objPRL080.Remark24 = PRL080.Remark24;
                    objPRL080.Remark25 = PRL080.Remark25;

                    objPRL080.Remark26 = PRL080.Remark26;
                    objPRL080.Remark27 = PRL080.Remark27;

                    objPRL080.EditedBy = objClsLoginInfo.UserName;
                    objPRL080.EditedOn = DateTime.Now;
                    objPRL080.Remark15_1 = PRL080.Remark15_1;
                    objPRL080.Remark15_1 = PRL080.Remark15_1.HasValue ? Manager.getDateTime(PRL080.Remark15_1.Value.ToShortDateString()) : PRL080.Remark15_1;

                    objPRL080.Remark25_1 = PRL080.Remark25_1;
                    objPRL080.Remark25_1 = PRL080.Remark25_1.HasValue ? Manager.getDateTime(PRL080.Remark25_1.Value.ToShortDateString()) : PRL080.Remark25_1;


                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL080.HeaderId;
                    #endregion

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL080 objPRL080 = db.PRL080.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL080 != null && string.IsNullOrWhiteSpace(objPRL080.ProtocolNo))
                    {
                        db.PRL081.RemoveRange(objPRL080.PRL081.ToList());
                        db.PRL082.RemoveRange(objPRL080.PRL082.ToList());

                        db.PRL080.Remove(objPRL080);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL080.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line1 details
        [HttpPost]
        public ActionResult GetProtocolLines1DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.LUGNo like '%" + param.sSearch
                        + "%' or pro.elevationreq like '%" + param.sSearch
                        + "%' or pro.elevationact like '%" + param.sSearch
                        + "%' or pro.orientationreqdeg like '%" + param.sSearch
                        + "%' or pro.orientationreqarc like '%" + param.sSearch
                        + "%' or pro.orientationactarc like '%" + param.sSearch
                        + "%' or pro.projectionreq like '%" + param.sSearch
                        + "%' or pro.projectionact like '%" + param.sSearch
                        + "%' or pro.hole1act like '%" + param.sSearch
                        + "%' or pro.hole2act like '%" + param.sSearch
                        + "%')";
                }

                var lstResult = db.SP_IPI_PROTOCOL017_LINKAGE_GET_LINES1_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "1",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "LUGNo", "", "", true, "", false, "20", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "elevationreq", "", "", true, "", false, "25", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "elevationact", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "orientationreqdeg", "", "", true, "", false, "25", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "orientationreqarc", "", "", true, "", false, "25", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "orientationactarc", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "projectionreq", "", "", true, "", false, "25", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "projectionact", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "hole1act", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "hole2act", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine1(0);")
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                "1",
                                Convert.ToString(headerId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "LUGNo", uc.LUGNo, "", true, "", false, "20", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "elevationreq", uc.elevationreq, "", true, "", false, "25", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "elevationact", uc.elevationact, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "orientationreqdeg", uc.orientationreqdeg, "", true, "", false, "25", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "orientationreqarc", uc.orientationreqarc, "", true, "", false, "25", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "orientationactarc", uc.orientationactarc, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "projectionreq", uc.projectionreq, "", true, "", false, "25", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "projectionact", uc.projectionact, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "hole1act", uc.hole1act, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "hole2act", uc.hole2act, "", true, "", false, "25", "PROD"),
                                HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine1Linkage','tblProtocolLines1')") : "")
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveProtocolLine1Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL081 objPRL081 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objPRL081 = db.PRL081.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();

                        objPRL081.LUGNo = !string.IsNullOrEmpty(fc["LUGNo" + refLineId]) ? Convert.ToString(fc["LUGNo" + refLineId]).Trim() : "";
                        objPRL081.elevationreq = !string.IsNullOrEmpty(fc["elevationreq" + refLineId]) ? Convert.ToString(fc["elevationreq" + refLineId]).Trim() : "";
                        objPRL081.elevationact = !string.IsNullOrEmpty(fc["elevationact" + refLineId]) ? Convert.ToString(fc["elevationact" + refLineId]).Trim() : "";
                        objPRL081.orientationreqdeg = !string.IsNullOrEmpty(fc["orientationreqdeg" + refLineId]) ? Convert.ToString(fc["orientationreqdeg" + refLineId]).Trim() : "";
                        objPRL081.orientationreqarc = !string.IsNullOrEmpty(fc["orientationreqarc" + refLineId]) ? Convert.ToString(fc["orientationreqarc" + refLineId]).Trim() : "";
                        objPRL081.orientationactarc = !string.IsNullOrEmpty(fc["orientationactarc" + refLineId]) ? Convert.ToString(fc["orientationactarc" + refLineId]).Trim() : "";
                        objPRL081.projectionreq = !string.IsNullOrEmpty(fc["projectionreq" + refLineId]) ? Convert.ToString(fc["projectionreq" + refLineId]).Trim() : "";
                        objPRL081.projectionact = !string.IsNullOrEmpty(fc["projectionact" + refLineId]) ? Convert.ToString(fc["projectionact" + refLineId]).Trim() : "";
                        objPRL081.hole1act = !string.IsNullOrEmpty(fc["hole1act" + refLineId]) ? Convert.ToString(fc["hole1act" + refLineId]).Trim() : "";
                        objPRL081.hole2act = !string.IsNullOrEmpty(fc["hole2act" + refLineId]) ? Convert.ToString(fc["hole2act" + refLineId]).Trim() : "";

                        objPRL081.EditedBy = objClsLoginInfo.UserName;
                        objPRL081.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL081 = new PRL081();
                        objPRL081.HeaderId = refHeaderId;
                        objPRL081.LUGNo = !string.IsNullOrEmpty(fc["LUGNo" + newRowIndex]) ? Convert.ToString(fc["LUGNo" + newRowIndex]).Trim() : "";
                        objPRL081.elevationreq = !string.IsNullOrEmpty(fc["elevationreq" + newRowIndex]) ? Convert.ToString(fc["elevationreq" + newRowIndex]).Trim() : "";
                        objPRL081.elevationact = !string.IsNullOrEmpty(fc["elevationact" + newRowIndex]) ? Convert.ToString(fc["elevationact" + newRowIndex]).Trim() : "";
                        objPRL081.orientationreqdeg = !string.IsNullOrEmpty(fc["orientationreqdeg" + newRowIndex]) ? Convert.ToString(fc["orientationreqdeg" + newRowIndex]).Trim() : "";
                        objPRL081.orientationreqarc = !string.IsNullOrEmpty(fc["orientationreqarc" + newRowIndex]) ? Convert.ToString(fc["orientationreqarc" + newRowIndex]).Trim() : "";
                        objPRL081.orientationactarc = !string.IsNullOrEmpty(fc["orientationactarc" + newRowIndex]) ? Convert.ToString(fc["orientationactarc" + newRowIndex]).Trim() : "";
                        objPRL081.projectionreq = !string.IsNullOrEmpty(fc["projectionreq" + newRowIndex]) ? Convert.ToString(fc["projectionreq" + newRowIndex]).Trim() : "";
                        objPRL081.projectionact = !string.IsNullOrEmpty(fc["projectionact" + newRowIndex]) ? Convert.ToString(fc["projectionact" + newRowIndex]).Trim() : "";
                        objPRL081.hole1act = !string.IsNullOrEmpty(fc["hole1act" + newRowIndex]) ? Convert.ToString(fc["hole1act" + newRowIndex]).Trim() : "";
                        objPRL081.hole2act = !string.IsNullOrEmpty(fc["hole2act" + newRowIndex]) ? Convert.ToString(fc["hole2act" + newRowIndex]).Trim() : "";

                        objPRL081.CreatedBy = objClsLoginInfo.UserName;
                        objPRL081.CreatedOn = DateTime.Now;
                        db.PRL081.Add(objPRL081);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL081.LineId;

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolLine1Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL081 objPRL081 = db.PRL081.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL081 != null)
                {
                    db.PRL081.Remove(objPRL081);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Line2 details
        [HttpPost]
        public ActionResult GetProtocolLines2DataLinkage(JQueryDataTableParamModel param, string headerId)
        {
            try
            {
                int intHeaderId = Convert.ToInt32(headerId);
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;

                if (!string.IsNullOrEmpty(headerId))
                {
                    whereCondition += " AND HeaderId = " + headerId;
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " AND (pro.LUGNo like '%" + param.sSearch
                        + "%' or pro.A like '%" + param.sSearch
                        + "%' or pro.B like '%" + param.sSearch
                        + "%' or pro.C like '%" + param.sSearch
                        + "%' or pro.D like '%" + param.sSearch
                        + "%' or pro.E like '%" + param.sSearch
                        + "%' or pro.F like '%" + param.sSearch
                        + "%' or pro.G like '%" + param.sSearch
                        + "%' or pro.H like '%" + param.sSearch
                        + "%' or pro.I like '%" + param.sSearch
                        + "%' or pro.J like '%" + param.sSearch
                        + "%')";
                }
                var lstResult = db.SP_IPI_PROTOCOL017_LINKAGE_GET_LINES2_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                   "2",
                                   Convert.ToString(headerId),
                                   Convert.ToString(newRecordId),
                                   Helper.GenerateHTMLTextbox(newRecordId, "LUGNo", "", "", true, "", false, "30", "QC"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "A", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "B", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "C", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "D", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "E", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "F", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "G", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "H", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "I", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateHTMLTextbox(newRecordId, "J", "", "", true, "", false, "25", "PROD"),
                                   Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveProtocolLine2(0);")
                                };

                var data = (from uc in lstResult
                            select new[]
                            {
                                "2",
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                Helper.GenerateHTMLTextbox(uc.LineId, "LUGNo", uc.LUGNo, "", true, "", false, "30", "QC"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "A", uc.A, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "B", uc.B, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "C", uc.C, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "D", uc.D, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "E", uc.E, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "F", uc.F, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "G", uc.G, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "H", uc.H, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "I", uc.I, "", true, "", false, "25", "PROD"),
                                Helper.GenerateHTMLTextbox(uc.LineId, "J", uc.J, "", true, "", false, "25", "PROD"),
                                HTMLActionString(uc.LineId,"Edit","Edit Record","fa fa-pencil-square-o iconspace editline","") + (Manager.GetUserAccessRightsForProtocol().UserRole.ToUpper() == clsImplementationEnum.UserRoleName.QC3.GetStringValue().ToUpper() ? HTMLActionString(uc.LineId, "Delete", "Delete Spot", "fa fa-trash-o", "DeleteProtocolLineWithConfirmation("+ uc.LineId +","+ uc.HeaderId +",'"+ControllerURL+"DeleteProtocolLine2Linkage','tblProtocolLines2')") : "")
                           }).ToList();
                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveProtocolLine2Linkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            PRL082 objPRL082 = null;
            int newRowIndex = 0;
            try
            {
                int refHeaderId = !string.IsNullOrEmpty(fc["HeaderId"]) ? Convert.ToInt32(fc["HeaderId"]) : 0;
                if (refHeaderId > 0)
                {
                    int refLineId = !string.IsNullOrEmpty(fc["LineId"]) ? Convert.ToInt32(fc["LineId"]) : 0;
                    if (refLineId > 0)
                    {
                        objPRL082 = db.PRL082.Where(x => x.HeaderId == refHeaderId && x.LineId == refLineId).FirstOrDefault();
                        objPRL082.LUGNo = !string.IsNullOrEmpty(fc["LUGNo" + refLineId]) ? Convert.ToString(fc["LUGNo" + refLineId]).Trim() : "";
                        objPRL082.A = !string.IsNullOrEmpty(fc["A" + refLineId]) ? Convert.ToString(fc["A" + refLineId]).Trim() : "";
                        objPRL082.B = !string.IsNullOrEmpty(fc["B" + refLineId]) ? Convert.ToString(fc["B" + refLineId]).Trim() : "";
                        objPRL082.C = !string.IsNullOrEmpty(fc["C" + refLineId]) ? Convert.ToString(fc["C" + refLineId]).Trim() : "";
                        objPRL082.D = !string.IsNullOrEmpty(fc["D" + refLineId]) ? Convert.ToString(fc["D" + refLineId]).Trim() : "";
                        objPRL082.E = !string.IsNullOrEmpty(fc["E" + refLineId]) ? Convert.ToString(fc["E" + refLineId]).Trim() : "";
                        objPRL082.F = !string.IsNullOrEmpty(fc["F" + refLineId]) ? Convert.ToString(fc["F" + refLineId]).Trim() : "";
                        objPRL082.G = !string.IsNullOrEmpty(fc["G" + refLineId]) ? Convert.ToString(fc["G" + refLineId]).Trim() : "";
                        objPRL082.H = !string.IsNullOrEmpty(fc["H" + refLineId]) ? Convert.ToString(fc["H" + refLineId]).Trim() : "";
                        objPRL082.I = !string.IsNullOrEmpty(fc["I" + refLineId]) ? Convert.ToString(fc["I" + refLineId]).Trim() : "";
                        objPRL082.J = !string.IsNullOrEmpty(fc["J" + refLineId]) ? Convert.ToString(fc["J" + refLineId]).Trim() : "";

                        objPRL082.EditedBy = objClsLoginInfo.UserName;
                        objPRL082.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = "Data updated successfully";
                    }
                    else
                    {
                        objPRL082 = new PRL082();
                        objPRL082.HeaderId = refHeaderId;
                        objPRL082.LUGNo = !string.IsNullOrEmpty(fc["LUGNo" + newRowIndex]) ? Convert.ToString(fc["LUGNo" + newRowIndex]).Trim() : "";
                        objPRL082.A = !string.IsNullOrEmpty(fc["A" + newRowIndex]) ? Convert.ToString(fc["A" + newRowIndex]).Trim() : "";
                        objPRL082.B = !string.IsNullOrEmpty(fc["B" + newRowIndex]) ? Convert.ToString(fc["B" + newRowIndex]).Trim() : "";
                        objPRL082.C = !string.IsNullOrEmpty(fc["C" + newRowIndex]) ? Convert.ToString(fc["C" + newRowIndex]).Trim() : "";
                        objPRL082.D = !string.IsNullOrEmpty(fc["D" + newRowIndex]) ? Convert.ToString(fc["D" + newRowIndex]).Trim() : "";
                        objPRL082.E = !string.IsNullOrEmpty(fc["E" + newRowIndex]) ? Convert.ToString(fc["E" + newRowIndex]).Trim() : "";
                        objPRL082.F = !string.IsNullOrEmpty(fc["F" + newRowIndex]) ? Convert.ToString(fc["F" + newRowIndex]).Trim() : "";
                        objPRL082.G = !string.IsNullOrEmpty(fc["G" + newRowIndex]) ? Convert.ToString(fc["G" + newRowIndex]).Trim() : "";
                        objPRL082.H = !string.IsNullOrEmpty(fc["H" + newRowIndex]) ? Convert.ToString(fc["H" + newRowIndex]).Trim() : "";
                        objPRL082.I = !string.IsNullOrEmpty(fc["I" + newRowIndex]) ? Convert.ToString(fc["I" + newRowIndex]).Trim() : "";
                        objPRL082.J = !string.IsNullOrEmpty(fc["J" + newRowIndex]) ? Convert.ToString(fc["J" + newRowIndex]).Trim() : "";

                        objPRL082.CreatedBy = objClsLoginInfo.UserName;
                        objPRL082.CreatedOn = DateTime.Now;
                        db.PRL082.Add(objPRL082);
                        db.SaveChanges();
                        objResponseMsg.Value = "Data added successfully";
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderId = objPRL082.LineId;
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteProtocolLine2Linkage(int lineId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                PRL082 objPRL082 = db.PRL082.FirstOrDefault(c => c.LineId == lineId);
                if (objPRL082 != null)
                {
                    db.PRL082.Remove(objPRL082);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data deleted successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion
        public string HTMLActionString(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", string onHrefURL = "", bool isDisable = false)
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? onClickMethod : "";
            string onHref = !string.IsNullOrEmpty(onHrefURL) ? "href='" + onHrefURL + "'" : "";

            if (isDisable)
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\" ></i>";
                //htmlControl = "<i id=\"" + inputID + "' name='" + inputID + "' style='cursor: pointer; opacity:0.3; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                htmlControl += "<i id=\"" + inputID.ToString() + "\" name=\"" + inputID + "\"  style='cursor: pointer; margin-right:10px;' Title=\"" + buttonTooltip + "\" class=\"" + className + "\"  onclick=\"" + onClickEvent + "\" ></i>";
                //htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer; margin-right:10px;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            }

            if (!string.IsNullOrEmpty(onHref))
            {
                htmlControl = "<a " + onHref + " >" + htmlControl + "</a>";
            }

            return htmlControl;
        }

    }
}





