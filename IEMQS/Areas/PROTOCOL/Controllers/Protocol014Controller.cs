﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PROTOCOL.Controllers
{
    public class Protocol014Controller : clsBase
    {
        string ControllerURL = "/PROTOCOL/Protocol014/";
        string Title = "NUB  DIMENSION INSPECTION REPORT OF SQUARE TYPE";
        #region Protocol Details Page
        [SessionExpireFilter] //, UserPermissions, AllowAnonymous]
        // GET: PROTOCOL/Protocol014
        public ActionResult Details(int? id,int? m)
        {
            PRO065 objPRO065 = new PRO065();
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            List<string> lstYesNoNA = clsImplementationEnum.getYesNoNA().ToList();
            ViewBag.YesNoNAEnum = lstYesNoNA.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();


            UserRoleAccessDetails objUserRoleAccessDetails = GetUserAccessRights();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;

            ViewBag.ControllerURL = ControllerURL;
            ViewBag.Title = Title;

            if (!id.HasValue)
            {
                objPRO065.ProtocolNo = string.Empty;
                objPRO065.CreatedBy = objClsLoginInfo.UserName;
                objPRO065.CreatedOn = DateTime.Now;
                db.PRO065.Add(objPRO065);
                db.SaveChanges();
                ViewBag.HeaderId = objPRO065.HeaderId;
            }
            else
            {
                objPRO065 = db.PRO065.Where(x => x.HeaderId == id).FirstOrDefault();
                ViewBag.HeaderId = id.Value;
            }
            if (m == 1)
            { ViewBag.isEditable = "true"; }
            else
            { ViewBag.isEditable = "false"; }
            return View(objPRO065);
        }

        [HttpPost]
        public ActionResult SaveProtocolHeaderData(PRO065 PRO065)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId =PRO065.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRO065 objPRO065 = db.PRO065.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    var lstPRO065 = db.PRO065.Where(o => o.HeaderId != refHeaderId && o.ProtocolNo.Trim() == PRO065.ProtocolNo.Trim()).ToList();
                    if (lstPRO065.Count == 0)
                    {
                        #region Save Data
                        objPRO065.ProtocolNo = PRO065.ProtocolNo;
                        objPRO065.elevationReq = PRO065.elevationReq;
                        objPRO065.elevationTol = PRO065.elevationTol;
                        objPRO065.nubwidthreq = PRO065.nubwidthreq;
                        objPRO065.nubwidthtol = PRO065.nubwidthtol;
                        objPRO065.nubheightreq = PRO065.nubheightreq;
                        objPRO065.nubheighttol = PRO065.nubheighttol;
                        objPRO065.nubIDreq = PRO065.nubIDreq;
                        objPRO065.nubIDtol = PRO065.nubIDtol;

                        objPRO065.elevation1 = PRO065.elevation1;
                        objPRO065.elevation2 = PRO065.elevation2;
                        objPRO065.elevation3 = PRO065.elevation3;
                        objPRO065.elevation4 = PRO065.elevation4;
                        objPRO065.elevation5 = PRO065.elevation5;
                        objPRO065.elevation6 = PRO065.elevation6;
                        objPRO065.elevation7 = PRO065.elevation7;
                        objPRO065.elevation8 = PRO065.elevation8;
                        objPRO065.elevation9 = PRO065.elevation9;
                        objPRO065.elevation10 = PRO065.elevation10;
                        objPRO065.elevation11 = PRO065.elevation11;
                        objPRO065.elevation12 = PRO065.elevation12;

                        objPRO065.nubwidth1 = PRO065.nubwidth1;
                        objPRO065.nubwidth2 = PRO065.nubwidth2;
                        objPRO065.nubwidth3 = PRO065.nubwidth3;
                        objPRO065.nubwidth4 = PRO065.nubwidth4;
                        objPRO065.nubwidth5 = PRO065.nubwidth5;
                        objPRO065.nubwidth6 = PRO065.nubwidth6;
                        objPRO065.nubwidth7 = PRO065.nubwidth7;
                        objPRO065.nubwidth8 = PRO065.nubwidth8;
                        objPRO065.nubwidth9 = PRO065.nubwidth9;
                        objPRO065.nubwidth10 = PRO065.nubwidth10;
                        objPRO065.nubwidth11 = PRO065.nubwidth11;
                        objPRO065.nubwidth12 = PRO065.nubwidth12;

                        objPRO065.nubheight1 = PRO065.nubheight1;
                        objPRO065.nubheight2 = PRO065.nubheight2;
                        objPRO065.nubheight3 = PRO065.nubheight3;
                        objPRO065.nubheight4 = PRO065.nubheight4;
                        objPRO065.nubheight5 = PRO065.nubheight5;
                        objPRO065.nubheight6 = PRO065.nubheight6;
                        objPRO065.nubheight7 = PRO065.nubheight7;
                        objPRO065.nubheight8 = PRO065.nubheight8;
                        objPRO065.nubheight9 = PRO065.nubheight9;
                        objPRO065.nubheight10 = PRO065.nubheight10;
                        objPRO065.nubheight11 = PRO065.nubheight11;
                        objPRO065.nubheight12 = PRO065.nubheight12;

                        objPRO065.nubID1 = PRO065.nubID1;
                        objPRO065.nubID2 = PRO065.nubID2;
                        objPRO065.nubID3 = PRO065.nubID3;
                        objPRO065.nubID4 = PRO065.nubID4;
                        objPRO065.nubID5 = PRO065.nubID5;
                        objPRO065.nubID6 = PRO065.nubID6;
                        objPRO065.nubID7 = PRO065.nubID7;
                        objPRO065.nubID8 = PRO065.nubID8;
                        objPRO065.nubID9 = PRO065.nubID9;
                        objPRO065.nubID10 = PRO065.nubID10;
                        objPRO065.nubID11 = PRO065.nubID11;
                        objPRO065.nubID12 = PRO065.nubID12;

                        objPRO065.A11 = PRO065.A11;
                        objPRO065.A12 = PRO065.A12;
                        objPRO065.A13 = PRO065.A13;
                        objPRO065.A14 = PRO065.A14;
                        objPRO065.A15 = PRO065.A15;
                        objPRO065.A16 = PRO065.A16;
                        objPRO065.A17 = PRO065.A17;
                        objPRO065.A18 = PRO065.A18;
                        objPRO065.A19 = PRO065.A19;
                        objPRO065.A110 = PRO065.A110;
                        objPRO065.A111 = PRO065.A111;
                        objPRO065.A112 = PRO065.A112;

                        objPRO065.A21 = PRO065.A21;
                        objPRO065.A22 = PRO065.A22;
                        objPRO065.A23 = PRO065.A23;
                        objPRO065.A24 = PRO065.A24;
                        objPRO065.A25 = PRO065.A25;
                        objPRO065.A26 = PRO065.A26;
                        objPRO065.A27 = PRO065.A27;
                        objPRO065.A28 = PRO065.A28;
                        objPRO065.A29 = PRO065.A29;
                        objPRO065.A210 = PRO065.A210;
                        objPRO065.A211 = PRO065.A211;
                        objPRO065.A212 = PRO065.A212;

                        objPRO065.B11 = PRO065.B11;
                        objPRO065.B12 = PRO065.B12;
                        objPRO065.B13 = PRO065.B13;
                        objPRO065.B14 = PRO065.B14;
                        objPRO065.B15 = PRO065.B15;
                        objPRO065.B16 = PRO065.B16;
                        objPRO065.B17 = PRO065.B17;
                        objPRO065.B18 = PRO065.B18;
                        objPRO065.B19 = PRO065.B19;
                        objPRO065.B110 = PRO065.B110;
                        objPRO065.B111 = PRO065.B111;
                        objPRO065.B112 = PRO065.B112;

                        objPRO065.B21 = PRO065.B21;
                        objPRO065.B22 = PRO065.B22;
                        objPRO065.B23 = PRO065.B23;
                        objPRO065.B24 = PRO065.B24;
                        objPRO065.B25 = PRO065.B25;
                        objPRO065.B26 = PRO065.B26;
                        objPRO065.B27 = PRO065.B27;
                        objPRO065.B28 = PRO065.B28;
                        objPRO065.B29 = PRO065.B29;
                        objPRO065.B210 = PRO065.B210;
                        objPRO065.B211 = PRO065.B211;
                        objPRO065.B212 = PRO065.B212;

                        objPRO065.C11 = PRO065.C11;
                        objPRO065.C12 = PRO065.C12;
                        objPRO065.C13 = PRO065.C13;
                        objPRO065.C14 = PRO065.C14;
                        objPRO065.C15 = PRO065.C15;
                        objPRO065.C16 = PRO065.C16;
                        objPRO065.C17 = PRO065.C17;
                        objPRO065.C18 = PRO065.C18;
                        objPRO065.C19 = PRO065.C19;
                        objPRO065.C110 = PRO065.C110;
                        objPRO065.C111 = PRO065.C111;
                        objPRO065.C112 = PRO065.C112;

                        objPRO065.C21 = PRO065.C21;
                        objPRO065.C22 = PRO065.C22;
                        objPRO065.C23 = PRO065.C23;
                        objPRO065.C24 = PRO065.C24;
                        objPRO065.C25 = PRO065.C25;
                        objPRO065.C26 = PRO065.C26;
                        objPRO065.C27 = PRO065.C27;
                        objPRO065.C28 = PRO065.C28;
                        objPRO065.C29 = PRO065.C29;
                        objPRO065.C210 = PRO065.C210;
                        objPRO065.C211 = PRO065.C211;
                        objPRO065.C212 = PRO065.C212;

                        objPRO065.Remark2 = PRO065.Remark2;
                        objPRO065.Remark3 = PRO065.Remark3;
                        objPRO065.Remark4 = PRO065.Remark4;
                        objPRO065.Remark5 = PRO065.Remark5;
                        objPRO065.Remark5_1 = PRO065.Remark5_1.HasValue ? Manager.getDateTime(PRO065.Remark5_1.Value.ToShortDateString()) : PRO065.Remark5_1;

                        objPRO065.Remark6 = PRO065.Remark6;                        
                        objPRO065.EditedBy = objClsLoginInfo.UserName;
                        objPRO065.EditedOn = DateTime.Now;

                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolSaved;
                        objResponseMsg.HeaderId = objPRO065.HeaderId;
                        #endregion
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.ProtocolExists;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult DeleteProtocolHeaderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId > 0)
                {
                    PRO065 objPRO065 = db.PRO065.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRO065 != null && string.IsNullOrWhiteSpace(objPRO065.ProtocolNo))
                    {
                        db.PRO065.Remove(objPRO065);
                        db.SaveChanges();
                        Manager.DeleteProtocol(clsImplementationEnum.ProtocolType.VISUAL_AND_DIMENSION_BEFORE_OVERLAY_FOR_SQUARE_TYPE_NUB.GetStringValue(), refHeaderId);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRO065.HeaderId;
                    }

                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region Linkage View Code
        #region Header
        [SessionExpireFilter]
        public ActionResult Linkage(int? id, int? m)
        {
            PRL065 objPRL065 = new PRL065();
            objPRL065 = db.PRL065.Where(i => i.HeaderId == id).FirstOrDefault();

            UserRoleAccessDetails objUserRoleAccessDetails = Manager.GetUserAccessRightsForProtocol();
            ViewBag.AccessRole = objUserRoleAccessDetails.UserDesignation;
            ViewBag.UserRole = objUserRoleAccessDetails.UserRole;
            ViewBag.Title = Title;
            ViewBag.ControllerURL = ControllerURL;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            if (objPRL065 != null)
            {
                var objQproject = db.QMS010.Where(x => x.QualityProject == objPRL065.QualityProject).FirstOrDefault();
                if (objQproject != null)
                {
                    ViewBag.ProjectDesc = db.COM001.Where(i => i.t_cprj == objQproject.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                    ViewBag.ManufacturingCode = objQproject.ManufacturingCode;
                }
                else
                {
                    ViewBag.ProjectDesc = "";
                    ViewBag.ManufacturingCode = "";
                }
            }
            else
            {
                ViewBag.ProjectDesc = "";
                ViewBag.ManufacturingCode = "";
            }

            if (m == 1)
            {
                ViewBag.isEditable = "true";
            }
            else
            { ViewBag.isEditable = "false"; }

            return View(objPRL065);
        }
        [HttpPost]
        public ActionResult SaveProtocolHeaderDataLinkage(PRL065 PRL065)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = PRL065.HeaderId;
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL065 objPRL065 = db.PRL065.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();

                    #region Save Data
                    objPRL065.DCRNo = PRL065.DCRNo;
                    objPRL065.ProtocolNo = PRL065.ProtocolNo != null ? PRL065.ProtocolNo : "";
                    objPRL065.elevationReq = PRL065.elevationReq;
                    objPRL065.elevationTol = PRL065.elevationTol;
                    objPRL065.nubwidthreq = PRL065.nubwidthreq;
                    objPRL065.nubwidthtol = PRL065.nubwidthtol;
                    objPRL065.nubheightreq = PRL065.nubheightreq;
                    objPRL065.nubheighttol = PRL065.nubheighttol;
                    objPRL065.nubIDreq = PRL065.nubIDreq;
                    objPRL065.nubIDtol = PRL065.nubIDtol;


                    objPRL065.elevation1 = PRL065.elevation1;
                    objPRL065.elevation2 = PRL065.elevation2;
                    objPRL065.elevation3 = PRL065.elevation3;
                    objPRL065.elevation4 = PRL065.elevation4;
                    objPRL065.elevation5 = PRL065.elevation5;
                    objPRL065.elevation6 = PRL065.elevation6;
                    objPRL065.elevation7 = PRL065.elevation7;
                    objPRL065.elevation8 = PRL065.elevation8;
                    objPRL065.elevation9 = PRL065.elevation9;
                    objPRL065.elevation10 =PRL065.elevation10;
                    objPRL065.elevation11 = PRL065.elevation11;
                    objPRL065.elevation12 = PRL065.elevation12;

                    objPRL065.nubwidth1 = PRL065.nubwidth1;
                    objPRL065.nubwidth2 = PRL065.nubwidth2;
                    objPRL065.nubwidth3 = PRL065.nubwidth3;
                    objPRL065.nubwidth4 = PRL065.nubwidth4;
                    objPRL065.nubwidth5 = PRL065.nubwidth5;
                    objPRL065.nubwidth6 = PRL065.nubwidth6;
                    objPRL065.nubwidth7 = PRL065.nubwidth7;
                    objPRL065.nubwidth8 = PRL065.nubwidth8;
                    objPRL065.nubwidth9 = PRL065.nubwidth9;
                    objPRL065.nubwidth10 = PRL065.nubwidth10;
                    objPRL065.nubwidth11 = PRL065.nubwidth11;
                    objPRL065.nubwidth12 = PRL065.nubwidth12;

                    objPRL065.nubheight1 = PRL065.nubheight1;
                    objPRL065.nubheight2 = PRL065.nubheight2;
                    objPRL065.nubheight3 = PRL065.nubheight3;
                    objPRL065.nubheight4 = PRL065.nubheight4;
                    objPRL065.nubheight5 = PRL065.nubheight5;
                    objPRL065.nubheight6 = PRL065.nubheight6;
                    objPRL065.nubheight7 = PRL065.nubheight7;
                    objPRL065.nubheight8 = PRL065.nubheight8;
                    objPRL065.nubheight9 = PRL065.nubheight9;
                    objPRL065.nubheight10 = PRL065.nubheight10;
                    objPRL065.nubheight11 = PRL065.nubheight11;
                    objPRL065.nubheight12 = PRL065.nubheight12;

                    objPRL065.nubID1 = PRL065.nubID1;
                    objPRL065.nubID2 = PRL065.nubID2;
                    objPRL065.nubID3 = PRL065.nubID3;
                    objPRL065.nubID4 = PRL065.nubID4;
                    objPRL065.nubID5 = PRL065.nubID5;
                    objPRL065.nubID6 = PRL065.nubID6;
                    objPRL065.nubID7 = PRL065.nubID7;
                    objPRL065.nubID8 = PRL065.nubID8;
                    objPRL065.nubID9 = PRL065.nubID9;
                    objPRL065.nubID10 = PRL065.nubID10;
                    objPRL065.nubID11 = PRL065.nubID11;
                    objPRL065.nubID12 = PRL065.nubID12;

                    objPRL065.A11 = PRL065.A11;
                    objPRL065.A12 = PRL065.A12;
                    objPRL065.A13 = PRL065.A13;
                    objPRL065.A14 = PRL065.A14;
                    objPRL065.A15 = PRL065.A15;
                    objPRL065.A16 = PRL065.A16;
                    objPRL065.A17 = PRL065.A17;
                    objPRL065.A18 = PRL065.A18;
                    objPRL065.A19 = PRL065.A19;
                    objPRL065.A110 = PRL065.A110;
                    objPRL065.A111 = PRL065.A111;
                    objPRL065.A112 = PRL065.A112;

                    objPRL065.A21 = PRL065.A21;
                    objPRL065.A22 = PRL065.A22;
                    objPRL065.A23 = PRL065.A23;
                    objPRL065.A24 = PRL065.A24;
                    objPRL065.A25 = PRL065.A25;
                    objPRL065.A26 = PRL065.A26;
                    objPRL065.A27 = PRL065.A27;
                    objPRL065.A28 = PRL065.A28;
                    objPRL065.A29 = PRL065.A29;
                    objPRL065.A210 = PRL065.A210;
                    objPRL065.A211 = PRL065.A211;
                    objPRL065.A212 = PRL065.A212;

                    objPRL065.B11 = PRL065.B11;
                    objPRL065.B12 = PRL065.B12;
                    objPRL065.B13 = PRL065.B13;
                    objPRL065.B14 = PRL065.B14;
                    objPRL065.B15 = PRL065.B15;
                    objPRL065.B16 = PRL065.B16;
                    objPRL065.B17 = PRL065.B17;
                    objPRL065.B18 = PRL065.B18;
                    objPRL065.B19 = PRL065.B19;
                    objPRL065.B110 = PRL065.B110;
                    objPRL065.B111 = PRL065.B111;
                    objPRL065.B112 = PRL065.B112;

                    objPRL065.B21 = PRL065.B21;
                    objPRL065.B22 = PRL065.B22;
                    objPRL065.B23 = PRL065.B23;
                    objPRL065.B24 = PRL065.B24;
                    objPRL065.B25 = PRL065.B25;
                    objPRL065.B26 = PRL065.B26;
                    objPRL065.B27 = PRL065.B27;
                    objPRL065.B28 = PRL065.B28;
                    objPRL065.B29 = PRL065.B29;
                    objPRL065.B210 = PRL065.B210;
                    objPRL065.B211 = PRL065.B211;
                    objPRL065.B212 = PRL065.B212;

                    objPRL065.C11 = PRL065.C11;
                    objPRL065.C12 = PRL065.C12;
                    objPRL065.C13 = PRL065.C13;
                    objPRL065.C14 = PRL065.C14;
                    objPRL065.C15 = PRL065.C15;
                    objPRL065.C16 = PRL065.C16;
                    objPRL065.C17 = PRL065.C17;
                    objPRL065.C18 = PRL065.C18;
                    objPRL065.C19 = PRL065.C19;
                    objPRL065.C110 = PRL065.C110;
                    objPRL065.C111 = PRL065.C111;
                    objPRL065.C112 = PRL065.C112;

                    objPRL065.C21 = PRL065.C21;
                    objPRL065.C22 = PRL065.C22;
                    objPRL065.C23 = PRL065.C23;
                    objPRL065.C24 = PRL065.C24;
                    objPRL065.C25 = PRL065.C25;
                    objPRL065.C26 = PRL065.C26;
                    objPRL065.C27 = PRL065.C27;
                    objPRL065.C28 = PRL065.C28;
                    objPRL065.C29 = PRL065.C29;
                    objPRL065.C210 = PRL065.C210;
                    objPRL065.C211 = PRL065.C211;
                    objPRL065.C212 = PRL065.C212;

                    objPRL065.Remark2 = PRL065.Remark2;
                    objPRL065.Remark3 = PRL065.Remark3;
                    objPRL065.Remark4 = PRL065.Remark4;
                    objPRL065.Remark5 = PRL065.Remark5;
                    objPRL065.Remark5_1 = PRL065.Remark5_1.HasValue ? Manager.getDateTime(PRL065.Remark5_1.Value.ToShortDateString()) : PRL065.Remark5_1;

                    objPRL065.Remark6 = PRL065.Remark6;
               

                    objPRL065.EditedBy = objClsLoginInfo.UserName;
                    objPRL065.EditedOn = DateTime.Now;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol saved successfully";
                    objResponseMsg.HeaderId = objPRL065.HeaderId;
                    #endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteProtocolHeaderDataLinkage(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                int? refHeaderId = Manager.GetIntegerValue(Convert.ToString(fc["MainHeaderId"]));
                if (refHeaderId.HasValue && refHeaderId > 0)
                {
                    PRL065 objPRL065 = db.PRL065.Where(c => c.HeaderId == refHeaderId).FirstOrDefault();
                    if (objPRL065 != null && string.IsNullOrWhiteSpace(objPRL065.ProtocolNo))
                    {
                        db.PRL065.Remove(objPRL065);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Protocol deleted successfully";
                        objResponseMsg.HeaderId = objPRL065.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion


        #region Common function
        public UserRoleAccessDetails GetUserAccessRights()
        {
            UserRoleAccessDetails objUserRoleAccessDetails = new UserRoleAccessDetails();

            try
            {
                var role = (from a in db.ATH001
                            join b in db.ATH004 on a.Role equals b.Id
                            where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

                if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.PROD3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else if (role.Where(i => (i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), StringComparison.OrdinalIgnoreCase) || i.RoleDesc == clsImplementationEnum.UserRoleName.QC2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI2.GetStringValue() || i.RoleDesc == clsImplementationEnum.UserRoleName.QI3.GetStringValue())).ToList().Any())
                {
                    objUserRoleAccessDetails.UserRole = clsImplementationEnum.UserRoleName.QC3.GetStringValue();
                    objUserRoleAccessDetails.UserDesignation = clsImplementationEnum.UserAccessRole.Initiator.GetStringValue();
                }
                else
                {
                    objUserRoleAccessDetails.UserRole = string.Empty;
                    objUserRoleAccessDetails.UserDesignation = string.Empty;
                }

            }
            catch
            {

            }
            return objUserRoleAccessDetails;
        }

        #endregion

    }
}



