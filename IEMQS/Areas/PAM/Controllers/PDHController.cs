﻿using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.PAM.Controllers
{
    public class PDHController : clsBase
    {

        #region Utility
        public static string GenerateTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxLength = "", bool disabled = false)
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id=" + inputID + " data-lineid='" + rowId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " maxlength='" + maxLength + "' " + (disabled ? "disabled" : "") + "  />";

            return htmlControl;
        }

        public static string GenerateGridButton(int rowId, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";


            htmlControl = "<a id='" + inputID + "' name='" + inputID + "' class='btn btn-outline btn-circle btn-sm blue' " + onClickEvent + " ><i class='" + className + "'></i> " + buttonName + "</a>";

            //htmlControl = "<i  data-modal='' id='" + inputID + "' name='" + inputID + "' style='cursor:Pointer;' Title='" + buttonTooltip + "' class='" + className + "' ></i>";

            return htmlControl;
        }

        public string GetChecklistCheckStyle(int LineId, string status, bool? isCheck, bool? isDisabled)
        {
            string rtnchecjbox = "";
            if (isCheck == null)
                isCheck = false;
            rtnchecjbox = "<input type='checkbox' style='text-align:center' name='chkChecklistCheck" + LineId.ToString() + "' id='chkChecklistCheck" + LineId.ToString() + "' class='make-switch col-md-3' data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' onchange='UpdateAccepted(this," + LineId.ToString() + ")' ";
            if (isDisabled == true)
            {
                rtnchecjbox += " disabled= 'disabled' ";
            }
            if (isCheck == true)
            {
                rtnchecjbox += " checked='checked' ";
            }
            //if (status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
            //{
            //    if (isCheck == true)
            //    {
            //        rtnchecjbox += " checked='checked' ";
            //    }
            //}
            //else
            //{
            //    rtnchecjbox += " disabled= 'disabled' ";
            //}
            rtnchecjbox += "/>";
            return rtnchecjbox;
        }

        public string GetSectionRemarkStyle(int LineId, string status, string sectionRemarks)
        {
            string rtnremarks = "";
            rtnremarks += "<input type='text' id='txtSectionRemark" + LineId.ToString() + "' name='txtSectionRemark" + LineId.ToString() + "' style='width:100%'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' onblur='UpdateRemarks(this," + LineId.ToString() + ")' ";
            if (status.ToLower() != clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
            {
                rtnremarks += " disabled='disabled' ";
            }
            rtnremarks += " />";
            return rtnremarks;
        }

        public JsonResult GetProductionCenter()
        {
            try
            {
                var lstProdCenter = clsImplementationEnum.GetProdCenter().Select(x => new { id = x.ToString(), text = x.ToString() }).ToList();
                //var lstBUs = (from a in db.COM002
                //              where a.t_dtyp == 2 && a.t_dimx != ""
                //              select new { a.t_dimx, Desc = a.t_desc }).ToList();

                //var items = (from li in lstBUs
                //             select new
                //             {
                //                 id = li.t_dimx,
                //                 text = li.Desc,
                //             }).ToList();

                return Json(lstProdCenter, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion




        // GET: PAM/PDH



        public ActionResult Index(string Project)
        {
            ViewBag.chkProject = Project;
            return View();
        }
        public ActionResult GetPDHGridDataPartial(string status, string ForApprove)
        {
            ViewBag.Status = status;
            ViewBag.ForApprove = ForApprove;
            return PartialView("_GetPDHGridDataPartial");
        }

        public ActionResult loadPDHDataTable(JQueryDataTableParamModel param, string status, string ForApprove, string Project)
        {
            try
            {
                if (Project != null && Project != "")
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                    string whereCondition = "1=1";
                    //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";
                    if (Convert.ToBoolean(ForApprove))
                    {
                        if (status.ToUpper() == "PENDING")
                        {
                            whereCondition += " and p9.status in ('" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                        }
                        else
                        {
                            whereCondition += " and p9.status in ('" + clsImplementationEnum.PAMStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.IMBStatus.Returned.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                        }
                    }
                    else
                    {
                        if (status.ToUpper() == "PENDING")
                        {
                            whereCondition += " and p9.status in ('" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Returned.GetStringValue() + "')";
                        }
                        else
                        {
                            whereCondition += " and p9.status in ('" + clsImplementationEnum.PAMStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.IMBStatus.Returned.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                        }
                    }
                    string[] columnName = { "p9.ProposalDesign", "ProposalDesignRev", "cont.t_cono", "cont.t_desc", "cont.t_ofbp", "cust.t_nama", "p1.LOINo", "p1.LOIDate", "p1.CustomerOrderNo", "p1.CustomerOrderDate", "p1.CDD", "p9.status" };

                    // whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    if (!string.IsNullOrEmpty(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }
                    whereCondition += " and g1.Project='" + Project.ToString() + "'";

                    var lstPam = db.SP_PAM_PDH_GETPDHDetails(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                    lstPam = lstPam.Where(x => x.Project.Contains(Project)).ToList();
                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = from h in lstPam
                              select new[] {
                              h.PDHId.ToString(),
                               h.HeaderId.ToString(),
                               h.ProposalDesign.ToString(),
                               h.Status.ToString(),
                               h.ConDesc.ToString(),
                               h.CustomerDesc.ToString(),
                               h.LOINo.ToString(),
                               h.LOIDate!=null?h.LOIDate.Value.ToString("dd/MM/yyyy"):string.Empty,
                               h.CustomerOrderNo.ToString(),
                               h.CustomerOrderDate!=null?h.CustomerOrderDate.Value.ToString("dd/MM/yyyy"):string.Empty,
                               h.CDD!=null?h.CDD.Value.ToString("dd/MM/yyyy"):string.Empty,
                               h.ProposalDesignRev.ToString(),
                               "" //Action
                    };
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = strSortOrder,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                    var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                    string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                    string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                    int? StartIndex = param.iDisplayStart + 1;
                    int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                    string whereCondition = "1=1";
                    //whereCondition += " and loidate.t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";
                    if (Convert.ToBoolean(ForApprove))
                    {
                        if (status.ToUpper() == "PENDING")
                        {
                            whereCondition += " and p9.status in ('" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                        }
                        else
                        {
                            whereCondition += " and p9.status in ('" + clsImplementationEnum.PAMStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.IMBStatus.Returned.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                        }
                    }
                    else
                    {
                        if (status.ToUpper() == "PENDING")
                        {
                            whereCondition += " and p9.status in ('" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Returned.GetStringValue() + "')";
                        }
                        else
                        {
                            whereCondition += " and p9.status in ('" + clsImplementationEnum.PAMStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.IMBStatus.Returned.GetStringValue() + "','" + clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue() + "')";
                        }
                    }
                    string[] columnName = { "p9.ProposalDesign", "ProposalDesignRev", "cont.t_cono", "cont.t_desc", "cont.t_ofbp", "cust.t_nama", "p1.LOINo", "p1.LOIDate", "p1.CustomerOrderNo", "p1.CustomerOrderDate", "p1.CDD", "p9.status" };

                    // whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    if (!string.IsNullOrEmpty(param.sSearch))
                    {
                        whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                    }
                    else
                    {
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                    }

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }

                    var lstPam = db.SP_PAM_PDH_GETPDHDetails(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                    int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                    var res = from h in lstPam
                              select new[] {
                              h.PDHId.ToString(),
                               h.HeaderId.ToString(),
                               h.ProposalDesign.ToString(),
                               h.Status.ToString(),
                               h.ConDesc.ToString(),
                               h.CustomerDesc.ToString(),
                               h.LOINo.ToString(),
                               h.LOIDate!=null?h.LOIDate.Value.ToString("dd/MM/yyyy"):string.Empty,
                               h.CustomerOrderNo.ToString(),
                               h.CustomerOrderDate!=null?h.CustomerOrderDate.Value.ToString("dd/MM/yyyy"):string.Empty,
                               h.CDD!=null?h.CDD.Value.ToString("dd/MM/yyyy"):string.Empty,
                               h.ProposalDesignRev.ToString(),
                               "" //Action
                    };
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                        whereCondition = whereCondition,
                        strSortOrder = strSortOrder,
                    }, JsonRequestBehavior.AllowGet);
                }
                
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.TimelineTitle = "Proposal Handover";
            model.Title = "PAM-PH";
            PAM009 objPAM009 = db.PAM009.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            model.CreatedBy = Manager.GetUserNameFromPsNo(objPAM009.CreatedBy);
            model.CreatedOn = objPAM009.CreatedOn;
            model.SubmittedBy = Manager.GetUserNameFromPsNo(objPAM009.SubmittedBy);
            model.SubmittedOn = objPAM009.SubmittedOn;
            model.ReturnedBy = Manager.GetUserNameFromPsNo(objPAM009.ReturnedBy);
            model.ReturnedOn = objPAM009.ReturnedOn;
            model.ApprovedBy = Manager.GetUserNameFromPsNo(objPAM009.ApprovedBy);
            model.ApprovedOn = objPAM009.ApprovedOn;

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult GetPDHHeaderDetails(int PDHId, int HeaderId)
        {
            PDHHeaderDtl header = new PDHHeaderDtl();
            if (HeaderId > 0)
            {
                header.PDHId = PDHId.ToString();
                header.HeaderId = HeaderId.ToString();

                PAM001 objPAM001 = db.PAM001.FirstOrDefault(x => x.HeaderId == HeaderId);
                header.PAMNo = objPAM001.PAM;
                header.Contract = db.Database.SqlQuery<string>(@"SELECT t_cono +'-'+ t_desc    FROM " + LNLinkedServer + ".dbo.ttpctm100175 where t_cono = '" + objPAM001.ContractNo + "'").FirstOrDefault();
                header.Customer = db.Database.SqlQuery<string>(@"SELECT  t_bpid +'-'+ t_nama   from " + LNLinkedServer + ".dbo.ttccom100175  where t_bpid = '" + objPAM001.Customer + "'").FirstOrDefault();
                header.ContractNo = objPAM001.ContractNo;
                if (PDHId == 0)
                {
                    var objPAM009 = new PAM009();
                    header.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                    header.ProposalDesign = MakeProposalDesign(header.ContractNo);
                    header.ProposalDesignRev = "0";
                    header.PDHId = "0";
                    header.HeaderId = HeaderId.ToString();
                }
                else
                {
                    var objPAM009 = db.PAM009.FirstOrDefault(x => x.PDHId == PDHId); //Change If any Primary Key Add
                    header.Status = objPAM009.Status;
                    if (PDHId == 0)
                    {
                        header.ProposalDesign = MakeProposalDesign(objPAM001.ContractNo);
                    }
                    else
                    {
                        header.ProposalDesign = objPAM009.ProposalDesign;
                    }
                    header.ProposalDesignRev = objPAM009.ProposalDesignRev.ToString();
                    header.CreatedOn = objPAM009.CreatedOn.ToString();
                    header.ReturnRemarks = objPAM009.ReturnRemark;
                    header.HPC1 = objPAM009.HPC1;
                    header.HPC3 = objPAM009.HPC3;
                    header.Combination = objPAM009.Combination;
                    header.ReferPlanningInput = objPAM009.ReferPlanningInput;
                    header.RollingCapacityNotAplicate = objPAM009.RollingCapacityNotAplicate;
                    header.ManufacturingCode = objPAM009.ManufacturingCode;
                    header.CodeEdition = objPAM009.CodeEdition;
                    header.ForgedShell = objPAM009.ForgedShell;
                    header.ASMEYes = objPAM009.ASMEYes;
                    header.ASMENo = objPAM009.ASMENo;
                    header.ShellConstructionMonowall = objPAM009.ShellConstructionMonowall;
                    header.ShellConstructionMultiwall = objPAM009.ShellConstructionMultiwall;
                    header.IBR = objPAM009.IBR;
                    header.PEDATEX = objPAM009.PEDATEX;
                    header.CRN = objPAM009.CRN;
                    header.DOSH = objPAM009.DOSH;
                    header.SELO = objPAM009.SELO;
                    header.GOST = objPAM009.GOST;
                    header.LocalRegulationOthers = objPAM009.LocalRegulationOthers;
                    header.Warm = objPAM009.Warm;
                    header.Cold = objPAM009.Cold;
                    header.Hot = objPAM009.Hot;
                    header.ShellRollingNotApplicable = objPAM009.ShellRollingNotApplicable;
                    header.ForgedShellWithProjection = objPAM009.ForgedShellWithProjection;
                    header.ForgedShellWithIntegralNub = objPAM009.ForgedShellWithIntegralNub;
                    header.ThkOnPlate = objPAM009.ThkOnPlate;
                    header.NoInsideProjection = objPAM009.NoInsideProjection;

                    header.NubShellNotApplicable = objPAM009.NubShellNotApplicable;
                    header.SinglePiece = objPAM009.SinglePiece;
                    header.Piece2 = objPAM009.Piece2;
                    header.Piece3 = objPAM009.Piece3;
                    header.TubeSheetNotApplicable = objPAM009.TubeSheetNotApplicable;
                    header.TubeSheetOthers = objPAM009.TubeSheetOthers;
                    header.SingleLayer = objPAM009.SingleLayer;
                    header.DoubleLayer = objPAM009.DoubleLayer;
                    header.Clad = objPAM009.Clad;
                    header.RefractoryAtShop = objPAM009.RefractoryAtShop;
                    header.RefractoryAtSite = objPAM009.RefractoryAtSite;
                    header.Liner = objPAM009.Liner;
                    header.OverlayNotApplicable = objPAM009.OverlayNotApplicable;
                    header.Part4 = objPAM009.Part4;
                    header.Part5 = objPAM009.Part5;
                    header.CatalystNotApplicable = objPAM009.CatalystNotApplicable;
                    header.SinglePiece2 = objPAM009.SinglePiece2;
                    header.Half2 = objPAM009.Half2;
                    header.Petals4 = objPAM009.Petals4;
                    header.Petals6 = objPAM009.Petals6;
                    header.HeadConstructionOthers = objPAM009.HeadConstructionOthers;
                    header.WithCrown = objPAM009.WithCrown;
                    header.WithoutCrown = objPAM009.WithoutCrown;
                    header.HeadConstructionCold = objPAM009.HeadConstructionCold;
                    header.HeadConstructionWarm = objPAM009.HeadConstructionWarm;
                    header.HeadConstructionHot = objPAM009.HeadConstructionHot;
                    header.FormingInIndia = objPAM009.FormingInIndia;
                    header.ReadyHeadImported = objPAM009.ReadyHeadImported;
                    header.LSRYes = objPAM009.LSRYes;
                    header.LSRNo = objPAM009.LSRNo;
                    header.InternalYes = objPAM009.InternalYes;
                    header.InternalNo = objPAM009.InternalNo;
                    header.SiteInstallationScope = objPAM009.SiteInstallationScope;
                    header.ThirdPartyInspectionIncludedYes = objPAM009.ThirdPartyInspectionIncludedYes;
                    header.ThirdPartyInspectionIncludedNo = objPAM009.ThirdPartyInspectionIncludedNo;
                    header.ProjectCostEstimationSheetInLNYes = objPAM009.ProjectCostEstimationSheetInLNYes;
                    header.ProjectCostEstimationSheetOnLNNo = objPAM009.ProjectCostEstimationSheetOnLNNo;
                    header.ProjectCostEstimationSheetOnLNRemark = objPAM009.ProjectCostEstimationSheetOnLNRemark;
                    header.DeliveryCommitted = objPAM009.DeliveryCommitted;
                    header.OtherPoints = Convert.ToString(objPAM009.OtherPoints);

                }
            }
            else
            {
                header.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                header.PDHId = PDHId.ToString();
                header.PAMNo = "";
            }
            return PartialView("_PDHHeaderDetails", header);
        }
        public ActionResult GetPDHDetails(int PDHId, int HeaderId)
        {

            var objPAM009 = db.PAM009.FirstOrDefault(x => x.PDHId == PDHId);
            //Change If any Primary Key Add
            PAM009 PAMOO9 = db.PAM009.FirstOrDefault(x => x.PDHId == PDHId);

            PAMOO9.ProposalDesignRev = Convert.ToInt32(objPAM009.ProposalDesignRev);

            // header.ReturnRemarks = objPAM009.ReturnRemark;
            PAMOO9.HPC1 = objPAM009.HPC1;
            PAMOO9.HPC3 = objPAM009.HPC3;
            PAMOO9.Combination = objPAM009.Combination;
            PAMOO9.ReferPlanningInput = objPAM009.ReferPlanningInput;
            PAMOO9.ForgedShell = objPAM009.ForgedShell;
            PAMOO9.RollingCapacityNotAplicate = objPAM009.RollingCapacityNotAplicate;
            PAMOO9.ManufacturingCode = objPAM009.ManufacturingCode;
            PAMOO9.CodeEdition = objPAM009.CodeEdition;
            PAMOO9.ASMEYes = objPAM009.ASMEYes;
            PAMOO9.ASMENo = objPAM009.ASMENo;
            PAMOO9.ShellConstructionMonowall = objPAM009.ShellConstructionMonowall;
            PAMOO9.ShellConstructionMultiwall = objPAM009.ShellConstructionMultiwall;
            PAMOO9.IBR = objPAM009.IBR;
            PAMOO9.PEDATEX = objPAM009.PEDATEX;
            PAMOO9.CRN = objPAM009.CRN;
            PAMOO9.DOSH = objPAM009.DOSH;
            PAMOO9.SELO = objPAM009.SELO;
            PAMOO9.GOST = objPAM009.GOST;
            PAMOO9.LocalRegulationOthers = objPAM009.LocalRegulationOthers;
            PAMOO9.Warm = objPAM009.Warm;
            PAMOO9.Cold = objPAM009.Cold;
            PAMOO9.Hot = objPAM009.Hot;
            PAMOO9.ShellRollingNotApplicable = objPAM009.ShellRollingNotApplicable;
            PAMOO9.ForgedShellWithProjection = objPAM009.ForgedShellWithProjection;
            PAMOO9.ForgedShellWithIntegralNub = objPAM009.ForgedShellWithIntegralNub;
            PAMOO9.ThkOnPlate = objPAM009.ThkOnPlate;
            PAMOO9.NoInsideProjection = objPAM009.NoInsideProjection;

            PAMOO9.NubShellNotApplicable = objPAM009.NubShellNotApplicable;
            PAMOO9.SinglePiece = objPAM009.SinglePiece;
            PAMOO9.Piece2 = objPAM009.Piece2;
            PAMOO9.Piece3 = objPAM009.Piece3;
            PAMOO9.TubeSheetNotApplicable = objPAM009.TubeSheetNotApplicable;
            PAMOO9.TubeSheetOthers = objPAM009.TubeSheetOthers;
            PAMOO9.SingleLayer = objPAM009.SingleLayer;
            PAMOO9.DoubleLayer = objPAM009.DoubleLayer;
            PAMOO9.Clad = objPAM009.Clad;
            PAMOO9.RefractoryAtShop = objPAM009.RefractoryAtShop;
            PAMOO9.RefractoryAtSite = objPAM009.RefractoryAtSite;
            PAMOO9.Liner = objPAM009.Liner;
            PAMOO9.OverlayNotApplicable = objPAM009.OverlayNotApplicable;
            PAMOO9.Part4 = objPAM009.Part4;
            PAMOO9.Part5 = objPAM009.Part5;
            PAMOO9.CatalystNotApplicable = objPAM009.CatalystNotApplicable;
            PAMOO9.SinglePiece2 = objPAM009.SinglePiece2;
            PAMOO9.Half2 = objPAM009.Half2;
            PAMOO9.Petals4 = objPAM009.Petals4;
            PAMOO9.Petals6 = objPAM009.Petals6;
            PAMOO9.HeadConstructionOthers = objPAM009.HeadConstructionOthers;
            PAMOO9.WithCrown = objPAM009.WithCrown;
            PAMOO9.WithoutCrown = objPAM009.WithoutCrown;
            PAMOO9.HeadConstructionCold = objPAM009.HeadConstructionCold;
            PAMOO9.HeadConstructionWarm = objPAM009.HeadConstructionWarm;
            PAMOO9.HeadConstructionHot = objPAM009.HeadConstructionHot;
            PAMOO9.FormingInIndia = objPAM009.FormingInIndia;
            PAMOO9.ReadyHeadImported = objPAM009.ReadyHeadImported;
            PAMOO9.LSRYes = objPAM009.LSRYes;
            PAMOO9.LSRNo = objPAM009.LSRNo;
            PAMOO9.InternalYes = objPAM009.InternalYes;
            PAMOO9.InternalNo = objPAM009.InternalNo;
            PAMOO9.SiteInstallationScope = objPAM009.SiteInstallationScope;
            PAMOO9.ThirdPartyInspectionIncludedYes = objPAM009.ThirdPartyInspectionIncludedYes;
            PAMOO9.ThirdPartyInspectionIncludedNo = objPAM009.ThirdPartyInspectionIncludedNo;
            PAMOO9.ProjectCostEstimationSheetInLNYes = objPAM009.ProjectCostEstimationSheetInLNYes;
            PAMOO9.ProjectCostEstimationSheetOnLNNo = objPAM009.ProjectCostEstimationSheetOnLNNo;
            PAMOO9.ProjectCostEstimationSheetOnLNRemark = objPAM009.ProjectCostEstimationSheetOnLNRemark;
            PAMOO9.DeliveryCommitted = objPAM009.DeliveryCommitted;
            PAMOO9.OtherPoints = Convert.ToString(objPAM009.OtherPoints);
            ViewBag.ForApprove = "false";
            ViewBag.PDHId = PDHId;

            return PartialView("_GetChecklistGridDataPartial", PAMOO9);
        }
        [SessionExpireFilter]
        public ActionResult AddPDH(int PDHId = 0, string Project="")
        {
            var objPAM009 = db.PAM009.Where(x => x.PDHId == PDHId).FirstOrDefault();
            if (objPAM009 == null)
            {
                objPAM009 = new PAM009();
                objPAM009.PDHId = 0;
                objPAM009.Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                objPAM009.HeaderId = 0;
            }
            Session["pdh"] = Convert.ToInt32(PDHId);

            string userid = (objClsLoginInfo.UserName);
            if (objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.ENGG3.GetStringValue()))
            {
                ViewBag.role = clsImplementationEnum.UserRoleName.ENGG3.GetStringValue();
            }
            else
            {
                ViewBag.role = "others";
            }
            if (objClsLoginInfo.GetUserRoleList().Contains(clsImplementationEnum.UserRoleName.EST3.GetStringValue()))
            {
                ViewBag.enginner = clsImplementationEnum.UserRoleName.EST3.GetStringValue();
            }
            else
            {
                ViewBag.enginner = "others";
            }

            ViewBag.IsApproval = false;
            ViewBag.chkProject = Project;
            return View(objPAM009);
        }

        public string MakeProposalDesign(string ContractNo)
        {
            var HandoverReport = string.Empty;
            var mtNumber = "1";
            var objPAM008 = db.PAM008.Where(i => i.HandoverReport.Contains(ContractNo + "/PDH")).ToList();
            if (objPAM008.Count > 0)
            {
                int MaxNo = 0;
                foreach (var item in objPAM008)
                {
                    string SplitId = item.HandoverReport.Split('/').Last();
                    if (MaxNo < Convert.ToInt32(SplitId))
                    {
                        MaxNo = Convert.ToInt32(SplitId);
                    }
                }
                mtNumber = (MaxNo + 1).ToString();
            }
            HandoverReport = ContractNo + "/PDH/" + mtNumber.PadLeft(3, '0');
            //HandoverReport = ContractNo + "/PDH/" + mtNumber.PadLeft(1, '0');
            return HandoverReport;
        }

        public ActionResult loadPDHLineDataTable(JQueryDataTableParamModel param, int DisId, string ForApprove)
        {
            int SrNo = 0;
            try
            {
                PAM009 objPAM009 = db.PAM009.Where(i => i.PDHId == DisId).FirstOrDefault();
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                bool isEditEnabled = true;
                string whereCondition = "1=1";
                whereCondition += " and PDHId= " + DisId.ToString();

                if (objPAM009.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower() ||
                    objPAM009.Status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
                {
                    isEditEnabled = false;
                }

                if (objPAM009.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower())
                {
                    whereCondition += " and IsAccepted = 1";
                }

                string[] columnName = { "Description", "DocNo", "DocRev", "Pages" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstPam = db.SP_PDH_GETPDHLINEDETAILS(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                if (ForApprove == null || ForApprove.ToLower() == "false")
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    "0",
                                    Helper.GenerateHidden(newRecordId, "HeaderId", Convert.ToString(DisId)),
                                    (SrNo).ToString(),
                                    GenerateTextboxFor(newRecordId,"Description","","","",false,"","100",false),
                                    GenerateTextboxFor(newRecordId,"DocNo","","","",false,"","15",false),
                                    GenerateTextboxFor(newRecordId,"DocRev","R1","","",false,"","5",false),
                                    GenerateTextboxFor(newRecordId,"Pages","","","",false,"","15",false),
                                    "",
                                    //objPAM009.Status != clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue().ToLower() ? GetChecklistCheckStyle(newRecordId,objPAM009.Status,true, false) : GetChecklistCheckStyle(newRecordId,objPAM009.Status,true, true),
                                    //GenerateTextboxFor(newRecordId,"Remarks","","","",false,"","500",false),
                                    GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveLineRecord(0);" ),
                                };

                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                    (++SrNo).ToString(),
                                    GenerateTextboxFor(h.LineId,"Description",h.Description,"","",false,"","100",false),
                                    GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","15",false),
                                    GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","5",false),
                                    GenerateTextboxFor(h.LineId,"Pages",h.Pages,"","",false,"","15",false),
                                    "",
                                    //objPAM009.Status != clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue().ToLower() ? GetChecklistCheckStyle(h.LineId,objPAM009.Status,h.IsAccepted, false) : GetChecklistCheckStyle(h.LineId,objPAM009.Status,h.IsAccepted, true),
                                    //GenerateTextboxFor(h.LineId,"Remarks",h.Remarks,"","",false,"","500",false),
                                    isEditEnabled ? GenerateGridButton(newRecordId, "Edit", "Edit Rocord", "fa fa-plus", "SaveLineRecord("+h.LineId+");" ) : "",
                                    }).ToList();

                    res.Insert(0, newRecord);
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "HeaderId", Convert.ToString(h.HeaderId)),
                                     (++SrNo).ToString(),
                                    GenerateTextboxFor(h.LineId,"Description",h.Description,"","",false,"","100",true),
                                    GenerateTextboxFor(h.LineId,"DocNo",h.DocNo,"","",false,"","15",true),
                                    GenerateTextboxFor(h.LineId,"DocRev",h.DocRev,"","",false,"","5",true),
                                    GenerateTextboxFor(h.LineId,"Pages",h.Pages,"","",false,"","15",true),
                                    "",
                                    GetChecklistCheckStyle(h.LineId,objPAM009.Status,h.IsAccepted, false),
                                    h.IsAccepted ? GenerateTextboxFor(h.LineId,"ReturnRemarks",h.Remarks,"UpdateRemark("+h.LineId+")","",true,"","500", false) : GenerateTextboxFor(h.LineId,"ReturnRemarks",h.Remarks,"UpdateRemark("+h.LineId+")","",false,"","500", false),

                    }).ToList();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult loadSupplierDataTable(JQueryDataTableParamModel param, int DisId, string ForApprove)
        {
            try
            {
                PAM009 objPAM009 = db.PAM009.Where(i => i.PDHId == DisId).FirstOrDefault();
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                bool isEditEnabled = true;
                string whereCondition = "1=1";
                whereCondition += " and PDHId= " + DisId.ToString();

                if (objPAM009.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower() ||
                    objPAM009.Status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
                {
                    isEditEnabled = false;
                }

                string[] columnName = { "Plates_supplier", "Forged_shell_supplier", "Tube_sheet_supplier" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstPam = db.SP_PDH_GET_SUPPLIER_DETAILS(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                if (ForApprove == null || ForApprove.ToLower() == "false")
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    "0",
                                    Helper.GenerateHidden(newRecordId, "PDHId", Convert.ToString(DisId)),
                                    GenerateTextboxFor(newRecordId,"Plates","","","",false,"","30",false),
                                    GenerateTextboxFor(newRecordId,"ForgedShell","","","",false,"","30",false),
                                    GenerateTextboxFor(newRecordId,"TubeSheet","","","",false,"","30",false),
                                    GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveSupplierRecord(0);" ),
                                };

                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "PDHId", Convert.ToString(h.PDHId)),
                                    GenerateTextboxFor(h.LineId,"Plates",h.Plates,"","",false,"","30",false),
                                    GenerateTextboxFor(h.LineId,"ForgedShell",h.ForgedShell,"","",false,"","30",false),
                                    GenerateTextboxFor(h.LineId,"TubeSheet",h.TubeSheet,"","",false,"","30",false),
                                    isEditEnabled ?GenerateGridButton(newRecordId, "Edit", "Edit Rocord", "fa fa-plus", "SaveSupplierRecord("+h.LineId+");" )  + GenerateGridButton(newRecordId, "Delete", "Delete Rocord", "fa fa-trash", "DeleteSupplierRecord("+h.LineId+");" ) : "",
                                    }).ToList();

                    res.Insert(0, newRecord);
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "PDHId", Convert.ToString(h.PDHId)),
                                    Convert.ToString(h.Plates),
                                    Convert.ToString(h.ForgedShell),
                                    Convert.ToString(h.TubeSheet),
                                    ""

                    }).ToList();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveSupplier(FormCollection fc, int Id = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int LineId = Id;
                // int HeaderId = Convert.ToInt32(fc["PDHId" + Id]);
                int PDHID = Convert.ToInt32(Session["pdh"]);
                PAM009 objPAM009 = db.PAM009.Where(x => x.PDHId == PDHID).FirstOrDefault();

                PAM019 objPAM019 = null;
                if (LineId > 0)
                {
                    objPAM019 = db.PAM019.Where(x => x.LineId == LineId).FirstOrDefault();
                }
                else
                {
                    objPAM019 = new PAM019();
                }
                objPAM019.PDHId = PDHID;
                objPAM019.Plates_supplier = fc["Plates" + LineId];
                objPAM019.Forged_shell_supplier = fc["ForgedShell" + LineId];
                objPAM019.Tube_sheet_supplier = fc["TubeSheet" + LineId];

                if (LineId > 0)
                {
                    objPAM019.EditedBy = objClsLoginInfo.UserName;
                    objPAM019.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {
                    objPAM019.CreatedBy = objClsLoginInfo.UserName;
                    objPAM019.CreatedOn = DateTime.Now;
                    db.PAM019.Add(objPAM019);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteSupplier(int LineId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objPAM019 = db.PAM019.FirstOrDefault(f => f.LineId == LineId);

                if (objPAM019 != null)
                {
                    db.PAM019.Remove(objPAM019);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Deleted Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found or already deleted";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult loadOfferedSizeQtyDataTable(JQueryDataTableParamModel param, int DisId, string ForApprove)
        {
            try
            {
                PAM009 objPAM009 = db.PAM009.Where(i => i.PDHId == DisId).FirstOrDefault();
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                bool isEditEnabled = true;
                string whereCondition = "1=1";
                whereCondition += " and PDHId= " + DisId.ToString();

                if (objPAM009.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower() ||
                    objPAM009.Status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
                {
                    isEditEnabled = false;
                }

                string[] columnName = { "Plates_Offeredsize_Qty", "Forged_shell_Offeredsize_Qty", "Tube_sheet_shell_Offeredsize_Qty" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstPam = db.SP_PDH_GET_OFFERED_SIZE_QTY_DETAILS(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                if (ForApprove == null || ForApprove.ToLower() == "false")
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    "0",
                                    Helper.GenerateHidden(newRecordId, "PDHId", Convert.ToString(DisId)),
                                    GenerateTextboxFor(newRecordId,"Plates","","","",false,"","1000",false),
                                    GenerateTextboxFor(newRecordId,"ForgedShell","","","",false,"","1000",false),
                                    GenerateTextboxFor(newRecordId,"TubeSheet","","","",false,"","1000",false),
                                    GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveOfferedSizeQtyRecord(0);" ),
                                };

                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "PDHId", Convert.ToString(h.PDHId)),
                                    GenerateTextboxFor(h.LineId,"Plates",h.Plates+"","","",false,"","1000",false),
                                    GenerateTextboxFor(h.LineId,"ForgedShell",h.ForgedShell+"","","",false,"","1000",false),
                                    GenerateTextboxFor(h.LineId,"TubeSheet",h.TubeSheet+"","","",false,"","1000",false),
                                    isEditEnabled ?GenerateGridButton(newRecordId, "Edit", "Edit Rocord", "fa fa-plus", "SaveOfferedSizeQtyRecord("+h.LineId+");" )  + GenerateGridButton(newRecordId, "Delete", "Delete Rocord", "fa fa-trash", "DeleteOfferedSizeQtyRecord("+h.LineId+");" ) : "",
                                    }).ToList();

                    res.Insert(0, newRecord);
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "PDHId", Convert.ToString(h.PDHId)),
                                    Convert.ToString(h.Plates),
                                    Convert.ToString(h.ForgedShell),
                                    Convert.ToString(h.TubeSheet),
                                    ""

                    }).ToList();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveOfferedSizeQty(FormCollection fc, int Id = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int LineId = Id;
                // int HeaderId = Convert.ToInt32(fc["PDHId" + Id]);
                int PDHID = Convert.ToInt32(Session["pdh"]);
                PAM009 objPAM009 = db.PAM009.Where(x => x.PDHId == PDHID).FirstOrDefault();

                PAM020 objPAM020 = null;
                if (LineId > 0)
                {
                    objPAM020 = db.PAM020.Where(x => x.LineId == LineId).FirstOrDefault();
                }
                else
                {
                    objPAM020 = new PAM020();
                }
                objPAM020.PDHId = PDHID;

                //int tempIntVal = 0;
                //if (string.IsNullOrWhiteSpace(fc["Plates" + LineId]))
                //    objPAM020.Plates_Offeredsize_Qty = null;
                //else if (int.TryParse(fc["Plates" + LineId], out tempIntVal))
                //    objPAM020.Plates_Offeredsize_Qty = tempIntVal;
                //if (string.IsNullOrWhiteSpace(fc["ForgedShell" + LineId]))
                //    objPAM020.Forged_shell_Offeredsize_Qty = null;
                //else if (int.TryParse(fc["ForgedShell" + LineId], out tempIntVal))
                //    objPAM020.Forged_shell_Offeredsize_Qty = tempIntVal;
                //if (string.IsNullOrWhiteSpace(fc["TubeSheet" + LineId]))
                //    objPAM020.Tube_sheet_shell_Offeredsize_Qty = null;
                //else if (int.TryParse(fc["TubeSheet" + LineId], out tempIntVal))
                //    objPAM020.Tube_sheet_shell_Offeredsize_Qty = tempIntVal;

                //Observation 15445 on 03-07-2018
                if (!string.IsNullOrEmpty(fc["Plates" + LineId]))
                {
                    objPAM020.Plates_Offeredsize_Qty = fc["Plates" + LineId];
                }
                if (!string.IsNullOrEmpty(fc["ForgedShell" + LineId]))
                {
                    objPAM020.Forged_shell_Offeredsize_Qty = fc["ForgedShell" + LineId];
                }
                if (!string.IsNullOrEmpty(fc["TubeSheet" + LineId]))
                {
                    objPAM020.Tube_sheet_shell_Offeredsize_Qty = fc["TubeSheet" + LineId];
                }

                if (LineId > 0)
                {
                    objPAM020.EditedBy = objClsLoginInfo.UserName;
                    objPAM020.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {
                    objPAM020.CreatedBy = objClsLoginInfo.UserName;
                    objPAM020.CreatedOn = DateTime.Now;
                    db.PAM020.Add(objPAM020);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteOfferedSizeQty(int LineId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objPAM020 = db.PAM020.FirstOrDefault(f => f.LineId == LineId);

                if (objPAM020 != null)
                {
                    db.PAM020.Remove(objPAM020);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Deleted Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found or already deleted";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult loadDeviationDataTable(JQueryDataTableParamModel param, int DisId, string ForApprove)
        {
            try
            {
                PAM009 objPAM009 = db.PAM009.Where(i => i.PDHId == DisId).FirstOrDefault();
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                bool isEditEnabled = true;
                string whereCondition = "1=1";
                whereCondition += " and PDHId= " + DisId.ToString();

                if (objPAM009.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower() ||
                    objPAM009.Status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
                {
                    isEditEnabled = false;
                }

                string[] columnName = { "Plates_Deviation", "Forged_shell_Deviation", "Tube_sheet_shell_Deviation" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstPam = db.SP_PDH_GET_DEVIATION_DETAILS(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                if (ForApprove == null || ForApprove.ToLower() == "false")
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    "0",
                                    Helper.GenerateHidden(newRecordId, "PDHId", Convert.ToString(DisId)),
                                    GenerateTextboxFor(newRecordId,"Plates","","","",false,"","30",false),
                                    GenerateTextboxFor(newRecordId,"ForgedShell","","","",false,"","30",false),
                                    GenerateTextboxFor(newRecordId,"TubeSheet","","","",false,"","30",false),
                                    GenerateGridButton(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveComponentLineRecord(0);" ),
                                };

                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "PDHId", Convert.ToString(h.PDHId)),
                                    GenerateTextboxFor(h.LineId,"Plates",h.Plates,"","",false,"","30",false),
                                    GenerateTextboxFor(h.LineId,"ForgedShell",h.ForgedShell,"","",false,"","30",false),
                                    GenerateTextboxFor(h.LineId,"TubeSheet",h.TubeSheet,"","",false,"","30",false),
                                    isEditEnabled ?GenerateGridButton(newRecordId, "Edit", "Edit Rocord", "fa fa-plus", "SaveDeviationRecord("+h.LineId+");" )  + GenerateGridButton(newRecordId, "Delete", "Delete Rocord", "fa fa-trash", "DeleteDeviationRecord("+h.LineId+");" ) : "",
                                    }).ToList();

                    res.Insert(0, newRecord);
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var res = (from h in lstPam
                               select new[] {
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId, "PDHId", Convert.ToString(h.PDHId)),
                                    Convert.ToString(h.Plates),
                                    Convert.ToString(h.ForgedShell),
                                    Convert.ToString(h.TubeSheet),
                                    ""

                    }).ToList();
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveDeviation(FormCollection fc, int Id = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int LineId = Id;
                // int HeaderId = Convert.ToInt32(fc["PDHId" + Id]);
                int PDHID = Convert.ToInt32(Session["pdh"]);
                PAM009 objPAM009 = db.PAM009.Where(x => x.PDHId == PDHID).FirstOrDefault();

                PAM021 objPAM021 = null;
                if (LineId > 0)
                {
                    objPAM021 = db.PAM021.Where(x => x.LineId == LineId).FirstOrDefault();
                }
                else
                {
                    objPAM021 = new PAM021();
                }
                objPAM021.PDHId = PDHID;
                objPAM021.Plates_Deviation = fc["Plates" + LineId];
                objPAM021.Forged_shell_Deviation = fc["ForgedShell" + LineId];
                objPAM021.Tube_sheet_shell_Deviation = fc["TubeSheet" + LineId];

                if (LineId > 0)
                {
                    objPAM021.EditedBy = objClsLoginInfo.UserName;
                    objPAM021.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {
                    objPAM021.CreatedBy = objClsLoginInfo.UserName;
                    objPAM021.CreatedOn = DateTime.Now;
                    db.PAM021.Add(objPAM021);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult DeleteDeviation(int LineId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objPAM021 = db.PAM021.FirstOrDefault(f => f.LineId == LineId);

                if (objPAM021 != null)
                {
                    db.PAM021.Remove(objPAM021);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Deleted Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found or already deleted";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GetPDHChecklist(int PDHId, string ForApprove)
        {
            ViewBag.PDHId = PDHId.ToString();
            // ViewBag.PDHId = "16";
            //if (ForApprove == null)
            //{
            //    ViewBag.ForApprove = "false";
            //}
            //else
            //{
            //    ViewBag.ForApprove = ForApprove;
            //}
            PAM009 objPAM009 = null;
            if (PDHId > 0)
            {
                objPAM009 = db.PAM009.FirstOrDefault(x => x.PDHId == PDHId);
            }
            else
            {
                objPAM009 = new PAM009();
            }
            ViewBag.ForApprove = ForApprove;
            return PartialView("_GetChecklistGridDataPartial", objPAM009);
            //if (PDHId==0)
            //{
            //    ViewBag.ForApprove = "false";
            //}
            //else
            //{
            //    if(objPAM009.Status== "Draft")
            //    {
            //        ViewBag.ForApprove = "false";
            //    }
            //    else
            //    {
            //        ViewBag.ForApprove = "true";
            //    }
            //    //ViewBag.ForApprove = objPAM009.Status;
            //  // ViewBag.ForApprove = "false";
            //}

            //Change If any Primary Key Add
            //PAM009 PAMOO9 = db.PAM009.FirstOrDefault(x => x.PDHId == PDHId);
            //if (PAMOO9 != null)
            //{
            //    PAMOO9.ProposalDesignRev = Convert.ToInt32(objPAM009.ProposalDesignRev);

            //    // header.ReturnRemarks = objPAM009.ReturnRemark;
            //    PAMOO9.HPC1 = objPAM009.HPC1;
            //    PAMOO9.HPC3 = objPAM009.HPC3;
            //    PAMOO9.Combination = objPAM009.Combination;
            //    PAMOO9.ReferPlanningInput = objPAM009.ReferPlanningInput;
            //    PAMOO9.ForgedShell = objPAM009.ForgedShell;
            //    PAMOO9.ASMEYes = objPAM009.ASMEYes;
            //    PAMOO9.ASMENo = objPAM009.ASMENo;
            //    PAMOO9.IBR = objPAM009.IBR;
            //    PAMOO9.PEDATEX = objPAM009.PEDATEX;
            //    PAMOO9.CRN = objPAM009.CRN;
            //    PAMOO9.DOSH = objPAM009.DOSH;
            //    PAMOO9.SELO = objPAM009.SELO;
            //    PAMOO9.GOST = objPAM009.GOST;
            //    PAMOO9.LocalRegulationOthers = objPAM009.LocalRegulationOthers;
            //    PAMOO9.Warm = objPAM009.Warm;
            //    PAMOO9.Cold = objPAM009.Cold;
            //    PAMOO9.Hot = objPAM009.Hot;
            //    PAMOO9.ShellRollingNotApplicable = objPAM009.ShellRollingNotApplicable;
            //    PAMOO9.ForgedShellWithProjection = objPAM009.ForgedShellWithProjection;
            //    PAMOO9.ForgedShellWithIntegralNub = objPAM009.ForgedShellWithIntegralNub;
            //    PAMOO9.ThkOnPlate = objPAM009.ThkOnPlate;
            //    PAMOO9.NoInsideProjection = objPAM009.NoInsideProjection;

            //    PAMOO9.NubShellNotApplicable = objPAM009.NubShellNotApplicable;
            //    PAMOO9.SinglePiece = objPAM009.SinglePiece;
            //    PAMOO9.Piece2 = objPAM009.Piece2;
            //    PAMOO9.Piece3 = objPAM009.Piece3;
            //    PAMOO9.TubeSheetNotApplicable = objPAM009.TubeSheetNotApplicable;
            //    PAMOO9.TubeSheetOthers = objPAM009.TubeSheetOthers;
            //    PAMOO9.SingleLayer = objPAM009.SingleLayer;
            //    PAMOO9.DoubleLayer = objPAM009.DoubleLayer;
            //    PAMOO9.Clad = objPAM009.Clad;
            //    PAMOO9.RefractoryAtShop = objPAM009.RefractoryAtShop;
            //    PAMOO9.RefractoryAtSite = objPAM009.RefractoryAtSite;
            //    PAMOO9.OverlayNotApplicable = objPAM009.OverlayNotApplicable;
            //    PAMOO9.Part4 = objPAM009.Part4;
            //    PAMOO9.Part5 = objPAM009.Part5;
            //    PAMOO9.CatalystNotApplicable = objPAM009.CatalystNotApplicable;
            //    PAMOO9.SinglePiece2 = objPAM009.SinglePiece2;
            //    PAMOO9.Half2 = objPAM009.Half2;
            //    PAMOO9.Petals4 = objPAM009.Petals4;
            //    PAMOO9.Petals6 = objPAM009.Petals6;
            //    PAMOO9.HeadConstructionOthers = objPAM009.HeadConstructionOthers;
            //    PAMOO9.WithCrown = objPAM009.WithCrown;
            //    PAMOO9.WithoutCrown = objPAM009.WithoutCrown;
            //    PAMOO9.HeadConstructionCold = objPAM009.HeadConstructionCold;
            //    PAMOO9.HeadConstructionWarm = objPAM009.HeadConstructionWarm;
            //    PAMOO9.HeadConstructionHot = objPAM009.HeadConstructionHot;
            //    PAMOO9.FormingInIndia = objPAM009.FormingInIndia;
            //    PAMOO9.ReadyHeadImported = objPAM009.ReadyHeadImported;
            //    PAMOO9.LSRYes = objPAM009.LSRYes;
            //    PAMOO9.LSRNo = objPAM009.LSRNo;
            //    PAMOO9.InternalYes = objPAM009.InternalYes;
            //    PAMOO9.InternalNo = objPAM009.InternalNo;
            //    PAMOO9.SiteInstallationScope = objPAM009.SiteInstallationScope;
            //    PAMOO9.OtherPoints = Convert.ToString(objPAM009.OtherPoints);
            //    PAMOO9.Plates_supplier = objPAM009.Plates_supplier;
            //    PAMOO9.Plates_Offeredsize_Qty = Convert.ToInt32(objPAM009.Plates_Offeredsize_Qty);
            //    PAMOO9.Plates_Deviation = objPAM009.Plates_Deviation;
            //    PAMOO9.Plates_Deviation_rolling = objPAM009.Plates_Deviation_rolling;
            //    PAMOO9.Forged_shell_supplier = objPAM009.Forged_shell_supplier;
            //    PAMOO9.Forged_shell_Offeredsize_Qty = Convert.ToInt32(objPAM009.Forged_shell_Offeredsize_Qty);
            //    PAMOO9.Forged_shell_Deviation = objPAM009.Forged_shell_Deviation;
            //    PAMOO9.Forged_shell_Deviation_rolling = objPAM009.Forged_shell_Deviation_rolling;
            //    PAMOO9.Tube_sheet_supplier = objPAM009.Tube_sheet_supplier;
            //    PAMOO9.Tube_sheet_shell_Offeredsize_Qty = Convert.ToInt32(objPAM009.Tube_sheet_shell_Offeredsize_Qty);
            //    PAMOO9.Tube_sheet_shell_Deviation = objPAM009.Tube_sheet_shell_Deviation;
            //    PAMOO9.Tube_sheet_shell_Deviation_rolling = objPAM009.Tube_sheet_shell_Deviation_rolling;

            //    return PartialView("_GetChecklistGridDataPartial", PAMOO9);
            //}
            //else
            //{
            //    return PartialView("_GetChecklistGridDataPartial");
            //}


        }


        public ActionResult PDHttachmentPartial(int? id)
        {
            PAM010 objPAM010 = db.PAM010.Where(x => x.LineId == id).FirstOrDefault();
            return PartialView("_PDHAttachmentPartial", objPAM010);
        }

        [HttpPost]
        public ActionResult SavePDHAttachment(string id, bool hasAttachments, Dictionary<string, string> Attach)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (hasAttachments)
                {

                    var folderPath = "PAM010/" + id;
                    Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "File Uploaded Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Upload Document";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        public ActionResult SendForApproval(int PDHId,string Project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PAM009 objPAM009 = new PAM009();
            //  bool IsValid = true;
            try
            {
                if (PDHId > 0)
                {
                    objPAM009 = db.PAM009.Where(i => i.PDHId == PDHId).FirstOrDefault();
                    // var objPAM010 = objPAM009.PAM010.Where(i => i.IsAvailable == false && (i.Remarks == string.Empty || i.Remarks ==null)).ToList();
                    var objPAM010 = db.PAM010.Where(i => i.PDHId == objPAM009.PDHId).FirstOrDefault();
                    if (objPAM010 == null)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please Add Document Design Lines";
                    }
                    else
                    {

                        if (objPAM009 != null)
                        {
                            objPAM009.Status = clsImplementationEnum.NDETechniqueStatus.SentForApproval.GetStringValue();
                            objPAM009.SubmittedBy = objClsLoginInfo.UserName;
                            objPAM009.SubmittedOn = DateTime.Now;
                            objPAM009.EditedBy = objClsLoginInfo.UserName;
                            objPAM009.EditedOn = DateTime.Now;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.SentForApproval.ToString();
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            ViewBag.chkProject = Project;
            return Json(objResponseMsg);
        }

        public ActionResult loadCheckListDataTable(JQueryDataTableParamModel param, int? PDHId, bool ForApproval)
        {
            try
            {

                string Status = clsImplementationEnum.PAMStatus.Draft.GetStringValue();
                var lstPam = db.SP_PAM_PDH_GetChecklistDetails(PDHId).ToList();

                var res = from h in lstPam
                          select new[] {
                              Convert.ToString( h.ROW_NO),
                              Convert.ToString( h.PDHId),
                           Convert.ToString( h.LineId),
                           Convert.ToString( h.ChecklistId),
                           Convert.ToString(h.Description),
                           GetChecklistCheckStyle(ForApproval,h.IsAvailable),
                           GetSectionRemarkStyle(ForApproval,h.Remarks),
                           ""//Attachment
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = lstPam.Count,
                    iTotalRecords = lstPam.Count,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string GetSectionRemarkStyle(bool ForApproval, string sectionRemarks)
        {
            //return "<input type='text' name='txtSectionRemark' style='width:100%'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' />";
            string rtnremarks = "";
            rtnremarks += "<input type='text' id='txtSectionRemark' name='txtSectionRemark' style='width:100%'  class='form-control input-sm  input-inline sectionTextbox' value='" + sectionRemarks + "' ";
            if (ForApproval)
            {
                rtnremarks += " disabled='disabled' ";
            }
            rtnremarks += " />";
            return rtnremarks;

        }

        public string GetChecklistCheckStyle(bool ForApproval, bool? isCheck)
        {
            string rtnchecjbox = "";
            if (isCheck == null)
                isCheck = false;
            rtnchecjbox = "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck' class='make-switch col-md-3 available' data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' ";

            if (isCheck == true)
            {
                rtnchecjbox += " checked='checked' ";
            }
            if (ForApproval)
            {
                rtnchecjbox += " disabled= 'disabled' ";
            }
            rtnchecjbox += "/>";
            return rtnchecjbox;
            //return "<input type='checkbox' name='chkChecklistCheck' id='chkChecklistCheck'  class='make-switch col-md-3 available' checked='checked'  data-on-text='Yes' data-off-text='No' data-on-color='success' data-off-color='danger' data-size='normal' />";

        }

        [HttpPost]
        public ActionResult GetPAMNoResult(string term,string Project)
        {
            if(Project!=null && Project!="")
            {
                string whereCondition = "1=1";
                whereCondition += " and t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";
                whereCondition += " and PAM like '%" + term + "%' ";                
                whereCondition += " and Project='" + Project.ToString() + "'";

                var lstPam = db.SP_PAM_GETPAMDETAILSCONTRACTWISE(0, 0, "", whereCondition).ToList();
                var res = from h in lstPam
                          select new
                          {
                              HeaderId = Convert.ToString(h.HeaderId),
                              PAM = Convert.ToString(h.PAM)
                          };
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            else
            {
                string whereCondition = "1=1";
                whereCondition += " and t_csbu collate SQL_Latin1_General_CP850_CI_AI in (select * from dbo.fn_split(dbo.GETBUFROMUSER('" + objClsLoginInfo.UserName + "'),',')) ";
                whereCondition += " and PAM like '%" + term + "%' ";


                var lstPam = db.SP_PAM_GETPAMDETAILSCONTRACTWISE(0, 0, "", whereCondition).ToList();
                var res = from h in lstPam
                          select new
                          {
                              HeaderId = Convert.ToString(h.HeaderId),
                              PAM = Convert.ToString(h.PAM)
                          };
                return Json(res, JsonRequestBehavior.AllowGet);
            }
            
        }

        public ActionResult GetManufacturingCode()
        {
            var lstCategory = Manager.GetSubCatagories("Manufacturing Code");
            var res = (from h in lstCategory
                       select new
                       {
                           id = h.Code,
                           text = h.Description
                       }).Distinct().ToList();
            return Json(res, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SavePDH(PAM009 model, string txtPAMNo,string Project)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                //if (model.HPC1 == false && model.HPC3 == false && model.Combination == false)
                //{
                //    objResponseMsg.Key = false;
                //    objResponseMsg.Value = "Please Select Atleaset One Production Center";
                //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

                //}
                if (model.ReferPlanningInput == false && model.ForgedShell == false && model.RollingCapacityNotAplicate == false)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please Select Atleaset Rolling Capacity";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

                }
                if (Convert.ToInt32(model.PDHId) > 0)
                {
                    var objPAM009 = db.PAM009.Where(i => i.PDHId == model.PDHId).FirstOrDefault();
                    if (objPAM009.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                    {
                        objPAM009.ProposalDesignRev = Convert.ToInt32(objPAM009.ProposalDesignRev) + 1;
                        objPAM009.ReturnRemark = null;
                        objPAM009.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                    }
                    var pam = db.PAM001.Where(m => m.PAM == txtPAMNo).FirstOrDefault();
                    objPAM009.HeaderId = Convert.ToInt32(pam.HeaderId);
                    objPAM009.HPC1 = model.HPC1;
                    objPAM009.HPC3 = model.HPC3;
                    objPAM009.Combination = model.Combination;
                    objPAM009.ReferPlanningInput = model.ReferPlanningInput;
                    objPAM009.ForgedShell = model.ForgedShell;
                    objPAM009.RollingCapacityNotAplicate = model.RollingCapacityNotAplicate;
                    objPAM009.ManufacturingCode = model.ManufacturingCode;
                    objPAM009.CodeEdition = model.CodeEdition;
                    objPAM009.ASMEYes = model.ASMEYes;
                    objPAM009.ASMENo = model.ASMENo;
                    objPAM009.ShellConstructionMonowall = model.ShellConstructionMonowall;
                    objPAM009.ShellConstructionMultiwall = model.ShellConstructionMultiwall;
                    objPAM009.IBR = model.IBR;
                    objPAM009.PEDATEX = model.PEDATEX;
                    objPAM009.CRN = model.CRN;
                    objPAM009.DOSH = model.DOSH;
                    objPAM009.SELO = model.SELO;
                    objPAM009.GOST = model.GOST;
                    objPAM009.LocalRegulationOthers = model.LocalRegulationOthers;
                    objPAM009.LocReguNotApplicable = model.LocReguNotApplicable;
                    objPAM009.Warm = model.Warm;
                    objPAM009.Cold = model.Cold;
                    objPAM009.Hot = model.Hot;
                    objPAM009.ShellRollingNotApplicable = model.ShellRollingNotApplicable;
                    objPAM009.ForgedShellWithProjection = model.ForgedShellWithProjection;
                    objPAM009.ForgedShellWithIntegralNub = model.ForgedShellWithIntegralNub;
                    objPAM009.ThkOnPlate = model.ThkOnPlate;
                    objPAM009.NoInsideProjection = model.NoInsideProjection;
                    objPAM009.NubShellNotApplicable = model.NubShellNotApplicable;
                    objPAM009.SinglePiece = model.SinglePiece;
                    objPAM009.Piece2 = model.Piece2;
                    objPAM009.Piece3 = model.Piece3;
                    objPAM009.TubeSheetNotApplicable = model.TubeSheetNotApplicable;
                    objPAM009.TubeSheetOthers = model.TubeSheetOthers;
                    objPAM009.SingleLayer = model.SingleLayer;
                    objPAM009.DoubleLayer = model.DoubleLayer;
                    objPAM009.Clad = model.Clad;
                    objPAM009.RefractoryAtShop = model.RefractoryAtShop;
                    objPAM009.RefractoryAtSite = model.RefractoryAtSite;
                    objPAM009.Liner = model.Liner;
                    objPAM009.OverlayNotApplicable = model.OverlayNotApplicable;
                    objPAM009.Part4 = model.Part4;
                    objPAM009.Part5 = model.Part5;
                    objPAM009.CatalystNotApplicable = model.CatalystNotApplicable;
                    objPAM009.SinglePiece2 = model.SinglePiece2;
                    objPAM009.Half2 = model.Half2;
                    objPAM009.Petals4 = model.Petals4;
                    objPAM009.Petals6 = model.Petals6;
                    objPAM009.HeadConstructionOthers = model.HeadConstructionOthers;
                    objPAM009.WithCrown = model.WithCrown;
                    objPAM009.WithoutCrown = model.WithoutCrown;
                    objPAM009.HeadConstructionCold = model.HeadConstructionCold;
                    objPAM009.HeadConstructionWarm = model.HeadConstructionWarm;
                    objPAM009.HeadConstructionHot = model.HeadConstructionHot;
                    objPAM009.FormingInIndia = model.FormingInIndia;
                    objPAM009.ReadyHeadImported = model.ReadyHeadImported;
                    objPAM009.LSRYes = model.LSRYes;
                    objPAM009.LSRNo = model.LSRNo;
                    objPAM009.InternalYes = model.InternalYes;
                    objPAM009.InternalNo = model.InternalNo;
                    objPAM009.SiteInstallationScope = model.SiteInstallationScope;
                    objPAM009.ThirdPartyInspectionIncludedYes = model.ThirdPartyInspectionIncludedYes;
                    objPAM009.ThirdPartyInspectionIncludedNo = model.ThirdPartyInspectionIncludedNo;
                    objPAM009.ProjectCostEstimationSheetInLNYes = model.ProjectCostEstimationSheetInLNYes;
                    objPAM009.ProjectCostEstimationSheetOnLNNo = model.ProjectCostEstimationSheetOnLNNo;
                    objPAM009.ProjectCostEstimationSheetOnLNRemark = model.ProjectCostEstimationSheetOnLNRemark;
                    objPAM009.DeliveryCommitted = model.DeliveryCommitted;
                    objPAM009.OtherPoints = Convert.ToString(model.OtherPoints);
                    objPAM009.ProductionCenter = model.ProductionCenter;
                    objPAM009.EditedBy = objClsLoginInfo.UserName;
                    objPAM009.EditedOn = DateTime.Now;
                    objPAM009.local_regulation_others = model.local_regulation_others;
                    objPAM009.Tube_sheet_construction_others = model.Tube_sheet_construction_others;
                    objPAM009.Site_installation = model.Site_installation;

                    ////Observation 25113, User can manual add new component
                    //bool isdataexist = db.PAM022.Where(x => x.PDHId == objPAM009.PDHId).Any();
                    //if (!isdataexist)
                    //{
                    //    //Observation 16441, Add default entry for plates, forged shell, tube sheet into PAM022
                    //    List<PAM022> lstPAM022 = new List<PAM022>();
                    //    List<string> lstComponents = clsImplementationEnum.GetPDHComponentsMethods().ToList();
                    //    foreach (var item in lstComponents)
                    //    {
                    //        PAM022 objPAM022 = new PAM022();
                    //        objPAM022.PDHId = objPAM009.PDHId;
                    //        objPAM022.Component = item;
                    //        objPAM022.ParentId = 0;
                    //        objPAM022.CreatedBy = objClsLoginInfo.UserName;
                    //        objPAM022.CreatedOn = DateTime.Now;

                    //        lstPAM022.Add(objPAM022);
                    //    }
                    //    db.PAM022.AddRange(lstPAM022);
                    //}
                    db.SaveChanges();
                    objResponseMsg.HeaderId = objPAM009.HeaderId;
                    objResponseMsg.PDHId = Convert.ToInt32(model.PDHId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Proposal Design Handover Updated Successfully";
                }
                else
                {
                    PAM009 objPAM009 = new PAM009();

                    objPAM009.ProposalDesign = model.ProposalDesign;
                    objPAM009.ProposalDesignRev = Convert.ToInt32(model.ProposalDesignRev);
                    objPAM009.Status = model.Status;
                    objPAM009.CreatedBy = objClsLoginInfo.UserName;
                    objPAM009.CreatedOn = DateTime.Now;
                    objPAM009.HPC1 = model.HPC1;
                    objPAM009.HPC3 = model.HPC3;
                    objPAM009.Combination = model.Combination;
                    objPAM009.ReferPlanningInput = model.ReferPlanningInput;
                    objPAM009.ForgedShell = model.ForgedShell;
                    objPAM009.RollingCapacityNotAplicate = model.RollingCapacityNotAplicate;
                    objPAM009.ManufacturingCode = model.ManufacturingCode;
                    objPAM009.CodeEdition = model.CodeEdition;
                    //objPAM009.ASMEYes = model.ASMEYes;
                    //objPAM009.ASMENo = model.ASMENo;
                    objPAM009.ShellConstructionMonowall = model.ShellConstructionMonowall;
                    objPAM009.ShellConstructionMultiwall = model.ShellConstructionMultiwall;
                    objPAM009.IBR = model.IBR;
                    objPAM009.PEDATEX = model.PEDATEX;
                    objPAM009.CRN = model.CRN;
                    objPAM009.DOSH = model.DOSH;
                    objPAM009.SELO = model.SELO;
                    objPAM009.GOST = model.GOST;
                    objPAM009.LocalRegulationOthers = model.LocalRegulationOthers;
                    objPAM009.LocReguNotApplicable = model.LocReguNotApplicable;
                    objPAM009.Warm = model.Warm;
                    objPAM009.Cold = model.Cold;
                    objPAM009.Hot = model.Hot;
                    objPAM009.ShellRollingNotApplicable = model.ShellRollingNotApplicable;
                    objPAM009.ForgedShellWithProjection = model.ForgedShellWithProjection;
                    objPAM009.ForgedShellWithIntegralNub = model.ForgedShellWithIntegralNub;
                    objPAM009.ThkOnPlate = model.ThkOnPlate;
                    objPAM009.NoInsideProjection = model.NoInsideProjection;
                    objPAM009.NubShellNotApplicable = model.NubShellNotApplicable;
                    objPAM009.SinglePiece = model.SinglePiece;
                    objPAM009.Piece2 = model.Piece2;
                    objPAM009.Piece3 = model.Piece3;
                    objPAM009.TubeSheetNotApplicable = model.TubeSheetNotApplicable;
                    objPAM009.TubeSheetOthers = model.TubeSheetOthers;
                    objPAM009.SingleLayer = model.SingleLayer;
                    objPAM009.DoubleLayer = model.DoubleLayer;
                    objPAM009.Clad = model.Clad;
                    objPAM009.RefractoryAtShop = model.RefractoryAtShop;
                    objPAM009.RefractoryAtSite = model.RefractoryAtSite;
                    objPAM009.Liner = model.Liner;
                    objPAM009.OverlayNotApplicable = model.OverlayNotApplicable;
                    objPAM009.Part4 = model.Part4;
                    objPAM009.Part5 = model.Part5;
                    objPAM009.CatalystNotApplicable = model.CatalystNotApplicable;
                    objPAM009.SinglePiece2 = model.SinglePiece2;
                    objPAM009.Half2 = model.Half2;
                    objPAM009.Petals4 = model.Petals4;
                    objPAM009.Petals6 = model.Petals6;
                    objPAM009.HeadConstructionOthers = model.HeadConstructionOthers;
                    objPAM009.WithCrown = model.WithCrown;
                    objPAM009.WithoutCrown = model.WithoutCrown;
                    objPAM009.HeadConstructionCold = model.HeadConstructionCold;
                    objPAM009.HeadConstructionWarm = model.HeadConstructionWarm;
                    objPAM009.HeadConstructionHot = model.HeadConstructionHot;
                    objPAM009.FormingInIndia = model.FormingInIndia;
                    objPAM009.ReadyHeadImported = model.ReadyHeadImported;
                    // objPAM009.LSRYes = LSRYes;
                    // objPAM009.LSRNo = LSRNo;
                    objPAM009.InternalYes = model.InternalYes;
                    objPAM009.InternalNo = model.InternalNo;
                    objPAM009.SiteInstallationScope = model.SiteInstallationScope;
                    objPAM009.ThirdPartyInspectionIncludedYes = model.ThirdPartyInspectionIncludedYes;
                    objPAM009.ThirdPartyInspectionIncludedNo = model.ThirdPartyInspectionIncludedNo;
                    objPAM009.ProjectCostEstimationSheetInLNYes = model.ProjectCostEstimationSheetInLNYes;
                    objPAM009.ProjectCostEstimationSheetOnLNNo = model.ProjectCostEstimationSheetOnLNNo;
                    objPAM009.ProjectCostEstimationSheetOnLNRemark = model.ProjectCostEstimationSheetOnLNRemark;
                    objPAM009.DeliveryCommitted = model.DeliveryCommitted;
                    objPAM009.OtherPoints = Convert.ToString(model.OtherPoints);
                    objPAM009.ProductionCenter = model.ProductionCenter;
                    objPAM009.local_regulation_others = model.local_regulation_others;
                    objPAM009.Tube_sheet_construction_others = model.Tube_sheet_construction_others;
                    objPAM009.Site_installation = model.Site_installation;
                    var pam = db.PAM001.Where(m => m.PAM == txtPAMNo).FirstOrDefault();
                    objPAM009.HeaderId = Convert.ToInt32(pam.HeaderId);
                    db.PAM009.Add(objPAM009);

                    ////Observation 25113, User can manual add new component
                    //////Observation 16441, Add default entry for plates, forged shell, tube sheet into PAM022
                    //List<PAM022> lstPAM022 = new List<PAM022>();
                    //List<string> lstComponents = clsImplementationEnum.GetPDHComponentsMethods().ToList();
                    //foreach (var item in lstComponents)
                    //{
                    //    PAM022 objPAM022 = new PAM022();
                    //    objPAM022.PDHId = objPAM009.PDHId;
                    //    objPAM022.Component = item;
                    //    objPAM022.ParentId = 0;
                    //    objPAM022.CreatedBy = objClsLoginInfo.UserName;
                    //    objPAM022.CreatedOn = DateTime.Now;

                    //    lstPAM022.Add(objPAM022);
                    //}
                    //db.PAM022.AddRange(lstPAM022);
                    db.SaveChanges();
                    var getpdhid = db.PAM009.Where(x => x.CreatedBy == objClsLoginInfo.UserName).OrderByDescending(x => x.PDHId).FirstOrDefault();

                    ViewBag.chkProject = Project;
                    objResponseMsg.PDHId = objPAM009.PDHId;
                    objResponseMsg.HeaderId = objPAM009.HeaderId;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Proposal Design Handover Save Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        // public ActionResult SavePDH(string PDHId, string HeaderId, string ProposalDesign, string ProposalDesignRev, string Status, bool HPC1, bool HPC3, bool Combination, bool ReferPlanningInput, bool ForgedShell, bool IBR, bool PEDATEX, bool CRN, bool DOSH, bool SELO, bool GOST, bool LocalRegulationOthers, bool Warm, bool Cold, bool Hot , bool ShellRollingNotApplicable, bool ForgedShellWithProjection, bool ForgedShellWithIntegralNub, bool ThkOnPlate, bool NoInsideProjection, bool NubShellNotApplicable, bool SinglePiece, bool Piece2, bool Piece3 , bool TubeSheetNotApplicable, bool TubeSheetOthers, bool SingleLayer, bool DoubleLayer, bool Clad, bool RefractoryAtShop, bool RefractoryAtSite, bool OverlayNotApplicable, bool Part4, bool Part5, bool CatalystNotApplicable, bool SinglePiece2, bool Half2, bool Petals4, bool Petals6, bool HeadConstructionOthers, bool WithCrown, bool WithoutCrown, bool HeadConstructionCold, bool HeadConstructionWarm, bool HeadConstructionHot, bool FormingInIndia, bool ReadyHeadImported,  bool InternalYes, bool InternalNo, bool SiteInstallationScope, string OtherPoints)
        //public ActionResult SavePDH(string txtPAMNo, string PDHId, string HeaderId, string ProposalDesign, string ProposalDesignRev, string Status, bool HPC1, bool HPC3,
        //                            bool Combination, bool ReferPlanningInput, bool ForgedShell, bool RollingCapacityNotAplicate, string ManufacturingCode, string CodeEdition, bool ShellConstructionMonowall,
        //                            bool ShellConstructionMultiwall, bool IBR, bool PEDATEX, bool CRN, bool DOSH, bool SELO, bool GOST, bool LocalRegulationOthers, bool Warm, bool Cold, bool Hot,
        //                            bool ShellRollingNotApplicable, bool ForgedShellWithProjection, bool ForgedShellWithIntegralNub, bool ThkOnPlate, bool NoInsideProjection, bool NubShellNotApplicable,
        //                            bool SinglePiece, bool Piece2, bool Piece3, bool TubeSheetNotApplicable, bool TubeSheetOthers, bool SingleLayer, bool DoubleLayer, bool Clad, bool RefractoryAtShop,
        //                            bool RefractoryAtSite, bool Liner, bool OverlayNotApplicable, bool Part4, bool Part5, bool CatalystNotApplicable, bool SinglePiece2, bool Half2,
        //                            bool Petals4, bool Petals6, bool HeadConstructionOthers, bool WithCrown, bool WithoutCrown, bool HeadConstructionCold,
        //                            bool HeadConstructionWarm, bool HeadConstructionHot, bool FormingInIndia, bool ReadyHeadImported, bool InternalYes, bool InternalNo,
        //                            bool SiteInstallationScope, bool ThirdPartyInspectionIncludedYes, bool ThirdPartyInspectionIncludedNo, bool ProjectCostEstimationSheetInLNYes,
        //                            bool ProjectCostEstimationSheetOnLNNo, string ProjectCostEstimationSheetOnLNRemark, int? DeliveryCommitted, string OtherPoints,
        //                            string local_regulation_others, string Tube_sheet_construction_others, string Site_installation, bool ASMEYes, bool ASMENo, bool LSRYes, bool LSRNo, string ProductionCenter)
        //// 
        //{
        //    clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
        //    int id = 0;
        //    try
        //    {
        //        if (HPC1 == false && HPC3 == false && Combination == false)
        //        {
        //            objResponseMsg.Key = false;
        //            objResponseMsg.Value = "Please Select Atleaset One Production Center";
        //            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        //        }
        //        if (ReferPlanningInput == false && ForgedShell == false && RollingCapacityNotAplicate == false)
        //        {
        //            objResponseMsg.Key = false;
        //            objResponseMsg.Value = "Please Select Atleaset Rolling Capacity";
        //            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        //        }
        //        if (Convert.ToInt32(PDHId) > 0)
        //        {
        //            id = Convert.ToInt32(PDHId);
        //            var objPAM009 = db.PAM009.Where(i => i.PDHId == id).FirstOrDefault();
        //            if (objPAM009.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
        //            {
        //                objPAM009.ProposalDesignRev = Convert.ToInt32(objPAM009.ProposalDesignRev) + 1;
        //                objPAM009.ReturnRemark = null;
        //                objPAM009.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
        //            }
        //            var pam = db.PAM001.Where(m => m.PAM == txtPAMNo).FirstOrDefault();
        //            objPAM009.HeaderId = Convert.ToInt32(pam.HeaderId);
        //            objPAM009.HPC1 = HPC1;
        //            objPAM009.HPC3 = HPC3;
        //            objPAM009.Combination = Combination;
        //            objPAM009.ReferPlanningInput = ReferPlanningInput;
        //            objPAM009.ForgedShell = ForgedShell;
        //            objPAM009.RollingCapacityNotAplicate = RollingCapacityNotAplicate;
        //            objPAM009.ManufacturingCode = ManufacturingCode;
        //            objPAM009.CodeEdition = CodeEdition;
        //            objPAM009.ASMEYes = ASMEYes;
        //            objPAM009.ASMENo = ASMENo;
        //            objPAM009.ShellConstructionMonowall = ShellConstructionMonowall;
        //            objPAM009.ShellConstructionMultiwall = ShellConstructionMultiwall;
        //            objPAM009.IBR = IBR;
        //            objPAM009.PEDATEX = PEDATEX;
        //            objPAM009.CRN = CRN;
        //            objPAM009.DOSH = DOSH;
        //            objPAM009.SELO = SELO;
        //            objPAM009.GOST = GOST;
        //            objPAM009.LocalRegulationOthers = LocalRegulationOthers;
        //            objPAM009.Warm = Warm;
        //            objPAM009.Cold = Cold;
        //            objPAM009.Hot = Hot;
        //            objPAM009.ShellRollingNotApplicable = ShellRollingNotApplicable;
        //            objPAM009.ForgedShellWithProjection = ForgedShellWithProjection;
        //            objPAM009.ForgedShellWithIntegralNub = ForgedShellWithIntegralNub;
        //            objPAM009.ThkOnPlate = ThkOnPlate;
        //            objPAM009.NoInsideProjection = NoInsideProjection;

        //            objPAM009.NubShellNotApplicable = NubShellNotApplicable;
        //            objPAM009.SinglePiece = SinglePiece;
        //            objPAM009.Piece2 = Piece2;
        //            objPAM009.Piece3 = Piece3;
        //            objPAM009.TubeSheetNotApplicable = TubeSheetNotApplicable;
        //            objPAM009.TubeSheetOthers = TubeSheetOthers;
        //            objPAM009.SingleLayer = SingleLayer;
        //            objPAM009.DoubleLayer = DoubleLayer;
        //            objPAM009.Clad = Clad;
        //            objPAM009.RefractoryAtShop = RefractoryAtShop;
        //            objPAM009.RefractoryAtSite = RefractoryAtSite;
        //            objPAM009.Liner = Liner;
        //            objPAM009.OverlayNotApplicable = OverlayNotApplicable;
        //            objPAM009.Part4 = Part4;
        //            objPAM009.Part5 = Part5;
        //            objPAM009.CatalystNotApplicable = CatalystNotApplicable;
        //            objPAM009.SinglePiece2 = SinglePiece2;
        //            objPAM009.Half2 = Half2;
        //            objPAM009.Petals4 = Petals4;
        //            objPAM009.Petals6 = Petals6;
        //            objPAM009.HeadConstructionOthers = HeadConstructionOthers;
        //            objPAM009.WithCrown = WithCrown;
        //            objPAM009.WithoutCrown = WithoutCrown;
        //            objPAM009.HeadConstructionCold = HeadConstructionCold;
        //            objPAM009.HeadConstructionWarm = HeadConstructionWarm;
        //            objPAM009.HeadConstructionHot = HeadConstructionHot;
        //            objPAM009.FormingInIndia = FormingInIndia;
        //            objPAM009.ReadyHeadImported = ReadyHeadImported;
        //            objPAM009.LSRYes = LSRYes;
        //            objPAM009.LSRNo = LSRNo;
        //            objPAM009.InternalYes = InternalYes;
        //            objPAM009.InternalNo = InternalNo;
        //            objPAM009.SiteInstallationScope = SiteInstallationScope;
        //            objPAM009.ThirdPartyInspectionIncludedYes = ThirdPartyInspectionIncludedYes;
        //            objPAM009.ThirdPartyInspectionIncludedNo = ThirdPartyInspectionIncludedNo;
        //            objPAM009.ProjectCostEstimationSheetInLNYes = ProjectCostEstimationSheetInLNYes;
        //            objPAM009.ProjectCostEstimationSheetOnLNNo = ProjectCostEstimationSheetOnLNNo;
        //            objPAM009.ProjectCostEstimationSheetOnLNRemark = ProjectCostEstimationSheetOnLNRemark;
        //            objPAM009.DeliveryCommitted = DeliveryCommitted;
        //            objPAM009.OtherPoints = Convert.ToString(OtherPoints);
        //            objPAM009.ProductionCenter = ProductionCenter;
        //            objPAM009.EditedBy = objClsLoginInfo.UserName;
        //            objPAM009.EditedOn = DateTime.Now;
        //            /*objPAM009.Plates_supplier = Plates_supplier;
        //            if(Plates_Offeredsize_Qty!="")
        //            {
        //                objPAM009.Plates_Offeredsize_Qty = Convert.ToInt32(Plates_Offeredsize_Qty);
        //            }

        //           // 
        //            objPAM009.Plates_Deviation=plates_deviation;
        //            objPAM009.Plates_Deviation_rolling = plates_deviation_rolling;
        //            objPAM009.Forged_shell_supplier = forged_shell_supplier;
        //            if(forged_shell_offeredsize_qty!="")
        //            {
        //                objPAM009.Forged_shell_Offeredsize_Qty = Convert.ToInt32(forged_shell_offeredsize_qty);
        //            }
        //           //
        //            objPAM009.Forged_shell_Deviation = forged_shell_deviation;
        //            objPAM009.Forged_shell_Deviation_rolling = forged_shell_deviation_rolling;
        //            objPAM009.Tube_sheet_supplier = tube_sheet_supplier;
        //            if(tube_sheet_shell_offeredsize_qty!="")
        //            {
        //                objPAM009.Tube_sheet_shell_Offeredsize_Qty = Convert.ToInt32(tube_sheet_shell_offeredsize_qty);
        //            }
        //           // 
        //            objPAM009.Tube_sheet_shell_Deviation = tube_sheet_shell_deviation;
        //            objPAM009.Tube_sheet_shell_Deviation_rolling = tube_sheet_shell_deviation_rolling;
        //            //foreach (var item in objPAM009.PAM010)
        //            //{
        //            //    //var checklist = listCheckListModel.Where(i => i.ChecklistId == item.ChecklistId).FirstOrDefault();
        //            //    //item.Remarks = checklist.Remarks;
        //            //    //item.IsAvailable = checklist.IsAvailable;
        //            //}*/
        //            objPAM009.local_regulation_others = local_regulation_others;
        //            objPAM009.Tube_sheet_construction_others = Tube_sheet_construction_others;
        //            objPAM009.Site_installation = Site_installation;

        //            db.SaveChanges();
        //            objResponseMsg.HeaderId = objPAM009.HeaderId;
        //            objResponseMsg.PDHId = Convert.ToInt32(PDHId);
        //            objResponseMsg.Key = true;
        //            objResponseMsg.Value = "Proposal Design Handover Updated Successfully";
        //        }
        //        else
        //        {
        //            PAM009 objPAM009 = new PAM009();

        //            objPAM009.ProposalDesign = ProposalDesign;
        //            objPAM009.ProposalDesignRev = Convert.ToInt32(ProposalDesignRev);
        //            objPAM009.Status = Status;
        //            objPAM009.CreatedBy = objClsLoginInfo.UserName;
        //            objPAM009.CreatedOn = DateTime.Now;
        //            // db.PAM009.Add(objPAM009);
        //            objPAM009.HPC1 = HPC1;
        //            objPAM009.HPC3 = HPC3;
        //            objPAM009.Combination = Combination;
        //            objPAM009.ReferPlanningInput = ReferPlanningInput;
        //            objPAM009.ForgedShell = ForgedShell;
        //            objPAM009.RollingCapacityNotAplicate = RollingCapacityNotAplicate;
        //            objPAM009.ManufacturingCode = ManufacturingCode;
        //            objPAM009.CodeEdition = CodeEdition;
        //            // objPAM009.ASMEYes = ASMEYes;
        //            // objPAM009.ASMENo = ASMENo;
        //            objPAM009.ShellConstructionMonowall = ShellConstructionMonowall;
        //            objPAM009.ShellConstructionMultiwall = ShellConstructionMultiwall;
        //            objPAM009.IBR = IBR;
        //            objPAM009.PEDATEX = PEDATEX;
        //            objPAM009.CRN = CRN;
        //            objPAM009.DOSH = DOSH;
        //            objPAM009.SELO = SELO;
        //            objPAM009.GOST = GOST;
        //            objPAM009.LocalRegulationOthers = LocalRegulationOthers;
        //            objPAM009.Warm = Warm;
        //            objPAM009.Cold = Cold;
        //            objPAM009.Hot = Hot;
        //            objPAM009.ShellRollingNotApplicable = ShellRollingNotApplicable;
        //            objPAM009.ForgedShellWithProjection = ForgedShellWithProjection;
        //            objPAM009.ForgedShellWithIntegralNub = ForgedShellWithIntegralNub;
        //            objPAM009.ThkOnPlate = ThkOnPlate;
        //            objPAM009.NoInsideProjection = NoInsideProjection;

        //            objPAM009.NubShellNotApplicable = NubShellNotApplicable;
        //            objPAM009.SinglePiece = SinglePiece;
        //            objPAM009.Piece2 = Piece2;
        //            objPAM009.Piece3 = Piece3;
        //            objPAM009.TubeSheetNotApplicable = TubeSheetNotApplicable;
        //            objPAM009.TubeSheetOthers = TubeSheetOthers;
        //            objPAM009.SingleLayer = SingleLayer;
        //            objPAM009.DoubleLayer = DoubleLayer;
        //            objPAM009.Clad = Clad;
        //            objPAM009.RefractoryAtShop = RefractoryAtShop;
        //            objPAM009.RefractoryAtSite = RefractoryAtSite;
        //            objPAM009.Liner = Liner;
        //            objPAM009.OverlayNotApplicable = OverlayNotApplicable;
        //            objPAM009.Part4 = Part4;
        //            objPAM009.Part5 = Part5;
        //            objPAM009.CatalystNotApplicable = CatalystNotApplicable;
        //            objPAM009.SinglePiece2 = SinglePiece2;
        //            objPAM009.Half2 = Half2;
        //            objPAM009.Petals4 = Petals4;
        //            objPAM009.Petals6 = Petals6;
        //            objPAM009.HeadConstructionOthers = HeadConstructionOthers;
        //            objPAM009.WithCrown = WithCrown;
        //            objPAM009.WithoutCrown = WithoutCrown;
        //            objPAM009.HeadConstructionCold = HeadConstructionCold;
        //            objPAM009.HeadConstructionWarm = HeadConstructionWarm;
        //            objPAM009.HeadConstructionHot = HeadConstructionHot;
        //            objPAM009.FormingInIndia = FormingInIndia;
        //            objPAM009.ReadyHeadImported = ReadyHeadImported;
        //            // objPAM009.LSRYes = LSRYes;
        //            // objPAM009.LSRNo = LSRNo;
        //            objPAM009.InternalYes = InternalYes;
        //            objPAM009.InternalNo = InternalNo;
        //            objPAM009.SiteInstallationScope = SiteInstallationScope;
        //            objPAM009.ThirdPartyInspectionIncludedYes = ThirdPartyInspectionIncludedYes;
        //            objPAM009.ThirdPartyInspectionIncludedNo = ThirdPartyInspectionIncludedNo;
        //            objPAM009.ProjectCostEstimationSheetInLNYes = ProjectCostEstimationSheetInLNYes;
        //            objPAM009.ProjectCostEstimationSheetOnLNNo = ProjectCostEstimationSheetOnLNNo;
        //            objPAM009.ProjectCostEstimationSheetOnLNRemark = ProjectCostEstimationSheetOnLNRemark;
        //            objPAM009.DeliveryCommitted = DeliveryCommitted;
        //            objPAM009.OtherPoints = Convert.ToString(OtherPoints);
        //            /*objPAM009.Plates_supplier = Plates_supplier;
        //            // objPAM009.Plates_Offeredsize_Qty = Convert.ToInt32(Plates_Offeredsize_Qty);
        //            if (Plates_Offeredsize_Qty != "")
        //            {
        //                objPAM009.Plates_Offeredsize_Qty = Convert.ToInt32(Plates_Offeredsize_Qty);
        //            }
        //            objPAM009.Plates_Deviation = plates_deviation;
        //            objPAM009.Plates_Deviation_rolling = plates_deviation_rolling;
        //            objPAM009.Forged_shell_supplier = forged_shell_supplier;
        //            if (forged_shell_offeredsize_qty != "")
        //            {
        //                objPAM009.Forged_shell_Offeredsize_Qty = Convert.ToInt32(forged_shell_offeredsize_qty);
        //            }
        //            // objPAM009.Forged_shell_Offeredsize_Qty = Convert.ToInt32(Forged_shell_Offeredsize_Qty);
        //            objPAM009.Forged_shell_Deviation=forged_shell_deviation;
        //            objPAM009.Forged_shell_Deviation_rolling = forged_shell_deviation_rolling;
        //            objPAM009.Tube_sheet_supplier = tube_sheet_supplier;
        //            if (tube_sheet_shell_offeredsize_qty != "")
        //            {
        //                objPAM009.Tube_sheet_shell_Offeredsize_Qty = Convert.ToInt32(tube_sheet_shell_offeredsize_qty);
        //            }

        //            //  objPAM009.Tube_sheet_shell_Offeredsize_Qty = Convert.ToInt32(Tube_sheet_shell_Offeredsize_Qty);
        //            objPAM009.Tube_sheet_shell_Deviation = tube_sheet_shell_deviation;
        //            objPAM009.Tube_sheet_shell_Deviation_rolling = tube_sheet_shell_deviation_rolling;*/
        //            objPAM009.local_regulation_others = local_regulation_others;
        //            objPAM009.Tube_sheet_construction_others = Tube_sheet_construction_others;
        //            objPAM009.Site_installation = Site_installation;
        //            var pam = db.PAM001.Where(m => m.PAM == txtPAMNo).FirstOrDefault();
        //            objPAM009.HeaderId = Convert.ToInt32(pam.HeaderId);
        //            db.PAM009.Add(objPAM009);
        //            db.SaveChanges();
        //            var getpdhid = db.PAM009.Where(x => x.CreatedBy == objClsLoginInfo.UserName).OrderByDescending(x => x.PDHId).FirstOrDefault();

        //            //foreach (var item in listCheckListModel)
        //            //{
        //            //    PAM010 pam010 = new PAM010();
        //            //    pam010.ChecklistId = item.ChecklistId;
        //            //    pam010.CreatedBy = objClsLoginInfo.UserName;
        //            //    pam010.CreatedOn = DateTime.Now;
        //            //    pam010.HeaderId = objPAM009.HeaderId;
        //            //    pam010.IsAvailable = item.IsAvailable;
        //            //    pam010.PDHId = objPAM009.PDHId;
        //            //    pam010.ProposalDesign = objPAM009.ProposalDesign;
        //            //    pam010.Remarks = item.Remarks != null ? item.Remarks : string.Empty;
        //            //    db.PAM010.Add(pam010);
        //            //}
        //            // db.SaveChanges();

        //            objResponseMsg.PDHId = getpdhid.PDHId;
        //            objResponseMsg.HeaderId = objPAM009.HeaderId;
        //            objResponseMsg.Key = true;
        //            objResponseMsg.Value = "Proposal Design Handover Save Successfully";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}

        public ActionResult PDHAttachmentPartial(int? id)
        {
            PAM010 objPAM010 = db.PAM010.Where(x => x.LineId == id).FirstOrDefault();
            return PartialView("_PDHAttachmentPartial", objPAM010);
        }

        //[HttpPost]
        //public ActionResult SavePDHAttachment(string id, bool hasAttachments, Dictionary<string, string> Attach)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    int LineId = 0;
        //    try
        //    {
        //        var folderPath = "PAM010/" + id;
        //        Manager.ManageDocuments(folderPath, hasAttachments, Attach, objClsLoginInfo.UserName);                
        //        objResponseMsg.Key = true;
        //        objResponseMsg.Value = "File Uploaded Successfully";
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = ex.ToString();
        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}

        [HttpPost]
        public ActionResult CheckPDHAlreadyCreateForHeaderId(int HeaderId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objPAM009 = db.PAM009.Where(i => i.HeaderId == HeaderId).FirstOrDefault();
                if (objPAM009 != null && objPAM009.PDHId > 0)
                {
                    var PAM = db.PAM001.FirstOrDefault(i => i.HeaderId == HeaderId).PAM;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = PAM;// "Proposal Design Handover Already Exist For " + PAM + ".";
                }
                else
                {
                    objResponseMsg.Key = false;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveLines(FormCollection fc, int Id = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int LineId = Id;
                // int HeaderId = Convert.ToInt32(fc["PDHId" + Id]);
                int PDHID = Convert.ToInt32(Session["pdh"]);
                PAM009 objPAM009 = db.PAM009.Where(x => x.PDHId == PDHID).FirstOrDefault();

                PAM010 objPAM010 = null;
                if (LineId > 0)
                {
                    objPAM010 = db.PAM010.Where(x => x.LineId == LineId).FirstOrDefault();
                }
                else
                {
                    objPAM010 = new PAM010();
                }
                objPAM010.PDHId = PDHID;
                objPAM010.ProposalDesign = objPAM009.ProposalDesign;
                objPAM010.HeaderId = objPAM010.HeaderId;

                objPAM010.Description = fc["Description" + LineId];
                objPAM010.DocNo = fc["DocNo" + LineId];
                objPAM010.DocRev = fc["DocRev" + LineId];
                objPAM010.Pages = fc["Pages" + LineId];
                //objPAM012.IncludeInDIS = fc["IncludeInDIS" + LineId];
                objPAM010.Remarks = fc["Remarks" + LineId];
                if (LineId > 0)
                {
                    objPAM010.EditedBy = objClsLoginInfo.UserName;
                    objPAM010.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {
                    objPAM010.CreatedBy = objClsLoginInfo.UserName;
                    objPAM010.CreatedOn = DateTime.Now;
                    db.PAM010.Add(objPAM010);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateAccepted(int LineId, bool IsAccepted)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PAM010 objPAM010 = db.PAM010.Where(x => x.LineId == LineId).FirstOrDefault();
                objPAM010.IsAccepted = IsAccepted;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        public ActionResult UpdateLineRemarks(int LineId, string Remarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PAM010 objPAM010 = db.PAM010.Where(x => x.LineId == LineId).FirstOrDefault();
                objPAM010.Remarks = Remarks;
                objPAM010.EditedBy = objClsLoginInfo.UserName;
                objPAM010.EditedOn = DateTime.Now;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg);
        }

        [SessionExpireFilter]
        public ActionResult ApprovePDH(string Project)
        {
            ViewBag.chkProject = Project;
            return View();
        }

        [SessionExpireFilter]
        public ActionResult ApproveDssDetails(int id, string Project)
        {
            PAM009 objPAM009 = db.PAM009.FirstOrDefault(x => x.PDHId == id);
            ViewBag.IsApproval = true;
            ViewBag.chkProject = Project;
            return View(objPAM009);
        }

        [SessionExpireFilter, HttpPost]
        public ActionResult ApprovePDHDetailById(int PDHId, string Project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (db.PAM010.Where(i => i.PDHId == PDHId && (i.IsAccepted == null || i.IsAccepted == false)).Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "All Line Must Be Accepted For Approve Proposal Design Handover.";
                }
                else
                {
                    db.SP_PAM_PDH_APPROVE(PDHId, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Proposal Design Handover Approved Successfully.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Proposal Design Handover Not Approved Successfully";
            }
            ViewBag.chkProject = Project;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [SessionExpireFilter, HttpPost]
        public ActionResult ReturnPDHDetailById(int PDHId, string Remarks, string Project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PAM009 objPAM009 = new PAM009();
            try
            {
                if (PDHId > 0)
                {
                    if (Remarks != "")
                    {
                        objPAM009 = db.PAM009.Where(i => i.PDHId == PDHId).FirstOrDefault();
                        objPAM009.Status = clsImplementationEnum.NDETechniqueStatus.Returned.GetStringValue();
                        objPAM009.ReturnRemark = Remarks;
                        objPAM009.ReturnedBy = objClsLoginInfo.UserName;
                        objPAM009.ReturnedOn = DateTime.Now;
                        objPAM009.EditedBy = objClsLoginInfo.UserName;
                        objPAM009.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Return.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Please Enter Remarks";
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            ViewBag.chkProject = Project;
            return Json(objResponseMsg);
        }


        public ActionResult Revice(int PDHId, string Project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            PAM009 objPAM009 = new PAM009();
            try
            {
                if (PDHId > 0)
                {

                    int id = Convert.ToInt32(PDHId);
                    objPAM009 = db.PAM009.Where(i => i.PDHId == id).FirstOrDefault();
                    if (objPAM009.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
                    {
                        objPAM009.ProposalDesignRev = Convert.ToInt32(objPAM009.ProposalDesignRev) + 1;
                        objPAM009.ReturnRemark = null;
                        objPAM009.ReturnedBy = null;
                        objPAM009.ReturnedOn = null;
                        objPAM009.SubmittedBy = null;
                        objPAM009.SubmittedOn = null;
                        objPAM009.ApprovedBy = null;
                        objPAM009.ApprovedOn = null;
                        objPAM009.EditedBy = objClsLoginInfo.UserName;
                        objPAM009.EditedOn = DateTime.Now;
                        objPAM009.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.PTTechniqueMeassage.Save.ToString();
                    }


                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            ViewBag.chkProject = Project;
            return Json(objResponseMsg);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
            {
                var lstResult = db.SP_PAM_PDH_GETPDHDetails(1, int.MaxValue, strSortOrder, whereCondition).Select(x => new { x.ProposalDesign, x.Status, x.ConDesc, x.CustomerDesc, x.LOINo, x.LOIDate, x.CustomerOrderNo, x.CustomerOrderDate, x.CDD, x.ProposalDesignRev }).ToList();

                if (lstResult != null && lstResult.Count > 0)
                {
                    string str = Helper.GenerateExcel(lstResult, objClsLoginInfo.UserName);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = str;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Data not available.";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Data not available.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region PDH Supplier, Deviation, Remarks Datagrid
        public ActionResult loadPDHRemarks(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                bool isEditEnabled = true;
                int PDHId = Convert.ToInt32(param.CTQHeaderId);
                string whereCondition = "1=1";
                whereCondition += " and PDHId= " + PDHId.ToString();

                if (PDHId > 0)
                {
                    PAM009 objPAM009 = db.PAM009.Where(i => i.PDHId == PDHId).FirstOrDefault();
                    if (objPAM009.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower() ||
                        objPAM009.Status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
                    {
                        isEditEnabled = false;
                    }
                }

                string[] columnName = { "Component", "SupplierDetails", "DeviationDetails", "Remarks" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstPam = db.SP_PDH_GET_SUPPLIER_DEVIATION_REMARKS_DETAILS(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Convert.ToString(h.PDHId),
                                    h.ParentId == 0 ? "<nobr><img onclick='ExpandCollapsChild("+ h.LineId +", this)' src='"+WebsiteURL+"/Images/details_close.png' /> "+ Helper.GenerateActionIcon(h.LineId,"Add componenet", "Add "+h.Component, "fa fa-plus", "AddComponentDetails("+h.LineId+",'"+h.Component+"')") + "</nobr>" : "<span class='"+ h.ParentId +" child' ></span>",
                                    h.ParentId == 0 ? Convert.ToString(h.Component) : "",
                                    Convert.ToString(h.SupplierDetails),
                                    Convert.ToString(h.DeviationDetails),
                                    Convert.ToString(h.Remarks)
                        }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ComponentDetailsPartial(int LineId, string ComponentName)
        {
            ViewBag.ComponentName = ComponentName;
            ViewBag.LineId = LineId;
            return PartialView("_ComponentDetailsPartial");
        }

        public ActionResult AddComponentDetails(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                bool isEditEnabled = true;
                int PDHId = Convert.ToInt32(param.CTQHeaderId);
                int ParentId = Convert.ToInt32(param.CTQLineHeaderId);
                string ComponentName = param.Department;
                string whereCondition = "1=1";
                whereCondition += " and PDHId= " + PDHId.ToString() + " and ParentId > 0 and Component='" + ComponentName + "'";

                PAM009 objPAM009 = db.PAM009.Where(i => i.PDHId == PDHId).FirstOrDefault();
                if (objPAM009.Status.ToLower() == clsImplementationEnum.PAMStatus.Approved.GetStringValue().ToLower() ||
                    objPAM009.Status.ToLower() == clsImplementationEnum.PAMStatus.SentForApproval.GetStringValue().ToLower())
                {
                    isEditEnabled = false;
                }

                string[] columnName = { "Component", "SupplierDetails", "DeviationDetails", "Remarks" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var lstPam = db.SP_PDH_GET_SUPPLIER_DEVIATION_REMARKS_DETAILS(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                int newRecordId = 0;
                var newRecord = new[] {
                                    "0",
                                    "0",
                                    Helper.GenerateHidden(newRecordId,"PDHId",Convert.ToString(PDHId)),
                                    GenerateTextboxFor(newRecordId,"Component",ComponentName,"","",true,"","30",false),
                                    GenerateTextboxFor(newRecordId,"SupplierDetails","","","",false,"","30",false),
                                    GenerateTextboxFor(newRecordId,"DeviationDetails","","","",false,"","30",false),
                                    GenerateTextboxFor(newRecordId,"Remarks","","","",false,"","30",false),
                                    Helper.GenerateActionIcon(newRecordId, "Add", "Add Rocord", "fa fa-plus", "SaveComponentLineRecord(0)")
                                };

                var res = (from h in lstPam
                           select new[] {
                                    Convert.ToString(h.ROW_NO),
                                    Convert.ToString(h.LineId),
                                    Helper.GenerateHidden(h.LineId,"PDHId",Convert.ToString(h.PDHId)),
                                    GenerateTextboxFor(h.LineId,"Component",h.Component+"","","",true,"","30",false),
                                    GenerateTextboxFor(h.LineId,"SupplierDetails",h.SupplierDetails+"","","",false,"","30",false),
                                    GenerateTextboxFor(h.LineId,"DeviationDetails",h.DeviationDetails+"","","",false,"","30",false),
                                    GenerateTextboxFor(h.LineId,"Remarks",h.Remarks+"","","",false,"","30",false),
                                    Helper.GenerateActionIcon(h.LineId, "Edit", "Edit Rocord", "fa fa-pencil-square-o", "SaveComponentLineRecord("+h.LineId+")" )  +Helper.GenerateActionIcon(h.LineId, "Delete", "Delete Rocord", "fa fa-trash", "DeleteComponentRecord("+h.LineId+")" )
                                }).ToList();

                res.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveComponentDetails(FormCollection fc, int LineId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int ParentId = Convert.ToInt32(fc["ParentId"]);
                int PDHID = Convert.ToInt32(fc["PDHId" + LineId]);
                PAM009 objPAM009 = db.PAM009.Where(x => x.PDHId == PDHID).FirstOrDefault();

                PAM022 objPAM022 = null;
                if (LineId > 0)
                {
                    objPAM022 = db.PAM022.Where(x => x.LineId == LineId).FirstOrDefault();
                }
                else
                {
                    objPAM022 = new PAM022();
                }
                objPAM022.PDHId = PDHID;
                objPAM022.Component = fc["Component" + LineId];
                objPAM022.SupplierDetails = fc["SupplierDetails" + LineId];
                objPAM022.DeviationDetails = fc["DeviationDetails" + LineId];
                objPAM022.Remarks = fc["Remarks" + LineId];
                objPAM022.ParentId = ParentId;
                if (LineId > 0)
                {
                    objPAM022.EditedBy = objClsLoginInfo.UserName;
                    objPAM022.EditedOn = DateTime.Now;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }
                else
                {
                    objPAM022.CreatedBy = objClsLoginInfo.UserName;
                    objPAM022.CreatedOn = DateTime.Now;
                    db.PAM022.Add(objPAM022);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();
                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteComponenet(int LineId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objPAM022 = db.PAM022.FirstOrDefault(f => f.LineId == LineId);

                if (objPAM022 != null)
                {
                    db.PAM022.Remove(objPAM022);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record Deleted Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Record not found or already deleted";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public JsonResult AddNewComponent(string ComponentName, int PDHId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                PAM022 objPAM022 = new PAM022();
                objPAM022.Component = ComponentName;
                objPAM022.CreatedBy = objClsLoginInfo.UserName;
                objPAM022.CreatedOn = DateTime.Now;
                objPAM022.PDHId = PDHId;
                db.PAM022.Add(objPAM022);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Component Added Successfully!";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }


    public class CheckListModel
    {
        public int ChecklistId { get; set; }
        public string Remarks { get; set; }
        public bool IsAvailable { get; set; }
    }

    public class PDHHeaderDtl
    {
        public string Contract { get; set; }
        public string PAMNo { get; set; }
        public string Customer { get; set; }
        public string ContractNo { get; set; }
        public string ProposalDesign { get; set; }
        public string ProposalDesignRev { get; set; }
        public string CreatedOn { get; set; }
        public string Status { get; set; }
        public string HeaderId { get; set; }
        public string PDHId { get; set; }
        public string ReturnRemarks { get; set; }

        public bool HPC1 { get; set; }
        public bool HPC3 { get; set; }
        public bool Combination { get; set; }
        public bool ReferPlanningInput { get; set; }
        public bool RollingCapacityNotAplicate { get; set; }
        public string ManufacturingCode { get; set; }
        public string CodeEdition { get; set; }
        public bool ForgedShell { get; set; }
        public bool ASMEYes { get; set; }
        public bool ASMENo { get; set; }
        public bool ShellConstructionMonowall { get; set; }
        public bool ShellConstructionMultiwall { get; set; }
        public bool IBR { get; set; }
        public bool PEDATEX { get; set; }
        public bool CRN { get; set; }
        public bool DOSH { get; set; }
        public bool SELO { get; set; }
        public bool GOST { get; set; }
        public bool LocalRegulationOthers { get; set; }
        public bool Warm { get; set; }
        public bool Cold { get; set; }
        public bool Hot { get; set; }
        public bool ShellRollingNotApplicable { get; set; }
        public bool ForgedShellWithProjection { get; set; }
        public bool ForgedShellWithIntegralNub { get; set; }
        public bool ThkOnPlate { get; set; }
        public bool NoInsideProjection { get; set; }
        public bool NubShellNotApplicable { get; set; }
        public bool SinglePiece { get; set; }
        public bool Piece2 { get; set; }
        public bool Piece3 { get; set; }
        public bool TubeSheetNotApplicable { get; set; }
        public bool TubeSheetOthers { get; set; }
        public bool SingleLayer { get; set; }
        public bool DoubleLayer { get; set; }
        public bool Clad { get; set; }
        public bool RefractoryAtShop { get; set; }
        public bool RefractoryAtSite { get; set; }
        public bool Liner { get; set; }
        public bool OverlayNotApplicable { get; set; }
        public bool Part4 { get; set; }
        public bool Part5 { get; set; }
        public bool CatalystNotApplicable { get; set; }
        public bool SinglePiece2 { get; set; }
        public bool Half2 { get; set; }
        public bool Petals4 { get; set; }
        public bool Petals6 { get; set; }

        public bool HeadConstructionOthers { get; set; }
        public bool WithCrown { get; set; }
        public bool WithoutCrown { get; set; }
        public bool HeadConstructionCold { get; set; }
        public bool HeadConstructionWarm { get; set; }
        public bool HeadConstructionHot { get; set; }
        public bool FormingInIndia { get; set; }
        public bool ReadyHeadImported { get; set; }

        public bool LSRYes { get; set; }
        public bool LSRNo { get; set; }
        public bool InternalYes { get; set; }
        public bool InternalNo { get; set; }
        public bool SiteInstallationScope { get; set; }

        public bool ThirdPartyInspectionIncludedYes { get; set; }
        public bool ThirdPartyInspectionIncludedNo { get; set; }
        public bool ProjectCostEstimationSheetInLNYes { get; set; }
        public bool ProjectCostEstimationSheetOnLNNo { get; set; }
        public string ProjectCostEstimationSheetOnLNRemark { get; set; }
        public int? DeliveryCommitted { get; set; }
        public string OtherPoints { get; set; }
    }

}