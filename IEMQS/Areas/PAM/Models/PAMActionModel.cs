﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.PAM.Models
{
    public class PAMActionModel : clsBase
    {  
        public void Add(string pam , string comments)
        {
            db.SP_PAM_ADD_ACTION(pam, objClsLoginInfo.UserName, "CREATE", comments);
        }
        public void Update(string pam, string comments)
        {
            db.SP_PAM_ADD_ACTION(pam, objClsLoginInfo.UserName, "UPDATE", comments);
        }
        public void Delete(string pam, string comments)
        {
            db.SP_PAM_ADD_ACTION(pam, objClsLoginInfo.UserName, "DELETE", comments);
        }
        public void Approve(string pam, string comments)
        {
            db.SP_PAM_ADD_ACTION(pam, objClsLoginInfo.UserName, "APPROVE", comments);
        }
        public void Return(string pam, string comments)
        {
            db.SP_PAM_ADD_ACTION(pam, objClsLoginInfo.UserName, "RETURN", comments);
        }
        public void Revise(string pam, string comments)
        {
            db.SP_PAM_ADD_ACTION(pam, objClsLoginInfo.UserName, "REVISE", comments);
        }

        public void SentForApproval(string pam, string comments)
        {
            db.SP_PAM_ADD_ACTION(pam, objClsLoginInfo.UserName, "SENTFORAPPROVAL", comments);
        }


    }
}