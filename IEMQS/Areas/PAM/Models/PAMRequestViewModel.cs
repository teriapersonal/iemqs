﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.PAM.Models
{
    public class PAMRequestViewModel
    {
        public string ContractNo { get; set; }
        public string ContractDesc { get; set; }
        public string Customer { get; set; }
        public string CustomerDesc { get; set; }
        [Required]
        public string BUHead { get; set; }
        [Required]
        public string ExpectedDateOfPO { get; set; }

        public string Comments { get; set; }

    }
}