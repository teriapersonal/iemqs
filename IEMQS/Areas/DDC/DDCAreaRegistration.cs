﻿using System.Web.Mvc;

namespace IEMQS.Areas.DDC
{
    public class DDCAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "DDC";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "DDC_default",
                "DDC/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}