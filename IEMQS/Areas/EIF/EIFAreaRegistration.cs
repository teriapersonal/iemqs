﻿using System.Web.Mvc;

namespace IEMQS.Areas.EIF
{
    public class EIFAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "EIF";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "EIF_default",
                "EIF/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}