﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.JPP.Controllers
{
    [SessionExpireFilter]
    public class PrintHistoryController : clsBase
    {
        // GET: JPP/Print
        public ActionResult Index(int HeaderId)
        {
            JPP001_Log objJPP001 = db.JPP001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
            var details = getCodeValue(objJPP001.Project, objJPP001.ApprovedBy, objJPP001.CreatedBy);
            objJPP001.Project = details.projdesc;
            objJPP001.Customer = details.custdesc;
            objJPP001.CreatedBy = details.createdesc;
            objJPP001.ApprovedBy = details.appdesc;
            ViewBag.objJPP002 = db.JPP002_Log.Where(q => q.RefId == objJPP001.Id).ToList();
            ViewBag.objJPP003 = db.JPP003_Log.Where(q => q.RefId == objJPP001.Id).ToList();
            ViewBag.objJPP004 = db.JPP004_Log.Where(q => q.RefId == objJPP001.Id).ToList();
            ViewBag.objJPP005 = db.JPP005_Log.Where(q => q.RefId == objJPP001.Id).ToList();
            ViewBag.objJPP006 = db.JPP006_Log.Where(q => q.RefId == objJPP001.Id).ToList();
            ViewBag.objJPP007 = db.JPP007_Log.Where(q => q.RefId == objJPP001.Id).ToList();
            ViewBag.objJPP008 = db.JPP008_Log.Where(q => q.RefId == objJPP001.Id).ToList();
            ViewBag.objJPP009 = db.JPP009_Log.Where(q => q.RefId == objJPP001.Id).ToList();
            return View(objJPP001);
        }

        public dynamic getCodeValue(string project, string approver, string create)
        {
            JPP001Extend result = new JPP001Extend();
            try
            {
                result.custdesc = Manager.GetCustomerCodeAndNameByProject(project);
                result.createdesc = db.COM003.Where(i => i.t_psno == create && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
                result.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                result.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex) { Elmah.ErrorSignal.FromCurrentContext().Raise(ex); }
            return result;

        }

        private class JPP001Extend
        {
            public string custdesc { get; set; }
            public string createdesc { get; set; }
            public string projdesc { get; set; }
            public string appdesc { get; set; }

        }
    }
}