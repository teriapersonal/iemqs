﻿using System.Web.Mvc;

namespace IEMQS.Areas.JPP
{
    public class JPPAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "JPP";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "JPP_default",
                "JPP/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}