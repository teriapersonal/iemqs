﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using static IEMQSImplementation.clsImplementationEnum;

namespace IEMQS.Areas.COP.Controllers
{
    public class MaintainSpoolFabricationController : clsBase
    {
        // GET: COP/MaintainSpoolFabrication
        //[SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult IndexFSP()
        {
            ViewBag.Title = "Maintain Spool Fabrication Data";
            return View("Index");
        }

        //[UserPermissions]
        public ActionResult DisplaySFS()
        {
            ViewBag.Title = "Display session";
            ViewBag.IsDisplay = true;
            return View("Index");
        }

        [HttpPost]
        public ActionResult LoadDataGridPartial(bool isDisplay, string role, string status)
        {
            ViewBag.IsDisplayy = isDisplay;
            ViewBag.Role = role;
            ViewBag.Status = status;
            if (!string.IsNullOrEmpty(role))
            {
                if (role == "oplng")
                    ViewBag.IsDisplayy = false;
                else
                    ViewBag.IsDisplayy = true;
            }
            ViewBag.DrpDispatch = new string[] { clsImplementationEnum.Dispatch.Yes.GetStringValue(), clsImplementationEnum.Dispatch.No.GetStringValue() };
            ViewBag.DrpOverlay = new string[] { clsImplementationEnum.Dispatch.Yes.GetStringValue(), clsImplementationEnum.Dispatch.No.GetStringValue() };
            return PartialView("_GetGridDataPartial");
        }

        [HttpPost]
        public ActionResult LoadDataGridData(JQueryDataTableParamModel param, string Status)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhereCondition += "1=1 ";

                if (Status.ToLower() == "SpoolCompleted".ToLower())
                {
                    strWhereCondition += " and DISP='Yes' ";
                }
                else
                {
                    strWhereCondition += " and ISNULL(DISP,'No')='No' ";
                }

                #endregion

                //search Condition 

                string[] columnName = {
                    "Project"
                    ,"c3.QualityProject"
                    ,"c3.Mark"
                    ,"Overlay"
                    ,"c3.Comp_Assly"
                    ,"c3.TypeOfComponent"
                    ,"DISP"
                    ,"PlannedReceipt"
                    ,"RequestedOn"
                    ,"Cycletime"
                    ,"WeekNo"
                    ,"IMRStatus"
                    ,"DispatchDate"
                };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_COP_SFS_GET_INDEX_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                var dtPlannedReceipt = new DateTime();
                var data = (from uc in lstResult
                            select new
                            {
                                HeaderId = Convert.ToString(uc.HeaderId),
                                Project = uc.Project,
                                QualityProject = uc.QualityProject,
                                Mark = uc.Mark,
                                Overlay = uc.Overlay,
                                Comp_Assly = uc.Comp_Assly,
                                TypeOfComponent = uc.TypeOfComponent,
                                PlannedReceipt = (DateTime.TryParse(uc.PlannedReceipt + "", out dtPlannedReceipt) ? dtPlannedReceipt.ToString("dd/MM/yyyy") : uc.PlannedReceipt),
                                RequestedOn = Convert.ToString(uc.RequestedOn.HasValue ? uc.RequestedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""),
                                Cycletime = Convert.ToString(uc.Cycletime),
                                WeekNo = Convert.ToString(uc.WeekNo),
                                IMRStatus = uc.IMRStatus,
                                Seam = "<a id='Seam" + uc.HeaderId + "' name='Seam' title='Seam details' class='blue action' onclick='Seamlist(" + uc.RefHeaderId + ")'><i class='fa fa-th'></i></a>"+
                                " &nbsp; &nbsp; <a id='PartMaterial" + uc.HeaderId + "' name='PartMaterial' title='Part Material' class='blue action' onclick='partmaterial(" + uc.HeaderId + ")'><i class='fa fa-chain-broken'></i></a>",//
                                IsPlanned = GenerateCheckbox(uc.HeaderId, "IsPlanned", uc.IsPlanned, true),
                                DISP = uc.DISP,
                                DispatchDate = uc.DispatchDate.HasValue ? uc.DispatchDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : "",
                                RefHeaderId = uc.RefHeaderId
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, string role, bool isReadOnly = false)
        {
            try
            {
                //var data = new List<a>();

                var lstResult = db.SP_COP_SFS_GET_INDEX_DATA(1, 0, "", "HeaderId = " + id).Take(1).ToList();
                var dtPlannedReceipt = new DateTime();
                var data = (from uc in lstResult
                            select new
                            {
                                HeaderId = uc.HeaderId.ToString(),
                                Project = uc.Project,
                                QualityProject = uc.QualityProject,
                                Mark = uc.Mark,
                                Overlay = (role == "oplng" && !isReadOnly) ? Helper.HTMLAutoComplete(uc.HeaderId, "Overlay", "", "UpdateData(this, " + uc.HeaderId + ",true)", false, "", "", false, "", "", "Overlay", "form-control editable") : uc.Overlay,
                                Comp_Assly = uc.Comp_Assly,
                                TypeOfComponent = uc.TypeOfComponent,
                                PlannedReceipt = (DateTime.TryParse(uc.PlannedReceipt + "", out dtPlannedReceipt) ? dtPlannedReceipt.ToString("dd/MM/yyyy") : uc.PlannedReceipt),
                                RequestedOn = Convert.ToString(uc.RequestedOn.HasValue ? uc.RequestedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""),
                                Cycletime = Convert.ToString(uc.Cycletime),
                                WeekNo = Convert.ToString(uc.WeekNo),
                                IMRStatus = uc.IMRStatus,
                                Seam = "<span><a id='Seam" + uc.HeaderId + "' name='Seam' title='Seam details' class='blue action' onclick='Seamlist(" + uc.RefHeaderId + ")'>" +
                                       "<i class='fa fa-th'></i></a>",//Helper.GenerateGridButton(uc.HeaderId, "Seam", "Seam details", "fa fa-th", "Seamlist(" + uc.RefHeaderId + ");"),//SRE_CMD.Avoid.GetStringValue(),
                                IsPlanned = (role == "oplng" && !isReadOnly) ? Helper.GenerateCheckboxWithEvent(uc.HeaderId, "IsPlanned", uc.IsPlanned, "IsPlannedChanged(this,\"IsPlanned\", " + uc.HeaderId + ")", true) : GenerateCheckbox(uc.HeaderId, "IsPlanned", uc.IsPlanned, true),
                                DISP = (role == "oplng" && !isReadOnly) ? Helper.HTMLAutoComplete(uc.HeaderId, "DISP", "", "UpdateData(this, " + uc.HeaderId + ",true)", false, "", "", false, "", "", "Dispacth", "form-control editable") : uc.DISP,
                                DispatchDate = uc.DispatchDate.HasValue ? uc.DispatchDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : "",
                                RefHeaderId = uc.RefHeaderId
                            }).ToList();

                return Json(new { aaData = data }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            var objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_COP_SFS_GET_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var dtPlannedReceipt = new DateTime();
                var data = (from uc in lst
                            select new
                            {
                                Project = uc.Project,
                                QualityProject = uc.QualityProject,
                                Mark = uc.Mark,
                                Overlay = uc.Overlay,
                                Comp_Assly = uc.Comp_Assly,
                                TypeOfComponent = uc.TypeOfComponent,
                                PlannedReceipt = (DateTime.TryParse(uc.PlannedReceipt + "", out dtPlannedReceipt) ? dtPlannedReceipt.ToString("dd/MM/yyyy") : uc.PlannedReceipt),
                                RequestedOn = uc.RequestedOn.HasValue ? uc.RequestedOn.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : "",
                                Cycletime = uc.Cycletime,
                                WeekNo = uc.WeekNo,
                                IMRStatus = uc.IMRStatus,
                                IsPlanned = uc.IsPlanned ? "Yes" : "No",
                                DISP = uc.DISP,
                                DispatchDate = uc.DispatchDate.HasValue ? uc.DispatchDate.Value.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture) : ""
                            }).ToList();

                strFileName = Helper.GenerateExcel(data, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult ExportSeamWiseStageList(string whereCondition, string strSortOrder)
        {
            var objResponseMsg = new clsHelper.ResponseMsg();
            List<SpoolData> lstSpoolData = new List<SpoolData>();
            try
            {
                var lst = db.SP_COP_SFS_GET_INDEX_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                foreach (var item in lst)
                {
                    var lstcop = db.COP003.Where(x => x.RefHeaderId == item.RefHeaderId).Select(x => new { Project = x.Project, QualityProject = x.QualityProject, Part = x.PartNos }).FirstOrDefault();

                    var lstPam = db.SP_COP_GET_SEAM_AND_STAGE_LIST(1, int.MaxValue, "", "", item.Project, item.QualityProject, lstcop.Part).ToList();
                    List<SeamPartWiseStageListEnt> listSeamWiseStageSeamListEnt = new List<SeamPartWiseStageListEnt>();
                    foreach (var SeamNo in lstPam.Select(i => i.SeamNo).Distinct())
                    {
                        List<SeamPartWiseStageStageListEnt> listSeamPartWiseStageStageListEnt = new List<SeamPartWiseStageStageListEnt>();
                        foreach (var stages in lstPam.Where(i => i.SeamNo == SeamNo))
                        {
                            listSeamPartWiseStageStageListEnt.Add(new SeamPartWiseStageStageListEnt
                            {
                                InspectionStatus = stages.InspectionStatus,
                                StageCode = stages.StageCode,
                                StageDesc = stages.StageDesc,
                            });
                        }
                        listSeamWiseStageSeamListEnt.Add(new SeamPartWiseStageListEnt { SeamNoPartNo = SeamNo, listSeamPartWiseStageStageListEnt = listSeamPartWiseStageStageListEnt });
                    }
                    lstSpoolData.Add(new SpoolData
                    {
                        listSeamPartWiseStageListEnt = listSeamWiseStageSeamListEnt,
                        project = item.Project,
                        qualityproject = item.QualityProject,
                        mark = item.Mark,
                        Overlay = item.Overlay,
                        comp_assly = item.Comp_Assly,
                        typeofcomponent = item.TypeOfComponent,
                        plannedreceipt = item.PlannedReceipt,
                        requestedon = Convert.ToString(item.RequestedOn),
                        cycletime = Convert.ToString(item.Cycletime),
                        weekno = Convert.ToString(item.WeekNo),
                        imrstatus = Convert.ToString(item.IMRStatus),
                        disp = item.DISP
                    });
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return Json(lstSpoolData);
        }

        public JsonResult UpdateDeselectAll()
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string yes = clsImplementationEnum.Overlay.Yes.GetStringValue().ToString();

                var CompleteData = db.COP003.Where(x => x.DISP == yes).ToList();

                if (CompleteData.Count > 0)
                {
                    CompleteData.ForEach(u =>
                    {
                        u.IsPlanned = false;
                    });
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "SuccessFully DeSelected All";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Record found!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult PartMaterialTracking(int headerid)
        {
            int HeaderId = 0;
            string NodeId = "";
            var objCOP003 = db.COP003.FirstOrDefault(f => f.HeaderId == headerid);
            if (objCOP003 != null && !String.IsNullOrWhiteSpace(objCOP003.PartNos))
            {
                var findNos = objCOP003.PartNos.Split(',');
                var objFKM102 = db.FKM102.Where(f => findNos.Contains(f.FindNo) && f.Project == objCOP003.Project);
                if(db.FKM102.Any())
                {
                    NodeId =  String.Join(",", objFKM102.Select(s => s.ParentNodeId).Distinct().ToList());
                    HeaderId = objFKM102.FirstOrDefault().HeaderId;
                }
            }
            ViewBag.HeaderId = HeaderId;
            ViewBag.Nodeid = NodeId;
            ViewBag.IsFromCOP = true;
            return PartialView("~/Areas/FKMS/Views/MaintainFKMS/_PartMaterialTracking.cshtml");
        }

        [HttpPost]
        public ActionResult LoadPartMaterialTracking(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int Headerid = Convert.ToInt32(param.Headerid); 

                var mainproject = db.FKM101.Where(x => x.HeaderId == Headerid).Select(x => x.Project).FirstOrDefault();

                if (!string.IsNullOrEmpty(mainproject))
                {
                    string whereCondition = "1=1";

                    if (!string.IsNullOrWhiteSpace(param.sSearch))
                    {
                        string[] columnName = { "Element", "ElementDesc", "PartNo", "ItemID", "ItemDesc", "CompType", "Quantity", "Unit", "PCRNo", "PCLNo", "NestedQty", "Status", "PlannerStatus", "OutboundKey", "OutboundQty", "POOrderQty", "POBalance", "Qtytobereceived", "UnderInspection", "ClearedInventory", "IssuedQty", "PPOQtyBuyr" };
                        whereCondition += columnName.MakeDatatableSearchCondition(param.SearchFilter);
                    }
                    else
                        whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                    string strSortOrder = string.Empty;
                    if (!string.IsNullOrWhiteSpace(sortColumnName))
                    {
                        strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                    }
                    var lstPam = new List<SP_FKMS_GET_PROJECT_WISE_MATERIAL_TRACKING_Result>();
                    int? totalRecords = 0;
                    foreach (var Nodeid in param.Nodeid.Split(','))
                    {
                        var tempData = db.SP_FKMS_GET_PROJECT_WISE_MATERIAL_TRACKING(mainproject, Convert.ToInt32(Nodeid), StartIndex, 0, strSortOrder, whereCondition).ToList();
                        lstPam = lstPam.Concat(tempData).ToList();
                        totalRecords += tempData.Select(i => i.TotalCount).FirstOrDefault();
                    }
                    lstPam = lstPam.Skip(param.iDisplayStart).Take(param.iDisplayLength).ToList();
                    var res = (from h in lstPam
                               select new[] {
                               Convert.ToString(h.Element),
                               Convert.ToString(h.ElementDesc),
                               Convert.ToString(h.PartNo),
                               Convert.ToString(h.ItemID),
                               Convert.ToString(h.ItemDesc),
                               Convert.ToString(h.CompType),
                               Convert.ToString(h.Quantity),
                               Convert.ToString(h.Unit),
                               Convert.ToString(h.PCRNo),
                               Convert.ToString(h.PCLNo),
                               Convert.ToString(h.NestedQty),
                               Convert.ToString(h.Status),
                               Convert.ToString(h.PlannerStatus),
                               Convert.ToString(h.OutboundKey),
                               Convert.ToString(h.OutboundQty),
                               Convert.ToString(h.POOrderQty),
                               Convert.ToString(h.POBalance),
                               Convert.ToString(h.Qtytobereceived),
                               Convert.ToString(h.UnderInspection),
                               Convert.ToString(h.ClearedInventory),
                               Convert.ToString(h.IssuedQty),
                               Convert.ToString(h.PPOQtyBuyr)
                    }).ToList();

                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                        aaData = res,
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        sEcho = param.sEcho,
                        iTotalDisplayRecords = "0",
                        iTotalRecords = "0",
                        aaData = new string[0]
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = string.Empty,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[0]
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateDetails(int id, string columnName, string columnValue)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_COMMON_TABLE_UPDATE("cop003", id, "HeaderId", columnName, columnValue, objClsLoginInfo.UserName);
                    if (columnName.ToUpper() == "DISP" && columnValue.ToUpper() == "YES")
                    {
                        db.SP_COMMON_TABLE_UPDATE("cop003", id, "HeaderId", "DispatchDate", DateTime.Now.ToString("yyyy-MM-dd"), objClsLoginInfo.UserName);
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.doesnotSubmitDetails.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult updateSpoolFabData(int id, string columnValue, string columnName)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var spooldata = new COP003();
                var fabdata = db.COP003.FirstOrDefault(x => x.RefHeaderId == id);
                if (fabdata != null)
                {
                    if (fabdata.Comp_Assly.ToLower() == clsImplementationEnum.ComponentAssembly.Assembly.GetStringValue().ToLower() &&
                        columnValue.ToLower() == clsImplementationEnum.ComponentAssembly.Component.GetStringValue().ToLower())
                    {
                        db.COP003.Remove(fabdata);
                    }

                    switch (columnName.ToUpper())
                    {
                        case "QUALITYPROJECT":
                            fabdata.QualityProject = columnValue;
                            break;
                        case "MARK":
                            fabdata.Mark = columnValue;
                            break;
                        case "COMP_ASSLY":
                            fabdata.Comp_Assly = columnValue;
                            break;
                        case "TYPEOFCOMPONENT":
                            fabdata.TypeOfComponent = columnValue;
                            break;
                    }
                    fabdata.EditedBy = objClsLoginInfo.UserName;
                    fabdata.EditedOn = DateTime.Now;
                    var copdata = db.COP001.FirstOrDefault(x => x.HeaderId == id);

                    if (db.COP003.Any(x => x.QualityProject.ToLower() == fabdata.QualityProject.ToLower() &&
                    x.Mark.ToLower() == fabdata.Mark.ToLower() &&
                    x.Comp_Assly.ToLower() == fabdata.Comp_Assly.ToLower() &&
                    x.TypeOfComponent.ToLower() == fabdata.TypeOfComponent.ToLower() &&
                    x.RefHeaderId != fabdata.RefHeaderId))
                    {
                        db.COP003.Remove(fabdata);
                    }
                    else if (copdata.Comp_Assly == clsImplementationEnum.ComponentAssembly.Assembly.GetStringValue())
                    {
                        string partnos = string.Join(",", db.COP001.Where(w => w.Project == copdata.Project && w.Mark == copdata.Mark && w.BU == copdata.BU && w.Comp_Assly == copdata.Comp_Assly).Select(s => s.Part).Distinct());
                        fabdata.PartNos = partnos;

                        COP003[] lstcop003 = null;
                        lstcop003 = db.COP003.Where(x => x.Project == copdata.Project && x.BU == copdata.BU && x.Mark == copdata.Mark && x.Comp_Assly == copdata.Comp_Assly).ToArray();

                        for (int i = 0; i < lstcop003.Length; i++)
                        {
                            lstcop003[i].PartNos = partnos;
                        }
                    }
                }
                else
                {
                    var copdata = db.COP001.Where(x => x.HeaderId == id).FirstOrDefault();
                    spooldata.RefHeaderId = id;
                    spooldata.Project = copdata.Project;

                    if (!string.IsNullOrEmpty(copdata.QualityProject))
                        spooldata.QualityProject = copdata.QualityProject;
                    else
                        spooldata.QualityProject = string.Empty;

                    spooldata.Mark = copdata.Mark;
                    spooldata.Comp_Assly = copdata.Comp_Assly;
                    spooldata.TypeOfComponent = copdata.TypeOfComponent;
                    spooldata.DISP = "No";
                    spooldata.BU = copdata.BU;
                    spooldata.CreatedBy = objClsLoginInfo.UserName;
                    spooldata.CreatedOn = DateTime.Now;

                    if (copdata.Comp_Assly != null)
                    {
                        if (copdata.Comp_Assly.ToLower() == clsImplementationEnum.ComponentAssembly.Assembly.GetStringValue().ToLower())
                        {
                            string partnos = string.Join(",", db.COP001.Where(w => w.Project == copdata.Project && w.Mark == copdata.Mark && w.BU == copdata.BU && w.Comp_Assly == copdata.Comp_Assly).Select(s => s.Part).Distinct());
                            spooldata.PartNos = partnos;

                            if (!db.COP003.Any(x =>
                                        x.Project == spooldata.Project && x.QualityProject == spooldata.QualityProject &&
                                        x.Mark == spooldata.Mark && x.Comp_Assly == spooldata.Comp_Assly 
                                        //&& x.TypeOfComponent == spooldata.TypeOfComponent 
                                        ))
                            {
                                db.COP003.Add(spooldata);
                            }

                            //var lstcop = db.COP003.Where(x => x.Project.ToLower() == spooldata.Project.ToLower()).ToList();
                            //foreach (var item in lstcop)
                            //{
                            //    item.PartNos = partnos;
                            //}
                            //db.COP003.AddRange(lstcop);
                        }
                    }
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [NonAction]
        public static string GenerateCheckbox(int rowId, string columnName, bool columnValue = false, bool isDisabled = false)
        {
            string htmlControl = "";
            string inputID = columnName + "" + rowId.ToString();
            bool inputValue = columnValue;

            htmlControl = "<input type=\"checkbox\" id=\"" + inputID + "\"" + (inputValue ? " checked " : "") + " name=\"" + inputID + "\" colname=\"" + columnName + "\"" + (isDisabled ? " disabled = disabled" : "") + "/>";
            return htmlControl;
        }

        public ActionResult GetSeamAndStageDataGridPartial(int HeaderId, string Type="")
        {
            ViewBag.HeaderId = HeaderId;
            ViewBag.Type = Type;
            return PartialView("_GetSeamAndStageDataGridPartial");
        }

        public ActionResult LoadSeamAndStageList(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;
                int headerid = Convert.ToInt32(param.Headerid);
                string whereCondition = "1=1 ";

                string[] columnName = { "q40.StageCode", "q2.StageDesc", "qms.SeamNo" };

                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                //var objFKM102 = db.FKM102.Where(i => i.NodeId == NodeId).FirstOrDefault();
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                List<SP_COP_GET_SEAM_AND_STAGE_LIST_Result> lstPam = new List<SP_COP_GET_SEAM_AND_STAGE_LIST_Result>();

                if (!string.IsNullOrEmpty(param.Title))
                {
                    if (param.Title == "COP")
                    {
                        var lstcop = db.COP001.Where(x => x.HeaderId == headerid).Select(x => new { Project = x.Project, QualityProject = x.QualityProject, Part = x.Part }).FirstOrDefault();
                        lstPam = db.SP_COP_GET_SEAM_AND_STAGE_LIST(StartIndex, EndIndex, strSortOrder, whereCondition, lstcop.Project, (lstcop.QualityProject != null ? lstcop.QualityProject : ""), lstcop.Part).ToList();
                    }
                }
                else
                {
                   var lstcop = db.COP003.Where(x => x.RefHeaderId == headerid).Select(x => new { Project = x.Project, QualityProject = x.QualityProject, Part = x.PartNos }).FirstOrDefault();
                    lstPam = db.SP_COP_GET_SEAM_AND_STAGE_LIST(StartIndex, EndIndex, strSortOrder, whereCondition, lstcop.Project, (lstcop.QualityProject != null ? lstcop.QualityProject : ""), lstcop.Part).ToList();
                }


                if (lstPam.Count == 0)
                {
                    return Json(new { sEcho = param.sEcho, iTotalDisplayRecords = "0", iTotalRecords = "0", aaData = new string[] { } }, JsonRequestBehavior.AllowGet);
                }

                var lstSeamAndStage = new List<SeamAndStageEnt>();

                #region New UI
                foreach (var item in lstPam.Select(i => i.SeamNo).Distinct())
                {
                    lstSeamAndStage.Add(new SeamAndStageEnt { SeamNo = item, StageDesc = GetAllSeamWiseStagesInTable(item, lstPam) });
                }

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();
                var res = (from h in lstSeamAndStage
                           select new[] {
                                       Convert.ToString(h.SeamNo),
                                       Convert.ToString(h.StageDesc),
                           }).ToList();
                #endregion

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetAllSeamWiseStagesInTable(string SeamNo, List<SP_COP_GET_SEAM_AND_STAGE_LIST_Result> lstPam)
        {
            string StageTable = "";
            try
            {
                var strClearStatus = SeamListInspectionStatus.Cleared.GetStringValue();
                var listStages = lstPam.Where(i => i.SeamNo == SeamNo);
                //StageTable = "<table class='tblstages'><tr>";
                foreach (var item in listStages)
                {
                    var tdClass = item.InspectionStatus == strClearStatus ? "partseamstage" : "";
                    //StageTable += "<div class='" + tdClass + "' style='padding:10px;display:inline-block;margin-right:10px;'>" + item.StageCode + "-" + item.StageDesc + "</div>";
                    StageTable += "<div class='" + tdClass + "' style='padding:10px;display:inline-block;margin-right:10px;' title=" + item.StageCode + "-" + item.StageDesc + ">" + item.StageDesc + "</div>";
                }
                //StageTable += "</tr></table>";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            return StageTable;
        }
    }


    public class SpoolData
    {
        public SpoolData() { }
        public string project { get; set; }
        public string qualityproject { get; set; }
        public string mark { get; set; }
        public string Overlay { get; set; }
        public string comp_assly { get; set; }
        public string typeofcomponent { get; set; }
        public string disp { get; set; }
        public string plannedreceipt { get; set; }
        public string requestedon { get; set; }
        public string cycletime { get; set; }
        public string weekno { get; set; }
        public string imrstatus { get; set; }
        public List<SeamPartWiseStageListEnt> listSeamPartWiseStageListEnt { get; set; }
    }

    public class SeamPartWiseStageListEnt
    {
        public SeamPartWiseStageListEnt() { }
        public string SeamNoPartNo { get; set; }
        public List<SeamPartWiseStageStageListEnt> listSeamPartWiseStageStageListEnt { get; set; }
    }

    public class SeamPartWiseStageStageListEnt
    {
        public SeamPartWiseStageStageListEnt() { }
        public string StageCode { get; set; }
        public string StageDesc { get; set; }
        public string InspectionStatus { get; set; }
    }

    public class SeamAndStageEnt : SP_COP_GET_SEAM_AND_STAGE_LIST_Result
    {
        public bool IsParent { get; set; }
        public bool IsClear { get; set; }
    }
}