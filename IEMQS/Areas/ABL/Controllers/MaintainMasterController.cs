﻿using IEMQS.Models;
using IEMQSImplementation;
using IEMQSImplementation.ABL;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.ABL.Controllers
{
    public class MaintainMasterController : clsBase


    {
        ABLEntitiesContext dbAbl = new ABLEntitiesContext();
        // GET: ABL/MaintainMaster
        public ActionResult Index()
        {
            return View();
        }
        public PartialViewResult LoadIndexGridDataPartial()
        {
            List<string> lstNDEtype = NDEType();
            ViewBag.NDETypeList = lstNDEtype.AsEnumerable().Select(x => new NDETypeModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            return PartialView("_LoadIndexGridDataPartial");
        }
        public ActionResult GetIndexGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1";
                string[] columnName = { "NDEType","SubCategory", "NDELength", "Vendor", "PONo", "POLine", "UnitRate" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                var lstLines = dbAbl.SP_ABL_GET_NDE_MASTER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.Id),
                                        Convert.ToString(c.NDEType),
                                          Convert.ToString(c.SubCategory),
                                        Convert.ToString(c.NDELength),
                                        Convert.ToString(c.Vendor + " - " + c.t_nama),
                                        Convert.ToString(c.PONo),
                                        Convert.ToString(c.POLine),
                                        Convert.ToString(c.UnitRate),
                                        Convert.ToString(c.CreatedBy),
                                        Convert.ToString(c.CreatedDate.Value.ToString("dd/MM/yyyy")),
                                        Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.Id + ");") + Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.Id + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.Id + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.Id +");"),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "ID", Convert.ToString(id)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtNDEType","","",  false,"","NDEType",false) +""+Helper.GenerateHidden(newRecordId, "NDEType", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtNDESubCategory","","",  false,"","SubCatgeory",false) +""+Helper.GenerateHidden(newRecordId, "SubCategory", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtNDELength","","",  false,"","NDELength",false) +""+Helper.GenerateHidden(newRecordId, "NDELength", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtVendor","","",  false,"","Vendor",false)+""+Helper.GenerateHidden(newRecordId, "Vendor", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtPONo","","",  false,"","PONo",false)+""+Helper.GenerateHidden(newRecordId, "PONo", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtPOLine","","",  false,"","POLine",false)+""+Helper.GenerateHidden(newRecordId, "POLine", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtUnitRate","","", false,"","UnitRate",false)+""+Helper.GenerateHidden(newRecordId, "UnitRate", Convert.ToString(newRecordId)),

                                    "",
                                    "",
                                     Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                    };
                    data.Add(newRecord);
                }
                else
                {

                    var lstLines = dbAbl.SP_ABL_GET_NDE_MASTER(1, 0, "", "Id = " + id).Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.ROW_NO),
                                    Helper.GenerateHidden(c.Id, "Id", Convert.ToString(id)),
                                    isReadOnly ? c.NDEType : Helper.HTMLAutoComplete(c.Id, "txtNDEType",WebUtility.HtmlEncode(c.NDEType),"", false,"","NDEType",false)+""+Helper.GenerateHidden(c.Id, "NDEType",Convert.ToString(c.NDEType)),
                                    isReadOnly ? c.SubCategory : Helper.HTMLAutoComplete(c.Id, "txtNDESubCategory",WebUtility.HtmlEncode(c.SubCategory),"", false,"","SubCategory",false)+""+Helper.GenerateHidden(c.Id, "SubCategory",Convert.ToString(c.SubCategory)),
                                    isReadOnly ? c.NDELength : Helper.HTMLAutoComplete(c.Id, "txtNDELength",WebUtility.HtmlEncode(c.NDELength),"", false,"","NDELength",false)+""+Helper.GenerateHidden(c.Id, "NDELength",Convert.ToString(c.NDELength)),
                                    isReadOnly ? c.Vendor : Helper.HTMLAutoComplete(c.Id, "txtVendor",WebUtility.HtmlEncode(c.Vendor),"", false,"","Vendor",false)+""+Helper.GenerateHidden(c.Id, "Vendor",Convert.ToString(c.Vendor)),
                                    isReadOnly ? c.PONo : Helper.HTMLAutoComplete(c.Id, "txtPONo",c.PONo,"", false,"","WeldType",false)+""+Helper.GenerateHidden(c.Id, "PONo",Convert.ToString(c.PONo)),
                                    isReadOnly ? Convert.ToString(c.POLine) : Helper.HTMLAutoComplete(c.Id, "txtPOLine",c.POLine.ToString(),"", false,"","POLine",false)+""+Helper.GenerateHidden(c.Id, "POLine",Convert.ToString(c.POLine)),
                                    isReadOnly ? Convert.ToString(c.UnitRate) : Helper.HTMLAutoComplete(c.Id, "txtUnitRate",c.UnitRate.ToString(),"", false,"","UnitRate",false)+""+Helper.GenerateHidden(c.Id, "UnitRate", Convert.ToString(c.UnitRate)),
                                    c.CreatedBy,
                                    Convert.ToDateTime(c.CreatedDate).ToString("dd/MM/yyyy"),
                                    Helper.HTMLActionString(c.Id,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.Id + ");") +
                                    Helper.HTMLActionString(c.Id,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.Id + ");","",false,"display:none") + " " +
                                    Helper.HTMLActionString(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.Id + "); ","",false,"display:none") + " " +
                                    Helper.HTMLActionString(c.Id,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.Id +");"),
                                }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult SaveData(FormCollection fc, int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    string NDEType = fc["txtNDEType" + Id];
                    string SubCategory = fc["txtNDESubCategory" + Id];
                    string NDELength = fc["txtNDELength" + Id];
                    string Vendor = fc["txtVendor" + Id];
                    string PONo = fc["txtPONo" + Id];
                    String POLine = fc["txtPOLine" + Id];
                    string RateUnit = fc["txtUnitRate" + Id];

                    if (Convert.ToDecimal(RateUnit) <= 0)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Unit Rate Can Not Be 0 or Negative.";
                    }
                    else
                    {
                        ABL003 objABL003 = null;
                        Vendor = Vendor.ToString().Split('-')[0].Trim();
                        if (Id > 0)
                        {
                            if (!dbAbl.ABL003.Any(x => x.NDEType == NDEType && x.SubCategory == SubCategory && x.NDELength == NDELength && x.Vendor == Vendor && x.PONo == PONo && x.POLine.ToString() == POLine.ToString()
                                                     && x.UnitRate.ToString() == RateUnit.ToString() && x.ID != Id))
                                if (true)
                                {
                                    objABL003 = dbAbl.ABL003.Where(x => x.ID == Id).FirstOrDefault();
                                    if (objABL003 != null)
                                    {
                                        objABL003.NDEType = NDEType;
                                        objABL003.SubCategory = SubCategory;
                                        objABL003.NDELength = NDELength;
                                        objABL003.Vendor = Vendor;
                                        objABL003.PONo = PONo;
                                        objABL003.POLine = Convert.ToInt32(POLine);
                                        objABL003.UnitRate = Convert.ToDecimal(RateUnit);

                                        dbAbl.SaveChanges();
                                        objResponseMsg.Key = true;
                                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                                    }
                                    else
                                    {
                                        objResponseMsg.Key = false;
                                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                                    }
                                }
                                else
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                                }
                        }
                        else
                        {
                            if (!dbAbl.ABL003.Any(x => x.NDEType == NDEType && x.SubCategory == SubCategory &&  x.NDELength == NDELength && x.Vendor == Vendor && x.PONo == PONo && x.POLine.ToString() == POLine.ToString()
                                                      && x.UnitRate.ToString() == RateUnit.ToString() && x.ID != Id))
                                if (true)
                                {
                                    objABL003 = new ABL003();
                                    objABL003.ID = Convert.ToInt32(8);
                                    objABL003.NDEType = NDEType;
                                    objABL003.SubCategory = SubCategory;
                                    objABL003.NDELength = NDELength;
                                    objABL003.Vendor = Vendor;
                                    objABL003.PONo = PONo;
                                    objABL003.POLine = Convert.ToInt32(POLine);

                                    objABL003.UnitRate = Convert.ToDecimal(RateUnit);
                                    objABL003.CreatedBy = objClsLoginInfo.UserName;
                                    objABL003.CreatedDate = DateTime.Now;

                                    dbAbl.ABL003.Add(objABL003);

                                    dbAbl.SaveChanges();
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                                }
                                else
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                                }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult DeleteData(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ABL003 objABL003 = dbAbl.ABL003.Where(x => x.ID == Id).FirstOrDefault();
                if (objABL003 != null)
                {
                    dbAbl.ABL003.Remove(objABL003);
                    dbAbl.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = dbAbl.SP_ABL_GET_NDE_MASTER(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from li in lst
                              select new
                              {
                                  NDEType = li.NDEType,
                                  NDELength = li.NDELength,
                                  Vendor = li.Vendor,
                                  PONo = li.PONo,
                                  POLine = li.POLine,
                                  UnitRate = li.UnitRate,
                                  CreatedBy = li.CreatedBy,
                                  CreatedDate = Convert.ToDateTime(li.CreatedDate).ToString("dd/MM/yyyy"),
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ReadExcelFile()
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            //if (Request.Files.Count == 0)
            //{
            //    objResponseMsg.Key = false;
            //    objResponseMsg.Value = "Please select a file first!";
            //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            //}

            //HttpPostedFileBase upload = Request.Files[0];

            //if (Path.GetExtension(upload.FileName) == ".xlsx" || Path.GetExtension(upload.FileName) == ".xls")
            //{
            //    ExcelPackage package = new ExcelPackage(upload.InputStream);
            //    //DataTable dt = Manager.ExcelToDataTable(package);
            //    bool isError;
            //    DataSet ds = ToAddSeamDetailsHeader(package, out isError);
            //    if (!isError)
            //    {
            //        var seamCategoryList = Manager.GetSubCatagorywithoutBULocation("Seam Category").ToList();


            //        var weldTypeList = (from c in db.BOM003
            //                            group c by new
            //                            {
            //                                c.JointTypeCode,
            //                                c.JointTypeDescription,
            //                            } into gcs
            //                            select new
            //                            {
            //                                CatID = gcs.Key.JointTypeCode,
            //                                CatDesc = gcs.Key.JointTypeDescription,
            //                            }).ToList();


            //        var nonOverlayThicknessList = (from u in db.CJC001
            //                                       where u.IsActive == true
            //                                       select new { CatID = u.Id, CatDesc = u.Description }).ToList();


            //        var nozzleSizeList = (from u in db.CJC002
            //                              where u.IsActive == true
            //                              select new { CatID = u.Id, CatDesc = u.Description }).ToList();

            //        var weldingProcessList = Manager.GetSubCatagorywithoutBULocation("Welding Process").ToList();



            //        //dia is displayed as ProductType in grid.
            //        var diaList = (from u in db.CJC003
            //                       where u.IsActive == true
            //                       select new { CatID = u.Id, CatDesc = u.Description }).ToList();


            //        List<DataRow> dtImported = (from u in ds.Tables[0].Rows.Cast<DataRow>()
            //                                    select u).ToList();

            //        foreach (DataRow dr in dtImported)
            //        {
            //            try
            //            {
            //                string SeamCategory = "";
            //                if (!string.IsNullOrEmpty(dr["SeamCategory"].ToString()))
            //                {
            //                    var tempSeamCategory = seamCategoryList.Where(x => x.CategoryDescription == dr["SeamCategory"].ToString()).FirstOrDefault();
            //                    if (tempSeamCategory != null)
            //                        SeamCategory = tempSeamCategory.Value;
            //                }

            //                string WeldingProcess = "";
            //                if (!string.IsNullOrEmpty(dr["Welding Process"].ToString()))
            //                {
            //                    var tempWeldingProcess = weldingProcessList.Where(x => x.CategoryDescription == dr["Welding Process"].ToString()).FirstOrDefault();
            //                    if (tempWeldingProcess != null)
            //                        WeldingProcess = tempWeldingProcess.Value;
            //                }


            //                string NozzleCategory = dr["NozzleCategory"].ToString();

            //                string WeldType = "";
            //                if (!string.IsNullOrEmpty(dr["WeldType"].ToString().Trim()))
            //                {
            //                    var a = dr["WeldType"].ToString();
            //                    var b = dr["WeldType"].ToString().Trim();

            //                    var tempWeldType = weldTypeList.Where(x => x.CatDesc == dr["WeldType"].ToString().Trim()).FirstOrDefault();
            //                    if (tempWeldType != null)
            //                        WeldType = tempWeldType.CatID;
            //                }


            //                string Setup = dr["Setup"].ToString();
            //                string SeamType = dr["SeamType"].ToString();

            //                int? NonOverlayThickness = null;
            //                if (dr["NonOverlayThickness"] != null && dr["NonOverlayThickness"].ToString() != "")
            //                {
            //                    var tempNonOverlayThickness = nonOverlayThicknessList.Where(x => x.CatDesc == dr["NonOverlayThickness"].ToString()).FirstOrDefault();
            //                    if (tempNonOverlayThickness != null)
            //                        NonOverlayThickness = tempNonOverlayThickness.CatID;
            //                }

            //                int? NozzleSize = null;
            //                if (dr["NozzleSize"] != null && dr["NozzleSize"].ToString() != "")
            //                {
            //                    var tempNozzleSize = nozzleSizeList.Where(x => x.CatDesc == dr["NozzleSize"].ToString()).FirstOrDefault();
            //                    if (tempNozzleSize != null)
            //                        NozzleSize = tempNozzleSize.CatID;
            //                }

            //                double? Rate = null;
            //                if (dr["Rate"] != null && dr["Rate"].ToString() != "")
            //                    Rate = Convert.ToDouble(dr["Rate"]);

            //                int? Dia = null;
            //                if (dr["ProductType"] != null && dr["ProductType"].ToString() != "")
            //                {
            //                    var tempDia = diaList.Where(x => x.CatDesc == dr["ProductType"].ToString()).FirstOrDefault();
            //                    if (tempDia != null)
            //                        Dia = tempDia.CatID;
            //                }


            //                CJC011 objCJC011 = db.CJC011.Where(x => x.SeamCategory == SeamCategory
            //                                            && x.SeamType == SeamType
            //                                            && x.WeldType == WeldType
            //                                            && x.NonOverlayThickness == NonOverlayThickness
            //                                            && x.NozzleSize == NozzleSize
            //                                            && x.WeldingProcess == WeldingProcess
            //                                            && x.NozzleCategory == NozzleCategory
            //                                            && x.Dia == Dia
            //                                            ).FirstOrDefault();

            //                if (objCJC011 == null)
            //                {
            //                    objCJC011 = new CJC011();

            //                    objCJC011.SeamCategory = SeamCategory;
            //                    objCJC011.WeldingProcess = WeldingProcess;
            //                    objCJC011.NozzleCategory = NozzleCategory;
            //                    objCJC011.NonOverlayThickness = NonOverlayThickness;
            //                    objCJC011.NozzleSize = NozzleSize;
            //                    objCJC011.Rate = Rate;
            //                    objCJC011.Dia = Dia;
            //                    objCJC011.WeldType = WeldType;
            //                    objCJC011.Setup = Setup;
            //                    objCJC011.SeamType = SeamType;
            //                    objCJC011.CreatedBy = objClsLoginInfo.UserName;
            //                    objCJC011.CreatedOn = DateTime.Now;

            //                    db.CJC011.Add(objCJC011);
            //                }
            //                else
            //                {
            //                    objCJC011.SeamCategory = SeamCategory;
            //                    objCJC011.WeldingProcess = WeldingProcess;
            //                    objCJC011.NozzleCategory = NozzleCategory;
            //                    objCJC011.NonOverlayThickness = NonOverlayThickness;
            //                    objCJC011.NozzleSize = NozzleSize;
            //                    objCJC011.Rate = Rate;
            //                    objCJC011.Dia = Dia;
            //                    objCJC011.WeldType = WeldType;
            //                    objCJC011.Setup = Setup;
            //                    objCJC011.SeamType = SeamType;
            //                    objCJC011.EditedBy = objClsLoginInfo.UserName;
            //                    objCJC011.EditedOn = DateTime.Now;
            //                }

            //                db.SaveChanges();
            //            }
            //            catch (Exception ex)
            //            {

            //            }
            //        }
            //        objResponseMsg.Key = true;
            //        objResponseMsg.Value = "Seam rate list import successfully";

            //    }
            //    else
            //    {
            //        objResponseMsg.Key = false;
            //        objResponseMsg.Value = "Please upload valid excel file!";
            //    }

            //    //return Json("");//validateAndSave(dt, HeaderId);
            //}
            //else
            //{
            //    objResponseMsg.Key = false;
            //    objResponseMsg.Value = "Please upload valid excel file!";
            //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            //}

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public DataSet ToAddSeamDetailsHeader(ExcelPackage package, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtLinesExcel = new DataTable();
            isError = false;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtLinesExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtLinesExcel.NewRow();

                    foreach (var cell in row)
                    {
                        newRow[cell.Start.Column - 1] = cell.Text;
                    }

                    dtLinesExcel.Rows.Add(newRow);
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtLinesExcel);
            return ds;
        }

        public List<string> NDEType()
        {
            List<string> items = clsImplementationEnum.GetListOfDescription<clsImplementationEnum.NDEType>();
            return items;
        }

        [HttpPost]
        public ActionResult GetLengthResult(string term)
        {
            var lstLength = new List<Projects>();

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstLength = db.Database.SqlQuery<Projects>("Select distinct NDELength as Value,NDELength as Text from [dbo].[ABL004] Where NDELength like '%" + term + "%' order by NDELength").Take(20).ToList();
            }
            else
            {
                lstLength = db.Database.SqlQuery<Projects>("Select distinct NDELength as Value,NDELength as Text from [dbo].[ABL004] order by NDELength").Take(20).ToList();
            }

            return Json(lstLength, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetVendorResult(string term)
        {
            var LNLinkedServer = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue());
            string LNCompanyId = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNCompanyId.GetStringValue());
            var lstVendor = new List<Projects>();

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstVendor = db.Database.SqlQuery<Projects>("Select distinct t_bpid as Value , t_bpid + '-' + t_nama as Text from " + LNLinkedServer + ".dbo.ttccom100" + LNCompanyId + "   Where UPPER(t_bpid + '-' + t_nama) like UPPER('%" + term + "%') order by t_bpid").Take(20).ToList();
            }
            else
            {
                lstVendor = db.Database.SqlQuery<Projects>("Select distinct t_bpid as Value , t_bpid + '-' + t_nama as Text from " + LNLinkedServer + ".dbo.ttccom100" + LNCompanyId + " order by t_bpid ").Take(20).ToList();
            }

            return Json(lstVendor, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetPOResult(string term)
        {
            var LNLinkedServer = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue());
            string LNCompanyId = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNCompanyId.GetStringValue());
            var lstPO = new List<Projects>();

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstPO = db.Database.SqlQuery<Projects>("Select distinct t_orno as Value,t_orno as Text from " + LNLinkedServer + ".dbo.ttdpur400" + LNCompanyId + "   Where UPPER(t_orno) like UPPER('%" + term + "%') order by t_orno").Take(20).ToList();
            }
            else
            {
                lstPO = db.Database.SqlQuery<Projects>("Select distinct t_orno as Value,t_orno as Text  from " + LNLinkedServer + ".dbo.ttdpur400" + LNCompanyId + " order by t_orno ").Take(20).ToList();
            }

            return Json(lstPO, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetPOLineResult(string term, string txtPONo)
        {
            var LNLinkedServer = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNLinkedServer.GetStringValue());
            string LNCompanyId = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.LNCompanyId.GetStringValue());
            var lstPO = new List<Projects>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstPO = db.Database.SqlQuery<Projects>("Select distinct cast(t_pono as varchar(10)) as Value,cast(t_pono as varchar(10)) as Text from " + LNLinkedServer + ".dbo.ttdpur401" + LNCompanyId + " child left join " + LNLinkedServer + ".dbo.ttdpur400" + LNCompanyId + " parent on child.t_orno = parent.t_orno Where " + "child.t_orno='" + Convert.ToString(txtPONo) + "' and child.t_pono like '%" + term + "%'").Take(20).ToList();
            }
            else
            {
                lstPO = db.Database.SqlQuery<Projects>("Select distinct cast(t_pono as varchar(10)) as Value,cast(t_pono as varchar(10)) as Text from " + LNLinkedServer + ".dbo.ttdpur401" + LNCompanyId + " child left join " + LNLinkedServer + ".dbo.ttdpur400" + LNCompanyId + " parent on child.t_orno = parent.t_orno Where child.t_orno='" + Convert.ToString(txtPONo) + "'").Take(20).ToList();
            }

            return Json(lstPO, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MaintainLength()
        {
            return View();
        }

        public PartialViewResult LoadLengthGridDataPartial()
        {
            //List<string> lstNDEtype = NDEType();
            //ViewBag.NDETypeList = lstNDEtype.AsEnumerable().Select(x => new NDETypeModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            return PartialView("_LoadLengthGridDataPartial");
        }

        public ActionResult GetLengthGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1";
                string[] columnName = { "NDELength", "MinimumLength", "MaximumLength" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                var lstLines = dbAbl.SP_ABL_GET_LENGTH_MASTER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.ID),
                                        Convert.ToString(c.NDELength),
                                        Convert.ToString(c.MinimumValue),
                                        Convert.ToString(c.MaximumValue),
                                        Helper.HTMLActionString(c.ID,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.ID + ");") + Helper.HTMLActionString(c.ID,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.ID + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.ID,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.ID + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.ID,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.ID +");"),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRowLength(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "ID", Convert.ToString(id)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtNDELength","","",  false,"","NDELength",false) +""+Helper.GenerateHidden(newRecordId, "NDELength", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtMinimumValue","","",  false,"","MinimumValue",false)+""+Helper.GenerateHidden(newRecordId, "MinimumValue", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtMaximumValue","","",  false,"","MaximumValue",false)+""+Helper.GenerateHidden(newRecordId, "MaximumValue", Convert.ToString(newRecordId)),

                                    "",
                                    "",
                                     Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                    };
                    data.Add(newRecord);
                }
                else
                {

                    var lstLines = dbAbl.SP_ABL_GET_LENGTH_MASTER(1, 0, "", "ID = " + id).Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.ROW_NO),
                                    Helper.GenerateHidden(c.ID, "ID", Convert.ToString(id)),
                                    isReadOnly ? c.NDELength : Helper.HTMLAutoComplete(c.ID, "txtNDELength",WebUtility.HtmlEncode(c.NDELength),"", false,"","NDELength",false)+""+Helper.GenerateHidden(c.ID, "NDELength",Convert.ToString(c.NDELength)),
                                    isReadOnly ? Convert.ToString(c.MinimumValue) : Helper.HTMLAutoComplete(c.ID, "txtMinimumValue",c.MinimumValue.ToString(),"", false,"","MinimumValue",false)+""+Helper.GenerateHidden(c.ID, "MinimumValue",Convert.ToString(c.MinimumValue)),
                                    isReadOnly ? Convert.ToString(c.MaximumValue) : Helper.HTMLAutoComplete(c.ID, "txtMaximumValue",c.MaximumValue.ToString(),"", false,"","MaximumValue",false)+""+Helper.GenerateHidden(c.ID, "MaximumValue",Convert.ToString(c.MaximumValue)),
                                    Helper.HTMLActionString(c.ID,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.ID + ");") +
                                    Helper.HTMLActionString(c.ID,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.ID + ");","",false,"display:none") + " " +
                                    Helper.HTMLActionString(c.ID,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.ID + "); ","",false,"display:none") + " " +
                                    Helper.HTMLActionString(c.ID,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.ID +");"),
                                }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveLengthData(FormCollection fc, int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    string NDELength = fc["txtNDELength" + Id];
                    string MinValue = fc["txtMinimumValue" + Id];
                    string MaxValue = fc["txtMaximumValue" + Id];
                    //int? Con_POLine = null;
                    //if (fc["POLine" + Id] != null && fc["POLine" + Id] != "")
                    //    Con_POLine = Convert. ToInt32(POLine);
                    //decimal? Con_RateUnit = null;
                    //if (fc["RateUnit" + Id] != null && fc["RateUnit" + Id] != "")
                    //    Con_RateUnit = Convert.ToDecimal(RateUnit);

                    //int? NonOverlayThickness = null;
                    //if (fc["NonOverlayThickness" + Id] != null && fc["NonOverlayThickness" + Id] != "")
                    //    NonOverlayThickness = Convert.ToInt32(fc["NonOverlayThickness" + Id]);

                    //int? NozzleSize = null;
                    //if (fc["NonOverlayThickness" + Id] != null && fc["NozzleSize" + Id] != "")
                    //    NozzleSize = Convert.ToInt32(fc["NozzleSize" + Id]);

                    //double? Rate = null;
                    //if (fc["txtRate" + Id] != null && fc["txtRate" + Id] != "")
                    //    Rate = Convert.ToDouble(fc["txtRate" + Id]);

                    //int? Dia = null;
                    //if (fc["Dia" + Id] != null && fc["Dia" + Id] != "")
                    //    Dia = Convert.ToInt32(fc["Dia" + Id]);

                    ABL004 objABL004 = null;
                    int MinimumValue = Convert.ToInt32(MinValue);
                    int MaximumValue = Convert.ToInt32(MaxValue);

                    if (Id > 0)
                    {
                        if (!dbAbl.ABL004.Any(x => x.NDELength == NDELength && x.MinimumValue == MinimumValue && x.MaximumValue == MaximumValue && x.ID != Id))
                            if (true)
                            {
                                objABL004 = dbAbl.ABL004.Where(x => x.ID == Id).FirstOrDefault();
                                if (objABL004 != null)
                                {
                                    objABL004.NDELength = NDELength;
                                    objABL004.MinimumValue = MinimumValue;
                                    objABL004.MaximumValue = MaximumValue;
                                    dbAbl.SaveChanges();
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                                }
                                else
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                                }
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                            }
                    }
                    else
                    {
                        if (!dbAbl.ABL004.Any(x => x.NDELength == NDELength && x.MinimumValue == MinimumValue && x.MaximumValue == MaximumValue && x.ID != Id))
                            if (true)
                            {
                                objABL004 = new ABL004();
                                objABL004.ID = Convert.ToInt32(8);
                                objABL004.NDELength = NDELength;
                                objABL004.MinimumValue = MinimumValue;
                                objABL004.MaximumValue = MaximumValue;
                                dbAbl.ABL004.Add(objABL004);

                                dbAbl.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteLengthData(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ABL004 objABL004 = dbAbl.ABL004.Where(x => x.ID == Id).FirstOrDefault();
                if (objABL004 != null)
                {
                    dbAbl.ABL004.Remove(objABL004);
                    dbAbl.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MaintainParameter()
        {
            return View();
        }

        public PartialViewResult LoadParameterGridDataPartial()
        {
            //List<string> lstNDEtype = NDEType();
            //ViewBag.NDETypeList = lstNDEtype.AsEnumerable().Select(x => new NDETypeModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            return PartialView("_LoadParameterGridDataPartial");
        }

        public ActionResult GetParameterGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1";
                string[] columnName = { "EffectiveDate", "Tolerance", "LaunchDate" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                var lstLines = dbAbl.SP_ABL_GET_PARAMETER_MASTER(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.ID),
                                        Convert.ToString(c.EffectiveDate.Value.ToString("dd/MM/yyyy")),
                                        Convert.ToString(c.Tolerance),
                                        Convert.ToString(c.LaunchDate.Value.ToString("dd/MM/yyyy")),
                                        "",
                                        //Helper.HTMLActionString(c.ID,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.ID + ");") + Helper.HTMLActionString(c.ID,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.ID + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.ID,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.ID + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.ID,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.ID +");"),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRowParameter(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "ID", Convert.ToString(id)),
                                    Helper.GenerateHTMLTextbox(newRecordId,"EffectiveDate","","",  false,"",false,"") ,/*+""+Helper.GenerateHidden(newRecordId, "EffectiveDate", Convert.ToString(newRecordId)),*/
                                    Helper.HTMLAutoComplete(newRecordId,"txtTolerance","","",  false,"","Tolerance",false)+""+Helper.GenerateHidden(newRecordId, "Tolerance", Convert.ToString(newRecordId)),
                                    Helper.GenerateHTMLTextbox(newRecordId,"LaunchDate","","",  false,"",false,"") ,
                                     Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                    };
                    data.Add(newRecord);
                }
                else
                {

                    var lstLines = dbAbl.SP_ABL_GET_PARAMETER_MASTER(1, 0, "", "ID = " + id).Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.ROW_NO),
                                    Helper.GenerateHidden(c.ID, "ID", Convert.ToString(id)),
                                    isReadOnly ? Convert.ToString(c.EffectiveDate) : Helper.HTMLAutoComplete(c.ID, "EffectiveDate",c.EffectiveDate.ToString(),"", false,"","EffectiveDate",false)+""+Helper.GenerateHidden(c.ID, "EffectiveDate",Convert.ToString(c.EffectiveDate)),
                                    isReadOnly ? Convert.ToString(c.Tolerance) : Helper.HTMLAutoComplete(c.ID, "txtTolerance",c.Tolerance.ToString(),"", false,"","Tolerance",false)+""+Helper.GenerateHidden(c.ID, "Tolerance",Convert.ToString(c.Tolerance)),
                                    isReadOnly ? Convert.ToString(c.LaunchDate) : Helper.HTMLAutoComplete(c.ID, "LaunchDate",c.LaunchDate.ToString(),"", false,"","LaunchDate",false)+""+Helper.GenerateHidden(c.ID, "LaunchDate",Convert.ToString(c.LaunchDate)),
                                    //Helper.HTMLActionString(c.ID,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.ID + ");") +
                                    //Helper.HTMLActionString(c.ID,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.ID + ");","",false,"display:none") + " " +
                                    //Helper.HTMLActionString(c.ID,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.ID + "); ","",false,"display:none") + " " +
                                    //Helper.HTMLActionString(c.ID,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.ID +");"),
                                }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveParametersData(FormCollection fc, int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    string EDate = fc["EffectiveDate" + Id];
                    string STolerance = fc["txtTolerance" + Id];
                    string LDate = fc["LaunchDate" + Id];

                    ABL005 objABL005 = null;
                    DateTime EffectiveDate = Convert.ToDateTime(EDate);
                    decimal Tolerance = Convert.ToDecimal(STolerance);
                    DateTime LaunchDate = Convert.ToDateTime(LDate);

                    if (Id > 0)
                    {
                        if (!dbAbl.ABL005.Any(x => x.EffectiveDate == EffectiveDate && x.Tolerance == Tolerance && x.LaunchDate == LaunchDate && x.ID != Id))
                            if (true)
                            {
                                objABL005 = dbAbl.ABL005.Where(x => x.ID == Id).FirstOrDefault();
                                if (objABL005 != null)
                                {
                                    objABL005.EffectiveDate = EffectiveDate;
                                    objABL005.Tolerance = Tolerance;
                                    objABL005.LaunchDate = LaunchDate;
                                    dbAbl.SaveChanges();
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                                }
                                else
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                                }
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                            }
                    }
                    else
                    {
                        if (!dbAbl.ABL005.Any(x => x.EffectiveDate == EffectiveDate && x.Tolerance == Tolerance && x.LaunchDate == LaunchDate && x.ID != Id))
                            if (true)
                            {
                                objABL005 = new ABL005();
                                objABL005.ID = Convert.ToInt32(8);
                                objABL005.EffectiveDate = EffectiveDate;
                                objABL005.Tolerance = Tolerance;
                                objABL005.LaunchDate = LaunchDate;
                                dbAbl.ABL005.Add(objABL005);

                                dbAbl.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                            }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteParameterData(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ABL005 objABL005 = dbAbl.ABL005.Where(x => x.ID == Id).FirstOrDefault();
                if (objABL005 != null)
                {
                    dbAbl.ABL005.Remove(objABL005);
                    dbAbl.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public ActionResult MaintainSubCategory()
        {
            return View();
        }
        [HttpPost]
        //public PartialViewResult LoadSubCategoryGridDataPartial()
        //{
        //    //List<string> lstNDEtype = NDEType();
        //    //ViewBag.NDETypeList = lstNDEtype.AsEnumerable().Select(x => new NDETypeModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
        //   //return PartialView("_LoadLengthGridDataPartial");
        //    return PartialView("_LoadLengthGridDataPartial");
        //}

        public PartialViewResult LoadCategoryGridDataPartial()
        {
            List<string> lstNDEtype = NDEType();
            ViewBag.NDETypeList = lstNDEtype.AsEnumerable().Select(x => new NDETypeModel() { CatDesc = x.ToString(), CatID = x.ToString() }).ToList();
            ViewBag.StageList = db.QMS002.Where(i => i.StageDepartment == "NDE").Select(n => new NDETypeModel { CatDesc = n.StageCode, CatID = n.StageCode }).Distinct().ToList();
            return PartialView("_LoadCategoryGridDataPartial");
        }

        public ActionResult GetSubCategoryGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);

                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = param.sColumns.Split(',')[sortColumnIndex];
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                string strSortOrder = string.Empty;

                string whereCondition = " 1=1";
                string[] columnName = { "NDEType", "SubCategory", "StageCode" };

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                else
                {
                    strSortOrder = " Order By EditedOn desc ";
                }

                var lstLines = dbAbl.SP_ABL_GET_NDE_SUBCATEGORY(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstLines.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from c in lstLines
                           select new[] {
                                        Convert.ToString(c.ROW_NO),
                                        Convert.ToString(c.ID),
                                        Convert.ToString(c.NDEType),
                                        Convert.ToString(c.SubCategory),
                                        Convert.ToString(c.StageCode),
                                        Helper.HTMLActionString(c.ID,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.ID + ");") + Helper.HTMLActionString(c.ID,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.ID + ");","",false,"display:none") + " " + Helper.HTMLActionString(c.ID,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.ID + "); ","",false,"display:none") + " " + Helper.HTMLActionString(c.ID,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.ID +");"),
                          }).ToList();

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableSubCategoryRowLength(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();


                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                    Helper.GenerateHidden(newRecordId, "ID", Convert.ToString(id)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtNDEType","","",  false,"","NDEType",false) +""+Helper.GenerateHidden(newRecordId, "NDEType", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtSubCategory","","",  false,"","SubCategory",false) +""+Helper.GenerateHidden(newRecordId, "SubCategory", Convert.ToString(newRecordId)),
                                    Helper.HTMLAutoComplete(newRecordId,"txtStageCode","","",  false,"","StageCode",false) +""+Helper.GenerateHidden(newRecordId, "StageCode", Convert.ToString(newRecordId)),

                                    "",
                                    "",
                                     Helper.HTMLActionString(newRecordId,"Add","Add New Record","fa fa-plus","SaveNewRecord();"),
                    };
                    data.Add(newRecord);
                }
                else
                {

                    var lstLines = dbAbl.SP_ABL_GET_NDE_SUBCATEGORY(1, 0, "", "Id = " + id).Take(1).ToList();

                    data = (from c in lstLines
                            select new[] {
                                    clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.ROW_NO),
                                    Helper.GenerateHidden(c.ID, "ID", Convert.ToString(id)),
                                    isReadOnly ? c.NDEType : Helper.HTMLAutoComplete(c.ID, "txtNDEType",WebUtility.HtmlEncode(c.NDEType),"", false,"","NDEType",false)+""+Helper.GenerateHidden(c.ID, "NDEType",Convert.ToString(c.NDEType)),
                                    isReadOnly ? c.SubCategory : Helper.HTMLAutoComplete(c.ID, "txtSubCategory",WebUtility.HtmlEncode(c.SubCategory),"", false,"","SubCategory",false)+""+Helper.GenerateHidden(c.ID, "SubCategory",Convert.ToString(c.SubCategory)),
                                    isReadOnly ? c.StageCode : Helper.HTMLAutoComplete(c.ID, "txtStageCode",WebUtility.HtmlEncode(c.StageCode),"", false,"","StageCode",false)+""+Helper.GenerateHidden(c.ID, "StageCode",Convert.ToString(c.StageCode)),
                                    Helper.HTMLActionString(c.ID,"Edit","Edit Record","fa fa-pencil-square-o","EnabledEditLine(" + c.ID + ");") +
                                    Helper.HTMLActionString(c.ID,"Update","Update Record","fa fa-floppy-o","EditLine(" + c.ID + ");","",false,"display:none") + " " +
                                    Helper.HTMLActionString(c.ID,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine(" + c.ID + "); ","",false,"display:none") + " " +
                                    Helper.HTMLActionString(c.ID,"Delete","Delete Record","fa fa-trash-o","DeleteLine("+ c.ID +");"),
                                }).ToList();
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveNDESubCAtegoryData(FormCollection fc, int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (fc != null)
                {
                    string NDEType = fc["txtNDEType" + Id];
                    string SubCategory = fc["txtSubCategory" + Id];
                    string StageCode = fc["txtStageCode" + Id];

                    //int? Con_POLine = null;
                    //if (fc["POLine" + Id] != null && fc["POLine" + Id] != "")
                    //    Con_POLine = Convert. ToInt32(POLine);
                    //decimal? Con_RateUnit = null;
                    //if (fc["RateUnit" + Id] != null && fc["RateUnit" + Id] != "")
                    //    Con_RateUnit = Convert.ToDecimal(RateUnit);

                    //int? NonOverlayThickness = null;
                    //if (fc["NonOverlayThickness" + Id] != null && fc["NonOverlayThickness" + Id] != "")
                    //    NonOverlayThickness = Convert.ToInt32(fc["NonOverlayThickness" + Id]);

                    //int? NozzleSize = null;
                    //if (fc["NonOverlayThickness" + Id] != null && fc["NozzleSize" + Id] != "")
                    //    NozzleSize = Convert.ToInt32(fc["NozzleSize" + Id]);

                    //double? Rate = null;
                    //if (fc["txtRate" + Id] != null && fc["txtRate" + Id] != "")
                    //    Rate = Convert.ToDouble(fc["txtRate" + Id]);

                    //int? Dia = null;
                    //if (fc["Dia" + Id] != null && fc["Dia" + Id] != "")
                    //    Dia = Convert.ToInt32(fc["Dia" + Id]);

                    ABL006 objABL006 = null;
                    //int MinimumValue = Convert.ToInt32(MinValue);
                    //int MaximumValue = Convert.ToInt32(MaxValue);

                    if (Id > 0)
                    {
                        if (!dbAbl.ABL006.Any(x => x.NDEType == NDEType && x.SubCategory == SubCategory && x.ID != Id))
                        {
                            if (true)
                            {
                                objABL006 = dbAbl.ABL006.Where(x => x.ID == Id).FirstOrDefault();
                                if (objABL006 != null)
                                {
                                    objABL006.NDEType = NDEType;
                                    objABL006.SubCategory = SubCategory;
                                    objABL006.StageCode = StageCode;
                                    dbAbl.SaveChanges();
                                    objResponseMsg.Key = true;
                                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                                }
                                else
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                                }
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }
                    }
                    else
                    {
                        if (!dbAbl.ABL006.Any(x => x.NDEType == NDEType && x.SubCategory == SubCategory && x.ID != Id))
                        {
                            if (true)
                            {
                                objABL006 = new ABL006();
                                objABL006.ID = Convert.ToInt32(8);
                                objABL006.NDEType = NDEType;
                                objABL006.SubCategory = SubCategory;
                                objABL006.StageCode = StageCode;
                                dbAbl.ABL006.Add(objABL006);

                                dbAbl.SaveChanges();
                                objResponseMsg.Key = true;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                            }
                            else
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                            }
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult DeleteNDESubCategoryData(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                ABL006 objABL006 = dbAbl.ABL006.Where(x => x.ID == Id).FirstOrDefault();
                if (objABL006 != null)
                {
                    dbAbl.ABL006.Remove(objABL006);
                    dbAbl.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Details not available for deleted.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult GetSubCategoryResult(string term)
        {
            var lstLength = new List<Projects>();

            if (!string.IsNullOrWhiteSpace(term))
            {
                //lstLength = db.Database.SqlQuery<Projects>("Select distinct SubCategory as Value,SubCategory as Text from [dbo].[ABL006] Where SubCategory like '%" + term + "%' order by SubCategory").Take(20).ToList();
                lstLength = db.Database.SqlQuery<Projects>("Select distinct SubCategory as Value,SubCategory as Text from [dbo].[ABL006] Where NDEType like '%" + term + "%' order by SubCategory").Take(20).ToList();
            }
            else
            {
                lstLength = db.Database.SqlQuery<Projects>("Select distinct SubCategory as Value,SubCategory as Text from [dbo].[ABL006] order by SubCategory").Take(20).ToList();
            }

            return Json(lstLength, JsonRequestBehavior.AllowGet);
        }

        public class NDETypeModel
        {
            public string CatID { get; set; }
            public string CatDesc { get; set; }
        }

        #region JS - Category Master Setup
        public void MaintainCategory()
        {

        }
        #endregion
    }
}