﻿using System.Web.Mvc;

namespace IEMQS.Areas.ABL
{
    public class ABLAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "ABL";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "ABL_default",
                "ABL/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}