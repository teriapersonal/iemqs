﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.PTS.Models
{
    public class PTSModel
    {
        public string PROJECTID { get; set; }
        public string ProjectCode { get; set; }
        public string UID { get; set; }
        public string id { get; set; }
        public string TaskDescription { get; set; }
        public string ActualFinish { get; set; }
        public string PlannedCompletion { get; set; }
        public string ExpectedFinish { get; set; }
        public string TaskStatus { get; set; }
        public int TaskType{ get; set; }
        public string CurrRemDur { get; set; }
        public string Shop { get; set; }
        public string IPDate { get; set; }
        public string PlannedDuration { get; set; }
        public string Predecessors { get; set; }
        public string Successors { get; set; }
        public string RCCP { get; set; }
        public string RTSDate { get; set; }
        public string ActualOrProjectedDuration { get; set; }
        public int Level { get; set; }
        public string TopParentUID { get; set; }
        public string CONTACT { get; set; }
        public string isCritical { get; set; }
        public string Camera { get; set; }
        public string CameraIP { get; set; }
        public bool isRoot { get; set; }
        public bool isGroup { get; set; }
        public string group { get; set; }

        public int ParentId { get; set; }
        public int Id { get; set; }
        public string name { get; set; }

        public List<PTSModel> Children { get; set; }

        public int x { get; set; }
        public int y { get; set; }

    }

    public class ProcessGroup
    {
        public int GroupId { get; set; }
        public string Project { get; set; }
        public string BU { get; set; }
        public string Customer { get; set; }
        public string GroupName { get; set; }
        public bool IsGroup { get; set; }
        public string Uids { get; set; }
        public int TotalCount { get; set; }
    }

    public class ParentChildArray
    {
        public NodeDetails parent { get; set; }
        public NodeDetails child { get; set; }
        public string ParentUID { get; set; }
        public string ChildUID { get; set; }
        public bool IsProceed { get; set; }
        public string isCritical { get; set; }
    }

    public class NodeDetails
    {
        public string id { get; set; }
        public string UID { get; set; }
        public string TaskDescription { get; set; }
        public string ActualFinish { get; set; }
        public string PlannedCompletion { get; set; }
        public string TaskStatus { get; set; }
        public int TaskType { get; set; }
        public string Shop { get; set; }
        public string RCCP { get; set; }
        public string CONTACT { get; set; }
        public string isCritical { get; set; }
        public bool isRoot { get; set; }
        public bool isGroup { get; set; }
        public string group { get; set; }
    }

    public class clsDataFlow
    {
        [JsonProperty("class")]
        public string Class { get; set; }
        public string nodeCategoryProperty { get; set; }
        public string linkFromPortIdProperty { get; set; }
        public string linkToPortIdProperty { get; set; }
        public List<nodeDataArray> nodeDataArray { get; set; }
        public List<linkDataArray> linkDataArray { get; set; }
        public clsDataFlow()
        {
            Class = "go.GraphLinksModel";
            nodeCategoryProperty = "type";
            linkFromPortIdProperty = "frompid";
            linkToPortIdProperty = "topid";
        }
    }

    public class nodeDataArray
    {
        public string key { get; set; }
        public string type { get; set; }
        public string name { get; set; }
        public string TaskDescription { get; set; }
        public string PlannedCompletion { get; set; }
        public string TaskStatus { get; set; }
        public int TaskType { get; set; }
        public string Shop { get; set; }
        public string RCCP { get; set; }
        public string CONTACT { get; set; }
        public string isCritical { get; set; }

        public string Camera { get; set; }
        public bool isRoot { get; set; }
        public bool isGroup { get; set; }
        public string group { get; set; }

        public nodeDataArray()
        {
            isCritical = "No";
            Camera = "";
            isRoot = false;
            isGroup = false;
            group = "";            
        }

    }

    public class linkDataArray
    {
        public string from { get; set; }
        public string frompid { get; set; }
        public string to { get; set; }
        public string topid { get; set; }
        public string isCritical { get; set; }

        public linkDataArray()
        {
            isCritical = "No";
        }
    }
}