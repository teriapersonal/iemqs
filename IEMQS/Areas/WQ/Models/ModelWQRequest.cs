﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IEMQSImplementation;
using IEMQS.Areas.NDE.Models;

namespace IEMQS.Areas.WQ.Models
{
    public class ModelWQRequest
    {
        public bool isheaderExist { get; set; }
        public List<CategoryData> JointType { get; set; }
        public string BU { get; set; }
        public string GeneralProject { get; set; }
        public List<CategoryData> WeldingProcess { get; set; }
        public List<CategoryData> WeldingType { get; set; }
        public List<CategoryData> WeldingPosition { get; set; }
        //public List<string> PNo { get; set; }
        //public List<string> FNo { get; set; }

        public List<CategoryData> CoupanType { get; set; }

        public List<CategoryData> GetSubCatagory(string Key, string strLoc, string BU)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            List<CategoryData> lstGLB002 = null;
            if (BU != string.Empty) //Get BU and Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).Distinct().ToList();


                //lstGLB002 = (from glb002 in db.GLB002
                //             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                //             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location) &&
                //                   glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase)
                //             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).ToList().ToList();
            }
            else
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).Distinct().ToList();

                //lstGLB002 = (from glb002 in db.GLB002
                //             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                //             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                //             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).ToList().ToList();
            }
            return lstGLB002;
        }

        public List<CategoryData> GetSubCatagorywithCode(string Key, string strLoc, string BU)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            List<CategoryData> lstGLB002 = null;
            if (BU != string.Empty) //Get BU and Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location) &&
                                   glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList().ToList();
            }
            else
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList().ToList();
            }
            return lstGLB002;
        }

        public List<CategoryData> LoadSubCatagoryDescription(string Key, string strLoc, string BU)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            List<CategoryData> lstGLB002 = null;
            if (BU != string.Empty) //Get BU and Location Base Sub Category
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location) &&
                                   glb002.BU.Equals(BU, StringComparison.OrdinalIgnoreCase)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).ToList().ToList();
            }
            else
            {
                lstGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                             select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).ToList().ToList();
            }
            return lstGLB002;
        }

        public CategoryData GetCategoryByKey(string mainCategory, string categoryCode, bool isOnlyDescriptionRequired = false)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            CategoryData objGLB002 = new CategoryData();
            ModelWQRequest obj = new ModelWQRequest();
            if (obj.IsCategorywithCodeExist(mainCategory, categoryCode))
            {
                if (!string.IsNullOrEmpty(categoryCode))
                {
                    if (isOnlyDescriptionRequired)
                    {
                        objGLB002 = db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Description }).FirstOrDefault();
                        objGLB002 = (from glb002 in db.GLB002
                                     join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                     where glb001.Category.Equals(mainCategory, StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(categoryCode) && glb002.IsActive == true
                                     select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Description }
                                     ).FirstOrDefault();
                    }
                    else
                    {
                        objGLB002 = (from glb002 in db.GLB002
                                     join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                     where glb001.Category.Equals(mainCategory, StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(categoryCode) && glb002.IsActive == true
                                     select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Description }
                                    ).FirstOrDefault();
                    }
                }
            }
            else
            {
                objGLB002.CategoryDescription = categoryCode;
            }

            return objGLB002;
        }
        public List<CategoryData> GetSubCatagorywithoutBULocationCode(string Key)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            List<CategoryData> lstGLB002 = null;

            lstGLB002 = (from glb002 in db.GLB002
                         join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                         where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true
                         select glb002).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Description }).ToList().ToList();

            return lstGLB002;
        }

        public CategoryData GetCategoryBULocationWise(string categoryCode, string bu, string location)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            CategoryData objGLB002 = new CategoryData();

            if (!string.IsNullOrEmpty(categoryCode))
            {
                objGLB002 = db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)
                                        && i.BU.Equals(bu, StringComparison.OrdinalIgnoreCase)
                                        && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).FirstOrDefault();
            }

            return objGLB002;
        }

        public CategoryData GetCategoryLocationWise(string categoryCode, string bu, string location)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            CategoryData objGLB002 = new CategoryData();

            if (!string.IsNullOrEmpty(categoryCode))
            {
                objGLB002 = db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)
                                        && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).FirstOrDefault();
            }

            return objGLB002;
        }

        public CategoryData GetCategoryDescriptionLocationWise(string categoryCode, string bu, string location)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            CategoryData objGLB002 = new CategoryData();
            if (IsCategoryCodeExist(categoryCode, location))
            {
                if (!string.IsNullOrEmpty(categoryCode))
                {
                    objGLB002 = db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)
                                            && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Description }).FirstOrDefault();
                }
            }
            else
            {
                objGLB002.Code = categoryCode;
                objGLB002.CategoryDescription = categoryCode;
            }
            return objGLB002;
        }
        public bool IsCategoryCodeExist(string categoryCode, string location)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            bool categoryExist = false;

            if (db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).Any())
                categoryExist = true;

            return categoryExist;
        }

        public bool IsCategorywithCodeExist(string mainCategory, string categoryCode)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            bool categoryExist = false;
            var objGLB002 = (from glb002 in db.GLB002
                             join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                             where glb001.Category.Equals(mainCategory, StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(categoryCode) && glb002.IsActive == true
                             select glb002).FirstOrDefault();
            if (objGLB002 != null)
            { categoryExist = true; }

            return categoryExist;
        }

        public CategoryData GetCategoryDescWithMainCategory(string mainCategory, string categoryCode, bool isOnlyDescriptionRequired = false)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            CategoryData objGLB002 = new CategoryData();
            if (IsCategoryExist(mainCategory, categoryCode))
            {
                if (!string.IsNullOrEmpty(categoryCode))
                {
                    if (isOnlyDescriptionRequired)
                    {
                        //objGLB002 = db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Code = i.Code, CategoryDescription = i.Description }).FirstOrDefault();
                        objGLB002 = (from glb002 in db.GLB002
                                     join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                     where glb001.Category.Equals(mainCategory, StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(categoryCode) && glb002.IsActive == true
                                     select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Description }
                                     ).FirstOrDefault();
                    }
                    else
                    {
                        objGLB002 = (from glb002 in db.GLB002
                                     join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                     where glb001.Category.Equals(mainCategory, StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(categoryCode) && glb002.IsActive == true
                                     select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Code + "-" + glb002.Description }
                                    ).FirstOrDefault();
                    }
                }
            }
            else
            {
                objGLB002.CategoryDescription = categoryCode;
            }
            return objGLB002;
        }

        public bool IsCategoryExist(string mainCategory, string categoryCode)
        {
            bool categoryExist = false;
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            var lstCategory = (from glb002 in db.GLB002
                               join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                               where glb001.Category.Equals(mainCategory, StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(categoryCode)
                               select glb002).ToList();

            if (lstCategory.Any())
                categoryExist = true;

            return categoryExist;
        }
        public List<ModelWelderDetail> GetIntExtWelders(string location)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            List<ModelWelderDetail> lstModelWelderDetail = new List<ModelWelderDetail>();

            lstModelWelderDetail = (from a in db.QMS003
                                    where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                                    select new ModelWelderDetail { WelderPSNo = a.WelderPSNo, ShopCode = a.ShopCode, Stamp = a.Stamp, WelderName = a.Name }
                                 )
                                 .Union(from b in db.QMS004
                                        where b.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany != null && b.ActiveWithCompany == true
                                        select new ModelWelderDetail { WelderPSNo = b.WelderPSNo, ShopCode = b.ShopCode, Stamp = b.Stamp, WelderName = b.Name }).ToList();

            return lstModelWelderDetail;
        }

        public string GetWelderName(string welderCode, string location)
        {
            IEMQSEntitiesContext db = new IEMQSEntitiesContext();
            string WelderNameByCode = string.Empty;
            var WelderPSNo = new ModelWQRequest().GetIntExtWelders(location).Where(i => i.WelderPSNo.Trim() == welderCode.Trim()).FirstOrDefault();
            if (WelderPSNo != null)
            {
                WelderNameByCode = WelderPSNo.WelderPSNo + "-" + WelderPSNo.WelderName;
            }
            //string WelderNameByCode = string.Empty;
            //var weldernameqms3 = db.QMS003.Where(a => a.WelderPSNo.Equals(welderCode)).Select(a => a.WelderPSNo + "-" + a.Name).FirstOrDefault();
            //var weldernameqms4 = db.QMS004.Where(a => a.WelderPSNo.Equals(welderCode)).Select(a => a.WelderPSNo + "-" + a.Name).FirstOrDefault();
            //if (weldernameqms3 != null)
            //{
            //    WelderNameByCode = Convert.ToString(weldernameqms3);
            //}
            //if (weldernameqms4 != null)
            //{
            //    WelderNameByCode = Convert.ToString(weldernameqms4);
            //}
            return WelderNameByCode;
        }


        //public bool IsWelderExist(string categoryCode)
        //{
        //    bool categoryExist = false;

        //    if (db.GLB002.Where(i => i.Code.Equals(categoryCode, StringComparison.OrdinalIgnoreCase)).Any())
        //        categoryExist = true;

        //    return categoryExist;
        //}
    }

    public class ModelRoleWiseIndex
    {
        //public string PageTitle { get; set; }
        public string AddNewHref { get; set; }
        public string AnchorText { get; set; }
        public string TableTitle { get; set; }
        public string LinkRoleWise { get; set; }
        public string PageTitle { get; set; }
    }

    public class ModelWelderDetail
    {
        public string WelderPSNo { get; set; }
        public string Stamp { get; set; }
        public string ShopCode { get; set; }
        public bool IsWelderExist { get; set; }
        public string WelderName { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
    }

    public enum RoleForWQ
    {
        [StringValue("Shop In Charge")]
        ShoInCharge = 1,
        [StringValue("WE")]
        WE = 2,
        [StringValue("Initiator")]
        Initiator = 3,
        [StringValue("WQTC PR")]
        WQTC = 4,
    }

    public enum WQRCertificateTab
    {
        [StringValue("tab1")]
        Tab1,
        [StringValue("tab2")]
        Tab2,
        [StringValue("tab3")]
        Tab3,
        [StringValue("tab4")]
        Tab4,
        [StringValue("tab5")]
        Tab5,
        [StringValue("Shop In Charge")]
        ShopInCharge,
    }
}