﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.WQ.Models;
using IEMQS.Models;
using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity.Core.Objects;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.WQ.Controllers
{
    public class MaintainWMLController : clsBase
    {
        ModelWQRequest objModelWQRequest = new ModelWQRequest();
        [SessionExpireFilter]
        // GET: WQ/MaintainWML
        public ActionResult Index()
        {
            var role = (from a in db.ATH001
                        join b in db.ATH004 on a.Role equals b.Id
                        where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                        select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();
            if (role.Where(i => i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.WE1.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                                || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), StringComparison.OrdinalIgnoreCase)
                                || i.RoleDesc.Equals(clsImplementationEnum.UserRoleName.WE3.GetStringValue(), StringComparison.OrdinalIgnoreCase)).ToList().Any())
            {
                ViewBag.role = "Shop In Charge";
            }
            else
            {
                ViewBag.role = "NA";
            }
            var lstLocation = (from a in db.WQR004
                               join b in db.WQR008 on a.Location equals b.Location
                               select new CategoryData { Value = a.Location, CategoryDescription = a.Location + " - " + b.Description }
                                 ).Distinct()
                                 .Union(from a in db.WQR005
                                        join b in db.WQR008 on a.Location equals b.Location
                                        select new CategoryData { Value = a.Location, CategoryDescription = a.Location + " - " + b.Description }
                                 ).Distinct().Union(from a in db.WQR006
                                                    join b in db.WQR008 on a.Location equals b.Location
                                                    select new CategoryData { Value = a.Location, CategoryDescription = a.Location + " - " + b.Description }
                                 ).Distinct();

            ViewBag.lstLocation = lstLocation;
            ViewBag.Title = "Maintain Welder Master List";
            return View();
        }

        [UserPermissions]
        public ActionResult DisplayWML()
        {
            var role = (from a in db.ATH001
                        join b in db.ATH004 on a.Role equals b.Id
                        where a.Employee.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                        select new { RoleId = a.Role, RoleDesc = b.Role }).ToList();

            ViewBag.role = "NA";
            var lstLocation = (from a in db.WQR004
                               join b in db.WQR008 on a.Location equals b.Location
                               select new CategoryData { Value = a.Location, CategoryDescription = a.Location + " - " + b.Description }
                                 ).Distinct()
                                 .Union(from a in db.WQR005
                                        join b in db.WQR008 on a.Location equals b.Location
                                        select new CategoryData { Value = a.Location, CategoryDescription = a.Location + " - " + b.Description }
                                 ).Distinct().Union(from a in db.WQR006
                                                    join b in db.WQR008 on a.Location equals b.Location
                                                    select new CategoryData { Value = a.Location, CategoryDescription = a.Location + " - " + b.Description }
                                 ).Distinct();

            ViewBag.lstLocation = lstLocation;
            ViewBag.Title = "Display Welder Master List";
            ViewBag.IsDisplay = true;
            return View("Index");
        }

        [SessionExpireFilter]
        [HttpPost]
        public ActionResult LoadTabWisePartial(string location, string role, string tabname, bool isDisplay, string project)
        {
            ViewBag.Location = location;
            ViewBag.tabname = tabname;
            ViewBag.person = role;
            ViewBag.IsDisplay = isDisplay;
            ViewBag.Project = project;
            var objWQR008 = db.WQR008.FirstOrDefault(f => f.Location == location);
            ViewBag.ParentLocation = objWQR008 != null? objWQR008.ParentLocation : location;

            string filepath = string.Empty;
            switch (tabname)
            {
                case "Groove":
                    ViewBag.Title = "Groove/Fillet";
                    ViewBag.GridType = clsImplementationEnum.GridType.WPQCertificate.GetStringValue();
                    filepath = "~/Areas/WQ/Views/MaintainWML/_LoadWPQCertificateListPartial.cshtml"; //"~/Areas/WQ/Views/WPQCertificate/_LoadWPQCertificateListPartial.cshtml";
                    break;
                case "Overlay":
                    ViewBag.Title = "Overlay";
                    ViewBag.GridType = clsImplementationEnum.GridType.WOPQCertificate.GetStringValue();
                    filepath = "~/Areas/WQ/Views/MaintainWML/_LoadWOPQCertificateListPartial.cshtml"; //"~/Areas/WQ/Views/WPQCertificate/_LoadWPQCertificateListPartial.cshtml";
                    break;
                case "TSS":
                    ViewBag.Title = "TSS";
                    ViewBag.GridType = clsImplementationEnum.GridType.TTSCertificate.GetStringValue();
                    filepath = "~/Areas/WQ/Views/MaintainWML/_LoadTSSCertificateListPartial.cshtml"; //"TSSCertificate";
                    break;
                default:
                    ViewBag.Title = "Groove/Fillet";
                    ViewBag.GridType = clsImplementationEnum.GridType.WPQCertificate.GetStringValue();
                    filepath = "~/Areas/WQ/Views/WPQCertificate/_LoadWPQCertificateListPartial.cshtml";
                    break;
            }
            return PartialView(filepath);
        }

        #region WMLHeader
        [HttpPost, SessionExpireFilter]
        public JsonResult SaveWMLHeader(WML005 objWML005, bool isChangeIProject = false, bool isDisplay = false)
        {
            var objResponseMsg = new clsHelper.ResponceMsgWithObject(); 
            try
            {
                if(db.WML005.Any(a => a.WMLType != objWML005.WMLType && a.Project == objWML005.Project))
                {
                    objResponseMsg.Value = "Project ("+ objWML005.Project+") Already being used in other WML Type!";
                    objResponseMsg.Key = false;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var lstIProject = new string[] { };
                if (!String.IsNullOrWhiteSpace(objWML005.IProject))
                {
                    lstIProject = objWML005.IProject.Split(',');
                    foreach (var iProject in lstIProject)
                    {
                        if (db.WML005.Any(a => a.WMLType != objWML005.WMLType && a.Project != objWML005.Project && a.IProject == iProject))
                        {
                            objResponseMsg.data = iProject;
                            objResponseMsg.Value = "Identical Project (" + iProject + ") Already being used in other WML Type!";
                            objResponseMsg.Key = false; ;
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
               
                var lstWML005 = db.WML005.Where(a => a.Location == objWML005.Location && a.WMLType == objWML005.WMLType && a.Project == objWML005.Project);
                if (lstWML005.Any())
                {
                    if (!isChangeIProject || isDisplay) // return exist data of Identical Project
                    {
                        objResponseMsg.data = String.Join(",", lstWML005.Select(s => s.IProject));
                        objResponseMsg.Value = "";
                        objResponseMsg.Key = true;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!String.IsNullOrWhiteSpace(objWML005.IProject))
                    {
                        foreach (var iProject in lstIProject)
                        {
                            if(!lstWML005.Any(f => f.IProject == iProject)) //Add Identical Project if not exist
                            {
                                var newWML005 = new WML005();
                                newWML005.Location = objWML005.Location;
                                newWML005.WMLType = objWML005.WMLType;
                                newWML005.Project = objWML005.Project;
                                newWML005.IProject = iProject;
                                newWML005.CreatedBy = objClsLoginInfo.UserName;
                                newWML005.CreatedOn = DateTime.Now;
                                db.WML005.Add(newWML005);
                            }
                        }
                        db.WML005.RemoveRange(lstWML005.Where(w => !lstIProject.Contains(w.IProject))); 
                    }
                    else // Clear Identical project
                    {
                        var isFirst = true;
                        foreach (var item in lstWML005)
                        {
                            if (isFirst) // set WML header without any Identical Project
                            {
                                item.Project = objWML005.Project;
                                item.IProject = "";
                                isFirst = false;
                            }
                            else
                                db.WML005.Remove(item);
                        }
                    }

                    db.SaveChanges();
                    objResponseMsg.Value = "Record updated successfully";
                    objResponseMsg.Key = true;
                }
                else if (isDisplay)
                {
                    objResponseMsg.Value = "";
                    objResponseMsg.Key = true;
                }
                else 
                {
                    if (!String.IsNullOrWhiteSpace(objWML005.IProject))
                    {
                        foreach (var iProject in objWML005.IProject.Split(','))
                        {
                            var newWML005 = new WML005();
                            newWML005.Location = objWML005.Location;
                            newWML005.WMLType = objWML005.WMLType;
                            newWML005.Project = objWML005.Project;
                            newWML005.IProject = iProject;
                            newWML005.CreatedBy = objClsLoginInfo.UserName;
                            newWML005.CreatedOn = DateTime.Now;
                            db.WML005.Add(newWML005);
                        }
                    }
                    else
                    {
                        objWML005.CreatedBy = objClsLoginInfo.UserName;
                        objWML005.CreatedOn = DateTime.Now;
                        objWML005.IProject = "";
                        db.WML005.Add(objWML005);
                    }
                    
                    db.SaveChanges();
                    objResponseMsg.Value = "Record added successfully";
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteWMLHeader(WML005 wml005)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            var lstWML005 = db.WML005.Where(x => x.Location == wml005.Location && x.WMLType == wml005.WMLType && x.Project == wml005.Project);
            try
            {
                if (lstWML005.Any())
                {
                    var lstwml1 = db.WML001.Where(w => w.Location == wml005.Location && w.WMLType == wml005.WMLType && w.Project == wml005.Project);
                    var psnowml1 = lstwml1.Select(s => s.Welder);
                    var lstUsedPsno1 = db.WPS025.Where(a => psnowml1.Contains(a.WelderPSNO) && a.JointType == "Groove/Fillet").Select(s=>s.WelderPSNO).Distinct();
                    db.WML001.RemoveRange(lstwml1.Where(w=> !lstUsedPsno1.Contains(w.Welder)));

                    var lstwml2 = db.WML002.Where(w => w.Location == wml005.Location && w.WMLType == wml005.WMLType && w.Project == wml005.Project);
                    var psnowml2 = lstwml2.Select(s => s.WelderName);
                    var lstUsedPsno2 = db.WPS025.Where(a => psnowml2.Contains(a.WelderPSNO) && a.JointType == "Overlay").Select(s => s.WelderPSNO).Distinct();
                    db.WML002.RemoveRange(lstwml2.Where(w => !lstUsedPsno2.Contains(w.WelderName)));

                    var lstwml3 = db.WML003.Where(w => w.Location == wml005.Location && w.WMLType == wml005.WMLType && w.Project == wml005.Project);
                    var psnowml3 = lstwml3.Select(s => s.WelderName);
                    var lstUsedPsno3 = db.WPS025.Where(a => psnowml3.Contains(a.WelderPSNO) && a.JointType == "T#TS").Select(s => s.WelderPSNO).Distinct();
                    db.WML003.RemoveRange(lstwml3.Where(w => !lstUsedPsno3.Contains(w.WelderName)));
 
                    db.WML005.RemoveRange(lstWML005);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Unused Welder Master Record are deleted successfully";
                }
                else
                {
                    objResponseMsg.Value = "Record already deleted";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }
        #endregion

        #region Welder Notes

        [HttpPost]
        public ActionResult _WelderMasterNotes(string tabname, string location, bool isDisplay, string WMLType)
        {
            ViewBag.tabname = tabname;
            ViewBag.location = location;
            ViewBag.WMLType = WMLType;
            ViewBag.IsDisplay = isDisplay;

            return PartialView("_WelderMasterNotes");
        }

        [HttpPost]
        public ActionResult LoadDataGridData(JQueryDataTableParamModel param, string tabname, string location, string WMLType)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhereCondition += "1=1 ";

                #endregion

                //search Condition 

                if (!string.IsNullOrEmpty(tabname))
                {
                    switch (tabname)
                    {
                        case "Groove":
                            strWhereCondition += "and IsGroove = 1 ";
                            break;
                        case "Overlay":
                            strWhereCondition += "and IsOverlay = 1 ";
                            break;
                        case "TSS":
                            strWhereCondition += "and IsTTS = 1 ";
                            break;
                    }
                }

                if (!string.IsNullOrEmpty(location))
                    strWhereCondition += "and Location = '" + location + "' ";

                if (!string.IsNullOrEmpty(WMLType))
                    strWhereCondition += "and WMLType = '" + WMLType + "' ";

                string[] columnName = { "Notes" };
                if (!string.IsNullOrEmpty(param.sSearch))
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);

                var lstResult = db.SP_WML_GET_NOTES_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.Id),
                                Convert.ToString(uc.Location),
                                Convert.ToString(uc.Notes),
                                ""
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetInlineEditableRow(int id, string tabname, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();

                if (id == 0)
                {
                    var QPitems = new SelectList(new List<string>());
                    int newRecordId = 0;
                    var newRecord = new[] {
                                        "0",
                                        Helper.GenerateHidden(newRecordId, "Location",""),
                                        Helper.GenerateTextbox(newRecordId, "Notes",  "", "", false, "", "","form-control Notes"),
                                        Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "AddFabrication('"+ tabname +"')" ),
                                    };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_WML_GET_NOTES_DATA(1, 0, "", "Id = " + id).Take(1).ToList();
                    if (isReadOnly)
                    {
                        data = (from uc in lstResult
                                select new[]
                               {
                                Convert.ToString(uc.Id)
                                ,Convert.ToString(uc.Location)
                                ,Convert.ToString(uc.Notes)
                                ,""
                          }).ToList();
                    }
                    else
                    {
                        data = (from uc in lstResult
                                select new[] {
                                        uc.Id.ToString()
                                        ,Helper.GenerateHidden(uc.Id, "Location","")
                                        ,Helper.GenerateTextbox(uc.Id, "Notes",uc.Notes, "UpdateData(this, "+ uc.Id+",true)", false, "", "","form-control editable Notes",false),
                                       "<span class='editable'><a id='Cancel' name='Cancel' title='Cancel' class='blue action' onclick='ReloadGrid()'><i class='fa fa-close'></i></a></span>" + Helper.GenerateActionIcon(uc.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteNotes("+uc.Id+")","",false),
                          }).ToList();
                    }
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult DeleteNotes(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            var objWML004 = db.WML004.FirstOrDefault(x => x.Id == Id);
            try
            {
                if (objWML004 != null)
                {
                    db.WML004.Remove(objWML004);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Record deleted successfully";
                }
                else
                {
                    objResponseMsg.Value = "Record already deleted";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }
        [HttpPost]
        public ActionResult SaveNewRecord(FormCollection fc)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var wml004 = new WML004();
                string notetype = fc["Notetype"];
                wml004.IsGroove = false;
                wml004.IsOverlay = false;
                wml004.IsTTS = false;
                if (notetype == "Groove")
                    wml004.IsGroove = true;
                else if (notetype == "Overlay")
                    wml004.IsOverlay = true;
                else if (notetype == "TSS")
                    wml004.IsTTS = true;
                wml004.Notes = fc["Notes" + 0];
                wml004.Location = fc["Location"];
                wml004.WMLType = fc["WMLType"];
                wml004.CreatedOn = DateTime.Now;
                wml004.CreatedBy = objClsLoginInfo.UserName;
                db.WML004.Add(wml004);
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data save successfully.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult UpdateDetails(int id, string columnName, string columnValue)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                db.SP_COMMON_TABLE_UPDATE("WML004", id, "id", columnName, columnValue, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region WML WPQ Grid
        [HttpPost]
        public JsonResult LoadWMLWPQCertificateData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                string[] columnName = { "WQTNo", "Welder", "WelderStampNo", "Shop", "TypeOfTestCoupon", "WeldingProcessQualified", "TypeQualified", "ValueUpto", "PNo", "FNo", "PlatePipeQualified", "DepositedThicknessQualified", "PlatePipePositionActual", "PipeWeldingPositionQualified", "PlatePipePositionQualified", "BaseMetalThicknessQualified", "BackingQualified", "InertGasBackingQualified", "CircWeldOvly", "Remarks" };
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhere += "1=1 and ValueUpto >= getdate() and Location='" + param.MTStatus + "' and WMLType='" + Request["WMLType"] + "'";
                if (!string.IsNullOrWhiteSpace(param.Project))
                    strWhere += " and Project='" + param.Project + "'";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                var lstResult = db.SP_WML_WPQCERTIFICATE_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                NDEModels objNDEModels = new NDEModels();
                ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();
                var WeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                var WeldingType = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                var Backing = Manager.GetSubCatagorywithoutBULocation("Backing");
                var InertGasBacking = Manager.GetSubCatagorywithoutBULocation("Inert gas backing");


                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.Id),
                                //!string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? Convert.ToString(uc.SpecificationNotes) : Helper.GenerateHTMLTextboxOnChanged(uc.Id, "SpecificationNotes", Convert.ToString(uc.SpecificationNotes), "UpdateCertificate(this,\"Groove\", "+ uc.Id +" );",false,"",string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ?false:true, "200"), //: Convert.ToString(uc.QualityIdDesc)),// Convert.ToString(uc.SpecificationNotes),
                                uc.WelderPsNo,
                                uc.Welder,
                                uc.WelderStampNo,
                                uc.Shop,//db.COM002.Where(i => i.t_dtyp == 3 && i.t_dimx != "" && (i.t_dimx== uc.Shop)).Select(i => i.t_dimx + "-" + i.t_desc).FirstOrDefault(),
                                GetCodeDesc(uc.WeldingProcessQualified,WeldingProcess),
                                //objModelWQRequest.GetCategoryDescWithMainCategory("Welding Process",uc.WeldingProcessQualified).CategoryDescription,
                                GetCodeDesc(uc.TypeQualified,WeldingType),
                                //objModelWQRequest.GetCategoryDescWithMainCategory("Welding Type",uc.TypeQualified).CategoryDescription,
                                //objModelWQRequest.GetCategoryDescWithMainCategory("Joint Type WQ",uc.JointType).CategoryDescription,
                                uc.PlatePipePositionActual,
                                uc.PipeWeldingPositionQualified,
                                uc.PlatePipePositionQualified,
                                uc.PNo,
                                uc.FNo,
                                uc.BaseMetalThicknessQualified,
                                uc.DepositedThicknessQualified,
                                uc.PlatePipeQualified,
                                GetCodeDesc(uc.BackingQualified,Backing),
                                GetCodeDesc(uc.InertGasBackingQualified,InertGasBacking),
                                uc.CircWeldOvly,
                                uc.LastWeldDate.HasValue? uc.LastWeldDate.Value.ToString("dd/MM/yyyy"): "",
                                uc.ValueUpto.HasValue? uc.ValueUpto.Value.ToString("dd/MM/yyyy"): "",
                                uc.WQTNo,
                                uc.Remarks,
                                (param.Flag ? "" :Helper.GenerateActionIcon(uc.Id, "Edit", "Edit Record", "fa fa-pencil-square-o")+' '+(db.WPS025.Any(a=>a.WelderPSNO == uc.WelderPsNo && a.JointType == "Groove/Fillet")? Helper.GenerateActionIcon(uc.Id, "Delete", "Token is generated", "fa fa-trash-o", "DeleteRecord("+uc.Id+")","",true): Helper.GenerateActionIcon(uc.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteRecord("+uc.Id+")")))
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetWPQInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();

                NDEModels objNDEModels = new NDEModels();
                ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                Convert.ToString(newRecordId),
                                //clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.SpecificationNotes),
                                Helper.HTMLAutoComplete(newRecordId,"txtWelderName","","",false,"","Welder",false)+ Helper.GenerateHidden(newRecordId,"Welder"),
                                Helper.GenerateLabelFor(newRecordId,"lblWelderName",classname:"clsWelderName"),
                                Helper.GenerateTextbox(newRecordId,"WelderStampNo","","",false,"","6","",false),
                                Helper.HTMLAutoComplete(newRecordId,"txtShop","","",false,"","Shop",false)+ Helper.GenerateHidden(newRecordId,"Shop"),
                                Helper.HTMLAutoComplete(newRecordId,"txtWeldingProcessQualified","","",false,"","WeldingProcessQualified",false)+ Helper.GenerateHidden(newRecordId,"WeldingProcessQualified"),
                                Helper.HTMLAutoComplete(newRecordId,"txtTypeQualified","","",false,"","TypeQualified",false)+ Helper.GenerateHidden(newRecordId,"TypeQualified"),
                                //Helper.HTMLAutoComplete(newRecordId,"txtJointType","","",false,"","JointType",false)+ Helper.GenerateHidden(newRecordId,"JointType"),
                                Helper.GenerateTextbox(newRecordId,"PlatePipePositionActual",maxlength:"50"),
                                Helper.GenerateTextbox(newRecordId,"PipeWeldingPositionQualified",maxlength:"50"),
                                Helper.GenerateTextbox(newRecordId,"PlatePipePositionQualified",maxlength:"50"),
                                Helper.GenerateTextbox(newRecordId,"PNo",maxlength:"50"),
                                Helper.GenerateTextbox(newRecordId,"FNo",maxlength:"50"),
                                Helper.GenerateTextbox(newRecordId,"BaseMetalThicknessQualified",maxlength:"30"),
                                Helper.GenerateTextbox(newRecordId,"DepositedThicknessQualified",maxlength:"50"),
                                Helper.GenerateTextbox(newRecordId,"PlatePipeQualified","Plate and pipe >=73", maxlength:"70"),
                                Helper.HTMLAutoComplete(newRecordId,"txtBackingQualified","","",false,"","BackingQualified",false)+ Helper.GenerateHidden(newRecordId,"BackingQualified"),
                                Helper.HTMLAutoComplete(newRecordId,"txtInertGasBackingQualified","","",false,"","InertGasBackingQualified",false)+ Helper.GenerateHidden(newRecordId,"InertGasBackingQualified"),
                                Helper.GenerateTextbox(newRecordId,"CircWeldOvly",maxlength:"10"),
                                Helper.GenerateTextbox(newRecordId,"LastWeldDate"), //c.LastWeldDate.HasValue? c.LastWeldDate.Value.ToString("dd/MM/yyyy"): "",
                                Helper.GenerateTextbox(newRecordId,"ValueUpto",isReadOnly: true), //c.ValueUpto.HasValue? c.ValueUpto.Value.ToString("dd/MM/yyyy"): "",
                                Helper.GenerateTextbox(newRecordId,"WQTNo",maxlength:"10"),
                                Helper.GenerateTextbox(newRecordId,"Remarks",maxlength:"100"),
                                Helper.GenerateGridButton(newRecordId, "Save", "Add New Record", "fa fa-plus", "EditRecord("+newRecordId+")")
                                };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_WML_WPQCERTIFICATE_DETAILS(1, 0, "", "Id = " + id).Take(1).ToList();
                    //var lstResult = db.WML001.Where(w=>w.Id == id).ToList();

                    foreach (var c in lstResult)
                    {
                        var isTokenExist = db.WPS025.Any(a => a.WelderPSNO == c.WelderPsNo && a.JointType == "Groove/Fillet");
                        data.Add(new string[]{
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                //clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//!string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? Convert.ToString(uc.SpecificationNotes) : Helper.GenerateHTMLTextboxOnChanged(uc.Id, "SpecificationNotes", Convert.ToString(uc.SpecificationNotes), "UpdateCertificate(this,\"Groove\", "+ uc.Id +" );",false,"",string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ?false:true, "200"), //: Convert.ToString(uc.QualityIdDesc)),// Convert.ToString(uc.SpecificationNotes),
                                isReadOnly || isTokenExist? c.WelderPsNo: Helper.HTMLAutoComplete(c.Id,"txtWelderName",c.WelderPsNo,"",false,"","Welder",false)+ Helper.GenerateHidden(c.Id,"Welder",c.WelderPsNo),
                                Helper.GenerateLabelFor(c.Id,"lblWelderName",c.Welder,"","clsWelderName"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.WelderStampNo: Helper.GenerateTextbox(c.Id,"WelderStampNo",c.WelderStampNo,"UpdateData(this, "+ c.Id +");",false,"","100","",false),
                                isReadOnly? c.Shop: Helper.HTMLAutoComplete(c.Id,"txtShop",c.Shop,"",false,"","Shop",false)+ Helper.GenerateHidden(c.Id,"Shop",c.Shop),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? objNDEModels.GetCategory(c.WeldingProcessQualified,true).CategoryDescription: Helper.HTMLAutoComplete(c.Id,"txtWeldingProcessQualified",objNDEModels.GetCategory(c.WeldingProcessQualified,true).CategoryDescription,"",false,"","WeldingProcessQualified")+ Helper.GenerateHidden(c.Id,"WeldingProcessQualified",c.WeldingProcessQualified),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? objNDEModels.GetCategory(c.TypeQualified,true).CategoryDescription: Helper.HTMLAutoComplete(c.Id,"txtTypeQualified",objNDEModels.GetCategory(c.TypeQualified,true).CategoryDescription,"",false,"","TypeQualified")+ Helper.GenerateHidden(c.Id,"TypeQualified",c.TypeQualified),
                                //clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),////Helper.HTMLAutoComplete(c.Id,"txtJointType","","",false,"","JointType",false)+ Helper.GenerateHidden(newRecordId,"JointType"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.PlatePipePositionActual:Helper.GenerateTextbox(c.Id,"PlatePipePositionActual",c.PlatePipePositionActual, maxlength: "50"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.PipeWeldingPositionQualified:Helper.GenerateTextbox(c.Id,"PipeWeldingPositionQualified",c.PipeWeldingPositionQualified,maxlength: "50"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.PlatePipePositionQualified:Helper.GenerateTextbox(c.Id,"PlatePipePositionQualified",c.PlatePipePositionQualified,maxlength: "50"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.PNo:Helper.GenerateTextbox(c.Id,"PNo",c.PNo,maxlength: "50"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.FNo:Helper.GenerateTextbox(c.Id,"FNo",c.FNo,maxlength: "50"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.BaseMetalThicknessQualified:Helper.GenerateTextbox(c.Id,"BaseMetalThicknessQualified",c.BaseMetalThicknessQualified,maxlength: "30"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.DepositedThicknessQualified:Helper.GenerateTextbox(c.Id,"DepositedThicknessQualified",c.DepositedThicknessQualified,maxlength: "50"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.PlatePipeQualified:Helper.GenerateTextbox(c.Id,"PlatePipeQualified",c.PlatePipeQualified, maxlength:"70", onchange:"setPlatePipeQualified(this)"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? objNDEModels.GetCategory(c.BackingQualified,true).CategoryDescription: Helper.HTMLAutoComplete(c.Id,"txtBackingQualified",objNDEModels.GetCategory(c.BackingQualified,true).CategoryDescription,"",false,"","BackingQualified")+ Helper.GenerateHidden(c.Id,"BackingQualified",c.BackingQualified),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? objNDEModels.GetCategory(c.InertGasBackingQualified,true).CategoryDescription: Helper.HTMLAutoComplete(c.Id,"txtInertGasBackingQualified",objNDEModels.GetCategory(c.InertGasBackingQualified,true).CategoryDescription,"",false,"","InertGasBackingQualified")+ Helper.GenerateHidden(c.Id,"InertGasBackingQualified",c.InertGasBackingQualified),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.CircWeldOvly: Helper.GenerateTextbox(c.Id,"CircWeldOvly",c.CircWeldOvly,maxlength:"10"),
                                isReadOnly? (c.LastWeldDate.HasValue? c.LastWeldDate.Value.ToString("dd/MM/yyyy"): ""): Helper.GenerateTextbox(c.Id,"LastWeldDate",c.LastWeldDate.HasValue? c.LastWeldDate.Value.ToString("yyyy-MM-dd"): ""),
                                isReadOnly? (c.ValueUpto.HasValue? c.ValueUpto.Value.ToString("dd/MM/yyyy"): "") :Helper.GenerateTextbox(c.Id,"ValueUpto",c.ValueUpto.HasValue? c.ValueUpto.Value.ToString("yyyy-MM-dd"): "",isReadOnly: true),
                                isReadOnly? c.WQTNo: Helper.GenerateTextbox(c.Id,"WQTNo",c.WQTNo,maxlength:"10"),
                                isReadOnly || isTokenExist? c.Remarks: Helper.GenerateTextbox(c.Id,"Remarks",c.Remarks,maxlength:"100"),
                                isReadOnly? Helper.GenerateActionIcon(c.Id, "Edit", "Edit Record", "fa fa-pencil-square-o")+' '+(isTokenExist ? Helper.GenerateActionIcon(c.Id, "Delete", "Token is generated", "fa fa-trash-o", "DeleteRecord("+c.Id+")","",true): Helper.GenerateActionIcon(c.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteRecord("+c.Id+")")) : Helper.GenerateActionIcon(c.Id, "Save", "Save Record", "fa fa-save", "EditRecord("+c.Id+")") + " " + Helper.GenerateActionIcon(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine();")
                        });
                    }
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveWMLWPQCertificate(FormCollection fc, int Id)
        {
            var objResponseMsg = new clsHelper.ResponseMsg();
            var objWML001 = new WML001();
            try
            {
                if (Id > 0)
                {
                    objWML001 = db.WML001.FirstOrDefault(w => w.Id == Id);
                    ////objWML001.Location = fc["Location" + Id];
                    //objWML001.SpecificationNotes = fc["SpecificationNotes" + Id];
                    if (!db.WPS025.Any(a => a.WelderPSNO == objWML001.Welder && a.JointType == "Groove/Fillet"))
                    {
                        objWML001.Welder = (fc["Welder" + Id] + "").Split('-')[0];
                        objWML001.Remarks = fc["Remarks" + Id];
                    }
                    //objWML001.WPSNo = fc["WPSNo" + Id];
                    //objWML001.WelderStampNo = fc["WelderStampNo" + Id];
                    //objWML001.TypeOfTestCoupon = fc["TypeOfTestCoupon" + Id];
                    //objWML001.WeldingProcessQualified = fc["WeldingProcessQualified" + Id];
                    //objWML001.TypeQualified = fc["TypeQualified" + Id];
                    //objWML001.PNo = fc["PNo" + Id];
                    //objWML001.FNo = fc["FNo" + Id];
                    //objWML001.PlatePipeQualified = fc["PlatePipeQualified" + Id];
                    //objWML001.DepositedThicknessQualified = fc["DepositedThicknessQualified" + Id];
                    //objWML001.PlatePipePositionActual = fc["PlatePipePositionActual" + Id];
                    //objWML001.PipeWeldingPositionQualified = fc["PipeWeldingPositionQualified" + Id];
                    //objWML001.PlatePipePositionQualified = fc["PlatePipePositionQualified" + Id];
                    //objWML001.BaseMetalThicknessQualified = fc["BaseMetalThicknessQualified" + Id];
                    //objWML001.BackingQualified = fc["BackingQualified" + Id];
                    //objWML001.InertGasBackingQualified = fc["InertGasBackingQualified" + Id];
                    //objWML001.CircWeldOvly = fc["CircWeldOvly" + Id];
                    objWML001.WQTNo = fc["WQTNo" + Id];
                    objWML001.Shop = (fc["Shop" + Id] + "").Split('-')[0];
                    objWML001.LastWeldDate = Convert.ToDateTime(fc["LastWeldDate" + Id]);
                    objWML001.ValueUpto = Convert.ToDateTime(fc["ValueUpto" + Id]);
                    //objWML001.JointType = fc["JointType" + Id];
                    objWML001.EditedBy = objClsLoginInfo.UserName;
                    objWML001.EditedOn = DateTime.Now;
                    //objWML001.MaintainedBy = fc["MaintainedBy" + Id];
                    //objWML001.ModifiedBy = fc["ModifiedBy" + Id];
                    //objWML001.MaintainedOn = Convert.ToDateTime(fc["MaintainedOn" + Id]);
                    //objWML001.ModifiedOn = Convert.ToDateTime(fc["ModifiedOn" + Id]);

                    db.SaveChanges();
                    objResponseMsg.Value = "Record updated successfully";
                    objResponseMsg.Key = true;
                }
                else
                {
                    objWML001.WQTNo = fc["WQTNo" + Id];
                    objWML001.Welder = fc["Welder" + Id];
                    objWML001.WelderStampNo = fc["WelderStampNo" + Id];
                    objWML001.Location = fc["Location" + Id];
                    objWML001.Project = fc["Project" + Id];
                    objWML001.WMLType = fc["WMLType" + Id]; 

                    var typeASME = clsImplementationEnum.WMLType.ASME.GetStringValue();
                    if (db.WML001.Any(a => a.Location == objWML001.Location && a.Welder == objWML001.Welder && a.WelderStampNo == objWML001.WelderStampNo && a.WMLType == objWML001.WMLType && a.WQTNo == objWML001.WQTNo && (a.WMLType == typeASME || a.Project == objWML001.Project))
                        || db.WML002.Any(a => a.Location == objWML001.Location && a.WelderName == objWML001.Welder && a.WelderStampNo == objWML001.WelderStampNo && a.WMLType == objWML001.WMLType && a.WQTNo == objWML001.WQTNo && (a.WMLType == typeASME || a.Project == objWML001.Project))
                        || db.WML003.Any(a => a.Location == objWML001.Location && a.WelderName == objWML001.Welder && a.WelderStampNo == objWML001.WelderStampNo && a.WMLType == objWML001.WMLType && a.WQTNo == objWML001.WQTNo && a.Project == objWML001.Project))
                    {
                        objResponseMsg.Value = "WQT No. is already being used";
                        objResponseMsg.Key = false;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objWML001.SpecificationNotes = fc["SpecificationNotes" + Id];
                    objWML001.Shop = fc["Shop" + Id];
                    objWML001.TypeOfTestCoupon = fc["TypeOfTestCoupon" + Id];
                    objWML001.WeldingProcessQualified = fc["WeldingProcessQualified" + Id];
                    objWML001.TypeQualified = fc["TypeQualified" + Id];
                    objWML001.ValueUpto = Convert.ToDateTime(fc["ValueUpto" + Id]);
                    objWML001.PNo = fc["PNo" + Id];
                    objWML001.FNo = fc["FNo" + Id];
                    objWML001.PlatePipeQualified = fc["PlatePipeQualified" + Id];
                    objWML001.DepositedThicknessQualified = fc["DepositedThicknessQualified" + Id];
                    objWML001.PlatePipePositionActual = fc["PlatePipePositionActual" + Id];
                    objWML001.PipeWeldingPositionQualified = fc["PipeWeldingPositionQualified" + Id];
                    objWML001.PlatePipePositionQualified = fc["PlatePipePositionQualified" + Id];
                    objWML001.BaseMetalThicknessQualified = fc["BaseMetalThicknessQualified" + Id];
                    objWML001.BackingQualified = fc["BackingQualified" + Id];
                    objWML001.InertGasBackingQualified = fc["InertGasBackingQualified" + Id];
                    objWML001.CircWeldOvly = fc["CircWeldOvly" + Id];
                    objWML001.Remarks = fc["Remarks" + Id];
                    objWML001.LastWeldDate = Convert.ToDateTime(fc["LastWeldDate" + Id]);
                    objWML001.JointType = fc["JointType" + Id];
                    objWML001.CreatedBy = objClsLoginInfo.UserName;
                    objWML001.CreatedOn = DateTime.Now;

                    db.WML001.Add(objWML001);
                    db.SaveChanges();
                    objResponseMsg.Value = "Record added successfully";
                    objResponseMsg.Key = true;

                    //#region Add New Spot Line1
                    //objCONFIG.Value = strValue;
                    //objCONFIG.CreatedBy = objClsLoginInfo.UserName;
                    //objCONFIG.CreatedOn = DateTime.Now;
                    //db.CONFIG.Add(objTCPobjCONFIG002);
                    //objResponseMsg.Value = "Value added successfully";
                    //objResponseMsg.Key = true;
                    //#endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteWMLWPQCertificate(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            var objWML001 = db.WML001.FirstOrDefault(x => x.Id == Id);
            try
            {
                if (objWML001 != null)
                {
                    if(db.WPS025.Any(a => a.WelderPSNO == objWML001.Welder && a.JointType == "Groove/Fillet"))
                        objResponseMsg.Value = "Welder is being used in token!";
                    else
                    {
                        db.WML001.Remove(objWML001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Record deleted successfully";
                    }
                }
                else
                    objResponseMsg.Value = "Record already deleted";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        #region Excel
        [SessionExpireFilter]
        public ActionResult ImportExcelWMLWPQ(HttpPostedFileBase upload, string Location, string WMLType, string Project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (upload != null)
            {
                if (Path.GetExtension(upload.FileName).ToLower() == ".xlsx" || Path.GetExtension(upload.FileName).ToLower() == ".xls")
                {
                    ExcelPackage package = new ExcelPackage(upload.InputStream);
                    ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();

                    DataTable dtHeaderExcel = new DataTable();
                    //List<CategoryData> errors = new List<CategoryData>();
                    bool isError;

                    DataSet ds = WPQToChechValidation(package, Location, WMLType, Project, out isError);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Record Found!";
                    }
                    else if (!isError)
                    {
                        try
                        {
                            foreach (DataRow item in ds.Tables[0].Rows)
                            {
                                string Welder = item.Field<string>("PS NO.");
                                item.Field<string>("NAME");
                                string WelderStampNo = item.Field<string>("STAMP");
                                string Shop = item.Field<string>("SHOP");
                                string WQTNo = item.Field<string>("WQT NO");
                                string WeldingProcessQualified = item.Field<string>("WeldingProcessQualifiedCode");
                                string TypeQualified = item.Field<string>("TypeQualifiedCode");
                                string PlatePipePositionActual = item.Field<string>("POSITION GROOVE PLATE & PIPE > = 610");
                                string PipeWeldingPositionQualified = item.Field<string>("POSITION GROOVE PIPE < 610");
                                string PlatePipePositionQualified = item.Field<string>("POSITION OF FILLET");
                                string PNo = item.Field<string>("P NO.");
                                string BaseMetalThicknessQualified = item.Field<string>("BASE MATERIAL THICKNESS IN (MM)");
                                string DepositedThicknessQualified = item.Field<string>("DEPOSITED THICKNESS(MM)");
                                string PlatePipeQualified = item.Field<string>("PIPE OD(mm) MIN");
                                string FNo = item.Field<string>("F NO.");
                                string BackingQualified = item.Field<string>("BackingQualifiedCode");
                                string InertGasBackingQualified = item.Field<string>("InertGasBackingQualifiedCode");
                                string CircWeldOvly = item.Field<string>("CIRCULAR WELD OVERLAY");
                                string Remarks = item.Field<string>("REMARKS");

                                string txtLastWeldDate = item.Field<string>("LAST WELD DATE");
                                DateTime? LastWeldDate = null, ValueUpto = null;
                                if (!string.IsNullOrWhiteSpace(txtLastWeldDate) && txtLastWeldDate != "01/01/0001")
                                {
                                    LastWeldDate = DateTime.ParseExact(txtLastWeldDate, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    ValueUpto = LastWeldDate.Value.AddDays(180);
                                }
                                #region Add/Edit WML GROOVE/FILLET

                                var isNew = false;
                                var typeASME = clsImplementationEnum.WMLType.ASME.GetStringValue();
                                var objWML001 = db.WML001.FirstOrDefault(a => a.Location == Location && a.Welder == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == WMLType && a.WQTNo == WQTNo && (a.WMLType == typeASME || a.Project == Project));
                                if (objWML001 == null)
                                {
                                    isNew = true;
                                    objWML001 = new WML001();
                                    objWML001.Location = Location;
                                    objWML001.WMLType = WMLType;
                                    objWML001.Project = Project;
                                    objWML001.WelderStampNo = WelderStampNo;
                                    objWML001.WeldingProcessQualified = WeldingProcessQualified;
                                    objWML001.TypeQualified = TypeQualified;
                                    //objWML001.//JointType = //JointType;
                                    objWML001.PlatePipePositionActual = PlatePipePositionActual;
                                    objWML001.PipeWeldingPositionQualified = PipeWeldingPositionQualified;
                                    objWML001.PlatePipePositionQualified = PlatePipePositionQualified;
                                    objWML001.PNo = PNo;
                                    objWML001.BaseMetalThicknessQualified = BaseMetalThicknessQualified;
                                    objWML001.DepositedThicknessQualified = DepositedThicknessQualified;
                                    objWML001.PlatePipeQualified = PlatePipeQualified;
                                    objWML001.FNo = FNo;
                                    objWML001.BackingQualified = BackingQualified;
                                    objWML001.InertGasBackingQualified = InertGasBackingQualified;
                                    objWML001.CircWeldOvly = CircWeldOvly;
                                }
                                if(isNew || !db.WPS025.Any(a => a.WelderPSNO == Welder && a.JointType == "Groove/Fillet"))
                                {
                                    objWML001.Welder = Welder;
                                    objWML001.Remarks = Remarks;
                                }
                                objWML001.Shop = Shop;
                                objWML001.WQTNo = WQTNo;
                                objWML001.LastWeldDate = LastWeldDate;
                                objWML001.ValueUpto = ValueUpto;

                                if (isNew)
                                {
                                    objWML001.CreatedBy = objClsLoginInfo.UserName;
                                    objWML001.CreatedOn = DateTime.Now;
                                    db.WML001.Add(objWML001);
                                }
                                else
                                {
                                    objWML001.EditedBy = objClsLoginInfo.UserName;
                                    objWML001.EditedOn = DateTime.Now;
                                }

                                //db.SaveChanges();
                                #endregion
                            }
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "WML Certificate added successfully";
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = ex.Message;
                        }
                    }
                    else
                    {
                        objResponseMsg = ErrorWPQExportToExcel(ds);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excel file is not valid";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please enter valid Project no.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponceMsgWithFileName ErrorWPQExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/WQ/WML Groove_Fillet Error Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WelderErrorMsg"))) { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>("WelderErrorMsg") + " )"; excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }
                            excelWorksheet.Cells[i, 2].Value = item.Field<string>(1);
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WelderStampNoErrorMsg"))) { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>("WelderStampNoErrorMsg") + " )"; excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("ShopErrorMsg"))) { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>("ShopErrorMsg") + " )"; excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WeldingProcessQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>("WeldingProcessQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("TypeQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>("TypeQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("PlatePipePositionActualErrorMsg"))) { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>("PlatePipePositionActualErrorMsg") + " )"; excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("PipeWeldingPositionQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7) + " (Note: " + item.Field<string>("PipeWeldingPositionQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("PlatePipePositionQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8) + " (Note: " + item.Field<string>("PlatePipePositionQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 9].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("PNoErrorMsg"))) { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9) + " (Note: " + item.Field<string>("PNoErrorMsg") + " )"; excelWorksheet.Cells[i, 10].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("FNoErrorMsg"))) { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10) + " (Note: " + item.Field<string>("FNoErrorMsg") + " )"; excelWorksheet.Cells[i, 11].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("BaseMetalThicknessQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11) + " (Note: " + item.Field<string>("BaseMetalThicknessQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 12].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("DepositedThicknessQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 13].Value = item.Field<string>(12) + " (Note: " + item.Field<string>("DepositedThicknessQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 13].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 13].Value = item.Field<string>(12); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("PlatePipeQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 14].Value = item.Field<string>(13) + " (Note: " + item.Field<string>("PlatePipeQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 14].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 14].Value = item.Field<string>(13); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("BackingQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 15].Value = item.Field<string>(14) + " (Note: " + item.Field<string>("BackingQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 15].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 15].Value = item.Field<string>(14); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("InertGasBackingQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 16].Value = item.Field<string>(15) + " (Note: " + item.Field<string>("InertGasBackingQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 16].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 16].Value = item.Field<string>(15); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("CircWeldOvlyErrorMsg"))) { excelWorksheet.Cells[i, 17].Value = item.Field<string>(16) + " (Note: " + item.Field<string>("CircWeldOvlyErrorMsg") + " )"; excelWorksheet.Cells[i, 17].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 17].Value = item.Field<string>(16); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("LastWeldDateErrorMsg"))) { excelWorksheet.Cells[i, 18].Value = item.Field<string>(17) + " (Note: " + item.Field<string>("LastWeldDateErrorMsg") + " )"; excelWorksheet.Cells[i, 18].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 18].Value = item.Field<string>(17); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WQTNoErrorMsg"))) { excelWorksheet.Cells[i, 19].Value = item.Field<string>(18) + " (Note: " + item.Field<string>("WQTNoErrorMsg") + " )"; excelWorksheet.Cells[i, 19].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 19].Value = item.Field<string>(18); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("RemarksErrorMsg"))) { excelWorksheet.Cells[i, 20].Value = item.Field<string>(19) + " (Note: " + item.Field<string>("RemarksErrorMsg") + " )"; excelWorksheet.Cells[i, 20].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 20].Value = item.Field<string>(19); }
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath(objClsLoginInfo.UserName, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public DataSet WPQToChechValidation(ExcelPackage package, string Location, string WMLType, string Project, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtHeaderExcel = new DataTable();
            isError = false;
            int notesIndex = 0;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtHeaderExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtHeaderExcel.NewRow();
                    if (excelWorksheet.Cells[rowNumber, 1].Value != null)
                    {
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                    }
                    else
                    {
                        notesIndex = rowNumber + 1;
                        break;
                    }

                    dtHeaderExcel.Rows.Add(newRow);
                }
                #endregion

                #region Validation for Lines


                //dtHeaderExcel.Columns.Add("LocationErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WelderErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WelderStampNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("ShopErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WQTNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WeldingProcessQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("TypeQualifiedErrorMsg", typeof(string));
                //dtHeaderExcel.Columns.Add("JointTypeErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("PlatePipePositionActualErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("PipeWeldingPositionQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("PlatePipePositionQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("PNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("BaseMetalThicknessQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("DepositedThicknessQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("PlatePipeQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("FNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("BackingQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("InertGasBackingQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("CircWeldOvlyErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("LastWeldDateErrorMsg", typeof(string));
                //dtHeaderExcel.Columns.Add("ValueUptoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("RemarksErrorMsg", typeof(string));



                dtHeaderExcel.Columns.Add("WeldingProcessQualifiedCode", typeof(string));
                dtHeaderExcel.Columns.Add("TypeQualifiedCode", typeof(string));
                dtHeaderExcel.Columns.Add("BackingQualifiedCode", typeof(string));
                dtHeaderExcel.Columns.Add("InertGasBackingQualifiedCode", typeof(string));

                var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
                var lstShopCode = db.COM002.Where(i => i.t_dtyp == 3 && i.t_dimx != "").Select(i => i.t_dimx).ToList();
                var lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                var lstTypeQualified = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                var lstBackingQualified = Manager.GetSubCatagorywithoutBULocation("Backing");
                var lstInertGasBackingQualified = Manager.GetSubCatagorywithoutBULocation("Inert gas backing");

                var lstModelWelderDetail = (from a in db.QMS003
                                            where a.Location.Equals(Location, StringComparison.OrdinalIgnoreCase)
                                            select new ModelWelderDetail { WelderPSNo = a.WelderPSNo, WelderName = string.IsNullOrEmpty(a.Name) ? "" : a.Name, ShopCode = a.ShopCode }
                                           )
                                      .Union(from b in db.QMS004
                                             where b.Location.Equals(Location, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany == true
                                             select new ModelWelderDetail { WelderPSNo = b.WelderPSNo, WelderName = string.IsNullOrEmpty(b.Name) ? "" : b.Name, ShopCode = b.ShopCode }).ToList();
                var typeASME = clsImplementationEnum.WMLType.ASME.GetStringValue();

                foreach (DataRow item in dtHeaderExcel.Rows)
                {
                    string Welder = item.Field<string>("PS NO.");
                    item.Field<string>("NAME");
                    string WelderStampNo = item.Field<string>("STAMP");
                    string Shop = item.Field<string>("SHOP");
                    string WQTNo = item.Field<string>("WQT NO");
                    string WeldingProcessQualified = item.Field<string>("PROCESS");
                    string TypeQualified = item.Field<string>("TYPE OF WELDING");
                    string PlatePipePositionActual = item.Field<string>("POSITION GROOVE PLATE & PIPE > = 610");
                    string PipeWeldingPositionQualified = item.Field<string>("POSITION GROOVE PIPE < 610");
                    string PlatePipePositionQualified = item.Field<string>("POSITION OF FILLET");
                    string PNo = item.Field<string>("P NO.");
                    string BaseMetalThicknessQualified = item.Field<string>("BASE MATERIAL THICKNESS IN (MM)");
                    string DepositedThicknessQualified = item.Field<string>("DEPOSITED THICKNESS(MM)");
                    string PlatePipeQualified = item.Field<string>("PIPE OD(mm) MIN");
                    string FNo = item.Field<string>("F NO.");
                    string BackingQualified = item.Field<string>("WITHOUT BACKING / WITH BACKING");
                    string InertGasBackingQualified = item.Field<string>("INERT GAS BACKING");
                    string CircWeldOvly = item.Field<string>("CIRCULAR WELD OVERLAY");
                    string LastWeldDate = item.Field<string>("LAST WELD DATE");
                    string Remarks = item.Field<string>("REMARKS");
                    //bool isWelderError = false;
                    string errorMessage = string.Empty;

                    if (string.IsNullOrEmpty(Welder)) { item["WelderErrorMsg"] = "Welder PS No is required"; isError = true; }
                    else if (Welder.Length > 9) { item["WelderErrorMsg"] = "Welder  PS No maxlength exceeded"; isError = true; }
                    else if (!lstModelWelderDetail.Any(x => x.WelderPSNo.Contains(Welder))) { item["WelderErrorMsg"] = "Welder PS No is Invalid"; isError = true; }

                    if (string.IsNullOrEmpty(WelderStampNo)) { item["WelderStampNoErrorMsg"] = "Welder Stamp No is required"; isError = true; }
                    else if (WelderStampNo.Length > 6) { item["WelderStampNoErrorMsg"] = "Welder Stamp No maxlength exceeded"; isError = true; }

                    if (string.IsNullOrEmpty(Shop)) { item["ShopErrorMsg"] = "Shop is required"; isError = true; }
                    //else if (!isWelderError ? !lstModelWelderDetail.Any(x => x.WelderPSNo.Trim() == Welder.Trim() && (x.ShopCode + "").Trim() == Shop.Trim()) : false)
                    //{ item["ShopErrorMsg"] = "Shop is Invalid for " + Welder; isError = true; }

                    if (string.IsNullOrEmpty(WQTNo)) { item["WQTNoErrorMsg"] = "WQT No is required"; isError = true; }
                    else if (WQTNo.Length > 10) { item["WQTNoErrorMsg"] = "WQT No maxlength exceeded"; isError = true; }
                    //else if (db.WML001.Any(a => a.WQTNo == WQTNo && a.Project != Project && a.WMLType != WMLType) || db.WML002.Any(a => a.WQTNo == WQTNo) || db.WML003.Any(a => a.WQTNo == WQTNo))
                    else if(db.WML001.Any(a =>  a.Location != Location && a.Welder != Welder && a.WelderStampNo != WelderStampNo && a.WMLType != WMLType && a.WQTNo == WQTNo && (a.WMLType == typeASME || a.Project != Project))
                      || db.WML002.Any(a => a.Location == Location && a.WelderName == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == WMLType && a.WQTNo == WQTNo && (a.WMLType == typeASME || a.Project == Project))
                      || db.WML003.Any(a => a.Location == Location && a.WelderName == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == WMLType && a.WQTNo == WQTNo && a.Project == Project)) { item["WQTNoErrorMsg"] = "WQT No. is already being used"; isError = true; }


                    string WeldingProcessQualifiedCode = string.Empty;
                    if (!string.IsNullOrEmpty(WeldingProcessQualified))
                    {
                        if ((WeldingProcessQualifiedCode = lstWeldingProcess.Where(w => w.CategoryDescription.Trim().ToLower() == WeldingProcessQualified.Trim().ToLower() || w.Value.Trim().ToLower() == WeldingProcessQualified.Trim().ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                        {
                            item["WeldingProcessQualifiedErrorMsg"] = "Welding Process Qualified is Invalid"; isError = true;
                            isError = true;
                        }
                        else
                            item["WeldingProcessQualifiedCode"] = WeldingProcessQualified;
                    }

                    string TypeQualifiedCode = string.Empty;
                    if (!string.IsNullOrEmpty(TypeQualified))
                    {
                        if ((TypeQualifiedCode = lstTypeQualified.Where(w => w.CategoryDescription.Trim().ToLower() == TypeQualified.Trim().ToLower() || w.Value.Trim().ToLower() == TypeQualified.Trim().ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                        {
                            item["TypeQualifiedErrorMsg"] = "Type Qualified is Invalid"; isError = true;
                            isError = true;
                        }
                        else
                            item["TypeQualifiedCode"] = TypeQualifiedCode;
                    }

                    if (!string.IsNullOrEmpty(PlatePipePositionActual) && PlatePipePositionActual.Length > 50) { item["PlatePipePositionActualErrorMsg"] = "Pos Groove Plate &amp; Pipe & > = 610 maxlength exceeded"; isError = true; }

                    if (!string.IsNullOrEmpty(PipeWeldingPositionQualified) && PipeWeldingPositionQualified.Length > 50) { item["PipeWeldingPositionQualifiedErrorMsg"] = "Pos Groove Pipe & <  610 maxlength exceeded"; isError = true; }

                    if (!string.IsNullOrEmpty(PlatePipePositionQualified) && PlatePipePositionQualified.Length > 9) { item["PlatePipePositionQualifiedErrorMsg"] = "Position of Fillet maxlength exceeded"; isError = true; }

                    if (!string.IsNullOrEmpty(PNo) && PNo.Length > 50) { item["PNoErrorMsg"] = "P No maxlength exceeded"; isError = true; }

                    if (!string.IsNullOrEmpty(BaseMetalThicknessQualified) && BaseMetalThicknessQualified.Length > 30) { item["BaseMetalThicknessQualifiedErrorMsg"] = "Base metal thickness in mm maxlength exceeded"; isError = true; }

                    if (string.IsNullOrEmpty(DepositedThicknessQualified)) { item["DepositedThicknessQualifiedErrorMsg"] = "Deposit thickness(mm) is required"; isError = true; }
                    else if (DepositedThicknessQualified.Length > 50) { item["DepositedThicknessQualifiedErrorMsg"] = "Deposit thickness(mm) maxlength exceeded"; isError = true; }

                    if (string.IsNullOrEmpty(PlatePipeQualified)) { item["PlatePipeQualifiedErrorMsg"] = "Plate/Pipe Dia is required"; isError = true; }
                    else if (PlatePipeQualified.Length > 70) { item["PlatePipeQualifiedErrorMsg"] = "Plate/Pipe Dia maxlength exceeded"; isError = true; }
                    //else if (!["Plate and pipe >=73", "Plate and Pipe"].Contains(PlatePipeQualified)) { item["PlatePipeQualifiedErrorMsg"] = "PlatePipeQualified is Invalid"; isError = true; }

                    if (!string.IsNullOrEmpty(FNo) && FNo.Length > 50) { item["FNoErrorMsg"] = "F No maxlength exceeded"; isError = true; }

                    string BackingQualifiedCode = string.Empty;
                    if (string.IsNullOrEmpty(BackingQualified)) { item["BackingQualifiedErrorMsg"] = "Backing is required"; isError = true; }
                    else if ((BackingQualifiedCode = lstBackingQualified.Where(w => w.CategoryDescription.Trim().ToLower() == BackingQualified.Trim().ToLower() || w.Value.Trim().ToLower() == BackingQualified.Trim().ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                    {
                        item["BackingQualifiedErrorMsg"] = "Backing is Invalid"; isError = true;
                    }
                    else
                        item["BackingQualifiedCode"] = BackingQualifiedCode;

                    string InertGasBackingQualifiedCode = string.Empty;
                    if (string.IsNullOrEmpty(InertGasBackingQualified)) { item["InertGasBackingQualifiedErrorMsg"] = "Inert Gas Backing is required"; isError = true; }
                    else if ((InertGasBackingQualifiedCode = lstInertGasBackingQualified.Where(w => w.CategoryDescription.Trim().ToLower() == InertGasBackingQualified.Trim().ToLower() || w.Value.Trim().ToLower() == InertGasBackingQualified.Trim().ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                    {
                        item["InertGasBackingQualifiedErrorMsg"] = "Inert Gas Backing is Invalid"; isError = true;
                    }
                    else
                        item["InertGasBackingQualifiedCode"] = InertGasBackingQualifiedCode;

                    if (!string.IsNullOrEmpty(CircWeldOvly) && CircWeldOvly.Length > 10) { item["CircWeldOvlyErrorMsg"] = "Circ Weld Ovly maxlength exceeded"; isError = true; }

                    DateTime tempParse;
                    if (string.IsNullOrEmpty(LastWeldDate)) { item["LastWeldDateErrorMsg"] = "Last Weld Date is required"; isError = true; }
                    else if (!DateTime.TryParseExact(LastWeldDate, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, 0, out tempParse)) { item["LastWeldDateErrorMsg"] = "LastWeldDate invalid date format"; isError = true; }

                    if (!string.IsNullOrEmpty(Remarks) && Remarks.Length > 100) { item["RemarksErrorMsg"] = "Remarks maxlength exceeded"; isError = true; }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                isError = true;
                //throw ex;
                dtHeaderExcel.Rows[0]["RemarksErrorMsg"] = "EXCEPTION:" + ex.Message;
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtHeaderExcel);
            return ds;
        }

        #endregion
        #endregion

        #region WML WOPQ Grid
        [HttpPost]
        public JsonResult LoadWMLWOPQCertificateData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                string[] columnName = { "WQTNo", "WelderName", "WelderStampNo", "Shop", "TypeOfTestCoupon", "WeldingProcessEsQualified", "TypeOfWeldingAutomaticQualified", "ValueUpto", "PNo", "FNo", "PlatePipeEnterDiaIfPipeOrTubeQualified", "DepositedThicknessQualified", "IPlatePipeOverlayWeldingPositionQualified", "IiPipeFilletWeldingPositionQualified", "BaseMetalThicknessQualified", "BackingQualified", "CircWeldOvly", "Remarks" };
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhere += "1=1 and ValueUpto >= getdate() and Location='" + param.MTStatus + "' and WMLType='" + Request["WMLType"] + "'";
                if (!String.IsNullOrWhiteSpace(param.Project))
                    strWhere += " and Project='" + param.Project+ "' ";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                var lstResult = db.SP_WML_WOPQCERTIFICATE_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                NDEModels objNDEModels = new NDEModels();
                ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();
                var WeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                var WeldingType = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                var Backing = Manager.GetSubCatagorywithoutBULocation("Backing");

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.Id),
                                //!string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? Convert.ToString(uc.SpecificationNotes) : Helper.GenerateHTMLTextboxOnChanged(uc.Id, "SpecificationNotes", Convert.ToString(uc.SpecificationNotes), "UpdateCertificate(this,\"Groove\", "+ uc.Id +" );",false,"",string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ?false:true, "200"), //: Convert.ToString(uc.QualityIdDesc)),// Convert.ToString(uc.SpecificationNotes),
                                uc.WelderPsNo,
                                uc.Welder,
                                uc.WelderStampNo,
                                uc.Shop,
                                GetCodeDesc(uc.WeldingProcessEsQualified,WeldingProcess),
                                //objModelWQRequest.GetCategoryDescWithMainCategory("Welding Process",uc.WeldingProcessEsQualified).CategoryDescription,
                                GetCodeDesc(uc.TypeOfWeldingAutomaticQualified,WeldingType),
                                //objModelWQRequest.GetCategoryDescWithMainCategory("Welding Type",uc.TypeOfWeldingAutomaticQualified).CategoryDescription,
                                //objModelWQRequest.GetCategoryDescWithMainCategory("Joint Type WQ",uc.JointType).CategoryDescription,
                                uc.IPlatePipeOverlayWeldingPositionQualified,
                                uc.IiPipeFilletWeldingPositionQualified,
                                uc.PNo,
                                uc.FNo,
                                uc.BaseMetalThicknessQualified,
                                uc.DepositedThicknessQualified,
                                uc.PlatePipeEnterDiaIfPipeOrTubeQualified,
                                GetCodeDesc(uc.BackingQualified,Backing),
                                uc.CircWeldOvly,
                                uc.LastWeldDate.HasValue? uc.LastWeldDate.Value.ToString("dd/MM/yyyy"): "",
                                uc.ValueUpto.HasValue? uc.ValueUpto.Value.ToString("dd/MM/yyyy"): "",
                                uc.WQTNo,
                                uc.Remarks,
                                (param.Flag ? "": Helper.GenerateActionIcon(uc.Id, "Edit", "Edit Record", "fa fa-pencil-square-o")+' '+(db.WPS025.Any(a=>a.WelderPSNO == uc.WelderPsNo && a.JointType == "Overlay")? Helper.GenerateActionIcon(uc.Id, "Delete", "Token is generated", "fa fa-trash-o", "DeleteRecord("+uc.Id+")","",true): Helper.GenerateActionIcon(uc.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteRecord("+uc.Id+")")))
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetWOPQInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();

                NDEModels objNDEModels = new NDEModels();
                ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                Convert.ToString(newRecordId),
                                //clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.SpecificationNotes),
                                Helper.HTMLAutoComplete(newRecordId,"txtWelderName","","",false,"","Welder",false)+ Helper.GenerateHidden(newRecordId,"Welder"),
                                Helper.GenerateLabelFor(newRecordId,"lblWelderName","","","clsWelderName"),
                                Helper.GenerateTextbox(newRecordId,"WelderStampNo","","",false,"","6","",false),
                                Helper.HTMLAutoComplete(newRecordId,"txtShop","","",false,"","Shop",false)+ Helper.GenerateHidden(newRecordId,"Shop"),
                                Helper.HTMLAutoComplete(newRecordId,"txtWeldingProcessEsQualified","","",false,"","WeldingProcessEsQualified",false)+ Helper.GenerateHidden(newRecordId,"WeldingProcessEsQualified"),
                                Helper.HTMLAutoComplete(newRecordId,"txtTypeOfWeldingAutomaticQualified","","",false,"","TypeOfWeldingAutomaticQualified",false)+ Helper.GenerateHidden(newRecordId,"TypeOfWeldingAutomaticQualified"),
                                //Helper.HTMLAutoComplete(newRecordId,"txtJointType","","",false,"","JointType",false)+ Helper.GenerateHidden(newRecordId,"JointType"),
                                Helper.GenerateTextbox(newRecordId,"IPlatePipeOverlayWeldingPositionQualified",maxlength:"50"),
                                Helper.GenerateTextbox(newRecordId,"IiPipeFilletWeldingPositionQualified",maxlength:"50"),
                                Helper.GenerateTextbox(newRecordId,"PNo",maxlength:"50"),
                                Helper.GenerateTextbox(newRecordId,"FNo",maxlength:"50"),
                                Helper.GenerateTextbox(newRecordId,"BaseMetalThicknessQualified",maxlength:"30"),
                                "NA",//Helper.GenerateTextbox(newRecordId,"DepositedThicknessQualified",maxlength:"50"),
                                Helper.GenerateTextbox(newRecordId,"PlatePipeEnterDiaIfPipeOrTubeQualified","Plate and pipe >=73", maxlength:"70"),
                                Helper.HTMLAutoComplete(newRecordId,"txtBackingQualified","","",false,"","BackingQualified",false)+ Helper.GenerateHidden(newRecordId,"BackingQualified"),
                                Helper.GenerateTextbox(newRecordId,"CircWeldOvly",maxlength:"10"),
                                Helper.GenerateTextbox(newRecordId,"LastWeldDate"), //c.LastWeldDate.HasValue? c.LastWeldDate.Value.ToString("dd/MM/yyyy"): "",
                                Helper.GenerateTextbox(newRecordId,"ValueUpto",isReadOnly: true), //c.ValueUpto.HasValue? c.ValueUpto.Value.ToString("dd/MM/yyyy"): "",
                                Helper.GenerateTextbox(newRecordId,"WQTNo",maxlength:"10"),
                                Helper.GenerateTextbox(newRecordId,"Remarks",maxlength:"100"),
                                Helper.GenerateGridButton(newRecordId, "Save", "Add New Record", "fa fa-plus", "EditRecord("+newRecordId+")")
                                };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_WML_WOPQCERTIFICATE_DETAILS(1, 0, "", "Id = " + id).Take(1).ToList();
                    //var lstResult = db.WML001.Where(w=>w.Id == id).ToList();

                    foreach (var c in lstResult)
                    {
                        var isTokenExist = db.WPS025.Any(a => a.WelderPSNO == c.WelderPsNo && a.JointType == "Overlay");
                        data.Add(new string[]{
                            clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                //clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//!string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? Convert.ToString(uc.SpecificationNotes) : Helper.GenerateHTMLTextboxOnChanged(uc.Id, "SpecificationNotes", Convert.ToString(uc.SpecificationNotes), "UpdateCertificate(this,\"Groove\", "+ uc.Id +" );",false,"",string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ?false:true, "200"), //: Convert.ToString(uc.QualityIdDesc)),// Convert.ToString(uc.SpecificationNotes),
                                isReadOnly || isTokenExist? c.WelderPsNo: Helper.HTMLAutoComplete(c.Id,"txtWelderName",c.WelderPsNo,"",false,"","Welder",false)+ Helper.GenerateHidden(c.Id,"Welder",c.WelderPsNo),
                                Helper.GenerateLabelFor(c.Id,"lblWelderName",c.Welder,"","clsWelderName"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.WelderStampNo: Helper.GenerateTextbox(c.Id,"WelderStampNo",c.WelderStampNo,"UpdateData(this, "+ c.Id +");",false,"","100","",false),
                                isReadOnly? c.Shop: Helper.HTMLAutoComplete(c.Id,"txtShop",c.Shop,"",false,"","Shop",false)+ Helper.GenerateHidden(c.Id,"Shop",c.Shop),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? objNDEModels.GetCategory(c.WeldingProcessEsQualified,true).CategoryDescription: Helper.HTMLAutoComplete(c.Id,"txtWeldingProcessEsQualified",objNDEModels.GetCategory(c.WeldingProcessEsQualified,true).CategoryDescription,"",false,"","WeldingProcessEsQualified")+ Helper.GenerateHidden(c.Id,"WeldingProcessEsQualified",c.WeldingProcessEsQualified),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? objNDEModels.GetCategory(c.TypeOfWeldingAutomaticQualified,true).CategoryDescription: Helper.HTMLAutoComplete(c.Id,"txtTypeOfWeldingAutomaticQualified",objNDEModels.GetCategory(c.TypeOfWeldingAutomaticQualified,true).CategoryDescription,"",false,"","TypeOfWeldingAutomaticQualified")+ Helper.GenerateHidden(c.Id,"TypeOfWeldingAutomaticQualified",c.TypeOfWeldingAutomaticQualified),
                                //clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),////Helper.HTMLAutoComplete(c.Id,"txtJointType","","",false,"","JointType",false)+ Helper.GenerateHidden(newRecordId,"JointType"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.IPlatePipeOverlayWeldingPositionQualified:Helper.GenerateTextbox(c.Id,"IPlatePipeOverlayWeldingPositionQualified",c.IPlatePipeOverlayWeldingPositionQualified, maxlength: "50"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.IiPipeFilletWeldingPositionQualified:Helper.GenerateTextbox(c.Id,"IiPipeFilletWeldingPositionQualified",c.IiPipeFilletWeldingPositionQualified,maxlength: "50"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.PNo:Helper.GenerateTextbox(c.Id,"PNo",c.PNo,maxlength: "50"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.FNo:Helper.GenerateTextbox(c.Id,"FNo",c.FNo,maxlength: "50"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//"NA",//isReadOnly? c.BaseMetalThicknessQualified:Helper.GenerateTextbox(c.Id,"BaseMetalThicknessQualified",c.BaseMetalThicknessQualified,maxlength: "30"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//"NA",//isReadOnly? c.DepositedThicknessQualified:Helper.GenerateTextbox(c.Id,"DepositedThicknessQualified",c.DepositedThicknessQualified,maxlength: "50"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.PlatePipeEnterDiaIfPipeOrTubeQualified:Helper.GenerateTextbox(c.Id,"PlatePipeEnterDiaIfPipeOrTubeQualified",c.PlatePipeEnterDiaIfPipeOrTubeQualified, maxlength:"70", onchange:"setPlatePipeEnterDiaIfPipeOrTubeQualified(this)"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? objNDEModels.GetCategory(c.BackingQualified,true).CategoryDescription: Helper.HTMLAutoComplete(c.Id,"txtBackingQualified",objNDEModels.GetCategory(c.BackingQualified,true).CategoryDescription,"",false,"","BackingQualified")+ Helper.GenerateHidden(c.Id,"BackingQualified",c.BackingQualified),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.CircWeldOvly: Helper.GenerateTextbox(c.Id,"CircWeldOvly",c.CircWeldOvly,maxlength:"10"),
                                isReadOnly? (c.LastWeldDate.HasValue? c.LastWeldDate.Value.ToString("dd/MM/yyyy"): ""): Helper.GenerateTextbox(c.Id,"LastWeldDate",c.LastWeldDate.HasValue? c.LastWeldDate.Value.ToString("yyyy-MM-dd"): ""),
                                isReadOnly? (c.ValueUpto.HasValue? c.ValueUpto.Value.ToString("dd/MM/yyyy"): "") :Helper.GenerateTextbox(c.Id,"ValueUpto",c.ValueUpto.HasValue? c.ValueUpto.Value.ToString("yyyy-MM-dd"): "",isReadOnly: true),
                                isReadOnly? c.WQTNo: Helper.GenerateTextbox(c.Id,"WQTNo",c.WQTNo,maxlength:"10"),
                                isReadOnly || isTokenExist? c.Remarks: Helper.GenerateTextbox(c.Id,"Remarks",c.Remarks,maxlength:"100"),
                                isReadOnly? Helper.GenerateActionIcon(c.Id, "Edit", "Edit Record", "fa fa-pencil-square-o")+' '+(isTokenExist? Helper.GenerateActionIcon(c.Id, "Delete", "Token is generated", "fa fa-trash-o", "DeleteRecord("+c.Id+")","",true): Helper.GenerateActionIcon(c.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteRecord("+c.Id+")")) : Helper.GenerateActionIcon(c.Id, "Save", "Save Record", "fa fa-save", "EditRecord("+c.Id+")") + " " + Helper.GenerateActionIcon(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine();")
                        });
                    }
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveWMLWOPQCertificate(FormCollection fc, int Id)
        {
            var objResponseMsg = new clsHelper.ResponseMsg();
            var objWML002 = new WML002();
            try
            {
                if (Id > 0)
                {
                    objWML002 = db.WML002.FirstOrDefault(w => w.Id == Id);
                    if (!db.WPS025.Any(a => a.WelderPSNO == objWML002.WelderName && a.JointType == "Overlay"))
                    {
                        objWML002.WelderName = (fc["Welder" + Id] + "").Split('-')[0];
                        objWML002.Remarks = fc["Remarks" + Id];
                    }
                    ////objWML001.Location = fc["Location" + Id];
                    //objWML002.SpecificationNotes = fc["SpecificationNotes" + Id];
                    //objWML002.WPSNo = fc["WPSNo" + Id];
                    //objWML002.WelderStampNo = fc["WelderStampNo" + Id];
                    //objWML002.TypeOfTestCoupon = fc["TypeOfTestCoupon" + Id];
                    //objWML002.WeldingProcessEsQualified = fc["WeldingProcessEsQualified" + Id];
                    //objWML002.TypeOfWeldingAutomaticQualified = fc["TypeOfWeldingAutomaticQualified" + Id];
                    //objWML002.PNo = fc["PNo" + Id];
                    //objWML002.FNo = fc["FNo" + Id];
                    //objWML002.PlatePipeEnterDiaIfPipeOrTubeQualified = fc["PlatePipeEnterDiaIfPipeOrTubeQualified" + Id];
                    ////objWML002.DepositedThicknessQualified = fc["DepositedThicknessQualified" + Id];
                    //objWML002.IPlatePipeOverlayWeldingPositionQualified = fc["IPlatePipeOverlayWeldingPositionQualified" + Id];
                    //objWML002.IiPipeFilletWeldingPositionQualified = fc["IiPipeFilletWeldingPositionQualified" + Id];
                    //objWML002.BaseMetalThicknessQualified = fc["BaseMetalThicknessQualified" + Id];
                    //objWML002.BackingQualified = fc["BackingQualified" + Id];
                    //objWML002.CircWeldOvly = fc["CircWeldOvly" + Id];
                    objWML002.WQTNo = fc["WQTNo" + Id];
                    objWML002.Shop = (fc["Shop" + Id] + "").Split('-')[0];
                    objWML002.ValueUpto = Convert.ToDateTime(fc["ValueUpto" + Id]);
                    objWML002.LastWeldDate = Convert.ToDateTime(fc["LastWeldDate" + Id]);
                    //objWML002.JointType = fc["JointType" + Id];
                    objWML002.EditedBy = objClsLoginInfo.UserName;
                    objWML002.EditedOn = DateTime.Now;
                    //objWML001.MaintainedBy = fc["MaintainedBy" + Id];
                    //objWML001.ModifiedBy = fc["ModifiedBy" + Id];
                    //objWML001.MaintainedOn = Convert.ToDateTime(fc["MaintainedOn" + Id]);
                    //objWML001.ModifiedOn = Convert.ToDateTime(fc["ModifiedOn" + Id]);

                    db.SaveChanges();
                    objResponseMsg.Value = "Record updated successfully";
                    objResponseMsg.Key = true;
                }
                else
                {
                    objWML002.WQTNo = fc["WQTNo" + Id];
                    objWML002.WelderName = fc["Welder" + Id];
                    objWML002.WelderStampNo = fc["WelderStampNo" + Id];
                    objWML002.Project = fc["Project" + Id];
                    objWML002.WMLType = fc["WMLType" + Id];
                    objWML002.Location = fc["Location" + Id];

                    var typeASME = clsImplementationEnum.WMLType.ASME.GetStringValue();
                    if (db.WML001.Any(a => a.Location == objWML002.Location && a.Welder == objWML002.WelderName && a.WelderStampNo == objWML002.WelderStampNo && a.WMLType == objWML002.WMLType && a.WQTNo == objWML002.WQTNo && (a.WMLType == typeASME || a.Project == objWML002.Project))
                       || db.WML002.Any(a => a.Location == objWML002.Location && a.WelderName == objWML002.WelderName && a.WelderStampNo == objWML002.WelderStampNo && a.WMLType == objWML002.WMLType && a.WQTNo == objWML002.WQTNo && (a.WMLType == typeASME || a.Project == objWML002.Project))
                       || db.WML003.Any(a => a.Location == objWML002.Location && a.WelderName == objWML002.WelderName && a.WelderStampNo == objWML002.WelderStampNo && a.WMLType == objWML002.WMLType && a.WQTNo == objWML002.WQTNo && a.Project == objWML002.Project))
                    {
                        objResponseMsg.Value = "WQT No. is already being used";
                        objResponseMsg.Key = false;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objWML002.SpecificationNotes = fc["SpecificationNotes" + Id];
                    objWML002.WPSNo = fc["WPSNo" + Id];
                    objWML002.Shop = fc["Shop" + Id];
                    objWML002.TypeOfTestCoupon = fc["TypeOfTestCoupon" + Id];
                    objWML002.WeldingProcessMachineQualified = objWML002.WeldingProcessEsQualified = fc["WeldingProcessEsQualified" + Id];
                    objWML002.TypeOfWeldingAutomaticQualified = fc["TypeOfWeldingAutomaticQualified" + Id];
                    objWML002.ValueUpto = Convert.ToDateTime(fc["ValueUpto" + Id]);
                    objWML002.PNo = fc["PNo" + Id];
                    objWML002.FNo = fc["FNo" + Id];
                    objWML002.PlatePipeEnterDiaIfPipeOrTubeQualified = fc["PlatePipeEnterDiaIfPipeOrTubeQualified" + Id];
                    //objWML002.DepositedThicknessQualified = fc["DepositedThicknessQualified" + Id];
                    objWML002.IPlatePipeOverlayWeldingPositionQualified = fc["IPlatePipeOverlayWeldingPositionQualified" + Id];
                    objWML002.IiPipeFilletWeldingPositionQualified = fc["IiPipeFilletWeldingPositionQualified" + Id];
                    objWML002.BaseMetalThicknessQualified = fc["BaseMetalThicknessQualified" + Id];
                    objWML002.BackingQualified = fc["BackingQualified" + Id];
                    objWML002.CircWeldOvly = fc["CircWeldOvly" + Id];
                    objWML002.Remarks = fc["Remarks" + Id];
                    objWML002.LastWeldDate = Convert.ToDateTime(fc["LastWeldDate" + Id]);
                    objWML002.JointType = fc["JointType" + Id];
                    objWML002.CreatedBy = objClsLoginInfo.UserName;
                    objWML002.CreatedOn = DateTime.Now;

                    db.WML002.Add(objWML002);
                    db.SaveChanges();
                    objResponseMsg.Value = "Record added successfully";
                    objResponseMsg.Key = true;

                    //#region Add New Spot Line1
                    //objCONFIG.Value = strValue;
                    //objCONFIG.CreatedBy = objClsLoginInfo.UserName;
                    //objCONFIG.CreatedOn = DateTime.Now;
                    //db.CONFIG.Add(objTCPobjCONFIG002);
                    //objResponseMsg.Value = "Value added successfully";
                    //objResponseMsg.Key = true;
                    //#endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteWMLWOPQCertificate(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            var objWML002 = db.WML002.FirstOrDefault(x => x.Id == Id);
            try
            {
                if (objWML002 != null)
                {
                    if (db.WPS025.Any(a => a.WelderPSNO == objWML002.WelderName && a.JointType == "Overlay"))
                        objResponseMsg.Value = "Welder is being used in token!";
                    else
                    {
                        db.WML002.Remove(objWML002);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Record deleted successfully";
                    }
                }
                else
                    objResponseMsg.Value = "Record already deleted";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        #region Excel
        [SessionExpireFilter]
        public ActionResult ImportExcelWMLWOPQ(HttpPostedFileBase upload, string Location, string WMLType, string Project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (upload != null)
            {
                if (Path.GetExtension(upload.FileName).ToLower() == ".xlsx" || Path.GetExtension(upload.FileName).ToLower() == ".xls")
                {
                    ExcelPackage package = new ExcelPackage(upload.InputStream);
                    ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();

                    DataTable dtHeaderExcel = new DataTable();
                    //List<CategoryData> errors = new List<CategoryData>();
                    bool isError;

                    DataSet ds = WOPQToChechValidation(package, Location, WMLType, Project, out isError);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Record Found!";
                    }
                    else if (!isError)
                    {
                        try
                        {
                            foreach (DataRow item in ds.Tables[0].Rows)
                            {
                                string Welder = item.Field<string>("PS NO.");
                                item.Field<string>("NAME");
                                string WelderStampNo = item.Field<string>("STAMP");
                                string Shop = item.Field<string>("SHOP");
                                string WQTNo = item.Field<string>("WQT NO");
                                string WeldingProcessQualified = item.Field<string>("WeldingProcessQualifiedCode");
                                string TypeQualified = item.Field<string>("TypeQualifiedCode");
                                string IPlatePipeOverlayWeldingPositionQualified = item.Field<string>("POSITION OVERLAY PLATE & PIPE > = 610");
                                string IiPipeFilletWeldingPositionQualified = item.Field<string>("POSITION OVERLAY PIPE < 610");
                                string PNo = item.Field<string>("P NO.");
                                string BaseMetalThicknessQualified = item.Field<string>("BASE MATERIAL THICKNESS IN (MM)");
                                string DepositedThicknessQualified = item.Field<string>("DEPOSITED THICKNESS(MM)");
                                string PlatePipeEnterDiaIfPipeOrTubeQualified = item.Field<string>("PIPE OD(mm) MIN");
                                string FNo = item.Field<string>("F NO.");
                                string BackingQualified = item.Field<string>("BackingQualifiedCode");
                                string CircWeldOvly = item.Field<string>("CIRCULAR WELD OVERLAY");
                                string Remarks = item.Field<string>("REMARKS");

                                string txtLastWeldDate = item.Field<string>("LAST WELD DATE");
                                DateTime? LastWeldDate = null, ValueUpto = null;
                                if (!string.IsNullOrWhiteSpace(txtLastWeldDate) && txtLastWeldDate != "01/01/0001")
                                {
                                    LastWeldDate = DateTime.ParseExact(txtLastWeldDate, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    ValueUpto = LastWeldDate.Value.AddDays(180);
                                }
                                #region Add New MIP Master Operations
                                var isNew = false;
                                var typeASME = clsImplementationEnum.WMLType.ASME.GetStringValue();
                                var objWML002 = db.WML002.FirstOrDefault(a => a.Location == Location && a.WelderName == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == WMLType && a.WQTNo == WQTNo && (a.WMLType == typeASME || a.Project == Project));
                                if(objWML002 == null)
                                {
                                    isNew = true;
                                    objWML002 = new WML002();
                                    objWML002.Location = Location;
                                    objWML002.WMLType = WMLType;
                                    objWML002.Project = Project;
                                    objWML002.WelderStampNo = WelderStampNo;
                                    objWML002.WeldingProcessMachineQualified = objWML002.WeldingProcessEsQualified = WeldingProcessQualified;
                                    objWML002.TypeOfWeldingAutomaticQualified = TypeQualified;
                                    objWML002.IPlatePipeOverlayWeldingPositionQualified = IPlatePipeOverlayWeldingPositionQualified;
                                    objWML002.IiPipeFilletWeldingPositionQualified = IiPipeFilletWeldingPositionQualified;
                                    objWML002.PNo = PNo;
                                    objWML002.BaseMetalThicknessQualified = BaseMetalThicknessQualified;
                                    objWML002.PlatePipeEnterDiaIfPipeOrTubeQualified = PlatePipeEnterDiaIfPipeOrTubeQualified;
                                    objWML002.FNo = FNo;
                                    objWML002.BackingQualified = BackingQualified;
                                    objWML002.CircWeldOvly = CircWeldOvly;
                                }
                                if (isNew || !db.WPS025.Any(a => a.WelderPSNO == objWML002.WelderName && a.JointType == "Overlay"))
                                {
                                    objWML002.WelderName = Welder;
                                    objWML002.Remarks = Remarks;
                                }
                                objWML002.Shop = Shop;
                                objWML002.WQTNo = WQTNo;
                                objWML002.LastWeldDate = LastWeldDate;
                                objWML002.ValueUpto = ValueUpto;

                                if (isNew)
                                {
                                    objWML002.CreatedBy = objClsLoginInfo.UserName;
                                    objWML002.CreatedOn = DateTime.Now;
                                    db.WML002.Add(objWML002);
                                }
                                else
                                {
                                    objWML002.EditedBy = objClsLoginInfo.UserName;
                                    objWML002.EditedOn = DateTime.Now;
                                }

                                //db.SaveChanges();
                                #endregion
                            }
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "WML Certificate added successfully";
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = ex.Message;
                        }
                    }
                    else
                    {
                        objResponseMsg = ErrorWOPQExportToExcel(ds);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excel file is not valid";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please enter valid Project no.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponceMsgWithFileName ErrorWOPQExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/WQ/WML Overlay Error Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WelderErrorMsg"))) { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>("WelderErrorMsg") + " )"; excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }
                            excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); /*+ " (Note: " + item.Field<string>("WelderErrorMsg") + " )"; excelWorksheet.Cells[i, 2].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 2].Value = item.Field<string>(1); }*/
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WelderStampNoErrorMsg"))) { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>("WelderStampNoErrorMsg") + " )"; excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("ShopErrorMsg"))) { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>("ShopErrorMsg") + " )"; excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WeldingProcessEsQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " +   item.Field<string>("WeldingProcessEsQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("TypeOfWeldingAutomaticQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " +   item.Field<string>("TypeOfWeldingAutomaticQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("IPlatePipeOverlayWeldingPositionQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " +   item.Field<string>("IPlatePipeOverlayWeldingPositionQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("IiPipeFilletWeldingPositionQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7) + " (Note: " +   item.Field<string>("IiPipeFilletWeldingPositionQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("PNoErrorMsg"))) { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8) + " (Note: " +   item.Field<string>("PNoErrorMsg") + " )"; excelWorksheet.Cells[i, 9].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("FNoErrorMsg"))) { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9) + " (Note: " +  item.Field<string>("FNoErrorMsg") + " )"; excelWorksheet.Cells[i, 10].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("BaseMetalThicknessQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10) + " (Note: " + item.Field<string>("BaseMetalThicknessQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 11].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 11].Value = item.Field<string>(10); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("DepositedThicknessQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11) + " (Note: " + item.Field<string>("DepositedThicknessQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 12].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 12].Value = item.Field<string>(11); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("PlatePipeEnterDiaIfPipeOrTubeQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 13].Value = item.Field<string>(12) + " (Note: " + item.Field<string>("PlatePipeEnterDiaIfPipeOrTubeQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 13].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 13].Value = item.Field<string>(12); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("BackingQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 14].Value = item.Field<string>(13) + " (Note: " + item.Field<string>("BackingQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 14].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 14].Value = item.Field<string>(13); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("CircWeldOvlyErrorMsg"))) { excelWorksheet.Cells[i, 15].Value = item.Field<string>(14) + " (Note: " + item.Field<string>("CircWeldOvlyErrorMsg") + " )"; excelWorksheet.Cells[i, 15].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 15].Value = item.Field<string>(14); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("LastWeldDateErrorMsg"))) { excelWorksheet.Cells[i, 16].Value = item.Field<string>(15) + " (Note: " + item.Field<string>("LastWeldDateErrorMsg") + " )"; excelWorksheet.Cells[i, 16].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 16].Value = item.Field<string>(15); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WQTNoErrorMsg"))) { excelWorksheet.Cells[i, 17].Value = item.Field<string>(16) + " (Note: " + item.Field<string>("WQTNoErrorMsg") + " )"; excelWorksheet.Cells[i, 17].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 17].Value = item.Field<string>(16); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("RemarksErrorMsg"))) { excelWorksheet.Cells[i, 18].Value = item.Field<string>(17) + " (Note: " + item.Field<string>("RemarksErrorMsg") + " )"; excelWorksheet.Cells[i, 18].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 18].Value = item.Field<string>(17); }
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath(objClsLoginInfo.UserName, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public DataSet WOPQToChechValidation(ExcelPackage package, string Location, string WMLType, string Project, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtHeaderExcel = new DataTable();
            isError = false;
            int notesIndex = 0;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtHeaderExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtHeaderExcel.NewRow();
                    if (excelWorksheet.Cells[rowNumber, 1].Value != null)
                    {
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                    }
                    else
                    {
                        notesIndex = rowNumber + 1;
                        break;
                    }

                    dtHeaderExcel.Rows.Add(newRow);
                }
                #endregion 

                #region Validation for Lines
                dtHeaderExcel.Columns.Add("LocationErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WelderErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WelderStampNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("ShopErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WQTNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WeldingProcessEsQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("TypeOfWeldingAutomaticQualifiedErrorMsg", typeof(string));
                //dtHeaderExcel.Columns.Add("JointTypeErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("IPlatePipeOverlayWeldingPositionQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("IiPipeFilletWeldingPositionQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("PNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("BaseMetalThicknessQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("DepositedThicknessQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("PlatePipeEnterDiaIfPipeOrTubeQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("FNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("BackingQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("CircWeldOvlyErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("LastWeldDateErrorMsg", typeof(string));
                //dtHeaderExcel.Columns.Add("ValueUptoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("RemarksErrorMsg", typeof(string));

                dtHeaderExcel.Columns.Add("WeldingProcessQualifiedCode", typeof(string));
                dtHeaderExcel.Columns.Add("TypeQualifiedCode", typeof(string));
                dtHeaderExcel.Columns.Add("BackingQualifiedCode", typeof(string));

                var lstLoc = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
                var lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                var lstTypeOfWeldingAutomaticQualified = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                var lstBackingQualified = Manager.GetSubCatagorywithoutBULocation("Backing");
                var lstModelWelderDetail = (from a in db.QMS003
                                            where a.Location.Equals(Location, StringComparison.OrdinalIgnoreCase)
                                            select new ModelWelderDetail { WelderPSNo = a.WelderPSNo, WelderName = string.IsNullOrEmpty(a.Name) ? "" : a.Name, ShopCode = a.ShopCode }
                           )
                      .Union(from b in db.QMS004
                             where b.Location.Equals(Location, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany == true
                             select new ModelWelderDetail { WelderPSNo = b.WelderPSNo, WelderName = string.IsNullOrEmpty(b.Name) ? "" : b.Name, ShopCode = b.ShopCode }).ToList();
                var typeASME = clsImplementationEnum.WMLType.ASME.GetStringValue();

                foreach (DataRow item in dtHeaderExcel.Rows)
                {
                    string Welder = item.Field<string>("PS NO.");
                    item.Field<string>("NAME");
                    string WelderStampNo = item.Field<string>("STAMP");
                    string Shop = item.Field<string>("SHOP");
                    string WQTNo = item.Field<string>("WQT NO");
                    string WeldingProcessQualified = item.Field<string>("PROCESS");
                    string TypeQualified = item.Field<string>("TYPE OF WELDING");
                    string IPlatePipeOverlayWeldingPositionQualified = item.Field<string>("POSITION OVERLAY PLATE & PIPE > = 610");
                    string IiPipeFilletWeldingPositionQualified = item.Field<string>("POSITION OVERLAY PIPE < 610");
                    string PNo = item.Field<string>("P NO.");
                    string BaseMetalThicknessQualified = item.Field<string>("BASE MATERIAL THICKNESS IN (MM)");
                    string DepositedThicknessQualified = item.Field<string>("DEPOSITED THICKNESS(MM)");
                    string PlatePipeEnterDiaIfPipeOrTubeQualified = item.Field<string>("PIPE OD(mm) MIN");
                    string FNo = item.Field<string>("F NO.");
                    string BackingQualified = item.Field<string>("WITHOUT BACKING / WITH BACKING");
                    string CircWeldOvly = item.Field<string>("CIRCULAR WELD OVERLAY");
                    string Remarks = item.Field<string>("REMARKS");
                    string LastWeldDate = item.Field<string>("LAST WELD DATE");
                    //  bool isWelderError = false;
                    string errorMessage = string.Empty;

                    if (string.IsNullOrEmpty(Welder)) { item["WelderErrorMsg"] = "Welder Name is required"; isError = true; }
                    else if (Welder.Length > 9) { item["WelderErrorMsg"] = "Welder Name maxlength exceeded"; isError = true; }
                    else if (!lstModelWelderDetail.Any(x => x.WelderPSNo.Trim() == Welder.Trim())) { item["WelderErrorMsg"] = "Welder PSNo is Invalid"; isError = true; }

                    if (string.IsNullOrEmpty(WelderStampNo)) { item["WelderStampNoErrorMsg"] = "Welder Stamp No is required"; isError = true; }
                    else if (WelderStampNo.Length > 6) { item["WelderStampNoErrorMsg"] = "Welder Stamp No maxlength exceeded"; isError = true; }

                    if (string.IsNullOrEmpty(Shop)) { item["ShopErrorMsg"] = "Shop is required"; isError = true; }
                    //else if (!isWelderError ? !lstModelWelderDetail.Any(x => x.WelderPSNo == Welder && x.ShopCode == Shop) : false)
                    //{ item["ShopErrorMsg"] = "Shop is Invalid for " + Welder; isError = true; }

                    if (string.IsNullOrEmpty(WQTNo)) { item["WQTNoErrorMsg"] = "WQT No is required"; isError = true; }
                    else if (WQTNo.Length > 10) { item["WQTNoErrorMsg"] = "WQT No maxlength exceeded"; isError = true; }
                    //else if (db.WML001.Any(a => a.WQTNo == WQTNo) || db.WML002.Any(a => a.WQTNo == WQTNo && a.Project != Project && a.WMLType != WMLType) || db.WML003.Any(a => a.WQTNo == WQTNo)) { item["WQTNoErrorMsg"] = "WQT No. is already being used"; isError = true; }
                    else if (db.WML002.Any(a => a.Location != Location && a.WelderName != Welder && a.WelderStampNo != WelderStampNo && a.WMLType != WMLType && a.WQTNo == WQTNo && (a.WMLType == typeASME || a.Project != Project))
                     || db.WML001.Any(a => a.Location == Location && a.Welder == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == WMLType && a.WQTNo == WQTNo && (a.WMLType == typeASME || a.Project == Project))
                     || db.WML003.Any(a => a.Location == Location && a.WelderName == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == WMLType && a.WQTNo == WQTNo && a.Project == Project)) { item["WQTNoErrorMsg"] = "WQT No. is already being used"; isError = true; }
                    
                    string WeldingProcessEsQualifiedCode = string.Empty;
                    if (!string.IsNullOrEmpty(WeldingProcessQualified))
                    {
                        if ((WeldingProcessEsQualifiedCode = lstWeldingProcess.Where(w => w.CategoryDescription == WeldingProcessQualified || w.Value == WeldingProcessQualified).Select(s => s.Value).FirstOrDefault()) == null)
                        {
                            item["WeldingProcessEsQualifiedErrorMsg"] = "Welding Process Qualified is Invalid"; isError = true;
                            isError = true;
                        }
                        else
                            item["WeldingProcessQualifiedCode"] = WeldingProcessEsQualifiedCode;
                    }
                    string TypeOfWeldingAutomaticQualifiedCode = string.Empty;
                    if (!string.IsNullOrEmpty(TypeQualified))
                    {
                        if ((TypeOfWeldingAutomaticQualifiedCode = lstTypeOfWeldingAutomaticQualified.Where(w => w.CategoryDescription.ToLower() == TypeQualified.ToLower() || w.Value.ToLower() == TypeQualified.ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                        {
                            item["TypeOfWeldingAutomaticQualifiedErrorMsg"] = "Type Qualified is Invalid"; isError = true;
                            isError = true;
                        }
                        else
                            item["TypeQualifiedCode"] = TypeOfWeldingAutomaticQualifiedCode;
                    }

                    if (!string.IsNullOrEmpty(IPlatePipeOverlayWeldingPositionQualified) && IPlatePipeOverlayWeldingPositionQualified.Length > 50) { item["IPlatePipeOverlayWeldingPositionQualifiedErrorMsg"] = "Pos Groove Plate &amp; Pipe & > = 610 maxlength exceeded"; isError = true; }

                    if (!string.IsNullOrEmpty(IiPipeFilletWeldingPositionQualified) && IiPipeFilletWeldingPositionQualified.Length > 50) { item["IiPipeFilletWeldingPositionQualifiedErrorMsg"] = "Pos Groove Pipe & <  610 maxlength exceeded"; isError = true; }

                    if (!string.IsNullOrEmpty(PNo) && PNo.Length > 50) { item["PNoErrorMsg"] = "P No maxlength exceeded"; isError = true; }

                    if (string.IsNullOrEmpty(PlatePipeEnterDiaIfPipeOrTubeQualified)) { item["PlatePipeEnterDiaIfPipeOrTubeQualifiedErrorMsg"] = "Plate/Pipe Dia is required"; isError = true; }
                    else if (PlatePipeEnterDiaIfPipeOrTubeQualified.Length > 70) { item["PlatePipeEnterDiaIfPipeOrTubeQualifiedErrorMsg"] = "Plate/Pipe Dia maxlength exceeded"; isError = true; }

                    if (!string.IsNullOrEmpty(FNo) && FNo.Length > 50) { item["FNoErrorMsg"] = "F No maxlength exceeded"; isError = true; }

                    if (!string.IsNullOrEmpty(BaseMetalThicknessQualified) && BaseMetalThicknessQualified.Length > 30) { item["BaseMetalThicknessQualifiedErrorMsg"] = "Base metal thickness in mm maxlength exceeded"; isError = true; }

                    string BackingQualifiedCode = string.Empty;
                    if (string.IsNullOrEmpty(BackingQualified)) { item["BackingQualifiedErrorMsg"] = "Backing is required"; isError = true; }
                    else if ((BackingQualifiedCode = lstBackingQualified.Where(w => w.CategoryDescription.ToLower() == BackingQualified.ToLower() || w.Value.ToLower() == BackingQualified.ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                    {
                        item["BackingQualifiedErrorMsg"] = "Backing is Invalid"; isError = true;
                    }
                    else
                        item["BackingQualifiedCode"] = BackingQualifiedCode;

                    if (!string.IsNullOrEmpty(CircWeldOvly) && CircWeldOvly.Length > 10) { item["CircWeldOvlyErrorMsg"] = "Circ Weld Ovly maxlength exceeded"; isError = true; }

                    DateTime tempParse;
                    if (string.IsNullOrEmpty(LastWeldDate)) { item["LastWeldDateErrorMsg"] = "LastWeldDate is required"; isError = true; }
                    else if (!DateTime.TryParseExact(LastWeldDate, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, 0, out tempParse)) { item["LastWeldDateErrorMsg"] = "LastWeldDate invalid date format"; isError = true; }

                    if (!string.IsNullOrEmpty(Remarks) && Remarks.Length > 100) { item["RemarksErrorMsg"] = "Remarks maxlength exceeded"; isError = true; }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                isError = true;
                dtHeaderExcel.Rows[0]["RemarksErrorMsg"] = "EXCEPTION:" + ex.Message;
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtHeaderExcel);
            return ds;
        }

        #endregion
        #endregion

        #region WML TSS Grid
        [HttpPost]
        public JsonResult LoadWMLTSSCertificateData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                string[] columnName = { "WQTNo", "WelderName", "WelderStampNo", "Shop", "TypeOfTestCoupon", "WeldingProcessEsQualified", "TypeOfWeldingAutomaticQualified", "ValueUpto", "PNo", "FNo", "PlatePipeEnterDiaIfPipeOrTubeQualified", "DepositedThicknessQualified", "IPlatePipeOverlayWeldingPositionQualified", "IiPipeFilletWeldingPositionQualified", "IPlatePipeFilletWeldingPositionQualified", "BaseMetalThicknessQualified", "BackingQualified", "InertGasBackingQualified", "CircWeldOvly", "Remarks" };
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhere += "1=1 and ValueUpto >= getdate() and Location='" + param.MTStatus + "' and WMLType='"+ Request["WMLType"] + "'";
                if(!String.IsNullOrWhiteSpace(param.Project))
                    strWhere += " and Project='" + param.Project + "' ";

                if (!string.IsNullOrWhiteSpace(param.Project))
                {
                    strWhere += " and project='" + param.Project + "'";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                else
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                var lstResult = db.SP_WML_TSSCERTIFICATE_DETAILS(StartIndex, EndIndex, strSortOrder, strWhere).ToList();

                NDEModels objNDEModels = new NDEModels();
                ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();
                var WeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                var WeldingType = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                var Backing = Manager.GetSubCatagorywithoutBULocation("Backing");
                var InertGasBacking = Manager.GetSubCatagorywithoutBULocation("Inert gas backing");
                var JointTypeWQ = Manager.GetSubCatagorywithoutBULocation("Joint Type WQ");

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.Id),
                                //!string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? Convert.ToString(uc.SpecificationNotes) : Helper.GenerateHTMLTextboxOnChanged(uc.Id, "SpecificationNotes", Convert.ToString(uc.SpecificationNotes), "UpdateCertificate(this,\"Groove\", "+ uc.Id +" );",false,"",string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ?false:true, "200"), //: Convert.ToString(uc.QualityIdDesc)),// Convert.ToString(uc.SpecificationNotes),
                                uc.WelderPsNo,
                                uc.WelderName,
                                uc.WelderStampNo,
                                uc.Shop,
                                uc.WQTNo,
                                GetCodeDesc(uc.WeldingProcessEsQualified,WeldingProcess),
                                //objModelWQRequest.GetCategoryDescWithMainCategory("Welding Process",uc.WeldingProcessEsQualified).CategoryDescription,
                                GetCodeDesc(uc.WeldingTypeQualified,WeldingType),
                                //objModelWQRequest.GetCategoryDescWithMainCategory("Welding Type",uc.TypeOfWeldingAutomaticQualified).CategoryDescription,
                                GetCodeDesc(uc.JoinType,JointTypeWQ),
                                //uc.IPlatePipeOverlayWeldingPositionQualified,
                                //uc.IiPipeFilletWeldingPositionQualified,
                                //uc.IPlatePipeFilletWeldingPositionQualified,
                                //uc.PNo,
                                //uc.BaseMetalThicknessQualified,
                                //uc.DepositedThicknessQualified,
                                //uc.PlatePipeEnterDiaIfPipeOrTubeQualified,
                                //uc.FNo,
                                //GetCodeDesc(uc.BackingQualified,Backing),
                                //GetCodeDesc(uc.InertGasBackingQualified,InertGasBacking),
                                //uc.CircWeldOvly,
                                uc.LastWeldDate.HasValue? uc.LastWeldDate.Value.ToString("dd/MM/yyyy"): "",
                                uc.ValueUpto.HasValue? uc.ValueUpto.Value.ToString("dd/MM/yyyy"): "",
                                uc.Remarks,
                                (param.Flag ? "": Helper.GenerateActionIcon(uc.Id, "Edit", "Edit Record", "fa fa-pencil-square-o")+' '+(db.WPS025.Any(a=>a.WelderPSNO == uc.WelderPsNo && a.JointType == "T#TS")? Helper.GenerateActionIcon(uc.Id, "Delete", "Token is generated", "fa fa-trash-o", "DeleteRecord("+uc.Id+")","",true): Helper.GenerateActionIcon(uc.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteRecord("+uc.Id+")")))
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetTSSInlineEditableRow(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();

                NDEModels objNDEModels = new NDEModels();
                ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();

                if (id == 0)
                {
                    int newRecordId = 0;
                    var newRecord = new[] {
                                Convert.ToString(newRecordId),
                                //clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//Convert.ToString(c.SpecificationNotes),
                                Helper.HTMLAutoComplete(newRecordId,"txtWelderName","","",false,"","Welder",false)+ Helper.GenerateHidden(newRecordId,"Welder"),
                                Helper.GenerateLabelFor(newRecordId,"lblWelderName", "","","clsWelderName"),
                                Helper.GenerateTextbox(newRecordId,"WelderStampNo","","",false,"","6","",false),
                                Helper.HTMLAutoComplete(newRecordId,"txtShop","","",false,"","Shop",false)+ Helper.GenerateHidden(newRecordId,"Shop"),
                                Helper.GenerateTextbox(newRecordId,"WQTNo",maxlength:"10"),
                                Helper.HTMLAutoComplete(newRecordId,"txtWeldingProcessEsQualified","","",false,"","WeldingProcessEsQualified",false) + Helper.GenerateHidden(newRecordId,"WeldingProcessEsQualified"),
                                Helper.HTMLAutoComplete(newRecordId,"txtWeldingTypeQualified","","",false,"","WeldingTypeQualified",false) + Helper.GenerateHidden(newRecordId,"WeldingTypeQualified"),
                                Helper.HTMLAutoComplete(newRecordId,"txtJoinType","","",false,"","JoinType",false)+ Helper.GenerateHidden(newRecordId,"JoinType"),
                                Helper.GenerateTextbox(newRecordId,"LastWeldDate"), //c.LastWeldDate.HasValue? c.LastWeldDate.Value.ToString("dd/MM/yyyy"): "",
                                Helper.GenerateTextbox(newRecordId,"ValueUpto",isReadOnly: true), //c.ValueUpto.HasValue? c.ValueUpto.Value.ToString("dd/MM/yyyy"): "",
                                Helper.GenerateTextbox(newRecordId,"Remarks",maxlength:"100"),
                                Helper.GenerateGridButton(newRecordId, "Save", "Add New Record", "fa fa-plus", "EditRecord("+newRecordId+")")
                                };
                    data.Add(newRecord);
                }
                else
                {
                    var lstResult = db.SP_WML_TSSCERTIFICATE_DETAILS(1, 0, "", "Id = " + id).Take(1).ToList();
                    //var lstResult = db.WML001.Where(w=>w.Id == id).ToList();

                    foreach (var c in lstResult)
                    {
                        var isTokenExist = db.WPS025.Any(a => a.WelderPSNO == c.WelderPsNo && a.JointType == "T#TS");
                        data.Add(new string[]{
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                //clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//!string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? Convert.ToString(uc.SpecificationNotes) : Helper.GenerateHTMLTextboxOnChanged(uc.Id, "SpecificationNotes", Convert.ToString(uc.SpecificationNotes), "UpdateCertificate(this,\"Groove\", "+ uc.Id +" );",false,"",string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ?false:true, "200"), //: Convert.ToString(uc.QualityIdDesc)),// Convert.ToString(uc.SpecificationNotes),
                                isReadOnly || isTokenExist? c.WelderPsNo: Helper.HTMLAutoComplete(c.Id,"txtWelderName",c.WelderPsNo,"",false,"","Welder",false)+ Helper.GenerateHidden(c.Id,"Welder",c.WelderPsNo),
                                Helper.GenerateLabelFor(c.Id,"lblWelderName",c.WelderName,"","clsWelderName"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? c.WelderStampNo: Helper.GenerateTextbox(c.Id,"WelderStampNo",c.WelderStampNo,"UpdateData(this, "+ c.Id +");",false,"","100","",false),
                                isReadOnly? c.Shop: Helper.HTMLAutoComplete(c.Id,"txtShop",c.Shop,"",false,"","Shop",false)+ Helper.GenerateHidden(c.Id,"Shop",c.Shop),
                                isReadOnly? c.WQTNo: Helper.GenerateTextbox(c.Id,"WQTNo",c.WQTNo,maxlength:"10"),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? objNDEModels.GetCategory(c.WeldingProcessEsQualified,true).CategoryDescription: Helper.HTMLAutoComplete(c.Id,"txtWeldingProcessEsQualified",objNDEModels.GetCategory(c.WeldingProcessEsQualified,true).CategoryDescription,"",false,"","WeldingProcessEsQualified")+ Helper.GenerateHidden(c.Id,"WeldingProcessEsQualified",c.WeldingProcessEsQualified),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? objNDEModels.GetCategory(c.WeldingTypeQualified,true).CategoryDescription: Helper.HTMLAutoComplete(c.Id,"txtWeldingTypeQualified",objNDEModels.GetCategory(c.WeldingTypeQualified,true).CategoryDescription,"",false,"","WeldingTypeQualified")+ Helper.GenerateHidden(c.Id,"WeldingTypeQualified",c.WeldingTypeQualified),
                                clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),//isReadOnly? objNDEModels.GetCategory(c.JoinType,true).CategoryDescription: Helper.HTMLAutoComplete(c.Id,"txtJoinType",objNDEModels.GetCategory(c.JoinType,true).CategoryDescription,"",false,"","JoinType")+ Helper.GenerateHidden(c.Id,"JoinType",c.JoinType),
                                isReadOnly? (c.LastWeldDate.HasValue? c.LastWeldDate.Value.ToString("dd/MM/yyyy"): ""): Helper.GenerateTextbox(c.Id,"LastWeldDate",c.LastWeldDate.HasValue? c.LastWeldDate.Value.ToString("yyyy-MM-dd"): ""),
                                isReadOnly? (c.ValueUpto.HasValue? c.ValueUpto.Value.ToString("dd/MM/yyyy"): "") :Helper.GenerateTextbox(c.Id,"ValueUpto",c.ValueUpto.HasValue? c.ValueUpto.Value.ToString("yyyy-MM-dd"): "",isReadOnly: true),
                                isReadOnly || isTokenExist? c.Remarks: Helper.GenerateTextbox(c.Id,"Remarks",c.Remarks,maxlength:"100"),
                                isReadOnly? Helper.GenerateActionIcon(c.Id, "Edit", "Edit Record", "fa fa-pencil-square-o")+' '+(isTokenExist? Helper.GenerateActionIcon(c.Id, "Delete", "Token is generated", "fa fa-trash-o", "DeleteRecord("+c.Id+")","",true): Helper.GenerateActionIcon(c.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteRecord("+c.Id+")")) : Helper.GenerateActionIcon(c.Id, "Save", "Save Record", "fa fa-save", "EditRecord("+c.Id+")") + " " + Helper.GenerateActionIcon(c.Id,"Cancel","Cancel Edit","fa fa-ban","CancelEditLine();")
                        });
                    }
                }
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult SaveWMLTSSCertificate(FormCollection fc, int Id)
        {
            var objResponseMsg = new clsHelper.ResponseMsg();
            var objWML003 = new WML003();
            try
            {
                if (Id > 0)
                {
                    objWML003 = db.WML003.FirstOrDefault(w => w.Id == Id);
                    if (!db.WPS025.Any(a => a.WelderPSNO == objWML003.WelderName && a.JointType == "T#TS"))
                    {
                        objWML003.WelderName = (fc["Welder" + Id] + "").Split('-')[0];
                        objWML003.Remarks = fc["Remarks" + Id];
                    }
                    ////objWML001.Location = fc["Location" + Id];
                    //objWML003.SpecificationNotes = fc["SpecificationNotes" + Id];
                    //objWML003.WPSNo = fc["WPSNo" + Id];
                    //objWML003.WelderStampNo = fc["WelderStampNo" + Id];
                    //objWML003.JoinType = fc["JoinType" + Id];
                    //objWML003.WeldingProcessEsQualified = fc["WeldingProcessEsQualified" + Id];
                    //objWML003.WeldingTypeQualified = fc["WeldingTypeQualified" + Id];
                    objWML003.WQTNo = fc["WQTNo" + Id];
                    objWML003.Shop = (fc["Shop" + Id] + "").Split('-')[0];
                    objWML003.LastWeldDate = Convert.ToDateTime(fc["LastWeldDate" + Id]);
                    objWML003.ValueUpto = Convert.ToDateTime(fc["ValueUpto" + Id]);
                    objWML003.EditedBy = objClsLoginInfo.UserName;
                    objWML003.EditedOn = DateTime.Now;
                    //objWML001.MaintainedBy = fc["MaintainedBy" + Id];
                    //objWML001.ModifiedBy = fc["ModifiedBy" + Id];
                    //objWML001.MaintainedOn = Convert.ToDateTime(fc["MaintainedOn" + Id]);
                    //objWML001.ModifiedOn = Convert.ToDateTime(fc["ModifiedOn" + Id]);

                    db.SaveChanges();
                    objResponseMsg.Value = "Record updated successfully";
                    objResponseMsg.Key = true;
                }
                else
                {
                    objWML003.WQTNo = fc["WQTNo" + Id];
                    objWML003.WelderName = fc["Welder" + Id];
                    objWML003.WelderStampNo = fc["WelderStampNo" + Id];
                    objWML003.Project = fc["Project" + Id];
                    objWML003.WMLType = fc["WMLType" + Id];
                    objWML003.Location = fc["Location" + Id];

                    var typeASME = clsImplementationEnum.WMLType.ASME.GetStringValue();
                    if (db.WML001.Any(a => a.Location == objWML003.Location && a.Welder == objWML003.WelderName && a.WelderStampNo == objWML003.WelderStampNo && a.WMLType == objWML003.WMLType && a.WQTNo == objWML003.WQTNo && (a.WMLType == typeASME || a.Project == objWML003.Project))
                       || db.WML002.Any(a => a.Location == objWML003.Location && a.WelderName == objWML003.WelderName && a.WelderStampNo == objWML003.WelderStampNo && a.WMLType == objWML003.WMLType && a.WQTNo == objWML003.WQTNo && (a.WMLType == typeASME || a.Project == objWML003.Project))
                       || db.WML003.Any(a => a.Location == objWML003.Location && a.WelderName == objWML003.WelderName && a.WelderStampNo == objWML003.WelderStampNo && a.WMLType == objWML003.WMLType && a.WQTNo == objWML003.WQTNo && a.Project == objWML003.Project))
                    {
                        objResponseMsg.Value = "WQT No. is already being used";
                        objResponseMsg.Key = false;
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    objWML003.SpecificationNotes = fc["SpecificationNotes" + Id];
                    //objWML003.WPSNo = fc["WPSNo" + Id];
                    objWML003.Shop = fc["Shop" + Id];
                    objWML003.JoinType = fc["JoinType" + Id];
                    objWML003.WeldingProcessEsQualified = fc["WeldingProcessEsQualified" + Id];
                    objWML003.WeldingTypeQualified = fc["WeldingTypeQualified" + Id];
                    objWML003.ValueUpto = Convert.ToDateTime(fc["ValueUpto" + Id]);
                    objWML003.LastWeldDate = Convert.ToDateTime(fc["LastWeldDate" + Id]);
                    objWML003.Remarks = fc["Remarks" + Id];
                    objWML003.CreatedBy = objClsLoginInfo.UserName;
                    objWML003.CreatedOn = DateTime.Now;

                    db.WML003.Add(objWML003);
                    db.SaveChanges();
                    objResponseMsg.Value = "Record added successfully";
                    objResponseMsg.Key = true;

                    //#region Add New Spot Line1
                    //objCONFIG.Value = strValue;
                    //objCONFIG.CreatedBy = objClsLoginInfo.UserName;
                    //objCONFIG.CreatedOn = DateTime.Now;
                    //db.CONFIG.Add(objTCPobjCONFIG002);
                    //objResponseMsg.Value = "Value added successfully";
                    //objResponseMsg.Key = true;
                    //#endregion
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteWMLTSSCertificate(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            var objWML003 = db.WML003.FirstOrDefault(x => x.Id == Id);
            try
            {
                if (objWML003 != null)
                {
                    if (db.WPS025.Any(a => a.WelderPSNO == objWML003.WelderName && a.JointType == "T#TS"))
                        objResponseMsg.Value = "Welder is being used in token!";
                    else
                    {
                        db.WML003.Remove(objWML003);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Record deleted successfully";
                    }
                }
                else
                    objResponseMsg.Value = "Record already deleted";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.ToString();
            }
            return Json(objResponseMsg);
        }

        #region Excel
        [SessionExpireFilter]
        public ActionResult ImportExcelWMLTSS(HttpPostedFileBase upload, string Location, string WMLType, string Project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            if (upload != null)
            {
                if (Path.GetExtension(upload.FileName).ToLower() == ".xlsx" || Path.GetExtension(upload.FileName).ToLower() == ".xls")
                {
                    ExcelPackage package = new ExcelPackage(upload.InputStream);
                    ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();

                    DataTable dtHeaderExcel = new DataTable();
                    //List<CategoryData> errors = new List<CategoryData>();
                    bool isError;

                    DataSet ds = TSSToChechValidation(package, Location, WMLType, Project, out isError);
                    if (ds.Tables[0].Rows.Count == 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Record Found!";
                    }
                    else if (!isError)
                    {
                        try
                        {
                            foreach (DataRow item in ds.Tables[0].Rows)
                            {
                                string Welder = item.Field<string>("PS NO.");
                                item.Field<string>("NAME");
                                string WelderStampNo = item.Field<string>("STAMP");
                                string Shop = item.Field<string>("SHOP");
                                string WQTNo = item.Field<string>("WQT NO");
                                string WeldingProcessQualified = item.Field<string>("WeldingProcessQualifiedCode");
                                string TypeQualified = item.Field<string>("TypeQualifiedCode");
                                string JoinType = item.Field<string>("JointTypeCode");
                                string Remarks = item.Field<string>("REMARKS");

                                string txtLastWeldDate = item.Field<string>("LAST WELD DATE");
                                DateTime? LastWeldDate = null, ValueUpto = null;
                                if (!string.IsNullOrWhiteSpace(txtLastWeldDate) && txtLastWeldDate != "01/01/0001")
                                {
                                    LastWeldDate = DateTime.ParseExact(txtLastWeldDate, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture);
                                    ValueUpto = LastWeldDate.Value.AddDays(180);
                                }

                                #region Add New MIP Master Operations
                                var isNew = false;
                                var typeASME = clsImplementationEnum.WMLType.ASME.GetStringValue();
                                var objWML003 = db.WML003.FirstOrDefault(a => a.Location == Location && a.WelderName == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == WMLType && a.WQTNo == WQTNo && a.Project == Project);
                                if(objWML003 == null)
                                {
                                    isNew = true;
                                    objWML003 = new WML003();
                                    objWML003.Location = Location;
                                    objWML003.WMLType = WMLType;
                                    objWML003.Project = Project;
                                    objWML003.WelderStampNo = WelderStampNo;
                                    objWML003.WeldingProcessEsQualified = WeldingProcessQualified;
                                    objWML003.WeldingTypeQualified = TypeQualified;
                                    objWML003.JoinType = JoinType;
                                }
                                if (isNew || !db.WPS025.Any(a => a.WelderPSNO == objWML003.WelderName && a.JointType == "T#TS"))
                                {
                                    objWML003.WelderName = Welder;
                                    objWML003.Remarks = Remarks;
                                }
                                objWML003.Shop = Shop;
                                objWML003.WQTNo = WQTNo;
                                objWML003.LastWeldDate = LastWeldDate;
                                objWML003.ValueUpto = ValueUpto;

                                if (isNew)
                                {
                                    objWML003.CreatedBy = objClsLoginInfo.UserName;
                                    objWML003.CreatedOn = DateTime.Now;
                                    db.WML003.Add(objWML003);
                                }
                                else
                                {
                                    objWML003.EditedBy = objClsLoginInfo.UserName;
                                    objWML003.EditedOn = DateTime.Now;
                                }
                                //db.SaveChanges();
                                #endregion
                            }
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "WML Certificate added successfully";
                        }
                        catch (Exception ex)
                        {
                            Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = ex.Message;
                        }
                    }
                    else
                    {
                        objResponseMsg = ErrorTSSExportToExcel(ds);
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Excel file is not valid";
                }
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please enter valid Project no.";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponceMsgWithFileName ErrorTSSExportToExcel(DataSet ds)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                MemoryStream ms = new MemoryStream();
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath("~/Resources/WQ/WML TSS Error Template.xlsx")))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        foreach (DataRow item in ds.Tables[0].Rows)
                        {
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WelderErrorMsg"))) { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0) + " (Note: " + item.Field<string>("WelderErrorMsg") + " )"; excelWorksheet.Cells[i, 1].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 1].Value = item.Field<string>(0); }
                            excelWorksheet.Cells[i, 2].Value = item.Field<string>(1);
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WelderStampNoErrorMsg"))) { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2) + " (Note: " + item.Field<string>("WelderStampNoErrorMsg") + " )"; excelWorksheet.Cells[i, 3].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 3].Value = item.Field<string>(2); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("ShopErrorMsg"))) { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3) + " (Note: " + item.Field<string>("ShopErrorMsg") + " )"; excelWorksheet.Cells[i, 4].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 4].Value = item.Field<string>(3); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WQTNoErrorMsg"))) { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4) + " (Note: " + item.Field<string>("WQTNoErrorMsg") + " )"; excelWorksheet.Cells[i, 5].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 5].Value = item.Field<string>(4); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WeldingProcessEsQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5) + " (Note: " + item.Field<string>("WeldingProcessEsQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 6].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 6].Value = item.Field<string>(5); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("WeldingTypeQualifiedErrorMsg"))) { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6) + " (Note: " + item.Field<string>("WeldingTypeQualifiedErrorMsg") + " )"; excelWorksheet.Cells[i, 7].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 7].Value = item.Field<string>(6); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("JoinTypeErrorMsg"))) { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7) + " (Note: " + item.Field<string>("JoinTypeErrorMsg") + " )"; excelWorksheet.Cells[i, 8].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 8].Value = item.Field<string>(7); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("LastWeldDateErrorMsg"))) { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8) + " (Note: " + item.Field<string>("LastWeldDateErrorMsg") + " )"; excelWorksheet.Cells[i, 9].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 9].Value = item.Field<string>(8); }
                            //if (!string.IsNullOrWhiteSpace(item.Field<string>("ValueUptoErrorMsg"))) { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9) + " (Note: " + item.Field<string>("ValueUptoErrorMsg") + " )"; excelWorksheet.Cells[i, 10].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9); }
                            if (!string.IsNullOrWhiteSpace(item.Field<string>("RemarksErrorMsg"))) { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9) + " (Note: " + item.Field<string>("RemarksErrorMsg") + " )"; excelWorksheet.Cells[i, 10].Style.Font.Color.SetColor(Color.Red); } else { excelWorksheet.Cells[i, 10].Value = item.Field<string>(9); }
                            i++;
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath(objClsLoginInfo.UserName, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        public DataSet TSSToChechValidation(ExcelPackage package, string Location, string WMLType, string Project, out bool isError)
        {
            ExcelWorksheet excelWorksheet = package.Workbook.Worksheets.First();
            DataTable dtHeaderExcel = new DataTable();
            isError = false;
            int notesIndex = 0;
            try
            {
                #region Read Lines data from excel 
                foreach (var firstRowCell in excelWorksheet.Cells[1, 1, 1, excelWorksheet.Dimension.End.Column])
                {
                    dtHeaderExcel.Columns.Add(firstRowCell.Text);
                }

                for (var rowNumber = 2; rowNumber <= excelWorksheet.Dimension.End.Row; rowNumber++)
                {
                    var row = excelWorksheet.Cells[rowNumber, 1, rowNumber, excelWorksheet.Dimension.End.Column];
                    var newRow = dtHeaderExcel.NewRow();
                    if (excelWorksheet.Cells[rowNumber, 1].Value != null)
                    {
                        foreach (var cell in row)
                        {
                            newRow[cell.Start.Column - 1] = cell.Text;
                        }
                    }
                    else
                    {
                        notesIndex = rowNumber + 1;
                        break;
                    }

                    dtHeaderExcel.Rows.Add(newRow);
                }
                #endregion 

                #region Validation for Lines
                dtHeaderExcel.Columns.Add("WelderErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WelderStampNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("ShopErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WQTNoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WeldingProcessEsQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WeldingTypeQualifiedErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("JoinTypeErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("LastWeldDateErrorMsg", typeof(string));
                //dtHeaderExcel.Columns.Add("ValueUptoErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("RemarksErrorMsg", typeof(string));
                dtHeaderExcel.Columns.Add("WeldingProcessQualifiedCode", typeof(string));
                dtHeaderExcel.Columns.Add("TypeQualifiedCode", typeof(string));
                dtHeaderExcel.Columns.Add("JointTypeCode", typeof(string));

                var lstWeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                var lstWeldingTypeQualified = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                var lstJoinType = Manager.GetSubCatagorywithoutBULocation("Joint Type WQ");

                var lstModelWelderDetail = (from a in db.QMS003
                                            where a.Location.Equals(Location, StringComparison.OrdinalIgnoreCase)
                                            select new ModelWelderDetail { WelderPSNo = a.WelderPSNo, WelderName = string.IsNullOrEmpty(a.Name) ? "" : a.Name, ShopCode = a.ShopCode }
                                        )
                                   .Union(from b in db.QMS004
                                          where b.Location.Equals(Location, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany == true
                                          select new ModelWelderDetail { WelderPSNo = b.WelderPSNo, WelderName = string.IsNullOrEmpty(b.Name) ? "" : b.Name, ShopCode = b.ShopCode }).ToList();
                var typeASME = clsImplementationEnum.WMLType.ASME.GetStringValue();

                var lstProjects = db.COM001.Select(x => new { Project = x.t_cprj }).Distinct().ToList();

                foreach (DataRow item in dtHeaderExcel.Rows)
                {

                    string Welder = item.Field<string>("PS NO.");
                    item.Field<string>("NAME");
                    string WelderStampNo = item.Field<string>("STAMP");
                    string Shop = item.Field<string>("SHOP");
                    string WQTNo = item.Field<string>("WQT NO");
                    string WeldingProcessQualified = item.Field<string>("PROCESS");
                    string TypeQualified = item.Field<string>("TYPE OF WELDING");
                    string JoinType = item.Field<string>("JOINT TYPE");
                    string Remarks = item.Field<string>("REMARKS");
                    string WeldingProcessEsQualifiedCode = null;
                    string WeldingTypeQualifiedCode = null;
                    string JoinTypeCode = null;
                    string LastWeldDate = item.Field<string>("LAST WELD DATE");

                    //bool isWelderError = false;
                    string errorMessage = string.Empty;

                    if (string.IsNullOrEmpty(Welder)) { item["WelderErrorMsg"] = "Welder Name is required"; isError = true; }
                    else if (Welder.Length > 9) { item["WelderErrorMsg"] = "Welder Name maxlength exceeded"; isError = true; }
                    else if (!lstModelWelderDetail.Any(x => x.WelderPSNo.Trim() == Welder.Trim())) { item["WelderErrorMsg"] = "Welder PSNo is Invalid"; isError = true; }

                    if (string.IsNullOrEmpty(WelderStampNo)) { item["WelderStampNoErrorMsg"] = "Welder Stamp No is required"; isError = true; }
                    else if (WelderStampNo.Length > 6) { item["WelderStampNoErrorMsg"] = "Welder Stamp No maxlength exceeded"; isError = true; }

                    if (string.IsNullOrEmpty(Shop)) { item["ShopErrorMsg"] = "Shop is required"; isError = true; }
                    //else if (!isWelderError ? !lstModelWelderDetail.Any(x => x.WelderPSNo.Trim() == Welder.Trim() && x.ShopCode.Trim() == Shop.Trim()) : false)
                    //{ item["ShopErrorMsg"] = "Shop is Invalid for Welder:" + Welder; isError = true; }

                    if (string.IsNullOrEmpty(WQTNo)) { item["WQTNoErrorMsg"] = "WQT No is required"; isError = true; }
                    else if (WQTNo.Length > 10) { item["WQTNoErrorMsg"] = "WQT No maxlength exceeded"; isError = true; }
                    //else if (db.WML001.Any(a => a.WQTNo == WQTNo) || db.WML002.Any(a => a.WQTNo == WQTNo) || db.WML003.Any(a => a.WQTNo == WQTNo && a.Project != Project && a.WMLType != WMLType)) { item["WQTNoErrorMsg"] = "WQT No. is already being used"; isError = true; }
                    else if (db.WML003.Any(a => a.Location != Location && a.WelderName != Welder && a.WelderStampNo != WelderStampNo && a.WMLType != WMLType && a.WQTNo == WQTNo && a.Project != Project)
                     || db.WML001.Any(a => a.Location == Location && a.Welder == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == WMLType && a.WQTNo == WQTNo && (a.WMLType == typeASME || a.Project == Project))
                     || db.WML002.Any(a => a.Location == Location && a.WelderName == Welder && a.WelderStampNo == WelderStampNo && a.WMLType == WMLType && a.WQTNo == WQTNo && (a.WMLType == typeASME || a.Project == Project))) { item["WQTNoErrorMsg"] = "WQT No. is already being used"; isError = true; }

                    if (!string.IsNullOrEmpty(WeldingProcessQualified))
                    {
                        if ((WeldingProcessEsQualifiedCode = lstWeldingProcess.Where(w => w.CategoryDescription.ToLower() == WeldingProcessQualified.ToLower() || w.Value.ToLower() == WeldingProcessQualified.ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                        {
                            item["WeldingProcessEsQualifiedErrorMsg"] = "Welding Process Qualified is Invalid"; isError = true;
                            isError = true;
                        }
                        else
                            item["WeldingProcessQualifiedCode"] = WeldingProcessEsQualifiedCode;
                    }
                    if (!string.IsNullOrEmpty(TypeQualified))
                    {
                        if ((WeldingTypeQualifiedCode = lstWeldingTypeQualified.Where(w => w.CategoryDescription.ToLower() == TypeQualified.ToLower() || w.Value.ToLower() == TypeQualified.ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                        {
                            item["WeldingTypeQualifiedErrorMsg"] = "Welding Type is Invalid"; isError = true;
                            isError = true;
                        }
                        else
                            item["TypeQualifiedCode"] = WeldingTypeQualifiedCode;
                    }
                    if (!string.IsNullOrEmpty(JoinType))
                    {
                        if ((JoinTypeCode = lstJoinType.Where(w => w.CategoryDescription.ToLower() == JoinType.ToLower() || w.Value.ToLower() == JoinType.ToLower()).Select(s => s.Value).FirstOrDefault()) == null)
                        {
                            item["JoinTypeErrorMsg"] = "JoinType is Invalid"; isError = true;
                            isError = true;
                        }
                        else
                            item["JointTypeCode"] = JoinTypeCode;
                    }

                    DateTime tempParse;
                    if (string.IsNullOrEmpty(LastWeldDate)) { item["LastWeldDateErrorMsg"] = "LastWeldDate is required"; isError = true; }
                    else if (!DateTime.TryParseExact(LastWeldDate, @"d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, 0, out tempParse)) { item["LastWeldDateErrorMsg"] = "LastWeldDate invalid date format"; isError = true; }

                    //if (string.IsNullOrEmpty(ValueUpto)) { item["ValueUptoErrorMsg"] = "ValueUpto is required"; isError = true; } else if (ValueUpto.Length > 9) { item["ValueUptoErrorMsg"] = "ValueUpto maxlength exceeded"; isError = true; } else if (!lstLocation.Contains(ValueUpto)) { item["ValueUptoErrorMsg"] = "ValueUpto is Invalid"; isError = true; }

                    if (!string.IsNullOrEmpty(Remarks) && Remarks.Length > 100) { item["RemarksErrorMsg"] = "Remarks maxlength exceeded"; isError = true; }
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                isError = true;
                dtHeaderExcel.Rows[0]["RemarksErrorMsg"] = "EXCEPTION:" + ex.Message;
            }
            DataSet ds = new DataSet();
            ds.Tables.Add(dtHeaderExcel);
            return ds;
        }

        #endregion
        #endregion

        #region ExcelHelper
        public ActionResult GenerateExcelWML(string whereCondition, string strSortOrder, string gridType = "", string m = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            clsHelper.ResponceMsgWithFileName objexport = new clsHelper.ResponceMsgWithFileName();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.WPQCertificate.GetStringValue())
                {
                    var lst = db.SP_WML_WPQCERTIFICATE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    objexport = GenerateGrooveExcel(lst, m);
                    strFileName = objexport.FileName;
                }
                else if (gridType == clsImplementationEnum.GridType.WOPQCertificate.GetStringValue())
                {
                    var lst = db.SP_WML_WOPQCERTIFICATE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    //var WeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                    //var WeldingType = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                    //var Backing = Manager.GetSubCatagorywithoutBULocation("Backing");
                    //var InertGasBacking = Manager.GetSubCatagorywithoutBULocation("Inert gas backing");

                    objexport = GenerateOverlayExcel(lst, m);
                    strFileName = objexport.FileName;
                }
                else if (gridType == clsImplementationEnum.GridType.TTSCertificate.GetStringValue())
                {
                    var lst = db.SP_WML_TSSCERTIFICATE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                    objexport = GenerateTTSExcel(lst, m);
                    strFileName = objexport.FileName;
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public clsHelper.ResponceMsgWithFileName GenerateGrooveExcel(List<SP_WML_WPQCERTIFICATE_DETAILS_Result> lstGroove, string DownLoadTemplate)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {
                var WeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                var WeldingType = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                var Backing = Manager.GetSubCatagorywithoutBULocation("Backing");
                var InertGasBacking = Manager.GetSubCatagorywithoutBULocation("Inert gas backing");
                string name = DownLoadTemplate == "y" ? "Import_Template" : objClsLoginInfo.UserName;
                MemoryStream ms = new MemoryStream();
                string path = DownLoadTemplate == "y" ? "~/Resources/WQ/WML Groove_Fillet Error Template.xlsx" : "~/Resources/WQ/WML Export Template.xlsx";
                if (lstGroove.Count == 0 && DownLoadTemplate == "y")
                {
                    path = "~/Resources/WQ/WML Groove_Fillet Template.xlsx";
                }

                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath(path)))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        if (lstGroove.Count > 0 && DownLoadTemplate == "y")
                        {
                            foreach (var item in lstGroove)
                            {
                                excelWorksheet.Cells[i, 1].Value = item.WelderPsNo;
                                excelWorksheet.Cells[i, 2].Value = item.Welder;
                                excelWorksheet.Cells[i, 3].Value = item.WelderStampNo;
                                excelWorksheet.Cells[i, 4].Value = item.Shop;
                                excelWorksheet.Cells[i, 5].Value = GetCodeDesc(item.WeldingProcessQualified, WeldingProcess);
                                excelWorksheet.Cells[i, 6].Value = GetCodeDesc(item.TypeQualified, WeldingType);
                                excelWorksheet.Cells[i, 7].Value = item.PlatePipePositionActual;
                                excelWorksheet.Cells[i, 8].Value = item.PipeWeldingPositionQualified;
                                excelWorksheet.Cells[i, 9].Value = item.PlatePipePositionQualified;
                                excelWorksheet.Cells[i, 10].Value = item.PNo;
                                excelWorksheet.Cells[i, 11].Value = item.FNo;
                                excelWorksheet.Cells[i, 12].Value = item.BaseMetalThicknessQualified;
                                excelWorksheet.Cells[i, 13].Value = item.DepositedThicknessQualified;
                                excelWorksheet.Cells[i, 14].Value = item.PlatePipeQualified;
                                excelWorksheet.Cells[i, 15].Value = GetCodeDesc(item.BackingQualified, Backing);
                                excelWorksheet.Cells[i, 16].Value = GetCodeDesc(item.InertGasBackingQualified, InertGasBacking);
                                excelWorksheet.Cells[i, 17].Value = item.CircWeldOvly;
                                excelWorksheet.Cells[i, 18].Value = item.LastWeldDate;
                                excelWorksheet.Cells[i, 19].Value = item.WQTNo;
                                excelWorksheet.Cells[i, 20].Value = item.Remarks;
                                i++;
                            }
                        }
                        else
                        {
                            foreach (var item in lstGroove)
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Location;
                                excelWorksheet.Cells[i, 2].Value = item.WMLType;
                                excelWorksheet.Cells[i, 3].Value = item.Project;
                                excelWorksheet.Cells[i, 4].Value = item.WelderPsNo;
                                excelWorksheet.Cells[i, 5].Value = item.Welder;
                                excelWorksheet.Cells[i, 6].Value = item.WelderStampNo;
                                excelWorksheet.Cells[i, 7].Value = item.Shop;
                                excelWorksheet.Cells[i, 8].Value = GetCodeDesc(item.WeldingProcessQualified, WeldingProcess);
                                excelWorksheet.Cells[i, 9].Value = GetCodeDesc(item.TypeQualified, WeldingType);
                                excelWorksheet.Cells[i, 10].Value = item.PlatePipePositionActual;
                                excelWorksheet.Cells[i, 11].Value = item.PipeWeldingPositionQualified;
                                excelWorksheet.Cells[i, 12].Value = item.PlatePipePositionQualified;
                                excelWorksheet.Cells[i, 13].Value = item.PNo;
                                excelWorksheet.Cells[i, 14].Value = item.FNo;
                                excelWorksheet.Cells[i, 15].Value = item.BaseMetalThicknessQualified;
                                excelWorksheet.Cells[i, 16].Value = item.DepositedThicknessQualified;
                                excelWorksheet.Cells[i, 17].Value = item.PlatePipeQualified;
                                excelWorksheet.Cells[i, 18].Value = GetCodeDesc(item.BackingQualified, Backing);
                                excelWorksheet.Cells[i, 19].Value = GetCodeDesc(item.InertGasBackingQualified, InertGasBacking);
                                excelWorksheet.Cells[i, 20].Value = item.CircWeldOvly;
                                excelWorksheet.Cells[i, 21].Value = item.LastWeldDate.HasValue ? item.LastWeldDate.Value.ToString("dd/MM/yyyy") : "";
                                excelWorksheet.Cells[i, 22].Value = item.ValueUpto.HasValue ? item.ValueUpto.Value.ToString("dd/MM/yyyy") : "";
                                excelWorksheet.Cells[i, 23].Value = item.WQTNo;
                                excelWorksheet.Cells[i, 24].Value = item.Remarks;
                                i++;
                            }
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;

                string fileName;
                string excelFilePath = Helper.ExcelFilePath(name, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }
        public clsHelper.ResponceMsgWithFileName GenerateOverlayExcel(List<SP_WML_WOPQCERTIFICATE_DETAILS_Result> lstOverlay, string DownLoadTemplate)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {

                MemoryStream ms = new MemoryStream();
                string name = DownLoadTemplate == "y" ? "Import_Template" : objClsLoginInfo.UserName;
                string path = DownLoadTemplate == "y" ? "~/Resources/WQ/WML Overlay Error Template.xlsx" : "~/Resources/WQ/WML Export Template.xlsx";
                if (lstOverlay.Count == 0 && DownLoadTemplate == "y")
                {
                    path = "~/Resources/WQ/WML Overlay Template.xlsx";
                }
                var WeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                var WeldingType = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                var Backing = Manager.GetSubCatagorywithoutBULocation("Backing");
                #region
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath(path)))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        if (lstOverlay.Count > 0 && DownLoadTemplate == "y")
                        {
                            foreach (var item in lstOverlay)
                            {
                                excelWorksheet.Cells[i, 1].Value = item.WelderPsNo;
                                excelWorksheet.Cells[i, 2].Value = item.Welder;
                                excelWorksheet.Cells[i, 3].Value = item.WelderStampNo;
                                excelWorksheet.Cells[i, 4].Value = item.Shop;
                                excelWorksheet.Cells[i, 5].Value = GetCodeDesc(item.WeldingProcessEsQualified, WeldingProcess);
                                excelWorksheet.Cells[i, 6].Value = GetCodeDesc(item.TypeOfWeldingAutomaticQualified, WeldingType);
                                excelWorksheet.Cells[i, 7].Value = item.IPlatePipeOverlayWeldingPositionQualified;
                                excelWorksheet.Cells[i, 8].Value = item.IiPipeFilletWeldingPositionQualified;
                                excelWorksheet.Cells[i, 9].Value =  item.PNo;
                                excelWorksheet.Cells[i, 10].Value = item.FNo;
                                excelWorksheet.Cells[i, 11].Value = item.BaseMetalThicknessQualified;
                                excelWorksheet.Cells[i, 12].Value = item.DepositedThicknessQualified;
                                excelWorksheet.Cells[i, 13].Value = item.PlatePipeEnterDiaIfPipeOrTubeQualified;
                                excelWorksheet.Cells[i, 14].Value = GetCodeDesc(item.BackingQualified, Backing);
                                excelWorksheet.Cells[i, 15].Value = item.CircWeldOvly;
                                excelWorksheet.Cells[i, 16].Value = item.LastWeldDate;
                                excelWorksheet.Cells[i, 17].Value = item.WQTNo;
                                excelWorksheet.Cells[i, 18].Value = item.Remarks;
                                i++;
                            }
                        }
                        else
                        {
                            foreach (var item in lstOverlay)
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Location;
                                excelWorksheet.Cells[i, 2].Value = item.WMLType;
                                excelWorksheet.Cells[i, 3].Value = item.Project;
                                excelWorksheet.Cells[i, 4].Value = item.WelderPsNo;
                                excelWorksheet.Cells[i, 5].Value = item.Welder;
                                excelWorksheet.Cells[i, 6].Value = item.WelderStampNo;
                                excelWorksheet.Cells[i, 7].Value = item.Shop;
                                excelWorksheet.Cells[i, 8].Value = GetCodeDesc(item.WeldingProcessEsQualified, WeldingProcess);
                                excelWorksheet.Cells[i, 9].Value = GetCodeDesc(item.TypeOfWeldingAutomaticQualified, WeldingType);
                                excelWorksheet.Cells[i, 10].Value = item.IPlatePipeOverlayWeldingPositionQualified;
                                excelWorksheet.Cells[i, 11].Value = item.IiPipeFilletWeldingPositionQualified;
                                excelWorksheet.Cells[i, 12].Value = item.IPlatePipeFilletWeldingPositionQualified;
                                excelWorksheet.Cells[i, 13].Value = item.PNo;
                                excelWorksheet.Cells[i, 14].Value = item.FNo;
                                excelWorksheet.Cells[i, 15].Value = item.BaseMetalThicknessQualified;
                                excelWorksheet.Cells[i, 16].Value = item.DepositedThicknessQualified;
                                excelWorksheet.Cells[i, 17].Value = item.PlatePipeEnterDiaIfPipeOrTubeQualified;
                                excelWorksheet.Cells[i, 18].Value = GetCodeDesc(item.BackingQualified, Backing);
                                excelWorksheet.Cells[i, 19].Value = item.InertGasBackingQualified;
                                excelWorksheet.Cells[i, 20].Value = item.CircWeldOvly;
                                excelWorksheet.Cells[i, 21].Value = item.LastWeldDate.HasValue ? item.LastWeldDate.Value.ToString("dd/MM/yyyy") : "";
                                excelWorksheet.Cells[i, 22].Value = item.ValueUpto.HasValue ? item.ValueUpto.Value.ToString("dd/MM/yyyy") : "";
                                excelWorksheet.Cells[i, 23].Value = item.WQTNo;
                                excelWorksheet.Cells[i, 24].Value = item.Remarks;
                                i++;
                            }
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;
                #endregion
                string fileName;
                string excelFilePath = Helper.ExcelFilePath(name, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }
        public clsHelper.ResponceMsgWithFileName GenerateTTSExcel(List<SP_WML_TSSCERTIFICATE_DETAILS_Result> lstTTS, string DownLoadTemplate)
        {
            clsHelper.ResponceMsgWithFileName objResponseMsg = new clsHelper.ResponceMsgWithFileName();
            try
            {

                MemoryStream ms = new MemoryStream();
                string name = DownLoadTemplate == "y" ? "Import_Template" : objClsLoginInfo.UserName;
                string path = DownLoadTemplate == "y" ? "~/Resources/WQ/WML TSS Error Template.xlsx" : "~/Resources/WQ/WML TSS Export Template.xlsx";
                if (lstTTS.Count == 0 && DownLoadTemplate == "y")
                {
                    path = "~/Resources/WQ/WML TSS Excel Template.xlsx";
                }
                var WeldingProcess = Manager.GetSubCatagorywithoutBULocation("Welding Process");
                var WeldingType = Manager.GetSubCatagorywithoutBULocation("Welding Type");
                var JointTypeWQ = Manager.GetSubCatagorywithoutBULocation("Joint Type WQ");
                #region
                using (FileStream fs = System.IO.File.OpenRead(Server.MapPath(path)))
                {
                    using (ExcelPackage excelPackage = new ExcelPackage(fs))
                    {
                        ExcelWorkbook excelWorkBook = excelPackage.Workbook;
                        ExcelWorksheet excelWorksheet = excelWorkBook.Worksheets.First();
                        int i = 2;
                        if (lstTTS.Count > 0 && DownLoadTemplate == "y")
                        {
                            foreach (var item in lstTTS)
                            {
                                excelWorksheet.Cells[i, 1].Value = item.WelderPsNo;
                                excelWorksheet.Cells[i, 2].Value = item.WelderName;
                                excelWorksheet.Cells[i, 3].Value = item.WelderStampNo;
                                excelWorksheet.Cells[i, 4].Value = item.Shop;
                                excelWorksheet.Cells[i, 5].Value = item.WQTNo;
                                excelWorksheet.Cells[i, 6].Value = GetCodeDesc(item.WeldingProcessEsQualified, WeldingProcess);
                                excelWorksheet.Cells[i, 7].Value = GetCodeDesc(item.WeldingTypeQualified, WeldingType);
                                excelWorksheet.Cells[i, 8].Value = GetCodeDesc(item.JoinType, JointTypeWQ);
                                excelWorksheet.Cells[i, 9].Value = item.LastWeldDate.HasValue ? item.LastWeldDate.Value.ToString("dd/MM/yyyy") : "";
                                excelWorksheet.Cells[i, 10].Value = item.Remarks;
                            }
                        }
                        else
                        {
                            foreach (var item in lstTTS)
                            {
                                excelWorksheet.Cells[i, 1].Value = item.Location;
                                excelWorksheet.Cells[i, 2].Value = item.WMLType;
                                excelWorksheet.Cells[i, 3].Value = item.Project;
                                excelWorksheet.Cells[i, 4].Value = item.WelderPsNo;
                                excelWorksheet.Cells[i, 5].Value = item.WelderName;
                                excelWorksheet.Cells[i, 6].Value = item.WelderStampNo;
                                excelWorksheet.Cells[i, 7].Value = item.Shop;
                                excelWorksheet.Cells[i, 8].Value = item.WQTNo;
                                excelWorksheet.Cells[i, 9].Value = GetCodeDesc(item.WeldingProcessEsQualified, WeldingProcess);
                                excelWorksheet.Cells[i, 10].Value = GetCodeDesc(item.WeldingTypeQualified, WeldingType);
                                excelWorksheet.Cells[i, 11].Value = GetCodeDesc(item.JoinType, JointTypeWQ);
                                excelWorksheet.Cells[i, 12].Value = item.LastWeldDate.HasValue ? item.LastWeldDate.Value.ToString("dd/MM/yyyy") : "";
                                excelWorksheet.Cells[i, 13].Value = item.ValueUpto.HasValue ? item.ValueUpto.Value.ToString("dd/MM/yyyy") : "";
                                excelWorksheet.Cells[i, 14].Value = item.Remarks;
                                i++;
                            }
                        }
                        excelPackage.SaveAs(ms);
                    }
                }

                ms.Position = 0;
                #endregion
                string fileName;
                string excelFilePath = Helper.ExcelFilePath(name, out fileName);

                Byte[] bin = ms.ToArray();
                System.IO.File.WriteAllBytes(excelFilePath, bin);

                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error";
                objResponseMsg.FileName = fileName;
                return objResponseMsg;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }
        #endregion
        private string GetCodeDesc(string Code, List<CategoryData> lstGBL)
        {
            try
            {
                string description = lstGBL.FirstOrDefault(f => f.Value == Code).CategoryDescription;
                if (string.IsNullOrWhiteSpace(description))
                {
                    description = Code;
                }
                return description;
            }
            catch (Exception)
            {
                return Code;
            }
        }

        #region tab 1 & 2
        //tab 1 & 2
        [HttpPost]
        public JsonResult LoadWMLDataWPQCertificateData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                string[] columnName = { "SpecificationNotes", "Location", "WQTNo", "WPSNo", "welders.Welder", "WelderStampNo", "TypeOfTestCoupon", "MaintainedBy", "MaintainedOn" };

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhere += "1=1 and ValueUpto >= getdate() and Location='" + param.MTStatus + "'";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_WQ_WPQCERTIFICATE_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();
                NDEModels objNDEModels = new NDEModels();
                ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();
                var data = (from uc in lstResult
                            select new[]
                           {
                                //Convert.ToString(uc.Location),
                                !string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? Convert.ToString(uc.SpecificationNotes) : Helper.GenerateHTMLTextboxOnChanged(uc.Id, "SpecificationNotes", Convert.ToString(uc.SpecificationNotes), "UpdateCertificate(this,\"Groove\", "+ uc.Id +" );",false,"",string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ?false:true, "200"), //: Convert.ToString(uc.QualityIdDesc)),// Convert.ToString(uc.SpecificationNotes),
                                Convert.ToString(uc.WQTNo),
                                Convert.ToString(uc.WelderStampNo),
                                Convert.ToString(uc.Welder),
                                Convert.ToString(uc.Shop),
                                Convert.ToString(objModelWQRequest.GetCategoryDescWithMainCategory("Joint Type WQ",uc.JointType).CategoryDescription),
                                Convert.ToString(objModelWQRequest.GetCategoryDescWithMainCategory("Welding Process",uc.WeldingProcessQualified).CategoryDescription),
                                Convert.ToString(objModelWQRequest.GetCategoryDescWithMainCategory("Welding Type",uc.TypeQualified).CategoryDescription),
                                Convert.ToDateTime(uc.ValueUpto).ToString("dd/MM/yyyy"),
                                Convert.ToString(uc.PNo),
                                Convert.ToString(uc.FNo),
                                Convert.ToString(uc.PlatePipeQualified),
                                Convert.ToString(uc.DepositedThicknessQualified),
                                Convert.ToString(uc.PlatePipePositionActual),
                                Convert.ToString(uc.PipeWeldingPositionQualified),
                                Convert.ToString(uc.PlatePipePositionQualified),
                                Convert.ToString(uc.BaseMetalThicknessQualified),
                                Convert.ToString(objNDEModels.GetCategory(uc.BackingQualified).CategoryDescription),
                                Convert.ToString(objNDEModels.GetCategory(uc.InertGasBackingQualified).CategoryDescription),
                                Convert.ToString(uc.CircWeldOvly),
                                Convert.ToString(uc.Remarks),
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadWMLDataWOPQCertificateData(JQueryDataTableParamModel param)
        {
            try
            {
                NDEModels objNDEModels = new NDEModels();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                string[] columnName = {"SpecificationNotes",
                                "WQTNo",
                                //"WPSNo",
                                "WelderName",
                                "WelderStampNo",
                                "wqr.Shop",
                                "TypeOfTestCoupon",
                                "WeldingProcessEsQualified",
                                "TypeOfWeldingAutomaticQualified",
                                "ValueUpto",
                               "PNo",
                               "FNo",
                                "PlatePipeEnterDiaIfPipeOrTubeQualified",
                                "IPlatePipeOverlayWeldingPositionQualified",
                                "IiPipeFilletWeldingPositionQualified",
                                "IPlatePipeFilletWeldingPositionQualified",
                                "BackingQualified",
                                "CircWeldOvly", "welders.Welder",
                                "BaseMetalThicknessQualified",//"DepositedThicknessQualified","InertGasBackingQualified",
                                "Remarks"};

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                var location = (from c2 in db.COM002
                                where c2.t_dimx == param.MTStatus
                                select c2.t_dimx + " - " + c2.t_desc).FirstOrDefault();

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhere += "1=1 and ValueUpto >= getdate() and wqr.Location='" + param.MTStatus + "'";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_WQ_WOPQCERTIFICATE_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               //Convert.ToString(uc.Location),
                               !string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ?Convert.ToString(uc.SpecificationNotes) : Helper.GenerateHTMLTextboxOnChanged(uc.Id, "SpecificationNotes", Convert.ToString(uc.SpecificationNotes), "UpdateCertificate(this,\"Overlay\", "+ uc.Id +" );",false,"",(string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue())) ?false:true, "200"), //: Convert.ToString(uc.QualityIdDesc)),// Convert.ToString(uc.SpecificationNotes),
                                Convert.ToString(uc.WQTNo),
                                //Convert.ToString(uc.WPSNo),
                                Convert.ToString(uc.Welder),
                                Convert.ToString(uc.WelderStampNo),
                                Convert.ToString(uc.Shop),
                                Convert.ToString(objModelWQRequest.GetCategoryDescWithMainCategory("Joint Type WQ",uc.JointType).CategoryDescription),
                                Convert.ToString(objModelWQRequest.GetCategoryDescWithMainCategory("Welding Process",uc.WeldingProcessEsQualified).CategoryDescription),
                                Convert.ToString(objModelWQRequest.GetCategoryDescWithMainCategory("Welding Type",uc.TypeOfWeldingAutomaticQualified).CategoryDescription),
                               Convert.ToDateTime(uc.ValueUpto).ToString("dd/MM/yyyy"),
                                Convert.ToString(uc.PNo),
                                Convert.ToString(uc.FNo),
                                Convert.ToString(uc.PlatePipeEnterDiaIfPipeOrTubeQualified),
                                Convert.ToString(uc.DepositedThicknessQualified),
                                Convert.ToString(uc.IPlatePipeOverlayWeldingPositionQualified),
                                Convert.ToString(uc.IiPipeFilletWeldingPositionQualified),
                                Convert.ToString(uc.IPlatePipeFilletWeldingPositionQualified),
                                Convert.ToString(uc.BaseMetalThicknessQualified),
                                Convert.ToString(objModelWQRequest.GetCategoryDescWithMainCategory("Backing",uc.BackingQualified).CategoryDescription),
                                Convert.ToString(uc.InertGasBackingQualified),
                                Convert.ToString(uc.CircWeldOvly),
                                Convert.ToString(uc.Remarks),
                            }).ToList();

                //Convert.ToString(objNDEModels.GetCategory(uc.JoinType).CategoryDescription),
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string getData(string wqtno, string colname)
        {
            string rtn = "";
            var wqt = db.WQR003.Where(a => a.WQTNo == wqtno).FirstOrDefault();
            if (wqt != null)
            {
                if (colname == "PNo")
                {
                    rtn = wqt.PNo;
                }
                if (colname == "FNo")
                {
                    rtn = wqt.PNo;
                }
            }
            else
            {
                rtn = "NA";
            }
            return rtn;
        }

        [HttpPost]
        public JsonResult LoadWMLDataTSSCertificateData(JQueryDataTableParamModel param, string location)
        {
            try
            {
                NDEModels objNDEModels = new NDEModels();
                ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();

                //if (roleId == Convert.ToInt32(RoleForWQ.ShoInCharge))
                //{
                //    objModelRoleWiseIndex.TableTitle = "Welder Qualification Request List";
                //    objModelRoleWiseIndex.LinkRoleWise = "ShopInCharge";
                //}
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                string[] columnName = {"SpecificationNotes","WQTNo","WelderStampNo","WelderName","wqr.Shop","JoinType","WeldingProcessEsQualified",
                                        "WeldingTypeQualified","ValueUpto","Remarks", "welders.Welder"};

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhere += "1=1 and ValueUpto >= getdate() and wqr.Location=" + Convert.ToString(location);
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_WQ_TSSCERTIFICATE_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                            //Convert.ToString(uc.Location),
                            !string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue(),StringComparison.OrdinalIgnoreCase) ?Convert.ToString(uc.SpecificationNotes) : Helper.GenerateHTMLTextboxOnChanged(uc.Id, "SpecificationNotes", Convert.ToString(uc.SpecificationNotes), "UpdateCertificate(this, \"TSS\","+ uc.Id +"); ",false,"",string.Equals(param.Department,WQRCertificateTab.ShopInCharge.GetStringValue()) ?false:true, "200"),
                            Convert.ToString(uc.WQTNo),
                            Convert.ToString(uc.WelderStampNo),
                            Convert.ToString(uc.WelderName),
                            Convert.ToString(uc.Shop),
                            Convert.ToString(uc.JoinType),
                            Convert.ToString(uc.WeldingProcessEsQualified),
                            Convert.ToString(uc.WeldingTypeQualified),
                           Convert.ToDateTime(uc.ValueUpto).ToString("dd/MM/yyyy"),
                            Convert.ToString(uc.Remarks),
                            }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult UpdateCertificate(int id, string columnName, string columnValue, string tabname)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS015 objQMS015 = new QMS015();
            try
            {
                switch (tabname)
                {
                    case "Groove":
                        WQR004 obj4 = new WQR004();
                        obj4 = db.WQR004.Where(a => a.Id == id).FirstOrDefault();
                        obj4.SpecificationNotes = columnValue;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        break;
                    case "Overlay":
                        WQR005 obj5 = new WQR005();
                        obj5 = db.WQR005.Where(a => a.Id == id).FirstOrDefault();
                        obj5.SpecificationNotes = columnValue;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        break;
                    case "TSS":
                        WQR006 obj6 = new WQR006();
                        obj6 = db.WQR006.Where(a => a.Id == id).FirstOrDefault();
                        obj6.SpecificationNotes = columnValue;
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        break;
                    default:
                        objResponseMsg.Key = false;
                        break;
                }
            }
            catch
            {
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Export Excel
        //Code By : nikita vibhandik 18/12/2017 (observation  14096)
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //t#TS certificate grid
                string strFileName = string.Empty;
                NDEModels objNDEModels = new NDEModels();
                if (gridType == clsImplementationEnum.GridType.TTSCertificate.GetStringValue())
                {
                    var lst = db.SP_WQ_TSSCERTIFICATE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      SpecificationNotes = Convert.ToString(uc.SpecificationNotes),
                                      WQTNo = Convert.ToString(uc.WQTNo),
                                      WelderStampNo = Convert.ToString(uc.WelderStampNo),
                                      WelderName = Convert.ToString(uc.WelderName),
                                      Shop = Convert.ToString(uc.Shop),
                                      JoinType = Convert.ToString(objNDEModels.GetCategory(uc.JoinType).CategoryDescription),
                                      WeldingProcessEsQualified = Convert.ToString(objNDEModels.GetCategory(uc.WeldingProcessEsQualified).CategoryDescription),
                                      WeldingTypeQualified = Convert.ToString(objNDEModels.GetCategory(uc.WeldingTypeQualified).CategoryDescription),
                                      ValueUpto = Convert.ToDateTime(uc.ValueUpto).ToString("dd/MM/yyyy"),
                                      Remarks = Convert.ToString(uc.Remarks),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //wpq certificate
                if (gridType == clsImplementationEnum.GridType.WPQCertificate.GetStringValue())
                {

                    var lst = db.SP_WQ_WPQCERTIFICATE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      SpecificationNotes = Convert.ToString(uc.SpecificationNotes),
                                      WQTNo = Convert.ToString(uc.WQTNo),
                                      WelderStampNo = Convert.ToString(uc.WelderStampNo),
                                      WelderName = Convert.ToString(uc.Welder),
                                      Shop = Convert.ToString(uc.Shop),
                                      TypeOfTestCoupon = Convert.ToString(objNDEModels.GetCategory(uc.TypeOfTestCoupon).CategoryDescription),
                                      WeldingProcessQualified = Convert.ToString(objNDEModels.GetCategory(uc.WeldingProcessQualified).CategoryDescription),
                                      TypeQualified = Convert.ToString(objNDEModels.GetCategory(uc.TypeQualified).CategoryDescription),
                                      ValueUpto = Convert.ToDateTime(uc.ValueUpto).ToString("dd/MM/yyyy"),
                                      PNo = Convert.ToString(uc.PNo),
                                      FNo = Convert.ToString(uc.FNo),
                                      PlatePipeQualified = Convert.ToString(uc.PlatePipeQualified),
                                      DepositedThicknessQualified = Convert.ToString(uc.DepositedThicknessQualified),
                                      PlatePipePositionActual = Convert.ToString(uc.PlatePipePositionActual),
                                      PipeWeldingPositionQualified = Convert.ToString(uc.PipeWeldingPositionQualified),
                                      PlatePipePositionQualified = Convert.ToString(uc.PlatePipePositionQualified),
                                      BaseMetalThicknessQualified = Convert.ToString(uc.BaseMetalThicknessQualified),
                                      BackingQualified = Convert.ToString(objNDEModels.GetCategory(uc.BackingQualified).CategoryDescription),
                                      InertGasBackingQualified = Convert.ToString(objNDEModels.GetCategory(uc.InertGasBackingQualified).CategoryDescription),
                                      CircWeldOvly = Convert.ToString(uc.CircWeldOvly),
                                      Remarks = Convert.ToString(uc.Remarks),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                //wOpq certificate
                if (gridType == clsImplementationEnum.GridType.WOPQCertificate.GetStringValue())
                {

                    var lst = db.SP_WQ_WOPQCERTIFICATE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      SpecificationNotes = Convert.ToString(uc.SpecificationNotes),
                                      WQTNo = Convert.ToString(uc.WQTNo),
                                      //Convert.ToString(uc.WPSNo),
                                      WelderName = Convert.ToString(uc.Welder),
                                      Shop = Convert.ToString(uc.Shop), //Convert.ToString(uc.WelderStampNo),
                                      TypeOfTestCoupon = Convert.ToString(objNDEModels.GetCategory(uc.TypeOfTestCoupon).CategoryDescription),
                                      WeldingProcessQualified = Convert.ToString(objNDEModels.GetCategory(uc.WeldingProcessEsQualified).CategoryDescription),
                                      TypeQualified = Convert.ToString(objNDEModels.GetCategory(uc.TypeOfWeldingAutomaticQualified).CategoryDescription),
                                      ValueUpto = Convert.ToDateTime(uc.ValueUpto).ToString("dd/MM/yyyy"),
                                      PNo = Convert.ToString(uc.PNo),
                                      FNo = Convert.ToString(uc.FNo),
                                      PlatePipeQualified = Convert.ToString(uc.PlatePipeEnterDiaIfPipeOrTubeQualified),
                                      DepositedThicknessQualified = Convert.ToString(uc.DepositedThicknessQualified),
                                      PlatePipePositionActual = Convert.ToString(uc.IPlatePipeOverlayWeldingPositionQualified),
                                      PipeWeldingPositionQualified = Convert.ToString(uc.IiPipeFilletWeldingPositionQualified),
                                      PlatePipePositionQualified = Convert.ToString(uc.IPlatePipeFilletWeldingPositionQualified),
                                      BaseMetalThicknessQualified = Convert.ToString(uc.BaseMetalThicknessQualified),
                                      BackingQualified = Convert.ToString(objNDEModels.GetCategory(uc.BackingQualified).CategoryDescription),
                                      InertGasBackingQualified = Convert.ToString(uc.InertGasBackingQualified),
                                      CircWeldOvly = Convert.ToString(uc.CircWeldOvly),
                                      Remarks = Convert.ToString(uc.Remarks)
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }


                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region
        [HttpPost]
        public ActionResult GetWQWelderDetail(string term, string jointtype)
        {
            List<Projects> lstModelWelderDetail = new List<Projects>();

            var listWelders = (from a in db.QMS003
                               select new ModelWelderDetail { WelderPSNo = a.WelderPSNo, WelderName = string.IsNullOrEmpty(a.Name) ? "" : a.Name }
                                  )
                             .Union(from b in db.QMS004
                                    where b.ActiveWithCompany == true //&& (b.Name.ToLower().Contains(term.ToLower()) || b.WelderPSNo.Contains(term))
                                    select new ModelWelderDetail { WelderPSNo = b.WelderPSNo, WelderName = (string.IsNullOrEmpty(b.Name) ? "" : b.Name) + " " + (string.IsNullOrEmpty(b.FatherName) ? "" : b.FatherName) + " " + (string.IsNullOrEmpty(b.Surname) ? "" : b.Surname) }).Select(i => new { WelderPSNo = i.WelderPSNo, WelderName = i.WelderName }).Distinct().ToList();

            if (jointtype == "groove")
            {
                var lstWML = db.WML001.Select(x => x.Welder).Distinct();
                lstModelWelderDetail = (from ls in listWelders
                                        join wml in lstWML on ls.WelderPSNo equals wml
                                        where (term == "" ? true : wml.ToLower().Contains(term.ToLower()) || ls.WelderName.Contains(term))
                                        select new { Value = wml, Text = wml + "-" + ls.WelderName })
                                    .Select(i => new Projects { Value = i.Value, Text = i.Text }).Distinct().Take(10).ToList();
            }
            if (jointtype == "overlay")
            {
                var lstWML = db.WML002.Select(x => new { WelderName = x.WelderName }).Distinct();
                lstModelWelderDetail = (from ls in listWelders
                                        join wml in lstWML on ls.WelderPSNo equals wml.WelderName
                                        where (term == "" ? true : wml.WelderName.ToLower().Contains(term.ToLower()) || ls.WelderName.Contains(term))
                                        select new Projects { Value = wml.WelderName, Text = wml.WelderName + "-" + ls.WelderName, projectCode = ls.WelderName })
                                    .Select(i => new Projects { Value = i.Value, Text = i.Text }).Distinct().Take(10).ToList();
            }
            if (jointtype == "tts")
            {
                var lstWML = db.WML003.Select(x => new { WelderName = x.WelderName }).Distinct();
                lstModelWelderDetail = (from ls in listWelders
                                        join wml in lstWML on ls.WelderPSNo equals wml.WelderName
                                        where (term == "" ? true : wml.WelderName.ToLower().Contains(term.ToLower()) || ls.WelderName.Contains(term))
                                        select new Projects { Value = wml.WelderName, Text = wml.WelderName + "-" + ls.WelderName, projectCode = ls.WelderName })
                                    .Select(i => new Projects { Value = i.Value, Text = i.Text }).Distinct().Take(10).ToList();
            }
            return Json(lstModelWelderDetail, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetWelderLocation(string welderpsno, string jointtype)
        {
            List<catdata> Location = new List<catdata>();

            if (jointtype == "groove")
            {
                var lstLocation = db.WML001.Where(x => x.Welder == welderpsno).Select(x => x.Location).Distinct();
                Location = (from ls in lstLocation
                            join wml in db.WQR008 on ls equals wml.Location
                            select new catdata { CatID = ls, CatDesc = ls + "-" + wml.Description }).Distinct().ToList();
            }
            if (jointtype == "overlay")
            {
                var lstLocation = db.WML002.Where(x => x.WelderName == welderpsno).Select(x => x.Location).Distinct();
                Location = (from ls in lstLocation
                            join wml in db.WQR008 on ls equals wml.Location
                            select new catdata { CatID = ls, CatDesc = ls + "-" + wml.Description }).Distinct().ToList();
            }
            if (jointtype == "tts")
            {
                var lstLocation = db.WML003.Where(x => x.WelderName == welderpsno).Select(x => x.Location).Distinct();
                Location = (from ls in lstLocation
                            join wml in db.WQR008 on ls equals wml.Location
                            select new catdata { CatID = ls, CatDesc = ls + "-" + wml.Description }).Distinct().ToList();
            }
            string LocationDesc = string.Empty;
            string LocationID = string.Empty;
            if (Location.Count > 0)
            {
                LocationDesc = Location.Select(x => x.CatDesc).FirstOrDefault();
                LocationID = Location.Select(x => x.CatID).FirstOrDefault();
            }
            var objData = new
            {
                count = Location.Count,
                lstMembers = Location,
                LocationDesc = LocationDesc,
                LocationID = LocationID
            };

            return Json(objData, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public bool checkPsnoExists(string welderpsno, string location)
        {
            var listWelders = (from b in db.QMS004
                               where b.ActiveWithCompany == true && b.Location == location && b.Location == location && b.WelderPSNo == welderpsno//&& (b.Name.ToLower().Contains(term.ToLower()) || b.WelderPSNo.Contains(term))
                               select new ModelWelderDetail { WelderPSNo = b.WelderPSNo, WelderName = (string.IsNullOrEmpty(b.Name) ? "" : b.Name) + " " + (string.IsNullOrEmpty(b.FatherName) ? "" : b.FatherName) + " " + (string.IsNullOrEmpty(b.Surname) ? "" : b.Surname) })
                               .Select(i => new { WelderPSNo = i.WelderPSNo, WelderName = i.WelderName }).Any();

            return listWelders;
        }
        #endregion

        #region Copy Certificate
        [HttpPost]
        public ActionResult LoadReplacePSNOPartial(string jointtype)
        {
            ViewBag.jointtype = jointtype;
            ViewBag.Location = db.WQR008.Select(i => new { CatID = i.Location, CatDesc = i.Location + "-" + i.Description }).ToList();
            return PartialView("_ReplacePSNOPartial");
        }

        [HttpPost]
        public ActionResult CopyToDestination(string Welder, string fromlocation, string newpsno, string newlocation, string wmltype, string project, string type)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<string> lstRemovedWML = new List<string>();
            int count = 0;
            try
            {
                if (type == "groove")
                {
                    List<WML001> lstSrcWML001 = (!string.IsNullOrWhiteSpace(project))? db.WML001.Where(x => x.Welder == Welder && x.Location == fromlocation && x.WMLType == wmltype && x.Project == project).ToList() :  db.WML001.Where(x => x.Welder == Welder && x.Location == fromlocation && x.WMLType == wmltype).ToList();
                    List<WML001> lstDesWML001 = new List<WML001>();
                    foreach (var item in lstSrcWML001)
                    {
                        if (!db.WML001.Any(a => a.WQTNo == item.WQTNo && a.Welder == newpsno && a.WelderStampNo == fromlocation) || db.WML002.Any(a => a.WQTNo == item.WQTNo) || db.WML003.Any(a => a.WQTNo == item.WQTNo))
                        {
                            item.Welder = newpsno;
                            item.Location = newlocation;
                            item.CreatedBy = objClsLoginInfo.UserName;
                            item.CreatedOn = DateTime.Now;
                            lstDesWML001.Add(item);
                        }
                        else lstRemovedWML.Add(item.WQTNo);
                    }
                    count = lstDesWML001.Count();
                    db.WML001.AddRange(lstDesWML001);
                    db.SaveChanges();
                }
                else if (type == "overlay")
                {
                    List<WML002> lstSrcWML002 = (!string.IsNullOrWhiteSpace(project)) ? db.WML002.Where(x => x.WelderName == Welder && x.Location == fromlocation && x.WMLType == wmltype && x.Project == project).ToList() : db.WML002.Where(x => x.WelderName == Welder && x.Location == fromlocation && x.WMLType == wmltype).ToList();
                    List<WML002> lstDesWML002 = new List<WML002>();

                    foreach (var x in lstSrcWML002)
                    {
                        if (!db.WML002.Any(a => a.WQTNo == x.WQTNo && a.WelderName == newpsno && a.WelderStampNo == x.WelderStampNo) || db.WML001.Any(a => a.WQTNo == x.WQTNo) || db.WML003.Any(a => a.WQTNo == x.WQTNo))
                        {
                            x.WelderName = newpsno;
                            x.Location = newlocation;
                            x.CreatedBy = objClsLoginInfo.UserName;
                            x.CreatedOn = DateTime.Now;
                            lstDesWML002.Add(x);
                        }
                        else lstRemovedWML.Add(x.WQTNo);
                    }
                    count = lstDesWML002.Count();
                    db.WML002.AddRange(lstDesWML002);
                    db.SaveChanges();
                }
                else if (type == "tts")
                {
                    List<WML003> lstSrcWML003;
                    if (wmltype != clsImplementationEnum.WMLType.ASME.GetStringValue() || !String.IsNullOrWhiteSpace(project))
                        lstSrcWML003 = db.WML003.Where(x => x.WelderName == Welder && x.Location == fromlocation && x.WMLType == wmltype && x.Project == project).ToList();
                    else
                        lstSrcWML003 = db.WML003.Where(x => x.WelderName == Welder && x.Location == fromlocation && x.WMLType == wmltype).ToList();
                    List<WML003> lstDesWML003 = new List<WML003>();
                    foreach (var item in lstSrcWML003)
                    {
                        if (!db.WML003.Any(a => a.WQTNo == item.WQTNo && a.WelderName == item.WelderName && a.WelderStampNo == item.WelderStampNo) || db.WML001.Any(a => a.WQTNo == item.WQTNo) || db.WML002.Any(a => a.WQTNo == item.WQTNo))
                        {
                            item.WelderName = newpsno;
                            item.Location = newlocation;
                            item.CreatedBy = objClsLoginInfo.UserName;
                            item.CreatedOn = DateTime.Now;
                            lstDesWML003.Add(item);
                        }
                        else lstRemovedWML.Add(item.WQTNo);
                    }
                    count = lstDesWML003.Count();
                    db.WML003.AddRange(lstDesWML003);
                    db.SaveChanges();
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = count + "Welder PS no replaced successfully";
                if (lstRemovedWML.Count > 0)
                {
                    objResponseMsg.item = "WQT No.(s) " + string.Join(",", lstRemovedWML.ToList()) + " is/are already being used.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public clsHelper.ResponseMsgWithStatus CopyWPQCertificate(WML001 objSrcWML001, string destWQTNo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objDestWML001 = new WML001();
                var lstModelWelderDetail = (from a in db.QMS003
                                            where a.WelderPSNo.Equals(objSrcWML001.Welder, StringComparison.OrdinalIgnoreCase)
                                            select new ModelWelderDetail { ShopCode = a.ShopCode }
                                                  )
                                                     .Union(from b in db.QMS004
                                                            where b.WelderPSNo.Equals(objSrcWML001.Welder, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany != null && b.ActiveWithCompany == true
                                                            select new ModelWelderDetail { ShopCode = b.ShopCode }).FirstOrDefault();

                WML001 objaddWML001 = new WML001();
                WQR003 objWQR003 = db.WQR003.Where(i => i.WQTNo == objSrcWML001.WQTNo).FirstOrDefault();
                //set flag of WQT No. (In WQR003)
                if (objWQR003 != null)
                {
                    objWQR003.CertificateGenerated = true;
                    objWQR003.CertificateGeneratedBy = objClsLoginInfo.UserName;
                    objWQR003.CertificateGeneratedOn = DateTime.Now;
                    objDestWML001.FNo = objWQR003.FNo;
                    objDestWML001.PNo = objWQR003.PNo;
                }
                else
                {
                    objDestWML001.FNo = "NA";
                    objDestWML001.PNo = "NA";
                }
                objDestWML001.Shop = objSrcWML001.Shop;
                objDestWML001.TypeOfTestCoupon = objSrcWML001.TypeOfTestCoupon;
                objDestWML001.WelderStampNo = objSrcWML001.WelderStampNo;
                objDestWML001.WPSNo = objSrcWML001.WPSNo;
                objDestWML001.Location = objSrcWML001.Location;
                objDestWML001.Welder = objSrcWML001.Welder;
                objDestWML001.WQTNo = destWQTNo;
                objDestWML001.JointType = objSrcWML001.JointType;

                objDestWML001.WeldingProcessQualified = objSrcWML001.WeldingProcessQualified;
                objDestWML001.TypeQualified = objSrcWML001.TypeQualified;
                objDestWML001.BackingQualified = objSrcWML001.BackingQualified;
                objDestWML001.PlatePipeQualified = objSrcWML001.PlatePipeQualified;
                objDestWML001.BaseMetalThicknessQualified = objSrcWML001.BaseMetalThicknessQualified;
                objDestWML001.DepositedThicknessQualified = objSrcWML001.DepositedThicknessQualified;
                objDestWML001.CircWeldOvly = objSrcWML001.CircWeldOvly;
                objDestWML001.PlatePipePositionActual = objSrcWML001.PlatePipePositionActual;
                objDestWML001.PlatePipePositionQualified = objSrcWML001.PlatePipePositionQualified;
                objDestWML001.PipeWeldingPositionQualified = objSrcWML001.PipeWeldingPositionQualified;
                objDestWML001.InertGasBackingQualified = objSrcWML001.InertGasBackingQualified;

                objDestWML001.LastWeldDate = objSrcWML001.LastWeldDate;
                objDestWML001.ValueUpto = objSrcWML001.ValueUpto;

                objDestWML001.Remarks = objSrcWML001.Remarks;
                objDestWML001.MaintainedBy = objClsLoginInfo.UserName;
                objDestWML001.MaintainedOn = DateTime.Now;
                objDestWML001.CreatedBy = objClsLoginInfo.UserName;
                objDestWML001.CreatedOn = DateTime.Now;
                db.WML001.Add(objDestWML001);
                db.SaveChanges();
                objResponseMsg.wqtno = objDestWML001.WQTNo; objResponseMsg.HeaderId = objDestWML001.Id;
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Record copied successfully!";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return objResponseMsg;
        }

        public clsHelper.ResponseMsgWithStatus CopyWOPQCertificate(WQR005 objSrcWQR005, string destWQTNo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objdestWQR005Update = new WQR005();
                var lstModelWelderDetail = (from a in db.QMS003
                                            where a.WelderPSNo.Equals(objSrcWQR005.WelderName, StringComparison.OrdinalIgnoreCase)
                                            select new ModelWelderDetail { ShopCode = a.ShopCode }
                                           )
                                              .Union(from b in db.QMS004
                                                     where b.WelderPSNo.Equals(objSrcWQR005.WelderName, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany != null && b.ActiveWithCompany == true
                                                     select new ModelWelderDetail { ShopCode = b.ShopCode }).FirstOrDefault();

                WQR005 objDestWQR005 = new WQR005();
                objDestWQR005.Shop = lstModelWelderDetail != null ? lstModelWelderDetail.ShopCode : "";
                var objWQR003 = db.WQR003.Where(i => i.WQTNo == objDestWQR005.WQTNo).FirstOrDefault();
                if (objWQR003 != null)
                {
                    objWQR003.CertificateGenerated = true;
                    objWQR003.CertificateGeneratedBy = objClsLoginInfo.UserName;
                    objWQR003.CertificateGeneratedOn = DateTime.Now;
                    objDestWQR005.FNo = objWQR003.FNo;
                    objDestWQR005.PNo = objWQR003.PNo;
                }
                else
                {
                    objDestWQR005.FNo = "NA";
                    objDestWQR005.PNo = "NA";
                }
                objDestWQR005.Location = objSrcWQR005.Location;
                objDestWQR005.WQTNo = destWQTNo;
                objDestWQR005.WelderName = objSrcWQR005.WelderName;
                objDestWQR005.WelderStampNo = objSrcWQR005.WelderStampNo;
                objDestWQR005.WeldingType = objSrcWQR005.WeldingType;
                objDestWQR005.DateOfWelding = objSrcWQR005.DateOfWelding;
                objDestWQR005.WPSNo = objSrcWQR005.WPSNo;
                objDestWQR005.TypeOfTestCoupon = objSrcWQR005.TypeOfTestCoupon;
                objDestWQR005.SpecificationTypeGrade = objSrcWQR005.SpecificationTypeGrade;
                objDestWQR005.TestCoupon = objSrcWQR005.TestCoupon;
                objDestWQR005.ProductionWeld = objSrcWQR005.ProductionWeld;
                objDestWQR005.Position = objSrcWQR005.Position;
                objDestWQR005.ThicknessMm = objSrcWQR005.ThicknessMm;
                objDestWQR005.JointType = objSrcWQR005.JointType;

                objDestWQR005.TypeOfWeldingAutomaticActual = objSrcWQR005.TypeOfWeldingAutomaticActual;
                objDestWQR005.TypeOfWeldingAutomaticQualified = objSrcWQR005.TypeOfWeldingAutomaticQualified;
                objDestWQR005.WeldingProcessEsQualified = objSrcWQR005.WeldingProcessEsQualified;
                objDestWQR005.WeldingProcessEsActual = objSrcWQR005.WeldingProcessEsActual;
                objDestWQR005.WeldingProcessMachineActual = objSrcWQR005.WeldingProcessMachineActual;
                objDestWQR005.WeldingProcessMachineQualified = objSrcWQR005.WeldingProcessMachineQualified;
                objDestWQR005.FillerMetalUsedEBWLBWActual = objSrcWQR005.FillerMetalUsedEBWLBWActual;
                objDestWQR005.FillerMetalUsedEBWLBWQualified = objSrcWQR005.FillerMetalUsedEBWLBWQualified;
                objDestWQR005.TypeOfLaserForLBWCO2ToYAGEtcActual = objSrcWQR005.TypeOfLaserForLBWCO2ToYAGEtcActual;
                objDestWQR005.TypeOfLaserForLBWCO2ToYAGEtcQualified = objSrcWQR005.TypeOfLaserForLBWCO2ToYAGEtcQualified;
                objDestWQR005.ContinuousDriveOrInertiaWeldingFWActual = objSrcWQR005.ContinuousDriveOrInertiaWeldingFWActual;
                objDestWQR005.ContinuousDriveOrInertiaWeldingFWQualified = objSrcWQR005.ContinuousDriveOrInertiaWeldingFWQualified;
                objDestWQR005.VacuumOrOutOfVacuumEBWActual = objSrcWQR005.VacuumOrOutOfVacuumEBWActual;
                objDestWQR005.VacuumOrOutOfVacuumEBWQualified = objSrcWQR005.VacuumOrOutOfVacuumEBWQualified;
                objDestWQR005.TypeOfWeldingMachineActual = objSrcWQR005.TypeOfWeldingMachineActual;
                objDestWQR005.TypeOfWeldingMachineQualified = objSrcWQR005.TypeOfWeldingMachineQualified;
                objDestWQR005.PlatePipeEnterDiaIfPipeOrTubeActual = objSrcWQR005.PlatePipeEnterDiaIfPipeOrTubeActual;
                objDestWQR005.PlatePipeEnterDiaIfPipeOrTubeActualTo = objSrcWQR005.PlatePipeEnterDiaIfPipeOrTubeActualTo;
                objDestWQR005.PlatePipeEnterDiaIfPipeOrTubeQualified = objSrcWQR005.PlatePipeEnterDiaIfPipeOrTubeQualified;
                objDestWQR005.DirectOrRemoteVisualControlActual = objSrcWQR005.DirectOrRemoteVisualControlActual;
                objDestWQR005.DirectOrRemoteVisualControlQualified = objSrcWQR005.DirectOrRemoteVisualControlQualified;
                objDestWQR005.AutomaticArcVoltageControlActual = objSrcWQR005.AutomaticArcVoltageControlActual;
                objDestWQR005.AutomaticArcVoltageControlQualified = objSrcWQR005.AutomaticArcVoltageControlQualified;
                objDestWQR005.AutomaticJointTrackingActual = objSrcWQR005.AutomaticJointTrackingActual;
                objDestWQR005.AutomaticJointTrackingQualified = objSrcWQR005.AutomaticJointTrackingQualified;
                objDestWQR005.WeldingPositionActual = objSrcWQR005.WeldingPositionActual;
                objDestWQR005.CircWeldOvly = objSrcWQR005.CircWeldOvly;
                objDestWQR005.IPlatePipeOverlayWeldingPositionQualified = objSrcWQR005.IPlatePipeOverlayWeldingPositionQualified;
                objDestWQR005.IPlatePipeFilletWeldingPositionQualified = objSrcWQR005.IPlatePipeFilletWeldingPositionQualified;
                objDestWQR005.IiPipeOverlayWeldingPositionQualified = objSrcWQR005.IiPipeOverlayWeldingPositionQualified;
                objDestWQR005.IiPipeFilletWeldingPositionQualified = objSrcWQR005.IiPipeFilletWeldingPositionQualified;
                objDestWQR005.ConsumableInsertActual = objSrcWQR005.ConsumableInsertActual;
                objDestWQR005.ConsumableInsertQualified = objSrcWQR005.ConsumableInsertQualified;
                objDestWQR005.BackingActual = objSrcWQR005.BackingActual;
                objDestWQR005.BackingQualified = objSrcWQR005.BackingQualified;
                objDestWQR005.SingleOrMultiplePassesPerSideActual = objSrcWQR005.SingleOrMultiplePassesPerSideActual;
                objDestWQR005.SingleOrMultiplePassesPerSideQualified = objSrcWQR005.SingleOrMultiplePassesPerSideQualified;
                objDestWQR005.Other = objSrcWQR005.Other;

                objDestWQR005.VisualExamOfCompWeld = objSrcWQR005.VisualExamOfCompWeld;
                objDestWQR005.LgRootFaceBend = objSrcWQR005.LgRootFaceBend;
                objDestWQR005.AltVolExam = objSrcWQR005.AltVolExam;
                objDestWQR005.VisualResult = objSrcWQR005.VisualResult;
                objDestWQR005.TrRootFace = objSrcWQR005.TrRootFace;
                objDestWQR005.UT = objSrcWQR005.UT;
                objDestWQR005.TrSideBend = objSrcWQR005.TrSideBend;
                objDestWQR005.LgRootResult = objSrcWQR005.LgRootResult;
                objDestWQR005.RT = objSrcWQR005.RT;
                objDestWQR005.AltVolResult = objSrcWQR005.AltVolResult;
                objDestWQR005.GrooveReport = objSrcWQR005.GrooveReport;
                objDestWQR005.Fillet = objSrcWQR005.Fillet;
                objDestWQR005.FilletResult = objSrcWQR005.FilletResult;
                objDestWQR005.FilletWeldsInPlate = objSrcWQR005.FilletWeldsInPlate;
                objDestWQR005.FilletWeldFractureTest = objSrcWQR005.FilletWeldFractureTest;
                objDestWQR005.MacroExamination = objSrcWQR005.MacroExamination;
                objDestWQR005.FilletReportNo = objSrcWQR005.FilletReportNo;
                objDestWQR005.FilletWeldsInPipe = objSrcWQR005.FilletWeldsInPipe;
                objDestWQR005.LengthAndPercentOfDefect = objSrcWQR005.LengthAndPercentOfDefect;
                objDestWQR005.FilletSizeMm = objSrcWQR005.FilletSizeMm;
                objDestWQR005.ConcavityConvexityMm = objSrcWQR005.ConcavityConvexityMm;
                objDestWQR005.CorrosionResistantOverlay = objSrcWQR005.CorrosionResistantOverlay;
                objDestWQR005.PipeBend = objSrcWQR005.PipeBend;
                objDestWQR005.PlateBend = objSrcWQR005.PlateBend;
                objDestWQR005.CorrosionResult = objSrcWQR005.CorrosionResult;
                objDestWQR005.CorrosionReportNo = objSrcWQR005.CorrosionReportNo;
                objDestWQR005.HardFacingOverlay = objSrcWQR005.HardFacingOverlay;
                objDestWQR005.MacroTestForPipe = objSrcWQR005.MacroTestForPipe;
                objDestWQR005.MacroTestForPlate = objSrcWQR005.MacroTestForPlate;
                objDestWQR005.LPExamination = objSrcWQR005.LPExamination;
                objDestWQR005.HardFacingReport = objSrcWQR005.HardFacingReport;
                objDestWQR005.TypeOfTest1 = objSrcWQR005.TypeOfTest1;
                objDestWQR005.Result1 = objSrcWQR005.Result1;
                objDestWQR005.ReportNo1 = objSrcWQR005.ReportNo1;
                objDestWQR005.TypeOfTest2 = objSrcWQR005.TypeOfTest2;
                objDestWQR005.Result2 = objSrcWQR005.Result2;
                objDestWQR005.ReportNo2 = objSrcWQR005.ReportNo2;
                objDestWQR005.TypeOfTest3 = objSrcWQR005.TypeOfTest3;
                objDestWQR005.Result3 = objSrcWQR005.Result3;
                objDestWQR005.ReportNo3 = objSrcWQR005.ReportNo3;
                objDestWQR005.TypeOfTest4 = objSrcWQR005.TypeOfTest4;
                objDestWQR005.Result4 = objSrcWQR005.Result4;
                objDestWQR005.ReportNo4 = objSrcWQR005.ReportNo4;
                objDestWQR005.TypeOfTest5 = objSrcWQR005.TypeOfTest5;
                objDestWQR005.Result5 = objSrcWQR005.Result5;
                objDestWQR005.ReportNo5 = objSrcWQR005.ReportNo5;
                objDestWQR005.TypeOfTest6 = objSrcWQR005.TypeOfTest6;
                objDestWQR005.Result6 = objSrcWQR005.Result6;
                objDestWQR005.ReportNo6 = objSrcWQR005.ReportNo6;
                objDestWQR005.FilmOrSpecimenEvaluatedBy = objSrcWQR005.FilmOrSpecimenEvaluatedBy;
                objDestWQR005.MechanicalTestsConductedBy = objSrcWQR005.MechanicalTestsConductedBy;
                objDestWQR005.WeldingSupervisedBy = objSrcWQR005.WeldingSupervisedBy;
                objDestWQR005.Company = objSrcWQR005.Company;
                objDestWQR005.LabTestNo = objSrcWQR005.LabTestNo;
                objDestWQR005.InspectionByAndDescription = objSrcWQR005.InspectionByAndDescription;
                objDestWQR005.Division1 = objSrcWQR005.Division1;
                objDestWQR005.Division2 = objSrcWQR005.Division2;
                objDestWQR005.Division3 = objSrcWQR005.Division3;
                objDestWQR005.LastWeldDate = objSrcWQR005.LastWeldDate;
                objDestWQR005.Remarks = objSrcWQR005.Remarks;
                objDestWQR005.ValueUpto = objSrcWQR005.ValueUpto;
                objDestWQR005.MaintainedBy = objClsLoginInfo.UserName;
                objDestWQR005.MaintainedOn = DateTime.Now;
                objDestWQR005.CreatedBy = objClsLoginInfo.UserName;
                objDestWQR005.CreatedOn = DateTime.Now;

                db.WQR005.Add(objDestWQR005);
                db.SaveChanges();
                objResponseMsg.HeaderId = objDestWQR005.Id;
                objResponseMsg.wqtno = objDestWQR005.WQTNo;
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Record copied successfully!";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return objResponseMsg;
        }

        public clsHelper.ResponseMsgWithStatus CopyTTSCertificate(WQR006 objSrcWQR006, string destWQTNo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WQR006 objWQR6 = new WQR006();

            try
            {

                var lstModelWelderDetail = (from a in db.QMS003
                                            where a.WelderPSNo.Equals(objSrcWQR006.WelderName, StringComparison.OrdinalIgnoreCase)
                                            select new ModelWelderDetail { ShopCode = a.ShopCode }
                                      )
                                         .Union(from b in db.QMS004
                                                where b.WelderPSNo.Equals(objSrcWQR006.WelderName, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany != null && b.ActiveWithCompany == true
                                                select new ModelWelderDetail { ShopCode = b.ShopCode }).FirstOrDefault();

                WQR003 objWQR003 = db.WQR003.Where(i => i.WQTNo == objSrcWQR006.WQTNo).FirstOrDefault();
                if (objWQR003 != null)
                {
                    objWQR003.CertificateGenerated = true;
                    objWQR003.CertificateGeneratedBy = objClsLoginInfo.UserName;
                    objWQR003.CertificateGeneratedOn = DateTime.Now;
                }
                // objWQR003.CertificateGenerated = true;
                objWQR6.Shop = (lstModelWelderDetail != null) ? lstModelWelderDetail.ShopCode : "";
                objWQR6.Location = objSrcWQR006.Location;
                objWQR6.WQTNo = destWQTNo;
                objWQR6.WelderName = objSrcWQR006.WelderName;
                objWQR6.WelderStampNo = objSrcWQR006.WelderStampNo;
                objWQR6.JoinType = objSrcWQR006.JoinType;
                objWQR6.WeldingType = objSrcWQR006.WeldingType;
                objWQR6.WPSNo = objSrcWQR006.WPSNo;
                objWQR6.FillerMetalAWS = objSrcWQR006.FillerMetalAWS;
                objWQR6.TestCoupon = objSrcWQR006.TestCoupon;
                objWQR6.ProductionWeld = objSrcWQR006.ProductionWeld;
                objWQR6.DateOfWelding = objSrcWQR006.DateOfWelding;
                objWQR6.SpecificationTypeGrade = objSrcWQR006.SpecificationTypeGrade;
                objWQR6.FillerMetalAWS = objSrcWQR006.FillerMetalAWS;

                objWQR6.WeldingProcessEsActual = objSrcWQR006.WeldingProcessEsActual;
                objWQR6.WeldingProcessEsQualified = objSrcWQR006.WeldingProcessEsQualified;
                objWQR6.WeldJointConfigurationPreplacedFillerMetal = objSrcWQR006.WeldJointConfigurationPreplacedFillerMetal;
                objWQR6.WeldJointConfigurationPreplacedFillerMetalQualified = objSrcWQR006.WeldJointConfigurationPreplacedFillerMetalQualified;
                objWQR6.WeldingTypeActual = objSrcWQR006.WeldingTypeActual;
                objWQR6.WeldingTypeQualified = objSrcWQR006.WeldingTypeQualified;
                objWQR6.JointConfigurationActual = objSrcWQR006.JointConfigurationActual;
                objWQR6.JointConfigurationQualified = objSrcWQR006.JointConfigurationQualified;
                objWQR6.DepthOfGrooveMmActual = objSrcWQR006.DepthOfGrooveMmActual;
                objWQR6.DepthOfGrooveMmQualified = objSrcWQR006.DepthOfGrooveMmQualified;
                objWQR6.GrooveAngleRadiusActualFrom = objSrcWQR006.GrooveAngleRadiusActualFrom;
                objWQR6.GrooveRadiusAngleQualified = objSrcWQR006.GrooveRadiusAngleQualified;
                objWQR6.TubeWallThicknessMmActual = objSrcWQR006.TubeWallThicknessMmActual;
                objWQR6.TubeWallThicknessMmQualified = objSrcWQR006.TubeWallThicknessMmQualified;
                objWQR6.TubeOutsideDiameterActual = objSrcWQR006.TubeOutsideDiameterActual;
                objWQR6.TubeOutsideDiameterQualified = objSrcWQR006.TubeOutsideDiameterQualified;
                objWQR6.PitchMmActual = objSrcWQR006.PitchMmActual;
                objWQR6.PitchMmQualified = objSrcWQR006.PitchMmQualified;
                objWQR6.LigamentMmActual = objSrcWQR006.LigamentMmActual;
                objWQR6.LigamentMmQualified = objSrcWQR006.LigamentMmQualified;
                objWQR6.TubeProjectionMmActual = objSrcWQR006.TubeProjectionMmActual;
                objWQR6.TubeProjectionMmQualified = objSrcWQR006.TubeProjectionMmQualified;
                objWQR6.SinglePassMultipassActual = objSrcWQR006.SinglePassMultipassActual;
                objWQR6.SinglePassMultpiassQualified = objSrcWQR006.SinglePassMultpiassQualified;
                objWQR6.WeldingPositionActual = objSrcWQR006.WeldingPositionActual;
                objWQR6.WeldingPositionQualified = objSrcWQR006.WeldingPositionQualified;
                objWQR6.VerticalProgressionActual = objSrcWQR006.VerticalProgressionActual;
                objWQR6.VerticalProgressionQualified = objSrcWQR006.VerticalProgressionQualified;
                objWQR6.TubeMaterialPNoActual = objSrcWQR006.TubeMaterialPNoActual;
                objWQR6.TubeMaterialPNoQualified = objSrcWQR006.TubeMaterialPNoQualified;
                objWQR6.TubesheetMaterialPNoIfTubesheetIsPartOfWeldActual = objSrcWQR006.TubesheetMaterialPNoIfTubesheetIsPartOfWeldActual;
                objWQR6.TubesheetMaterialPNoIfTubesheetIsPartOfWeldQualified = objSrcWQR006.TubesheetMaterialPNoIfTubesheetIsPartOfWeldQualified;
                objWQR6.TubesheetCladdingMaterialPNoIfCladdingIsPartOfWeld = objSrcWQR006.TubesheetCladdingMaterialPNoIfCladdingIsPartOfWeld;
                objWQR6.TubesheetCladdingMatlANoIfCladdingIsPartOfWeldActual = objSrcWQR006.TubesheetCladdingMatlANoIfCladdingIsPartOfWeldActual;
                objWQR6.TubesheetCladdingMatlANoIfCladdingIsPartOfWeldQualified = objSrcWQR006.TubesheetCladdingMatlANoIfCladdingIsPartOfWeldQualified;
                objWQR6.TubesheetCladdingMaterialPNoIfCladdingIsPartOfWeldQualified = objSrcWQR006.TubesheetCladdingMaterialPNoIfCladdingIsPartOfWeldQualified;
                objWQR6.Others = objSrcWQR006.Others;

                objWQR6.ANoOfWeldDepositIfFillerMetalIsAddedActual = objSrcWQR006.ANoOfWeldDepositIfFillerMetalIsAddedActual;
                objWQR6.ANoOfWeldDepositIfFillerMetalIsAddedQualified = objSrcWQR006.ANoOfWeldDepositIfFillerMetalIsAddedQualified;
                objWQR6.PreheatTempMinActual = objSrcWQR006.PreheatTempMinActual;
                objWQR6.PreheatTempMinQualified = objSrcWQR006.PreheatTempMinQualified;
                objWQR6.InterpassTempMaxActual = objSrcWQR006.InterpassTempMaxActual;
                objWQR6.InterpassTempMaxQualified = objSrcWQR006.InterpassTempMaxQualified;
                objWQR6.PWHTActual = objSrcWQR006.PWHTActual;
                objWQR6.PWHTQualified = objSrcWQR006.PWHTQualified;
                objWQR6.CurrentTypeActual = objSrcWQR006.CurrentTypeActual;
                objWQR6.CurrentTypeQualified = objSrcWQR006.CurrentTypeQualified;
                objWQR6.CurrentPolarityActual = objSrcWQR006.CurrentPolarityActual;
                objWQR6.CurrentPolarityQualified = objSrcWQR006.CurrentPolarityQualified;
                objWQR6.FillerMetalElectrodeDiaMmActual = objSrcWQR006.FillerMetalElectrodeDiaMmActual;
                objWQR6.FillerMetalElectrodeDiaMmQualified = objSrcWQR006.FillerMetalElectrodeDiaMmQualified;
                objWQR6.CurrentAActual = objSrcWQR006.CurrentAActual;
                objWQR6.CurrentAQualified = objSrcWQR006.CurrentAQualified;
                objWQR6.FNoActual = objSrcWQR006.FNoActual;
                objWQR6.FNoQualified = objSrcWQR006.FNoQualified;
                objWQR6.FillerMetalElectrodeDiaMmActual1 = objSrcWQR006.FillerMetalElectrodeDiaMmActual1;
                objWQR6.FillerMetalElectrodeDiaMmQualified1 = objSrcWQR006.FillerMetalElectrodeDiaMmQualified1;
                objWQR6.CurrentAActual1 = objSrcWQR006.CurrentAActual1;
                objWQR6.CurrentAQualified2 = objSrcWQR006.CurrentAQualified2;
                objWQR6.FNoActual1 = objSrcWQR006.FNoActual1;
                objWQR6.FNoQualified1 = objSrcWQR006.FNoQualified1;
                objWQR6.TubeExpansionPriorToWeldingActual = objSrcWQR006.TubeExpansionPriorToWeldingActual;
                objWQR6.TubeExpansionPriorToWeldingQualified = objSrcWQR006.TubeExpansionPriorToWeldingQualified;
                objWQR6.MethodOfCleaningActual = objSrcWQR006.MethodOfCleaningActual;
                objWQR6.MethodOfCleaningQualified = objSrcWQR006.MethodOfCleaningQualified;
                objWQR6.SizeShapeOfPreplacedMetalMmActual = objSrcWQR006.SizeShapeOfPreplacedMetalMmActual;
                objWQR6.SizeShapeOfPreplacedMetalMmQualified = objSrcWQR006.SizeShapeOfPreplacedMetalMmQualified;
                objWQR6.ShieldingGasEsActual = objSrcWQR006.ShieldingGasEsActual;
                objWQR6.ShieldingGasEsQualified = objSrcWQR006.ShieldingGasEsQualified;
                objWQR6.FlowRateOfMinorGasConstituentLMinActual = objSrcWQR006.FlowRateOfMinorGasConstituentLMinActual;
                objWQR6.FlowRateOfMinorGasConstituentLMinQualified = objSrcWQR006.FlowRateOfMinorGasConstituentLMinQualified;
                objWQR6.FillerMetalGTAWPAWActual = objSrcWQR006.FillerMetalGTAWPAWActual;
                objWQR6.FillerMetalGTAWPAWQualified = objSrcWQR006.FillerMetalGTAWPAWQualified;
                objWQR6.AuxiliaryGasShieldActual = objSrcWQR006.AuxiliaryGasShieldActual;
                objWQR6.AuxiliaryGasShieldQualified = objSrcWQR006.AuxiliaryGasShieldQualified;
                objWQR6.AutomaticArcVoltageControlActual = objSrcWQR006.AutomaticArcVoltageControlActual;
                objWQR6.AutomaticArcVoltageControlQualified = objSrcWQR006.AutomaticArcVoltageControlQualified;

                objWQR6.CompleteFusionObserved = objSrcWQR006.CompleteFusionObserved;
                objWQR6.NoBurnThroughInTubeWall = objSrcWQR006.NoBurnThroughInTubeWall;
                objWQR6.NoCracks = objSrcWQR006.NoCracks;
                objWQR6.NoPorosity = objSrcWQR006.NoPorosity;
                objWQR6.NoPorosityResult = objSrcWQR006.NoPorosityResult;
                objWQR6.NoPorosityReportNo = objSrcWQR006.NoPorosityReportNo;
                objWQR6.LiquidPenetrationResult = objSrcWQR006.LiquidPenetrationResult;
                objWQR6.LiquidPenetrationReportNo = objSrcWQR006.LiquidPenetrationReportNo;
                objWQR6.Required = objSrcWQR006.Required;
                objWQR6.ObservedMin = objSrcWQR006.ObservedMin;
                objWQR6.ObservedMax = objSrcWQR006.ObservedMax;
                objWQR6.BCrackSObserved = objSrcWQR006.BCrackSObserved;
                objWQR6.CCompleteFusion = objSrcWQR006.CCompleteFusion;
                objWQR6.DCompletePenetration = objSrcWQR006.DCompletePenetration;
                objWQR6.Result = objSrcWQR006.Result;
                objWQR6.ReportNo = objSrcWQR006.ReportNo;
                objWQR6.TypeOfTest1 = objSrcWQR006.TypeOfTest1;
                objWQR6.TypeOfTestResult1 = objSrcWQR006.TypeOfTestResult1;
                objWQR6.TypeOfTestReportNo1 = objSrcWQR006.TypeOfTestReportNo1;
                objWQR6.TypeOfTest2 = objSrcWQR006.TypeOfTest2;
                objWQR6.TypeOfTestResult2 = objSrcWQR006.TypeOfTestResult2;
                objWQR6.TypeOfTestReportNo2 = objSrcWQR006.TypeOfTestReportNo2;
                objWQR6.TypeOfTest3 = objSrcWQR006.TypeOfTest3;
                objWQR6.TypeOfTestResult3 = objSrcWQR006.TypeOfTestResult3;
                objWQR6.TypeOfTestReportNo3 = objSrcWQR006.TypeOfTestReportNo3;
                objWQR6.FilmOrSpecimenEvaluatedBy = objSrcWQR006.FilmOrSpecimenEvaluatedBy;
                objWQR6.MechanicalTestsConductedBy = objSrcWQR006.MechanicalTestsConductedBy;
                objWQR6.WeldingSupervisedBy = objSrcWQR006.WeldingSupervisedBy;
                objWQR6.Company = objSrcWQR006.Company;
                objWQR6.LabTestNo = objSrcWQR006.LabTestNo;
                objWQR6.InspectionByAndDescription = objSrcWQR006.InspectionByAndDescription;
                objWQR6.Division1 = objSrcWQR006.Division1;
                objWQR6.Division2 = objSrcWQR006.Division2;
                objWQR6.Division3 = objSrcWQR006.Division3;
                objWQR6.MacroResult = objSrcWQR006.MacroResult;
                objWQR6.MacroReportNo = objSrcWQR006.MacroReportNo;
                objWQR6.LastWeldDate = objSrcWQR006.LastWeldDate;
                objWQR6.Remarks = objSrcWQR006.Remarks;
                objWQR6.ValueUpto = objSrcWQR006.ValueUpto;
                objWQR6.CreatedBy = objClsLoginInfo.UserName;
                objWQR6.CreatedOn = DateTime.Now;
                objWQR6.MaintainedBy = objClsLoginInfo.UserName;
                objWQR6.MaintainedOn = DateTime.Now;
                db.WQR006.Add(objWQR6);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Record copied successfully!";
                objResponseMsg.HeaderId = objWQR6.Id;
                objResponseMsg.wqtno = objWQR6.WQTNo;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return objResponseMsg;
        }

        #endregion

        [HttpPost]
        public JsonResult GetAllProjects(string term)
        {
            dynamic lstProjects;

            if (!string.IsNullOrWhiteSpace(term))
            {
                lstProjects = (from c1 in db.COM001
                               where c1.t_dsca.Contains(term) || c1.t_cprj.Contains(term)
                               select new Projects
                               {
                                   Value = c1.t_cprj,
                                   Text = c1.t_cprj + " - " + c1.t_dsca
                               }).Distinct().ToList();
            }
            else
            {
                lstProjects = (from c1 in db.COM001
                               select new Projects
                               {
                                   Value = c1.t_cprj,
                                   Text = c1.t_cprj + " - " + c1.t_dsca
                               }).Distinct().Take(10).ToList();
            }

            return Json(lstProjects, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetIntExtWelderDetail(string term, string location)
        {
            List<ModelWelderDetail> lst = new List<ModelWelderDetail>();
            List<ModelWelderDetail> lstModelWelderDetail = new List<ModelWelderDetail>();
            if (!string.IsNullOrEmpty(term))
            {
                var listWelders = (from a in db.QMS003
                                   where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                                   select new ModelWelderDetail { WelderPSNo = a.WelderPSNo, WelderName = string.IsNullOrEmpty(a.Name) ? "" : a.Name, ShopCode = a.ShopCode }
                                      )
                                 .Union(from b in db.QMS004
                                        where b.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany == true //&& (b.Name.ToLower().Contains(term.ToLower()) || b.WelderPSNo.Contains(term))
                                        select new ModelWelderDetail { WelderPSNo = b.WelderPSNo, WelderName = (string.IsNullOrEmpty(b.Name) ? "" : b.Name) + " " + (string.IsNullOrEmpty(b.FatherName) ? "" : b.FatherName) + " " + (string.IsNullOrEmpty(b.Surname) ? "" : b.Surname), ShopCode = b.ShopCode }).Select(i => new { WelderPSNo = i.WelderPSNo, WelderName = i.WelderName, ShopCode = i.ShopCode }).Distinct().ToList();
                lstModelWelderDetail = listWelders.Where(a => (a.WelderName.ToLower().Contains(term.ToLower()) || a.WelderPSNo.Contains(term))).Select(i => new ModelWelderDetail { WelderPSNo = i.WelderPSNo, WelderName = i.WelderName }).ToList();
            }
            else
            {
                lstModelWelderDetail = (from a in db.QMS003
                                        where a.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                                        select new ModelWelderDetail { WelderPSNo = a.WelderPSNo, WelderName = string.IsNullOrEmpty(a.Name) ? "" : a.Name, ShopCode = a.ShopCode }
                      )
                 .Union(from b in db.QMS004
                        where b.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && b.ActiveWithCompany == true
                        select new ModelWelderDetail { WelderPSNo = b.WelderPSNo, WelderName = (string.IsNullOrEmpty(b.Name) ? "" : b.Name) + " " + (string.IsNullOrEmpty(b.FatherName) ? "" : b.FatherName) + " " + (string.IsNullOrEmpty(b.Surname) ? "" : b.Surname), ShopCode = b.ShopCode }).ToList();
            }
            lst = lstModelWelderDetail.Select(i => new ModelWelderDetail { Code = i.WelderPSNo, Name = i.WelderPSNo + "-" + i.WelderName + "#" + i.ShopCode, WelderName = i.WelderName }).Take(10).ToList();

            return Json(lst, JsonRequestBehavior.AllowGet);
        }

        public class catdata : CategoryData
        {
            public string CatID { get; set; }
            public string CatDesc { get; set; }
        }
    }
}