﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.WQ.Models;

namespace IEMQS.Areas.WQ.Controllers
{
    public class ApproveWQRequestController : clsBase
    {
        // GET: WQ/ApproveWQRequest
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetAprovedWQRequestPartial(string status)
        {
            ViewBag.PTRStatus = clsImplementationEnum.getPassFail().ToArray();
            ViewBag.status = status;
            return PartialView("_GetAprovedWQRequestPartial");
        }

        public ActionResult LoadApprovedWQRequestDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                ModelRoleWiseIndex objModelRoleWiseIndex = new ModelRoleWiseIndex();

                string Role = RoleForWQ.WQTC.GetStringValue();
                List<ApproverModel> lstApproverModel = new List<ApproverModel>();

                int RoleId = db.ATH004.Where(x => x.Role == Role).Select(x => x.Id).FirstOrDefault();
                var lstApproverList = db.ATH001.
                        Join(db.COM003, x => x.Employee, y => y.t_psno,
                        (x, y) => new { x, y })
                        .Where(z => z.y.t_actv == 1 && z.y.t_psno == objClsLoginInfo.UserName && z.x.Role == RoleId)
                        .Distinct()
                        .Select(z => z.x.Location).Distinct().ToList();
                string location = string.Format("`{0}`", string.Join("`,`", lstApproverList)).Replace("'", "''").Replace("`", "'");
                //string.Join("','", lstApproverList);

                if (lstApproverList != null)
                {
                    if (lstApproverList.Any(x => x == "HZW"))
                    {
                        location = location + ",'LEM'";
                    }
                    whereCondition += " AND wqr1.Location in (" + location + ") and wqr1.WQTC='" + objClsLoginInfo.UserName + "'";
                }
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    //whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or Location like '%" + param.sSearch + "%' or wps10.WPSNumber like '%" + param.sSearch + "%' or WPSRevNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%' or wps10.CreatedBy like '%" + param.sSearch + "%')";
                    whereCondition += Helper.MakeDatatableSearchCondition(new string[]
                                      { "wqr3.RequestNo", "wqr3.Welder","wqr3.WelderStamp", "wqr1.QualificationProject","com1.t_dsca","wqr3.JointType","wqr3.WeldingProcess","wqr3.WeldType","wqr3.PNo","wqr3.WPSNo",
                                      "wqr3.WQTNo","wqr3.NDTStatus","wqr3.PTRStatus"}, param.sSearch);
                }
                if (status.ToLower() == "pending")
                {
                    whereCondition += " AND wqr3.CertificateGenerated=0";
                }
                //if (status.ToLower() == "all")
                //{
                //    whereCondition += " AND wqr3.CertificateGenerated=1";
                //}

                var lstHeader = db.SP_WQ_GET_APPROVEDWQREQUEST(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                ModelWQRequest objModel = new ModelWQRequest();
                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.Id),
                        h.Location,
                        h.RequestNo,
                        Convert.ToString(h.LineId),
                        objModel.GetWelderName(Convert.ToString(h.Welder),h.Location).ToString(),//h.Welder,
                        h.WelderStamp,
                        h.QualificationProject,
                        h.JointType,
                        h.WeldingProcess,
                        h.WeldType,
                        h.PNo,
                        h.FNo,
                        //Helper.GenerateTextbox(h.Id,"WPSNo",h.WPSNo,"Update(this,"+h.Id+")"),
                          Helper.GenerateTextbox(h.Id, "WPSNo",h.WPSNo,"Update(this, "+h.Id+");",false,"","25"),
                        // HTMLAutoComplete(h.Id,"txtWPSNo",h.WPSNo,"Update(this,"+h.Id+")",false,"","WPSNo")+" "+Helper.GenerateHidden(h.Id,"WPSNo",h.WPSNo),
                        h.WQTNo,
                        h.NDTStatus,
                         HTMLAutoComplete(h.Id,"PTRStatus",h.PTRStatus,"Update(this,"+h.Id+")",false,"","PTRStatus"),
                        //Helper.GenerateTextbox(h.Id,"PTRStatus",h.PTRStatus,"Update(this,"+h.Id+")"),
                        Convert.ToString(h.QualificationStatus),
                         Helper.GenerateActionIcon(h.HeaderId, "View", "View Detail", "fa fa-eye", "",WebsiteURL +"/WQ/ApproveWQRequest/ViewWQRequest?headerId="+h.HeaderId,false)
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult ViewWQRequest(int? headerId)
        {
            WQR001 objWQR001 = new WQR001();
            var location = objClsLoginInfo.Location;
            //var userRole = objClsLoginInfo.GetUserRoleList();
            ViewBag.ReqForLocation = db.WQR001.Where(i => i.Location.Equals(location)).Count();
            ModelWQRequest objModelWQRequest = new ModelWQRequest();
            if (headerId > 0)
            {
                objWQR001 = db.WQR001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                ViewBag.Location = db.WQR008.Where(x => x.Location == objWQR001.Location).Select(x => x.Location + "-" + x.Description).FirstOrDefault();
                ViewBag.WeldingProcess1 = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.WeldingProcess1, string.Empty, location).CategoryDescription;
                ViewBag.WeldingType1 = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.WeldingType1, string.Empty, location).CategoryDescription;
                ViewBag.WeldingPosition1 = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.WeldingPosition1, string.Empty, location).CategoryDescription;
                ViewBag.WeldingProcess2 = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.WeldingProcess2, string.Empty, location).CategoryDescription;
                ViewBag.WeldingType2 = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.WeldingType2, string.Empty, location).CategoryDescription;
                ViewBag.WeldingPosition2 = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.WeldingPosition2, string.Empty, location).CategoryDescription;
                ViewBag.JointType = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.Jointtype, string.Empty, location).CategoryDescription;
                ViewBag.CouponType = objModelWQRequest.GetCategoryDescriptionLocationWise(objWQR001.CouponType, string.Empty, location).CategoryDescription;
                ViewBag.ShopInCharge = db.COM003.Where(i => i.t_psno.Equals(objWQR001.ShopInCharge, StringComparison.OrdinalIgnoreCase) && i.t_actv == 1).Select(i => i.t_psno + "-" + i.t_name).FirstOrDefault();
                ViewBag.WE = db.COM003.Where(i => i.t_psno.Equals(objWQR001.WE, StringComparison.OrdinalIgnoreCase) && i.t_actv == 1).Select(i => i.t_psno + "-" + i.t_name).FirstOrDefault();
                ViewBag.WQTC = db.COM003.Where(i => i.t_psno.Equals(objWQR001.WQTC, StringComparison.OrdinalIgnoreCase) && i.t_actv == 1).Select(i => i.t_psno + "-" + i.t_name).FirstOrDefault();

                if (!string.IsNullOrWhiteSpace(objWQR001.QualificationProject))
                {
                    ViewBag.QualificationProject = db.COM001.Where(i => i.t_cprj.Equals(objWQR001.QualificationProject, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
                    objWQR001.BU = db.COM002.Where(a => a.t_dtyp == 2 && a.t_dimx == objWQR001.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                }
                else
                {
                    ViewBag.QualificationProject = "";
                    objWQR001.BU = "";
                }
            }
            else
            {
            }

            return View(objWQR001);
        }


        public ActionResult LoadWQRWelderData(JQueryDataTableParamModel param, int refheaderId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;

                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "wps10.BU", "wps10.Location");

                if (refheaderId > 0)
                    whereCondition += " and wqr2.HeaderId=" + refheaderId + " ";

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    whereCondition += " and (w.Stamp like '%" + param.sSearch + "%' or w.ShopCode like '%" + param.sSearch + "%' or  w.ShopCode+'-'+  com2.t_desc " + param.sSearch + " or wqr2.Welder like '%" + param.sSearch + "%' )";
                    //whereCondition += Helper.MakeDatatableSearchCondition(new string[] { "w.Stamp,w.ShopCode,wqr2.Welder" }, param.sSearch);
                }

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                whereCondition += " AND (wqr2.acceptedByShop=1 And wqr2.Status Not In ('" + clsImplementationEnum.WQLinesStatus.RejectedByWeld.GetStringValue() + "') OR wqr2.acceptedByWeld=1 )";

                var lstHeader = db.SP_WQ_GET_WELDERDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                ModelWQRequest objModel = new ModelWQRequest();

                var res = (from h in lstHeader
                           select new[] {
                            Helper.GenerateHidden(h.LineId,"LineId",Convert.ToString(h.LineId)),
                            Helper.GenerateHidden(h.HeaderId,"HeaderId",Convert.ToString(h.HeaderId)),
                            Convert.ToString(h.ROW_NO),
                            objModel.GetWelderName(Convert.ToString(h.Welder),db.WQR001.Where(x=>x.HeaderId == h.HeaderId).Select(x=>x.Location).FirstOrDefault()).ToString(),
                            Convert.ToString(h.Stamp),
                            Convert.ToString(h.ShopCode),
                    }).ToList();


                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult UpdateWQ(int id, string colName, string colValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WQR003 objWQR003 = new WQR003();
            try
            {
                if (!string.IsNullOrEmpty(colName) && !string.IsNullOrEmpty(colValue))
                {
                    if (id > 0)
                    {
                        objWQR003 = db.WQR003.Where(i => i.Id == id).FirstOrDefault();
                        db.SP_WQ_UPDATE_APPROVEDWELDER(id, colName, colValue);
                        if (objWQR003 != null)
                        {
                            if (!string.IsNullOrEmpty(objWQR003.PTRStatus) && !string.IsNullOrEmpty(objWQR003.NDTStatus))
                            {
                                if ((objWQR003.PTRStatus.Equals(clsImplementationEnum.WQCertificateStatus.NA.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.IsNullOrEmpty(objWQR003.PTRStatus)) && objWQR003.NDTStatus.Equals(clsImplementationEnum.WQCertificateStatus.Pass.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                                {
                                    objWQR003.QualificationStatus = clsImplementationEnum.WQCertificateStatus.Pass.GetStringValue();
                                }
                                else if (objWQR003.PTRStatus.Equals(clsImplementationEnum.WQCertificateStatus.Pass.GetStringValue(), StringComparison.OrdinalIgnoreCase) && objWQR003.NDTStatus.Equals(clsImplementationEnum.WQCertificateStatus.Pass.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                                {
                                    objWQR003.QualificationStatus = clsImplementationEnum.WQCertificateStatus.Pass.GetStringValue();
                                }
                                else if (objWQR003.PTRStatus.Equals(clsImplementationEnum.WQCertificateStatus.Fail.GetStringValue(), StringComparison.OrdinalIgnoreCase) && objWQR003.NDTStatus.Equals(clsImplementationEnum.WQCertificateStatus.Pass.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                                {
                                    objWQR003.QualificationStatus = clsImplementationEnum.WQCertificateStatus.Fail.GetStringValue();
                                }
                            }
                            db.SaveChanges();
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Updated Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult CopyApprovedWelder(int id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WQR003 objWQR003 = new WQR003();
            try
            {
                if (id > 0)
                {
                    objWQR003 = db.WQR003.Where(i => i.Id == id).FirstOrDefault();
                    var wqr2 = db.WQR002.Where(i => i.LineId == objWQR003.LineId).FirstOrDefault();
                    var wqr1 = db.WQR001.Where(i => i.HeaderId == wqr2.HeaderId).FirstOrDefault();
                    WQR002 objWQR002 = new WQR002();
                    objWQR002.HeaderId = wqr2.HeaderId;
                    objWQR002.Welder = objWQR003.Welder;
                    objWQR002.acceptedByShop = wqr2.acceptedByShop;
                    objWQR002.acceptedByWeld = wqr2.acceptedByWeld;
                    objWQR002.Status = wqr2.Status;
                    objWQR002.CreatedBy = objClsLoginInfo.UserName;
                    objWQR002.CreatedOn = DateTime.Now;
                    db.WQR002.Add(objWQR002);
                    db.SaveChanges();
                    db.WQR003.Add(new WQR003
                    {
                        RequestNo = objWQR003.RequestNo,
                        LineId = objWQR002.LineId,
                        Welder = objWQR002.Welder,
                        WelderStamp = new ModelWQRequest().GetIntExtWelders(wqr1.Location).Where(i => i.WelderPSNo.Equals(objWQR002.Welder, StringComparison.OrdinalIgnoreCase)).FirstOrDefault().Stamp,
                        QualificationProject = objWQR003.QualificationProject,
                        JointType = objWQR003.JointType,
                        WeldingProcess = objWQR003.WeldingProcess,
                        WeldType = objWQR003.WeldType,
                        PNo = objWQR003.PNo,
                        WPSNo = objWQR003.WPSNo,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now
                    });
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Data Copied Successfully";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GenerateWQT(int id, string location)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            WQR003 objWQR003 = new WQR003();
            try
            {
                //int Id = Convert.ToInt32(id);
                if (id > 0)
                {
                    objWQR003 = db.WQR003.Where(i => i.Id == id).FirstOrDefault();
                    if (objWQR003.CertificateGenerated)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = string.Format("Certificate is already generated for {0} for process {1}", objWQR003.Welder, objWQR003.WeldingProcess);
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(objWQR003.WQTNo))
                        {
                            objWQR003.WQTNo = GenerateWQTNo(location);
                            objWQR003.WQTGenerated = DateTime.Now;
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "WQT No. Generated Successfully";
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "WQT No. Already Generated";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GenerateCertificate(int id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            WQR003 objWQR003 = new WQR003();
            try
            {
                if (id > 0)
                {
                    objWQR003 = db.WQR003.Where(i => i.Id == id).FirstOrDefault();
                    if (objWQR003 != null)
                    {
                        if (!string.IsNullOrEmpty(objWQR003.WQTNo))
                        {
                            if (objWQR003.NDTStatus == clsImplementationEnum.WQCertificateStatus.Pass.GetStringValue() && objWQR003.PTRStatus == clsImplementationEnum.WQCertificateStatus.Pass.GetStringValue())
                            {
                                objWQR003.QualificationStatus = clsImplementationEnum.WQCertificateStatus.Pass.GetStringValue();
                            }
                            db.SaveChanges();
                            if (objWQR003.CertificateGenerated == true)
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Certificate Is Already Generated";
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(objWQR003.QualificationStatus) && objWQR003.QualificationStatus.Equals(clsImplementationEnum.WQCertificateStatus.Pass.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                                {
                                    objResponseMsg.Key = true;
                                    objResponseMsg.wqtno = objWQR003.WQTNo;
                                }
                                else
                                {
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = "Certificate Cannot Be Generated With QualificationStatus Fail/Null";
                                }
                            }

                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Certificate Cannot Be Generated Without Generating WQT No.";
                            objResponseMsg.dataValue = objWQR003.Location;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GETWPSNo(string term)
        {
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            string strWPSStatus = clsImplementationEnum.WPSHeaderStatus.Approved.GetStringValue();
            if (!string.IsNullOrEmpty(term))
            {
                lstCategoryData = db.WPS010.Where(i => i.Status.Equals(strWPSStatus, StringComparison.OrdinalIgnoreCase) && i.WPSNumber.ToLower().Contains(term.ToLower())).Select(i => new CategoryData { Value = i.WPSNumber }).ToList();
            }
            else
            {
                lstCategoryData = db.WPS010.Where(i => i.Status.Equals(strWPSStatus, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.WPSNumber }).ToList();
            }
            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }
        public string GenerateWQTNo(string location)
        {
            string strRequestNo = string.Empty;
            var refc = (from a in db.WQR003
                        join b in db.WQR001 on a.RequestNo equals b.RequestNo
                        where b.Location.Equals(location, StringComparison.OrdinalIgnoreCase)
                        select a).ToList();
            //db.WQR003.Where(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase)).ToList();

            //var loc= db.WQR003.Where(i => i.WQTNo.Contains(location)).ToList()
            if (refc.Where(i => !string.IsNullOrEmpty(i.WQTNo)).Any())
            {
                var reqNo = refc.Where(i => !string.IsNullOrEmpty(i.WQTNo)).OrderByDescending(i => i.WQTNo).FirstOrDefault();
                int subloc = Convert.ToInt32(reqNo.WQTNo.Substring(location.Length + 1, (reqNo.WQTNo.Length - location.Length) - 1)) + 1;//reqNo.Substring("HZW".Length+1, reqNo.Length);
                strRequestNo = location + (subloc).ToString("D7");
                //string output = (reqNo.RequestNo.ToLower()).Contains(location.ToLower());//(0, reqNo.IndexOf("-")).Replace(" ", "");
            }
            else
            {
                int num = 1;
                strRequestNo = location + num.ToString("D7");
            }
            return strRequestNo;
        }

        public string HTMLAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "", string validate = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string FieldValidate = !string.IsNullOrEmpty(validate) ? "validate='" + validate + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " " + MaxCharcters + " " + FieldValidate + " />";

            return strAutoComplete;
        }

        #region Export Excel
        //Code By : nikita vibhandik 18/12/2017 (observation  14096)
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //Generate WQT grid
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    ModelWQRequest objModel = new ModelWQRequest();
                    var lst = db.SP_WQ_GET_APPROVEDWQREQUEST(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from h in lst
                                  select new
                                  {
                                      Location = h.Location,
                                      RequestNo = h.RequestNo,
                                      Welder = objModel.GetWelderName(Convert.ToString(h.Welder), h.Location).ToString(),//h.Welder,
                                      WelderStamp = h.WelderStamp,
                                      QualificationProject = h.QualificationProject,
                                      JointType = h.JointType,
                                      WeldingProcess = h.WeldingProcess,
                                      WeldType = h.WeldType,
                                      PNo = h.PNo,
                                      FNo = h.FNo,
                                      WPSNo = h.WPSNo,
                                      WQTNo = h.WQTNo,
                                      NDTStatus = h.NDTStatus,
                                      PTRStatus = h.PTRStatus,
                                      QualificationStatus = Convert.ToString(h.QualificationStatus),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}