﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;

namespace IEMQS.Areas.RLF.Controllers
{
    public class RollingFeasibilitySchafferController : clsBase
    {
        // GET: RLF/RollingFeasibilitySchaffer

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index(int HeaderId = 0)
        {
            RLF011 objRLF011 = new RLF011();
            if (HeaderId > 0)
                objRLF011 = db.RLF011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
            else
                objRLF011 = db.RLF011.FirstOrDefault();

            if (objRLF011 != null)
            {
                return RedirectToAction("RollingFeasibilitySchafferDetails", "RollingFeasibilitySchaffer", new { HeaderId = objRLF011.HeaderId });
            }
            return RedirectToAction("RollingFeasibilitySchafferDetails", "RollingFeasibilitySchaffer", new { HeaderId = 0 });
        }
       
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult RollingFeasibilitySchafferDetails(int HeaderId = 0)
        {
            RLF011 objRLF011 = new RLF011();
            ViewBag.MaterialType = db.RLF014.Select(x => new { CatID = x.HeaderId, CatDesc = x.Material }).ToList();

            if (HeaderId > 0)
            {
                objRLF011 = db.RLF011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objRLF011 != null)
                {
                    ViewBag.txtMaterialType = db.RLF014.Where(x => x.HeaderId == objRLF011.MaterialType).FirstOrDefault().Material;

                    //double? Diameter = 0;
                    //if (objRLF011.ShellDiameter > 4000)
                    //{
                    //    Diameter = objRLF011.ShellDiameter;
                    //}
                    //else
                    //{
                    //    if (objRLF011.ShellDiameter <= 4000 && objRLF011.ShellDiameter > 3000)
                    //    {
                    //        Diameter = 3000;
                    //    }
                    //    else
                    //    {
                    //        if (objRLF011.ShellDiameter <= 3000 && objRLF011.ShellDiameter > 2200)
                    //        {
                    //            Diameter = 2200;
                    //        }
                    //        else
                    //        {
                    //            if (objRLF011.ShellDiameter <= 2200 && objRLF011.ShellDiameter > 1800)
                    //                Diameter = 1800;
                                
                    //            else
                    //                Diameter = 4000;
                    //        }
                    //    }
                    //}
                    //ViewBag.txtDiameter = Diameter;
                }
                else
                {
                    return RedirectToAction("RollingFeasibilitySchafferDetails", "RollingFeasibilitySchaffer", new { HeaderId = 0 });
                }
            }
            return View(objRLF011);
        }

        [HttpPost]
        public ActionResult SaveRLFDetails(RLF011 model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                RLF011 objRLF011 = new RLF011();
                if (model != null)
                {
                    if (model.HeaderId > 0)
                    {
                        objRLF011 = db.RLF011.Where(x => x.HeaderId == model.HeaderId).FirstOrDefault();
                    }

                    objRLF011.JobDescription = model.JobDescription;
                    objRLF011.Customer = (!string.IsNullOrEmpty(model.Customer)) ? model.Customer : string.Empty;
                    objRLF011.EnquiryNumber = (!string.IsNullOrEmpty(model.EnquiryNumber)) ? model.EnquiryNumber : string.Empty;
                    objRLF011.EnquiryBy = (!string.IsNullOrEmpty(model.EnquiryBy)) ? model.EnquiryBy : string.Empty;
                    objRLF011.ShellDiameter = model.ShellDiameter;
                    objRLF011.MaterialType = model.MaterialType;
                    objRLF011.MaterialYieldStrength = model.MaterialYieldStrength;
                    objRLF011.UTS = model.UTS;
                    objRLF011.Thickness = model.Thickness;

                    if (model.HeaderId > 0)
                    {
                        objRLF011.EditedBy = objClsLoginInfo.UserName;
                        objRLF011.EditedOn = DateTime.Now;
                        db.SaveChanges();
                    }
                    else
                    {
                        objRLF011.CreatedBy = objClsLoginInfo.UserName;
                        objRLF011.CreatedOn = DateTime.Now;
                        db.RLF011.Add(objRLF011);
                        db.SaveChanges();
                    }
                    objResponseMsg.Key = true;
                    if (model.HeaderId > 0)
                        objResponseMsg.HeaderId = model.HeaderId;
                    
                    else
                        objResponseMsg.HeaderId = db.RLF011.Max(x => x.HeaderId);
                    
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        [HttpPost]
        public ActionResult GetRollingFeasibilitySchafferList(int HeaderId)
        {
            return PartialView("_RollingFeasibilitySchafferList");
        }

        public class SelectRLFList
        {
            public int ROW_NO { get; set; }
            public string ColdRolling { get; set; }
            public string WarmRolling { get; set; }
            public string HotRolling { get; set; }
        }

        public class SelectRLFListActual
        {
            public int ROW_NO { get; set; }
            public string ColumnName { get; set; }
            public string ColdRolling { get; set; }
            public string WarmRolling { get; set; }
            public string HotRolling { get; set; }
        }
        [HttpPost]
        public ActionResult LoadRLFSchafferActualLineData(JQueryDataTableParamModel param)
        {
            try
            {
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);

                List<SelectRLFListActual> lstResult = db.Database.SqlQuery<SelectRLFListActual>("SP_RLF_SCHAFFER_LINES @HeaderId", new SqlParameter("HeaderId", HeaderId)).ToList();
              
                var data = (from fx in lstResult
                            select new[]
                            {
                                "",
                                fx.ColumnName,
                                fx.ColdRolling,
                                fx.WarmRolling,
                                fx.HotRolling
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0) ? lstResult.Count : 0,
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.Count : 0),
                    aaData = data,
                    strSortOrder = "",
                    whereCondition = ""
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

        [HttpPost]
        public ActionResult LoadRLFSchafferLineData(JQueryDataTableParamModel param)
        {
            try
            {
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);
                RLF011 objRLF011 = db.RLF011.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                double? Thickness = 0;
                double? MaterialYieldStrength = 0;
                double? Diameter = 0;
                string Material = string.Empty;

                if (objRLF011 != null)
                {
                    Thickness = objRLF011.Thickness;
                    MaterialYieldStrength = objRLF011.MaterialYieldStrength;
                    Diameter = objRLF011.ShellDiameter;
                    Material = db.RLF014.Where(x => x.HeaderId == objRLF011.MaterialType).FirstOrDefault().Material;
                }

                List<SelectRLFList> lstResult = new List<SelectRLFList>();
                List<SelectRLFList> resultPre = db.Database.SqlQuery<SelectRLFList>("SP_RLF_SCHAFFER_GET_PRE_BENDING_LINES @HeaderId,@Thickness,@MaterialYieldStrength,@Diameter", new SqlParameter("HeaderId", HeaderId), new SqlParameter("Thickness", Thickness), new SqlParameter("MaterialYieldStrength", MaterialYieldStrength), new SqlParameter("Diameter", Diameter)).ToList();
                List<SelectRLFList> resultNormal = db.Database.SqlQuery<SelectRLFList>("SP_RLF_SCHAFFER_GET_NORMAL_BENDING_LINES @HeaderId,@Thickness,@MaterialYieldStrength,@Diameter,@Material", new SqlParameter("HeaderId", HeaderId), new SqlParameter("Thickness", Thickness), new SqlParameter("MaterialYieldStrength", MaterialYieldStrength), new SqlParameter("Diameter", Diameter), new SqlParameter("Material", Material)).ToList();
                lstResult.AddRange(resultPre);
                lstResult.AddRange(resultNormal);
                var data = (from fx in lstResult
                            select new[]
                            {
                                "",
                                (fx.ROW_NO == 1) ? "Pre-Bending" : (fx.ROW_NO == 13) ? "Normal-Bending" : (fx.ROW_NO == 3 || fx.ROW_NO == 15) ? "Yield Strength" :  (fx.ROW_NO == 6 || fx.ROW_NO == 18)  ? "Plate Thickness": (fx.ROW_NO == 9 || fx.ROW_NO == 21)  ? "Plate Width" : "<span class='child' ></span>",
                                (fx.ROW_NO == 1 || fx.ROW_NO == 13) ? "Final Yield Strength" : (fx.ROW_NO == 2 || fx.ROW_NO == 5 || fx.ROW_NO == 8 || fx.ROW_NO == 14 || fx.ROW_NO == 17 || fx.ROW_NO == 20) ? "Large Value" :  (fx.ROW_NO == 3 || fx.ROW_NO == 6 || fx.ROW_NO == 9 || fx.ROW_NO == 15 || fx.ROW_NO == 18 || fx.ROW_NO == 21) ? "Small Value" : (fx.ROW_NO == 7 || fx.ROW_NO == 10 || fx.ROW_NO == 19 || fx.ROW_NO == 22) ? "Difference"  :  (fx.ROW_NO == 11 || fx.ROW_NO == 23) ? "Ratio" : (fx.ROW_NO == 12 || fx.ROW_NO == 24) ? "Final Width" : (fx.ROW_NO == 4 || fx.ROW_NO == 16) ? "-" :  "<span class='child'></span>",
                                (fx.ROW_NO == 2 || fx.ROW_NO == 3 || fx.ROW_NO == 4 || fx.ROW_NO == 14 || fx.ROW_NO == 15 || fx.ROW_NO == 16 || fx.ROW_NO == 5 || fx.ROW_NO == 6 || fx.ROW_NO == 7 || fx.ROW_NO == 17 || fx.ROW_NO == 18 || fx.ROW_NO == 19) ? (fx.ColdRolling != "0"? fx.ColdRolling :"") : (fx.ROW_NO == 12 || fx.ROW_NO == 24) ? (fx.ColdRolling == "0"? "Not Possible" : fx.ColdRolling) : fx.ColdRolling,
                                (fx.ROW_NO == 2 || fx.ROW_NO == 3 || fx.ROW_NO == 4 || fx.ROW_NO == 14 || fx.ROW_NO == 15 || fx.ROW_NO == 16 || fx.ROW_NO == 5 || fx.ROW_NO == 6 || fx.ROW_NO == 7 || fx.ROW_NO == 17 || fx.ROW_NO == 18 || fx.ROW_NO == 19) ? (fx.WarmRolling != "0"? fx.WarmRolling :"") : (fx.ROW_NO == 12 || fx.ROW_NO == 24) ? (fx.WarmRolling == "0"? "Not Possible" : fx.WarmRolling) : fx.WarmRolling,
                                (fx.ROW_NO == 2 || fx.ROW_NO == 3 || fx.ROW_NO == 4 || fx.ROW_NO == 14 || fx.ROW_NO == 15 || fx.ROW_NO == 16 || fx.ROW_NO == 5 || fx.ROW_NO == 6 || fx.ROW_NO == 7 || fx.ROW_NO == 17 || fx.ROW_NO == 18 || fx.ROW_NO == 19) ? (fx.HotRolling != "0"? fx.HotRolling :"") : (fx.ROW_NO == 12 || fx.ROW_NO == 24) ? (fx.HotRolling == "0"? "Not Possible" : fx.HotRolling) : fx.HotRolling,
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0) ? lstResult.Count : 0,
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.Count : 0),
                    aaData = data,
                    strSortOrder = "",
                    whereCondition = ""
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }

    }
}
