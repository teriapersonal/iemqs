﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.RLF.Controllers
{
    public class RollingFeasibilityController : clsBase
    {
        #region Utility        
        public int MaterialCalculation(RLF001 objRLF001, RLF001 model)
        {
            int ShellID = model.ShellID;
            objRLF001.ShellStdID = null;
            string flag = "edit";
            #region Header Field Calculation
            if (ShellID == 1000)
            {
                objRLF001.ShellStdID = ShellID;
            }
            else
            {

                RLF004 objRLF004 = db.RLF004.Where(x => x.From < ShellID && ShellID <= x.To).FirstOrDefault();
                if (objRLF004 != null)
                {
                    objRLF001.ShellStdID = objRLF004.StandardValue;
                }
            }
            if (model.Thickness > 0)
            {
                objRLF001.FibreElongation = Math.Round(((model.Thickness * 100.00) / ShellID), 2);
            }
            objRLF001.TypeRollingFE = objRLF001.FibreElongation <= 5 ? "Cold Rolling" : objRLF001.FibreElongation > 5 && objRLF001.FibreElongation <= 9 ? "Warm Rolling" : objRLF001.FibreElongation > 9 ? "Hot Rolling" : null;

            List<RLF002> oldRLF002 = null;
            if (objRLF001.HeaderId == 0)
            {
                db.RLF001.Add(objRLF001);
                flag = "add";
            }
            else
            {
                oldRLF002 = db.RLF002.ToList();
            }
            db.SaveChanges();
            int HeaderId = objRLF001.HeaderId;
            #endregion

            #region Line level calculations
            List<RLF006> objRLF006 = db.RLF006.ToList();
            List<RLF005> objRLF005 = db.RLF005.ToList();
            RLF002 objRLF002 = null;
            foreach (var item in objRLF006)
            {
                if (flag.ToLower() == "add")
                {
                    objRLF002 = new RLF002();
                    objRLF002.HeaderId = HeaderId;
                    objRLF002.TypeRollingID = item.Id;
                    objRLF002.CreatedBy = objClsLoginInfo.UserName;
                    objRLF002.CreatedOn = DateTime.Now;
                }
                else
                {
                    objRLF002 = oldRLF002.Where(x => x.TypeRollingID == item.Id).FirstOrDefault();
                    objRLF002.EditedBy = objClsLoginInfo.UserName;
                    objRLF002.EditedOn = DateTime.Now;
                }

                if (item.RType.ToLower() == "Cold Rolling".ToLower())
                {
                    objRLF002.RolYieldStrength = objRLF001.TCYieldStrength > 0 ? Math.Round((Convert.ToDouble(objRLF001.TCYieldStrength * 1.05)), 2, MidpointRounding.AwayFromZero) : Math.Round(Convert.ToDouble(objRLF001.CodeYieldStrength * 1.4), 2, MidpointRounding.AwayFromZero);
                    objRLF002.EquivThickness = Convert.ToDouble(Math.Ceiling(Convert.ToDecimal(Math.Sqrt(Convert.ToDouble(objRLF002.RolYieldStrength * objRLF001.Thickness * objRLF001.Thickness / 260)))));


                    if (ShellID < 1000)
                    {
                        objRLF002.WidthRolling = "Not Feasible";
                        objRLF002.WidthPrebend = "Not Feasible";
                    }
                    else
                    {
                        if (objRLF002.EquivThickness > 0 && objRLF002.EquivThickness <= 89)
                        {
                            objRLF002.WidthRolling = item.Rolling.ToString();
                        }
                        else
                        {
                            RLF005 objWidth = objRLF005.Where(x => x.Type.ToLower() == "ROLLING".ToLower() && x.ForDia == objRLF001.ShellStdID && x.Thickness == objRLF002.EquivThickness).FirstOrDefault();
                            if (objWidth != null)
                            {
                                objRLF002.WidthRolling = objWidth.Width;
                            }
                            else
                            {
                                objRLF002.WidthRolling = "Not Feasible";
                            }

                        }

                        if (objRLF002.EquivThickness > 0 && objRLF002.EquivThickness <= 70)
                        {
                            objRLF002.WidthPrebend = item.PreBending.ToString();
                        }
                        else
                        {
                            RLF005 objWidth = objRLF005.Where(x => x.Type.ToLower() == "PRE-BENDING".ToLower() && x.ForDia == objRLF001.ShellStdID && x.Thickness == objRLF002.EquivThickness).FirstOrDefault();
                            if (objWidth != null)
                            {
                                objRLF002.WidthPrebend = objWidth.Width;
                            }
                            else
                            {
                                objRLF002.WidthPrebend = "Not Feasible";
                            }
                        }
                    }
                }
                else if (item.RType.ToLower() == "Warm Rolling (400-450°C)".ToLower())
                {
                    objRLF002.RolYieldStrength = objRLF001.TCYieldStrength > 0 ? Math.Round((Convert.ToDouble(objRLF001.TCYieldStrength * 1.05 * 0.8)), 2, MidpointRounding.AwayFromZero) : Math.Round(Convert.ToDouble(objRLF001.CodeYieldStrength * 1.4 * 0.8), 2, MidpointRounding.AwayFromZero);
                    objRLF002.EquivThickness = Convert.ToDouble(Math.Ceiling(Convert.ToDecimal(Math.Sqrt(Convert.ToDouble(objRLF002.RolYieldStrength * objRLF001.Thickness * objRLF001.Thickness / 260)))));

                    if (ShellID < 1000)
                    {
                        objRLF002.WidthRolling = "Not Feasible";
                        objRLF002.WidthPrebend = "Not Feasible";
                    }
                    else
                    {
                        if (objRLF002.EquivThickness > 0 && objRLF002.EquivThickness <= 89)
                        {
                            objRLF002.WidthRolling = item.Rolling.ToString();
                        }
                        else
                        {
                            RLF005 objWidth = objRLF005.Where(x => x.Type.ToLower() == "ROLLING".ToLower() && x.ForDia == objRLF001.ShellStdID && x.Thickness == objRLF002.EquivThickness).FirstOrDefault();
                            if (objWidth != null)
                            {
                                objRLF002.WidthRolling = objWidth.Width;
                            }
                            else
                            {
                                objRLF002.WidthRolling = "Not Feasible";
                            }
                        }

                        if (objRLF002.EquivThickness > 0 && objRLF002.EquivThickness <= 70)
                        {
                            objRLF002.WidthPrebend = item.PreBending.ToString();
                        }
                        else
                        {
                            RLF005 objWidth = objRLF005.Where(x => x.Type.ToLower() == "PRE-BENDING".ToLower() && x.ForDia == objRLF001.ShellStdID && x.Thickness == objRLF002.EquivThickness).FirstOrDefault();
                            if (objWidth != null)
                            {
                                objRLF002.WidthPrebend = objWidth.Width;
                            }
                            else
                            {
                                objRLF002.WidthPrebend = "Not Feasible";
                            }
                        }
                    }
                }
                else if (item.RType.ToLower() == "Warm Rolling (600-650°C)".ToLower())
                {
                    objRLF002.RolYieldStrength = objRLF001.TCYieldStrength > 0 ? Math.Round((Convert.ToDouble(objRLF001.TCYieldStrength * 1.05 * 0.7)), 2, MidpointRounding.AwayFromZero) : Math.Round(Convert.ToDouble(objRLF001.CodeYieldStrength * 1.4 * 0.7), 2, MidpointRounding.AwayFromZero);
                    objRLF002.EquivThickness = Convert.ToDouble(Math.Ceiling(Convert.ToDecimal(Math.Sqrt(Convert.ToDouble(objRLF002.RolYieldStrength * objRLF001.Thickness * objRLF001.Thickness / 260)))));

                    if (ShellID < 1000)
                    {
                        objRLF002.WidthRolling = "Not Feasible";
                        objRLF002.WidthPrebend = "Not Feasible";
                    }
                    else
                    {
                        if (objRLF002.EquivThickness > 0 && objRLF002.EquivThickness <= 89)
                        {
                            objRLF002.WidthRolling = item.Rolling.ToString();
                        }
                        else
                        {
                            RLF005 objWidth = objRLF005.Where(x => x.Type.ToLower() == "ROLLING".ToLower() && x.ForDia == objRLF001.ShellStdID && x.Thickness == objRLF002.EquivThickness).FirstOrDefault();
                            if (objWidth != null)
                            {
                                objRLF002.WidthRolling = objWidth.Width;
                            }
                            else
                            {
                                objRLF002.WidthRolling = "Not Feasible";
                            }
                        }

                        if (objRLF002.EquivThickness > 0 && objRLF002.EquivThickness <= 70)
                        {
                            objRLF002.WidthPrebend = item.PreBending.ToString();
                        }
                        else
                        {
                            RLF005 objWidth = objRLF005.Where(x => x.Type.ToLower() == "PRE-BENDING".ToLower() && x.ForDia == objRLF001.ShellStdID && x.Thickness == objRLF002.EquivThickness).FirstOrDefault();
                            if (objWidth != null)
                            {
                                objRLF002.WidthPrebend = objWidth.Width;
                            }
                            else
                            {
                                objRLF002.WidthPrebend = "Not Feasible";
                            }
                        }
                    }
                }
                else if (item.RType.ToLower() == "Hot Rolling (above 900°C)".ToLower())
                {
                    objRLF002.RolYieldStrength = objRLF001.TCYieldStrength > 0 ? Math.Round((Convert.ToDouble(objRLF001.TCYieldStrength * 1.05 * 0.5)), 2, MidpointRounding.AwayFromZero) : Math.Round(Convert.ToDouble(objRLF001.CodeYieldStrength * 1.4 * 0.5), 2, MidpointRounding.AwayFromZero);
                    objRLF002.EquivThickness = Convert.ToDouble(Math.Ceiling(Convert.ToDecimal(Math.Sqrt(Convert.ToDouble(objRLF002.RolYieldStrength * objRLF001.Thickness * objRLF001.Thickness / 260)))));

                    if (ShellID < 1000)
                    {
                        objRLF002.WidthRolling = "Not Feasible";
                        objRLF002.WidthPrebend = "Not Feasible";
                    }
                    else
                    {
                        if (objRLF002.EquivThickness > 0 && objRLF002.EquivThickness <= 89)
                        {
                            objRLF002.WidthRolling = item.Rolling.ToString();
                        }
                        else
                        {
                            RLF005 objWidth = objRLF005.Where(x => x.Type.ToLower() == "ROLLING".ToLower() && x.ForDia == objRLF001.ShellStdID && x.Thickness == objRLF002.EquivThickness).FirstOrDefault();
                            if (objWidth != null)
                            {
                                objRLF002.WidthRolling = objWidth.Width;
                            }
                            else
                            {
                                objRLF002.WidthRolling = "Not Feasible";
                            }
                        }

                        if (objRLF002.EquivThickness > 0 && objRLF002.EquivThickness <= 70)
                        {
                            objRLF002.WidthPrebend = item.PreBending.ToString();
                        }
                        else
                        {
                            RLF005 objWidth = objRLF005.Where(x => x.Type.ToLower() == "PRE-BENDING".ToLower() && x.ForDia == objRLF001.ShellStdID && x.Thickness == objRLF002.EquivThickness).FirstOrDefault();
                            if (objWidth != null)
                            {
                                objRLF002.WidthPrebend = objWidth.Width;
                            }
                            else
                            {
                                objRLF002.WidthPrebend = "Not Feasible";
                            }
                        }
                    }
                }

                if (flag.ToLower() == "add")
                {
                    db.RLF002.Add(objRLF002);
                }
                db.SaveChanges();
            }
            #endregion

            return HeaderId;
        }
        #endregion
        // GET: RLF/RollingFeasibility
        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult Index()
        {
            RLF001 objRLF001 = db.RLF001.ToList().FirstOrDefault();

            if (objRLF001 != null)
            {
                return RedirectToAction("RollingFeasibilityDetails", "RollingFeasibility", new { HeaderId = objRLF001.HeaderId });
            }
            return RedirectToAction("RollingFeasibilityDetails", "RollingFeasibility", new { HeaderId = 0 });
        }

        [HttpPost]
        public ActionResult LoadRLFHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;

                List<RLF006> objRLF006 = db.RLF006.ToList();
                List<RLF003> objRLF003 = db.RLF003.ToList();

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                strWhere = "1=1";

                var lstResult = db.SP_RLF_GET_HEADER
                   (
                       0,
                       StartIndex,
                       EndIndex,
                       "",
                       strWhere
                   ).ToList();

                var data = (from fx in lstResult
                            select new[]
                            {
                                Convert.ToString(fx.ROW_NO),
                                Convert.ToString(fx.HeaderId),
                                Convert.ToString(objRLF003.Where(x => x.Id == fx.Material).FirstOrDefault().MaterialName),
                                Convert.ToString(fx.Thickness),
                                Convert.ToString(fx.ShellID),
                                Convert.ToString(fx.Diameter),
                                Convert.ToString(fx.CreatedBy),
                                fx.CreatedOn.HasValue ?  Convert.ToString(fx.CreatedOn.Value.ToShortDateString()) : "",
                                "<center><a title='View' href='"+WebsiteURL+"/RLF/RollingFeasibility/RollingFeasibilityDetails?HeaderId="+Convert.ToInt32(fx.HeaderId)+"'><i style='margin-left:5px;' class='fa fa-eye'></i></a></center>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter, AllowAnonymous, UserPermissions]
        public ActionResult RollingFeasibilityDetails(int HeaderId = 0)
        {
            RLF001 objRLF001 = null;
            if (HeaderId > 0)
            {
                // view existing rolling feasibility object
                objRLF001 = db.RLF001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                if (objRLF001 != null)
                {
                    var EquivalantMaterial = db.RLF003.ToList();
                    ViewBag.EquiMaterial = EquivalantMaterial.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.MaterialName, CatID = x.Id.ToString() }).ToList();
                    ViewBag.txtMaterial = EquivalantMaterial.Where(x => x.Id == objRLF001.Material).FirstOrDefault().MaterialName;
                }
                else
                {
                    // error code
                }
            }
            else
            {
                // new object creation for rolling feasibility
                objRLF001 = new RLF001();

                var EquivalantMaterial = db.RLF003.ToList();
                ViewBag.EquiMaterial = EquivalantMaterial.AsEnumerable().Select(x => new BULocWiseCategoryModel() { CatDesc = x.MaterialName, CatID = x.Id.ToString() }).ToList();
            }

            return View(objRLF001);
        }

        [HttpPost]
        public ActionResult SaveRLFDetails(RLF001 model)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                RLF001 objRLF001 = null;
                if (model != null)
                {
                    if (model.HeaderId > 0)
                    {
                        objRLF001 = db.RLF001.Where(x => x.HeaderId == model.HeaderId).FirstOrDefault();
                    }
                    else
                    {
                        objRLF001 = new RLF001();
                    }

                    objRLF001.Material = model.Material;
                    objRLF001.Thickness = model.Thickness;
                    objRLF001.ShellID = model.ShellID;
                    objRLF001.TCYieldStrength = model.TCYieldStrength;
                    objRLF001.Diameter = model.Diameter;
                    objRLF001.CreatedBy = objClsLoginInfo.UserName;
                    objRLF001.CreatedOn = DateTime.Now;
                    objRLF001.CodeYieldStrength = db.RLF003.Where(x => x.Id == model.Material).FirstOrDefault().YieldStrength;
                    int headerId = MaterialCalculation(objRLF001, model);
                    objResponseMsg.HeaderId = headerId;

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();

                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg);
        }

        public ActionResult LoadRLFLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = string.Empty;
                int HeaderId = Convert.ToInt32(param.CTQHeaderId);

                RLF001 objRLF001 = db.RLF001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                List<RLF006> objRLF006 = db.RLF006.ToList();
                List<RLF003> objRLF003 = db.RLF003.ToList();

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                strWhere = "1=1 and HeaderId = " + HeaderId;

                var lstResult = db.SP_RLF_GET_LINES
                   (
                       HeaderId,
                       StartIndex,
                       EndIndex,
                       "",
                       strWhere
                   ).ToList();

                var data = (from fx in lstResult
                            select new[]
                            {
                                //fx.ROW_NO == 2 ? Convert.ToString(fx.ROW_NO) : "<span class='child' ></span>",
                                Convert.ToString(fx.LineId),
                                Convert.ToString(fx.HeaderId),
                                fx.ROW_NO == 2 ? Convert.ToString(objRLF003.Where(x => x.Id == objRLF001.Material).FirstOrDefault().MaterialName) : "<span class='child' ></span>",
                                fx.ROW_NO == 2 ? Convert.ToString(objRLF001.Thickness) : "<span class='child' ></span>",
                                fx.ROW_NO == 2 ? Convert.ToString(objRLF001.ShellID) : "<span class='child' ></span>",
                                Convert.ToString(objRLF006.Where(x => x.Id == fx.TypeRollingID).FirstOrDefault().RType),
                                fx.ROW_NO == 2 ? Convert.ToString(objRLF001.CodeYieldStrength) : "<span class='child' ></span>",
                                Convert.ToString(fx.RolYieldStrength),
                                Convert.ToString(fx.EquivThickness),
                                Convert.ToString(fx.WidthRolling),
                                Convert.ToString(fx.WidthPrebend),
                                fx.ROW_NO == 2 ? Convert.ToString(objRLF001.FibreElongation) : "<span class='child' ></span>",
                                fx.ROW_NO == 2 ? Convert.ToString(objRLF001.TypeRollingFE) : "<span class='child' ></span>",
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }
        }
    }
}