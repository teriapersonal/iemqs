﻿using System.Web.Mvc;

namespace IEMQS.Areas.RLF
{
    public class RLFAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "RLF";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "RLF_default",
                "RLF/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}