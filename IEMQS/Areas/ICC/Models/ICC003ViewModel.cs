﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.ICC.Models
{
    public class ICC003ViewModel
    {
        public int HeaderId { get; set; }
        public string MaterialCategory { get; set; }
        public string MaterialGroup { get; set; }
        public string DistinctItem { get; set; }
        public string UOM { get; set; }
        public string ItemCode { get; set; }
        public string LongDescription { get; set; }
        public string ShortDescription { get; set; }
        public string NatureOfItem { get; set; }
        public string ItemType { get; set; }
        public string ItemSubType { get; set; }
        public Nullable<double> SafetyLevel_HZW { get; set; }
        public Nullable<double> SafetyLevel_PEW { get; set; }
        public Nullable<double> SafetyLevel_RNW { get; set; }
        public Nullable<double> ReorderLevel_HZW { get; set; }
        public Nullable<double> ReorderLevel_PEW { get; set; }
        public Nullable<double> ReorderLevel_RNW { get; set; }
        public Nullable<double> MaxStock_HZW { get; set; }
        public Nullable<double> MaxStock_PEW { get; set; }
        public Nullable<double> MaxStock_RNW { get; set; }
        public Nullable<double> FixedOrderQty_HZW { get; set; }
        public Nullable<double> FixedOrderQty_PEW { get; set; }
        public Nullable<double> FixedOrderQty_RNW { get; set; }
        public Nullable<double> ReStockLevel_HZW { get; set; }
        public Nullable<double> ReStockLevel_PEW { get; set; }
        public Nullable<double> ReStockLevel_RNW { get; set; }
        public string ItemCodeRequestor { get; set; }
        public string Status { get; set; }

        public int RequestId { get; set; }
        public string ChangeDetails { get; set; }
        public string RequestStatus { get; set; }
        public string RequestedBy { get; set; }
        public Nullable<System.DateTime> RequestedOn { get; set; }
        public string ApprovedBy { get; set; }
        public Nullable<System.DateTime> ApprovedOn { get; set; }
        public string ReturnedBy { get; set; }
        public Nullable<System.DateTime> ReturnedOn { get; set; }
        public string ReturnRemarks { get; set; }

    }
}