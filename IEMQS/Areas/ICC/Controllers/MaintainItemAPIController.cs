﻿using IEMQSImplementation;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace IEMQS.Areas.ICC.Controllers
{
    /// <summary>
    /// Create By Darshan Dave 13/12/2018
    /// This Api is to post the data for ICC 
    /// Note: This API will using in CodeGen
    /// </summary>
    public class MaintainItemAPIController : ApiController
    {
        public readonly IEMQSEntitiesContext db;

        public MaintainItemAPIController()
        {
            if (db == null)
            {
                db = new IEMQSEntitiesContext();
            }
        }

        // POST: ICC/api/MaintainItemAPI       
        public HttpResponseMessage Post([Bind(Include = "MaterialCategory, MaterialGroup, DistinctItem, UOM, ItemCode, LongDescription, CreatedBy")]IList<ICC002> item)
        {
            HttpRequestMessage request = new HttpRequestMessage();
            try
            {

                var notSavedItem = new List<ICC002>();
                var isError = false;
                if (item.Any())
                {
                    foreach (var data in item)
                    {
                        if (!string.IsNullOrEmpty(data.ItemCode))
                        {
                            data.Status = clsImplementationEnum.ICCStatus.Not_Created.GetStringValue();
                            data.CreatedOn = DateTime.Now;
                            data.ItemCodeRequestor = data.CreatedBy;
                            db.ICC002.Add(data);
                            db.SaveChanges();


                            var itemGroupData = db.ICC001.FirstOrDefault(x =>
                            x.MaterialCategory.Equals(data.MaterialCategory) &&
                            x.MaterialGroup.Equals(data.MaterialGroup) &&
                            x.UOM.Equals(data.UOM) &&
                            x.DistinctItem.Equals(data.DistinctItem));

                            if (itemGroupData == null)
                            {
                                var itemGroup = new ICC001()
                                {
                                    CreatedBy = data.CreatedBy,
                                    CreatedOn = DateTime.Now,
                                    DistinctItem = data.DistinctItem,
                                    MaterialCategory = data.MaterialCategory,
                                    MaterialGroup = data.MaterialGroup,
                                    UOM = data.UOM
                                };
                                db.ICC001.Add(itemGroup);
                                db.SaveChanges();
                            }
                        }
                        else
                        {
                            isError = true;
                            notSavedItem.Add(data);
                        }
                        var userEmail = (from ath1 in db.ATH001
                                         join com7 in db.COM007 on ath1.Employee equals com7.t_emno
                                         join ath4 in db.ATH004 on ath1.Role equals ath4.Id
                                         where ath4.Role.Equals("CR1")
                                         select com7.t_mail).Distinct().ToList();
                        MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.ICC.ItemCreation).FirstOrDefault();
                        if (objTemplateMaster != null)
                        {
                            Hashtable _ht = new Hashtable();
                            EmailSend _objEmail = new EmailSend();
                            _objEmail.MailToAdd = string.Join(",", userEmail); // Get ALL MDM User Email Ids
                            _ht["[ItemCode]"] = data.ItemCode;
                            _ht["[ItemLongDescription]"] = data.LongDescription;
                            _ht["[MaterialCategory]"] = data.MaterialCategory;
                            _ht["[MaterialGroup]"] = data.MaterialGroup;
                            _ht["[DistinctItem]"] = data.DistinctItem;
                            _ht["[UOM]"] = data.UOM;
                            _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                        }
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
                if (isError)
                {
                    return Request.CreateResponse(notSavedItem.Select(x => new
                    {
                        x.MaterialCategory,
                        x.MaterialGroup,
                        x.DistinctItem,
                        x.UOM,
                        x.ItemCode,
                        x.LongDescription,
                        x.CreatedBy
                    }));
                }
                return Request.CreateResponse(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, ex.Message);
            }
        }
    }
}
