﻿using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.ICC.Controllers
{
    public class MaintainWarehouseController : clsBase
    {
        [AllowAnonymous, SessionExpireFilter, UserPermissions]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadWarehouseData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhereCondition += "1=1 ";

                #endregion         

                string[] columnName = { "Location", "Warehouse" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var lstResult = db.SP_ICC_GET_WAREHOUSE_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            uc.Location,
                            Helper.GenerateHTMLTextbox(uc.Id,"Warehouse",uc.Warehouse,"UpdateData(this, "+ uc.Id+",true);",false,"",false,"6"),
                            Convert.ToString(uc.Id)
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult UpdateData(string rowId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int Id = Convert.ToInt32(rowId);
                columnValue = columnValue == null ? null : columnValue.Replace("'", "''");
                db.SP_COMMON_TABLE_UPDATE("ICC004", Id, "Id", columnName, columnValue, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                var lst = db.SP_ICC_GET_WAREHOUSE_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                switch ((clsImplementationEnum.GridType)Enum.Parse(typeof(clsImplementationEnum.GridType), gridType))
                {
                    case clsImplementationEnum.GridType.GetItemGroupMaster:
                        var itemUpdateCreatedList = (from uc in lst
                                                     select new
                                                     {
                                                         Location = uc.Location,
                                                         Warehouse = uc.Warehouse,
                                                     }).ToList();

                        strFileName = Helper.GenerateExcel(itemUpdateCreatedList, objClsLoginInfo.UserName);
                        break;
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
    }
}