﻿using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.ICC.Controllers
{
    /// <summary>
    /// Created By Darshan Dave 14/12/2018
    /// </summary>
    [SessionExpireFilter]
    public class MaintainItemGroupController : clsBase
    {
        public ActionResult Index()
        {
            ViewBag.Title = "Item Group Master";
            return View();
        }

        [HttpPost]
        public ActionResult ItemGroupGridData(JQueryDataTableParamModel param)
        {
            try
            {
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhereCondition = string.Empty;

                #region Datatable Sorting 
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                strWhereCondition += "1=1 ";

                #endregion               
                //search Condition 

                string[] columnName = { "MaterialCategory", "MaterialGroup", "DistinctItem", "UOM", "a.ItemGroup", "a.Description" };
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    strWhereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var LNItemGroup = db.SP_ICC_GET_ITEM_GROUP(" ", false).ToList();

                var tempDropDown = new SelectList(LNItemGroup, "ItemGroup", "Description");
                var lstResult = db.SP_ICC_GET_ITEM_GROUP_MASTER_DATA(StartIndex, EndIndex, strSortOrder, strWhereCondition).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                            //Convert.ToString(uc.ROW_NO),
                            uc.MaterialCategory,
                            uc.MaterialGroup,
                            uc.DistinctItem,
                            uc.UOM,
                            Helper.GenerateHTMLTextbox(uc.Id, "LNItemGroup-", (uc.ERPLNItemGroup)
                            ,"" ,false,"",false,"","","GetItemGroup(this)"),
                            Convert.ToString(uc.Id)
                          }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    whereCondition = strWhereCondition,
                    strSortOrder = strSortOrder
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult AddItemGroupById(int id, string itemGroupId)
        {
            var result = new clsHelper.ResponseMsg();
            try
            {
                var itemGroup = db.ICC001.Find(id);

                itemGroup.ERPLNItemGroup = itemGroupId;
                itemGroup.EditedBy = objClsLoginInfo.UserName;
                itemGroup.EditedOn = DateTime.Now;
                db.SaveChanges();
                result.Key = true;
                result.Value = "Item Group has updated.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                result.Key = false;
                result.Value = ex.Message;
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult GetLNItemGroup(string term)
        {
            try
            {
                var LNItemGroup = db.SP_ICC_GET_ITEM_GROUP(term, false).ToList();
                return Json(LNItemGroup);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(ex.Message);
            }
        }

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;                
                var lst = db.SP_ICC_GET_ITEM_GROUP_MASTER_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                switch ((clsImplementationEnum.GridType)Enum.Parse(typeof(clsImplementationEnum.GridType), gridType))
                {
                    case clsImplementationEnum.GridType.GetItemGroupMaster:
                        var itemUpdateCreatedList = (from uc in lst
                                                     select new
                                                     {
                                                         MaterialCategory = uc.MaterialCategory,
                                                         MaterialGroup = uc.MaterialGroup,
                                                         DistinctItem = uc.DistinctItem,
                                                         UOM = uc.UOM,
                                                         ERPLNItemGroup = uc.ERPLNItemGroup,
                                                     }).ToList();

                        strFileName = Helper.GenerateExcel(itemUpdateCreatedList, objClsLoginInfo.UserName);
                        break;
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ShowTimeline(int id)
        {
            var model = new TimelineViewModel();
            model.Title = "ICC-GroupMaster";
            model.TimelineTitle = "Item Group Master";

            if (id > 0)
            {
                var objICC001 = db.ICC001.Find(id);
                model.CreatedBy = objICC001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objICC001.CreatedBy) : null;
                model.CreatedOn = objICC001.CreatedOn;
                model.EditedBy = objICC001.EditedBy != null ? Manager.GetUserNameFromPsNo(objICC001.EditedBy) : null;
                model.EditedOn = objICC001.EditedOn;
            }
            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
    }
}