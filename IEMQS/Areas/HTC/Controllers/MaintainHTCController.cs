﻿using IEMQS.Areas.HTC.Models;
using IEMQS.Areas.MSW.Controllers;
using IEMQS.Models;
using IEMQS.Areas.Utility.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace IEMQS.Areas.HTC.Controllers
{
    public class MaintainHTCController : clsBase
    {
        // GET: HTC/MaintainHTC
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult HTCDetail(int HeaderID = 0)
        {
            HTC001 objHTC001 = new HTC001();

            var user = objClsLoginInfo.UserName;

            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
            ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            var approver = (from t1 in db.ATH001
                            join t2 in db.COM003 on t1.Employee equals t2.t_psno
                            where t2.t_actv == 1
                            select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
            ViewBag.approver = new SelectList(approver, "Employee", "Desc");
            var project = (from a in db.HTC001
                           where a.HeaderId == HeaderID
                           select a.Project).FirstOrDefault();
            ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);

            List<string> lstFuranceCharge = clsImplementationEnum.getFurnaceCharge().ToList();
            ViewBag.lstFuranceCharge = lstFuranceCharge.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            List<string> lstFurnaceTypes = clsImplementationEnum.getFurnacetypes().ToList();
            ViewBag.lstFurnaceTypes = lstFurnaceTypes.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            if (HeaderID > 0)
            {
                objHTC001 = db.HTC001.Where(x => x.HeaderId == HeaderID).FirstOrDefault();

                var PlanningDinID = db.PDN002.Where(x => x.RefId == HeaderID && x.DocumentNo == objHTC001.Document).OrderByDescending(x => x.HeaderId).Select(x => x.HeaderId).FirstOrDefault();
                ViewBag.PlanningDinID = PlanningDinID;

                ViewBag.IsReviseBtnEnabled = Manager.IsReviseEnabled(PlanningDinID, objHTC001.HeaderId, objHTC001.Document);
                ViewBag.DocMessage = clsImplementationMessage.CommonMessages.DocMessage.ToString();
                //select* from PDN002 where RefId = '90' and DocumentNo = 'H02_244B_PL04' and
                //if (objHTC001.CreatedBy != objClsLoginInfo.UserName)
                //{
                //    return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                //}
            }

            return View(objHTC001);
        }

        [HttpPost]
        public ActionResult GetHeaderData(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();
                var ExistsPrject = db.COM001.Any(x => x.t_cprj == project);
                if (ExistsPrject == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    string customer = Manager.GetCustomerCodeAndNameByProject(project);

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = customer;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowHistoryTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();

            model.TimelineTitle = "HTC TimeLine";
            model.Title = "PDinDoc";
            if (HeaderId > 0)
            {

                HTC001_Log objHTC001_Log = db.HTC001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objHTC001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objHTC001_Log.CreatedBy) : null;
                model.CreatedOn = objHTC001_Log.CreatedOn;
                model.EditedBy = objHTC001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objHTC001_Log.EditedBy) : null;
                model.EditedOn = objHTC001_Log.EditedOn;
                model.SubmittedBy = objHTC001_Log.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objHTC001_Log.SubmittedBy) : null;
                model.SubmittedOn = objHTC001_Log.SubmittedOn;

                model.ApprovedBy = objHTC001_Log.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objHTC001_Log.ApprovedBy) : null;
                model.ApprovedOn = objHTC001_Log.ApprovedOn;

            }
            else
            {

                HTC001_Log objHTC001_Log = db.HTC001_Log.Where(x => x.Id == HeaderId).FirstOrDefault();
                model.CreatedBy = objHTC001_Log.CreatedBy != null ? Manager.GetUserNameFromPsNo(objHTC001_Log.CreatedBy) : null;
                model.CreatedOn = objHTC001_Log.CreatedOn;
                model.EditedBy = objHTC001_Log.EditedBy != null ? Manager.GetUserNameFromPsNo(objHTC001_Log.EditedBy) : null;
                model.EditedOn = objHTC001_Log.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            model.Title = "PDinDoc";
            model.TimelineTitle = "HTC Timeline";

            if (HeaderId > 0)
            {
                HTC001 objHTC001 = db.HTC001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objHTC001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objHTC001.CreatedBy) : null;
                model.CreatedOn = objHTC001.CreatedOn;
                model.EditedBy = objHTC001.EditedBy != null ? Manager.GetUserNameFromPsNo(objHTC001.EditedBy) : null;
                model.EditedOn = objHTC001.EditedOn;

                // model.SubmittedBy = PMB001_Log.SendToCompiledOn != null ? Manager.GetUserNameFromPsNo(PMB001_Log.SendToCompiledOn) : null;
                //model.SubmittedOn = PMB001_Log.SendToCompiledOn;
                model.SubmittedBy = objHTC001.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objHTC001.SubmittedBy) : null;
                model.SubmittedOn = objHTC001.SubmittedOn;
                //model.CompiledBy = PMB001_Log.CompiledBy != null ? Manager.GetUserNameFromPsNo(PMB001_Log.CompiledBy) : null;
                // model.CompiledOn = PMB001_Log.CompiledOn;
                model.ApprovedBy = objHTC001.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objHTC001.ApprovedBy) : null;
                model.ApprovedOn = objHTC001.ApprovedOn;

            }
            else
            {
                HTC001 objHTC001 = db.HTC001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objHTC001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objHTC001.CreatedBy) : null;
                model.CreatedOn = objHTC001.CreatedOn;
                model.EditedBy = objHTC001.EditedBy != null ? Manager.GetUserNameFromPsNo(objHTC001.EditedBy) : null;
                model.EditedOn = objHTC001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress.cshtml", model);
        }
        [HttpPost]
        public ActionResult getHeaderId(string project)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                project = project.Split('-')[0].Trim();
                int Headerid = (from a in db.HTC001
                                where a.Project == project
                                select a.HeaderId).FirstOrDefault();
                string status = (from a in db.HTC001
                                 where a.Project == project
                                 select a.Status).FirstOrDefault();
                objResponseMsg.Key = true;
                objResponseMsg.Value = Headerid.ToString() + "|" + status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHTCHeader(HTC001 htc001, FormCollection fc)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                if (!string.IsNullOrWhiteSpace(fc["hfHeaderId"].ToString()) && Convert.ToInt32(fc["hfHeaderId"].ToString()) > 0)
                {
                    int hid = Convert.ToInt32(fc["hfHeaderId"].ToString());
                    HTC001 objHTC001 = db.HTC001.Where(x => x.HeaderId == hid).FirstOrDefault();
                    string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                    objHTC001.Project = fc["hfProject"].ToString();
                    objHTC001.Document = fc["txtdoc"].ToString();
                    objHTC001.Customer = fc["txtCust"].ToString().Split('-')[0];
                    objHTC001.ReviseRemark = fc["ReviseRemark"].ToString();
                    if (objHTC001.Status == clsImplementationEnum.PTMTCTQStatus.Approved.GetStringValue())
                    {
                        objHTC001.RevNo = Convert.ToInt32(objHTC001.RevNo) + 1;
                        objHTC001.Status = clsImplementationEnum.PTMTCTQStatus.DRAFT.GetStringValue();
                        objHTC001.ReturnRemark = null;
                        objHTC001.ApprovedOn = null;

                    }
                    //objHTC001.Product = fc["ddlCat"].ToString();
                    objHTC001.ProcessLicensor = fc["txtlicence"].ToString();
                    objHTC001.EditedBy = objClsLoginInfo.UserName;
                    objHTC001.EditedOn = DateTime.Now;
                    objHTC001.ApprovedBy = fc["hfApp"].ToString().Split('-')[0].Trim();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CTQMessages.Update.ToString();
                    objResponseMsg.Revision = "R" + objHTC001.RevNo.ToString();
                    objResponseMsg.status = objHTC001.Status;
                    Manager.UpdatePDN002(objHTC001.HeaderId, objHTC001.Status, objHTC001.RevNo, objHTC001.Project, objHTC001.Document);
                }
                else
                {
                    string project = fc["ddlProject"].ToString().Split('-')[0].Trim();

                    var isvalid = db.HTC001.Any(x => x.Project == project);
                    if (isvalid == false)
                    {
                        string rev = Regex.Replace(fc["txtRev"].ToString(), "[^0-9]+", string.Empty);
                        HTC001 objhtc001 = new HTC001();
                        objhtc001.Project = project;
                        objhtc001.Document = fc["txtdoc"].ToString();
                        objhtc001.Customer = fc["txtCust"].ToString().Split('-')[0];
                        objhtc001.RevNo = Convert.ToInt32(rev);
                        objhtc001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                        objhtc001.Product = fc["ddlCat"].ToString();
                        objhtc001.ProcessLicensor = fc["txtlicence"].ToString();
                        objhtc001.CreatedBy = objClsLoginInfo.UserName;
                        objhtc001.CreatedOn = DateTime.Now;
                        objhtc001.ApprovedBy = fc["ddlApprover"].ToString().Split('-')[0].Trim();
                        objhtc001.ReviseRemark = fc["ReviseRemark"].ToString();
                        db.HTC001.Add(objhtc001);
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CTQMessages.Insert.ToString();
                        objResponseMsg.Revision = "R" + objhtc001.RevNo.ToString();
                        objResponseMsg.status = objhtc001.Status;
                        Manager.UpdatePDN002(objhtc001.HeaderId, objhtc001.Status, objhtc001.RevNo, objhtc001.Project, objhtc001.Document);
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.Message.ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetInsertView(string project, string document, string RevNo)
        {
            project = project.Split('-')[0].Trim();
            int Headerid = (from a in db.HTC001
                            where a.Project == project
                            select a.HeaderId).FirstOrDefault();
            List<string> lstFuranceCharge = clsImplementationEnum.getFurnaceCharge().ToList();
            ViewBag.lstFuranceCharge = lstFuranceCharge.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            List<string> lstFurnaceTypes = clsImplementationEnum.getFurnacetypes().ToList();
            ViewBag.lstFurnaceTypes = lstFurnaceTypes.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            ViewBag.project = project;
            ViewBag.document = document;
            ViewBag.RevNo = RevNo;
            ViewBag.Headerid = Headerid;

            HTC002 objHTC002 = new HTC002();
            return PartialView("HTCLines", objHTC002);
        }

        [HttpPost]
        public ActionResult GetUpdateView(string project, string document, string RevNo, int lineid)
        {
            project = project.Split('-')[0].Trim();
            HTC002 objHTC002 = new HTC002();
            var isvalid = db.HTC002.Any(x => x.LineId == lineid);
            if (isvalid == true)
            {
                objHTC002 = db.HTC002.Where(x => x.LineId == lineid).FirstOrDefault();
                if (objHTC002 != null)
                {
                    int Headerid = (from a in db.HTC001
                                    where a.Project == project
                                    select a.HeaderId).FirstOrDefault();
                    List<string> lstFuranceCharge = clsImplementationEnum.getFurnaceCharge().ToList();
                    ViewBag.lstFuranceCharge = lstFuranceCharge.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                    List<string> lstFurnaceTypes = clsImplementationEnum.getFurnacetypes().ToList();
                    ViewBag.lstFurnaceTypes = lstFurnaceTypes.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                    ViewBag.project = project;
                    ViewBag.document = document;
                    ViewBag.RevNo = RevNo;
                    ViewBag.Headerid = Headerid;


                }
            }
            return PartialView("HTCLines", objHTC002);
        }

        [HttpPost]
        public ActionResult GetHTRNumber(int id, string identicalHTR)
        {
            List<CategoryData> lstHTRDetails = new List<CategoryData>();
            if (id > 0)
            {
                string LinkedHTR = id.ToString(); ;
                if (!string.IsNullOrWhiteSpace(identicalHTR))
                {
                    LinkedHTR += "," + identicalHTR;
                }
                string[] lstHTRs = LinkedHTR.Split(',');
                foreach (var item in lstHTRs)
                {
                    if (!string.IsNullOrWhiteSpace(item))
                    {
                        int htrid = Convert.ToInt32(item);
                        var objhtr = db.HTR001.Where(x => x.HeaderId == htrid).FirstOrDefault();
                        if (objhtr != null)
                        {
                            lstHTRDetails.Add(new CategoryData()
                            {
                                Id = htrid,
                                catValue = objhtr.HTRNo,
                                CategoryDescription = "R" + objhtr.RevNo,
                                Value = objhtr.Project,
                                Description = db.COM001.Where(i => i.t_cprj == objhtr.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault()
                            });
                        }
                    }
                }
            }
            if (lstHTRDetails.Count > 0)
            {
                ViewBag.HTRData = lstHTRDetails.OrderBy(x => x.Description).ToList();
            }
            else
            { ViewBag.HTRData = null; }

            return PartialView("HTCLines");
        }


        [HttpPost]
        public ActionResult SaveHTCLines(HTC002 htc002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                HTC002 objHTC002 = new HTC002();
                objHTC002.HeaderId = Convert.ToInt32(fc["hfHeaderId"].ToString());
                objHTC002.Project = fc["hfProject"].ToString();
                objHTC002.Document = fc["hfdoc"].ToString();
                objHTC002.RevNo = Convert.ToInt32(Regex.Replace(fc["hfRev"].ToString(), "[^0-9]+", string.Empty));
                objHTC002.SectionDescription = fc["txtSecDesc"].ToString();
                objHTC002.FurnaceCharge = fc["ddlFCharge"].ToString();
                objHTC002.Height = string.IsNullOrWhiteSpace(fc["txtHeight"].ToString()) ? 0 : Convert.ToInt32(fc["txtHeight"]);
                objHTC002.Width = string.IsNullOrWhiteSpace(fc["txtWidth"].ToString()) ? 0 : Convert.ToInt32(fc["txtWidth"]); //Convert.ToInt32(fc["txtWidth"]);
                objHTC002.Length = string.IsNullOrWhiteSpace(fc["txtLength"].ToString()) ? 0 : Convert.ToInt32(fc["txtLength"]); //Convert.ToInt32(fc["txtLength"]);
                objHTC002.Weight = string.IsNullOrWhiteSpace(fc["txtWeight"].ToString()) ? 0 : Convert.ToDecimal(fc["txtWeight"], CultureInfo.InvariantCulture);//Convert.ToInt32(fc["txtWeight"]);
                objHTC002.Furnace = fc["ddlFType"].ToString();
                objHTC002.CreatedBy = objClsLoginInfo.UserName;
                objHTC002.CreatedOn = DateTime.Now;
                db.HTC002.Add(objHTC002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult LoadHTCLineData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);
                string headerstatus = param.MTStatus;
                //string strWhere = string.Empty;
                string strWhere = "1=1 ";
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " and (SectionDescription like '%" + param.sSearch
                        + "%' or FurnaceCharge like '%" + param.sSearch
                        + "%' or Height like '%" + param.sSearch
                        + "%' or Width like '%" + param.sSearch
                        + "%' or Length like '%" + param.sSearch
                        + "%' or Weight like '%" + param.sSearch
                        + "%' or Furnace like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    //strWhere = "1=1";
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_HTC_GET_LINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();
                int newRecordId = 0;
                var newRecord = new[] {
                                        //Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                        GenerateTextboxFor(newRecordId, "SectionDescription", "", "","", false,"", "100"),
                                        GenerateAutoComplete(newRecordId,"FurnaceCharge","","",false,"","FurnaceCharge"),
                                        //GenerateTextboxFor(newRecordId, "FurnaceCharge", Convert.ToString(uc.FurnaceCharge),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");"),
                                        GenerateNumericTextboxFor(newRecordId, "Length", "", "","", false,"", "10","numberonly"),
                                        GenerateNumericTextboxFor(newRecordId, "Width", "", "","", false,"", "10","numberonly"),
                                        GenerateNumericTextboxFor(newRecordId, "Height", "", "","", false,"", "10","numberonly"),
                                        GenerateNumericTextboxFor(newRecordId, "Weight", "", "","", false,"", "10"),
                                        GenerateAutoComplete(newRecordId,"Furnace","","",false,"","Furnace"),
                                        //GenerateTextboxFor(newRecordId, "Furnace", Convert.ToString(uc.Furnace),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");"),
                                             "",
                                     //"No",
                                          Helper.GenerateGridButton(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveHTCLineDetails();" ),
                                        Helper.GenerateHidden(newRecordId, "HeaderId",Convert.ToString(param.Headerid)),
                                        "",

                                        };
                SourceHTC htc = new SourceHTC();
                var data = (from uc in lstResult
                            select new[]
                            {
                            GenerateTextboxFor(uc.LineId, "SectionDescription", Convert.ToString(uc.SectionDescription), "UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"","100"),
                            GenerateAutoComplete(uc.LineId, "FurnaceCharge", Convert.ToString(uc.FurnaceCharge),"UpdateData(this, "+ uc.LineId +");",false,"","FurnaceCharge",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)),
                            //GenerateTextboxFor(uc.HeaderId, "FurnaceCharge", Convert.ToString(uc.FurnaceCharge),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");"),
                            GenerateNumericTextboxFor(uc.LineId, "Length", Convert.ToString(uc.Length),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"","10","numberonly"),
                            GenerateNumericTextboxFor(uc.LineId, "Width", Convert.ToString(uc.Width),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"","10","numberonly"),
                            GenerateNumericTextboxFor(uc.LineId, "Height", Convert.ToString(uc.Height),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"","10","numberonly"),
                            GenerateNumericTextboxFor(uc.LineId, "Weight", Convert.ToString(uc.Weight),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false),"","10"),
                            GenerateAutoComplete(uc.LineId, "Furnace", Convert.ToString(uc.Furnace),"UpdateData(this, "+ uc.LineId +");",false,"","Furnace",(string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(headerstatus,clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase)?true:false)),
                            Helper.GenerateActionIcon(uc.LineId,"LinkedHTR","Linked HTR","fa fa-fire-extinguisher", "LinkedHTR('"+ uc.HTRHeaderId+"','"+uc.IdenticalHTRHeaderIds+"')","",false),
                                //htc.htrLink(Convert.ToInt32(uc.HTRHeaderId)),
                            //Convert.ToString(uc.IsHTRCreated == true ? "Yes":"No"),
                            //GenerateTextboxFor(uc.HeaderId, "Furnace", Convert.ToString(uc.Furnace),"UpdateData(this, "+ uc.LineId +");","checkStatus("+ uc.HeaderId +");"),
                            HTMLActionString(uc.LineId,headerstatus,"Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +");"),
                            Convert.ToString(uc.HeaderId),
                             Convert.ToString(uc.LineId) ,
                           }).ToList();

                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        public string GeneratehtrLink(int id, string htrids)
        {
            string htmlControl = "";
            List<ReturnedData> lstReturnDetails = new List<ReturnedData>();
            if (id > 0)
            {
                string LinkedHTR = id + "," + htrids;
                string[] lstHTRs = LinkedHTR.Split(',');
                foreach (var item in lstHTRs)
                {
                    int htrid = Convert.ToInt32(item);
                    var objhtr = db.HTR001.Where(x => x.HeaderId == htrid).FirstOrDefault();
                    if (objhtr != null)
                    {
                        htmlControl = "<a href=\"" + clsBase.WebsiteURL + "/HTR/Maintain/AddHeader?HeaderID=" + htrid + "\" name=\"documentLink\">" + objhtr.HTRNo + " R" + objhtr.RevNo + "</a>";
                    }
                    else
                    {
                        htmlControl = "";
                    }
                }
            }
            else
            {
                htmlControl = "";
            }

            return htmlControl;
        }
        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }
        [HttpPost]
        public JsonResult GetHeaderDetailsForPrintReport(int HeaderId)
        {
            var objHTC001 = db.HTC001.Where(i => i.HeaderId == HeaderId).Select(i => new { i.HeaderId, i.Project, i.Document, i.RevNo }).FirstOrDefault();
            return Json(objHTC001, JsonRequestBehavior.AllowGet);
        }
        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";
            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
            {
                if (string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.Approved.GetStringValue()))
                {
                    htmlControl = "";// "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
            }

            return htmlControl;
        }
        [HttpPost]
        public ActionResult UpdateDetails(int lineId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string tableName = "HTC002";
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, tableName, objClsLoginInfo.UserName);
                    //ReviseData(headerId);
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveHTCLineDetails(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int newRowIndex = 0;
                HTC001 objHTC001 = new HTC001();
                HTC002 objHTC002 = new HTC002();
                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                objHTC001 = db.HTC001.Where(x => x.HeaderId == headerId).FirstOrDefault();

                objHTC002.HeaderId = headerId;
                objHTC002.Project = objHTC001.Project;
                objHTC002.Document = objHTC001.Document;
                objHTC002.RevNo = objHTC001.RevNo;
                objHTC002.SectionDescription = fc["SectionDescription" + newRowIndex];
                objHTC002.FurnaceCharge = fc["FurnaceCharge" + newRowIndex];
                objHTC002.Height = string.IsNullOrWhiteSpace(fc["Height" + newRowIndex].ToString()) ? 0 : Convert.ToInt32(fc["Height" + newRowIndex]);
                objHTC002.Width = string.IsNullOrWhiteSpace(fc["Width" + newRowIndex].ToString()) ? 0 : Convert.ToInt32(fc["Width" + newRowIndex]); //Convert.ToInt32(fc["txtWidth" + newRowIndex]);
                objHTC002.Length = string.IsNullOrWhiteSpace(fc["Length" + newRowIndex].ToString()) ? 0 : Convert.ToInt32(fc["Length" + newRowIndex]); //Convert.ToInt32(fc["txtLength" + newRowIndex]);
                objHTC002.Weight = string.IsNullOrWhiteSpace(fc["Weight" + newRowIndex].ToString()) ? 0 : Convert.ToDecimal(fc["Weight" + newRowIndex], CultureInfo.InvariantCulture);//Convert.ToInt32(fc["txtWeight" + newRowIndex]);
                objHTC002.Furnace = fc["Furnace" + newRowIndex].ToString();
                objHTC002.CreatedBy = objClsLoginInfo.UserName;
                objHTC002.CreatedOn = DateTime.Now;
                db.HTC002.Add(objHTC002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Insert.ToString();

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public class CustomResponceMsg : clsHelper.ResponseMsg
        {
            public int HeaderID;
            public string Status;
            public int? rev;
        }

        [HttpPost]
        public ActionResult ReviseData(int strHeaderId, string strRemarks)
        {
            CustomResponceMsg objResponseMsg = new CustomResponceMsg();
            try
            {
                string status = clsImplementationEnum.PLCStatus.Approved.GetStringValue();
                HTC001 objHTC001 = db.HTC001.Where(u => u.HeaderId == strHeaderId && u.Status == status).FirstOrDefault();
                if (objHTC001 != null)
                {
                    objHTC001.RevNo = Convert.ToInt32(objHTC001.RevNo) + 1;
                    objHTC001.Status = clsImplementationEnum.CommonStatus.DRAFT.GetStringValue();
                    objHTC001.ReviseRemark = strRemarks;
                    objHTC001.EditedBy = objClsLoginInfo.UserName;
                    objHTC001.EditedOn = DateTime.Now;
                    objHTC001.ReturnRemark = null;
                    objHTC001.ApprovedOn = null;
                    objHTC001.SubmittedBy = null;
                    objHTC001.SubmittedOn = null;
                    db.SaveChanges();
                    Manager.UpdatePDN002(objHTC001.HeaderId, objHTC001.Status, objHTC001.RevNo, objHTC001.Project, objHTC001.Document);
                    objResponseMsg.Key = true;
                    objResponseMsg.HeaderID = objHTC001.HeaderId;
                    objResponseMsg.Status = objHTC001.Status;
                    objResponseMsg.rev = objHTC001.RevNo;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision;
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        //[HttpPost]
        //public ActionResult ReviseData(int headerId)
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        HTC001 objHTC001 = db.HTC001.Where(x => x.HeaderId == headerId).FirstOrDefault();
        //        if (objHTC001.Status == clsImplementationEnum.UTCTQStatus.Approved.GetStringValue())
        //        {
        //            objHTC001.RevNo = Convert.ToInt32(objHTC001.RevNo) + 1;
        //            objHTC001.Status = clsImplementationEnum.UTCTQStatus.DRAFT.GetStringValue();
        //            objHTC001.EditedBy = objClsLoginInfo.UserName;
        //            objHTC001.EditedOn = DateTime.Now;
        //            db.SaveChanges();
        //        }
        //        objResponseMsg.Key = true;
        //        objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = ex.Message;

        //    }
        //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //}

        public static string GenerateTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "disabled" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' " + MaxCharcters + " style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";

            return htmlControl;
        }
        public static string GenerateNumericTextboxFor(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string maxlength = "", string classname = "")
        {
            string htmlControl = "";

            string inputID = columnName + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = !string.IsNullOrEmpty(classname) ? "form-control " + classname : "form-control numeric-only";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<input type='text' " + (isReadOnly ? "disabled" : "") + " id='" + inputID + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' " + MaxCharcters + " style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + "  />";

            return htmlControl;
        }

        [HttpPost]
        public ActionResult LoadHTCLineData_old(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(param.Headerid);

                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (SectionDescription like '%" + param.sSearch
                        + "%' or FurnaceCharge like '%" + param.sSearch
                        + "%' or Height like '%" + param.sSearch
                        + "%' or Width like '%" + param.sSearch
                        + "%' or Length like '%" + param.sSearch
                        + "%' or Weight like '%" + param.sSearch
                        + "%' or Furnace like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var lstResult = db.SP_HTC_GET_LINEDETAILS
                                (
                                StartIndex, EndIndex, "", headerid, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.SectionDescription),
                           Convert.ToString(uc.FurnaceCharge),
                           Convert.ToString(uc.Length),
                           Convert.ToString(uc.Width),
                           Convert.ToString(uc.Height),
                           Convert.ToString(uc.Weight),
                           Convert.ToString(uc.Furnace),
                           Convert.ToString(uc.LineId),
                           Convert.ToString(uc.HeaderId),
                           Convert.ToString(uc.LineId)
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }


        [HttpPost]
        public JsonResult LoadHTCHeaderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                string strSortOrder = string.Empty;
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += "1=1 and status in('" + clsImplementationEnum.CTQStatus.DRAFT.GetStringValue() + "','" + clsImplementationEnum.CTQStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += "1=1";
                }
                //strWhere += " and CreatedBy=" + user;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += "and (Project like '%" + param.sSearch
                        + "%' or Document like '%" + param.sSearch
                        + "%' or Customer like '%" + param.sSearch
                        + "%' or RevNo like '%" + param.sSearch
                        + "%' or Product like '%" + param.sSearch
                        + "%' or ProcessLicensor like '%" + param.sSearch
                        + "%' or Status like '%" + param.sSearch + "%')";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                var lstResult = db.SP_HTC_GET_HEADERDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                               Convert.ToString(uc.HeaderId),
                               Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Customer)),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                               Convert.ToString(uc.SubmittedBy),
                               Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                               Convert.ToString(uc.ApprovedBy),
                               Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                            "<nobr><center>"+"<a class=\"iconspace\" href=\""+WebsiteURL+"/HTC/MaintainHTC/HTCDetail?HeaderID=" + uc.HeaderId + "\"><i class=\"fa fa-eye\"></i></a>"+
                            Helper.GenerateActionIcon(uc.HeaderId,"Delete","Delete Record","fa fa-trash-o", "DeleteDocument("+ uc.HeaderId +",'/HTC/MaintainHTC/DeleteHeader', {headerid:"+uc.HeaderId+"}, 'tblHTCHeader')","",  (( uc.RevNo >0 && uc.Status.ToLower() != clsImplementationEnum.CommonStatus.SendForApprovel.GetStringValue().ToLower()) || ( uc.RevNo == 0 && uc.Status.ToLower() == clsImplementationEnum.CommonStatus.Approved.GetStringValue().ToLower()) ) ? false:true) +
                            "<i class='iconspace fa fa-clock-o' title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/HTC/MaintainHTC/ShowTimeline?HeaderID=" + Convert.ToInt32(uc.HeaderId) + "')></i>" +
                            (uc.RevNo > 0 ? "<i  title=\"History\" class=\"iconspace fa fa-history\" onClick=\"history(" + uc.HeaderId + ")\"></i>" : "<i  title=\"History\" class=\"disabledicon fa fa-history\" \"></i>") +
                            "<i title=\"Print Report\" class=\"iconspace fa fa-print\" onClick=\"PrintReport(" + uc.HeaderId + ")\"></i> " +
                            "</center></nobr>"
                                  }).ToList();



                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //[HttpPost]
        //public ActionResult GetHistoryDetails(string strRole, string HeaderId)
        //{
        //    ViewBag.Role = strRole;
        //    ViewBag.HeaderId = HeaderId;
        //    return PartialView("_getHistoryPartial");
        //}

        [HttpPost]
        public ActionResult DeleteHeader(int headerid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = Manager.DeletePDINDocument(headerid, clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetHeaderGridDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("GetHTCGridDataPartial");
        }

        [HttpPost]
        public ActionResult DeleteHTCLine(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                HTC002 objHTC002 = db.HTC002.Where(x => x.LineId == Id).FirstOrDefault();
                db.HTC002.Remove(objHTC002);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateHTCLines(HTC002 htc002, FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int lineid = Convert.ToInt32(fc["hfLineid"]);
                if (lineid > 0)
                {
                    HTC002 objHTC002 = db.HTC002.Where(x => x.LineId == lineid).FirstOrDefault();
                    objHTC002.HeaderId = Convert.ToInt32(fc["hfHeaderId"]);
                    objHTC002.Project = fc["hfProject"].ToString();
                    objHTC002.Document = fc["hfdoc"].ToString();
                    objHTC002.RevNo = Convert.ToInt32(Regex.Replace(fc["hfRev"].ToString(), "[^0-9]+", string.Empty));
                    objHTC002.SectionDescription = fc["txtSecDesc"].ToString();
                    objHTC002.FurnaceCharge = fc["ddlFCharge"].ToString();
                    objHTC002.Height = string.IsNullOrWhiteSpace(fc["txtHeight"].ToString()) ? 0 : Convert.ToInt32(fc["txtHeight"]);
                    objHTC002.Width = string.IsNullOrWhiteSpace(fc["txtWidth"].ToString()) ? 0 : Convert.ToInt32(fc["txtWidth"]); //Convert.ToInt32(fc["txtWidth"]);
                    objHTC002.Length = string.IsNullOrWhiteSpace(fc["txtLength"].ToString()) ? 0 : Convert.ToInt32(fc["txtLength"]); //Convert.ToInt32(fc["txtLength"]);
                    if (!string.IsNullOrWhiteSpace(fc["txtWeight"].ToString()))
                    {
                        objHTC002.Weight = Convert.ToDecimal(fc["txtWeight"], CultureInfo.InvariantCulture);//Convert.ToInt32(fc["txtWeight"]);
                    }
                    objHTC002.Furnace = fc["ddlFType"].ToString();
                    objHTC002.EditedBy = objClsLoginInfo.UserName;
                    objHTC002.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.AuthMatrixMessages.Update.ToString();
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult sentForApproval(string Approver, int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (headerid > 0)
                {
                    if (Approver.Split('-')[0].ToString().Trim() == objClsLoginInfo.UserName)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.SMessage.ToString();
                    }
                    else
                    {
                        HTC001 objHTC001 = db.HTC001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                        objHTC001.Status = clsImplementationEnum.CTQStatus.SendForApprovel.GetStringValue();
                        objHTC001.ApprovedBy = Approver.Split('-')[0].ToString().Trim();
                        objHTC001.SubmittedBy = objClsLoginInfo.UserName;
                        objHTC001.SubmittedOn = DateTime.Now;

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.AMessage.ToString();
                        Manager.UpdatePDN002(objHTC001.HeaderId, objHTC001.Status, objHTC001.RevNo, objHTC001.Project, objHTC001.Document);

                        #region Send Notification
                        (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PLNG2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.PLNG1.GetStringValue(), objHTC001.Project, "", "", Manager.GetPDINDocumentNotificationMsg(objHTC001.Project, clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet.GetStringValue(), objHTC001.RevNo.Value.ToString(), objHTC001.Status), clsImplementationEnum.NotificationType.ActionRequired.GetStringValue(), Manager.GetPDINDocRedirectURLForNotification(clsImplementationEnum.PlanList.Heat_Treatment_Charge_Sheet.GetStringValue(), objHTC001.HeaderId.ToString(), true), objHTC001.ApprovedBy);
                        #endregion
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult createRevison(int headerid)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int? revison = (from a in db.HTC001
                                where a.HeaderId == headerid
                                select a.RevNo).FirstOrDefault();
                revison = revison + 1;
                HTC001 objHTC001 = db.HTC001.Where(x => x.HeaderId == headerid).FirstOrDefault();
                objHTC001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                objHTC001.RevNo = revison;
                objHTC001.EditedBy = objClsLoginInfo.UserName;
                objHTC001.EditedOn = DateTime.Now;
                objHTC001.ReturnRemark = null;
                objHTC001.ApprovedOn = null;

                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = revison.ToString();
                Manager.UpdatePDN002(objHTC001.HeaderId, objHTC001.Status, objHTC001.RevNo, objHTC001.Project, objHTC001.Document);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }



        [HttpPost]
        public JsonResult LoadHTCHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                var user = objClsLoginInfo.UserName;

                var HeaderId = param.CTQHeaderId;
                var role = param.CTQCompileStatus;

                string strWhere = string.Empty;
                strWhere += " 1=1 and HeaderId=" + HeaderId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (Project like '%" + param.sSearch
                       + "%' or Document like '%" + param.sSearch
                       + "%' or Customer like '%" + param.sSearch
                       + "%' or RevNo like '%" + param.sSearch
                       + "%' or Product like '%" + param.sSearch
                       + "%' or ProcessLicensor like '%" + param.sSearch
                       + "%' or Status like '%" + param.sSearch + "%')";
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_HTC_GET_HISTORY_HEADERDETAIL
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                               Convert.ToString(uc.Document),
                               Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                             //  Convert.ToDateTime(uc.CDD).ToString("dd/MM/yyyy"),
                               Convert.ToString(uc.Product),
                               Convert.ToString(uc.ProcessLicensor),
                               Convert.ToString("R"+uc.RevNo),
                               Convert.ToString(uc.Status),
                                 Convert.ToString(uc.SubmittedBy),
                                 Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                 Convert.ToString(uc.ApprovedBy),
                                 Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA":Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),

                               Convert.ToString(uc.HeaderId),

                                Convert.ToString(db.HTC001_Log.FirstOrDefault(q => (q.HeaderId == uc.HeaderId && q.RevNo == uc.RevNo)).Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult CopyHeaderPartial(int Id)
        {
            var lstProjectDesc = (from a in db.COM001
                                  select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
            ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

            return PartialView("CopyToDestinationPartial");
        }


        [HttpPost]
        public ActionResult CopyToDestination(int headerId, string destProject)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            HTC001 sourceHTC001 = new HTC001();
            HTC001 destHTC001 = new HTC001();
            SourceHTC objSourceHTC = new SourceHTC();

            try
            {
                string currentUser = objClsLoginInfo.UserName;
                if (headerId > 0)
                {
                    sourceHTC001 = db.HTC001.Where(i => i.HeaderId == headerId).FirstOrDefault();
                    objSourceHTC = GetSourceMSWDetail(sourceHTC001.HeaderId);
                }
                if (!string.IsNullOrEmpty(destProject))
                {
                    destHTC001 = db.HTC001.Where(i => i.Project.Equals(destProject, StringComparison.OrdinalIgnoreCase)).FirstOrDefault();

                    if (destHTC001 != null)
                    {
                        if (string.Equals(destHTC001.Status, clsImplementationEnum.QCPStatus.Draft.GetStringValue(), StringComparison.OrdinalIgnoreCase))
                        {
                            objResponseMsg = InsertOrUpdateMSW(objSourceHTC, destHTC001, true);
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "You cannot copy to " + destProject + " as it is in " + destHTC001.Status + "Staus";
                        }
                    }
                    else
                    {
                        destHTC001 = new HTC001();
                        destHTC001.Project = destProject;
                        objResponseMsg = InsertOrUpdateMSW(objSourceHTC, destHTC001, false);
                    }
                }
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Value = ex.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public SourceHTC GetSourceMSWDetail(int headerId)
        {
            SourceHTC objSourceMSW = new SourceHTC();
            objSourceMSW.SourceHeader = db.HTC001.Where(i => i.HeaderId == headerId).FirstOrDefault();
            objSourceMSW.SourceCategories = db.HTC002.Where(i => i.HeaderId == headerId).ToList();
            return objSourceMSW;
        }

        public clsHelper.ResponseMsg InsertOrUpdateMSW(SourceHTC sourceHTC, HTC001 objHTC001, bool isUpdate)//(SourceQCP sourceQCP,QCP001 objQCP001,bool isUpdate)
        {
            clsHelper.ResponseMsg objResponse = new clsHelper.ResponseMsg();
            var sourceHeader = sourceHTC.SourceHeader;

            try
            {
                if (isUpdate)
                {
                    var lstExistingCategory = db.HTC002.Where(i => i.HeaderId == objHTC001.HeaderId).ToList();// sourceQCP.SourceCategories;
                    if (lstExistingCategory.Any())
                    {
                        db.HTC002.RemoveRange(lstExistingCategory);
                        db.SaveChanges();
                    }

                    #region MSW Header Copy To Existing Header

                    objHTC001.Document = sourceHeader.Document;
                    objHTC001.Customer = sourceHeader.Customer;
                    objHTC001.RevNo = sourceHeader.RevNo;
                    objHTC001.Product = sourceHeader.Product;
                    objHTC001.ProcessLicensor = sourceHeader.ProcessLicensor;
                    objHTC001.EditedBy = objClsLoginInfo.UserName;
                    objHTC001.EditedOn = DateTime.Now;
                    db.SaveChanges();

                    #endregion
                }
                else
                {
                    #region QCP Header To Create New Header

                    objHTC001.RevNo = 0;
                    objHTC001.Document = sourceHeader.Document;
                    objHTC001.Customer = sourceHeader.Customer;
                    objHTC001.RevNo = sourceHeader.RevNo;
                    objHTC001.Product = sourceHeader.Product;
                    objHTC001.ProcessLicensor = sourceHeader.ProcessLicensor;
                    objHTC001.Status = "Draft";
                    objHTC001.CreatedBy = objClsLoginInfo.UserName;
                    objHTC001.CreatedOn = DateTime.Now;
                    db.HTC001.Add(objHTC001);
                    db.SaveChanges();
                    #endregion
                }

                #region Add lines

                var sourceCategory = sourceHTC.SourceCategories.Where(i => i.HeaderId == sourceHeader.HeaderId).ToList();

                foreach (var item in sourceCategory)
                {
                    HTC002 objHTC002 = new HTC002();
                    objHTC002.HeaderId = objHTC001.HeaderId;
                    objHTC002.Project = objHTC001.Project;
                    objHTC002.Document = objHTC001.Document;
                    objHTC002.RevNo = objHTC001.RevNo;
                    objHTC002.Furnace = item.Furnace;
                    objHTC002.FurnaceCharge = item.FurnaceCharge;
                    objHTC002.Height = item.Height;
                    objHTC002.Weight = item.Weight;
                    objHTC002.Length = item.Length;
                    objHTC002.Width = item.Width;
                    objHTC002.SectionDescription = item.SectionDescription;
                    objHTC002.CreatedBy = objClsLoginInfo.UserName;
                    objHTC002.CreatedOn = DateTime.Now;

                    db.HTC002.Add(objHTC002);
                    db.SaveChanges();
                }

                #endregion
                Manager.UpdatePDN002(objHTC001.HeaderId, objHTC001.Status, objHTC001.RevNo, objHTC001.Project, objHTC001.Document);
                objResponse.Key = true;
                objResponse.Value = "MSW Copied Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponse.Key = false;
                objResponse.Value = ex.Message;
            }

            return objResponse;
        }
        public string GetCodeAndNameByProject(string project)
        {
            var projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            return projdesc;
        }
        [HttpPost]
        public ActionResult GetHistoryView(int headerid = 0)
        {
            HTC001_Log objHTC001 = new HTC001_Log();
            // ViewBag.Role = strRole;
            ViewBag.HeaderId = headerid;
            Session["Headerid"] = Convert.ToInt32(headerid);
            return PartialView("getHistoryPartial", objHTC001);
        }

        [SessionExpireFilter]
        public ActionResult getHistoryDetail(int Id = 0)
        {
            HTC001_Log objHTC001 = new HTC001_Log();
            objHTC001 = db.HTC001_Log.Where(x => x.Id == Id).FirstOrDefault();
            var user = objClsLoginInfo.UserName;
            if (objHTC001 != null)
            {
                var lstProjectDesc = (from a in db.COM001
                                      select new Projects { projectCode = a.t_cprj, projectDescription = a.t_cprj + " - " + a.t_dsca }).ToList();
                ViewBag.Project = new SelectList(lstProjectDesc, "projectCode", "projectDescription");

                List<string> lstProCat = clsImplementationEnum.getProductCategory().ToList();
                ViewBag.lstProCat = lstProCat.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

                var approver = (from t1 in db.ATH001
                                join t2 in db.COM003 on t1.Employee equals t2.t_psno
                                where t2.t_actv == 1
                                select new { t1.Employee, Desc = t2.t_psno + " - " + t2.t_name }).Distinct().ToList();
                ViewBag.approver = new SelectList(approver, "Employee", "Desc");
                var project = (from a in db.HTC001
                               where a.HeaderId == Id
                               select a.Project).FirstOrDefault();

                ViewBag.Customer = Manager.GetCustomerCodeAndNameByProject(project);
            }
            var urlPrefix = Request.Url.AbsoluteUri.Substring(0, Request.Url.AbsoluteUri.LastIndexOf('=') + 1);
            if (!objClsLoginInfo.GetUserRoleList().Contains("SHOP"))
            {
                ViewBag.RevPrev = (db.HTC001_Log.Any(q => (q.HeaderId == objHTC001.HeaderId && q.RevNo == (objHTC001.RevNo - 1))) ? urlPrefix + db.HTC001_Log.Where(q => (q.HeaderId == objHTC001.HeaderId && q.RevNo == (objHTC001.RevNo - 1))).FirstOrDefault().Id : null);
                ViewBag.RevNext = (db.HTC001_Log.Any(q => (q.HeaderId == objHTC001.HeaderId && q.RevNo == (objHTC001.RevNo + 1))) ? urlPrefix + db.HTC001_Log.Where(q => (q.HeaderId == objHTC001.HeaderId && q.RevNo == (objHTC001.RevNo + 1))).FirstOrDefault().Id : null);
            }
            return View(objHTC001);
        }

        [HttpPost]
        public ActionResult LoadHTCLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                int headerid = Convert.ToInt32(Request["HeaderId"]);

                string strWhere = string.Empty;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere = " (SectionDescription like '%" + param.sSearch
                        + "%' or FurnaceCharge like '%" + param.sSearch
                        + "%' or Height like '%" + param.sSearch
                        + "%' or Width like '%" + param.sSearch
                        + "%' or Length like '%" + param.sSearch
                        + "%' or Weight like '%" + param.sSearch
                        + "%' or Furnace like '%" + param.sSearch
                        + "%')";
                }
                else
                {
                    strWhere = "1=1";
                }
                var strSortOrder = string.Empty;
                var lstResult = db.SP_HTC_GET_HISTORY_LINEDETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, headerid, strWhere
                                ).ToList();
                SourceHTC htc = new SourceHTC();
                var data = (from uc in lstResult
                            select new[]
                            {
                           Convert.ToString(uc.SectionDescription),
                           Convert.ToString(uc.FurnaceCharge),
                           Convert.ToString(uc.Length),
                           Convert.ToString(uc.Width),
                           Convert.ToString(uc.Height),
                           Convert.ToString(uc.Weight),
                           Convert.ToString(uc.Furnace),
                         Helper.GenerateActionIcon(uc.LineId,"LinkedHTR","Linked HTR","fa fa-fire-extinguisher", "LinkedHTR('"+ uc.HTRHeaderId+"','"+uc.IdenticalHTRHeaderIds+"')","",false),
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }
        [HttpPost]
        public ActionResult getCodeValue(string project, string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {

                objResponseMsg.projdesc = db.COM001.Where(i => i.t_cprj == project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objResponseMsg.appdesc = db.COM003.Where(i => i.t_psno == approver && i.t_actv == 1).Select(i => i.t_psno + " - " + i.t_name).FirstOrDefault();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult verifyApprover(string approver)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                approver = approver.Split('-')[0].Trim();
                var Exists = db.COM003.Any(x => x.t_psno == approver && x.t_actv == 1);
                if (Exists == false)
                {
                    objResponseMsg.Key = false;
                }
                else
                {
                    objResponseMsg.Key = true;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult retrack(string headerid)
        {
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                int hd = Convert.ToInt32(headerid);
                if (hd > 0)
                {
                    HTC001 objHTC001 = db.HTC001.Where(x => x.HeaderId == hd).FirstOrDefault();
                    objHTC001.SubmittedOn = null;
                    objHTC001.SubmittedBy = null;

                    objHTC001.Status = clsImplementationEnum.CTQStatus.DRAFT.GetStringValue();
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    Manager.UpdatePDN002(objHTC001.HeaderId, objHTC001.Status, objHTC001.RevNo, objHTC001.Project, objHTC001.Document);
                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        #region Export Grid Data
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "", int? HeaderId = 0)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    List<SP_HTC_GET_HEADERDETAILS_Result> lst = db.SP_HTC_GET_HEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                                      Document = Convert.ToString(uc.Document),
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Customer)),
                                      Product = Convert.ToString(uc.Product),
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      Status = Convert.ToString(uc.Status),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy = Convert.ToString(uc.ApprovedBy),
                                      ApprovedOn = Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_HTC_GET_LINEDETAILS(1, int.MaxValue, strSortOrder, HeaderId, whereCondition).ToList();

                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      SectionDescription = Convert.ToString(uc.SectionDescription),
                                      FurnaceCharge = Convert.ToString(uc.FurnaceCharge),
                                      Length = Convert.ToString(uc.Length),
                                      Width = Convert.ToString(uc.Width),
                                      Height = Convert.ToString(uc.Height),
                                      Weight = Convert.ToString(uc.Weight),
                                      Furnace = Convert.ToString(uc.Furnace),
                                      HTRNO = Convert.ToString((uc.HTRHeaderId != null && uc.HTRHeaderId > 0) ? (db.HTR001.Where(x => x.HeaderId == uc.HTRHeaderId).FirstOrDefault()?.HTRNo) + (db.HTR001.Where(x => x.HeaderId == uc.HTRHeaderId).FirstOrDefault() != null ? " R":"") + (db.HTR001.Where(x => x.HeaderId == uc.HTRHeaderId).FirstOrDefault()?.RevNo) : ""),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {

                    var lst = db.SP_HTC_GET_HISTORY_HEADERDETAIL(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      Project = Convert.ToString(GetCodeAndNameByProject(uc.Project)),
                                      Document = Convert.ToString(uc.Document),
                                      Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(uc.Project)),
                                      //  Convert.ToDateTime(uc.CDD).ToString("dd/MM/yyyy"),
                                      Product = Convert.ToString(uc.Product),
                                      ProcessLicensor = Convert.ToString(uc.ProcessLicensor),
                                      RevNo = Convert.ToString("R" + uc.RevNo),
                                      Status = Convert.ToString(uc.Status),
                                      SubmittedBy = Convert.ToString(uc.SubmittedBy),
                                      SubmittedOn = Convert.ToString(Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(uc.SubmittedOn).ToString("dd/MM/yyyy")),
                                      ApprovedBy = Convert.ToString(uc.ApprovedBy),
                                      ApprovedOn = Convert.ToString(Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(uc.ApprovedOn).ToString("dd/MM/yyyy")),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYLINES.GetStringValue())
                {
                    var lst = db.SP_HTC_GET_HISTORY_LINEDETAILS(1, int.MaxValue, strSortOrder, HeaderId, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from uc in lst
                                  select new
                                  {
                                      SectionDescription = Convert.ToString(uc.SectionDescription),
                                      FurnaceCharge = Convert.ToString(uc.FurnaceCharge),
                                      Length = Convert.ToString(uc.Length),
                                      Width = Convert.ToString(uc.Width),
                                      Height = Convert.ToString(uc.Height),
                                      Weight = Convert.ToString(uc.Weight),
                                      Furnace = Convert.ToString(uc.Furnace),
                                      HTRNO = Convert.ToString((uc.HTRHeaderId != null && uc.HTRHeaderId > 0) ? (db.HTR001.Where(x => x.HeaderId == uc.HTRHeaderId).FirstOrDefault()?.HTRNo) + (db.HTR001.Where(x => x.HeaderId == uc.HTRHeaderId).FirstOrDefault() != null ? " R" : "") + (db.HTR001.Where(x => x.HeaderId == uc.HTRHeaderId).FirstOrDefault()?.RevNo) : ""),
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
    }
}