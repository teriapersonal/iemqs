﻿using System.Web.Mvc;

namespace IEMQS.Areas.FKM
{
    public class FKMAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "FKM";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "FKM_default",
                "FKM/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}