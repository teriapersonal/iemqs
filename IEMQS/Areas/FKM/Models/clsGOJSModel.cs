﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.FKM.Models
{
    public class clsGOJSModel
    {
        public clsGOJSModel()
        {
            nodeDataArray = new List<NodeData>();
            linkDataArray = new List<LinkData>();
        }
        //"class"= "go.GraphLinksModel",
        public string nodeCategoryProperty { get; set; } = "type";
        public string linkFromPortIdProperty { get; set; } = "";
        public string linkToPortIdProperty { get; set; } = "";
        public List<NodeData> nodeDataArray { get; set; }
        public List<LinkData> linkDataArray { get; set; }
    }

    public class NodeData
    {
        public int key { get; set; }
        public string name { get; set; }
        public int level { get; set; }
        public string type { get; set; } = "Label";
        public int? parent { get; set; } = 0;
        public int? mdel { get; set; }
    }
    public class LinkData
    {
        public int? from { get; set; }
        public int? to { get; set; }
    }
    public class HKitNodeData : NodeData
    {
        public string findno { get; set; }
        public string part { get; set; }
        public string producttype { get; set; }
        public string parentfindno { get; set; }
        public string parentpart { get; set; }
        public bool selectable { get; set; } = true;
    }

    public class FKMSNodeData : NodeData
    {
        public string findno { get; set; }
        public string part { get; set; }
        public string producttype { get; set; }
        public string parentfindno { get; set; }
        public string parentpart { get; set; }
        public bool isrequested { get; set; }
        public int isplannerasm { get; set; }
        public double? actualdur { get; set; }
        public string actualdate { get; set; }
        public string delevertotxt { get; set; }
        public string deleverto { get; set; }
        public bool? cc { get; set; }
        public bool? sunfact { get; set; }
        public string taskdesc { get; set; }
        public double? planneddur { get; set; }
        public string planneddate { get; set; }
        public string taskmanager { get; set; }
        public double? rccp { get; set; }
        public double? cumudur { get; set; }
    }

    public class ESPNodeData : NodeData
    {
        public string findno { get; set; }
        public string part { get; set; }
        public string producttype { get; set; }
        public string parentfindno { get; set; }
        public string parentpart { get; set; }
        public bool isrequested { get; set; }
        public int isplannerasm { get; set; }
        public double? actualdur { get; set; }
        public string actualdate { get; set; }
        public string delevertotxt { get; set; }
        public string deleverto { get; set; }
        public bool? cc { get; set; }
        public bool? sunfact { get; set; }
        public string taskdesc { get; set; }
        public double? planneddur { get; set; }
        public string planneddate { get; set; }
        public string taskmanager { get; set; }
        public double? rccp { get; set; }
        public double? cumudur { get; set; }
    }
}