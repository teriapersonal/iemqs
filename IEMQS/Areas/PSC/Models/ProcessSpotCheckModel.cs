﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.PSC.Models
{
    public class ProcessSpotCheckModel
    {
        public string button { get; set; }
        public Int64? HeaderId { get; set; }
        public int? ReportNo { get; set; }
        public string Dept { get; set; }
        public string Project { get; set; }
        public string ShopInCharge { get; set; }
        public string ItemNo { get; set; }
        public string SeamNo { get; set; }
        public string DescriptionOfActivity { get; set; }
        public string CarriedOutBy { get; set; }
        public string Reciepient { get; set; }
        public string AckBy { get; set; }
        public string CreatedBy { get; set; }
        public string EditedBy { get; set; }
        public DateTime? CreatedOn { get; set; }
        public DateTime? EditedOn { get; set; }
        public string Status { get; set; }
        public List<PSC011> LineDetails { get; set; }
        public int? LineId { get; set; }

        public string Customer { get; set; }
        public string ActionTaken { get; set; }
    }
}