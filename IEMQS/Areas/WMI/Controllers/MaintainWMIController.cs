﻿using IEMQS.Areas.PDIN.Controllers;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Configuration;
using System.Collections;
using System.Web.Script.Serialization;
using System.Net;
using System.Net.Mail;
using IEMQS.Areas.Utility.Models;
using IEMQSImplementation.WMI;
using IEMQSImplementation.Models;

namespace IEMQS.Areas.WMI.Controllers
{

    public class MaintainWMIController : clsBase
    {
        WMIEntities WMIDB = new WMIEntities();
        // GET: WMI/MaintainWMI
        string ControllerURL = "/WMI/MaintainWMI/";
        [SessionExpireFilter]
        public ActionResult Index()
        {
            return View();
        }
        [SessionExpireFilter]
        public ActionResult Maintain()
        {
            var objclsLoginInfo = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
            if (objclsLoginInfo != null)
            {
                if (objclsLoginInfo.listMenuResult != null)
                {
                    int Cnt = objclsLoginInfo.listMenuResult.Where(x => x.Process == "Maintain WMI").ToList().Count();
                    if (Cnt == 0)
                        return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.IsDisplayOnly = false;
            ViewBag.Role = "Maintain";
            ViewBag.Title = "Maintain Sheet";
            return View("Index");
        }
        [SessionExpireFilter]
        public ActionResult Approve()
        {
            var objclsLoginInfo = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
            if (objclsLoginInfo != null)
            {
                if (objclsLoginInfo.listMenuResult != null)
                {
                    int Cnt = objclsLoginInfo.listMenuResult.Where(x => x.Process == "Approve WMI").ToList().Count();
                    if (Cnt == 0)
                        return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.IsDisplayOnly = true;
            ViewBag.Role = "Approve";
            ViewBag.Title = "Approve Sheet";
            return View("Index");
        }
        [SessionExpireFilter]
        public ActionResult Display()
        {
            var objclsLoginInfo = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
            if (objclsLoginInfo != null)
            {
                if (objclsLoginInfo.listMenuResult != null)
                {
                    int Cnt = objclsLoginInfo.listMenuResult.Where(x => x.Process == "Display WMI").ToList().Count();
                    if (Cnt == 0)
                        return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                }
            }
            ViewBag.IsDisplayOnly = true;
            ViewBag.Role = "Display";
            ViewBag.Title = "Display Sheet";
            return View("Index");
        }
        [HttpPost]
        public ActionResult GetGridDataPartial(string status, string role, bool IsDisplayOnly = false)
        {
            ViewBag.IsDisplayOnly = IsDisplayOnly;
            ViewBag.Status = status;
            ViewBag.Role = role;

            return PartialView("_GetGridDataPartial");
        }
        public ActionResult LoadWMIHeaderData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string status = param.Status;
                string role = param.Roles;



                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                string whereCondition = "1=1 and islatest = 1";

                if (role == "maintain")
                {
                    if (status == "pending")
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.WMIStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.WMIStatus.Returned.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.WMIStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.WMIStatus.SendToApproval.GetStringValue() + "','" + clsImplementationEnum.WMIStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.WMIStatus.Returned.GetStringValue() + "')";
                    }
                }
                else if (role == "display")
                {
                    //whereCondition += " and status in ('" + clsImplementationEnum.WMIStatus.Approved.GetStringValue() + "')";
                    whereCondition += " and status in ('" + clsImplementationEnum.WMIStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.WMIStatus.SendToApproval.GetStringValue() + "','" + clsImplementationEnum.WMIStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.WMIStatus.Returned.GetStringValue() + "')";

                }
                else if (role == "approve")
                {
                    if (status == "pending")
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.WMIStatus.SendToApproval.GetStringValue() + "')";
                    }
                    else
                    {
                        whereCondition += " and status in ('" + clsImplementationEnum.WMIStatus.SendToApproval.GetStringValue() + "','" + clsImplementationEnum.WMIStatus.Approved.GetStringValue() + "','" + clsImplementationEnum.WMIStatus.Returned.GetStringValue() + "')";
                    }
                }


                //else
                //    whereCondition += " and status in ('" + clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue() + "')";

                //whereCondition += IsDisplayOnly ? (" and isLatest =1 ") : "";

                string[] columnName = { "Project", "Location", "Status", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)", "RevNo" };


                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = WMIDB.SP_WMI_GETHEADERDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.Project),
                               Convert.ToString(h.Location),
                               Convert.ToString(h.Status),
                               Convert.ToString("R"+h.RevNo),
                               Convert.ToString(h.CreatedBy),
                               h.CreatedOn.Value.ToShortDateString(),
                               "<center>"+ (Helper.GenerateActionIcon(h.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/WMI/MaintainWMI/WMIDetails?id="+h.HeaderId+"&role="+role ,false))
                               +
                               (h.RevNo > 0 ? HTMLHistoryString(h.HeaderId,h.Status,"History","History","iconspace fa fa-history","GetHistoryDetails(\""+ h.Projectt.Trim() +"\",\""+h.Location+"\");") + "" : "<i title='History' style = '' class='disabledicon fa fa-history'></i>")
                                //(h.RevNo > 0 ||(h.RevNo==0 && h.Status==clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ? HTMLHistoryString(h.HeaderId,h.Status,"History","History","iconspace fa fa-history","GetHistoryDetails(\""+ h.Project.Split('-')[0].Trim() +"\",\""+h.Location+"\");") + "" : "<i title='History' style = '' class='disabledicon fa fa-history'></i>")
                               +"<i title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/WMI/MaintainWMI/ShowTimeline?HeaderId=" + Convert.ToInt32(h.HeaderId) + "')  class='iconspace fa fa-clock-o' ></i>"
                               +Helper.GenerateActionIcon(h.HeaderId, "Print", "Print Detail", "fa fa-print","printReport("+h.HeaderId+")","" ,false)
                               +"</center>"
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string HTMLHistoryString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
            return htmlControl;
        }
        [HttpPost]
        public ActionResult GetHistoryView(string Project, string Location, string Role)
        {
            ViewBag.Project = Project;
            ViewBag.Location = Location;
            ViewBag.Role = Role;
            return PartialView("_WMIHistoryGrid2");
        }
        //public ActionResult GenerateExcelHistroy(string whereCondition, string strSortOrder, string gridType = "")
        //{
        //    clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
        //    try
        //    {
        //        string strFileName = string.Empty;
        //        if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
        //        {
        //            var lst = db.SP_WMI_GETHEADERDETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
        //            if (!lst.Any())
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "No Data Found";
        //                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //            }
        //            var newlst = (from li in lst
        //                          select new
        //                          {
        //                              Project = Convert.ToString(GetCodeAndNameByProject(li.Project)),
        //                              Customer = Convert.ToString(Manager.GetCustomerCodeAndNameByProject(li.Project)),
        //                              Document = Convert.ToString(li.Document),
        //                              Status = Convert.ToString(li.Status),
        //                              CDDDate = Convert.ToDateTime(li.CDDDate).ToString("dd/MM/yyyy"),
        //                              RevNo = Convert.ToString("R" + li.RevNo),
        //                              SubmittedBy = Convert.ToString(li.SubmittedBy),
        //                              SubmittedOn = Convert.ToString(Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.SubmittedOn).ToString("dd/MM/yyyy")),
        //                              ApprovedBy = Convert.ToString(li.ApprovedBy),
        //                              ApprovedOn = Convert.ToString(Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy") == "01/01/0001" ? "NA" : Convert.ToDateTime(li.ApprovedOn).ToString("dd/MM/yyyy")),
        //                          }).ToList();

        //            strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
        //        }
        //        else if (gridType == clsImplementationEnum.GridType.MajorMaterialConstructionLine.GetStringValue())
        //        {
        //            int headerid = Convert.ToInt32(Session["Headerid"]);

        //            var lst = db.SP_JPP_GET_MajorMaterialConstruction_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
        //            if (!lst.Any())
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "No Data Found";
        //                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //            }
        //            var newlst = (from li in lst
        //                          select new
        //                          {
        //                              ROW_NO = li.ROW_NO,
        //                              //Project = li.Project,
        //                              //Document = li.Document,
        //                              //Revno = Convert.ToString("R" + li.RevNo),
        //                              MajorMaterialofConstruction = Convert.ToString(li.MajorMaterialofConstruction)

        //                          }).ToList();

        //            strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);

        //        }
        //        else if (gridType == clsImplementationEnum.GridType.SpecificCustomerRequirementLine.GetStringValue())
        //        {
        //            int headerid = Convert.ToInt32(Session["Headerid"]);
        //            var lst = db.SP_JPP_GET_SpecificCustomerRequirement_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
        //            if (!lst.Any())
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "No Data Found";
        //                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //            }
        //            var newlst = (from li in lst
        //                          select new
        //                          {
        //                              ROW_NO = li.ROW_NO,
        //                              //Project = li.Project,
        //                              //Document = li.Document,
        //                              //Revno = Convert.ToString("R" + li.RevNo),
        //                              SpecificCustomerRequirement = Convert.ToString(li.MajorMaterialofConstruction)

        //                          }).ToList();

        //            strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
        //        }
        //        else if (gridType == clsImplementationEnum.GridType.PaymentTermsLines.GetStringValue())
        //        {
        //            int headerid = Convert.ToInt32(Session["Headerid"]);
        //            var lst = db.SP_JPP_GET_PaymentTerms_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
        //            if (!lst.Any())
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "No Data Found";
        //                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //            }
        //            var newlst = (from li in lst
        //                          select new
        //                          {
        //                              ROW_NO = li.ROW_NO,
        //                              //Project = li.Project,
        //                              //Document = li.Document,
        //                              //Revno = Convert.ToString("R" + li.RevNo),
        //                              PaymentTerms = Convert.ToString(li.PaymentTerms)

        //                          }).ToList();

        //            strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
        //        }
        //        else if (gridType == clsImplementationEnum.GridType.DocumentsDrawingDescriptionLines.GetStringValue())
        //        {

        //            int headerid = Convert.ToInt32(Session["Headerid"]);
        //            var lst = db.SP_JPP_GET_Documents_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
        //            if (!lst.Any())
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "No Data Found";
        //                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //            }
        //            var newlst = (from li in lst
        //                          select new
        //                          {
        //                              ROW_NO = li.ROW_NO,
        //                              //Project = li.Project,
        //                              //Document = li.Document,
        //                              //Revno = Convert.ToString("R" + li.RevNo),
        //                              DocumentsDrawingDescription = Convert.ToString(li.DocumentsDrawingDescription),
        //                              RequiredDate = Convert.ToDateTime(li.RequiredDate).ToString("dd/MM/yyyy"),
        //                              ExpectedDate = Convert.ToDateTime(li.ExpectedDate).ToString("dd/MM/yyyy")


        //                          }).ToList();
        //            strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
        //        }
        //        else if (gridType == clsImplementationEnum.GridType.ProcurementMaterialItemLines.GetStringValue())
        //        {
        //            int headerid = Convert.ToInt32(Session["Headerid"]);
        //            var lst = db.SP_JPP_GET_Procurement_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
        //            if (!lst.Any())
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "No Data Found";
        //                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //            }
        //            var newlst = (from li in lst
        //                          select new
        //                          {
        //                              ROW_NO = li.ROW_NO,
        //                              //Project = li.Project,
        //                              //Document = li.Document,
        //                              //Revno = Convert.ToString("R" + li.RevNo),
        //                              ProcurementMaterialItem = Convert.ToString(li.ProcurementMaterialItem),
        //                              SizeQuantity = Convert.ToString(li.SizeQuantity),
        //                              RequiredDatetomeetCDD = Convert.ToDateTime(li.RequiredDatetomeetCDD).ToString("dd/MM/yyyy"),
        //                              PONodate = Convert.ToString(li.PONodate),
        //                              Vendor = Convert.ToString(li.Vendor),
        //                              ExpectedDate = Convert.ToDateTime(li.ExpectedDate).ToString("dd/MM/yyyy")

        //                          }).ToList();
        //            strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
        //        }
        //        else if (gridType == clsImplementationEnum.GridType.SubcontractingPlanLines.GetStringValue())
        //        {
        //            int headerid = Convert.ToInt32(Session["Headerid"]);
        //            var lst = db.SP_JPP_GET_SubcontractingPlan_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
        //            if (!lst.Any())
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "No Data Found";
        //                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //            }
        //            var newlst = (from li in lst
        //                          select new
        //                          {
        //                              ROW_NO = li.ROW_NO,
        //                              //Project = li.Project,
        //                              //Document = li.Document,
        //                              //Revno = Convert.ToString("R" + li.RevNo),
        //                              SubcontractingPlan = Convert.ToString(li.SubcontractingPlan)

        //                          }).ToList();
        //            strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
        //        }
        //        else if (gridType == clsImplementationEnum.GridType.SpecialAssemblyLines.GetStringValue())
        //        {

        //            int headerid = Convert.ToInt32(Session["Headerid"]);
        //            var lst = db.SP_JPP_GET_SpecialAssemblies_History(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
        //            if (!lst.Any())
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "No Data Found";
        //                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //            }
        //            var newlst = (from li in lst
        //                          select new
        //                          {
        //                              ROW_NO = li.ROW_NO,
        //                              //Project = li.Project,
        //                              //Document = li.Document,
        //                              //Revno = Convert.ToString("R" + li.RevNo),
        //                              SpecialAssembly = Convert.ToString(li.SpecialAssembly)
        //                          }).ToList();
        //            strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
        //        }
        //        else if (gridType == clsImplementationEnum.GridType.DiscussionPointsLines.GetStringValue())
        //        {

        //            int headerid = Convert.ToInt32(Session["Headerid"]);
        //            var lst = db.SP_JPP_GET_DiscussionPoints_HISTORY(1, int.MaxValue, strSortOrder, headerid, whereCondition).ToList();
        //            if (!lst.Any())
        //            {
        //                objResponseMsg.Key = false;
        //                objResponseMsg.Value = "No Data Found";
        //                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //            }
        //            var newlst = (from li in lst
        //                          select new
        //                          {
        //                              ROW_NO = li.ROW_NO,
        //                              //Project = li.Project,
        //                              //Document = li.Document,
        //                              //Revno = Convert.ToString("R" + li.RevNo),
        //                              DiscussionPoints = Convert.ToString(li.DiscussionPoints)
        //                          }).ToList();
        //            strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
        //        }
        //        objResponseMsg.Key = true;
        //        objResponseMsg.Value = strFileName;
        //        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        objResponseMsg.Key = false;
        //        objResponseMsg.Value = "Error in excel generate, Please try again";
        //        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        //    }
        //}

        [HttpPost]
        public JsonResult LoadHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string Project = param.Project;

                string Location = param.Location;

                string role = param.Roles.ToLower();
                var IsDisplayOnly = !string.IsNullOrEmpty(Request["IsDisplayOnly"]) ? Convert.ToBoolean(Request["IsDisplayOnly"].ToString()) : false;
                string whereCondition = "1=1";

                whereCondition += " and Projectt='" + Project + "' and Location = '" + Location + "' and RevNo != (select Max(RevNo) FROM [WMI001] where project='" + Project + "')";


                //else
                //    whereCondition += " and status in ('" + clsImplementationEnum.PlanningDinStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.PlanningDinStatus.RELEASED.GetStringValue() + "')";

                //whereCondition += IsDisplayOnly ? (" and isLatest =1 ") : "";

                string[] columnName = { "Project", "Location", "Status", "CreatedBy", "CONVERT(nvarchar(20),CreatedOn,103)", "RevNo" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstPam = WMIDB.SP_WMI_GETHEADERDETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstPam.Select(i => i.TotalCount).FirstOrDefault();

                var res = (from h in lstPam
                           select new[] {
                               Convert.ToString(h.HeaderId),
                               Convert.ToString(h.Project),
                               Convert.ToString(h.Location),
                               Convert.ToString(h.Status),
                               Convert.ToString("R"+h.RevNo),
                               Convert.ToString(h.CreatedBy),
                               h.CreatedOn.Value.ToShortDateString(),
                                "<center>"+ (Helper.GenerateActionIcon(h.HeaderId, "View", "View Detail", "fa fa-eye", "", WebsiteURL + "/WMI/MaintainWMI/WMIDetails?id="+h.HeaderId+"&role="+role ,false))
                               //+
                               //(h.RevNo > 0 ||(h.RevNo==0 && h.Status==clsImplementationEnum.CommonStatus.Approved.GetStringValue()) ? HTMLHistoryString(h.HeaderId,h.Status,"History","History","iconspace fa fa-history","GetHistoryDetails(\""+ h.Project.Split('-')[0].Trim() +"\",\""+h.Location+"\");") + "" : "<i title='History' style = '' class='disabledicon fa fa-history'></i>")

                               +"<i title='Show Timeline'  href='javascript:void(0)' onclick=ShowTimeline('/WMI/MaintainWMI/ShowTimeline?HeaderId=" + Convert.ToInt32(h.HeaderId) + "')  class='iconspace fa fa-clock-o' ></i>"
                                +Helper.GenerateActionIcon(h.HeaderId, "Print", "Print Detail", "fa fa-print","printReport("+h.HeaderId+")","" ,false)
                               +"</center>"
                    }).ToList();
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                });
            }
        }

        public ActionResult ShowTimeline(int HeaderId)
        {
            TimelineViewModel model = new TimelineViewModel();
            //model.Title = "WMI";
            //model.TimelineTitle = "WMI Timeline";

            if (HeaderId > 0)
            {
                WMI001 objWMI001 = WMIDB.WMI001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                model.CreatedBy = Manager.GetUserNameFromPsNo(objWMI001.CreatedBy);
                model.CreatedOn = objWMI001.CreatedOn;

                model.EditedBy = Manager.GetUserNameFromPsNo(objWMI001.EditedBy);
                model.EditedOn = objWMI001.EditedOn;

                model.SubmittedBy = Manager.GetUserNameFromPsNo(objWMI001.SubmittedBy);
                model.SubmittedOn = objWMI001.SubmittedOn;

                model.ReturnedBy = Manager.GetUserNameFromPsNo(objWMI001.ReturnedBy);
                model.ReturnedOn = objWMI001.ReturnedOn;

                model.ApprovedBy = Manager.GetUserNameFromPsNo(objWMI001.ApprovedBy);
                model.ApprovedOn = objWMI001.ApprovedOn;

            }
            else
            {
                WMI001 objWMI001 = WMIDB.WMI001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.CreatedBy = objWMI001.CreatedBy != null ? Manager.GetUserNameFromPsNo(objWMI001.CreatedBy) : null;
                model.CreatedOn = objWMI001.CreatedOn;
                model.EditedBy = objWMI001.EditedBy != null ? Manager.GetUserNameFromPsNo(objWMI001.EditedBy) : null;
                model.EditedOn = objWMI001.EditedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }
        [SessionExpireFilter]
        public ActionResult WMIDetails(int? id, string role)
        {
            ViewBag.Role = role;

            var objclsLoginInfo = (clsLoginInfo)System.Web.HttpContext.Current.Session[clsImplementationMessage.Session.LoginInfo];
            if (objclsLoginInfo != null)
            {
                int Cnt = 0;
                if (objclsLoginInfo.listMenuResult != null)
                {
                    if (role != null && role.ToLower() == "maintain")
                    {
                        Cnt = objclsLoginInfo.listMenuResult.Where(x => x.Process == "Maintain WMI").ToList().Count();
                        if (Cnt == 0)
                            return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                    }
                    if (role != null && role.ToLower() == "approve")
                    {
                        Cnt = objclsLoginInfo.listMenuResult.Where(x => x.Process == "Approve WMI").ToList().Count();
                        if (Cnt == 0)
                            return RedirectToAction("AccessDenied", "Authenticate", new { area = "Authentication" });
                    }
                }
            }

            ViewBag.IsDisplayOnly = false;
            ViewBag.ControllerURL = ControllerURL;
            ViewBag.isEdit = false;
            ViewBag.lstWMI002 = null;
            ViewBag.lstWMI003 = null;
            ViewBag.lstWMI004 = null;
            ViewBag.lstWMI005 = null;
            ViewBag.lstWMI006 = null;
            ViewBag.lstWMI007 = null;
            ViewBag.lstWMI008 = null;
            ViewBag.lstWMI009 = null;
            ViewBag.CanCreateRevision = false;

            WMI001 objWMI001 = null;

            int? HeaderId = id.HasValue ? id : 0;
            if (HeaderId > 0)
            {
                objWMI001 = WMIDB.WMI001.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                int maxRevNo = WMIDB.WMI001.Where(x => x.Project == objWMI001.Project && x.Location == objWMI001.Location).Select(x => x.RevNo).Max().Value;
                if (objWMI001.RevNo == maxRevNo)
                    ViewBag.CanCreateRevision = true;

                //ViewBag.Project = Manager.GetProjectAndDescription(objWMI001.Project);
                //var MaxIssueNoHeaderId = WMIDB.WDN001.Where(x => x.Project == objWMI001.Project && x.Location == objWMI001.Location).Max(i => i.HeaderId);
                //ViewBag.IsMaxIssueNoHeaderId = MaxIssueNoHeaderId == objWMI001.HeaderId;
                #region BindLines

                List<WMI002> lstWMI002 = WMIDB.WMI002.Where(x => x.HeaderId == HeaderId).ToList();
                List<WMI003> lstWMI003 = WMIDB.WMI003.Where(x => x.HeaderId == HeaderId).ToList();
                List<WMI004> lstWMI004 = WMIDB.WMI004.Where(x => x.HeaderId == HeaderId).ToList();
                List<WMI005> lstWMI005 = WMIDB.WMI005.Where(x => x.HeaderId == HeaderId).ToList();
                List<WMI006> lstWMI006 = WMIDB.WMI006.Where(x => x.HeaderId == HeaderId).ToList();
                List<WMI007> lstWMI007 = WMIDB.WMI007.Where(x => x.HeaderId == HeaderId).ToList();
                List<WMI008> lstWMI008 = WMIDB.WMI008.Where(x => x.HeaderId == HeaderId).ToList();
                List<WMI009> lstWMI009 = WMIDB.WMI009.Where(x => x.HeaderId == HeaderId).ToList();

                ViewBag.lstWMI002 = lstWMI002;
                ViewBag.lstWMI003 = lstWMI003;
                ViewBag.lstWMI004 = lstWMI004;
                ViewBag.lstWMI005 = lstWMI005;
                ViewBag.lstWMI006 = lstWMI006;
                ViewBag.lstWMI007 = lstWMI007;
                ViewBag.lstWMI008 = lstWMI008;
                ViewBag.lstWMI009 = lstWMI009;
                #endregion
            }
            else
            {
                objWMI001 = new WMI001();
                //objWMI001.IssueNo = 1;
                objWMI001.Status = clsImplementationEnum.WMIStatus.Draft.GetStringValue();
                //ViewBag.IsMaxIssueNoHeaderId = false;
            }

            if ((objWMI001.Status == "Draft" || objWMI001.Status == "Returned") && role == "maintain")
            {
                ViewBag.isEdit = true;
            }

            return View(objWMI001);
        }

        [SessionExpireFilter, HttpPost]
        public ActionResult SaveNewRecord(WMI001 model)
        {
            WMI001 objWMI001 = null;
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string action = Convert.ToString(Request["Action"]);
                if (model.HeaderId > 0)
                {
                    objWMI001 = WMIDB.WMI001.Where(x => x.HeaderId == model.HeaderId).FirstOrDefault();
                    objWMI001.ProjectDesc = model.ProjectDesc;
                    objWMI001.Note1 = model.Note1;
                    objWMI001.Note2 = model.Note2;
                    objWMI001.Note3 = model.Note3;
                    objWMI001.Note4 = model.Note4;
                    objWMI001.Note5 = model.Note5;
                    objWMI001.Note6 = model.Note6;
                    objWMI001.EMail = model.EMail;

                    if (action == "Submit")
                    {
                        objWMI001.Status = clsImplementationEnum.WMIStatus.SendToApproval.GetStringValue();
                        objWMI001.SubmittedBy = objClsLoginInfo.UserName;
                        objWMI001.SubmittedOn = DateTime.Now;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.RecordSave.ToString();

                    }
                    else if (action == "Approve")
                    {
                        objWMI001.Remark = model.Remark;
                        objWMI001.Status = clsImplementationEnum.WMIStatus.Approved.GetStringValue();
                        objWMI001.EMail = model.EMail;
                        objWMI001.ApprovedBy = objClsLoginInfo.UserName;
                        objWMI001.ApprovedOn = DateTime.Now;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Approve.ToString();

                        #region Send Mail
                        List<string> emailTo = new List<string>();
                        if (!string.IsNullOrWhiteSpace(objWMI001.EMail))
                        {
                            foreach (var person in objWMI001.EMail.Split(','))
                            {
                                emailTo.Add(Manager.GetMailIdFromPsNo(person));
                            }
                        }

                        string Project = Convert.ToString(objWMI001.Project);
                        #region generate pdf

                        string filenameformat = Convert.ToString(objWMI001.Project);
                        string downloadpath = "~/Resources/WMIEmailAttachments/";
                        string Reporturl = "/WMI/WMI Details";

                        List<SSRSParam> reportParams = new List<SSRSParam>();
                        reportParams.Add(new SSRSParam { Param = "HeaderId", Value = Convert.ToString(objWMI001.HeaderId) });

                        //string enumICSReportImageURL = clsImplementationEnum.ConfigParameter.ExternalImagePath.GetStringValue();

                        //string ICSReportImageURL = Manager.GetConfigValue(enumICSReportImageURL).ToString();

                        string reportFilename = (new Utility.Controllers.GeneralController()).GeneratePDFfromSSRSreport(Server.MapPath(downloadpath + filenameformat + ".pdf"), Reporturl, reportParams);
                        //string reportImageFilename = (new Utility.Controllers.GeneralController()).GenerateUploadImagefromSSRSreport(Server.MapPath(downloadpath + filenameformat + ".jpeg"), Reporturl, reportParams);

                        string folderpath = "WMI001/" + objWMI001.HeaderId;
                        var doc = (new clsFileUpload()).GetDocuments(folderpath, true).ToList();

                        #endregion

                        if (emailTo.Count > 0)
                        {
                            try
                            {
                                //Hashtable _ht = new Hashtable();
                                EmailSend _objEmail = new EmailSend();
                                //_ht["[CC]"] = ccTo;
                                //_ht["[ICSNo]"] = ICSNo;

                                //_ht["[FromEmail]"] = Manager.GetUserNameFromPsNo(emailfrom);
                                //_ht["[ProjectNo]"] = objICS001.QualityProject;

                                System.Net.Mail.AttachmentCollection lstAttch = _objEmail.MailAttachMentCollection;

                                //System.Net.Mail.Attachment att = new System.Net.Mail.Attachment(reportImageFilename);
                                string contentID = Project;
                                //att.ContentId = contentID;

                                //_ht["[ICSReportPath]"] = "cid:" + contentID;
                                //lstAttch.Add(att);

                                lstAttch.Add(new System.Net.Mail.Attachment(reportFilename));
                                foreach (var item in doc)
                                {
                                    var client = new System.Net.WebClient();
                                    var UserName = System.Configuration.ConfigurationManager.AppSettings["File_Upload_UserID"];
                                    var Password = System.Configuration.ConfigurationManager.AppSettings["File_Upload_Password"];
                                    var NetworkDomain = System.Configuration.ConfigurationManager.AppSettings["NetworkDomain"];

                                    client.Credentials = new NetworkCredential(UserName, Password, NetworkDomain);
                                    client.DownloadFile(item.URL, Server.MapPath(downloadpath + item.Name));
                                    Attachment attachment = new Attachment(Server.MapPath(downloadpath + item.Name));
                                    lstAttch.Add(attachment);
                                }
                                MAIL001 objTemplateMaster = new MAIL001();
                                objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.WMI.WMIApproval).SingleOrDefault();
                                _objEmail.MailToAdd = string.Join(",", emailTo.Distinct());
                                _objEmail.MailAttachMentCollection = lstAttch;
                                _objEmail.SendMail(_objEmail, null, objTemplateMaster);
                            }
                            catch (Exception ex)
                            {
                                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                            }
                        }
                        #endregion

                    }
                    else if (action == "Return")
                    {
                        objWMI001.Remark = model.Remark;
                        objWMI001.Status = clsImplementationEnum.WMIStatus.Returned.GetStringValue();
                        objWMI001.EMail = model.EMail;
                        objWMI001.ReturnedBy = objClsLoginInfo.UserName;
                        objWMI001.ReturnedOn = DateTime.Now;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Return.ToString();
                    }
                    if (action == "Submit" || action == "SaveAndDraft")
                    {
                        objWMI001.EditedBy = objClsLoginInfo.UserName;
                        objWMI001.EditedOn = DateTime.Now;
                        if (action == "SaveAndDraft")
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.RecordSave.ToString();

                        }
                    }

                    if (action == "Revision")
                    {
                        var Revision = WMIDB.WMI001.Where(x => x.HeaderId == model.HeaderId).Select(x => x.RevNo).Max();
                        objWMI001.RevNo = Revision + 1;
                        objWMI001.Status = "Draft";
                        objWMI001.CreatedOn = DateTime.Now;
                        objWMI001.CreatedBy = objClsLoginInfo.UserName;
                        objWMI001.EditedBy = objClsLoginInfo.UserName;
                        objWMI001.EditedOn = DateTime.Now;
                        objWMI001.ApprovedBy = null;
                        objWMI001.ApprovedOn = null;
                        objWMI001.ReturnedBy = null;
                        objWMI001.ReturnedOn = null;
                        objWMI001.SubmittedBy = null;
                        objWMI001.SubmittedOn = null;
                        WMIDB.WMI001.Add(objWMI001);
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Revision.ToString();


                    }


                    WMIDB.SaveChanges();

                    #region Send Notification
                    if (action == "Return")
                    {
                        string struser = Manager.GetUserNameFromPsNo(objWMI001.ReturnedBy);
                        (new clsManager()).SendNotification(null, objWMI001.Project, "", "", "WMI: Request for " + objWMI001.Project + " has been Returned by " + struser, clsImplementationEnum.NotificationType.Information.GetStringValue(), "", objWMI001.CreatedBy);
                    }
                    else if (action == "Submit")
                    {
                        clsManager objManager = new clsManager();
                        string[] psnolist = objManager.getEmployeeFromRole(clsImplementationEnum.UserRoleName.WE2.GetStringValue(), objWMI001.Location).Select(x => x.psnum).ToArray();
                        string strPSNoList = string.Join(",", psnolist);
                        (new clsManager()).SendNotification(null, objWMI001.Project, "", "", "WMI: Request for " + objWMI001.Project + " has been submitted, please review and approve.", clsImplementationEnum.NotificationType.Information.GetStringValue(), "", strPSNoList);
                        //(new clsManager()).SendNotification(null, objWMI001.Project, "", "", "WMI: Request for " + objWMI001.Project + " are submitted, please review and approve.", clsImplementationEnum.NotificationType.Information.GetStringValue(), "", strPSNoList);
                    }
                    #endregion
                    objResponseMsg.HeaderId = objWMI001.HeaderId;


                }
                else
                {
                    string project = model.Project.ToString();
                    string Location = model.Location.ToString();
                    if (WMIDB.WMI001.Any(x => x.Project.ToLower() == project && x.Location == Location))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate.ToString();
                        //objResponseMsg.HeaderId = objWMI001.HeaderId;
                    }
                    else
                    {
                        objWMI001 = new WMI001();
                        objWMI001.Project = model.Project;
                        objWMI001.ProjectDesc = model.ProjectDesc;
                        objWMI001.Status = model.Status;
                        objWMI001.Location = model.Location;
                        objWMI001.RevNo = 0;
                        objWMI001.Note1 = "Notes:\n1.Heat No. of PTC material shall be any one of the heats used in the pressure parts of the same equipment.\n2.Heat treatment condition of PTC material shall be the same as used in pressure parts.\n3.The requirement is for one equipment only.";
                        //objWMI001.Note1 = "Default Nots:\n1.Heat No. of PTC material ";
                        objWMI001.Note2 = "Notes:\n1.This requirement is applicable to all equipment";
                        objWMI001.Note3 = "Notes:\n1.This requirement is applicable to all equipment";
                        objWMI001.Note4 = "Notes:\n1.This requirement is applicable to all equipment";
                        objWMI001.Note5 = "Notes:\n1.WEP mentioned above related to that mentioned in procedure no. HE-HZMC-WEL-S-011 on HZW Standards";
                        objWMI001.Note6 = "Notes:\n1. “X” hrs to be calculated as per customer specification.\n2.Cl. 8 of F674-MC - SPEC - MC3 - 123C - ACEAR requires LAS # CS weld to be subjected to LAS temperature. To be taken up for TQ.\n3.For maximum PWHT include ramp up / down time temperature as below (to be calculated as per HF parameter): Temperature: 690 °C ROH / ROC: 50 °C / Hr.\n4.This simulation is not required if tube sheet is subject to PWHT along with channel assembly before drilling.";
                        //objWMI001.Note5 = "Default Nots:\n1.WEP mentioned above related to ";
                        objWMI001.CreatedOn = DateTime.Now;
                        objWMI001.CreatedBy = objClsLoginInfo.UserName;
                        objWMI001.EditedBy = objClsLoginInfo.UserName;
                        objWMI001.EditedOn = DateTime.Now;
                        WMIDB.WMI001.Add(objWMI001);
                        WMIDB.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert.ToString();
                        objResponseMsg.HeaderId = objWMI001.HeaderId;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg);
        }


        [HttpPost]
        //public JsonResult WMI002Linkage(FormCollection form, int HeaderId)
        public JsonResult WMI002Linkage(List<WMI002> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<WMI002> lstAddWMI002 = new List<WMI002>();
                List<WMI002> lstDeleteWMI002 = new List<WMI002>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        WMI002 obj = WMIDB.WMI002.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null || item.CreatedBy == "Revision")
                        {
                            obj = new WMI002();
                            obj.HeaderId = HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.RestrictionDetail = item.RestrictionDetail;
                        obj.Limit = item.Limit;
                        obj.Remark = item.Remark;

                        if (isAdded)
                        {
                            lstAddWMI002.Add(obj);
                        }
                    }
                }
                if (lstAddWMI002.Count > 0)
                {
                    WMIDB.WMI002.AddRange(lstAddWMI002);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeleteWMI002 = WMIDB.WMI002.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeleteWMI002 = WMIDB.WMI002.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeleteWMI002.Count > 0)
                {
                    WMIDB.WMI002.RemoveRange(lstDeleteWMI002);
                }
                WMIDB.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult WMI003Linkage(List<WMI003> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<WMI003> lstAddWMI003 = new List<WMI003>();
                List<WMI003> lstDeleteWMI003 = new List<WMI003>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        WMI003 obj = WMIDB.WMI003.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null || item.CreatedBy == "Revision")
                        {
                            obj = new WMI003();
                            obj.HeaderId = HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Type = item.Type;
                        obj.SoakingTemperature = item.SoakingTemperature;
                        obj.SoakingTime = item.SoakingTime;
                        obj.HeatingCoolingRate = item.HeatingCoolingRate;
                        obj.Applicability = item.Applicability;

                        if (isAdded)
                        {
                            lstAddWMI003.Add(obj);
                        }
                    }
                }
                if (lstAddWMI003.Count > 0)
                {
                    WMIDB.WMI003.AddRange(lstAddWMI003);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeleteWMI003 = WMIDB.WMI003.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeleteWMI003 = WMIDB.WMI003.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeleteWMI003.Count > 0)
                {
                    WMIDB.WMI003.RemoveRange(lstDeleteWMI003);
                }
                WMIDB.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult WMI004Linkage(List<WMI004> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<WMI004> lstAddWMI004 = new List<WMI004>();
                List<WMI004> lstDeleteWMI004 = new List<WMI004>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        WMI004 obj = WMIDB.WMI004.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null || item.CreatedBy == "Revision")
                        {
                            obj = new WMI004();
                            obj.HeaderId = HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.Component = item.Component;
                        obj.WeldingProcess = item.WeldingProcess;
                        obj.OverlayThickness = item.OverlayThickness;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddWMI004.Add(obj);
                        }
                    }
                }
                if (lstAddWMI004.Count > 0)
                {
                    WMIDB.WMI004.AddRange(lstAddWMI004);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeleteWMI004 = WMIDB.WMI004.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeleteWMI004 = WMIDB.WMI004.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeleteWMI004.Count > 0)
                {
                    WMIDB.WMI004.RemoveRange(lstDeleteWMI004);
                }
                WMIDB.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult WMI005Linkage(List<WMI005> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<WMI005> lstAddWMI005 = new List<WMI005>();
                List<WMI005> lstDeleteWMI005 = new List<WMI005>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        WMI005 obj = WMIDB.WMI005.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null || item.CreatedBy == "Revision")
                        {
                            obj = new WMI005();
                            obj.HeaderId = HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.MaterialSpecification = item.MaterialSpecification;
                        obj.Size = item.Size;
                        obj.Qty = item.Qty;
                        obj.ApplicableJoint = item.ApplicableJoint;
                        obj.OtherJoint = item.OtherJoint;
                        obj.WeldingProcess = item.WeldingProcess;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddWMI005.Add(obj);
                        }
                    }
                }
                if (lstAddWMI005.Count > 0)
                {
                    WMIDB.WMI005.AddRange(lstAddWMI005);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeleteWMI005 = WMIDB.WMI005.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeleteWMI005 = WMIDB.WMI005.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeleteWMI005.Count > 0)
                {
                    WMIDB.WMI005.RemoveRange(lstDeleteWMI005);
                }
                WMIDB.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult WMI006Linkage(List<WMI006> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<WMI006> lstAddWMI006 = new List<WMI006>();
                List<WMI006> lstDeleteWMI006 = new List<WMI006>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        WMI006 obj = WMIDB.WMI006.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null || item.CreatedBy == "Revision")
                        {
                            obj = new WMI006();
                            obj.HeaderId = HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.MaterialSpecification = item.MaterialSpecification;
                        obj.Size = item.Size;
                        obj.Qty = item.Qty;
                        obj.HTCondition = item.HTCondition;
                        obj.ArrivalDate = item.ArrivalDate;
                        obj.WldingProcess = item.WldingProcess;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddWMI006.Add(obj);
                        }
                    }
                }
                if (lstAddWMI006.Count > 0)
                {
                    WMIDB.WMI006.AddRange(lstAddWMI006);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeleteWMI006 = WMIDB.WMI006.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeleteWMI006 = WMIDB.WMI006.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeleteWMI006.Count > 0)
                {
                    WMIDB.WMI006.RemoveRange(lstDeleteWMI006);
                }
                WMIDB.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult WMI007Linkage(List<WMI007> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<WMI007> lstAddWMI007 = new List<WMI007>();
                List<WMI007> lstDeleteWMI007 = new List<WMI007>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        WMI007 obj = WMIDB.WMI007.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null || item.CreatedBy == "Revision")
                        {
                            obj = new WMI007();
                            obj.HeaderId = HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.MaterialSpecification = item.MaterialSpecification;
                        obj.Size = item.Size;
                        obj.Qty = item.Qty;
                        obj.HTCondition = item.HTCondition;
                        obj.ArrivalDate = item.ArrivalDate;
                        obj.WldingProcess = item.WldingProcess;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddWMI007.Add(obj);
                        }
                    }
                }
                if (lstAddWMI007.Count > 0)
                {
                    WMIDB.WMI007.AddRange(lstAddWMI007);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeleteWMI007 = WMIDB.WMI007.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeleteWMI007 = WMIDB.WMI007.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeleteWMI007.Count > 0)
                {
                    WMIDB.WMI007.RemoveRange(lstDeleteWMI007);
                }
                WMIDB.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult WMI008Linkage(List<WMI008> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<WMI008> lstAddWMI008 = new List<WMI008>();
                List<WMI008> lstDeleteWMI008 = new List<WMI008>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        WMI008 obj = WMIDB.WMI008.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null || item.CreatedBy == "Revision")
                        {
                            obj = new WMI008();
                            obj.HeaderId = HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }

                        obj.MaterialSpecification = item.MaterialSpecification;
                        obj.Size = item.Size;
                        obj.Qty = item.Qty;
                        obj.HTCondition = item.HTCondition;
                        obj.ArrivalDate = item.ArrivalDate;
                        obj.WldingProcess = item.WldingProcess;
                        obj.Remarks = item.Remarks;

                        if (isAdded)
                        {
                            lstAddWMI008.Add(obj);
                        }
                    }
                }
                if (lstAddWMI008.Count > 0)
                {
                    WMIDB.WMI008.AddRange(lstAddWMI008);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeleteWMI008 = WMIDB.WMI008.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeleteWMI008 = WMIDB.WMI008.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeleteWMI008.Count > 0)
                {
                    WMIDB.WMI008.RemoveRange(lstDeleteWMI008);
                }
                WMIDB.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult WMI009Linkage(List<WMI009> lst, int HeaderId, string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                UserName = APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName;
                List<WMI009> lstAddWMI009 = new List<WMI009>();
                List<WMI009> lstDeleteWMI009 = new List<WMI009>();

                if (lst != null)
                {
                    foreach (var item in lst)
                    {
                        bool isAdded = false;
                        WMI009 obj = WMIDB.WMI009.Where(x => x.LineId == item.LineId).FirstOrDefault();
                        if (obj == null || item.CreatedBy == "Revision")
                        {
                            obj = new WMI009();
                            obj.HeaderId = HeaderId;
                            obj.CreatedBy = UserName;
                            obj.CreatedOn = DateTime.Now;
                            isAdded = true;
                        }
                        else
                        {
                            obj.EditedBy = UserName;
                            obj.EditedOn = DateTime.Now;
                        }


                        obj.WEPNo = item.WEPNo;
                        obj.ApplicableJoints = item.ApplicableJoints;

                        if (isAdded)
                        {
                            lstAddWMI009.Add(obj);
                        }
                    }
                }
                if (lstAddWMI009.Count > 0)
                {
                    WMIDB.WMI009.AddRange(lstAddWMI009);
                }
                if (lst != null)
                {
                    var LineIds = lst.Where(x => x.LineId != 0).Select(a => a.LineId);
                    lstDeleteWMI009 = WMIDB.WMI009.Where(x => !LineIds.Contains(x.LineId) && x.HeaderId == HeaderId).ToList();
                }
                else
                {
                    lstDeleteWMI009 = WMIDB.WMI009.Where(x => x.HeaderId == HeaderId).ToList();
                }
                if (lstDeleteWMI009.Count > 0)
                {
                    WMIDB.WMI009.RemoveRange(lstDeleteWMI009);
                }
                WMIDB.SaveChanges();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.DuplicateMessage.EMessage.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult getDEForDropdown(string term = "")
        {
            //if (term != "")
            //{
            //    var lstData = (from a in db.ATH015
            //                   join b in db.COM007 on a.Employee equals b.t_emno
            //                   join c in db.COM003 on a.Employee equals c.t_psno
            //                   where (a.Employee.Contains(term) || b.t_mail.Contains(term) || c.t_name.Contains(term)) && (a.Role == 87 || a.Role == 88)
            //                   select new  { Value = a.Employee, Text = a.Employee + " - " + c.t_name + " - " + b.t_mail }).Take(10).ToList();
            //    return Json(lstData, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    var lstData = (from a in db.ATH015
            //                   join b in db.COM007 on a.Employee equals b.t_emno
            //                   join c in db.COM003 on a.Employee equals c.t_psno
            //                   where (a.Role == 87 || a.Role == 88)
            //                   select new { Value = a.Employee, Text = a.Employee + " - " + c.t_name + " - " + b.t_mail }).ToList();
            //    return Json(lstData, JsonRequestBehavior.AllowGet);
            //}
            if (term != "")
            {
                var lstData = (from a in db.ATH001
                               join b in db.COM007 on a.Employee equals b.t_emno
                               join c in db.COM003 on a.Employee equals c.t_psno
                               where (a.Employee.Contains(term) || b.t_mail.Contains(term) || c.t_name.Contains(term)) && (a.Role == 87 || a.Role == 88)
                               select new { Value = a.Employee, Text = c.t_name }).Take(10).Distinct().ToList();
                return Json(lstData, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var lstData = (from a in db.ATH001
                               join b in db.COM007 on a.Employee equals b.t_emno
                               join c in db.COM003 on a.Employee equals c.t_psno
                               where (a.Role == 87 || a.Role == 88)
                               select new { Value = a.Employee, Text = c.t_name }).Distinct().ToList();
                return Json(lstData, JsonRequestBehavior.AllowGet);
            }
        }
    }
}