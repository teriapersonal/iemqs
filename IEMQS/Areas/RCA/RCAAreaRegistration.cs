﻿using System.Web.Mvc;

namespace IEMQS.Areas.RCA
{
    public class RCAAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "RCA";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "RCA_default",
                "RCA/{controller}/{action}/{id}",
                new { action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}