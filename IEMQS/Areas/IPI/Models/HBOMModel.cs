﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IEMQS.Areas.IPI.Models
{
    public class HBOMModel
    {
        public int RowNo { get; set; }
        public string Part { get; set; }
        public int PartLevel { get; set; }
        public string ParentPart { get; set; }
        public string Description { get; set; }
        public string ItemGroup { get; set; }
        public string RevisionNo { get; set; }
        public string FindNo { get; set; }
        public string MaterialSpecification { get; set; }
        public string MaterialDescription { get; set; }
        public string Length { get; set; }
        public string Width { get; set; }
        public string Thickness { get; set; }
        public string NoOfPieces { get; set; }
        public string Weight { get; set; }
        public string UOM { get; set; }
        public string ParentSection { get; set; }
        public string ParentSectionDesc { get; set; }
        public string DrawingNo { get; internal set; }
        public string NodeColor { get; internal set; }
        public string ItemStatus { get; internal set; }
        public string ProductType { get; internal set; }

        public List<HBOMModel> PartList = new List<HBOMModel>();

    }
}