﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class MaintainICLPartController : clsBase
    {
        // GET: IPI/MaintainICLPart
        [SessionExpireFilter]
        [AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult LoadICLPartHeaderDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetHeaderGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadICLPartData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strSortOrder = string.Empty;
                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.ICLPartStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.ICLPartStatus.Returned.GetStringValue() + "')";
                }
                else
                {
                    strWhere += " 1=1";
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }



                string[] columnName = { "Project", "Location", "BU" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms35.BU", "qms35.Location");

                var lstResult = db.SP_IPI_PART_ICL_DATA
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                //Convert.ToString(uc.QualityProject),
                                Convert.ToString(uc.Project)+"-"+Convert.ToString(uc.ProjectDesc),
                                 Convert.ToString(uc.BU)+"-"+Convert.ToString(uc.BUDesc),
                                Convert.ToString(uc.Location)+"-"+Convert.ToString(uc.LocationDesc),
                                "<center><a  href='" + WebsiteURL + "/IPI/MaintainICLPart/AddHeader?HeaderID="+uc.HeaderId+"'><i class='fa fa-eye'></i></a></center>"
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LoadHeaderInnerGridDataPartial(string status, string qualityProject)
        {
            ViewBag.Status = status;
            ViewBag.qualityProject = qualityProject;
            var lstQualityIds1 = (from a in db.QMS015_Log
                                  where a.QualityProject == qualityProject && (a.QualityIdApplicableFor.ToLower() == "part" || a.QualityIdApplicableFor.ToLower() == "assembly")
                                  select new CategoryData { Value = a.QualityId, Code = a.QualityId, CategoryDescription = a.QualityId + " - " + a.QualityIdDesc }).Distinct().ToList();
            ViewBag.QualityID = lstQualityIds1;

            return PartialView("_GetHeaderInnerGridDataPartial");
        }

        [HttpPost]
        public JsonResult LoadICLPartHeaderData(JQueryDataTableParamModel param, string project, string bu, string location)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;
                if (param.CTQCompileStatus.ToUpper() == "PENDING")
                {
                    strWhere += " 1=1 and status in('" + clsImplementationEnum.ICLPartStatus.Draft.GetStringValue() + "', '" + clsImplementationEnum.ICLPartStatus.Returned.GetStringValue() + "') ";
                }
                else
                {
                    strWhere += " 1=1 ";
                }
                strWhere += string.Format(" and ( qms35.Project = '{0}' and qms35.Bu = '{1}' and qms35.Location = '{2}' ) ", project, bu, location);
                string[] columnName = { "QualityProject", "Location", "BU", "PartNo", "ParentPartNo", "QualityId", "Status" };
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                //strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms35.BU", "qms35.Location");
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_PART_ICL_HEADER_DETAILS
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.Project),
                                Convert.ToString(uc.QualityProject),
                                uc.QualityProject, //(uc.RevNo == 0 && (uc.Status.Equals(clsImplementationEnum.ICLPartStatus.Draft.GetStringValue())||uc.Status.Equals(clsImplementationEnum.ICLPartStatus.Returned.GetStringValue()))) ? GetQualityProjects(uc.QualityProject,uc.Status,uc.HeaderId):uc.QualityProject,
                                Convert.ToString(uc.PartNo),
                                Convert.ToString(uc.ChildPartNo),
                                Convert.ToString(uc.Quantity),
                                Convert.ToString(uc.ParentPartNo),
                                //Convert.ToString(uc.QualityId),
                                uc.QualityId, //((uc.RevNo == 0 && (uc.Status.Equals(clsImplementationEnum.ICLPartStatus.Draft.GetStringValue())||uc.Status.Equals(clsImplementationEnum.ICLPartStatus.Returned.GetStringValue())))?GetQID(uc.QualityProject,uc.Status,uc.QualityId,uc.HeaderId,uc.BU.Split('-')[0].Trim(),uc.Location.Split('-')[0].Trim()):uc.QualityId),
                                Convert.ToString(uc.QualityIdRevNo),
                                Convert.ToString(uc.RevNo),
                                uc.Remarks,
                                Convert.ToString(uc.Status),                                
                                "<center style=\"white-space:nowrap;\"> "+Manager.generatePartGridButton(uc.QualityProject,uc.Project, uc.BU,uc.Location,uc.PartNo,uc.ParentPartNo,uc.ChildPartNo,"Part Details","","Part Details") +"</center>"

                                


                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetSREICLPartHeaderData(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                var result = new List<string>();

                var lstLines = db.SP_IPI_PART_ICL_HEADER_DETAILS(1, 0, "", "HeaderId = " + id).Take(1).ToList();

                data = (from uc in lstLines
                        select new[] {
                                       clsImplementationEnum.SRE_CMD.Avoid.GetStringValue(),
                                       Convert.ToString(uc.HeaderId),
                                       Convert.ToString(uc.Project),
                                       Convert.ToString(uc.QualityProject),
                                       isReadOnly ? uc.QualityProject : ((uc.RevNo == 0 && (uc.Status.Equals(clsImplementationEnum.ICLPartStatus.Draft.GetStringValue())||uc.Status.Equals(clsImplementationEnum.ICLPartStatus.Returned.GetStringValue()))) ? GetQualityProjects(uc.QualityProject,uc.Status,uc.HeaderId):uc.QualityProject),
                                       Convert.ToString(uc.PartNo),
                                       Convert.ToString(uc.ChildPartNo),
                                       Convert.ToString(uc.Quantity),
                                       Convert.ToString(uc.ParentPartNo),
                                       isReadOnly ? uc.QualityId : (((uc.RevNo == 0 && (uc.Status.Equals(clsImplementationEnum.ICLPartStatus.Draft.GetStringValue())||uc.Status.Equals(clsImplementationEnum.ICLPartStatus.Returned.GetStringValue())))?GetQID(uc.QualityProject,uc.Status,uc.QualityId,uc.HeaderId,uc.BU.Split('-')[0].Trim(),uc.Location.Split('-')[0].Trim()):uc.QualityId)),
                                       Convert.ToString(uc.QualityIdRevNo),
                                       Convert.ToString(uc.RevNo),
                                       uc.Remarks,
                                       Convert.ToString(uc.Status),
                                       "<center style=\"white-space:nowrap;\"> "+Manager.generatePartGridButton(uc.QualityProject,uc.Project, uc.BU,uc.Location,uc.PartNo,uc.ParentPartNo,uc.ChildPartNo,"Part Details","","Part Details") +"</center>",
                        }).ToList();
                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GetQualityProjects(string qualityProject, string status, int headerID)
        {
            string htmlOption = "";
            if (status != clsImplementationEnum.ICLPartStatus.SendForApproval.GetStringValue())
            {
                htmlOption = "<input type='text' id='txtQualityProject" + headerID.ToString() + "' value='" + qualityProject + "' headerid = " + headerID + " status = '" + status + "' colname = 'txtQualityProject' name='txtQualityProject' class='form-control col-md-3 autocomplete ' />";
            }
            else
            {
                htmlOption = "<input type='text' readonly='readonly' id='txtQualityProject" + headerID.ToString() + "' value='" + qualityProject + "' name='txtQualityProject' class='form-control col-md-3' />";
            }

            return htmlOption;
        }

        public string GetQID(string strQProject, string status, string QualityId, int headerID, string bu = "", string loc = "")
        {
            clsManager objManager = new clsManager();
            string htmlOption = "<option value=\"\"> Select Quality Id </option>";

            List<DropdownModel> lstQIDs = objManager.GetQualityIDsByQualityProject(strQProject, "Part", bu, loc);

            foreach (var item in lstQIDs)
            {
                if (item.Code == QualityId)
                {
                    htmlOption += "<option selected value=\"" + item.Code + "\">" + item.Code + "</option>";
                }
                else
                {
                    htmlOption += "<option value=\"" + item.Code + "\">" + item.Code + "</option>";
                }
            }

            string cEvent = " onchange=\"AssignSaveHeaderAction(this, '" + status + "'," + headerID + ",'" + strQProject + "')\"";

            if (status != clsImplementationEnum.ICLPartStatus.SendForApproval.GetStringValue())
            {
                return "<select id='ddlQID" + headerID + "' " + cEvent + " name='ddlQID" + headerID + "'  class='form-control input-sm input-inline'>" + htmlOption + "</select>";
            }
            else
            {
                return "<select id='ddlQID" + headerID + "' " + cEvent + " name='ddlQID" + headerID + "' disabled class='form-control input-sm input-inline'>" + htmlOption + "</select>";
            }

        }

        #region Header Details
        [SessionExpireFilter]
        public ActionResult AddHeader(int HeaderID)
        {
            ViewBag.HeaderID = HeaderID;
            return View();
        }

        [HttpPost]
        public ActionResult LoadICLPartHeaderForm(int headerID)
        {
            QMS035 objQMS035 = new QMS035();
            if (headerID > 0)
            {
                objQMS035 = db.QMS035.Where(x => x.HeaderId == headerID).FirstOrDefault();
                ViewBag.ProjectNo = objQMS035.Project;
                objQMS035.BU = db.COM002.Where(a => a.t_dimx == objQMS035.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objQMS035.Project = db.COM001.Where(i => i.t_cprj == objQMS035.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objQMS035.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS035.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                //objQMS035.QualityId = db.QMS015.Where(a => a.QualityId == objQMS035.QualityId).Select(a => a.QualityId + " - " + a.QualityIdDesc).FirstOrDefault();
                if (!string.IsNullOrEmpty(objQMS035.QualityId))
                    ViewBag.QualityIdDesc = db.QMS015.Where(a => a.QualityId == objQMS035.QualityId).Select(a => a.QualityIdDesc).FirstOrDefault();
            }

            return PartialView("_LoadICLPartHeaderForm", objQMS035);
        }


        [HttpPost]
        public ActionResult UpdateHeader(string headerId, string qualityProject, string strQID)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                int headID = Convert.ToInt32(headerId);
                QMS035 objQMS035 = db.QMS035.Where(x => x.HeaderId == headID).FirstOrDefault();
                if (objQMS035 != null)
                {
                    if (objQMS035.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                    {
                        string ApprovedStatus = clsImplementationEnum.QualityIdStageStatus.APPROVED.GetStringValue();
                        string DraftStatus = clsImplementationEnum.ICLPartStatus.Draft.GetStringValue();
                        List<string> lstQproj = new List<string>();
                       // lstQproj.Add(objQMS035.QualityProject);
                        lstQproj.Add(qualityProject);

                        lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS035.BU && c.Location == objQMS035.Location && c.IQualityProject == objQMS035.QualityProject).Select(x => x.QualityProject).ToList());
                        QMS015_Log objQMS015_Log = db.QMS015_Log.Where(x => lstQproj.Contains(x.QualityProject) && x.QualityId == strQID && x.Status == ApprovedStatus && x.Location == objQMS035.Location && x.BU == objQMS035.BU).FirstOrDefault();

                        objQMS035.QualityProject = qualityProject;

                        objQMS035.QualityId = !string.IsNullOrWhiteSpace(strQID) ? strQID : null;

                        if (objQMS015_Log != null)
                            objQMS035.QualityIdRevNo = objQMS015_Log.RevNo;
                        else
                            objQMS035.QualityIdRevNo = null;

                        if (objQMS035.Status == ApprovedStatus)
                        {
                            objQMS035.RevNo = objQMS035.RevNo + 1;
                        }

                        // objQMS035.Status = DraftStatus;
                        objQMS035.EditedBy = objClsLoginInfo.UserName;
                        objQMS035.EditedOn = DateTime.Now;

                        db.SaveChanges();
                        objResponseMsg = CopyQIDLines(objQMS035, true);
                        if (objResponseMsg.Key == true)
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.HeaderId = objQMS035.HeaderId;
                            objResponseMsg.QIdDesc = db.QMS015.Where(a => a.QualityId == objQMS035.QualityId).Select(a => a.QualityIdDesc).FirstOrDefault();
                            objResponseMsg.QIdRevision = objQMS035.QualityIdRevNo;
                            objResponseMsg.RevNo = objQMS035.RevNo;
                            objResponseMsg.Status = objQMS035.Status;
                            objResponseMsg.Value = "Quality Id has been updated";


                        }
                        else
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.Error;
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.AlreadySubmitted;
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ResponceMsgWithHeaderID CopyQIDLines(QMS035 objSrcQMS035, bool IsEdited)
        {
            var ApproveString = clsImplementationEnum.ICLPartStatus.Approved.GetStringValue();
            var AddedString = clsImplementationEnum.ICLPartStatus.Added.GetStringValue();
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                QMS016_Log objSRCQMS016_Log = new QMS016_Log();
                var lstQMS36 = db.QMS036.Where(x => x.HeaderId == objSrcQMS035.HeaderId).ToList();

                var objQMS16 = db.QMS016_Log.Where(x => x.QualityId == objSrcQMS035.QualityId && x.Project == objSrcQMS035.Project && x.QualityProject == objSrcQMS035.QualityProject).Select(x => x.HeaderId).Distinct().FirstOrDefault();

                if (IsEdited)
                {
                    if (lstQMS36 != null && lstQMS36.Count > 0)
                    {
                        db.QMS036.RemoveRange(lstQMS36);
                    }
                    db.SaveChanges();
                    List<QMS016_Log> lstSrcQMS016_Log = db.QMS016_Log.Where(x => x.HeaderId == objQMS16 && x.RevNo == objSrcQMS035.QualityIdRevNo && x.StageStatus == ApproveString).ToList();
                    if (lstSrcQMS016_Log != null)
                    {
                        db.QMS036.AddRange(
                                       lstSrcQMS016_Log.Select(x =>
                                       new QMS036
                                       {
                                           HeaderId = objSrcQMS035.HeaderId,
                                           QualityProject = objSrcQMS035.QualityProject,
                                           Project = objSrcQMS035.Project,
                                           BU = objSrcQMS035.BU,
                                           Location = objSrcQMS035.Location,
                                           PartNo = objSrcQMS035.PartNo,
                                           ParentPartNo = objSrcQMS035.ParentPartNo,
                                           ChildPartNo = objSrcQMS035.ChildPartNo,
                                           StageCode = x.StageCode,
                                           StageSequence = db.QMS002.Where(i => i.StageCode == x.StageCode).FirstOrDefault().SequenceNo,
                                           StageStatus = AddedString,
                                           Parallel = false,
                                           AcceptanceStandard = x.AcceptanceStandard,
                                           ApplicableSpecification = x.ApplicableSpecification,
                                           InspectionExtent = x.InspectionExtent,
                                           Remarks = x.Remarks,
                                           CreatedBy = objClsLoginInfo.UserName,
                                           CreatedOn = DateTime.Now,
                                       })
                                     );
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.LineUpdate.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Details not available for this Quality Id";
                    }
                }
                else
                {
                    List<QMS016_Log> lstSrcQMS016_Log = db.QMS016_Log.Where(x => x.HeaderId == objQMS16 && x.RevNo == objSrcQMS035.QualityIdRevNo && x.StageStatus == ApproveString).ToList();
                    if (lstSrcQMS016_Log.Count > 0)
                    {
                        db.QMS036.AddRange(
                                        lstSrcQMS016_Log.Select(x =>
                                        new QMS036
                                        {
                                            HeaderId = objSrcQMS035.HeaderId,
                                            QualityProject = objSrcQMS035.QualityProject,
                                            Project = objSrcQMS035.Project,
                                            BU = objSrcQMS035.BU,
                                            Location = objSrcQMS035.Location,
                                            PartNo = objSrcQMS035.PartNo,
                                            ParentPartNo = objSrcQMS035.ParentPartNo,
                                            ChildPartNo = objSrcQMS035.ChildPartNo,
                                            StageCode = x.StageCode,
                                            StageSequence = db.QMS002.Where(i => i.StageCode == x.StageCode).FirstOrDefault().SequenceNo,
                                            StageStatus = AddedString,
                                            AcceptanceStandard = objSRCQMS016_Log.AcceptanceStandard,
                                            ApplicableSpecification = x.ApplicableSpecification,
                                            InspectionExtent = x.InspectionExtent,
                                            Remarks = x.Remarks,
                                            CreatedBy = objClsLoginInfo.UserName,
                                            CreatedOn = DateTime.Now,
                                        })
                                      );
                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.ICLPartMessages.HeaderInsert.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Details not available for this Quality Id";
                    }
                }

                objResponseMsg.HeaderId = objSrcQMS035.HeaderId;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return objResponseMsg;
        }


        #endregion

        #region Item Details

        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]
        //load partial for test plan stages
        [HttpPost]
        public ActionResult LoadPartICLInnerLineData(int headerId)
        {
            NDEModels objNDEModels = new NDEModels();
            QMS035 objQMS035 = db.QMS035.Where(x => x.HeaderId == headerId).FirstOrDefault();
            if (objQMS035 != null)
            {
                var lstAcceptanceStandard = Manager.GetSubCatagories("Acceptance Standard", objQMS035.BU, objQMS035.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
                var lstApplicableSpecification = Manager.GetSubCatagories("Applicable Specification", objQMS035.BU, objQMS035.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).ToList();
                var lstInspectionExtent = Manager.GetSubCatagories("Inspection Extent", objQMS035.BU, objQMS035.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
                string StageTypeExludedinOfferDropdown = clsImplementationEnum.ConfigParameter.StageTypeExludedinOfferDropdown.GetStringValue();
                var lstStagetypes = Manager.GetConfigValue(StageTypeExludedinOfferDropdown).ToUpper().Split(',').ToArray();

                ViewBag.StagesCode = db.QMS002.Where(i => !lstStagetypes.Contains(i.StageType) && i.BU.Equals(objQMS035.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS035.Location, StringComparison.OrdinalIgnoreCase)).OrderBy(o => o.SequenceNo).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
                ViewBag.AcceptanceStandard = lstAcceptanceStandard;
                ViewBag.ApplicableSpecification = lstApplicableSpecification;
                ViewBag.InspectionExtent = lstInspectionExtent;
                ViewBag.lstProtocolType = Manager.GetSubCatagories("Protocol Type", objQMS035.BU, objQMS035.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            }

            return PartialView("_PartICLInnerLineForm", objQMS035);
        }


        //editable grid for stages
        [HttpPost]
        public JsonResult LoadPartICLLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                NDEModels objNDEModels = new NDEModels();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();
                //string[] arrayCTQStatus = { clsImplementationEnum.CTQStatus.DRAFT.GetStringValue(), clsImplementationEnum.CTQStatus.Returned.GetStringValue() };

                string[] columnName = { "StageCode", "StageSequence", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strStatus = param.MTStatus;
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_ICL_PART_LINE_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                int newRecordId = 0;
                var newRecord = new[] {
                                        Helper.GenerateHidden(newRecordId, "HeaderId", param.CTQHeaderId),
                                        GenerateAutoCompleteonBlur(newRecordId,"txtStageCode","","getSequence(this);",false,"","StageCode")+""+Helper.GenerateHidden(newRecordId,"StageCode"),
                                        Helper.GenerateTextbox(newRecordId,"StageSequance","","CheckDuplicate(this);"),
                                        //"",
                                        //"",
                                        //"",
                                        //"",
                                        GenerateAutoComplete(newRecordId, "AcceptanceStandard","","",false,"","AcceptanceStandard")+""+Helper.GenerateHidden(newRecordId,"AcceptanceStandard"),
                                        GenerateAutoComplete(newRecordId, "ApplicableSpecification","","",false,"","ApplicableSpecification")+""+Helper.GenerateHidden(newRecordId,"ApplicableSpecification"),
                                        GenerateAutoComplete(newRecordId, "txtInspectionExtent","","",false,"","InspectionExtent")+""+Helper.GenerateHidden(newRecordId,"InspectionExtent"),
                                        Helper.GenerateTextbox(newRecordId,"Remarks"),
                                         GenerateAutoComplete(newRecordId, "txtProtocolType","","",false,"","ProtocolType")+""+Helper.GenerateHidden(newRecordId,"ProtocolType"),
                                         clsImplementationEnum.TestPlanStatus.Added.GetStringValue(),
                                        Helper.GenerateGridButton(newRecordId, "Save", "Add Record", "fa fa-save", "SaveNewStage();" ),
                                        ""
                                        };
                int hID = Convert.ToInt32(param.CTQHeaderId);
                QMS035 objQMS035 = db.QMS035.FirstOrDefault(c => c.HeaderId == hID);
                int maxOfferedSequenceNo = (new clsManager()).GetMaxOfferedSequenceNo_PART(objQMS035.QualityProject, objQMS035.BU, objQMS035.Location, objQMS035.PartNo, objQMS035.ParentPartNo);
                bool isLTFPSProject = Manager.IsLTFPSProject(objQMS035.Project, objQMS035.BU, objQMS035.Location);
                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.StageCode)+""+Helper.GenerateHidden(uc.LineId,"StageCode",uc.StageCode.Split('-')[0]),
                                Convert.ToString(uc.StageSequence),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                !string.IsNullOrWhiteSpace(uc.InspectionExtent) ? uc.InspectionExtent.Split('-')[1].Trim() : "",
                                Convert.ToString(uc.Remarks),
                                (string.Equals(strStatus,clsImplementationEnum.ICLSeamStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(uc.StageStatus,clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || (isLTFPSProject && isStageLinkedInLTFPS(uc.LineId)) || !(new MaintainSeamICLController()).isProtocolApplicable(uc.StageCode, uc.Location,uc.BU)) ? uc.ProtocolTypeDescription : GenerateAutoComplete(uc.LineId,"txtProtocolType", uc.ProtocolTypeDescription,"",false,"","ProtocolType")+""+Helper.GenerateHidden(uc.LineId,"ProtocolType", uc.ProtocolType)+"<a Title='Link Existing Protocol' onclick='LinkExistingProtocol("+uc.LineId+");'><nobr>Link Protocol</nobr></a>",
                                Convert.ToString(uc.StageStatus),
                                ((uc.StageSequence <= maxOfferedSequenceNo || uc.StageStatus.Equals(clsImplementationEnum.ICLPartStatus.Deleted.GetStringValue()) || isStageLinkedInLTFPS(uc.LineId)) ? HTMLActionString(uc.HeaderId,uc.StageStatus,"Delete","Delete Record","fa fa-trash-o","",true):string.Equals(strStatus,clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? HTMLActionString(uc.HeaderId,uc.StageStatus,"Delete","Delete Record","fa fa-trash-o","",true) : HTMLActionString(uc.HeaderId,uc.StageStatus,"Delete","Delete Record","fa fa-trash-o","DeleteRecord("+ uc.LineId +", \""+ strStatus +"\");"))
                                +"<a  href='javascript:void(0)' onclick=ShowTimeline('/IPI/MaintainICLPart/ShowTimeline?HeaderID="+Convert.ToInt32(uc.HeaderId)+"&LineId="+Convert.ToInt32(uc.LineId)+"');><i style='margin-left:5px;' class='fa fa-clock-o'></i></a>"
                               + ((isLTFPSProject && isStageLinkedInLTFPS(uc.LineId)) ?
                                HTMLActionString(uc.LineId,uc.StageStatus,"ViewProtocol","View Protocol","fa fa-file-text", uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? "OpenAttachmentPopUp("+uc.LineId+", false, \"QMS031/" + uc.LineId+"\")" : "ShowProtocol(\"" + WebsiteURL + "/PROTOCOL/" + uc.ProtocolType + "/Linkage/" + Convert.ToInt32(uc.ProtocolId) + "\")", uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? false : uc.ProtocolId == null)
                                :(new MaintainSeamICLController()).HTMLActionStringWithoutStatus(uc.LineId,"ViewProtocol","View Protocol","fa fa-file-text",string.Equals(objQMS035.Status,clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue(),StringComparison.OrdinalIgnoreCase) ? "AskForICLRevision("+ uc.HeaderId +","+ uc.LineId +",\"" + WebsiteURL + "/PROTOCOL/"+ uc.ProtocolType +"/Linkage/"+ Convert.ToInt32(uc.ProtocolId) + "\" ,\""+uc.ProtocolType+"\")" :
                                uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? "ShowOtherFiles("+uc.LineId+","+((string.Equals(objQMS035.Status,clsImplementationEnum.ICLSeamStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(uc.StageStatus,clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) ||  (isLTFPSProject && isStageLinkedInLTFPS(uc.LineId))) ? "0" : "1") + ")":"ShowProtocol(\"" + WebsiteURL + "/PROTOCOL/"+ uc.ProtocolType +"/Linkage/"+ Convert.ToInt32(uc.ProtocolId) + ((string.Equals(objQMS035.Status,clsImplementationEnum.ICLSeamStatus.SendForApproval.GetStringValue(),StringComparison.OrdinalIgnoreCase) || string.Equals(uc.StageStatus,clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue(),StringComparison.OrdinalIgnoreCase) || (isLTFPSProject && isStageLinkedInLTFPS(uc.LineId))) ? "" : "?m=1") + "\")",uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? false : uc.ProtocolId == null ))
                                //(isLTFPSProject && isStageLinkedInLTFPS(uc.LineId)) ?
                                //HTMLActionString(uc.LineId,uc.StageStatus,"ViewProtocol","View Protocol","fa fa-file-text", uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? "OpenAttachmentPopUp("+uc.LineId+", false, \"QMS031/" + uc.LineId+"\")" : "ShowProtocol(\"" + WebsiteURL + "/PROTOCOL/" + uc.ProtocolType + "/Linkage/" + Convert.ToInt32(uc.ProtocolId) + "\")", uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? false : uc.ProtocolId == null)
                                //:(new MaintainSeamICLController()).HTMLActionStringWithoutStatus(uc.LineId, "ViewProtocol", "View Protocol", "fa fa-file-text", string.Equals(objQMS035.Status, clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue(), StringComparison.OrdinalIgnoreCase) ? "AskForICLRevision(" + uc.HeaderId + "," + uc.LineId + ",\"" + WebsiteURL + "/PROTOCOL/" + uc.ProtocolType + "/Linkage/" + Convert.ToInt32(uc.ProtocolId) + "\" ,\"" + uc.ProtocolType + "\")" :
                                //uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? "ShowOtherFiles(" + uc.LineId + "," + ((string.Equals(objQMS035.Status, clsImplementationEnum.ICLSeamStatus.SendForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(uc.StageStatus, clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue(), StringComparison.OrdinalIgnoreCase) || (isLTFPSProject && isStageLinkedInLTFPS(uc.LineId))) ? "0" : "1") + ")" : "ShowProtocol(\"" + WebsiteURL + "/PROTOCOL/" + uc.ProtocolType + "/Linkage/" + Convert.ToInt32(uc.ProtocolId) + ((string.Equals(objQMS035.Status, clsImplementationEnum.ICLSeamStatus.SendForApproval.GetStringValue(), StringComparison.OrdinalIgnoreCase) || string.Equals(uc.StageStatus, clsImplementationEnum.ICLSeamStatus.Deleted.GetStringValue(), StringComparison.OrdinalIgnoreCase) || (isLTFPSProject && isStageLinkedInLTFPS(uc.LineId))) ? "" : "?m=1") + "\")", uc.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() ? false : uc.ProtocolId == null),
                           }).ToList();
                data.Insert(0, newRecord);
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult CheckDuplicateStages(string stagecode, int headerId, int stageseq)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();

            if (CheckDuplicateStage(headerId, stagecode, stageseq))
            {
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.DuplicateStage;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult CheckSequenceNo(string stagecode, int headerId, int stageseq)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            QMS035 objQMS035 = db.QMS035.Where(x => x.HeaderId == headerId).FirstOrDefault();
            int maxOfferedSequenceNo = (new clsManager()).GetMaxOfferedSequenceNo_PART(objQMS035.QualityProject, objQMS035.BU, objQMS035.Location, objQMS035.PartNo, objQMS035.ParentPartNo);
            if (stageseq <= maxOfferedSequenceNo)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Sequence no should be greater then " + maxOfferedSequenceNo;
            }
            else
                objResponseMsg.Key = true;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onchange='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' dataid=" + rowId + " " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + MaxCharcters + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public string GenerateAutoCompleteonBlur(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string maxlength = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string MaxCharcters = !string.IsNullOrEmpty(maxlength) ? "maxlength='" + maxlength + "'" : "";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + "  " + MaxCharcters + "  " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "", bool isDisable = false)
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";
            if (isDisable)
            {
                htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;opacity:0.5' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
            }
            else
            {
                if (!string.Equals(status, clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue()))
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";
                }
                else
                {
                    htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;opacity:0.5' Title='" + buttonTooltip + "' class='" + className + "' ></i>";
                }
            }


            return htmlControl;
        }


        [HttpPost]
        public JsonResult LoadICLPartLineGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.CTQHeaderId).ToString();

                string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_ICL_PART_LINE_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                //Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequence),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                Convert.ToString(uc.InspectionExtent),
                                Convert.ToString(uc.Parallel),
                                 Convert.ToString(uc.StageStatus),
                                Convert.ToString(uc.Remarks),
                                Convert.ToString(uc.LineId),
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadICLHeaderHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                strWhere += "1=1 and HeaderId=" + param.Headerid;
                string[] columnName = { "QualityProject", "Location", "QualityId", "QualityIdDesc", "Status" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_ICL_PART_HEADER_HISTORY_DETAILS
                                (
                                StartIndex, EndIndex, sortDirection, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                            Convert.ToString(uc.QualityProject),
                             Convert.ToString(uc.BU),
                            Convert.ToString(uc.PartNo),
                            Convert.ToString(uc.CreatedBy),
                             Convert.ToString(uc.Location),
                              Convert.ToString(uc.Status),
                            "<center><a class='btn btn-xs green' href='" + WebsiteURL + "/IPI/MaintainICLPart/ViewLogDetails?Id="+uc.Id+"'>View<i style='margin-left:5px;' class='fa fa-eye'></i></a></center>",
                            }).ToList();



                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ViewLogDetails(int? Id)
        {
            QMS035_Log objQMS035 = new QMS035_Log();
            if (Id > 0)
            {
                objQMS035 = db.QMS035_Log.Where(i => i.Id == Id).FirstOrDefault();
                objQMS035.BU = db.COM002.Where(a => a.t_dimx == objQMS035.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                objQMS035.Project = db.COM001.Where(i => i.t_cprj == objQMS035.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objQMS035.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS035.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
                ViewBag.Action = "edit";
            }
            return View(objQMS035);
        }


        [HttpPost]
        public ActionResult GetICLPartHistoryDetails(int Id)
        {
            QMS035_Log objQMS035Log = new QMS035_Log();
            objQMS035Log = db.QMS035_Log.Where(x => x.HeaderId == Id).FirstOrDefault();
            return PartialView("_ICLPartHistoryPartial", objQMS035Log);
        }

        [HttpPost]
        public JsonResult LoadICLLineHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += "1=1 and RefId=" + param.Headerid;

                string[] columnName = { "StageCode", "StageSequance", "AcceptanceStandard", "ApplicableSpecification", "InspectionExtent", "RevNo", "StageStatus", "Remarks", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_ICL_PART_LINE_HISTORY_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               sortDirection,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                //Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequence),
                                Convert.ToString(uc.AcceptanceStandard),
                                Convert.ToString(uc.ApplicableSpecification),
                                Convert.ToString(uc.InspectionExtent),
                                Convert.ToString(uc.Parallel),
                                Convert.ToString(uc.StageStatus),
                                Convert.ToString(uc.Remarks),
                           }).ToList();


                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [SessionExpireFilter]
        [AllowAnonymous]
        //[UserPermissions]

        [HttpPost]
        public ActionResult LoadICLPartLineData(int headerId, int lineId)
        {
            QMS035 objQMS035 = new QMS035();
            QMS036 objQMS036 = new QMS036();
            NDEModels objNDEModels = new NDEModels();

            objQMS035 = db.QMS035.Where(x => x.HeaderId == headerId).FirstOrDefault();

            ViewBag.Stages = db.QMS002.Where(i => i.BU.Equals(objQMS035.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS035.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
            var lstAcceptanceStandard = Manager.GetSubCatagories("Acceptance Standard", objQMS035.BU, objQMS035.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            var lstApplicableSpecification = Manager.GetSubCatagories("Applicable Specification", objQMS035.BU, objQMS035.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
            var lstInspectionExtent = Manager.GetSubCatagories("Inspection Extent", objQMS035.BU, objQMS035.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();

            ViewBag.AcceptanceStandard = lstAcceptanceStandard;
            ViewBag.ApplicableSpecification = lstApplicableSpecification;
            ViewBag.InspectionExtent = lstInspectionExtent;

            if (lineId > 0)
            {
                objQMS036 = db.QMS036.Where(x => x.LineId == lineId && x.CreatedBy == objClsLoginInfo.UserName).FirstOrDefault();
                ViewBag.StageSelected = db.QMS002.Where(i => i.StageCode.Equals(objQMS036.StageCode, StringComparison.OrdinalIgnoreCase)).Select(i => i.StageCode + "-" + i.StageDesc).FirstOrDefault();
                ViewBag.AcceptanceStandardSelected = objNDEModels.GetCategory(objQMS036.AcceptanceStandard).CategoryDescription;
                ViewBag.ApplicableSpecificationSelected = objNDEModels.GetCategory(objQMS036.ApplicableSpecification).CategoryDescription;
                ViewBag.InspectionExtentSelected = objNDEModels.GetCategory(objQMS036.InspectionExtent).CategoryDescription;
                ViewBag.Action = "line edit";
            }

            ViewBag.QProject = objQMS035.QualityProject;
            ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS035.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.Project = db.COM001.Where(i => i.t_cprj == objQMS035.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
            ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS035.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.PartNo = objQMS035.PartNo;
            ViewBag.ParentNo = objQMS035.ParentPartNo;
            ViewBag.ChildPartNo = objQMS035.ChildPartNo;
            ViewBag.RevNo = objQMS035.RevNo;

            return PartialView("_LoadICLPartLinesDetailsForm", objQMS036);
        }

        [HttpPost]
        public ActionResult DeleteLine(int LineID)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string headerstatus = string.Empty;
                QMS036 objQMS036 = db.QMS036.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objQMS036 != null)
                {
                    headerstatus = objQMS036.QMS035.Status;
                    if (objQMS036.QMS035.Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                    {
                        if (objQMS036.QMS035.Status == clsImplementationEnum.TestPlanStatus.Approved.GetStringValue())
                        {
                            objQMS036.QMS035.Status = clsImplementationEnum.TestPlanStatus.Draft.GetStringValue();
                            objQMS036.QMS035.RevNo = objQMS036.QMS035.RevNo + 1;
                            objQMS036.QMS035.EditedBy = objClsLoginInfo.UserName;
                            objQMS036.QMS035.EditedOn = DateTime.Now;
                        }
                        objResponseMsg.HeaderStatus = objQMS036.QMS035.Status;
                        if (objQMS036.QMS035.RevNo == 0 && (objQMS036.QMS035.Status == clsImplementationEnum.TestPlanStatus.Draft.GetStringValue() || objQMS036.QMS035.Status == clsImplementationEnum.TestPlanStatus.Returned.GetStringValue()))
                        {
                            db.QMS036.Remove(objQMS036);
                        }
                        else
                        {
                            objQMS036.StageStatus = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();
                            objQMS036.EditedBy = objClsLoginInfo.UserName;
                            objQMS036.EditedOn = DateTime.Now;
                        }
                        db.SaveChanges();
                        objResponseMsg.Key = true;

                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Delete.ToString();
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objQMS036.StageStatus = headerstatus;
                        objResponseMsg.Remarks = "This data is already submitted, it cannot be delete now!!";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ShowTimeline(int HeaderId, int LineId = 0)
        {
            TimelineViewModel model = new TimelineViewModel();
            if (LineId > 0)
            {
                QMS036 objQMS036 = db.QMS036.Where(x => x.LineId == LineId).FirstOrDefault();
                model.ApprovedBy = objQMS036.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS036.ApprovedBy) : null;
                model.ApprovedOn = objQMS036.ApprovedOn;
                model.SubmittedBy = objQMS036.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS036.SubmittedBy) : null;
                model.SubmittedOn = objQMS036.SubmittedOn;
                model.CreatedBy = objQMS036.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS036.CreatedBy) : null;
                model.CreatedOn = objQMS036.CreatedOn;
                model.EditedBy = objQMS036.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS036.EditedBy) : null;
                model.EditedOn = objQMS036.EditedOn;
            }
            else
            {
                QMS035 objQMS035 = db.QMS035.Where(x => x.HeaderId == HeaderId).FirstOrDefault();
                model.ApprovedBy = objQMS035.ApprovedBy != null ? Manager.GetUserNameFromPsNo(objQMS035.ApprovedBy) : null;
                model.ApprovedOn = objQMS035.ApprovedOn;
                model.SubmittedBy = objQMS035.SubmittedBy != null ? Manager.GetUserNameFromPsNo(objQMS035.SubmittedBy) : null;
                model.SubmittedOn = objQMS035.SubmittedOn;
                model.CreatedBy = objQMS035.CreatedBy != null ? Manager.GetUserNameFromPsNo(objQMS035.CreatedBy) : null;
                model.CreatedOn = objQMS035.CreatedOn;
                model.EditedBy = objQMS035.EditedBy != null ? Manager.GetUserNameFromPsNo(objQMS035.EditedBy) : null;
                model.EditedOn = objQMS035.EditedOn;
                model.ReturnedBy = objQMS035.ReturnedOn != null ? Manager.GetUserNameFromPsNo(objQMS035.ReturnedBy) : null;
                model.ReturnedOn = objQMS035.ReturnedOn;
            }

            return PartialView("~/Views/Shared/_TimelineProgress2.cshtml", model);
        }
        [HttpPost]
        public ActionResult LoadResequenceData(int headerId)
        {
            QMS035 objQMS035 = new QMS035();
            objQMS035 = db.QMS035.Where(x => x.HeaderId == headerId).FirstOrDefault();
            return PartialView("_LoadResequenceData", objQMS035);
        }

        [HttpPost]
        public JsonResult LoadResequenceGridData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = "";
                strWhere += Manager.MakeWhereConditionForCTQHeaderLines(param.Headerid).ToString();
                int HeaderId = Convert.ToInt32(param.Headerid);
                QMS035 objQMS035 = db.QMS035.Where(x => x.HeaderId == HeaderId).FirstOrDefault();

                string[] columnName = { "StageCode", "StageSequance", "NewSequance", "Status", "LineId", };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);
                int maxOfferedSequenceNo = (new clsManager()).GetMaxOfferedSequenceNo_PART(objQMS035.QualityProject, objQMS035.BU, objQMS035.Location, objQMS035.PartNo, objQMS035.ParentPartNo);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {

                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_ICL_PART_RESEQUENCING_DETAILS
                                (
                               StartIndex,
                               EndIndex,
                               strSortOrder,
                               strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                                Convert.ToString(uc.ROW_NO),
                                Convert.ToString(uc.StageCode),
                                Convert.ToString(uc.StageSequence),
                                uc.StageSequence <= maxOfferedSequenceNo?Convert.ToString(uc.NewSequence): getNewSequence(uc.NewSequence),
                                Convert.ToString(uc.LineId),
                                Convert.ToString(uc.HeaderId),
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SendForApproval(string strHeader)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            List<QMS035> lstQualityIds = new List<QMS035>();
            List<string> lstQIDWithoutLines = new List<string>();
            List<string> lstQIDNotDraft = new List<string>();
            List<string> lstQIDSubmitted = new List<string>();
            List<string> lstNotApplicableForParallel = new List<string>();
            List<string> lstTPIStagesSkipped = new List<string>();
            List<string> lstTPIStagesforPartSkipped = new List<string>();
            List<string> lstStagesForProtocol = new List<string>();
            List<string> lstRequireProtocol = new List<string>();
            string deleted = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();

            try
            {
                if (!string.IsNullOrEmpty(strHeader))
                {
                    int[] headerIds = Array.ConvertAll(strHeader.Split(','), s => int.Parse(s));
                    lstQualityIds = db.QMS035.Where(i => headerIds.Contains(i.HeaderId)).ToList();

                    foreach (var item in lstQualityIds)
                    {
                        var qms35 = db.QMS035.Where(i => i.HeaderId == item.HeaderId).FirstOrDefault();
                        if (qms35 != null)
                        {
                            if (string.Equals(qms35.Status, clsImplementationEnum.ICLPartStatus.Draft.GetStringValue()) || string.Equals(qms35.Status, clsImplementationEnum.ICLPartStatus.Returned.GetStringValue()))
                            {
                                if (db.QMS036.Where(i => i.HeaderId == qms35.HeaderId).Any())
                                {
                                    List<string> lstRequireStages = new List<string>();
                                    if (!Manager.IsProtocolRequiredForStageInPart(qms35, out lstRequireStages))
                                    {
                                        if (Manager.IsApplicableForParallelStageForPartICL(qms35.Project, qms35.QualityProject, qms35.BU, qms35.Location, qms35.PartNo))
                                        {
                                            var lstQMS036 = (from dbo in db.QMS036
                                                             where dbo.HeaderId == qms35.HeaderId && dbo.StageStatus != deleted
                                                             select dbo.StageCode.Trim()).Distinct().ToArray();
                                            bool isStageExists = true;
                                            UpdateTPIStagesInICL(qms35.HeaderId);

                                            var lstQMS025 = (from dbo in db.QMS025
                                                             where dbo.Location == qms35.Location && dbo.Project == qms35.Project && dbo.QualityProject == qms35.QualityProject && dbo.BU == qms35.BU
                                                             select dbo.Stage.Trim()).ToList();
                                            List<string> stageType = new List<string>() { "PT", "MT", "RT", "UT" };

                                            foreach (var lst in lstQMS036)
                                            {
                                                var sType = db.QMS002.Where(x => x.Location == qms35.Location && x.BU == qms35.BU && x.StageCode == lst).Select(x => x.StageType).FirstOrDefault();
                                                if (stageType.Contains(sType))
                                                {
                                                    if (!lstQMS025.Contains(lst))
                                                    {
                                                        lstTPIStagesSkipped.Add(lst);
                                                        lstTPIStagesforPartSkipped.Add(qms35.PartNo);
                                                        isStageExists = false;
                                                    }
                                                }
                                            }
                                            if (isStageExists)
                                            {
                                                qms35.Status = clsImplementationEnum.ICLPartStatus.SendForApproval.GetStringValue();
                                                qms35.SubmittedBy = objClsLoginInfo.UserName;
                                                qms35.SubmittedOn = DateTime.Now;
                                                qms35.ReturnedBy = null;
                                                qms35.ReturnedOn = null;
                                                qms35.Remarks = null;
                                                db.QMS036.Where(i => i.HeaderId == qms35.HeaderId).ToList().ForEach(i => { i.SubmittedBy = objClsLoginInfo.UserName; i.SubmittedOn = DateTime.Now; });
                                                db.SaveChanges();

                                                #region Send Notification
                                                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC2.GetStringValue(), qms35.Project, qms35.BU, qms35.Location, "ICL for Part: " + qms35.PartNo + " of Project: " + qms35.QualityProject + "  has been submitted for your Approval", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/ApproveICLPart/Viewdetails?headerid=" + qms35.HeaderId);
                                                #endregion

                                                lstQIDSubmitted.Add(qms35.PartNo);
                                            }
                                        }
                                        else
                                        {
                                            lstNotApplicableForParallel.Add(qms35.PartNo);
                                        }
                                    }
                                    else
                                    {
                                        lstStagesForProtocol.AddRange(lstRequireStages);
                                        lstRequireProtocol.Add(qms35.PartNo);
                                    }

                                }
                                else
                                {
                                    lstQIDWithoutLines.Add(qms35.PartNo);
                                }
                            }
                            else
                            {
                                lstQIDNotDraft.Add(qms35.PartNo);
                            }
                        }
                    }
                    objResponseMsg.Key = true;
                    if (lstQIDSubmitted.Any())
                    {

                        objResponseMsg.Value = string.Format("ICL is sent for approval successfully");
                    }
                    if (lstQIDWithoutLines.Any())
                    {
                        objResponseMsg.WithoutLines = string.Format("No Stage(s) Exist(s) For PartNo :{0}.Please Add Stages.", string.Join(",", lstQIDWithoutLines));
                    }
                    if (lstQIDNotDraft.Any())
                    {
                        objResponseMsg.NotDraft = string.Format("PartNo(s) {0} have been skipped as PartNo(s) {0} are either Sent For Approval Or Approved", string.Join(",", lstQIDNotDraft));
                    }
                    if (lstNotApplicableForParallel.Any())
                    {
                        objResponseMsg.NotApplicableForParallel = string.Format("Parts {0} have been skipped as PT/MT cannot be the parallel stages with RT/UT", string.Join(",", lstNotApplicableForParallel));
                    }
                    if (lstTPIStagesSkipped.Any())
                    {
                        objResponseMsg.TPISkipped = string.Format("Part {0} have been skipped as TPI Agency &  Intervention not maintained for stage(s) : {1}", string.Join(",", lstTPIStagesforPartSkipped.Distinct()), string.Join(", ", lstTPIStagesSkipped.Distinct()));
                    }
                    if (lstRequireProtocol.Any())
                    {
                        objResponseMsg.RequiredProtocol = string.Format("Part {0} have been skipped as protocol not maintained/attached/linked for stage(s) : {1}", string.Join(",", lstRequireProtocol), string.Join(", ", lstStagesForProtocol.Distinct()));
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please select record";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public clsHelper.ResponseMsg UpdateTPIStagesInICL(int strHeaderId)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                string deleted = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();
                QMS036 objQMS036 = new QMS036();
                var lstQMS036 = (from dbo in db.QMS036
                                 where dbo.HeaderId == strHeaderId && dbo.StageStatus != deleted
                                 select dbo.StageCode.Trim()).Distinct().ToArray();
                //string strStage = lstQMS036[i];
                for (int j = 0; j < lstQMS036.Length; j++)
                {
                    string strStage = lstQMS036[j];
                    //QMS036 newobjQMS036 = db.QMS036.Where(x => x.StageCode == strStage && x.HeaderId == strHeaderId).FirstOrDefault();
                    var obj036 = db.QMS036.Where(x => x.StageCode == strStage && x.HeaderId == strHeaderId).ToList();
                    ////QMS025 objQMS025 = db.QMS025.Where(a => a.Location == objQMS036.Location && a.BU == objQMS036.BU && a.Project == objQMS036.Project && a.QualityProject == objQMS036.QualityProject && strStage.Contains(a.Stage)).FirstOrDefault();
                    foreach (var newobjQMS036 in obj036)
                    {
                        QMS025 objQMS025 = db.QMS025.Where(a => a.Location == newobjQMS036.Location && a.BU == newobjQMS036.BU && a.Project == newobjQMS036.Project && a.QualityProject == newobjQMS036.QualityProject && strStage.Contains(a.Stage)).FirstOrDefault();

                        if (objQMS025 != null)
                        {
                            newobjQMS036.FirstTPIAgency = objQMS025.FirstTPIAgency;
                            newobjQMS036.FirstTPIIntervention = objQMS025.FirstTPIIntervention;
                            newobjQMS036.SecondTPIAgency = objQMS025.SecondTPIAgency;
                            newobjQMS036.SecondTPIIntervention = objQMS025.SecondTPIIntervention;
                            newobjQMS036.ThirdTPIAgency = objQMS025.ThirdTPIAgency;
                            newobjQMS036.ThirdTPIIntervention = objQMS025.ThirdTPIIntervention;
                            newobjQMS036.FourthTPIAgency = objQMS025.ForthTPIAgency;
                            newobjQMS036.FourthTPIIntervention = objQMS025.ForthTPIIntervention;
                            newobjQMS036.FifthTPIAgency = objQMS025.FifthTPIAgency;
                            newobjQMS036.FifthTPIIntervention = objQMS025.FifthTPIIntervention;
                            newobjQMS036.SixthTPIAgency = objQMS025.SixthTPIAgency;
                            newobjQMS036.SixthTPIIntervention = objQMS025.SixthTPIIntervention;
                            newobjQMS036.EditedBy = objClsLoginInfo.UserName;
                            newobjQMS036.EditedOn = DateTime.Now;
                            db.SaveChanges();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return objResponseMsg;
        }
        [HttpPost]
        public ActionResult SaveNewStages(FormCollection fc)
        {
            int newRowIndex = 0;
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                int headerId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                bool key = true;
                QMS035 objQMS035 = db.QMS035.Where(x => x.HeaderId == headerId).FirstOrDefault();
                if (objQMS035.Status == clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    objResponseMsg.Status = objQMS035.Status;
                    objResponseMsg.Key = true;
                    objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.AlreadySubmitted;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                int maxOfferedSequenceNo = (new clsManager()).GetMaxOfferedSequenceNo_PART(objQMS035.QualityProject, objQMS035.BU, objQMS035.Location, objQMS035.PartNo, objQMS035.ParentPartNo);
                if (Convert.ToInt32(fc["StageSequance" + newRowIndex]) <= maxOfferedSequenceNo)
                    key = false;
                else
                    key = true;
                if (!key)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Sequence no should be greater then " + maxOfferedSequenceNo;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                if (CheckDuplicateStage(headerId, fc["StageCode" + newRowIndex], Convert.ToInt32(fc["StageSequance" + newRowIndex])))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.DuplicateStage;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                QMS036 objQMS036 = new QMS036();
                //QMS035 objQMS035 = new QMS035();

                string addedStatus = clsImplementationEnum.TestPlanStatus.Added.GetStringValue();
                string modifiedStatus = clsImplementationEnum.TestPlanStatus.Modified.GetStringValue();
                string deletedStatus = clsImplementationEnum.TestPlanStatus.Deleted.GetStringValue();

                objQMS035 = db.QMS035.Where(x => x.HeaderId == headerId).FirstOrDefault();
                objQMS036.QualityProject = objQMS035.QualityProject;
                objQMS036.Project = objQMS035.Project;
                objQMS036.HeaderId = objQMS035.HeaderId;
                objQMS036.BU = objQMS035.BU;
                objQMS036.Location = objQMS035.Location;
                objQMS036.PartNo = objQMS035.PartNo;
                objQMS036.ParentPartNo = objQMS035.ParentPartNo;
                objQMS036.ChildPartNo = objQMS035.ChildPartNo;
                objQMS036.StageCode = fc["StageCode" + newRowIndex];
                objQMS036.StageSequence = Convert.ToInt32(fc["StageSequance" + newRowIndex]);
                objQMS036.AcceptanceStandard = fc["AcceptanceStandard" + newRowIndex].Replace(",", ""); ;
                objQMS036.ApplicableSpecification = fc["ApplicableSpecification" + newRowIndex].Replace(",", ""); ;
                objQMS036.InspectionExtent = fc["InspectionExtent" + newRowIndex];
                objQMS036.Remarks = fc["Remarks" + newRowIndex];
                objQMS036.HeaderId = objQMS035.HeaderId;
                if (objQMS035.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                {
                    objQMS035.RevNo = Convert.ToInt32(objQMS035.RevNo) + 1;
                    objQMS035.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                    objQMS035.EditedBy = objClsLoginInfo.UserName;
                    objQMS035.EditedOn = DateTime.Now;
                }

                //objQMS036.RevNo = Convert.ToInt32(fc["RevNo" + newRowIndex]);
                objQMS036.StageStatus = addedStatus;
                objQMS036.CreatedBy = objClsLoginInfo.UserName;
                objQMS036.CreatedOn = DateTime.Now;
                db.QMS036.Add(objQMS036);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.LineInsert;
                objResponseMsg.Status = objQMS035.Status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.TestPlanMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult GetStageSquence(string stagecode, int headerId)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            objResponseMsg.Key = false;
            string strStage = stagecode.Trim();
            if (!string.IsNullOrWhiteSpace(stagecode))
            {
                QMS035 objQMS35 = db.QMS035.Where(i => i.HeaderId == headerId).FirstOrDefault();
                QMS002 objQMS002 = db.QMS002.Where(i => i.StageCode == stagecode && i.Location == objQMS35.Location && i.BU == objQMS35.BU).FirstOrDefault();

                if (objQMS002 != null)
                {
                    objResponseMsg.Sequence = Convert.ToInt32(objQMS002.SequenceNo);
                    objResponseMsg.Key = true;
                }
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public bool CheckDuplicateStage(int headerID, string stageCode, int stageSequence)
        {
            return db.QMS036.Any(c => c.HeaderId == headerID && c.StageCode == stageCode && c.StageSequence == stageSequence);
        }
        public string getNewSequence(int? newSequance)
        {
            if (newSequance != 0)
            {
                return "<input type='text' name='txtNewSequence'  onkeypress = 'return isNumber(event)' class='form-control input-sm input-small input-inline' value='" + newSequance + "' />";
            }
            else
            {
                return "<input type='text' name='txtNewSequence'  onkeypress = 'return isNumber(event)' class='form-control input-sm input-small input-inline' value='" + "" + "' />";
            }
        }

        [HttpPost]
        public ActionResult UpdateSequence(int lineId, int header, int newsequence)
        {
            //clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                QMS035 objQMS035 = new QMS035();
                QMS036 objQMS036 = new QMS036();
                objQMS035 = db.QMS035.Where(x => x.HeaderId == header).FirstOrDefault();
                if (objQMS035.Status != clsImplementationEnum.TestPlanStatus.SendForApproval.GetStringValue())
                {
                    objQMS036 = db.QMS036.Where(x => x.LineId == lineId).FirstOrDefault();
                    if (objQMS036 != null)
                    {
                        if (newsequence != 0)
                        {
                            if (db.QMS036.Any(c => c.HeaderId == header && c.StageCode == objQMS036.StageCode && c.StageSequence == newsequence))
                            {
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Stage with same sequence already exists";
                                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                            }
                            if (objQMS035.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                            {
                                objQMS035.RevNo = Convert.ToInt32(objQMS035.RevNo) + 1;
                                objQMS035.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                                objQMS035.EditedBy = objClsLoginInfo.UserName;
                                objQMS035.EditedOn = DateTime.Now;
                            }
                            objQMS036.StageSequence = newsequence;
                            objQMS036.StageStatus = clsImplementationEnum.ICLSeamStatus.Modified.GetStringValue();
                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Stage Sequence has been updated for Stage " + objQMS036.StageCode;
                            objResponseMsg.Status = objQMS035.Status;
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Remarks = "This data is already submitted, it cannot be delete now!!";
                    objResponseMsg.Status = objQMS035.Status;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Protocol
        [HttpPost]
        public ActionResult LoadLinkProtocolPartial(int LineId)
        {
            ViewBag.LineId = LineId;
            ViewBag.isSeam =false;
            return PartialView("~/Areas/IPI/Views/MaintainSeamICL/_LinkProtocol.cshtml");
        }

        [HttpPost]
        public JsonResult LoadLinkProtocolGridData(JQueryDataTableParamModel param)
        {
            try
            {
                NDEModels objNDEModels = new NDEModels();
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strWhere = " 1=1 ";

                string[] columnName = { "ProtocolType", "Description" };
                strWhere += columnName.MakeDatatableSearchCondition(param.sSearch);

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);


                int LineId = Convert.ToInt32(param.Headerid);
                QMS036 objQMS036 = db.QMS036.FirstOrDefault(c => c.LineId == LineId);
                if (objQMS036 != null)
                {
                    strWhere += " AND StageCode='" + objQMS036.StageCode + "' AND BU='" + objQMS036.BU + "' AND Location='" + objQMS036.Location + "'";
                }


                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_IPI_MAINTAIN_PROTOCOLS_INDEX_DATA
                                (
                                   StartIndex,
                                   EndIndex,
                                   strSortOrder,
                                   strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.ProtocolDescription),
                                Convert.ToString(uc.Description),
                                "<nobr><center>"
                                + "<input class=\"btn submit\" id=\"btnViewProtocol\" onclick=\"ViewProtocolDetails('/PROTOCOL/"+ uc.ProtocolType +"/Details/"+ uc.ProtocolHeaderId +"');\" value=\"View\" style=\"margin-right: 5px;\" type=\"button\">"
                                + "<input class=\"btn save\" id=\"btnLinkProtocol\" onclick=\"LinkProtocol("+ LineId +",'"+ uc.ProtocolType +"','"+ uc.ProtocolHeaderId +"');\" value=\"Link\" type=\"button\">"
                                 + "</center></nobr>"
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult LinkProtocol(int LineID)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS036 objQMS036 = db.QMS036.Where(x => x.LineId == LineID).FirstOrDefault();
                if (objQMS036 != null)
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Protocol linked successfully.";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Notavailable.ToString();
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult AttachedProtocol(int LineId, string ProtocolType, string ProtocolNo, string ProtocolHeaderId = "")
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                if (LineId > 0)
                {
                    int? pHeaderId = null;
                    if (!string.IsNullOrWhiteSpace(Convert.ToString(ProtocolHeaderId)))
                    {
                        pHeaderId = Convert.ToInt32(ProtocolHeaderId);
                    }

                    QMS036 objQMS036 = db.QMS036.Where(x => x.LineId == LineId).FirstOrDefault();

                    if (Convert.ToString(objQMS036.ProtocolType) == ProtocolType)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "nochanged";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    else
                    {
                        clsHelper.ProtocolHeaderData headerData = new clsHelper.ProtocolHeaderData();
                        headerData.QualityProject = objQMS036.QualityProject;
                        headerData.SeamNo = objQMS036.PartNo;
                        headerData.Project = objQMS036.Project;
                        headerData.BU = objQMS036.BU;
                        headerData.Location = objQMS036.Location;
                        headerData.StageCode = objQMS036.StageCode;
                        headerData.StageSequence = objQMS036.StageSequence.Value;
                        headerData.ICLRevNo = objQMS036.QMS035.RevNo;

                        int headerId = 0;
                        if (!string.IsNullOrWhiteSpace(ProtocolType) && ProtocolType != clsImplementationEnum.ProtocolType.Other_Files.GetStringValue() && string.IsNullOrWhiteSpace(objQMS036.ProtocolType))
                        {
                            #region Code for attach protocol
                            string role = clsImplementationEnum.ProtocolRoleType.R_Requiredvalue_for_QC.GetStringValue();
                            headerId = Manager.LinkProtocolWithStage(ProtocolType, ProtocolNo, headerData, pHeaderId, role);

                            #endregion
                        }
                        else if (string.IsNullOrWhiteSpace(ProtocolType) && !string.IsNullOrWhiteSpace(objQMS036.ProtocolType))
                        {
                            if (objQMS036.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue())
                            {
                                #region Code for remove all attached files

                                string deletefolderPath = "QMS036/" + objQMS036.LineId;
                                var lstDocs = (new clsFileUpload()).GetDocuments(deletefolderPath, true).Select(x => x.Name).ToList();
                                foreach (var filename in lstDocs)
                                {
                                    (new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.ToString());
                                }
                                headerId = -1;
                                #endregion
                            }
                            else
                            {
                                #region Code for detach old protocol

                                if (objQMS036.ProtocolId.HasValue)
                                {
                                    if (Manager.DelinkProtocolWithStage(objQMS036.ProtocolId.Value, objQMS036.ProtocolType))
                                        headerId = -1;
                                }
                                #endregion
                            }
                        }
                        else if (!string.IsNullOrWhiteSpace(ProtocolType) && !string.IsNullOrWhiteSpace(objQMS036.ProtocolType))
                        {

                            if (objQMS036.ProtocolType == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue())
                            {
                                #region Code for remove all attached files

                                string deletefolderPath = "QMS036/" + objQMS036.LineId;
                                var lstDocs = (new clsFileUpload()).GetDocuments(deletefolderPath, true).Select(x => x.Name).ToList();
                                foreach (var filename in lstDocs)
                                {
                                    (new clsFileUpload()).DeleteFileAsync(deletefolderPath, filename.ToString());
                                }
                                #endregion
                            }
                            else
                            {
                                #region Code for detach old protocol

                                if (objQMS036.ProtocolId.HasValue)
                                {
                                    Manager.DelinkProtocolWithStage(objQMS036.ProtocolId.Value, objQMS036.ProtocolType);
                                }
                                #endregion
                            }

                            if (ProtocolType != clsImplementationEnum.ProtocolType.Other_Files.GetStringValue())
                            {
                                #region Code for attach new protocol
                                string role = clsImplementationEnum.ProtocolRoleType.R_Requiredvalue_for_QC.GetStringValue();
                                headerId = Manager.LinkProtocolWithStage(ProtocolType, ProtocolNo, headerData, pHeaderId, role);

                                #endregion
                            }

                        }

                        if (Convert.ToString(ProtocolType) == clsImplementationEnum.ProtocolType.Other_Files.GetStringValue())
                        {
                            headerId = -2;
                        }

                        if (headerId != 0)
                        {
                            #region Update Seam ICL Data With Attched / Detached Protocol

                            if (objQMS036.QMS035.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                            {
                                objQMS036.QMS035.RevNo = Convert.ToInt32(objQMS036.QMS035.RevNo) + 1;
                                objQMS036.QMS035.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                                objQMS036.QMS035.EditedBy = objClsLoginInfo.UserName;
                                objQMS036.QMS035.EditedOn = DateTime.Now;
                                objQMS036.QMS035.SubmittedBy = null;
                                objQMS036.QMS035.SubmittedOn = null;
                                objQMS036.QMS035.ApprovedBy = null;
                                objQMS036.QMS035.ApprovedOn = null;
                            }
                            objQMS036.StageStatus = clsImplementationEnum.ICLSeamStatus.Modified.GetStringValue();
                            objQMS036.EditedBy = objClsLoginInfo.UserName;
                            objQMS036.EditedOn = DateTime.Now;
                            if (headerId > 0)
                            {
                                objQMS036.ProtocolId = headerId;
                                objQMS036.ProtocolType = ProtocolType;
                                objQMS036.ProtocolLinkedBy = objClsLoginInfo.UserName;
                                objQMS036.ProtocolLinkedOn = DateTime.Now;
                                objResponseMsg.Value = "protocol successfully attached";
                            }
                            else
                            {
                                if (headerId == -2)
                                {
                                    objQMS036.ProtocolId = null;
                                    objQMS036.ProtocolType = ProtocolType;
                                    objQMS036.ProtocolLinkedBy = objClsLoginInfo.UserName;
                                    objQMS036.ProtocolLinkedOn = DateTime.Now;
                                    objResponseMsg.Value = "you can attach protocol as form of files.";
                                }
                                else
                                {
                                    objQMS036.ProtocolId = null;
                                    objQMS036.ProtocolType = null;
                                    objQMS036.ProtocolLinkedBy = null;
                                    objQMS036.ProtocolLinkedOn = null;
                                    objResponseMsg.Value = "protocol successfully detached";
                                }
                            }

                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.HeaderStatus = objQMS036.QMS035.Status;
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

                            #endregion
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "Link protocol fail...!";
                            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Link protocol fail...!";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }


            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occured, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion
        [HttpPost]
        public ActionResult GetSquence(string stagecode)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            QMS002 objQMS002 = new QMS002();
            string strStage = stagecode;
            var Sequence = db.QMS002.Where(i => i.StageCode == stagecode).FirstOrDefault().SequenceNo;
            objResponseMsg.Sequence = Convert.ToInt32(Sequence);
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SaveICLPartLines(FormCollection fc, QMS036 qms036)
        {
            QMS036 objQMS036 = new QMS036();
            QMS035 objQMS035 = new QMS035();

            string addedStatus = clsImplementationEnum.ICLPartStatus.Added.GetStringValue();
            string modifiedStatus = clsImplementationEnum.ICLPartStatus.Modified.GetStringValue();
            string deletedStatus = clsImplementationEnum.ICLPartStatus.Deleted.GetStringValue();

            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                int headerID = qms036.HeaderId;
                bool IsEdited = false;
                objQMS035 = db.QMS035.Where(x => x.HeaderId == headerID).FirstOrDefault();
                if (qms036.LineId > 0)
                {
                    objQMS036 = db.QMS036.Where(x => x.LineId == qms036.LineId).FirstOrDefault();
                    IsEdited = true;
                }
                objQMS036.QualityProject = objQMS035.QualityProject;
                objQMS036.Project = objQMS035.Project;
                objQMS036.BU = objQMS035.BU;
                objQMS036.Location = objQMS035.Location;
                objQMS036.PartNo = objQMS035.PartNo;
                objQMS036.ParentPartNo = objQMS035.ParentPartNo;
                objQMS036.ChildPartNo = objQMS035.ChildPartNo;
                objQMS036.StageCode = qms036.StageCode;
                objQMS036.StageSequence = qms036.StageSequence;
                objQMS036.AcceptanceStandard = qms036.AcceptanceStandard;
                objQMS036.ApplicableSpecification = qms036.ApplicableSpecification;
                objQMS036.InspectionExtent = qms036.InspectionExtent;
                objQMS036.Remarks = qms036.Remarks;
                objQMS036.HeaderId = objQMS035.HeaderId;
                if (objQMS035.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                {
                    objQMS035.RevNo = Convert.ToInt32(objQMS035.RevNo) + 1;
                    objQMS035.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                    objQMS035.EditedBy = objClsLoginInfo.UserName;
                    objQMS035.EditedOn = DateTime.Now;
                }
                if (IsEdited)
                {
                    objQMS036.StageStatus = modifiedStatus;
                    objQMS036.EditedBy = objClsLoginInfo.UserName;
                    objQMS036.EditedOn = DateTime.Now;
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.LineUpdate;
                }
                else
                {
                    objQMS036.StageStatus = addedStatus;
                    objQMS036.CreatedBy = objClsLoginInfo.UserName;
                    objQMS036.CreatedOn = DateTime.Now;
                    db.QMS036.Add(objQMS036);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.LineInsert;
                }
                objResponseMsg.Status = objQMS035.Status;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ICLSeamMessages.Error.ToString();
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetUpdateBOMView()
        {
            ViewBag.Location = objClsLoginInfo.Location;
            ViewBag.LocationDescription = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objClsLoginInfo.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            return PartialView("_GetHBOMData");
        }

        [HttpPost]
        public ActionResult GetHBOMData(string Project, string BU, string Location)
        {
            ResponceMsgWithHeaderID objResponseMsg = new ResponceMsgWithHeaderID();
            try
            {
                db.SP_IPI_GET_BOM_PART_ICL(Project, BU, Location, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = "BOM data successfully updated.";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetProjectDetails(string project)
        {
            ProjectDetails objModel = new ProjectDetails();
            try
            {
                var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);
                objModel.BUDescription = ProjectWiseBULocation.QCPBU;
                objModel.BU = objModel.BUDescription.Split('-')[0].Trim();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }

            return Json(objModel, JsonRequestBehavior.AllowGet);
        }
        public bool isStageLinkedInLTFPS(int LineID)
        {
            var objQMS036 = db.QMS036.Where(x => x.LineId == LineID).FirstOrDefault();
            string deleted = clsImplementationEnum.LTFPSLineStatus.Deleted.GetStringValue();
            if (objQMS036 != null)
            {
                if (db.LTF002.Any(x => x.QualityProject.Equals(objQMS036.QualityProject) && x.Location.Equals(objQMS036.Location) && x.BU.Equals(objQMS036.BU) && x.PartNo.Equals(objQMS036.PartNo) && x.Stage.Equals(objQMS036.StageCode) && x.StageSequence == objQMS036.StageSequence && x.LineStatus != deleted))
                    return true;
            }
            return false;
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                if (gridType == clsImplementationEnum.GridType.MAINTAININDEX.GetStringValue())
                {
                    var lst = db.SP_IPI_PART_ICL_DATA(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                {
                    var lst = db.SP_IPI_PART_ICL_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      PartNo = li.PartNo,
                                      ParentPartNo = li.ParentPartNo,
                                      ChildPartNo = li.ChildPartNo,
                                      QualityId = li.QualityId,
                                      QualityIdRevNo = li.QualityIdRevNo,
                                      RevNo = li.RevNo,
                                      Status = li.Status,
                                      Quantity = li.Quantity,
                                      UOM = li.UOM,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      SubmittedBy = li.SubmittedBy,
                                      SubmittedOn = li.SubmittedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                      ReturnedBy = li.ReturnedBy,
                                      ReturnedOn = li.ReturnedOn,
                                      Remarks = li.Remarks
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.LINES.GetStringValue())
                {
                    var lst = db.SP_IPI_ICL_PART_LINE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      PartNo = li.PartNo,
                                      ParentPartNo = li.ParentPartNo,
                                      ChildPartNo = li.ChildPartNo,
                                      StageCode = li.StageCode,
                                      StageSequence = li.StageSequence,
                                      StageStatus = li.StageStatus,
                                      Parallel = li.Parallel,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      InspectionExtent = li.InspectionExtent,
                                      Remarks = li.Remarks,
                                      FirstTPIAgency = li.FirstTPIAgency,
                                      FirstTPIIntervention = li.FirstTPIIntervention,
                                      SecondTPIAgency = li.SecondTPIAgency,
                                      SecondTPIIntervention = li.SecondTPIIntervention,
                                      ThirdTPIAgency = li.ThirdTPIAgency,
                                      ThirdTPIIntervention = li.ThirdTPIIntervention,
                                      FourthTPIAgency = li.FourthTPIAgency,
                                      FourthTPIIntervention = li.FourthTPIIntervention,
                                      FifthTPIAgency = li.FifthTPIAgency,
                                      FifthTPIIntervention = li.FifthTPIIntervention,
                                      SixthTPIAgency = li.SixthTPIAgency,
                                      SixthTPIIntervention = li.SixthTPIIntervention,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else if (gridType == clsImplementationEnum.GridType.HISTORYHEADER.GetStringValue())
                {
                    var lst = db.SP_IPI_SEAM_ICL_HISTORY_HEADER_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      RevNo = li.RevNo,
                                      TPRevNo = li.TPRevNo,
                                      PTCApplicable = li.PTCApplicable,
                                      PTCNumber = li.PTCNumber,
                                      Status = li.Status,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn,
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                else
                {
                    var lst = db.SP_IPI_SEAM_ICL_HISTORY_LINE_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                    if (!lst.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No Data Found";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    var newlst = (from li in lst
                                  select new
                                  {
                                      QualityProject = li.QualityProject,
                                      Project = li.Project,
                                      BU = li.BU,
                                      Location = li.Location,
                                      SeamNo = li.SeamNo,
                                      StageCode = li.StageCode,
                                      StageSequance = li.StageSequance,
                                      StageStatus = li.StageStatus,
                                      Parallel = li.Parallel,
                                      FirstTPIAgency = li.FirstTPIAgency,
                                      FirstTPIIntervention = li.FirstTPIIntervention,
                                      SecondTPIAgency = li.SecondTPIAgency,
                                      SecondTPIIntervention = li.SecondTPIIntervention,
                                      ThirdTPIAgency = li.ThirdTPIAgency,
                                      ThirdTPIIntervention = li.ThirdTPIIntervention,
                                      ForthTPIAgency = li.ForthTPIAgency,
                                      ForthTPIIntervention = li.ForthTPIIntervention,
                                      FifthTPIAgency = li.FifthTPIAgency,
                                      FifthTPIIntervention = li.FifthTPIIntervention,
                                      SixthTPIAgency = li.SixthTPIAgency,
                                      SixthTPIIntervention = li.SixthTPIIntervention,
                                      AcceptanceStandard = li.AcceptanceStandard,
                                      ApplicableSpecification = li.ApplicableSpecification,
                                      InspectionExtent = li.InspectionExtent,
                                      Remarks = li.Remarks,
                                      CreatedBy = li.CreatedBy,
                                      CreatedOn = li.CreatedOn,
                                      EditedBy = li.EditedBy,
                                      EditedOn = li.EditedOn,
                                      ApprovedBy = li.ApprovedBy,
                                      ApprovedOn = li.ApprovedOn
                                  }).ToList();

                    strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                }
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public string Status;
            public int HeaderId;
            public int Sequence;
            public string Stage;
            public string QIdDesc;
            public int? QIdRevision;
            public int? RevNo;
            public string Remarks;
        }

        public class ProjectDetails
        {
            public string BU;
            public string BUDescription;
            public string Location;
            public string LocationDescription;
        }
    }
}