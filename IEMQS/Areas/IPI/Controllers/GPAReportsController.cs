﻿using IEMQSImplementation;
using OfficeOpenXml;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;


namespace IEMQS.Areas.IPI.Controllers
{
    public class GPAReportsController : clsBase
    {
        // GET: IPI/GPAReports

        [SessionExpireFilter]
        public ActionResult Index()
        {
            var lstLocation = Manager.GetUserwiseLocation(objClsLoginInfo.UserName);
            ViewBag.Location = (from ath1 in lstLocation
                                join com2 in db.COM002 on ath1 equals com2.t_dimx
                                select new { Value = ath1, Text = com2.t_desc }).Distinct().ToList();
            var lastYears = Enumerable
                                  .Range(0, 19)
                                  .Select(i => DateTime.Now.AddYears(i - 19))
                                  .Select(date => new { yyyy = date.ToString("yyyy"), yy = date.ToString("yy") })
                                  .ToList();

            var lstLast20Years = lastYears.Select(x => new { Value = x.yyyy.ToString(), Text = x.yyyy.ToString(), ID = x.yyyy.ToString() + "-" + (Convert.ToInt32(x.yy) + 1).ToString("D2"), QYear = x.yyyy.ToString() + "-" + (Convert.ToInt32(x.yyyy) + 1).ToString() })
                                .OrderByDescending(x => x.ToString()).ToList();
            string currentYear = DateTime.Now.ToString("yyyy");
            string currentyyYear = DateTime.Now.ToString("yy");
            lstLast20Years.Insert(0, new { Value = currentYear, Text = currentYear, ID = currentYear + "-" + (Convert.ToInt32(currentyyYear) + 1).ToString("D2"), QYear = currentYear + "-" + (Convert.ToInt32(currentyyYear) + 1).ToString() });
            ViewBag.DefaultYear = currentYear;
            ViewBag.DefaultFY = currentYear + "-" + (Convert.ToInt32(currentyyYear) + 1).ToString("D2");
            ViewBag.DefaulthdFY = currentYear + "-" + (Convert.ToInt32(currentYear) + 1);
            ViewBag.Years = lstLast20Years;

            var lstMonthNames = DateTimeFormatInfo.CurrentInfo.MonthNames;
            ViewBag.MonthNames = lstMonthNames.Where(x => x.ToString() != "").Select(i => new { Value = i.ToString(), Text = i.ToString() }).ToList();

            var lst = clsImplementationEnum.GetddlQuarter().ToList();
            ViewBag.Quarters = lst.Select(x => new Projects { Value = x.ToString(), Text = x.ToString() }).ToList();

            ViewBag.Shop = db.QMS060.Select(x => x.Shop)
                                    .Where(x => x != null && x != string.Empty)
                                    .Select(x => new { Value = x, Text = x }).OrderBy(x => x.Value).Distinct().ToList();

            return View();
        }

        [HttpPost]
        public JsonResult GetProjectList()
        {
            try
            {
                var lstProjectByUser = Manager.getProjectsByUser(objClsLoginInfo.UserName.Trim());
                var lstQMS060 = db.QMS060.Select(x => new { Project = x.Project }).Distinct();
                var lstProjectList = (from ls in lstProjectByUser
                                 join qms060 in lstQMS060 on ls.projectCode equals qms060.Project
                                 select new { id = qms060.Project, text = ls.projectDescription })
                               .OrderBy(x => x.id)
                               .Distinct().ToList();

                //var lstBusLoc = (from ath1 in db.ATH001
                //                 join com2 in db.COM002 on ath1.Location equals com2.t_dimx
                //                 where ath1.Employee.Trim() == objClsLoginInfo.UserName.Trim() && (selectedBU != "" ? selectedBU.Contains(ath1.BU) : true)
                //                 select new { id = ath1.Location, text = com2.t_desc }).Distinct().ToList();
                return Json(lstProjectList, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpPost]
        public ActionResult FetchReportData(string reportType)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<AutoCompleteModel> lstReport = new List<AutoCompleteModel>();
            if (reportType.ToLower() == "monthly")
            {
                var lst = clsImplementationEnum.GetMonthlyReports().ToList();
                lstReport = lst.Select(x => new AutoCompleteModel { Value = x.ToString(), Text = x.ToString() }).ToList();
            }
            else
            {
                var lst = clsImplementationEnum.GetQuarterlyReports().ToList();
                lstReport = lst.Select(x => new AutoCompleteModel { Value = x.ToString(), Text = x.ToString() }).ToList();
            }
            var objData = new
            {
                count = lstReport.Count,
                lstReportType = lstReport
            };
            //objResponseMsg
            return Json(objData, JsonRequestBehavior.AllowGet);
        }

    }
}