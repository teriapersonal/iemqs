﻿using IEMQS.Areas.NDE.Models;
using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class MaintainTPIAgencyInterventionController : clsBase
    {
        /// <summary>
        /// Created By :Deepak tiwari
        /// Created Date :16/08/2017
        /// Description: TPI Agency and Intervention
        /// </summary>
        /// <returns></returns>
        // GET:TPI/MaintainTPIIntervention
        [SessionExpireFilter]
        // [AllowAnonymous]
        // [UserPermissions]

        #region first Page
        public ActionResult Index()
        {
            return View();
        }

        // load datatable
        public JsonResult LoadMTData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = "1=1";
                //if (param.MTStatus.ToUpper() == "PENDING")
                //{
                //    strWhere += "1=1 and status in('Draft','Returned')";
                //}
                //else
                //{
                //    strWhere += "1=1";
                //}
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and  (QualityProject like '%" + param.sSearch + "%' or Stage like '%" + param.sSearch + "%' or StageType like '%" + param.sSearch + "%' or StageDesc like '%" + param.sSearch + "%' )";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                //strWhere += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "BU", "Location");
                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_TPT_GetTPIAgencyHeader
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.QualityProject),
                           Convert.ToString(uc.StageType),
                           Convert.ToString(uc.Stage),
                           Convert.ToString(uc.StageDesc),
                          Convert.ToString(uc.Id)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        public ActionResult UpdateTPIFormPartial(string qualityProject, string project)
        {
            string approve = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
            var strproject = (from a in db.COM001
                              where a.t_cprj == project
                              select a.t_cprj + " - " + a.t_dsca).FirstOrDefault();
            ViewBag.SourceProject = strproject != null ? strproject : project;
            ViewBag.qualityProject = qualityProject;
            ViewBag.SeamList = db.QMS012_Log.Where(m => m.QualityProject == qualityProject && m.Status == approve).OrderBy(m => m.SeamNo).Select(m => new { Text = m.SeamNo, Value = m.SeamNo }).Distinct().ToList();
            ViewBag.StageList = db.QMS025.Where(m => m.QualityProject == qualityProject).Select(m => new { Text = m.Stage, Value = m.Stage });
            return PartialView("_UpdateTPIFormPartial");
        }

        public ActionResult SaveUpdateTPI(QMS025 QMS025, string fromSeam, string toSeam, string StageCode)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                objResponseMsg = SaveUpdateTPIinICL(QMS025, fromSeam, toSeam, StageCode);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateTPIinICL(int id, string fromSeam, string toSeam, string StageCode)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS025 obj = db.QMS025.Where(x => x.Id == id).FirstOrDefault();
                objResponseMsg = SaveUpdateTPIinICL(obj, fromSeam, toSeam, StageCode);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public clsHelper.ResponseMsgWithStatus SaveUpdateTPIinICL(QMS025 QMS025, string fromSeam, string toSeam, string StageCode)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string approve = clsImplementationEnum.CommonStatus.Approved.GetStringValue();
                var seamList = db.QMS012_Log.Where(m => m.QualityProject == QMS025.QualityProject && m.Status == approve).OrderBy(m => m.SeamNo);
                List<QMS012_Log> newSeamList = new List<QMS012_Log>();
                bool start = false;
                foreach (var seam in seamList)
                {
                    if (seam.SeamNo == fromSeam)
                        start = true;
                    int seamExist = (new clsManager()).GetMaxOfferedSequenceNo(seam.QualityProject, QMS025.BU, seam.Location, seam.SeamNo);
                    if (start && seamExist <= 0)
                        newSeamList.Add(seam);
                    if (seam.SeamNo == toSeam)
                        break;
                }
                string Project = QMS025.Project?.Split('-')[0].Trim();
                string Location = QMS025.Location?.Split('-')[0].Trim();
                string BU = QMS025.BU?.Split('-')[0].Trim();

                if (!string.IsNullOrWhiteSpace(QMS025.FirstTPIAgency) && !string.IsNullOrWhiteSpace(QMS025.FirstTPIIntervention))
                {
                    string[] arr = newSeamList.Select(i => i.SeamNo).ToArray();
                    List<QMS031> lstQMS031 = db.QMS031.Where(x => x.StageCode == StageCode
                                        && x.QualityProject == QMS025.QualityProject
                                        && x.Project.Trim() == Project && x.Location.Trim() == Location && x.BU.Trim() == BU
                                       && arr.Contains(x.SeamNo)).ToList();

                    lstQMS031.ForEach(i =>
                    {
                        i.FirstTPIIntervention = objClsLoginInfo.UserName;
                        i.FirstTPIIntervention = QMS025.FirstTPIIntervention;
                        i.SecondTPIIntervention = QMS025.SecondTPIIntervention;
                        i.ThirdTPIIntervention = QMS025.ThirdTPIIntervention;
                        i.ForthTPIIntervention = QMS025.ForthTPIIntervention;
                        i.FifthTPIIntervention = QMS025.FifthTPIIntervention;
                        i.SixthTPIIntervention = QMS025.SixthTPIIntervention;
                        i.FirstTPIAgency = QMS025.FirstTPIAgency;
                        i.SecondTPIAgency = QMS025.SecondTPIAgency;
                        i.ThirdTPIAgency = QMS025.ThirdTPIAgency;
                        i.ForthTPIAgency = QMS025.ForthTPIAgency;
                        i.FifthTPIAgency = QMS025.FifthTPIAgency;
                        i.SixthTPIAgency = QMS025.SixthTPIAgency;
                    });
                    db.SaveChanges();
                    if (lstQMS031.Count > 0)
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = lstQMS031.Count + " Stage has been updated.";
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "No any Seam available for update seam.";
                    }
                }
                else
                {
                    objResponseMsg.dataKey = true;
                    objResponseMsg.dataValue = string.Format("Stage : {0} have been skipped as First TPI Agency and First TPI Intervention", QMS025.Stage);
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return objResponseMsg;
        }

        #region Tab
        [HttpPost]
        public ActionResult GetMTPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetTPiInterventionHtmlPartial");
        }
        #endregion

        #region Create Consumable
        [SessionExpireFilter]
        public ActionResult AddTPIAgency(int? id)
        {

            QMS025 objQMS025 = new QMS025();
            QMS010 objQMS010 = new QMS010();

            var user = objClsLoginInfo.UserName;
            var location = objClsLoginInfo.Location;


            if (id != null)
            {
                var objId = Convert.ToInt32(id);
                objQMS025 = db.QMS025.Where(x => x.Id == objId).FirstOrDefault();
                ViewBag.QualityProject = objQMS025.QualityProject;
                ViewBag.Project = objQMS025.Project;
                ViewBag.Stages = db.QMS002.Where(i => i.BU.Equals(objQMS025.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS025.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).ToList();
                var lstTPIAgency = Manager.GetSubCatagories("TPI Agency", objQMS025.BU, objQMS025.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
                var lstTPIIntervention = Manager.GetSubCatagories("TPI Intervention", objQMS025.BU, objQMS025.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
                ViewBag.TPIAgency = lstTPIAgency;
                ViewBag.TPIIntervention = lstTPIIntervention;
                NDEModels objNDEModels = new NDEModels();

                ViewBag.StageDesc = db.QMS002.Where(i => i.StageCode == objQMS025.Stage && i.BU.Equals(objQMS025.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS025.Location, StringComparison.OrdinalIgnoreCase)).Select(i => i.StageCode + "-" + i.StageDesc).FirstOrDefault();
                ViewBag.ToStageDesc = db.QMS002.Where(i => i.StageCode == objQMS025.Stage && i.BU.Equals(objQMS025.BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objQMS025.Location, StringComparison.OrdinalIgnoreCase)).Select(i => i.StageCode + "-" + i.StageDesc).FirstOrDefault();

                ViewBag.FirstTPIAgency = objNDEModels.GetCategory("TPI Agency", objQMS025.FirstTPIAgency, objQMS025.BU, objQMS025.Location, true).CategoryDescription;
                ViewBag.FirstTPIIntervention = objNDEModels.GetCategory("TPI Intervention", objQMS025.FirstTPIIntervention, objQMS025.BU, objQMS025.Location, true).CategoryDescription;

                ViewBag.SecondTPIAgency = objNDEModels.GetCategory("TPI Agency", objQMS025.SecondTPIAgency, objQMS025.BU, objQMS025.Location, true).CategoryDescription;
                ViewBag.SecondTPIIntervention = objNDEModels.GetCategory("TPI Intervention", objQMS025.SecondTPIIntervention, objQMS025.BU, objQMS025.Location, true).CategoryDescription;

                ViewBag.ThirdTPIAgency = objNDEModels.GetCategory("TPI Agency", objQMS025.ThirdTPIAgency, objQMS025.BU, objQMS025.Location, true).CategoryDescription;
                ViewBag.ThirdTPIIntervention = objNDEModels.GetCategory("TPI Intervention", objQMS025.ThirdTPIIntervention, objQMS025.BU, objQMS025.Location, true).CategoryDescription;

                ViewBag.ForthTPIAgency = objNDEModels.GetCategory("TPI Agency", objQMS025.ForthTPIAgency, objQMS025.BU, objQMS025.Location, true).CategoryDescription;
                ViewBag.ForthTPIIntervention = objNDEModels.GetCategory("TPI Intervention", objQMS025.ForthTPIIntervention, objQMS025.BU, objQMS025.Location, true).CategoryDescription;

                ViewBag.FifthTPIAgency = objNDEModels.GetCategory("TPI Agency", objQMS025.FifthTPIAgency, objQMS025.BU, objQMS025.Location, true).CategoryDescription;
                ViewBag.FifthTPIIntervention = objNDEModels.GetCategory("TPI Intervention", objQMS025.FifthTPIIntervention, objQMS025.BU, objQMS025.Location, true).CategoryDescription;

                ViewBag.SixthTPIAgency = objNDEModels.GetCategory("TPI Agency", objQMS025.SixthTPIAgency, objQMS025.BU, objQMS025.Location, true).CategoryDescription;
                ViewBag.SixthTPIIntervention = objNDEModels.GetCategory("TPI Intervention", objQMS025.SixthTPIIntervention, objQMS025.BU, objQMS025.Location, true).CategoryDescription;

                objQMS025.Location = db.COM002.Where(i => i.t_dtyp == 1 && i.t_dimx == objQMS025.Location).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                objQMS025.Project = db.COM001.Where(i => i.t_cprj == objQMS025.Project).Select(i => i.t_cprj + " - " + i.t_dsca).FirstOrDefault();
                objQMS025.BU = db.COM002.Where(i => i.t_dtyp == 2 && i.t_dimx == objQMS025.BU).Select(i => i.t_dimx + " - " + i.t_desc).FirstOrDefault();
                ViewBag.Action = "Edit";
            }
            else
            {

                objQMS025.Location = location;
                ViewBag.Action = "AddNew";
            }


            return View(objQMS025);
        }


        [HttpPost]
        public ActionResult SaveTPI(FormCollection fc, QMS025 QMS025)
        {

            QMS025 objQMS025 = new QMS025();
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                string project = fc["Project"].Split('-')[0];
                string BU = fc["BU"].Split('-')[0];
                string location = fc["Location"].Split('-')[0];
                string Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                string Approved = clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue();
                if (QMS025.Id > 0)
                {
                    objQMS025 = db.QMS025.Where(x => x.Id == QMS025.Id).FirstOrDefault();
                    objQMS025.QualityProject = QMS025.QualityProject;
                    //objQMS025.Stage = QMS025.Stage;
                    objQMS025.FirstTPIAgency = QMS025.FirstTPIAgency.Split('-')[0];
                    objQMS025.FirstTPIIntervention = QMS025.FirstTPIIntervention.Split('-')[0];
                    if (!string.IsNullOrEmpty(QMS025.SecondTPIAgency))
                    {
                        objQMS025.SecondTPIAgency = QMS025.SecondTPIAgency.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.SecondTPIAgency = null;
                    }
                    if (!string.IsNullOrEmpty(QMS025.SecondTPIIntervention))
                    {
                        objQMS025.SecondTPIIntervention = QMS025.SecondTPIIntervention.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.SecondTPIIntervention = null;
                    }
                    if (!string.IsNullOrEmpty(QMS025.ThirdTPIAgency))
                    {
                        objQMS025.ThirdTPIAgency = QMS025.ThirdTPIAgency.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.ThirdTPIAgency = null;
                    }
                    if (!string.IsNullOrEmpty(QMS025.ThirdTPIIntervention))
                    {
                        objQMS025.ThirdTPIIntervention = QMS025.ThirdTPIIntervention.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.ThirdTPIIntervention = null;
                    }
                    if (!string.IsNullOrEmpty(QMS025.ForthTPIAgency))
                    {
                        objQMS025.ForthTPIAgency = QMS025.ForthTPIAgency.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.ForthTPIAgency = null;
                    }
                    if (!string.IsNullOrEmpty(QMS025.ForthTPIIntervention))
                    {
                        objQMS025.ForthTPIIntervention = QMS025.ForthTPIIntervention.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.ForthTPIIntervention = null;
                    }


                    if (!string.IsNullOrEmpty(QMS025.FifthTPIAgency))
                    {
                        objQMS025.FifthTPIAgency = QMS025.FifthTPIAgency.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.FifthTPIAgency = null;
                    }
                    if (!string.IsNullOrEmpty(QMS025.FifthTPIIntervention))
                    {
                        objQMS025.FifthTPIIntervention = QMS025.FifthTPIIntervention.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.FifthTPIIntervention = null;
                    }


                    if (!string.IsNullOrEmpty(QMS025.SixthTPIAgency))
                    {
                        objQMS025.SixthTPIAgency = QMS025.SixthTPIAgency.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.SixthTPIAgency = null;
                    }
                    if (!string.IsNullOrEmpty(QMS025.SixthTPIIntervention))
                    {
                        objQMS025.SixthTPIIntervention = QMS025.SixthTPIIntervention.Split('-')[0];
                    }
                    else
                    {
                        objQMS025.SixthTPIIntervention = null;
                    }
                    objQMS025.Remarks = QMS025.Remarks;


                    objQMS025.CreatedBy = QMS025.CreatedBy;
                    objQMS025.CreatedOn = QMS025.CreatedOn;
                    objQMS025.EditedBy = objClsLoginInfo.UserName;
                    objQMS025.EditedOn = DateTime.Now;
                    int Id = objQMS025.Id;
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = Id.ToString();
                    // objResponseMsg.Status = objNDE020.Status;

                    db.SaveChanges();
                }
                else
                {

                    string loc = fc["Location"].Split('-')[0];
                    //string Fromstage = fc["Stage"].Split('-')[0];
                    //string Tostage = fc["ToStage"].Split('-')[0];

                    string Fromstage = fc["ddlstage"];
                    string Tostage = fc["ddltostage"];

                    var lstResultForstage = db.SP_GET_STAGECODE_FOR_TPI_SEAM_PART
                             (
                             Fromstage, Tostage, QMS025.QualityProject).ToList();

                    if (lstResultForstage.Count > 0)
                    {
                        //int count = lstResultForstage.Count;
                        //for(int i=0;i<count;i++)
                        //{
                        int insertedCnt = 0;
                        foreach (var Stagecode in lstResultForstage.ToList())
                        {
                            if (!db.QMS025.Any(x => x.QualityProject == QMS025.QualityProject && x.Project == project && x.Location == location && x.BU == BU && x.Stage == Stagecode.ToString()))
                            {
                                insertedCnt++;
                                objQMS025.QualityProject = QMS025.QualityProject;
                                objQMS025.Project = fc["Project"].Split('-')[0];
                                objQMS025.BU = fc["BU"].Split('-')[0];
                                objQMS025.Location = loc;
                                objQMS025.Stage = Stagecode.ToString();
                                //objQMS025.Stage = fc["Location"].Split('-')[0];
                                objQMS025.FirstTPIAgency = fc["FirstTPIAgency"].Split('-')[0];
                                objQMS025.FirstTPIIntervention = fc["FirstTPIIntervention"].Split('-')[0];

                                if (fc["SecondTPIAgency"] != null)
                                {
                                    objQMS025.SecondTPIAgency = fc["SecondTPIAgency"].Split('-')[0];
                                }
                                else
                                {
                                    objQMS025.SecondTPIAgency = null;
                                }
                                if (fc["SecondTPIIntervention"] != null)
                                {
                                    objQMS025.SecondTPIIntervention = fc["SecondTPIIntervention"].Split('-')[0];
                                }
                                else
                                {
                                    objQMS025.SecondTPIIntervention = null;
                                }

                                if (fc["ThirdTPIAgency"] != null)
                                {
                                    objQMS025.ThirdTPIAgency = fc["ThirdTPIAgency"].Split('-')[0];
                                }
                                else
                                {
                                    objQMS025.ThirdTPIAgency = null;
                                }
                                if (fc["ThirdTPIIntervention"] != null)
                                {
                                    objQMS025.ThirdTPIIntervention = fc["ThirdTPIIntervention"].Split('-')[0];
                                }
                                else
                                {
                                    objQMS025.ThirdTPIIntervention = null;
                                }

                                if (fc["ForthTPIAgency"] != null)
                                {
                                    objQMS025.ForthTPIAgency = fc["ForthTPIAgency"].Split('-')[0];
                                }
                                else
                                {
                                    objQMS025.ForthTPIAgency = null;
                                }
                                if (fc["ForthTPIIntervention"] != null)
                                {
                                    objQMS025.ForthTPIIntervention = fc["ForthTPIIntervention"].Split('-')[0];
                                }
                                else
                                {
                                    objQMS025.ForthTPIIntervention = null;
                                }

                                if (fc["FifthTPIAgency"] != null)
                                {
                                    objQMS025.FifthTPIAgency = fc["FifthTPIAgency"].Split('-')[0];
                                }
                                else
                                {
                                    objQMS025.FifthTPIAgency = null;
                                }
                                if (fc["FifthTPIIntervention"] != null)
                                {
                                    objQMS025.FifthTPIIntervention = fc["FifthTPIIntervention"].Split('-')[0];
                                }
                                else
                                {
                                    objQMS025.FifthTPIIntervention = null;
                                }

                                if (fc["SixthTPIAgency"] != null)
                                {
                                    objQMS025.SixthTPIAgency = fc["SixthTPIAgency"].Split('-')[0];
                                }
                                else
                                {
                                    objQMS025.SixthTPIAgency = null;
                                }
                                if (fc["SixthTPIIntervention"] != null)
                                {
                                    objQMS025.SixthTPIIntervention = fc["SixthTPIIntervention"].Split('-')[0];
                                }
                                else
                                {
                                    objQMS025.SixthTPIIntervention = null;
                                }
                                objQMS025.CreatedBy = objClsLoginInfo.UserName;
                                objQMS025.CreatedOn = DateTime.Now;
                                db.QMS025.Add(objQMS025);
                                db.SaveChanges();
                                int Id = objQMS025.Id;
                            }
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = insertedCnt + " records added successfully";
                        }
                        //}

                    }
                    else
                    {
                        //Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Record Not Found";
                        objResponseMsg.Status = "Record Not Found";
                        ViewBag.Action = "Add";
                        return Json(objResponseMsg);
                    }



                    //objResponseMsg.Status = objNDE020.Status;
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg);



        }
        [HttpPost]
        public ActionResult DeleteTPI(int Id)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS025 objQMS025 = db.QMS025.Where(x => x.Id == Id).FirstOrDefault();
                db.QMS025.Remove(objQMS025);
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Delete.ToString();
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Copy

        [HttpPost]
        public ActionResult GetCopyInterventionDetails(string Projectno)
        {
            ViewBag.Quilityproject = Projectno;
            var objQMS010 = db.QMS010.Where(a => a.QualityProject == Projectno).FirstOrDefault();

            var Projectdesc = (from a in db.COM001
                               where a.t_cprj == objQMS010.Project
                               select new { ProjectDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();
            ViewBag.Projectdesc = Projectdesc.ProjectDesc;
            ViewBag.Qualityprojectno = db.QMS010.Where(i => i.QualityProject != objQMS010.QualityProject && i.BU == objQMS010.BU && i.Location == objQMS010.Location).OrderBy(i => i.QualityProject).Select(i => new { id = i.QualityProject }).ToList();
            // ViewBag.Stagecode = db.QMS025.Where(i => i.QualityProject.Equals(Projectno, StringComparison.OrdinalIgnoreCase)).OrderBy(i => i.Stage).Select(i => new { id = i.Stage }).ToList();

            return PartialView("_LoadTPIInterventionCopyPartial");
        }

        [HttpPost]
        public ActionResult LoadCopyTPIAgencyWithinProject(int Id)
        {
            var lstResult = db.SP_TPT_GetTPIAgencyHeader(1, 0, "", "A.Id = " + Id).Take(1).ToList();
            if (lstResult != null && lstResult.Count > 0)
            {
                ViewBag.Id = Id;
                ViewBag.QualityProject = lstResult[0].QualityProject;
                ViewBag.Stage = lstResult[0].StageDesc;
                ViewBag.FirstTPIAgency = lstResult[0].FirstTPIAgencyDesc;
                ViewBag.FirstTPIIntervention = lstResult[0].FirstTPIInterventionDesc;
                ViewBag.SecondTPIAgency = lstResult[0].SecondTPIAgencyDesc;
                ViewBag.SecondTPIIntervention = lstResult[0].SecondTPIInterventionDesc;
                ViewBag.ThirdTPIAgency = lstResult[0].ThirdTPIAgencyDesc;
                ViewBag.ThirdTPIIntervention = lstResult[0].ThirdTPIInterventionDesc;
                ViewBag.ForthTPIAgency = lstResult[0].ForthTPIAgencyDesc;
                ViewBag.ForthTPIIntervention = lstResult[0].ForthTPIInterventionDesc;
                ViewBag.FifthTPIAgency = lstResult[0].FifthTPIAgencyDesc;
                ViewBag.FifthTPIIntervention = lstResult[0].FifthTPIInterventionDesc;
                ViewBag.SixthTPIAgency = lstResult[0].SixthTPIAgencyDesc;
                ViewBag.SixthTPIIntervention = lstResult[0].SixthTPIInterventionDesc;
            }
            return PartialView("_LoadCopyTPIAgencyWithinProjectPartial");
        }


        [HttpPost]
        public ActionResult GetStageByQualityProject(string term, string qualityProject)
        {
            try
            {
                var data = db.QMS025.Where(x => x.QualityProject.Equals(qualityProject) && x.Stage.Contains(term)).Select(x => x.Stage).Distinct().ToList();
                return Json(new { Data = data });
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.Message });
            }
        }

        [HttpPost]
        public ActionResult GetPendingTPIStageByQualityProject(string term, string qualityProject)
        {
            try
            {
                var data = db.QMS025.Where(x => x.QualityProject.Equals(qualityProject) && string.IsNullOrEmpty(x.FirstTPIAgency) && string.IsNullOrEmpty(x.FirstTPIIntervention) && x.Stage.Contains(term)).Select(x => x.Stage).Distinct().ToList();
                return Json(new { Data = data });
            }
            catch (Exception ex)
            {
                return Json(new { Error = ex.Message });
            }
        }

        [HttpPost]
        public ActionResult CopyQualityProject(int Id, string Project, string QualityProject, string QualityProjectFrom, string QualityProjectTo, string StageCode)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {

                List<string> lstFirstTPINotMaintained = new List<string>();
                #region Get Destination Quality Project For Copy
                var objQMS010 = db.QMS010.Where(a => a.QualityProject == QualityProject).FirstOrDefault();

                List<QMS010> allqualityproject = db.QMS010.Where(i => i.QualityProject != QualityProject && i.BU == objQMS010.BU && i.Location == objQMS010.Location).OrderBy(i => i.QualityProject).ToList();
                List<string> newqualityprojectList = new List<string>();
                bool start = false;
                int totCount = 0;

                foreach (QMS010 qualityproject in allqualityproject)
                {
                    if (qualityproject.QualityProject == QualityProjectFrom)
                    {
                        start = true;
                    }
                    if (start)
                        newqualityprojectList.Add(qualityproject.QualityProject);
                    if (qualityproject.QualityProject == QualityProjectTo)
                    {
                        break;
                    }
                }
                #endregion
                QMS025 QMS025 = new QMS025();

                foreach (var qmsProjects in newqualityprojectList.ToList())
                {
                    var objDestQMS010 = db.QMS010.Where(a => a.QualityProject == qmsProjects).FirstOrDefault();

                    if (StageCode.ToLower() == "all")
                    {
                        var Stagecode = db.QMS025.Where(m => m.QualityProject == QualityProject && m.Project == Project && m.BU == objQMS010.BU && m.Location == objQMS010.Location).Select(m => m.Stage).Distinct().ToList();
                        if (Stagecode.Count > 0)
                        {
                            foreach (var Itemstagecode in Stagecode)
                            {
                                bool isCopy = true;
                                QMS025 ObjQMS025 = db.QMS025.Where(x => x.QualityProject == objQMS010.QualityProject && x.Project == objQMS010.Project && x.Location == objQMS010.Location && x.BU == objQMS010.BU && x.Stage == Itemstagecode).FirstOrDefault();
                                if (ObjQMS025 != null ? string.IsNullOrWhiteSpace(ObjQMS025.FirstTPIAgency) && string.IsNullOrWhiteSpace(ObjQMS025.FirstTPIIntervention) : false)
                                {
                                    isCopy = false;
                                    lstFirstTPINotMaintained.Add(ObjQMS025.Stage);
                                }
                                if (isCopy && !db.QMS025.Any(x => x.QualityProject == objDestQMS010.QualityProject && x.Project == objDestQMS010.Project && x.Location == objDestQMS010.Location && x.BU == objDestQMS010.BU && x.Stage == Itemstagecode))
                                {
                                    QMS025.QualityProject = objDestQMS010.QualityProject;
                                    QMS025.Project = objDestQMS010.Project;
                                    QMS025.BU = objDestQMS010.BU;
                                    QMS025.Location = objDestQMS010.Location;
                                    QMS025.Stage = Itemstagecode;
                                    QMS025.FirstTPIAgency = ObjQMS025.FirstTPIAgency;
                                    QMS025.FirstTPIIntervention = ObjQMS025.FirstTPIIntervention;
                                    QMS025.SecondTPIAgency = ObjQMS025.SecondTPIAgency;
                                    QMS025.SecondTPIIntervention = ObjQMS025.SecondTPIIntervention;
                                    QMS025.ThirdTPIAgency = ObjQMS025.ThirdTPIAgency;
                                    QMS025.ThirdTPIIntervention = ObjQMS025.ThirdTPIIntervention;
                                    QMS025.ForthTPIAgency = ObjQMS025.ForthTPIAgency;
                                    QMS025.ForthTPIIntervention = ObjQMS025.ForthTPIIntervention;
                                    QMS025.FifthTPIAgency = ObjQMS025.FifthTPIAgency;
                                    QMS025.FifthTPIIntervention = ObjQMS025.FifthTPIIntervention;
                                    QMS025.SixthTPIAgency = ObjQMS025.SixthTPIAgency;
                                    QMS025.SixthTPIIntervention = ObjQMS025.SixthTPIIntervention;
                                    QMS025.CreatedBy = objClsLoginInfo.UserName;
                                    QMS025.CreatedOn = DateTime.Now;
                                    db.QMS025.Add(QMS025);
                                    db.SaveChanges();
                                    totCount = totCount + 1;
                                }
                            }
                        }
                    }
                    else
                    {
                        bool isCopy = true;
                        QMS025 ObjQMS025 = db.QMS025.Where(x => x.QualityProject == objQMS010.QualityProject && x.Project == objQMS010.Project && x.Location == objQMS010.Location && x.BU == objQMS010.BU && x.Stage == StageCode && (x.FirstTPIAgency != null && x.FirstTPIAgency != "") && (x.FirstTPIIntervention != null && x.FirstTPIIntervention != "")).FirstOrDefault();
                        if (ObjQMS025 != null ? string.IsNullOrWhiteSpace(ObjQMS025.FirstTPIAgency) && string.IsNullOrWhiteSpace(ObjQMS025.FirstTPIIntervention) : false)
                        {
                            isCopy = false;
                            lstFirstTPINotMaintained.Add(ObjQMS025.Stage);
                        }
                        if (isCopy && !db.QMS025.Any(x => x.QualityProject == objDestQMS010.QualityProject && x.Project == objDestQMS010.Project && x.Location == objDestQMS010.Location && x.BU == objDestQMS010.BU && x.Stage == StageCode))
                        {
                            QMS025.QualityProject = objDestQMS010.QualityProject;
                            QMS025.Project = objDestQMS010.Project;
                            QMS025.BU = objDestQMS010.BU;
                            QMS025.Location = objDestQMS010.Location;
                            QMS025.Stage = StageCode;
                            QMS025.FirstTPIAgency = ObjQMS025.FirstTPIAgency;
                            QMS025.FirstTPIIntervention = ObjQMS025.FirstTPIIntervention;
                            QMS025.SecondTPIAgency = ObjQMS025.SecondTPIAgency;
                            QMS025.SecondTPIIntervention = ObjQMS025.SecondTPIIntervention;
                            QMS025.ThirdTPIAgency = ObjQMS025.ThirdTPIAgency;
                            QMS025.ThirdTPIIntervention = ObjQMS025.ThirdTPIIntervention;
                            QMS025.ForthTPIAgency = ObjQMS025.ForthTPIAgency;
                            QMS025.ForthTPIIntervention = ObjQMS025.ForthTPIIntervention;
                            QMS025.FifthTPIAgency = ObjQMS025.FifthTPIAgency;
                            QMS025.FifthTPIIntervention = ObjQMS025.FifthTPIIntervention;
                            QMS025.SixthTPIAgency = ObjQMS025.SixthTPIAgency;
                            QMS025.SixthTPIIntervention = ObjQMS025.SixthTPIIntervention;
                            QMS025.CreatedBy = objClsLoginInfo.UserName;
                            QMS025.CreatedOn = DateTime.Now;
                            db.QMS025.Add(QMS025);
                            db.SaveChanges();
                            totCount = totCount + 1;
                        }

                    }

                }
                objResponseMsg.Key = true;
                if (lstFirstTPINotMaintained.Count() > 0)
                {
                    objResponseMsg.dataKey = true;
                    objResponseMsg.dataValue = string.Format("Stage(s) {0} have been skipped as First TPI Agency and First TPI Intervention", string.Join(",", lstFirstTPINotMaintained));
                }
                objResponseMsg.Value = totCount + " TPI Agency and Intervention Data Copied Successfully";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CopyTPIAgencyWithinProject(int Id, string StageCode)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                var objQMS025 = db.QMS025.Where(x => x.Id == Id).FirstOrDefault();
                if (objQMS025 != null)
                {
                    List<QMS025> objQMS025List = new List<QMS025>();
                    if (StageCode.Trim().ToUpper() == "ALL")
                    {
                        objQMS025List = db.QMS025.Where(x => x.Id != Id && x.QualityProject.Equals(objQMS025.QualityProject) && string.IsNullOrEmpty(x.FirstTPIAgency) && string.IsNullOrEmpty(x.FirstTPIIntervention)).ToList();
                    }
                    else
                    {
                        objQMS025List = db.QMS025.Where(x => x.Id != Id && x.QualityProject.Equals(objQMS025.QualityProject) && string.IsNullOrEmpty(x.FirstTPIAgency) && string.IsNullOrEmpty(x.FirstTPIIntervention) && x.Stage.Equals(StageCode)).ToList();
                    }

                    if (objQMS025List.Count > 0)
                    {
                        objQMS025List.ForEach(i =>
                        {
                            i.FirstTPIAgency = objQMS025.FirstTPIAgency;
                            i.FirstTPIIntervention = objQMS025.FirstTPIIntervention;

                            i.SecondTPIAgency = objQMS025.SecondTPIAgency;
                            i.SecondTPIIntervention = objQMS025.SecondTPIIntervention;

                            i.ThirdTPIAgency = objQMS025.ThirdTPIAgency;
                            i.ThirdTPIIntervention = objQMS025.ThirdTPIIntervention;

                            i.ForthTPIAgency = objQMS025.ForthTPIAgency;
                            i.ForthTPIIntervention = objQMS025.ForthTPIIntervention;

                            i.FifthTPIAgency = objQMS025.FifthTPIAgency;
                            i.FifthTPIIntervention = objQMS025.FifthTPIIntervention;

                            i.SixthTPIAgency = objQMS025.SixthTPIAgency;
                            i.SixthTPIIntervention = objQMS025.SixthTPIIntervention;

                            i.EditedBy = objClsLoginInfo.UserName;
                            i.EditedOn = DateTime.Now;
                        });

                        db.SaveChanges();
                    }

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = objQMS025List.Count + " TPI Agency and Intervention Data Copied Successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No record found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Common Functions

        [HttpPost]
        public ActionResult GetProject(string qms)
        {   //get project and BU by Qms project
            if (!string.IsNullOrWhiteSpace(qms))
            {

                BUWiseDropdown objProjectDataModel = new BUWiseDropdown();

                string project = db.QMS010.Where(a => a.QualityProject == qms).FirstOrDefault().Project;

                var lstProject = (from a in db.COM001
                                  where a.t_cprj == project
                                  select new { ProjectDesc = a.t_cprj + " - " + a.t_dsca }).FirstOrDefault();
                string BU = db.COM001.Where(i => i.t_cprj == project).FirstOrDefault().t_entu;

                var BUDescription = (from a in db.COM002
                                     where a.t_dtyp == 2 && a.t_dimx == BU
                                     select new { BUDesc = a.t_dimx + " - " + a.t_desc }).FirstOrDefault();

                objProjectDataModel.Project = lstProject.ProjectDesc;
                objProjectDataModel.Location = objClsLoginInfo.Location;
                objProjectDataModel.BUDescription = BUDescription.BUDesc;

                List<BULocWiseCategoryModel> lstBULocWiseMethodModel;


                List<QMS002> lstMethodGLB002 = GetStageType(BU, objClsLoginInfo.Location).ToList();
                if (lstMethodGLB002.Count > 0)
                {
                    lstBULocWiseMethodModel = lstMethodGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.StageCode + "-" + x.StageDesc), CatID = x.StageCode }).ToList();
                    objProjectDataModel.lstBULocWiseMethodModel = lstBULocWiseMethodModel;
                }

                var lstStage = db.QMS002.Where(i => i.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objClsLoginInfo.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).OrderBy(i => i.Code).ToList();
                var lstTPIAgency = Manager.GetSubCatagories("TPI Agency", BU, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
                var lstTPIIntervention = Manager.GetSubCatagories("TPI Intervention", BU, objClsLoginInfo.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();

                objProjectDataModel.Project = lstProject.ProjectDesc;
                objProjectDataModel.BUDescription = BUDescription.BUDesc;

                objProjectDataModel.TPIAgency = lstTPIAgency.Any() ? lstTPIAgency : null;
                objProjectDataModel.TPIIntervention = lstTPIIntervention.Any() ? lstTPIIntervention : null;
                objProjectDataModel.Stage = lstStage.Any() ? lstStage : null;

                if (lstTPIAgency.Any() && lstTPIIntervention.Any() && lstStage.Any())
                {
                    objProjectDataModel.Key = true;
                }
                else
                {
                    List<string> lstCategory = new List<string>();
                    if (!lstTPIAgency.Any())
                        lstCategory.Add("TPI Agency");
                    if (!lstTPIIntervention.Any())
                        lstCategory.Add("TPI Intervention");

                    objProjectDataModel.Value = string.Join(",", lstCategory.ToList()) + " Category not exist for selected project/Quality Project, Please contact Admin";
                }

                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetProjectBULocation(string qms)
        {
            //get project and BU by Qms project
            if (!string.IsNullOrWhiteSpace(qms))
            {
                BUWiseDropdown objProjectDataModel = new BUWiseDropdown();

                string project = db.QMS010.Where(a => a.QualityProject == qms).FirstOrDefault().Project;
                string BU = db.COM001.Where(i => i.t_cprj == project).FirstOrDefault().t_entu;

                objProjectDataModel.Project = project;
                objProjectDataModel.Location = objClsLoginInfo.Location;
                objProjectDataModel.BUDescription = BU;

                var lstStage = db.QMS002.Where(i => i.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && i.Location.Equals(objClsLoginInfo.Location, StringComparison.OrdinalIgnoreCase)).Select(i => new CategoryData { Value = i.StageCode, Code = i.StageCode, CategoryDescription = i.StageCode + "-" + i.StageDesc }).OrderBy(i => i.Code).ToList();
                objProjectDataModel.Stage = lstStage.Any() ? lstStage : null;

                return Json(objProjectDataModel, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public List<QMS002> GetStageType(string strBU, string strLoc)
        {
            //List<QMS002> lstqms002 = (from QMS002 in db.QMS002                      
            //                          select QMS002).ToList();
            //return lstqms002;

            List<QMS002> lstGLB002 = (from qmS002 in db.QMS002
                                      where strBU.Equals(qmS002.BU) && strLoc.Equals(qmS002.Location)

                                      select qmS002).ToList();
            return lstGLB002;


        }




        #endregion

        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;

                var lst = db.SP_TPT_GetTPIAgencyHeader(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                var newlst = (from uc in lst
                              select new
                              {
                                  QualityProject = uc.QualityProject,
                                  Stage = uc.StageDesc,
                                  FirstTPIAgency = uc.FirstTPIAgency,
                                  FirstTPIIntervention = uc.FirstTPIIntervention,
                                  SecondTPIAgency = uc.SecondTPIAgency,
                                  SecondTPIIntervention = uc.SecondTPIIntervention,
                                  ThirdTPIAgency = uc.ThirdTPIAgency,
                                  ThirdTPIIntervention = uc.ThirdTPIIntervention,
                                  ForthTPIAgency = uc.ForthTPIAgency,
                                  ForthTPIIntervention = uc.ForthTPIIntervention,
                                  FifthTPIAgency = uc.FifthTPIAgency,
                                  FifthTPIIntervention = uc.FifthTPIIntervention,
                                  SixthTPIAgency = uc.SixthTPIAgency,
                                  SixthTPIIntervention = uc.SixthTPIIntervention,
                              }).ToList();

                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult LoadMTHistoryData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = string.Empty;

                strWhere += "1=1 and Id=" + param.Headerid;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or (NDE.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%' or  QualityProject like '%" + param.sSearch + "%' or MTConsuNo like '%" + param.sSearch + "%'  or Status like '%" + param.sSearch + "%')";
                }
                var lstResult = db.SP_NDE_MT_CONSUMABLE_HISTORY_DETAILS
                                (
                                StartIndex, EndIndex, "", strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {
                           Convert.ToString(uc.QualityProject),
                           Convert.ToString(uc.Method),
                           Convert.ToString(uc.MTConsuNo),
                           Convert.ToString("R " +uc.RevNo),
                           Convert.ToString(uc.Status),
                           Convert.ToString(uc.CreatedBy),
                           Convert.ToString(uc.LogId)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }


        #region Copy data
        [HttpPost]
        public ActionResult GetCopyDetails(string projectid) //partial for Copy Details
        {
            //ViewBag.QualityProject = new SelectList(db.TMP001.ToList(), "QualityProject", "QualityProject");
            return PartialView("~/Areas/NDE/Views/Shared/_CopyQualityProjectPartial.cshtml");
        }

        public class ResponceMsgWithHeaderID : clsHelper.ResponseMsg
        {
            public int HeaderID;
        }


        #endregion

        #region Classes
        public class ResponceMsgWithStatus : clsHelper.ResponseMsg
        {
            public string Status;
        }

        public class BUWiseDropdown : ProjectDataModel
        {
            public string Project { get; set; }
            public string Location { get; set; }
            public List<BULocWiseCategoryModel> lstBULocWiseMethodModel { get; set; }
            public List<BULocWiseCategoryModel> lstBULocWiseBrandModel { get; set; }

            public bool Key;
            public string Value;

            public List<CategoryData> TPIAgency { get; set; }
            public List<CategoryData> TPIIntervention { get; set; }
            public List<CategoryData> Stage { get; set; }


        }

        #endregion

        #region Utility Grid
        [SessionExpireFilter]
        public ActionResult List(string Project = "")
        {
            NDEModels objNDEModels = new NDEModels();
            if(Project!="")
            {
                ViewBag.Project = Project.Split('-')[0].Trim();
            }
            else
            {
                ViewBag.Project = "";
            }
            return View();
        }

        public ActionResult GetGridDataPartial(string status, string Project)
        {
            ViewBag.status = status;
            ViewBag.Project = Project;
            return PartialView("_GetGridDataPartial");
        }
        public JsonResult LoadTPIAgencyData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;

                string strWhere = "1=1";
                if (param.Status.ToUpper() == "PENDING")
                {
                    strWhere += " and (Isnull(FirstTPIAgency,'')='' and Isnull(FirstTPIIntervention,'')='')";
                }
                if (!string.IsNullOrWhiteSpace(param.Project))
                {
                    strWhere += " and Project='" + param.Project + "'";
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    strWhere += " and  (QualityProject like '%" + param.sSearch + "%' or Stage like '%" + param.sSearch + "%' or StageType like '%" + param.sSearch + "%' or StageDesc like '%" + param.sSearch
                        + "%' or dbo.fun_GETCATEGORYONLYDESCRIPTION(FirstTPIAgency, tm.Location, tm.BU, 'TPI Agency'))'%" + param.sSearch
                        + "%' or dbo.fun_GETCATEGORYONLYDESCRIPTION(FirstTPIIntervention, tm.Location, tm.BU, 'TPI Intervention'))  '%" + param.sSearch
                        + "%' or dbo.fun_GETCATEGORYONLYDESCRIPTION(SecondTPIAgency, tm.Location, tm.BU, 'TPI Agency'))	'%" + param.sSearch
                        + "%' or dbo.fun_GETCATEGORYONLYDESCRIPTION(SecondTPIIntervention, tm.Location, tm.BU, 'TPI Intervention')) '%" + param.sSearch
                        + "%' or dbo.fun_GETCATEGORYONLYDESCRIPTION(ThirdTPIAgency, tm.Location, tm.BU, 'TPI Agency'))	'%" + param.sSearch
                        + "%' or dbo.fun_GETCATEGORYONLYDESCRIPTION(ThirdTPIIntervention, tm.Location, tm.BU, 'TPI Intervention'))  '%" + param.sSearch
                        + "%' or dbo.fun_GETCATEGORYONLYDESCRIPTION(ForthTPIAgency, tm.Location, tm.BU, 'TPI Agency'))	'%" + param.sSearch
                        + "%' or dbo.fun_GETCATEGORYONLYDESCRIPTION(ForthTPIIntervention, tm.Location, tm.BU, 'TPI Intervention'))  '%" + param.sSearch
                        + "%' or dbo.fun_GETCATEGORYONLYDESCRIPTION(FifthTPIAgency, tm.Location, tm.BU, 'TPI Agency'))	'%" + param.sSearch
                        + "%' or dbo.fun_GETCATEGORYONLYDESCRIPTION(FifthTPIIntervention, tm.Location, tm.BU, 'TPI Intervention')) '%" + param.sSearch
                        + "%' or dbo.fun_GETCATEGORYONLYDESCRIPTION(SixthTPIAgency, tm.Location, tm.BU, 'TPI Agency'))'%" + param.sSearch
                        + "%' or dbo.fun_GETCATEGORYONLYDESCRIPTION(SixthTPIIntervention, tm.Location, tm.BU, 'TPI Intervention'))  '%" + param.sSearch
                        + "%' or B.StageDesc  '%" + param.sSearch + "%'"
                        + ")";
                }
                else
                {
                    strWhere += Manager.MakeDatatableForSearch(param.SearchFilter);
                }

                string strSortOrder = string.Empty;
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstResult = db.SP_TPT_GetTPIAgencyHeader
                                (
                                StartIndex, EndIndex, strSortOrder, strWhere
                                ).ToList();

                var data = (from uc in lstResult
                            select new[]
                           {

                               Convert.ToString(uc.Id),
                               Convert.ToString(uc.BU),
                               Convert.ToString(uc.Location),
                               Convert.ToString(uc.QualityProject),
                               Convert.ToString(uc.StageDesc),
                               Convert.ToString(uc.FirstTPIAgencyDesc),
                               Convert.ToString(uc.FirstTPIInterventionDesc),
                               Convert.ToString(uc.SecondTPIAgencyDesc),
                               Convert.ToString(uc.SecondTPIInterventionDesc),
                               Convert.ToString(uc.ThirdTPIAgencyDesc),
                               Convert.ToString(uc.ThirdTPIInterventionDesc),
                               Convert.ToString(uc.ForthTPIAgencyDesc),
                               Convert.ToString(uc.ForthTPIInterventionDesc),
                               Convert.ToString(uc.FifthTPIAgencyDesc),
                               Convert.ToString(uc.FifthTPIInterventionDesc),
                               Convert.ToString(uc.SixthTPIAgencyDesc),
                               Convert.ToString(uc.SixthTPIInterventionDesc),
                               "<center>"+
                               "<a href=\"" + WebsiteURL + "/IPI/MaintainTPIAgencyIntervention/AddTPIAgency?Id=" + uc.Id + "\"><i class=\"iconclass fa fa-eye\"></i></a>"+
                               Helper.HTMLActionString(uc.Id, "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + uc.Id + ");") + Helper.HTMLActionString(uc.Id, "Update", "Update Record", "fa fa-floppy-o", "SaveEditRecord(" + uc.Id + ");", "", false, "display:none") + Helper.HTMLActionString(uc.Id, "Cancel", "Cancel Edit", "fa fa-ban", "CancelEditLine(" + uc.Id + "); ", "", false, "display:none")+
                               ((!string.IsNullOrWhiteSpace(Convert.ToString(uc.FirstTPIAgency)) && !string.IsNullOrWhiteSpace(Convert.ToString(uc.FirstTPIIntervention))) ? "<i id='btnCopyData' name='Copy' title='Copy Record' class='iconspace fa fa-files-o' onclick='CopyTPI("+uc.Id+",\""+uc.Project +"\",\""+uc.QualityProject +"\");'></i>" : "<i id='btnCopyData' name='Copy' title='Copy Record' class='disabledicon fa fa-files-o'></i>" )+
                               ((!string.IsNullOrWhiteSpace(Convert.ToString(uc.FirstTPIAgency)) && !string.IsNullOrWhiteSpace(Convert.ToString(uc.FirstTPIIntervention))) ? "<i id='btnCopyTPIAgency' name='Copy TPI Agency' title='Copy TPI Agency' class='iconspace fa fa-files-o' onclick='CopyTPIAgencyWithinProject("+uc.Id+");'></i>" : "<i name='Copy TPI Agency' title='Copy TPI Agency' class='disabledicon fa fa-files-o'></i>" )+
                               ((!string.IsNullOrWhiteSpace(Convert.ToString(uc.FirstTPIAgency)) && !string.IsNullOrWhiteSpace(Convert.ToString(uc.FirstTPIIntervention))) ? "<i id='btnUpdateTPIData' name='Update TPI Data' title='Update TPI Data in ICL' class='iconspace fa fa-share-square' onclick='UpdateTPIPopup("+uc.Id+",\""+uc.Project +"\",\""+uc.QualityProject +"\");'></i>":"<i id='btnUpdateTPIData' name='Update TPI Data' title='Update TPI Data' class='disabledicon fa fa-share-square'></i>" )+
                               "</center>"
            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 && lstResult.FirstOrDefault().TotalCount > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = strWhere
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                ex.Message.ToString();
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public JsonResult LoadTPIAgencyEditableData(int id, bool isReadOnly = false)
        {
            try
            {
                var data = new List<string[]>();
                if (id > 0)
                {
                    var lstResult = db.SP_TPT_GetTPIAgencyHeader(1, 0, "", "A.Id = " + id).Take(1).ToList();

                    data = (from uc in lstResult
                            select new[]
                           {
                                isReadOnly ? Convert.ToString(uc.Id): Helper.GenerateHidden(uc.Id, "BU",uc.BU),
                                isReadOnly ? Convert.ToString(uc.BU): Helper.GenerateHidden(uc.Id, "Location",uc.Location),
                                isReadOnly ? Convert.ToString(uc.Location):Convert.ToString(uc.Location),
                                isReadOnly ? Convert.ToString(uc.QualityProject):Convert.ToString(uc.QualityProject),
                                isReadOnly ? Convert.ToString(uc.StageDesc):Convert.ToString(uc.StageDesc),
                                isReadOnly ? Convert.ToString(uc.FirstTPIAgency):Helper.HTMLAutoComplete(uc.Id, "txtFirstTPIAgency", uc.FirstTPIAgencyDesc, "", false, "", "FirstTPIAgency", false,"","","Select","clsMandatory") + "" + Helper.GenerateHidden(uc.Id, "FirstTPIAgency", uc.FirstTPIAgency) ,
                                isReadOnly ? Convert.ToString(uc.FirstTPIIntervention):Helper.HTMLAutoComplete(uc.Id, "txtFirstTPIIntervention", uc.FirstTPIInterventionDesc,"", false, "", "FirstTPIIntervention", false,"","","Select","clsMandatory") + "" + Helper.GenerateHidden(uc.Id, "FirstTPIIntervention", uc.FirstTPIIntervention) ,
                                isReadOnly ? Convert.ToString(uc.SecondTPIAgency):Helper.HTMLAutoComplete(uc.Id, "txtSecondTPIAgency", uc.SecondTPIAgencyDesc, "UpdateIntervention(this," + uc.Id + ")", false, "", "SecondTPIAgency", false,"","","Select") + "" + Helper.GenerateHidden(uc.Id, "SecondTPIAgency", uc.SecondTPIAgency) ,
                                isReadOnly ? Convert.ToString(uc.SecondTPIIntervention):Helper.HTMLAutoComplete(uc.Id, "txtSecondTPIIntervention", uc.SecondTPIInterventionDesc, "UpdateIntervention(this," + uc.Id + ")", false, "", "SecondTPIIntervention", false,"","","Select") + "" + Helper.GenerateHidden(uc.Id, "SecondTPIIntervention", uc.SecondTPIIntervention) ,
                                isReadOnly ? Convert.ToString(uc.ThirdTPIAgency):Helper.HTMLAutoComplete(uc.Id, "txtThirdTPIAgency", uc.ThirdTPIAgencyDesc, "UpdateIntervention(this," + uc.Id + ")", false, "", "ThirdTPIAgency", false,"","","Select") + "" + Helper.GenerateHidden(uc.Id, "ThirdTPIAgency", uc.ThirdTPIAgency) ,
                                isReadOnly ? Convert.ToString(uc.ThirdTPIIntervention):Helper.HTMLAutoComplete(uc.Id, "txtThirdTPIIntervention", uc.ThirdTPIInterventionDesc, "UpdateIntervention(this," + uc.Id + ")", false, "", "ThirdTPIIntervention", false,"","","Select") + "" + Helper.GenerateHidden(uc.Id, "ThirdTPIIntervention", uc.ThirdTPIIntervention) ,
                                isReadOnly ? Convert.ToString(uc.ForthTPIAgency):Helper.HTMLAutoComplete(uc.Id, "txtForthTPIAgency", uc.ForthTPIAgencyDesc, "UpdateIntervention(this," + uc.Id + ")", false, "", "ForthTPIAgency", false,"","","Select") + "" + Helper.GenerateHidden(uc.Id, "ForthTPIAgency", uc.ForthTPIAgency),
                                isReadOnly ? Convert.ToString(uc.ForthTPIIntervention):Helper.HTMLAutoComplete(uc.Id, "txtForthTPIIntervention", uc.ForthTPIInterventionDesc, "UpdateIntervention(this," + uc.Id + ")", false, "", "ForthTPIIntervention", false,"","","Select") + "" + Helper.GenerateHidden(uc.Id, "ForthTPIIntervention", uc.ForthTPIIntervention) ,
                                isReadOnly ? Convert.ToString(uc.FifthTPIAgency):Helper.HTMLAutoComplete(uc.Id, "txtFifthTPIAgency", uc.FifthTPIAgencyDesc, "UpdateIntervention(this," + uc.Id + ")", false, "", "FifthTPIAgency", false,"","","Select") + "" + Helper.GenerateHidden(uc.Id, "FifthTPIAgency", uc.FifthTPIAgency),
                                isReadOnly ? Convert.ToString(uc.FifthTPIIntervention):Helper.HTMLAutoComplete(uc.Id, "txtFifthTPIIntervention", uc.FifthTPIInterventionDesc, "UpdateIntervention(this," + uc.Id + ")", false, "", "FifthTPIIntervention", false,"","","Select") + "" + Helper.GenerateHidden(uc.Id, "FifthTPIIntervention", uc.FifthTPIIntervention)  ,
                                isReadOnly ? Convert.ToString(uc.SixthTPIAgency):Helper.HTMLAutoComplete(uc.Id, "txtSixthTPIAgency", uc.SixthTPIAgencyDesc, "UpdateIntervention(this," + uc.Id + ")", false, "", "SixthTPIAgency", false,"","","Select") + "" + Helper.GenerateHidden(uc.Id, "SixthTPIAgency", uc.SixthTPIAgency),
                                isReadOnly ? Convert.ToString(uc.SixthTPIIntervention):Helper.HTMLAutoComplete(uc.Id, "txtSixthTPIIntervention", uc.SixthTPIInterventionDesc, "UpdateIntervention(this," + uc.Id + ")", false, "", "SixthTPIIntervention", false,"","","Select") + "" + Helper.GenerateHidden(uc.Id, "SixthTPIIntervention", uc.SixthTPIIntervention) ,
                               "<center>"+
                               "<a href=\"" + WebsiteURL + "/IPI/MaintainTPIAgencyIntervention/AddTPIAgency?Id=" + uc.Id + "\"><i class=\"iconclass fa fa-eye\"></i></a>"+
                               Helper.HTMLActionString(uc.Id, "Edit", "Edit Record", "fa fa-pencil-square-o", "EnabledEditLine(" + uc.Id + ");","",false,isReadOnly ? "":"display:none") + Helper.HTMLActionString(uc.Id, "Update", "Update Record", "fa fa-floppy-o", "SaveEditRecord(" + uc.Id + ");", "", false, isReadOnly ? "display:none":"") + Helper.HTMLActionString(uc.Id, "Cancel", "Cancel Edit", "fa fa-ban", "CancelEditLine(" + uc.Id + "); ", "", false,isReadOnly ? "display:none":"") +
                               ((!string.IsNullOrWhiteSpace(Convert.ToString(uc.FirstTPIAgency)) && !string.IsNullOrWhiteSpace(Convert.ToString(uc.FirstTPIIntervention))) ? "<i id='btnCopyData' name='Copy' title='Copy Record' class='iconspace fa fa-files-o' onclick='CopyTPI("+uc.Id+",\""+uc.Project +"\",\""+uc.QualityProject +"\");'></i>" : "<i id='btnCopyData' name='Copy' title='Copy Record' class='disabledicon fa fa-files-o'></i>" )+
                               ((!string.IsNullOrWhiteSpace(Convert.ToString(uc.FirstTPIAgency)) && !string.IsNullOrWhiteSpace(Convert.ToString(uc.FirstTPIIntervention))) ? "<i id='btnUpdateTPIData' name='UpdateTPIData' title='Update TPI Data  in Seam ICL' class='iconspace fa fa-share-square' onclick='UpdateTPIPopup("+uc.Id+",\""+uc.Project +"\",\""+uc.QualityProject +"\");'></i>":"<i id='btnUpdateTPIData' name='Update TPI Data' title='Update TPI Data' class='disabledicon fa fa-share-square'></i>" )+
                               "</center>"
                           }).ToList();
                }
                else
                {
                    int newRecordId = 0;
                    data.Add(new[]
                           {
                                "0",
                                Helper.GenerateHidden(newRecordId, "BU",""),
                                Helper.GenerateHidden(newRecordId, "Location",""),
                                Helper.HTMLAutoComplete(newRecordId, "txtQualityProject", "", "", false, "", "QualityProject", false,"","","Select","clsMandatory") + "" + Helper.GenerateHidden(newRecordId, "QualityProject", "") ,
                                Helper.HTMLAutoComplete(newRecordId, "txtStageCode", "", "", false, "", "StageCode", false,"","","Select","clsMandatory") + "" + Helper.GenerateHidden(newRecordId, "StageCode", "") ,
                                Helper.HTMLAutoComplete(newRecordId, "txtFirstTPIAgency", "", "", false, "", "FirstTPIAgency", false,"","","Select","clsMandatory") + "" + Helper.GenerateHidden(newRecordId, "FirstTPIAgency", "") ,
                                Helper.HTMLAutoComplete(newRecordId, "txtFirstTPIIntervention", "","", false, "", "FirstTPIIntervention", false,"","","Select","clsMandatory") + "" + Helper.GenerateHidden(newRecordId, "FirstTPIIntervention", "") ,
                                Helper.HTMLAutoComplete(newRecordId, "txtSecondTPIAgency","", "", false, "", "SecondTPIAgency", false,"","","Select") + "" + Helper.GenerateHidden(newRecordId, "SecondTPIAgency", "") ,
                                Helper.HTMLAutoComplete(newRecordId, "txtSecondTPIIntervention", "", "", false, "", "SecondTPIIntervention", false,"","","Select") + "" + Helper.GenerateHidden(newRecordId, "SecondTPIIntervention", "") ,
                                Helper.HTMLAutoComplete(newRecordId, "txtThirdTPIAgency", "", "", false, "", "ThirdTPIAgency", false,"","","Select") + "" + Helper.GenerateHidden(newRecordId, "ThirdTPIAgency", "") ,
                                Helper.HTMLAutoComplete(newRecordId, "txtThirdTPIIntervention", "", "", false, "", "ThirdTPIIntervention", false,"","","Select") + "" + Helper.GenerateHidden(newRecordId, "ThirdTPIIntervention","") ,
                                Helper.HTMLAutoComplete(newRecordId, "txtForthTPIAgency", "", "", false, "", "ForthTPIAgency", false,"","","Select") + "" + Helper.GenerateHidden(newRecordId, "ForthTPIAgency", ""),
                                Helper.HTMLAutoComplete(newRecordId, "txtForthTPIIntervention", "", "", false, "", "ForthTPIIntervention", false,"","","Select") + "" + Helper.GenerateHidden(newRecordId, "ForthTPIIntervention","") ,
                                Helper.HTMLAutoComplete(newRecordId, "txtFifthTPIAgency", "", "", false, "", "FifthTPIAgency", false,"","","Select") + "" + Helper.GenerateHidden(newRecordId, "FifthTPIAgency", ""),
                                Helper.HTMLAutoComplete(newRecordId, "txtFifthTPIIntervention","", "", false, "", "FifthTPIIntervention", false,"","","Select") + "" + Helper.GenerateHidden(newRecordId, "FifthTPIIntervention", "")  ,
                                Helper.HTMLAutoComplete(newRecordId, "txtSixthTPIAgency", "", "", false, "", "SixthTPIAgency", false,"","","Select") + "" + Helper.GenerateHidden(newRecordId, "SixthTPIAgency", ""),
                                Helper.HTMLAutoComplete(newRecordId, "txtSixthTPIIntervention","", "", false, "", "SixthTPIIntervention", false,"","","Select") + "" + Helper.GenerateHidden(newRecordId, "SixthTPIIntervention", "") ,
                               "<center>"+ Helper.HTMLActionString(id,"Add","Add New Record","fa fa-plus","SaveEditRecord(0);")+"</center>"
                           });
                }

                return Json(new
                {
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    aaData = new string[] { }
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult SaveUtiltiyTPI(FormCollection fc)
        {
            int Id = Convert.ToInt32(fc["MainId"]);
            QMS025 objQMS025 = new QMS025();
            ResponceMsgWithStatus objResponseMsg = new ResponceMsgWithStatus();
            try
            {
                string Status = clsImplementationEnum.NDETechniqueStatus.Draft.GetStringValue();
                string Approved = clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue();
                if (Id > 0)
                {
                    objQMS025 = db.QMS025.Where(x => x.Id == Id).FirstOrDefault();
                    if (objQMS025 != null)
                    {
                        objQMS025.FirstTPIAgency = fc["FirstTPIAgency" + Id];
                        objQMS025.FirstTPIIntervention = fc["FirstTPIIntervention" + Id];
                        objQMS025.SecondTPIAgency = fc["SecondTPIAgency" + Id];
                        objQMS025.SecondTPIIntervention = fc["SecondTPIIntervention" + Id];
                        objQMS025.ThirdTPIAgency = fc["ThirdTPIAgency" + Id];
                        objQMS025.ThirdTPIIntervention = fc["ThirdTPIIntervention" + Id];
                        objQMS025.ForthTPIAgency = fc["ForthTPIAgency" + Id];
                        objQMS025.ForthTPIIntervention = fc["ForthTPIIntervention" + Id];
                        objQMS025.FifthTPIAgency = fc["FifthTPIAgency" + Id];
                        objQMS025.FifthTPIIntervention = fc["FifthTPIIntervention" + Id];
                        objQMS025.SixthTPIAgency = fc["SixthTPIAgency" + Id];
                        objQMS025.SixthTPIIntervention = fc["SixthTPIIntervention" + Id];
                        objQMS025.EditedBy = objClsLoginInfo.UserName;
                        objQMS025.EditedOn = DateTime.Now;
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = Id.ToString();
                    }
                    // objResponseMsg.Status = objNDE020.Status;

                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
                }
                else
                {
                    string QualityProject = fc["QualityProject" + Id];
                    string Project = db.QMS010.Where(a => a.QualityProject == QualityProject).FirstOrDefault().Project;
                    string BU = fc["BU" + Id];
                    string Location = fc["Location" + Id];
                    string StageCode = fc["StageCode" + Id];

                    if (!db.QMS025.Any(x => x.QualityProject == QualityProject && x.Project == Project && x.Location == Location && x.BU == BU && x.Stage == StageCode))
                    {
                        objQMS025 = new QMS025();
                        objQMS025.QualityProject = QualityProject;
                        objQMS025.Project = Project;
                        objQMS025.BU = BU;
                        objQMS025.Location = Location;
                        objQMS025.Stage = StageCode;
                        objQMS025.FirstTPIAgency = fc["FirstTPIAgency" + Id];
                        objQMS025.FirstTPIIntervention = fc["FirstTPIIntervention" + Id];
                        objQMS025.SecondTPIAgency = fc["SecondTPIAgency" + Id];
                        objQMS025.SecondTPIIntervention = fc["SecondTPIIntervention" + Id];
                        objQMS025.ThirdTPIAgency = fc["ThirdTPIAgency" + Id];
                        objQMS025.ThirdTPIIntervention = fc["ThirdTPIIntervention" + Id];
                        objQMS025.ForthTPIAgency = fc["ForthTPIAgency" + Id];
                        objQMS025.ForthTPIIntervention = fc["ForthTPIIntervention" + Id];
                        objQMS025.FifthTPIAgency = fc["FifthTPIAgency" + Id];
                        objQMS025.FifthTPIIntervention = fc["FifthTPIIntervention" + Id];
                        objQMS025.SixthTPIAgency = fc["SixthTPIAgency" + Id];
                        objQMS025.SixthTPIIntervention = fc["SixthTPIIntervention" + Id];
                        objQMS025.CreatedBy = objClsLoginInfo.UserName;
                        objQMS025.CreatedOn = DateTime.Now;
                        db.QMS025.Add(objQMS025);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Message.ToString();

                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg);



        }

        [HttpPost]
        public JsonResult FillAutocompleteForTPI(string term, string qualityproject, string bu, string location, bool isAgency)
        {   //get project and BU by Qms project

            if (isAgency)
            {
                //var lstTPIAgencyX = Manager.GetSubCatagories("TPI Agency", bu, location)
                //                          .Where(x => (term == "" || (x.Code.ToLower().Contains(term.ToLower()) || x.Description.ToLower().Contains(term.ToLower()))))
                //                          .Select(i => new { Value = i.Code, Text = i.Description }).ToList();

                var lstTPIAgency = Manager.GetSubCatagories("TPI Agency", bu, location)
                                            .Where(x => (term == "" || (x.Code.ToLower().Contains(term.ToLower()) || x.Description.ToLower().Contains(term.ToLower()))))
                                            .Select(i => new { Value = i.Code, Text = i.Description }).ToList();

                //Added By Deval Barot on 11-03-2020
                //This will display agency which is available in qms010

                // 
                var qms010Result = db.QMS010.Where(x => x.QualityProject == qualityproject && x.Location == location).FirstOrDefault();
                //

                //var lstTPIAgency = db.GLB002.Where(x => x.Location == location && x.BU == bu && (x.Code == qms010Result.TPI1
                //                            || x.Code == qms010Result.TPI2 || x.Code == qms010Result.TPI3
                //                            || x.Code == qms010Result.TPI4 || x.Code == qms010Result.TPI5
                //                            || x.Code == qms010Result.TPI6))
                //                    .Select(i => new { Value = i.Code, Text = i.Description }).ToList().Distinct();

                //var lstTPIAgency = (from glb002 in db.GLB002
                //                    join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                //                    where glb001.Category.Equals("TPI Agency") && glb002.IsActive == true && glb002.BU == bu && glb002.Location == location && (glb002.Code == qms010Result.TPI1
                //                                               || glb002.Code == qms010Result.TPI2 || glb002.Code == qms010Result.TPI3
                //                                               || glb002.Code == qms010Result.TPI4 || glb002.Code == qms010Result.TPI5
                //                                               || glb002.Code == qms010Result.TPI6)
                //                    select new { Value = glb002.Code, Text = glb002.Description }).ToList().Distinct();

                //End Here

                return Json(lstTPIAgency, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var lstTPIIntervention = Manager.GetSubCatagories("TPI Intervention", bu, location)
                                                .Where(x => (term == "" || (x.Code.ToLower().Contains(term.ToLower()) || x.Description.ToLower().Contains(term.ToLower()))))
                                                .Select(i => new { Value = i.Code, Text = i.Description }).ToList();

                return Json(lstTPIIntervention, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult UpdateDetails(int id, string columnName, string columnValue)
        {
            var objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                db.SP_COMMON_TABLE_UPDATE("QMS025", id, "Id", columnName, columnValue, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}