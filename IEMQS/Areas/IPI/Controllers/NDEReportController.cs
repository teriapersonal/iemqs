﻿using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class NDEReportController : clsBase
    {
        // GET: IPI/NDEReport


        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            ViewBag.Title = "NDE Reports";
            return View();
        }

        [HttpPost]
        public ActionResult LoadNDEReportInputFilterPartial(string reporttype)
        {
            //0 - false, 1 - true

            var lstStageType = new List<SelectListItem>()
            { new SelectListItem { Text = "PT", Value = "PT" },
              new SelectListItem { Text = "MT", Value = "MT" },
              new SelectListItem { Text = "UT", Value = "UT" },
              new SelectListItem { Text = "RT", Value = "RT" },
            };
            ViewBag.StageType = lstStageType;
            ViewBag.ReportType = reporttype;
            ViewBag.PrintedBy = objClsLoginInfo.UserName;

            var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
            var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();

            var listLocation = (from c1 in db.COM002
                                where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                              ).Distinct().ToList();
            ViewBag.Location = listLocation;
            var location1 = (from a in db.COM003
                             join b in db.COM002 on a.t_loca equals b.t_dimx
                             where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase) && a.t_actv == 1
                             select b.t_dimx + " - " + b.t_desc).FirstOrDefault();
            ViewBag.WeldingProcess = GetSubCatagory("Welding Process", location1.Split('-')[0]).Select(i => new { i.Code, i.Description }).Distinct().Select(i => new { id = i.Code, text = i.Code + "-" + i.Description }).Distinct().ToList();


            if (reporttype == clsImplementationEnum.IPI_NDEReports.Annexure_II.GetStringValue())
            {
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowStageType = 1;
                ViewBag.ShowMultiSelectLocation = 1;
                ViewBag.ShowFromToDate = 1;
                ViewBag.ShowMultiSelectStage = 1;
            }
            else if (reporttype == clsImplementationEnum.IPI_NDEReports.Seam_Status.GetStringValue())
            {
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowStageType = 1;
                ViewBag.ShowMultiSelectLocation = 1;
                ViewBag.ShowFromToDate = 1;
                ViewBag.ShowMultiSelectStage = 1;
            }
            else if (reporttype == clsImplementationEnum.IPI_NDEReports.MT_Report.GetStringValue())
            {
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowFromToSeamNo = 1;
                ViewBag.ShowFromToStage = 1;
                ViewBag.ShowFromToDate = 1;
                ViewBag.ShowLocation = 1;
                ViewBag.StageTypeName = "MT";
            }
            else if (reporttype == clsImplementationEnum.IPI_NDEReports.PT_Report.GetStringValue())
            {
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowFromToSeamNo = 1;
                ViewBag.ShowFromToStage = 1;
                ViewBag.ShowFromToDate = 1;
                ViewBag.ShowLocation = 1;
                ViewBag.StageTypeName = "PT";
            }
            else if (reporttype == clsImplementationEnum.IPI_NDEReports.NDE_DPP.GetStringValue())
            {
                ViewBag.ShowLocationOnTop = 1;
                ViewBag.ShowStageTypeNotRequired = 1;
                ViewBag.ShowFromToDateRequired = 1;
                ViewBag.ShowFromToShop = 1;

                var lstDPPReportType = new List<SelectListItem>()
                { new SelectListItem { Text = "General", Value = "General" },
                  new SelectListItem { Text = "Specific", Value = "Specific" }
                };
                ViewBag.DPPReportType = lstDPPReportType;
                ViewBag.ShowDPPReportType = 1;

            }
            else if (reporttype == clsImplementationEnum.IPI_NDEReports.Request_Details.GetStringValue())
            {
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowFromToSeamNo = 1;
                ViewBag.ShowFromToDateRequired = 1;
                var lstSeamRequestStatus = new List<SelectListItem>()
                                            { 
                                              new SelectListItem { Text = "Attended", Value = "Attended" },
                                              new SelectListItem { Text = "Not Attended", Value = "Not Attended" }
                                            };

                ViewBag.SeamRequestStatus = lstSeamRequestStatus;
                ViewBag.DDLRequestStatus = 1;

            }
            else if (reporttype == clsImplementationEnum.IPI_NDEReports.Daily_Attended_Report.GetStringValue())
            {
                ViewBag.ShowFromToDate = 1;
                //ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowLocation = 1;
                ViewBag.ShowFromToShop = 1;
                ViewBag.ShowStageTypeNotRequired = 1;
                ViewBag.ShowFromToQualityProjectNotRequired = 1;
            }
            else if (reporttype == clsImplementationEnum.IPI_NDEReports.Repair_Data.GetStringValue())
            {
                ViewBag.ShowFromToDate = 1;
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowLocation = 1;
                ViewBag.ShowFromToShop = 1;
                ViewBag.ShowStageTypeNotRequired = 1;
                ViewBag.ShowWeldingProcess = 1;
            }
            else if (reporttype == clsImplementationEnum.IPI_NDEReports.Reshoot_Report.GetStringValue())
            {
                lstStageType.Clear();
                lstStageType.Add(new SelectListItem { Text = "UT", Value = "UT" });
                lstStageType.Add(new SelectListItem { Text = "RT", Value = "RT" });

                ViewBag.ShowFromToDate = 1;
                ViewBag.ShowFromToQualityProjectNotRequired = 1;
                ViewBag.ShowLocation = 1;
                ViewBag.ShowFromToShop = 1;
                ViewBag.ShowStageTypeNotRequired = 1;
            }
            else if (reporttype == clsImplementationEnum.IPI_NDEReports.TPIWiseSummary_Report.GetStringValue())
            {

                //
                var TPIAgencyNameList = Manager.GetSubCatagories("TPI Agency").GroupBy(t => t.Code)
                                                        .Select(c => new SelectListItem
                                                        {
                                                            Value = c.Select(x => x.Code).First(),
                                                            Text = c.Select(x => x.Description).First()
                                                        });
                ViewBag.TPIAgencyNameList = TPIAgencyNameList;
                //

                ViewBag.ShowTPIAgencyType = 1;
                ViewBag.ShowFromToQualityProjectNotRequired = 1;
                ViewBag.ShowFromToSeamNo = 1;
                //ViewBag.ShowStageType = 1;
                ViewBag.ShowMultiSelectStageType = 1;
                ViewBag.ShowFromToStage = 1;
                ViewBag.ShowFromToDate = 1;
                //ViewBag.ShowLocation = 1;
                //ViewBag.ShowFromToShop = 1;
                //ViewBag.ShowStageTypeNotRequired = 1;
            }
            else if (reporttype == clsImplementationEnum.IPI_NDEReports.Log_For_PT_MT.GetStringValue())
            {

                //

                //
                ViewBag.ShowFromToQualityProjectNotRequired = 1;
                ViewBag.ShowFromToDateRequired = 1;
                ViewBag.ShowLocation = 1;
                //ViewBag.ShowFromToShop = 1;
                //ViewBag.ShowStageTypeNotRequired = 1;
            }
            else if (reporttype == clsImplementationEnum.IPI_NDEReports.Annexure_II_TPI.GetStringValue())
            {
                ViewBag.ShowFromToQualityProject = 1;
                ViewBag.ShowStageType = 1;
                ViewBag.ShowMultiSelectLocation = 1;
                ViewBag.ShowFromToDate = 1;
                ViewBag.ShowMultiSelectStage = 1;
            }
            return PartialView("_NDEReportInputFilterHtmlPartial");
        }

        public List<GLB002> GetSubCatagory(string Key, string strLoc)
        {
            List<GLB002> lstGLB002 = (from glb002 in db.GLB002
                                      join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                                      where glb001.Category.Equals(Key, StringComparison.OrdinalIgnoreCase) && glb002.IsActive == true && strLoc.Equals(glb002.Location)
                                      select glb002).ToList();
            //var category = lstGLB002.Select(i => new { i.Code, i.Description }).Distinct().ToList();
            return lstGLB002;
        }
        //public ActionResult Index()
        //{
        //    ViewBag.Title = "NDE Report";
        //    ViewBag.ReportType = new List<SelectListItem>()
        //    { new SelectListItem { Text = "Annexure", Value = "ANNEXURE II" },
        //      new SelectListItem { Text = "Seam Status", Value = "SEAM STATUS" },
        //      new SelectListItem { Text = "MT Report", Value = "MT_NDE_Report" },
        //      new SelectListItem { Text = "PT Report", Value = "PT_NDE_Report" },
        //    };

        //    ViewBag.StageType = new List<SelectListItem>()
        //    { new SelectListItem { Text = "PT", Value = "PT" },
        //      new SelectListItem { Text = "MT", Value = "MT" },
        //      new SelectListItem { Text = "UT", Value = "UT" },
        //      new SelectListItem { Text = "RT", Value = "RT" },
        //    };

        //    var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
        //    var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();

        //    var listLocation = (from c1 in db.COM002
        //                        where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
        //                        select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
        //                      ).Distinct().ToList();
        //    ViewBag.Location = listLocation;
        //    return View();
        //}

        [HttpPost]
        public ActionResult GetToAuthorisedQualityProject(string term, string fromproject)
        {
            try
            {

                var bu = db.QMS010.Where(i => i.QualityProject.Equals(fromproject, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).FirstOrDefault();

                List<Projects> lstProjectByUser = Manager.getProjectsByUser(objClsLoginInfo.UserName.Trim());
                var item = (from lst in lstProjectByUser
                            join qms10 in db.QMS010 on lst.projectCode.Trim() equals qms10.Project.Trim()
                            where (term == "" || qms10.QualityProject.Trim().Contains(term.Trim())) && qms10.BU.Trim() == bu.Trim()
                            orderby qms10.QualityProject
                            select new { Value = qms10.QualityProject, Text = qms10.QualityProject }).Distinct().Take(10).ToList();

                return Json(item, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json("", JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetSeamNoResult(string term = "", string fromQProject = "", string toQProject = "")
        {
            var lstAllQualityProject = (from a in db.QMS060
                                        where a.HeaderId != null
                                        select a.QualityProject).OrderBy(x => x.ToString()).Distinct().ToList();

            List<string> newQualityProjectList = new List<string>();
            bool start = false;

            foreach (var proj in lstAllQualityProject)
            {
                if (proj == fromQProject)
                {
                    start = true;
                }

                if (start)
                    newQualityProjectList.Add(proj);

                if (proj == toQProject)
                {
                    break;
                }
            }
            newQualityProjectList = newQualityProjectList.Distinct().ToList();
            var lstRequestNo = (from a in db.QMS060
                                where newQualityProjectList.Contains(a.QualityProject) && (term != "" ? a.SeamNo.Contains(term) : true)
                                select new { seamNo = a.SeamNo }).Distinct().Take(10).ToList();
            lstRequestNo.Insert(0, new { seamNo = "All" });
            return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSeamsbyQualityproject(string term, string location, string fromProject, string toProject)
        {
            if (string.IsNullOrWhiteSpace(location))
            {
                location = objClsLoginInfo.Location;
            }

            if (!string.IsNullOrWhiteSpace(term))
            {
                if (!string.IsNullOrWhiteSpace(fromProject)){
                    var lstSeamNo = (from a in db.QMS012
                                     where a.SeamNo.Contains(term) && a.QualityProject.CompareTo(fromProject) >= 0 && a.QualityProject.CompareTo(toProject) <= 0
                                     select new { seamNo = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.seamNo).ToList();
                    return Json(lstSeamNo, JsonRequestBehavior.AllowGet);
                }
                else {
                    var lstSeamNo = (from a in db.QMS012
                                     where a.SeamNo.Contains(term) 
                                     select new { seamNo = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.seamNo).ToList();
                    return Json(lstSeamNo, JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                if (!string.IsNullOrWhiteSpace(fromProject))
                {
                    var lstSeamNo = (from a in db.QMS012
                                     where a.QualityProject.CompareTo(fromProject) >= 0 && a.QualityProject.CompareTo(toProject) <= 0
                                     select new { seamNo = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.seamNo).ToList();
                    lstSeamNo.Insert(0, new { seamNo = "All" });
                    return Json(lstSeamNo, JsonRequestBehavior.AllowGet);
                }
                else {
                    var lstSeamNo = (from a in db.QMS012
                                     select new { seamNo = a.SeamNo }).Distinct().Take(10).OrderBy(x => x.seamNo).ToList();
                    lstSeamNo.Insert(0, new { seamNo = "All" });
                    return Json(lstSeamNo, JsonRequestBehavior.AllowGet);
                }
            }

        }


        [HttpPost]
        public ActionResult GetStagecodeResult(string term = "", string stagetype = "")
        {
            if (!string.IsNullOrWhiteSpace(stagetype))
            {
                var lstRequestNo = (from a in db.QMS060
                                    join b in db.QMS002 on new { a.StageCode, a.BU, a.Location } equals new
                                    {
                                        b.StageCode,
                                        b.BU,
                                        b.Location
                                    }
                                    where (term != "" ? a.StageCode.Contains(term) : true) && b.StageType == stagetype
                                    select new { stageCode = a.StageCode }).Distinct().ToList();
                return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
            }
            else
            {
                var lstRequestNo = (from a in db.QMS060
                                    where (term != "" ? a.StageCode.Contains(term) : true)
                                    select new { stageCode = a.StageCode }).Distinct().ToList();
                return Json(lstRequestNo, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult FetchBU(string qproject)
        {
            string bu = Manager.fun_GETBUFROMQUALITYPROJECT(qproject);
            return Json(bu, JsonRequestBehavior.AllowGet);
        }
        #region Common Methods

        [HttpPost]
        public JsonResult GetUserLocation()
        {
            try
            {
                var objATH001 = db.ATH001.Where(i => i.Employee == objClsLoginInfo.UserName).Select(i => new { BU = i.BU, Location = i.Location, Role = i.Role }).ToList();
                var objLocation = objATH001.Select(i => i.Location).Distinct().ToList();

                var listLocation = (from c1 in db.COM002
                                    where c1.t_dtyp == 1 && objLocation.Contains(c1.t_dimx)
                                    select new { id = c1.t_dimx, text = c1.t_desc, CatID = c1.t_dimx, CatDesc = c1.t_desc }
                                  ).Distinct().ToList();
                return Json(listLocation, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(null, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult GetQualityProjects(string search)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<ddlValue> lstQualityProject = new List<ddlValue>();
            if (!string.IsNullOrEmpty(search))
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.ToLower().Contains(search.ToLower())
                                                                 && lstCOMLocation.Contains(i.Location)
                                                                 && lstCOMBU.Contains(i.BU)
                                                               ).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject, Value = i.QualityProject, Text = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => lstCOMLocation.Contains(i.Location)
                                                        && lstCOMBU.Contains(i.BU)
                                                        ).Select(i => new ddlValue { id = i.QualityProject, text = i.QualityProject, Value = i.QualityProject, Text = i.QualityProject }).Take(10).Distinct().ToList();
            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetQualityProjectsV2(string term, string location = "", bool isNuclearProjectIncluded = true)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<ddlValue> lstQualityProject = new List<ddlValue>();
            if (!string.IsNullOrEmpty(term))
            {
                lstQualityProject = db.QMS010.Where(i => i.QualityProject.Contains(term)
                                                                 && (location == "" ? lstCOMLocation.Contains(i.Location) : i.Location == location)
                                                                 && (lstCOMBU.Contains(i.BU))
                                                                 && (isNuclearProjectIncluded == false ? (i.BU != "02") : (i.BU == i.BU))
                                                               ).Select(i => new ddlValue { Value = i.QualityProject, Text = i.QualityProject }).Distinct().ToList();//&& i.CreatedBy.Equals(username,StringComparison.OrdinalIgnoreCase)
            }
            else
            {
                lstQualityProject = db.QMS010.Where(i => (location == "" ? lstCOMLocation.Contains(i.Location) : i.Location == location)
                                                        && (lstCOMBU.Contains(i.BU))
                                                        && (isNuclearProjectIncluded == false ? (i.BU != "02") : (i.BU == i.BU))
                                                        ).Select(i => new ddlValue { Value = i.QualityProject, Text = i.QualityProject }).Take(10).Distinct().ToList();
            }

            return Json(lstQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetStagecode(string term, string location, string stagetype)
        {
            List<ddlValue> lstStagecode = new List<ddlValue>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstStagecode = (from a in db.QMS002
                                where a.Location == location && a.StageType == stagetype && (a.StageDesc.Contains(term) || a.StageCode.Contains(term))
                                select new ddlValue { Text = a.StageCode + "-" + a.StageDesc, Value = a.StageCode }).Distinct().ToList();
            }
            else
            {
                lstStagecode = (from a in db.QMS002
                                where a.Location == location && a.StageType == stagetype
                                select new ddlValue { Text = a.StageCode + "-" + a.StageDesc, Value = a.StageCode }).Distinct().ToList();
            }
            return Json(lstStagecode, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetShopV2(string term)
        {
            string username = objClsLoginInfo.UserName;
            var lstATHLocation = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.Location).Distinct().ToList();
            var lstCOMLocation = db.COM003.Where(i => i.t_psno.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_loca).Distinct().ToList();

            var lstCOMBU = db.ATH001.Where(i => i.Employee.Equals(username, StringComparison.OrdinalIgnoreCase)).Select(i => i.BU).Distinct().ToList();

            lstCOMLocation = lstATHLocation.Union(lstCOMLocation).ToList();

            List<ddlValue> lstShop = new List<ddlValue>();
            if (!string.IsNullOrWhiteSpace(term))
            {
                lstShop = (from glb002 in db.GLB002
                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                           where glb001.Category.Equals("Shop", StringComparison.OrdinalIgnoreCase) && lstCOMLocation.Contains(glb002.Location) && lstCOMBU.Contains(glb002.BU)
                                 && (glb002.Description.Contains(term) || glb002.Code.Contains(term))
                           select new ddlValue { Text = glb002.Description, Value = glb002.Code }).Distinct().OrderBy(x => x.Value).ToList();
            }
            else
            {
                lstShop = (from glb002 in db.GLB002
                           join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                           where glb001.Category.Equals("Shop", StringComparison.OrdinalIgnoreCase) && lstCOMLocation.Contains(glb002.Location) && lstCOMBU.Contains(glb002.BU)
                           select new ddlValue { Text = glb002.Description, Value = glb002.Code }).Distinct().OrderBy(x => x.Value).Take(10).ToList();
            }
            return Json(lstShop, JsonRequestBehavior.AllowGet);
        }

        #endregion

    }
}