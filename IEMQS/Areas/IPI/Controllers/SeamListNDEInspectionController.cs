﻿using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.NDE.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Models;
using IEMQSImplementation;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using System.Data;
using System.Globalization;

namespace IEMQS.Areas.IPI.Controllers
{
    public class SeamListNDEInspectionController : clsBase
    {

        #region QC

        [SessionExpireFilter]
        public ActionResult QCIndexView()
        {
            var role = Manager.GetUserRoles(objClsLoginInfo.UserName);
            ViewBag.UserRole = "";
            ViewBag.HeaderTitle = "Request Generation";
            var lstRoles = Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.PTMTForRepairSpots.GetStringValue()).Split(',');
            if (role.Any(i => lstRoles.Contains(i.Role)))
            {
                ViewBag.UserRole = "NDE";
                ViewBag.HeaderTitle = "PT-MT for Repair Spot";
            }
            return View();
        }
        public ActionResult GetQCSeamTestDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetQCSeamTestDataPartial");
        }

        public ActionResult loadQCSeamTestDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and RequestNo IS NOT NULL and RequestGeneratedBy is null and RequestStatus ='" + clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue() + "' and QCIsReturned IS NULL ";
                }
                else if (status.ToUpper() == "PTMTCELL")
                {
                    whereCondition += " and RequestNo IS NOT NULL and RequestGeneratedBy IS NOT NULL and ConfirmedBy IS NULL and ISNULL(IsPTMTCellRequired,0)=1 and RequestStatus ='" + clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue() + "' and QCIsReturned IS NULL ";
                }

                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project", "qms.StageCode", "qms.RequestStatus", "qms.ManufacturingCode", "qms.AcceptanceStandard", "qms.ApplicableSpecification", "qms.InspectionExtent", "qms.WeldingEngineeringRemarks", "qms.WeldingProcess", "qms.SeamStatus", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark", "qms.SurfaceCondition", "qms.WeldThicknessReinforcement", "qms.SpotGenerationtype", "qms.PrintSummary", "qms.NDETechniqueNo", "qms.NDETechniqueRevisionNo" };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty; ;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_IPI_SEAMLISTNDETEST_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           h.QualityProject,
                           Convert.ToString(h.RequestId),
                           Convert.ToString(h.HeaderId),
                           Convert.ToString( h.Project),
                           Convert.ToString( h.SeamNo),
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString(h.RequestNo),
                           Convert.ToString(h.RequestNoSequence),
                           Convert.ToString(h.RequestStatus),
                           Convert.ToString(h.SeamLength),
                           Convert.ToString(h.OverlayArea),
                           Convert.ToString(h.OverlayThickness),
                           Convert.ToString(h.Jointtype),
                           Convert.ToString(h.ManufacturingCode),
                           Convert.ToString(h.Part1Position),
                           Convert.ToString(h.Part1thickness),
                           Convert.ToString(h.Part1Material),
                           Convert.ToString(h.Part2Position),
                           Convert.ToString(h.Part2thickness),
                           Convert.ToString(h.Part2Material),
                           Convert.ToString(h.Part3Position),
                           Convert.ToString(h.Part3thickness),
                           Convert.ToString(h.Part3Material),
                           Convert.ToString(h.AcceptanceStandard),
                           Convert.ToString(h.ApplicableSpecification),
                           Convert.ToString(h.InspectionExtent),
                           Convert.ToString(h.WeldingEngineeringRemarks),
                           Convert.ToString(h.WeldingProcess),
                           Convert.ToString(h.SeamStatus),
                           Convert.ToString(h.Shop),
                           Convert.ToString(h.ShopLocation),
                           Convert.ToString(h.ShopRemark),
                           Convert.ToString(h.SurfaceCondition),
                           Convert.ToString(h.WeldThicknessReinforcement),
                           Convert.ToString(h.SpotGenerationtype),
                           Convert.ToString(h.SpotLength),
                           Convert.ToString(h.PrintSummary),
                           Convert.ToString( h.CreatedBy),
                           Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.RequestGeneratedBy),
                           Convert.ToDateTime(h.RequestGeneratedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.ConfirmedBy),
                           Convert.ToDateTime(h.ConfirmedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.NDETechniqueNo),
                           Convert.ToString( h.NDETechniqueRevisionNo),
                           "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a   href=\"" + WebsiteURL + (status.ToUpper() == "PTMTCELL" ? "/IPI/SeamListNDEInspection/PTMTConfirmation/" : "/IPI/SeamListNDEInspection/QCSeamTestDetails/")  +h.RequestId+"\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>&nbsp;"+Manager.generateIPIGridButtons(h.QualityProject,h.Project,h.BU,h.Location,h.SeamNo,"","",h.StageCode,Convert.ToString(h.StageSequence),"Seam Details",60,"true","Seam Details","Seam Request Details")+"&nbsp;&nbsp;<a  Title=\"View Timeline\" onclick=\"ShowTimeline("+ h.RequestId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a></center>"

                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    whereCondition = whereCondition,
                    strSortOrder = strSortOrder

                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult QCSeamTestDetails(int id)
        {
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == id);
            ViewBag.IsLTFPS = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
            ViewBag.SurfaceCondition = GetCategoryDescription("Surface Condition", objQMS060.SurfaceCondition, objQMS060.BU, objQMS060.Location).CategoryDescription;
            objQMS060.Project = Manager.GetProjectAndDescription(objQMS060.Project);
            objQMS060.StageCode = Manager.GetStageCodeDescriptionFromCode(objQMS060.StageCode, objQMS060.BU, objQMS060.Location);
            objQMS060.SeamNo = db.QMS012.Where(x => x.QualityProject == objQMS060.QualityProject && x.SeamNo == objQMS060.SeamNo).Select(x => x.SeamNo + "-" + x.SeamDescription).FirstOrDefault();
            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", objQMS060.BU, objQMS060.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", objQMS060.BU, objQMS060.Location, null);
            objQMS060.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency1);
            objQMS060.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency2);
            objQMS060.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency3);
            objQMS060.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency4);
            objQMS060.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency5);
            objQMS060.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency6);
            objQMS060.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency1Intervention);
            objQMS060.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency2Intervention);
            objQMS060.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency3Intervention);
            objQMS060.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency4Intervention);
            objQMS060.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency5Intervention);
            objQMS060.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency6Intervention);
            objQMS060.Location = db.COM002.Where(x => x.t_dimx == objQMS060.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS060.BU = db.COM002.Where(x => x.t_dimx == objQMS060.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS060.CreatedBy = Manager.GetPsidandDescription(objQMS060.CreatedBy);
            objQMS060.RequestGeneratedBy = Manager.GetPsidandDescription(objQMS060.RequestGeneratedBy);
            objQMS060.OfferedBy = Manager.GetPsidandDescription(objQMS060.OfferedBy);
            objQMS060.ConfirmedBy = Manager.GetPsidandDescription(objQMS060.ConfirmedBy);

            if (objQMS060.SeamStatus == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue())
            {
                objQMS060.SeamStatus = "Yes";
            }
            else
            {
                if (objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue())
                {
                    objQMS060.SeamStatus = "No";
                }
                else
                {
                    objQMS060.SeamStatus = "";
                }
            }

            ViewBag.IsQCReturned = (objQMS060.QCIsReturned == true ? "Yes" : "No");

            #region Get Welder Stamps
            if (objQMS060.Welder1 == null)
            {
                int _Count = 1;
                clsHelper.Welders objWelderDetails = new clsHelper.Welders();
                if (ViewBag.IsLTFPS)
                {
                    objWelderDetails = GetWelderStamps(objQMS060.LTF002.LineId, true);
                }
                else
                {
                    objWelderDetails = GetWelderStamps(objQMS060.QMS040.HeaderId, false);
                }

                foreach (var item in objWelderDetails.WelderStamps)
                {
                    #region maintain welder weld length
                    switch (_Count)
                    {
                        case 1:
                            objQMS060.Welder1 = item;
                            break;
                        case 2:
                            objQMS060.Welder2 = item;
                            break;
                        case 3:
                            objQMS060.Welder3 = item;
                            break;
                        case 4:
                            objQMS060.Welder4 = item;
                            break;
                        case 5:
                            objQMS060.Welder5 = item;
                            break;
                        case 6:
                            objQMS060.Welder6 = item;
                            break;
                        case 7:
                            objQMS060.Welder7 = item;
                            break;
                        case 8:
                            objQMS060.Welder8 = item;
                            break;
                        case 9:
                            objQMS060.Welder9 = item;
                            break;
                        case 10:
                            objQMS060.Welder10 = item;
                            break;
                        case 11:
                            objQMS060.Welder11 = item;
                            break;
                        case 12:
                            objQMS060.Welder12 = item;
                            break;
                        case 13:
                            objQMS060.Welder13 = item;
                            break;
                        case 14:
                            objQMS060.Welder14 = item;
                            break;
                        case 15:
                            objQMS060.Welder15 = item;
                            break;
                        case 16:
                            objQMS060.Welder16 = item;
                            break;
                        case 17:
                            objQMS060.Welder17 = item;
                            break;
                        case 18:
                            objQMS060.Welder18 = item;
                            break;
                        case 19:
                            objQMS060.Welder19 = item;
                            break;
                        case 20:
                            objQMS060.Welder20 = item;
                            break;
                        case 21:
                            objQMS060.Welder21 = item;
                            break;
                        case 22:
                            objQMS060.Welder22 = item;
                            break;
                        case 23:
                            objQMS060.Welder23 = item;
                            break;
                        case 24:
                            objQMS060.Welder24 = item;
                            break;
                        default:
                            break;
                    }
                    _Count++;
                    #endregion
                }
                objQMS060.WeldingProcess = objWelderDetails.WeldingProcess;
            }
            #endregion


            return View(objQMS060);
        }

        public clsHelper.Welders GetWelderStamps(int headerId, bool isLTFPS)
        {
            clsHelper.Welders objResponseMsg = new clsHelper.Welders();
            List<QMS061> lstQMS061 = new List<QMS061>();

            (new MaintainSeamListOfferInspectionController()).CopyWelderWeldLengthFromPreviousNDEStage(headerId, isLTFPS);

            bool isAlreadyMaintained = false;

            if (isLTFPS)
            {
                lstQMS061 = db.QMS061.Where(c => c.LTFPSHeaderId == headerId).ToList();
            }
            else
            {
                lstQMS061 = db.QMS061.Where(c => c.HeaderId == headerId).ToList();
            }
            if (lstQMS061.Count > 0)
            {
                isAlreadyMaintained = true;
            }

            List<string> _weldingProcess = new List<string>();
            List<string> lstWelders = new List<string>();

            if (!isAlreadyMaintained)
            {
                #region Get Welders from Parameter slip

                List<WPS025> lstWPS025 = new List<WPS025>();
                if (isLTFPS)
                {
                    LTF002 objLTF002 = db.LTF002.FirstOrDefault(x => x.LineId == headerId);
                    lstWPS025 = db.WPS025.Where(x => x.BU == objLTF002.BU && x.Location == objLTF002.Location && x.QualityProject == objLTF002.QualityProject && x.Project == objLTF002.Project && x.SeamNo == objLTF002.SeamNo).OrderBy(o => o.PrintedOn).ToList();
                }
                else
                {
                    QMS040 objQMS040 = db.QMS040.FirstOrDefault(x => x.HeaderId == headerId);
                    //lstWPS025 = db.WPS025.Where(x => x.BU == objQMS040.BU && x.Location == objQMS040.Location && x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project && x.SeamNo == objQMS040.SeamNo).OrderByDescending(o => o.PrintedOn).ToList();
                    lstWPS025 = db.WPS025.Where(x => x.BU == objQMS040.BU && x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project && x.SeamNo == objQMS040.SeamNo).OrderBy(o => o.PrintedOn).ToList();
                }
                foreach (var wps25 in lstWPS025)
                {
                    if (!lstWelders.Contains(wps25.WelderStamp))
                    {
                        lstWelders.Add(wps25.WelderStamp);
                        _weldingProcess.Add(wps25.WeldingProcess);
                    }
                }
                #endregion
            }
            else
            {
                #region Get Welders from maintain welder weld length

                foreach (QMS061 objQMS061 in lstQMS061)
                {
                    if (!lstWelders.Contains(objQMS061.welderstamp))
                    {
                        lstWelders.Add(objQMS061.welderstamp);
                        _weldingProcess.AddRange(objQMS061.WeldingProcess.Split('+'));
                    }
                }
                #endregion
            }

            objResponseMsg.WelderStamps = lstWelders;
            objResponseMsg.WeldingProcess = string.Join("+", _weldingProcess.Distinct());
            objResponseMsg.Key = true;
            objResponseMsg.Value = "Welders Captured Successfully";

            return objResponseMsg;
        }

        public CategoryData GetCategoryDescription(string category, string categoryCode, string BU, string Location)
        {
            CategoryData objGLB002 = new CategoryData();

            objGLB002 = (from glb002 in db.GLB002
                         join glb001 in db.GLB001 on glb002.Category equals glb001.Id
                         where glb001.Category.Equals(category, StringComparison.OrdinalIgnoreCase) && glb002.Code.Equals(categoryCode) && glb002.IsActive == true && BU.Equals(glb002.BU) && Location.Equals(glb002.Location)
                         select new CategoryData { Code = glb002.Code, CategoryDescription = glb002.Description }).FirstOrDefault();
            if (objGLB002 == null)
            {
                objGLB002 = new CategoryData();
                objGLB002.CategoryDescription = categoryCode;
            }

            return objGLB002;
        }
        public string GetCategoryDescriptionFromList(List<GLB002> lst, string code)
        {
            if (!string.IsNullOrWhiteSpace(code))
                return lst.Where(x => x.Code == code).Select(s => s.Code + " - " + s.Description).FirstOrDefault();
            else
                return null;
        }

        [HttpPost]
        public ActionResult QCReturnRequest(int requestId, string ReturnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                //if (!IsApplicableForNDERequestGenerate(requestId, clsImplementationEnum.InspectionFor.SEAM.GetStringValue()))
                //{
                //    objResponseMsg.Key = false;
                //    objResponseMsg.Value = "This request has been already attended";
                //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                //}

                QMS060 objQMS060 = db.QMS060.Where(x => x.RequestId == requestId).FirstOrDefault();
                bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
                string parallelRequestId = requestId.ToString();

                if (isLTFPSProject)
                {
                    parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_LTFPS_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
                }
                else
                {
                    parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_IPI_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
                }

                List<int> parallelId = new List<int>();

                if (objQMS060.IterationNo > 1)
                {
                    parallelId.Add(requestId);
                }
                else
                {
                    parallelId = parallelRequestId.Split(',').Select(Int32.Parse).ToList();
                }

                foreach (var reqId in parallelId)
                {
                    QMS060 objDestQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == reqId);
                    objDestQMS060.QCIsReturned = true;
                    objDestQMS060.QCReturnedRemarks = ReturnRemarks;
                    objDestQMS060.QCReturnedBy = objClsLoginInfo.UserName;
                    objDestQMS060.QCReturnedOn = DateTime.Now;
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Request has been returned successfully"; ;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult NDEReturnRequest(int requestId, string ReturnRemarks)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS060 objQMS060 = db.QMS060.Where(x => x.RequestId == requestId).FirstOrDefault();

                if (!IsApplicableForReturnRequest(objQMS060))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Request is not applicable for Return.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
                string parallelRequestId = requestId.ToString();

                //if (isLTFPSProject)
                //{
                //    parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_LTFPS_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
                //}
                //else
                //{
                //    parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_IPI_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
                //}

                List<int> parallelId = parallelRequestId.Split(',').Select(Int32.Parse).ToList();

                foreach (var reqId in parallelId)
                {
                    QMS060 objDestQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == reqId);
                    objDestQMS060.NDEIsReturned = true;
                    objDestQMS060.NDEReturnedRemarks = ReturnRemarks;
                    objDestQMS060.NDEReturnedBy = objClsLoginInfo.UserName;
                    objDestQMS060.NDEReturnedOn = DateTime.Now;
                    if (isLTFPSProject)
                    {
                        objDestQMS060.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.UnderInspection.GetStringValue();
                    }
                    else
                    {
                        objDestQMS060.QMS040.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.UnderInspection.GetStringValue();
                    }
                }
                db.SaveChanges();

                #region Maintain Return Request History
                foreach (var reqId in parallelId)
                {
                    QMS062 objQMS062 = db.QMS062.Where(c => c.RequestId == reqId && c.ReturnedBy == null).OrderByDescending(d => d.Id).FirstOrDefault();
                    if (objQMS062 != null)
                    {
                        objQMS062.ReturnedBy = objClsLoginInfo.UserName;
                        objQMS062.ReturnedOn = DateTime.Now;
                        objQMS062.ReturnedRemarks = ReturnRemarks;
                        db.SaveChanges();
                    }
                    else
                    {
                        QMS060 objQMS060New = db.QMS060.FirstOrDefault(x => x.RequestId == reqId);
                        objQMS062 = new QMS062();
                        objQMS062.RequestId = objQMS060New.RequestId;
                        objQMS062.OfferedBy = objQMS060New.OfferedBy;
                        objQMS062.OfferedOn = objQMS060New.OfferedOn;
                        objQMS062.OfferedRemarks = objQMS060New.ShopRemark;
                        objQMS062.ReturnedBy = objClsLoginInfo.UserName;
                        objQMS062.ReturnedOn = DateTime.Now;
                        objQMS062.ReturnedRemarks = ReturnRemarks;
                        objQMS062.CreatedBy = objClsLoginInfo.UserName;
                        objQMS062.CreatedOn = DateTime.Now;
                        db.QMS062.Add(objQMS062);
                        db.SaveChanges();
                    }
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Request has been returned successfully";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool IsApplicableForReturnRequest(QMS060 objQMS060)
        {
            bool returnValue = true;
            try
            {
                ///string parallelRequestId = objQMS060.RequestId.ToString();
                bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
                //if (isLTFPSProject)
                //{
                //    parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_LTFPS_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
                //}
                //else
                //{
                //    parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_IPI_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
                //}

                //List<int> parallelId = parallelRequestId.Split(',').Select(Int32.Parse).ToList();
                //foreach (var reqId in parallelId)
                //{
                //QMS060 objDestQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == reqId);
                if (!string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) || (isLTFPSProject ? objQMS060.LTF002.InspectionStatus != clsImplementationEnum.SeamListInspectionStatus.OfferedToNDE.GetStringValue() : objQMS060.QMS040.InspectionStatus != clsImplementationEnum.SeamListInspectionStatus.OfferedToNDE.GetStringValue()) || objQMS060.IterationNo > 2)
                {
                    returnValue = false;
                    //break;
                }
                //}
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                returnValue = false;
            }
            return returnValue;
        }

        [HttpPost]
        public ActionResult RespondeGenerateRequest(int requestId, string AutoUserPSNo = "", string APIRequestFrom = "", string UserName = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (!IsApplicableForNDERequestGenerate(requestId, clsImplementationEnum.InspectionFor.SEAM.GetStringValue()))
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "This request has been already attended";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

            if (string.IsNullOrWhiteSpace(AutoUserPSNo))
                AutoUserPSNo = (APIRequestFrom == "Mobile" ? UserName : objClsLoginInfo.UserName);

            string response = Convert.ToString(db.SP_IPI_UPDATE_REQUESTNO_NDE_SEAM(requestId, AutoUserPSNo).FirstOrDefault());
            db.SaveChanges();

            QMS060 objQMS060 = db.QMS060.Where(x => x.RequestId == requestId).FirstOrDefault();
            bool isConfirmationRequired = Manager.IsNDERequestConfirmationRequiredOnFirstInteration(objQMS060.Project, objQMS060.BU, objQMS060.Location);
            bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);

            if (!isConfirmationRequired)
            {
                #region auto confirm request by iEMQS system user

                string parallelRequestId = requestId.ToString();
                if (isLTFPSProject)
                {
                    parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_LTFPS_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
                }
                else
                {
                    parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_IPI_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
                }

                List<int> parallelId = parallelRequestId.Split(',').Select(Int32.Parse).ToList();

                //Add Welders in Single/Parallel Request                
                List<QMS061> lstQMS061 = db.QMS061.Where(x => x.RefRequestId == requestId).ToList();

                foreach (var reqId in parallelId)
                {
                    QMS060 objDestQMS060 = new QMS060();
                    objDestQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == reqId);

                    List<QMS061> WelderList = new List<QMS061>();
                    if (isLTFPSProject)
                    {
                        WelderList = objDestQMS060.LTF002.QMS061.ToList();
                    }
                    else
                    {
                        WelderList = objDestQMS060.QMS040.QMS061.ToList();
                    }

                    int _Count = 1;
                    List<string> _weldingProcess = new List<string>();

                    foreach (QMS061 welder in WelderList)
                    {
                        welder.RequestNo = objDestQMS060.RequestNo;
                        welder.IterationNo = objDestQMS060.IterationNo;
                        welder.RequestNoSequence = objDestQMS060.RequestNoSequence;
                        welder.RefRequestId = objDestQMS060.RequestId;

                        #region maintain welder weld length
                        switch (_Count)
                        {
                            case 1:
                                objDestQMS060.Welder1 = welder.welderstamp;
                                break;
                            case 2:
                                objDestQMS060.Welder2 = welder.welderstamp;
                                break;
                            case 3:
                                objDestQMS060.Welder3 = welder.welderstamp;
                                break;
                            case 4:
                                objDestQMS060.Welder4 = welder.welderstamp;
                                break;
                            case 5:
                                objDestQMS060.Welder5 = welder.welderstamp;
                                break;
                            case 6:
                                objDestQMS060.Welder6 = welder.welderstamp;
                                break;
                            case 7:
                                objDestQMS060.Welder7 = welder.welderstamp;
                                break;
                            case 8:
                                objDestQMS060.Welder8 = welder.welderstamp;
                                break;
                            case 9:
                                objDestQMS060.Welder9 = welder.welderstamp;
                                break;
                            case 10:
                                objDestQMS060.Welder10 = welder.welderstamp;
                                break;
                            case 11:
                                objDestQMS060.Welder11 = welder.welderstamp;
                                break;
                            case 12:
                                objDestQMS060.Welder12 = welder.welderstamp;
                                break;
                            case 13:
                                objDestQMS060.Welder13 = welder.welderstamp;
                                break;
                            case 14:
                                objDestQMS060.Welder14 = welder.welderstamp;
                                break;
                            case 15:
                                objDestQMS060.Welder15 = welder.welderstamp;
                                break;
                            case 16:
                                objDestQMS060.Welder16 = welder.welderstamp;
                                break;
                            case 17:
                                objDestQMS060.Welder17 = welder.welderstamp;
                                break;
                            case 18:
                                objDestQMS060.Welder18 = welder.welderstamp;
                                break;
                            case 19:
                                objDestQMS060.Welder19 = welder.welderstamp;
                                break;
                            case 20:
                                objDestQMS060.Welder20 = welder.welderstamp;
                                break;
                            case 21:
                                objDestQMS060.Welder21 = welder.welderstamp;
                                break;
                            case 22:
                                objDestQMS060.Welder22 = welder.welderstamp;
                                break;
                            case 23:
                                objDestQMS060.Welder23 = welder.welderstamp;
                                break;
                            case 24:
                                objDestQMS060.Welder24 = welder.welderstamp;
                                break;
                            default:
                                break;
                        }

                        foreach (var wp in welder.WeldingProcess.Split('+'))
                        {
                            if (!string.IsNullOrWhiteSpace(wp))
                            {
                                _weldingProcess.Add(wp);
                            }
                        }

                        _Count++;
                        #endregion
                    }

                    if (_weldingProcess.Count > 0)
                    {
                        objDestQMS060.WeldingProcess = string.Join("+", _weldingProcess.Distinct());
                    }

                    objDestQMS060.ConfirmedBy = Manager.GetSystemUserPSNo();
                    objDestQMS060.ConfirmedOn = DateTime.Now;
                    if (isLTFPSProject)
                    {
                        objDestQMS060.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.OfferedToNDE.GetStringValue();
                    }
                    else
                    {
                        objDestQMS060.QMS040.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.OfferedToNDE.GetStringValue();
                    }

                    if (string.IsNullOrWhiteSpace(objDestQMS060.Part6thickness) && string.IsNullOrWhiteSpace(objDestQMS060.Part6Material) && objDestQMS060.IsOverlay.Value)
                    {
                        var QMS060Overlay = db.QMS060.Where(x => x.QualityProject == objDestQMS060.QualityProject && x.SeamNo == objDestQMS060.SeamNo && x.Part6thickness != null && x.Part6Material != null).OrderByDescending(o => o.RequestId).FirstOrDefault();
                        if (QMS060Overlay != null)
                        {
                            objDestQMS060.Part6thickness = QMS060Overlay.Part6thickness;
                            objDestQMS060.Part6Material = QMS060Overlay.Part6Material;
                        }
                    }

                    db.SaveChanges();

                    #region Send Notification
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objQMS060.Project, objQMS060.BU, objQMS060.Location, "NDT Request " + objQMS060.RequestNo + " for Stage: " + objQMS060.StageCode + " of Seam: " + objQMS060.SeamNo + " & Project No: " + objQMS060.QualityProject + " has been confirmed and ready for your further action", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/SeamListNDEInspection/NDESeamTestDetails/" + objQMS060.RequestId, null, APIRequestFrom, UserName);
                    #endregion

                }


                #endregion
            }
            else
            {
                #region Send Notification
                if (objQMS060 != null)
                {
                    (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS060.Project, objQMS060.BU, objQMS060.Location, "NDT Request " + objQMS060.RequestNo + " for Stage: " + objQMS060.StageCode + " of Seam: " + objQMS060.SeamNo + " & Project No: " + objQMS060.QualityProject + " has been submitted for your Confirmation", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/SeamListNDEInspection/ShopSeamTestDetails/" + objQMS060.RequestId, null, APIRequestFrom, UserName);
                }
                #endregion
            }

            objResponseMsg.Key = true;
            objResponseMsg.Value = response;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetReturnHistoryPopup(string RequestId)
        {
            ViewBag.ReqId = RequestId;
            return PartialView("~/Areas/IPI/Views/SeamListNDEInspection/_GetReturnHistoryGridDataPartial.cshtml");
        }
        [HttpPost]
        public JsonResult LoadReturnHistoryDataGrid(JQueryDataTableParamModel param, int ReqId)
        {
            try
            {
                var lstResult = db.QMS062.Where(c => c.RequestId == ReqId).OrderBy(o => o.Id).ToList();
                var data = (from uc in lstResult
                            select new[]
                           {
                            string.IsNullOrWhiteSpace(uc.OfferedBy) ? "" :Manager.GetPsidandDescription(uc.OfferedBy).Split('-')[0]+"<br>"+Manager.GetPsidandDescription(uc.OfferedBy).Split('-')[1],
                            (uc.OfferedOn.HasValue ? (uc.OfferedOn.Value.ToString("dd/MM/yyyy")+"<br>"+uc.OfferedOn.Value.ToString("hh:mm tt") ): ""),
                            Convert.ToString(uc.OfferedRemarks),
                            string.IsNullOrWhiteSpace(uc.ReturnedBy) ? "" : Manager.GetPsidandDescription(uc.ReturnedBy).Split('-')[0]+"<br>"+Manager.GetPsidandDescription(uc.ReturnedBy).Split('-')[1],
                            (uc.ReturnedOn.HasValue ? (uc.ReturnedOn.Value.ToString("dd/MM/yyyy")+"<br>"+uc.ReturnedOn.Value.ToString("hh:mm tt") ): ""),
                            Convert.ToString(uc.ReturnedRemarks)
                          }).ToList();
                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = lstResult.Count,
                    iTotalDisplayRecords = lstResult.Count,
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool IsApplicableForNDERequestGenerate(int RequestId, string InspectionFor)
        {
            bool isApplicable = false;

            if (InspectionFor == clsImplementationEnum.InspectionFor.SEAM.GetStringValue())
            {
                QMS060 objQMS060 = db.QMS060.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if ((!objQMS060.QCIsReturned.HasValue || objQMS060.QCIsReturned.Value == false) && string.IsNullOrWhiteSpace(Convert.ToString(objQMS060.RequestGeneratedBy)) && objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue())
                {
                    isApplicable = true;
                }
            }
            else
            {
                QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if ((!objQMS065.QCIsReturned.HasValue || objQMS065.QCIsReturned.Value == false) && string.IsNullOrWhiteSpace(Convert.ToString(objQMS065.RequestGeneratedBy)) && objQMS065.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue())
                {
                    isApplicable = true;
                }
            }

            return isApplicable;
        }

        public bool IsApplicableForNDERequestConfirmation(int RequestId, string InspectionFor)
        {
            bool isApplicable = false;

            if (InspectionFor == clsImplementationEnum.InspectionFor.SEAM.GetStringValue())
            {
                QMS060 objQMS060 = db.QMS060.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS060.RequestGeneratedBy)) && string.IsNullOrWhiteSpace(Convert.ToString(objQMS060.ConfirmedBy)) && objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue())
                {
                    isApplicable = true;
                }
            }
            else
            {
                QMS065 objQMS065 = db.QMS065.Where(x => x.RequestId == RequestId).FirstOrDefault();
                if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS065.RequestGeneratedBy)) && string.IsNullOrWhiteSpace(Convert.ToString(objQMS065.ConfirmedBy)) && objQMS065.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue())
                {
                    isApplicable = true;
                }
            }

            return isApplicable;
        }

        public bool IsApplicableForNDEPTMTConfirmation(int RequestId)
        {
            bool isApplicable = false;

            QMS060 objQMS060 = db.QMS060.Where(x => x.RequestId == RequestId).FirstOrDefault();
            if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS060.RequestGeneratedBy)) && string.IsNullOrWhiteSpace(Convert.ToString(objQMS060.ConfirmedBy)) && objQMS060.IsPTMTCellRequired == true && objQMS060.QCIsReturned == null && objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue())
            {
                isApplicable = true;
            }

            return isApplicable;
        }

        public bool IsApplicableForNDERegenerateRequest(int RequestId)
        {
            bool isApplicable = false;

            QMS060 objQMS060 = db.QMS060.Where(x => x.RequestId == RequestId).FirstOrDefault();
            if (objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() && objQMS060.IsPartiallyOffered != true)
            {
                isApplicable = true;
            }

            return isApplicable;
        }



        #endregion

        #region Shop
        [SessionExpireFilter]
        public ActionResult ShopIndexView(string tab)
        {
            ViewBag.Tab = tab;
            return View();
        }

        public ActionResult ShowTimelineNDEInspectionPartial(int requestId)
        {
            TimelineViewModel model = new TimelineViewModel();
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
            model.OfferedBy = Manager.GetPsidandDescription(objQMS060.OfferedBy);
            model.OfferedOn = objQMS060.OfferedOn;
            model.RequestGeneratedBy = Manager.GetPsidandDescription(objQMS060.RequestGeneratedBy);
            model.RequestGeneratedOn = objQMS060.RequestGeneratedOn;
            model.ConfirmedBy = Manager.GetPsidandDescription(objQMS060.ConfirmedBy);
            model.ConfirmedOn = objQMS060.ConfirmedOn;
            return PartialView("_ShowTimelineNDEInspectionPartial", model);
        }

        public ActionResult GetShopSeamTestDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetShopSeamTestDataPartial");
        }
        public ActionResult loadShopSeamTestDataTable(JQueryDataTableParamModel param, string status, string qualityProject, string seamNumber)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                string _urlFrom = "all";
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and ( RequestNo IS NOT NULL and RequestGeneratedBy IS NOT NULL and ConfirmedBy IS NULL and RequestStatus='Not Attended' ) ";
                    _urlFrom = "confirmation";
                }
                else if (status.ToUpper() == "REGENERATE")
                {
                    whereCondition += " and ( (dbo.fun_IPI_SPOTCLEAR_BY_REQUEST_ID(qms.HeaderId) = 0 and ISNULL(IsPartiallyOffered, 0) = 0 and ISNULL(IsPTMTCellRequired, 0) = 0 and RequestStatus = '" + clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() + "') OR (RequestNo IS NOT NULL and RequestGeneratedBy IS NOT NULL and ConfirmedBy IS NULL and ISNULL(IsPTMTCellRequired, 0) = 0 and RequestStatus='" + clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue() + "')) and Isnull(TPIResultPending,0) <> 1  ";
                    _urlFrom = "regenerate";
                }
                else if (status.ToUpper() == "RETURNNDEREQUEST")
                {
                    whereCondition += " and ( (QCIsReturned=1 or NDEIsReturned=1) and RequestStatus = '" + clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue() + "' ) ";
                    _urlFrom = "returnnderequest";
                }
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project", "qms.StageCode", "qms.RequestStatus", "qms.RequestNo", "qms.ManufacturingCode", "qms.AcceptanceStandard", "qms.ApplicableSpecification", "qms.InspectionExtent", "qms.WeldingEngineeringRemarks", "qms.WeldingProcess", "qms.SeamStatus", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark", "qms.SurfaceCondition", "qms.WeldThicknessReinforcement", "qms.SpotGenerationtype", "qms.PrintSummary", "qms.NDETechniqueNo", "qms.NDETechniqueRevisionNo" };

                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                if (!string.IsNullOrEmpty(qualityProject) && !string.IsNullOrEmpty(seamNumber))
                {
                    whereCondition += (seamNumber.Equals("ALL") ? " AND qms.QualityProject = '" + qualityProject + "' " : " AND qms.QualityProject = '" + qualityProject + "' AND qms.SeamNo ='" + seamNumber + "'");
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty; ;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstHeader = db.SP_IPI_SEAMLISTNDETEST_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           Convert.ToString( h.SeamNo),
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString(h.RequestNo),
                           Convert.ToString(h.RequestNoSequence),
                           Convert.ToString(h.RequestStatus),
                           Convert.ToString(h.SeamLength),
                           Convert.ToString(h.OverlayArea),
                           Convert.ToString(h.OverlayThickness),
                           Convert.ToString(h.Jointtype),
                           Convert.ToString(h.ManufacturingCode),
                           Convert.ToString(h.Part1Position),
                           Convert.ToString(h.Part1thickness),
                           Convert.ToString(h.Part1Material),
                           Convert.ToString(h.Part2Position),
                           Convert.ToString(h.Part2thickness),
                           Convert.ToString(h.Part2Material),
                           Convert.ToString(h.Part3Position),
                           Convert.ToString(h.Part3thickness),
                           Convert.ToString(h.Part3Material),
                           Convert.ToString(h.AcceptanceStandard),
                           Convert.ToString(h.ApplicableSpecification),
                           Convert.ToString(h.InspectionExtent),
                           Convert.ToString(h.WeldingEngineeringRemarks),
                           Convert.ToString(h.WeldingProcess),
                           Convert.ToString(h.SeamStatus),
                           Convert.ToString(h.Shop),
                           Convert.ToString(h.ShopLocation),
                           Convert.ToString(h.ShopRemark),
                           Convert.ToString(h.SurfaceCondition),
                           Convert.ToString(h.WeldThicknessReinforcement),
                           Convert.ToString(h.SpotGenerationtype),
                           Convert.ToString(h.SpotLength),
                           Convert.ToString(h.PrintSummary),
                           Convert.ToString( h.CreatedBy),
                           Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.RequestGeneratedBy),
                           Convert.ToDateTime(h.RequestGeneratedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.ConfirmedBy),
                           Convert.ToDateTime(h.ConfirmedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.NDETechniqueNo),
                           Convert.ToString( h.NDETechniqueRevisionNo),
                           h.QualityProject,
                           Convert.ToString(h.RequestId),
                           Convert.ToString(h.HeaderId),
                           Convert.ToString( h.Project),
                           (status.ToUpper() == "RETURNNDEREQUEST" ? "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a   href=\"" + WebsiteURL + "/IPI/MaintainSeamListOfferInspection/ReturnRequestDetails/"+h.RequestId+"/?from="+ _urlFrom  +"\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a></center>" : "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a   href=\"" + WebsiteURL + "/IPI/SeamListNDEInspection/ShopSeamTestDetails/"+h.RequestId+"/?from="+ _urlFrom  +"\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>&nbsp;"+Manager.generateIPIGridButtons(h.QualityProject,h.Project,h.BU,h.Location,h.SeamNo,"","",h.StageCode,Convert.ToString(h.StageSequence),"Seam Details",60,"true","Seam Details","Seam Request Details")+"&nbsp;&nbsp;<a  href=\"javascript: void(0);\" Title=\"View Timeline\" onclick=\"ShowTimeline("+ h.RequestId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a></center>" )


                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        [SessionExpireFilter]
        public ActionResult ShopSeamTestDetails(int id, string from)
        {
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == id);

            bool isLTFPSproj = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
            ViewBag.IsLTFPS = isLTFPSproj;

            ViewBag.SurfaceCondition = GetCategoryDescription("Surface Condition", objQMS060.SurfaceCondition, objQMS060.BU, objQMS060.Location).CategoryDescription;
            bool isSpotEditable = true;
            if (objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo))
            {
                isSpotEditable = false;
            }
            ViewBag.isSpotEditable = isSpotEditable;
            objQMS060.Project = Manager.GetProjectAndDescription(objQMS060.Project);
            objQMS060.StageCode = db.Database.SqlQuery<string>("SELECT dbo.GET_IPI_STAGECODE_DESCRIPTION('" + objQMS060.StageCode + "','" + objQMS060.BU + "','" + objQMS060.Location + "')").FirstOrDefault();
            objQMS060.SeamNo = db.QMS012.Where(x => x.QualityProject == objQMS060.QualityProject && x.SeamNo == objQMS060.SeamNo).Select(x => x.SeamNo + "-" + x.SeamDescription).FirstOrDefault();

            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", objQMS060.BU, objQMS060.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", objQMS060.BU, objQMS060.Location, null);
            objQMS060.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency1);
            objQMS060.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency2);
            objQMS060.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency3);
            objQMS060.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency4);
            objQMS060.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency5);
            objQMS060.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency6);
            objQMS060.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency1Intervention);
            objQMS060.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency2Intervention);
            objQMS060.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency3Intervention);
            objQMS060.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency4Intervention);
            objQMS060.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency5Intervention);
            objQMS060.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency6Intervention);
            objQMS060.Location = db.COM002.Where(x => x.t_dimx == objQMS060.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS060.BU = db.COM002.Where(x => x.t_dimx == objQMS060.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS060.CreatedBy = Manager.GetPsidandDescription(objQMS060.CreatedBy);
            objQMS060.RequestGeneratedBy = Manager.GetPsidandDescription(objQMS060.RequestGeneratedBy);
            objQMS060.OfferedBy = Manager.GetPsidandDescription(objQMS060.OfferedBy);
            objQMS060.ConfirmedBy = Manager.GetPsidandDescription(objQMS060.ConfirmedBy);
            ViewBag.fromURL = from;

            if (objQMS060.SeamStatus == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue())
            {
                objQMS060.SeamStatus = "Yes";
            }
            else
            {
                if (objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue())
                {
                    objQMS060.SeamStatus = "No";
                }
                else
                {
                    objQMS060.SeamStatus = "";
                }
            }

            if (isLTFPSproj)
            {
                ViewBag.isSPOTCLEAR_BY_REQUESTNO = db.Database.SqlQuery<bool>("SELECT dbo.fun_LTFPS_SPOTCLEAR_BY_REQUESTNO(" + objQMS060.LTFPSHeaderId + ")").FirstOrDefault();
                ViewBag.UnclearSeam = GetUnclearedSpotCount(objQMS060.LTFPSHeaderId, true);
            }
            else
            {
                ViewBag.isSPOTCLEAR_BY_REQUESTNO = false;
                ViewBag.UnclearSeam = GetUnclearedSpotCount(objQMS060.HeaderId);
            }


            string strRejected = clsImplementationEnum.SpotResults.Rejected.GetStringValue();
            List<QMS070> lstRequestSpots = db.QMS070.Where(x => x.RequestId == objQMS060.RequestId).ToList();
            ViewBag.IsWelderRequired = "No";
            foreach (var itemSpot in lstRequestSpots)
            {
                QMS070 maxSpotResult = null;
                if (isLTFPSproj)
                {
                    maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.StageCode == itemSpot.StageCode && x.SpotResult != null && x.RequestNo == itemSpot.RequestNo).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                }
                else
                {
                    maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.SpotResult != null && x.HeaderId == itemSpot.HeaderId).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                }

                if (maxSpotResult != null)
                {
                    if (maxSpotResult.SpotResult == strRejected || maxSpotResult.IsRejected == true)
                    {
                        ViewBag.IsWelderRequired = "Yes";
                        break;
                    }
                }
            }



            return View(objQMS060);
        }

        [HttpPost]
        public ActionResult GetWelderWiseWeldTypePartial(int requestId, bool flag = false)
        {
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
            ViewBag.requestId = requestId;
            ViewBag.IsOverlay = objQMS060.IsOverlay;
            if (objQMS060.IsOverlay != null && objQMS060.IsOverlay.Value)
            {
                ViewBag.MaxWeldLength = Convert.ToInt32(objQMS060.OverlayArea);
            }
            else
            {
                ViewBag.MaxWeldLength = Convert.ToInt32(objQMS060.SeamLength);
            }
            ViewBag.EnableFlag = flag;
            ViewBag.headerId = 0;
            ViewBag.ltfpsHeaderId = 0;
            ViewBag.IsLTFPS = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
            return PartialView("_GetWelderWiseWeldTypePartial");
        }

        [HttpPost]
        public ActionResult GetWelderWiseWeldTypePartialBeforeOffer(int headerId, bool flag = false)
        {
            QMS040 objQMS040 = db.QMS040.FirstOrDefault(x => x.HeaderId == headerId);
            ViewBag.headerId = headerId;
            ViewBag.requestId = 0;
            ViewBag.ltfpsHeaderId = 0;
            ViewBag.IsOverlay = Manager.IsOverlaySeam(objQMS040.QualityProject, objQMS040.SeamNo);
            ViewBag.IsLTFPS = false;
            ViewBag.EnableFlag = flag;
            var objQMS012 = db.QMS012.FirstOrDefault(c => c.QualityProject == objQMS040.QualityProject && c.SeamNo == objQMS040.SeamNo);
            ViewBag.MaxWeldLength = (objQMS012.SeamLengthArea.HasValue ? objQMS012.SeamLengthArea.Value : 0);

            return PartialView("_GetWelderWiseWeldTypePartial");
        }

        [HttpPost]
        public ActionResult GetWelderWiseWeldTypePartialForLTFPSBeforeOffer(int ltfpsHeaderId, bool flag = false)
        {
            LTF002 objLTF002 = db.LTF002.FirstOrDefault(x => x.LineId == ltfpsHeaderId);
            ViewBag.ltfpsHeaderId = ltfpsHeaderId;
            ViewBag.requestId = 0;
            ViewBag.headerId = 0;
            ViewBag.IsOverlay = Manager.IsOverlaySeam(objLTF002.QualityProject, objLTF002.SeamNo);
            ViewBag.IsLTFPS = true;
            ViewBag.EnableFlag = flag;
            var objQMS012 = db.QMS012.FirstOrDefault(c => c.QualityProject == objLTF002.QualityProject && c.SeamNo == objLTF002.SeamNo);
            ViewBag.MaxWeldLength = (objQMS012.SeamLengthArea.HasValue ? objQMS012.SeamLengthArea.Value : 0);

            return PartialView("_GetWelderWiseWeldTypePartial");
        }

        [HttpPost]
        public ActionResult loadWeldwiseWeldLengthDataTable(JQueryDataTableParamModel param, string id, string type, bool isLTFPS = false, bool IsVisible = false)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                if (type == "beforeOffer")
                {
                    if (isLTFPS)
                    {
                        whereCondition += " and LTFPSHeaderId = " + id;
                    }
                    else
                    {
                        whereCondition += " and HeaderId = " + id;
                    }
                }
                else
                {
                    whereCondition += " and refRequestId = " + id;
                }

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "welderstamp" };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                var lstHeader = db.SP_IPI_SEAMWELDLENGTH(StartIndex, EndIndex, "", whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           Convert.ToString(h.ROW_NO),
                           Convert.ToString(h.RefRequestId),
                           Convert.ToString(h.welderstamp),
                           IsVisible ?  "<input type='text' id= 'txtWeldlength_"+h.Id+"' spara= '"+h.Id+"' name='txtWeldlength'  class='form-control input-sm input-small input-inline weldtype set-numeric ' value='" + h.Weldlength + "' />":Convert.ToString(h.Weldlength),
                           IsVisible ? "<input type='text' id= 'txtWeldarea_"+h.Id+"' spara= '"+h.Id+"' name='txtWeldarea'  class='form-control input-sm input-small input-inline weldtype set-numeric' value='" + h.Weldarea+ "' />":Convert.ToString(h.Weldarea),
                          Convert.ToString(h.Id),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        public async Task<ActionResult> UpdateWeldingValue(QMS061 qms61)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var objQMS061 = await db.QMS061.FirstOrDefaultAsync(x => x.Id == qms61.Id);
                objQMS061.Weldarea = qms61.Weldarea;
                objQMS061.Weldlength = qms61.Weldlength;
                await db.SaveChangesAsync();
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public decimal TruncateDecimal(decimal value, int precision)
        {
            return Math.Ceiling(value * 20) / 20;
        }
        [HttpPost]
        public async Task<ActionResult> UpdateWeldingValueByAportionAssign(int requestId, bool isOverlay, string updateType, string value)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                decimal decValue;
                QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
                bool isLTFPS = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
                decValue = Decimal.TryParse(value, out decValue) ? decValue : 0;
                if (decValue > 0)
                {
                    if (objQMS060.IterationNo > 1)
                    {
                        int? defectArea = 0;
                        string strRejected = clsImplementationEnum.SpotResults.Rejected.GetStringValue();
                        List<QMS070> lstSpots = db.QMS070.Where(x => x.RequestId == requestId).ToList();
                        foreach (var itemSpot in lstSpots)
                        {
                            //QMS070 maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.SpotResult == strRejected && (isLTFPS ? (x.LTFPSHeaderId == itemSpot.LTFPSHeaderId) : (x.HeaderId == itemSpot.HeaderId))).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                            QMS070 maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.SpotResult == strRejected && x.RequestNo == objQMS060.RequestNo).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                            if (maxSpotResult != null)
                            {
                                if (isOverlay)
                                {
                                    defectArea += maxSpotResult.DefectLength * maxSpotResult.DefectWidth;
                                }
                                else
                                {
                                    defectArea += maxSpotResult.DefectLength;
                                }
                            }
                        }
                        //QMS060 prevIterationQMS060 = db.QMS060.Where(x => x.IterationNo == objQMS060.IterationNo - 1 && x.HeaderId == objQMS060.HeaderId).FirstOrDefault();

                        decValue = Convert.ToDecimal(defectArea);
                    }

                    List<QMS061> lstQMS061 = db.QMS061.Where(x => x.RefRequestId == requestId).ToList();
                    if (updateType == "Aportion")
                    {
                        decValue = decValue / lstQMS061.Count;
                        decValue = TruncateDecimal(decValue, 2);
                    }
                    if (isOverlay)
                    {
                        foreach (var item in lstQMS061)
                        {
                            item.Weldarea = decValue;
                            item.EditedBy = objClsLoginInfo.UserName;
                            item.EditedOn = DateTime.Now;
                        }
                    }
                    else
                    {
                        foreach (var item in lstQMS061)
                        {
                            item.Weldlength = decValue;
                            item.EditedBy = objClsLoginInfo.UserName;
                            item.EditedOn = DateTime.Now;
                        }
                    }
                    await db.SaveChangesAsync();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = Convert.ToString(decValue);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = isOverlay == true ? "Overlay area is not in proper format" : "Seamlength area is not in proper format";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public async Task<ActionResult> UpdateWeldingValueByAportionAssignBeforeOffer(int headerId, bool isOverlay, string updateType, string value, bool isLTFPS = false)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                decimal decValue;
                decValue = Decimal.TryParse(value, out decValue) ? decValue : 0;
                if (decValue > 0)
                {
                    List<QMS061> lstQMS061 = db.QMS061.Where(x => (isLTFPS ? (x.LTFPSHeaderId == headerId) : (x.HeaderId == headerId))).ToList();
                    if (updateType == "Aportion")
                    {
                        decValue = decValue / lstQMS061.Count;
                        decValue = TruncateDecimal(decValue, 2);
                    }
                    if (isOverlay)
                    {
                        foreach (var item in lstQMS061)
                        {
                            item.Weldarea = decValue;
                            item.EditedBy = objClsLoginInfo.UserName;
                            item.EditedOn = DateTime.Now;
                        }
                    }
                    else
                    {
                        foreach (var item in lstQMS061)
                        {
                            item.Weldlength = decValue;
                            item.EditedBy = objClsLoginInfo.UserName;
                            item.EditedOn = DateTime.Now;
                        }
                    }
                    await db.SaveChangesAsync();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = Convert.ToString(decValue);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = isOverlay == true ? "Overlay area is not in proper format" : "Seamlength area is not in proper format";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ConfirmValidation(int requestId, bool isOverlay, string updateType, string value)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
                string message = string.Empty;
                decimal decValue;
                decValue = Decimal.TryParse(value, out decValue) ? decValue : 0;
                if (decValue > 0)
                {
                    List<QMS061> lstQMS061 = db.QMS061.Where(x => x.RefRequestId == requestId).ToList();
                    decimal? sum = 0;
                    if (isOverlay)
                    {
                        if (objQMS060.IterationNo == 1)
                        {
                            sum = lstQMS061.Sum(x => x.Weldarea);
                            message = (sum <= 0 ? "Total weld area should be greater than 0." : "");
                            //message = sum == decValue ? "Total weld Area is equal to seam overlay area" : sum > decValue ? "Total Weld Area is greater than seam overlay area" : "Total weld area is lesser than seam overlay area";
                        }
                        else
                        {
                            string strRejected = clsImplementationEnum.SpotResults.Rejected.GetStringValue();
                            List<QMS070> lstSpots = db.QMS070.Where(x => x.RequestId == requestId).ToList();
                            int? defectArea = 0;
                            foreach (var itemSpot in lstSpots)
                            {
                                QMS070 maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.SpotResult == strRejected && x.RequestNo == itemSpot.RequestNo && x.StageCode == itemSpot.StageCode).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                                if (maxSpotResult != null)
                                {
                                    defectArea += maxSpotResult.DefectLength * maxSpotResult.DefectWidth;
                                }
                            }
                            sum = lstQMS061.Sum(x => x.Weldarea);
                            if (defectArea > 0 && sum <= 0)
                            {
                                message = "Total weld area should be greater than 0.";
                            }
                            //message = sum == Convert.ToDecimal(defectArea) ? "Total weld Area is equal to seam defect overlay area" : sum > Convert.ToDecimal(defectArea) ? "Total Weld Area is greater than seam defect overlay area" : "Total weld area is lesser than seam defect overlay area";
                        }
                    }
                    else
                    {
                        if (objQMS060.IterationNo == 1)
                        {
                            sum = lstQMS061.Sum(x => x.Weldlength);
                            message = (sum <= 0 ? "Total weld length should be greater than 0." : "");
                            //message = sum == decValue ? "Total weld length is equal to seam length" : sum > decValue ? "Total weld length is greater than seam length" : "Total weld length is lesser than seam length";
                        }
                        else
                        {
                            string strRejected = clsImplementationEnum.SpotResults.Rejected.GetStringValue();
                            List<QMS070> lstSpots = db.QMS070.Where(x => x.RequestId == requestId).ToList();
                            int? defectArea = 0;
                            foreach (var itemSpot in lstSpots)
                            {
                                QMS070 maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.SpotResult == strRejected && x.HeaderId == itemSpot.HeaderId).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                                if (maxSpotResult != null)
                                {
                                    defectArea += maxSpotResult.DefectLength;
                                }
                            }
                            sum = lstQMS061.Sum(x => x.Weldlength);
                            if (defectArea > 0 && sum <= 0)
                            {
                                message = "Total weld length should be greater than 0.";
                            }
                            //message = sum == Convert.ToDecimal(defectArea) ? "Total weld length is equal to defect seam length" : sum > Convert.ToDecimal(defectArea) ? "Total weld length is greater than defect seam length" : "Total weld length is lesser than defect seam length";
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = message;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = isOverlay == true ? "Overlay area is not in proper format" : "Seamlength area is not in proper format";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ConfirmValidationBeforeOffer(int headerId, bool isOverlay, string updateType, string value, bool isLTFPS = false)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string BU = string.Empty;
                string Location = string.Empty;
                string StageCode = string.Empty;

                if (isLTFPS)
                {
                    var obj = db.LTF002.FirstOrDefault(c => c.LineId == headerId);
                    if (obj != null)
                    {
                        BU = obj.BU;
                        Location = obj.Location;
                        StageCode = obj.Stage;
                    }
                }
                else
                {
                    var obj = db.QMS040.FirstOrDefault(c => c.HeaderId == headerId);
                    if (obj != null)
                    {
                        BU = obj.BU;
                        Location = obj.Location;
                        StageCode = obj.StageCode;
                    }
                }

                if (!Manager.IsWelderRequiredInNDEStageOffer(BU, Location, StageCode))
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Empty;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                string message = string.Empty;
                decimal decValue;
                decValue = Decimal.TryParse(value, out decValue) ? decValue : 0;
                if (decValue > 0)
                {
                    List<QMS061> lstQMS061 = db.QMS061.Where(x => (isLTFPS ? (x.LTFPSHeaderId == headerId) : (x.HeaderId == headerId))).ToList();
                    decimal? sum = 0;
                    if (isOverlay)
                    {
                        sum = lstQMS061.Sum(x => x.Weldarea);
                        if (sum <= 0)
                        {
                            message = "Total weld area should be greater than 0.";
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = message;
                        }
                        else
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = message;
                        }
                    }
                    else
                    {
                        sum = lstQMS061.Sum(x => x.Weldlength);
                        if (sum <= 0)
                        {
                            message = "Total weld length should be greater than 0.";
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = message;
                        }
                        else
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = message;
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = isOverlay == true ? "Overlay area is not in proper format" : "Seam length area is not in proper format";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult RespondeConfirmRequest(int requestId, bool isConfirm, string spotIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string message = string.Empty;
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
            bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
            if (isConfirm)
            {
                if (objQMS060.IterationNo > 1)
                {
                    List<int> arraySpotIds = spotIds.Split(',').Select(int.Parse).ToList();
                    List<QMS070> deleteSpotId = db.QMS070.Where(x => !arraySpotIds.Contains(x.SpotId) && x.RequestId == requestId).ToList();
                    db.QMS070.RemoveRange(deleteSpotId);
                    db.SaveChanges();
                }
                string parallelRequestId = requestId.ToString();
                if (isLTFPSProject)
                {
                    parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_LTFPS_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
                }
                else
                {
                    parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_IPI_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
                }

                List<int> parallelId = parallelRequestId.Split(',').Where(x => x != Convert.ToString(objQMS060.RequestId)).Select(Int32.Parse).ToList();
                //Add Welders in Parallel Request
                List<QMS061> destWelderList = new List<QMS061>();
                if (!db.QMS060.Any(c => parallelId.Contains(c.RequestId) && c.ConfirmedBy != null && c.IterationNo == 1) && objQMS060.IterationNo == 1)
                {
                    List<QMS061> lstQMS061 = db.QMS061.Where(x => x.RefRequestId == requestId).ToList();

                    foreach (var reqId in parallelId)
                    {
                        QMS060 objDestQMS060 = new QMS060();

                        objDestQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == reqId);

                        objDestQMS060.Welder1 = objQMS060.Welder1;
                        objDestQMS060.Welder2 = objQMS060.Welder2;
                        objDestQMS060.Welder3 = objQMS060.Welder3;
                        objDestQMS060.Welder4 = objQMS060.Welder4;
                        objDestQMS060.Welder5 = objQMS060.Welder5;
                        objDestQMS060.Welder6 = objQMS060.Welder6;
                        objDestQMS060.Welder7 = objQMS060.Welder7;
                        objDestQMS060.Welder8 = objQMS060.Welder8;
                        objDestQMS060.Welder9 = objQMS060.Welder9;
                        objDestQMS060.Welder10 = objQMS060.Welder10;
                        objDestQMS060.Welder11 = objQMS060.Welder11;
                        objDestQMS060.Welder12 = objQMS060.Welder12;
                        objDestQMS060.Welder13 = objQMS060.Welder13;
                        objDestQMS060.Welder14 = objQMS060.Welder14;
                        objDestQMS060.Welder15 = objQMS060.Welder15;
                        objDestQMS060.Welder16 = objQMS060.Welder16;
                        objDestQMS060.Welder17 = objQMS060.Welder17;
                        objDestQMS060.Welder18 = objQMS060.Welder18;
                        objDestQMS060.Welder19 = objQMS060.Welder19;
                        objDestQMS060.Welder20 = objQMS060.Welder20;
                        objDestQMS060.Welder21 = objQMS060.Welder21;
                        objDestQMS060.Welder22 = objQMS060.Welder22;
                        objDestQMS060.Welder23 = objQMS060.Welder23;
                        objDestQMS060.Welder24 = objQMS060.Welder24;

                        foreach (var QMS061 in lstQMS061)
                        {
                            QMS061 objQMS061 = new QMS061
                            {
                                HeaderId = QMS061.HeaderId,
                                RefRequestId = objDestQMS060.RequestId,
                                QualityProject = objDestQMS060.QualityProject,
                                Project = objDestQMS060.Project,
                                BU = objDestQMS060.BU,
                                Location = objDestQMS060.Location,
                                SeamNo = objDestQMS060.SeamNo,
                                StageCode = objDestQMS060.StageCode,
                                StageSequence = objDestQMS060.StageSequence,
                                IterationNo = objDestQMS060.IterationNo,
                                RequestNo = objDestQMS060.RequestNo,
                                RequestNoSequence = objDestQMS060.RequestNoSequence,
                                welderstamp = QMS061.welderstamp,
                                Weldlength = QMS061.Weldlength,
                                Weldarea = QMS061.Weldarea,
                                CreatedBy = objClsLoginInfo.UserName,
                                CreatedOn = DateTime.Now,
                                LTFPSHeaderId = QMS061.LTFPSHeaderId
                            };
                            destWelderList.Add(objQMS061);
                        }
                        db.QMS061.AddRange(destWelderList);
                        db.SaveChanges();
                    }
                }

                objQMS060.ConfirmedBy = objClsLoginInfo.UserName;
                objQMS060.ConfirmedOn = DateTime.Now;
                if (isLTFPSProject)
                {
                    objQMS060.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.OfferedToNDE.GetStringValue();
                }
                else
                {
                    objQMS060.QMS040.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.OfferedToNDE.GetStringValue();
                }
                db.SaveChanges();

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.NDE3.GetStringValue(), objQMS060.Project, objQMS060.BU, objQMS060.Location, "NDT Request " + objQMS060.RequestNo + " for Stage: " + objQMS060.StageCode + " of Seam: " + objQMS060.SeamNo + " & Project No: " + objQMS060.QualityProject + " has been confirmed and ready for your further action", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/SeamListNDEInspection/NDESeamTestDetails/" + objQMS060.RequestId);
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Seam Confirmed successfully";
            }
            else
            {
                //if (isLTFPSProject)
                //{
                //    objResponseMsg.Key = true;
                //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                //}

                if (!isLTFPSProject && objQMS060.IterationNo > 1 && objQMS060.IsPTMTCellRequired == true)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "The Request already sent to PT/MT Cell to perform PT and MT.";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                if (!IsApplicableForNDERequestConfirmation(objQMS060.RequestId, clsImplementationEnum.InspectionFor.SEAM.GetStringValue()))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This request has been already confirmed";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                string strRejected = clsImplementationEnum.SpotResults.Rejected.GetStringValue();
                List<int> arraySpotIds = spotIds.Split(',').Select(int.Parse).ToList();
                List<QMS070> lstRequestSpots = db.QMS070.Where(x => x.RequestId == requestId && arraySpotIds.Contains(x.SpotId)).ToList();
                string StageType = (new clsManager()).GetStageType(objQMS060.BU.Trim(), objQMS060.Location.Trim(), objQMS060.StageCode.Trim());
                bool isWelderRequried = false;
                bool isPTMTRequired = false;
                foreach (var itemSpot in lstRequestSpots)
                {
                    QMS070 maxSpotResult = null;
                    if (isLTFPSProject)
                    {
                        maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.StageCode == itemSpot.StageCode && x.SpotResult != null && x.RequestNo == itemSpot.RequestNo).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                    }
                    else
                    {
                        maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.SpotResult != null && x.HeaderId == itemSpot.HeaderId).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                    }

                    if (maxSpotResult.SpotResult == strRejected || maxSpotResult.IsRejected == true)
                    {
                        isWelderRequried = true;

                        // PT/MT Cell Only For Repair in RT or UT Stage
                        if (!isLTFPSProject && !string.IsNullOrWhiteSpace(StageType) && (StageType.ToUpper() == "RT" || StageType.ToUpper() == "UT"))
                        {
                            isPTMTRequired = true;
                        }
                        break;
                    }
                }

                if (!isWelderRequried && lstRequestSpots.Count > 0 && objQMS060.QMS061.Count() == 0)
                {
                    CopyWelderFromPreviousIteration(objQMS060);
                }

                if (!Manager.IsWelderRequiredInNDEStageOffer(objQMS060.BU, objQMS060.Location, objQMS060.StageCode))
                {
                    if (isPTMTRequired && objQMS060.IterationNo > 1)
                    {
                        objResponseMsg.Value = "PTMTCell";
                    }
                    objResponseMsg.Key = true;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                if (objQMS060.QMS061.Count() == 0)
                {
                    AutoCatureWelderWithWeldLength(objQMS060, isLTFPSProject);
                }

                List<QMS061> lsWelders = db.QMS061.Where(x => x.RefRequestId == requestId).ToList();

                if (lsWelders.Any())
                {
                    decimal? sum = 0;
                    int? defectArea = 0;
                    if (objQMS060.IsOverlay == true)
                    {
                        if (objQMS060.IterationNo == 1)
                        {
                            sum = lsWelders.Sum(x => x.Weldarea);
                            message = (sum <= 0 ? "Total weld area should be greater than 0." : "");
                            //message = sum == Convert.ToDecimal(objQMS060.OverlayArea) ? "Total weld Area is equal to seam overlay area" : sum > Convert.ToDecimal(objQMS060.OverlayArea) ? "Total Weld Area is greater than seam overlay area" : "Total weld area is lesser than seam overlay area";
                        }
                        else
                        {

                            List<QMS070> lstSpots = db.QMS070.Where(x => x.RequestId == requestId && arraySpotIds.Contains(x.SpotId)).ToList();
                            foreach (var itemSpot in lstSpots)
                            {
                                QMS070 maxSpotResult = null;
                                if (isLTFPSProject)
                                {
                                    maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.SpotResult == strRejected && x.RequestNo == itemSpot.RequestNo).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                                }
                                else
                                {
                                    maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.SpotResult == strRejected && x.HeaderId == itemSpot.HeaderId).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                                }

                                if (maxSpotResult != null)
                                {
                                    defectArea += maxSpotResult.DefectLength * maxSpotResult.DefectWidth;
                                }
                            }
                            sum = lsWelders.Sum(x => x.Weldarea);
                            if (defectArea > 0 && sum <= 0)
                            {
                                message = "Total weld area should be greater than 0.";
                            }
                            //message = sum == Convert.ToDecimal(defectArea) ? "Total weld Area is equal to seam defect overlay area" : sum > Convert.ToDecimal(defectArea) ? "Total Weld Area is greater than seam defect overlay area" : "Total weld area is lesser than seam defect overlay area";
                        }
                    }
                    else
                    {
                        if (objQMS060.IterationNo == 1)
                        {
                            sum = lsWelders.Sum(x => x.Weldlength);
                            message = (sum <= 0 ? "Total weld length should be greater than 0." : "");
                            //message = sum == Convert.ToDecimal(objQMS060.SeamLength) ? "Total weld length is equal to seam length" : sum > Convert.ToDecimal(objQMS060.SeamLength) ? "Total weld length is greater than seam length" : "Total weld length is lesser than seam length";
                        }
                        else
                        {
                            List<QMS070> lstSpots = db.QMS070.Where(x => x.RequestId == requestId).ToList();
                            foreach (var itemSpot in lstSpots)
                            {
                                QMS070 maxSpotResult = null;
                                if (isLTFPSProject)
                                {
                                    maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.SpotResult == strRejected && x.RequestNo == itemSpot.RequestNo).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                                }
                                else
                                {
                                    maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.SpotResult == strRejected && x.HeaderId == itemSpot.HeaderId).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                                }

                                if (maxSpotResult != null)
                                {
                                    defectArea += maxSpotResult.DefectLength;
                                }
                            }
                            sum = lsWelders.Sum(x => x.Weldlength);
                            if (defectArea > 0 && sum <= 0)
                            {
                                message = "Total weld length should be greater than 0.";
                            }
                            //message = sum == Convert.ToDecimal(defectArea) ? "Total weld length is equal to defect seam length" : sum > Convert.ToDecimal(defectArea) ? "Total weld length is greater than defect seam length" : "Total weld length is lesser than defect seam length";
                        }

                    }

                    if (message.Length > 0)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = message;
                    }
                    else
                    {
                        if (isPTMTRequired && objQMS060.IterationNo > 1)
                        {
                            objResponseMsg.Value = "PTMTCell";
                        }

                        objResponseMsg.Key = true;
                    }



                    //if (objQMS060.IsOverlay == true)
                    //{
                    //      if (objQMS060.IterationNo == 1 ? (sum == Convert.ToDecimal(objQMS060.OverlayArea) || sum > Convert.ToDecimal(objQMS060.OverlayArea)) : (sum == Convert.ToDecimal(defectArea) || sum > Convert.ToDecimal(defectArea)))
                    //      {
                    //          objResponseMsg.Key = true;
                    //      }
                    //      else
                    //      {
                    //          objResponseMsg.Key = false;
                    //          objResponseMsg.Value = message;
                    //      }
                    //}
                    //else
                    //{
                    //    if (objQMS060.IterationNo == 1 ? (sum == Convert.ToDecimal(objQMS060.SeamLength) || sum > Convert.ToDecimal(objQMS060.SeamLength)) : (sum == Convert.ToDecimal(defectArea) || sum > Convert.ToDecimal(defectArea)))
                    //    {
                    //        objResponseMsg.Key = true;
                    //    }
                    //    else
                    //    {
                    //        objResponseMsg.Key = false;
                    //        objResponseMsg.Value = message;
                    //    }
                    //}
                }
                else
                {
                    if (isWelderRequried)
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Please maintain welder wise weld length";

                        if (Manager.GetConfigValue(clsImplementationEnum.ConfigParameter.AskWelderRequiredForStageTypeOnRegenerateNDERequest.GetStringValue()).Split(',').Contains(StageType) && objQMS060.IterationNo > 1)
                        {
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "AskForWelder";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                    }
                }
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public bool disableFields(int requestId, bool flag)
        {
            string message = string.Empty;
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
            bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
            if (!isLTFPSProject && objQMS060.IterationNo > 1 && objQMS060.IsPTMTCellRequired == true)
            {
                flag = false;
            }
            return flag;
        }
        public void AutoCatureWelderWithWeldLength(QMS060 objQMS060, bool isLTFPS = false)
        {
            // Get all welders
            if (isLTFPS)
            {
                CaptureWeldersForLTFPS(objQMS060.RequestId, objQMS060.LTFPSHeaderId.Value);
            }
            else
            {
                CaptureWelders(objQMS060.RequestId, objQMS060.HeaderId.Value);
            }

            // Apply defect length to all welders
            decimal? defect = 0;
            string strRejected = clsImplementationEnum.SpotResults.Rejected.GetStringValue();
            List<QMS070> lstSpots = objQMS060.QMS070.ToList();
            foreach (var itemSpot in lstSpots)
            {
                QMS070 maxSpotResult = null;
                if (isLTFPS)
                {
                    maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.SpotResult == strRejected && x.RequestNo == objQMS060.RequestNo).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                }
                else
                {
                    maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.SpotResult == strRejected && x.HeaderId == itemSpot.HeaderId).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                }
                if (maxSpotResult != null)
                {
                    if (objQMS060.IsOverlay == true)
                    {
                        defect += maxSpotResult.DefectLength * maxSpotResult.DefectWidth;
                    }
                    else
                    {
                        defect += maxSpotResult.DefectLength;
                    }
                }
            }

            List<QMS061> lstQMS061 = objQMS060.QMS061.ToList();
            foreach (var objQMS061 in lstQMS061)
            {
                if (objQMS060.IsOverlay == true)
                {
                    objQMS061.Weldarea = defect;
                }
                else
                {
                    objQMS061.Weldlength = defect;
                }
            }
            db.SaveChanges();

        }

        [HttpPost]
        public ActionResult WelderRequiredSubmission(int requestId, bool isWelderRequired)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS060 ObjQMS060 = db.QMS060.FirstOrDefault(c => c.RequestId == requestId);
                ObjQMS060.IsWelderRequired = isWelderRequired;
                ObjQMS060.IsWelderRequiredSubmittedBy = objClsLoginInfo.UserName;
                ObjQMS060.IsWelderRequiredSubmittedOn = DateTime.Now;
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Data saved successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult QCConfirmationForPTMT(int requestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!IsApplicableForNDEPTMTConfirmation(requestId))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This request has been already confirmed";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                List<int> selectedSpots = db.QMS070.Where(x => x.RequestId == requestId).Select(s => s.SpotId).ToList();
                string spots = string.Join(",", selectedSpots);
                RespondeConfirmRequest(requestId, true, spots);

                QMS060 ObjQMS060 = db.QMS060.FirstOrDefault(c => c.RequestId == requestId);
                ObjQMS060.IsPTMTCellRequired = false;
                db.SaveChanges();

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Seam Confirmed successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        [HttpPost]
        public ActionResult SendForPTMTCellForQCConfirmation(int requestId, string spotIds)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
                objQMS060.IsPTMTCellRequired = true;
                db.SaveChanges();

                if (objQMS060.IterationNo > 1)
                {
                    List<int> arraySpotIds = spotIds.Split(',').Select(int.Parse).ToList();
                    List<QMS070> deleteSpotId = db.QMS070.Where(x => !arraySpotIds.Contains(x.SpotId) && x.RequestId == requestId).ToList();
                    db.QMS070.RemoveRange(deleteSpotId);
                    db.SaveChanges();
                }

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Request " + objQMS060.RequestNo + " has been sent to PT/MT Cell to perform PT and MT.";

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objQMS060.Project, objQMS060.BU, objQMS060.Location, "NDT Request " + objQMS060.RequestNo + " for Stage: " + objQMS060.StageCode + " of Seam: " + objQMS060.SeamNo + " & Project No: " + objQMS060.QualityProject + " has comes for your PT/MT confirmation.", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/SeamListNDEInspection/PTMTConfirmation/" + objQMS060.RequestId);
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [SessionExpireFilter]
        public ActionResult PTMTConfirmation(int id)
        {
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == id);
            ViewBag.SurfaceCondition = GetCategoryDescription("Surface Condition", objQMS060.SurfaceCondition, objQMS060.BU, objQMS060.Location).CategoryDescription;

            var objQMS002 = db.QMS002.Where(i => i.StageCode == objQMS060.StageCode && i.BU == objQMS060.BU && i.Location == objQMS060.Location).FirstOrDefault();
            if (objQMS002 != null)
            {
                ViewBag.StageType = objQMS002.StageType;
            }

            objQMS060.Project = Manager.GetProjectAndDescription(objQMS060.Project);
            objQMS060.StageCode = db.Database.SqlQuery<string>("SELECT dbo.GET_IPI_STAGECODE_DESCRIPTION('" + objQMS060.StageCode + "','" + objQMS060.BU + "','" + objQMS060.Location + "')").FirstOrDefault();
            objQMS060.SeamNo = Manager.ReplaceIllegalCharForSeamNo(db.QMS012.Where(x => x.QualityProject == objQMS060.QualityProject && x.SeamNo == objQMS060.SeamNo).Select(x => x.SeamNo).FirstOrDefault());

            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", objQMS060.BU, objQMS060.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", objQMS060.BU, objQMS060.Location, null);
            objQMS060.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency1);
            objQMS060.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency2);
            objQMS060.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency3);
            objQMS060.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency4);
            objQMS060.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency5);
            objQMS060.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency6);
            objQMS060.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency1Intervention);
            objQMS060.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency2Intervention);
            objQMS060.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency3Intervention);
            objQMS060.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency4Intervention);
            objQMS060.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency5Intervention);
            objQMS060.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency6Intervention);
            objQMS060.Location = db.COM002.Where(x => x.t_dimx == objQMS060.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS060.BU = db.COM002.Where(x => x.t_dimx == objQMS060.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS060.CreatedBy = Manager.GetPsidandDescription(objQMS060.CreatedBy);
            objQMS060.RequestGeneratedBy = Manager.GetPsidandDescription(objQMS060.RequestGeneratedBy);
            objQMS060.OfferedBy = Manager.GetPsidandDescription(objQMS060.OfferedBy);
            objQMS060.ConfirmedBy = Manager.GetPsidandDescription(objQMS060.ConfirmedBy);

            if (objQMS060.SeamStatus == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue())
            {
                objQMS060.SeamStatus = "Yes";
            }
            else
            {
                if (objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue())
                {
                    objQMS060.SeamStatus = "No";
                }
                else
                {
                    objQMS060.SeamStatus = "";
                }
            }
            return View(objQMS060);
        }

        public ActionResult IsParallelSeamConfirmed(int requestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            objResponseMsg.Key = false;
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
            if (objQMS060.IterationNo == 1)
            {
                string parallelRequestId = "";
                bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
                if (isLTFPSProject)
                {
                    parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_LTFPS_PARALLEL_SEAM_NDE_REQUEST('" + requestId + "')").FirstOrDefault();
                }
                else
                {
                    parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_IPI_PARALLEL_SEAM_NDE_REQUEST('" + requestId + "')").FirstOrDefault();
                }

                List<int> parallelId = parallelRequestId.Split(',').Where(x => x != Convert.ToString(requestId)).Select(Int32.Parse).ToList();

                foreach (var item in parallelId)
                {
                    QMS060 obj = new QMS060();
                    obj = db.QMS060.FirstOrDefault(x => x.RequestId == item && x.IterationNo == 1);
                    if (obj != null && !string.IsNullOrWhiteSpace(obj.ConfirmedBy))
                    {
                        objResponseMsg.Key = true;
                    }
                    break;
                }
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CopyWelderFromPreviousIteration(QMS060 objQMS060)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<string> _weldingProcess = new List<string>();
            QMS060 objPrevQMS06 = db.QMS060.FirstOrDefault(x => x.RequestNo == objQMS060.RequestNo && x.StageCode == objQMS060.StageCode && x.IterationNo == (objQMS060.IterationNo - 1));

            if (objPrevQMS06 != null)
            {
                int count = 1;
                List<QMS061> lstPrevQMS061 = objPrevQMS06.QMS061.ToList();
                List<QMS061> lstQMS061 = new List<QMS061>();
                foreach (var qms61 in lstPrevQMS061)
                {
                    QMS061 objQMS061 = new QMS061
                    {
                        HeaderId = objQMS060.HeaderId,
                        RefRequestId = objQMS060.RequestId,
                        QualityProject = objQMS060.QualityProject,
                        Project = objQMS060.Project,
                        BU = objQMS060.BU,
                        Location = objQMS060.Location,
                        SeamNo = objQMS060.SeamNo,
                        StageCode = objQMS060.StageCode,
                        StageSequence = objQMS060.StageSequence,
                        IterationNo = objQMS060.IterationNo,
                        RequestNo = objQMS060.RequestNo,
                        RequestNoSequence = objQMS060.RequestNoSequence,
                        welderstamp = qms61.welderstamp,
                        Weldlength = qms61.Weldlength,
                        Weldarea = qms61.Weldarea,
                        WeldingProcess = qms61.WeldingProcess,
                        CreatedBy = objClsLoginInfo.UserName,
                        CreatedOn = DateTime.Now,
                        LTFPSHeaderId = objQMS060.LTFPSHeaderId
                    };
                    lstQMS061.Add(objQMS061);
                    switch (count)
                    {
                        case 1:
                            objQMS060.Welder1 = qms61.welderstamp;
                            break;
                        case 2:
                            objQMS060.Welder2 = qms61.welderstamp;
                            break;
                        case 3:
                            objQMS060.Welder3 = qms61.welderstamp;
                            break;
                        case 4:
                            objQMS060.Welder4 = qms61.welderstamp;
                            break;
                        case 5:
                            objQMS060.Welder5 = qms61.welderstamp;
                            break;
                        case 6:
                            objQMS060.Welder6 = qms61.welderstamp;
                            break;
                        case 7:
                            objQMS060.Welder7 = qms61.welderstamp;
                            break;
                        case 8:
                            objQMS060.Welder8 = qms61.welderstamp;
                            break;
                        case 9:
                            objQMS060.Welder9 = qms61.welderstamp;
                            break;
                        case 10:
                            objQMS060.Welder10 = qms61.welderstamp;
                            break;
                        case 11:
                            objQMS060.Welder11 = qms61.welderstamp;
                            break;
                        case 12:
                            objQMS060.Welder12 = qms61.welderstamp;
                            break;
                        case 13:
                            objQMS060.Welder13 = qms61.welderstamp;
                            break;
                        case 14:
                            objQMS060.Welder14 = qms61.welderstamp;
                            break;
                        case 15:
                            objQMS060.Welder15 = qms61.welderstamp;
                            break;
                        case 16:
                            objQMS060.Welder16 = qms61.welderstamp;
                            break;
                        case 17:
                            objQMS060.Welder17 = qms61.welderstamp;
                            break;
                        case 18:
                            objQMS060.Welder18 = qms61.welderstamp;
                            break;
                        case 19:
                            objQMS060.Welder19 = qms61.welderstamp;
                            break;
                        case 20:
                            objQMS060.Welder20 = qms61.welderstamp;
                            break;
                        case 21:
                            objQMS060.Welder21 = qms61.welderstamp;
                            break;
                        case 22:
                            objQMS060.Welder22 = qms61.welderstamp;
                            break;
                        case 23:
                            objQMS060.Welder23 = qms61.welderstamp;
                            break;
                        case 24:
                            objQMS060.Welder24 = qms61.welderstamp;
                            break;
                        default:
                            break;
                    }
                    _weldingProcess.AddRange(qms61.WeldingProcess.Split('+'));
                    count++;

                }
                db.QMS061.AddRange(lstQMS061);
                if (_weldingProcess.Count > 0)
                {
                    objQMS060.WeldingProcess = string.Join("+", _weldingProcess.Distinct());
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Welders Copied Successfully";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CaptureWelders(int requestId, int headerId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<string> _weldingProcess = new List<string>();
            List<WPS025> lstWPS025 = new List<WPS025>();
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
            List<QMS060> lstRequestList = db.QMS060.Where(x => x.HeaderId == headerId).ToList();

            List<QMS061> lstWelderList = db.QMS061.Where(x => x.RefRequestId == requestId).ToList();
            List<string> lstAssigedWeldwers = lstWelderList.Select(y => y.welderstamp).ToList();

            DateTime? lastIterationConfirmDate = db.QMS060.Where(x => x.IterationNo == objQMS060.IterationNo - 1 && x.HeaderId == headerId).Select(x => x.ConfirmedOn).FirstOrDefault();
            if (lastIterationConfirmDate.HasValue)
                //lstWPS025 = db.WPS025.Where(x => /*!lstAssigedWeldwers.Contains(x.WelderPSNO) &&*/ x.BU == objQMS060.BU && x.Location == objQMS060.Location && x.QualityProject == objQMS060.QualityProject && x.Project == objQMS060.Project && x.SeamNo == objQMS060.SeamNo && x.PrintedOn > lastIterationConfirmDate).ToList();
                lstWPS025 = db.WPS025.Where(x => /*!lstAssigedWeldwers.Contains(x.WelderPSNO) &&*/ x.BU == objQMS060.BU && x.QualityProject == objQMS060.QualityProject && x.Project == objQMS060.Project && x.SeamNo == objQMS060.SeamNo && x.PrintedOn > lastIterationConfirmDate).OrderBy(o => o.PrintedOn).ToList();
            else
                //lstWPS025 = db.WPS025.Where(x => /*!lstAssigedWeldwers.Contains(x.WelderPSNO) &&*/ x.BU == objQMS060.BU && x.Location == objQMS060.Location && x.QualityProject == objQMS060.QualityProject && x.Project == objQMS060.Project && x.SeamNo == objQMS060.SeamNo).ToList();
                lstWPS025 = db.WPS025.Where(x => /*!lstAssigedWeldwers.Contains(x.WelderPSNO) &&*/ x.BU == objQMS060.BU && x.QualityProject == objQMS060.QualityProject && x.Project == objQMS060.Project && x.SeamNo == objQMS060.SeamNo).OrderBy(o => o.PrintedOn).ToList();


            if (lstWPS025.Count > 0)
            {
                List<QMS061> lstQMS061 = new List<QMS061>();
                List<string> lstWelders = new List<string>();
                int count = 1;
                if (lstWelderList.Count > 0)
                {
                    count = lstWelderList.Count + 1;
                }
                foreach (var wps25 in lstWPS025)
                {
                    if (!lstWelderList.Select(y => y.welderstamp).Contains(wps25.WelderStamp) && !lstWelders.Contains(wps25.WelderStamp))
                    {
                        QMS061 objQMS061 = new QMS061
                        {
                            HeaderId = Convert.ToInt32(objQMS060.HeaderId),
                            RefRequestId = objQMS060.RequestId,
                            QualityProject = objQMS060.QualityProject,
                            Project = objQMS060.Project,
                            BU = objQMS060.BU,
                            Location = objQMS060.Location,
                            SeamNo = objQMS060.SeamNo,
                            StageCode = objQMS060.StageCode,
                            StageSequence = objQMS060.StageSequence,
                            IterationNo = objQMS060.IterationNo,
                            RequestNo = objQMS060.RequestNo,
                            RequestNoSequence = objQMS060.RequestNoSequence,
                            welderstamp = wps25.WelderStamp,
                            WeldingProcess = wps25.WeldingProcess,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        };
                        lstQMS061.Add(objQMS061);
                        switch (count)
                        {
                            case 1:
                                objQMS060.Welder1 = wps25.WelderStamp;
                                break;
                            case 2:
                                objQMS060.Welder2 = wps25.WelderStamp;
                                break;
                            case 3:
                                objQMS060.Welder3 = wps25.WelderStamp;
                                break;
                            case 4:
                                objQMS060.Welder4 = wps25.WelderStamp;
                                break;
                            case 5:
                                objQMS060.Welder5 = wps25.WelderStamp;
                                break;
                            case 6:
                                objQMS060.Welder6 = wps25.WelderStamp;
                                break;
                            case 7:
                                objQMS060.Welder7 = wps25.WelderStamp;
                                break;
                            case 8:
                                objQMS060.Welder8 = wps25.WelderStamp;
                                break;
                            case 9:
                                objQMS060.Welder9 = wps25.WelderStamp;
                                break;
                            case 10:
                                objQMS060.Welder10 = wps25.WelderStamp;
                                break;
                            case 11:
                                objQMS060.Welder11 = wps25.WelderStamp;
                                break;
                            case 12:
                                objQMS060.Welder12 = wps25.WelderStamp;
                                break;
                            case 13:
                                objQMS060.Welder13 = wps25.WelderStamp;
                                break;
                            case 14:
                                objQMS060.Welder14 = wps25.WelderStamp;
                                break;
                            case 15:
                                objQMS060.Welder15 = wps25.WelderStamp;
                                break;
                            case 16:
                                objQMS060.Welder16 = wps25.WelderStamp;
                                break;
                            case 17:
                                objQMS060.Welder17 = wps25.WelderStamp;
                                break;
                            case 18:
                                objQMS060.Welder18 = wps25.WelderStamp;
                                break;
                            case 19:
                                objQMS060.Welder19 = wps25.WelderStamp;
                                break;
                            case 20:
                                objQMS060.Welder20 = wps25.WelderStamp;
                                break;
                            case 21:
                                objQMS060.Welder21 = wps25.WelderStamp;
                                break;
                            case 22:
                                objQMS060.Welder22 = wps25.WelderStamp;
                                break;
                            case 23:
                                objQMS060.Welder23 = wps25.WelderStamp;
                                break;
                            case 24:
                                objQMS060.Welder24 = wps25.WelderStamp;
                                break;
                            default:
                                break;
                        }
                        lstWelders.Add(wps25.WelderStamp);
                        _weldingProcess.Add(wps25.WeldingProcess);
                        count++;
                    }
                    else
                    {
                        if (lstWelderList.Select(y => y.welderstamp).Contains(wps25.WelderStamp))
                        {
                            QMS061 existingWelder = lstWelderList.FirstOrDefault(x => x.welderstamp == wps25.WelderStamp);
                            if (existingWelder != null && !existingWelder.WeldingProcess.Split('+').Contains(wps25.WeldingProcess))
                                existingWelder.WeldingProcess = existingWelder.WeldingProcess + "+" + wps25.WeldingProcess;
                        }
                        else if (lstQMS061.Select(y => y.welderstamp).Contains(wps25.WelderStamp))
                        {
                            lstQMS061.Where(w => w.welderstamp == wps25.WelderStamp && !w.WeldingProcess.Split('+').Contains(wps25.WeldingProcess)).ToList().ForEach(x => x.WeldingProcess = (x.WeldingProcess + "+" + wps25.WeldingProcess));
                        }

                        _weldingProcess.Add(wps25.WeldingProcess);

                    }

                }
                db.QMS061.AddRange(lstQMS061);
                if (_weldingProcess.Count > 0)
                {
                    objQMS060.WeldingProcess = string.Join("+", _weldingProcess.Distinct());
                }
                db.SaveChanges();
                objResponseMsg.Remarks = objQMS060.WeldingProcess;
                objResponseMsg.Status = string.Join(",", lstWelders);
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Welders Captured Successfully";
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please add Welders in Welder wise Parameter Slip";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CaptureWeldersForLTFPS(int requestId, int ltfpsHeaderId)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            List<string> _weldingProcess = new List<string>();
            List<WPS025> lstWPS025 = new List<WPS025>();
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
            List<QMS060> lstRequestList = db.QMS060.Where(x => x.LTFPSHeaderId == ltfpsHeaderId).ToList();
            List<QMS061> lstWelderList = db.QMS061.Where(x => x.RefRequestId == requestId).ToList();
            List<string> lstAssigedWeldwers = lstWelderList.Select(y => y.welderstamp).ToList();

            DateTime? lastIterationConfirmDate = db.QMS060.Where(x => x.IterationNo == objQMS060.IterationNo - 1 && x.StageCode == objQMS060.StageCode && x.RequestNo == objQMS060.RequestNo).Select(x => x.ConfirmedOn).FirstOrDefault();
            if (lastIterationConfirmDate.HasValue)
                lstWPS025 = db.WPS025.Where(x => /*!lstAssigedWeldwers.Contains(x.WelderPSNO) &&*/ x.BU == objQMS060.BU && x.QualityProject == objQMS060.QualityProject && x.Project == objQMS060.Project && x.SeamNo == objQMS060.SeamNo && x.PrintedOn > lastIterationConfirmDate).OrderBy(o => o.PrintedOn).ToList();
            else
                lstWPS025 = db.WPS025.Where(x => /*!lstAssigedWeldwers.Contains(x.WelderPSNO) &&*/ x.BU == objQMS060.BU && x.QualityProject == objQMS060.QualityProject && x.Project == objQMS060.Project && x.SeamNo == objQMS060.SeamNo).OrderBy(o => o.PrintedOn).ToList();


            if (lstWPS025.Count > 0)
            {
                List<QMS061> lstQMS061 = new List<QMS061>();
                List<string> lstWelders = new List<string>();
                int count = 1;
                if (lstWelderList.Count > 0)
                {
                    count = lstWelderList.Count + 1;
                }
                foreach (var wps25 in lstWPS025)
                {
                    if (!lstWelderList.Select(y => y.welderstamp).Contains(wps25.WelderStamp) && !lstWelders.Contains(wps25.WelderStamp))
                    {
                        QMS061 objQMS061 = new QMS061
                        {
                            LTFPSHeaderId = Convert.ToInt32(objQMS060.LTFPSHeaderId),
                            RefRequestId = objQMS060.RequestId,
                            QualityProject = objQMS060.QualityProject,
                            Project = objQMS060.Project,
                            BU = objQMS060.BU,
                            Location = objQMS060.Location,
                            SeamNo = objQMS060.SeamNo,
                            StageCode = objQMS060.StageCode,
                            StageSequence = objQMS060.StageSequence,
                            IterationNo = objQMS060.IterationNo,
                            RequestNo = objQMS060.RequestNo,
                            RequestNoSequence = objQMS060.RequestNoSequence,
                            welderstamp = wps25.WelderStamp,
                            WeldingProcess = wps25.WeldingProcess,
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now
                        };
                        lstQMS061.Add(objQMS061);
                        switch (count)
                        {
                            case 1:
                                objQMS060.Welder1 = wps25.WelderStamp;
                                break;
                            case 2:
                                objQMS060.Welder2 = wps25.WelderStamp;
                                break;
                            case 3:
                                objQMS060.Welder3 = wps25.WelderStamp;
                                break;
                            case 4:
                                objQMS060.Welder4 = wps25.WelderStamp;
                                break;
                            case 5:
                                objQMS060.Welder5 = wps25.WelderStamp;
                                break;
                            case 6:
                                objQMS060.Welder6 = wps25.WelderStamp;
                                break;
                            case 7:
                                objQMS060.Welder7 = wps25.WelderStamp;
                                break;
                            case 8:
                                objQMS060.Welder8 = wps25.WelderStamp;
                                break;
                            case 9:
                                objQMS060.Welder9 = wps25.WelderStamp;
                                break;
                            case 10:
                                objQMS060.Welder10 = wps25.WelderStamp;
                                break;
                            case 11:
                                objQMS060.Welder11 = wps25.WelderStamp;
                                break;
                            case 12:
                                objQMS060.Welder12 = wps25.WelderStamp;
                                break;
                            case 13:
                                objQMS060.Welder13 = wps25.WelderStamp;
                                break;
                            case 14:
                                objQMS060.Welder14 = wps25.WelderStamp;
                                break;
                            case 15:
                                objQMS060.Welder15 = wps25.WelderStamp;
                                break;
                            case 16:
                                objQMS060.Welder16 = wps25.WelderStamp;
                                break;
                            case 17:
                                objQMS060.Welder17 = wps25.WelderStamp;
                                break;
                            case 18:
                                objQMS060.Welder18 = wps25.WelderStamp;
                                break;
                            case 19:
                                objQMS060.Welder19 = wps25.WelderStamp;
                                break;
                            case 20:
                                objQMS060.Welder20 = wps25.WelderStamp;
                                break;
                            case 21:
                                objQMS060.Welder21 = wps25.WelderStamp;
                                break;
                            case 22:
                                objQMS060.Welder22 = wps25.WelderStamp;
                                break;
                            case 23:
                                objQMS060.Welder23 = wps25.WelderStamp;
                                break;
                            case 24:
                                objQMS060.Welder24 = wps25.WelderStamp;
                                break;
                            default:
                                break;
                        }
                        lstWelders.Add(wps25.WelderStamp);
                        _weldingProcess.Add(wps25.WeldingProcess);
                        count++;
                    }
                    else
                    {
                        if (lstWelderList.Select(y => y.welderstamp).Contains(wps25.WelderStamp))
                        {
                            QMS061 existingWelder = lstWelderList.FirstOrDefault(x => x.welderstamp == wps25.WelderStamp);
                            if (existingWelder != null && !existingWelder.WeldingProcess.Split('+').Contains(wps25.WeldingProcess))
                                existingWelder.WeldingProcess = existingWelder.WeldingProcess + "+" + wps25.WeldingProcess;
                        }
                        else if (lstQMS061.Select(y => y.welderstamp).Contains(wps25.WelderStamp))
                        {
                            lstQMS061.Where(w => w.welderstamp == wps25.WelderStamp && !w.WeldingProcess.Split('+').Contains(wps25.WeldingProcess)).ToList().ForEach(x => x.WeldingProcess = (x.WeldingProcess + "+" + wps25.WeldingProcess));
                        }

                        _weldingProcess.Add(wps25.WeldingProcess);

                    }
                }
                db.QMS061.AddRange(lstQMS061);
                if (_weldingProcess.Count > 0)
                {
                    objQMS060.WeldingProcess = string.Join("+", _weldingProcess.Distinct());
                }
                db.SaveChanges();
                objResponseMsg.Remarks = objQMS060.WeldingProcess;
                objResponseMsg.Status = string.Join(",", lstWelders);
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Welders Captured Successfully";
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please add Welders in Welder wise Parameter Slip";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CheckWelders(int requestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<QMS061> lstQMS061 = db.QMS061.Where(x => x.RefRequestId == requestId).ToList();
            if (lstQMS061.Any())
            {
                objResponseMsg.Key = true;
            }
            else
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public ActionResult CheckSpotWelders(int requestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS060 objQMS060 = db.QMS060.Where(x => x.RequestId == requestId).FirstOrDefault();

            if (!Manager.IsWelderRequiredInNDEStageOffer(objQMS060.BU, objQMS060.Location, objQMS060.StageCode))
            {
                objResponseMsg.Key = true;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

            //if (Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location))
            //{
            //    objResponseMsg.Key = true;
            //    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            //}
            bool isValid = true;
            string strRejectedStatus = clsImplementationEnum.SpotResults.Rejected.GetStringValue();
            List<QMS070> lstQMS070 = db.QMS070.Where(x => x.RequestId == requestId && x.SpotResult == strRejectedStatus).ToList();
            foreach (QMS070 item in lstQMS070)
            {
                if (string.IsNullOrWhiteSpace(item.Welder1))
                {
                    isValid = false;
                    objResponseMsg.Value = "Please assign welders in rejected spots before generate request";
                    break;
                }
            }
            objResponseMsg.Key = isValid;
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReGenerateRequest(int requestId)
        {
            int newRequestId = 0;
            LTF002 reworkLTFPS = null;
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if (!IsApplicableForNDERegenerateRequest(requestId))
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "This request has been already regenerated";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

            List<string> lstMRRResultType = new List<string> { "Miscellaneous", "Reshoot", "Re-Scan", "Re-Take" };
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
            bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);

            if (isLTFPSProject)
            {
                reworkLTFPS = db.LTF002.Where(c => c.ParentLineId == objQMS060.LTFPSHeaderId && c.TestType == objQMS060.LTF002.TestType && c.QualityProject == objQMS060.LTF002.QualityProject && c.Project == objQMS060.LTF002.Project && c.SeamNo == objQMS060.LTF002.SeamNo && c.Location == objQMS060.LTF002.Location && c.BU == objQMS060.LTF002.BU && c.TestFor == objQMS060.LTF002.TestFor).FirstOrDefault();
            }
            string strDiscard = clsImplementationEnum.SpotResults.Discard.GetStringValue();
            string strSpotResultclear = clsImplementationEnum.SpotResults.Cleared.GetStringValue();
            string strSpotResultRejected = clsImplementationEnum.SpotResults.Rejected.GetStringValue();
            //int? repairDesignation = db.QMS001.Where(x => x.Project == objQMS060.Project && x.BU == objQMS060.BU && x.Location == objQMS060.Location).Select(x => x.RepairDesignationToSendEmail).FirstOrDefault();
            QMS001 objQMS001 = db.QMS001.Where(x => x.Project == objQMS060.Project && x.BU == objQMS060.BU && x.Location == objQMS060.Location).FirstOrDefault();

            string parallelHeaderId;
            List<int> parallelId;
            List<QMS070> lstAllSpot = new List<QMS070>();
            if (isLTFPSProject)
            {
                parallelHeaderId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_LTFPS_PARALLEL_SEAM('" + objQMS060.LTFPSHeaderId + "')").FirstOrDefault();
                parallelId = parallelHeaderId.Split(',').Where(x => x != Convert.ToString(objQMS060.LTFPSHeaderId)).Select(Int32.Parse).ToList();
                lstAllSpot = db.QMS070.Where(x => x.RequestNo == objQMS060.RequestNo && x.StageCode == objQMS060.StageCode && x.IterationNo == 1).ToList();
            }
            else
            {
                parallelHeaderId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_IPI_PARALLEL_SEAM('" + objQMS060.HeaderId + "')").FirstOrDefault();
                parallelId = parallelHeaderId.Split(',').Where(x => x != Convert.ToString(objQMS060.HeaderId)).Select(Int32.Parse).ToList();
                lstAllSpot = db.QMS070.Where(x => x.HeaderId == objQMS060.HeaderId && x.IterationNo == 1).ToList();
            }
            bool isParallelReqGenearte = false;
            bool isRequestGenerated = false;
            // For Parallel
            if (parallelId.Any())
            {
                if (isLTFPSProject)
                {
                    foreach (QMS070 itemSpot in lstAllSpot)
                    {
                        QMS070 maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.StageCode == objQMS060.StageCode && x.RequestNo == itemSpot.RequestNo).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                        if (lstMRRResultType.Contains(maxSpotResult.SpotResult) || ((maxSpotResult.IsRejected == true || maxSpotResult.SpotResult == strSpotResultRejected) && (db.QMS070.Any(c => c.SpotNumber == maxSpotResult.SpotNumber && c.RepairDesignationValue == maxSpotResult.RepairDesignationValue && parallelId.Contains(c.LTFPSHeaderId.Value) && c.SpotResult != null))))
                        {
                            isParallelReqGenearte = true;
                        }
                    }
                }
                else
                {
                    foreach (QMS070 itemSpot in lstAllSpot)
                    {
                        QMS070 maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.HeaderId == itemSpot.HeaderId).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                        if (lstMRRResultType.Contains(maxSpotResult.SpotResult) || ((maxSpotResult.IsRejected == true || maxSpotResult.SpotResult == strSpotResultRejected) && (db.QMS070.Any(c => c.SpotNumber == maxSpotResult.SpotNumber && c.RepairDesignationValue == maxSpotResult.RepairDesignationValue && parallelId.Contains(c.HeaderId.Value) && c.SpotResult != null))))
                        {
                            isParallelReqGenearte = true;
                        }
                    }
                }
            }

            QMS060 objNewQMS060 = new QMS060();

            if ((parallelId.Any() && isParallelReqGenearte) || !parallelId.Any())
            {
                objNewQMS060.HeaderId = objQMS060.HeaderId;
                objNewQMS060.QualityProject = objQMS060.QualityProject;
                objNewQMS060.Project = objQMS060.Project;
                objNewQMS060.BU = objQMS060.BU;
                objNewQMS060.Location = objQMS060.Location;
                objNewQMS060.SeamNo = objQMS060.SeamNo;
                objNewQMS060.StageCode = objQMS060.StageCode;
                objNewQMS060.StageSequence = objQMS060.StageSequence;
                objNewQMS060.IterationNo = objQMS060.IterationNo + 1;
                objNewQMS060.RequestNo = objQMS060.RequestNo;
                objNewQMS060.RequestNoSequence = objQMS060.RequestNoSequence;
                objNewQMS060.RequestStatus = clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue();
                objNewQMS060.SeamLength = objQMS060.SeamLength;
                objNewQMS060.OverlayArea = objQMS060.OverlayArea;
                objNewQMS060.OverlayThickness = objQMS060.OverlayThickness;
                objNewQMS060.Jointtype = objQMS060.Jointtype;
                objNewQMS060.ManufacturingCode = objQMS060.ManufacturingCode;
                objNewQMS060.Part1Position = objQMS060.Part1Position;
                objNewQMS060.Part1thickness = objQMS060.Part1thickness;
                objNewQMS060.Part1Material = objQMS060.Part1Material;
                objNewQMS060.Part2Position = objQMS060.Part2Position;
                objNewQMS060.Part2thickness = objQMS060.Part2thickness;
                objNewQMS060.Part2Material = objQMS060.Part2Material;
                objNewQMS060.Part3Position = objQMS060.Part3Position;
                objNewQMS060.Part3thickness = objQMS060.Part3thickness;
                objNewQMS060.Part3Material = objQMS060.Part3Material;
                objNewQMS060.Part4Position = objQMS060.Part4Position;
                objNewQMS060.Part4thickness = objQMS060.Part4thickness;
                objNewQMS060.Part4Material = objQMS060.Part4Material;
                objNewQMS060.Part5Position = objQMS060.Part5Position;
                objNewQMS060.Part5thickness = objQMS060.Part5thickness;
                objNewQMS060.Part5Material = objQMS060.Part5Material;
                objNewQMS060.Part6Position = objQMS060.Part6Position;
                objNewQMS060.Part6thickness = objQMS060.Part6thickness;
                objNewQMS060.Part6Material = objQMS060.Part6Material;
                objNewQMS060.AcceptanceStandard = objQMS060.AcceptanceStandard;
                objNewQMS060.ApplicableSpecification = objQMS060.ApplicableSpecification;
                objNewQMS060.InspectionExtent = objQMS060.InspectionExtent;
                objNewQMS060.WeldingEngineeringRemarks = objQMS060.WeldingEngineeringRemarks;
                objNewQMS060.TPIAgency1 = objQMS060.TPIAgency1;
                objNewQMS060.TPIAgency1Intervention = objQMS060.TPIAgency1Intervention;
                objNewQMS060.TPIAgency2 = objQMS060.TPIAgency2;
                objNewQMS060.TPIAgency2Intervention = objQMS060.TPIAgency2Intervention;
                objNewQMS060.TPIAgency3 = objQMS060.TPIAgency3;
                objNewQMS060.TPIAgency3Intervention = objQMS060.TPIAgency3Intervention;
                objNewQMS060.TPIAgency4 = objQMS060.TPIAgency4;
                objNewQMS060.TPIAgency4Intervention = objQMS060.TPIAgency4Intervention;
                objNewQMS060.TPIAgency5 = objQMS060.TPIAgency5;
                objNewQMS060.TPIAgency5Intervention = objQMS060.TPIAgency5Intervention;
                objNewQMS060.TPIAgency6 = objQMS060.TPIAgency6;
                objNewQMS060.TPIAgency6Intervention = objQMS060.TPIAgency6Intervention;
                //objNewQMS060.WeldingProcess = objQMS060.WeldingProcess;
                //objNewQMS060.SeamStatus = objQMS060.SeamStatus;
                objNewQMS060.IsPartiallyOffered = false;
                objNewQMS060.Shop = objQMS060.Shop;
                objNewQMS060.ShopLocation = objQMS060.ShopLocation;
                objNewQMS060.ShopRemark = objQMS060.ShopRemark;
                objNewQMS060.SurfaceCondition = objQMS060.SurfaceCondition;
                objNewQMS060.WeldThicknessReinforcement = objQMS060.WeldThicknessReinforcement;
                objNewQMS060.SpotGenerationtype = objQMS060.SpotGenerationtype;
                objNewQMS060.SpotLength = objQMS060.SpotLength;
                objNewQMS060.PrintSummary = objQMS060.PrintSummary;
                objNewQMS060.CreatedBy = objClsLoginInfo.UserName;
                objNewQMS060.CreatedOn = DateTime.Now;
                objNewQMS060.RequestGeneratedBy = objClsLoginInfo.UserName;
                objNewQMS060.RequestGeneratedOn = DateTime.Now;
                objNewQMS060.IsOverlay = objQMS060.IsOverlay;
                objNewQMS060.IsParallel = objQMS060.IsParallel;
                objNewQMS060.OfferedBy = objQMS060.OfferedBy;
                objNewQMS060.OfferedOn = objQMS060.OfferedOn;
                objNewQMS060.LTFPSHeaderId = (reworkLTFPS != null ? reworkLTFPS.LineId : objQMS060.LTFPSHeaderId);

                db.QMS060.Add(objNewQMS060);
                objQMS060.IsPartiallyOffered = true;
                if (isLTFPSProject)
                {
                    objNewQMS060.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.PendingForConfirmation.GetStringValue();
                    objNewQMS060.LTF002.RequestNo = objNewQMS060.RequestNo;
                    objNewQMS060.LTF002.IterationNo = objNewQMS060.IterationNo;
                    objNewQMS060.IsPartiallyOffered = true;
                }
                else
                {
                    objQMS060.QMS040.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.PendingForConfirmation.GetStringValue();
                }
                db.SaveChanges();
                newRequestId = objNewQMS060.RequestId;
                isRequestGenerated = true;

            }

            if (isRequestGenerated)
            {
                if (parallelId.Any())
                {
                    #region For Parallel
                    List<QMS070> lstNewSpots = new List<QMS070>();
                    QMS070 maxSpotResult = new QMS070();
                    foreach (QMS070 itemSpot in lstAllSpot)
                    {
                        bool isSpotAdd = false;
                        if (isLTFPSProject)
                        {
                            maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.StageCode == objQMS060.StageCode && x.RequestNo == itemSpot.RequestNo).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                            if (lstMRRResultType.Contains(maxSpotResult.SpotResult) || ((maxSpotResult.IsRejected == true || maxSpotResult.SpotResult == strSpotResultRejected) && (db.QMS070.Any(c => c.SpotNumber == maxSpotResult.SpotNumber && c.RepairDesignationValue == maxSpotResult.RepairDesignationValue && parallelId.Contains(c.LTFPSHeaderId.Value) && c.SpotResult != null))))
                            {
                                isSpotAdd = true;
                            }
                        }
                        else
                        {
                            maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.HeaderId == itemSpot.HeaderId).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                            if (lstMRRResultType.Contains(maxSpotResult.SpotResult) || ((maxSpotResult.IsRejected == true || maxSpotResult.SpotResult == strSpotResultRejected) && (db.QMS070.Any(c => c.SpotNumber == maxSpotResult.SpotNumber && c.RepairDesignationValue == maxSpotResult.RepairDesignationValue && parallelId.Contains(c.HeaderId.Value) && c.SpotResult != null))))
                            {
                                isSpotAdd = true;
                            }
                        }

                        if (isSpotAdd)
                        {
                            maxSpotResult.RepairDesignationValue = (maxSpotResult.IsRejected == true || maxSpotResult.SpotResult == strSpotResultRejected) ? maxSpotResult.RepairDesignationValue + 1 : maxSpotResult.RepairDesignationValue;
                            maxSpotResult.IterationNo = objNewQMS060.IterationNo;
                            maxSpotResult.RequestId = newRequestId;
                            maxSpotResult.SpotResult = null;
                            //maxSpotResult.SpotLength = maxSpotResult.SpotResult == strSpotResultRejected ? maxSpotResult.DefectLength : maxSpotResult.SpotLength;
                            maxSpotResult.DefectLength = null;
                            maxSpotResult.DefectWidth = null;
                            maxSpotResult.IsRejected = isParallelSpotRejected(maxSpotResult.RepairDesignationValue.Value, maxSpotResult.SpotNumber.Value, parallelId);
                            maxSpotResult.TypeofIndication = null;
                            maxSpotResult.Reason = null;
                            maxSpotResult.InspectionRemarks = null;
                            maxSpotResult.CreatedBy = objClsLoginInfo.UserName;
                            maxSpotResult.CreatedOn = DateTime.Now;
                            maxSpotResult.EditedBy = null;
                            maxSpotResult.EditedOn = null;
                            maxSpotResult.LTFPSHeaderId = (reworkLTFPS != null ? reworkLTFPS.LineId : maxSpotResult.LTFPSHeaderId);
                            maxSpotResult.LNTSpotResult = null;
                            maxSpotResult.TPI1SpotResult = null;
                            maxSpotResult.TPI2SpotResult = null;
                            maxSpotResult.TPI3SpotResult = null;
                            maxSpotResult.TPI4SpotResult = null;
                            maxSpotResult.TPI5SpotResult = null;
                            maxSpotResult.TPI6SpotResult = null;
                            lstNewSpots.Add(maxSpotResult);
                        }
                    }
                    db.QMS070.AddRange(lstNewSpots);
                    db.SaveChanges();
                    #endregion
                }
                else
                {
                    #region For Non Parallel
                    List<QMS070> lstMRR = new List<QMS070>(); //db.QMS070.Where(x => x.RequestId == requestId && x.SpotResult != strSpotResultclear).ToList();
                    bool isNewSpotApplicable = false;

                    #region For inspection Extent < 100

                    int inspectionExtent1;
                    if (int.TryParse(objNewQMS060.InspectionExtent.Replace("%", "").Trim(), out inspectionExtent1))
                    {
                        if (inspectionExtent1 < 100)
                        {
                            List<int> spotsNos = new List<int>();
                            spotsNos = db.QMS070.Where(c => c.RequestNo == objQMS060.RequestNo).GroupBy(g => g.SpotNumber.Value).Select(x => x.OrderByDescending(o => o.SpotId).FirstOrDefault().SpotId).ToList();
                            if (spotsNos.Count > 0)
                            {
                                lstAllSpot.Clear();
                                lstAllSpot = db.QMS070.Where(c => spotsNos.Contains(c.SpotId)).ToList();
                            }
                        }
                    }

                    #endregion

                    foreach (QMS070 itemSpot in lstAllSpot)
                    {
                        QMS070 maxSpotResult = new QMS070();
                        if (isLTFPSProject)
                        {
                            maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.RequestNo == itemSpot.RequestNo).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                        }
                        else
                        {
                            maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.HeaderId == itemSpot.HeaderId).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                        }

                        if (maxSpotResult.SpotResult != strSpotResultclear && maxSpotResult.SpotResult != strDiscard)
                        {
                            lstMRR.Add(maxSpotResult);
                        }
                    }


                    if (lstMRR.Any(c => c.SpotResult == strSpotResultRejected))
                    {
                        isNewSpotApplicable = true;

                        #region For inspection Extent < 100

                        //int inspectionExtent1;
                        //if (int.TryParse(objNewQMS060.InspectionExtent.Replace("%", "").Trim(), out inspectionExtent1))
                        //{
                        //    if (inspectionExtent1 < 100)
                        //    {
                        //        lstAllSpot.Clear();
                        //        List<int> spotsNos = new List<int>();
                        //        if (isLTFPSProject)
                        //        {
                        //            spotsNos = db.QMS070.Where(c => c.RequestNo == objQMS060.RequestNo).GroupBy(g => g.SpotNumber.Value).Select(x => x.OrderByDescending(o => o.SpotId).FirstOrDefault().SpotId).ToList();
                        //        }
                        //        else
                        //        {
                        //            spotsNos = db.QMS070.Where(c => c.HeaderId == objQMS060.HeaderId).GroupBy(g => g.SpotNumber.Value).Select(x => x.OrderByDescending(o => o.SpotId).FirstOrDefault().SpotId).ToList();
                        //        }
                        //        lstAllSpot = db.QMS070.Where(c => spotsNos.Contains(c.SpotId)).ToList();
                        //    }
                        //}

                        #endregion



                        //foreach (QMS070 itemSpot in lstAllSpot)
                        //{
                        //    QMS070 maxSpotResult = new QMS070();
                        //    if (isLTFPSProject)
                        //    {
                        //        maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.RequestNo == itemSpot.RequestNo).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                        //    }
                        //    else
                        //    {
                        //        maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.HeaderId == itemSpot.HeaderId).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                        //    }

                        //    if (maxSpotResult.SpotResult == strSpotResultRejected)
                        //    {
                        //        isNewSpotApplicable = true;
                        //        lstMRR.Add(itemSpot);
                        //    }
                        //}

                    }
                    List<QMS070> lstnewQMS070 = new List<QMS070>();
                    foreach (var item in lstMRR)
                    {
                        QMS070 obj = new QMS070()
                        {
                            RequestId = objNewQMS060.RequestId,
                            HeaderId = item.HeaderId,
                            QualityProject = item.QualityProject,
                            Project = item.Project,
                            BU = item.BU,
                            Location = item.Location,
                            SeamNo = item.SeamNo,
                            StageCode = item.StageCode,
                            StageSequence = item.StageSequence,
                            IterationNo = objNewQMS060.IterationNo,
                            RequestNo = item.RequestNo,
                            RequestNoSequence = item.RequestNoSequence,
                            SpotNumber = item.SpotNumber,
                            SpotDescription = item.SpotDescription,
                            SpotLength = item.SpotLength,
                            RepairDesignation = item.RepairDesignation,
                            RepairDesignationValue = (item.SpotResult == strSpotResultRejected ? item.RepairDesignationValue + 1 : item.RepairDesignationValue),
                            CreatedBy = objClsLoginInfo.UserName,
                            CreatedOn = DateTime.Now,
                            LTFPSHeaderId = (reworkLTFPS != null ? reworkLTFPS.LineId : item.LTFPSHeaderId)
                        };

                        lstnewQMS070.Add(obj);
                    };

                    db.QMS070.AddRange(lstnewQMS070);

                    int inspectionExtent;
                    if (int.TryParse(objNewQMS060.InspectionExtent.Replace("%", "").Trim(), out inspectionExtent))
                    {
                        if (inspectionExtent < 100 && isNewSpotApplicable)
                        {
                            objNewQMS060.IsApplicableForNewSpot = isNewSpotApplicable;
                        }
                    }
                    db.SaveChanges();
                    #endregion
                }

                #region Email send for repairdesignation increased.
                try
                {
                    if (!string.IsNullOrWhiteSpace(objQMS001.RepairDesignationEmailTo))
                    {
                        if (isLTFPSProject)
                        {
                            lstAllSpot = db.QMS070.Where(x => x.RequestNo == objQMS060.RequestNo && x.IterationNo == 1).ToList();
                        }
                        else
                        {
                            lstAllSpot = db.QMS070.Where(x => x.HeaderId == objQMS060.HeaderId && x.IterationNo == 1).ToList();
                        }

                        int count = 0;
                        foreach (QMS070 itemSpot in lstAllSpot)
                        {
                            QMS070 maxSpotResult = new QMS070();
                            if (isLTFPSProject)
                            {
                                maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.RequestNo == itemSpot.RequestNo).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                            }
                            else
                            {
                                maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && x.HeaderId == itemSpot.HeaderId).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                            }
                            if (maxSpotResult.RepairDesignationValue == objQMS001.RepairDesignationToSendEmail)
                            {
                                count++;
                            }
                        }
                        if (count > 0)
                        {
                            Hashtable _ht = new Hashtable();
                            EmailSend _objEmail = new EmailSend();
                            _ht["[QualityProject]"] = objQMS060.QualityProject;
                            _ht["[SeamNo]"] = objQMS060.SeamNo;
                            _ht["[StageCode]"] = objQMS060.StageCode;
                            MAIL001 objTemplateMaster = db.MAIL001.Where(ii => ii.TamplateName == MailTemplates.IPI.RepairDesignationWarning).SingleOrDefault();
                            _objEmail.MailToAdd = objQMS001.RepairDesignationEmailTo.Trim().ToLower().Replace(",", ";");
                            _ht["[Subject]"] = objQMS060.QualityProject + "," + objQMS060.SeamNo;
                            _objEmail.SendMail(_objEmail, _ht, objTemplateMaster);
                        }
                    }
                }
                catch (Exception)
                {
                }
                #endregion

                objResponseMsg.Key = true;
                objResponseMsg.Value = "Request Generated Successfully";
            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please fill result for parallel stage(s)";
            }

            var response = new
            {
                Key = objResponseMsg.Key,
                Value = objResponseMsg.Value,
                RequestId = newRequestId
            };

            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public bool? isParallelSpotRejected(int RepairDesignation, int SpotNumber, List<int> parallelId)
        {
            bool? isReject = null;
            foreach (var reqId in parallelId)
            {
                QMS070 objQMS070 = db.QMS070.Where(c => c.SpotNumber == SpotNumber && c.RepairDesignationValue == RepairDesignation && c.HeaderId == reqId).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                if (objQMS070 != null)
                {
                    if (objQMS070.SpotResult == clsImplementationEnum.SpotResults.Rejected.GetStringValue())
                    {
                        isReject = true;
                        break;
                    }
                }
            }
            return isReject;
        }

        [HttpPost]
        public ActionResult UpdateWeldersBySpotId(int spotId, string columnName, string columnValue)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            try
            {
                QMS070 objQMS070 = db.QMS070.Where(i => i.SpotId == spotId).FirstOrDefault();
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder1) && objQMS070.Welder1 == columnValue && columnName != "Welder1")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder1";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder2) && objQMS070.Welder2 == columnValue && columnName != "Welder2")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder2";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder3) && objQMS070.Welder3 == columnValue && columnName != "Welder3")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder3";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder4) && objQMS070.Welder4 == columnValue && columnName != "Welder4")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder4";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder5) && objQMS070.Welder5 == columnValue && columnName != "Welder5")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder5";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder6) && objQMS070.Welder6 == columnValue && columnName != "Welder6")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder6";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder7) && objQMS070.Welder7 == columnValue && columnName != "Welder7")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder7";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder8) && objQMS070.Welder8 == columnValue && columnName != "Welder8")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder8";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder9) && objQMS070.Welder9 == columnValue && columnName != "Welder9")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder9";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder10) && objQMS070.Welder10 == columnValue && columnName != "Welder10")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder10";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder11) && objQMS070.Welder11 == columnValue && columnName != "Welder11")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder11";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder12) && objQMS070.Welder12 == columnValue && columnName != "Welder12")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder12";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder13) && objQMS070.Welder13 == columnValue && columnName != "Welder13")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder13";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder14) && objQMS070.Welder14 == columnValue && columnName != "Welder14")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder14";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder15) && objQMS070.Welder15 == columnValue && columnName != "Welder15")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder15";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder16) && objQMS070.Welder16 == columnValue && columnName != "Welder16")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder16";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder17) && objQMS070.Welder17 == columnValue && columnName != "Welder17")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder17";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder18) && objQMS070.Welder18 == columnValue && columnName != "Welder18")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder18";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder19) && objQMS070.Welder19 == columnValue && columnName != "Welder19")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder19";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder20) && objQMS070.Welder20 == columnValue && columnName != "Welder20")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder20";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder21) && objQMS070.Welder21 == columnValue && columnName != "Welder21")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder21";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder22) && objQMS070.Welder22 == columnValue && columnName != "Welder22")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder22";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder23) && objQMS070.Welder23 == columnValue && columnName != "Welder23")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder23";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }
                    if (!string.IsNullOrWhiteSpace(objQMS070.Welder24) && objQMS070.Welder24 == columnValue && columnName != "Welder24")
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Welder is already assigned in Welder24";
                        return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                    }

                    db.SP_IPI_UPDATE_SPOT_WELDERS(spotId, columnName, columnValue);
                }

                db.SaveChanges();
                objResponseMsg.Key = true;
                objResponseMsg.Value = "Saved Successfully";
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult LoadWelderSpotResultDataTable(JQueryDataTableParamModel param, int requestId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                string whereCondition = "1=1";
                whereCondition += " AND  qms070.RequestId = " + requestId;

                var lstStages = db.SP_FETCH_IPI_SPOT_RESULT(startIndex, endIndex, "", whereCondition).ToList();
                int? totalRecords = lstStages.Select(i => i.TotalCount).FirstOrDefault();
                QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
                string strSpotResultRejected = clsImplementationEnum.SpotResults.Rejected.GetStringValue();

                var data = (from uc in lstStages
                            select new[]
                            {
                            Convert.ToString(uc.SpotNumber),
                            Convert.ToString(uc.SpotId),
                            Convert.ToString(uc.RequestId),
                            Convert.ToString(uc.SpotDescription),
                            Convert.ToString(uc.SpotResult),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder1",uc.Welder1, "UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder1", false,"")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder1", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder1) ,
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder2",uc.Welder2,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder2", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder2", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder2) ,
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder3",uc.Welder3,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder3", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder3", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder3) ,
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder4",uc.Welder4,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder4", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder4", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder4) ,
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder5",uc.Welder5,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder5", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder5", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder5) ,
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder6",uc.Welder6,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder6", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder6", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder6) ,
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder7",uc.Welder7,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder7", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder7", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder7) ,
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder8",uc.Welder8,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder8", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder8", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder8) ,
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder9",uc.Welder9,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder9", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder9", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder9) ,
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder10",uc.Welder10,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder10", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder10", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder10),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder11",uc.Welder11,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder11", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder11", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder11),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder12",uc.Welder12,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder12", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder12", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder12),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder13",uc.Welder13,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder13", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder13", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder13),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder14",uc.Welder14,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder14", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder14", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder14),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder15",uc.Welder15,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder15", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder15", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder15),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder16",uc.Welder16,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder16", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder16", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder16),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder17",uc.Welder17,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder17", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder17", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder17),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder18",uc.Welder18,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder18", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder18", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder18),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder19",uc.Welder19,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder19", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder19", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder19),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder20",uc.Welder20,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder20", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder20", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder20),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder21",uc.Welder21,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder21", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder21", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder21),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder22",uc.Welder22,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder22", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder22", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder22),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder23",uc.Welder23,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder23", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder23", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder23),
                            IsWelderEditable(objQMS060,uc.SpotResult) ? GenerateAutoComplete(uc.SpotId, "Welder24",uc.Welder24,"UpdateWelders(this, "+ uc.SpotId +");", "",false,"","Welder24", true, "")+""+Helper.GenerateHidden(uc.SpotId, "hdnWelder24", Convert.ToString(uc.SpotId)) : Convert.ToString(uc.Welder24)
                           }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool IsWelderEditable(QMS060 objQMS060, string spotResult)
        {
            if (objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() && objQMS060.IsPartiallyOffered == false && spotResult == clsImplementationEnum.SpotResults.Rejected.GetStringValue())
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string GenerateAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", string onClickMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false, string Validate = "")
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick=" + onClickMethod + "" : "";
            string onValidate = !string.IsNullOrEmpty(Validate) ? "validate=" + Validate + "" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' data-lineid='" + rowId + "' hdElement='" + hdElement + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + onClickEvent + " " + onValidate + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        [HttpPost]
        public ActionResult LoadSpotResultforShopDataPartial(string status, int requestId)
        {
            var lstSpotResult = clsImplementationEnum.getSeamSpotResultType();
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
            ViewBag.welders = db.QMS061.Where(x => x.RequestNo == objQMS060.RequestNo).Select(x => new CategoryData { Code = x.welderstamp, CategoryDescription = x.welderstamp }).Distinct().ToList();
            ViewBag.SpotResults = lstSpotResult.AsEnumerable().Select(x => new CategoryData { Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            ViewBag.TypeOfIndication = Manager.GetSubCatagories("Indication Type", objQMS060.BU, objQMS060.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            ViewBag.isApplicableForNewSpot = false;

            ViewBag.id = requestId;
            if (status == "SpotResult")
                return PartialView("_LoadSpotResultDataForShopPartial", objQMS060);
            else
                return PartialView("_LoadWeldersDataForShopPartial");
        }

        [HttpPost]
        public ActionResult LoadSpotResultDataForShopTable(JQueryDataTableParamModel param, int requestId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
                bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
                var items = Manager.GetSubCatagories("Indication Type", objQMS060.BU, objQMS060.Location).Select(i => new SelectItemList { id = i.Code, text = i.Description }).ToList();
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                string whereCondition = "1=1";
                whereCondition += " AND  qms070.RequestId = " + requestId;
                var lstStages = db.SP_FETCH_IPI_SPOT_RESULT(startIndex, endIndex, "", whereCondition).ToList();
                int? totalRecords = lstStages.Select(i => i.TotalCount).FirstOrDefault();
                var data = (from uc in lstStages
                            select new[]
                            {        Convert.ToString(uc.SpotId),
                                     Convert.ToString(uc.SpotNumber),
                                     Convert.ToString(uc.SpotId),
                                     Convert.ToString(uc.RequestId),
                                     Convert.ToString(uc.SpotDescription),
                                     Convert.ToString(uc.SpotLength),
                                     Convert.ToString(uc.SpotResult),
                                     Convert.ToString(uc.IsRejected==true?"Yes":"No"),
                                     uc.RepairDesignation+Convert.ToString(uc.RepairDesignationValue),
                                     Convert.ToString(uc.DefectLength),
                                     Convert.ToString(uc.DefectWidth),
                                     Convert.ToString(uc.TypeofIndication),
                                     Convert.ToString(uc.Reason),
                                     Convert.ToString(uc.InspectionRemarks),
                            }).ToList();

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        #endregion

        #region NDE
        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult NDEView()
        {
            return View();
        }
        public ActionResult GetNDESeamTestDataPartial(string status)
        {
            ViewBag.Status = status;
            return PartialView("_GetNDESeamTestDataPartial");
        }
        public ActionResult loadNDESeamTestDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                if (status.ToUpper() == "PENDING")
                {
                    whereCondition += " and ConfirmedBy IS NOT NULL and RequestStatus='Not Attended' and ISNULL(QCIsReturned,0)=0 and ISNULL(NDEIsReturned,0)=0 ";
                }
                if (status.ToUpper() == "TPI")
                {
                    whereCondition += " and TPIResultPending=1 and RequestStatus='Attended' ";
                }
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "qms.SeamNo", "qms.InspectedOn", "qms040.InspectionStatus", "qms.QualityProject", "qms.Project", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms.StageCode,qms.BU,qms.Location)", "qms002.StageType", "qms.RequestStatus", "qms.RequestNo", "qms.ManufacturingCode", "qms.AcceptanceStandard", "qms.ApplicableSpecification", "qms.InspectionExtent", "qms.WeldingEngineeringRemarks", "qms.WeldingProcess", "qms.SeamStatus", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark", "qms.SurfaceCondition", "qms.WeldThicknessReinforcement", "qms.SpotGenerationtype", "qms.PrintSummary", "qms.NDETechniqueNo", "qms.NDETechniqueRevisionNo" };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                else
                {
                    whereCondition += Manager.MakeDatatableForSearch(param.SearchFilter);
                }
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = string.Empty;
                sortDirection = Convert.ToString(Request["sSortDir_0"]); // asc or desc
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_IPI_SEAMLISTNDETEST_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           h.QualityProject,
                           Convert.ToString(h.RequestId),
                           Convert.ToString(h.HeaderId),
                           Convert.ToString( h.Project),
                           Convert.ToString( h.SeamNo),
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageType),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString(h.RequestNo),
                           Convert.ToString(h.RequestNoSequence),
                           h.InspectedOn==null || h.InspectedOn.Value==DateTime.MinValue?"":h.InspectedOn.Value.ToString("dd/MM/yyyy" ,CultureInfo.InvariantCulture),
                           Convert.ToString(h.InspectionStatus),
                           Convert.ToString(h.RequestStatus),
                           Convert.ToString(h.SeamLength),
                           Convert.ToString(h.OverlayArea),
                           Convert.ToString(h.OverlayThickness),
                           Convert.ToString(h.Jointtype),
                           Convert.ToString(h.ManufacturingCode),
                           Convert.ToString(h.Part1Position),
                           Convert.ToString(h.Part1thickness),
                           Convert.ToString(h.Part1Material),
                           Convert.ToString(h.Part2Position),
                           Convert.ToString(h.Part2thickness),
                           Convert.ToString(h.Part2Material),
                           Convert.ToString(h.Part3Position),
                           Convert.ToString(h.Part3thickness),
                           Convert.ToString(h.Part3Material),
                           Convert.ToString(h.AcceptanceStandard),
                           Convert.ToString(h.ApplicableSpecification),
                           Convert.ToString(h.InspectionExtent),
                           Convert.ToString(h.WeldingEngineeringRemarks),
                           Convert.ToString(h.WeldingProcess),
                           Convert.ToString(h.SeamStatus),
                           Convert.ToString(h.Shop),
                           Convert.ToString(h.ShopLocation),
                           Convert.ToString(h.ShopRemark),
                           Convert.ToString(h.SurfaceCondition),
                           Convert.ToString(h.WeldThicknessReinforcement),
                           Convert.ToString(h.SpotGenerationtype),
                           Convert.ToString(h.SpotLength),
                           Convert.ToString(h.PrintSummary),
                           Convert.ToString( h.CreatedBy),
                           Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.RequestGeneratedBy),
                           Convert.ToDateTime(h.RequestGeneratedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.ConfirmedBy),
                           Convert.ToDateTime(h.ConfirmedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.NDETechniqueNo),
                           Convert.ToString( h.NDETechniqueRevisionNo),
                           "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a target=\"_blank\" href=\"" + WebsiteURL + "/IPI/SeamListNDEInspection/NDESeamTestDetails/"+h.RequestId+"\" Title=\"View Details\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a>&nbsp;"
                           + ( h.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.NotAttended.GetStringValue() ?  "<a title=\"Return\"  aria-hidden=\"true\" style=\"cursor:pointer;\" onclick=\"ReturnRemark("+h.RequestId+")\"><i style=\"margin - left:5px;\" class=\"fa fa-reply-all\"></i></a></a>&nbsp;" : "<a title=\"Return\"  aria-hidden=\"true\" style=\"cursor:pointer;\" )\"><i style=\"margin - left:5px; opacity:0.3\" class=\"fa fa-reply-all\"></i></a></a>&nbsp;")
                           + GenerateReportIcon(h.RequestId) +Manager.generateIPIGridButtons(h.QualityProject,h.Project,h.BU,h.Location,h.SeamNo,"","",h.StageCode,Convert.ToString(h.StageSequence),"Seam Details",60,"true","Seam Details","Seam Request Details")+"&nbsp;&nbsp;<a  href=\"javascript: void(0);\" Title=\"View Timeline\" onclick=\"ShowTimeline("+ h.RequestId +")\"><i style=\"margin - left:5px;\" class=\"fa fa-clock-o\"></i></a></center>"

                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public string GenerateReportIcon(int RequestId)
        {
            string ActionHTML = string.Empty;
            string ReportType = string.Empty;

            QMS060 objQMS060 = db.QMS060.FirstOrDefault(c => c.RequestId == RequestId);
            if (objQMS060 != null && !string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) && objQMS060.NDETechniqueRevisionNo.HasValue)
            {
                QMS002 objQMS002 = db.QMS002.FirstOrDefault(c => c.BU == objQMS060.BU && c.Location == objQMS060.Location && c.StageCode == objQMS060.StageCode);
                if (objQMS002 != null)
                {
                    if (objQMS002.StageType == "RT" || objQMS002.StageType == "UT")
                    {
                        if (objQMS002.StageType == "RT")
                        {
                            ReportType = "RT";
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS060.NDETechniqueNo)))
                                ReportType = objQMS060.NDETechniqueNo.Split('/')[1].ToString();
                        }
                        if (!string.IsNullOrWhiteSpace(ReportType))
                            ActionHTML = "onclick=\"OpenNDEReport('" + ReportType + "','" + objQMS060.QualityProject + "','" + objQMS002.StageType + "','" + objQMS060.SeamNo.Replace("\\", "\\\\") + "','" + objQMS060.StageCode + "','" + objQMS060.StageSequence + "','" + objQMS060.IterationNo + "','" + objQMS060.RequestNo + "')\"";
                    }
                }
            }

            string NDEDetailsReport = "<a onclick=\"OpenNDERequestDetails(" + RequestId + ")\" Title=\"Request Details\" ><i style=\"margin-left:5px;\" class=\"fa fa-newspaper-o\"></i></a>";

            if (!string.IsNullOrWhiteSpace(ActionHTML))
            {
                return "<a " + ActionHTML + " Title=\"View Report\" ><i style=\"margin-left:5px;\" class=\"fa fa-print\"></i></a>&nbsp;" + NDEDetailsReport;
            }
            else
            {
                return "<a Title=\"Report Not Available\" ><i style=\"cursor: pointer; opacity:0.3; margin-left:5px;\" class=\"fa fa-print\"></i></a>&nbsp;" + NDEDetailsReport;
            }

        }

        public string GenerateReportButton(int RequestId)
        {
            string ActionHTML = string.Empty;
            string ReportType = string.Empty;

            QMS060 objQMS060 = db.QMS060.FirstOrDefault(c => c.RequestId == RequestId);
            if (objQMS060 != null && !string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) && objQMS060.NDETechniqueRevisionNo.HasValue)
            {
                QMS002 objQMS002 = db.QMS002.FirstOrDefault(c => c.BU == objQMS060.BU && c.Location == objQMS060.Location && c.StageCode == objQMS060.StageCode);
                if (objQMS002 != null)
                {
                    bool isLTFPS = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
                    if (objQMS002.StageType == "RT" || objQMS002.StageType == "UT")
                    {
                        if (objQMS002.StageType == "RT")
                        {
                            ReportType = "RT";
                        }
                        else
                        {
                            if (!string.IsNullOrWhiteSpace(Convert.ToString(objQMS060.NDETechniqueNo)))
                                ReportType = objQMS060.NDETechniqueNo.Split('/')[1].ToString();
                        }
                        if (!string.IsNullOrWhiteSpace(ReportType))
                        {
                            if (isLTFPS)
                            {
                                ActionHTML = "onclick=\"OpenNDEReportForLTFPS('" + ReportType + "','" + objQMS060.QualityProject + "','" + objQMS002.StageType + "','" + objQMS060.SeamNo.Replace("\\", "\\\\") + "','" + objQMS060.StageCode + "','" + objQMS060.StageSequence + "','" + objQMS060.IterationNo + "','" + objQMS060.RequestNo + "')\"";
                            }
                            else
                            {
                                ActionHTML = "onclick=\"OpenNDEReport('" + ReportType + "','" + objQMS060.QualityProject + "','" + objQMS002.StageType + "','" + objQMS060.SeamNo.Replace("\\", "\\\\") + "','" + objQMS060.StageCode + "','" + objQMS060.StageSequence + "','" + objQMS060.IterationNo + "','" + objQMS060.RequestNo + "')\"";
                            }
                        }
                    }
                }
            }

            if (!string.IsNullOrWhiteSpace(ActionHTML))
            {
                return "<input " + ActionHTML + " type=\"button\" class=\"btn return \" id=\"btnReport\" value=\"Report\">";
            }

            return "";

        }

        public ActionResult MaintainTechValidForNonParalleStg(int requestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            objResponseMsg.Key = true;
            decimal seamLength = 0;
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);

            if (objQMS060.IterationNo == 1)
            {
                if (objQMS060.IsOverlay == true)
                {
                    seamLength = Convert.ToDecimal(objQMS060.OverlayArea);
                }
                else
                {
                    seamLength = Convert.ToDecimal(objQMS060.SeamLength);
                }
                int inspectionExtent = Convert.ToInt32(objQMS060.InspectionExtent.Split('%')[0].Trim());
                decimal requiredSpotLength = (seamLength * inspectionExtent) / 100;
                int totalSpotLength = 0;
                int? tSpotLength = db.QMS070.Where(x => x.RequestId == requestId).Sum(s => s.SpotLength);
                if (tSpotLength != null && tSpotLength != 0)
                {
                    totalSpotLength = Convert.ToInt32(tSpotLength);
                }
                if (totalSpotLength < requiredSpotLength)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Total spot length should be greater or equal to " + requiredSpotLength + " (mm)";
                }
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult UpdateSpotLengthandSpotType(int requestId, string spotLength, string spotType)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
                if (!string.IsNullOrWhiteSpace(spotLength))
                {
                    objQMS060.SpotLength = Convert.ToInt32(spotLength);
                    objResponseMsg.Value = "Spot Length Saved Successfully";
                }
                if (!string.IsNullOrWhiteSpace(spotType))
                {
                    objQMS060.SpotGenerationtype = spotType;
                    objResponseMsg.Value = "Spot Type Saved Successfully";
                }
                db.SaveChanges();
                objResponseMsg.Key = true;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                objResponseMsg.Key = false;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }

        }



        [SessionExpireFilter]
        [UserPermissions]
        public ActionResult NDESeamTestDetails(int id)
        {
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == id);

            #region Check Return Applicable or not for this request
            ViewBag.isReturnApplicable = IsApplicableForReturnRequest(objQMS060);
            #endregion

            ViewBag.IsLTFPS = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
            ViewBag.SurfaceCondition = GetCategoryDescription("Surface Condition", objQMS060.SurfaceCondition, objQMS060.BU, objQMS060.Location).CategoryDescription;
            int InspectionExtentPerc = Convert.ToInt32(objQMS060.InspectionExtent.Split('%')[0].Trim());
            ViewBag.AllowSpotLengthSave = "No";
            if (InspectionExtentPerc < 100)
            {
                ViewBag.AllowSpotLengthSave = "Yes";
            }
            var objQMS002 = db.QMS002.Where(i => i.StageCode == objQMS060.StageCode && i.BU == objQMS060.BU && i.Location == objQMS060.Location).FirstOrDefault();
            if (objQMS002 != null)
            {
                ViewBag.StageType = objQMS002.StageType;
                bool isUpdate = false;
                if (string.IsNullOrWhiteSpace(objQMS060.NDEExecution))
                {
                    objQMS060.NDEExecution = "Single";
                    if (string.IsNullOrWhiteSpace(objQMS060.NDEActual) && (objQMS002.StageType == "PT" || objQMS002.StageType == "MT"))
                    {
                        objQMS060.NDEActual = objQMS002.StageType;
                    }
                    isUpdate = true;
                }
                if (string.IsNullOrWhiteSpace(objQMS060.ExaminationBy))
                {
                    var obj = db.QMS060.Where(x => x.InspectedBy == objClsLoginInfo.UserName).OrderByDescending(x => x.InspectedOn).Select(x => x.ExaminationBy).FirstOrDefault();
                    objQMS060.ExaminationBy = obj;
                    isUpdate = true;
                }
                if (isUpdate)
                {
                    db.SaveChanges();
                }
            }
            objQMS060.Project = Manager.GetProjectAndDescription(objQMS060.Project);
            ViewBag.StageCode = objQMS060.StageCode;
            objQMS060.StageCode = db.Database.SqlQuery<string>("SELECT dbo.GET_IPI_STAGECODE_DESCRIPTION('" + objQMS060.StageCode + "','" + objQMS060.BU + "','" + objQMS060.Location + "')").FirstOrDefault();
            objQMS060.SeamNo = Manager.ReplaceIllegalCharForSeamNo(db.QMS012.Where(x => x.QualityProject == objQMS060.QualityProject && x.SeamNo == objQMS060.SeamNo).Select(x => x.SeamNo).FirstOrDefault());

            List<GLB002> lstCategory = Manager.GetSubCatagories("TPI Agency", objQMS060.BU, objQMS060.Location, null);
            List<GLB002> lstCategoryTPI = Manager.GetSubCatagories("TPI Intervention", objQMS060.BU, objQMS060.Location, null);
            objQMS060.TPIAgency1 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency1);
            objQMS060.TPIAgency2 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency2);
            objQMS060.TPIAgency3 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency3);
            objQMS060.TPIAgency4 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency4);
            objQMS060.TPIAgency5 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency5);
            objQMS060.TPIAgency6 = GetCategoryDescriptionFromList(lstCategory, objQMS060.TPIAgency6);
            objQMS060.TPIAgency1Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency1Intervention);
            objQMS060.TPIAgency2Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency2Intervention);
            objQMS060.TPIAgency3Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency3Intervention);
            objQMS060.TPIAgency4Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency4Intervention);
            objQMS060.TPIAgency5Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency5Intervention);
            objQMS060.TPIAgency6Intervention = GetCategoryDescriptionFromList(lstCategoryTPI, objQMS060.TPIAgency6Intervention);
            objQMS060.Location = db.COM002.Where(x => x.t_dimx == objQMS060.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS060.BU = db.COM002.Where(x => x.t_dimx == objQMS060.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS060.CreatedBy = Manager.GetPsidandDescription(objQMS060.CreatedBy);
            objQMS060.RequestGeneratedBy = Manager.GetPsidandDescription(objQMS060.RequestGeneratedBy);
            objQMS060.OfferedBy = Manager.GetPsidandDescription(objQMS060.OfferedBy);
            objQMS060.ConfirmedBy = Manager.GetPsidandDescription(objQMS060.CreatedBy);
            #region check for spot result exists for parallel stage 

            bool isSpotGenerateEditable = true;
            if ((new clsManager()).CheckSpotResultExistsForParallelStages(objQMS060.RequestId))
            {
                isSpotGenerateEditable = false;
            }
            ViewBag.isSpotGenerateEditable = isSpotGenerateEditable;

            #endregion

            ViewBag.YesNo = clsImplementationEnum.getyesno().ToArray();


            if (!string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo))
            {
                string techName = objQMS060.NDETechniqueNo.Split('/')[1].ToString();
                string strURL = string.Empty;
                switch (techName.ToUpper())
                {
                    case "ASCAN":
                        QMS080 objQMS080 = db.QMS080.Where(x => x.RefHeaderId == objQMS060.RequestId).FirstOrDefault();
                        strURL = WebsiteURL + "/IPI/MaintainNDESeamTechnique/AscanUTHeader?id=" + Convert.ToInt32(objQMS080.HeaderId) + "";
                        ViewBag.strURL = strURL;
                        break;
                    case "TOFD":
                        QMS082 objQMS082 = db.QMS082.Where(x => x.RefHeaderId == objQMS060.RequestId).FirstOrDefault();
                        strURL = WebsiteURL + "/IPI/MaintainNDESeamTechnique/TOFDUTHeader?id=" + Convert.ToInt32(objQMS082.HeaderId) + "";
                        ViewBag.strURL = strURL;
                        break;
                    case "PAUT":
                        QMS084 objQMS084 = db.QMS084.Where(x => x.RefHeaderId == objQMS060.RequestId).FirstOrDefault();
                        strURL = WebsiteURL + "/IPI/MaintainNDESeamTechnique/PAUTHeader?id=" + Convert.ToInt32(objQMS084.HeaderId) + "";
                        ViewBag.strURL = strURL;
                        break;
                    default:
                        break;
                }
            }

            if (objQMS060.SeamStatus == clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue())
            {
                objQMS060.SeamStatus = "Yes";
            }
            else
            {
                if (objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue())
                {
                    objQMS060.SeamStatus = "No";
                }
                else
                {
                    objQMS060.SeamStatus = "";
                }
            }
            if (ViewBag.IsLTFPS)
            {
                ViewBag.InspectionStatus = objQMS060.LTF002.InspectionStatus;
            }
            else
            {
                ViewBag.InspectionStatus = objQMS060.QMS040.InspectionStatus;
            }
            return View(objQMS060);
        }
        [HttpPost]
        public JsonResult GenerateSpot(int id, int spotLength, string spotGenerationType)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var lst = db.SP_IPI_GENERATE_SPOT_RESULT(id, spotLength, spotGenerationType, objClsLoginInfo.UserName);

                objResponseMsg.Key = true;
                objResponseMsg.Value = "" + lst.Count() + " Spot(s) Generated Successfully";

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error Occured, Please try again";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public ActionResult SportGenerationPartial()
        {
            return PartialView("_SportGenerationPartial");
        }

        [HttpPost]
        public ActionResult LoadSpotResultDataPartial(string status, int requestId)
        {
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);

            string StageType = (new clsManager()).GetStageType(objQMS060.BU.Trim(), objQMS060.Location.Trim(), objQMS060.StageCode.Trim());
            var lstSpotResult = clsImplementationEnum.getSeamSpotResultType();
            if (StageType.ToUpper() == "UT")
            {
                lstSpotResult.Remove("Re-Take");
                lstSpotResult.Remove("Reshoot");
                lstSpotResult.Remove("Miscellaneous");
            }
            else if (StageType.ToUpper() == "PT" || StageType.ToUpper() == "MT")
            {
                lstSpotResult.Remove("Re-Scan");
                lstSpotResult.Remove("Re-Take");
                lstSpotResult.Remove("Reshoot");
                lstSpotResult.Remove("Miscellaneous");
            }
            else if (StageType.ToUpper() == "RT")
            {
                lstSpotResult.Remove("Re-Scan");
            }

            ViewBag.SpotResults = lstSpotResult.AsEnumerable().Select(x => new CategoryData { Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
            ViewBag.TypeOfIndication = Manager.GetSubCatagories("Indication Type", objQMS060.BU, objQMS060.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Code + "-" + i.Description }).ToList();

            bool isApplicableForNewSpotAdded = false;
            int inspectionExtent;
            string inspExtent = objQMS060.InspectionExtent.Replace("%", "").Trim();
            if (int.TryParse(inspExtent, out inspectionExtent))
            {
                if ((inspectionExtent < 100 && string.IsNullOrEmpty(objQMS060.NDETechniqueNo) && objQMS060.IsApplicableForNewSpot != null && objQMS060.IsApplicableForNewSpot.Value))
                {
                    isApplicableForNewSpotAdded = true;
                }
            }
            ViewBag.isApplicableForNewSpot = isApplicableForNewSpotAdded;
            ViewBag.TPIResultPending = Convert.ToBoolean(objQMS060.TPIResultPending);
            ViewBag.id = requestId;
            if (status == "SpotResult")
                return PartialView("_LoadSpotResultDataPartial");
            else if (status == "TPI")
                return PartialView("_LoadSpotResultForTPIPartial");
            else
                return PartialView("_LoadWeldersDataPartial");
        }
        [HttpPost]
        public ActionResult loadGeneratedSpotDataTable(JQueryDataTableParamModel param)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                var lstSpotDetails = (List<SpotDetails>)Session["SpotDetails"];
                int? totalRecords = lstSpotDetails.Count();
                var res = from h in lstSpotDetails
                          select new[] {
                           Convert.ToString(h.SpotNumber),
                           Convert.ToString(h.SpotDescription),
                           Convert.ToString( h.SpotLength),
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
            finally
            {
                Session["SpotDetails"] = null;
            }
        }


        public ActionResult IsSpotEditable(int requestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
            objResponseMsg.Key = true;
            if (objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo))
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult LoadSpotResultDataTable(JQueryDataTableParamModel param, int requestId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
                bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
                var items = Manager.GetSubCatagories("Indication Type", objQMS060.BU, objQMS060.Location).Select(i => new SelectItemList { id = i.Code, text = i.Description }).ToList();
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                string whereCondition = "1=1";
                whereCondition += " AND  qms070.RequestId = " + requestId;

                var lstStages = db.SP_FETCH_IPI_SPOT_RESULT(startIndex, endIndex, "", whereCondition).ToList();
                int? totalRecords = lstStages.Select(i => i.TotalCount).FirstOrDefault();
                bool isSpotEditable = true;
                if (objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo))
                {
                    isSpotEditable = false;
                }
                ViewBag.isSpotEditable = isSpotEditable;

                bool isSpotDescEditable = true;
                List<int> parallelRequestIds = GetParallelRequestIds(objQMS060.RequestId);
                foreach (int pRequestId in parallelRequestIds)
                {
                    QMS060 objParallelQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == pRequestId);
                    if (objParallelQMS060 != null && objParallelQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue())
                    {
                        isSpotDescEditable = false;
                        break;
                    }
                }


                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateTextbox(newRecordId, "SpotNumber", "", "",false, "", "4"),
                                    Helper.GenerateTextbox(newRecordId, "SpotId", Convert.ToString(newRecordId), "", true),
                                    Helper.GenerateTextbox(newRecordId, "RequestId",Convert.ToString(requestId),"",true),
                                    Helper.GenerateTextbox(newRecordId, "SpotDescription", "", "", false, "", "10"),
                                    Helper.GenerateTextbox(newRecordId, "SpotLength", "", "", false, "", "8"),
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    Helper.GenerateGridButtonNew(newRecordId, "Save", "Add New Spot", "fa fa-save", "SaveNewSpot("+ Convert.ToString(isLTFPSProject?objQMS060.LTFPSHeaderId:objQMS060.HeaderId) +", "+ Convert.ToString(requestId) +");" ),
                                };

                var data = (from uc in lstStages
                            select new[]
                            {
                            objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) ?   Convert.ToString(uc.SpotNumber) +""+Helper.GenerateHidden(uc.SpotId, "SpotNumber", Convert.ToString(uc.SpotNumber)):   Helper.GenerateTextbox(uc.SpotId, "SpotNumber",Convert.ToString(uc.SpotNumber),"",true),
                            Helper.GenerateTextbox(uc.SpotId, "SpotId",Convert.ToString(uc.SpotId),"",true),
                            Helper.GenerateTextbox(uc.RequestId, "RequestId",Convert.ToString(uc.RequestId),"",true),
                            objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) /*|| string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo)*/ || objQMS060.IterationNo > 1 || !isSpotDescEditable ? Convert.ToString(uc.SpotDescription)+""+Helper.GenerateHidden(uc.SpotId, "SpotDescription",Convert.ToString(uc.SpotDescription)) : Helper.GenerateTextbox(uc.SpotId, "SpotDescription",Convert.ToString(uc.SpotDescription),"",false,"","10","",false,"OnChangeSpotDescription("+ uc.SpotId +", this)"),
                            objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) ? Convert.ToString(uc.SpotLength)+""+Helper.GenerateHidden(uc.SpotId, "SpotLength",Convert.ToString(uc.SpotLength)): Helper.GenerateTextbox(uc.SpotId, "SpotLength",Convert.ToString(uc.SpotLength),"",true),
                            objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) ? Convert.ToString(uc.SpotResult)+""+Helper.GenerateHidden(uc.SpotId, "SpotResult", Convert.ToString(uc.SpotResult)): Helper.GenerateTextbox(uc.SpotId, "SpotResult", Convert.ToString(uc.SpotResult)),
                            Convert.ToString(uc.IsRejected==true?"Yes":"No"),
                            uc.RepairDesignation+Convert.ToString(uc.RepairDesignationValue),
                            (objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() && objQMS060.TPIResultPending != true) || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) ? Convert.ToString(uc.DefectLength) : Helper.GenerateTextbox(uc.SpotId, "DefectLength", Convert.ToString(uc.DefectLength),"",IsTextBoxEnable(uc.SpotResult,"DefectLength")),
                            (objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() && objQMS060.TPIResultPending != true) || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) || objQMS060.IsOverlay != true ? Convert.ToString(uc.DefectWidth) : Helper.GenerateTextbox(uc.SpotId, "DefectWidth", Convert.ToString(uc.DefectWidth)),
                            (objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() && objQMS060.TPIResultPending != true) || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) ? Convert.ToString(uc.TypeofIndication)  : MultiSelectDropdown(uc.SpotId,items,uc.TypeofIndicationCode,false,"","TypeofIndication"),
                            //objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) ? Convert.ToString(uc.TypeofIndication)  : Helper.GenerateTextbox(uc.SpotId, "TypeOfIndication", Convert.ToString(uc.TypeofIndication)),
                            (objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() && objQMS060.TPIResultPending != true) || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) ? Convert.ToString(uc.Reason) : Helper.GenerateTextbox(uc.SpotId, "Reason", Convert.ToString(uc.Reason),"",IsTextBoxEnable(uc.SpotResult,"Reason")),
                            (objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() && objQMS060.TPIResultPending != true) || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) ? Convert.ToString(uc.InspectionRemarks) : Helper.GenerateTextbox(uc.SpotId, "remarks", Convert.ToString(uc.InspectionRemarks)),
                            "<center style=\"white-space: nowrap;\" class=\"clsDisplayInlineEle\"><a    href=\"javascript: void(0);\"  onclick=\"MaitainSpotParameters('" + uc.SpotId + "','" + uc.RequestId + "')\"   Title=\"Main Parameter\" ><i style=\"margin - left:5px;\" class=\"fa fa-eye\"></i></a></center>",
                            string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) && isSpotApplicableForDelete(isLTFPSProject?objQMS060.LTFPSHeaderId.Value : uc.HeaderId.Value, uc.SpotNumber.Value,isLTFPSProject?true : false)  ? HTMLActionString(uc.SpotId, "", "Delete","Delete Spot","fa fa-trash-o","DeleteSpot("+ uc.SpotId +");") : ""
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public bool IsTextBoxEnable(string spotresult, string columnId)
        {
            bool flg = true;
            if (columnId == "DefectLength" && spotresult == clsImplementationEnum.SpotResults.Rejected.GetStringValue())
            { flg = false; }
            if (columnId == "Reason" && (spotresult != clsImplementationEnum.SpotResults.Rejected.GetStringValue() && spotresult != clsImplementationEnum.SpotResults.Cleared.GetStringValue() && spotresult != clsImplementationEnum.SpotResults.Discard.GetStringValue()))
            { flg = false; }
            return flg;
        }
        #region HardikMasalawala OBS: 30731
        [HttpPost]
        public ActionResult LoadTPIDataTable(JQueryDataTableParamModel param, int requestId)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
                bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
                var items = Manager.GetSubCatagories("Indication Type", objQMS060.BU, objQMS060.Location).Select(i => new SelectItemList { id = i.Code, text = i.Description }).ToList();
                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                string whereCondition = "1=1";
                whereCondition += " AND  qms070.RequestId = " + requestId;

                var lstStages = db.SP_FETCH_IPI_SPOT_RESULT(startIndex, endIndex, "", whereCondition).ToList();
                int? totalRecords = lstStages.Select(i => i.TotalCount).FirstOrDefault();
                bool isSpotEditable = true;
                if (objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo))
                {
                    isSpotEditable = false;
                }
                bool isEnableTPI = false;
                if (objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() && objQMS060.TPIResultPending == true)
                {
                    isEnableTPI = true;
                }
                ViewBag.isSpotEditable = isSpotEditable;

                bool isSpotDescEditable = true;
                List<int> parallelRequestIds = GetParallelRequestIds(objQMS060.RequestId);
                foreach (int pRequestId in parallelRequestIds)
                {
                    QMS060 objParallelQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == pRequestId);
                    if (objParallelQMS060 != null && objParallelQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue())
                    {
                        isSpotDescEditable = false;
                        break;
                    }
                }


                int newRecordId = 0;
                var newRecord = new[] {
                                    Helper.GenerateTextbox(newRecordId, "SpotNumber", "", "",false, "", "4"),
                                    Helper.GenerateTextbox(newRecordId, "SpotId", Convert.ToString(newRecordId), "", true),
                                    Helper.GenerateTextbox(newRecordId, "RequestId",Convert.ToString(requestId),"",true),
                                    Helper.GenerateTextbox(newRecordId, "SpotDescription", "", "", false, "", "10"),
                                    Helper.GenerateTextbox(newRecordId, "SpotLength", "", "", false, "", "8"),
                                    "",
                                    "","","","","","",""
                                    //Helper.GenerateGridButtonNew(newRecordId, "Save", "Add New Spot", "fa fa-save", "SaveNewSpot("+ Convert.ToString(isLTFPSProject?objQMS060.LTFPSHeaderId:objQMS060.HeaderId) +", "+ Convert.ToString(requestId) +");" ),
                                };

                var data = (from uc in lstStages
                            select new[]
                            {
                            objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) ?   Convert.ToString(uc.SpotNumber) :   Helper.GenerateTextbox(uc.SpotId, "SpotNumber",Convert.ToString(uc.SpotNumber),"",true),
                            Helper.GenerateTextbox(uc.SpotId, "SpotId",Convert.ToString(uc.SpotId),"",true),
                            Helper.GenerateTextbox(uc.RequestId, "RequestId",Convert.ToString(uc.RequestId),"",true),
                            objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) /*|| string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo)*/ || objQMS060.IterationNo > 1 || !isSpotDescEditable ? Convert.ToString(uc.SpotDescription) : Helper.GenerateTextbox(uc.SpotId, "SpotDescription",Convert.ToString(uc.SpotDescription),"",true,"","10","",false,"OnChangeSpotDescription("+ uc.SpotId +", this)"),
                            objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) ? Convert.ToString(uc.SpotLength) : Helper.GenerateTextbox(uc.SpotId, "SpotLength",Convert.ToString(uc.SpotLength),"",true),
                            objQMS060.RequestStatus== clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue() || string.IsNullOrWhiteSpace(objQMS060.ConfirmedBy) || string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) ? Convert.ToString(uc.LNTSpotResult) : Helper.GenerateTextbox(uc.SpotId, "SpotResult", Convert.ToString(uc.LNTSpotResult),isReadOnly: true),
                            /*TPI RESULT START*/
                            !isEnableTPI ? Convert.ToString(uc.TPI1SpotResult) : Helper.GenerateTextbox(uc.SpotId, "TPIResult1", Convert.ToString(uc.TPI1SpotResult),onchange : "OnChangeTPIResult("+ uc.SpotId +", this)"),
                            !isEnableTPI ? Convert.ToString(uc.TPI2SpotResult) : Helper.GenerateTextbox(uc.SpotId, "TPIResult2", Convert.ToString(uc.TPI2SpotResult),onchange : "OnChangeTPIResult("+ uc.SpotId +", this)"),
                            !isEnableTPI ? Convert.ToString(uc.TPI3SpotResult) : Helper.GenerateTextbox(uc.SpotId, "TPIResult3", Convert.ToString(uc.TPI3SpotResult),onchange : "OnChangeTPIResult("+ uc.SpotId +", this)"),
                            !isEnableTPI ? Convert.ToString(uc.TPI4SpotResult) : Helper.GenerateTextbox(uc.SpotId, "TPIResult4", Convert.ToString(uc.TPI4SpotResult),onchange : "OnChangeTPIResult("+ uc.SpotId +", this)"),
                            !isEnableTPI ? Convert.ToString(uc.TPI5SpotResult) : Helper.GenerateTextbox(uc.SpotId, "TPIResult5", Convert.ToString(uc.TPI5SpotResult),onchange : "OnChangeTPIResult("+ uc.SpotId +", this)"),
                            !isEnableTPI ? Convert.ToString(uc.TPI6SpotResult) : Helper.GenerateTextbox(uc.SpotId, "TPIResult6", Convert.ToString(uc.TPI6SpotResult),onchange : "OnChangeTPIResult("+ uc.SpotId +", this)"),
                            /*TPI RESULT END*/
                            string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo) && isSpotApplicableForDelete(isLTFPSProject?objQMS060.LTFPSHeaderId.Value : uc.HeaderId.Value, uc.SpotNumber.Value,isLTFPSProject?true : false)  ? HTMLActionString(uc.SpotId, "", "Delete","Delete Spot","fa fa-trash-o","DeleteSpot("+ uc.SpotId +");") : ""
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstStages.Count > 0 ? lstStages.FirstOrDefault().TotalCount : 0),
                    aaData = data
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult TPIResultHideShow(int requestId)
        {
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            List<int> ShowTPIAgencylist = new List<int>();
            int[] terms = null;
            if (objQMS060 != null)
            {


                #region Update Spot Description For Parallel Stages

                if (!String.IsNullOrEmpty(objQMS060.TPIAgency1)) { ShowTPIAgencylist.Add(1); }
                if (!String.IsNullOrEmpty(objQMS060.TPIAgency2)) { ShowTPIAgencylist.Add(2); }
                if (!String.IsNullOrEmpty(objQMS060.TPIAgency3)) { ShowTPIAgencylist.Add(3); }
                if (!String.IsNullOrEmpty(objQMS060.TPIAgency4)) { ShowTPIAgencylist.Add(4); }
                if (!String.IsNullOrEmpty(objQMS060.TPIAgency5)) { ShowTPIAgencylist.Add(5); }
                if (!String.IsNullOrEmpty(objQMS060.TPIAgency6)) { ShowTPIAgencylist.Add(6); }
                #endregion
                terms = ShowTPIAgencylist.ToArray();
                objResponseMsg.Key = true;
            }
            else
            {
                objResponseMsg.Key = false;
            }
            return Json(terms, JsonRequestBehavior.AllowGet);
        }

        public ActionResult OnChangeTPIResult(string TPIID, string value, int spotId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS070 objQMS070 = db.QMS070.FirstOrDefault(x => x.SpotId == spotId);
            if (objQMS070 != null && !string.IsNullOrWhiteSpace(value))
            {
                if (TPIID.Contains("TPIResult1")) { objQMS070.TPI1SpotResult = value; }
                else if (TPIID.Contains("TPIResult2")) { objQMS070.TPI2SpotResult = value; }
                else if (TPIID.Contains("TPIResult3")) { objQMS070.TPI3SpotResult = value; }
                else if (TPIID.Contains("TPIResult4")) { objQMS070.TPI4SpotResult = value; }
                else if (TPIID.Contains("TPIResult5")) { objQMS070.TPI5SpotResult = value; }
                else if (TPIID.Contains("TPIResult6")) { objQMS070.TPI6SpotResult = value; }

                #region Update Spot Result as per Seq
                List<string> lstTPI = new List<string>();
                lstTPI.Add(objQMS070.TPI1SpotResult);
                lstTPI.Add(objQMS070.TPI2SpotResult);
                lstTPI.Add(objQMS070.TPI3SpotResult);
                lstTPI.Add(objQMS070.TPI4SpotResult);
                lstTPI.Add(objQMS070.TPI5SpotResult);
                lstTPI.Add(objQMS070.TPI6SpotResult);

                string Rejected = clsImplementationEnum.SpotResults.Rejected.GetStringValue();
                string Reshoot = clsImplementationEnum.SpotResults.Reshoot.GetStringValue();
                string Rescan = clsImplementationEnum.SpotResults.Rescan.GetStringValue();
                string Retake = clsImplementationEnum.SpotResults.Retake.GetStringValue();
                string Miscellaneous = clsImplementationEnum.SpotResults.Mishelenious.GetStringValue();
                string Cleared = clsImplementationEnum.SpotResults.Cleared.GetStringValue();
                string Discard = clsImplementationEnum.SpotResults.Discard.GetStringValue();

                if (lstTPI.Any(x => x == Rejected))
                {
                    objQMS070.SpotResult = Rejected;
                }
                else if (lstTPI.Any(x => x == Reshoot))
                {
                    objQMS070.SpotResult = Reshoot;
                }
                else if (lstTPI.Any(x => x == Rescan))
                {
                    objQMS070.SpotResult = Rescan;
                }
                else if (lstTPI.Any(x => x == Retake))
                {
                    objQMS070.SpotResult = Retake;
                }
                else if (lstTPI.Any(x => x == Miscellaneous))
                {
                    objQMS070.SpotResult = Miscellaneous;
                }
                else if (lstTPI.Any(x => x == Cleared))
                {
                    objQMS070.SpotResult = Cleared;
                }
                else if (lstTPI.Any(x => x == Discard))
                {
                    objQMS070.SpotResult = Discard;
                }
                #endregion

                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            else
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        public ActionResult TPIResultPending(int RequestID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == RequestID);
            if (objQMS060 != null)
            {
                objQMS060.TPIResultPending = false;
                //if (objQMS060.TPIResultPending == true) {
                //    objQMS060.TPIResultPending = false;
                //}
                db.SaveChanges();
                objResponseMsg.Value = "TPI Result submitted successfully";
                objResponseMsg.Key = true;
            }
            else
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public bool TPIResultValidate(int RequestID)
        {
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == RequestID);
            List<QMS070> lstQMS070 = db.QMS070.Where(x => x.RequestId == RequestID).ToList();
            bool ismaintained = true;
            foreach (var item in lstQMS070)
            {
                if (!string.IsNullOrEmpty(objQMS060.TPIAgency1) && (string.IsNullOrEmpty(item.TPI1SpotResult)))
                {
                    ismaintained = false;
                    break;
                }
                if (!string.IsNullOrEmpty(objQMS060.TPIAgency2) && (string.IsNullOrEmpty(item.TPI2SpotResult)))
                {
                    ismaintained = false;
                    break;
                }
                if (!string.IsNullOrEmpty(objQMS060.TPIAgency3) && (string.IsNullOrEmpty(item.TPI3SpotResult)))
                {
                    ismaintained = false;
                    break;
                }
                if (!string.IsNullOrEmpty(objQMS060.TPIAgency4) && (string.IsNullOrEmpty(item.TPI4SpotResult)))
                {
                    ismaintained = false;
                    break;
                }
                if (!string.IsNullOrEmpty(objQMS060.TPIAgency5) && (string.IsNullOrEmpty(item.TPI5SpotResult)))
                {
                    ismaintained = false;
                    break;
                }
                if (!string.IsNullOrEmpty(objQMS060.TPIAgency6) && (string.IsNullOrEmpty(item.TPI6SpotResult)))
                {
                    ismaintained = false;
                    break;
                }
            }
            return ismaintained;
        }


        [HttpPost]
        public bool SpotResultValidate(int RequestID)
        {
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == RequestID);

            string Cleared = clsImplementationEnum.SpotResults.Cleared.GetStringValue();
            string Discard = clsImplementationEnum.SpotResults.Discard.GetStringValue();
            string Rejected = clsImplementationEnum.SpotResults.Rejected.GetStringValue();

            List<QMS070> lstQMS070 = db.QMS070.Where(x => x.RequestId == RequestID && (x.SpotResult != Cleared && x.SpotResult != Discard)).ToList();
            bool ismaintained = true;
            foreach (var item in lstQMS070)
            {
                if (item.SpotResult == Rejected && ((objQMS060.IsOverlay == true ? (!item.DefectWidth.HasValue && item.DefectWidth == 0) : true) || (!item.DefectLength.HasValue && item.DefectLength == 0) || string.IsNullOrWhiteSpace(item.TypeofIndication)))
                {
                    ismaintained = false;
                    break;
                }

                if (item.SpotResult != Rejected && string.IsNullOrWhiteSpace(item.Reason))
                {
                    ismaintained = false;
                    break;
                }
            }
            return ismaintained;
        }

        public ActionResult IsAttatchmentDisabled(int RequestID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == RequestID);
            if (objQMS060 != null)
            {
                if (objQMS060.TPIResultPending == false && objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue())
                {
                    objQMS060.IsAttatchmentDisabled = true;
                    db.SaveChanges();
                }
                objResponseMsg.Key = Convert.ToBoolean(objQMS060.IsAttatchmentDisabled);
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        public ActionResult ISTPIResultPending(int RequestID)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == RequestID);
            if (objQMS060 != null)
            {
                objResponseMsg.Key = objQMS060.TPIResultPending == null ? true : Convert.ToBoolean(objQMS060.TPIResultPending);
            }
            else
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult OnChangeSpotDescription(int spotId, string spotDescription)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS070 objQMS070 = db.QMS070.FirstOrDefault(x => x.SpotId == spotId);
            if (objQMS070 != null && !string.IsNullOrWhiteSpace(spotDescription))
            {
                objQMS070.SpotDescription = spotDescription;

                #region Update Spot Description For Parallel Stages

                List<int> parallelRequestIds = GetParallelRequestIds(objQMS070.RequestId);
                List<int> lstParallelRequests = parallelRequestIds.Where(x => x != objQMS070.RequestId).ToList();
                foreach (int pRequestId in lstParallelRequests)
                {
                    QMS070 objParallelQMS070 = db.QMS070.FirstOrDefault(x => x.RequestId == pRequestId && x.SpotNumber == objQMS070.SpotNumber);
                    if (objParallelQMS070 != null)
                    {
                        objParallelQMS070.SpotDescription = spotDescription;
                    }
                }
                #endregion

                db.SaveChanges();
                objResponseMsg.Key = true;
            }
            else
            {
                objResponseMsg.Key = false;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public List<int> GetParallelRequestIds(int RequestId)
        {
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == RequestId);
            string parallelRequestIds = string.Empty;
            if (Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location))
            {
                parallelRequestIds = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_LTFPS_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
            }
            else
            {
                parallelRequestIds = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_IPI_PARALLEL_SEAM_NDE_REQUEST('" + objQMS060.RequestId + "')").FirstOrDefault();
            }
            List<int> lstParallelRequests = parallelRequestIds.Split(',').Where(x => x != Convert.ToString(objQMS060.RequestId)).Select(int.Parse).ToList();
            return lstParallelRequests;
        }

        public bool isSpotApplicableForDelete(int headerID, int spotNumber, bool isLTFPSProject)
        {
            bool returnvalue = true;
            if (isLTFPSProject)
            {
                if (db.QMS070.Count(c => c.LTFPSHeaderId == headerID && c.SpotNumber == spotNumber) > 1)
                {
                    returnvalue = false;
                }
            }
            else
            {
                if (db.QMS070.Count(c => c.HeaderId == headerID && c.SpotNumber == spotNumber) > 1)
                {
                    returnvalue = false;
                }
            }

            return returnvalue;
        }

        public string HTMLActionString(int rowId, string status, string buttonName, string buttonTooltip = "", string className = "", string onClickMethod = "")
        {
            string htmlControl = "";

            string inputID = buttonName + "" + rowId.ToString();
            string onClickEvent = !string.IsNullOrEmpty(onClickMethod) ? "onclick='" + onClickMethod + "'" : "";

            htmlControl = "<i id='" + inputID + "' name='" + inputID + "' style='cursor: pointer;' Title='" + buttonTooltip + "' class='" + className + "'" + onClickEvent + " ></i>";

            return htmlControl;
        }

        public ActionResult SaveNewSpot(int headerId, int requestId, string spotNumber, string spotDescription, string spotLength)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int spotNo, spotLen;

                QMS060 objQMS060 = db.QMS060.FirstOrDefault(c => c.RequestId == requestId);
                bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
                if (int.TryParse(spotNumber, out spotNo) && int.TryParse(spotLength, out spotLen))
                {

                    if (db.QMS070.Any(c => c.SpotNumber == spotNo && (isLTFPSProject ? (c.RequestNo == objQMS060.RequestNo) : (c.HeaderId == headerId))))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Spot number already exists.";
                    }
                    else
                    {
                        int OverlayArea = 0;
                        bool applicableForAdd = true;
                        int? totSpotLength = db.QMS070.Where(c => c.RequestId == requestId && (isLTFPSProject ? (c.RequestNo == objQMS060.RequestNo) : (c.HeaderId == headerId))).Sum(s => s.SpotLength);
                        totSpotLength = (totSpotLength == null ? 0 : totSpotLength);
                        if (objQMS060.IsOverlay.Value)
                        {
                            if (int.TryParse(objQMS060.OverlayArea, out OverlayArea))
                            {
                                if ((totSpotLength + spotLen) > OverlayArea)
                                {
                                    applicableForAdd = false;
                                    objResponseMsg.Key = false;
                                    objResponseMsg.Value = "Total spots length should not be greater than seam overlay area";
                                }
                            }
                        }
                        else
                        {
                            if ((totSpotLength + spotLen) > objQMS060.SeamLength)
                            {
                                applicableForAdd = false;
                                objResponseMsg.Key = false;
                                objResponseMsg.Value = "Total spots length should not be greater than seam length";
                            }
                        }

                        if (applicableForAdd)
                        {
                            QMS070 objNewQMS070 = new QMS070();
                            objNewQMS070.RequestId = objQMS060.RequestId;
                            objNewQMS070.HeaderId = objQMS060.HeaderId;
                            objNewQMS070.QualityProject = objQMS060.QualityProject;
                            objNewQMS070.Project = objQMS060.Project;
                            objNewQMS070.BU = objQMS060.BU;
                            objNewQMS070.Location = objQMS060.Location;
                            objNewQMS070.SeamNo = objQMS060.SeamNo;
                            objNewQMS070.StageCode = objQMS060.StageCode;
                            objNewQMS070.StageSequence = objQMS060.StageSequence;
                            objNewQMS070.IterationNo = objQMS060.IterationNo;
                            objNewQMS070.RequestNo = objQMS060.RequestNo;
                            objNewQMS070.RequestNoSequence = objQMS060.RequestNoSequence;
                            objNewQMS070.SpotNumber = spotNo;
                            objNewQMS070.SpotDescription = spotDescription;
                            objNewQMS070.SpotLength = spotLen;
                            objNewQMS070.CreatedBy = objClsLoginInfo.UserName;
                            objNewQMS070.CreatedOn = DateTime.Now;
                            objNewQMS070.RepairDesignation = "R";
                            objNewQMS070.RepairDesignationValue = 0;
                            objNewQMS070.LTFPSHeaderId = objQMS060.LTFPSHeaderId;
                            db.QMS070.Add(objNewQMS070);
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "New spot added successfully";
                        }

                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Please enter correct value";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        public ActionResult RemoveSpot(int spotId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                QMS070 objQMS070 = db.QMS070.FirstOrDefault(c => c.SpotId == spotId);
                if (objQMS070 != null)
                {
                    db.QMS070.Remove(objQMS070);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Spot deleted successfully";
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No spot found. Please try again";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);

        }

        public ActionResult loadWeldersDataTable(JQueryDataTableParamModel param, int requestId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;


                string whereCondition = "1=1";
                //whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms070.BU", "qms070.Location");
                whereCondition += " AND  qms070.RequestId = " + requestId;
                var lstWelders = db.SP_FETCH_IPI_SPOT_RESULT(StartIndex, EndIndex, "", whereCondition).ToList();
                int? totalRecords = lstWelders.Select(i => i.TotalCount).FirstOrDefault();

                var res = from c in lstWelders
                          select new[] {
                                        Convert.ToString(c.SpotNumber),
                                        Convert.ToString(c.SpotId),
                                        Convert.ToString(c.RequestId),
                                        Convert.ToString(c.SpotDescription),
                                        Convert.ToString(c.Welder1),
                                        Convert.ToString(c.Welder2),
                                        Convert.ToString(c.Welder3),
                                        Convert.ToString(c.Welder4),
                                        Convert.ToString(c.Welder5),
                                        Convert.ToString(c.Welder6),
                                        Convert.ToString(c.Welder7),
                                        Convert.ToString(c.Welder8),
                                        Convert.ToString(c.Welder9),
                                        Convert.ToString(c.Welder10),
                                        Convert.ToString(c.Welder11),
                                        Convert.ToString(c.Welder12),
                                        Convert.ToString(c.Welder13),
                                        Convert.ToString(c.Welder14),
                                        Convert.ToString(c.Welder15),
                                        Convert.ToString(c.Welder16),
                                        Convert.ToString(c.Welder17),
                                        Convert.ToString(c.Welder18),
                                        Convert.ToString(c.Welder19),
                                        Convert.ToString(c.Welder20),
                                        Convert.ToString(c.Welder21),
                                        Convert.ToString(c.Welder22),
                                        Convert.ToString(c.Welder23),
                                        Convert.ToString(c.Welder24)
                          };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,

                }, JsonRequestBehavior.AllowGet);

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""

                }, JsonRequestBehavior.AllowGet);
            }
        }
        public string HTMLAutoComplete(int rowId, string columnName, string columnValue = "", string onBlurMethod = "", bool isReadOnly = false, string inputStyle = "", string hdElement = "", bool disabled = false)
        {
            string strAutoComplete = string.Empty;

            string inputID = columnName + "" + rowId.ToString();
            string hdElementId = hdElement + "" + rowId.ToString();
            string inputName = columnName;
            string inputValue = columnValue;
            string className = "autocomplete form-control";
            string onBlurEvent = !string.IsNullOrEmpty(onBlurMethod) ? "onblur='" + onBlurMethod + "'" : "";

            strAutoComplete = "<input type='text' " + (isReadOnly ? "readonly='readonly'" : "") + " id='" + inputID + "' hdElement='" + hdElementId + "' value='" + inputValue + "' colname='" + columnName + "' name='" + inputID + "' class='" + className + "' style='" + inputStyle + "'  " + onBlurEvent + " " + (disabled ? "disabled" : "") + " />";

            return strAutoComplete;
        }

        public JsonResult GetReasonCategory(string seamCategory, int spotId)
        {
            QMS070 objQMS070 = db.QMS070.FirstOrDefault(x => x.SpotId == spotId);
            List<CategoryData> lstCategoryData = new List<CategoryData>();
            if (seamCategory == "Reshoot")
            {
                lstCategoryData = Manager.GetSubCatagories("Reshoot Reason", objQMS070.BU, objQMS070.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            }
            else if (seamCategory == "Re-Scan")
            {
                lstCategoryData = Manager.GetSubCatagories("Rescan Reason", objQMS070.BU, objQMS070.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            }
            else if (seamCategory == "Miscellaneous")
            {
                lstCategoryData = Manager.GetSubCatagories("Miscellaneous Reason", objQMS070.BU, objQMS070.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            }
            else if (seamCategory == "Re-Take")
            {
                lstCategoryData = Manager.GetSubCatagories("Retake Reason", objQMS070.BU, objQMS070.Location).Select(i => new CategoryData { Value = i.Code, Code = i.Code, CategoryDescription = i.Description }).ToList();
            }
            return Json(lstCategoryData, JsonRequestBehavior.AllowGet);
        }

        public ActionResult SaveSpotResult(string strSpotResult, int requestId, bool isTpiResult = false)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            if(objClsLoginInfo == null)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Session Expired";
                return Json(objResponseMsg);
            }

            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
            bool isLTFPSProject = Manager.IsLTFPSProject(objQMS060.Project, objQMS060.BU, objQMS060.Location);
            List<SpotResult> lstSpotResult = new List<SpotResult>();
            if (isTpiResult)
            {
                if (objQMS060.TPIResultPending == true || objQMS060.RequestStatus == clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue())
                {
                    objResponseMsg.Key = true;
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "This request has been already submitted";
                }
            }
            else
            {
                objResponseMsg = Manager.IsApplicableForAttend(objQMS060.RequestId, clsImplementationEnum.InspectionFor.SEAM.GetStringValue(), "NDE");

            }
            if (!objResponseMsg.Key)
            {
                return Json(objResponseMsg);
            }

            if (!string.IsNullOrWhiteSpace(strSpotResult))
            {
                lstSpotResult = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<SpotResult>>(strSpotResult);
            }
            string parallelRequestId = string.Empty;
            if (isLTFPSProject)
            {
                parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_LTFPS_PARALLEL_SEAM_NDE_REQUEST('" + requestId + "')").FirstOrDefault();
            }
            else
            {
                parallelRequestId = db.Database.SqlQuery<string>("SELECT dbo.FUN_GET_IPI_PARALLEL_SEAM_NDE_REQUEST('" + requestId + "')").FirstOrDefault();
            }


            List<int> parallelId = parallelRequestId.Split(',').Where(x => x != Convert.ToString(requestId)).Select(int.Parse).ToList();

            //string[] arrRequestId = parallelRequestId.Split(',');
            //arrRequestId = arrRequestId.Where(val => val != requestId).ToArray();

            List<QMS070> lstQMS70 = db.QMS070.Where(x => x.RequestId == requestId).ToList();
            List<QMS070> SrcQMS70 = new List<QMS070>();

            lstSpotResult.RemoveAt(0);
            bool isReworkLTFPS = false;
            bool isCalledOfferedSP = false;
            bool parentReadyToOffer = false;
            bool seamCleared = true;

            foreach (var item in lstSpotResult)
            {

                if (item.Result == clsImplementationEnum.SpotResults.Rejected.GetStringValue() && string.IsNullOrWhiteSpace(item.IndicationType))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Type Of Indication required in Spot No" + item.SpotNumber.ToString();
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                QMS070 obj = lstQMS70.Where(x => x.SpotId == item.SpotId && x.RequestId == requestId).FirstOrDefault();
                obj.SpotResult = item.Result;
                if (!isTpiResult)
                {
                    obj.LNTSpotResult = item.Result;
                }
                obj.DefectLength = item.DefectLength;
                obj.DefectWidth = item.DefectWidth;
                obj.TypeofIndication = item.IndicationType;
                obj.Reason = item.Reason;
                obj.InspectionRemarks = item.Remarks;
                if (item.Result == clsImplementationEnum.SpotResults.Rejected.GetStringValue())
                {
                    SrcQMS70.Add(obj);
                    isReworkLTFPS = true;
                }
                if ((item.Result != clsImplementationEnum.SpotResults.Cleared.GetStringValue() && item.Result != clsImplementationEnum.SpotResults.Discard.GetStringValue()) || obj.IsRejected == true)
                {
                    seamCleared = false;
                }
            }

            if (!string.IsNullOrWhiteSpace(objQMS060.TPIAgency1))
            {
                if (isTpiResult)
                {
                    objQMS060.TPIResultPending = false;
                }
                else
                {
                    objQMS060.TPIResultPending = true;
                }
            }
            db.SaveChanges();


            bool allCleared = true;

            #region check intervention is hold or witness flag
            bool isHW = false;// is intervention is hold or witness flag
            if (!isTpiResult)
            {
                if (!string.IsNullOrWhiteSpace(objQMS060.TPIAgency1) || !string.IsNullOrWhiteSpace(objQMS060.TPIAgency2) || !string.IsNullOrWhiteSpace(objQMS060.TPIAgency3) || !string.IsNullOrWhiteSpace(objQMS060.TPIAgency4) || !string.IsNullOrWhiteSpace(objQMS060.TPIAgency5) || !string.IsNullOrWhiteSpace(objQMS060.TPIAgency6))
                {
                    isHW = true;
                }
                //List<string> lstIntervention = new List<string>();
                //lstIntervention.Add(GetCategoryDescription("TPI Intervention", objQMS060.TPIAgency1Intervention, objQMS060.BU, objQMS060.Location).CategoryDescription);
                //lstIntervention.Add(GetCategoryDescription("TPI Intervention", objQMS060.TPIAgency2Intervention, objQMS060.BU, objQMS060.Location).CategoryDescription);
                //lstIntervention.Add(GetCategoryDescription("TPI Intervention", objQMS060.TPIAgency3Intervention, objQMS060.BU, objQMS060.Location).CategoryDescription);
                //lstIntervention.Add(GetCategoryDescription("TPI Intervention", objQMS060.TPIAgency4Intervention, objQMS060.BU, objQMS060.Location).CategoryDescription);
                //lstIntervention.Add(GetCategoryDescription("TPI Intervention", objQMS060.TPIAgency5Intervention, objQMS060.BU, objQMS060.Location).CategoryDescription);
                //lstIntervention.Add(GetCategoryDescription("TPI Intervention", objQMS060.TPIAgency6Intervention, objQMS060.BU, objQMS060.Location).CategoryDescription);

                //if (lstIntervention.Any(x => x == "Hold" || x == "Witness" || x == "H" || x == "W"))
                //{
                //    isHW = true;
                //}
            }

            #endregion

            if (!isHW)
            {
                #region Set Rejected Flag to Parallel Stages
                if (parallelId.Count > 0)
                {
                    foreach (QMS070 sQMS70 in SrcQMS70)
                    {
                        foreach (int pReqID in parallelId)
                        {
                            var tmpQMS060 = db.QMS060.FirstOrDefault(c => c.RequestId == pReqID);
                            var destQMS70 = db.QMS070.FirstOrDefault(c => c.SpotId != sQMS70.SpotId && c.RequestNo == tmpQMS060.RequestNo && c.SpotNumber == sQMS70.SpotNumber && c.RepairDesignationValue == sQMS70.RepairDesignationValue);
                            if (destQMS70 != null)
                            {
                                destQMS70.IsRejected = true;
                                db.SaveChanges();
                            }
                        }
                    }
                }
                #endregion
            }
            objQMS060.RequestStatus = clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue();
            if (!isTpiResult)
            {
                objQMS060.InspectedBy = objClsLoginInfo.UserName;
                objQMS060.InspectedOn = DateTime.Now;
            }
            else
            {
                objQMS060.TPIInspectedBy = objClsLoginInfo.UserName;
                objQMS060.TPIInspectedOn = DateTime.Now;
            }

            db.SaveChanges();

            List<QMS070> lstAllSpot;

            lstAllSpot = db.QMS070.Where(x => (isLTFPSProject ? (x.RequestNo == objQMS060.RequestNo && x.StageCode == objQMS060.StageCode) : (x.HeaderId == objQMS060.HeaderId)) && x.IterationNo == 1).ToList();

            int inspectionExtent1;
            if (int.TryParse(objQMS060.InspectionExtent.Replace("%", "").Trim(), out inspectionExtent1))
            {
                if (inspectionExtent1 < 100)
                {
                    lstAllSpot.Clear();
                    List<int> spotsNos = db.QMS070.Where(c => isLTFPSProject ? (c.RequestNo == objQMS060.RequestNo && c.StageCode == objQMS060.StageCode) : (c.HeaderId == objQMS060.HeaderId)).GroupBy(g => g.SpotNumber.Value).Select(x => x.OrderByDescending(o => o.SpotId).FirstOrDefault().SpotId).ToList();
                    lstAllSpot = db.QMS070.Where(c => spotsNos.Contains(c.SpotId)).ToList();
                }
            }

            bool allParallalCleared = true;
            bool ParallelPendingResult = true;

            foreach (QMS070 itemSpot in lstAllSpot)
            {
                QMS070 maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && (isLTFPSProject ? (x.RequestNo == objQMS060.RequestNo && x.StageCode == objQMS060.StageCode) : (x.HeaderId == objQMS060.HeaderId))).OrderByDescending(o => o.IterationNo).FirstOrDefault();

                if (seamCleared == true && ((maxSpotResult.SpotResult != clsImplementationEnum.SpotResults.Cleared.GetStringValue() && maxSpotResult.SpotResult != clsImplementationEnum.SpotResults.Discard.GetStringValue()) || maxSpotResult.IsRejected == true))
                {
                    seamCleared = false;
                }

                if (maxSpotResult.SpotResult != clsImplementationEnum.SpotResults.Cleared.GetStringValue() && maxSpotResult.SpotResult != clsImplementationEnum.SpotResults.Discard.GetStringValue())
                {
                    allCleared = false;
                    break;
                }

            }

            if (seamCleared)
            {
                objQMS060.SeamStatus = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
            }
            else
            {
                objQMS060.SeamStatus = null;
            }

            if (!isHW)
            {
                if (allCleared)
                {
                    List<QMS060> lstParallel = db.QMS060.Where(c => parallelId.Contains(c.RequestId)).ToList();
                    foreach (var qms060 in lstParallel)
                    {
                        List<QMS070> lstAllSpotParallel = db.QMS070.Where(x => (isLTFPSProject ? (x.RequestNo == qms060.RequestNo && x.StageCode == qms060.StageCode) : (x.HeaderId == qms060.HeaderId)) && x.IterationNo == 1).ToList();

                        foreach (QMS070 itemSpot in lstAllSpotParallel)
                        {
                            QMS070 maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && (isLTFPSProject ? (x.RequestNo == qms060.RequestNo && x.StageCode == qms060.StageCode) : (x.HeaderId == qms060.HeaderId))).OrderByDescending(o => o.IterationNo).FirstOrDefault();

                            if ((maxSpotResult.SpotResult == null && isLTFPSProject) || qms060.TPIResultPending == true)
                            {
                                ParallelPendingResult = true;
                                allParallalCleared = false;
                                break;
                            }

                            if ((maxSpotResult.SpotResult != clsImplementationEnum.SpotResults.Cleared.GetStringValue() && maxSpotResult.SpotResult != clsImplementationEnum.SpotResults.Discard.GetStringValue()) || maxSpotResult.IsRejected == true)
                            {
                                allParallalCleared = false;
                                break;
                            }
                        }
                        if (!allParallalCleared)
                            break;
                    }

                    if (!allParallalCleared && !ParallelPendingResult && isLTFPSProject)
                    {
                        allCleared = false;
                        isReworkLTFPS = true;
                    }

                    if (allCleared)
                    {
                        if (isLTFPSProject)
                        {
                            objQMS060.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
                            objQMS060.LTF002.LNTStatus = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
                            objQMS060.LTF002.LNTInspectorResultOn = DateTime.Now;
                        }
                        else
                        {
                            objQMS060.QMS040.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
                            objQMS060.QMS040.LNTInspectorResult = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
                            objQMS060.QMS040.LNTInspectorResultOn = DateTime.Now;
                        }

                    }

                    if (allParallalCleared)
                    {
                        foreach (var qms060 in lstParallel)
                        {
                            if (isLTFPSProject)
                            {
                                qms060.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
                                qms060.LTF002.LNTStatus = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
                                qms060.LTF002.LNTInspectorResultOn = DateTime.Now;
                            }
                            else
                            {
                                qms060.QMS040.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
                                qms060.QMS040.LNTInspectorResult = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();
                                qms060.QMS040.LNTInspectorResultOn = DateTime.Now;
                            }
                        }

                        db.SaveChanges();

                        if (isLTFPSProject)
                        {
                            isCalledOfferedSP = true;

                        }
                        else
                        {
                            string ClearedStatus = clsImplementationEnum.SeamListInspectionStatus.Cleared.GetStringValue();

                            List<QMS040> lstObjQMS040 = db.QMS040.Where(c => c.SeamNo == objQMS060.QMS040.SeamNo && c.QualityProject == objQMS060.QMS040.QualityProject
                                  && c.BU == objQMS060.QMS040.BU && c.Location == objQMS060.QMS040.Location && c.InspectionStatus == null).ToList();

                            if (lstObjQMS040.Count > 0 && !db.QMS040.Any(c => c.SeamNo == objQMS060.QMS040.SeamNo && c.QualityProject == objQMS060.QMS040.QualityProject && c.BU == objQMS060.QMS040.BU && c.Location == objQMS060.QMS040.Location && c.StageSequence <= objQMS060.StageSequence && c.InspectionStatus != ClearedStatus))
                            {
                                int lowerSeq = lstObjQMS040.OrderBy(o => o.StageSequence).FirstOrDefault().StageSequence.Value;
                                var LowerSeqStages = lstObjQMS040.Where(c => c.StageSequence == lowerSeq);
                                foreach (var lowerStage in LowerSeqStages)
                                {
                                    lowerStage.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.ReadyToOffer.GetStringValue();
                                }
                            }

                            parentReadyToOffer = true;
                        }
                    }

                }

                if (!allCleared)
                {
                    if (isLTFPSProject)
                    {
                        if (isReworkLTFPS)
                        {
                            objQMS060.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.ReworkNotAttached.GetStringValue();
                            objQMS060.LTF002.LNTStatus = clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue();
                            objQMS060.IsPartiallyOffered = true;
                        }
                        else
                        {
                            objQMS060.LTF002.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.Rework.GetStringValue();
                            objQMS060.LTF002.LNTStatus = clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue();
                            objQMS060.IsPartiallyOffered = false;
                        }
                        objQMS060.LTF002.LNTInspectorResultOn = DateTime.Now;
                    }
                    else
                    {
                        objQMS060.QMS040.InspectionStatus = clsImplementationEnum.SeamListInspectionStatus.Rework.GetStringValue();
                        objQMS060.QMS040.LNTInspectorResult = clsImplementationEnum.SeamListInspectionStatus.Rejected.GetStringValue();
                        objQMS060.QMS040.LNTInspectorResultOn = DateTime.Now;
                    }

                }
            }

            db.SaveChanges();

            if (isCalledOfferedSP)
            {
                string result = db.SP_LTFPS_RELEASE_READY_TO_OFFER(objQMS060.LTF002.HeaderId).FirstOrDefault();
            }

            //if (parentReadyToOffer)
            //{
            #region Code for make ready to offer parent part
            //string ApprovedStatus = clsImplementationEnum.ICLPartStatus.Approved.GetStringValue();
            //if (Manager.IsSEAMFullyCleared(objQMS060.Project, objQMS060.QualityProject, objQMS060.BU, objQMS060.Location, objQMS060.SeamNo))
            //{
            //    QMS012 objQMS012 = db.QMS012.FirstOrDefault(c => c.Project == objQMS060.Project && c.QualityProject == objQMS060.QualityProject && c.SeamNo == objQMS060.SeamNo);
            //    QMS035 objQMS035 = db.QMS035.FirstOrDefault(c => c.Project == objQMS060.Project && c.QualityProject == objQMS060.QualityProject && c.BU == objQMS060.BU && c.Location == objQMS060.Location && c.ChildPartNo == objQMS012.AssemblyNo && c.Status == ApprovedStatus);
            //    if (objQMS035 != null)
            //    {
            //        Manager.ReadyToOffer_PART(objQMS035);
            //    }
            //}
            #endregion
            //}


            objResponseMsg.Key = true;
            if (allCleared)
            {
                objResponseMsg.Value = "NDE Stage Cleared Successfully";

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS060.Project, objQMS060.BU, objQMS060.Location, "NDT Request " + objQMS060.RequestNo + " for Stage: " + objQMS060.StageCode + " of Seam: " + objQMS060.SeamNo + " & Project No: " + objQMS060.QualityProject + " has been Cleared", clsImplementationEnum.NotificationType.Information.GetStringValue());
                #endregion
            }
            else
            {
                objResponseMsg.Value = "Spot result saved successfully";

                #region Send Notification
                (new clsManager()).SendNotification(clsImplementationEnum.UserRoleName.PROD3.GetStringValue(), objQMS060.Project, objQMS060.BU, objQMS060.Location, "NDT Request " + objQMS060.RequestNo + " for Stage: " + objQMS060.StageCode + " of Seam: " + objQMS060.SeamNo + " & Project No: " + objQMS060.QualityProject + " has been Inspected", clsImplementationEnum.NotificationType.Information.GetStringValue(), "/IPI/SeamListNDEInspection/ShopSeamTestDetails/" + objQMS060.RequestId);
                #endregion
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public int GetUnclearedSpotCount(int? headerId, bool isLTFPSProject = false)
        {
            long? _requestNo = null;
            if (isLTFPSProject)
            {
                _requestNo = db.QMS060.Where(c => c.LTFPSHeaderId == headerId).FirstOrDefault().RequestNo;
            }
            List<QMS070> lstAllSpot = db.QMS070.Where(x => (isLTFPSProject ? (x.RequestNo == _requestNo) : (x.HeaderId == headerId)) && x.IterationNo == 1).ToList();
            int count = 0;
            foreach (QMS070 itemSpot in lstAllSpot)
            {
                QMS070 maxSpotResult = db.QMS070.Where(x => x.SpotNumber == itemSpot.SpotNumber && (isLTFPSProject ? (x.RequestNo == _requestNo) : (x.HeaderId == itemSpot.HeaderId))).OrderByDescending(o => o.IterationNo).FirstOrDefault();
                if (maxSpotResult.SpotResult != clsImplementationEnum.SpotResults.Cleared.GetStringValue() && maxSpotResult.SpotResult != clsImplementationEnum.SpotResults.Discard.GetStringValue())
                {
                    count++;
                }
            }
            return count;
        }
        public ActionResult IsMaintainParameters(int requestId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string techniqueNo = db.QMS060.Where(x => x.RequestId == requestId).Select(s => s.NDETechniqueNo).FirstOrDefault();
            objResponseMsg.Key = false;
            if (!string.IsNullOrEmpty(techniqueNo))
            {
                string techName = techniqueNo.Split('/')[1].ToString();
                if (techName.ToUpper() == "ASCAN" || techName.ToUpper() == "TOFD" || techName.ToUpper() == "PAUT")
                {
                    objResponseMsg.Key = true;
                }
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MaintainSpotParameter(int spotId, int requestId)
        {
            string strURL = string.Empty;
            QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == requestId);
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            string technique = string.Empty;
            try
            {
                if (!string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo))
                {
                    string techName = objQMS060.NDETechniqueNo.Split('/')[1].ToString();
                    switch (techName.ToUpper())
                    {
                        case "ASCAN":
                            //QMS071              
                            strURL = WebsiteURL + "/IPI/TechniqueSpotParameters/AscanUTDetails/" + spotId;
                            ViewBag.strURL = strURL;
                            technique = "ASCAN";
                            break;
                        case "TOFD":
                            //QMS072
                            strURL = WebsiteURL + "/IPI/TechniqueSpotParameters/PAUTDetails/" + spotId;
                            ViewBag.strURL = strURL;
                            technique = "TOFD";
                            break;
                        case "PAUT":
                            //QMS072
                            strURL = WebsiteURL + "/IPI/TechniqueSpotParameters/PAUTDetails/" + spotId;
                            ViewBag.strURL = strURL;
                            technique = "PAUT";
                            break;
                        default:
                            break;
                    }
                    objResponseMsg.Key = true;
                    //objResponseMsg.Value = "Parameter Saved Successfully";
                }

                if (string.IsNullOrWhiteSpace(technique))
                {
                    objResponseMsg.Key = false;
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }

                //if (technique == "ASCAN")
                //{
                //    QMS070 objQMS070 = db.QMS070.FirstOrDefault(x => x.SpotId == spotId);
                //    QMS071 objQMS071 = db.QMS071.FirstOrDefault(x => x.SpotId == spotId);
                //    if (objQMS071 == null)
                //    {
                //        string commonRemarks = string.Empty;
                //        QMS071 objQMS071First = db.QMS071.FirstOrDefault(x => x.RequestId == objQMS070.RequestId);
                //        if (objQMS071First != null)
                //        {
                //            commonRemarks = objQMS071First.CommonRemark;
                //        }

                //        objQMS071 = new QMS071
                //        {
                //            SpotId = objQMS070.SpotId,
                //            RequestId = objQMS070.RequestId,
                //            QualityProject = objQMS070.QualityProject,
                //            Project = objQMS070.Project,
                //            BU = objQMS070.BU,
                //            Location = objQMS070.Location,
                //            SeamNo = objQMS070.SeamNo,
                //            StageCode = objQMS070.StageCode,
                //            StageSequence = objQMS070.StageSequence,
                //            IterationNo = objQMS070.IterationNo,
                //            RequestNo = objQMS070.RequestNo,
                //            RequestNoSequence = objQMS070.RequestNoSequence,
                //            SpotNumber = objQMS070.SpotNumber,
                //            CreatedBy = objClsLoginInfo.UserName,
                //            CreatedOn = DateTime.Now,
                //            CommonRemark = commonRemarks
                //        };
                //        db.QMS071.Add(objQMS071);
                //        db.SaveChanges();
                //    }
                //    objResponseMsg.Key = true;
                //    objResponseMsg.Value = "Parameter Saved Successfully";
                //}
                //else
                //{
                //    QMS070 objQMS070 = db.QMS070.FirstOrDefault(x => x.SpotId == spotId);
                //    QMS072 objQMS072 = db.QMS072.FirstOrDefault(x => x.SpotId == spotId);
                //    if (objQMS072 == null)
                //    {
                //        string commonRemarks = string.Empty;
                //        QMS072 objQMS072First = db.QMS072.FirstOrDefault(x => x.RequestId == objQMS070.RequestId);
                //        if (objQMS072First != null)
                //        {
                //            commonRemarks = objQMS072First.CommonRemark;
                //        }

                //        objQMS072 = new QMS072
                //        {
                //            SpotId = objQMS070.SpotId,
                //            RequestId = objQMS070.RequestId,
                //            QualityProject = objQMS070.QualityProject,
                //            Project = objQMS070.Project,
                //            BU = objQMS070.BU,
                //            Location = objQMS070.Location,
                //            SeamNo = objQMS070.SeamNo,
                //            StageCode = objQMS070.StageCode,
                //            StageSequence = objQMS070.StageSequence,
                //            IterationNo = objQMS070.IterationNo,
                //            RequestNo = objQMS070.RequestNo,
                //            RequestNoSequence = objQMS070.RequestNoSequence,
                //            SpotNumber = objQMS070.SpotNumber,
                //            CreatedBy = objClsLoginInfo.UserName,
                //            CreatedOn = DateTime.Now,
                //            CommonRemark = commonRemarks
                //        };
                //        db.QMS072.Add(objQMS072);
                //        db.SaveChanges();
                //    }
                //    objResponseMsg.Key = true;
                //    objResponseMsg.Value = "Parameter Saved Successfully";
                //}
                var response = new
                {
                    Key = objResponseMsg.Key,
                    Value = objResponseMsg.Value,
                    Url = strURL
                };
                return Json(response, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error! Please try again !";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                string strFileName = string.Empty;
                //if (gridType == clsImplementationEnum.GridType.HEADER.GetStringValue())
                //{
                List<SP_IPI_SEAMLISTNDETEST_DETAILS_Result> lst = db.SP_IPI_SEAMLISTNDETEST_DETAILS(1, int.MaxValue, strSortOrder, whereCondition).ToList();
                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No Data Found";
                    return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
                }
                List<GLB002> lstTPIAgencyDescription = Manager.GetSubCatagories("TPI Agency");
                List<NDESeamExportExcel> newlst = (from li in lst
                                                   select new NDESeamExportExcel
                                                   {
                                                       QualityProjectNo = li.QualityProject,
                                                       SeamNo = li.SeamNo,
                                                       SeamNoDescription = li.SeamDesc, //db.QMS012.Where(x => x.SeamNo == li.SeamNo && x.QualityProject == li.QualityProject).Select(x => x.SeamDescription).FirstOrDefault(),
                                                       RequestNo = li.RequestNo,
                                                       Iteration = li.IterationNo,
                                                       StageCode = !string.IsNullOrWhiteSpace(li.StageCode) ? li.StageCode.Split('-')[0].Trim() : "",
                                                       StageDescription = !string.IsNullOrWhiteSpace(li.StageCode) ? li.StageCode.Split('-')[1].Trim() : "", //Manager.GetStageDescriptionByCode(li.StageCode, li.BU, li.Location),
                                                       SequenceNo = li.StageSequence,
                                                       InspectionExtent = li.InspectionExtent,
                                                       BMThk1 = li.Part1thickness,
                                                       BMThk2 = li.Part2thickness,
                                                       WeldThkwithReinforcement = li.WeldThicknessReinforcement,
                                                       SeamLength = li.SeamLength,
                                                       SeamArea = li.OverlayArea,
                                                       JointType = li.Jointtype,
                                                       Shop = li.Shop,
                                                       FirstTPIAgency = li.TPIAgency1,
                                                       FirstTPIAgencyDescription = Manager.GetSubCategoryDescriptionFromList(li.TPIAgency1, li.BU, li.Location, lstTPIAgencyDescription),
                                                       FirstTPIIntervention = li.TPIAgency1Intervention,
                                                       SecondTPIAgency = li.TPIAgency2,
                                                       SecondTPIAgencyDescription = Manager.GetSubCategoryDescriptionFromList(li.TPIAgency2, li.BU, li.Location, lstTPIAgencyDescription),
                                                       SecondTPIIntervention = li.TPIAgency2Intervention,
                                                       ThirdTPIAgency = li.TPIAgency3,
                                                       ThirdTPIAgencyDescription = Manager.GetSubCategoryDescriptionFromList(li.TPIAgency3, li.BU, li.Location, lstTPIAgencyDescription),
                                                       ThirdTPIIntervention = li.TPIAgency3Intervention,
                                                       FourthTPIAgency = li.TPIAgency4,
                                                       FourthTPIAgencyDescription = Manager.GetSubCategoryDescriptionFromList(li.TPIAgency4, li.BU, li.Location, lstTPIAgencyDescription),
                                                       FourthTPIIntervention = li.TPIAgency4Intervention,
                                                       FifthTPIAgency = li.TPIAgency5,
                                                       FifthTPIAgencyDescription = Manager.GetSubCategoryDescriptionFromList(li.TPIAgency5, li.BU, li.Location, lstTPIAgencyDescription),
                                                       FifthTPIIntervention = li.TPIAgency5Intervention,
                                                       SixthTPIAgency = li.TPIAgency6,
                                                       SixthTPIAgencyDescription = Manager.GetSubCategoryDescriptionFromList(li.TPIAgency6, li.BU, li.Location, lstTPIAgencyDescription),
                                                       SixthTPIIntervention = li.TPIAgency6Intervention,
                                                       ApplicableSpecification = li.ApplicableSpecification,
                                                       AcceptanceStandard = li.AcceptanceStandard,
                                                       WeldingRemarks = li.WeldingEngineeringRemarks,
                                                       ShopRemarks = li.ShopRemark,
                                                       CTQApplicable = "true",
                                                       WelderStamp1 = li.Welder1,
                                                       WelderStamp2 = li.Welder2,
                                                       WelderStamp3 = li.Welder3,
                                                       WelderStamp4 = li.Welder4,
                                                       WelderStamp5 = li.Welder5,
                                                       WelderStamp6 = li.Welder6,
                                                       WelderStamp7 = li.Welder7,
                                                       WelderStamp8 = li.Welder8,
                                                       WelderStamp9 = li.Welder9,
                                                       WelderStamp10 = li.Welder10,
                                                       WelderStamp11 = li.Welder11,
                                                       WelderStamp12 = li.Welder12,
                                                       WelderStamp13 = li.Welder13,
                                                       WelderStamp14 = li.Welder14,
                                                       WelderStamp15 = li.Welder15,
                                                       WelderStamp16 = li.Welder16,
                                                       WelderStamp17 = li.Welder17,
                                                       WelderStamp18 = li.Welder18,
                                                       WelderStamp19 = li.Welder19,
                                                       WelderStamp20 = li.Welder20,
                                                       WelderStamp21 = li.Welder21,
                                                       WelderStamp22 = li.Welder22,
                                                       WelderStamp23 = li.Welder23,
                                                       WelderStamp24 = li.Welder24,

                                                       WeldingProcess = li.WeldingProcess,
                                                       ManufacturingCode = li.ManufacturingCode,
                                                       SurfaceCondition = li.SurfaceCondition,
                                                       OfferDate = String.Format("{0:dd/MM/yyyy}", li.RequestGeneratedOn),
                                                       OfferTime = String.Format("{0:hh:mm:tt}", li.RequestGeneratedOn),
                                                       InspectionDate = String.Format("{0:dd/MM/yyyy}", li.InspectedOn),
                                                       InspectionTime = String.Format("{0:hh:mm:tt}", li.InspectedOn),
                                                       InspectedBy = li.InspectedBy,
                                                       InspectionStatus = li.InspectionStatus,
                                                       LNTTestResult = li.LNTInspectorResult,
                                                       NDTRemarks = li.NDTRemarks

                                                   }).ToList();
                strFileName = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = strFileName;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public JsonResult GetDdlSeamList(string project, string qualityProject, string BU, string location)
        {
            var result = db.QMS060.
                Where(x => x.RequestStatus == "attended"
                && x.Project == project
                && x.QualityProject == qualityProject
                && x.Location == location
                && x.BU == BU).Select(x => x.SeamNo).Distinct().ToList();
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDdlStageList(string project, string qualityProject, string BU, string location, string seam, string stageType)
        {
            var attended = clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue();

            var result = (from a in db.QMS060
                          join b in db.QMS002 on new { a.StageCode, a.BU, a.Location } equals new { b.StageCode, b.BU, b.Location }
                          where a.Project == project
                          && a.RequestStatus == attended
                          && a.QualityProject == qualityProject
                          && a.Location == location
                          && a.BU == BU
                          && a.SeamNo == seam
                          && b.StageType == stageType
                          select b.StageCode).Distinct().ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDdlSeqList(string seam, string qualityProject, string stage)
        {
            var attended = clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue();

            var result = db.QMS060.
                Where(x => x.SeamNo == seam
                && x.QualityProject == qualityProject
                && x.StageCode == stage
                && x.RequestStatus == attended).Select(x => x.StageSequence).Distinct().ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult GetDdlIterationNoList(string seam, string qualityProject, string stage, int seq)
        {
            var attended = clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue();

            var result = db.QMS060.
                Where(x => x.SeamNo == seam
                && x.QualityProject == qualityProject
                && x.StageCode == stage
                && x.StageSequence == seq
                && x.RequestStatus == attended).Select(x => x.IterationNo).ToList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult CopyNDESeamTestDetails(string requestId, string headerId, string seam, string stage, string seq, string stageType, string iterationNo)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                if (!string.IsNullOrEmpty(requestId) && !string.IsNullOrEmpty(headerId))
                {
                    int intRequestId = Convert.ToInt32(requestId);
                    int intHeaderId = Convert.ToInt32(headerId);
                    int intSeq = Convert.ToInt32(seq);
                    int intIterationNo = Convert.ToInt32(iterationNo);

                    var Destination = db.QMS060.Find(intRequestId);

                    if (Destination.NDETechniqueNo == null)
                    {
                        var Source = db.QMS060.Where(x => x.SeamNo == seam
                                                    && x.StageCode == stage
                                                    && x.QualityProject == Destination.QualityProject
                                                    && x.IterationNo == intIterationNo
                                                    && x.StageSequence == intSeq).FirstOrDefault();

                        if (Source != null)
                        {
                            if (stageType == "PT" || stageType == "MT")
                            {
                                Destination.NDETechniqueNo = Source.NDETechniqueNo;
                                Destination.NDETechniqueRevisionNo = Source.NDETechniqueRevisionNo;
                                Destination.NDTRemarks = Source.NDTRemarks;
                                Destination.ExaminationBy = Source.ExaminationBy;
                                Destination.PrintSummary = Source.PrintSummary;
                                Destination.NDEExecution = Source.NDEExecution;
                                Destination.NDEActual = Source.NDEActual;
                                db.SaveChanges();
                            }
                            else if (stageType == "RT")
                            {
                                Destination.NDETechniqueNo = Source.NDETechniqueNo;
                                Destination.NDETechniqueRevisionNo = Source.NDETechniqueRevisionNo;
                                Destination.Radiation = Source.Radiation;
                                Destination.WeldThickness = Source.WeldThickness;
                                Destination.NDTRemarks = Source.NDTRemarks;
                                Destination.ExaminationBy = Source.ExaminationBy;
                                Destination.PrintSummary = Source.PrintSummary;
                                Destination.NDEExecution = Source.NDEExecution;
                                Destination.NDEActual = Source.NDEActual;
                                db.SaveChanges();
                            }
                            else if (stageType == "UT")  //Working on UT
                            {
                                Destination.NDETechniqueNo = Source.NDETechniqueNo;
                                Destination.NDETechniqueRevisionNo = Source.NDETechniqueRevisionNo;
                                Destination.FromDate = Source.FromDate;
                                Destination.ToDate = Source.ToDate;
                                Destination.ExaminationBy = Source.ExaminationBy;
                                Destination.PrintSummary = Source.PrintSummary;
                                Destination.NDEExecution = Source.NDEExecution;
                                Destination.NDEActual = Source.NDEActual;
                                db.SaveChanges();

                                if (!string.IsNullOrWhiteSpace(Destination.NDETechniqueNo))
                                {
                                    string techName = Destination.NDETechniqueNo.Split('/')[1].ToString();
                                    string strURL = string.Empty;
                                    switch (techName.ToUpper())
                                    {
                                        case "ASCAN":
                                            {
                                                QMS080 objQMS080 = db.QMS080.Where(x => x.RefHeaderId == Destination.RequestId).FirstOrDefault();
                                                QMS080 sourceQMS080 = db.QMS080.Where(x => x.RefHeaderId == Source.RequestId).FirstOrDefault();
                                                if (objQMS080 == null && sourceQMS080 != null)
                                                {
                                                    objQMS080 = new QMS080();
                                                    objQMS080.RefHeaderId = Destination.RequestId;
                                                    objQMS080.QualityProject = Destination.QualityProject;
                                                    objQMS080.Project = Destination.Project;
                                                    objQMS080.BU = Destination.BU;
                                                    objQMS080.Location = Destination.Location;
                                                    objQMS080.SeamNo = Destination.SeamNo;
                                                    objQMS080.StageCode = Destination.StageCode;
                                                    objQMS080.StageSequence = Destination.StageSequence;
                                                    objQMS080.IterationNo = Destination.IterationNo;
                                                    objQMS080.RequestNo = Destination.RequestNo;
                                                    objQMS080.RequestNoSequence = Destination.RequestNoSequence;
                                                    objQMS080.UTTechNo = Destination.NDETechniqueNo;
                                                    objQMS080.RevNo = Destination.NDETechniqueRevisionNo;
                                                    objQMS080.Status = sourceQMS080.Status;
                                                    objQMS080.UTProcedureNo = sourceQMS080.UTProcedureNo;
                                                    objQMS080.RecordableIndication = sourceQMS080.RecordableIndication;
                                                    objQMS080.ScanTechnique = sourceQMS080.ScanTechnique;
                                                    objQMS080.DGSDiagram = sourceQMS080.DGSDiagram;
                                                    objQMS080.BasicCalibrationBlock = sourceQMS080.BasicCalibrationBlock;
                                                    objQMS080.SimulationBlock1 = sourceQMS080.SimulationBlock1;
                                                    objQMS080.SimulationBlock2 = sourceQMS080.SimulationBlock2;
                                                    objQMS080.CouplantUsed = sourceQMS080.CouplantUsed;
                                                    objQMS080.ScanningSensitivity = sourceQMS080.ScanningSensitivity;
                                                    objQMS080.PlannerRemarks = sourceQMS080.PlannerRemarks;
                                                    objQMS080.ApproverRemarks = sourceQMS080.ApproverRemarks;
                                                    objQMS080.TestInstrument = sourceQMS080.TestInstrument;
                                                    objQMS080.ExaminationDate = sourceQMS080.ExaminationDate;
                                                    objQMS080.NDTDuration = sourceQMS080.NDTDuration;
                                                    objQMS080.Rejectcontrol = sourceQMS080.Rejectcontrol;
                                                    objQMS080.Damping = sourceQMS080.Damping;
                                                    objQMS080.RestrictedAccessAreaInaccessibleWelds = sourceQMS080.RestrictedAccessAreaInaccessibleWelds;
                                                    objQMS080.TransferCorrection = sourceQMS080.TransferCorrection;
                                                    objQMS080.TransferCorrectiondB = sourceQMS080.TransferCorrectiondB;
                                                    objQMS080.Noofpoints = sourceQMS080.Noofpoints;
                                                    objQMS080.AveragedB = sourceQMS080.AveragedB;
                                                    objQMS080.AfterTestCalibrationrechecked = sourceQMS080.AfterTestCalibrationrechecked;
                                                    objQMS080.AnySpecialAccessoriesUsed = sourceQMS080.AnySpecialAccessoriesUsed;
                                                    objQMS080.AnyRecordableIndication = sourceQMS080.AnyRecordableIndication;
                                                    objQMS080.NDTremarks = sourceQMS080.NDTremarks;
                                                    objQMS080.EditedBy = objClsLoginInfo.UserName;
                                                    objQMS080.EditedOn = DateTime.Now;
                                                    objQMS080.CreatedBy = objClsLoginInfo.UserName;
                                                    objQMS080.CreatedOn = DateTime.Now;
                                                    db.QMS080.Add(objQMS080);
                                                    db.SaveChanges();

                                                    var sourceQMS081 = sourceQMS080.QMS081.ToList();
                                                    if (sourceQMS081 != null)
                                                    {
                                                        foreach (var item in sourceQMS081)
                                                        {
                                                            QMS081 newQMS081 = new QMS081();
                                                            newQMS081.HeaderId = objQMS080.HeaderId;
                                                            newQMS081.RefHeaderId = objQMS080.RefHeaderId;
                                                            newQMS081.QualityProject = objQMS080.QualityProject;
                                                            newQMS081.Project = objQMS080.Project;
                                                            newQMS081.BU = objQMS080.BU;
                                                            newQMS081.Location = objQMS080.Location;
                                                            newQMS081.SeamNo = objQMS080.SeamNo;
                                                            newQMS081.StageCode = objQMS080.StageCode;
                                                            newQMS081.StageSequence = objQMS080.StageSequence;
                                                            newQMS081.IterationNo = objQMS080.IterationNo;
                                                            newQMS081.RequestNo = objQMS080.RequestNo;
                                                            newQMS081.RequestNoSequence = objQMS080.RequestNoSequence;
                                                            newQMS081.UTTechNo = objQMS080.UTTechNo;
                                                            newQMS081.RevNo = objQMS080.RevNo;
                                                            newQMS081.SearchUnit = item.SearchUnit;
                                                            newQMS081.ReferenceReflector = item.ReferenceReflector;
                                                            newQMS081.CableType = item.CableType;
                                                            newQMS081.CableLength = item.CableLength;
                                                            newQMS081.ReferencegaindB = item.ReferencegaindB;
                                                            newQMS081.Range = item.Range;
                                                            newQMS081.AmpInd1 = item.AmpInd1;
                                                            newQMS081.BPInd1 = item.BPInd1;
                                                            newQMS081.AmpInd2 = item.AmpInd2;
                                                            newQMS081.BPInd2 = item.BPInd2;
                                                            newQMS081.AmpInd3 = item.AmpInd3;
                                                            newQMS081.BPInd3 = item.BPInd3;
                                                            newQMS081.AmpInd4 = item.AmpInd4;
                                                            newQMS081.BPInd4 = item.BPInd4;
                                                            newQMS081.AmpInd5 = item.AmpInd5;
                                                            newQMS081.BPInd5 = item.BPInd5;
                                                            newQMS081.AmpInd6 = item.AmpInd6;
                                                            newQMS081.BPInd6 = item.BPInd6;
                                                            newQMS081.ProbeSerialNo = item.ProbeSerialNo;
                                                            newQMS081.CreatedBy = objClsLoginInfo.UserName;
                                                            newQMS081.CreatedOn = DateTime.Now;
                                                            newQMS081.EditedBy = objClsLoginInfo.UserName;
                                                            newQMS081.EditedOn = DateTime.Now;
                                                            db.QMS081.Add(newQMS081);
                                                        }
                                                        db.SaveChanges();
                                                    }
                                                }
                                                break;
                                            }
                                        case "TOFD":
                                            {

                                                QMS082 objQMS082 = db.QMS082.Where(x => x.RefHeaderId == Destination.RequestId).FirstOrDefault();
                                                var sourceQMS082 = db.QMS082.Where(x => x.RefHeaderId == Source.RequestId).FirstOrDefault();
                                                if (objQMS082 == null && sourceQMS082 != null)
                                                {
                                                    objQMS082 = new QMS082();
                                                    objQMS082.RefHeaderId = Destination.RequestId;
                                                    objQMS082.QualityProject = Destination.QualityProject;
                                                    objQMS082.Project = Destination.Project;
                                                    objQMS082.BU = Destination.BU;
                                                    objQMS082.Location = Destination.Location;
                                                    objQMS082.SeamNo = Destination.SeamNo;
                                                    objQMS082.StageCode = Destination.StageCode;
                                                    objQMS082.StageSequence = Destination.StageSequence;
                                                    objQMS082.IterationNo = Destination.IterationNo;
                                                    objQMS082.RequestNo = Destination.RequestNo;
                                                    objQMS082.RequestNoSequence = Destination.RequestNoSequence;
                                                    objQMS082.UTTechNo = Destination.NDETechniqueNo;
                                                    objQMS082.RevNo = Destination.NDETechniqueRevisionNo;
                                                    objQMS082.Status = sourceQMS082.Status;
                                                    objQMS082.UTProcedureNo = sourceQMS082.UTProcedureNo;
                                                    objQMS082.CouplantUsed = sourceQMS082.CouplantUsed;
                                                    objQMS082.ReferenceReflectorSize = sourceQMS082.ReferenceReflectorSize;
                                                    objQMS082.ReferenceReflectorDistance = sourceQMS082.ReferenceReflectorDistance;
                                                    objQMS082.CalibrationBlockID = sourceQMS082.CalibrationBlockID;
                                                    objQMS082.CalibrationBlockIDThickness = sourceQMS082.CalibrationBlockIDThickness;
                                                    objQMS082.ScanningDetail = sourceQMS082.ScanningDetail;
                                                    objQMS082.EncoderType = sourceQMS082.EncoderType;
                                                    objQMS082.ScanPlan = sourceQMS082.ScanPlan;
                                                    objQMS082.ScannerType = sourceQMS082.ScannerType;
                                                    objQMS082.ScannerMake = sourceQMS082.ScannerMake;
                                                    objQMS082.CableType = sourceQMS082.CableType;
                                                    objQMS082.CableLength = sourceQMS082.CableLength;
                                                    objQMS082.PlannerRemarks = sourceQMS082.PlannerRemarks;
                                                    objQMS082.ApproverRemarks = sourceQMS082.ApproverRemarks;
                                                    objQMS082.TestInstrument = sourceQMS082.TestInstrument;
                                                    objQMS082.ExaminationDate = sourceQMS082.ExaminationDate;
                                                    objQMS082.NDTDuration = sourceQMS082.NDTDuration;
                                                    objQMS082.DataSamplingSpacing = sourceQMS082.DataSamplingSpacing;
                                                    objQMS082.DataStorageLocation = sourceQMS082.DataStorageLocation;
                                                    objQMS082.ScanningStartLocation = sourceQMS082.ScanningStartLocation;
                                                    objQMS082.EncoderCalibration = sourceQMS082.EncoderCalibration;
                                                    objQMS082.Software = sourceQMS082.Software;
                                                    objQMS082.Version = sourceQMS082.Version;
                                                    objQMS082.ScanningSurface = sourceQMS082.ScanningSurface;
                                                    objQMS082.FinalDisplayProcessingLevels = sourceQMS082.FinalDisplayProcessingLevels;
                                                    objQMS082.CompAscanUT = sourceQMS082.CompAscanUT;
                                                    objQMS082.AfterTestCalibrationrechecked = sourceQMS082.AfterTestCalibrationrechecked;
                                                    objQMS082.AnySpecialAccessoriesUsed = sourceQMS082.AnySpecialAccessoriesUsed;
                                                    objQMS082.AnyRecordableIndication = sourceQMS082.AnyRecordableIndication;
                                                    objQMS082.NDTremarks = sourceQMS082.NDTremarks;
                                                    objQMS082.CreatedBy = objClsLoginInfo.UserName;
                                                    objQMS082.CreatedOn = DateTime.Now;
                                                    objQMS082.EditedBy = objClsLoginInfo.UserName;
                                                    objQMS082.EditedOn = DateTime.Now;
                                                    db.QMS082.Add(objQMS082);
                                                    db.SaveChanges();

                                                    var sourceQMS083 = sourceQMS082.QMS083.ToList();
                                                    if (sourceQMS083 != null && sourceQMS083.Count > 0)
                                                    {
                                                        foreach (var item in sourceQMS083)
                                                        {
                                                            QMS083 newQMS083 = new QMS083();
                                                            newQMS083.HeaderId = objQMS082.HeaderId;
                                                            newQMS083.RefHeaderId = objQMS082.RefHeaderId;
                                                            newQMS083.QualityProject = objQMS082.QualityProject;
                                                            newQMS083.Project = objQMS082.Project;
                                                            newQMS083.BU = objQMS082.BU;
                                                            newQMS083.Location = objQMS082.Location;
                                                            newQMS083.SeamNo = objQMS082.SeamNo;
                                                            newQMS083.StageCode = objQMS082.StageCode;
                                                            newQMS083.StageSequence = objQMS082.StageSequence;
                                                            newQMS083.IterationNo = objQMS082.IterationNo;
                                                            newQMS083.RequestNo = objQMS082.RequestNo;
                                                            newQMS083.RequestNoSequence = objQMS082.RequestNoSequence;
                                                            newQMS083.UTTechNo = objQMS082.UTTechNo;
                                                            newQMS083.RevNo = objQMS082.RevNo;
                                                            newQMS083.ScanNo = item.ScanNo;
                                                            newQMS083.SearchUnitSerialNo = item.SearchUnitSerialNo;
                                                            newQMS083.ScanType = item.ScanType;
                                                            newQMS083.PCS = item.PCS;
                                                            newQMS083.ReferencegaindB = item.ReferencegaindB;
                                                            newQMS083.CreatedBy = objClsLoginInfo.UserName;
                                                            newQMS083.CreatedOn = DateTime.Now;
                                                            newQMS083.EditedBy = objClsLoginInfo.UserName;
                                                            newQMS083.EditedOn = DateTime.Now;
                                                            db.QMS083.Add(newQMS083);
                                                        }
                                                        db.SaveChanges();
                                                    }
                                                }
                                                break;
                                            }
                                        case "PAUT":
                                            {

                                                QMS084 objQMS084 = db.QMS084.Where(x => x.RefHeaderId == Destination.RequestId).FirstOrDefault();
                                                var sourceQMS084 = db.QMS084.Where(x => x.RefHeaderId == Source.RequestId).FirstOrDefault();
                                                if (objQMS084 == null && sourceQMS084 != null)
                                                {
                                                    objQMS084 = new QMS084();
                                                    objQMS084.RefHeaderId = Destination.RequestId;
                                                    objQMS084.QualityProject = Destination.QualityProject;
                                                    objQMS084.Project = Destination.Project;
                                                    objQMS084.BU = Destination.BU;
                                                    objQMS084.Location = Destination.Location;
                                                    objQMS084.SeamNo = Destination.SeamNo;
                                                    objQMS084.StageCode = Destination.StageCode;
                                                    objQMS084.StageSequence = Destination.StageSequence;
                                                    objQMS084.IterationNo = Destination.IterationNo;
                                                    objQMS084.RequestNo = Destination.RequestNo;
                                                    objQMS084.RequestNoSequence = Destination.RequestNoSequence;
                                                    objQMS084.UTTechNo = Destination.NDETechniqueNo;
                                                    objQMS084.RevNo = Destination.NDETechniqueRevisionNo;
                                                    objQMS084.Status = sourceQMS084.Status;
                                                    objQMS084.UTProcedureNo = sourceQMS084.UTProcedureNo;
                                                    objQMS084.CouplantUsed = sourceQMS084.CouplantUsed;
                                                    objQMS084.ReferenceReflectorSize = sourceQMS084.ReferenceReflectorSize;
                                                    objQMS084.ReferenceReflectorDistance = sourceQMS084.ReferenceReflectorDistance;
                                                    objQMS084.EncoderType = sourceQMS084.EncoderType;
                                                    objQMS084.CalibrationBlockID = sourceQMS084.CalibrationBlockID;
                                                    objQMS084.CalibrationBlockIDThickness = sourceQMS084.CalibrationBlockIDThickness;
                                                    objQMS084.SimulationBlockID = sourceQMS084.SimulationBlockID;
                                                    objQMS084.SimulationBlockIDThickness = sourceQMS084.SimulationBlockIDThickness;
                                                    objQMS084.ScannerType = sourceQMS084.ScannerType;
                                                    objQMS084.ScannerMake = sourceQMS084.ScannerMake;
                                                    objQMS084.CableType = sourceQMS084.CableType;
                                                    objQMS084.CableLength = sourceQMS084.CableLength;
                                                    objQMS084.ScanPlan = sourceQMS084.ScanPlan;
                                                    objQMS084.PlannerRemarks = sourceQMS084.PlannerRemarks;
                                                    objQMS084.ApproverRemarks = sourceQMS084.ApproverRemarks;
                                                    objQMS084.TestInstrument = sourceQMS084.TestInstrument;
                                                    objQMS084.ExaminationDate = sourceQMS084.ExaminationDate;
                                                    objQMS084.NDTDuration = sourceQMS084.NDTDuration;
                                                    objQMS084.DataSamplingSpacing = sourceQMS084.DataSamplingSpacing;
                                                    objQMS084.DataStorageLocation = sourceQMS084.DataStorageLocation;
                                                    objQMS084.ScanningStartLocation = sourceQMS084.ScanningStartLocation;
                                                    objQMS084.EncoderCalibration = sourceQMS084.EncoderCalibration;
                                                    objQMS084.Software = sourceQMS084.Software;
                                                    objQMS084.Version = sourceQMS084.Version;
                                                    objQMS084.FinalDisplayProcessingLevels = sourceQMS084.FinalDisplayProcessingLevels;
                                                    objQMS084.CompAscanUT = sourceQMS084.CompAscanUT;
                                                    objQMS084.AfterTestCalibrationrechecked = sourceQMS084.AfterTestCalibrationrechecked;
                                                    objQMS084.AnySpecialAccessoriesUsed = sourceQMS084.AnySpecialAccessoriesUsed;
                                                    objQMS084.AnyRecordableIndication = sourceQMS084.AnyRecordableIndication;
                                                    objQMS084.NDTremarks = sourceQMS084.NDTremarks;
                                                    objQMS084.CreatedBy = objClsLoginInfo.UserName;
                                                    objQMS084.CreatedOn = DateTime.Now;
                                                    objQMS084.EditedBy = objClsLoginInfo.UserName;
                                                    objQMS084.EditedOn = DateTime.Now;
                                                    db.QMS084.Add(objQMS084);
                                                    db.SaveChanges();

                                                    var sourceQMS085 = sourceQMS084.QMS085.ToList();

                                                    if (sourceQMS085 != null && sourceQMS085.Count > 0)
                                                    {
                                                        foreach (var item in sourceQMS085)
                                                        {
                                                            QMS085 newQMS085 = new QMS085();
                                                            newQMS085.HeaderId = objQMS084.HeaderId;
                                                            newQMS085.RefHeaderId = objQMS084.RefHeaderId;
                                                            newQMS085.QualityProject = objQMS084.QualityProject;
                                                            newQMS085.Project = objQMS084.Project;
                                                            newQMS085.BU = objQMS084.BU;
                                                            newQMS085.Location = objQMS084.Location;
                                                            newQMS085.SeamNo = objQMS084.SeamNo;
                                                            newQMS085.StageCode = objQMS084.StageCode;
                                                            newQMS085.StageSequence = objQMS084.StageSequence;
                                                            newQMS085.IterationNo = objQMS084.IterationNo;
                                                            newQMS085.RequestNo = objQMS084.RequestNo;
                                                            newQMS085.RequestNoSequence = objQMS084.RequestNoSequence;
                                                            newQMS085.UTTechNo = objQMS084.UTTechNo;
                                                            newQMS085.RevNo = objQMS084.RevNo;
                                                            newQMS085.ZoneNo = item.ZoneNo;
                                                            newQMS085.Probe = item.Probe;
                                                            newQMS085.Wedge = item.Wedge;
                                                            newQMS085.NearRefractedArea = item.NearRefractedArea;
                                                            newQMS085.Skip = item.Skip;
                                                            newQMS085.ScanType = item.ScanType;
                                                            newQMS085.ScannedSpots = item.ScannedSpots;
                                                            newQMS085.ScannedLength = item.ScannedLength;
                                                            newQMS085.ScanningSurface = item.ScanningSurface;
                                                            newQMS085.ReferencegaindB = item.ReferencegaindB;
                                                            newQMS085.TCGIndicationAmplitude = item.TCGIndicationAmplitude;
                                                            newQMS085.CreatedBy = objClsLoginInfo.UserName;
                                                            newQMS085.CreatedOn = DateTime.Now;
                                                            newQMS085.EditedBy = objClsLoginInfo.UserName;
                                                            newQMS085.EditedOn = DateTime.Now;
                                                            db.QMS085.Add(newQMS085);
                                                            db.SaveChanges();

                                                            var sourceQMS086 = item.QMS086.ToList();

                                                            if (sourceQMS086 != null && sourceQMS086.Count > 0)
                                                            {
                                                                foreach (var subitem in sourceQMS086)
                                                                {
                                                                    QMS086 newQMS086 = new QMS086();
                                                                    newQMS086.LineId = newQMS085.LineId;
                                                                    newQMS086.HeaderId = newQMS085.HeaderId;
                                                                    newQMS086.RefHeaderId = newQMS085.RefHeaderId;
                                                                    newQMS086.QualityProject = newQMS085.QualityProject;
                                                                    newQMS086.Project = newQMS085.Project;
                                                                    newQMS086.BU = newQMS085.BU;
                                                                    newQMS086.Location = newQMS085.Location;
                                                                    newQMS086.SeamNo = newQMS085.SeamNo;
                                                                    newQMS086.StageCode = newQMS085.StageCode;
                                                                    newQMS086.StageSequence = newQMS085.StageSequence;
                                                                    newQMS086.IterationNo = newQMS085.IterationNo;
                                                                    newQMS086.RequestNo = newQMS085.RequestNo;
                                                                    newQMS086.RequestNoSequence = newQMS085.RequestNoSequence;
                                                                    newQMS086.UTTechNo = newQMS085.UTTechNo;
                                                                    newQMS086.RevNo = newQMS085.RevNo;
                                                                    newQMS086.ZoneNo = subitem.ZoneNo;
                                                                    newQMS086.GroupNo = subitem.GroupNo;
                                                                    newQMS086.Angle = subitem.Angle;
                                                                    newQMS086.DepthCoverage = subitem.DepthCoverage;
                                                                    newQMS086.FocalDepth = subitem.FocalDepth;
                                                                    newQMS086.TCGIndicationAmplitude = subitem.TCGIndicationAmplitude;
                                                                    newQMS086.StartElement = subitem.StartElement;
                                                                    newQMS086.NumberofElements = subitem.NumberofElements;
                                                                    newQMS086.AngularIncrementalChange = subitem.AngularIncrementalChange;
                                                                    newQMS086.FocalPlane = subitem.FocalPlane;
                                                                    newQMS086.CreatedBy = objClsLoginInfo.UserName;
                                                                    newQMS086.CreatedOn = DateTime.Now;
                                                                    newQMS086.EditedBy = objClsLoginInfo.UserName;
                                                                    newQMS086.EditedOn = DateTime.Now;
                                                                    db.QMS086.Add(newQMS086);
                                                                }
                                                                db.SaveChanges();
                                                            }

                                                        }
                                                    }

                                                }
                                                break;
                                            }
                                        default:
                                            break;
                                    }
                                }

                            }

                            objResponseMsg.Key = true;
                            objResponseMsg.Value = "Copy data sucessfully done";
                        }
                        else
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = "No source data found";
                        }
                    }
                    else
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "NDE technique already linked";
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No data found";
                }
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json("error", JsonRequestBehavior.AllowGet);
            }
        }

        #region Partial for NOT ATTENDED REPORT STAGE TYPE
        public ActionResult GetNotAttendedSeamRequestReportPartial(string StageType)
        {

            return PartialView("_GetNotAttendedSeamRequestReportPartial");
        }

        #endregion


        #region  Observation 15957 on 02-08-2018
        [HttpPost]
        public ActionResult GetPTMTNDETechniques(string project, string qualityProject, string BU, string Location)
        {
            ViewBag.Project = project;
            ViewBag.QualityProject = qualityProject;
            ViewBag.BU = BU;
            ViewBag.Location = Location;
            return PartialView("_GetPTMTNDETechniques");
        }

        [HttpPost]
        public ActionResult GetPTMTNDEAutoTechniques(string term, string project, string qualityProject, string BU, string Location)
        {
            var lstTechniques = Manager.getPTMTNDETechniques(project, qualityProject, BU, Location);
            return Json(lstTechniques, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult SavePTMTNDETechnique(QMS060 qms060, string TechName)
        {
            TechniqueResponseMsg objResponseMsg = new TechniqueResponseMsg();
            try
            {
                string strURL = string.Empty;
                #region SEAM
                QMS060 objQMS060 = db.QMS060.Where(x => x.RequestId == qms060.RequestId).FirstOrDefault();
                if (objQMS060 != null)
                {
                    objQMS060.PTMTNDETechniqueNo = qms060.PTMTNDETechniqueNo;
                    objQMS060.PTMTNDETechniqueRevisionNo = qms060.PTMTNDETechniqueRevisionNo;
                    objQMS060.PTMTType = TechName;

                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "NDE Technique successfully linked.";
                    objResponseMsg.TechNo = objQMS060.PTMTNDETechniqueNo;
                    objResponseMsg.TechRev = Convert.ToInt32(objQMS060.PTMTNDETechniqueRevisionNo);
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "NDE offer not available.";
                }
                #endregion
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #endregion

        #region AscanUT
        //public ActionResult AscanUTIndexView()
        //{
        //    return View();
        //}
        //public ActionResult GetAscanUTDataPartial(string status)
        //{
        //    ViewBag.Status = status;
        //    return PartialView("_GetAscanUTDataPartial");
        //}
        //public ActionResult loadAscanUTHeaderDataTable(JQueryDataTableParamModel param, string status)
        //{
        //    try
        //    {
        //        int? StartIndex = param.iDisplayStart + 1;
        //        int? EndIndex = param.iDisplayStart + param.iDisplayLength;

        //        string whereCondition = "1=1";
        //        //if (status.ToUpper() == "PENDING")
        //        //{
        //        //    whereCondition += " and RequestNo IS NULL ";
        //        //}

        //        if (!string.IsNullOrWhiteSpace(param.sSearch))
        //        {
        //            string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.StageCode", "qms.RequestStatus", "qms.ManufacturingCode", "qms.AcceptanceStandard", "qms.ApplicableSpecification", "qms.InspectionExtent", "qms.WeldingEngineeringRemarks", "qms.WeldingProcess", "qms.SeamStatus", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark", "qms.SurfaceCondition", "qms.WeldThicknessReinforcement", "qms.SpotGenerationtype", "qms.PrintSummary", "qms.NDETechniqueNo", "qms.NDETechniqueRevisionNo" };
        //            whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
        //        }

        //        var lstHeader = db.SP_IPI_QMS080(StartIndex, EndIndex, "", whereCondition).ToList();

        //        int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
        //        var res = from h in lstHeader
        //                  select new[] {
        //                Convert.ToString(h.ROW_NO),
        //                Convert.ToString(h.HeaderId),
        //                Convert.ToString(h.RefHeaderId),
        //                Convert.ToString(h.QualityProject),
        //                Convert.ToString(h.Project),
        //                Convert.ToString(h.BU),
        //                Convert.ToString(h.Location),
        //                Convert.ToString(h.SeamNo),
        //                Convert.ToString(h.StageCode),
        //                Convert.ToString(h.StageSequence),
        //                Convert.ToString(h.IterationNo),
        //                Convert.ToString(h.RequestNo),
        //                Convert.ToString(h.RequestNoSequence),
        //                Convert.ToString(h.UTTechNo),
        //                Convert.ToString(h.RevNo),
        //                Convert.ToString(h.Status),
        //                Convert.ToString(h.UTProcedureNo),
        //                Convert.ToString(h.RecordableIndication),
        //                Convert.ToString(h.ScanTechnique),
        //                Convert.ToString(h.DGSDiagram),
        //                Convert.ToString(h.BasicCalibrationBlock),
        //                Convert.ToString(h.SimulationBlock1),
        //                Convert.ToString(h.SimulationBlock2),
        //                Convert.ToString(h.CouplantUsed),
        //                Convert.ToString(h.ScanPlan),
        //                Convert.ToString(h.ScanningSensitivity),
        //                Convert.ToString(h.PlannerRemarks),
        //                Convert.ToString(h.ApproverRemarks),
        //                Convert.ToString(h.TestInstrument),
        //                Convert.ToString(h.ExaminationDate),
        //                Convert.ToString(h.NDTDuration),
        //                Convert.ToString(h.Rejectcontrol),
        //                Convert.ToString(h.Damping),
        //                Convert.ToString(h.RestrictedAccessAreaInaccessibleWelds),
        //                Convert.ToString(h.TransferCorrection),
        //                Convert.ToString(h.TransferCorrectiondB),
        //                Convert.ToString(h.Noofpoints),
        //                Convert.ToString(h.AveragedB),
        //                Convert.ToString(h.AfterTestCalibrationrechecked),
        //                Convert.ToString(h.AnySpecialAccessoriesUsed),
        //                Convert.ToString(h.AnyRecordableIndication),
        //                Convert.ToString(h.NDTremarks),
        //                Convert.ToString(h.CreatedBy),
        //                Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
        //                Convert.ToString(h.EditedBy),
        //                Convert.ToDateTime(h.EditedOn).ToString("dd/MM/yyyy"),
        //               ""
        //            };
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
        //            iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
        //            aaData = res,
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalDisplayRecords = "0",
        //            iTotalRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        public ActionResult AscanUTHeader(int id)
        {
            NDEModels objNDEModels = new NDEModels();
            QMS080 objQMS080 = db.QMS080.Where(x => x.HeaderId == id).FirstOrDefault();
            objQMS080.ScanTechnique = objNDEModels.GetCategory("Scan Technique", objQMS080.ScanTechnique, objQMS080.BU, objQMS080.Location, true).CategoryDescription;
            objQMS080.CouplantUsed = objNDEModels.GetCategory("Couplant", objQMS080.CouplantUsed, objQMS080.BU, objQMS080.Location, true).CategoryDescription;
            objQMS080.SimulationBlock1 = objNDEModels.GetCategory("Simulation Block", objQMS080.SimulationBlock1, objQMS080.BU, objQMS080.Location, true).CategoryDescription;
            objQMS080.ScanningSensitivity = objNDEModels.GetCategory("Scanning Sensitivity", objQMS080.ScanningSensitivity, objQMS080.BU, objQMS080.Location, true).CategoryDescription;
            objQMS080.BasicCalibrationBlock = objNDEModels.GetCategory("Basic Calibration Block", objQMS080.BasicCalibrationBlock, objQMS080.BU, objQMS080.Location, true).CategoryDescription;
            objQMS080.RecordableIndication = objNDEModels.GetCategory("Recordable Indication At Reference Gain", objQMS080.RecordableIndication, objQMS080.BU, objQMS080.Location, true).CategoryDescription;

            objQMS080.Project = Manager.GetProjectAndDescription(objQMS080.Project);
            objQMS080.Location = db.COM002.Where(x => x.t_dimx == objQMS080.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS080.BU = db.COM002.Where(x => x.t_dimx == objQMS080.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();
            return View(objQMS080);
        }
        public ActionResult AscanUTLinesPartial(int headerId, int lineId)
        {
            QMS081 objQMS081 = db.QMS081.Where(x => x.HeaderId == headerId && x.LineId == lineId).FirstOrDefault();
            return View("_AscanUTLinesPartial", objQMS081);
        }

        [HttpPost]
        public bool checkSpotExists(int reqestid)
        {
            bool isMaintainSpot = false;

            if (db.QMS070.Any(c => c.RequestId == reqestid))
            {
                isMaintainSpot = true;
            }

            return isMaintainSpot;
        }

        [HttpPost]
        public bool checkStage(int reqestid)
        {
            QMS060 objQMS060 = db.QMS060.Where(x => x.RequestId == reqestid).FirstOrDefault();
            bool isMaintainFlag = false;

            if (!string.IsNullOrWhiteSpace(objQMS060.NDETechniqueNo))
            {
                string techName = objQMS060.NDETechniqueNo.Split('/')[1].ToString();
                string strURL = string.Empty;
                switch (techName.ToUpper())
                {
                    case "ASCAN":
                        QMS080 objQMS080 = db.QMS080.Where(x => x.RefHeaderId == objQMS060.RequestId).FirstOrDefault();
                        if (!string.IsNullOrWhiteSpace(objQMS080.EditedBy))
                        {
                            isMaintainFlag = true;
                        }
                        break;
                    case "TOFD":
                        QMS082 objQMS082 = db.QMS082.Where(x => x.RefHeaderId == objQMS060.RequestId).FirstOrDefault();
                        if (!string.IsNullOrWhiteSpace(objQMS082.EditedBy))
                        {
                            isMaintainFlag = true;
                        }
                        break;
                    case "PAUT":
                        QMS084 objQMS084 = db.QMS084.Where(x => x.RefHeaderId == objQMS060.RequestId).FirstOrDefault();
                        if (!string.IsNullOrWhiteSpace(objQMS084.EditedBy))
                        {
                            isMaintainFlag = true;
                        }
                        break;
                    default:
                        break;
                }
            }

            return isMaintainFlag;
        }

        public ActionResult loadAscanUTLinesDataTable(JQueryDataTableParamModel param, int? id)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition = " and HeaderId = " + id;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.StageCode", "qms.RequestStatus", "qms.ManufacturingCode", "qms.AcceptanceStandard", "qms.ApplicableSpecification", "qms.InspectionExtent", "qms.WeldingEngineeringRemarks", "qms.WeldingProcess", "qms.SeamStatus", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark", "qms.SurfaceCondition", "qms.WeldThicknessReinforcement", "qms.SpotGenerationtype", "qms.PrintSummary", "qms.NDETechniqueNo", "qms.NDETechniqueRevisionNo" };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }
                var lstHeader = db.SP_IPI_QMS081(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.ROW_NO),
                        Convert.ToString(h.LineId),
                        Convert.ToString(h.HeaderId),
                        Convert.ToString(h.RefHeaderId),
                        Convert.ToString(h.UTTechNo),
                        Convert.ToString(h.RevNo),
                        Convert.ToString(h.SearchUnit),
                        Convert.ToString(h.ReferenceReflector),
                        Convert.ToString(h.CableType),
                        Convert.ToString(h.CableLength),
                        Convert.ToString(h.ReferencegaindB),
                        Convert.ToString(h.Range),
                        Convert.ToString(h.AmpInd1),
                        Convert.ToString(h.BPInd1),
                        Convert.ToString(h.AmpInd2),
                        Convert.ToString(h.BPInd2),
                        Convert.ToString(h.AmpInd3),
                        Convert.ToString(h.BPInd3),
                        Convert.ToString(h.AmpInd4),
                        Convert.ToString(h.BPInd4),
                        Convert.ToString(h.AmpInd5),
                        Convert.ToString(h.BPInd5),
                        Convert.ToString(h.AmpInd6),
                        Convert.ToString(h.BPInd6),
                       ""
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region TOFDUT
        //public ActionResult TOFDUTIndexView()
        //{
        //    return View();
        //}
        //public ActionResult GetTOFDUTDataPartial(string status)
        //{
        //    ViewBag.Status = status;
        //    return PartialView("_GetTOFDUTDataPartial");
        //}
        //public ActionResult loadTOFDUTHeaderDataTable(JQueryDataTableParamModel param, string status)
        //{
        //    try
        //    {
        //        int? StartIndex = param.iDisplayStart + 1;
        //        int? EndIndex = param.iDisplayStart + param.iDisplayLength;

        //        string whereCondition = "1=1";
        //        //if (status.ToUpper() == "PENDING")
        //        //{
        //        //    whereCondition += " and RequestNo IS NULL ";
        //        //}

        //        if (!string.IsNullOrWhiteSpace(param.sSearch))
        //        {
        //            string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.StageCode", "qms.RequestStatus", "qms.ManufacturingCode", "qms.AcceptanceStandard", "qms.ApplicableSpecification", "qms.InspectionExtent", "qms.WeldingEngineeringRemarks", "qms.WeldingProcess", "qms.SeamStatus", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark", "qms.SurfaceCondition", "qms.WeldThicknessReinforcement", "qms.SpotGenerationtype", "qms.PrintSummary", "qms.NDETechniqueNo", "qms.NDETechniqueRevisionNo" };
        //            whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
        //        }

        //        var lstHeader = db.SP_IPI_QMS082(StartIndex, EndIndex, "", whereCondition).ToList();

        //        int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
        //        var res = from h in lstHeader
        //                  select new[] {
        //                Convert.ToString(h.ROW_NO),
        //                Convert.ToString(h.HeaderId),
        //                Convert.ToString(h.RefHeaderId),
        //                Convert.ToString(h.QualityProject),
        //                Convert.ToString(h.Project),
        //                Convert.ToString(h.BU),
        //                Convert.ToString(h.Location),
        //                Convert.ToString(h.SeamNo),
        //                Convert.ToString(h.StageCode),
        //                Convert.ToString(h.StageSequence),
        //                Convert.ToString(h.IterationNo),
        //                Convert.ToString(h.RequestNo),
        //                Convert.ToString(h.RequestNoSequence),
        //                Convert.ToString(h.UTTechNo),
        //                Convert.ToString(h.RevNo),
        //                Convert.ToString(h.Status),
        //                Convert.ToString(h.UTProcedureNo),
        //                Convert.ToString(h.CouplantUsed),
        //                Convert.ToString(h.ReferenceReflectorSize),
        //                Convert.ToString(h.ReferenceReflectorDistance),
        //                Convert.ToString(h.CalibrationBlockID),
        //                Convert.ToString(h.CalibrationBlockIDThickness),
        //                Convert.ToString(h.ScanningDetail),
        //                Convert.ToString(h.EncoderType),
        //                Convert.ToString(h.ScannerType),
        //                Convert.ToString(h.ScannerMake),
        //                Convert.ToString(h.CableType),
        //                Convert.ToString(h.CableLength),
        //                Convert.ToString(h.ScanPlan),
        //                Convert.ToString(h.PlannerRemarks),
        //                Convert.ToString(h.ApproverRemarks),
        //                Convert.ToString(h.TestInstrument),
        //                Convert.ToString(h.ExaminationDate),
        //                Convert.ToString(h.NDTDuration),
        //                Convert.ToString(h.DataSamplingSpacing),
        //                Convert.ToString(h.DataStorageLocation),
        //                Convert.ToString(h.ScanningStartLocation),
        //                Convert.ToString(h.EncoderCalibration),
        //                Convert.ToString(h.Software),
        //                Convert.ToString(h.Version),
        //                Convert.ToString(h.ScanningSurface),
        //                Convert.ToString(h.FinalDisplayProcessingLevels),
        //                Convert.ToString(h.CompAscanUT),
        //                Convert.ToString(h.AfterTestCalibrationrechecked),
        //                Convert.ToString(h.AnySpecialAccessoriesUsed),
        //                Convert.ToString(h.AnyRecordableIndication),
        //                Convert.ToString(h.NDTremarks),
        //                Convert.ToString(h.CreatedBy),
        //                Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
        //                Convert.ToString(h.EditedBy),
        //                Convert.ToDateTime(h.EditedOn).ToString("dd/MM/yyyy"),
        //                ""
        //            };
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
        //            iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
        //            aaData = res,
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalDisplayRecords = "0",
        //            iTotalRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        public ActionResult TOFDUTHeader(int id)
        {
            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            QMS082 objQMS082 = db.QMS082.Where(x => x.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();
            objQMS082.CouplantUsed = objNDEModels.GetCategory("Couplant", objQMS082.CouplantUsed, objQMS082.BU, objQMS082.Location, true).CategoryDescription;
            objQMS082.CableType = objNDEModels.GetCategory("Cable Type", objQMS082.CableType, objQMS082.BU, objQMS082.Location, true).CategoryDescription;
            objQMS082.CableLength = objNDEModels.GetCategory("Cable Length", objQMS082.CableLength, objQMS082.BU, objQMS082.Location, true).CategoryDescription;
            objQMS082.ScanningDetail = objNDEModels.GetCategory("Scanning Detail", objQMS082.ScanningDetail, objQMS082.BU, objQMS082.Location, true).CategoryDescription;
            objQMS082.ReferenceReflectorSize = objNDEModels.GetCategory("Reference Reflector Size", objQMS082.ReferenceReflectorSize, objQMS082.BU, objQMS082.Location, true).CategoryDescription;

            ViewBag.lstyesno = lstyesno.AsEnumerable().Select(x => new SelectListItem() { Text = x.ToString(), Value = x.ToString() }).ToList();

            objQMS082.Project = Manager.GetProjectAndDescription(objQMS082.Project);
            objQMS082.Location = db.COM002.Where(x => x.t_dimx == objQMS082.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS082.BU = db.COM002.Where(x => x.t_dimx == objQMS082.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            return View(objQMS082);

        }

        public ActionResult TOFDUTLinesPartial(int headerId, int lineId)
        {
            QMS083 objQMS083 = db.QMS083.Where(x => x.HeaderId == headerId && x.LineId == lineId).FirstOrDefault();
            return PartialView("_TOFDUTLinesPartial", objQMS083);
        }
        public ActionResult loadTOFDUTLinesDataTable(JQueryDataTableParamModel param, int? id)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition = " and HeaderId = " + id;

                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.StageCode", "qms.RequestStatus", "qms.ManufacturingCode", "qms.AcceptanceStandard", "qms.ApplicableSpecification", "qms.InspectionExtent", "qms.WeldingEngineeringRemarks", "qms.WeldingProcess", "qms.SeamStatus", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark", "qms.SurfaceCondition", "qms.WeldThicknessReinforcement", "qms.SpotGenerationtype", "qms.PrintSummary", "qms.NDETechniqueNo", "qms.NDETechniqueRevisionNo" };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                var lstHeader = db.SP_IPI_QMS083(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.ROW_NO),
                        Convert.ToString(h.LineId),
                        Convert.ToString(h.HeaderId),
                        Convert.ToString(h.RefHeaderId),
                        Convert.ToString(h.UTTechNo),
                        Convert.ToString(h.RevNo),
                        Convert.ToString(h.ScanNo),
                        Convert.ToString(h.SearchUnit),
                        Convert.ToString(h.ScanType),
                        Convert.ToString(h.PCS),
                        Convert.ToString(h.ReferencegaindB),
                        Convert.ToString(h.QualityProject),
                        Convert.ToString(h.Project),
                        Convert.ToString(h.BU),
                        Convert.ToString(h.Location),
                        Convert.ToString(h.SeamNo),
                        Convert.ToString(h.StageCode),
                        Convert.ToString(h.StageSequence),
                        Convert.ToString(h.IterationNo),
                        Convert.ToString(h.RequestNo),
                        Convert.ToString(h.RequestNoSequence),
                        Convert.ToString(h.CreatedBy),
                        Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                        Convert.ToString(h.EditedBy),
                        Convert.ToDateTime(h.EditedOn).ToString("dd/MM/yyyy"),
                        ""
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion

        #region PAUT
        //public ActionResult PAUTIndexView()
        //{
        //    return View();
        //}

        //public ActionResult GetPAUTDataPartial(string status)
        //{
        //    ViewBag.Status = status;
        //    return PartialView("_GetPAUTDataPartial");
        //}

        //public ActionResult loadPAUTHeaderDataTable(JQueryDataTableParamModel param, string status)
        //{
        //    try
        //    {
        //        int? StartIndex = param.iDisplayStart + 1;
        //        int? EndIndex = param.iDisplayStart + param.iDisplayLength;

        //        string whereCondition = "1=1";
        //        //if (status.ToUpper() == "PENDING")
        //        //{
        //        //    whereCondition += " and RequestNo IS NULL ";
        //        //}

        //        if (!string.IsNullOrWhiteSpace(param.sSearch))
        //        {
        //            string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.StageCode", "qms.RequestStatus", "qms.ManufacturingCode", "qms.AcceptanceStandard", "qms.ApplicableSpecification", "qms.InspectionExtent", "qms.WeldingEngineeringRemarks", "qms.WeldingProcess", "qms.SeamStatus", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark", "qms.SurfaceCondition", "qms.WeldThicknessReinforcement", "qms.SpotGenerationtype", "qms.PrintSummary", "qms.NDETechniqueNo", "qms.NDETechniqueRevisionNo" };
        //            whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
        //        }

        //        var lstHeader = db.SP_IPI_QMS084(StartIndex, EndIndex, "", whereCondition).ToList();

        //        int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
        //        var res = from h in lstHeader
        //                  select new[] {
        //                Convert.ToString(h.ROW_NO),
        //                Convert.ToString(h.HeaderId),
        //                Convert.ToString(h.RefHeaderId),
        //                Convert.ToString(h.QualityProject),
        //                Convert.ToString(h.Project),
        //                Convert.ToString(h.BU),
        //                Convert.ToString(h.Location),
        //                Convert.ToString(h.SeamNo),
        //                Convert.ToString(h.StageCode),
        //                Convert.ToString(h.StageSequence),
        //                Convert.ToString(h.IterationNo),
        //                Convert.ToString(h.RequestNo),
        //                Convert.ToString(h.RequestNoSequence),
        //                Convert.ToString(h.UTTechNo),
        //                Convert.ToString(h.RevNo),
        //                Convert.ToString(h.Status),
        //                Convert.ToString(h.UTProcedureNo),
        //                Convert.ToString(h.CouplantUsed),
        //                Convert.ToString(h.ReferenceReflectorSize),
        //                Convert.ToString(h.ReferenceReflectorDistance),
        //                Convert.ToString(h.CalibrationBlockID),
        //                Convert.ToString(h.CalibrationBlockIDThickness),
        //                 Convert.ToString(h.SimulationBlockID),
        //                Convert.ToString(h.SimulationBlockIDThickness),
        //                Convert.ToString(h.EncoderType),
        //                Convert.ToString(h.ScannerType),
        //                Convert.ToString(h.ScannerMake),
        //                Convert.ToString(h.CableType),
        //                Convert.ToString(h.CableLength),
        //                Convert.ToString(h.ScanPlan),
        //                Convert.ToString(h.PlannerRemarks),
        //                Convert.ToString(h.ApproverRemarks),
        //                Convert.ToString(h.TestInstrument),
        //                Convert.ToString(h.ExaminationDate),
        //                Convert.ToString(h.NDTDuration),
        //                Convert.ToString(h.DataSamplingSpacing),
        //                Convert.ToString(h.DataStorageLocation),
        //                Convert.ToString(h.ScanningStartLocation),
        //                Convert.ToString(h.EncoderCalibration),
        //                Convert.ToString(h.Software),
        //                Convert.ToString(h.Version),
        //                Convert.ToString(h.ScanningSurface),
        //                Convert.ToString(h.FinalDisplayProcessingLevels),
        //                Convert.ToString(h.CompAscanUT),
        //                Convert.ToString(h.AfterTestCalibrationrechecked),
        //                Convert.ToString(h.AnySpecialAccessoriesUsed),
        //                Convert.ToString(h.AnyRecordableIndication),
        //                Convert.ToString(h.NDTremarks),
        //                Convert.ToString(h.CreatedBy),
        //                Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
        //                Convert.ToString(h.EditedBy),
        //                Convert.ToDateTime(h.EditedOn).ToString("dd/MM/yyyy"),
        //                ""
        //            };
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
        //            iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
        //            aaData = res,
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
        //        return Json(new
        //        {
        //            sEcho = param.sEcho,
        //            iTotalDisplayRecords = "0",
        //            iTotalRecords = "0",
        //            aaData = ""
        //        }, JsonRequestBehavior.AllowGet);
        //    }
        //}
        public ActionResult PAUTHeader(int id)
        {
            QMS084 objQMS084 = db.QMS084.Where(x => x.HeaderId == id).FirstOrDefault();
            NDEModels objNDEModels = new NDEModels();

            objQMS084.CouplantUsed = objNDEModels.GetCategory("Couplant", objQMS084.CouplantUsed, objQMS084.BU, objQMS084.Location, true).CategoryDescription;
            objQMS084.ReferenceReflectorSize = objNDEModels.GetCategory("Reflector Size", objQMS084.ReferenceReflectorSize, objQMS084.BU, objQMS084.Location, true).CategoryDescription;
            objQMS084.CableType = objNDEModels.GetCategory("Cable Type", objQMS084.CableType, objQMS084.BU, objQMS084.Location, true).CategoryDescription;
            objQMS084.CableLength = objNDEModels.GetCategory("Cable Length", objQMS084.CableLength, objQMS084.BU, objQMS084.Location, true).CategoryDescription;

            objQMS084.Project = Manager.GetProjectAndDescription(objQMS084.Project);
            objQMS084.Location = db.COM002.Where(x => x.t_dimx == objQMS084.Location).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            objQMS084.BU = db.COM002.Where(x => x.t_dimx == objQMS084.BU).Select(x => x.t_dimx + "-" + x.t_desc).FirstOrDefault();
            return View(objQMS084);
        }
        //public ActionResult PAUTLinesIndex(int id)
        //{
        //    QMS084 objQMS084 = db.QMS084.Where(x => x.HeaderId == id).FirstOrDefault();
        //    return View(objQMS084);
        //}
        public ActionResult loadPAUTLinesDataTable(JQueryDataTableParamModel param, int? id)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";
                whereCondition = " and HeaderId = " + id;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.StageCode", "qms.RequestStatus", "qms.ManufacturingCode", "qms.AcceptanceStandard", "qms.ApplicableSpecification", "qms.InspectionExtent", "qms.WeldingEngineeringRemarks", "qms.WeldingProcess", "qms.SeamStatus", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark", "qms.SurfaceCondition", "qms.WeldThicknessReinforcement", "qms.SpotGenerationtype", "qms.PrintSummary", "qms.NDETechniqueNo", "qms.NDETechniqueRevisionNo" };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                var lstHeader = db.SP_IPI_QMS085(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.ROW_NO),
                        Convert.ToString(h.LineId),
                        Convert.ToString(h.HeaderId),
                        Convert.ToString(h.RefHeaderId),
                        //Convert.ToString(h.UTTechNo),
                        //Convert.ToString(h.RevNo),
                        Convert.ToString(h.ZoneNo),
                        Convert.ToString(h.Probe),
                        Convert.ToString(h.Wedge),
                        Convert.ToString(h.NearRefractedArea),
                        Convert.ToString(h.Skip),
                        Convert.ToString(h.ScanType),
                        Convert.ToString(h.ScannedSpots),
                        Convert.ToString(h.ScannedLength),
                        Convert.ToString(h.ScanningSurface),
                        Convert.ToString(h.ReferencegaindB),
                        Convert.ToString(h.TCGIndicationAmplitude),
                        //Convert.ToString(h.QualityProject),
                        //Convert.ToString(h.Project),
                        //Convert.ToString(h.BU),
                        //Convert.ToString(h.Location),
                        //Convert.ToString(h.SeamNo),
                        //Convert.ToString(h.StageCode),
                        //Convert.ToString(h.StageSequence),
                        //Convert.ToString(h.IterationNo),
                        //Convert.ToString(h.RequestNo),
                        //Convert.ToString(h.RequestNoSequence),
                        //Convert.ToString(h.CreatedBy),
                        //Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                        //Convert.ToString(h.EditedBy),
                        //Convert.ToDateTime(h.EditedOn).ToString("dd/MM/yyyy"),
                        ""
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //public ActionResult PAUTLinesView()
        //{
        //    return View();
        //}
        public ActionResult PAUTSubLinesView(int sublineId, int lineId, int headerId)
        {
            QMS086 objQMS086 = db.QMS086.Where(x => x.SubLineId == sublineId && x.LineId == lineId && x.HeaderId == headerId).FirstOrDefault();
            return PartialView("_PAUTSubLinesView", objQMS086);
        }

        public ActionResult loadPAUTSubLinesDataTable(JQueryDataTableParamModel param, int? id, int? lineId, int? sublineId)
        {
            try
            {
                int? StartIndex = param.iDisplayStart + 1;
                int? EndIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition = " and HeaderId = " + id + " and SubLineId = " + sublineId + " and LineId = " + lineId;
                if (!string.IsNullOrWhiteSpace(param.sSearch))
                {
                    string[] columnName = { "qms.SeamNo", "qms.QualityProject", "qms.Project+'-'+com1.t_dsca", "qms.StageCode", "qms.RequestStatus", "qms.ManufacturingCode", "qms.AcceptanceStandard", "qms.ApplicableSpecification", "qms.InspectionExtent", "qms.WeldingEngineeringRemarks", "qms.WeldingProcess", "qms.SeamStatus", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark", "qms.SurfaceCondition", "qms.WeldThicknessReinforcement", "qms.SpotGenerationtype", "qms.PrintSummary", "qms.NDETechniqueNo", "qms.NDETechniqueRevisionNo" };
                    whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);
                }

                var lstHeader = db.SP_IPI_QMS086(StartIndex, EndIndex, "", whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                        Convert.ToString(h.ROW_NO),
                        Convert.ToString(h.SubLineId),
                        Convert.ToString(h.LineId),
                        Convert.ToString(h.HeaderId),
                        Convert.ToString(h.RefHeaderId),
                        Convert.ToString(h.UTTechNo),
                        Convert.ToString(h.RevNo),
                        Convert.ToString(h.ZoneNo),
                        Convert.ToString(h.GroupNo),
                        Convert.ToString(h.Angle),
                        Convert.ToString(h.DepthCoverage),
                        Convert.ToString(h.FocalDepth),
                        Convert.ToString(h.TCGIndicationAmplitude),
                        Convert.ToString(h.StartElement),
                        Convert.ToString(h.NumberofElements),
                        Convert.ToString(h.AngularIncrementalChange),
                        Convert.ToString(h.FocalPlane),
                        Convert.ToString(h.QualityProject),
                        Convert.ToString(h.Project),
                        Convert.ToString(h.BU),
                        Convert.ToString(h.Location),
                        Convert.ToString(h.SeamNo),
                        Convert.ToString(h.StageCode),
                        Convert.ToString(h.StageSequence),
                        Convert.ToString(h.IterationNo),
                        Convert.ToString(h.RequestNo),
                        Convert.ToString(h.RequestNoSequence),
                        Convert.ToString(h.CreatedBy),
                        Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                        Convert.ToString(h.EditedBy),
                        Convert.ToDateTime(h.EditedOn).ToString("dd/MM/yyyy"),
                        ""
                    };
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        #endregion
        public static string MultiSelectDropdown(int rowId, List<SelectItemList> list, string selectedValue, bool Disabled = false, string OnChangeEvent = "", string columnName = "")
        {
            string multipleSelect = string.Empty;
            string[] arrayVal = { };

            try
            {
                if (!string.IsNullOrWhiteSpace(selectedValue))
                {
                    arrayVal = selectedValue.Split(',');
                }
                string inputID = columnName + "" + rowId.ToString();

                multipleSelect += "<select name=" + inputID + " id=" + inputID + " multiple='multiple'  style='width: 100 % ' colname='" + columnName + "' class='form-control multiselect' " + (Disabled ? "disabled " : "") + (OnChangeEvent != string.Empty ? "onchange=" + OnChangeEvent + "" : "") + "  >";
                foreach (var item in list)
                {
                    multipleSelect += "<option value='" + item.id + "' spara='" + rowId + "' " + ((arrayVal.Contains(item.id)) ? " selected" : "") + " >" + item.text + "</option>";
                }
                multipleSelect += "</select>";
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return multipleSelect;
        }



        //Createby Vinod
        public ActionResult GroupClearancePartial()
        {
            ViewBag.MaxRevisionNo = 0;
            ViewBag.Location = "";
            ViewBag.BU = "";
            string whereCondition = "1=1";
            whereCondition += " and (RequestStatus like '%Not Attended%')";
            whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");

            List<SP_IPI_SEAMLISTNDETEST_DETAILS_Result> lstClearanceSeam = db.SP_IPI_SEAMLISTNDETEST_DETAILS(1, int.MaxValue, "", whereCondition).ToList();
            List<string> lstDistinctQualityProject = lstClearanceSeam.Select(x => x.QualityProject).Distinct().ToList();
            List<lststages> lstDistinctStages = lstClearanceSeam.Select(x => new lststages { StageCode = x.StageCode, BU = x.BU, Location = x.Location }).Distinct().ToList();
            List<BULocWiseCategoryModel> lstQualityProject = lstDistinctQualityProject.Select(x => new BULocWiseCategoryModel { CatDesc = (x.ToString()), CatID = x.ToString() }).ToList();
            ViewBag.lstQualityProject = new JavaScriptSerializer().Serialize(lstQualityProject);

            lstDistinctStages.ForEach(x =>
            {
                x.StageDesc = x.StageCode;
                x.Location = !string.IsNullOrWhiteSpace(x.Location) ? x.Location.Split('-')[0] : "";
                x.StageCode = !string.IsNullOrWhiteSpace(x.StageCode) ? x.StageCode.Split('-')[0] : "";
                x.BU = !string.IsNullOrWhiteSpace(x.BU) ? x.BU.Split('-')[0] : "";
            });

            var lstStages = lstDistinctStages.Where(c => c.StageCode != null || c.StageCode != "")
                            .Select(x => new { StageDesc = (x.StageCode), StageCode = x.StageCode, BU = x.BU, Location = x.Location }).ToList();

            string StageTypeExludedinOfferDropdown = clsImplementationEnum.ConfigParameter.StageTypeExludedinOfferDropdown.GetStringValue();
            var lstStagetypes = Manager.GetConfigValue(StageTypeExludedinOfferDropdown).ToUpper().Split(',').ToArray();
            var lstClearStages = (from stg in lstStages
                                  join q02 in db.QMS002 on new { stg.StageCode, stg.BU, stg.Location } equals new { q02.StageCode, q02.BU, q02.Location }
                                  //where !lstStagetypes.Contains(q02.StageType.ToUpper())
                                  select new { CatDesc = stg.StageDesc, CatID = q02.StageCode }).Distinct().ToList();
            ViewBag.lstStages = new JavaScriptSerializer().Serialize(lstClearStages);


            return PartialView("_GroupClearancePartial");
        }

        public ActionResult loadSeamListGroupClearanceDataTable(JQueryDataTableParamModel param, string qualityProject, string stageCode)
        {
            try
            {
                //objClsLoginInfo.Location = "HZW";

                string BU = db.QMS060.Where(x => x.QualityProject == qualityProject && x.Location == objClsLoginInfo.Location).Select(x => x.BU).FirstOrDefault();
                //string BU = db.QMS060.Where(x => x.QualityProject == qualityProject).Select(x => x.BU).FirstOrDefault();
                var IsProtocolApplicable = db.QMS002.Where(x => x.Location == objClsLoginInfo.Location && x.BU == BU && x.StageCode == stageCode).Select(x => x.IsProtocolApplicable).FirstOrDefault();
                if (BU == null)
                {
                    BU = "";
                }
                List<GLB002> lstGLB002 = Manager.GetSubCatagories("Shop", BU, objClsLoginInfo.Location).ToList();
                List<BULocWiseCategoryModel> lstBULocWiseCategoryModel = lstGLB002.Select(x => new BULocWiseCategoryModel { CatDesc = (x.Code + "-" + x.Description), CatID = x.Code }).ToList();

                string whereCondition = "1=1";
                //whereCondition += " and (TestResult IS NULL or TestResult='Returned by TPI') and OfferedBy IS NOT NULL";
                whereCondition += " and qms.QualityProject in('" + qualityProject + "')";
                whereCondition += " and qms.StageCode in('" + stageCode + "')";
                whereCondition += " and (RequestStatus like '%Not Attended%')";

                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms.BU", "qms.Location");

                string[] columnName = { "qms.SeamNo", "dbo.GET_PROJECTDESC_BY_CODE(qms.Project)", "qms.OfferedBy", "qms.CreatedBy", "qms.Location", "dbo.GET_IPI_STAGECODE_DESCRIPTION(qms.StageCode, qms.BU, qms.Location) ", "qms.RequestNo", "qms.InspectedBy", "qms.QCRemarks", "qms.OfferedtoCustomerBy", "qms.Shop", "qms.ShopLocation", "qms.ShopRemark" };

                whereCondition += columnName.MakeDatatableSearchCondition(param.sSearch);

                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                var lstSeamList = db.SP_IPI_SEAMLISTNDETEST_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                var preheat = clsImplementationEnum.HeatTreatment.Pre.GetStringValue();
                var postheat = clsImplementationEnum.HeatTreatment.Post.GetStringValue();
                var objHeatTreatment = db.QMS002.Where(x => x.StageCode == stageCode && x.BU == BU && x.Location == objClsLoginInfo.Location).FirstOrDefault();
                bool ispreheat = false;
                bool ispostheat = false;
                bool isGroundspot = false;
                if (objHeatTreatment != null)
                {
                    ispreheat = (objHeatTreatment.PrePostWeldHeadTreatment == clsImplementationEnum.HeatTreatment.Pre.GetStringValue());
                    ispostheat = (objHeatTreatment.PrePostWeldHeadTreatment == clsImplementationEnum.HeatTreatment.Post.GetStringValue());
                    isGroundspot = (objHeatTreatment.IsGroundSpot) == true ? true : false;
                }

                string allowseamcategorygroupoffer = clsImplementationEnum.ConfigParameter.AllowSeamCategoryGroupOffer.GetStringValue();
                lstSeamList.ForEach(x =>
                {
                    x.Project = x.Project.Split('-')[0];
                });
                var lstAllowSeam = Manager.GetConfigValue(allowseamcategorygroupoffer).ToUpper().Split(',').ToArray();
                var lstHeader = (from lst in lstSeamList
                                 join seam in db.QMS012 on new { lst.QualityProject, lst.Project, lst.SeamNo } equals new { seam.QualityProject, seam.Project, seam.SeamNo }
                                 where (isGroundspot ? false : (ispreheat ? lstAllowSeam.Contains(seam.SeamCategory.Trim().ToUpper()) : true))
                                 select lst).ToList();
                //var lstHeader = db.SP_IPI_SEAMLISTNDETEST_DETAILS(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();

                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();
                var res = from h in lstHeader
                          select new[] {
                           Convert.ToString(h.RequestId)+'$'+Convert.ToString( h.NDETechniqueNo),
                           h.QualityProject,
                           Convert.ToString(h.RequestId),
                           Convert.ToString(h.HeaderId),
                           Convert.ToString( h.Project),
                           Convert.ToString( h.SeamNo),
                           Convert.ToString(h.StageCode),
                           Convert.ToString(h.StageType),
                           Convert.ToString(h.StageSequence),
                           Convert.ToString(h.IterationNo),
                           Convert.ToString(h.RequestNo),
                           Convert.ToString(h.RequestNoSequence),

                           Convert.ToString(h.RequestStatus),
                           Convert.ToString(h.SeamLength),
                           Convert.ToString(h.OverlayArea),
                           Convert.ToString(h.OverlayThickness),
                           Convert.ToString(h.Jointtype),
                           Convert.ToString(h.ManufacturingCode),
                           Convert.ToString(h.Part1Position),
                           Convert.ToString(h.Part1thickness),
                           Convert.ToString(h.Part1Material),
                           Convert.ToString(h.Part2Position),
                           Convert.ToString(h.Part2thickness),
                           Convert.ToString(h.Part2Material),
                           Convert.ToString(h.Part3Position),
                           Convert.ToString(h.Part3thickness),
                           Convert.ToString(h.Part3Material),
                           Convert.ToString(h.AcceptanceStandard),
                           Convert.ToString(h.ApplicableSpecification),
                           Convert.ToString(h.InspectionExtent),
                           Convert.ToString(h.WeldingEngineeringRemarks),
                           Convert.ToString(h.WeldingProcess),
                           Convert.ToString(h.SeamStatus),
                           Convert.ToString(h.Shop),
                           Convert.ToString(h.ShopLocation),
                           Convert.ToString(h.ShopRemark),
                           Convert.ToString(h.SurfaceCondition),
                           Convert.ToString(h.WeldThicknessReinforcement),
                           Convert.ToString(h.SpotGenerationtype),
                           Convert.ToString(h.SpotLength),
                           Convert.ToString(h.PrintSummary),
                           Convert.ToString( h.CreatedBy),
                           Convert.ToDateTime(h.CreatedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.RequestGeneratedBy),
                           Convert.ToDateTime(h.RequestGeneratedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.ConfirmedBy),
                           Convert.ToDateTime(h.ConfirmedOn).ToString("dd/MM/yyyy"),
                           Convert.ToString( h.NDETechniqueNo),
                           Convert.ToString( h.NDETechniqueRevisionNo)
                    };

                QMS060 objQMS060 = db.QMS060.OrderByDescending(u => u.NDETechniqueRevisionNo).FirstOrDefault();
                ViewBag.MaxRevisionNo = objQMS060.NDETechniqueRevisionNo;
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    iTotalRecords = totalRecords != null && totalRecords > 0 ? totalRecords : 0,
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = "",

                }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult SaveLinkTechnique(string QltyProject, string stage, string RequIds, string NDETechNo, string NDERevNo)
        {
            string Massage = "Something went wrong!";
            try
            {
                string[] ArrayReqId = RequIds.Split(',');
                foreach (string ReqID in ArrayReqId)
                {
                    int ReqID1 = Convert.ToInt32(ReqID);
                    int NDERevNo1 = Convert.ToInt32(NDERevNo);
                    QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == ReqID1);
                    if (objQMS060 != null)
                    {
                        objQMS060.NDETechniqueNo = NDETechNo;
                        objQMS060.NDETechniqueRevisionNo = NDERevNo1;
                        db.SaveChanges();
                    }

                }
                Massage = "Success";
            }
            catch (Exception ex)
            {
                Massage = ex.Message;
            }
            return Json(Massage);
        }

        [HttpPost]
        public ActionResult GetNDEAutoTechniques(string term, string qualityProject, string stageCode)
        {
            //qualityProject = "0047004/1";
            //stageCode = "IPI-STG2";
            string BU = db.QMS060.Where(x => x.QualityProject == qualityProject && x.Location == objClsLoginInfo.Location).Select(x => x.BU).FirstOrDefault();
            //objClsLoginInfo.Location = "HZW";
            string Location = objClsLoginInfo.Location;
            var lstTechniques = getNDETechniques(qualityProject, stageCode, BU, Location);
            return Json(lstTechniques, JsonRequestBehavior.AllowGet);
        }

        public List<clsNDETechniques> getNDETechniques(string qualityProject, string stageCode, string BU, string Location)
        {

            //stageCode = stageCode.Split('-')[0].Trim();

            List<clsNDETechniques> lstNDETechniques = new List<clsNDETechniques>();
            List<QMS002> lstQMS002 = db.QMS002.Where(x => x.BU == BU && x.Location == Location && x.StageCode == stageCode).ToList();
            if (lstQMS002 != null && lstQMS002.Count > 0)
            {
                List<string> lstStageType = new List<string>();
                string TecStatus = clsImplementationEnum.NDETechniqueStatus.Approved.GetStringValue();
                foreach (QMS002 objQMS002 in lstQMS002)
                {
                    lstStageType.Add(objQMS002.StageType);
                }
                foreach (string strStageType in lstStageType)
                {
                    switch (strStageType.ToUpper())
                    {
                        case "PT":
                            lstNDETechniques = db.NDE007_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "PT", TechniqueNo = x.PTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/PTTechniqueMaster/ViewHistory?id=" + x.Id }).ToList();
                            break;
                        case "MT":
                            lstNDETechniques = db.NDE008_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "MT", TechniqueNo = x.MTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/MaintainMTTechnique/ViewHistory?id=" + x.Id }).ToList();
                            break;
                        case "RT":
                            lstNDETechniques = db.NDE009_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "RT", TechniqueNo = x.RTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/MaintainRTTechnique/ViewHistory?id=" + x.Id }).ToList();
                            break;
                        case "UT":
                            List<clsNDETechniques> lstNDETechniquesAscan = db.NDE010_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "ASCAN UT", TechniqueNo = x.UTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/MaintainScanUTTech/ViewLogDetails?Id=" + x.Id }).ToList();

                            if (lstNDETechniquesAscan != null && lstNDETechniquesAscan.Count > 0)
                                lstNDETechniques.AddRange(lstNDETechniquesAscan);

                            List<clsNDETechniques> lstNDETechniquesTOFD = db.NDE012_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "TOFD UT", TechniqueNo = x.UTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/MaintainTOFDUTTech/ViewLogDetails?ID=" + x.Id }).ToList();

                            if (lstNDETechniquesTOFD != null && lstNDETechniquesTOFD.Count > 0)
                                lstNDETechniques.AddRange(lstNDETechniquesTOFD);

                            List<clsNDETechniques> lstNDETechniquesPAUT = db.NDE014_Log.Where(x => x.Status == TecStatus && x.QualityProject == qualityProject && x.BU == BU && x.Location == Location).Select(x => new clsNDETechniques { TechniqueName = "PAUT", TechniqueNo = x.UTTechNo, TechniqueDisplayNo = x.TechniqueSheetNo, TechniqueRev = x.RevNo, TechniqueUrl = WebsiteURL + "/NDE/MaintainPAUTTechnique/ViewHistory?id=" + x.Id }).ToList();

                            if (lstNDETechniquesPAUT != null && lstNDETechniquesPAUT.Count > 0)
                                lstNDETechniques.AddRange(lstNDETechniquesPAUT);

                            break;
                        default:
                            break;
                    }
                }
            }

            return lstNDETechniques;
        }

        //[HttpPost]
        //public ActionResult AddPAUTDetails(string rows, string QltyProject, string Project, string SeamNo, string StageCode,
        //    string Equipment, string ModelNo, string Software, string SrNo)
        //{

        //    DataTable dt = JsonConvert.DeserializeObject<DataTable>(rows);


        //    return Json("Success");
        //}


        public ActionResult GetPautData(int RequestId)
        {
            try
            {

                QMS060 objQMS060 = db.QMS060.FirstOrDefault(x => x.RequestId == RequestId);
                ViewBag.RequestID = objQMS060.RequestId;
                ViewBag.HeaderID = objQMS060.HeaderId;
                ViewBag.QltyProject = objQMS060.QualityProject;
                ViewBag.Project = objQMS060.Project;
                ViewBag.StageCode = objQMS060.StageCode;
                ViewBag.SeamNo = objQMS060.SeamNo;
                var otherData = db.QMS088.Where(x => x.RefHeaderId == RequestId);
                ViewBag.ListPaut = otherData;

                QMS087 objQMS087 = db.QMS087.FirstOrDefault(x => x.RefHeaderId == RequestId);
                if (objQMS087 != null)
                {
                    ViewBag.Equipment = objQMS087.Equipment;
                    ViewBag.Model = objQMS087.Model;
                    ViewBag.Software = objQMS087.Software;
                    ViewBag.SrNo = objQMS087.SrNo;
                }


                QMS080 objQMS080 = db.QMS080.Where(x => x.HeaderId == objQMS060.HeaderId).FirstOrDefault();
                if (objQMS080 != null)
                {
                    if (ISRequestAttended(objQMS080.RefHeaderId))
                    {
                        ViewBag.isDisabled = "disabled";
                    }
                    else
                    {
                        ViewBag.isDisabled = "enable";
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return View();
        }

        public ActionResult SavePautDetails(string QltyProject, string Project, string SeamNo, string StageCode,
            string Equipment, string ModelNo, string Software, string SrNo, string RequestID)
        {
            string Message = "Something went wrong";
            try
            {
                QMS087 objQMS087 = new QMS087();
                objQMS087.QualityProject = QltyProject;
                objQMS087.Project = Project;
                objQMS087.SeamNo = SeamNo;
                objQMS087.StageCode = StageCode;
                objQMS087.Equipment = Equipment;
                objQMS087.Model = ModelNo;
                objQMS087.Software = Software;
                objQMS087.SrNo = SrNo;
                objQMS087.RefHeaderId = Convert.ToInt32(RequestID);
                objQMS087.CreatedBy = objClsLoginInfo.UserName;
                objQMS087.CreatedOn = DateTime.Now;
                db.QMS087.Add(objQMS087);
                db.SaveChanges();
                int id = objQMS087.HeaderId;
                if (id > 0)
                {
                    QMS088 objQMS088 = new QMS088();
                    objQMS088.HeaderId = id;
                    objQMS088.RefHeaderId = Convert.ToInt32(RequestID);
                    objQMS088.QualityProject = QltyProject;
                    objQMS088.Project = Project;
                    objQMS088.SeamNo = SeamNo;
                    objQMS088.StageCode = StageCode;
                    objQMS088.CreatedBy = objClsLoginInfo.UserName;
                    objQMS088.CreatedOn = DateTime.Now;
                    db.QMS088.Add(objQMS088);
                    db.SaveChanges();
                }
                Message = "Success";
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return Json(Message);
        }

        public ActionResult SavePautLineDetails(string LineId, string Thickness, string Scan, string Technique,
            string Depth, string Probe, string Make, string Frequency, string Wedge
            , string Skip, string Angle, string Start, string Active, string Focus, string Gain, string RequestID)
        {


            string Message = "Something went wrong";
            try
            {
                int LineID1 = Convert.ToInt32(LineId);
                QMS088 objQMS088 = db.QMS088.FirstOrDefault(x => x.LineId == LineID1);

                if (objQMS088 != null)
                {
                    if (objQMS088.Thickness == null || objQMS088.Thickness == "")
                    {
                        QMS088 NewobjQMS088 = new QMS088();
                        NewobjQMS088.HeaderId = objQMS088.HeaderId;
                        NewobjQMS088.RefHeaderId = Convert.ToInt32(RequestID);
                        NewobjQMS088.QualityProject = objQMS088.QualityProject;
                        NewobjQMS088.Project = objQMS088.Project;
                        NewobjQMS088.SeamNo = objQMS088.SeamNo;
                        NewobjQMS088.StageCode = objQMS088.StageCode;
                        NewobjQMS088.CreatedBy = objClsLoginInfo.UserName;
                        NewobjQMS088.CreatedOn = DateTime.Now;
                        db.QMS088.Add(NewobjQMS088);
                        db.SaveChanges();
                    }
                    objQMS088.Thickness = Thickness;
                    objQMS088.ScanPlan = Scan;
                    objQMS088.TypeofTecnique = Technique;
                    objQMS088.DepthCoverage = Depth;
                    objQMS088.Probe = Probe;
                    objQMS088.Make = Make;
                    objQMS088.Frequency = Frequency;
                    objQMS088.Wedge = Wedge;
                    objQMS088.Skip = Skip;
                    objQMS088.Angle = Angle;
                    objQMS088.StartElement = Convert.ToInt32(Start);
                    objQMS088.ActiveElement = Convert.ToInt32(Active);
                    objQMS088.FocusDepth = Convert.ToInt32(Focus);
                    objQMS088.ReferenceGain = Convert.ToInt32(Gain);
                    objQMS088.EditedBy = objClsLoginInfo.UserName;
                    objQMS088.EditedOn = DateTime.Now;
                    db.SaveChanges();
                }

                Message = "Success";
            }
            catch (Exception ex)
            {
                Message = ex.Message;
            }
            return Json(Message);
        }

        public bool ISRequestAttended(int? reqId)
        {
            bool flag = false;
            string attended = clsImplementationEnum.NDESeamRequestStatus.Attended.GetStringValue();
            var attendedreq = db.QMS060.Where(x => x.RequestId == reqId && x.RequestStatus == attended).FirstOrDefault();
            if (attendedreq != null)
            { flag = true; }

            return flag;
        }
    }

}