﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQSImplementation.Models;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;
using System.Text;

namespace IEMQS.Areas.IPI.Controllers
{
    public class MaintainController : clsBase
    {
        // GET: IPI/Maintain
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetQualityProjectGridDataPartial(string status)
        {
            ViewBag.status = status;
            return PartialView("_GetQualityProjectGridDataPartial");
        }

        public ActionResult LoadHeaderDataTable(JQueryDataTableParamModel param, string status)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);
                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int? startIndex = param.iDisplayStart + 1;
                int? endIndex = param.iDisplayStart + param.iDisplayLength;

                string whereCondition = "1=1";
                whereCondition += Manager.MakeDefaultWhere(objClsLoginInfo.UserName, "qms10.BU", "qms10.Location");
                //if (status.ToUpper() == "PENDING")
                //{
                //    whereCondition += " and nde7.Status in('" + clsImplementationEnum.QCPStatus.Draft.GetStringValue() + "','" + clsImplementationEnum.QCPStatus.Returned.GetStringValue() + "')";
                //}
                if (!string.IsNullOrEmpty(param.sSearch))
                {
                    whereCondition += " and (bcom2.t_desc like '%" + param.sSearch + "%' or lcom2.t_desc like '%" + param.sSearch + "%' or qms10.QualityProject like '%" + param.sSearch + "%' or (qms10.Project+'-'+com1.t_dsca) like '%" + param.sSearch + "%')";
                }
                string strSortOrder = string.Empty;
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }

                var lstHeader = db.SP_IPI_GetQualityProject(startIndex, endIndex, strSortOrder, whereCondition).ToList();
                int? totalRecords = lstHeader.Select(i => i.TotalCount).FirstOrDefault();

                var res = from a in lstHeader
                          select new[] {
                        a.QualityProject,
                        a.Project,
                        a.BU,
                        a.Location,
                        a.CreatedBy,
                        Convert.ToString(a.CreatedOn),
                        Convert.ToString(a.Id)
                        //a.Status,
                    };

                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    iTotalRecords = (totalRecords != null && totalRecords > 0 ? totalRecords : 0),
                    aaData = res,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalDisplayRecords = "0",
                    iTotalRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }

        }

        #region Quality Project
        [SessionExpireFilter, UserPermissions, AllowAnonymous]
        public ActionResult AddQualityProject(int? id)
        {
            QMS010 objQMS010 = new QMS010();

            var location = (from a in db.COM003
                            join b in db.COM002 on a.t_loca equals b.t_dimx
                            where b.t_dtyp == 1 && a.t_psno.Equals(objClsLoginInfo.UserName, StringComparison.OrdinalIgnoreCase)
                            select b.t_dimx + " - " + b.t_desc).FirstOrDefault();

            ViewBag.Location = location;

            if (id > 0)
            {
                objQMS010 = db.QMS010.Where(i => i.Id == id).FirstOrDefault();

                ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objQMS010.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
                ViewBag.ManufacturingCode = new NDEModels().GetCategory(objQMS010.ManufacturingCode).CategoryDescription;
                ViewBag.SpecificationCode = new NDEModels().GetCategory(objQMS010.SpecificationCode).CategoryDescription;
                ViewBag.InspectionCentre = new NDEModels().GetCategory(objQMS010.InspectionCentre).CategoryDescription;
                if (!string.IsNullOrEmpty(objQMS010.Inspector1))
                    ViewBag.Inspector1 = db.COM003.Where(i => i.t_psno.Equals(objQMS010.Inspector1, StringComparison.OrdinalIgnoreCase) && i.t_actv == 1).Select(i => i.t_psno + "-" + i.t_name).FirstOrDefault();
                if (!string.IsNullOrEmpty(objQMS010.Inspector2))
                    ViewBag.Inspector2 = db.COM003.Where(i => i.t_psno.Equals(objQMS010.Inspector2, StringComparison.OrdinalIgnoreCase) && i.t_actv == 1).Select(i => i.t_psno + "-" + i.t_name).FirstOrDefault();

                bool Flag = false;
                Flag = db.QMS020.Where(x => x.QualityProject == objQMS010.QualityProject).FirstOrDefault() != null ? true : false;
                if (Flag)
                {
                    ViewBag.Flag = Flag;
                }
                else
                {
                    Flag = db.QMS035.Where(x => x.QualityProject == objQMS010.QualityProject).FirstOrDefault() != null ? true : false;
                    if (Flag)
                    {
                        ViewBag.Flag = Flag;
                    }
                    else
                    {
                        ViewBag.Flag = false;
                    }
                }
                ViewBag.Location = db.COM002.Where(a => a.t_dtyp == 1 && a.t_dimx == objQMS010.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault(); 
            }

            return View(objQMS010);
        }

        [HttpPost]
        public ActionResult Save(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS010 objQMS010 = new QMS010();
            try
            {
                if (fc != null)
                {
                    string strProject = !string.IsNullOrEmpty(fc["Project"]) ? fc["Project"] : string.Empty;
                    string strBU = !string.IsNullOrEmpty(fc["BU"]) ? fc["BU"].Split('-')[0].Trim() : string.Empty;
                    string strLocation = !string.IsNullOrEmpty(fc["Location"]) ? fc["Location"].Split('-')[0].Trim() : string.Empty;
                    string strQualityTag = !string.IsNullOrEmpty(fc["QualityTag"]) ? fc["QualityTag"] : string.Empty;
                    int qualityProjectId = Convert.ToInt32(fc["Id"]);
                    if (IsQualityProjectExist(strProject, strBU, strLocation, strQualityTag, qualityProjectId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = string.Format(clsImplementationMessage.QualityProjectMessage.Duplicate, strProject + '/' + strQualityTag);
                    }
                    else
                    {
                        if (qualityProjectId > 0)
                        {
                            #region Update Existing Quality Project

                            objQMS010 = db.QMS010.Where(i => i.Id == qualityProjectId).FirstOrDefault();
                            objQMS010.GeneralAssemblyDrawingNo = fc["GeneralAssemblyDrawingNo"];
                            objQMS010.GeneralAssemblyDrawingNoDesc = fc["GeneralAssemblyDrawingNoDesc"];
                            objQMS010.ManufacturingCode = fc["ManufacturingCode"];
                            objQMS010.SpecificationCode = fc["SpecificationCode"];
                            objQMS010.InspectionCentre = fc["InspectionCentre"];
                            objQMS010.Inspector1 = fc["Inspector1"];
                            objQMS010.Inspector2 = fc["Inspector2"];
                            objQMS010.TPIOnlineApproval = Convert.ToBoolean(fc["TPIOnlineApproval"]);
                            objQMS010.EditedBy = objClsLoginInfo.UserName;
                            objQMS010.EditedOn = DateTime.Now;

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Status = string.Format(clsImplementationMessage.QualityProjectMessage.Update, objQMS010.QualityProject);
                            objResponseMsg.Value = Convert.ToString(objQMS010.Id);

                            #endregion
                        }
                        else
                        {
                            #region Add New Quality Project

                            objQMS010.Project = strProject;
                            objQMS010.BU = strBU;
                            objQMS010.Location = strLocation;
                            objQMS010.QualityTag = strQualityTag;
                            objQMS010.GeneralAssemblyDrawingNo = fc["GeneralAssemblyDrawingNo"];
                            objQMS010.GeneralAssemblyDrawingNoDesc = fc["GeneralAssemblyDrawingNoDesc"];
                            objQMS010.QualityProject = fc["QualityProject"];
                            objQMS010.ManufacturingCode = fc["ManufacturingCode"];
                            objQMS010.SpecificationCode = fc["SpecificationCode"];
                            objQMS010.InspectionCentre = fc["InspectionCentre"];
                            objQMS010.Inspector1 = fc["Inspector1"];
                            objQMS010.Inspector2 = fc["Inspector2"];
                            objQMS010.TPIOnlineApproval = Convert.ToBoolean(fc["TPIOnlineApproval"]);
                            objQMS010.CreatedBy = objClsLoginInfo.UserName;
                            objQMS010.CreatedOn = DateTime.Now;

                            db.QMS010.Add(objQMS010);
                            db.SaveChanges();

                            objResponseMsg.Key = true;
                            objResponseMsg.Status = string.Format(clsImplementationMessage.QualityProjectMessage.Save, objQMS010.QualityProject);
                            objResponseMsg.Value = Convert.ToString(objQMS010.Id);

                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Status = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteQualityProject(int qualityProjId)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            QMS010 objQMS010 = new QMS010();
            try
            {
                if (qualityProjId > 0)
                    objQMS010 = db.QMS010.Where(i => i.Id == qualityProjId).FirstOrDefault();

                if (objQMS010 != null)
                {
                    List<QMS020> objQMS020 = db.QMS020.Where(i => i.QualityProject == objQMS010.QualityProject && i.Location == objQMS010.Location).ToList();
                    if (objQMS020.Any())
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = "Quality Project is used in QID, It cannot be deleted";
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.QualityProjectMessage.Delete;
                        db.QMS010.Remove(objQMS010);
                        db.SaveChanges();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Quality project do not exist";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Copy Quality Project
        [HttpPost]
        public ActionResult CopyQualityProjectPartial()
        {
            return PartialView("_CopyQualityProjectPartial");
        }

        [HttpPost]
        public ActionResult CopyToDestination(int headerId, string destProject, string tagFrom, string tagTo)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            if (!string.IsNullOrWhiteSpace(destProject))
            {
                int inTagFrom = 0, inTagTo = 0;
                List<string> existingQualityProject = new List<string>();
                List<QMS010> lstQMS010 = new List<QMS010>();

                QMS010 objQMS010Src = new QMS010();
                QMS010 objQMS010Dest = new QMS010();
                try
                {
                    string currentUser = objClsLoginInfo.UserName;

                    var currentLoc = (from a in db.COM003
                                      join b in db.COM002 on a.t_loca equals b.t_dimx
                                      where b.t_dtyp == 1 && a.t_actv == 1
                                      && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                      select b.t_dimx).FirstOrDefault().ToString();

                    if (headerId > 0)
                    {
                        objQMS010Src = db.QMS010.Where(i => i.Id == headerId).FirstOrDefault();
                    }
                    var ProjectWiseBULocation = Manager.getProjectWiseBULocation(destProject);
                    var BU = ProjectWiseBULocation.QCPBU.Split('-')[0].Trim();

                    if (String.IsNullOrWhiteSpace(tagFrom) || String.IsNullOrWhiteSpace(tagTo))
                    {
                        #region Find with Same Project name
                        if (db.QMS010.Any(i => i.Location.Equals(currentLoc, StringComparison.OrdinalIgnoreCase) && i.Project.Equals(destProject, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(BU, StringComparison.OrdinalIgnoreCase) && (i.QualityTag == "" || i.QualityTag == null)))
                            existingQualityProject.Add(destProject);
                        else
                        {
                            objQMS010Dest = new QMS010();
                            objQMS010Dest.Project = destProject;
                            objQMS010Dest.BU = BU;
                            objQMS010Dest.Location = currentLoc;
                            objQMS010Dest.QualityTag = "";
                            objQMS010Dest.GeneralAssemblyDrawingNo = objQMS010Src.GeneralAssemblyDrawingNo;
                            objQMS010Dest.GeneralAssemblyDrawingNoDesc = objQMS010Src.GeneralAssemblyDrawingNoDesc;
                            objQMS010Dest.QualityProject = destProject;
                            objQMS010Dest.ManufacturingCode = objQMS010Src.ManufacturingCode;
                            objQMS010Dest.SpecificationCode = objQMS010Src.SpecificationCode;
                            objQMS010Dest.InspectionCentre = objQMS010Src.InspectionCentre;
                            objQMS010Dest.Inspector1 = objQMS010Src.Inspector1;
                            objQMS010Dest.Inspector2 = objQMS010Src.Inspector2;
                            objQMS010Dest.TPIOnlineApproval = objQMS010Src.TPIOnlineApproval;
                            objQMS010Dest.HardnessProcedureNo = objQMS010Src.HardnessProcedureNo;
                            objQMS010Dest.PMIProcedureNo = objQMS010Src.PMIProcedureNo;
                            objQMS010Dest.ChemicalProcedureNo = objQMS010Src.ChemicalProcedureNo;
                            objQMS010Dest.FerriteProcedureNo = objQMS010Src.FerriteProcedureNo;
                            objQMS010Dest.CreatedBy = objClsLoginInfo.UserName;
                            objQMS010Dest.CreatedOn = DateTime.Now;

                            lstQMS010.Add(objQMS010Dest);
                        }
                        #endregion
                    }
                    else
                    {
                        string[] arrAlphabet = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z" };
                        if (arrAlphabet.Contains(tagFrom, StringComparer.OrdinalIgnoreCase) && arrAlphabet.Contains(tagTo, StringComparer.OrdinalIgnoreCase))
                        {
                            inTagFrom = Array.FindIndex(arrAlphabet, i => i.Equals(tagFrom, StringComparison.OrdinalIgnoreCase));//Array.IndexOf(arrAlphabet, tagFrom);
                            inTagTo = Array.FindIndex(arrAlphabet, i => i.Equals(tagTo, StringComparison.OrdinalIgnoreCase));// Array.IndexOf(arrAlphabet, tagTo);
                            #region Alphabetic Range
                            while (inTagFrom <= inTagTo)
                            {
                                var item = arrAlphabet.GetValue(inTagFrom);
                                var qualityTag = item.ToString();//Convert.ToChar(inTagFrom).ToString(); 

                                if (IsQualityProjectExist(destProject, BU, currentLoc, qualityTag))
                                {
                                    existingQualityProject.Add(destProject + ((!string.IsNullOrEmpty(qualityTag) && !string.IsNullOrWhiteSpace(qualityTag)) ? "/" + qualityTag : ""));
                                }
                                else
                                {
                                    objQMS010Dest = new QMS010();
                                    objQMS010Dest.Project = destProject;
                                    objQMS010Dest.BU = BU;
                                    objQMS010Dest.Location = currentLoc;
                                    objQMS010Dest.QualityTag = qualityTag;
                                    objQMS010Dest.GeneralAssemblyDrawingNo = objQMS010Src.GeneralAssemblyDrawingNo;
                                    objQMS010Dest.GeneralAssemblyDrawingNoDesc = objQMS010Src.GeneralAssemblyDrawingNoDesc;
                                    objQMS010Dest.QualityProject = destProject + ((!string.IsNullOrEmpty(qualityTag) && !string.IsNullOrWhiteSpace(qualityTag)) ? "/" + qualityTag : "");
                                    objQMS010Dest.ManufacturingCode = objQMS010Src.ManufacturingCode;
                                    objQMS010Dest.SpecificationCode = objQMS010Src.SpecificationCode;
                                    objQMS010Dest.InspectionCentre = objQMS010Src.InspectionCentre;
                                    objQMS010Dest.Inspector1 = objQMS010Src.Inspector1;
                                    objQMS010Dest.Inspector2 = objQMS010Src.Inspector2;
                                    objQMS010Dest.TPIOnlineApproval = objQMS010Src.TPIOnlineApproval;
                                    objQMS010Dest.HardnessProcedureNo = objQMS010Src.HardnessProcedureNo;
                                    objQMS010Dest.PMIProcedureNo = objQMS010Src.PMIProcedureNo;
                                    objQMS010Dest.ChemicalProcedureNo = objQMS010Src.ChemicalProcedureNo;
                                    objQMS010Dest.FerriteProcedureNo = objQMS010Src.FerriteProcedureNo;
                                    objQMS010Dest.CreatedBy = objClsLoginInfo.UserName;
                                    objQMS010Dest.CreatedOn = DateTime.Now;

                                    lstQMS010.Add(objQMS010Dest);
                                }
                                inTagFrom++;
                            }
                            #endregion
                        }
                        else
                        {
                            inTagFrom = Convert.ToInt32(tagFrom);
                            inTagTo = Convert.ToInt32(tagTo);
                            #region Numeric Range
                            while (inTagFrom <= inTagTo)
                            {
                                var qualityTag = Convert.ToString(inTagFrom);
                                if (IsQualityProjectExist(destProject, BU, currentLoc, qualityTag))
                                {
                                    existingQualityProject.Add(destProject + ((!string.IsNullOrEmpty(qualityTag) && !string.IsNullOrWhiteSpace(qualityTag)) ? "/" + qualityTag : ""));
                                }
                                else
                                {
                                    objQMS010Dest = new QMS010();
                                    objQMS010Dest.Project = destProject;
                                    objQMS010Dest.BU = BU;
                                    objQMS010Dest.Location = currentLoc;
                                    objQMS010Dest.QualityTag = qualityTag;
                                    objQMS010Dest.GeneralAssemblyDrawingNo = objQMS010Src.GeneralAssemblyDrawingNo;
                                    objQMS010Dest.GeneralAssemblyDrawingNoDesc = objQMS010Src.GeneralAssemblyDrawingNoDesc;
                                    objQMS010Dest.QualityProject = destProject + ((!string.IsNullOrEmpty(qualityTag) && !string.IsNullOrWhiteSpace(qualityTag)) ? "/" + qualityTag : "");
                                    objQMS010Dest.ManufacturingCode = objQMS010Src.ManufacturingCode;
                                    objQMS010Dest.SpecificationCode = objQMS010Src.SpecificationCode;
                                    objQMS010Dest.InspectionCentre = objQMS010Src.InspectionCentre;
                                    objQMS010Dest.Inspector1 = objQMS010Src.Inspector1;
                                    objQMS010Dest.Inspector2 = objQMS010Src.Inspector2;
                                    objQMS010Dest.TPIOnlineApproval = objQMS010Src.TPIOnlineApproval;
                                    objQMS010Dest.HardnessProcedureNo = objQMS010Src.HardnessProcedureNo;
                                    objQMS010Dest.PMIProcedureNo = objQMS010Src.PMIProcedureNo;
                                    objQMS010Dest.ChemicalProcedureNo = objQMS010Src.ChemicalProcedureNo;
                                    objQMS010Dest.FerriteProcedureNo = objQMS010Src.FerriteProcedureNo;
                                    objQMS010Dest.CreatedBy = objClsLoginInfo.UserName;
                                    objQMS010Dest.CreatedOn = DateTime.Now;

                                    lstQMS010.Add(objQMS010Dest);
                                }
                                inTagFrom++;
                            }
                            #endregion
                        }
                    }
                    string strQualityProject = string.Empty;
                    string strExistingQualityProject = string.Empty;
                    if (existingQualityProject.Any())
                    {
                        strExistingQualityProject = string.Join(",", existingQualityProject) + " Already Exist(s)";
                        objResponseMsg.Status = strExistingQualityProject;
                    }

                    db.QMS010.AddRange(lstQMS010);
                    db.SaveChanges();

                    if (lstQMS010.Count > 3)
                    {
                        strQualityProject = string.Join(",", lstQMS010.Take(2).OrderBy(i => i.Id).Select(i => i.QualityProject).ToList()) + ".." + lstQMS010.Select(i => i.QualityProject).Last();
                    }
                    else
                        strQualityProject = string.Join(",", lstQMS010.Select(i => i.QualityProject));

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format(clsImplementationMessage.QualityProjectMessage.Save.ToString(), strQualityProject);
                    //objResponseMsg.Status = strExistingQualityProject;
                }
                catch (Exception ex)
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = ex.Message;
                }


            }
            else
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Please select quality project from list";
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CopyToDestination_old(int headerId, string destProject, string tagFrom, string tagTo)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();

            //string s = "ABCD";
            //byte[] bytes = Encoding.ASCII.GetBytes(s);
            //int result = BitConverter.ToInt32(bytes, 0);

            QMS010 objQMS010Src = new QMS010();
            QMS010 objQMS010Dest = new QMS010();
            try
            {
                //byte[] bytesFromTag = new byte[4];
                byte[] bytesFromTag = Encoding.UTF8.GetBytes(tagFrom);
                int inTagFrom = BitConverter.ToInt32(bytesFromTag, 0);//Convert.ToChar(tagFrom.ToUpper());
                byte[] bytesToTag = Encoding.UTF8.GetBytes(tagTo);
                //bytesToTag = Encoding.ASCII.GetBytes(tagTo);
                int inTagTo = BitConverter.ToInt32(bytesToTag, 0);//Convert.ToChar(tagTo.ToUpper());


                string currentUser = objClsLoginInfo.UserName;

                var currentLoc = (from a in db.COM003
                                  join b in db.COM002 on a.t_loca equals b.t_dimx
                                  where b.t_dtyp == 1 && a.t_actv == 1
                                  && a.t_psno.Equals(currentUser, StringComparison.OrdinalIgnoreCase)
                                  select b.t_dimx).FirstOrDefault().ToString();

                if (headerId > 0)
                {
                    objQMS010Src = db.QMS010.Where(i => i.Id == headerId).FirstOrDefault();
                }

                List<string> existingQualityProject = new List<string>();
                var newLstQMS010 = new List<QMS010>();
                while (inTagFrom <= inTagTo)
                {
                    //int i = result;
                    //byte[] bytes2 = BitConverter.GetBytes(i);
                    //string s2 = Encoding.ASCII.GetString(bytes);
                    int i = inTagFrom;
                    byte[] bytes2 = BitConverter.GetBytes(i);
                    var qualityTag = Encoding.UTF8.GetString(bytes2);//Convert.ToChar(inTagFrom).ToString();
                    var ProjectWiseBULocation = Manager.getProjectWiseBULocation(destProject);

                    if (IsQualityProjectExist(destProject, ProjectWiseBULocation.QCPBU, currentLoc, qualityTag))
                    {
                        existingQualityProject.Add(destProject + ((!string.IsNullOrEmpty(qualityTag) && !string.IsNullOrWhiteSpace(qualityTag)) ? "/" + qualityTag : ""));
                    }
                    else
                    {
                        objQMS010Dest = new QMS010();
                        objQMS010Dest.BU = ProjectWiseBULocation.QCPBU;
                        objQMS010Dest.Location = currentLoc;
                        objQMS010Dest.GeneralAssemblyDrawingNo = objQMS010Src.GeneralAssemblyDrawingNo;
                        objQMS010Dest.GeneralAssemblyDrawingNoDesc = objQMS010Src.GeneralAssemblyDrawingNoDesc;
                        objQMS010Dest.QualityProject = destProject + ((!string.IsNullOrEmpty(qualityTag) && !string.IsNullOrWhiteSpace(qualityTag)) ? "/" + qualityTag : "");
                        objQMS010Dest.ManufacturingCode = objQMS010Src.ManufacturingCode;
                        objQMS010Dest.SpecificationCode = objQMS010Src.SpecificationCode;
                        objQMS010Dest.InspectionCentre = objQMS010Src.InspectionCentre;
                        objQMS010Dest.Inspector1 = objQMS010Src.Inspector1;
                        objQMS010Dest.Inspector2 = objQMS010Src.Inspector2;
                        objQMS010Dest.TPIOnlineApproval = objQMS010Src.TPIOnlineApproval;
                        objQMS010Dest.CreatedBy = objClsLoginInfo.UserName;
                        objQMS010Dest.CreatedOn = DateTime.Now;

                        newLstQMS010.Add(objQMS010Dest);
                    }
                    inTagFrom++;
                }
                if (existingQualityProject.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = string.Join(",", existingQualityProject) + "Already Exist(s)";
                }
                else
                {
                    db.QMS010.AddRange(newLstQMS010);
                    //db.SaveChanges();
                    string strQualityProject = string.Empty;
                    if (newLstQMS010.Count > 3)
                    {
                        strQualityProject = string.Join(",", newLstQMS010.Take(2).OrderBy(i => i.Id).Select(i => i.QualityProject).ToList()) + ".." + newLstQMS010.Select(i => i.QualityProject).Last();
                    }
                    else
                        strQualityProject = string.Join(",", newLstQMS010.Select(i => i.QualityProject));

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = string.Format(clsImplementationMessage.QualityProjectMessage.Save.ToString(), strQualityProject);
                }
            }
            catch (Exception ex)
            {
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;
            }

            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Quality Project Common Methods       
        [HttpPost]
        public ActionResult GetProjectDetails(string term)
        {
            string strUser = objClsLoginInfo.UserName;
            List<Projects> project = new List<Projects>();

            if (!string.IsNullOrWhiteSpace(term))
            {
                project = Manager.getParametersProjectsByUserAutocomplete(strUser, term);
            }
            else
            {
                project = Manager.getParametersProjectsByUserAutocomplete(strUser, term).Take(10).ToList();
            }
            //var projectDetail = project.Where(i => i.projectCode.Contains(term) || i.projectDescription.Contains(term)).ToList();
            return Json(project, JsonRequestBehavior.AllowGet);
        }
        public ActionResult GenerateExcel(string whereCondition, string strSortOrder, string gridType = "")
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                var lst = db.SP_IPI_GetQualityProject(1, int.MaxValue, strSortOrder, whereCondition).ToList();

                if (!lst.Any())
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "No data available";
                }

                var newlst = (from li in lst
                              select new
                              {
                                  QualityProject = li.QualityProject,
                                  Project = li.Project,
                                  Location = li.Location,
                                  BU = li.BU,
                                  QualityTag = li.QualityTag,
                                  GeneralAssemblyDrawingNo = li.GeneralAssemblyDrawingNo,
                                  GeneralAssemblyDrawingNoDesc = li.GeneralAssemblyDrawingNoDesc,
                                  ManufacturingCode = li.ManufacturingCode,
                                  SpecificationCode = li.SpecificationCode,
                                  InspectionCentre = li.InspectionCentre,
                                  Inspector1 = li.Inspector1,
                                  Inspector2 = li.Inspector2,
                                  TPIOnlineApproval = li.TPIOnlineApproval,
                                  CreatedBy = li.CreatedBy,
                                  CreatedOn = li.CreatedOn,
                                  EditedBy = li.EditedBy,
                                  EditedOn = li.EditedOn
                              }).ToList();

                string str = Helper.GenerateExcel(newlst, objClsLoginInfo.UserName);
                objResponseMsg.Key = true;
                objResponseMsg.Value = str;
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = "Error in excel generate, Please try again";
                return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
            }
        }
        [HttpPost]
        public ActionResult GetBUProjectWise(string project, string location, string qualityTag)
        {
            ModelQualityProject objModelQualityProject = new ModelQualityProject();
            try
            {
                var ProjectWiseBULocation = Manager.getProjectWiseBULocation(project);
                objModelQualityProject.QCPBU = ProjectWiseBULocation.QCPBU;

                if (IsQualityProjectExist(project, ProjectWiseBULocation.QCPBU.Split('-')[0], location, qualityTag))
                {
                    objModelQualityProject.headerAlreadyExist = true;
                }
                else
                {
                    objModelQualityProject.headerAlreadyExist = false;
                }
                var lstManufacturingCode = Manager.GetSubCatagories("Manufacturing Code", ProjectWiseBULocation.QCPBU.Split('-')[0], location).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                var lstSpecificationCode = Manager.GetSubCatagories("Specification Code", ProjectWiseBULocation.QCPBU.Split('-')[0], location).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();
                var lstInspectionCentre = Manager.GetSubCatagories("Inspection Centre", ProjectWiseBULocation.QCPBU.Split('-')[0], location).Select(i => new CategoryData { Value = i.Code, CategoryDescription = i.Code + " - " + i.Description }).ToList();

                objModelQualityProject.ManufacturingCode = lstManufacturingCode.Any() ? lstManufacturingCode : null;
                objModelQualityProject.SpecificationCode = lstSpecificationCode.Any() ? lstSpecificationCode : null;
                objModelQualityProject.InspectionCentre = lstInspectionCentre.Any() ? lstInspectionCentre : null;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                throw;
            }

            return Json(objModelQualityProject, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetInspector(string term)
        {
            List<SP_GET_APPROVER_Result> items = Manager.GetApprover(clsImplementationEnum.UserRoleName.QC2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objClsLoginInfo.Location, true, objClsLoginInfo.UserName, term).ToList();
            var employee = (from li in items
                            select new
                            {
                                Value = li.id,
                                CategoryDescription = li.text
                            }).ToList();
            return Json(employee, JsonRequestBehavior.AllowGet);

            //{
            //    var employee = lstemployee.Where(i => i.isActive == 1).Select(i => new CategoryData { Value = i.psnum, CategoryDescription = i.name }).ToList();
            //    return Json(employee, JsonRequestBehavior.AllowGet);
            //}
            //else
            //{
            //    var employee = lstemployee.Where(i => i.isActive == 1).Select(i => new CategoryData { Value = i.psnum, CategoryDescription = i.name }).Take(50).ToList();
            //    return Json(employee, JsonRequestBehavior.AllowGet);
            //}

        }

        public bool IsQualityProjectExist(string project, string strBU, string location, string qualityTag, int id = 0)
        {
            bool IsQualityProjectExist = false;
            if (!string.IsNullOrEmpty(project))
            {
                if (db.QMS010.Any(i => i.Location.Equals(location, StringComparison.OrdinalIgnoreCase) && i.Project.Equals(project, StringComparison.OrdinalIgnoreCase) && i.BU.Equals(strBU, StringComparison.OrdinalIgnoreCase) && i.QualityTag.Equals(qualityTag, StringComparison.OrdinalIgnoreCase) && i.Id != id))
                    IsQualityProjectExist = true;
            }
            return IsQualityProjectExist;
        }
        #endregion

        #region Maintain Project Details

        [HttpPost]
        public ActionResult LoadMaintainProjectDetailsPartial(int Id)
        {
            QMS010 objQMS010 = new QMS010();
            if (Id > 0)
            {
                objQMS010 = db.QMS010.Where(x => x.Id == Id).FirstOrDefault();
            }
            return PartialView("_MaintainProjectDetailsPartial", objQMS010);
        }

        [HttpPost]
        public ActionResult SaveMaintainProjectDetails(FormCollection fc)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {
                int Id = (fc["Id"] != null && fc["Id"] != "") ? Convert.ToInt32(fc["Id"]) : 0;
                if (Id > 0)
                {
                    string HardnessProcedureNo = Convert.ToString(fc["HardnessProcedureNo"]);
                    string PMIProcedureNo = Convert.ToString(fc["PMIProcedureNo"]);
                    string ChemicalProcedureNo = Convert.ToString(fc["ChemicalProcedureNo"]);
                    string FerriteProcedureNo = Convert.ToString(fc["FerriteProcedureNo"]);

                    var objQMS010 = db.QMS010.Where(x => x.Id == Id).FirstOrDefault();
                    if (objQMS010 != null)
                    {
                        objQMS010.HardnessProcedureNo = HardnessProcedureNo;
                        objQMS010.PMIProcedureNo = PMIProcedureNo;
                        objQMS010.ChemicalProcedureNo = ChemicalProcedureNo;
                        objQMS010.FerriteProcedureNo = FerriteProcedureNo;
                        objQMS010.EditedBy = objClsLoginInfo.UserName;
                        objQMS010.EditedOn = DateTime.Now;

                        db.SaveChanges();
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                    }
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Id should be greater than 0";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }


        //Added By Deval Barot on 04-03-2020
        [HttpPost]
        public ActionResult FillAutocompleteForTPI(string term, string bu, string location, bool isAgency)
        {
            var lstTPIAgency = Manager.GetSubCatagories("TPI Agency", bu, location)
                                        .Where(x => (term == "" || (x.Code.ToLower().Contains(term.ToLower()) || x.Description.ToLower().Contains(term.ToLower()))))
                                        .Select(i => new { Value = i.Code, Text = i.Description }).ToList();

            return Json(lstTPIAgency, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult FillAutocompleteForTPICopy(string term, string bu, string location, bool isAgency, string TPI1Agency, string TPI2Agency,string TPI3Agency,string tpiAgency4,string TPI5Agency,string TPI6Agency)
        {
            var lstTPIAgency = Manager.GetSubCatagories("TPI Agency", bu, location)
                                        .Where(x => (term == "" || (x.Code.ToLower().Contains(term.ToLower()) || x.Description.ToLower().Contains(term.ToLower()))))
                                        .Select(i => new { Value = i.Code, Text = i.Description }).ToList();
            
            if (!(string.IsNullOrEmpty(TPI1Agency)))
            {
                lstTPIAgency = lstTPIAgency.Where(a => a.Text != TPI1Agency).ToList();
            }

            if (!(string.IsNullOrEmpty(TPI2Agency)))
            {
                lstTPIAgency = lstTPIAgency.Where(a => a.Text != TPI2Agency).ToList();
            }
            if (!(string.IsNullOrEmpty(TPI3Agency)))
            {
                lstTPIAgency = lstTPIAgency.Where(a => a.Text != TPI3Agency).ToList();
            }
            if (!(string.IsNullOrEmpty(tpiAgency4)))
            {
                lstTPIAgency = lstTPIAgency.Where(a => a.Text != tpiAgency4).ToList();
            }

            if (!(string.IsNullOrEmpty(TPI5Agency)))
            {
                lstTPIAgency = lstTPIAgency.Where(a => a.Text != TPI5Agency).ToList();
            }
            if (!(string.IsNullOrEmpty(TPI6Agency)))
            {
                lstTPIAgency = lstTPIAgency.Where(a => a.Text != TPI6Agency).ToList();
            }

            return Json(lstTPIAgency, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateTPIAgencyData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            QMS010 objQMS010 = new QMS010();
            try
            {
                if (fc != null)
                {
                    string strProject = !string.IsNullOrEmpty(fc["Project"]) ? fc["Project"] : string.Empty;
                    string strBU = !string.IsNullOrEmpty(fc["BU"]) ? fc["BU"].Split('-')[0].Trim() : string.Empty;
                    string strLocation = !string.IsNullOrEmpty(fc["Location"]) ? fc["Location"].Split('-')[0].Trim() : string.Empty;
                    string strQualityTag = !string.IsNullOrEmpty(fc["QualityTag"]) ? fc["QualityTag"] : string.Empty;
                    int qualityProjectId = Convert.ToInt32(fc["Id"]);
                    if (IsQualityProjectExist(strProject, strBU, strLocation, strQualityTag, qualityProjectId))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = string.Format(clsImplementationMessage.QualityProjectMessage.Duplicate, strProject + '/' + strQualityTag);
                    }
                    else
                    {
                        if (qualityProjectId > 0)
                        {
                            #region Update Existing Quality Project

                            objQMS010 = db.QMS010.Where(i => i.Id == qualityProjectId).FirstOrDefault();
                            objQMS010.TPI1 = fc["TPI1"];
                            objQMS010.TPI2 = fc["TPI2"];
                            objQMS010.TPI3 = fc["TPI3"];
                            objQMS010.TPI4 = fc["TPI4"];
                            objQMS010.TPI5 = fc["TPI5"];
                            objQMS010.TPI6 = fc["TPI6"];
                            objQMS010.TPIAgency1Name = fc["TPIAgency1Name"];
                            objQMS010.TPIAgency2Name = fc["TPIAgency2Name"];
                            objQMS010.TPIAgency3Name = fc["TPIAgency3Name"];
                            objQMS010.TPIAgency4Name = fc["TPIAgency4Name"];
                            objQMS010.TPIAgency5Name = fc["TPIAgency5Name"];
                            objQMS010.TPIAgency6Name = fc["TPIAgency6Name"];
                            objQMS010.EditedBy = objClsLoginInfo.UserName;
                            objQMS010.EditedOn = DateTime.Now;

                            db.SaveChanges();
                            objResponseMsg.Key = true;
                            objResponseMsg.Status = string.Format(clsImplementationMessage.QualityProjectMessage.Update, objQMS010.QualityProject);
                            objResponseMsg.Value = Convert.ToString(objQMS010.Id);

                            #endregion
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Status = ex.Message;
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataForTPI(int ID)
        {
            var lstTPIAgency = db.QMS010.Where(e => e.Id == ID).FirstOrDefault();

            var TPI1 = lstTPIAgency.TPI1;
            var TPI2 = lstTPIAgency.TPI2;
            var TPI3 = lstTPIAgency.TPI3;
            var TPI4 = lstTPIAgency.TPI4;
            var TPI5 = lstTPIAgency.TPI5;
            var TPI6 = lstTPIAgency.TPI6;
            var TPIAgency1 = db.GLB002.Where(e => e.Code == TPI1).
                Select(i => new { TPI1 = i.Description }).FirstOrDefault();
            var TPIAgency2 = db.GLB002.Where(e => e.Code == TPI2).
                Select(i => new { TPI2 = i.Description }).FirstOrDefault();
            var TPIAgency3 = db.GLB002.Where(e => e.Code == TPI3).
                Select(i => new { TPI3 = i.Description }).FirstOrDefault();
            var TPIAgency4 = db.GLB002.Where(e => e.Code == TPI4).
                Select(i => new { TPI4 = i.Description }).FirstOrDefault();
            var TPIAgency5 = db.GLB002.Where(e => e.Code == TPI5).
                Select(i => new { TPI5 = i.Description }).FirstOrDefault();
            var TPIAgency6 = db.GLB002.Where(e => e.Code == TPI6).
                Select(i => new { TPI6 = i.Description }).FirstOrDefault();

            if (TPIAgency1 != null)
            {
                lstTPIAgency.TPI1 = TPIAgency1.TPI1;
            }
            if (TPIAgency2 != null)
            {
                lstTPIAgency.TPI2 = TPIAgency2.TPI2;
            }
            if (TPIAgency3 != null)
            {
                lstTPIAgency.TPI3 = TPIAgency3.TPI3;
            }
            if (TPIAgency4 != null)
            {
                lstTPIAgency.TPI4 = TPIAgency4.TPI4;
            }
            if (TPIAgency5 != null)
            {
                lstTPIAgency.TPI5 = TPIAgency5.TPI5;
            }
            if (TPIAgency6 != null)
            {
                lstTPIAgency.TPI6 = TPIAgency6.TPI6;
            }

            //List<SP_GET_APPROVER_Result> items = Manager.GetApprover(clsImplementationEnum.UserRoleName.QC2.GetStringValue() + "," + clsImplementationEnum.UserRoleName.QC3.GetStringValue(), objClsLoginInfo.Location, true, objClsLoginInfo.UserName, "").ToList();

            var items = (from COM in db.COM003
                         join ATH in db.ATH001 on COM.t_psno equals ATH.Employee
                         where COM.t_actv == 1 && COM.t_name != ""
                         select new { id = COM.t_psno, text = COM.t_psno + "-" + COM.t_name }).ToList().Distinct();

            var TPI1Name = lstTPIAgency.TPIAgency1Name;
            var TPI2Name = lstTPIAgency.TPIAgency2Name;
            var TPI3Name = lstTPIAgency.TPIAgency3Name;
            var TPI4Name = lstTPIAgency.TPIAgency4Name;
            var TPI5Name = lstTPIAgency.TPIAgency5Name;
            var TPI6Name = lstTPIAgency.TPIAgency6Name;

            var TPIAgency1Name = items.Where(x => x.id == TPI1Name).Select(i => new { TPIAgency1Name = i.text }).FirstOrDefault();
            var TPIAgency2Name = items.Where(x => x.id == TPI2Name).Select(i => new { TPIAgency2Name = i.text }).FirstOrDefault();
            var TPIAgency3Name = items.Where(x => x.id == TPI3Name).Select(i => new { TPIAgency3Name = i.text }).FirstOrDefault();
            var TPIAgency4Name = items.Where(x => x.id == TPI4Name).Select(i => new { TPIAgency4Name = i.text }).FirstOrDefault();
            var TPIAgency5Name = items.Where(x => x.id == TPI5Name).Select(i => new { TPIAgency5Name = i.text }).FirstOrDefault();
            var TPIAgency6Name = items.Where(x => x.id == TPI6Name).Select(i => new { TPIAgency6Name = i.text }).FirstOrDefault();

            if (TPIAgency1Name != null)
            {
                lstTPIAgency.TPIAgency1Name = TPIAgency1Name.TPIAgency1Name;
            }
            if (TPIAgency2Name != null)
            {
                lstTPIAgency.TPIAgency2Name = TPIAgency2Name.TPIAgency2Name;
            }
            if (TPIAgency3Name != null)
            {
                lstTPIAgency.TPIAgency3Name = TPIAgency3Name.TPIAgency3Name;
            }
            if (TPIAgency4Name != null)
            {
                lstTPIAgency.TPIAgency4Name = TPIAgency4Name.TPIAgency4Name;
            }
            if (TPIAgency5Name != null)
            {
                lstTPIAgency.TPIAgency5Name = TPIAgency5Name.TPIAgency5Name;
            }
            if (TPIAgency6Name != null)
            {
                lstTPIAgency.TPIAgency6Name = TPIAgency6Name.TPIAgency6Name;
            }
            return Json(lstTPIAgency, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetDataforTPIName(string term)
        {
            var items = (from COM in db.COM003
                         join ATH in db.ATH001 on COM.t_psno equals ATH.Employee
                         join ath4 in db.ATH004 on ATH.Role equals ath4.Id
                         where COM.t_actv == 1 && COM.t_name != "" && ath4.Role == "TPI2" && (term == "" || COM.t_psno.Contains(term.ToLower()) || COM.t_name.Contains(term.ToLower()))
                         select new { Value = COM.t_psno, CategoryDescription = COM.t_psno + "-" + COM.t_name }).ToList().Distinct();

            //var employee = (from li in items
            //                select new
            //                {
            //                    Value = li.id,
            //                    CategoryDescription = li.text
            //                }).ToList();
            return Json(items, JsonRequestBehavior.AllowGet);
        }
        //End Here

        #endregion
    }
}