﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IEMQSImplementation;
using IEMQS.Models;
using IEMQS.Areas.NDE.Models;
using System.Data.Entity.Core.Objects;
using IEMQS.Areas.IPI.Models;
using IEMQS.Areas.Utility.Models;
using IEMQS.Areas.WQ.Models;

namespace IEMQS.Areas.IPI.Controllers
{
    public class ChemicalAnalysisController : clsBase
    {
        // GET: IPI/ChemicalAnalysis
        public ActionResult Index()
        {
            return View();
        }

        #region QID Sampling plan
        [HttpPost]//(int headerid, int lineid, int revno, bool isEditable)
        public ActionResult MaintainSamplingPlan(int headerid = 0, int lineid = 0, string qProj = "", string Project = "", string bu = "", string loc = "", string qId = "", int qRev = 0, string stage = "", int stageseq = 0, bool isFromQID = false, bool isEditable = false)
        {
            QMS018 objQMS018 = new QMS018();
            NDEModels objNDEModels = new NDEModels();
            List<string> lstQproj = new List<string>();
            lstQproj.Add(qProj);
            lstQproj.AddRange(db.QMS017.Where(c => c.BU == bu && c.Location == loc && c.IQualityProject == qProj).Select(x => x.QualityProject).ToList());

            if (isFromQID)
            {
                objQMS018 = db.QMS018.Where(i => lstQproj.Contains(i.QualityProject) //&& i.Project == Project
                                              && i.BU == bu && i.Location == loc // && i.QualityId == qId && i.QualityIdRev == qRev
                                              && i.StageCode == stage && i.QIDLineId == lineid
                                          ).OrderByDescending(x => x.QualityIdRev).FirstOrDefault();
                int? objQID = db.QMS016.Where(x => x.LineId == lineid).FirstOrDefault().RevNo;
                qRev = objQID > 0 ? Convert.ToInt32(objQID) : qRev;
                if (objQMS018 == null)
                {

                    objQMS018 = new QMS018();
                    ViewBag.HeaderId = 0;
                    objQMS018.QIDLineId = lineid;
                    objQMS018.QualityProject = qProj;
                    objQMS018.QualityId = qId;
                    objQMS018.StageCode = stage;
                    objQMS018.StageSequence = stageseq;
                }
                ViewBag.isEditable = isEditable;
            }
            else
            {
                objQMS018 = db.QMS018.Where(i => lstQproj.Contains(i.QualityProject) //&& i.Project == Project
                                               && i.BU == bu && i.Location == loc && i.QualityId == qId && i.QualityIdRev == qRev
                                               && i.StageCode == stage
                                           ).FirstOrDefault();
                ViewBag.isEditable = false;
            }
            if (objQMS018 != null)
            {
                ViewBag.HeaderId = objQMS018.HeaderId;
            }

            ViewBag.RevNo = qRev;
            ViewBag.StageCode = objNDEModels.GetCategory(stage, bu, loc).CategoryDescription;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
            ViewBag.Location = db.COM002.Where(a => a.t_dimx == loc).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.BU = db.COM002.Where(a => a.t_dimx == bu).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.QualityId = qId;
            ViewBag.QualityProject = qProj;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            return PartialView("_LoadMaintainSamplingPlanPartial", objQMS018);
        }

        [HttpPost]
        public ActionResult SaveSamplingPlan(QMS018 objQMS)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                QMS018 objQMS018 = new QMS018();
                QMS016 objQMS016 = db.QMS016.Where(i => i.LineId == objQMS.QIDLineId).FirstOrDefault();
                if (objQMS016.QMS015.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                {
                    if (objQMS.HeaderId > 0)
                    {
                        objQMS018 = db.QMS018.Where(x => x.HeaderId == objQMS.HeaderId).FirstOrDefault();
                        objQMS018.QIDLineId = objQMS016.LineId;
                        objQMS018.Project = objQMS016.Project;
                        objQMS018.QualityProject = objQMS016.QualityProject;
                        objQMS018.BU = objQMS016.BU;
                        objQMS018.Location = objQMS016.Location;
                        objQMS018.QualityId = objQMS016.QualityId;
                        objQMS018.QualityIdRev = objQMS016.RevNo;
                        objQMS018.StageCode = objQMS016.StageCode;
                        objQMS018.StageSequence = objQMS016.StageSequance;
                        objQMS018.Depth = objQMS.Depth;
                        objQMS018.Component = objQMS.Component;
                        objQMS018.NoOfComponents = objQMS.NoOfComponents;
                        objQMS018.Procedure = objQMS.Procedure;
                        objQMS018.NoOfProcedures = objQMS.NoOfProcedures;
                        objQMS018.Welder = objQMS.Welder;
                        objQMS018.NoOfWelders = objQMS.NoOfWelders;
                        objQMS018.Position = objQMS.Position;
                        objQMS018.NoOfPositions = objQMS.NoOfPositions;
                        objQMS018.EditedBy = objClsLoginInfo.UserName;
                        objQMS018.EditedOn = DateTime.Now;
                        if (objQMS016.QMS015.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                        {
                            objQMS016.QMS015.RevNo = Convert.ToInt32(objQMS016.QMS015.RevNo) + 1;
                            objQMS016.QMS015.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                            objQMS016.QMS015.EditedBy = objClsLoginInfo.UserName;
                            objQMS016.QMS015.EditedOn = DateTime.Now;
                            objQMS016.QMS015.SubmittedBy = null;
                            objQMS016.QMS015.SubmittedOn = null;
                            objQMS016.QMS015.ApprovedBy = null;
                            objQMS016.QMS015.ApprovedOn = null;
                            objQMS016.QMS015.ReturnedBy = null;
                            objQMS016.QMS015.ReturnedOn = null; objQMS016.QMS015.ReturnRemark = null;
                        }

                        if (objQMS016.StageStatus == clsImplementationEnum.QualityIdStageStatus.APPROVED.GetStringValue())
                        {
                            objQMS016.StageStatus = clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue();
                            objQMS016.EditedBy = objClsLoginInfo.UserName;
                            objQMS016.EditedOn = DateTime.Now;
                        }
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;

                    }
                    else
                    {
                        objQMS018.QIDLineId = objQMS016.LineId;
                        objQMS018.Project = objQMS016.Project;
                        objQMS018.QualityProject = objQMS016.QualityProject;
                        objQMS018.BU = objQMS016.BU;
                        objQMS018.Location = objQMS016.Location;
                        objQMS018.QualityId = objQMS016.QualityId;
                        objQMS018.QualityIdRev = objQMS016.RevNo;
                        objQMS018.StageCode = objQMS016.StageCode;
                        objQMS018.StageSequence = objQMS016.StageSequance;
                        objQMS018.Depth = objQMS.Depth;
                        objQMS018.Component = objQMS.Component;
                        objQMS018.NoOfComponents = objQMS.NoOfComponents;
                        objQMS018.Procedure = objQMS.Procedure;
                        objQMS018.NoOfProcedures = objQMS.NoOfProcedures;
                        objQMS018.Welder = objQMS.Welder;
                        objQMS018.NoOfWelders = objQMS.NoOfWelders;
                        objQMS018.Position = objQMS.Position;
                        objQMS018.NoOfPositions = objQMS.NoOfPositions;
                        objQMS018.CreatedBy = objClsLoginInfo.UserName;
                        objQMS018.CreatedOn = DateTime.Now;
                        if (objQMS016.QMS015.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                        {
                            objQMS016.QMS015.RevNo = Convert.ToInt32(objQMS016.QMS015.RevNo) + 1;
                            objQMS016.QMS015.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                            objQMS016.QMS015.EditedBy = objClsLoginInfo.UserName;
                            objQMS016.QMS015.EditedOn = DateTime.Now;
                            objQMS016.QMS015.SubmittedBy = null;
                            objQMS016.QMS015.SubmittedOn = null;
                            objQMS016.QMS015.ApprovedBy = null;
                            objQMS016.QMS015.ApprovedOn = null;
                            objQMS016.QMS015.ReturnedBy = null;
                            objQMS016.QMS015.ReturnedOn = null; objQMS016.QMS015.ReturnRemark = null;
                        }

                        if (objQMS016.StageStatus == clsImplementationEnum.QualityIdStageStatus.APPROVED.GetStringValue())
                        {
                            objQMS016.StageStatus = clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue();
                            objQMS016.EditedBy = objClsLoginInfo.UserName;
                            objQMS016.EditedOn = DateTime.Now;
                        }
                        db.QMS018.Add(objQMS018);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                    }
                }
                else
                {
                    objResponseMsg.Remarks = "Quality Id is already submitted, it cannot be add/modify now!!";
                }
                objResponseMsg.HeaderId = objQMS018.HeaderId;
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region QID Maintain Element
        [HttpPost]
        public ActionResult LoadMaintainElement(int planid, bool isEditable)
        {
            QMS018 objQMS018 = db.QMS018.Where(i => i.HeaderId == planid).FirstOrDefault();
            ViewBag.PlanId = planid;
            ViewBag.isEditable = isEditable;
            ModelWQRequest objModelWQRequest = new ModelWQRequest();
            var lstElement = objModelWQRequest.LoadSubCatagoryDescription("Element", objQMS018.Location, objQMS018.BU).Select(x => new { Code = x.Value, CategoryDescription = x.CategoryDescription });
            ViewBag.Element = lstElement.Any() ? lstElement : null;
            return PartialView("_LoadMaintainElementPartial", objQMS018);
        }

        [HttpPost]
        public ActionResult GetElementData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;
                int headerId = Convert.ToInt32(param.Headerid);
                whereCondition += " AND HeaderId = " + headerId;

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                bool isEditable = param.IsVisible;
                string LineStatus = param.MTStatus;
                var lstResult = db.SP_IPI_GET_ELEMENT_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                List<SelectItemList> lst = new List<SelectItemList>();
                var lst12Ele = db.QMS019.Where(x => x.HeaderId == headerId).ToList();
                bool is12Element = false;
                if (lst12Ele.Count() == 12)
                {
                    is12Element = true;
                }
                int newRecordId = 0;
                var newRecord = new[] {
                                   Helper.GenerateHidden(newRecordId,"HeaderId",param.Headerid),
                                   Convert.ToString(newRecordId),
                                   Helper.HTMLAutoComplete(newRecordId, "txtElement","","",false,"","Element")+""+Helper.GenerateHidden(newRecordId,"Element"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqMinValue", "", "", false, "", false, "10"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqMaxValue",   "", "", false, "",false, "8"),
                                   Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewElement();" ),
                                };
                NDEModels objNDEModels = new NDEModels();
                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                isEditable ? Helper.HTMLAutoComplete(uc.LineId, "txtElement", objNDEModels.GetCategory("Element",uc.Element, uc.BU.Trim(), uc.Location.Trim(),true).CategoryDescription,"UpdateData(this, "+ uc.LineId +","+ uc.HeaderId +",true);",false,"","Element",isEditable ? false:true,"","required")+""+Helper.GenerateHidden(uc.LineId,"Element",uc.Element) : Convert.ToString(objNDEModels.GetCategory("Element",uc.Element, uc.BU.Trim(), uc.Location.Trim(),true).CategoryDescription),
                                isEditable ? Helper.GenerateNumericTextbox(uc.LineId, "ReqMinValue", Convert.ToString(uc.ReqMinValue), "UpdateData(this, " + uc.LineId + ","+ uc.HeaderId +",true);",isEditable ? false:true,"",false , "8","") : Convert.ToString(uc.ReqMinValue),
                                isEditable ?Helper.GenerateNumericTextbox(uc.LineId, "ReqMaxValue",    Convert.ToString(uc.ReqMaxValue),"UpdateData(this, "+ uc.LineId+","+ uc.HeaderId +",true);",isEditable ? false:true,"",false , "8","") : Convert.ToString(uc.ReqMaxValue),
                                "<nobr>" + Helper.GenerateActionIcon(uc.LineId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteElement("+uc.LineId+")","",isEditable ? false:true)+"</nobr>"
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    is12Element = is12Element
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //Save New Record
        [HttpPost]
        public ActionResult SaveNewElement(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int newRowIndex = 0;
            try
            {
                int planId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                QMS019 objQMS019 = new QMS019();
                QMS018 objQMS018 = db.QMS018.Where(x => x.HeaderId == planId).FirstOrDefault();
                QMS016 objQMS016 = db.QMS016.Where(i => i.LineId == objQMS018.QIDLineId).FirstOrDefault();
                if (objQMS016.QMS015.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                {
                    if (isElemetnExist(fc["Element" + newRowIndex], planId, 0, objQMS018.QualityIdRev))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                    }
                    else
                    {
                        objQMS019.HeaderId = objQMS018.HeaderId;
                        objQMS019.QIDLineId = objQMS018.QIDLineId;
                        objQMS019.Project = objQMS018.Project;
                        objQMS019.QualityProject = objQMS018.QualityProject;
                        objQMS019.BU = objQMS018.BU;
                        objQMS019.Location = objQMS018.Location;
                        objQMS019.QualityId = objQMS018.QualityId;
                        objQMS019.QualityIdRev = objQMS018.QualityIdRev;
                        objQMS019.StageCode = objQMS018.StageCode;
                        objQMS019.StageSequence = objQMS018.StageSequence;
                        objQMS019.Element = fc["Element" + newRowIndex];
                        objQMS019.ReqMinValue = Convert.ToDouble(fc["ReqMinValue" + newRowIndex]);
                        objQMS019.ReqMaxValue = Convert.ToDouble(fc["ReqMaxValue" + newRowIndex]);
                        objQMS019.CreatedBy = objClsLoginInfo.UserName;
                        objQMS019.CreatedOn = DateTime.Now;
                        if (objQMS016.QMS015.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                        {
                            objQMS016.QMS015.RevNo = Convert.ToInt32(objQMS016.QMS015.RevNo) + 1;
                            objQMS016.QMS015.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                            objQMS016.QMS015.EditedBy = objClsLoginInfo.UserName;
                            objQMS016.QMS015.EditedOn = DateTime.Now;
                            objQMS016.QMS015.SubmittedBy = null;
                            objQMS016.QMS015.SubmittedOn = null;
                            objQMS016.QMS015.ApprovedBy = null;
                            objQMS016.QMS015.ApprovedOn = null;
                            objQMS016.QMS015.ReturnedBy = null;
                            objQMS016.QMS015.ReturnedOn = null; objQMS016.QMS015.ReturnRemark = null;
                        }

                        if (objQMS016.StageStatus == clsImplementationEnum.QualityIdStageStatus.APPROVED.GetStringValue())
                        {
                            objQMS016.StageStatus = clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue();
                            objQMS016.EditedBy = objClsLoginInfo.UserName;
                            objQMS016.EditedOn = DateTime.Now;
                        }
                        db.QMS019.Add(objQMS019);
                        db.SaveChanges();


                        objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Insert;
                    }
                }
                else
                {
                    objResponseMsg.Remarks = "Quality Id is already submitted, it cannot be add/modify now!!";
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateElement(int lineId, int headerid, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string tableName = "QMS019";
                QMS018 objQMS018 = db.QMS018.Where(x => x.HeaderId == headerid).FirstOrDefault();
                QMS016 objQMS016 = db.QMS016.Where(x => x.LineId == objQMS018.QIDLineId).FirstOrDefault();
                if (objQMS016.QMS015.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                {
                    if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                    {
                        if (objQMS018 != null ? isElemetnExist(columnValue, headerid, lineId, objQMS018.QualityIdRev) : false)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                        }
                        else
                        {
                            db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, tableName, objClsLoginInfo.UserName);
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteElement(int id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS019 objQMS019 = db.QMS019.Where(x => x.LineId == id).FirstOrDefault();
                QMS016 objQMS016 = db.QMS016.Where(x => x.LineId == objQMS019.QIDLineId).FirstOrDefault();
                if (objQMS016.QMS015.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                {
                    if (objQMS019 != null)
                    {
                        var planId = objQMS019.HeaderId;
                        db.QMS019.Remove(objQMS019);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Element deleted successfully.";
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details not available for delete.";
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Remarks = "Quality Id is already submitted, it cannot be delete now!!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Test Plan Sampling plan
        [HttpPost]//(int headerid, int lineid, int revno, bool isEditable)
        public ActionResult MaintainTestPlanSamplingPlan(int headerid = 0, int lineid = 0, string qProj = "", string Project = "", string bu = "", string loc = "", string qId = "", int qRev = 0, string stage = "", int stageseq = 0, bool isFromTP = false, bool isEditable = false)
        {
            QMS022 objQMS022 = new QMS022();
            NDEModels objNDEModels = new NDEModels();
            ViewBag.isEditable = isEditable;
            QMS021 objQMS021 = db.QMS021.Where(i => i.LineId == lineid).OrderByDescending(x => x.RevNo).FirstOrDefault();
            string approved = clsImplementationEnum.QualityIdStageStatus.APPROVED.GetStringValue();
            objQMS022 = db.QMS022.Where(i => i.TPLineId == lineid).OrderByDescending(x => x.TPRevNo).FirstOrDefault();
            if (isFromTP && objQMS022 == null)
            {
                List<string> lstQproj = new List<string>();
                lstQproj.Add(objQMS021.QualityProject);
                lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS021.BU && c.Location == objQMS021.Location && c.IQualityProject == objQMS021.QualityProject).Select(x => x.QualityProject).ToList());

                var QualityId = objQMS021.QMS020.QualityId;

                QMS016_Log objQIDLine = new QMS016_Log();
                objQIDLine = (from a in db.QMS015_Log
                              join x in db.QMS016_Log on a.Id equals x.RefId
                              where x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == objQMS021.Location && x.StageCode == objQMS021.StageCode && x.StageSequance == objQMS021.StageSequance && a.Status == approved
                              select x).FirstOrDefault();
                if (objQIDLine == null)
                {
                    objQIDLine = (from a in db.QMS015_Log
                                  join x in db.QMS016_Log on a.Id equals x.RefId
                                  where x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == objQMS021.Location && x.StageCode == objQMS021.StageCode && a.Status == approved
                                  select x).FirstOrDefault();
                }
                // var objQIDLine = db.QMS016_Log.OrderByDescending(x => x.RevNo).FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == objQMS021.Location && x.StageCode == objQMS021.StageCode && x.StageSequance == objQMS021.StageSequance && x.StageStatus == approved);

                if (objQIDLine != null && QualityId != null)
                {
                    QMS018 objQMS018 = db.QMS018.Where(i => lstQproj.Contains(i.QualityProject)
                                            && i.BU == bu && i.Location == loc && i.QualityId == QualityId && i.QualityIdRev == objQIDLine.RevNo
                                            && i.StageCode == stage && i.StageSequence == objQMS021.StageSequance
                                        ).FirstOrDefault();
                    if (objQMS018 == null)
                    {
                        objQMS018 = db.QMS018.Where(i => lstQproj.Contains(i.QualityProject)
                                        && i.BU == objQMS021.BU && i.Location == objQMS021.Location && i.QualityId == QualityId && i.QualityIdRev == objQIDLine.RevNo
                                        && i.StageCode == objQMS021.StageCode
                                    ).FirstOrDefault();
                    }
                    if (objQMS018 != null)
                    {
                        objQMS022 = new QMS022();
                        objQMS022.TPLineId = objQMS021.LineId;
                        objQMS022.Project = objQMS021.Project;
                        objQMS022.QualityProject = objQMS021.QualityProject;
                        objQMS022.BU = objQMS021.BU;
                        objQMS022.Location = objQMS021.Location;
                        objQMS022.SeamNo = objQMS021.SeamNo;
                        objQMS022.TPRevNo = objQMS021.QMS020.RevNo;
                        objQMS022.StageCode = objQMS018.StageCode;
                        objQMS022.StageSequence = objQMS018.StageSequence;
                        objQMS022.Depth = objQMS018.Depth;
                        objQMS022.Component = objQMS018.Component;
                        objQMS022.NoOfComponents = objQMS018.NoOfComponents;
                        objQMS022.Procedure = objQMS018.Procedure;
                        objQMS022.NoOfProcedures = objQMS018.NoOfProcedures;
                        objQMS022.Welder = objQMS018.Welder;
                        objQMS022.NoOfWelders = objQMS018.NoOfWelders;
                        objQMS022.Position = objQMS018.Position;
                        objQMS022.NoOfPositions = objQMS018.NoOfPositions;
                        objQMS022.CreatedBy = objClsLoginInfo.UserName;
                        objQMS022.CreatedOn = DateTime.Now;
                        db.QMS022.Add(objQMS022);
                        db.SaveChanges();
                        List<QMS019> lstQMS019 = db.QMS019.Where(x => x.HeaderId == objQMS018.HeaderId).ToList();
                        if (lstQMS019 != null)
                        {
                            db.QMS023.AddRange(
                                            lstQMS019.Select(x =>
                                            new QMS023
                                            {
                                                TPLineId = objQMS021.LineId,
                                                Project = objQMS021.Project,
                                                QualityProject = objQMS021.QualityProject,
                                                BU = objQMS021.BU,
                                                Location = objQMS021.Location,
                                                SeamNo = objQMS021.SeamNo,
                                                TPRevNo = objQMS021.QMS020.RevNo,
                                                StageCode = x.StageCode,
                                                StageSequence = x.StageSequence,
                                                HeaderId = objQMS022.HeaderId,
                                                Element = x.Element,
                                                ReqMinValue = x.ReqMinValue,
                                                ReqMaxValue = x.ReqMaxValue,
                                                CreatedBy = objClsLoginInfo.UserName,
                                                CreatedOn = DateTime.Now,
                                            })
                                            );
                        }
                        db.SaveChanges();
                        objQMS022 = db.QMS022.Where(i => i.TPLineId == lineid).OrderByDescending(x => x.TPRevNo).FirstOrDefault();
                    }
                    else
                    {
                        objQMS022 = new QMS022();
                        ViewBag.HeaderId = 0;
                        objQMS022.TPLineId = lineid;
                        objQMS022.TPRevNo = objQMS021?.QMS020.RevNo;
                        objQMS022.QualityProject = objQMS021?.QualityProject;
                        objQMS022.SeamNo = objQMS021?.SeamNo;
                        objQMS022.StageCode = objQMS021?.StageCode;
                        objQMS022.StageSequence = objQMS021?.StageSequance;
                    }
                }
            }

            if (objQMS022 == null)
            {
                objQMS022 = new QMS022();
                ViewBag.HeaderId = 0;
                objQMS022.TPLineId = lineid;
                objQMS022.TPRevNo = objQMS021?.QMS020.RevNo;
                objQMS022.QualityProject = objQMS021?.QualityProject;
                objQMS022.SeamNo = objQMS021?.SeamNo;
                objQMS022.StageCode = objQMS021?.StageCode;
                objQMS022.StageSequence = objQMS021?.StageSequance;
            }
            if (objQMS022 != null)
            {
                ViewBag.HeaderId = objQMS022.HeaderId;
                ViewBag.RevNo = objQMS022.TPRevNo;
            }

            ViewBag.StageCode = objNDEModels.GetCategory(stage, bu, loc).CategoryDescription;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
            ViewBag.Location = db.COM002.Where(a => a.t_dimx == loc).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.BU = db.COM002.Where(a => a.t_dimx == bu).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.SeamNo = qId;
            ViewBag.QualityProject = qProj;

            List<string> lstyesno = clsImplementationEnum.getyesno().ToList();
            ViewBag.YesNoEnum = lstyesno.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();

            return PartialView("_LoadTestPlanSamplingPlanPartial", objQMS022);
        }

        [HttpPost]
        public ActionResult SaveTestPlanSamplingPlan(QMS022 objQMS)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                QMS022 objQMS022 = new QMS022();
                QMS021 objQMS021 = db.QMS021.Where(i => i.LineId == objQMS.TPLineId).FirstOrDefault();
                if (objQMS021.QMS020.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                {
                    if (objQMS021.QMS020.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                    {
                        objQMS021.QMS020.RevNo = Convert.ToInt32(objQMS021.QMS020.RevNo) + 1;
                        objQMS021.QMS020.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                        objQMS021.QMS020.EditedBy = objClsLoginInfo.UserName;
                        objQMS021.QMS020.EditedOn = DateTime.Now;
                        objQMS021.QMS020.SubmittedBy = null;
                        objQMS021.QMS020.SubmittedOn = null;
                        objQMS021.QMS020.ApprovedBy = null;
                        objQMS021.QMS020.ApprovedOn = null;
                        objQMS021.QMS020.ReturnedBy = null;
                        objQMS021.QMS020.ReturnedOn = null; objQMS021.QMS020.ReturnRemark = null;
                    }

                    if (objQMS021.StageStatus == clsImplementationEnum.QualityIdStageStatus.APPROVED.GetStringValue())
                    {
                        objQMS021.StageStatus = clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue();
                        objQMS021.EditedBy = objClsLoginInfo.UserName;
                        objQMS021.EditedOn = DateTime.Now;
                    }

                    if (objQMS.HeaderId > 0)
                    {
                        objQMS022 = db.QMS022.Where(x => x.HeaderId == objQMS.HeaderId).FirstOrDefault();
                        objQMS022.TPLineId = objQMS021.LineId;
                        objQMS022.Project = objQMS021.Project;
                        objQMS022.QualityProject = objQMS021.QualityProject;
                        objQMS022.BU = objQMS021.BU;
                        objQMS022.Location = objQMS021.Location;
                        objQMS022.SeamNo = objQMS021.SeamNo;
                        objQMS022.TPRevNo = objQMS021.RevNo;
                        objQMS022.StageCode = objQMS021.StageCode;
                        objQMS022.StageSequence = objQMS021.StageSequance;
                        objQMS022.Depth = objQMS.Depth;
                        objQMS022.Component = objQMS.Component;
                        objQMS022.NoOfComponents = objQMS.NoOfComponents;
                        objQMS022.Procedure = objQMS.Procedure;
                        objQMS022.NoOfProcedures = objQMS.NoOfProcedures;
                        objQMS022.Welder = objQMS.Welder;
                        objQMS022.NoOfWelders = objQMS.NoOfWelders;
                        objQMS022.Position = objQMS.Position;
                        objQMS022.NoOfPositions = objQMS.NoOfPositions;
                        objQMS022.EditedBy = objClsLoginInfo.UserName;
                        objQMS022.EditedOn = DateTime.Now;
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update;

                    }
                    else
                    {
                        objQMS022.TPLineId = objQMS021.LineId;
                        objQMS022.Project = objQMS021.Project;
                        objQMS022.QualityProject = objQMS021.QualityProject;
                        objQMS022.BU = objQMS021.BU;
                        objQMS022.Location = objQMS021.Location;
                        objQMS022.SeamNo = objQMS021.SeamNo;
                        objQMS022.TPRevNo = objQMS021.RevNo;
                        objQMS022.StageCode = objQMS021.StageCode;
                        objQMS022.StageSequence = objQMS021.StageSequance;
                        objQMS022.Depth = objQMS.Depth;
                        objQMS022.Component = objQMS.Component;
                        objQMS022.NoOfComponents = objQMS.NoOfComponents;
                        objQMS022.Procedure = objQMS.Procedure;
                        objQMS022.NoOfProcedures = objQMS.NoOfProcedures;
                        objQMS022.Welder = objQMS.Welder;
                        objQMS022.NoOfWelders = objQMS.NoOfWelders;
                        objQMS022.Position = objQMS.Position;
                        objQMS022.NoOfPositions = objQMS.NoOfPositions;
                        objQMS022.CreatedBy = objClsLoginInfo.UserName;
                        objQMS022.CreatedOn = DateTime.Now;
                        db.QMS022.Add(objQMS022);
                        db.SaveChanges();
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Insert;
                    }
                }
                else
                {
                    objResponseMsg.Remarks = "Seam is already submitted, it cannot be add/modify now!!";
                }
                objResponseMsg.HeaderId = objQMS022.HeaderId;
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Maintain Test Plan Element
        [HttpPost]
        public ActionResult LoadTPMaintainElement(int planid, bool isEditable)
        {
            QMS022 objQMS022 = db.QMS022.Where(i => i.HeaderId == planid).FirstOrDefault();
            ViewBag.PlanId = planid;
            ViewBag.isEditable = isEditable;
            ModelWQRequest objModelWQRequest = new ModelWQRequest();
            var lstElement = objModelWQRequest.LoadSubCatagoryDescription("Element", objQMS022.Location, objQMS022.BU).Select(x => new { Code = x.Value, CategoryDescription = x.CategoryDescription });
            ViewBag.Element = lstElement.Any() ? lstElement : null;
            return PartialView("_LoadTestPlanElementPartial", objQMS022);
        }

        [HttpPost]
        public ActionResult GetTestPlanElementData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;
                int headerId = Convert.ToInt32(param.Headerid);
                whereCondition += " AND HeaderId = " + headerId;

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                bool isEditable = param.IsVisible;
                string LineStatus = param.MTStatus;
                var lstResult = db.SP_IPI_GET_TESTPLAN_ELEMENT_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                List<SelectItemList> lst = new List<SelectItemList>();
                var lst12Ele = db.QMS023.Where(x => x.HeaderId == headerId).ToList();
                bool is12Element = false;
                if (lst12Ele.Count() == 12)
                {
                    is12Element = true;
                }
                int newRecordId = 0;
                var newRecord = new[] {
                                   Helper.GenerateHidden(newRecordId,"HeaderId",param.Headerid),
                                   Convert.ToString(newRecordId),
                                   Helper.HTMLAutoComplete(newRecordId, "txtElement","","",false,"","Element")+""+Helper.GenerateHidden(newRecordId,"Element"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqMinValue", "", "", false, "", false, "10"),
                                   Helper.GenerateNumericTextbox(newRecordId, "ReqMaxValue",   "", "", false, "",false, "8"),
                                   Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveNewElement();" ),
                                };
                NDEModels objNDEModels = new NDEModels();
                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.LineId),
                                isEditable ? Helper.HTMLAutoComplete(uc.LineId, "txtElement", objNDEModels.GetCategory("Element",uc.Element, uc.BU.Trim(), uc.Location.Trim(),true).CategoryDescription,"UpdateData(this, "+ uc.LineId +","+ uc.HeaderId +",true);",false,"","Element",isEditable ? false:true,"","required")+""+Helper.GenerateHidden(uc.LineId,"Element",uc.Element) : Convert.ToString(objNDEModels.GetCategory("Element",uc.Element, uc.BU.Trim(), uc.Location.Trim(),true).CategoryDescription),
                                isEditable ? Helper.GenerateNumericTextbox(uc.LineId, "ReqMinValue", Convert.ToString(uc.ReqMinValue), "UpdateData(this, " + uc.LineId + ","+ uc.HeaderId +",true);",isEditable ? false:true,"",false , "8","") : Convert.ToString(uc.ReqMinValue),
                                isEditable ?Helper.GenerateNumericTextbox(uc.LineId, "ReqMaxValue",    Convert.ToString(uc.ReqMaxValue),"UpdateData(this, "+ uc.LineId+","+ uc.HeaderId +",true);",isEditable ? false:true,"",false , "8","") : Convert.ToString(uc.ReqMaxValue),
                                "<nobr>" + Helper.GenerateActionIcon(uc.LineId, "Delete", "Delete Record", "fa fa-trash-o", "DeleteElement("+uc.LineId+")","",isEditable ? false:true)+"</nobr>"
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                    is12Element = is12Element
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }
        //Save New Record
        [HttpPost]
        public ActionResult SaveNewTestPlanElement(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int newRowIndex = 0;
            try
            {
                int planId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                QMS023 objQMS023 = new QMS023();
                QMS022 objQMS022 = db.QMS022.Where(x => x.HeaderId == planId).FirstOrDefault();
                QMS021 objQMS021 = db.QMS021.Where(i => i.LineId == objQMS022.TPLineId).FirstOrDefault();
                if (objQMS021.QMS020.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                {
                    if (isElementExistinTP(fc["Element" + newRowIndex], planId, 0, objQMS022.TPRevNo))
                    {
                        objResponseMsg.Key = false;
                        objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                    }
                    else
                    {
                        objQMS023.HeaderId = objQMS022.HeaderId;
                        objQMS023.TPLineId = objQMS022.TPLineId;
                        objQMS023.Project = objQMS022.Project;
                        objQMS023.QualityProject = objQMS022.QualityProject;
                        objQMS023.BU = objQMS022.BU;
                        objQMS023.Location = objQMS022.Location;
                        objQMS023.SeamNo = objQMS022.SeamNo;
                        objQMS023.TPRevNo = objQMS022.QMS021.QMS020.RevNo;
                        objQMS023.StageCode = objQMS022.StageCode;
                        objQMS023.StageSequence = objQMS022.StageSequence;
                        objQMS023.Element = fc["Element" + newRowIndex];
                        objQMS023.ReqMinValue = Convert.ToDouble(fc["ReqMinValue" + newRowIndex]);
                        objQMS023.ReqMaxValue = Convert.ToDouble(fc["ReqMaxValue" + newRowIndex]);
                        objQMS023.CreatedBy = objClsLoginInfo.UserName;
                        objQMS023.CreatedOn = DateTime.Now;
                        if (objQMS021.QMS020.Status == clsImplementationEnum.ICLSeamStatus.Approved.GetStringValue())
                        {
                            objQMS021.QMS020.RevNo = Convert.ToInt32(objQMS021.QMS020.RevNo) + 1;
                            objQMS021.QMS020.Status = clsImplementationEnum.ICLSeamStatus.Draft.GetStringValue();
                            objQMS021.QMS020.EditedBy = objClsLoginInfo.UserName;
                            objQMS021.QMS020.EditedOn = DateTime.Now;
                            objQMS021.QMS020.SubmittedBy = null;
                            objQMS021.QMS020.SubmittedOn = null;
                            objQMS021.QMS020.ApprovedBy = null;
                            objQMS021.QMS020.ApprovedOn = null;
                            objQMS021.QMS020.ReturnedBy = null;
                            objQMS021.QMS020.ReturnedOn = null; objQMS021.QMS020.ReturnRemark = null;
                        }

                        if (objQMS021.StageStatus == clsImplementationEnum.QualityIdStageStatus.APPROVED.GetStringValue())
                        {
                            objQMS021.StageStatus = clsImplementationEnum.QualityIdStageStatus.MODIFIED.GetStringValue();
                            objQMS021.EditedBy = objClsLoginInfo.UserName;
                            objQMS021.EditedOn = DateTime.Now;
                        }

                        db.QMS023.Add(objQMS023);
                        db.SaveChanges();

                        objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Insert;
                    }
                }
                else
                {
                    objResponseMsg.Remarks = "Seam is already submitted, it cannot be add/modify now!!";
                }
                objResponseMsg.Key = true;
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateTestPlanElement(int lineId, int headerid, string columnName, string columnValue)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string tableName = "QMS023";
                QMS022 objQMS022 = db.QMS022.Where(x => x.HeaderId == headerid).FirstOrDefault();
                QMS021 objQMS021 = db.QMS021.Where(x => x.LineId == objQMS022.TPLineId).FirstOrDefault();
                if (objQMS021.QMS020.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                {
                    if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                    {
                        if (objQMS022 != null ? isElementExistinTP(columnValue, headerid, lineId, objQMS022.TPRevNo) : false)
                        {
                            objResponseMsg.Key = false;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.Duplicate;
                        }
                        else
                        {
                            db.SP_COMMON_LINES_UPDATE(lineId, columnName, columnValue, tableName, objClsLoginInfo.UserName);
                            objResponseMsg.Key = true;
                            objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();
                        }
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Remarks = clsImplementationMessage.CommonMessages.NotInEditStatus;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteTestPlanElement(int id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS023 objQMS023 = db.QMS023.Where(x => x.LineId == id).FirstOrDefault();
                QMS021 objQMS021 = db.QMS021.Where(x => x.LineId == objQMS023.TPLineId).FirstOrDefault();
                if (objQMS021.QMS020.Status != clsImplementationEnum.QualityIdHeaderStatus.SentForApproval.GetStringValue())
                {
                    if (objQMS023 != null)
                    {
                        var planId = objQMS023.HeaderId;
                        db.QMS023.Remove(objQMS023);
                        db.SaveChanges();

                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Element deleted successfully.";
                    }
                    else
                    {
                        objResponseMsg.Key = true;
                        objResponseMsg.Value = "Details not available for delete.";
                    }
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Remarks = "Seam is already submitted, it cannot be delete now!!";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Chemical Test Details
        public ActionResult ChemicalTestDetails(int headerid = 0, int iteration = 0, string role = "", bool isEditable = false, int reqid = 0, bool isHistory = false)
        {
            QMS040 objQMS040 = new QMS040();
            string Instrument = string.Empty, InstrumentDesc = string.Empty;
            if (headerid > 0)
            {
                objQMS040 = db.QMS040.Where(i => i.HeaderId == headerid).FirstOrDefault();
            }
            List<GLB002> lstInstrument = Manager.GetSubCatagories("Instrument Group", objQMS040.BU, objQMS040.Location, null);
            var InstrumentList = lstInstrument.AsEnumerable().Select(x => new { CatID = x.Code, CatDesc = x.Description, id = x.Code, text = x.Description }).ToList();

            if (reqid > 0)
            {
                var chemDetail = db.QMS051.Where(i => i.RefRequestId == reqid).ToList();

                if (chemDetail.Count > 0)
                {
                    chemDetail.ForEach(x => x.IterationNo = iteration);
                    Instrument = chemDetail.Select(x => x.Instrument).FirstOrDefault();
                    InstrumentDesc = InstrumentList.Count() > 0 ? InstrumentList.Where(x => x.CatID == Instrument).Select(x => x.CatDesc).FirstOrDefault() : "";
                    db.SaveChanges();
                }
            }
          

            string InstrumentCode = Instrument;
            if (!string.IsNullOrWhiteSpace(InstrumentCode))
            {
                string[] arr = Instrument.Split(',');
                ViewBag.InstrumentDesc = string.Join(",", lstInstrument.Where(x => arr.Contains(x.Code)).Select(x => x.Description).ToList());
            }
            ViewBag.InstrumentCode = InstrumentCode;

            ViewBag.InstrumentList = InstrumentList;
            ViewBag.InstrumentDesc = InstrumentDesc;

            ViewBag.Stage = db.QMS002.Where(x => x.StageCode == objQMS040.StageCode && x.BU == objQMS040.BU && x.Location == objQMS040.Location).Select(x => x.StageCode + "-" + x.StageDesc).FirstOrDefault();
            ViewBag.iteration = iteration;
            ViewBag.Project = db.COM001.Where(i => i.t_cprj.Equals(objQMS040.Project, StringComparison.OrdinalIgnoreCase)).Select(i => i.t_cprj + "-" + i.t_dsca).FirstOrDefault();
            ViewBag.Location = db.COM002.Where(a => a.t_dimx == objQMS040.Location).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.BU = db.COM002.Where(a => a.t_dimx == objQMS040.BU).Select(a => a.t_dimx + " - " + a.t_desc).FirstOrDefault();
            ViewBag.Role = role;
            ViewBag.reqid = reqid;
            ViewBag.isEditable = isEditable;
            ViewBag.isHistory = isHistory;
            return PartialView("_LoadChemicalTestDetailsPartial", objQMS040);
        }

        public ActionResult GetChemicalTestDataPartial(int headerid, string tabname, string flag, string role, string reqid, string isHistory)
        {
            QMS040 objQMS040 = db.QMS040.Where(o => o.HeaderId == headerid).FirstOrDefault();
            ViewBag.HeaderId = headerid;
            ViewBag.tabname = tabname;
            ViewBag.isEditable = flag;
            ViewBag.Role = role;
            ViewBag.isHistory = isHistory;

            string[] arrElement = new string[12];
            string[] arrMin = new string[12];
            string[] arrMax = new string[12];
            NDEModels objNDEModels = new NDEModels();
            int j = 0;
            if (objQMS040 != null)
            {
                var objTestPlan = db.QMS021.Where(x => x.QualityProject == objQMS040.QualityProject && x.SeamNo == objQMS040.SeamNo && x.Location == objQMS040.Location && x.StageCode == objQMS040.StageCode && x.StageSequance == objQMS040.StageSequence).FirstOrDefault();
                if (objTestPlan != null)
                {
                    var lstQMS023 = objTestPlan != null ? db.QMS023.Where(i => i.TPLineId == objTestPlan.LineId).OrderByDescending(x => x.TPRevNo).ToList() : null;
                    if (lstQMS023.Count() > 0 && lstQMS023 != null)
                    {
                        #region Fetch element data from Test Plan(QMS022/QMS023)
                        foreach (var item in lstQMS023)
                        {
                            arrElement[j] = objNDEModels.GetCategory("Element", item.Element, item.BU.Trim(), item.Location.Trim(), true).CategoryDescription;
                            arrMin[j] = item.ReqMinValue.ToString();
                            arrMax[j] = item.ReqMaxValue.ToString();
                            if (j == 11)
                            {
                                break;
                            }
                            j = j + 1;
                        }
                        #endregion
                    }
                    else
                    {
                        #region Fecth from quality id (QMS018/QMS019)
                        List<string> lstQproj = new List<string>();
                        lstQproj.Add(objQMS040.QualityProject);
                        lstQproj.AddRange(db.QMS017.Where(c => c.BU == objQMS040.BU && c.Location == objQMS040.Location && c.IQualityProject == objQMS040.QualityProject).Select(x => x.QualityProject).ToList());

                        var QualityId = db.QMS020.FirstOrDefault(x => x.QualityProject == objQMS040.QualityProject && x.SeamNo == objQMS040.SeamNo && x.Location == objQMS040.Location).QualityId;
                        var Qrev = db.QMS015.FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == objQMS040.Location).RevNo;

                        if (!string.IsNullOrWhiteSpace(QualityId))
                        {
                            string approved = clsImplementationEnum.QualityIdStageStatus.APPROVED.GetStringValue();
                            QMS016_Log objQIDLine = new QMS016_Log();
                            objQIDLine = (from a in db.QMS015_Log
                                          join x in db.QMS016_Log on a.Id equals x.RefId
                                          where x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == objTestPlan.Location && x.StageCode == objTestPlan.StageCode && x.StageSequance == objTestPlan.StageSequance && a.Status == approved
                                          select x).FirstOrDefault();
                            if (objQIDLine == null)
                            {
                                objQIDLine = (from a in db.QMS015_Log
                                              join x in db.QMS016_Log on a.Id equals x.RefId
                                              where x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == objTestPlan.Location && x.StageCode == objTestPlan.StageCode && a.Status == approved
                                              select x).FirstOrDefault();
                            }
                            // var objQIDLine = db.QMS016_Log.OrderByDescending(x => x.RevNo).FirstOrDefault(x => x.QualityId == QualityId && lstQproj.Contains(x.QualityProject) && x.Location == objQMS021.Location && x.StageCode == objQMS021.StageCode && x.StageSequance == objQMS021.StageSequance && x.StageStatus == approved);
                            QMS022 objQMS022 = new QMS022();

                            if (objQIDLine != null && QualityId != null)
                            {
                                QMS018 objQMS018 = db.QMS018.Where(i => lstQproj.Contains(i.QualityProject)
                                                        && i.BU == objQMS040.BU && i.Location == objQMS040.Location && i.QualityId == QualityId && i.QualityIdRev == objQIDLine.RevNo
                                                        && i.StageCode == objQMS040.StageCode && i.StageSequence == objQMS040.StageSequence
                                                    ).FirstOrDefault();
                                if (objQMS018 == null)
                                {
                                    objQMS018 = db.QMS018.Where(i => lstQproj.Contains(i.QualityProject)
                                                    && i.BU == objQMS040.BU && i.Location == objQMS040.Location && i.QualityId == QualityId && i.QualityIdRev == objQIDLine.RevNo
                                                    && i.StageCode == objQMS040.StageCode
                                                ).FirstOrDefault();
                                }
                                if (objQMS018 != null)
                                {
                                    AddElementsinTestplan(objQMS018, objTestPlan);
                                    objQMS022 = db.QMS022.Where(i => i.TPLineId == objTestPlan.LineId).OrderByDescending(x => x.TPRevNo).FirstOrDefault();
                                    if (objQMS022 != null)
                                    {
                                        var lstaQMS023 = objTestPlan != null ? db.QMS023.Where(i => i.TPLineId == objTestPlan.LineId).OrderByDescending(x => x.TPRevNo).ToList() : null;
                                        if (lstaQMS023.Count() > 0 && lstaQMS023 != null)
                                        {
                                            #region Fetch element data from Test Plan(QMS022/QMS023)
                                            foreach (var item in lstaQMS023)
                                            {
                                                arrElement[j] = objNDEModels.GetCategory("Element", item.Element, item.BU.Trim(), item.Location.Trim(), true).CategoryDescription;
                                                arrMin[j] = item.ReqMinValue.ToString();
                                                arrMax[j] = item.ReqMaxValue.ToString();
                                                if (j == 11)
                                                {
                                                    break;
                                                }
                                                j = j + 1;
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            List<QMS019> lstaQMS019 = new List<QMS019>();
                                            lstaQMS019 = db.QMS019.Where(i => lstQproj.Contains(i.QualityProject)
                                                                    && i.BU == objQMS040.BU && i.Location == objQMS040.Location && i.QualityId == QualityId && i.QualityIdRev == Qrev
                                                                    && i.StageCode == objQMS040.StageCode
                                                                ).ToList();
                                            if (lstaQMS019.Count() > 0)
                                            {
                                                foreach (var item in lstaQMS019)
                                                {
                                                    arrElement[j] = objNDEModels.GetCategory("Element", item.Element, item.BU.Trim(), item.Location.Trim(), true).CategoryDescription;
                                                    arrMin[j] = item.ReqMinValue.ToString();
                                                    arrMax[j] = item.ReqMaxValue.ToString();
                                                    if (j == 11)
                                                    {
                                                        break;
                                                    }
                                                    j = j + 1;
                                                }
                                            }
                                        }

                                    }
                                    #endregion

                                }
                            }
                        }
                    }
                }

                ViewBag.Elements = arrElement;
                ViewBag.ReqMinValue = arrMin;
                ViewBag.ReqMaxValue = arrMax;

                var lstWeldingProcess = db.WPS025.Where(x => x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project && x.SeamNo == objQMS040.SeamNo && x.BU == objQMS040.BU)
                                       .Select(x => x.WeldingProcess)
                                       .Distinct().ToList();

                ViewBag.WeldingProcess = lstWeldingProcess.AsEnumerable().Select(x => new CategoryData { Value = x.ToString(), Code = x.ToString(), CategoryDescription = x.ToString() }).ToList();
                ViewBag.lstPosition = Manager.GetSubCatagories("Position", objQMS040.BU, objQMS040.Location).Select(i => new CategoryData { Value = i.Description, Code = i.Description, CategoryDescription = i.Description }).Distinct().ToList();
            }
            if (tabname == "Samples")
            {
                ViewBag.redirect = "GetSampleData";
            }
            else { ViewBag.redirect = "GetWelderData"; }
            ViewBag.reqid = reqid;
            return PartialView("_GetChemicalGridDataPartial");
        }

        public int AddElementsinTestplan(QMS018 objQMS018, QMS021 objTestPlan)
        {
            QMS022 objQMS022 = new QMS022();
            #region Add record in test plan
            if (objQMS018 != null)
            {
                objQMS022.TPLineId = objTestPlan.LineId;
                objQMS022.Project = objTestPlan.Project;
                objQMS022.QualityProject = objTestPlan.QualityProject;
                objQMS022.BU = objTestPlan.BU;
                objQMS022.Location = objTestPlan.Location;
                objQMS022.SeamNo = objTestPlan.SeamNo;
                objQMS022.TPRevNo = objTestPlan.QMS020.RevNo;
                objQMS022.StageCode = objTestPlan.StageCode;
                objQMS022.StageSequence = objTestPlan.StageSequance;
                objQMS022.Depth = objQMS018.Depth;
                objQMS022.Component = objQMS018.Component;
                objQMS022.NoOfComponents = objQMS018.NoOfComponents;
                objQMS022.Procedure = objQMS018.Procedure;
                objQMS022.NoOfProcedures = objQMS018.NoOfProcedures;
                objQMS022.Welder = objQMS018.Welder;
                objQMS022.NoOfWelders = objQMS018.NoOfWelders;
                objQMS022.Position = objQMS018.Position;
                objQMS022.NoOfPositions = objQMS018.NoOfPositions;
                objQMS022.CreatedBy = objClsLoginInfo.UserName;
                objQMS022.CreatedOn = DateTime.Now;
                db.QMS022.Add(objQMS022);
                db.SaveChanges();
                List<QMS019> lstQMS019 = db.QMS019.Where(x => x.HeaderId == objQMS018.HeaderId).ToList();
                if (lstQMS019 != null)
                {
                    db.QMS023.AddRange(
                                    lstQMS019.Select(x =>
                                    new QMS023
                                    {
                                        TPLineId = objTestPlan.LineId,
                                        Project = objTestPlan.Project,
                                        QualityProject = objTestPlan.QualityProject,
                                        BU = objTestPlan.BU,
                                        Location = objTestPlan.Location,
                                        SeamNo = objTestPlan.SeamNo,
                                        TPRevNo = objTestPlan.QMS020.RevNo,
                                        StageCode = objTestPlan.StageCode,
                                        StageSequence = objTestPlan.StageSequance,
                                        HeaderId = objQMS022.HeaderId,
                                        Element = x.Element,
                                        ReqMinValue = x.ReqMinValue,
                                        ReqMaxValue = x.ReqMaxValue,
                                        CreatedBy = objClsLoginInfo.UserName,
                                        CreatedOn = DateTime.Now,
                                    })
                                    );
                }
                db.SaveChanges();
            }
            #endregion

            return objQMS022.HeaderId;
        }

        //samples and welders
        [HttpPost]
        public ActionResult GetSampleData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;
                int headerId = Convert.ToInt32(param.Headerid);
                whereCondition += " AND HeaderId = " + headerId;
                if (param.Roles == "PROD")
                {
                    int? reqid = db.QMS051.Where(x => x.HeaderId == headerId).OrderByDescending(x => x.Id).Select(x => x.RefRequestId).FirstOrDefault();
                    if (reqid > 0)
                    {
                        whereCondition += " AND RefRequestId = " + reqid;
                    }
                }
                else
                {
                    if (param.CTQLineHeaderId > 0)
                    {
                        if (param.isHistory)
                        {
                            whereCondition += " AND RefRequestId <> " + param.CTQLineHeaderId;
                        }
                        else
                        {
                            whereCondition += " AND RefRequestId = " + param.CTQLineHeaderId;
                        }
                    }
                }
                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                bool isEditable = param.IsVisible;
                bool isQC = false;
                bool isPROD = false;
                if (param.Roles == "PROD" && isEditable && !param.isHistory)
                {
                    isPROD = true;
                }
                if (param.Roles == "QC" && isEditable && !param.isHistory)
                {
                    isQC = true;
                }
                var lstResult = db.SP_IPI_GET_CHEMICAL_TEST_DETAIL_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                List<SelectItemList> lst = new List<SelectItemList>();

                isEditable = true;
                int newRecordId = 0;
                var newRecord = new[] {
                                   Helper.GenerateHidden(newRecordId,"HeaderId",param.Headerid),
                                   Helper.GenerateHidden(newRecordId,"RefRequestId",Convert.ToString(param.CTQLineHeaderId)),
                                   Convert.ToString(newRecordId),
                                   Helper.HTMLAutoComplete(newRecordId, "txtWeldingProcess","","",false,"","WeldingProcess")+""+Helper.GenerateHidden(newRecordId,"WeldingProcess"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Sample", "", "", false, "", false, "8","","intnumber"),
                                   Helper.HTMLAutoComplete(newRecordId, "txtPosition","","",false,"","Position")+""+Helper.GenerateHidden(newRecordId,"Position"),
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveFromSampleForm();" ),
                                };
                NDEModels objNDEModels = new NDEModels();
                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.HeaderId),
                                Convert.ToString(uc.RefRequestId),
                                Convert.ToString(uc.Id),
                                isPROD ? Helper.HTMLAutoComplete(uc.Id, "txtWeldingProcess", Convert.ToString(uc.WeldingProcess),"UpdateData(this, "+ uc.Id +","+ uc.HeaderId +",true);",false,"","WeldingProcess",isPROD ? false:true,"","required")+""+Helper.GenerateHidden(uc.Id,"WeldingProcess",uc.WeldingProcess) : Convert.ToString(uc.WeldingProcess),
                                isPROD ? Helper.GenerateNumericTextbox(uc.Id, "Sample", Convert.ToString(uc.Sample), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isPROD ? false:true,"",false , "8","","intnumber") : Convert.ToString(uc.Sample),
                                isPROD ?Helper.HTMLAutoComplete(uc.Id, "txtPosition", Convert.ToString(objNDEModels.GetCategory("Position",uc.Position, uc.BU.Trim(), uc.Location.Trim(),true).CategoryDescription),"UpdateData(this, "+ uc.Id +","+ uc.HeaderId +",true);",false,"","Position",isPROD ? false:true,"","required")+""+Helper.GenerateHidden(uc.Id,"Position",uc.Position) : Convert.ToString(objNDEModels.GetCategory("Position",uc.Position, uc.BU.Trim(), uc.Location.Trim(),true).CategoryDescription),
                                isQC ? Helper.GenerateNumericTextbox(uc.Id, "ExaminationResult1", Convert.ToString(uc.ExaminationResult1), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isQC ? false:true,"",false , "8","") :  Convert.ToString(uc.ExaminationResult1),
                                isQC ? Helper.GenerateNumericTextbox(uc.Id, "ExaminationResult2", Convert.ToString(uc.ExaminationResult2), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isQC ? false:true,"",false , "8","") :  Convert.ToString(uc.ExaminationResult2),
                                isQC ? Helper.GenerateNumericTextbox(uc.Id, "ExaminationResult3", Convert.ToString(uc.ExaminationResult3), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isQC ? false:true,"",false , "8","") :  Convert.ToString(uc.ExaminationResult3),
                                isQC ? Helper.GenerateNumericTextbox(uc.Id, "ExaminationResult4", Convert.ToString(uc.ExaminationResult4), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isQC ? false:true,"",false , "8","") :  Convert.ToString(uc.ExaminationResult4),
                                isQC ? Helper.GenerateNumericTextbox(uc.Id, "ExaminationResult5", Convert.ToString(uc.ExaminationResult5), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isQC ? false:true,"",false , "8","") :  Convert.ToString(uc.ExaminationResult5),
                                isQC ? Helper.GenerateNumericTextbox(uc.Id, "ExaminationResult6", Convert.ToString(uc.ExaminationResult6), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isQC ? false:true,"",false , "8","") :  Convert.ToString(uc.ExaminationResult6),
                                isQC ? Helper.GenerateNumericTextbox(uc.Id, "ExaminationResult7", Convert.ToString(uc.ExaminationResult7), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isQC ? false:true,"",false , "8","") :  Convert.ToString(uc.ExaminationResult7),
                                isQC ? Helper.GenerateNumericTextbox(uc.Id, "ExaminationResult8", Convert.ToString(uc.ExaminationResult8), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isQC ? false:true,"",false , "8","") :  Convert.ToString(uc.ExaminationResult8),
                                isQC ? Helper.GenerateNumericTextbox(uc.Id, "ExaminationResult9", Convert.ToString(uc.ExaminationResult9), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isQC ? false:true,"",false , "8","") :  Convert.ToString(uc.ExaminationResult9),
                                isQC ? Helper.GenerateNumericTextbox(uc.Id,"ExaminationResult10", Convert.ToString(uc.ExaminationResult10), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isQC ? false:true,"",false , "8","") : Convert.ToString(uc.ExaminationResult10),
                                isQC ? Helper.GenerateNumericTextbox(uc.Id,"ExaminationResult11", Convert.ToString(uc.ExaminationResult11), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isQC ? false:true,"",false , "8","") : Convert.ToString(uc.ExaminationResult11),
                                isQC ? Helper.GenerateNumericTextbox(uc.Id,"ExaminationResult12", Convert.ToString(uc.ExaminationResult12), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isQC ? false:true,"",false , "8","") : Convert.ToString(uc.ExaminationResult12),
                                "<nobr>" + Helper.GenerateActionIcon(uc.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteData("+uc.Id+", $('#tblSampleList'))","",isPROD ? false:true)+"</nobr>"
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        //results
        [HttpPost]
        public ActionResult GetWelderData(JQueryDataTableParamModel param)
        {
            try
            {
                var isLocationSortable = Convert.ToBoolean(Request["bSortable_1"]);
                var sortColumnIndex = Convert.ToInt32(Request["iSortCol_0"]);

                string sortColumnName = Convert.ToString(Request["sColumns"].Split(',')[sortColumnIndex]);
                string sortDirection = Convert.ToString(Request["sSortDir_0"]);

                int StartIndex = param.iDisplayStart + 1;
                int EndIndex = param.iDisplayStart + param.iDisplayLength;
                string whereCondition = "1=1";

                string strSortOrder = string.Empty;
                int headerId = Convert.ToInt32(param.Headerid);
                whereCondition += " AND HeaderId = " + headerId;

                if (!string.IsNullOrWhiteSpace(sortColumnName))
                {
                    strSortOrder = " Order By " + sortColumnName + " " + sortDirection + " ";
                }
                bool isEditable = param.IsVisible;
                if (param.CTQLineHeaderId > 0)
                {
                    if (param.isHistory)
                    {
                        whereCondition += " AND RefRequestId <> " + param.CTQLineHeaderId;
                    }
                    else if (param.Roles == "PROD")
                    {
                        int? reqid = db.QMS051.Where(x => x.HeaderId == headerId).OrderByDescending(x => x.Id).Select(x => x.RefRequestId).FirstOrDefault();
                        if (reqid > 0)
                        {
                            whereCondition += " AND RefRequestId = " + reqid;
                        }
                    }
                    else
                    {
                        whereCondition += " AND RefRequestId = " + param.CTQLineHeaderId;
                    }
                }
                bool isPROD = false;
                if (param.Roles == "PROD" && isEditable && !param.isHistory)
                {
                    isPROD = true;
                }

                var lstResult = db.SP_IPI_GET_CHEMICAL_TEST_DETAIL_DATA(StartIndex, EndIndex, strSortOrder, whereCondition).ToList();
                List<SelectItemList> lst = new List<SelectItemList>();
                int newRecordId = 0;
                var newRecord = new[] {
                                   Helper.GenerateHidden(newRecordId, "HeaderId", param.Headerid),
                                   Helper.GenerateHidden(newRecordId,"RefRequestId",Convert.ToString(param.CTQLineHeaderId)),
                                   Convert.ToString(newRecordId),
                                   Helper.HTMLAutoComplete(newRecordId, "txtWeldingProcess", "", "", false, "", "WeldingProcess") + "" + Helper.GenerateHidden(newRecordId, "WeldingProcess"),
                                   Helper.GenerateNumericTextbox(newRecordId, "Sample", "", "", false, "", false, "8","","intnumber"),
                                   Helper.HTMLAutoComplete(newRecordId, "txtPosition", "", "", false, "", "Position") + "" + Helper.GenerateHidden(newRecordId, "Position"),
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   "",
                                   Helper.GenerateGridButtonNew(newRecordId, "Save", "Save Rocord", "fa fa-save", "SaveFromWelderForm();"),
                                };
                var data = (from uc in lstResult
                            select new[]
                            {
                                Convert.ToString(uc.HeaderId), Convert.ToString(uc.RefRequestId),
                                Convert.ToString(uc.Id),
                                isPROD ? Helper.HTMLAutoComplete(uc.Id, "txtWeldingProcess", Convert.ToString(uc.WeldingProcess),"UpdateData(this, "+ uc.Id +","+ uc.HeaderId +",true);",false,"","WeldingProcess",isEditable ? false:true,"","required")+""+Helper.GenerateHidden(uc.Id,"WeldingProcess",uc.WeldingProcess) : Convert.ToString(uc.WeldingProcess),
                                isPROD ? Helper.GenerateNumericTextbox(uc.Id, "Sample", Convert.ToString(uc.Sample), "UpdateData(this, " + uc.Id + ","+ uc.HeaderId +",true);",isEditable ? false:true,"",false , "8","","intnumber") : Convert.ToString(uc.Sample),
                                isPROD ?Helper.HTMLAutoComplete(uc.Id, "txtPosition", Convert.ToString(uc.Position),"UpdateData(this, "+ uc.Id +","+ uc.HeaderId +",true);",false,"","Position",isEditable ? false:true,"","required")+""+Helper.GenerateHidden(uc.Id,"Position",uc.Position) : Convert.ToString(uc.Position),
                                Convert.ToString(uc.Welder1),
                                Convert.ToString(uc.Welder2),
                                Convert.ToString(uc.Welder3),
                                Convert.ToString(uc.Welder4),
                                Convert.ToString(uc.Welder5),
                                Convert.ToString(uc.Welder6),
                                Convert.ToString(uc.Welder7),
                                Convert.ToString(uc.Welder8),
                                Convert.ToString(uc.Welder9),
                                Convert.ToString(uc.Welder10),
                                Convert.ToString(uc.Welder11),
                                Convert.ToString(uc.Welder12),
                                Convert.ToString(uc.Welder13),
                                Convert.ToString(uc.Welder14),
                                Convert.ToString(uc.Welder15),
                                Convert.ToString(uc.Welder16),
                                Convert.ToString(uc.Welder17),
                                Convert.ToString(uc.Welder18),
                                Convert.ToString(uc.Welder19),
                                Convert.ToString(uc.Welder20),
                                Convert.ToString(uc.Welder21),
                                Convert.ToString(uc.Welder22),
                                Convert.ToString(uc.Welder23),
                                Convert.ToString(uc.Welder24),
                                "<nobr>" + Helper.GenerateActionIcon(uc.Id, "Delete", "Delete Record", "fa fa-trash-o", "DeleteData("+uc.Id+", $('#tblWelderList'))","",isPROD ? false:true)+"</nobr>"
                           }).ToList();

                data.Insert(0, newRecord);

                return Json(new
                {
                    sEcho = Convert.ToInt32(param.sEcho),
                    iTotalRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    iTotalDisplayRecords = (lstResult.Count > 0 ? lstResult.FirstOrDefault().TotalCount : 0),
                    aaData = data,
                    strSortOrder = strSortOrder,
                    whereCondition = whereCondition,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                return Json(new
                {
                    sEcho = param.sEcho,
                    iTotalRecords = "0",
                    iTotalDisplayRecords = "0",
                    aaData = ""
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SaveWelderData(FormCollection fc)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            int newRowIndex = 0;
            try
            {
                int planId = Convert.ToInt32(fc["Id" + newRowIndex]);
                int? HeaderId = Convert.ToInt32(fc["HeaderId" + newRowIndex]);
                QMS051 objQMS051 = new QMS051();
                QMS040 objQMS040 = db.QMS040.Where(o => o.HeaderId == HeaderId).FirstOrDefault();
                int reqid = Convert.ToInt32(fc["RefRequestId" + newRowIndex]);
                int Id = Convert.ToInt32(fc["Id" + newRowIndex]);
                if (reqid > 0)
                {
                    QMS050 obQMS050 = db.QMS050.Where(x => x.RequestId == reqid).FirstOrDefault();

                    if (obQMS050 != null)
                    {
                        objQMS051.StageSequence = obQMS050.StageSequence;
                        objQMS051.IterationNo = obQMS050.IterationNo;
                        objQMS051.RefRequestId = Convert.ToInt32(fc["RefRequestId" + newRowIndex]);
                        objQMS051.RequestNo = obQMS050.RequestNo;
                        objQMS051.RequestNoSequence = obQMS050.RequestNoSequence;
                    }
                }
                else
                {
                    objQMS051.StageSequence = objQMS040.StageSequence;
                    objQMS051.IterationNo = objQMS040.IterationNo;
                }
                int Sample = Convert.ToInt32(fc["Sample" + newRowIndex]);

                if (db.QMS051.Any(x => x.Sample == Sample && x.RefRequestId == objQMS051.RefRequestId && x.HeaderId == HeaderId && x.Id != Id))
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Sample " + Sample + " already exists.";
                }
                else
                {
                    objQMS051.HeaderId = objQMS040.HeaderId;
                    objQMS051.QualityProject = objQMS040.QualityProject;
                    objQMS051.Project = objQMS040.Project;
                    objQMS051.BU = objQMS040.BU;
                    objQMS051.Location = objQMS040.Location;
                    objQMS051.SeamNo = objQMS040.SeamNo;
                    objQMS051.StageCode = objQMS040.StageCode;
                    objQMS051.WeldingProcess = fc["WeldingProcess" + newRowIndex];
                    objQMS051.Sample = Convert.ToInt32(fc["Sample" + newRowIndex]);
                    objQMS051.Position = fc["Position" + newRowIndex];

                    #region add welders
                    var lstWelders = db.WPS025.Where(x => x.WeldingProcess.Trim() == objQMS051.WeldingProcess.Trim() &&
                                                       x.QualityProject.Trim() == objQMS040.QualityProject.Trim() && x.Project.Trim() == objQMS040.Project.Trim()
                                                        && x.BU.Trim() == objQMS040.BU.Trim() && x.SeamNo.Trim() == objQMS040.SeamNo.Trim()
                                      ).Select(x => x.WelderStamp).Distinct().ToList();
                    var lstWelder = lstWelders.Take(24);
                    string[] arrElement = new string[24];
                    int i = 0;
                    foreach (var item in lstWelder)
                    {
                        arrElement[i] = item;
                        i = i + 1;
                        if (i > 23)
                        {
                            break;
                        }
                    }
                    objQMS051.Welder1 = arrElement[0];
                    objQMS051.Welder2 = arrElement[1];
                    objQMS051.Welder3 = arrElement[2];
                    objQMS051.Welder4 = arrElement[3];
                    objQMS051.Welder5 = arrElement[4];
                    objQMS051.Welder6 = arrElement[5];
                    objQMS051.Welder7 = arrElement[6];
                    objQMS051.Welder8 = arrElement[7];
                    objQMS051.Welder9 = arrElement[8];
                    objQMS051.Welder10 = arrElement[9];
                    objQMS051.Welder11 = arrElement[10];
                    objQMS051.Welder12 = arrElement[11];
                    objQMS051.Welder13 = arrElement[12];
                    objQMS051.Welder14 = arrElement[13];
                    objQMS051.Welder15 = arrElement[14];
                    objQMS051.Welder16 = arrElement[15];
                    objQMS051.Welder17 = arrElement[16];
                    objQMS051.Welder18 = arrElement[17];
                    objQMS051.Welder19 = arrElement[18];
                    objQMS051.Welder20 = arrElement[19];
                    objQMS051.Welder21 = arrElement[20];
                    objQMS051.Welder22 = arrElement[21];
                    objQMS051.Welder23 = arrElement[22];
                    objQMS051.Welder24 = arrElement[23];
                    #endregion

                    objQMS051.Element1 = fc["Element1"];
                    objQMS051.Element2 = fc["Element2"];
                    objQMS051.Element3 = fc["Element3"];
                    objQMS051.Element4 = fc["Element4"];
                    objQMS051.Element5 = fc["Element5"];
                    objQMS051.Element6 = fc["Element6"];
                    objQMS051.Element7 = fc["Element7"];
                    objQMS051.Element8 = fc["Element8"];
                    objQMS051.Element9 = fc["Element9"];
                    objQMS051.Element10 = fc["Element10"];
                    objQMS051.Element11 = fc["Element11"];
                    objQMS051.Element12 = fc["Element12"];

                    objQMS051.CreatedBy = objClsLoginInfo.UserName;
                    objQMS051.CreatedOn = DateTime.Now;
                    db.QMS051.Add(objQMS051);
                    db.SaveChanges();

                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.NDEConsumeMessage.Insert;
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.CTQMessages.Error.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult UpdateWelderData(int lineId, int headerid, string columnName, string columnValue, string reqid)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                string tableName = "QMS051";
                if (!string.IsNullOrEmpty(columnName) && !string.IsNullOrEmpty(columnValue))
                {
                    objResponseMsg.dataKey = true;
                    if (columnName.ToLower() == "sample")
                    {
                        int sample = Convert.ToInt32(columnValue);
                        int requestid = Convert.ToInt32(reqid);
                        if (db.QMS051.Any(x => x.Sample == sample && x.RefRequestId == requestid && x.Id != lineId))
                        {
                            objResponseMsg.dataKey = false;
                            objResponseMsg.Value = "Sample " + sample + " already exists.";
                        }
                    }

                    if (objResponseMsg.dataKey)
                    {
                        db.SP_COMMON_TABLE_UPDATE(tableName, Convert.ToInt32(lineId), "Id", columnName, columnValue, objClsLoginInfo.UserName);

                        if (columnName.ToLower() == "weldingprocess")
                        {
                            QMS040 objQMS040 = db.QMS040.Where(o => o.HeaderId == headerid).FirstOrDefault();
                            QMS051 objQMS051 = db.QMS051.Where(o => o.Id == lineId).FirstOrDefault();
                            var lstWelder = db.WPS025.Where(x => x.WeldingProcess == columnValue &&
                                                               x.QualityProject == objQMS040.QualityProject && x.Project == objQMS040.Project
                                                                && x.BU == objQMS040.BU && x.Location == objQMS040.Location && x.SeamNo == objQMS040.SeamNo
                                              ).Distinct().Take(24).ToList();
                            string[] arrWelder = new string[24];
                            int i = 0;
                            foreach (var item in lstWelder)
                            {
                                arrWelder[i] = item.WelderStamp;
                                i = i + 1;
                                if (i > 23)
                                {
                                    break;
                                }
                            }
                            objQMS051.Welder1 = arrWelder[0];
                            objQMS051.Welder2 = arrWelder[1];
                            objQMS051.Welder3 = arrWelder[2];
                            objQMS051.Welder4 = arrWelder[3];
                            objQMS051.Welder5 = arrWelder[4];
                            objQMS051.Welder6 = arrWelder[5];
                            objQMS051.Welder7 = arrWelder[6];
                            objQMS051.Welder8 = arrWelder[7];
                            objQMS051.Welder9 = arrWelder[8];
                            objQMS051.Welder10 = arrWelder[9];
                            objQMS051.Welder11 = arrWelder[10];
                            objQMS051.Welder12 = arrWelder[11];
                            objQMS051.Welder13 = arrWelder[12];
                            objQMS051.Welder14 = arrWelder[13];
                            objQMS051.Welder15 = arrWelder[14];
                            objQMS051.Welder16 = arrWelder[15];
                            objQMS051.Welder17 = arrWelder[16];
                            objQMS051.Welder18 = arrWelder[17];
                            objQMS051.Welder19 = arrWelder[18];
                            objQMS051.Welder20 = arrWelder[19];
                            objQMS051.Welder21 = arrWelder[20];
                            objQMS051.Welder22 = arrWelder[21];
                            objQMS051.Welder23 = arrWelder[22];
                            objQMS051.Welder24 = arrWelder[23];
                            db.SaveChanges();
                        }
                    }
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.HeaderUpdate.ToString();

                }

            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult DeleteWelderData(int id)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            try
            {
                QMS051 objQMS051 = db.QMS051.Where(x => x.Id == id).FirstOrDefault();
                if (objQMS051 != null)
                {
                    db.QMS051.Remove(objQMS051);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Welder Data deleted successfully.";
                }
                else
                {
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = "Details not available for delete.";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = ex.Message;

            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        public ActionResult UpdateInstrumentData(int reqid, string InstrumentCode)
        {
            clsHelper.ResponseMsg objResponseMsg = new clsHelper.ResponseMsg();
            try
            {

                var chemDetail = db.QMS051.Where(i => i.RefRequestId == reqid).ToList();
                if (chemDetail.Count > 0)
                {
                    chemDetail.ForEach(x => x.Instrument = InstrumentCode);
                    db.SaveChanges();
                    objResponseMsg.Key = true;
                    objResponseMsg.Value = clsImplementationMessage.CommonMessages.Update.ToString();
                }
                else
                {
                    objResponseMsg.Key = false;
                    objResponseMsg.Value = "Sample Test result data not found";
                }
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Common Methods
        public string GenerateLable(int rowId, string columnName, string columnValue = "", string inputStyle = "")
        {
            string strlbl = string.Empty;
            string lblID = columnName + "" + rowId.ToString();
            //string inputValue = columnValue;
            strlbl = "<label id='" + lblID + "'></label>";
            return strlbl;
        }
        public bool isElemetnExist(string element, int headerid, int lineid, int? qidRevno)
        {
            bool flag = false;
            List<QMS019> lstQMS019 = db.QMS019.Where(i => i.HeaderId == headerid && i.QualityIdRev == qidRevno && i.Element == element && i.LineId != lineid).ToList();
            if (lstQMS019.Count() > 0)
            {
                flag = true;
            }
            return flag;
        }
        public bool isElementExistinTP(string element, int headerid, int lineid, int? tprevno)
        {
            bool flag = false;
            List<QMS023> lstQMS023 = db.QMS023.Where(i => i.HeaderId == headerid && i.TPRevNo == tprevno && i.Element == element && i.LineId != lineid).ToList();
            if (lstQMS023.Count() > 0)
            {
                flag = true;
            }
            return flag;
        }
        #endregion
    }
}