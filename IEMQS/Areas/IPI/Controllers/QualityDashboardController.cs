﻿using IEMQS.Models;
using IEMQSImplementation;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IEMQS.Areas.IPI.Controllers
{
    public class QualityDashboardController : clsBase
    {
        // GET: IPI/QualityDashboard
        public ActionResult Index()
        {
            DateTime fromdate = Convert.ToDateTime("2019-01-01");
            DateTime todate = DateTime.Now;
            SP_IPI_FETCH_QUALITY_dASHBOARD_DATA_Result result = db.SP_IPI_FETCH_QUALITY_dASHBOARD_DATA(fromdate, todate, "HZW").FirstOrDefault();
            ViewBag.RTNDEData = result.RTTotalRepairPercentage.Value.ToString("G29");
            ViewBag.UTNDEData = result.UTTotalRepairPercentage.Value.ToString("G29");
            ViewBag.RTUTNDEData = result.RTUTTotalRepairPercentage.Value.ToString("G29");

            ViewBag.SetUpRejection = result.SetUpRejection.Value.ToString("G29");
            ViewBag.Averagetime = result.Averagetime;

            ViewBag.NoOfNCR = result.NoOfNCR;
            ViewBag.NoOfCFAR = result.NoOfCFAR;
            return View(result);
        }

        [HttpPost]
        public ActionResult GetRepairData(string type)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();
            
            try
            {
                string fromdate = "2019-01-01";
                string todate = DateTime.Now.ToString("yyyy-MM-dd");
                string location = "HZW";
                double value = FN_IPI_GET_TOTAL_REPAIR_LENGTH_RT(fromdate, todate, type, location);

                objResponseMsg.Key = true;
                objResponseMsg.Value = String.Format("{0:0.00}", value); 
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetSetupRejection(string type)
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                string fromdate = "2019-01-01";
                string todate = DateTime.Now.ToString("yyyy-MM-dd");
                string location = "HZW";
                decimal value = FN_IPI_GET_SETUP_REJECTION(fromdate, todate, type, location);

                objResponseMsg.Key = true;
                objResponseMsg.Value = String.Format("{0:0.00}", value);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult GetAvgTime()
        {
            clsHelper.ResponseMsgWithStatus objResponseMsg = new clsHelper.ResponseMsgWithStatus();

            try
            {
                string fromdate = "2019-01-01";
                string todate = DateTime.Now.ToString("yyyy-MM-dd");
                string location = "HZW";
                string value = FN_IPI_GET_AVG_TIME_BY_QC(fromdate, todate, location);

                objResponseMsg.Key = true;
                objResponseMsg.Value = String.Format("{0:0.00}", value);
            }
            catch (Exception ex)
            {
                Elmah.ErrorSignal.FromCurrentContext().Raise(ex);
                objResponseMsg.Key = false;
                objResponseMsg.Value = clsImplementationMessage.ExceptionMessages.Message.ToString();
            }
            return Json(objResponseMsg, JsonRequestBehavior.AllowGet);
        }

        #region function calls
        public double FN_IPI_GET_TOTAL_REPAIR_LENGTH_RT(string fromdate, string todate, string type, string location)
        {
            return db.Database.SqlQuery<double>("SELECT TotalRepairPercentage from dbo.FN_IPI_GET_TOTAL_REPAIR_LENGTH('" + fromdate + "','" + todate + "','"+ type + "','"+ location +"',0,0)").FirstOrDefault();
        }

        public decimal FN_IPI_GET_SETUP_REJECTION(string fromdate, string todate, string type, string location)
        {
            return db.Database.SqlQuery<decimal>("SELECT dbo.FN_IPI_GET_SETUP_REJECTION('" + fromdate + "','" + todate + "','" + type + "','" + location + "',0)").FirstOrDefault();
        }

        public string FN_IPI_GET_AVG_TIME_BY_QC(string fromdate, string todate, string location)
        {
            return db.Database.SqlQuery<string>("SELECT dbo.FN_IPI_GET_AVG_TIME_BY_QC('" + fromdate + "','" + todate + "','" + location + "',0)").FirstOrDefault();
        }

        #endregion
    }
}